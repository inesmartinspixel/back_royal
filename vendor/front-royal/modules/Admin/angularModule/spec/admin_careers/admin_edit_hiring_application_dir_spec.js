import 'AngularSpecHelper';
import 'Admin/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import 'Careers/angularModule/spec/_mock/fixtures/hiring_application_fixtures';
import 'Careers/angularModule/spec/_mock/fixtures/hiring_relationship_fixtures';
import 'Careers/angularModule/spec/_mock/fixtures/hiring_team_fixtures';
import 'Users/angularModule/spec/_mock/fixtures/users';
import fieldOptionsLocales from 'Careers/locales/careers/field_options-en.json';
import hiringManagerCardLocales from 'Careers/locales/careers/hiring_manager_card-en.json';
import editHiringManagerProfileLocales from 'Careers/locales/careers/edit_hiring_manager_profile-en.json';
import editCareerProfileLocales from 'Careers/locales/careers/edit_career_profile-en.json';
import companyFormLocales from 'Careers/locales/careers/edit_hiring_manager_profile/company_form-en.json';
import youFormLocales from 'Careers/locales/careers/edit_hiring_manager_profile/you_form-en.json';
import uploadAvatarLocales from 'FrontRoyalForm/locales/front_royal_form/inputs/upload_avatar-en.json';
import writeTextAboutLocales from 'FrontRoyalForm/locales/front_royal_form/inputs/write_text_about-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(
    fieldOptionsLocales,
    hiringManagerCardLocales,
    editHiringManagerProfileLocales,
    editCareerProfileLocales,
    companyFormLocales,
    youFormLocales,
    uploadAvatarLocales,
    writeTextAboutLocales,
);

describe('Admin::AdminEditHiringApplication', () => {
    let renderer;
    let CareerProfile;
    let users;
    let elem;
    let scope;
    let SpecHelper;
    let HiringApplication;
    let hiringApplication;
    let HiringRelationship;
    let existingHiringRelationships;
    let amDateFormat;
    let $location;
    let ngToast;
    let User;
    let HiringTeam;
    let hiringTeams;
    let CareerProfileList;
    let careerProfileLists;
    let options;
    let $httpBackend;
    let $window;
    let $timeout;
    let DialogModal;
    let LogInAs;
    let hiringManager;
    let OpenPosition;
    let ClientStorage;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Admin', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            SpecHelper.stubCurrentUser('admin');
            CareerProfile = $injector.get('CareerProfile');
            CareerProfileList = $injector.get('CareerProfileList');
            HiringApplication = $injector.get('HiringApplication');
            HiringRelationship = $injector.get('HiringRelationship');
            $httpBackend = $injector.get('$httpBackend');
            $window = $injector.get('$window');
            amDateFormat = $injector.get('$filter')('amDateFormat');
            $location = $injector.get('$location');
            $timeout = $injector.get('$timeout');
            ngToast = $injector.get('ngToast');
            User = $injector.get('User');
            DialogModal = $injector.get('DialogModal');
            LogInAs = $injector.get('LogInAs');
            HiringTeam = $injector.get('HiringTeam');
            OpenPosition = $injector.get('OpenPosition');
            ClientStorage = $injector.get('ClientStorage');

            $injector.get('UserFixtures');
            $injector.get('HiringTeamFixtures');
            $injector.get('HiringApplicationFixtures');
            $injector.get('HiringRelationshipFixtures');
            $injector.get('CareerProfileFixtures');

            const user1 = User.fixtures.getInstance(); // regular candidate WITHOUT a relationship with the hiring manager
            const user2 = User.fixtures.getInstance(); // candidate WITH a relationship with the hiring manager that's visible to candidate
            const user3 = User.fixtures.getInstance(); // candidate WITH a relationship with the hiring manager that's waiting on hiring manager
            const user4 = User.fixtures.getInstance(); // career profile list user WITHOUT a relationship with the hiring manager
            const createdAt = 1503433167;
            users = [
                _.extend(user1, {
                    career_profile: CareerProfile.fixtures.getInstance({
                        created_at: createdAt,
                        user_id: user1.id,
                    }),
                }),
                _.extend(user2, {
                    career_profile: CareerProfile.fixtures.getInstance({
                        created_at: createdAt,
                        user_id: user2.id,
                    }),
                }),
                _.extend(user3, {
                    career_profile: CareerProfile.fixtures.getInstance({
                        created_at: createdAt,
                        user_id: user3.id,
                    }),
                }),
                _.extend(user4, {
                    career_profile: CareerProfile.fixtures.getInstance({
                        created_at: createdAt,
                        user_id: user4.id,
                    }),
                }),
            ];

            hiringApplication = HiringApplication.fixtures.getInstance();
            existingHiringRelationships = [
                HiringRelationship.fixtures.getInstance({
                    candidate_id: user2.id,
                    hiring_manager_id: hiringApplication.user_id,
                }),
                HiringRelationship.fixtures.getInstance({
                    candidate_id: user3.id,
                    candidate_status: 'hidden',
                    hiring_manager_id: hiringApplication.user_id,
                    hiring_manager_status: 'pending',
                }),
            ];
            careerProfileLists = [
                CareerProfileList.new({
                    id: 'id',
                    name: 'Foo Bar',
                    career_profile_ids: [user4.career_profile.id],
                }),
            ];
            hiringTeams = [
                HiringTeam.fixtures.getInstance(),
                HiringTeam.fixtures.getInstance(),
                HiringTeam.fixtures.getInstance(),
                HiringTeam.fixtures.getInstance(),
            ];
            options = {
                hiringApplicationStatus: 'accepted',
            };

            hiringManager = User.fixtures.getInstance({
                hiring_application: hiringApplication,
            });
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    it('should show hiring manager card', () => {
        render(options);
        const card = SpecHelper.expectElement(elem, 'hiring-manager-card');
        expect(card.isolateScope().hiringApplication.id).toBe(hiringApplication.id);
    });

    it('should have a button to go back', () => {
        render(options);
        SpecHelper.expectElement(elem, '[name="back-to-list"]');
        SpecHelper.click(elem, '[name="back-to-list"]');
        expect(renderer.scope.goBack).toHaveBeenCalled();
    });

    it('should allow for changing the status of an application', () => {
        assertSaved('[name="save"]');
    });

    it('should have a working save-and-return button', () => {
        assertSaved('[name="save-and-return"]');
    });

    it('should not allow for changing the status from accepted to anything else', () => {
        render(options);
        hiringApplication.status = 'accepted';
        scope.$digest();
        SpecHelper.expectNoElement(elem, '[name="status"]');
    });

    it('should not disable the select when the proxy is set to accepted, only when the application is accepted in the db', () => {
        render(options);
        hiringApplication.status = 'pending';
        scope.$digest();
        SpecHelper.updateSelect(elem, '[name="status"]', 'accepted');
        SpecHelper.expectElementEnabled(elem, '[name="status"]');
    });

    it('should support logging in as user', () => {
        render(options);

        jest.spyOn(scope, 'logInAsUser');
        jest.spyOn(LogInAs, 'logInAsUser').mockImplementation(() => {});

        SpecHelper.click(elem, '[name="logInAs"]');

        expect(scope.logInAsUser).toHaveBeenCalledWith(scope.hiringApplication);
        expect(LogInAs.logInAsUser).toHaveBeenCalledWith({
            id: scope.hiringApplication.user_id,
            email: scope.hiringApplication.email,
        });
    });

    it('should call gotoEditHiringApplication when userLoaded if currentTab is edit-profile', () => {
        SpecHelper.stubDirective('locationAutocomplete');
        render(options);
        jest.spyOn(scope, 'gotoEditHiringApplication').mockImplementation(() => {});

        scope.userLoaded = false;
        scope.currentTab = 'foo';
        scope.$digest();
        expect(scope.gotoEditHiringApplication).not.toHaveBeenCalled();

        scope.userLoaded = true;
        scope.$digest();
        expect(scope.gotoEditHiringApplication).not.toHaveBeenCalled();

        scope.userLoaded = false;
        scope.$digest();

        scope.userLoaded = true;
        scope.currentTab = 'edit-profile';
        scope.$digest();
        expect(scope.gotoEditHiringApplication).toHaveBeenCalled();
    });

    describe('currentTab', () => {
        it('should update in local storage when changed', () => {
            render(options);
            jest.spyOn(ClientStorage, 'setItem').mockImplementation(() => {});
            SpecHelper.click(elem, '[name="open-positions-tab"]');
            expect(ClientStorage.setItem).toHaveBeenCalledWith(
                'adminEditHiringApplication_currentTab',
                'open-positions',
            );
        });

        it('should initialize to value in local storage', () => {
            jest.spyOn(ClientStorage, 'getItem').mockReturnValue('foo');
            render(
                _.extend(options, {
                    currentTab: 'foo',
                }),
            );
            expect(scope.currentTab).toBe('foo');
        });

        it('should reset the page parameter when switching tabs', () => {
            render(options);
            $location.search('page', 1337);
            expect($location.search().page).toEqual(1337);
            scope.currentTab = 'foo';
            scope.$digest();
            expect($location.search().page).toBeUndefined();
        });

        describe('hiring relationships loading', () => {
            it('should load hiring relationships when first switching to career-network tab', () => {
                SpecHelper.stubDirective('editableThingsList');
                jest.spyOn(ClientStorage, 'getItem').mockReturnValue('foo');
                render(
                    _.extend(options, {
                        currentTab: 'foo',
                    }),
                );
                HiringRelationship.expect('index').returns([]);
                scope.currentTab = 'career-network';
                scope.$digest();
                SpecHelper.expectElement(elem, '[name="loading-hiring-relationships"]');
                HiringRelationship.flush('index');
                SpecHelper.expectNoElement(elem, '[name="loading-hiring-relationships"]');
            });

            it('should not load hiring relationships again when switching back to career-network tab', () => {
                render(options);
                expect(scope.hiringRelationshipsLoaded).toBe(true);
                scope.currentTab = 'foo';
                scope.$digest();
                scope.currentTab = 'career-network';
                scope.$digest();
            });
        });
    });

    describe('batch add candidates', () => {
        it('should create a hiring relationship for the candidates in the career profile list', () => {
            render(options);
            expect(scope.proxy.careerProfileList).toBeUndefined();
            SpecHelper.expectElementDisabled(elem, 'button[name="batch_add"]');

            SpecHelper.updateSelect(elem, '[name="batch_add_candidates"]', careerProfileLists[0]);
            SpecHelper.expectElementEnabled(elem, 'button[name="batch_add"]');

            expect(scope.proxy.careerProfileList).toEqual(careerProfileLists[0]);

            $httpBackend
                .expectPOST(`${$window.ENDPOINT_ROOT}/api/hiring_relationships/batch_create.json`)
                .respond(200, {});
            SpecHelper.click(elem, 'button[name="batch_add"]');
        });
    });

    describe('hiring teams section', () => {
        it('should support adding the hiring manager to a hiring team', () => {
            renderAndAddHiringManagerToTeam();
        });

        it('should support creating a new hiring team and automatically adding the hiring manager as a team member', () => {
            render();
            SpecHelper.expectNoElement(elem, 'multi-select .item');
            SpecHelper.selectizeCreateItem(elem, 'multi-select[name="hiring_teams"] selectize', 'New Team');

            scope.hiringApplicationProxy._beforeSave(); // trigger any beforeSave calculations in advance for expectation

            HiringApplication.expect('update')
                .toBeCalledWith(scope.hiringApplicationProxy, {
                    hiring_team_title: 'New Team',
                })
                .returns(scope.hiringApplicationProxy);

            SpecHelper.click(elem, '[name="save"]');

            HiringApplication.flush('update');
        });

        it('should disable the multi-select if the hiring manager has been added to a team', () => {
            renderAndAddHiringManagerToTeam();
            SpecHelper.expectElementDisabled(elem, 'multi-select[name="hiring_teams"] selectize');
        });

        it('should support removing a hiring manager from a hiring team if not accepted', () => {
            options = {}; // not accepted
            renderAndAddHiringManagerToTeam();
            const expectedHiringTeam = HiringTeam.new(scope.hiringTeamsProxy.hiringTeams[0].asJson());
            const index = _.findIndex(
                scope.hiringTeamsProxy.addedHiringTeams[0].hiring_manager_ids,
                hiringManagerId => hiringManagerId === scope.hiringApplication.user_id,
            );
            expectedHiringTeam.hiring_manager_ids.splice(index, 1);

            SpecHelper.updateSelect(elem, '[name="status"]', 'pending');
            SpecHelper.click(elem, 'multi-select[name="hiring_teams"] .item .remove');

            HiringApplication.expect('update')
                .toBeCalledWith(
                    _.extend(scope.hiringApplicationProxy, {
                        hiring_team_id: null,
                    }),
                    {
                        hiring_team_id: null,
                    },
                )
                .returns(scope.hiringApplicationProxy);

            SpecHelper.click(elem, '[name="save"]');
            HiringApplication.flush('update');

            SpecHelper.expectNoElement(elem, 'multi-select[name="hiring_teams"] .item');
        });

        it('should not support removing a hiring manager from a team if accepted', () => {
            options = {}; // not accepted
            renderAndAddHiringManagerToTeam();
            const expectedHiringTeam = HiringTeam.new(scope.hiringTeamsProxy.hiringTeams[0].asJson());
            const index = _.findIndex(
                scope.hiringTeamsProxy.addedHiringTeams[0].hiring_manager_ids,
                hiringManagerId => hiringManagerId === scope.hiringApplication.user_id,
            );
            expectedHiringTeam.hiring_manager_ids.splice(index, 1);
            SpecHelper.updateSelect(elem, '[name="status"]', 'accepted');
            SpecHelper.click(elem, 'multi-select[name="hiring_teams"] .item .remove');
            HiringApplication.expect('update').toBeCalledWith(expectedHiringTeam);
            SpecHelper.expectElementDisabled(elem, '[name="save"]');
        });
    });

    describe('save buttons', () => {
        it('should be enabled if hiring application status has changed', () => {
            options = {}; // not accepted
            render(options);
            SpecHelper.expectElementDisabled(elem, '[name="save"]');
            SpecHelper.updateSelect(elem, '[name="status"]', 'rejected');
            SpecHelper.expectElementEnabled(elem, '[name="save"]');
            SpecHelper.updateSelect(elem, '[name="status"]', 'pending');
            SpecHelper.expectElementDisabled(elem, '[name="save"]');
        });

        it('should be enabled if a hiring team was selected', () => {
            render(options); // accepted

            SpecHelper.updateMultiSelect(
                elem,
                'multi-select[name="hiring_teams"]',
                scope.hiringTeamsProxy.hiringTeams[0],
            );
            SpecHelper.expectElementEnabled(elem, '[name="save"]');
            SpecHelper.click(elem, 'multi-select[name="hiring_teams"] .item .remove');
            SpecHelper.expectElementDisabled(elem, '[name="save"]');
        });

        it('should be enabled if a hiring team that was already added was removed', () => {
            options = {}; // not accepted
            renderAndAddHiringManagerToTeam();

            SpecHelper.expectElementDisabled(elem, '[name="save"]');
            SpecHelper.click(elem, 'multi-select[name="hiring_teams"] .item .remove');
            SpecHelper.expectElementEnabled(elem, '[name="save"]');
            SpecHelper.updateMultiSelect(
                elem,
                'multi-select[name="hiring_teams"]',
                scope.hiringTeamsProxy.hiringTeams[0],
            );
            SpecHelper.expectElementDisabled(elem, '[name="save"]');
        });
    });

    describe('candidates table', () => {
        const renderWithCandidates = () => {
            // the second call to the Users API to fetch relationships
            User.expect('index').returns({
                result: users,
                meta: {
                    total_count: users.length,
                },
            });

            render(options);

            $timeout.flush();

            // Force initial load
            scope.updateFilters();
            scope.$digest();
            User.flush('index');
        };

        it('should only be visible if not rejected', () => {
            // it should hide save buttons and show a message warning of autosaving
            renderWithCandidates();
            SpecHelper.expectNoElement(elem, '[name="status"]');
            SpecHelper.expectElement(elem, 'editable-things-list');

            hiringApplication.status = 'rejected';
            scope.$digest();
            SpecHelper.expectNoElement(elem, 'editable-things-list');
        });

        it('should show existing hiring relationships', () => {
            renderWithCandidates();
            SpecHelper.expectElement(elem, 'editable-things-list');
            const user = users[2];
            SpecHelper.assertRowValues(elem, 'table tbody tr:eq(2) td', {
                careerProfileCreatedAt: amDateFormat(1000 * user.career_profile.created_at, 'MM/DD/YY HH:mm'),
                name: user.name,
                email: user.email,
                careerProfileLocation: user.careerProfileLocation,
                cohortApplicationsStatusesString: user.cohortApplicationsStatusesString,

                // NOTE: ng-class transforms `.` into `-` to ensure valid CSS class names -- hence `hiring_relationships-hiring_manager_priority`
                'hiring_relationships-hiring_manager_priority': scope.existingHiringRelationshipsByCandidateId[
                    user.id
                ].hiring_manager_priority.toString(),
                editorAbilities: '',
            });
        });

        describe('custom actions', () => {
            it('should support adding a hiring relationship', () => {
                renderWithCandidates();

                SpecHelper.expectElements(elem, 'table tbody tr', users.length); // ensure that the expected users are populating the table

                expect(
                    scope.existingHiringRelationshipsByCandidateId[existingHiringRelationships[0].candidate_id],
                ).toBeDefined();
                expect(
                    scope.existingHiringRelationshipsByCandidateId[existingHiringRelationships[1].candidate_id],
                ).toBeDefined();

                expect(scope.existingHiringRelationshipsByCandidateId[users[0].id]).toBeUndefined();

                HiringRelationship.expect('create');
                SpecHelper.click(elem, 'button[data-id="add"]', 0);
                HiringRelationship.flush('create');

                expect(scope.existingHiringRelationshipsByCandidateId[users[0].id]).toBeDefined();
            });

            it('should not set hiring_manager_status when creating hiring relationship', () => {
                renderWithCandidates();

                const expected = HiringRelationship.new({
                    candidate_id: users[0].id,
                    hiring_manager_id: hiringApplication.user_id,
                });

                // have to mock this out to prevent bumping of updated_at timestamp in _beforeSave callback
                jest.spyOn(HiringRelationship.prototype, '_beforeSave').mockImplementation(angular.noop);

                HiringRelationship.expect('create').toBeCalledWith(expected);
                SpecHelper.click(elem, 'button[data-id="add"]', 0);
                HiringRelationship.flush('create');
            });

            it("should allow previewing a candidate's profile in a dialog modal if candidate has no existing relationship with the hiring manager", () => {
                renderWithCandidates();
                CareerProfile.expect('show');
                jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
                SpecHelper.click(elem, 'tbody tr:eq(0) button[data-id="view"]');
                CareerProfile.flush('show');
                expect(DialogModal.alert).toHaveBeenCalled();
            });

            it('should allow editing a hiring relationship in a dialog modal if candidate has existing relationship with the hiring manager', () => {
                renderWithCandidates();
                CareerProfile.expect('show');
                jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
                SpecHelper.click(elem, 'tbody tr:eq(1) button[data-id="edit"]');
                CareerProfile.flush('show');
                expect(DialogModal.alert).toHaveBeenCalled();
            });
        });
    });

    describe('edit profile tab', () => {
        beforeEach(() => {
            SpecHelper.stubConfig();
            SpecHelper.stubEventLogging();
            SpecHelper.stubDirective('locationAutocomplete'); // no need to load google places this way

            render(options);

            jest.spyOn($location, 'path').mockReturnValue('/admin/careers/hiring_applications');

            SpecHelper.expectNoElement(elem, 'edit-hiring-manager-profile');
            SpecHelper.click(elem, '[name="edit-profile-tab"]');
            $timeout.flush();
        });

        it('should load properly and support navigation between sections', () => {
            SpecHelper.expectElement(elem, 'edit-hiring-manager-profile');
            SpecHelper.expectElement(elem, 'company-form');

            // navigate to you form
            SpecHelper.expectNoElement(elem, 'you-form');
            SpecHelper.click(elem, '[name="application_you"]');
            SpecHelper.expectNoElement(elem, 'company-form');
            SpecHelper.expectElement(elem, 'you-form');
        });

        it('should allow saving and properly update proxy', () => {
            SpecHelper.expectElementAttr(
                elem,
                'hiring-manager-card .website-link',
                'href',
                scope.userProxy.hiring_application.website_url,
            );
            SpecHelper.expectTextInputVal(
                elem,
                'company-form input[name="website_url"]',
                scope.userProxy.hiring_application.website_url,
            );

            // update name and save
            HiringApplication.expect('save');

            SpecHelper.updateTextInput(elem, 'company-form input[name="website_url"]', 'https://david-s-pumpkins.com');

            // mock validity of formController
            const editHiringManagerProfileScope = elem.find('edit-hiring-manager-profile').isolateScope();
            editHiringManagerProfileScope.currentForm.$valid = true;

            // click and flush
            SpecHelper.click(elem, 'edit-hiring-manager-profile button[name="save"]');
            HiringApplication.flush('save');
            $timeout.flush();

            // verify that card is refreshed and shows the new name
            expect(scope.userProxy.hiring_application.website_url).toEqual('https://david-s-pumpkins.com');
            SpecHelper.expectElementAttr(
                elem,
                'hiring-manager-card .website-link',
                'href',
                scope.userProxy.hiring_application.website_url,
            );
            SpecHelper.expectTextInputVal(
                elem,
                'company-form input[name="website_url"]',
                scope.userProxy.hiring_application.website_url,
            );
        });
    });

    describe('positions tab', () => {
        beforeEach(() => {
            render(options);
            jest.spyOn($location, 'path').mockReturnValue('/admin/careers/hiring_applications');
            SpecHelper.expectNoElement(elem, 'edit-hiring-manager-profile');
            SpecHelper.click(elem, '[name="open-positions-tab"]');

            OpenPosition.expect('index').returns({
                result: [
                    OpenPosition.new({
                        archived: false,
                    }),
                ],
                meta: {
                    total_count: 3,
                },
            });

            $timeout.flush();
        });

        it('should load properly', () => {
            SpecHelper.expectElement(elem, '[name="create"]');
            SpecHelper.expectElement(elem, '[name="open-position-editable-things"]');
        });

        describe('openPositionMode', () => {
            it('should show open positions and create button when list', () => {
                expect(scope.indexParamsForOpenPositions).toEqual({
                    filters: {
                        hiring_manager_id: scope.hiringApplication.user_id,
                    },
                    'except[]': ['hiring_application'],
                    'fields[]': ['ADMIN_FIELDS'],
                });
                SpecHelper.stubDirective('editableThingsList');
                scope.openPositionMode = 'list';
                scope.$digest();
                SpecHelper.expectElement(elem, '[name="create"]');
                SpecHelper.expectElement(elem, '[name="open-position-editable-things"]');
            });

            it('should show back button and position-form when create', () => {
                SpecHelper.stubDirective('positionForm');
                scope.openPositionMode = 'create';
                scope.$digest();
                SpecHelper.expectElement(elem, '[name="back-to-open-position-list"]');
                SpecHelper.expectElement(elem, 'position-form');
            });
        });

        it('should load route onOpenPositionClick is triggered', () => {
            const openPosition = OpenPosition.new();
            jest.spyOn(scope, 'loadUrl').mockImplementation(() => {});
            scope.onOpenPositionClick(openPosition);
            expect(scope.loadUrl).toHaveBeenCalled();
        });

        describe('createOpenPosition', () => {
            it('should create a new OpenPosition and set mode to create', () => {
                expect(scope.tempOpenPosition).toBeUndefined();
                scope.createOpenPosition();
                expect(scope.tempOpenPosition).toBeDefined();
                expect(scope.openPositionMode).toEqual('create');
            });
        });

        describe('setOpenPositionMode', () => {
            it('should set the tempOpenPosition when create', () => {
                expect(scope.tempOpenPosition).toBeUndefined();

                const openPosition = OpenPosition.new();
                scope.setOpenPositionMode('create', openPosition);
                expect(scope.tempOpenPosition).toBe(openPosition);
                expect(scope.openPositionMode).toEqual('create');
            });

            it('should remove the page and positionId parameters when list', () => {
                jest.spyOn($location, 'search').mockImplementation(() => {});
                scope.setOpenPositionMode('list');
                expect($location.search).toHaveBeenCalledWith('page', null);
                expect($location.search).toHaveBeenCalledWith('positionId', null);
            });
        });

        describe('onSavePositionFormCallback', () => {
            it('should set the mode back to list', () => {
                jest.spyOn(scope, 'setOpenPositionMode').mockImplementation(() => {});
                scope.onSavePositionFormCallback();
                expect(scope.setOpenPositionMode).toHaveBeenCalledWith('list');
            });
        });
    });

    function addHiringManagerToTeam(opts) {
        SpecHelper.expectNoElement(elem, 'multi-select .item');

        const isAccepted = !!(opts && opts.hiringApplicationStatus === 'accepted');

        const expectedHiringTeam = HiringTeam.new(scope.hiringTeamsProxy.hiringTeams[0].asJson());
        expectedHiringTeam.hiring_manager_ids.push(scope.hiringApplication.user_id);
        SpecHelper.updateMultiSelect(elem, 'multi-select[name="hiring_teams"]', scope.hiringTeamsProxy.hiringTeams[0]);

        HiringApplication.expect('save');

        if (isAccepted) {
            HiringRelationship.expect('index');
        }

        SpecHelper.click(elem, '[name="save"]');
        HiringApplication.flush('save');

        if (isAccepted) {
            HiringRelationship.flush('index');
        }

        SpecHelper.expectElement(elem, 'multi-select[name="hiring_teams"] .item');
    }

    function renderAndAddHiringManagerToTeam() {
        render(options);
        addHiringManagerToTeam(options);
    }

    function assertSaved(buttonSelector) {
        // mock out automatic changes that make it hard to assert
        jest.spyOn(HiringApplication.prototype, '_beforeSave').mockImplementation(() => {});

        HiringRelationship.expect('index').returns(existingHiringRelationships);
        options = {
            hiringApplicationStatus: 'pending',
        };

        renderAndAddHiringManagerToTeam();

        expect(hiringApplication.status).not.toEqual('accepted');
        // if the value hasn't changed, submit is disabled
        SpecHelper.expectElementDisabled(elem, buttonSelector);
        jest.spyOn(ngToast, 'create').mockImplementation(() => {});

        SpecHelper.updateSelect(elem, '[name="status"]', 'accepted');

        _.extend(hiringApplication, {
            status: 'accepted',
        });

        HiringApplication.expect('update')
            .toBeCalledWith(hiringApplication, {
                hiring_team_id: scope.hiringTeamsProxy.addedHiringTeams[0].id,
            })
            .returns(hiringApplication);

        SpecHelper.click(elem, buttonSelector);
        // during save, submit is disabled
        SpecHelper.expectElementDisabled(elem, buttonSelector);
        HiringApplication.flush('update');

        if (buttonSelector === '[name="save-and-return"]') {
            expect(renderer.scope.goBack).toHaveBeenCalledWith();
        } else if (buttonSelector === '[name="save"]') {
            expect(scope.hiringApplicationProxy.status).toEqual('accepted');
        } else {
            throw new Error('unexpected selector');
        }

        expect(ngToast.create).toHaveBeenCalledWith({
            content: 'Hiring application saved.',
            className: 'success',
        });
    }

    function render(overrides = {}) {
        overrides.currentTab = overrides.currentTab || 'career-network';

        HiringTeam.expect('index').returns(hiringTeams);
        CareerProfileList.expect('index').returns(careerProfileLists);

        // the first call to the Users controller is to fetch the Hiring Manager user data
        User.expect('show').returns({
            result: hiringManager,
        });

        // non-rejected hiring managers will fetch a list of hiring relationships on the career-network tab
        if (options.hiringApplicationStatus !== 'rejected' && overrides.currentTab === 'career-network') {
            HiringRelationship.expect('index').returns(existingHiringRelationships);
        }

        renderer = SpecHelper.renderer();
        renderer.scope.goBack = jest.fn();
        hiringApplication.status = overrides.hiringApplicationStatus
            ? overrides.hiringApplicationStatus
            : hiringApplication.status;
        renderer.scope.hiringApplication = hiringApplication;

        renderer.render(
            '<admin-edit-hiring-application thing="hiringApplication" go-back="goBack()"></admin-edit-hiring-application>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();

        User.flush('show');
        CareerProfileList.flush('index');
        HiringTeam.flush('index');

        // flush everything necessary for non-rejected hiring managers on the career-network tab
        if (options.hiringApplicationStatus !== 'rejected' && overrides.currentTab === 'career-network') {
            HiringRelationship.flush('index');
        }
    }
});
