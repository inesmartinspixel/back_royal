import 'AngularSpecHelper';
import 'Admin/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import 'Users/angularModule/spec/_mock/fixtures/users';
import jobPreferencesFormLocales from 'Careers/locales/careers/edit_career_profile/job_preferences_form-en.json';
import fieldOptionsLocales from 'Careers/locales/careers/field_options-en.json';
import candidateListCardLocales from 'Careers/locales/careers/candidate_list_card-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(jobPreferencesFormLocales, fieldOptionsLocales, candidateListCardLocales);

describe('Admin::AdminCandidateCard', () => {
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let CareerProfile;
    let careerProfile;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Admin', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            $injector.get('UserFixtures');
            CareerProfile = $injector.get('CareerProfile');
            $injector.get('CareerProfileFixtures');
        });

        careerProfile = CareerProfile.fixtures.getInstance();
        SpecHelper.stubCurrentUser();
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.careerProfile = careerProfile;
        renderer.render('<admin-candidate-card career-profile="careerProfile"></admin-candidate-card>');
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    it('should have a careerProfile', () => {
        render();
        expect(scope.careerProfile).toBe(careerProfile);
    });

    it('should toggle full profile', () => {
        render();
        SpecHelper.expectNoElement(elem, '.full-profile');
        SpecHelper.click(elem, 'button[name="toggle"]');
        SpecHelper.expectElement(elem, '.full-profile');
        SpecHelper.click(elem, 'button[name="toggle"]');
        SpecHelper.expectNoElement(elem, '.full-profile');
    });
});
