import 'AngularSpecHelper';
import 'Admin/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/streams';

describe('Authentication::AdminGroupsDir', () => {
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let $window;
    let $auth;
    let ngToast;
    let Stream;
    let Group;
    let streams;
    let groups;
    let $injector;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Admin', 'SpecHelper');

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;

            SpecHelper = $injector.get('SpecHelper');
            $window = $injector.get('$window');
            ngToast = $injector.get('ngToast');
            Stream = $injector.get('Lesson.Stream');
            Group = $injector.get('Group');
            $injector.get('MockIguana');
            $auth = $injector.get('$auth');
            $injector.get('StreamFixtures');
        });

        jest.spyOn($auth, 'validateUser').mockImplementation(() => {});

        SpecHelper.stubCurrentUser('admin');

        // TODO: move to fixtures? pretty specialized atm
        streams = [Stream.fixtures.getInstance(), Stream.fixtures.getInstance(), Stream.fixtures.getInstance()];
        groups = [
            {
                name: 'Group 1',
                stream_locale_pack_ids: [],
                created_at: 0,
                updated_at: 0,
            },
            {
                name: 'Group 2',
                stream_locale_pack_ids: [],
                created_at: 0,
                updated_at: 0,
            },
            {
                name: 'Group 3',
                stream_locale_pack_ids: [],
                created_at: 0,
                updated_at: 0,
            },
        ];

        Stream.expect('index').returns(streams);
    });

    describe('header', () => {
        it('should set the text to Group Admin', () => {
            const AppHeaderViewModel = $injector.get('Navigation.AppHeader.AppHeaderViewModel');

            jest.spyOn(AppHeaderViewModel, 'setTitleHTML').mockImplementation(() => {});
            renderAndFlush();

            expect(AppHeaderViewModel.setTitleHTML).toHaveBeenCalledWith('GROUP<br>ADMIN');
        });
    });

    describe('create', () => {
        it('should allow for creating new groups', () => {
            renderAndFlush();

            const createButton = SpecHelper.expectElementText(elem, 'button[name="create_group"]', 'Create Group', 0);
            SpecHelper.updateTextInput(elem, 'input[name="new_group"]', 'New Group');

            Group.expect('create').returns({
                result: {
                    name: 'New Group',
                    created_at: 0,
                    updated_at: 0,
                },
            });

            createButton.click();
            jest.spyOn(ngToast, 'create').mockImplementation(() => {});
            Group.flush('create');

            expect(ngToast.create).toHaveBeenCalledWith({
                content: 'New Group successfully created.',
                className: 'success',
            });
        });
    });

    describe('delete', () => {
        it('should delete if confirmed', () => {
            renderAndFlush();

            const deleteButton = SpecHelper.expectElements(elem, 'button.btn-danger', scope.groups.length).eq(0);
            expect(scope.groups.length).toBe(3);

            jest.spyOn($window, 'confirm').mockReturnValue(true);
            Group.expect('destroy');
            jest.spyOn(ngToast, 'create').mockImplementation(() => {});
            deleteButton.click();
            Group.flush('destroy');

            expect(ngToast.create).toHaveBeenCalledWith({
                content: 'Group 1 successfully deleted.',
                className: 'success',
            });

            expect(scope.groups.length).toBe(2);
        });

        it('should not delete if not confirmed', () => {
            renderAndFlush();

            const deleteButton = SpecHelper.expectElements(elem, 'button.btn-danger', scope.groups.length).eq(0);
            expect(scope.groups.length).toBe(3);

            jest.spyOn($window, 'confirm').mockReturnValue(false);
            jest.spyOn(ngToast, 'create').mockImplementation(() => {});
            deleteButton.click();

            expect(ngToast.create).not.toHaveBeenCalled();
        });
    });

    describe('streams', () => {
        it('should allow for setting streams', () => {
            let group = groups[0];
            groups = [group];
            renderAndFlush();
            group = scope.groups[0];
            expect(group.stream_locale_pack_ids).toEqual([]);

            streams[0].ensureLocalePack();
            Stream.flush('index');
            SpecHelper.updateSelectize(elem, 'locale-pack-selectize selectize', [streams[0].localePackId]);
            expect(group.stream_locale_pack_ids).toEqual([streams[0].localePackId]);
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        Group.expect('index').returns(groups);
        renderer = SpecHelper.renderer();
        renderer.render('<admin-groups></admin-groups>');
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.groups = [];
    }

    function renderAndFlush() {
        render();
        Group.flush('index');
    }
});
