import 'AngularSpecHelper';
import 'Admin/angularModule';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';

describe('Admin::BatchEditUsers::ReviewChangesDir', () => {
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let User;
    let users;
    let cohorts;
    let Cohort;
    let $httpBackend;
    let BatchEditUsersViewModel;
    let HttpQueue;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Admin', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            User = $injector.get('User');
            SpecHelper.stubCurrentUser('admin');
            $injector.get('CohortFixtures');
            SpecHelper.disableHttpQueue();
            Cohort = $injector.get('Cohort');
            $httpBackend = $injector.get('$httpBackend');
            BatchEditUsersViewModel = $injector.get('BatchEditUsersViewModel');
            HttpQueue = $injector.get('HttpQueue');

            users = [
                User.fixtures.getInstance(),
                User.fixtures.getInstance(),
                User.fixtures.getInstance(),
                User.fixtures.getInstance(),
            ];

            cohorts = [Cohort.fixtures.getInstance(), Cohort.fixtures.getInstance()];
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    it('should show a paginated list of users', () => {
        render();
        const sortedUsers = _.sortBy(users, 'accountId');
        scope.perPage = 2;
        scope.$apply();

        SpecHelper.expectElements(elem, '.user-row', 2);
        SpecHelper.expectElementText(elem, '.user-row:eq(0) td:eq(0)', sortedUsers[0].accountId);
        SpecHelper.click(elem, 'a[name="next"]');
        SpecHelper.expectElements(elem, '.user-row', 2);
        SpecHelper.expectElementText(elem, '.user-row:eq(0) td:eq(0)', sortedUsers[2].accountId);
    });

    it('should allow for removing users', () => {
        render();
        const origLength = scope.viewModel.loadedUsers.length;
        SpecHelper.click(elem, '[name="remove"]', 0);
        expect(scope.viewModel.loadedUsers.length).toBe(origLength - 1);
    });

    it('should show update users after a successful save', () => {
        render();
        const response = JSON.parse(JSON.stringify(_.invoke(users, 'asJson'))); // clone users
        _.each(response, user => {
            user.email = 'changedOnServer';
        });
        save(200, {
            contents: {
                users: response,
            },
        });
        SpecHelper.expectElementText(elem, '.user-row:eq(0) td:eq(0)', 'changedOnServer');
        SpecHelper.expectNoElement(elem, '[name="remove"]'); // cannot remove updated users
        expect(scope.viewModel.loadedUsers).toEqual([]);
    });

    it('should show errors after an unsuccessful save', () => {
        render();
        const response = {
            errors: {},
        };
        const sortedUsers = _.sortBy(users, 'accountId');
        const erroredUser = sortedUsers[2];
        const errors = ['Just did not work with this guy', 'Man it was bad'];
        response.errors[erroredUser.id] = errors;

        jest.spyOn(HttpQueue.instance, 'unfreezeAfterError').mockImplementation(() => {});
        save(406, response);
        expect(HttpQueue.instance.unfreezeAfterError).toHaveBeenCalled();
        SpecHelper.expectElementText(elem, '.user-row:eq(0) td:eq(0)', erroredUser.accountId);
        SpecHelper.expectElementText(elem, '.error-row:eq(0) td:eq(0)', errors.join(', '));
        expect(scope.viewModel.loadedUsers).toEqual(users);
    });

    it('should chunk based on batch size', () => {
        for (let i = 0; i < 50; i++) {
            users.push(User.fixtures.getInstance()); // add at least 1 batch of additional users
        }
        render();
        const response = JSON.parse(JSON.stringify(_.invoke(users, 'asJson'))); // clone users
        _.each(response, user => {
            user.email = 'changedOnServer';
        });
        save(
            200,
            {
                contents: {
                    users: response,
                },
            },
            2,
        ); // 2 chunks expected
        SpecHelper.expectElementText(elem, '.user-row:eq(0) td:eq(0)', 'changedOnServer');
        SpecHelper.expectNoElement(elem, '[name="remove"]'); // cannot remove updated users
        expect(scope.viewModel.loadedUsers).toEqual([]);
    });

    function save(code, response, expectedChunkCount = 1) {
        scope.viewModel.updateParams = {
            a: 'b',
        };
        scope.$digest();

        const responseConfig = {};

        for (let i = 0; i < expectedChunkCount; i++) {
            $httpBackend
                .expectPUT(`${window.ENDPOINT_ROOT}/api/users/batch_update.json`, {
                    user_ids: _.chain(scope.viewModel.loadedUsers).pluck('id').chunk(50).value()[i],
                    a: 'b',
                })
                .respond(code, response, responseConfig);
        }

        expect(scope.reviewChangesForm.$valid).toBe(true);
        SpecHelper.submitForm(elem);
        $httpBackend.flush();
    }

    function render() {
        renderer = SpecHelper.renderer();
        Cohort.expect('index').returns(cohorts);
        renderer.scope.viewModel = BatchEditUsersViewModel.instance;
        Cohort.flush('index');
        renderer.scope.viewModel.loadedUsers = users;
        renderer.render('<batch-edit-users-review-changes view-model="viewModel"></batch-edit-users-review-changes>');
        elem = renderer.elem;
        scope = elem.isolateScope();
        jest.spyOn(scope.viewModel, 'gotoSection').mockImplementation(() => {});
    }
});
