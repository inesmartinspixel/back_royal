import 'AngularSpecHelper';
import 'Admin/angularModule';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';

describe('Admin::BatchEditUsers::UpdateUsersDir', () => {
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let User;
    let users;
    let cohorts;
    let Cohort;
    let BatchEditUsersViewModel;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Admin', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            User = $injector.get('User');
            SpecHelper.stubCurrentUser('admin');
            $injector.get('CohortFixtures');
            SpecHelper.disableHttpQueue();
            Cohort = $injector.get('Cohort');
            BatchEditUsersViewModel = $injector.get('BatchEditUsersViewModel');

            users = [
                User.fixtures.getInstance(),
                User.fixtures.getInstance(),
                User.fixtures.getInstance(),
                User.fixtures.getInstance(),
            ];

            cohorts = [Cohort.fixtures.getInstance(), Cohort.fixtures.getInstance()];
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    it('should show a summary of loaded users', () => {
        render();
        SpecHelper.expectElementText(
            elem,
            '.loaded-users-summary',
            `${scope.viewModel.loadedUsers.length} users loaded. Select the changes you would like to make. You will be able to review each user on the next page before saving.`,
        );
    });

    it('should prevent submission until valid', () => {
        render();
        SpecHelper.expectElementDisabled(elem, '[name="review-changes"]');
        Object.defineProperty(scope.viewModel, 'validUpdateParams', {
            value: true,
        });
        scope.$digest();
        SpecHelper.expectElementEnabled(elem, '[name="review-changes"]');
    });

    it('should prep the viewModel for reset on submit', () => {
        render();
        Object.defineProperty(scope.viewModel, 'validUpdateParams', {
            value: true,
        });
        scope.$digest();
        jest.spyOn(scope.viewModel, 'resetForReview');
        SpecHelper.submitForm(elem, '[name="review-changes"]');
        expect(scope.viewModel.resetForReview).toHaveBeenCalled();
    });

    it('should rewrite rejected status option', () => {
        render();
        expect(
            _.findWhere(scope.cohortApplicationStatusOptions, {
                value: 'rejected',
            }).text,
        ).toEqual('REJECTED (EXPELLED WHEN APPROPRIATE)');
    });

    it("should set viewModel.updateParams.cohort_id immediately if viewModel.selectStrategy is 'cohortStatus'", () => {
        const opts = {
            selectStrategy: 'cohortStatus',
        };
        render(opts);
        expect(scope.viewModel.updateParams.cohort_id).toEqual(scope.viewModel.cohortId);
    });

    it('should be possible to just set the cohort status and move on if selectStrategy is cohortStatus', () => {
        const opts = {
            selectStrategy: 'cohortStatus',
        };
        render(opts);
        SpecHelper.updateSelect(elem, '[name="cohort-status"]', 'rejected');

        expect(scope.viewModel.validUpdateParams).toBe(true);
        expect(scope.viewModel.updateParams.cohort_id).toEqual(scope.viewModel.cohortId);
        expect(scope.viewModel.updateParams.cohort_status).toEqual('rejected');

        SpecHelper.submitForm(elem);
        expect(scope.viewModel.gotoSection).toHaveBeenCalledWith('review-changes');
    });

    it('should be possible to just set graduation status and move on if selectStrategy is cohortStatus', () => {
        const opts = {
            selectStrategy: 'cohortStatus',
        };
        render(opts);
        SpecHelper.updateSelect(elem, '[name="cohort"]', cohorts[1].id);
        SpecHelper.updateSelect(elem, '[name="graduation-status"]', 'graduated');

        expect(scope.viewModel.validUpdateParams).toBe(true);
        expect(scope.viewModel.updateParams.cohort_id).toEqual(cohorts[1].id);
        expect(scope.viewModel.updateParams.graduation_status).toEqual('graduated');

        SpecHelper.submitForm(elem);
        expect(scope.viewModel.gotoSection).toHaveBeenCalledWith('review-changes');
    });

    it('should be possible to set can_edit_career_profile and move on', () => {
        render();
        SpecHelper.toggleRadio(elem, '[name="can-edit-career-profile"]', 1);
        expect(scope.viewModel.validUpdateParams).toBe(true);
        expect(scope.viewModel.updateParams.can_edit_career_profile).toEqual(true);

        SpecHelper.toggleRadio(elem, '[name="can-edit-career-profile"]', 2);
        expect(scope.viewModel.validUpdateParams).toBe(true);
        expect(scope.viewModel.updateParams.can_edit_career_profile).toEqual(false);

        SpecHelper.toggleRadio(elem, '[name="can-edit-career-profile"]', 0);
        expect(scope.viewModel.validUpdateParams).toBe(false);
        expect(scope.viewModel.updateParams.can_edit_career_profile).toEqual(null);

        // make it valid again and click next
        SpecHelper.toggleRadio(elem, '[name="can-edit-career-profile"]', 1);
        SpecHelper.submitForm(elem);
        expect(scope.viewModel.gotoSection).toHaveBeenCalledWith('review-changes');
    });

    it('should require cohort to be selected if selectStrategy is not cohortStatus and updating graduation status', () => {
        const opts = {
            selectStrategy: 'listIdentifiers',
        };
        render(opts);
        SpecHelper.updateSelect(elem, '[name="graduation-status"]', 'graduated');
        expect(scope.viewModel.validUpdateParams).toBe(false);
        expect(scope.viewModel.updateParams.cohort_id).toBeUndefined();
        expect(scope.viewModel.updateParams.graduation_status).toEqual('graduated');

        SpecHelper.updateSelect(elem, '[name="cohort"]', cohorts[1].id);
        expect(scope.viewModel.validUpdateParams).toBe(true);
        expect(scope.viewModel.updateParams.cohort_id).toEqual(cohorts[1].id);
        expect(scope.viewModel.updateParams.graduation_status).toEqual('graduated');

        SpecHelper.submitForm(elem);
        expect(scope.viewModel.gotoSection).toHaveBeenCalledWith('review-changes');
    });

    it('should require cohort to be selected if selectStrategy is not cohortStatus and updating cohort status', () => {
        const opts = {
            selectStrategy: 'listIdentifiers',
        };
        render(opts);
        SpecHelper.updateSelect(elem, '[name="cohort-status"]', 'accepted');
        expect(scope.viewModel.validUpdateParams).toBe(false);
        expect(scope.viewModel.updateParams.cohort_id).toBeUndefined();
        expect(scope.viewModel.updateParams.cohort_status).toEqual('accepted');

        SpecHelper.updateSelect(elem, '[name="cohort"]', cohorts[1].id);
        expect(scope.viewModel.validUpdateParams).toBe(true);
        expect(scope.viewModel.updateParams.cohort_id).toEqual(cohorts[1].id);
        expect(scope.viewModel.updateParams.cohort_status).toEqual('accepted');

        SpecHelper.submitForm(elem);
        expect(scope.viewModel.gotoSection).toHaveBeenCalledWith('review-changes');
    });

    it('should show a warning about can_edit_career_profile', () => {
        jest.spyOn(cohorts[1], 'supportsDelayedCareerNetworkAccessOnAcceptance', 'get').mockReturnValue(true);
        render();
        SpecHelper.updateSelect(elem, '[name="cohort"]', cohorts[1].id);
        SpecHelper.expectNoElement(elem, '.error');
        SpecHelper.toggleRadio(elem, '[name="can-edit-career-profile"]', 1);
        SpecHelper.expectNoElement(elem, '.error');
        SpecHelper.updateSelect(elem, '[name="cohort-status"]', 'accepted');
        SpecHelper.expectElementText(
            elem,
            '.error',
            'We generally do not put students from degreee programs into the Career Network immediately upon acceptance. Are you sure you want to do this?',
        );
    });

    function render(opts = {}) {
        renderer = SpecHelper.renderer();
        Cohort.expect('index').returns(cohorts);
        renderer.scope.viewModel = BatchEditUsersViewModel.instance;
        renderer.scope.viewModel.selectStrategy = opts.selectStrategy || 'cohortStatus';
        renderer.scope.viewModel.cohortId = opts.selectStrategy === 'cohortStatus' ? cohorts[0].id : undefined;
        Cohort.flush('index');
        renderer.scope.viewModel.loadedUsers = users;
        renderer.render('<batch-edit-users-update-users view-model="viewModel"></batch-edit-users-update-users>');
        elem = renderer.elem;
        scope = elem.isolateScope();
        jest.spyOn(scope.viewModel, 'gotoSection').mockImplementation(() => {});
    }
});
