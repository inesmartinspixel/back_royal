import 'AngularSpecHelper';
import 'Admin/angularModule';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';

describe('Admin::BatchEditUsers::SelectUsersDir', () => {
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let User;
    let $window;
    let users;
    let cohorts;
    let Cohort;
    let BatchEditUsersViewModel;
    let $httpBackend;
    let guid;
    let DialogModal;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Admin', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            User = $injector.get('User');
            SpecHelper.stubCurrentUser('admin');
            $injector.get('CohortFixtures');
            SpecHelper.disableHttpQueue();
            Cohort = $injector.get('Cohort');
            BatchEditUsersViewModel = $injector.get('BatchEditUsersViewModel');
            $httpBackend = $injector.get('$httpBackend');
            $window = $injector.get('$window');
            guid = $injector.get('guid');
            DialogModal = $injector.get('DialogModal');

            users = [
                User.fixtures.getInstance(),
                User.fixtures.getInstance(),
                User.fixtures.getInstance(),
                User.fixtures.getInstance(),
            ];

            cohorts = [Cohort.fixtures.getInstance(), Cohort.fixtures.getInstance()];

            render();
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    it('should show a message if no users are loaded', () => {
        scope.userIdentifiers = [users[0].id, users[1].email];
        $httpBackend
            .expectPOST(`${$window.ENDPOINT_ROOT}/api/users/batch_edit_users_index.json`, {
                identifiers: JSON.stringify(scope.userIdentifiers),
                per_page: 9999999999,
            })
            .respond(200, {
                contents: {
                    users: [],
                },
            });
        scope.$digest();
        // SpecHelper.click(elem, '[name="load-users"]');
        SpecHelper.submitForm(elem);
        $httpBackend.flush();
        SpecHelper.expectElementText(elem, '.warning', 'No users found.');
    });

    describe('selectStrategy === listIdentifiers', () => {
        it('should support different delimiters', () => {
            SpecHelper.updateTextArea(elem, '[name="user-identifiers"] textarea', 'a,b;c d  e   f  ;,    g');
            expect(scope.userIdentifiers).toEqual(['a', 'b', 'c', 'd', 'e', 'f', 'g']);
        });

        it('should load users by identifier', () => {
            SpecHelper.expectElementDisabled(elem, '[name="load-users"]');
            scope.userIdentifiers = [users[0].id, users[1].email];
            $httpBackend
                .expectPOST(`${$window.ENDPOINT_ROOT}/api/users/batch_edit_users_index.json`, {
                    identifiers: JSON.stringify(scope.userIdentifiers),
                    per_page: 9999999999,
                })
                .respond({
                    contents: {
                        users: [users[0], users[1]],
                    },
                });
            scope.$digest();
            // SpecHelper.click(elem, '[name="load-users"]');
            SpecHelper.submitForm(elem);
            SpecHelper.expectElementDisabled(elem, '[name="user-identifiers"] textarea');
            $httpBackend.flush();
            assertLoadedUsers([users[0], users[1]]);
            expect(scope.viewModel.gotoSection).toHaveBeenCalledWith('update-users');
        });

        it('should warn if any users are not found', () => {
            searchForSomeMissingUsers();
            const expectedWarning = `WARNING: could not find any users for the following entries: ${users[1].email} You can continue to update the users that were successfully loaded, or you can enter a new list above.`;
            SpecHelper.expectElementText(elem, '.warning', expectedWarning);
            expect(scope.viewModel.gotoSection).not.toHaveBeenCalled();

            // ensure we can search again
            searchForSomeMissingUsers();
            SpecHelper.expectElementText(elem, '.warning', expectedWarning);
            expect(scope.viewModel.gotoSection).not.toHaveBeenCalled();

            // We do not move on automatically, but the user can click to move on if she wishes to
            SpecHelper.click(elem, '[name="update-users"]');
            expect(scope.viewModel.gotoSection).toHaveBeenCalledWith('update-users');
        });

        it('should show modal if trying to update more than 1000 users', () => {
            SpecHelper.expectElementDisabled(elem, '[name="load-users"]');

            const user_ids = [];
            const _users = [];
            for (let i = 0; i < 1001; i++) {
                const user_id = guid.generate();
                user_ids.push(user_id);
                _users.push(
                    User.fixtures.getInstance({
                        id: user_id,
                    }),
                );
            }

            scope.userIdentifiers = user_ids;
            $httpBackend
                .expectPOST(`${$window.ENDPOINT_ROOT}/api/users/batch_edit_users_index.json`, {
                    identifiers: JSON.stringify(scope.userIdentifiers),
                    per_page: 9999999999,
                })
                .respond({
                    contents: {
                        users: _users,
                    },
                });

            scope.$digest();
            SpecHelper.submitForm(elem);

            jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
            $httpBackend.flush();

            expect(DialogModal.alert).toHaveBeenCalledWith({
                content: 'Please update 1000 or less users (currently 1001)',
            });
            expect(scope.viewModel.loadedUsers).toBeUndefined();
        });

        function searchForSomeMissingUsers() {
            scope.userIdentifiers = [users[0].id, users[1].email];
            $httpBackend
                .expectPOST(`${$window.ENDPOINT_ROOT}/api/users/batch_edit_users_index.json`, {
                    identifiers: JSON.stringify(scope.userIdentifiers),
                    per_page: 9999999999,
                })
                .respond({
                    contents: {
                        users: [users[0]],
                    },
                });
            scope.$digest();
            // SpecHelper.click(elem, '[name="load-users"]');
            SpecHelper.submitForm(elem);
            $httpBackend.flush();
        }
    });

    describe('selectStrategy === cohortStatus', () => {
        it('should load users by cohort', () => {
            SpecHelper.toggleRadio(elem, 'input[value="cohortStatus"]');

            // Needed to ensure 'application-registered' is in the DOM
            jest.spyOn(cohorts[1], 'supportsRecurringPayments', 'get').mockReturnValue(true);

            SpecHelper.expectElementDisabled(elem, '[name="load-users"]');
            SpecHelper.updateSelect(elem, '[name="cohort"]', cohorts[1]);
            SpecHelper.updateSelect(elem, '[name="cohort-status"]', 'rejected');
            SpecHelper.updateSelect(elem, '[name="graduation-status"]', 'pending');
            SpecHelper.updateSelect(elem, '[name="application-registered"]', 'TRUE');

            $httpBackend
                .expectPOST(`${$window.ENDPOINT_ROOT}/api/users/batch_edit_users_index.json`, {
                    cohort_id: cohorts[1].id,
                    cohort_status: 'rejected',
                    graduation_status: 'pending',
                    application_registered: 'TRUE',
                    per_page: 9999999999,
                })
                .respond({
                    contents: {
                        users: [users[0], users[1]],
                    },
                });
            // SpecHelper.click(elem, '[name="load-users"]');
            SpecHelper.submitForm(elem);
            SpecHelper.expectElementDisabled(elem, '[name="cohort"]');
            SpecHelper.expectElementDisabled(elem, '[name="cohort-status"]');
            SpecHelper.expectElementDisabled(elem, '[name="graduation-status"]');
            SpecHelper.expectElementDisabled(elem, '[name="application-registered"]');
            $httpBackend.flush();
            assertLoadedUsers([users[0], users[1]]);
            expect(scope.viewModel.gotoSection).toHaveBeenCalledWith('update-users');
        });

        it('should show modal if trying to update more than 1000 users', () => {
            SpecHelper.toggleRadio(elem, 'input[value="cohortStatus"]');

            // Needed to ensure 'application-registered' is in the DOM
            jest.spyOn(cohorts[1], 'supportsRecurringPayments', 'get').mockReturnValue(true);

            SpecHelper.expectElementDisabled(elem, '[name="load-users"]');
            SpecHelper.updateSelect(elem, '[name="cohort"]', cohorts[1]);
            SpecHelper.updateSelect(elem, '[name="cohort-status"]', 'rejected');
            SpecHelper.updateSelect(elem, '[name="graduation-status"]', 'pending');
            SpecHelper.updateSelect(elem, '[name="application-registered"]', 'TRUE');

            const _users = [];
            for (let i = 0; i < 1001; i++) {
                _users.push(
                    User.fixtures.getInstance({
                        id: guid.generate(),
                    }),
                );
            }

            $httpBackend
                .expectPOST(`${$window.ENDPOINT_ROOT}/api/users/batch_edit_users_index.json`, {
                    cohort_id: cohorts[1].id,
                    cohort_status: 'rejected',
                    graduation_status: 'pending',
                    application_registered: 'TRUE',
                    per_page: 9999999999,
                })
                .respond({
                    contents: {
                        users: _users,
                    },
                });

            SpecHelper.submitForm(elem);

            jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
            $httpBackend.flush();

            expect(DialogModal.alert).toHaveBeenCalledWith({
                content: 'Please update 1000 or less users (currently 1001)',
            });
            expect(scope.viewModel.loadedUsers).toBeUndefined();
        });
    });

    function assertLoadedUsers(expectedUsers) {
        expect(_.pluck(scope.viewModel.loadedUsers, 'id')).toEqual(_.pluck(expectedUsers, 'id'));
    }

    function render() {
        renderer = SpecHelper.renderer();
        Cohort.expect('index').returns(cohorts);
        renderer.scope.viewModel = BatchEditUsersViewModel.instance;
        Cohort.flush('index');
        renderer.render('<batch-edit-users-select-users view-model="viewModel" ></batch-edit-users-select-users>');
        elem = renderer.elem;
        scope = elem.isolateScope();
        jest.spyOn(scope.viewModel, 'gotoSection').mockImplementation(() => {});
    }
});
