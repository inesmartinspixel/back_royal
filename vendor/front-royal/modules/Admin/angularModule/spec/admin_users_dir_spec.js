import 'AngularSpecHelper';
import 'Admin/angularModule';
import sectionNavigationLocales from 'Navigation/locales/navigation/section_navigation-en.json';
import selectSkillsFormLocales from 'Careers/locales/careers/edit_career_profile/select_skills_form-en.json';
import fieldOptionsLocales from 'Careers/locales/careers/field_options-en.json';
import jobPreferencesFormLocales from 'Careers/locales/careers/edit_career_profile/job_preferences_form-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(sectionNavigationLocales, selectSkillsFormLocales, fieldOptionsLocales, jobPreferencesFormLocales);

describe('Admin::AdminUsers', () => {
    let $injector;
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let currentUser;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Admin', 'SpecHelper');

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            SpecHelper = $injector.get('SpecHelper');
        });

        currentUser = SpecHelper.stubCurrentUser('admin');
        SpecHelper.stubDirective('editableThingsList');
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.render('<admin-users section="applicants"></admin-users>');
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    it('should remove Users admin for interviewers', () => {
        jest.spyOn(currentUser, 'roleName').mockReturnValue('interviewer');
        render();
        expect(scope.availableSections.map(s => s.id)).toEqual(['applicants']);
    });
});
