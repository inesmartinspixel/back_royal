import 'AngularSpecHelper';
import 'Admin/angularModule';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';
import sectionNavigationLocales from 'Navigation/locales/navigation/section_navigation-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(sectionNavigationLocales);

describe('Admin::BatchEditUsersDir', () => {
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let User;
    let users;
    let $timeout;
    let viewModel;
    let cohorts;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Admin', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            User = $injector.get('User');
            SpecHelper.stubCurrentUser('admin');
            SpecHelper.disableHttpQueue();
            $timeout = $injector.get('$timeout');
            const Cohort = $injector.get('Cohort');
            $injector.get('CohortFixtures');
            cohorts = [Cohort.fixtures.getInstance(), Cohort.fixtures.getInstance()];
            Cohort.expect('index').returns(cohorts);
            viewModel = $injector.get('BatchEditUsersViewModel').instance;
            Cohort.flush('index');

            users = [
                User.fixtures.getInstance(),
                User.fixtures.getInstance(),
                User.fixtures.getInstance(),
                User.fixtures.getInstance(),
            ];
            viewModel.loadedUsers = users;
            jest.spyOn(viewModel, 'gotoSection').mockImplementation(() => {});
            render();
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    it('should forward back to select page from update page if there are no loaded users', () => {
        scope.section = 'update-users';
        viewModel.gotoSection.mockClear();
        viewModel.loadedUsers = [];
        scope.$digest();
        $timeout.flush();
        expect(viewModel.gotoSection).toHaveBeenCalledWith('select-users');
    });

    it('should allow nav to update page if there are loaded users', () => {
        scope.section = 'update-users';
        viewModel.gotoSection.mockClear();
        scope.$digest();
        $timeout.flush();
        expect(viewModel.gotoSection).not.toHaveBeenCalled();
    });

    it('should forward back to update page from review page if there are incomplete update params', () => {
        scope.section = 'review-changes';
        viewModel.updateParams = {
            cohort_id: 'asdasd', // not valid because no cohort_status
        };
        viewModel.gotoSection.mockClear();
        scope.$digest();
        $timeout.flush();
        expect(viewModel.gotoSection).toHaveBeenCalledWith('update-users');
    });

    it('should forward back to select page from review page if there are no loaded users', () => {
        scope.section = 'review-changes';
        viewModel.updateParams = {
            cohort_id: 'asdasd',
            cohort_status: 'asdasdasdas',
        };
        viewModel.loadedUsers = [];
        viewModel.gotoSection.mockClear();
        scope.$digest();
        $timeout.flush();
        expect(viewModel.gotoSection).toHaveBeenCalledWith('select-users');
    });

    it('should allow nav to review page if there are valid update params and loaded users', () => {
        scope.section = 'review-changes';
        viewModel.updateParams = {
            cohort_id: 'asdasd',
            cohort_status: 'asdasdasdas',
        };
        viewModel.gotoSection.mockClear();
        scope.$digest();
        $timeout.flush();
        expect(viewModel.gotoSection).not.toHaveBeenCalled();
    });

    it('should allow nav to review page if there are updated users', () => {
        scope.section = 'review-changes';
        viewModel.updateParams = {
            cohort_id: 'asdasd',
            cohort_status: 'asdasdasdas',
        };
        scope.viewModel.loadedUsers = [];
        scope.viewModel.updatedUsers = users;
        viewModel.gotoSection.mockClear();
        scope.$digest();
        $timeout.flush();
        expect(viewModel.gotoSection).not.toHaveBeenCalled();
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.render('<batch-edit-users ></batch-edit-users>');
        elem = renderer.elem;
        scope = elem.isolateScope();
    }
});
