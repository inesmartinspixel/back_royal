import 'AngularSpecHelper';
import 'Admin/angularModule';

describe('Admin::EditGroupDir', () => {
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let Group;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Admin', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            Group = $injector.get('Group');
            SpecHelper.stubCurrentUser('admin');
            render();
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    it('should allow for editing a group name', () => {
        expect(scope.group.name).toBe('A_GROUP_NAME');
        SpecHelper.updateTextInput(elem, 'input[name="group_name"]', 'ANOTHER_GROUP_NAME');
        expect(scope.group.name).toBe('ANOTHER_GROUP_NAME');
    });

    it('should save and update the group name when update button is clicked', () => {
        Group.expect('save');
        SpecHelper.expectElement(elem, 'button[type="submit"]');
        // updateButton.click();
        SpecHelper.submitForm(elem);
        Group.flush('save');
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.group = Group.new({
            id: 1,
            name: 'A_GROUP_NAME',
        });

        renderer.render('<edit-group group="group" ></edit-group>');
        elem = renderer.elem;
        scope = elem.isolateScope();
    }
});
