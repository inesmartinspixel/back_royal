import 'AngularSpecHelper';
import 'Admin/angularModule';
import 'Users/angularModule/spec/_mock/fixtures/users';

import { uniqBy } from 'lodash/fp';

describe('Admin::adminUsersLegacyDir', () => {
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let $injector;
    let User;
    let DialogModal;
    let ngToast;
    let CohortApplication;
    let LogInAs;
    let dateHelper;
    let Institution;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Admin', 'FrontRoyal.Institutions', 'SpecHelper');

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;

            SpecHelper = $injector.get('SpecHelper');
            User = $injector.get('User');
            $injector.get('UserFixtures');
            DialogModal = $injector.get('DialogModal');
            ngToast = $injector.get('ngToast');
            CohortApplication = $injector.get('CohortApplication');
            LogInAs = $injector.get('LogInAs');
            dateHelper = $injector.get('dateHelper');
            Institution = $injector.get('Institution');
        });

        render();
    });

    describe('header', () => {
        it('should set the text to User Admin', () => {
            const AppHeaderViewModel = $injector.get('Navigation.AppHeader.AppHeaderViewModel');

            jest.spyOn(AppHeaderViewModel, 'setTitleHTML').mockImplementation(() => {});
            render();

            expect(AppHeaderViewModel.setTitleHTML).toHaveBeenCalledWith('USER<br>ADMIN');
        });
    });

    it('should show a list of users', () => {
        const user = scope.users[0];

        user.institutions = [
            Institution.new({
                id: '111',
                name: 'abc',
                groups: [
                    {
                        name: 'institution_group',
                    },
                ],
            }),
            Institution.new({
                id: '222',
                name: 'def',
                groups: [],
            }),
        ];

        user.cohort_applications = [
            CohortApplication.new({
                status: 'accepted',
                cohort_id: 'some_cohort',
                cohort_name: 'SOME COHORT',
                graduation_status: 'failed',
            }),
            CohortApplication.new({
                status: 'accepted',
                cohort_id: 'some_pending_cohort',
                cohort_name: 'SOME PENDING COHORT',
                graduation_status: 'pending',
            }),
        ];

        scope.$digest();
        const trs = SpecHelper.expectElements(elem, 'tr[name="user"]', 5);
        const tds = trs.eq(0).find('td');
        SpecHelper.expectElementText(elem, tds.eq(0), user.name);
        SpecHelper.expectElementText(elem, tds.eq(1), user.email);
        SpecHelper.expectElementText(elem, tds.eq(2), dateHelper.formattedUserFacingDateTime(user.created_at * 1000));
        SpecHelper.expectElementText(elem, tds.eq(3), 'LEARNER');
        SpecHelper.expectElementText(
            elem,
            tds.eq(4),
            `${user.userGroupNames().join(' ')} INSTITUTION_GROUP COHORT_GROUP`,
        );
        SpecHelper.expectElementText(elem, tds.eq(5), 'ABC DEF');
        SpecHelper.expectElementText(elem, tds.eq(6).find('h3').eq(0), 'SOME COHORT / FAILED');
        SpecHelper.expectElementText(elem, tds.eq(6).find('h3').eq(1).eq(0), 'SOME PENDING COHORT / ACCEPTED');
        SpecHelper.expectElementText(elem, tds.eq(7), 'Edit');
        SpecHelper.expectElementText(elem, tds.eq(8), 'Delete');
        SpecHelper.expectElementText(elem, tds.eq(9), 'Log In As');
    });

    it('should show a group accessed through a pending cohort', () => {
        const user = scope.users[0];

        user.institutions = [
            Institution.new({
                id: '111',
                name: 'abc',
                groups: [
                    {
                        name: 'institution_group',
                    },
                ],
            }),
            Institution.new({
                id: '222',
                name: 'def',
                groups: [],
            }),
        ];

        user.cohort_applications = [
            CohortApplication.new({
                status: 'pending',
                cohort_id: 'some_pending_cohort',
            }),
        ];

        scope.$digest();
        const trs = SpecHelper.expectElements(elem, 'tr[name="user"]', 5);
        const tds = trs.eq(0).find('td');
        SpecHelper.expectElementText(
            elem,
            tds.eq(4),
            `${user.userGroupNames().join(' ')} INSTITUTION_GROUP PENDING_COHORT_GROUP`,
        );
        SpecHelper.expectElementText(
            elem,
            tds.eq(4),
            `${user.userGroupNames().join(' ')} INSTITUTION_GROUP PENDING_COHORT_GROUP`,
        );
    });

    it('should show a pagination component', () => {
        const topPagination = SpecHelper.expectElements(elem, 'ul.pagination', 2).eq(0);
        SpecHelper.expectElements(topPagination, 'li.prev');
        SpecHelper.expectElements(topPagination, 'li.next');
        SpecHelper.expectElements(topPagination, 'li.ng-scope', 1);
        scope.totalPages = 5;
        scope.$digest();
        SpecHelper.expectElements(topPagination, 'li.ng-scope', 5);
    });

    it('should allow for freetext search', () => {
        expectIndexCall();
        SpecHelper.updateTextInput(elem, 'input[name="freetext_search"]', 'pedago');
        const submitButton = SpecHelper.expectElement(elem, 'button[name="freetext_search_submit"]');
        submitButton.click();
        User.flush('index');
        // ensure we always reset back to page 1 on a new freetext search
        expect(scope.params.page).toEqual(1);
    });

    it('should allow for filtering by group', () => {
        render();
        expectIndexCall();
        SpecHelper.selectOptionByLabel(elem, '[ng-model="params.group_name"]', 'ghi');
        SpecHelper.click(elem, '[name="freetext_search_submit"]');
        User.flush('index');
        // ensure we always reset back to page 1 on a filter
        expect(scope.params.page).toEqual(1);
    });

    it('should allow for filtering by role', () => {
        render();
        expectIndexCall();
        SpecHelper.selectOptionByLabel(elem, '[ng-model="params.role_id"]', 'ADMIN');
        SpecHelper.click(elem, '[name="freetext_search_submit"]');
        User.flush('index');
        // ensure we always reset back to page 1 on a filter
        expect(scope.params.page).toEqual(1);
    });

    it('should allow for filtering by institution', () => {
        render();
        expectIndexCall();
        SpecHelper.selectOptionByLabel(elem, '[ng-model="params.institution_id"]', 'DEMOUSER');
        SpecHelper.click(elem, '[name="freetext_search_submit"]');
        User.flush('index');
        // ensure we always reset back to page 1 on a filter
        expect(scope.params.page).toEqual(1);
    });

    it('should allow for filtering by last cohort status', () => {
        render();
        expectIndexCall();
        SpecHelper.selectOptionByLabel(elem, '[ng-model="params.last_cohort_status"]', 'DEFERRED');
        SpecHelper.click(elem, '[name="freetext_search_submit"]');
        User.flush('index');
        // ensure we always reset back to page 1 on a filter
        expect(scope.params.page).toEqual(1);
    });

    it('should allow for filtering by cohort application registered status', () => {
        render();
        expectIndexCall();
        SpecHelper.selectOptionByLabel(elem, '[ng-model="params.application_registered"]', 'TRUE');
        SpecHelper.click(elem, '[name="freetext_search_submit"]');
        User.flush('index');
        // ensure we always reset back to page 1 on a filter
        expect(scope.params.page).toEqual(1);
    });

    it('should open the edit page for a user when editUser is clicked', () => {
        jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
        SpecHelper.click(elem, 'button[name="edit"]', 0);
        const user = scope.users[0];
        expect(DialogModal.alert).toHaveBeenCalledWith({
            content:
                '<edit-user-details enable-update="true" users="users" user="user" group-options="groupOptions" institution-options="institutionOptions" cohort-options="cohortOptions" global-role-options="globalRoleOptions" editor-role-options="editorRoleOptions" cohort-promotions="cohortPromotions"></edit-user-details>',
            title: user.email,
            size: 'large',
            scope: {
                users: scope.users,
                user,
                groupOptions: scope.groupOptions,
                globalRoleOptions: scope.globalRoleOptions,
                editorRoleOptions: scope.editorRoleOptions,
                institutionOptions: scope.institutionOptions,
                cohortOptions: scope.cohortOptions,
                cohortPromotions: scope.cohortPromotions,
            },
        });
    });

    it('should show the create form when createUser is clicked', () => {
        SpecHelper.stubDirective('addUser');
        SpecHelper.expectNoElement(elem, '.user-creation-form');
        SpecHelper.click(elem, 'button[name="showCreate"]', 0);
        SpecHelper.expectElement(elem, '.user-creation-form', 1);
        SpecHelper.expectNoElement(elem, '.main-users-list');
    });

    describe('delete', () => {
        it('should delete if confirmed', () => {
            jest.spyOn(DialogModal, 'confirm').mockImplementation(() => {
                scope.__onDeleteConfirm();
            });
            const user = scope.users[0];
            User.expect('destroy');
            jest.spyOn(ngToast, 'create').mockImplementation(() => {});
            SpecHelper.click(elem, 'button[name="delete"]', 0);
            User.flush('destroy');
            expect(ngToast.create).toHaveBeenCalledWith({
                content: `${user.email} successfully deleted.`,
                className: 'success',
            });
        });
        it('should not delete if not confirmed', () => {
            jest.spyOn(DialogModal, 'confirm').mockImplementation(() => {});
            // this would error if the destroy call was made.  Since
            // there is no error, we know it wasn't made
            SpecHelper.click(elem, 'button[name="delete"]', 0);
        });
    });

    describe('logInAsUser', () => {
        it('should log in the admin as another user', () => {
            const user = scope.users[0];
            jest.spyOn(scope, 'logInAsUser');
            jest.spyOn(LogInAs, 'logInAsUser').mockImplementation(() => {});

            SpecHelper.click(elem, 'button[name="logInAs"]', 0);

            expect(scope.logInAsUser).toHaveBeenCalledWith(user);
            expect(LogInAs.logInAsUser).toHaveBeenCalledWith(user);
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function expectIndexCall() {
        const users = [
            User.fixtures.getInstance(),
            User.fixtures.getInstance(),
            User.fixtures.getInstance(),
            User.fixtures.getInstance(),
            User.fixtures.getInstance(),
        ];
        const uniqInstitutionsFromUsers = uniqBy('id')(users.map(user => user.institutions).flat());
        const institutions = uniqInstitutionsFromUsers.concat([{ id: 1, name: 'demouser' }]);
        User.expect('index').returns({
            result: users,
            meta: {
                available_groups: ['abc', 'def', 'ghi'],
                institutions,
                cohorts: [
                    {
                        id: 'some_cohort',
                        name: 'SOME COHORT',
                        groups: [
                            {
                                name: 'cohort_group',
                            },
                        ],
                    },
                    {
                        id: 'some_pending_cohort',
                        name: 'SOME PENDING COHORT',
                        groups: [
                            {
                                name: 'pending_cohort_group',
                            },
                        ],
                    },
                ],
                cohort_promotions: [
                    {
                        prop: 'just pass me along',
                    },
                ],
                global_roles: [
                    {
                        id: 1,
                        name: 'admin',
                        resource_id: null,
                        resource_type: null,
                        created_at: '2014-01-14T22:59:56.678Z',
                        updated_at: '2014-01-14T22:59:56.678Z',
                    },
                    {
                        id: 2,
                        name: 'learner',
                        resource_id: null,
                        resource_type: null,
                        created_at: '2014-01-14T23:30:53.412Z',
                        updated_at: '2014-01-14T23:30:53.412Z',
                    },
                    {
                        id: 3,
                        name: 'editor',
                        resource_id: null,
                        resource_type: null,
                        created_at: '2014-01-14T23:30:53.462Z',
                        updated_at: '2014-01-14T23:30:53.462Z',
                    },
                ],
                scoped_roles: [
                    {
                        id: 4,
                        name: 'editor',
                        resource_id: 'some_lesson_id',
                        resource_type: 'Lesson',
                        created_at: '2014-01-14T22:59:56.678Z',
                        updated_at: '2014-01-14T22:59:56.678Z',
                    },
                ],
                total_count: 5,
                per_page: 10,
                page: 1,
            },
        });
    }

    function render() {
        expectIndexCall();

        renderer = SpecHelper.renderer();
        renderer.render('<admin-users-legacy></admin-users-legacy>');
        elem = renderer.elem;
        scope = elem.isolateScope();
        SpecHelper.expectElement(elem, 'front-royal-spinner');
        User.flush('index');
        SpecHelper.expectNoElement(elem, 'front-royal-spinner');
    }
});
