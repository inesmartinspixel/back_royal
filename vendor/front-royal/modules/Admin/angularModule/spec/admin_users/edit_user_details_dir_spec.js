import 'AngularSpecHelper';
import 'Admin/angularModule';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';
import 'Lessons/angularModule/spec/_mock/fixtures/lessons';
import telInputLocales from 'Telephone/locales/telephone/tel_input-en.json';
import setSpecLocales from 'Translation/setSpecLocales';
import 'Users/angularModule/spec/_mock/fixtures/users';

setSpecLocales(telInputLocales);

describe('Admin::EditUserDetailsDir', () => {
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let $window;
    let User;
    let Lesson;
    let HiringApplication;
    let userIndex;
    let $injector;
    let BillingTransaction;
    let Institution;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Admin', 'FrontRoyal.Institutions', 'SpecHelper');

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            SpecHelper = $injector.get('SpecHelper');
            $window = $injector.get('$window');
            User = $injector.get('User');
            Lesson = $injector.get('Lesson');
            SpecHelper.stubCurrentUser('admin');
            $injector.get('UserFixtures');
            $injector.get('LessonFixtures');
            $injector.get('CohortFixtures');
            Lesson.expect('index');
            SpecHelper.stubCurrentUser('admin');
            HiringApplication = $injector.get('HiringApplication');
            BillingTransaction = $injector.get('BillingTransaction');
            Institution = $injector.get('Institution');

            SpecHelper.stubDirective('editUserDetailsEnrollment');
            SpecHelper.stubDirective('itemGroupsEditor');
            SpecHelper.stubConfig();
            render();
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('global role', () => {
        it('should allow for editing a role', () => {
            expect(scope.editUser.roleName()).toBe('learner');
            SpecHelper.updateSelectize(elem, '[name="addRole"]', 3);
            expect(scope.editUser.roleName()).toBe('editor');
        });
    });

    describe('mba content locked flag', () => {
        it('should allow for toggling content locked flag if in smarter group', () => {
            scope.editUser.mba_content_lockable = true;
            scope.editUser.groups = [
                {
                    id: 'some_guid',
                    name: 'SMARTER',
                },
            ];
            scope.$digest();
            SpecHelper.expectCheckboxChecked(elem, 'input[name="mba_content_lockable"]');
            SpecHelper.toggleCheckbox(elem, 'input[name="mba_content_lockable"]');
            expect(scope.editUser.mba_content_lockable).toBe(false);
        });
    });

    describe('deactivated flag', () => {
        it('should allow for toggling deactivated flag', () => {
            scope.editUser.deactivated = false;
            scope.$digest();
            SpecHelper.expectCheckboxUnchecked(elem, 'input[name="deactivated"]');
            SpecHelper.toggleCheckbox(elem, 'input[name="deactivated"]');
            expect(scope.editUser.deactivated).toBe(true);
        });
    });

    describe('academic_hold flag', () => {
        it('should allow for toggling academic_hold flag', () => {
            scope.editUser.academic_hold = false;
            scope.$digest();
            SpecHelper.expectCheckboxUnchecked(elem, 'input[name="academic_hold"]');

            SpecHelper.toggleCheckbox(elem, 'input[name="academic_hold"]');
            SpecHelper.expectCheckboxChecked(elem, 'input[name="academic_hold"]');
            expect(scope.editUser.academic_hold).toBe(true);

            SpecHelper.toggleCheckbox(elem, 'input[name="academic_hold"]');
            SpecHelper.expectCheckboxUnchecked(elem, 'input[name="academic_hold"]');
            expect(scope.editUser.academic_hold).toBe(false);
        });
    });

    describe('institutions', () => {
        it('should allow for adding a institution', () => {
            scope.editUser.institutions = [];
            scope.$digest();
            expect(scope.editUser.institutions.length).toBe(0);
            SpecHelper.updateSelectize(elem, '[name="addInstitution"]', scope.institutionOptions[1].id);
            expect(scope.editUser.institutions.length).toBe(1);
            expect(scope.editUser.institutions[0].name).toEqual('def');
        });

        it('should allow for deleting an institution', () => {
            scope.editUser.institutions = [
                Institution.new({
                    id: '123',
                    name: 'abc',
                }),
            ];
            scope.$digest();
            expect(scope.editUser.institutions.length).toBe(1);
            const deleteButton = SpecHelper.expectElements(
                elem,
                '.form-group div.col-xs-9 > div > button[data-id="remove_institution"]',
            );

            jest.spyOn($window, 'confirm').mockReturnValue(true);
            deleteButton.click();
            expect($window.confirm).toHaveBeenCalled();
            expect(scope.editUser.institutions.length).toBe(0);
        });

        describe('active_institution', () => {
            it('should allow changing', () => {
                scope.editUser.institutions = [
                    Institution.new({
                        id: '123',
                        name: 'abc',
                    }),
                ];
                scope.editUser.active_institution = scope.editUser.institutions[0];
                scope.$digest();

                SpecHelper.updateSelectize(elem, '[name="addInstitution"]', scope.institutionOptions[1].id);
                SpecHelper.selectOptionByLabel(
                    elem,
                    '[name="active-institution-select"]',
                    scope.institutionOptions[1].name,
                );
                expect(scope.editUser.active_institution.id).toEqual(scope.institutionOptions[1].id);
            });
        });
    });

    describe('hiring applications', () => {
        it('should allow for adding a hiring application', () => {
            expect(scope.editUser.hiring_application).toBe(null);
            SpecHelper.updateSelectize(elem, '[name="hiringApplicationStatus"]', 'pending');
            expect(scope.editUser.hiring_application).not.toBe(null);
            expect(scope.editUser.hiring_application.status).toEqual('pending');
        });

        it('should allow for editing a hiring application', () => {
            scope.editUser.hiring_application = HiringApplication.new({
                status: 'pending',
            });
            SpecHelper.updateSelectize(elem, '[name="hiringApplicationStatus"]', 'accepted');
            expect(scope.editUser.hiring_application.status).toEqual('accepted');
        });
    });

    describe('additional details', () => {
        it('should allow for editing of school, company, job_title, phone, and country fields', () => {
            expect(scope.editUser.school).toBeUndefined();
            SpecHelper.updateTextInput(elem, '[name="school"]', 'school');
            expect(scope.editUser.school).toBe('school');

            expect(scope.editUser.professional_organization).toEqual({
                text: '',
                locale: scope.editUser.pref_locale,
            });

            $injector
                .get('$httpBackend')
                .expectGET(
                    `${$window.ENDPOINT_ROOT}/api/auto_suggest_options.json?filters%5Bin_locale_or_en%5D=en&filters%5Bsearch_text%5D=Foo+Bar,+Inc.&filters%5Bsuggested%5D=true&filters%5Btype%5D=professional_organization&limit=10`,
                )
                .respond(200, {
                    contents: {
                        professional_organization_options: [],
                    },
                });

            SpecHelper.updateInput(elem, '[name="organization"]', 'Foo Bar, Inc.');
            expect(scope.editUser.professional_organization).toEqual({
                locale: scope.editUser.pref_locale,
                text: 'Foo Bar, Inc.',
            });

            expect(scope.editUser.job_title).toBeUndefined();
            SpecHelper.updateTextInput(elem, '[name="job_title"]', 'job_title');
            expect(scope.editUser.job_title).toBe('job_title');

            expect(scope.editUser.phone).toBeNull();

            SpecHelper.updateInput(elem, 'input[name="phone"]', '(201) 555-5555');
            expect(scope.editUser.phone).toBeNull();

            SpecHelper.updateSelect(elem, 'select[name="countryCode"]', 'US');
            expect(scope.editUser.phone).toBe('+12015555555');

            expect(scope.editUser.country).toBeUndefined();
            const expectedCountry = scope.countryOptions[1].value;
            expect(expectedCountry).not.toBeUndefined();
            SpecHelper.updateSelectize(elem, '[name="country"]', expectedCountry);
            expect(scope.editUser.country).toBe(expectedCountry);
        });

        it('should set the professional_organization locale to the pref_locale of the user being edited', () => {
            // check pref_locale and professional organization locale for user being edited
            expect(scope.editUser.pref_locale).toEqual('en');
            expect(scope.editUser.professional_organization).toEqual({
                text: '',
                locale: 'en',
            });

            // re-render and change the locale of the user being edited to something other than 'en'
            const locale = 'es';
            render(undefined, {
                prefLocale: locale,
            });

            // expect the edited user's pref_locale to have changed and the locale of the professional organization
            expect(scope.editUser.pref_locale).toEqual(locale);
            expect(scope.editUser.professional_organization).toEqual({
                text: '',
                locale,
            });
        });

        it("should change the locale of the professional_organization if the edited user's pref_locale changes in the form", () => {
            expect(scope.editUser.pref_locale).toEqual('en');
            SpecHelper.updateSelectize(elem, '[name="pref_locale"]', 'es');

            expect(scope.editUser.pref_locale).toEqual('es');
            expect(scope.editUser.professional_organization).toEqual({
                text: '',
                locale: 'es',
            });
        });

        it('should prevent update if invalid phone number', () => {
            expect(scope.editUser.phone).toBeNull();

            SpecHelper.updateInput(elem, 'input[name="phone"]', '(201) 555');
            expect(scope.editUser.phone).toBeNull();

            SpecHelper.updateSelect(elem, 'select[name="countryCode"]', 'US');
            expect(scope.editUser.phone).toBe('+1201555');

            SpecHelper.expectElementDisabled(elem, 'button[type="submit"]');

            SpecHelper.updateInput(elem, 'input[name="phone"]', '(201) 555-5555');
            SpecHelper.expectElementEnabled(elem, 'button[type="submit"]');
        });
    });

    describe('form submission', () => {
        it('should save and update the source user and transmit any cancellation request and refund request when submitted', () => {
            scope.editUser.$$subscriptionCancellationReason = 'some reason';
            scope.editUser.$$issueRefund = true;
            scope.editUser.$$enrollmentActionLabel = 'some label';
            scope.editUser.$$billingTransactionToRecord = BillingTransaction.new({
                amount: 4200,
                provider: 'Paypal',
            });
            User.expect('save').toBeCalledWith(scope.editUser.asJson(), {
                subscription_cancellation_reason: 'some reason',
                issue_refund: true,
                enrollment_action_label: 'some label',
                billing_transaction_to_record: scope.editUser.$$billingTransactionToRecord.asJson(),
            });
            SpecHelper.expectElement(elem, 'button[type="submit"]');
            // submitButton.click();
            SpecHelper.submitForm(elem);
            User.flush('save');
        });
    });

    describe('lesson access listing', () => {
        it('should default to sorting by tag', () => {
            // Expect a class to Lesson.show with our lesson's lesson_id to return the expected lesson
            scope = renderer.childScope;
            expect(scope.sort.column).toEqual('tag');
        });

        it('can sort by title', () => {
            scope = renderer.childScope;
            scope.changeSorting('title');
            expect(scope.sort.column).toEqual('title');
        });

        it('can sort by has reviewer access', () => {
            scope = renderer.childScope;
            scope.changeSorting('reviewer');
            expect(scope.sort.column).toEqual('reviewer');
        });
    });

    describe('lesson initialization', () => {
        it('should initialize all lessons to hidden except when user is author', () => {
            const lesson1 = Lesson.fixtures.getInstance();
            const lesson2 = Lesson.fixtures.getInstance();
            scope.editUser = User.fixtures.getInstanceWithRole('editor');
            const someoneElse = User.fixtures.getInstanceWithRole('editor');
            lesson1.author = {
                id: scope.editUser.id,
            };

            lesson2.author = {
                id: someoneElse.id,
            };

            scope.lessons = [lesson1, lesson2];
            scope.$digest();

            expect(scope.editUser.lessonPermissions[lesson1.id]).toBe('lesson_editor');
            expect(scope.editUser.lessonPermissions[lesson2.id]).toBeUndefined();
        });
    });

    describe('canEditLesson', () => {
        it('should be true if user is author', () => {
            const lesson = Lesson.fixtures.getInstance();
            scope.editUser = User.fixtures.getInstanceWithRole('editor');

            lesson.author = {
                id: scope.editUser.id,
            };

            scope.lessons = [lesson];
            scope.$digest();

            expect(scope.editUser.canEditLesson(lesson)).toBe(true);
        });

        it('should be true only when granted if user is not author', () => {
            const lesson = Lesson.fixtures.getInstance();
            const author = User.fixtures.getInstanceWithRole('editor');
            lesson.author = {
                id: author.id,
            };

            // make sure by default user cant edit someone else's lesson
            scope.lessons = [lesson];
            scope.$digest();
            expect(scope.editUser.canEditLesson(lesson)).toBe(false);

            // non-author user can only edit lesson if explicitly granted
            scope.editUser.roles = [
                {
                    name: 'editor',
                    resource_id: null,
                },
                {
                    name: 'lesson_editor',
                    resource_id: lesson.id,
                },
            ];

            scope.initializeLessons();
            expect(scope.editUser.canEditLesson(lesson)).toBe(true);
        });
    });

    describe('hideAllLessons', () => {
        it('should set all lesson permissions to hidden except those where user is author', () => {
            scope.editUser = User.fixtures.getInstanceWithRole('editor');
            const lesson1 = Lesson.fixtures.getInstance();
            const lesson2 = Lesson.fixtures.getInstance();
            const lesson3 = Lesson.fixtures.getInstance();
            const someoneElse = User.fixtures.getInstanceWithRole('editor');
            lesson1.author = {
                id: someoneElse.id,
            };
            lesson2.author = {
                id: someoneElse.id,
            };
            lesson3.author = {
                id: scope.editUser.id,
            };

            scope.updateLessonPermissions(lesson1, 'reviewer');
            scope.updateLessonPermissions(lesson2, 'previewer');
            scope.updateLessonPermissions(lesson3, 'lesson_editor');
            scope.lessons = [lesson1, lesson2, lesson3];
            scope.$digest();

            scope.onAllClicked('assigned', undefined);
            scope.$digest();

            expect(scope.editUser.lessonPermissions[lesson1.id]).toBeUndefined();
            expect(scope.editUser.lessonPermissions[lesson2.id]).toBeUndefined();
            expect(scope.editUser.lessonPermissions[lesson3.id]).toBe('lesson_editor');
        });
    });

    describe('previewAllLessons', () => {
        it('should set all lesson permissions to previewer except those where user is author', () => {
            scope.editUser = User.fixtures.getInstanceWithRole('editor');
            const lesson1 = Lesson.fixtures.getInstance();
            const lesson2 = Lesson.fixtures.getInstance();
            const lesson3 = Lesson.fixtures.getInstance();
            const someoneElse = User.fixtures.getInstanceWithRole('editor');
            lesson1.author = {
                id: someoneElse.id,
            };
            lesson2.author = {
                id: someoneElse.id,
            };
            lesson3.author = {
                id: scope.editUser.id,
            };

            scope.updateLessonPermissions(lesson1, 'reviewer');
            scope.updateLessonPermissions(lesson2);
            scope.updateLessonPermissions(lesson3, 'lesson_editor');
            scope.lessons = [lesson1, lesson2, lesson3];
            scope.$digest();

            scope.onAllClicked('assigned', 'previewer');
            scope.$digest();

            expect(scope.editUser.lessonPermissions[lesson1.id]).toBe('previewer');
            expect(scope.editUser.lessonPermissions[lesson2.id]).toBeUndefined();
            expect(scope.editUser.lessonPermissions[lesson3.id]).toBe('lesson_editor');

            scope.onAllClicked('unassigned', 'previewer');
            scope.$digest();

            expect(scope.editUser.lessonPermissions[lesson1.id]).toBe('previewer');
            expect(scope.editUser.lessonPermissions[lesson2.id]).toBe('previewer');
            expect(scope.editUser.lessonPermissions[lesson3.id]).toBe('lesson_editor');
        });
    });

    describe('reviewAllLessons', () => {
        it('should set all lesson permissions to previewer except those where user is author', () => {
            scope.editUser = User.fixtures.getInstanceWithRole('editor');
            const lesson1 = Lesson.fixtures.getInstance();
            const lesson2 = Lesson.fixtures.getInstance();
            const lesson3 = Lesson.fixtures.getInstance();
            const someoneElse = User.fixtures.getInstanceWithRole('editor');
            lesson1.author = {
                id: someoneElse.id,
            };
            lesson2.author = {
                id: someoneElse.id,
            };
            lesson3.author = {
                id: scope.editUser.id,
            };

            scope.updateLessonPermissions(lesson1, 'previewer');
            scope.updateLessonPermissions(lesson2);
            scope.updateLessonPermissions(lesson3, 'lesson_editor');
            scope.lessons = [lesson1, lesson2, lesson3];
            scope.$digest();

            scope.onAllClicked('assigned', 'reviewer');
            scope.$digest();

            expect(scope.editUser.lessonPermissions[lesson1.id]).toBe('reviewer');
            expect(scope.editUser.lessonPermissions[lesson2.id]).toBeUndefined();
            expect(scope.editUser.lessonPermissions[lesson3.id]).toBe('lesson_editor');

            scope.onAllClicked('unassigned', 'reviewer');
            scope.$digest();

            expect(scope.editUser.lessonPermissions[lesson1.id]).toBe('reviewer');
            expect(scope.editUser.lessonPermissions[lesson2.id]).toBe('reviewer');
            expect(scope.editUser.lessonPermissions[lesson3.id]).toBe('lesson_editor');
        });
    });

    describe('editAllLessons', () => {
        it('should set all lesson permissions to lesson_editor', () => {
            scope.editUser = User.fixtures.getInstanceWithRole('editor');
            const lesson1 = Lesson.fixtures.getInstance();
            const lesson2 = Lesson.fixtures.getInstance();
            const lesson3 = Lesson.fixtures.getInstance();
            const someoneElse = User.fixtures.getInstanceWithRole('editor');
            lesson1.author = {
                id: someoneElse.id,
            };
            lesson2.author = {
                id: someoneElse.id,
            };
            lesson3.author = {
                id: scope.editUser.id,
            };

            scope.updateLessonPermissions(lesson1, 'previewer');
            scope.updateLessonPermissions(lesson2);
            scope.updateLessonPermissions(lesson3, 'lesson_editor');
            scope.lessons = [lesson1, lesson2, lesson3];
            scope.$digest();

            scope.onAllClicked('assigned', 'lesson_editor');
            scope.$digest();

            expect(scope.editUser.lessonPermissions[lesson1.id]).toBe('lesson_editor');
            expect(scope.editUser.lessonPermissions[lesson2.id]).toBeUndefined();
            expect(scope.editUser.lessonPermissions[lesson3.id]).toBe('lesson_editor');

            scope.onAllClicked('unassigned', 'lesson_editor');
            scope.$digest();

            expect(scope.editUser.lessonPermissions[lesson1.id]).toBe('lesson_editor');
            expect(scope.editUser.lessonPermissions[lesson2.id]).toBe('lesson_editor');
            expect(scope.editUser.lessonPermissions[lesson3.id]).toBe('lesson_editor');
        });
    });

    describe('dupeUser', () => {
        it('should make a duplicate of the user so that uncommitted changes to not show up in the users list', () => {
            SpecHelper.updateTextInput(elem, '[name="job_title"]', 'job_title');
            expect(scope.editUser.job_title).toEqual('job_title');
            expect(scope.users[userIndex].job_title).not.toEqual('job_title');

            // save should commit changes to users list
            save();
            expect(scope.editUser.job_title).toEqual('job_title');
            expect(scope.users[userIndex].job_title).toEqual('job_title');

            // after save, we should still be editing a dupe
            SpecHelper.updateTextInput(elem, '[name="job_title"]', 'job_title_updated_again');
            expect(scope.editUser.job_title).toEqual('job_title_updated_again');
            expect(scope.users[userIndex].job_title).not.toEqual('job_title_updated_again');

            // save should commit changes to users list
            save();
            expect(scope.editUser.job_title).toEqual('job_title_updated_again');
            expect(scope.users[userIndex].job_title).toEqual('job_title_updated_again');
        });

        it('should edit the actual user passed in if dupeUser=false', () => {
            render({
                dupeUser: false,
            });
            SpecHelper.updateTextInput(elem, '[name="job_title"]', 'job_title');
            expect(scope.editUser.job_title).toEqual('job_title');
            expect(scope.users[userIndex].job_title).toEqual('job_title');
        });

        function save() {
            User.expect('update');
            SpecHelper.expectElement(elem, 'button[type="submit"]');
            // submitButton.click();
            SpecHelper.submitForm(elem);
            User.flush('update');
        }
    });

    describe('error_issuing_refund', () => {
        it('should show a modal', () => {
            const DialogModal = $injector.get('DialogModal');
            jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
            save();
            expect(DialogModal.alert).toHaveBeenCalledWith({
                title: 'Error issuing refund',
                content: 'Some error message',
            });
        });

        function save() {
            User.expect('update').returnsMeta({
                error_issuing_refund: 'Some error message',
            });
            SpecHelper.expectElement(elem, 'button[type="submit"]');
            // submitButton.click();
            SpecHelper.submitForm(elem);
            User.flush('update');
        }
    });

    function render(opts = {}, userOverrides = {}) {
        renderer = SpecHelper.renderer();
        renderer.scope.users = [
            User.fixtures.getInstance(),
            User.fixtures.getInstance(),
            User.fixtures.getInstance(),
            User.fixtures.getInstance(),
            User.fixtures.getInstance(),
        ];
        userIndex = 1;
        const user = renderer.scope.users[userIndex];
        user.pref_locale = userOverrides.prefLocale || 'en';

        renderer.scope.user = user;
        renderer.scope.groupOptions = ['abc', 'def', 'ghi'];

        renderer.scope.institutionOptions = [
            {
                id: '1',
                name: 'abc',
            },
            {
                id: '2',
                name: 'def',
            },
            {
                id: '3',
                name: 'ghi',
            },
        ];

        renderer.scope.globalRoleOptions = [
            {
                id: 1,
                name: 'admin',
                resource_id: null,
                resource_type: null,
                created_at: '2014-01-14T22:59:56.678Z',
                updated_at: '2014-01-14T22:59:56.678Z',
            },
            {
                id: 2,
                name: 'learner',
                resource_id: null,
                resource_type: null,
                created_at: '2014-01-14T23:30:53.412Z',
                updated_at: '2014-01-14T23:30:53.412Z',
            },
            {
                id: 3,
                name: 'editor',
                resource_id: null,
                resource_type: null,
                created_at: '2014-01-14T23:30:53.462Z',
                updated_at: '2014-01-14T23:30:53.462Z',
            },
        ];
        renderer.scope.editorRoleOptions = [
            {
                id: 1,
                name: 'reviewer',
                resource_id: 'some_guid',
                resource_type: 'Lesson',
                created_at: '2014-01-14T22:59:56.678Z',
                updated_at: '2014-01-14T22:59:56.678Z',
            },
        ];

        _.extend(renderer.scope, opts);
        let extra = '';
        if (_.has(renderer.scope, 'dupeUser')) {
            extra = 'dupe-user="dupeUser"';
        }
        renderer.render(
            `<edit-user-details enable-update="true" users="users" user="user" group-options="groupOptions" institution-options="institutionOptions" global-role-options="globalRoleOptions" editor-role-options="editorRoleOptions" ${extra}></edit-user-details>`,
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
    }
});
