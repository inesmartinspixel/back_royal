import 'AngularSpecHelper';
import 'Admin/angularModule';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohort_applications';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';
import 'Users/angularModule/spec/_mock/fixtures/users';
import moment from 'moment-timezone';

describe('Admin::EditUserDetailsEnrollmentDir', () => {
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let $window;
    let User;
    let CohortApplication;
    let Cohort;
    let baseStartDate;
    let $timeout;
    let $injector;
    let embaAcceptedMonthlyAppAttrs;
    let refundDetails;
    let $http;
    let $q;
    let DeferralLink;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Admin', 'SpecHelper');

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            SpecHelper = $injector.get('SpecHelper');
            $window = $injector.get('$window');
            $timeout = $injector.get('$timeout');
            User = $injector.get('User');
            $injector.get('UserFixtures');
            $injector.get('CohortFixtures');
            CohortApplication = $injector.get('CohortApplication');
            Cohort = $injector.get('Cohort');
            $injector.get('CohortApplicationFixtures');
            $http = $injector.get('$http');
            $q = $injector.get('$q');
            DeferralLink = $injector.get('DeferralLink');

            // I'm sure there is a cleaner way to mock out the refund_details
            // call, but this works.
            const get = $http.get;
            jest.spyOn($http, 'get').mockImplementation((...args) => {
                if (args[0].match('api/cohort_applications/refund_details.json')) {
                    return $q.when({
                        data: {
                            contents: {
                                refund_details: refundDetails,
                            },
                        },
                    });
                }
                return get.apply($http, args);
            });

            SpecHelper.stubCurrentUser('admin');
            baseStartDate = Date.now() / 1000;

            const scholarshipLevels = [
                {
                    name: 'No Scholarship',
                    coupons: {
                        default_plan_id: {
                            id: 'none',
                            amount_off: 0,
                            percent_off: 0,
                        },
                    },
                    standard: {
                        default_plan_id: {
                            id: 'none',
                            amount_off: 0,
                            percent_off: 0,
                        },
                    },
                    early: {
                        default_plan_id: {
                            id: 'none',
                            amount_off: 0,
                            percent_off: 0,
                        },
                    },
                },
                {
                    name: 'Level 1',
                    coupons: {
                        default_plan_id: {
                            id: 'discount_150',
                            amount_off: 15000,
                            percent_off: undefined,
                        },
                    },
                    standard: {
                        default_plan_id: {
                            id: 'discount_150',
                            amount_off: 15000,
                            percent_off: undefined,
                        },
                    },
                    early: {
                        default_plan_id: {
                            id: 'discount_150',
                            amount_off: 15000,
                            percent_off: undefined,
                        },
                    },
                },
            ];
            embaAcceptedMonthlyAppAttrs = CohortApplication.fixtures.getInstance({
                cohort_id: '5',
                program_type: 'emba',
                status: 'accepted',
                total_num_required_stripe_payments: 12,
                registered: true,
                stripe_plans: [
                    {
                        id: 'default_plan_id',
                        frequency: 'monthly',
                        amount: 80000,
                    },
                    {
                        id: 'another_plan_id',
                        frequency: 'once',
                        amount: 960000,
                    },
                ],
                scholarship_levels: scholarshipLevels,
                scholarship_level: scholarshipLevels[1],
                cohort_slack_room_id: 'cohort_slack_room_id',
            });

            render();
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('enrollment actions', () => {
        const actionOptionsSelector =
            '[name="enrollment_action"] + .selectize-control .selectize-dropdown-content .option';
        const cohortOptionsSelector =
            '[name="transfer_cohort"] + .selectize-control .selectize-dropdown-content .option';
        const cancelOptionsSelector =
            '[name="cancellation_reason"] selectize + .selectize-control .selectize-dropdown-content .option';

        let cohortApplications;

        beforeEach(() => {
            cohortApplications = [CohortApplication.fixtures.getInstance(embaAcceptedMonthlyAppAttrs)];
        });

        it('should set $$enrollmentActionLabel', () => {
            render(undefined, {
                cohort_applications: cohortApplications,
            });
            const actionOption = elem.find(actionOptionsSelector).eq(0);
            const action = actionOption.attr('data-value');

            expect(action).toBe('expel'); // sanity
            SpecHelper.updateSelectize(elem, '[name="enrollment_action"]', action);
            SpecHelper.click(elem, 'button.setup-action');
            expect(scope.user.$$enrollmentActionLabel).toEqual('Expel');
        });

        describe('users with a primarySubscription', () => {
            let lastCohortApplication;

            beforeEach(() => {
                render(undefined, {
                    primarySubscription: $injector.get('Subscription').new({}),
                    cohort_applications: cohortApplications,
                });
                lastCohortApplication = scope.user.lastCohortApplication;
                lastCohortApplication.stripe_plan_id = lastCohortApplication.stripe_plans[0].id;
            });

            it('should display the appropriate actions', () => {
                SpecHelper.expectElements(elem, actionOptionsSelector, 5);
                SpecHelper.expectElementText(elem, actionOptionsSelector, 'Expel', 0);
                SpecHelper.expectElementText(elem, actionOptionsSelector, 'Defer Indefinitely', 1);
                SpecHelper.expectElementText(
                    elem,
                    actionOptionsSelector,
                    'Defer into cohort and cancel subscription',
                    2,
                );
                SpecHelper.expectElementText(
                    elem,
                    actionOptionsSelector,
                    'Defer into cohort and maintain subscription',
                    3,
                );
                SpecHelper.expectElementText(
                    elem,
                    actionOptionsSelector,
                    'Cancel subscription and mark as fully paid',
                    4,
                );
            });

            it('should handle the "Expel" action appropriately', () => {
                const actionOption = elem.find(actionOptionsSelector).eq(0);
                const action = actionOption.attr('data-value');

                expect(action).toBe('expel'); // sanity

                SpecHelper.expectNoElement(elem, 'button.setup-action');
                SpecHelper.updateSelectize(elem, '[name="enrollment_action"]', action);
                SpecHelper.expectNoElement(elem, '[name="transfer_cohort"]');

                const reason = elem.find(cancelOptionsSelector).eq(0).attr('data-value');
                expect(reason).not.toBeUndefined();
                SpecHelper.updateSelectize(elem, '[name="cancellation_reason"] selectize', reason);

                SpecHelper.click(elem, 'button.setup-action');

                lastCohortApplication = scope.user.lastCohortApplication;
                expect(lastCohortApplication.status).toEqual('expelled');
                expect(lastCohortApplication.registered).toEqual(false);
                expect(scope.user.$$subscriptionCancellationReason).toEqual(reason);
            });

            it('should handle the "Defer Indefinitely" action appropriately', () => {
                const actionOption = elem.find(actionOptionsSelector).eq(1);
                const action = actionOption.attr('data-value');

                expect(action).toBe('deferIndefinitely'); // sanity

                SpecHelper.expectNoElement(elem, 'button.setup-action');
                SpecHelper.updateSelectize(elem, '[name="enrollment_action"]', action);
                SpecHelper.expectNoElement(elem, '[name="transfer_cohort"]');

                const reason = elem.find(cancelOptionsSelector).eq(0).attr('data-value');
                expect(reason).not.toBeUndefined();
                SpecHelper.updateSelectize(elem, '[name="cancellation_reason"] selectize', reason);

                SpecHelper.click(elem, 'button.setup-action');

                lastCohortApplication = scope.user.lastCohortApplication;
                expect(lastCohortApplication.status).toEqual('deferred');
                expect(lastCohortApplication.registered).toEqual(false);
                expect(scope.user.$$subscriptionCancellationReason).toEqual(reason);
            });

            it('should handle the "Defer into cohort and cancel subscription" action appropriately', () => {
                lastCohortApplication = scope.user.lastCohortApplication;
                const actionOption = elem.find(actionOptionsSelector).eq(2);
                const action = actionOption.attr('data-value');

                const specialScholarshipLevel = {
                    name: 'Special',
                    coupons: {
                        default_plan_id: {
                            id: 'discount_150',
                            amount_off: 15000,
                            percent_off: undefined,
                        },
                    },
                    standard: {
                        default_plan_id: {
                            id: 'discount_150',
                            amount_off: 15000,
                            percent_off: undefined,
                        },
                    },
                    early: {
                        default_plan_id: {
                            id: 'discount_150',
                            amount_off: 15000,
                            percent_off: undefined,
                        },
                    },
                };
                lastCohortApplication.scholarship_levels = [specialScholarshipLevel];
                lastCohortApplication.scholarship_level = specialScholarshipLevel;

                expect(action).toBe('deferCohortCancel'); // sanity

                SpecHelper.expectNoElement(elem, 'button.setup-action');
                SpecHelper.updateSelectize(elem, '[name="enrollment_action"]', action);

                const cohortId = elem.find(cohortOptionsSelector).eq(0).attr('data-value');
                expect(cohortId).not.toBeUndefined();
                SpecHelper.updateSelectize(elem, '[name="transfer_cohort"]', cohortId);

                const reason = elem.find(cancelOptionsSelector).eq(0).attr('data-value');
                expect(reason).not.toBeUndefined();
                SpecHelper.updateSelectize(elem, '[name="cancellation_reason"] selectize', reason);

                SpecHelper.click(elem, 'button.setup-action');

                expect(lastCohortApplication.status).toEqual('deferred');
                expect(lastCohortApplication.registered).toEqual(false);

                const newApplication = scope.user.lastCohortApplication;
                expect(newApplication.status).toEqual('pre_accepted');
                expect(newApplication.registered).toEqual(false);
                expect(newApplication.scholarship_level).toEqual(specialScholarshipLevel);
                expect(newApplication.scholarship_levels).toEqual(lastCohortApplication.scholarship_levels);

                expect(scope.user.$$subscriptionCancellationReason).toEqual(reason);
            });

            it('should handle the "Defer into cohort and maintain subscription" action appropriately', () => {
                lastCohortApplication = scope.user.lastCohortApplication;
                const actionOption = elem.find(actionOptionsSelector).eq(3);
                const action = actionOption.attr('data-value');

                expect(action).toBe('deferCohortMaintain'); // sanity

                // In reality, we would never have a situation where payment_grace_period_end_at was set
                // and the user was already marked as locked_due_to_past_due_payment, but I'm doing this
                // here to verify that the values get transferred over to the newApplication as expected.
                const paymentGracePeriodEndAt = Math.floor(Date.now() / 1000);
                lastCohortApplication.payment_grace_period_end_at = paymentGracePeriodEndAt;
                lastCohortApplication.locked_due_to_past_due_payment = true;

                SpecHelper.expectNoElement(elem, 'button.setup-action');
                SpecHelper.updateSelectize(elem, '[name="enrollment_action"]', action);

                const cohortId = elem.find(cohortOptionsSelector).eq(0).attr('data-value');
                expect(cohortId).not.toBeUndefined();
                SpecHelper.updateSelectize(elem, '[name="transfer_cohort"]', cohortId);

                SpecHelper.expectNoElement(elem, '[name="cancellation_reason"] selectize');

                SpecHelper.click(elem, 'button.setup-action');

                expect(lastCohortApplication.status).toEqual('deferred');
                expect(lastCohortApplication.registered).toEqual(false);

                const newApplication = scope.user.lastCohortApplication;
                expect(newApplication.status).toEqual('accepted');
                expect(newApplication.registered).toEqual(true);
                expect(newApplication.payment_grace_period_end_at).toEqual(paymentGracePeriodEndAt);
                expect(newApplication.locked_due_to_past_due_payment).toBe(true);

                expect(scope.user.$$subscriptionCancellationReason).toBeUndefined();
            });

            it('should handle the "Cancel subscription and mark as fully paid" action appropriately', () => {
                setupAndSelectCancelFullyPaid();

                const stripePlan = lastCohortApplication.stripe_plans[1];

                const lastAppStatus = lastCohortApplication.status;

                SpecHelper.expectNoElement(elem, '.enrollment-action-inputs [name="transfer_cohort"]');

                const reason = elem.find(cancelOptionsSelector).eq(0).attr('data-value');
                expect(reason).not.toBeUndefined();
                SpecHelper.updateSelectize(
                    elem,
                    '.enrollment-action-inputs [name="cancellation_reason"] selectize',
                    reason,
                );

                expect(lastCohortApplication.registered_early).toBe(false);
                SpecHelper.assertSelectizeValue(elem, '.enrollment-action-inputs [name="registered_early"]', 'no');
                SpecHelper.updateSelectize(elem, '.enrollment-action-inputs [name="registered_early"]', 'yes');

                expect(lastCohortApplication.stripe_plan_id).not.toBe(stripePlan.id);
                expect(lastCohortApplication.stripe_plan_id).not.toBeUndefined();
                SpecHelper.assertSelectizeValue(
                    elem,
                    '.enrollment-action-inputs [name="stripe_plan_id"]',
                    lastCohortApplication.stripe_plan_id,
                );
                SpecHelper.updateSelectize(elem, '.enrollment-action-inputs [name="stripe_plan_id"]', stripePlan.id);

                const expectedBillingTransactionAttrs = fillOutBillingTransactionForm();

                // sanity checks
                expect(lastCohortApplication.status).toEqual(lastAppStatus);
                expect(lastCohortApplication.registered).toEqual(true);
                expect(lastCohortApplication.total_num_required_stripe_payments).not.toEqual(0);
                expect(scope.user.$$subscriptionCancellationReason).toBeUndefined();
                expect(lastCohortApplication.notes).toBeUndefined();

                // We assert below that these values gets updated as part of this action
                lastCohortApplication.locked_due_to_past_due_payment = true;
                lastCohortApplication.payment_grace_period_end_at = Math.floor(Date.now() / 1000);

                SpecHelper.click(elem, 'button.setup-action');

                expect(lastCohortApplication.status).toEqual(lastAppStatus); // should not have changed
                expect(lastCohortApplication.registered).toEqual(true); // should not have changed
                expect(lastCohortApplication.registered_early).toEqual(true);
                expect(lastCohortApplication.stripe_plan_id).toEqual(stripePlan.id);
                expect(lastCohortApplication.total_num_required_stripe_payments).toEqual(0);
                expect(lastCohortApplication.locked_due_to_past_due_payment).toBe(false);
                expect(lastCohortApplication.payment_grace_period_end_at).toBeNull();
                expect(scope.user.$$subscriptionCancellationReason).toEqual(reason);
                expect(lastCohortApplication.notes).toEqual(
                    'NOTE: This application was marked as fully paid outside of the product at time of cancellation.',
                );
                expect(scope.user.$$billingTransactionToRecord.asJson()).toEqual(expectedBillingTransactionAttrs);
            });

            it('should show a confirm if no billingTransaction provided when selecting cancelFullyPaid', () => {
                setupAndSelectCancelFullyPaid();
                assertHandleActionWithoutBillingTransactionRequiresConfirm();
            });

            function setupAndSelectCancelFullyPaid() {
                lastCohortApplication = scope.user.lastCohortApplication;
                const actionOption = elem.find(actionOptionsSelector).eq(4);
                const action = actionOption.attr('data-value');

                expect(action).toBe('cancelFullyPaid'); // sanity

                SpecHelper.expectNoElement(elem, '.enrollment-action-inputs button.setup-action');
                SpecHelper.updateSelectize(elem, '.enrollment-action-inputs [name="enrollment_action"]', action);
            }
        });

        describe('users that are not yet registered', () => {
            let lastCohortApplication;

            beforeEach(() => {
                cohortApplications[0].registered = false;
                render(undefined, {
                    primarySubscription: undefined,
                    cohort_applications: cohortApplications,
                });
            });

            it('should display the appropriate actions', () => {
                SpecHelper.expectElements(elem, actionOptionsSelector, 4);
                SpecHelper.expectElementText(elem, actionOptionsSelector, 'Expel', 0);
                SpecHelper.expectElementText(elem, actionOptionsSelector, 'Defer Indefinitely', 1);
                SpecHelper.expectElementText(elem, actionOptionsSelector, 'Defer into cohort', 2);
                SpecHelper.expectElementText(
                    elem,
                    actionOptionsSelector,
                    'Register the user with a one-time out of product payment',
                    3,
                );
            });

            it('should handle the "Expel" action appropriately', () => {
                const actionOption = elem.find(actionOptionsSelector).eq(0);
                const action = actionOption.attr('data-value');

                expect(action).toBe('expel'); // sanity

                SpecHelper.expectNoElement(elem, 'button.setup-action');
                SpecHelper.updateSelectize(elem, '[name="enrollment_action"]', action);
                SpecHelper.expectNoElement(elem, '[name="transfer_cohort"]');
                SpecHelper.expectNoElement(elem, '[name="cancellation_reason"]');

                SpecHelper.click(elem, 'button.setup-action');

                lastCohortApplication = scope.user.lastCohortApplication;
                expect(lastCohortApplication.status).toEqual('expelled');
                expect(lastCohortApplication.registered).toEqual(false);
            });

            it('should handle the "Defer Indefinitely" action appropriately', () => {
                const actionOption = elem.find(actionOptionsSelector).eq(1);
                const action = actionOption.attr('data-value');

                expect(action).toBe('deferIndefinitely'); // sanity

                SpecHelper.expectNoElement(elem, 'button.setup-action');
                SpecHelper.updateSelectize(elem, '[name="enrollment_action"]', action);
                SpecHelper.expectNoElement(elem, '[name="transfer_cohort"]');
                SpecHelper.expectNoElement(elem, '[name="cancellation_reason"]');

                SpecHelper.click(elem, 'button.setup-action');

                lastCohortApplication = scope.user.lastCohortApplication;
                expect(lastCohortApplication.status).toEqual('deferred');
                expect(lastCohortApplication.registered).toEqual(false);
            });

            it('should handle the "Defer into cohort" action appropriately', () => {
                lastCohortApplication = scope.user.lastCohortApplication;
                const actionOption = elem.find(actionOptionsSelector).eq(2);
                const action = actionOption.attr('data-value');

                expect(action).toBe('deferCohort'); // sanity

                // In reality, we wouldn't have an application with a payment grace period
                // and locked_due_to_past_due_payment, but we're mocking this just for tests
                // so that we can verify that this info doesn't get transferred to the new
                // cohort application.
                lastCohortApplication.payment_grace_period_end_at = Math.floor(Date.now() / 1000);
                lastCohortApplication.locked_due_to_past_due_payment = true;

                SpecHelper.expectNoElement(elem, 'button.setup-action');
                SpecHelper.updateSelectize(elem, '[name="enrollment_action"]', action);

                const cohortId = elem.find(cohortOptionsSelector).eq(0).attr('data-value');
                expect(cohortId).not.toBeUndefined();
                SpecHelper.updateSelectize(elem, '[name="transfer_cohort"]', cohortId);
                SpecHelper.expectNoElement(elem, '[name="cancellation_reason"]');

                SpecHelper.click(elem, 'button.setup-action');

                expect(lastCohortApplication.status).toEqual('deferred');
                expect(lastCohortApplication.registered).toEqual(false);

                const newApplication = scope.user.lastCohortApplication;
                expect(newApplication.status).toEqual('pre_accepted');
                expect(newApplication.registered).toEqual(false);
                expect(newApplication.payment_grace_period_end_at).toBeUndefined(); // should not have been transferred from previous application
                expect(newApplication.locked_due_to_past_due_payment).toBeUndefined(); // should not have been transferred from previous application

                expect(scope.user.$$subscriptionCancellationReason).toBeUndefined();
            });

            it('should handle the "Register the user with a one-time out of product payment" action appropriately', () => {
                setupAndSelectRegisterFullyPaid();

                const stripePlan = lastCohortApplication.stripe_plans[1];

                SpecHelper.expectNoElement(elem, '[name="transfer_cohort"]');
                SpecHelper.expectNoElement(elem, '[name="cancellation_reason"]');

                expect(lastCohortApplication.registered_early).toBe(false);
                SpecHelper.assertSelectizeValue(elem, '.enrollment-action-inputs [name="registered_early"]', 'no');
                SpecHelper.updateSelectize(elem, '.enrollment-action-inputs [name="registered_early"]', 'yes');

                expect(lastCohortApplication.stripe_plan_id).not.toBe(stripePlan.id);

                // Until stripe plan is selected, prepare changes button should be disabled
                SpecHelper.expectElementDisabled(elem, 'button.setup-action');
                SpecHelper.updateSelectize(elem, '.enrollment-action-inputs [name="stripe_plan_id"]', stripePlan.id);

                const expectedBillingTransactionAttrs = fillOutBillingTransactionForm();

                // We assert below that these values gets udpated as part of this action
                lastCohortApplication.locked_due_to_past_due_payment = true;
                lastCohortApplication.payment_grace_period_end_at = Math.floor(Date.now() / 1000);

                SpecHelper.click(elem, 'button.setup-action');

                expect(lastCohortApplication.status).toEqual('accepted'); // was previously acepted
                expect(lastCohortApplication.total_num_required_stripe_payments).toBe(0);
                expect(lastCohortApplication.num_charged_payments).toBe(0);
                expect(lastCohortApplication.num_refunded_payments).toBe(0);
                expect(lastCohortApplication.stripe_plan_id).toEqual(stripePlan.id);
                expect(lastCohortApplication.registered_early).toEqual(true);
                expect(lastCohortApplication.locked_due_to_past_due_payment).toBe(false);
                expect(lastCohortApplication.payment_grace_period_end_at).toBeNull();
                expect(lastCohortApplication.notes).toEqual(
                    'NOTE: This application was marked as fully paid outside of the product at time of registration.',
                );
                expect(lastCohortApplication.registered).toEqual(true);
                expect(scope.user.$$billingTransactionToRecord.asJson()).toEqual(expectedBillingTransactionAttrs);
            });

            it('should show a confirm if no billingTransaction provided when selecting registerFullyPaid', () => {
                setupAndSelectRegisterFullyPaid();
                assertHandleActionWithoutBillingTransactionRequiresConfirm();
            });

            function setupAndSelectRegisterFullyPaid() {
                lastCohortApplication = scope.user.lastCohortApplication;

                const actionOption = elem.find(actionOptionsSelector).eq(3);
                const action = actionOption.attr('data-value');

                expect(action).toBe('registerFullyPaid'); // sanity
                expect(lastCohortApplication.status).toEqual('accepted'); // sanity

                SpecHelper.expectNoElement(elem, 'button.setup-action');
                SpecHelper.updateSelectize(elem, '[name="enrollment_action"]', action);
            }
        });

        describe('users that are registered and fully paid', () => {
            beforeEach(() => {
                cohortApplications[0].registered = true;
                render(undefined, {
                    primarySubscription: undefined,
                    cohort_applications: cohortApplications,
                });
            });

            it('should show an unlockable section for editing plan and registered_early', () => {
                const lastCohortApplication = scope.user.lastCohortApplication;

                const stripePlan = lastCohortApplication.stripe_plans[1];
                expect(lastCohortApplication.stripe_plan_id).not.toBe(stripePlan.id);
                expect(lastCohortApplication.registered_early).not.toBe(true);

                SpecHelper.expectElementDisabled(elem, '.edit-plan-and-early-registration [name="registered_early"]');
                SpecHelper.expectElementDisabled(elem, '.edit-plan-and-early-registration [name="stripe_plan_id"]');
                SpecHelper.expectNoElement(elem, '.enrollment-action-inputs .billing-transaction-form');

                jest.spyOn($window, 'confirm').mockReturnValue(true);
                SpecHelper.click(elem, '.edit-plan-and-early-registration button.unlock');
                expect($window.confirm).toHaveBeenCalled();

                SpecHelper.expectElementEnabled(elem, '.edit-plan-and-early-registration [name="registered_early"]');
                SpecHelper.expectElementEnabled(elem, '.edit-plan-and-early-registration [name="stripe_plan_id"]');

                SpecHelper.assertSelectizeValue(
                    elem,
                    '.edit-plan-and-early-registration [name="registered_early"]',
                    'no',
                );
                SpecHelper.assertSelectizeValue(
                    elem,
                    '.edit-plan-and-early-registration [name="stripe_plan_id"]',
                    lastCohortApplication.stripe_plan_id,
                );

                SpecHelper.updateSelectize(elem, '.edit-plan-and-early-registration [name="registered_early"]', 'yes');
                expect(lastCohortApplication.registered_early).toBe(true);

                SpecHelper.updateSelectize(
                    elem,
                    '.edit-plan-and-early-registration [name="stripe_plan_id"]',
                    stripePlan.id,
                );
                expect(lastCohortApplication.stripe_plan_id).toBe(stripePlan.id);
                // BillingTransaction stuff is hidden except when first recording a one-time payment.  Admins
                // can use the payment UI in applicants in that case.
                SpecHelper.expectNoElement(
                    elem,
                    '.edit-plan-and-early-registration [name="payment_provider"]',
                    'Paypal - Q4XWQ',
                );
                expect(scope.user.$$billingTransactionToRecord).toBeUndefined();
            });

            it('should not show the section with the plan and registered early if irrelevant', () => {
                const lastCohortApplication = scope.user.lastCohortApplication;

                SpecHelper.expectElement(elem, '.edit-plan-and-early-registration');

                // should disappear if not registered
                lastCohortApplication.registered = false;
                scope.$digest();
                SpecHelper.expectNoElement(elem, '.edit-plan-and-early-registration');

                // it should be visible even if not registered if some payments have been made
                lastCohortApplication.num_charged_payments = 1;
                scope.$digest();
                SpecHelper.expectElement(elem, '.edit-plan-and-early-registration');

                // reset
                lastCohortApplication.registered = true;
                lastCohortApplication.num_charged_payments = 0;
                scope.$digest();
                SpecHelper.expectElement(elem, '.edit-plan-and-early-registration');

                // should disappear if full scholarship
                jest.spyOn(lastCohortApplication, 'hasFullScholarship', 'get').mockReturnValue(true);
                scope.$digest();
                SpecHelper.expectNoElement(elem, '.edit-plan-and-early-registration');
            });

            it('should not allow for unlocking the plan/registered_early section if there is an active subscription', () => {
                jest.spyOn(scope.user, 'primarySubscription', 'get').mockReturnValue({});
                scope.$digest();
                SpecHelper.expectElementDisabled(elem, '.edit-plan-and-early-registration [name="registered_early"]');
                SpecHelper.expectElementDisabled(elem, '.edit-plan-and-early-registration [name="stripe_plan_id"]');
                SpecHelper.expectNoElement(elem, '.edit-plan-and-early-registration .billing-transaction-form');
                SpecHelper.expectNoElement(elem, '.edit-plan-and-early-registration button.unlock');
                SpecHelper.expectElement(elem, '.edit-plan-and-early-registration .unlock-disabled');
            });
        });

        describe('users with pre_accepted applications', () => {
            beforeEach(() => {
                cohortApplications[0].status = 'pre_accepted';
                cohortApplications[0].registered = false;
            });

            it('should display the appropriate actions', () => {
                jest.spyOn(Cohort.prototype, 'supportsRegistrationDeadline', 'get').mockReturnValue(true);
                render(undefined, {
                    primarySubscription: undefined,
                    cohort_applications: cohortApplications,
                });
                SpecHelper.expectElements(elem, actionOptionsSelector, 5);
                SpecHelper.expectElementText(elem, actionOptionsSelector, 'Reject/Expel', 0);
                SpecHelper.expectElementText(elem, actionOptionsSelector, 'Defer Indefinitely', 1);
                SpecHelper.expectElementText(elem, actionOptionsSelector, 'Defer into cohort', 2);
                SpecHelper.expectElementText(
                    elem,
                    actionOptionsSelector,
                    'Register the user with a one-time out of product payment',
                    3,
                );
                SpecHelper.expectElementText(
                    elem,
                    actionOptionsSelector,
                    'Register user with outside payment grace period',
                    4,
                );
            });

            it('should display the appropriate actions for cohorts without registration', () => {
                jest.spyOn(Cohort.prototype, 'supportsRegistrationDeadline', 'get').mockReturnValue(false);
                render(undefined, {
                    primarySubscription: undefined,
                    cohort_applications: cohortApplications,
                });
                SpecHelper.expectElements(elem, actionOptionsSelector, 3);
                SpecHelper.expectElementText(elem, actionOptionsSelector, 'Reject/Expel', 0);
                SpecHelper.expectElementText(elem, actionOptionsSelector, 'Defer Indefinitely', 1);
                SpecHelper.expectElementText(elem, actionOptionsSelector, 'Defer into cohort', 2);
            });

            it('should display the appropriate actions when user hasFullScholarship', () => {
                jest.spyOn(Cohort.prototype, 'supportsRegistrationDeadline', 'get').mockReturnValue(true);
                jest.spyOn(cohortApplications[0], 'hasFullScholarship', 'get').mockReturnValue(true);
                render(undefined, {
                    primarySubscription: undefined,
                    cohort_applications: cohortApplications,
                });
                SpecHelper.expectElements(elem, actionOptionsSelector, 3);
                SpecHelper.expectElementText(elem, actionOptionsSelector, 'Reject/Expel', 0);
                SpecHelper.expectElementText(elem, actionOptionsSelector, 'Defer Indefinitely', 1);
                SpecHelper.expectElementText(elem, actionOptionsSelector, 'Defer into cohort', 2);
            });

            it('should handle the "Reject" action appropriately', () => {
                render(undefined, {
                    primarySubscription: undefined,
                    cohort_applications: cohortApplications,
                });

                const actionOption = elem.find(actionOptionsSelector).eq(0);
                const action = actionOption.attr('data-value');

                expect(action).toBe('reject'); // sanity

                SpecHelper.expectNoElement(elem, 'button.setup-action');
                SpecHelper.updateSelectize(elem, '[name="enrollment_action"]', action);
                SpecHelper.expectNoElement(elem, '[name="transfer_cohort"]');
                SpecHelper.expectNoElement(elem, '[name="cancellation_reason"]');

                SpecHelper.click(elem, 'button.setup-action');

                const lastCohortApplication = scope.user.lastCohortApplication;
                expect(lastCohortApplication.status).toEqual('rejected_or_expelled');
                expect(lastCohortApplication.registered).toEqual(false);
            });

            it('should handle the "Register user with outside payment grace period" action appropriately', () => {
                render(undefined, {
                    primarySubscription: undefined,
                    cohort_applications: cohortApplications,
                });

                const lastCohortApplication = scope.user.lastCohortApplication;

                // we assert below that this changes to false as part of the enrollment action logic
                lastCohortApplication.locked_due_to_past_due_payment = true;

                expect(lastCohortApplication.registered_early).toBe(false);

                const stripePlan = lastCohortApplication.stripe_plans[1];
                expect(lastCohortApplication.stripe_plan_id).not.toBe(stripePlan.id);

                const actionOption = elem.find(actionOptionsSelector).eq(4);
                const action = actionOption.attr('data-value');

                expect(action).toBe('registerWithOutsidePaymentGracePeriod'); // sanity

                SpecHelper.expectNoElement(elem, 'button.setup-action');
                SpecHelper.updateSelectize(elem, '[name="enrollment_action"]', action);
                SpecHelper.expectNoElement(elem, '[name="transfer_cohort"]');
                SpecHelper.expectNoElement(elem, '[name="cancellation_reason"]');

                SpecHelper.expectElement(elem, 'admin-datetimepicker[name="payment-grace-period"]');
                SpecHelper.assertSelectizeValue(elem, '.enrollment-action-inputs [name="registered_early"]', 'no');
                SpecHelper.updateSelectize(elem, '.enrollment-action-inputs [name="registered_early"]', 'yes');

                // single payment plan is the only option and should be the prefilled value
                const singlePaymentPlan = lastCohortApplication.stripe_plans.find(plan => plan.frequency === 'once');
                expect(scope.enrollmentProxy.supportedStripePlans).toEqual([singlePaymentPlan]);
                SpecHelper.assertSelectizeValue(
                    elem,
                    '.enrollment-action-inputs [name="stripe_plan_id"]',
                    singlePaymentPlan.id,
                );

                SpecHelper.click(elem, 'button.setup-action');

                expect(lastCohortApplication.status).toEqual('pre_accepted'); // should not have changed
                expect(lastCohortApplication.total_num_required_stripe_payments).toBe(0);
                expect(lastCohortApplication.num_charged_payments).toBe(0);
                expect(lastCohortApplication.num_refunded_payments).toBe(0);
                expect(lastCohortApplication.stripe_plan_id).toEqual(stripePlan.id);
                expect(lastCohortApplication.registered_early).toEqual(true);
                expect(lastCohortApplication.locked_due_to_past_due_payment).toBe(false);
                expect(lastCohortApplication.notes).toEqual(
                    'NOTE: This application was marked as registered on the promise of payment in the future outside of the product. ',
                );
                expect(lastCohortApplication.registered).toEqual(true);
                const expectedPaymentGracePeriodEndAt = moment().add(30, 'days').startOf('day').toDate() / 1000; // defaults to 30 days from now
                expect(scope.user.lastCohortApplication.payment_grace_period_end_at).toEqual(
                    expectedPaymentGracePeriodEndAt,
                );
            });
        });

        describe('users with non-schedulable cohort applications', () => {
            beforeEach(() => {
                cohortApplications[0].cohort_id = 2; // career_network_only
                render(undefined, {
                    primarySubscription: undefined,
                    cohort_applications: cohortApplications,
                });
            });

            it('should display the appropriate actions', () => {
                SpecHelper.expectElements(elem, actionOptionsSelector, 1);
                SpecHelper.expectElementText(elem, actionOptionsSelector, 'Expel', 0);
            });
        });

        describe('users with no primarySubscription (fully paid/full scholarship/registered on promise of outside payment)', () => {
            beforeEach(() => {
                cohortApplications[0].registered = true;
                render(undefined, {
                    primarySubscription: undefined,
                    cohort_applications: cohortApplications,
                });
            });

            it('should display the appropriate actions', () => {
                SpecHelper.expectElements(elem, actionOptionsSelector, 3);
                SpecHelper.expectElementText(elem, actionOptionsSelector, 'Expel', 0);
                SpecHelper.expectElementText(elem, actionOptionsSelector, 'Defer Indefinitely', 1);
                SpecHelper.expectElementText(elem, actionOptionsSelector, 'Defer into cohort', 2);
            });

            it('should handle the "Defer into cohort" action appropriately for ongoing cohorts', () => {
                const lastCohortApplication = scope.user.lastCohortApplication;
                ensureStripePlan(lastCohortApplication);
                const actionOption = elem.find(actionOptionsSelector).eq(2);
                const action = actionOption.attr('data-value');

                expect(action).toBe('deferCohort'); // sanity

                // In reality, we wouldn't have an application with a payment grace period
                // and locked_due_to_past_due_payment, but we're mocking this just for tests
                // so that we can verify that this info gets transferred to the new application.
                const paymentGracePeriodEndAt = Math.floor(Date.now() / 1000);
                lastCohortApplication.payment_grace_period_end_at = paymentGracePeriodEndAt;
                lastCohortApplication.locked_due_to_past_due_payment = true;

                SpecHelper.expectNoElement(elem, 'button.setup-action');
                SpecHelper.updateSelectize(elem, '[name="enrollment_action"]', action);

                const cohortId = elem.find(cohortOptionsSelector).eq(0).attr('data-value');
                expect(cohortId).not.toBeUndefined();
                const cohort = _.findWhere(scope.cohortOptions, {
                    id: cohortId,
                });

                const startDate = new Date();
                startDate.setDate(startDate.getDate() - 10);
                jest.spyOn(cohort, 'startDate', 'get').mockReturnValue(startDate);

                SpecHelper.updateSelectize(elem, '[name="transfer_cohort"]', cohortId);
                SpecHelper.expectNoElement(elem, '[name="cancellation_reason"]');

                SpecHelper.click(elem, 'button.setup-action');

                expect(lastCohortApplication.status).toEqual('deferred');
                expect(lastCohortApplication.registered).toEqual(false);

                const newApplication = scope.user.lastCohortApplication;
                expect(newApplication.status).toEqual('accepted');
                expect(newApplication.registered).toEqual(true);
                expect(newApplication.payment_grace_period_end_at).toEqual(paymentGracePeriodEndAt);
                expect(newApplication.locked_due_to_past_due_payment).toBe(true);

                expect(scope.user.$$subscriptionCancellationReason).toBeUndefined();
            });

            it('should handle the "Defer into cohort" action appropriately for future cohorts', () => {
                const lastCohortApplication = scope.user.lastCohortApplication;
                ensureStripePlan(lastCohortApplication);
                const actionOption = elem.find(actionOptionsSelector).eq(2);
                const action = actionOption.attr('data-value');

                expect(action).toBe('deferCohort'); // sanity

                // In reality, we wouldn't have an application with a payment grace period
                // and locked_due_to_past_due_payment, but we're mocking this just for tests
                // so that we can verify that this info doesn't get transferred to the new
                // cohort application.
                const paymentGracePeriodEndAt = Math.floor(Date.now() / 1000);
                lastCohortApplication.payment_grace_period_end_at = paymentGracePeriodEndAt;
                lastCohortApplication.locked_due_to_past_due_payment = true;

                SpecHelper.expectNoElement(elem, 'button.setup-action');
                SpecHelper.updateSelectize(elem, '[name="enrollment_action"]', action);

                const cohortId = elem.find(cohortOptionsSelector).eq(0).attr('data-value');
                expect(cohortId).not.toBeUndefined();
                const cohort = _.findWhere(scope.cohortOptions, {
                    id: cohortId,
                });

                const startDate = new Date();
                startDate.setDate(startDate.getDate() + 10);
                jest.spyOn(cohort, 'startDate', 'get').mockReturnValue(startDate);

                SpecHelper.updateSelectize(elem, '[name="transfer_cohort"]', cohortId);
                SpecHelper.expectNoElement(elem, '[name="cancellation_reason"]');

                SpecHelper.click(elem, 'button.setup-action');

                expect(lastCohortApplication.status).toEqual('deferred');
                expect(lastCohortApplication.registered).toEqual(false);

                const newApplication = scope.user.lastCohortApplication;
                expect(newApplication.status).toEqual('pre_accepted');
                expect(newApplication.registered).toEqual(false);
                expect(newApplication.payment_grace_period_end_at).toBeNull(); // should not have been transferred from previous application
                expect(newApplication.locked_due_to_past_due_payment).toBe(false); // should not have been transferred from previous application

                expect(scope.user.$$subscriptionCancellationReason).toBeUndefined();
            });
        });

        describe('users with refund details', () => {
            beforeEach(() => {
                refundDetails = {
                    total_required_stripe_amount: 4200,
                    total_tuition_retained_after_refund: 0,
                    total_amount_paid_in_stripe: 2100,
                    refund_percent: 1,
                    has_started_playlist: false,
                    playlists_completed: 0,
                    refund_amount: 2100,
                    stripe_plan_id: 'stripe_plan_id',
                    coupon_id: 'coupon_id',
                    can_issue_refund: true,
                };
            });

            it('should work when pre_accepted', () => {
                cohortApplications[0].status = 'pre_accepted';
                ensureStripePlan(cohortApplications[0]);
                render(undefined, {
                    primarySubscription: $injector.get('Subscription').new({}),
                    cohort_applications: cohortApplications,
                });
                SpecHelper.expectElements(elem, actionOptionsSelector, 6);
                SpecHelper.expectElementText(elem, actionOptionsSelector, 'Reject/Expel', 0);
                SpecHelper.expectElementText(elem, actionOptionsSelector, 'Reject/Expel and Refund', 1);
                SpecHelper.expectElementText(elem, actionOptionsSelector, 'Defer Indefinitely', 2);
                SpecHelper.expectElementText(
                    elem,
                    actionOptionsSelector,
                    'Defer into cohort and cancel subscription',
                    3,
                );
                SpecHelper.expectElementText(
                    elem,
                    actionOptionsSelector,
                    'Defer into cohort and maintain subscription',
                    4,
                );
                SpecHelper.expectElementText(
                    elem,
                    actionOptionsSelector,
                    'Cancel subscription and mark as fully paid',
                    5,
                );
                assertRefund('rejectAndRefund', 'rejected_or_expelled', true);
            });

            it('should work when accepted', () => {
                cohortApplications[0].status = 'accepted';
                ensureStripePlan(cohortApplications[0]);
                render(undefined, {
                    primarySubscription: $injector.get('Subscription').new({}),
                    cohort_applications: cohortApplications,
                });
                SpecHelper.expectElements(elem, actionOptionsSelector, 6);
                SpecHelper.expectElementText(elem, actionOptionsSelector, 'Expel', 0);
                SpecHelper.expectElementText(elem, actionOptionsSelector, 'Expel and Refund', 1);
                SpecHelper.expectElementText(elem, actionOptionsSelector, 'Defer Indefinitely', 2);
                SpecHelper.expectElementText(
                    elem,
                    actionOptionsSelector,
                    'Defer into cohort and cancel subscription',
                    3,
                );
                SpecHelper.expectElementText(
                    elem,
                    actionOptionsSelector,
                    'Defer into cohort and maintain subscription',
                    4,
                );
                SpecHelper.expectElementText(
                    elem,
                    actionOptionsSelector,
                    'Cancel subscription and mark as fully paid',
                    5,
                );
                assertRefund('expelAndRefund', 'expelled', true);
            });

            it('should work when expelled', () => {
                cohortApplications[0].status = 'expelled';
                cohortApplications[0].registered = false;
                render(undefined, {
                    primarySubscription: undefined,
                    cohort_applications: cohortApplications,
                });
                SpecHelper.expectElements(elem, actionOptionsSelector, 1);
                SpecHelper.expectElementText(elem, actionOptionsSelector, 'Issue Refund', 0);
                assertRefund('refund', 'expelled', false);
            });

            it('should show refund details', () => {
                cohortApplications[0].status = 'accepted';
                render(undefined, {
                    primarySubscription: $injector.get('Subscription').new({}),
                    cohort_applications: cohortApplications,
                });

                SpecHelper.updateSelectize(elem, '[name="enrollment_action"]', 'expelAndRefund');
                SpecHelper.expectNoElement(elem, '[name="transfer_cohort"]');
                SpecHelper.expectElement(elem, '.refund-details');

                SpecHelper.expectElementText(elem, '.refund-details tr:eq(0) th', 'Stripe plan');
                SpecHelper.expectElementText(elem, '.refund-details tr:eq(0) td', refundDetails.stripe_plan_id);
                SpecHelper.expectElementText(elem, '.refund-details tr:eq(1) th', 'Coupon');
                SpecHelper.expectElementText(elem, '.refund-details tr:eq(1) td', refundDetails.coupon_id);
                SpecHelper.expectElementText(elem, '.refund-details tr:eq(2) th', 'Original tuition due');
                SpecHelper.expectElementText(elem, '.refund-details tr:eq(2) td', '$42');
                SpecHelper.expectElementText(elem, '.refund-details tr:eq(3) th', 'Tuition after refund');
                SpecHelper.expectElementText(elem, '.refund-details tr:eq(3) td', '$0');
                SpecHelper.expectElementText(elem, '.refund-details tr:eq(4) th', 'Total tuition paid');
                SpecHelper.expectElementText(elem, '.refund-details tr:eq(4) td', '$21');
                SpecHelper.expectElementText(elem, '.refund-details tr:eq(5) th', 'Status');
                SpecHelper.expectElementText(elem, '.refund-details tr:eq(5) td', 'No concentrations started');
                SpecHelper.expectElementText(elem, '.refund-details tr:eq(6) th', 'Refund percent');
                SpecHelper.expectElementText(elem, '.refund-details tr:eq(6) td', '100%');
                SpecHelper.expectElementText(elem, '.refund-details tr:eq(7) th', 'Refund amount');
                SpecHelper.expectElementText(elem, '.refund-details tr:eq(7) td', '$21');
            });

            it('should work when no refund can be issued', () => {
                refundDetails.can_issue_refund = false;
                refundDetails.custom_message = 'Here is why you cannot issue a refund.';

                cohortApplications[0].status = 'accepted';
                cohortApplications[0].registered = true;
                ensureStripePlan(cohortApplications[0]);
                render(undefined, {
                    primarySubscription: $injector.get('Subscription').new({}),
                    cohort_applications: cohortApplications,
                });
                SpecHelper.updateSelectize(elem, '[name="enrollment_action"]', 'expelAndRefund');

                SpecHelper.expectElement(elem, '.refund-details');
                SpecHelper.expectElementText(
                    elem,
                    '.refund-details .custom-message',
                    'Here is why you cannot issue a refund.',
                );

                const reason = elem.find(cancelOptionsSelector).eq(0).attr('data-value');
                expect(reason).not.toBeUndefined();
                SpecHelper.updateSelectize(elem, '[name="cancellation_reason"] selectize', reason);

                SpecHelper.click(elem, 'button.setup-action');

                const lastCohortApplication = scope.user.lastCohortApplication;
                expect(lastCohortApplication.status).toEqual('expelled');
                expect(lastCohortApplication.registered).toEqual(false);
                expect(scope.user.$$issueRefund).toBe(false); // no refund issued
            });

            function assertRefund(actionType, expectedStatus, supportsCancellationReason) {
                SpecHelper.expectNoElement(elem, '.refund-details');
                SpecHelper.expectNoElement(elem, 'button.setup-action');
                SpecHelper.updateSelectize(elem, '[name="enrollment_action"]', actionType);
                SpecHelper.expectNoElement(elem, '[name="transfer_cohort"]');

                SpecHelper.expectElement(elem, '.refund-details');

                if (supportsCancellationReason) {
                    SpecHelper.expectElement(elem, '[name="cancellation_reason"]');
                    const reason = elem.find(cancelOptionsSelector).eq(0).attr('data-value');
                    expect(reason).not.toBeUndefined();
                    SpecHelper.updateSelectize(elem, '[name="cancellation_reason"] selectize', reason);
                } else {
                    SpecHelper.expectNoElement(elem, '[name="cancellation_reason"]');
                }

                SpecHelper.click(elem, 'button.setup-action');

                const lastCohortApplication = scope.user.lastCohortApplication;
                expect(lastCohortApplication.status).toEqual(expectedStatus);
                expect(lastCohortApplication.registered).toEqual(false);
                expect(scope.user.$$issueRefund).toBe(true);
            }
        });
    });

    describe('deferral link', () => {
        let activeDeferralLink;

        beforeEach(() => {
            activeDeferralLink = DeferralLink.new({
                user_id: scope.user.id,
                used: false,
                expires_at: moment().add(30, 'day').unix(),
            });
        });

        it('should show generate button when applicable and allow creating a deferral link', () => {
            render(
                {},
                {
                    cohort_applications: [
                        CohortApplication.new({
                            cohort_id: '1',
                            applied_at: 1,
                            status: 'deferred',
                            program_type: 'mba',
                        }),
                    ],
                },
            );
            SpecHelper.expectElement(elem, '[name="generate-deferral-link"]');
            DeferralLink.expect('create').returns(activeDeferralLink);
            SpecHelper.click(elem, '[name="generate-deferral-link"] button');
            DeferralLink.flush('create');
            expect(scope.notProxyUser.active_deferral_link.id).toBe(activeDeferralLink.id);
            SpecHelper.expectElement(elem, '[name="active-deferral-link"]');
        });

        it('should show an active deferral link', () => {
            render(
                {},
                {
                    cohort_applications: [
                        CohortApplication.new({
                            cohort_id: '1',
                            applied_at: 1,
                            status: 'deferred',
                            program_type: 'mba',
                        }),
                    ],
                    active_deferral_link: DeferralLink.new({
                        user_id: scope.user.id,
                        used: false,
                        expires_at: moment().add(30, 'day').unix(),
                    }),
                },
            );
            SpecHelper.expectNoElement(elem, '[name="generate-deferral-link"]');
            SpecHelper.expectElement(elem, '[name="active-deferral-link"]');
        });
    });

    describe('fallback program type', () => {
        it('should allow for setting program type', () => {
            expect(elem.find('[name="program_type"] option'));
            SpecHelper.updateSelectize(elem, '[name="fallback_program_type"]', 'emba');
            expect(scope.user.fallback_program_type).toEqual('emba');
            SpecHelper.updateSelectize(elem, '[name="fallback_program_type"]', 'mba');
            expect(scope.user.fallback_program_type).toEqual('mba');
        });
    });

    describe('cohort applications', () => {
        it('should allow for adding a cohort application', () => {
            expect(scope.user.cohort_applications.length).toBe(0);
            SpecHelper.updateSelectize(elem, '[name="addCohort"]', scope.cohortOptions[1].id);
            expect(scope.user.cohort_applications.length).toBe(1);
            expect(scope.user.cohort_applications[0].cohort.name).toEqual('def');
        });

        it('should setup plans and scholarship levels when adding a cohort application', () => {
            expect(scope.user.cohort_applications.length).toBe(0);
            const cohortOption = _.detect(
                scope.cohortOptions,
                opt => _.any(opt.stripe_plans) && _.any(opt.scholarship_levels),
            );
            SpecHelper.updateSelectize(elem, '[name="addCohort"]', cohortOption.id);
            const app = scope.user.cohort_applications[0];
            expect(app.stripe_plans).toEqual(cohortOption.stripe_plans);
            expect(app.scholarship_levels).toEqual(cohortOption.scholarship_levels);
        });

        it('should copy payment info to a billable-cohort application from a previously deferred billable-cohort application', () => {
            render(undefined, {
                cohort_applications: [
                    CohortApplication.fixtures.getInstance(
                        _.extend(embaAcceptedMonthlyAppAttrs, {
                            cohort_id: '1',
                            cohort_name: 'Cohort 1',
                            status: 'deferred',
                            applied_at: 2,
                            total_num_required_stripe_payments: 12,
                            stripe_plan_id: 'default_plan_id',
                            num_charged_payments: 3,
                            num_refunded_payments: 1,
                        }),
                    ),
                ],
            });

            expect(scope.cohortOptions[4].stripe_plans).toBeDefined(); // sanity

            SpecHelper.updateSelectize(elem, '[name="addCohort"]', scope.cohortOptions[4].id);
            expect(scope.user.cohort_applications.length).toBe(2);

            expect(scope.user.cohort_applications[0].cohort_id).toEqual('1');
            expect(scope.user.cohort_applications[0].total_num_required_stripe_payments).toBe(12);
            expect(scope.user.cohort_applications[0].stripe_plan_id).toEqual('default_plan_id');
            expect(scope.user.cohort_applications[0].num_charged_payments).toBe(3);
            expect(scope.user.cohort_applications[0].num_refunded_payments).toBe(1);

            expect(scope.user.cohort_applications[1].cohort_id).toEqual('5');
            expect(scope.user.cohort_applications[1].total_num_required_stripe_payments).toBe(12);
            expect(scope.user.cohort_applications[1].stripe_plan_id).toEqual('default_plan_id');
            expect(scope.user.cohort_applications[1].num_charged_payments).toBe(3);
            expect(scope.user.cohort_applications[1].num_refunded_payments).toBe(1);
        });

        it('should allow for removing a cohort application', () => {
            render(undefined, {
                cohort_applications: [
                    CohortApplication.fixtures.getInstance({
                        cohort_id: '1',
                        program_type: 'mba',
                        cohort_name: 'Cohort 1',
                        status: 'rejected',
                        applied_at: 4,
                    }),
                    CohortApplication.fixtures.getInstance({
                        cohort_id: '2',
                        program_type: 'career_network_only',
                        cohort_name: 'Cohort 2',
                        status: 'rejected',
                        applied_at: 3,
                    }),
                    CohortApplication.fixtures.getInstance(embaAcceptedMonthlyAppAttrs),
                ],
            });
            const origLength = scope.user.cohort_applications.length;
            jest.spyOn($window, 'confirm').mockReturnValue(true);
            SpecHelper.click(elem, '.cohort-container button[name="remove"]', 1);
            expect($window.confirm).toHaveBeenCalledWith(
                "Are you sure you want to remove this user's application for Cohort 2?",
            );
            expect(scope.user.cohort_applications.length).toEqual(origLength - 1);
            expect(
                _.findWhere(scope.user.cohort_applications, {
                    cohort_id: '2',
                }),
            ).toBeUndefined();
        });

        it('should order by applied_at and disable everything but the most recent', () => {
            render(undefined, {
                cohort_applications: [
                    CohortApplication.fixtures.getInstance({
                        cohort_id: '1',
                        program_type: 'mba',
                        status: 'rejected',
                        applied_at: 2,
                    }),
                    CohortApplication.fixtures.getInstance({
                        cohort_id: '2',
                        program_type: 'career_network_only',
                        status: 'rejected',
                        applied_at: 1,
                    }),
                    CohortApplication.fixtures.getInstance({
                        cohort_id: '3',
                        program_type: 'emba',
                        status: 'rejected',
                        applied_at: 3,
                    }),
                ],
            });
            expect(scope.user.cohort_applications.length).toEqual(3);
            SpecHelper.expectElements(elem, '.cohort-container', 3);
            SpecHelper.assertSelectValue(elem, '.cohort-container:eq(0) .expanded .form-group:eq(0) selectize', '3'); // cohort id
            SpecHelper.assertSelectValue(elem, '.cohort-container:eq(1) .expanded .form-group:eq(0) selectize', '1');
            SpecHelper.assertSelectValue(elem, '.cohort-container:eq(2) .expanded .form-group:eq(0) selectize', '2');

            SpecHelper.expectElementEnabled(elem, '.cohort-container:eq(0) .expanded .form-group:eq(0) selectize');
            SpecHelper.expectElementDisabled(elem, '.cohort-container:eq(1) .expanded .form-group:eq(0) selectize');
            SpecHelper.expectElementDisabled(elem, '.cohort-container:eq(2) .expanded .form-group:eq(0) selectize');
        });

        it('should set defaults when changing to cohort with differing payment status', () => {
            render(undefined, {
                cohort_applications: [
                    CohortApplication.fixtures.getInstance({
                        cohort_id: '5',
                        program_type: 'emba',
                        total_num_required_stripe_payments: 12,
                    }),
                ],
            });
            expect(scope.user.cohort_applications[0].total_num_required_stripe_payments).toBe(12);

            SpecHelper.updateSelectize(elem, '[name="cohort_id"]', 'alreadyStarted');
            expect(scope.user.cohort_applications[0].total_num_required_stripe_payments).toBeNull();
        });

        describe('addCohortApplication', () => {
            it('should work', () => {
                const cohort = scope.cohortOptions[0];
                scope.addCohortApplication(cohort);
                const cohortApplication = _.last(scope.user.cohort_applications);
                expect(cohortApplication.cohort).toBe(cohort);
                expect(cohortApplication.status).toBe('pending');
                expect(new Date() - cohortApplication.appliedAt < 1000).toBe(true);
                expect(scope.user.fallback_program_type).toEqual(cohort.program_type);
            });

            it('should set the status to pre_accepted if last application was deferred', () => {
                const cohort = Cohort.fixtures.getInstance();
                scope.cohortOptionsByCohortIdMap[cohort.id] = cohort;
                cohort.program_type = 'some_program_type';
                scope.user.cohort_applications = [
                    CohortApplication.new({
                        applied_at: 1,
                        status: 'deferred',
                    }),
                ];
                scope.addCohortApplication(cohort);

                const cohortApplication = _.last(scope.user.cohort_applications);
                expect(cohortApplication.cohort).toBe(cohort);
                expect(cohortApplication.status).toBe('pre_accepted');
                expect(new Date() - cohortApplication.appliedAt < 1000).toBe(true);
                expect(scope.user.fallback_program_type).toEqual('some_program_type');
            });

            it('should default scholarship_level to previous scholarship_level when deferred or expelled from a billable cohort and retain payment info', () => {
                const cohort = Cohort.fixtures.getInstance();
                cohort.program_type = 'some_program_type';
                cohort.scholarship_levels = [
                    {
                        name: 'level1',
                    },
                    {
                        name: 'level2',
                    },
                ];
                scope.cohortOptionsByCohortIdMap[cohort.id] = cohort;
                jest.spyOn(cohort, 'supportsRecurringPayments', 'get').mockReturnValue(true);

                _.each(['deferred', 'expelled'], status => {
                    scope.user.cohort_applications = [
                        CohortApplication.new({
                            applied_at: 1,
                            status,
                            stripe_plans: [
                                {
                                    name: 'plan1',
                                },
                                {
                                    name: 'plan2',
                                },
                            ],
                            scholarship_level: {
                                name: 'level1',
                            },
                            total_num_required_stripe_payments: 12,
                            num_charged_payments: 3,
                            num_refunded_payments: 1,
                            program_type: 'some_program_type',
                        }),
                    ];
                    scope.addCohortApplication(cohort);

                    const cohortApplication = _.last(scope.user.cohort_applications);
                    expect(cohortApplication.cohort).toBe(cohort);
                    expect(cohortApplication.status).toBe(status === 'deferred' ? 'pre_accepted' : 'pending');
                    expect(cohortApplication.scholarship_level.name).toBe('level1');
                    expect(cohortApplication.total_num_required_stripe_payments).toBe(12);
                    expect(cohortApplication.num_charged_payments).toBe(3);
                    expect(cohortApplication.num_refunded_payments).toBe(1);
                    expect(new Date() - cohortApplication.appliedAt < 1000).toBe(true);
                    expect(scope.user.fallback_program_type).toEqual('some_program_type');
                });
            });

            it('should should alert if the program types differ and payment is involved', () => {
                const cohort = Cohort.fixtures.getInstance();
                cohort.program_type = 'some_program_type';
                cohort.scholarship_levels = [
                    {
                        name: 'level1',
                    },
                    {
                        name: 'level2',
                    },
                ];
                scope.cohortOptionsByCohortIdMap[cohort.id] = cohort;
                jest.spyOn(cohort, 'supportsRecurringPayments', 'get').mockReturnValue(true);
                scope.user.cohort_applications = [
                    CohortApplication.new({
                        applied_at: 1,
                        status: 'deferred',
                        stripe_plans: [
                            {
                                name: 'plan1',
                            },
                            {
                                name: 'plan2',
                            },
                        ],
                        scholarship_level: {
                            name: 'level1',
                        },
                        total_num_required_stripe_payments: 12,
                        num_charged_payments: 3,
                        num_refunded_payments: 1,
                        program_type: 'a_different_program_type',
                    }),
                ];
                jest.spyOn($window, 'alert').mockImplementation(() => {});
                $window.alert.mockClear();
                scope.addCohortApplication(cohort);
                expect($window.alert).toHaveBeenCalledWith(
                    'Unable to transfer students deferred from one paid plan into a paid plan of a different program type.',
                );
                expect(scope.user.cohort_applications.length).toBe(1);
            });

            it('should set the registered_early to true if last application was registered_early', () => {
                const cohort = Cohort.fixtures.getInstance();
                cohort.program_type = 'some_program_type';
                cohort.scholarship_levels = [
                    {
                        name: 'level1',
                    },
                    {
                        name: 'level2',
                    },
                ];
                scope.cohortOptionsByCohortIdMap[cohort.id] = cohort;
                jest.spyOn(cohort, 'supportsRecurringPayments', 'get').mockReturnValue(true);

                _.each(['deferred', 'expelled'], status => {
                    scope.user.cohort_applications = [
                        CohortApplication.new({
                            applied_at: 1,
                            status,
                            stripe_plans: [
                                {
                                    name: 'plan1',
                                },
                                {
                                    name: 'plan2',
                                },
                            ],
                            scholarship_level: {
                                name: 'level1',
                            },
                            total_num_required_stripe_payments: 12,
                            num_charged_payments: 3,
                            num_refunded_payments: 1,
                            program_type: 'some_program_type',
                            registered_early: true,
                        }),
                    ];
                    scope.addCohortApplication(cohort);

                    const cohortApplication = _.last(scope.user.cohort_applications);
                    expect(cohortApplication.registered_early).toBe(true);
                });
            });

            it('should set the cohort_slack_room to auto-assign on a cohort that has already started', () => {
                const cohort = _.findWhere(scope.cohortOptions, {
                    id: 'alreadyStarted',
                });
                cohort.slack_rooms = [
                    {
                        id: '1',
                    },
                    {
                        id: '2',
                    },
                ];
                const cohortApplication = scope.addCohortApplication(cohort);
                expect(cohortApplication.cohort_slack_room_id).toEqual('AUTO_ASSIGN');
            });

            it('should not set the cohort_slack_room to auto-assign on a cohort that has not yet started', () => {
                const cohort = _.findWhere(scope.cohortOptions, {
                    id: 'startsInTheFuture',
                });
                cohort.slack_rooms = [
                    {
                        id: '1',
                    },
                    {
                        id: '2',
                    },
                ];
                const cohortApplication = scope.addCohortApplication(cohort);
                expect(cohortApplication.cohort_slack_room_id).toBeUndefined();
            });
        });

        describe('cohort_id', () => {
            it('should set stripe_plans and scholarship_levels if not set', () => {
                render(undefined, {
                    cohort_applications: [
                        CohortApplication.fixtures.getInstance({
                            cohort_id: '1',
                            program_type: 'mba',
                            cohort_name: 'Cohort 1',
                            status: 'rejected',
                            applied_at: 4,
                        }),
                    ],
                });
                SpecHelper.updateSelectize(elem, '[name="cohort_id"]', scope.cohortOptions[5].id);
                expect(scope.cohortOptions[5].stripe_plans).not.toBeUndefined(); // sanity check
                expect(scope.user.cohort_applications[0].stripe_plans).toEqual(scope.cohortOptions[5].stripe_plans);
                expect(scope.user.cohort_applications[0].scholarship_levels).toEqual(
                    scope.cohortOptions[5].scholarship_levels,
                );
            });

            it('should not set stripe_plans and scholarship_levels if already set', () => {
                render(undefined, {
                    cohort_applications: [CohortApplication.fixtures.getInstance(embaAcceptedMonthlyAppAttrs)],
                });
                SpecHelper.updateSelectize(elem, '[name="cohort_id"]', scope.cohortOptions[5].id);
                expect(scope.cohortOptions[5].stripe_plans).not.toBeUndefined(); // sanity check
                expect(scope.user.cohort_applications[0].stripe_plans).not.toEqual(scope.cohortOptions[5].stripe_plans);
                expect(scope.user.cohort_applications[0].scholarship_levels).not.toEqual(
                    scope.cohortOptions[5].scholarship_levels,
                );
            });
        });

        describe('status', () => {
            describe('when rejecting EMBA application when previous application has been rejected_and_conerted_to_emba', () => {
                beforeEach(() => {
                    render(undefined, {
                        cohort_applications: [
                            CohortApplication.fixtures.getInstance({
                                id: '1',
                                cohort_id: '1',
                                program_type: 'emba',
                                cohort_name: 'Cohort 1',
                                status: 'pending',
                                applied_at: 4,
                            }),
                            CohortApplication.fixtures.getInstance({
                                id: '2',
                                cohort_id: '2',
                                program_type: 'the_business_certificate',
                                cohort_name: 'Cohort 2',
                                status: 'rejected',
                                applied_at: 3,
                                rejected_and_converted_to_emba: true,
                            }),
                        ],
                    });
                });

                it("should confirm and reset the previous cohort application status to 'accepted'", () => {
                    jest.spyOn($window, 'confirm').mockReturnValue(true);
                    const previousCohortApplication = scope.user.cohort_applications[1];
                    expect(previousCohortApplication.id).toEqual('2');
                    SpecHelper.updateSelectize(elem, '[name="status"]:eq(0)', 'rejected');
                    expect($window.confirm).toHaveBeenCalledWith(
                        "Are you sure you want to update this user's status to 'rejected'? Doing so will update their previous cohort application to 'accepted'.",
                    );
                    expect(previousCohortApplication.status).toEqual('accepted');
                });

                it('should revert status update if confirm failed', () => {
                    jest.spyOn($window, 'confirm').mockReturnValue(false);
                    const currentCohortApplication = scope.user.cohort_applications[0];
                    const previousCohortApplication = scope.user.cohort_applications[1];
                    expect(previousCohortApplication.id).toEqual('2');
                    SpecHelper.updateSelectize(elem, '[name="status"]:eq(0)', 'rejected');
                    expect($window.confirm).toHaveBeenCalledWith(
                        "Are you sure you want to update this user's status to 'rejected'? Doing so will update their previous cohort application to 'accepted'.",
                    );
                    expect(currentCohortApplication.status).toEqual('pending');
                    expect(previousCohortApplication.status).toEqual('rejected');
                });
            });

            describe('without hasFullScholarship', () => {
                it('should alert / revert changes when attempting to update to a unregistered-related status once registered', () => {
                    render(undefined, {
                        cohort_applications: [CohortApplication.fixtures.getInstance(embaAcceptedMonthlyAppAttrs)],
                    });
                    expect(scope.user.cohort_applications.length).toEqual(1);
                    const cohort = _.findWhere(scope.cohortOptions, {
                        id: '5',
                    });
                    expect(cohort.supportsRecurringPayments).toBe(true); // sanity check

                    jest.spyOn($window, 'alert').mockImplementation(() => {});
                    ['rejected', 'expelled', 'deferred'].forEach(targetStatus => {
                        SpecHelper.updateSelectize(elem, '[name="status"]', targetStatus);
                        expect($window.alert).toHaveBeenCalledWith(
                            `The user cannot be ${targetStatus} once accepted and registered. Please cancel the subscription in Stripe. If the user has remaining payments they will be unregistered and can have their status updated to ${targetStatus}.`,
                        );
                        expect(scope.user.cohort_applications[0].status).toBe('accepted');
                        $window.alert.mockClear();
                    });
                });

                it('should confirm changes when attempting to defer or expel if registered but no payment required', () => {
                    render(undefined, {
                        cohort_applications: [
                            CohortApplication.fixtures.getInstance(
                                _.extend(embaAcceptedMonthlyAppAttrs, {
                                    total_num_required_stripe_payments: 0,
                                    scholarship_level: {
                                        name: 'Full Scholarship',
                                        coupons: {
                                            default_plan_id: {
                                                id: '100_percent_off',
                                                amount_off: 0,
                                                percent_off: 1000,
                                            },
                                        },
                                        standard: {
                                            default_plan_id: {
                                                id: '100_percent_off',
                                                amount_off: 0,
                                                percent_off: 1000,
                                            },
                                        },
                                        early: {
                                            default_plan_id: {
                                                id: '100_percent_off',
                                                amount_off: 0,
                                                percent_off: 1000,
                                            },
                                        },
                                    },
                                }),
                            ),
                        ],
                    });
                    expect(scope.user.cohort_applications.length).toEqual(1);
                    const cohort = _.findWhere(scope.cohortOptions, {
                        id: '5',
                    });
                    expect(cohort.supportsRecurringPayments).toBe(true); // sanity check

                    jest.spyOn($window, 'confirm').mockImplementation(() => {});

                    ['expelled', 'deferred'].forEach(targetStatus => {
                        SpecHelper.updateSelectize(elem, '[name="status"]', targetStatus);
                        expect($window.confirm).toHaveBeenCalledWith(
                            "Are you sure you want to update this registered user's status? This will automatically unregister them.",
                        );
                        expect(scope.user.cohort_applications[0].status).toBe('accepted');
                        $window.confirm.mockClear();
                    });
                });
            });

            describe('registered', () => {
                it('should be visibile, but not editable for applications without full scholarships (Stripe customers)', () => {
                    render(undefined, {
                        cohort_applications: [CohortApplication.fixtures.getInstance(embaAcceptedMonthlyAppAttrs)],
                    });
                    SpecHelper.expectElementDisabled(elem, 'input[name="registered"]');
                });

                it('should be editable for applications with full scholarships accounts (non-Stripe customers)', () => {
                    render(undefined, {
                        cohort_applications: [
                            CohortApplication.fixtures.getInstance(
                                _.extend(embaAcceptedMonthlyAppAttrs, {
                                    total_num_required_stripe_payments: 0,
                                    scholarship_level: {
                                        name: 'Full Scholarship',
                                        coupons: {
                                            default_plan_id: {
                                                id: '100_percent_off',
                                                amount_off: 0,
                                                percent_off: 100,
                                            },
                                        },
                                        standard: {
                                            default_plan_id: {
                                                id: '100_percent_off',
                                                amount_off: 0,
                                                percent_off: 100,
                                            },
                                        },
                                        early: {
                                            default_plan_id: {
                                                id: '100_percent_off',
                                                amount_off: 0,
                                                percent_off: 100,
                                            },
                                        },
                                    },
                                }),
                            ),
                        ],
                    });
                    SpecHelper.expectElementEnabled(elem, 'input[name="registered"]');
                });
            });

            describe('with hasFullScholarship', () => {
                beforeEach(() => {
                    render(undefined, {
                        cohort_applications: [
                            CohortApplication.fixtures.getInstance(
                                _.extend(embaAcceptedMonthlyAppAttrs, {
                                    total_num_required_stripe_payments: 0,
                                    scholarship_level: {
                                        name: 'Full Scholarship',
                                        coupons: {
                                            default_plan_id: {
                                                id: '100_percent_off',
                                                amount_off: 0,
                                                percent_off: 100,
                                            },
                                        },
                                        standard: {
                                            default_plan_id: {
                                                id: '100_percent_off',
                                                amount_off: 0,
                                                percent_off: 100,
                                            },
                                        },
                                        early: {
                                            default_plan_id: {
                                                id: '100_percent_off',
                                                amount_off: 0,
                                                percent_off: 100,
                                            },
                                        },
                                    },
                                    scholarship_levels: [
                                        {
                                            name: 'Full Scholarship',
                                            coupons: {
                                                default_plan_id: {
                                                    id: '100_percent_off',
                                                    amount_off: 0,
                                                    percent_off: 100,
                                                },
                                            },
                                            standard: {
                                                default_plan_id: {
                                                    id: '100_percent_off',
                                                    amount_off: 0,
                                                    percent_off: 100,
                                                },
                                            },
                                            early: {
                                                default_plan_id: {
                                                    id: '100_percent_off',
                                                    amount_off: 0,
                                                    percent_off: 100,
                                                },
                                            },
                                        },
                                    ],
                                }),
                            ),
                        ],
                    });
                });

                it('should confirm / revert (on cancellation) changes when attempting to update to a unregistered-related status once registered', () => {
                    expect(scope.user.cohort_applications.length).toEqual(1);
                    const cohort = _.findWhere(scope.cohortOptions, {
                        id: '5',
                    });
                    expect(cohort.supportsRecurringPayments).toBe(true); // sanity check

                    jest.spyOn($window, 'confirm').mockReturnValue(false);
                    SpecHelper.updateSelectize(elem, '[name="status"]', 'rejected');
                    expect($window.confirm).toHaveBeenCalledWith(
                        "Are you sure you want to update this registered user's status? This will automatically unregister them.",
                    );
                    expect(scope.user.cohort_applications[0].status).toBe('accepted');
                });

                it('should confirm / persist (on acceptance) changes when attempting to update to a unregistered-related status once registered', () => {
                    expect(scope.user.cohort_applications.length).toEqual(1);
                    const cohort = _.findWhere(scope.cohortOptions, {
                        id: '5',
                    });
                    expect(cohort.supportsRecurringPayments).toBe(true); // sanity check

                    jest.spyOn($window, 'confirm').mockReturnValue(true);
                    SpecHelper.updateSelectize(elem, '[name="status"]', 'rejected');
                    expect($window.confirm).toHaveBeenCalledWith(
                        "Are you sure you want to update this registered user's status? This will automatically unregister them.",
                    );
                    expect(scope.user.cohort_applications[0].status).toBe('rejected');
                    expect(scope.user.cohort_applications[0].registered).toBe(false);
                });
            });

            describe('with previous deferred application', () => {
                let targetCohort;
                beforeEach(() => {
                    render(undefined, {
                        cohort_applications: [
                            CohortApplication.fixtures.getInstance(
                                _.extend(embaAcceptedMonthlyAppAttrs, {
                                    cohort_id: '1',
                                    cohort_name: 'Cohort 1',
                                    status: 'deferred',
                                }),
                            ),
                        ],
                    });
                    targetCohort = _.findWhere(scope.cohortOptions, {
                        id: '5',
                    });
                    expect(targetCohort.supportsRecurringPayments).toBe(true); // sanity check
                });

                it('should force status to pre_accepted or accepted only when adding new application', () => {
                    SpecHelper.updateSelectize(elem, '[name="addCohort"]', targetCohort.id);
                    const lastStatusElem = SpecHelper.expectElement(
                        elem,
                        'selectize[name="status"]:eq(0)  + .selectize-control .selectize-dropdown',
                    );
                    SpecHelper.expectElement(lastStatusElem, 'div[data-value="pre_accepted"]');
                    SpecHelper.expectElement(lastStatusElem, 'div[data-value="accepted"]');
                    SpecHelper.expectNoElement(lastStatusElem, 'div[data-value="deferred"]');
                    SpecHelper.expectNoElement(lastStatusElem, 'div[data-value="rejected"]');
                    SpecHelper.expectNoElement(lastStatusElem, 'div[data-value="expelled"]');
                });
            });
        });

        describe('cohort_slack_room_id', () => {
            it('should be hidden if the application is not accepted or pre_accepted', () => {
                const application = CohortApplication.fixtures.getInstance({
                    cohort_id: '1',
                    status: 'rejected',
                });
                render(undefined, {
                    cohort_applications: [application],
                });
                SpecHelper.expectNoElement(elem, '[name="cohort_slack_room_id"]');
                SpecHelper.updateSelect(elem, '[name="status"]', 'pre_accepted');
                SpecHelper.expectElement(elem, '[name="cohort_slack_room_id"]');
                SpecHelper.updateSelect(elem, '[name="status"]', 'accepted');
                SpecHelper.expectElement(elem, '[name="cohort_slack_room_id"]');
            });

            it('should be hidden if the !cohort', () => {
                const application = CohortApplication.fixtures.getInstance({
                    cohort_id: '1',
                    status: 'accepted',
                });
                render(undefined, {
                    cohort_applications: [application],
                });
                const cohort = scope.findCohort(application.cohort_id);
                jest.spyOn(cohort, 'supportsSlackRooms', 'get').mockReturnValue(true);
                scope.$digest();
                SpecHelper.expectElement(elem, '[name="cohort_slack_room_id"]');
                application.cohort_id = undefined; // this makes it so that no cohort is found for the application
                scope.$digest();
                SpecHelper.expectNoElement(elem, '[name="cohort_slack_room_id"]');
            });

            it('should be hidden if the !cohort.supportsSlackRooms', () => {
                const application = CohortApplication.fixtures.getInstance({
                    cohort_id: '1',
                    status: 'accepted',
                });
                render(undefined, {
                    cohort_applications: [application],
                });
                const cohort = scope.findCohort(application.cohort_id);
                jest.spyOn(cohort, 'supportsSlackRooms', 'get').mockReturnValue(true);
                scope.$digest();
                SpecHelper.expectElement(elem, '[name="cohort_slack_room_id"]');
                jest.spyOn(cohort, 'supportsSlackRooms', 'get').mockReturnValue(false);
                scope.$digest();
                SpecHelper.expectNoElement(elem, '[name="cohort_slack_room_id"]');
            });

            it('should be hidden if the cohort has no slack rooms', () => {
                const application = CohortApplication.fixtures.getInstance({
                    cohort_id: '1',
                    status: 'accepted',
                });
                render(undefined, {
                    cohort_applications: [application],
                });
                const cohort = scope.findCohort(application.cohort_id);
                SpecHelper.expectElement(elem, '[name="cohort_slack_room_id"]');
                cohort.slack_rooms = [];
                scope.$digest();
                SpecHelper.expectNoElement(elem, '[name="cohort_slack_room_id"]');
            });

            it('should work', () => {
                const application = CohortApplication.fixtures.getInstance({
                    cohort_id: '1',
                    status: 'accepted',
                });
                render(undefined, {
                    cohort_applications: [application],
                });
                const cohort = scope.findCohort(application.cohort_id);
                const id = cohort.slack_rooms[1].id;
                SpecHelper.updateSelect(elem, '[name="cohort_slack_room_id"]', id);
                expect(application.cohort_slack_room_id).toEqual(id);
            });

            it('should have an auto-assign option if the cohort has already started', () => {
                const application = CohortApplication.fixtures.getInstance({
                    cohort_id: 'alreadyStarted',
                    status: 'accepted',
                });
                render(undefined, {
                    cohort_applications: [application],
                });
                const cohort = scope.findCohort(application.cohort_id);
                SpecHelper.assertSelectizeOptionValues(
                    elem,
                    '[name="cohort_slack_room_id"]',
                    ['AUTO_ASSIGN'].concat(_.pluck(cohort.slack_rooms, 'id')),
                );
            });

            it('should not have an auto-assign option if !cohort.supportsSlackRooms', () => {
                const application = CohortApplication.fixtures.getInstance({
                    cohort_id: '7',
                    status: 'accepted',
                });
                render(undefined, {
                    cohort_applications: [application],
                });
                const cohort = scope.findCohort(application.cohort_id);
                expect(cohort.supportsSlackRooms).toBe(false);

                // We assert against the return value of slackRoomOptions here because the selectize element
                // isn't visible in this case.
                expect(_.pluck(scope.slackRoomOptions(application.cohort_id), 'id')).not.toContain('AUTO_ASSIGN');
            });

            it("should not have an auto-assign option if the cohort doesn't have any slack rooms", () => {
                const application = CohortApplication.fixtures.getInstance({
                    cohort_id: 'alreadyStartedWithNoSlackRooms',
                    status: 'accepted',
                });
                render(undefined, {
                    cohort_applications: [application],
                });
                const cohort = scope.findCohort(application.cohort_id);
                expect(cohort.supportsSlackRooms).toBe(true); // assert against false-positive

                // We assert against the return value of slackRoomOptions here because the selectize element
                // isn't visible in this case.
                expect(_.pluck(scope.slackRoomOptions(application.cohort_id), 'id')).not.toContain('AUTO_ASSIGN');
            });

            it('should not have an auto-assign option if the cohort has not already started', () => {
                const application = CohortApplication.fixtures.getInstance({
                    cohort_id: 'startsInTheFuture',
                    status: 'accepted',
                });
                render(undefined, {
                    cohort_applications: [application],
                });
                const cohort = scope.findCohort(application.cohort_id);
                SpecHelper.assertSelectizeOptionValues(
                    elem,
                    '[name="cohort_slack_room_id"]',
                    _.pluck(cohort.slack_rooms, 'id'),
                );
            });
        });

        describe('scholarship_level', () => {
            it('should only be visible if underlying cohort supportsScholarshipLevels and scholarshipLevelOptions have been created', () => {
                const application = CohortApplication.fixtures.getInstance({
                    cohort_id: '5',
                });
                render(undefined, {
                    cohort_applications: [application],
                });
                expect(scope.scholarshipLevelOptions).toBeTruthy();

                // when there's no cohort found for the application, this field should be hidden.
                application.cohort_id = undefined;
                scope.$digest();
                SpecHelper.expectNoElement(elem, '[name="scholarship_level"]');

                const cohort = _.findWhere(scope.cohortOptions, {
                    id: '5',
                });
                application.cohort_id = cohort.id;
                Object.defineProperty(cohort, 'supportsScholarshipLevels', {
                    value: false,
                    configurable: true,
                });
                scope.$digest();
                SpecHelper.expectNoElement(elem, '[name="scholarship_level"]');

                Object.defineProperty(cohort, 'supportsScholarshipLevels', {
                    value: true,
                });
                scope.$digest();
                SpecHelper.expectElement(elem, '[name="scholarship_level"]');
            });

            it("should default to cohort's first scholarship level if application's cohort has changed  from a cohort without scholarship levels to one with", () => {
                render(undefined, {
                    cohort_applications: [
                        CohortApplication.fixtures.getInstance({
                            cohort_id: '1',
                        }),
                    ],
                });
                expect(scope.user.cohort_applications[0].scholarship_level).toBeUndefined();
                SpecHelper.updateSelectize(elem, '[name="cohort_id"]', '5');
                const cohort = _.findWhere(scope.cohortOptions, {
                    id: '5',
                });
                const expectedScholarshipLevel = _.first(cohort.scholarship_levels);
                expect(scope.user.cohort_applications[0].scholarship_level).toEqual(expectedScholarshipLevel);
            });

            it('should not change scholarship_level when switching between cohorts with scholarships', () => {
                render(undefined, {
                    cohort_applications: [
                        CohortApplication.fixtures.getInstance(
                            angular.extend({}, embaAcceptedMonthlyAppAttrs, {
                                cohort_id: '3',
                            }),
                        ),
                    ],
                });
                // sanity checks, cohorts 3 and 5 should both be emba
                const oldCohort = _.findWhere(scope.cohortOptions, {
                    id: '3',
                    program_type: 'emba',
                });
                expect(oldCohort).not.toBeUndefined();
                const newCohort = _.findWhere(scope.cohortOptions, {
                    id: '5',
                    program_type: 'emba',
                });
                expect(newCohort).not.toBeUndefined();
                expect(newCohort.scholarship_levels).not.toBe(oldCohort.scholarship_levels);
                newCohort.scholarship_levels.push({
                    name: 'Another one',
                });

                const application = scope.user.cohort_applications[0];
                const origScholarshipLevel = _.clone(application.scholarship_level);
                const origScholarshipLevels = _.clone(application.scholarship_levels);
                expect(origScholarshipLevel).not.toBeUndefined();
                SpecHelper.updateSelectize(elem, '[name="cohort_id"]', '5');

                expect(application.scholarship_levels).toEqual(origScholarshipLevels);
                expect(application.scholarship_level).toEqual(origScholarshipLevel);
            });

            it('should set appropriate values on application upon scholarship_level selection', () => {
                render(undefined, {
                    cohort_applications: [
                        CohortApplication.fixtures.getInstance(
                            angular.extend({}, embaAcceptedMonthlyAppAttrs, {
                                cohort_id: '5',
                                program_type: 'emba',
                                status: 'pending',
                                total_num_required_stripe_payments: 12,
                            }),
                        ),
                    ],
                });
                SpecHelper.updateSelectize(elem, '[name="scholarship_level"]', 'Level 1');

                const editedApplication = scope.user.cohort_applications[0];
                expect(editedApplication.total_num_required_stripe_payments).toBe(12);
                expect(editedApplication.scholarship_level.coupons.default_plan_id.id).toBe('discount_150');
                expect(editedApplication.scholarship_level.coupons.default_plan_id.amount_off).toBe(15000);
                expect(editedApplication.scholarship_level.coupons.default_plan_id.percent_off).toBeUndefined();
            });

            it('should not allow for any change if the scholarship level is custom', () => {
                render(undefined, {
                    cohort_applications: [
                        CohortApplication.fixtures.getInstance({
                            cohort_id: '6',
                            program_type: 'emba',
                            status: 'pre_accepted',
                            scholarship_level: {
                                name: 'Custom Scholarship',
                            },
                        }),
                    ],
                });

                SpecHelper.expectNoElement(elem, '[name="scholarship_level"]');
            });

            it('should not be required for older applications', () => {
                render(undefined, {
                    cohort_applications: [
                        CohortApplication.fixtures.getInstance({
                            cohort_id: '6',
                            program_type: 'emba',
                            status: 'pre_accepted',
                        }),
                        CohortApplication.fixtures.getInstance({
                            cohort_id: '5',
                            program_type: 'emba',
                            status: 'rejected',
                        }),
                    ],
                });

                expect(scope.user.cohort_applications.length).toEqual(2);
                SpecHelper.expectElements(elem, '.cohort-container', 2);
                SpecHelper.expectElements(elem, '[name="scholarship_level"]', 2);

                // only the scholarship_level input should be required for the most recent cohort application
                SpecHelper.expectElementAttr(elem, '[name="scholarship_level"]:eq(0)', 'required', 'required');
                SpecHelper.expectElementAttr(elem, '[name="scholarship_level"]:eq(1)', 'required', undefined);
            });
        });

        describe('payment info', () => {
            it('should be present if there have been payments made already', () => {
                render(undefined, {
                    cohort_applications: [CohortApplication.fixtures.getInstance(embaAcceptedMonthlyAppAttrs)],
                });
                SpecHelper.expectNoElement(elem, '.payment-container');
                scope.user.cohort_applications[0].total_num_required_stripe_payments = 12;
                scope.user.cohort_applications[0].num_charged_payments = 1;
                scope.user.cohort_applications[0].num_refunded_payments = 0;
                scope.$digest();
                SpecHelper.expectElement(elem, '.payment-container');
                SpecHelper.expectNoElement(elem, '.payment-container .red strong');
            });

            it('should be present if out-of-product payments were made', () => {
                render(undefined, {
                    cohort_applications: [CohortApplication.fixtures.getInstance(embaAcceptedMonthlyAppAttrs)],
                });
                SpecHelper.expectNoElement(elem, '.payment-container');
                jest.spyOn(Cohort, 'supportsRecurringPayments').mockReturnValue(true);
                scope.user.cohort_applications[0].total_num_required_stripe_payments = 0;
                scope.user.cohort_applications[0].num_charged_payments = 0;
                scope.user.cohort_applications[0].num_refunded_payments = 0;
                scope.$digest();
                SpecHelper.expectElement(elem, '.payment-container');
                SpecHelper.expectElementText(
                    elem,
                    '.payment-container .red strong',
                    'NOTE: Payment was probably promised to be made outside of Stripe!',
                );
            });

            it('should hide the input when full scholarship', () => {
                render(undefined, {
                    cohort_applications: [
                        CohortApplication.fixtures.getInstance(
                            _.extend(embaAcceptedMonthlyAppAttrs, {
                                scholarship_level: {
                                    name: 'Full Scholarship',
                                    coupons: {
                                        default_plan_id: {
                                            id: '100_percent_off',
                                            amount_off: 0,
                                            percent_off: 100,
                                        },
                                    },
                                    standard: {
                                        default_plan_id: {
                                            id: '100_percent_off',
                                            amount_off: 0,
                                            percent_off: 100,
                                        },
                                    },
                                    early: {
                                        default_plan_id: {
                                            id: '100_percent_off',
                                            amount_off: 0,
                                            percent_off: 100,
                                        },
                                    },
                                },
                            }),
                        ),
                    ],
                });
                expect(scope.user.cohort_applications.length).toEqual(1);
                const cohort = _.findWhere(scope.cohortOptions, {
                    id: '5',
                });
                expect(cohort.supportsRecurringPayments).toBe(true); // sanity check
                SpecHelper.expectNoElement(elem, '[name="total_num_required_stripe_payments"]');
            });
        });

        describe('as registered', () => {
            it('should disable certain elements and display a message', () => {
                render(undefined, {
                    cohort_applications: [CohortApplication.fixtures.getInstance(embaAcceptedMonthlyAppAttrs)],
                });
                expect(scope.user.cohort_applications.length).toEqual(1);
                const cohort = _.findWhere(scope.cohortOptions, {
                    id: '5',
                });
                expect(cohort.supportsRecurringPayments).toBe(true); // sanity check
                SpecHelper.expectElementDisabled(elem, 'button[title="Delete"]');
                SpecHelper.expectElementDisabled(elem, 'selectize[name="cohort_id"]');
                SpecHelper.expectElementDisabled(elem, 'selectize[name="scholarship_level"]');
                SpecHelper.expectElementText(
                    elem,
                    '.small',
                    'Note: This user has already been registered for this Cohort, and as such, certain fields are presently disabled. To delete this entry, the subscription must first be terminated in Stripe.',
                    0,
                );
            });
        });

        describe('skip_period_expulsion', () => {
            it('should allow for flagging an application with skip_period_expulsion', () => {
                Object.defineProperty(Cohort.prototype, 'supportsEnrollmentDeadline', {
                    value: true,
                    configurable: true,
                });
                render(undefined, {
                    cohort_applications: [
                        CohortApplication.fixtures.getInstance({
                            cohort_id: '1',
                        }),
                    ],
                });

                SpecHelper.expectCheckboxChecked(elem, '[name="skip_period_expulsion"]', false);
                SpecHelper.checkCheckbox(elem, '[name="skip_period_expulsion"]');
                SpecHelper.expectCheckboxChecked(elem, '[name="skip_period_expulsion"]', true);
            });

            it('should hide if not supportsEnrollmentDeadline', () => {
                Object.defineProperty(Cohort.prototype, 'supportsEnrollmentDeadline', {
                    value: false,
                    configurable: true,
                });
                render(undefined, {
                    cohort_applications: [
                        CohortApplication.fixtures.getInstance({
                            cohort_id: '1',
                        }),
                    ],
                });

                SpecHelper.expectNoElement(elem, '[name="skip_period_expulsion"]', false);
            });
        });

        describe('disable_exam_locking', () => {
            it('should allow for flagging an application with disable_exam_locking', () => {
                Object.defineProperty(Cohort.prototype, 'supportsSchedule', {
                    value: true,
                    configurable: true,
                });
                render(undefined, {
                    cohort_applications: [
                        CohortApplication.fixtures.getInstance({
                            cohort_id: '1',
                        }),
                    ],
                });

                SpecHelper.expectCheckboxChecked(elem, '[name="disable_exam_locking"]', false);
                SpecHelper.checkCheckbox(elem, '[name="disable_exam_locking"]');
                SpecHelper.expectCheckboxChecked(elem, '[name="disable_exam_locking"]', true);
            });

            it('should hide if not supportsSchedule', () => {
                Object.defineProperty(Cohort.prototype, 'supportsSchedule', {
                    value: false,
                    configurable: true,
                });
                render(undefined, {
                    cohort_applications: [
                        CohortApplication.fixtures.getInstance({
                            cohort_id: '1',
                        }),
                    ],
                });

                SpecHelper.expectNoElement(elem, '[name="disable_exam_locking"]', false);
            });
        });

        describe('allow_early_registration_pricing', () => {
            it('should allow for flagging an application with allow_early_registration_pricing', () => {
                Object.defineProperty(Cohort.prototype, 'supportsEarlyRegistrationDeadline', {
                    value: true,
                    configurable: true,
                });
                render(undefined, {
                    cohort_applications: [
                        CohortApplication.fixtures.getInstance({
                            cohort_id: '1',
                        }),
                    ],
                });

                SpecHelper.expectCheckboxChecked(elem, '[name="allow_early_registration_pricing"]', false);
                SpecHelper.checkCheckbox(elem, '[name="allow_early_registration_pricing"]');
                SpecHelper.expectCheckboxChecked(elem, '[name="allow_early_registration_pricing"]', true);
            });

            it('should hide if not supportsEarlyRegistrationDeadline', () => {
                Object.defineProperty(Cohort.prototype, 'supportsEarlyRegistrationDeadline', {
                    value: false,
                    configurable: true,
                });
                render(undefined, {
                    cohort_applications: [
                        CohortApplication.fixtures.getInstance({
                            cohort_id: '1',
                        }),
                    ],
                });

                SpecHelper.expectNoElement(elem, '[name="allow_early_registration_pricing"]', false);
            });
        });

        describe('targetableProgramTypeOptions', () => {
            it('should return values for any program type that matches the current option and has a cohort starting in the future', () => {
                const oneDayInSeconds = 86400000 / 1000;
                const promotedBusCertCohort = Cohort.fixtures.getInstance({
                    program_type: 'the_business_certificate',
                    start_date: baseStartDate - oneDayInSeconds,
                });
                const unpromotedBusCertCohort = Cohort.fixtures.getInstance({
                    program_type: 'the_business_certificate',
                    start_date: baseStartDate - oneDayInSeconds,
                });
                const promotedEMBACohort = Cohort.fixtures.getInstance({
                    program_type: 'emba',
                    start_date: baseStartDate + 2 * oneDayInSeconds,
                });
                scope.cohortOptions = [
                    // this one is filterd out because it is the current one
                    Cohort.fixtures.getInstance({
                        program_type: 'mba',
                        start_date: baseStartDate + oneDayInSeconds,
                    }),
                    // this one is filtered out because it is old
                    Cohort.fixtures.getInstance({
                        program_type: 'emba',
                        start_date: baseStartDate - oneDayInSeconds,
                    }),
                    // this one is used because it is in the future
                    Cohort.fixtures.getInstance({
                        program_type: 'emba',
                        start_date: baseStartDate + oneDayInSeconds,
                    }),
                    // There will be a cohortPromotion for emba and mba cohorts,
                    // but since they are not in Cohort.promotableProgramTypes,
                    // we ignore that and go with the next one, paying attention
                    // the schedule instead.
                    //
                    // This is so that, when we are in between the application deadline and the
                    // start date of a cohort, it is no longer promoted, but
                    // we still want to retarget people into it.
                    promotedEMBACohort,
                    // this whole program type is unavailable because there is
                    // only an old one and no promoted cohort
                    Cohort.fixtures.getInstance({
                        program_type: 'career_network_only',
                        start_date: baseStartDate - oneDayInSeconds,
                    }),
                    // this one is not available because unpromoted
                    unpromotedBusCertCohort,
                    // this one is available because promoted,
                    promotedBusCertCohort,
                ];
                scope.cohortPromotions = [
                    {
                        id: 1,
                        cohort_id: promotedBusCertCohort.id,
                        program_type: promotedBusCertCohort.program_type,
                    },
                    {
                        id: 2,
                        cohort_id: promotedEMBACohort.id,
                        program_type: promotedEMBACohort.program_type,
                    },
                ];
                scope.$apply();
                const cohort = scope.cohortOptions[0];
                const application = CohortApplication.fixtures.getInstance({
                    cohort_id: cohort.id,
                });
                const result = scope.targetableProgramTypeOptions(application);
                expect(result).toEqual([
                    {
                        label: 'EMBA',
                        cohortId: scope.cohortOptions[2].id,
                    },
                    {
                        label: 'The Business Certificate (free)',
                        cohortId: promotedBusCertCohort.id,
                    },
                ]);
            });
        });

        describe('retargetApplication', () => {
            it('should work', () => {
                const mbaCohort = _.findWhere(scope.cohortOptions, {
                    program_type: 'mba',
                });
                expect(mbaCohort).not.toBeUndefined();
                // next emba cohort
                const embaCohort = _.chain(scope.cohortOptions)
                    .sortBy('startDate')
                    .findWhere({
                        program_type: 'emba',
                    })
                    .value();
                expect(embaCohort).not.toBeUndefined();

                const application = CohortApplication.fixtures.getInstance({
                    cohort_id: mbaCohort.id,
                    status: 'pending',
                });
                scope.user.cohort_applications = [application];
                scope.$apply();

                SpecHelper.updateSelectize(elem, '[name="retarget-program-type"]', embaCohort.id);

                expect(application.cohort_id).toEqual(embaCohort.id);
                expect(application.retargeted_from_program_type).toEqual('mba');

                // The status changes to accepted if we switch to the business
                // certificate program type, but not here
                expect(application.status).toEqual('pending');

                const retargetedFromProgramTypeInput = elem
                    .find('[name="retargeted-from-program-type"]')
                    .next()
                    .find('.selectize-input');
                const cohortInput = elem.find('[name="cohort_id"]').next().find('.selectize-input');
                const statusInput = elem.find('[name="status"]').next().find('.selectize-input');
                SpecHelper.expectElementHasClass(retargetedFromProgramTypeInput, 'flash-blue-border');
                SpecHelper.expectElementHasClass(cohortInput, 'flash-blue-border');

                // status did not change, so no styling here
                SpecHelper.expectElementDoesNotHaveClass(statusInput, 'flash-blue-border');

                $timeout.flush();
                SpecHelper.expectElementDoesNotHaveClass(retargetedFromProgramTypeInput, 'flash-blue-border');
                SpecHelper.expectElementDoesNotHaveClass(cohortInput, 'flash-blue-border');
                SpecHelper.expectElementDoesNotHaveClass(statusInput, 'flash-blue-border');
            });

            it('should support the NONE option appropriately', () => {
                const embaCohort = _.chain(scope.cohortOptions)
                    .findWhere({
                        program_type: 'emba',
                    })
                    .value();

                const application = CohortApplication.fixtures.getInstance({
                    cohort_id: embaCohort.id,
                    status: 'pending',
                    retargeted_from_program_type: 'mba',
                });
                scope.user.cohort_applications = [application];
                scope.$digest();

                const noneOption = _.findWhere(scope.cohortProgramTypesWithNullOption, {
                    label: 'NONE',
                });

                const selectizeSelector = '[name="retargeted-from-program-type"]';
                SpecHelper.updateSelectize(elem, selectizeSelector, noneOption.key);
                scope.$digest();

                expect(application.retargeted_from_program_type).toEqual(null);
            });

            // FIXME: Before updating to jasmine-core: 2.99.1 this was an "it" and the below nested "it" was just
            // not running. After updating to 2.99.1 the below nested "it" started failing due to Cohort not being defined yet
            // in the top-level beforeEach. I believe the intent of this test was to test all of the cert programs, but only
            // the "the_business_certificate" seems to pass due to scope.cohortOptions only containing "the_business_certificate".
            // Since the tests were just not running before and we are now holding up some important things, I just hardcoded
            // "the_business_certificate" for now and will come back later to fix.
            describe('should set status = accepted when retargeting into cert programs', () => {
                function assertAccepted(program_type) {
                    const mbaCohort = _.findWhere(scope.cohortOptions, {
                        program_type: 'mba',
                    });
                    expect(mbaCohort).not.toBeUndefined();

                    const busCertCohort = _.chain(scope.cohortOptions)
                        .sortBy('startDate')
                        .findWhere({
                            program_type,
                        })
                        .value();
                    expect(busCertCohort).not.toBeUndefined();

                    const application = CohortApplication.fixtures.getInstance({
                        cohort_id: mbaCohort.id,
                        status: 'pending',
                    });
                    scope.user.cohort_applications = [application];
                    scope.$apply();

                    scope.retargetApplication(0, application, busCertCohort.id);

                    expect(application.cohort_id).toEqual(busCertCohort.id);
                    expect(application.retargeted_from_program_type).toEqual('mba');
                    expect(application.status).toEqual('accepted');

                    const statusInput = elem.find('[name="status"]').next().find('.selectize-input');
                    SpecHelper.expectElementHasClass(statusInput, 'flash-blue-border');
                    $timeout.flush();
                    SpecHelper.expectElementDoesNotHaveClass(statusInput, 'flash-blue-border');
                }

                _.each(['the_business_certificate'], program_type => {
                    it(`should set status to accepted when retargeting someone into ${program_type}`, () => {
                        assertAccepted(program_type);
                    });
                });
            });
        });

        describe('notes', () => {
            it('should allow editing of notes', () => {
                render(undefined, {
                    cohort_applications: [
                        CohortApplication.fixtures.getInstance({
                            cohort_id: '5',
                            program_type: 'mba',
                            status: 'accepted',
                            notes: 'foo',
                        }),
                    ],
                });

                expect(scope.user.cohort_applications.length).toEqual(1);
                SpecHelper.updateTextArea(elem, 'textarea[name="notes"]', 'bar');
                const editedApplication = scope.user.cohort_applications[0];
                expect(editedApplication.notes).toBe('bar');
            });
        });
    });

    function render(opts = {}, userOverrides = {}) {
        renderer = SpecHelper.renderer();
        const user = User.fixtures.getInstance();
        user.cohort_applications = userOverrides.cohort_applications || [];
        user.active_deferral_link = userOverrides.active_deferral_link;
        user.relevant_cohort_id = '1';
        jest.spyOn(user, 'primarySubscription', 'get').mockReturnValue(userOverrides.primarySubscription);
        renderer.scope.user = user;

        const notProxyUser = User.new(user.asJson());
        renderer.scope.notProxyUser = notProxyUser;

        const oneDayInSeconds = 86400000 / 1000;

        const defaultStripePlans = [
            {
                id: 'default_plan_id',
                name: 'Monthly Plan',
                amount: '87500',
                frequency: 'monthly',
            },
            {
                id: 'emba_once_9600',
                name: 'Single Payment',
                amount: '960000',
                frequency: 'once',
            },
        ];

        const defaultScholarshipLevels = [
            {
                name: 'Level 1',
                coupons: {
                    default_plan_id: {
                        id: 'discount_150',
                        amount_off: 15000,
                        percent_off: undefined,
                    },
                },
                standard: {
                    default_plan_id: {
                        id: 'discount_150',
                        amount_off: 15000,
                        percent_off: undefined,
                    },
                },
                early: {
                    default_plan_id: {
                        id: 'discount_150',
                        amount_off: 15000,
                        percent_off: undefined,
                    },
                },
            },
            {
                name: 'Full Scholarship',
                coupons: {
                    default_plan_id: {
                        id: '100_percent_off',
                        amount_off: 0,
                        percent_off: 100,
                    },
                },
                standard: {
                    default_plan_id: {
                        id: '100_percent_off',
                        amount_off: 0,
                        percent_off: 100,
                    },
                },
                early: {
                    default_plan_id: {
                        id: '100_percent_off',
                        amount_off: 0,
                        percent_off: 100,
                    },
                },
            },
        ];

        renderer.scope.cohortOptions = _.map(
            [
                {
                    id: '1',
                    name: 'abc',
                    program_type: 'mba',
                    start_date: baseStartDate,
                },
                {
                    id: '2',
                    name: 'def',
                    program_type: 'career_network_only',
                    start_date: baseStartDate + oneDayInSeconds,
                },
                {
                    id: '3',
                    name: 'ghi',
                    program_type: 'emba',
                    start_date: baseStartDate + oneDayInSeconds * 2,
                    stripe_plans: defaultStripePlans,
                    scholarship_levels: _.clone(defaultScholarshipLevels),
                },
                {
                    id: 'startsInTheFuture',
                    name: 'jkl',
                    program_type: 'mba',
                    start_date: baseStartDate + oneDayInSeconds * 3,
                },
                {
                    id: '5',
                    name: 'ghi',
                    program_type: 'emba',
                    start_date: baseStartDate + oneDayInSeconds * 4,
                    stripe_plans: defaultStripePlans,
                    scholarship_levels: _.clone(defaultScholarshipLevels),
                },
                {
                    id: '6',
                    name: 'ghi',
                    program_type: 'emba',
                    start_date: baseStartDate + oneDayInSeconds * 5,
                    stripe_plans: defaultStripePlans,
                    scholarship_levels: _.clone(defaultScholarshipLevels),
                },
                {
                    id: '7',
                    name: 'ghi',
                    program_type: 'the_business_certificate',
                    start_date: baseStartDate - oneDayInSeconds * 5,
                },
                {
                    id: 'alreadyStarted',
                    name: 'ghi',
                    program_type: 'mba',
                    start_date: baseStartDate - oneDayInSeconds * 5,
                },
                {
                    id: 'alreadyStartedWithNoSlackRooms',
                    name: 'ghi',
                    program_type: 'mba',
                    start_date: baseStartDate - oneDayInSeconds * 5,
                    slack_rooms: [],
                },
            ],
            attrs => Cohort.fixtures.getInstance(attrs),
        );

        jest.spyOn(renderer.scope.cohortOptions[0], 'supportsDeferralLink', 'get').mockReturnValue(true);

        const promotedBusCertCohort = _.findWhere(renderer.scope.cohortOptions, {
            program_type: 'the_business_certificate',
        });
        renderer.scope.cohortPromotions = [
            {
                id: '1',
                cohort_id: promotedBusCertCohort.id,
                program_type: 'the_business_certificate',
            },
        ];

        _.extend(renderer.scope, opts);

        renderer.render(
            '<edit-user-details-enrollment user="user" not-proxy-user="notProxyUser" cohort-options="cohortOptions" cohort-promotions="cohortPromotions"></edit-user-details-enrollment>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    function assertHandleActionWithoutBillingTransactionRequiresConfirm() {
        // Need to do this in order to enable the setup-action button
        jest.spyOn(scope, 'enrollmentOptionsValid', 'get').mockReturnValue(true);
        scope.$digest();

        jest.spyOn($window, 'confirm').mockReturnValue(false);
        SpecHelper.click(elem, 'button.setup-action');
        expect(scope.enrollmentProxy.changesApplied).not.toBe(true);
        expect($window.confirm).toHaveBeenCalledWith(
            'Are you sure you want to proceed without recording a transaction?',
        );

        $window.confirm.mockReset();
        jest.spyOn($window, 'confirm').mockReturnValue(true);
        SpecHelper.click(elem, 'button.setup-action');
        expect(scope.enrollmentProxy.changesApplied).toBe(true);
        expect($window.confirm).toHaveBeenCalledWith(
            'Are you sure you want to proceed without recording a transaction?',
        );
    }

    function ensureStripePlan(application) {
        if (application.stripe_plan_id) {
            return;
        }
        application.stripe_plan_id = application.stripe_plans[0].id;
    }

    function fillOutBillingTransactionForm() {
        // Hide amount and id until provider is selected
        const transactionTime = new Date('2017-01-01');
        SpecHelper.expectNoElement(elem, '.enrollment-action-inputs [name="billing_transaction_amount"]');
        SpecHelper.expectNoElement(elem, '.enrollment-action-inputs [name="provider_transaction_id"]');
        SpecHelper.updateSelectize(elem, '.enrollment-action-inputs [name="payment_provider"]', 'Paypal - Q4XWQ');
        SpecHelper.updateTextInput(elem, '.enrollment-action-inputs [name="billing_transaction_amount"]', '4200');
        SpecHelper.updateTextInput(elem, '.enrollment-action-inputs [name="provider_transaction_id"]', '12345');

        // Until all billingTransaction details are complete, setup-action should be disabled
        SpecHelper.expectElementDisabled(elem, 'button.setup-action');
        SpecHelper.updateAdminDatetimePicker(
            elem,
            '.enrollment-action-inputs [name="transaction_time"]',
            transactionTime,
        );

        return {
            amount: 4200,
            provider: 'Paypal - Q4XWQ',
            provider_transaction_id: '12345',
            transaction_time: transactionTime.getTime() / 1000,
            __iguana_type: 'BillingTransaction',
            currency: 'usd',
            transaction_type: 'payment',
        };
    }
});
