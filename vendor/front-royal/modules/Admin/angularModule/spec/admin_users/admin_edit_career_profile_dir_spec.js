import 'AngularSpecHelper';
import 'Admin/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import 'Careers/angularModule/spec/_mock/fixtures/hiring_application_fixtures';
import 'Careers/angularModule/spec/_mock/fixtures/hiring_relationship_fixtures';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohort_applications';
import 'Users/angularModule/spec/_mock/fixtures/users';
import 'Careers/angularModule/spec/_mock/fixtures/career_profile_lists.js';
import previewCandidateCardLocales from 'Careers/locales/careers/preview_candidate_card-en.json';
import jobPreferencesFormLocales from 'Careers/locales/careers/edit_career_profile/job_preferences_form-en.json';
import fieldOptionsLocales from 'Careers/locales/careers/field_options-en.json';
import candidateListCardLocales from 'Careers/locales/careers/candidate_list_card-en.json';
import editDocumentsLocales from 'Settings/locales/settings/edit_documents-en.json';
import editCareerProfileLocales from 'Careers/locales/careers/edit_career_profile-en.json';
import basicInfoFormLocales from 'Careers/locales/careers/edit_career_profile/basic_info_form-en.json';
import educationFormLocales from 'Careers/locales/careers/edit_career_profile/education_form-en.json';
import educationExperienceDetailLocales from 'Careers/locales/careers/edit_career_profile/education_experience_detail-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(
    previewCandidateCardLocales,
    jobPreferencesFormLocales,
    fieldOptionsLocales,
    candidateListCardLocales,
    editDocumentsLocales,
    editCareerProfileLocales,
    basicInfoFormLocales,
    educationFormLocales,
    educationExperienceDetailLocales,
);

describe('Admin::AdminEditCareerProfile', () => {
    let renderer;
    let CareerProfile;
    let User;
    let careerProfile;
    let elem;
    let scope;
    let SpecHelper;
    let HiringApplication;
    let hiringApplications;
    let HiringRelationship;
    let hiringRelationships;
    let amDateFormat;
    let $location;
    let $timeout;
    let user;
    let CohortApplication;
    let cohortApplication;
    let users;
    let CareerProfileList;
    let careerProfileLists;
    let dateHelper;
    let LogInAs;
    let EditCareerProfileHelper;
    let $rootScope;
    let $window;
    let ClientStorage;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Admin', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            SpecHelper.stubCurrentUser('admin');
            CareerProfile = $injector.get('CareerProfile');
            User = $injector.get('User');
            CohortApplication = $injector.get('CohortApplication');
            HiringApplication = $injector.get('HiringApplication');
            HiringRelationship = $injector.get('HiringRelationship');
            CareerProfileList = $injector.get('CareerProfileList');
            amDateFormat = $injector.get('$filter')('amDateFormat');
            $location = $injector.get('$location');
            $rootScope = $injector.get('$rootScope');
            dateHelper = $injector.get('dateHelper');
            $timeout = $injector.get('$timeout');
            $window = $injector.get('$window');
            LogInAs = $injector.get('LogInAs');
            EditCareerProfileHelper = $injector.get('EditCareerProfileHelper');
            ClientStorage = $injector.get('ClientStorage');

            $injector.get('HiringApplicationFixtures');
            $injector.get('HiringRelationshipFixtures');
            $injector.get('CareerProfileFixtures');
            $injector.get('UserFixtures');
            $injector.get('CohortApplicationFixtures');
            $injector.get('CareerProfileListFixtures');

            SpecHelper.stubDirective('traverseListButtons');
            SpecHelper.stubConfig();

            // mock out the user
            user = User.fixtures.getInstance({
                email: 'foo@bar.com',
                updated_at: '2017-06-04 03:58:11.516827',
                last_seen_at: '2017-06-04 02:58:11.516827',
            });
            const date = new Date().getTime() / 1000;
            cohortApplication = CohortApplication.fixtures.getInstance({
                user_id: user.id,
                applied_at: Math.round(Number(date / 1000)), // the server returns the number of seconds since the EPOCH
                cohort_name: 'MBA Foo',
            });

            // mock out the career profile
            careerProfile = CareerProfile.fixtures.getInstance({
                id: user.id,
                interested_in_joining_new_company: 'very_interested',
            });
            user.cohort_applications = [cohortApplication];
            user.career_profile = careerProfile;

            // mock out users to be passed in
            users = [
                user,
                User.fixtures.getInstance({
                    career_profile: CareerProfile.fixtures.getInstance(),
                }),
                User.fixtures.getInstance({
                    career_profile: CareerProfile.fixtures.getInstance(),
                }),
            ];

            // mock out career profile lists
            careerProfileLists = [
                CareerProfileList.fixtures.getInstance({
                    career_profile_ids: [users[0].career_profile.id],
                }),
                CareerProfileList.fixtures.getInstance({
                    career_profile_ids: [users[1].career_profile.id],
                }),
                CareerProfileList.fixtures.getInstance({
                    career_profile_ids: [users[2].career_profile.id],
                }),
            ];

            // mock out hiring applications and hiring relationships
            hiringApplications = [HiringApplication.fixtures.getInstance(), HiringApplication.fixtures.getInstance()];
            hiringRelationships = [
                HiringRelationship.fixtures.getInstance({
                    candidate_id: careerProfile.user_id,
                    candidate_display_name: careerProfile.name,
                    candidate_status: 'hidden',
                    hiring_manager_id: hiringApplications[1].user_id,
                    hiring_manager_display_name: hiringApplications[1].name,
                    hiring_manager_status: 'pending',
                }),
                HiringRelationship.fixtures.getInstance({
                    candidate_id: careerProfile.user_id,
                    candidate_display_name: careerProfile.name,
                    candiate_status: 'pending',
                    hiring_manager_id: hiringApplications[0].user_id,
                    hiring_manager_display_name: hiringApplications[0].name,
                    hiring_manager_status: 'accepted',
                }),
            ];
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('currentTab', () => {
        it('should update in local storage when changed', () => {
            renderAndFlush();
            jest.spyOn(ClientStorage, 'setItem').mockImplementation(() => {});
            SpecHelper.click(elem, '[name="student-records-tab"]');
            expect(ClientStorage.setItem).toHaveBeenCalledWith('adminEditCareerProfile_currentTab', 'student-records');
        });

        it('should initialize to value in local storage', () => {
            jest.spyOn(ClientStorage, 'getItem').mockReturnValue('foo');
            render();
            expect(scope.currentTab).toBe('foo');
        });

        it('should reset the page parameter when switching tabs', () => {
            render();
            $location.search('page', 1337);
            expect($location.search().page).toEqual(1337);
            scope.currentTab = 'foo';
            scope.$digest();
            expect($location.search().page).toBeUndefined();
        });
    });

    it('should show candidate card', () => {
        render();
        CareerProfile.flush('show');
        $timeout.flush();
        const card = SpecHelper.expectElement(elem, 'candidate-list-card');
        expect(card.isolateScope().careerProfile.id).toBe(careerProfile.id);
    });

    it('should have a button to go back', () => {
        render();
        SpecHelper.expectElement(elem, '[name="back-to-list"]');
        SpecHelper.click(elem, '[name="back-to-list"]');
        expect(renderer.scope.goBack).toHaveBeenCalled();
    });

    it('should show a list of hiring relationships if the candidate has any', () => {
        const hiringRelationship = hiringRelationships[0];
        renderAndFlush();
        SpecHelper.assertRowValues(elem, 'tr.hiring-relationship:eq(0) td', {
            created_at: amDateFormat(1000 * hiringRelationship.created_at, 'MM/DD/YY HH:mm'),
            updated_at: amDateFormat(1000 * hiringRelationship.updated_at, 'MM/DD/YY HH:mm'),
            hiring_manager_display_name: hiringRelationship.hiring_manager_display_name,
            hiring_manager_email: hiringRelationship.hiring_manager_email,
            status: 'Waiting on Hiring Manager',
        });
    });

    it('should not show a list of hiring relationships if the candidate does not have any', () => {
        const opts = {
            accepted: false,
        };
        renderAndFlush(opts);
        expect(scope.hiringRelationships).toEqual([]);
        SpecHelper.expectNoElement(elem, '[name="relationshipsTable"]');
    });

    it("should support disabling and enabling a candidate's profile", () => {
        renderAndFlush();

        expect(scope.toggleDisableText).toEqual('Enabled');
        SpecHelper.expectElement(elem, 'button[name="toggle_disable"]');
        expect(scope.user.career_profile.do_not_create_relationships).toEqual(false);
        SpecHelper.expectElementText(elem, 'button[name="toggle_disable"]', 'Enabled');

        CareerProfile.expect('update');
        SpecHelper.click(elem, 'button[name="toggle_disable"]');
        CareerProfile.flush('update');

        expect(scope.user.career_profile.do_not_create_relationships).toEqual(true);
        SpecHelper.expectElementText(elem, 'button[name="toggle_disable"]', 'Disabled');

        CareerProfile.expect('update');
        SpecHelper.click(elem, 'button[name="toggle_disable"]');
        CareerProfile.flush('update');

        expect(scope.user.career_profile.do_not_create_relationships).toEqual(false);
        SpecHelper.expectElementText(elem, 'button[name="toggle_disable"]', 'Enabled');
    });

    it('should display related info about the displayed user', () => {
        renderAndFlush();

        SpecHelper.expectElements(elem, '.related-info .sub-text', 8);
        SpecHelper.expectElementText(elem, '.related-info .sub-text', 'Applied to MBA Foo / pending (100%)', 0);
        SpecHelper.expectElementText(
            elem,
            '.related-info .sub-text',
            `Applied at ${dateHelper.formattedUserFacingDateTime(user.lastCohortApplication.applied_at * 1000)}`,
            1,
        );
        SpecHelper.expectElementText(
            elem,
            '.related-info .sub-text',
            `Last seen at ${dateHelper.formattedUserFacingDateTime(user.last_seen_at * 1000)}`,
            2,
        );
        SpecHelper.expectElementText(
            elem,
            '.related-info .sub-text',
            `Last updated at ${dateHelper.formattedUserFacingDateTime(user.updated_at * 1000)}`,
            3,
        );
        SpecHelper.expectElementText(
            elem,
            '.related-info .sub-text',
            `Last updated by student at ${dateHelper.formattedUserFacingDateTime(
                user.career_profile.last_updated_at_by_student * 1000,
            )}`,
            4,
        );
        SpecHelper.expectElementText(
            elem,
            '.related-info .sub-text',
            `Last confirmed by student at ${dateHelper.formattedUserFacingDateTime(
                user.career_profile.last_confirmed_at_by_student * 1000,
            )}`,
            5,
        );
        SpecHelper.expectElementText(
            elem,
            '.related-info .sub-text',
            `Last confirmed internally at ${dateHelper.formattedUserFacingDateTime(
                user.career_profile.last_confirmed_at_by_internal * 1000,
            )}`,
            6,
        );
        SpecHelper.expectElementText(
            elem,
            '.related-info .sub-text',
            `Last confirmed internally by ${user.career_profile.last_confirmed_internally_by}`,
            7,
        );
    });

    it("should support saving data on a candidate's profile", () => {
        renderAndFlush();

        User.expect('update');
        SpecHelper.updateSelect(elem, 'select[name="profile_status"]', 'not_interested');
        SpecHelper.click(elem, '[name="save"]');

        User.flush('update');
        expect(scope.user.career_profile.interested_in_joining_new_company).toEqual('not_interested');
    });

    it('should support confirming profile content and status', () => {
        renderAndFlush();
        // We have to mock out the _beforeSave callback on the career profile to avoid resetting
        // last_calculated_complete_percentage, which we don't care about since the career
        // profile is mocked out.
        jest.spyOn(CareerProfile.prototype, '_beforeSave').mockImplementation(angular.noop);
        jest.spyOn(scope, 'confirmProfileContentAndStatus');
        SpecHelper.expectElementText(
            elem,
            '.related-info .sub-text',
            `Last confirmed internally at ${dateHelper.formattedUserFacingDateTime(
                user.career_profile.last_confirmed_at_by_internal * 1000,
            )}`,
            6,
        );
        SpecHelper.expectElementText(
            elem,
            '.related-info .sub-text',
            `Last confirmed internally by ${user.career_profile.last_confirmed_internally_by}`,
            7,
        );

        const expectedDate = new Date().getTime() / 1000;
        CareerProfile.expect('update')
            .toBeCalledWith(scope.userProxy.career_profile, {
                confirmed_by_internal: true,
                confirmed_internally_by: $rootScope.currentUser.name,
            })
            .returns(
                _.extend(scope.userProxy.career_profile.asJson(), {
                    last_confirmed_at_by_internal: expectedDate, // increased by one day
                    last_confirmed_internally_by: $rootScope.currentUser.name,
                }),
            );
        SpecHelper.click(elem, '[name="confirm_content_and_status"]');
        expect(scope.confirmProfileContentAndStatus).toHaveBeenCalled();
        CareerProfile.flush('update');
        SpecHelper.expectElementText(
            elem,
            '.related-info .sub-text',
            `Last confirmed internally at ${dateHelper.formattedUserFacingDateTime(expectedDate * 1000)}`,
            6,
        );
        SpecHelper.expectElementText(
            elem,
            '.related-info .sub-text',
            `Last confirmed internally by ${$rootScope.currentUser.name}`,
            7,
        );
    });

    it("should set the containerViewModel header to the user's email", () => {
        renderAndFlush();
        expect(scope.containerViewModel.headerOverride).toBe('foo@bar.com');
    });

    it('should support logging in as user', () => {
        render();

        jest.spyOn(scope, 'logInAsUser');
        jest.spyOn(LogInAs, 'logInAsUser').mockImplementation(() => {});

        SpecHelper.click(elem, '[name="logInAs"]');

        expect(scope.logInAsUser).toHaveBeenCalledWith(scope.user);
        expect(LogInAs.logInAsUser).toHaveBeenCalledWith(scope.user);
    });

    it("should support copying deep link to candidate's career profile", () => {
        render();
        jest.spyOn(scope, 'copyCareerProfileDeepLinkUrl').mockImplementation(() => {});
        SpecHelper.expectElementText(elem, '[name="copyLink"]', 'Copy Link');
        SpecHelper.click(elem, '[name="copyLink"]');
        expect(scope.copyCareerProfileDeepLinkUrl).toHaveBeenCalled();
    });

    describe('application status section', () => {
        it("should be visible if containerViewModel.section equals 'applicants' and scope.statusOptions is set", () => {
            const opts = {
                section: 'applicants',
            };
            renderAndFlush(opts);
            expect(scope.containerViewModel.section).toEqual('applicants');
            expect(scope.statusOptions).toBeDefined();
            SpecHelper.expectElement(elem, '[name="application_status"]');
        });

        it('should have a dropdown list of available status updates including their current status, pending, rejected, accepted', () => {
            const opts = {
                status: 'expelled',
                section: 'applicants',
            };
            renderAndFlush(opts);
            const expectedStatusOptionValues = ['pending', 'accepted', 'rejected', 'expelled'];

            // Plucking the option values here because comparing against the status options directly is difficult
            // since they each get a unique $$hashKey value.
            _.each(expectedStatusOptionValues, expectedStatusOption => {
                expect(_.pluck(scope.statusOptions, 'value')).toContain(expectedStatusOption);
            });
        });

        it("should support updating the applicant's last cohort application status", () => {
            const opts = {
                section: 'applicants',
            };
            renderAndFlush(opts);
            SpecHelper.expectElementDisabled(elem, '[name="save"]');
            SpecHelper.updateSelect(elem, '[name="application_status"]', 'rejected');
            SpecHelper.expectElementEnabled(elem, '[name="save"]');
            expect(scope.userProxy.lastCohortApplication.status).toEqual('rejected');
            User.expect('update');
            SpecHelper.click(elem, '[name="save"]');
        });
    });

    describe('profile feedback section', () => {
        it('should disable all related action buttons unless the textarea has content', () => {
            const opts = {
                section: 'applicants',
            };
            renderAndFlush(opts);
            SpecHelper.expectElementDisabled(elem, 'button[name="clear"]');
            SpecHelper.expectElementDisabled(elem, 'button[name="save"]');
            SpecHelper.expectElementDisabled(elem, 'button[name="save-and-send"]');

            SpecHelper.updateTextArea(elem, '.contact textarea', 'This is some feedback.');

            SpecHelper.expectElementEnabled(elem, 'button[name="clear"]');
            SpecHelper.expectElementEnabled(elem, 'button[name="save"]');
            SpecHelper.expectElementEnabled(elem, 'button[name="save-and-send"]');
        });

        it('should have a caption showing the date the feedback was last sent at only if feedback_last_sent_at is not null', () => {
            const opts = {
                section: 'applicants',
            };
            renderAndFlush(opts);
            SpecHelper.expectNoElement(elem, '.contact .sub-text');

            scope.userProxy.career_profile.feedback_last_sent_at = Date.now() / 1000;
            scope.$digest();

            SpecHelper.expectElement(elem, '.contact .sub-text');
        });

        it('should support clearing the profile feedback and saving the profile', () => {
            const expectedFeedback = 'This is some feedback.';
            user.career_profile.profile_feedback = expectedFeedback;
            const opts = {
                section: 'applicants',
            };
            renderAndFlush(opts);

            expect(scope.userProxy.career_profile.profile_feedback).toEqual(expectedFeedback);
            const expected = scope.userProxy.career_profile;
            expected.profile_feedback = '';

            SpecHelper.click(elem, 'button[name="clear"]');
            expect(scope.userProxy.career_profile.profile_feedback).toEqual('');
            SpecHelper.expectElementText(elem, '.contact textarea', '');
        });

        it('should support saving profile feedback', () => {
            const expectedFeedback = 'This is some feedback.';
            const opts = {
                section: 'applicants',
            };
            renderAndFlush(opts);
            SpecHelper.updateTextArea(elem, '.contact textarea', expectedFeedback);
            expect(scope.userProxy.career_profile.profile_feedback).toEqual(expectedFeedback);

            jest.spyOn(scope, 'save');
            const meta = {};
            User.expect('update').toBeCalledWith(scope.userProxy, meta);
            SpecHelper.click(elem, 'button[name="save"]');
            expect(scope.save).toHaveBeenCalled();
            User.flush('update');
        });

        it('should support saving and sending career profile feedback', () => {
            const expectedFeedback = 'This is some feedback.';
            const opts = {
                section: 'applicants',
            };
            renderAndFlush(opts);
            SpecHelper.updateTextArea(elem, '.contact textarea', expectedFeedback);
            expect(scope.userProxy.career_profile.profile_feedback).toEqual(expectedFeedback);

            jest.spyOn(scope, 'saveAndSendFeedback');
            const meta = {
                send_feedback_email: true,
            };
            User.expect('update').toBeCalledWith(scope.userProxy, meta);
            SpecHelper.click(elem, 'button[name="save-and-send"]');
            expect(scope.saveAndSendFeedback).toHaveBeenCalled();
            User.flush('update');
        });
    });

    describe('candidate lists section', () => {
        it('should show a multi-select for selecting candidate lists', () => {
            const opts = {
                section: 'applicants',
            };
            renderAndFlush(opts);

            SpecHelper.expectElements(elem, 'multi-select[name="career_profile_lists"]');
        });

        it("should support adding the displayed candidate's career profile id to a candidate list", () => {
            const opts = {
                section: 'applicants',
            };
            renderAndFlush(opts);

            // toggle and press save button
            const index = 1;
            const expected = CareerProfileList.new(careerProfileLists[index].asJson());
            expected.career_profile_ids.push(user.career_profile.id);
            CareerProfileList.expect('update').toBeCalledWith(expected);
            SpecHelper.updateMultiSelect(elem, '[name="career_profile_lists"]', careerProfileLists[index]);
            CareerProfileList.expect('update');
        });

        it("should support removing the displayed candidate's career profile id from a candidate list", () => {
            const opts = {
                section: 'applicants',
            };
            renderAndFlush(opts);

            // toggle and press save button
            const index = 1;
            const expected = CareerProfileList.new(careerProfileLists[index].asJson());
            expected.career_profile_ids = _.without(
                careerProfileLists[index].career_profile_ids,
                user.career_profile.id,
            );
            CareerProfileList.expect('update').toBeCalledWith(expected);
            SpecHelper.click(elem, 'multi-select [name="remove"]');
            CareerProfileList.expect('update');
        });

        it("should say where the candidate's career profile is in the candidate list", () => {
            const opts = {
                section: 'applicants',
            };
            renderAndFlush(opts);

            const index = _.findIndex(scope.careerProfileLists, candidateList =>
                _.contains(candidateList.career_profile_ids, user.career_profile.id),
            );
            // this option should already be selected as it's been mocked so that the displayed user's career profile id is in this list
            SpecHelper.expectElementText(
                elem,
                'multi-select span.text',
                `${scope.careerProfileLists[index].name} (#1 of 1 candidates)`,
            );
        });
    });

    describe('edit profile tab', () => {
        beforeEach(() => {
            SpecHelper.stubEventLogging();
            SpecHelper.stubDirective('locationAutocomplete'); // no need to load google places this way

            jest.spyOn(EditCareerProfileHelper, 'isApplicantEditor').mockReturnValue(true);

            const opts = {
                section: 'applicants',
            };
            renderAndFlush(opts);

            SpecHelper.expectNoElement(elem, 'edit-career-profile');
            SpecHelper.click(elem, '[name="edit-profile-tab"]');
            $timeout.flush();
        });

        it('should load properly and support navigation between sections', () => {
            SpecHelper.expectElement(elem, 'edit-career-profile');
            SpecHelper.expectElement(elem, 'basic-info-form');

            // navigate to education step
            SpecHelper.expectNoElement(elem, 'education-form');
            SpecHelper.click(elem, '[name="application_education"]');
            SpecHelper.expectNoElement(elem, 'basic-info-form');
            SpecHelper.expectElement(elem, 'education-form');
        });

        it('should allow saving and properly update proxy', () => {
            HiringRelationship.expect('index');
            SpecHelper.expectElementText(
                elem,
                'candidate-list-card .user-info .name',
                scope.userProxy.career_profile.name,
            );
            SpecHelper.expectTextInputVal(
                elem,
                'basic-info-form input[name="name"]',
                scope.userProxy.career_profile.name,
            );

            // update name and save
            CareerProfile.expect('save');

            SpecHelper.updateTextInput(elem, 'basic-info-form input[name="name"]', 'David S. Pumpkins');

            // mock validity of formController
            const editCareerProfileScope = elem.find('edit-career-profile').isolateScope();
            editCareerProfileScope.currentForm.$valid = true;

            // click and flush
            SpecHelper.click(elem, 'edit-career-profile button[name="save"]');
            CareerProfile.flush('save');
            $timeout.flush();

            // verify that card is refreshed and shows the new name
            expect(scope.userProxy.career_profile.name).toEqual('David S. Pumpkins');
            SpecHelper.expectElementText(
                elem,
                'candidate-list-card .user-info .name',
                scope.userProxy.career_profile.name,
            );
            SpecHelper.expectTextInputVal(
                elem,
                'basic-info-form input[name="name"]',
                scope.userProxy.career_profile.name,
            );
        });
    });

    describe('transcripts tab', () => {
        it('should show a spinner when transcript state shows downloading', () => {
            const opts = {
                section: 'transcripts',
            };
            renderAndFlush(opts);
            SpecHelper.click(elem, '[name="transcripts-tab"]');
            $timeout.flush();
            jest.spyOn(scope.transcriptHelper, 'allowTranscriptDownload').mockReturnValue(true);
            scope.$digest();
            SpecHelper.expectElement(elem, '[name="digital-without-grades"]');
            jest.spyOn(scope.transcriptHelper, 'downloadTranscript').mockImplementation(() => {});

            SpecHelper.expectNoElement(elem, 'front-royal-spinner');
            scope.transcriptHelper.downloadingTranscript = true;
            scope.$digest();
            SpecHelper.expectElement(elem, 'front-royal-spinner');
        });

        it('should allow for downloading a transcript', () => {
            const opts = {
                section: 'transcripts',
            };
            renderAndFlush(opts);
            SpecHelper.click(elem, '[name="transcripts-tab"]');
            $timeout.flush();
            jest.spyOn(scope.transcriptHelper, 'allowTranscriptDownload').mockReturnValue(true);
            scope.$digest();
            SpecHelper.expectElement(elem, '[name="digital-without-grades"]');
            jest.spyOn(scope.transcriptHelper, 'downloadTranscript').mockImplementation(() => {});
            SpecHelper.click(elem, '[name="digital-without-grades"]');
            expect(scope.transcriptHelper.downloadTranscript).toHaveBeenCalledWith(scope.userProxy, 'digital', false);
        });
    });

    function renderAndFlush(opts = {}) {
        if (angular.isUndefined(opts.accepted)) {
            opts.accepted = true;
        }
        render(opts);
        CareerProfile.flush('show');
        HiringRelationship.flush('index');
        CareerProfileList.flush('index');
    }

    function render(opts = {}) {
        renderer = SpecHelper.renderer();
        if (opts.status) {
            user.lastCohortApplication.status = opts.status;
        }
        renderer.scope.user = user;
        renderer.scope.users = users;
        renderer.scope.goBack = jest.fn();
        renderer.scope.containerViewModel = {
            section: opts.section,
        };
        if (opts.accepted) {
            HiringRelationship.expect('index').returns(hiringRelationships);
        } else {
            HiringRelationship.expect('index').returns([]);
        }
        CareerProfile.expect('show')
            .toBeCalledWith(careerProfile.id, {
                view: 'editable',
            })
            .returns(careerProfile);
        CareerProfileList.expect('index').returns(careerProfileLists);
        renderer.render(
            '<admin-edit-career-profile thing="user" things="users" go-back="goBack()" container-view-model="containerViewModel"></admin-edit-career-profile>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
    }
});
