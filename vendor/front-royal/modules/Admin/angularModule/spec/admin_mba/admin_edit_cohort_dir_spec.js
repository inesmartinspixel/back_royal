import 'AngularSpecHelper';
import 'Admin/angularModule';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';

describe('Admin::AdminEditCohort', () => {
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let Cohort;
    let cohort;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Admin', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            Cohort = $injector.get('Cohort');

            $injector.get('CohortFixtures');
        });
        cohort = Cohort.fixtures.getInstance({
            published_at: Date.now(),
        });

        SpecHelper.stubCurrentUser('admin');

        SpecHelper.stubDirective('importCurriculumTemplate');
        SpecHelper.stubDirective('editSchedule');
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.cohort = cohort;
        renderer.scope.listMeta = {
            some_prop: 'some_value',
        };
        renderer.render('<admin-edit-cohort thing="cohort" list-meta="listMeta"></admin-edit-cohort>');
        elem = renderer.elem;
        scope = elem.isolateScope();
    }
    it('should support accepting and passing listMeta', () => {
        render();
        expect(renderer.scope.listMeta).not.toBeUndefined();
        expect(scope.listMeta).toEqual(renderer.scope.listMeta);
        SpecHelper.expectElement(
            elem,
            'edit-schedule[stripe-plans="listMeta.stripe_plans"][stripe-coupons="listMeta.stripe_coupons"][available-learner-projects-json="listMeta.learner_projects"]',
        );
    });

    // See also content_item_save_publish_dir.js
    describe('metadata', () => {
        it('should set publish metadata correctly if publish checkbox is checked', () => {
            render();
            SpecHelper.checkCheckbox(elem, '[name="publish_on_save"]');
            scope.$digest();
            expect(scope.metadata.should_publish).toEqual(true);
            expect(scope.metadata.should_unpublish).toEqual(false);
        });

        it('should set publish metadata correctly if unpublish checkbox is checked', () => {
            render();
            SpecHelper.checkCheckbox(elem, '[name="unpublish_on_save"]');
            scope.$digest();
            expect(scope.metadata.should_publish).toEqual(false);
            expect(scope.metadata.should_unpublish).toEqual(true);
        });
    });
});
