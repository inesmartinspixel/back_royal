import 'AngularSpecHelper';
import 'Admin/angularModule';

describe('Admin::AdminEditCohort', () => {
    let renderer;
    let SpecHelper;
    let Cohort;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Admin', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            Cohort = $injector.get('Cohort');
        });

        SpecHelper.stubCurrentUser('admin');
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        Cohort.expect('index');

        renderer = SpecHelper.renderer();
        renderer.scope.calendarService = {
            initializeCalendar() {},
            gotoDate() {},
            refetchEvents() {},
            setOption() {},
        };
        renderer.render('<admin-view-calendar calendar-service="calendarService"></admin-view-calendar>');
    }

    // FIXME: https://trello.com/c/kUa7jY6M

    it('should render', () => {
        render();
    });
});
