import 'AngularSpecHelper';
import 'Admin/angularModule';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';

describe('Admin::AdminCohort', () => {
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let Cohort;
    let cohort;
    let $location;
    let goBack;
    let listMeta;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Admin', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            Cohort = $injector.get('Cohort');
            $location = $injector.get('$location');

            $injector.get('CohortFixtures');
        });
        cohort = Cohort.fixtures.getInstance({
            published_at: Date.now(),
        });

        SpecHelper.stubCurrentUser('admin');
        SpecHelper.stubDirective('adminCohortSlackAssignments');
        SpecHelper.stubDirective('adminCohortEnrollmentStatus');
        SpecHelper.stubDirective('adminEditCohort');
        SpecHelper.stubDirective('adminCohortGradebook');

        goBack = jest.fn();
        listMeta = {
            some_prop: 'some_value',
        };
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.cohort = cohort;
        renderer.scope.goBack = goBack;
        renderer.scope.listMeta = listMeta;
        renderer.render('<admin-cohort thing="cohort" go-back="goBack()" list-meta="listMeta"></admin-cohort>');
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    it('should have working tabs', () => {
        expect($location.search().tab).toBeUndefined();
        render();

        expect($location.search().tab).toEqual('edit');
        SpecHelper.expectElement(elem, 'admin-edit-cohort');

        SpecHelper.click(elem, '[name="slack-assignments-tab"]');
        expect($location.search().tab).toEqual('slack-assignments');
        SpecHelper.expectElement(elem, 'admin-cohort-slack-assignments');

        SpecHelper.click(elem, '[name="enrollment-status-tab"]');
        expect($location.search().tab).toEqual('enrollment-status');
        SpecHelper.expectElement(elem, 'admin-cohort-enrollment-status');

        SpecHelper.click(elem, '[name="gradebook-tab"]');
        expect($location.search().tab).toEqual('gradebook');
        SpecHelper.expectElement(elem, 'admin-cohort-gradebook');

        SpecHelper.click(elem, '[name="diff-from-published-version-tab"]');
        expect($location.search().tab).toEqual('diff-from-published-version');
        SpecHelper.expectElement(elem, '.diff-from-published-version');
    });

    it('should hide gradebook tab if cohort is not supportsGradebook', () => {
        let supportsGradebook = true;
        render();
        jest.spyOn(scope.thing, 'supportsGradebook', 'get').mockImplementation(() => supportsGradebook);
        scope.$digest();
        SpecHelper.expectElement(elem, '[name="gradebook-tab"]');
        SpecHelper.expectNoElement(elem, '.ng-hide [name="gradebook-tab"]');

        supportsGradebook = false;
        scope.$digest();
        SpecHelper.expectElement(elem, '.ng-hide [name="gradebook-tab"]');
    });

    it('should have a working goBack button', () => {
        render();
        SpecHelper.click(elem, '[name="back-to-list"]');
        expect(goBack).toHaveBeenCalled();
    });

    it('should support accepting and passing listMeta', () => {
        render();
        expect(scope.listMeta).toEqual(renderer.scope.listMeta);
        SpecHelper.expectElement(elem, 'admin-edit-cohort[list-meta="listMeta"]');
    });
});
