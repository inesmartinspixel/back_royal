import 'AngularSpecHelper';
import 'Admin/angularModule';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohort_applications';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';
import 'Playlists/angularModule/spec/_mock/fixtures/playlists';
import 'Users/angularModule/spec/_mock/fixtures/users';
import 'Lessons/angularModule/spec/_mock/fixtures/streams';

describe('Admin::AdminCohortGradebook', () => {
    let $injector;
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let Cohort;
    let cohort;
    let User;
    let students;
    let CohortApplication;
    let $timeout;
    let Stream;
    let requiredExamStream;
    let specializationExamStream;
    let Playlist;
    let specializationPlaylist;
    let listMeta;

    beforeEach(() => {
        // need the editor module so editable-things-list will render
        angular.mock.module('FrontRoyal.Admin', 'SpecHelper');

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            SpecHelper = $injector.get('SpecHelper');
            Cohort = $injector.get('Cohort');
            User = $injector.get('User');
            CohortApplication = $injector.get('CohortApplication');
            $timeout = $injector.get('$timeout');
            Stream = $injector.get('Lesson.Stream');
            Playlist = $injector.get('Playlist');

            $injector.get('CohortFixtures');
            $injector.get('UserFixtures');
            $injector.get('PlaylistFixtures');
            $injector.get('StreamFixtures');
            $injector.get('CohortApplicationFixtures');
        });

        requiredExamStream = Stream.fixtures
            .getInstance({
                title: 'Exam Stream 1',
                exam: true,
            })
            .addLocalePackFixture();

        specializationExamStream = Stream.fixtures
            .getInstance({
                title: 'Specialization Exam',
                exam: true,
            })
            .addLocalePackFixture();

        specializationPlaylist = Playlist.fixtures
            .getInstance({
                stream_entries: [
                    {
                        stream_id: specializationExamStream.id,
                        locale_pack_id: specializationExamStream.localePackId,
                    },
                ],
            })
            .addLocalePackFixture();

        cohort = Cohort.fixtures.getInstance({
            specialization_playlist_pack_ids: [specializationPlaylist.localePackId],
            periods: [
                {
                    stream_entries: [
                        {
                            locale_pack_id: requiredExamStream.localePackId,
                            required: true,
                        },
                    ],
                },
            ],
        });

        students = [
            User.fixtures.getInstance({
                cohort_applications: [getCohortApplication()],
                cohort_user_progress_record: {},
                exam_progress: [],
                project_progress: [],
            }),
            User.fixtures.getInstance({
                cohort_applications: [getCohortApplication()],
                cohort_user_progress_record: {},
                exam_progress: [],
                project_progress: [],
            }),
            User.fixtures.getInstance({
                cohort_applications: [getCohortApplication()],
                cohort_user_progress_record: {},
                exam_progress: [],
                project_progress: [],
            }),
            User.fixtures.getInstance({
                cohort_applications: [getCohortApplication()],
                cohort_user_progress_record: {},
                exam_progress: [],
                project_progress: [],
            }),
        ];

        listMeta = {};
    });

    function getCohortApplication(opts) {
        opts = opts || {};
        return CohortApplication.fixtures.getInstance(
            _.extend(
                {
                    cohort_id: cohort.id,
                    status: 'accepted',
                },
                opts,
            ),
        );
    }

    function render() {
        User.expect('index')
            .toBeCalledWith({
                filters: {
                    cohort_id: cohort.id,
                    cohort_status: 'accepted',
                },
                select_cohort_user_progress_record: true,
                select_exam_progress: true,
                select_project_progress: true,
                per_page: Number.MAX_SAFE_INTEGER,
            })
            .returns(students);

        Stream.expect('index')
            .toBeCalledWith({
                filters: {
                    exam: true,
                    locale: 'en',
                    locale_pack_id: [specializationExamStream.localePackId, requiredExamStream.localePackId],
                },
            })
            .returns([requiredExamStream, specializationExamStream]);

        Playlist.expect('index')
            .toBeCalledWith({
                filters: {
                    locale_pack_id: cohort.playlistPackIds,
                },
            })
            .returns([specializationPlaylist]);

        renderer = SpecHelper.renderer();
        renderer.scope.cohort = cohort;
        renderer.scope.listMeta = listMeta;
        renderer.render('<admin-cohort-gradebook cohort="cohort" list-meta="listMeta"></admin-cohort-gradebook>');
        elem = renderer.elem;
        scope = elem.isolateScope();
        $timeout.flush(); // get the editable-things-list to render
        Playlist.flush('index');
        Stream.flush('index');
        User.flush('index');
        students = scope.adminCohortStudentsTableHelper.students;
    }

    describe('exam columns', () => {
        it('should add a column for each required exam', () => {
            render();
            students[0].exam_progress.push({
                locale_pack_id: requiredExamStream.localePackId,
                official_test_score: 0.42,
            });
            const column = _.findWhere(scope.columns, {
                label: requiredExamStream.title,
            });
            expect(column).not.toBeUndefined();
            scope.$apply();
            expect(
                elem
                    .find(`[data-id="${students[0].id}"]`)
                    .find(`td[data-id="exam-${requiredExamStream.localePackId}"]`)
                    .text()
                    .trim(),
            ).toEqual('42%');
        });

        it('should add a column for each specialization exam', () => {
            render();
            students[0].exam_progress.push({
                locale_pack_id: requiredExamStream.localePackId,
                official_test_score: 0.42,
            });
            const column = _.findWhere(scope.columns, {
                label: specializationExamStream.title,
            });
            expect(column).not.toBeUndefined();
            scope.$apply();
            expect(
                elem
                    .find(`[data-id="${students[0].id}"]`)
                    .find(`td[data-id="exam-${requiredExamStream.localePackId}"]`)
                    .text()
                    .trim(),
            ).toEqual('42%');
        });
    });

    describe('project columns', () => {
        describe('all project_types', () => {
            beforeEach(() => {
                setupProjects();
            });

            it('should add a column for each project', () => {
                render();
                expect(
                    _.findWhere(scope.columns, {
                        label: 'Capstone Project',
                    }),
                ).not.toBeUndefined();
                scope.$apply();
                SpecHelper.expectElement(elem, `[data-id="${students[0].id}"]`);
            });

            describe('score', () => {
                let scoreSelector;
                let column;
                let tr;

                beforeEach(() => {
                    students[0].project_progress.push({
                        requirement_identifier: 'some_capstone',
                        score: 4,
                    });
                    render();
                    scoreSelector = 'td[data-id="project-2"]:eq(0) select:eq(0)';
                    column = _.findWhere(scope.columns, {
                        label: 'Capstone Project',
                    });
                    tr = SpecHelper.expectElement(elem, `[data-id="${students[0].id}"]`);
                });

                afterEach(() => {
                    // https://trello.com/c/8BVLNXbM
                    SpecHelper.updateSelect(tr, 'td[data-id="project-1"] select:eq(0)', 4);
                    SpecHelper.updateSelect(tr, scoreSelector, null);
                    SpecHelper.expectElementEnabled(tr, 'button[data-id="save"]');
                });

                it('should add score selector', () => {
                    SpecHelper.assertSelectOptions(elem, scoreSelector, [
                        'object:null',
                        'number:0',
                        'number:1',
                        'number:2',
                        'number:3',
                        'number:4',
                        'number:5',
                        'string:waived',
                        'string:marked_as_passed',
                    ]);
                });

                it('should toggle disabled property of save button when changed', () => {
                    SpecHelper.expectInputValue(tr, scoreSelector, 4);
                    expect(column.prop(students[0])).toEqual(4); // this is used in csv output
                    SpecHelper.expectElementDisabled(tr, 'button[data-id="save"]'); // save button is disabled before we change anything

                    // we should be able to change the value
                    SpecHelper.updateSelect(tr, scoreSelector, 5);

                    // value has changed and save button enabled
                    expect(students[0].$$proxy.project_progress[0].score).toEqual(5);
                    SpecHelper.expectElementEnabled(tr, 'button[data-id="save"]');

                    // we should be able to change the value to waived
                    SpecHelper.updateSelect(tr, scoreSelector, 'waived');
                    expect(students[0].$$proxy.project_progress[0].score).toEqual(null);
                    expect(students[0].$$proxy.project_progress[0].waived).toEqual(true);
                    SpecHelper.expectElementEnabled(tr, 'button[data-id="save"]');

                    // we should be able to change the value to marked_as_passed
                    SpecHelper.updateSelect(tr, scoreSelector, 'marked_as_passed');
                    expect(students[0].$$proxy.project_progress[0].score).toEqual(null);
                    expect(students[0].$$proxy.project_progress[0].waived).toEqual(false);
                    expect(students[0].$$proxy.project_progress[0].marked_as_passed).toEqual(true);
                    SpecHelper.expectElementEnabled(tr, 'button[data-id="save"]');

                    // we should be able to change the value to null
                    SpecHelper.updateSelect(tr, scoreSelector, null);
                    expect(students[0].$$proxy.project_progress[0].score).toEqual(null);
                    expect(students[0].$$proxy.project_progress[0].waived).toEqual(false);
                    expect(students[0].$$proxy.project_progress[0].marked_as_passed).toEqual(false);
                    SpecHelper.expectElementEnabled(tr, 'button[data-id="save"]');

                    // we should _not_ see the save button become disabled when we set it back to it's first state
                    // because the status was set.
                    SpecHelper.updateSelect(tr, scoreSelector, 4);
                    SpecHelper.expectElementEnabled(tr, 'button[data-id="save"]');
                });

                it("should set status to 'submitted' when setting score", () => {
                    expect(students[0].$$proxy.project_progress[0].status).toBeUndefined();
                    SpecHelper.updateSelect(tr, scoreSelector, 5);
                    expect(students[0].$$proxy.project_progress[0].status).toEqual('submitted');
                });
            });

            describe('status', () => {
                let statusSelector;
                let tr;

                beforeEach(() => {
                    students[0].project_progress.push({
                        requirement_identifier: 'some_capstone',
                        score: 4,
                    });
                    render();
                    statusSelector = 'td[data-id="project-2"]:eq(0) select:eq(1)';
                    tr = SpecHelper.expectElement(elem, `[data-id="${students[0].id}"]`);
                });

                it('should add status selector', () => {
                    SpecHelper.assertSelectOptions(elem, statusSelector, [
                        'undefined:undefined',
                        'string:unsubmitted',
                        'string:submitted',
                        'string:delayed_grading',
                        'string:will_not_submit',
                        'string:plagiarized',
                        'string:plagiarized_resubmitted',
                        'string:plagiarized_resubmission_ok',
                        'string:plagiarized_resubmission_not_ok',
                    ]);
                });

                it('should toggle disabled property of save button when changed', () => {
                    SpecHelper.expectInputValue(tr, statusSelector, undefined);
                    SpecHelper.expectElementDisabled(tr, 'button[data-id="save"]'); // save button is disabled before we change anything

                    // we should be able to change the value
                    SpecHelper.updateSelect(tr, statusSelector, 'submitted');

                    // value has changed and save button enabled
                    expect(students[0].$$proxy.project_progress[0].status).toEqual('submitted');
                    SpecHelper.expectElementEnabled(tr, 'button[data-id="save"]');

                    // we should be able to change the value
                    SpecHelper.updateSelect(tr, statusSelector, 'delayed_grading');
                    expect(students[0].$$proxy.project_progress[0].status).toEqual('delayed_grading');
                    SpecHelper.expectElementEnabled(tr, 'button[data-id="save"]');
                });
            });

            it('should work when there is initially no project record for the student', () => {
                setupProjects();
                render();
                const column = _.findWhere(scope.columns, {
                    label: 'Capstone Project',
                });
                expect(column).not.toBeUndefined();
                scope.$apply();
                const tr = SpecHelper.expectElement(elem, `[data-id="${students[0].id}"]`);
                const scoreSelector = 'td[data-id="project-2"]:eq(0) select:eq(0)';

                SpecHelper.expectInputValue(tr, scoreSelector, null);
                SpecHelper.expectElementDisabled(tr, 'button[data-id="save"]'); // save button is disabled before we change anything

                // setting to 0 should enable the save
                SpecHelper.updateSelect(tr, scoreSelector, '0');
                SpecHelper.expectElementEnabled(tr, 'button[data-id="save"]');

                // setting to >0 should enable the save
                SpecHelper.updateSelect(tr, scoreSelector, '5'); // we should be able to change the value
                SpecHelper.expectElementEnabled(tr, 'button[data-id="save"]');

                // setting to waived should enable the save
                SpecHelper.updateSelect(tr, scoreSelector, 'waived');
                SpecHelper.expectElementEnabled(tr, 'button[data-id="save"]');
            });

            it('should show the total project score column only if there are projects', () => {
                cohort.learner_project_ids = [];
                _.each(cohort.periods, period => {
                    period.learner_project_ids = [];
                });
                render();
                SpecHelper.expectNoElement(elem, 'th[data-id="project_score"]');
            });

            it('should add a hover to the total project score column', () => {
                setupProjects();
                cohort.learner_project_ids = ['1', '2'];
                // Mock out the program_type to emba so the capstone project doesn't
                // adversely impact the weights
                cohort.program_type = 'emba';
                render();

                const column = _.findWhere(scope.columns, {
                    id: 'project_score',
                });
                const cell = SpecHelper.expectElement(
                    elem,
                    `tr[data-id="${students[0].id}"] td[data-id="project_score"]`,
                );

                // No project_progresses have scores.  No score in the deprecated user_project_scores table
                expect(column.getRolloverText(students[0])).toEqual('0 + 0 = 0');
                SpecHelper.expectElementText(cell, 'modal-popup .project-score', '0');
                SpecHelper.expectElementHidden(cell, 'modal-popup .replaced-score');

                // Capstone score set to a failing value
                const capstoneProgress = _.findWhere(students[0].$$proxy.project_progress, {
                    requirement_identifier: 'some_capstone',
                });
                capstoneProgress.score = 2;
                scope.$digest();
                expect(column.getRolloverText(students[0])).toEqual('0 + 0 = 0');
                SpecHelper.expectElementText(cell, 'modal-popup .project-score', '0');
                SpecHelper.expectElementHidden(cell, 'modal-popup .replaced-score');

                // Capstone score set to a passing value
                capstoneProgress.score = 3;
                scope.$digest();
                expect(column.getRolloverText(students[0])).toEqual('0 + 0.67 = 0.67');
                SpecHelper.expectElementText(cell, 'modal-popup .project-score', '0.67');
                SpecHelper.expectElementHidden(cell, 'modal-popup .replaced-score');

                // Regular project score set to a failing value
                const regularProgress = _.findWhere(students[0].$$proxy.project_progress, {
                    requirement_identifier: 'regular_project',
                });
                regularProgress.score = 0;
                scope.$digest();
                expect(column.getRolloverText(students[0])).toEqual('0 + 0.67 = 0.67');
                SpecHelper.expectElementText(cell, 'modal-popup .project-score', '0.67');
                SpecHelper.expectElementHidden(cell, 'modal-popup .replaced-score');

                // Regular project score set to a passing value
                regularProgress.score = 2;
                scope.$digest();
                expect(column.getRolloverText(students[0])).toEqual('0.33 + 0.67 = 1');
                SpecHelper.expectElementText(cell, 'modal-popup .project-score', '1');
                SpecHelper.expectElementHidden(cell, 'modal-popup .replaced-score');

                // Score set in the deprecated user_project_scores table, but replaced by the scores set
                // in project_progress
                students[0].project_score_override = 0.42;
                scope.$digest();
                expect(column.getRolloverText(students[0])).toEqual(
                    '0.33 + 0.67 = 1   (By setting a score for this user, you will replace the old way of setting project scores.)',
                );
                expect(column.getProjectScore(students[0])).toEqual(1);
                expect(column.getReplacedScore(students[0])).toEqual(0.42);
                SpecHelper.expectElementText(cell, 'modal-popup .project-score', '1');
                SpecHelper.expectElementText(cell, 'modal-popup .replaced-score', '[0.42]');
                SpecHelper.expectElementNotHidden(cell, 'modal-popup .replaced-score');

                // even if the progress_score is set to 0, it should still repalce the score
                // from the user_project_scores table
                regularProgress.score = 0;
                capstoneProgress.score = 0;
                scope.$digest();
                expect(column.getRolloverText(students[0])).toEqual(
                    '0 + 0 = 0   (By setting a score for this user, you will replace the old way of setting project scores.)',
                );
                expect(column.getProjectScore(students[0])).toEqual(0);
                expect(column.getReplacedScore(students[0])).toEqual(0.42);
                SpecHelper.expectElementText(cell, 'modal-popup .project-score', '0');
                SpecHelper.expectElementText(cell, 'modal-popup .replaced-score', '[0.42]');
                SpecHelper.expectElementNotHidden(cell, 'modal-popup .replaced-score');

                // Value only set in user_project_scores, with no scores set in project_progress
                regularProgress.score = null;
                capstoneProgress.score = null;
                scope.$digest();
                expect(column.getRolloverText(students[0])).toEqual(
                    'Cumulative project score was set directly in the database using the old method of setting scores.',
                );
                expect(column.getProjectScore(students[0])).toEqual(0.42);
                SpecHelper.expectElementText(cell, 'modal-popup .project-score', '0.42');
                SpecHelper.expectElementHidden(cell, 'modal-popup .replaced-score');

                // Waived project
                students[0].project_score_override = null;
                capstoneProgress.score = 3;
                regularProgress.waived = true;
                regularProgress.marked_as_passed = false;
                scope.$digest();
                expect(column.getRolloverText(students[0])).toEqual('1 = 1');
                SpecHelper.expectElementText(cell, 'modal-popup .project-score', '1');
                SpecHelper.expectElementHidden(cell, 'modal-popup .replaced-score');

                // Project marked_as_passed
                regularProgress.score = null;
                regularProgress.waived = false;
                regularProgress.marked_as_passed = true;
                scope.$digest();
                expect(column.getRolloverText(students[0])).toEqual('0.33 + 0.67 = 1');
                SpecHelper.expectElementText(cell, 'modal-popup .project-score', '1');
                SpecHelper.expectElementHidden(cell, 'modal-popup .replaced-score');
            });
        });

        describe('id_verified', () => {
            it("should not add select for id_verified when project_type is 'standard'", () => {
                setupProjects('standard');
                render();
                expect(
                    _.findWhere(scope.columns, {
                        label: 'Capstone Project',
                    }).isPresentationProject,
                ).toEqual(false);

                SpecHelper.expectNoElement(elem, 'td[data-id="project-1"]:eq(0) select:eq(2)');
            });

            describe("when project_type is 'presentation'", () => {
                let idVerifiedSelector;
                let tr;

                beforeEach(() => {
                    setupProjects('presentation');
                    render();
                    idVerifiedSelector = SpecHelper.expectElement(elem, 'td[data-id="project-1"]:eq(0) select:eq(2)');
                    tr = SpecHelper.expectElement(elem, `[data-id="${students[0].id}"]`);
                });

                it('should add select for id_verified', () => {
                    SpecHelper.assertSelectOptions(elem, idVerifiedSelector, [
                        'undefined:undefined',
                        'boolean:true',
                        'boolean:false',
                    ]);
                });

                it('should toggle disabled property of save button when changed', () => {
                    SpecHelper.expectInputValue(tr, idVerifiedSelector, undefined);
                    SpecHelper.expectElementDisabled(tr, 'button[data-id="save"]'); // save button is disabled before we change anything

                    // we should be able to change the value
                    SpecHelper.updateSelect(tr, idVerifiedSelector, true);

                    // value has changed and save button enabled
                    expect(students[0].$$proxy.project_progress[0].id_verified).toEqual(true);
                    SpecHelper.expectElementEnabled(tr, 'button[data-id="save"]');

                    // we should be able to change the value
                    SpecHelper.updateSelect(tr, idVerifiedSelector, false);
                    expect(students[0].$$proxy.project_progress[0].id_verified).toEqual(false);
                    SpecHelper.expectElementEnabled(tr, 'button[data-id="save"]');
                });
            });
        });

        function setupProjects(project_type) {
            project_type = project_type || 'standard';
            listMeta = {
                learner_projects: [
                    {
                        id: '1',
                        title: 'Regular Project',
                        requirement_identifier: 'regular_project',
                        project_type,
                    },
                    {
                        id: '2',
                        title: 'Capstone Project',
                        requirement_identifier: 'some_capstone',
                        project_type: 'capstone',
                    },
                ],
            };
            cohort.learner_project_ids = ['1', '2'];
        }
    });

    describe('final_score', () => {
        it('should show the score from the accepted application', () => {
            students[0].acceptedCohortApplication.final_score = 0.42;
            render();
            SpecHelper.expectElementText(elem, `tr[data-id="${students[0].id}"] td[data-id="final_score"]`, '42%');
        });
    });

    describe('filters', () => {
        it('should remove specialization filter if irrelevant', () => {
            let supportsSpecializations = true;
            jest.spyOn(cohort, 'supportsSpecializations', 'get').mockImplementation(() => supportsSpecializations);
            render();

            // We need to actively remove this one, because otherwise it could
            // be saved in local storage and hide everything.
            scope.clientFilters = {
                all_required_specializations_complete: true,
            };
            scope.$apply();
            expect(scope.clientFilters.all_required_specializations_complete).not.toBeUndefined();

            supportsSpecializations = false;
            scope.$apply();
            expect(scope.clientFilters.all_required_specializations_complete).toBeUndefined();
        });
    });

    describe('csvExportColunns', () => {
        it('should not include save column', () => {
            render();
            expect(_.pluck(scope.columns, 'id')).toContain('editorAbilities');
            expect(_.pluck(scope.csvExportColunns, 'id')).not.toContain('editorAbilities');
        });
    });
});
