import 'AngularSpecHelper';
import 'Admin/angularModule';
import 'Users/angularModule/spec/_mock/fixtures/users';

describe('Admin::AdminEditInstitution', () => {
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let guid;
    let Institution;
    let institution;
    let User;
    let $window;
    let $injector;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Admin', 'SpecHelper');

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            SpecHelper = $injector.get('SpecHelper');
            Institution = $injector.get('Institution');
            guid = $injector.get('guid');
            User = $injector.get('User');
            $window = $injector.get('$window');

            $injector.get('UserFixtures');
        });

        institution = Institution.new({
            id: guid.generate(),
            reports_viewer_ids: [],
            users: [User.fixtures.getInstance(), User.fixtures.getInstance()],
            external: true,
        });

        SpecHelper.stubCurrentUser('admin');
        SpecHelper.stubDirective('itemGroupsEditor');
        SpecHelper.stubDirective('localePackSelectize');
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        Institution.expect('show');

        renderer = SpecHelper.renderer();
        renderer.scope.thing = institution;
        renderer.render('<admin-edit-institution thing="thing"></admin-edit-institution>');
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    it('should only load users and show report viewers UI if the institution is external', () => {
        institution.external = false;
        render();
        SpecHelper.expectElement(elem, '#external');
    });

    it('should save an institution', () => {
        render();
        scope.proxyIsValid = true;
        scope.$digest();
        Institution.expect('update');
        SpecHelper.click(elem, '[name="save"]');
        Institution.flush('update');
    });

    it('should disable delete button and confirm before save when a special institution', () => {
        institution.name = 'Quantic';
        institution.id = '85fec419-8dc5-45a5-afbd-0cc285a595b9';
        render();

        SpecHelper.expectElementDisabled(elem, '[name="destroy"]');
        jest.spyOn($window, 'confirm').mockReturnValue(true);
        scope.proxyIsValid = true;
        scope.$digest();
        Institution.expect('update');
        SpecHelper.click(elem, '[name="save"]');
        expect($window.confirm).toHaveBeenCalledWith('Are you sure that you want to save the Quantic institution?');
        Institution.flush('update');
    });

    describe('scorm token', () => {
        it('should only show the UI for external institutions', () => {
            institution.external = true;
            render();
            SpecHelper.expectElement(elem, '#scorm-token');
        });

        it('should generate a scorm token', () => {
            institution.external = true;
            render();
            expect(scope.proxy.scorm_token).toBeUndefined();
            SpecHelper.click(elem, '[name="generate-token"]');
            expect(scope.proxy.scorm_token).toBeDefined();
        });
    });
});
