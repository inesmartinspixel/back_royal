import 'AngularSpecHelper';
import 'Admin/angularModule';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';

describe('Admin::ImportCurriculumTemplate', () => {
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let Cohort;
    let CurriculumTemplate;
    let guid;
    let $window;
    let schedulableItem;
    let curriculumTemplates;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Admin', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            Cohort = $injector.get('Cohort');
            CurriculumTemplate = $injector.get('CurriculumTemplate');
            guid = $injector.get('guid');
            $window = $injector.get('$window');

            $injector.get('CohortFixtures');
        });

        schedulableItem = Cohort.fixtures.getInstance();
        curriculumTemplates = [
            CurriculumTemplate.new({
                name: 'test template 1',
            }),
            CurriculumTemplate.new({
                name: 'test template 2',
            }),
            CurriculumTemplate.new({
                name: 'test template 3',
                groups: ['foo', 'bar', 'baz'],
                periods: [
                    {
                        days: 1337,
                    },
                ],
                specialization_playlist_pack_ids: [guid.generate(), guid.generate()],
                num_required_specializations: 2,
                graduation_days_offset_from_end: 4242,
                diploma_generation_days_offset_from_end: 10,
                registration_deadline_days_offset: 3,
                early_registration_deadline_days_offset: 8,
                project_submission_email: 'project_submission@pedago.com',
                playlist_collections: [
                    {
                        title: '',
                        required_playlist_pack_ids: [guid.generate(), guid.generate()],
                    },
                ],
            }),
        ];

        SpecHelper.stubCurrentUser('admin');
        SpecHelper.stubDirective('localePackSelectize');
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.schedulableItem = schedulableItem;
        renderer.render('<import-curriculum-template schedulable-item="schedulableItem"></import-curriculum-template>');
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    it('should import a schedule', () => {
        jest.spyOn($window, 'confirm').mockReturnValue(true);
        CurriculumTemplate.expect('index').returns(curriculumTemplates);
        render();
        CurriculumTemplate.flush('index');

        scope.importCurriculumTemplate(curriculumTemplates[2]);

        expect(_.invoke(schedulableItem.groups, 'asJson')).toEqual(_.invoke(curriculumTemplates[2].groups, 'asJson'));
        expect(_.invoke(schedulableItem.periods, 'asJson')).toEqual(_.invoke(curriculumTemplates[2].periods, 'asJson'));
        expect(_.invoke(schedulableItem.admission_rounds, 'asJson')).toEqual(
            _.invoke(curriculumTemplates[2].admission_rounds, 'asJson'),
        );
        expect(_.invoke(schedulableItem.id_verification_periods, 'asJson')).toEqual(
            _.invoke(curriculumTemplates[2].id_verification_periods, 'asJson'),
        );

        expect(schedulableItem.specialization_playlist_pack_ids).toEqual(
            curriculumTemplates[2].specialization_playlist_pack_ids,
        );
        expect(schedulableItem.num_required_specializations).toEqual(
            curriculumTemplates[2].num_required_specializations,
        );
        expect(schedulableItem.emba_interest_days_offset).toEqual(curriculumTemplates[2].emba_interest_days_offset);
        expect(schedulableItem.registration_deadline_days_offset).toEqual(
            curriculumTemplates[2].registration_deadline_days_offset,
        );
        expect(schedulableItem.early_registration_deadline_days_offset).toEqual(
            curriculumTemplates[2].early_registration_deadline_days_offset,
        );
        expect(schedulableItem.enrollment_deadline_days_offset).toEqual(
            curriculumTemplates[2].enrollment_deadline_days_offset,
        );
        expect(schedulableItem.graduation_days_offset_from_end).toEqual(
            curriculumTemplates[2].graduation_days_offset_from_end,
        );
        expect(schedulableItem.program_type).toEqual(curriculumTemplates[2].program_type);
        expect(schedulableItem.diploma_generation_days_offset_from_end).toEqual(
            curriculumTemplates[2].diploma_generation_days_offset_from_end,
        );
        expect(schedulableItem.project_submission_email).toEqual(curriculumTemplates[2].project_submission_email);
        expect(schedulableItem.project_documents).toEqual(curriculumTemplates[2].project_documents);
        expect(schedulableItem.enrollment_agreement_template_id).toEqual(
            curriculumTemplates[2].enrollment_agreement_template_id,
        );
        expect(schedulableItem.playlist_collections).toEqual(curriculumTemplates[2].playlist_collections);
    });

    it('should not blow away the program guide', () => {
        const guideUrl = 'https://quantic.edu/supercoolguide';
        jest.spyOn($window, 'confirm').mockReturnValue(true);
        CurriculumTemplate.expect('index').returns(curriculumTemplates);
        schedulableItem.program_guide_url = guideUrl;
        render();
        CurriculumTemplate.flush('index');

        scope.importCurriculumTemplate(curriculumTemplates[2]);

        expect(schedulableItem.program_guide_url).toEqual(guideUrl);
    });
});
