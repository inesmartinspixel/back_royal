import 'AngularSpecHelper';
import 'Admin/angularModule';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';

describe('Admin::EditProjectDocuments', () => {
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let LearnerProject;
    let learnerProject;
    let NavigationHelperMixin;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Admin', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            LearnerProject = $injector.get('LearnerProject');
            $injector.get('CohortFixtures');
            NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
        });

        learnerProject = LearnerProject.new({
            project_documents: [
                {
                    file_file_name: 'foo.pdf',
                    title: 'foo',
                    url: 'foo.com',
                },
                {
                    file_file_name: 'bar.pdf',
                    title: 'bar',
                    url: 'bar.com',
                },
            ],
        });

        render();
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.ngModel = learnerProject;
        renderer.render('<edit-project-documents ng-model="ngModel"></edit-project-documents>');
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    it('should allow reordering documents', () => {
        SpecHelper.expectTextInputVal(elem, '[name="project-document-title"]:eq(0)', 'foo');
        SpecHelper.expectTextInputVal(elem, '[name="project-document-title"]:eq(1)', 'bar');
        SpecHelper.click(elem, '[name="move-project-document-down"]:eq(0)');
        SpecHelper.expectTextInputVal(elem, '[name="project-document-title"]:eq(0)', 'bar');
        SpecHelper.expectTextInputVal(elem, '[name="project-document-title"]:eq(1)', 'foo');
    });

    it('should open document when clicking the file name', () => {
        jest.spyOn(NavigationHelperMixin, 'loadUrl').mockImplementation(() => {});
        SpecHelper.click(elem, '[name="project-document-file-name"]:eq(0)');
        expect(NavigationHelperMixin.loadUrl).toHaveBeenCalledWith('foo.com', '_blank');
    });

    it('should allow for removing a file in the project documents section', () => {
        SpecHelper.expectElements(elem, '[name="remove-project-document"]', 2);
        SpecHelper.click(elem, '[name="remove-project-document"]:eq(0)');
        expect(scope.ngModel.project_documents.length).toEqual(1);
        expect(scope.ngModel.project_documents[0].file_file_name).toEqual('bar.pdf');
    });
});
