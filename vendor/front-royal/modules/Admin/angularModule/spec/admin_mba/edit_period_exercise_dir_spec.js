import 'AngularSpecHelper';
import 'Admin/angularModule';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';

describe('Admin::EditPeriodExercise', () => {
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let Cohort;
    let cohort;
    let exercise;
    let $injector;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Admin', 'SpecHelper');

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            SpecHelper = $injector.get('SpecHelper');
            Cohort = $injector.get('Cohort');
            $injector.get('CohortFixtures');
        });
        cohort = Cohort.fixtures.getInstance({
            periods: [
                {
                    exercises: [
                        {
                            id: 'be83235e-ee1c-41e4-aeba-2d02bff27ed4',
                            hours_offset_from_end: -831,
                            message: 'Here is a message',
                            channel: '#video-discussion',
                            document: {
                                id: '7deda14a-34bd-46a5-aa1a-4d7697ca43d6',
                                file_file_name: 'elephants.png',
                                url:
                                    'https://uploads-development.smart.ly/exercise_documents/c55b5aebbcd2f9f06e0c8e1d3d33682c.png',
                                title: 'elephants.png',
                            },
                        },
                    ],
                },
            ],
        });
        expect(cohort.slack_rooms.length).toEqual(2); // sanity check
        exercise = cohort.periods[0].exercises[0];
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.exercise = exercise;
        renderer.render('<edit-period-exercise exercise="exercise"></edit-period-exercise exercise>');
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    describe('message', () => {
        it('should support setting a message on the exercise or on each slack room', () => {
            render();
            SpecHelper.expectNoElement(elem, '.messages .sub-text');
            SpecHelper.updateInput(elem, '[name="message"]', 'New message');
            expect(exercise.message).toEqual('New message');

            // Switch to distinct messages
            SpecHelper.checkCheckbox(elem, '[name="send-distinct-message"]');
            SpecHelper.expectElements(elem, '.messages .sub-text', 2);
            SpecHelper.expectElementText(elem, '.messages .sub-text', `Message for ${cohort.slack_rooms[0].title}`, 0);
            expect(exercise.message).toBeUndefined();
            expect(exercise.slack_room_overrides[cohort.slack_rooms[0].id].message).toEqual('New message');
            expect(exercise.slack_room_overrides[cohort.slack_rooms[1].id].message).toEqual('New message');

            // Update one of the messages
            SpecHelper.updateInput(elem, '[name="message"]:eq(0)', 'Distinct message');
            expect(exercise.slack_room_overrides[cohort.slack_rooms[0].id].message).toEqual('Distinct message');
            expect(exercise.slack_room_overrides[cohort.slack_rooms[1].id].message).toEqual('New message');

            // Switch back to 1 default message
            SpecHelper.uncheckCheckbox(elem, '[name="send-distinct-message"]');
            expect(exercise.message).toEqual('Distinct message');
            expect(exercise.slack_room_overrides[cohort.slack_rooms[0].id].message).toBeUndefined();
            expect(exercise.slack_room_overrides[cohort.slack_rooms[1].id].message).toBeUndefined();
        });

        it('should support clicking to send distinct messages when there is not yet a message', () => {
            delete exercise.message;
            render();
            SpecHelper.checkCheckbox(elem, '[name="send-distinct-message"]');
            SpecHelper.expectElements(elem, '.messages .sub-text', 2);
            SpecHelper.expectElementText(elem, '.messages .sub-text', `Message for ${cohort.slack_rooms[0].title}`, 0);
        });
    });

    describe('document', () => {
        it('should support setting a document on the exercise or on each slack room', () => {
            render();
            SpecHelper.expectNoElement(elem, '.documents .sub-text');
            SpecHelper.updateInput(elem, '.documents [name="exercise-document-title"]', 'New title');
            expect(exercise.document.title).toEqual('New title');

            // Switch to distinct documents
            SpecHelper.checkCheckbox(elem, '[name="send-distinct-document"]');
            SpecHelper.expectElements(elem, '.documents .sub-text', 2);
            SpecHelper.expectElementText(
                elem,
                '.documents .sub-text',
                `Document for ${cohort.slack_rooms[0].title}`,
                0,
            );
            expect(exercise.document).toBeUndefined();
            expect(exercise.slack_room_overrides[cohort.slack_rooms[0].id].document.title).toEqual('New title');
            expect(exercise.slack_room_overrides[cohort.slack_rooms[1].id].document.title).toEqual('New title');

            // Update one of the documents
            SpecHelper.updateInput(elem, '.documents [name="exercise-document-title"]:eq(0)', 'Distinct title');
            expect(exercise.slack_room_overrides[cohort.slack_rooms[0].id].document.title).toEqual('Distinct title');
            expect(exercise.slack_room_overrides[cohort.slack_rooms[1].id].document.title).toEqual('New title');

            // Switch back to 1 default documents
            SpecHelper.uncheckCheckbox(elem, '[name="send-distinct-document"]');
            expect(exercise.document.title).toEqual('Distinct title');
            expect(exercise.slack_room_overrides[cohort.slack_rooms[0].id].document).toBeUndefined();
            expect(exercise.slack_room_overrides[cohort.slack_rooms[1].id].document).toBeUndefined();
        });

        it('should support clicking to send distinct documents when there is not yet a document', () => {
            delete exercise.document;
            render();
            SpecHelper.checkCheckbox(elem, '[name="send-distinct-document"]');
            SpecHelper.expectElements(elem, '.documents .sub-text', 4); // one for the "Document for" string and one for the error container
            SpecHelper.expectElements(elem, '.documents input[type="button"]', 2);
            SpecHelper.expectElementText(
                elem,
                '.documents .sub-text',
                `Document for ${cohort.slack_rooms[0].title}`,
                0,
            );
        });
    });

    it('should hide checkboxes if there are less than two slack rooms', () => {
        render();
        SpecHelper.expectElement(elem, '[name="send-distinct-document"]');
        SpecHelper.expectElement(elem, '[name="send-distinct-message"]');
        expect(cohort.slack_rooms.length).toEqual(2); // sanity check
        cohort.removeSlackRoom(cohort.slack_rooms[0]);
        cohort.removeSlackRoom(cohort.slack_rooms[0]);
        scope.$apply();
        SpecHelper.expectNoElement(elem, '[name="send-distinct-document"]');
        SpecHelper.expectNoElement(elem, '[name="send-distinct-message"]');
    });

    it('should handle another slack room being added to the list', () => {
        render();
        SpecHelper.checkCheckbox(elem, '[name="send-distinct-document"]');
        SpecHelper.checkCheckbox(elem, '[name="send-distinct-message"]');
        SpecHelper.expectElements(elem, '.messages .sub-text', 2);
        cohort.addSlackRoom();
        scope.$digest();
        SpecHelper.expectElements(elem, '.messages .sub-text', 3);
    });

    it('should handle a slack room being removed from the list', () => {
        cohort.addSlackRoom();
        expect(cohort.slack_rooms.length).toEqual(3); // sanity check
        render();
        SpecHelper.checkCheckbox(elem, '[name="send-distinct-document"]');
        SpecHelper.checkCheckbox(elem, '[name="send-distinct-message"]');
        SpecHelper.expectElements(elem, '.messages .sub-text', 3);

        // Removing a slack room should remove the inputs for it
        cohort.removeSlackRoom(cohort.slack_rooms[0]);
        scope.$digest();
        SpecHelper.expectElements(elem, '.messages .sub-text', 2);
        SpecHelper.expectCheckboxChecked(elem, '[name="send-distinct-document"]');
        SpecHelper.expectCheckboxChecked(elem, '[name="send-distinct-message"]');

        // Once we are down to 1 slack room, we should see the message and
        // document just on the exercise
        const exercise = cohort.periods[0].exercises[0];
        const message = exercise.slack_room_overrides[cohort.slack_rooms[0].id].message;
        const doc = exercise.slack_room_overrides[cohort.slack_rooms[0].id].document;
        cohort.removeSlackRoom(cohort.slack_rooms[0]);
        scope.$digest();
        SpecHelper.expectElements(elem, '.messages .sub-text', 0);
        SpecHelper.expectNoElement(elem, '[name="send-distinct-document"]');
        SpecHelper.expectNoElement(elem, '[name="send-distinct-message"]');
        expect(exercise.message).toEqual(message);
        expect(exercise.document).toEqual(doc);
    });
});
