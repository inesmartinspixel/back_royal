import 'AngularSpecHelper';
import 'Admin/angularModule';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';
import 'Users/angularModule/spec/_mock/fixtures/users';

describe('Admin::AdminCohortStudentsTableHelper', () => {
    let $injector;
    let SpecHelper;
    let Cohort;
    let cohort;
    let students;
    let $q;
    let $window;
    let $http;
    let ngToast;
    let ClientStorage;
    let helper;
    let User;
    let AdminCohortStudentsTableHelper;
    let $rootScope;
    let $timeout;
    let guid;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Admin', 'SpecHelper');

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            SpecHelper = $injector.get('SpecHelper');
            Cohort = $injector.get('Cohort');
            User = $injector.get('User');
            AdminCohortStudentsTableHelper = $injector.get('AdminCohortStudentsTableHelper');
            $rootScope = $injector.get('$rootScope');
            ngToast = $injector.get('ngToast');
            $q = $injector.get('$q');
            $window = $injector.get('$window');
            $http = $injector.get('$http');
            $timeout = $injector.get('$timeout');
            ClientStorage = $injector.get('ClientStorage');
            guid = $injector.get('guid');

            $injector.get('CohortFixtures');
            $injector.get('UserFixtures');
        });

        cohort = Cohort.fixtures.getInstance();
        students = [
            User.fixtures.getInstance(),
            User.fixtures.getInstance(),
            User.fixtures.getInstance(),
            User.fixtures.getInstance(),
            User.fixtures.getInstance(),
        ];

        helper = new AdminCohortStudentsTableHelper(cohort, '/batch/update');
        helper.students = students;
        helper._onStudentsLoaded();
    });

    describe('hasUnsavedChanges', () => {
        it('should be true only if one of the tracked values has changed on the proxy', () => {
            helper.trackStudentChanges(student => student.propWeCareAbout);
            const student = students[0];
            expect(helper.hasUnsavedChanges).toBe(false);
            student.$$proxy.anotherProp = true;
            expect(helper.hasUnsavedChanges).toBe(false);
            student.$$proxy.propWeCareAbout = 'some value';
            expect(helper.hasUnsavedChanges).toBe(true);

            // Test the integration with _onStudentsLoaded.  Once we run that method,
            // hasUnsavedChanges should revert to false.
            helper._onStudentsLoaded();
            expect(helper.hasUnsavedChanges).toBe(false);
        });
    });

    describe('currentPageHasUnsavedChanges', () => {
        it('should be true only if one of the tracked values has changed on the proxy for a student on the current page', () => {
            helper.trackStudentChanges(student => student.propWeCareAbout);
            const student = students[0];
            const otherStudent = students[1];
            helper.getStudentsOnCurrentPage = () => [student];
            expect(helper.currentPageHasUnsavedChanges).toBe(false);
            otherStudent.$$proxy.propWeDontCareAbout = true;
            expect(helper.currentPageHasUnsavedChanges).toBe(false);
            otherStudent.$$proxy.propWeCareAbout = true;
            expect(helper.currentPageHasUnsavedChanges).toBe(false);
            student.$$proxy.propWeDontCareAbout = true;
            expect(helper.currentPageHasUnsavedChanges).toBe(false);
            student.$$proxy.propWeCareAbout = true;
            expect(helper.currentPageHasUnsavedChanges).toBe(true);

            // resetting the proxy should revert the currentPageHasUnsavedChanges flag
            student.$$proxy = User.new(student.asJson());
            expect(helper.currentPageHasUnsavedChanges).toBe(false);
        });

        it('should not error if no getStudentsOnCurrentPage function is present', () => {
            helper.getStudentsOnCurrentPage = undefined;
            expect(helper.currentPageHasUnsavedChanges).toBe(false);
        });
    });

    _.each(['getNameColumn', 'getEmailColumn'], meth => {
        describe(meth, () => {
            let columnConfig;
            let student;

            beforeEach(() => {
                columnConfig = helper[meth]();
                student = students[0];
            });

            it('should got to student detail page on click', () => {
                const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
                jest.spyOn(NavigationHelperMixin, 'loadUrl').mockImplementation(() => {});
                jest.spyOn(ClientStorage, 'setItem').mockImplementation(() => {});
                const elem = renderColumn(columnConfig, student).elem;
                SpecHelper.click(elem, '[ng-click]');
                expect(NavigationHelperMixin.loadUrl).toHaveBeenCalledWith(
                    `/admin/users/applicants?id=${student.id}&tab=student-records`,
                    '_blank',
                );
            });
        });
    });

    describe('save column', () => {
        let saveColumnConfig;
        let student;

        beforeEach(() => {
            saveColumnConfig = helper.getSaveColumn();
            student = students[0];
        });

        it('should disable button when there is no change or while saving', () => {
            let updated = false;
            jest.spyOn(helper, '_studentHasUnsavedChanges').mockImplementation(() => updated);
            const renderer = renderColumn(saveColumnConfig, student);
            const elem = renderer.elem;
            const scope = renderer.scope;
            SpecHelper.expectElementDisabled(elem, 'button');
            updated = true;
            scope.$digest();
            SpecHelper.expectElementEnabled(elem, 'button');
            helper.saving = true;
            scope.$digest();
            SpecHelper.expectElementDisabled(elem, 'button');
        });

        it('should call saveAll on click', () => {
            jest.spyOn(saveColumnConfig.callbacks, 'isDisabled').mockReturnValue(false);
            const elem = renderColumn(saveColumnConfig, student).elem;
            jest.spyOn(helper, 'saveAll').mockImplementation(() => {});
            SpecHelper.click(elem, 'button');
            expect(helper.saveAll).toHaveBeenCalledWith([student], false);
        });
    });

    describe('disableSaveCurrentPage', () => {
        it('should be true if !currentPageHasUnsavedChanges or saving', () => {
            const unsavedChangesSpy = jest.spyOn(helper, 'currentPageHasUnsavedChanges', 'get').mockReturnValue(true);
            helper.saving = false;
            expect(helper.disableSaveCurrentPage).toBe(false);
            unsavedChangesSpy.mockReturnValue(false);
            expect(helper.disableSaveCurrentPage).toBe(true);
            unsavedChangesSpy.mockReturnValue(true);
            expect(helper.disableSaveCurrentPage).toBe(false);
            helper.saving = true;
            expect(helper.disableSaveCurrentPage).toBe(true);
        });
    });

    describe('disableSaveCurrentPageAndGoToNextPage', () => {
        it("should be true if disableSaveCurrentPage or we're onLastPage", () => {
            const disableSaveSpy = jest.spyOn(helper, 'disableSaveCurrentPage', 'get').mockReturnValue(false);
            const onLastPageSpy = jest.spyOn(helper, 'onLastPage', 'get').mockReturnValue(false);
            expect(helper.disableSaveCurrentPageAndGoToNextPage).toBe(false);
            disableSaveSpy.mockReturnValue(true);
            expect(helper.disableSaveCurrentPageAndGoToNextPage).toBe(true);
            disableSaveSpy.mockReturnValue(false);
            expect(helper.disableSaveCurrentPageAndGoToNextPage).toBe(false);
            onLastPageSpy.mockReturnValue(true);
            expect(helper.disableSaveCurrentPageAndGoToNextPage).toBe(true);
        });
    });

    describe('onLastPage', () => {
        it('should be calculated using clientPaginatePerPage and the current page number', () => {
            // NOTE: The helper has been mocked to have 5 students, so page 3 should be the last page.
            let currentPage = 1;
            helper.getCurrentPage = () => currentPage;
            helper.clientPaginatePerPage = 2;
            expect(helper.onLastPage).toBe(false);
            currentPage = 2;
            expect(helper.onLastPage).toBe(false);
            currentPage = 3;
            expect(helper.onLastPage).toBe(true);

            // Add 1 more student so that we're up to 6 students and verify that onLastPage is true
            // when the last page is maxed out based on the clientPaginatePerPage value.
            helper.students.push(User.fixtures.getInstance());
            expect(helper.onLastPage).toBe(true);
        });
    });

    describe('saveAll', () => {
        it('should prompt user to confirm save action', () => {
            jest.spyOn(helper, '_studentHasUnsavedChanges').mockReturnValue(true);
            jest.spyOn($window, 'confirm').mockImplementation(() => {});
            helper.saveAll();
            expect($window.confirm).toHaveBeenCalledWith(
                `You are about to save ${students.length} users. Okay to proceed?`,
            );
        });

        it('should use passed in confirmationMessage', () => {
            jest.spyOn(helper, '_studentHasUnsavedChanges').mockReturnValue(true);
            jest.spyOn($window, 'confirm').mockImplementation(() => {});
            helper.saveAll([], true, 'Custom confirmation message!');
            expect($window.confirm).toHaveBeenCalledWith('Custom confirmation message!');
        });

        it('should resolve with false if save is canceled via confirmation dialog', () => {
            jest.spyOn(helper, '_studentHasUnsavedChanges').mockReturnValue(true);
            jest.spyOn($window, 'confirm').mockReturnValue(false);
            const promise = helper.saveAll();
            expect(promise.$$state.status).toBe(1); // promise should be resolved
            expect(promise.$$state.value).toBe(false); // resolved value should be `false`
        });

        describe('when saveAllInBatches', () => {
            beforeEach(() => {
                helper.saveAllBatchSize = 2;
                helper.saveAllInBatches = true;
            });

            it('should _batchSave students in batches of saveAllBatchSize', () => {
                jest.spyOn(helper, '_studentHasUnsavedChanges').mockReturnValue(true); // make all students have unsaved changes
                const deferredBatchSaveCall = $q.defer();
                const deferred = $q.defer();
                jest.spyOn(helper, '_batchSave').mockReturnValue(deferredBatchSaveCall.promise);
                jest.spyOn($q, 'all').mockReturnValue(deferred.promise);
                jest.spyOn(helper, '_onBatchSaveResponse').mockImplementation(() => {});
                jest.spyOn(helper, '_resetSavingAllFlag').mockImplementation(() => {});

                helper.saveAll(null, false);

                expect(helper.saving).toBe(true);
                expect(helper._batchSave).toHaveBeenCalledTimes(3);
                expect(helper._batchSave).toHaveBeenNthCalledWith(1, students.slice(0, 2));
                expect(helper._batchSave).toHaveBeenNthCalledWith(2, students.slice(2, 4));
                expect(helper._batchSave).toHaveBeenNthCalledWith(3, students.slice(4, 5)); // only 5 students attached to helper
                expect(helper._onBatchSaveResponse).not.toHaveBeenCalled();
                expect(helper._resetSavingAllFlag).not.toHaveBeenCalled();

                deferred.resolve(['foo', 'bar', 'baz']); // mock the resolved values of the 3 _batchSave calls
                $rootScope.$apply(); // Propagate promise resolution to 'then' functions

                expect(helper._onBatchSaveResponse).toHaveBeenCalledWith(['foo', 'bar', 'baz'], students);
                expect(helper._resetSavingAllFlag).toHaveBeenCalled();
            });
        });

        describe('when !saveAllInBatches', () => {
            beforeEach(() => {
                helper.saveAllInBatches = false;
            });

            it('should save all updated students in a single _batchSave call', () => {
                jest.spyOn(helper, '_studentHasUnsavedChanges').mockReturnValue(true); // make all students have unsaved changes
                const deferred = $q.defer();
                jest.spyOn(helper, '_batchSave').mockReturnValue(deferred.promise);
                jest.spyOn(helper, '_onBatchSaveResponse').mockImplementation(() => {});
                jest.spyOn(helper, '_resetSavingAllFlag').mockImplementation(() => {});

                helper.saveAll(null, false);

                expect(helper.saving).toBe(true);
                expect(helper._batchSave).toHaveBeenCalledTimes(1);
                expect(helper._batchSave).toHaveBeenCalledWith(students); // all students were updated
                expect(helper._onBatchSaveResponse).not.toHaveBeenCalled();
                expect(helper._resetSavingAllFlag).not.toHaveBeenCalled();

                deferred.resolve('foo'); // mock resolved value for _batchSave call
                $rootScope.$apply(); // Propagate promise resolution to 'then' functions

                expect(helper._onBatchSaveResponse).toHaveBeenCalledWith('foo', students);
                expect(helper._resetSavingAllFlag).toHaveBeenCalled();
            });
        });
    });

    describe('saveCurrentPage', () => {
        it('should saveAll studentsOnCurrentPage with the appropriate confirmation message', () => {
            helper.trackStudentChanges(student => student.propWeCareAbout);
            const studentsOnCurrentPage = students.slice(0, 2);
            const studentsOnOtherPages = students.slice(2);
            helper.getStudentsOnCurrentPage = () => studentsOnCurrentPage;
            jest.spyOn(helper, 'saveAll').mockImplementation(() => {});

            // one student on current page with unsaved changes, no students on other pages with unsaved changes.
            studentsOnCurrentPage[0].$$proxy.propWeCareAbout = true;
            helper.saveCurrentPage();
            let expectedMessage = 'You are about to save 1 user from the current page.\n\nOkay to proceed?';
            expect(helper.saveAll).toHaveBeenCalledWith(studentsOnCurrentPage, true, expectedMessage);
            helper.saveAll.mockClear();

            // one student on current page with unsaved changes, one student on another page with unsaved changes.
            studentsOnOtherPages[0].$$proxy.propWeCareAbout = true;
            helper.saveCurrentPage();
            expectedMessage =
                'You are about to save 1 user from the current page.\n\n1 user not on the current page has unsaved changes that will not be saved. Users with unsaved changes on other pages will retain their unsaved changes after the user from the current page has been saved.\n\nOkay to proceed?';
            expect(helper.saveAll).toHaveBeenCalledWith(studentsOnCurrentPage, true, expectedMessage);
            helper.saveAll.mockClear();

            // one student on current page with unsaved changes, multiple students on other pages with unsaved changes.
            studentsOnOtherPages[1].$$proxy.propWeCareAbout = true;
            helper.saveCurrentPage();
            expectedMessage =
                'You are about to save 1 user from the current page.\n\n2 users not on the current page have unsaved changes that will not be saved. Users with unsaved changes on other pages will retain their unsaved changes after the user from the current page has been saved.\n\nOkay to proceed?';
            expect(helper.saveAll).toHaveBeenCalledWith(studentsOnCurrentPage, true, expectedMessage);
            helper.saveAll.mockClear();

            // two students on current page with unsaved changes, no students on other pages with unsaved changes.
            studentsOnCurrentPage[1].$$proxy.propWeCareAbout = true;
            delete studentsOnOtherPages[0].$$proxy.propWeCareAbout;
            delete studentsOnOtherPages[1].$$proxy.propWeCareAbout;
            helper.saveCurrentPage();
            expectedMessage = 'You are about to save 2 users from the current page.\n\nOkay to proceed?';
            expect(helper.saveAll).toHaveBeenCalledWith(studentsOnCurrentPage, true, expectedMessage);
            helper.saveAll.mockClear();

            // two students on current page with unsaved changes, one student on another page with unsaved changes.
            studentsOnOtherPages[0].$$proxy.propWeCareAbout = true;
            helper.saveCurrentPage();
            expectedMessage =
                'You are about to save 2 users from the current page.\n\n1 user not on the current page has unsaved changes that will not be saved. Users with unsaved changes on other pages will retain their unsaved changes after the users from the current page have been saved.\n\nOkay to proceed?';
            expect(helper.saveAll).toHaveBeenCalledWith(studentsOnCurrentPage, true, expectedMessage);
            helper.saveAll.mockClear();

            // two students on current page with unsaved changes, multiple students on other pages with unsaved changes.
            studentsOnOtherPages[1].$$proxy.propWeCareAbout = true;
            helper.saveCurrentPage();
            expectedMessage =
                'You are about to save 2 users from the current page.\n\n2 users not on the current page have unsaved changes that will not be saved. Users with unsaved changes on other pages will retain their unsaved changes after the users from the current page have been saved.\n\nOkay to proceed?';
            expect(helper.saveAll).toHaveBeenCalledWith(studentsOnCurrentPage, true, expectedMessage);
        });

        it('should not try to save any users and immediately resolve with false if no students on current page', () => {
            helper.getStudentsOnCurrentPage = () => [];
            jest.spyOn(helper, 'saveAll');
            const promise = helper.saveCurrentPage();
            expect(helper.saveAll).not.toHaveBeenCalled();
            expect(promise.$$state.status).toEqual(1); // resolved
            expect(promise.$$state.value).toBe(false); // no save happened, so false is returned
        });
    });

    describe('saveCurrentPageAndGoToNextPage', () => {
        it('should saveCurrentPage and then go to next page if we successfully saved the users', () => {
            const deferred = $q.defer();
            jest.spyOn(helper, 'saveCurrentPage').mockReturnValue(deferred.promise);
            helper.goToPage = jest.fn();
            helper.getCurrentPage = jest.fn(() => 1);
            helper.saveCurrentPageAndGoToNextPage();
            expect(helper.saveCurrentPage).toHaveBeenCalled();
            expect(helper.getCurrentPage).not.toHaveBeenCalled();
            expect(helper.goToPage).not.toHaveBeenCalled();
            deferred.resolve();
            $timeout.flush();
            expect(helper.getCurrentPage).toHaveBeenCalled();
            expect(helper.goToPage).toHaveBeenCalledWith(2);
        });

        it('should saveCurrentPage but not go to next page if we did not successfully save the users', () => {
            const deferred = $q.defer();
            jest.spyOn(helper, 'saveCurrentPage').mockReturnValue(deferred.promise);
            helper.goToPage = jest.fn();
            helper.getCurrentPage = jest.fn(() => 1);
            helper.saveCurrentPageAndGoToNextPage();
            expect(helper.saveCurrentPage).toHaveBeenCalled();
            expect(helper.getCurrentPage).not.toHaveBeenCalled();
            expect(helper.goToPage).not.toHaveBeenCalled();
            deferred.resolve(false); // false indicates we failed to save the users
            $timeout.flush();
            expect(helper.getCurrentPage).not.toHaveBeenCalled();
            expect(helper.goToPage).not.toHaveBeenCalled();
        });
    });

    describe('_batchSave', () => {
        it('should _getRecordsForBatchSave and make an API call to batchSaveEndpoint', () => {
            const updatedStudents = students.slice(0, 2);
            jest.spyOn(helper, '_getRecordsForBatchSave').mockReturnValue(updatedStudents);
            const deferred = $q.defer();
            jest.spyOn($http, 'put').mockReturnValue(deferred.promise);

            helper._batchSave(updatedStudents);

            expect($http.put).toHaveBeenCalled();
            expect($http.put.mock.calls[0][0]).toEqual(helper.batchSaveEndpoint);
            expect($http.put.mock.calls[0][1].records).toEqual(updatedStudents);
            expect($http.put.mock.calls[0][1].meta).toEqual({
                cohort_id: cohort.id,
            });
        });
    });

    describe('_onBatchSaveResponse', () => {
        it('should copy over any values from $$proxy and any values from response', () => {
            const updatedStudents = students.slice(0, 2);
            updatedStudents.forEach(student => {
                student.$$proxy = User.new(student.asJson());
                student.$$proxy.foo = 'foo';
            });
            const responseUsers = updatedStudents.map(student => ({ id: student.id, bar: 'bar' }));
            const response = {
                data: {
                    contents: {
                        users: responseUsers,
                    },
                },
            };
            jest.spyOn(ngToast, 'create').mockImplementation(() => {});
            jest.spyOn(helper, '_onStudentsLoaded').mockImplementation(() => {});

            updatedStudents.forEach(student => {
                expect(student.foo).toBeUndefined(); // prevent false-positive
                expect(student.bar).toBeUndefined(); // prevent false-positive
            });

            helper._onBatchSaveResponse(response, updatedStudents);

            updatedStudents.forEach(student => {
                expect(student.foo).toEqual('foo'); // $$proxy value is copied over
                expect(student.bar).toEqual('bar'); // response value is copied over
            });
            expect(ngToast.create).toHaveBeenCalledWith({
                content: `2 Students Saved`,
                className: 'success',
            });
            expect(helper._onStudentsLoaded).toHaveBeenCalledWith(updatedStudents);
        });

        it('should handle multiple responses', () => {
            const updatedStudents = students.slice(0, 4);
            updatedStudents.forEach(student => {
                student.$$proxy = User.new(student.asJson());
                student.$$proxy.foo = 'foo';
            });
            const batchSize = 2;
            const responses = [];
            for (let i = 0; i < batchSize; i++) {
                const index = i * batchSize;
                const responseUsers = updatedStudents
                    .slice(index, index + batchSize)
                    .map(student => ({ id: student.id, bar: 'bar' }));
                const response = {
                    data: {
                        contents: {
                            users: responseUsers,
                        },
                    },
                };
                responses.push(response);
            }
            jest.spyOn(ngToast, 'create').mockImplementation(() => {});
            jest.spyOn(helper, '_onStudentsLoaded').mockImplementation(() => {});

            updatedStudents.forEach(student => {
                expect(student.foo).toBeUndefined(); // prevent false-positive
                expect(student.bar).toBeUndefined(); // prevent false-positive
            });

            helper._onBatchSaveResponse(responses, updatedStudents);

            updatedStudents.forEach(student => {
                expect(student.foo).toEqual('foo'); // $$proxy value is copied over
                expect(student.bar).toEqual('bar'); // response value is copied over
            });
            expect(ngToast.create).toHaveBeenCalledWith({
                content: `4 Students Saved`,
                className: 'success',
            });
            expect(helper._onStudentsLoaded).toHaveBeenCalledWith(updatedStudents);
        });
    });

    describe('_onStudentLoad', () => {
        let student;

        beforeEach(() => {
            student = _.first(students);
        });

        it('should set $$proxy on passed in student', () => {
            student.$$proxy = undefined;
            helper._onStudentLoad(student);
            expect(student.$$proxy.asJson()).toEqual(student.asJson());
        });

        it('should set relevant_cohort on passed in student and copy it onto $$proxy', () => {
            student.relevant_cohort = undefined;
            helper._onStudentLoad(student);
            expect(student.relevant_cohort).toBe(cohort);
            expect(student.$$proxy.relevant_cohort.asJson()).toEqual(cohort.asJson());
        });

        it('should ensure the tracked values have been added to the cache', () => {
            student.propWeCareAbout = true;
            const tracker = { id: 'some_tracker', getTrackedValue: jest.fn() };
            helper.changeTrackers.push(tracker);
            jest.spyOn(helper, '_ensureTrackedValueAddedToCache').mockImplementation(() => {});
            helper._onStudentLoad(student);
            expect(helper._ensureTrackedValueAddedToCache).toHaveBeenCalledWith(student, tracker, true); // true means we should reset the cached
        });
    });

    describe('trackStudentChanges', () => {
        it('should add a tracker with a unique id to the changeTrackers array and add create an entry for it in the $$trackedValuesCache', () => {
            expect(helper.changeTrackers).toEqual([]);
            const trackerFunc = () => {};
            jest.spyOn(guid, 'generate').mockReturnValue('foo');
            helper.trackStudentChanges(trackerFunc);
            expect(helper.changeTrackers).toEqual([{ id: 'foo', getTrackedValue: trackerFunc }]);
            expect(helper.$$trackedValuesCache.foo).toEqual({});
        });
    });

    describe('_studentHasUnsavedChanges', () => {
        it("should return false if $$proxy hasn't been setup on student", () => {
            const student = students[0];
            student.$$proxy = undefined;
            expect(helper._studentHasUnsavedChanges(student)).toBe(false);
        });

        it("should be true if tracked proxy value doesn't match cached tracked value for student", () => {
            const student = students[0];
            helper.changeTrackers.push({ id: 'some_tracker', getTrackedValue: jest.fn(() => 'not_foo') });
            jest.spyOn(helper, '_ensureTrackedValueAddedToCache').mockReturnValue('foo');
            expect(helper._studentHasUnsavedChanges(student)).toBe(true);
        });
    });

    describe('_ensureTrackedValueAddedToCache', () => {
        it('should return the cached value', () => {
            const student = students[0];
            const tracker = { id: 'some_tracker', getTrackedValue: jest.fn() };

            // pre-warm the cache with a value for the student
            helper.$$trackedValuesCache[tracker.id] = {};
            helper.$$trackedValuesCache[tracker.id][student.id] = 'foo';

            const val = helper._ensureTrackedValueAddedToCache(student, tracker);
            expect(tracker.getTrackedValue).not.toHaveBeenCalled();
            expect(val).toEqual('foo');
        });

        it("should add the tracked value to cache and return it if it's not already in the cache", () => {
            const student = students[0];
            const tracker = { id: 'some_tracker', getTrackedValue: jest.fn(() => 'foo') };

            // _ensureTrackedValueAddedToCache should be responsible for making sure that
            // the tracker is appropriately entered into the cache if it hasn't been already.
            expect(helper.$$trackedValuesCache[tracker.id]).toBeUndefined();

            const val = helper._ensureTrackedValueAddedToCache(student, tracker);
            expect(tracker.getTrackedValue).toHaveBeenCalled();
            expect(val).toEqual('foo');
            expect(helper.$$trackedValuesCache[tracker.id][student.id]).toEqual('foo');
        });

        it('should reset the tracked value in the cache when told to', () => {
            const student = students[0];
            const tracker = { id: 'some_tracker', getTrackedValue: jest.fn(() => 'bar') };

            // pre-warm the cache with a value for the student
            helper.$$trackedValuesCache[tracker.id] = {};
            helper.$$trackedValuesCache[tracker.id][student.id] = 'foo';

            const val = helper._ensureTrackedValueAddedToCache(student, tracker, true); // true means "reset the cached value"
            expect(tracker.getTrackedValue).toHaveBeenCalled();
            expect(val).toEqual('bar');
            expect(helper.$$trackedValuesCache[tracker.id][student.id]).toEqual('bar');
        });
    });

    describe('_clearTrackedValuesFromCache', () => {
        it('should remove the all of the tracked values from the cache for the student', () => {
            const student = students[0];
            const firstTracker = { id: 'first_tracker' };
            const secondTracker = { id: 'second_tracker' };
            helper.changeTrackers.push(firstTracker);
            helper.changeTrackers.push(secondTracker);

            // add values to cache for the student
            helper.$$trackedValuesCache[firstTracker.id] = {};
            helper.$$trackedValuesCache[firstTracker.id][student.id] = 'foo';
            helper.$$trackedValuesCache[secondTracker.id] = {};
            helper.$$trackedValuesCache[secondTracker.id][student.id] = 'bar';

            helper._clearTrackedValuesFromCache(student);
            expect(helper.$$trackedValuesCache[firstTracker.id][student.id]).toBeUndefined();
            expect(helper.$$trackedValuesCache[secondTracker.id][student.id]).toBeUndefined();
        });
    });

    function renderColumn(columnConfig, student) {
        const renderer = SpecHelper.renderer();
        renderer.scope.column = columnConfig;
        renderer.scope.thing = student;
        renderer.render('<div><ng-include src="column.templateUrl"></ng-include></div>');
        return renderer;
    }
});
