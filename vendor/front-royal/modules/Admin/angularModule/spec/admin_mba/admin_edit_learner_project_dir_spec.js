import 'AngularSpecHelper';
import 'Admin/angularModule';
import 'Users/angularModule/spec/_mock/fixtures/users';

describe('Admin::AdminEditLearnerProject', () => {
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let LearnerProject;
    let learnerProject;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Admin', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            LearnerProject = $injector.get('LearnerProject');
            $injector.get('UserFixtures');
        });

        learnerProject = LearnerProject.new();

        SpecHelper.stubCurrentUser('admin');

        render();
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.thing = learnerProject;
        renderer.render('<admin-edit-learner-project thing="thing"></admin-edit-learner-project>');
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    describe('project_type', () => {
        it('should toggle project_type', () => {
            SpecHelper.updateSelectize(elem, '[name="project_type"]', 'standard');
            expect(scope.proxy.project_type).toEqual('standard');
            SpecHelper.updateSelectize(elem, '[name="project_type"]', 'presentation');
            expect(scope.proxy.project_type).toEqual('presentation');
            SpecHelper.updateSelectize(elem, '[name="project_type"]', 'capstone');
            expect(scope.proxy.project_type).toEqual('capstone');
        });
    });
});
