import 'AngularSpecHelper';
import 'Admin/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';

describe('Admin::AdminCohortSlackAssignments', () => {
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let Cohort;
    let cohort;
    let CareerProfile;
    let $timeout;
    let $q;
    let students;
    let $injector;
    let expectedIndexParams;
    let guid;
    let $httpBackend;
    let HttpQueue;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Admin', 'SpecHelper');

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            SpecHelper = $injector.get('SpecHelper');
            Cohort = $injector.get('Cohort');
            CareerProfile = $injector.get('CareerProfile');
            $injector.get('CareerProfileFixtures');
            $timeout = $injector.get('$timeout');
            $q = $injector.get('$q');
            $injector.get('CohortFixtures');
            guid = $injector.get('guid');
            $httpBackend = $injector.get('$httpBackend');
            HttpQueue = $injector.get('HttpQueue');
        });
        cohort = Cohort.fixtures.getInstance();

        SpecHelper.stubCurrentUser('admin');

        expectedIndexParams = {
            filters: {
                accepted_or_pre_accepted_in: cohort.id,
            },
            view: 'editable',
        };

        jest.spyOn(CareerProfile, 'index').mockImplementation(() =>
            $q.when({
                result: students,
            }),
        );
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function save(code, response, expectedChunkCount = 1) {
        const responseConfig = {};
        const chunks = _.chain(scope.proxy.students)
            .select(s => s.cohort_slack_room_id !== null)
            .map(s => {
                return { id: s.last_application_id, cohort_slack_room_id: s.cohort_slack_room_id };
            })
            .chunk(50)
            .value();

        scope.hasChanges = true; // enable the button
        scope.$digest();

        // Hack-ish: ensure that the HttpQueue `queued_for_seconds` is always zero for testability
        jest.spyOn(HttpQueue.prototype, '_msSince').mockReturnValue(0);

        for (let i = 0; i < expectedChunkCount; i++) {
            $httpBackend
                .expectPUT(`${window.ENDPOINT_ROOT}/api/cohort_applications/batch_update.json`, {
                    records: chunks[i],
                    http_queue: {
                        queued_for_seconds: 0,
                        retry: false,
                        attempt_count: 1,
                    },
                })
                .respond(code, response, responseConfig);
        }

        SpecHelper.click(elem, '[name="save"]');
        SpecHelper.expectNoElement(elem, '[name="save"]');
        SpecHelper.expectElement(elem, 'front-royal-spinner');
        $httpBackend.flush();
        SpecHelper.expectElement(elem, '[name="save"]');
        SpecHelper.expectNoElement(elem, 'front-royal-spinner');
        expect(scope.hasChanges).toBe(false);
    }

    function renderAndFlush(_students) {
        render(_students);
        $timeout.flush();
        expect(CareerProfile.index).toHaveBeenCalledWith(expectedIndexParams);
    }

    function render(_students) {
        students = _students || [
            CareerProfile.fixtures.getInstance({
                cohort_status: 'accepted',
                cohort_slack_room_id: cohort.slack_rooms[0].id,
                last_application_id: guid.generate(),
                name: 'A',
            }),
            CareerProfile.fixtures.getInstance({
                cohort_status: 'pre_accepted',
                cohort_slack_room_id: null,
                last_application_id: guid.generate(),
                name: 'B',
            }),
            CareerProfile.fixtures.getInstance({
                cohort_status: 'pre_accepted',
                cohort_slack_room_id: null,
                last_application_id: guid.generate(),
                name: 'C',
            }),
        ];
        cohort.addSlackRoomAssignment(
            _.map(students, student => ({
                user_id: student.user_id,
                id: student.last_application_id,
            })),
        );
        renderer = SpecHelper.renderer();
        renderer.scope.cohort = cohort;
        renderer.render('<admin-cohort-slack-assignments cohort="cohort"></admin-cohort-slack-assignments>');
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    describe('indexParams', () => {
        it('should filter for registered users when the cohort supports payment', () => {
            jest.spyOn(cohort, 'supportsPayments', 'get').mockReturnValue(true);
            expectedIndexParams.filters.registered = true;
            renderAndFlush();
        });
    });

    describe('slack room', () => {
        it('should allow for setting the slack room on a pre_accepted student', () => {
            renderAndFlush();
            expect(scope.proxy.students[1].cohort_status).toBe('pre_accepted');
            expect(scope.proxy.students[1].cohort_slack_room_id).toBe(null);
            SpecHelper.updateSelect(slackRoomCell(1), 'select', cohort.slack_rooms[0].id);
            expect(scope.proxy.students[1].cohort_slack_room_id).toEqual(cohort.slack_rooms[0].id);
        });

        it('should not allow for setting the slack room on an accepted student who already has a slack room', () => {
            renderAndFlush();
            expect(scope.proxy.students[0].cohort_status).toBe('accepted');
            expect(scope.proxy.students[0].cohort_slack_room_id).toBe(cohort.slack_rooms[0].id);
            SpecHelper.expectNoElement(slackRoomCell(0), 'select');
            expect(slackRoomCell(0).text().trim()).toEqual(cohort.slack_rooms[0].title);
        });
    });

    describe('columns', () => {
        it('should include full scholarship column only if supportsScholarshipLevels', () => {
            renderAndFlush();
            expect(scope.cohort.supportsScholarshipLevels).toBe(false);
            expect(_.chain(scope.columns).pluck('prop').contains('has_full_scholarship').value()).toBe(false);

            jest.spyOn(cohort, 'supportsScholarshipLevels', 'get').mockReturnValue(true);
            renderAndFlush();
            expect(_.chain(scope.columns).pluck('prop').contains('has_full_scholarship').value()).toBe(true);
        });
    });

    describe('buttons', () => {
        it('should be disabled at the right times', () => {
            renderAndFlush();
            SpecHelper.expectElementDisabled(elem, '[name="save"]');
            SpecHelper.expectElementDisabled(elem, '[name="reset"]');
            SpecHelper.expectElementEnabled(elem, '[name="auto-assign"]');

            expect(scope.proxy.students[1].cohort_slack_room_id).toBe(null);
            SpecHelper.updateSelect(slackRoomCell(1), 'select', cohort.slack_rooms[0].id);
            SpecHelper.expectElementEnabled(elem, '[name="save"]');
            SpecHelper.expectElementEnabled(elem, '[name="reset"]');
            SpecHelper.expectElementEnabled(elem, '[name="auto-assign"]');
        });

        it('should have a working save button with just one change', () => {
            renderAndFlush();

            // only changed students get sent up in the request
            expect(students.length > 1).toBe(true);
            const studentToSave = students[0];
            studentToSave.cohort_slack_room_id = 'changed';

            save(200, {}, 1); // 1 chunk expected
        });

        it('should chunk based on batch size', () => {
            const _students = [];
            const batches = 2;
            const numStudents = (batches - 1) * 50 + 1; // add one batch plus an extra user
            for (let i = 0; i < numStudents; i++) {
                _students.push(
                    CareerProfile.fixtures.getInstance({
                        cohort_status: 'pre_accepted',
                        cohort_slack_room_id: null,
                        last_application_id: guid.generate(),
                        name: `${guid.generate()}`,
                    }),
                );
            }
            renderAndFlush(_students);

            // only changed students get sent up in the request; change them all!
            students.forEach(student => {
                student.cohort_slack_room_id = 'changed';
            });

            save(200, {}, batches);
        });

        it('should have a working reset button', () => {
            renderAndFlush();
            const student = students[0];
            const orig = student.cohort_slack_room_id;
            student.cohort_slack_room_id = 'changed';
            scope.hasChanges = true; // enable the button
            scope.$digest();

            SpecHelper.click(elem, '[name="reset"]');
            expect(student.cohort_slack_room_id).toEqual(orig);
            expect(scope.hasChanges).toBe(false);
        });

        it('actions that change data should reset stats', () => {
            renderAndFlush();
            scope.proxy.showStats = true;
            scope.hasChanges = true; // enable the buttons
            scope.$digest();
            const statsScope = elem.find('admin-cohort-slack-rooms').isolateScope();
            jest.spyOn(statsScope, 'reset').mockImplementation(() => {});

            SpecHelper.click(elem, '[name="auto-assign"]');
            expect(statsScope.reset).toHaveBeenCalled();
            statsScope.reset.mockClear();

            SpecHelper.click(elem, '[name="reset"]');
            expect(statsScope.reset).toHaveBeenCalled();
            statsScope.reset.mockClear();
        });

        it('should have a working auto-assign button', () => {
            renderAndFlush();
            const student = _.findWhere(scope.proxy.students, {
                cohort_slack_room_id: null,
            });
            const expectedSlackRoomId = _.detect(cohort.slack_room_assignment.slack_room_assignments, entry =>
                _.contains(entry.cohort_application_ids, student.last_application_id),
            ).slack_room_id;

            SpecHelper.click(elem, '[name="auto-assign"]');
            expect(student.cohort_slack_room_id).toEqual(expectedSlackRoomId);
            expect(scope.hasChanges).toBe(true);
        });

        it('should have a working toggle stats button', () => {
            const scrollHelper = $injector.get('scrollHelper');
            jest.spyOn(scrollHelper, 'scrollToTop').mockImplementation(() => {});
            renderAndFlush();
            SpecHelper.expectNoElement(elem, 'admin-cohort-slack-rooms');
            SpecHelper.expectElementNotHidden(elem, 'editable-things-list');

            SpecHelper.click(elem, '[name="toggle-stats"]');
            expect(scrollHelper.scrollToTop).toHaveBeenCalled();
            scrollHelper.scrollToTop.mockClear();
            const slackRoomDir = SpecHelper.expectElement(elem, 'admin-cohort-slack-rooms');
            SpecHelper.expectElementHidden(elem, 'editable-things-list');
            expect(slackRoomDir.isolateScope().cohort).toBe(scope.cohort);
            expect(slackRoomDir.isolateScope().students).toBe(scope.proxy.students);

            SpecHelper.click(elem, '[name="toggle-stats"]');
            expect(scrollHelper.scrollToTop).toHaveBeenCalled();
            SpecHelper.expectNoElement(elem, 'admin-cohort-slack-rooms');
            SpecHelper.expectElementNotHidden(elem, 'editable-things-list');
        });
    });

    function slackRoomCell(i) {
        return elem.find(`editable-things-list tbody tr:eq(${i}) [data-id="cohort_slack_room_id"]`);
    }
});
