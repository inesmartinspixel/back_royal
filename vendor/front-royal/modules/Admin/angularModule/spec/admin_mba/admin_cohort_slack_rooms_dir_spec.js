import 'AngularSpecHelper';
import 'Admin/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';

describe('Admin::AdminCohortSlackRooms', () => {
    let renderer;
    let elem;
    let SpecHelper;
    let Cohort;
    let cohort;
    let CareerProfile;
    let students;
    let $injector;
    let supportsScholarshipLevels;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Admin', 'SpecHelper');

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            SpecHelper = $injector.get('SpecHelper');
            Cohort = $injector.get('Cohort');
            CareerProfile = $injector.get('CareerProfile');
            $injector.get('CareerProfileFixtures');

            $injector.get('CohortFixtures');
        });
        supportsScholarshipLevels = true;
        cohort = Cohort.fixtures.getInstance();

        SpecHelper.stubCurrentUser('admin');

        const guid = $injector.get('guid');
        students = [
            CareerProfile.fixtures.getInstance({
                cohort_status: 'accepted',
                cohort_slack_room_id: cohort.slack_rooms[0].id,
                last_application_id: guid.generate(),
                name: 'A',
            }),
            CareerProfile.fixtures.getInstance({
                cohort_status: 'pre_accepted',
                cohort_slack_room_id: cohort.slack_rooms[1].id,
                last_application_id: guid.generate(),
                name: 'B',
            }),
            CareerProfile.fixtures.getInstance({
                cohort_status: 'pre_accepted',
                cohort_slack_room_id: cohort.slack_rooms[0].id,
                last_application_id: guid.generate(),
                name: 'C',
            }),
            CareerProfile.fixtures.getInstance({
                cohort_status: 'pre_accepted',
                cohort_slack_room_id: cohort.slack_rooms[0].id,
                last_application_id: guid.generate(),
                name: 'D',
            }),
        ];

        cohort.addSlackRoomAssignment(
            _.map(students, student => ({
                user_id: student.user_id,
                id: student.last_application_id,
            })),
        );
        jest.spyOn(cohort, 'supportsScholarshipLevels', 'get').mockImplementation(() => supportsScholarshipLevels);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.cohort = cohort;
        renderer.scope.students = students;
        renderer.render('<admin-cohort-slack-rooms cohort="cohort" students="students"></admin-cohort-slack-rooms>');
        elem = renderer.elem;
    }

    it('should show gender info', () => {
        students[0].sex = 'male';
        students[1].sex = 'female';
        students[2].sex = 'no_identify';
        students[3].sex = 'no_identify';

        render();
        let headerRow;
        elem.find('tr').each(function () {
            const tr = $(this);
            if (tr.hasClass('feature-header') && tr.text().trim() === 'Gender') {
                headerRow = tr;
            }
        });

        const maleRow = headerRow.next();
        expect(maleRow.find('th').text()).toEqual('Male');
        expect(SpecHelper.trimText(maleRow.find('td').eq(0).text())).toEqual('1');
        expect(SpecHelper.trimText(maleRow.find('td').eq(1).text())).toEqual('0');
        const femaleRow = maleRow.next();
        expect(femaleRow.find('th').text()).toEqual('Female');
        expect(SpecHelper.trimText(femaleRow.find('td').eq(0).text())).toEqual('0');
        expect(SpecHelper.trimText(femaleRow.find('td').eq(1).text())).toEqual('1');
        const unknownRow = femaleRow.next();
        expect(unknownRow.find('th').text()).toEqual('Unknown');
        expect(SpecHelper.trimText(unknownRow.find('td').eq(0).text())).toEqual('2');
        expect(SpecHelper.trimText(unknownRow.find('td').eq(1).text())).toEqual('0');
    });

    it('should show full scholarship info', () => {
        students[0].has_full_scholarship = true;
        students[1].has_full_scholarship = false;
        students[2].has_full_scholarship = true;
        students[3].has_full_scholarship = false;

        render();
        let headerRow;
        elem.find('tr').each(function () {
            const tr = $(this);
            if (tr.hasClass('feature-header') && tr.text().trim() === 'Scholarship') {
                headerRow = tr;
            }
        });

        const trueRow = headerRow.next();
        expect(trueRow.find('th').text()).toEqual('Full Scholarship');
        expect(SpecHelper.trimText(trueRow.find('td').eq(0).text())).toEqual('2');
        expect(SpecHelper.trimText(trueRow.find('td').eq(1).text())).toEqual('0');
        const falseRow = trueRow.next();
        expect(falseRow.find('th').text()).toEqual('Other');
        expect(SpecHelper.trimText(falseRow.find('td').eq(0).text())).toEqual('1');
        expect(SpecHelper.trimText(falseRow.find('td').eq(1).text())).toEqual('1');
    });

    it('should hide scholarship info for a cohort that does not support scholarships', () => {
        supportsScholarshipLevels = false;
        render();
        let headerRow;
        elem.find('tr').each(function () {
            const tr = $(this);
            if (tr.hasClass('feature-header') && tr.text().trim() === 'Scholarship') {
                headerRow = tr;
            }
        });
        expect(headerRow).toBeUndefined();
    });

    it('should show location info', () => {
        // These locations come from addSlackRoomAssignment() defined in cohorts.js fixture file
        const counts = {
            'Canada, (all cities)': {},
            'United States, San Jose, CA': {},
            'United States, (other cities)': {},
            'Other Countries, ': {},
        };

        _.each(cohort.slack_rooms, slackRoom => {
            counts['Canada, (all cities)'][slackRoom.id] = 0;
            counts['United States, San Jose, CA'][slackRoom.id] = 0;
            counts['United States, (other cities)'][slackRoom.id] = 0;
            counts['Other Countries, '][slackRoom.id] = 0;
        });

        students.forEach(student => {
            const location = cohort.slack_room_assignment.location_assignments[student.user_id].join(', ');
            counts[location][student.cohort_slack_room_id] += 1;
        });

        render();
        let headerRow;
        elem.find('tr').each(function () {
            const tr = $(this);
            if (tr.hasClass('feature-header') && tr.text().trim() === 'Locations') {
                headerRow = tr;
            }
        });

        let row = headerRow.next();
        let location = 'Canada, (all cities)';
        expect(SpecHelper.trimText(row.find('th').eq(0).text())).toEqual('Canada');
        expect(SpecHelper.trimText(row.find('th').eq(1).text())).toEqual('(all cities)');
        expect(SpecHelper.trimText(row.find('td').eq(0).text())).toEqual(
            counts[location][cohort.slack_rooms[0].id].toString(),
        );
        expect(SpecHelper.trimText(row.find('td').eq(1).text())).toEqual(
            counts[location][cohort.slack_rooms[1].id].toString(),
        );
        row = row.next();
        location = 'United States, San Jose, CA';
        expect(SpecHelper.trimText(row.find('th').eq(0).text())).toEqual('United States');
        expect(SpecHelper.trimText(row.find('th').eq(1).text())).toEqual('San Jose, CA');
        expect(SpecHelper.trimText(row.find('td').eq(0).text())).toEqual(
            counts[location][cohort.slack_rooms[0].id].toString(),
        );
        expect(SpecHelper.trimText(row.find('td').eq(1).text())).toEqual(
            counts[location][cohort.slack_rooms[1].id].toString(),
        );
        row = row.next();
        location = 'United States, (other cities)';
        expect(SpecHelper.trimText(row.find('th').eq(0).text())).toEqual('United States');
        expect(SpecHelper.trimText(row.find('th').eq(1).text())).toEqual('(other cities)');
        expect(SpecHelper.trimText(row.find('td').eq(0).text())).toEqual(
            counts[location][cohort.slack_rooms[0].id].toString(),
        );
        expect(SpecHelper.trimText(row.find('td').eq(1).text())).toEqual(
            counts[location][cohort.slack_rooms[1].id].toString(),
        );
        row = row.next();
        location = 'Other Countries, ';
        expect(SpecHelper.trimText(row.find('th').eq(0).text())).toEqual('Other Countries');
        expect(SpecHelper.trimText(row.find('th').eq(1).text())).toEqual('');
        expect(SpecHelper.trimText(row.find('td').eq(0).text())).toEqual(
            counts[location][cohort.slack_rooms[0].id].toString(),
        );
        expect(SpecHelper.trimText(row.find('td').eq(1).text())).toEqual(
            counts[location][cohort.slack_rooms[1].id].toString(),
        );
    });
});
