import 'AngularSpecHelper';
import 'Admin/angularModule';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohort_applications';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';
import 'Users/angularModule/spec/_mock/fixtures/users';

describe('Admin::AdminCohortEnrollmentStatus', () => {
    let $injector;
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let Cohort;
    let cohort;
    let User;
    let students;
    let CohortApplication;
    let $timeout;
    let DialogModal;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Admin', 'SpecHelper');

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            SpecHelper = $injector.get('SpecHelper');
            Cohort = $injector.get('Cohort');
            User = $injector.get('User');
            CohortApplication = $injector.get('CohortApplication');
            $timeout = $injector.get('$timeout');
            DialogModal = $injector.get('DialogModal');

            $injector.get('CohortFixtures');
            $injector.get('UserFixtures');
            $injector.get('CohortApplicationFixtures');
        });

        cohort = Cohort.fixtures.getInstance({
            id_verification_periods: [
                {
                    start_date_days_offset: 10,
                    due_date_days_offset: 20,
                },
                {
                    start_date_days_offset: 20,
                    due_date_days_offset: 30,
                },
            ],
        });
        students = [
            User.fixtures.getInstance({
                cohort_applications: [
                    getCohortApplication({
                        status: 'accepted',
                    }),
                ],
            }),
            User.fixtures.getInstance({
                cohort_applications: [
                    getCohortApplication({
                        status: 'accepted',
                    }),
                ],
            }),
            User.fixtures.getInstance({
                cohort_applications: [
                    getCohortApplication({
                        status: 'pre_accepted',
                    }),
                ],
            }),
            User.fixtures.getInstance({
                cohort_applications: [
                    getCohortApplication({
                        status: 'pre_accepted',
                    }),
                ],
            }),
        ];
    });

    function getCohortApplication(opts = {}) {
        return CohortApplication.fixtures.getInstance(
            _.extend(
                {
                    cohort_id: cohort.id,
                },
                opts,
            ),
        );
    }

    function render() {
        User.expect('index')
            .toBeCalledWith({
                filters: {
                    cohort_id: cohort.id,
                    cohort_status: ['accepted', 'pre_accepted'],
                },
                select_career_profile: true,
                select_education_experiences: true,
                select_signable_documents: true,
                select_transcripts: true,
                per_page: Number.MAX_SAFE_INTEGER,
            })
            .returns(students);
        renderer = SpecHelper.renderer();
        renderer.scope.cohort = cohort;
        renderer.render('<admin-cohort-enrollment-status cohort="cohort"></admin-cohort-enrollment-status>');
        elem = renderer.elem;
        scope = elem.isolateScope();
        $timeout.flush(); // get the editable-things-list to render
        User.flush('index');
    }

    describe('csvExportColunns', () => {
        it('should not include save column', () => {
            render();
            expect(_.pluck(scope.columns, 'id')).toContain('editorAbilities');
            expect(_.pluck(scope.csvExportColunns, 'id')).not.toContain('editorAbilities');
        });
    });

    describe('idVerificationPeriod column', () => {
        let columnConfig;
        let student;

        beforeEach(() => {
            render();
            columnConfig = _.find(scope.columns, column => column.id === 'verification-period-0');
            student = scope.adminCohortStudentsTableHelper.students[0];
        });

        it('should be present for each id_verification_period', () => {
            SpecHelper.expectElement(elem, 'th[data-id="verification-period-0"]');
            SpecHelper.expectElement(elem, 'th[data-id="verification-period-1"]');
        });

        describe('when no userIdVerification exists', () => {
            it('should show an unchecked checkbox', () => {
                jest.spyOn(columnConfig.callbacks, 'userIdVerification').mockReturnValue(undefined);
                scope.$digest();

                SpecHelper.expectCheckboxUnchecked(
                    elem,
                    `editable-things-list tbody tr[data-id="${student.id}"] td[data-id="${columnConfig.id}"] input[type="checkbox"]`,
                );
            });

            it('should disable checkbox when isSaving', () => {
                SpecHelper.expectElementEnabled(
                    elem,
                    `editable-things-list tbody tr[data-id="${student.id}"] td[data-id="${columnConfig.id}"] input[type="checkbox"]`,
                );
                jest.spyOn(columnConfig.callbacks, 'isSaving').mockReturnValue(true);
                scope.$digest();
                SpecHelper.expectElementDisabled(
                    elem,
                    `editable-things-list tbody tr[data-id="${student.id}"] td[data-id="${columnConfig.id}"] input[type="checkbox"]`,
                );
            });
        });

        describe('when userIdVerification exists', () => {
            describe('when userIdVerification verification_method is verified_by_idology', () => {
                beforeEach(() => {
                    const mockUserIdVerification = {
                        verification_method: 'verified_by_idology',
                    };
                    jest.spyOn(columnConfig.callbacks, 'userIdVerification').mockReturnValue(mockUserIdVerification);
                    scope.$digest();
                });

                it('should show ID icon instead of checkbox', () => {
                    expect(columnConfig.callbacks.userIdVerification).toHaveBeenCalledWith(
                        scope.adminCohortStudentsTableHelper.students[0],
                    );
                    SpecHelper.expectNoElement(
                        elem,
                        `editable-things-list tbody tr[data-id="${student.id}"] td[data-id="${columnConfig.id}"] input[type="checkbox"]`,
                    );
                    SpecHelper.expectElement(
                        elem,
                        `editable-things-list tbody tr[data-id="${student.id}"] td[data-id="${columnConfig.id}"] .fa-id-card`,
                    );
                });
            });

            describe('when userIdVerification verification_method is waived_due_to_late_enrollment', () => {
                beforeEach(() => {
                    const mockUserIdVerification = {
                        verification_method: 'waived_due_to_late_enrollment',
                    };
                    jest.spyOn(columnConfig.callbacks, 'userIdVerification').mockReturnValue(mockUserIdVerification);
                    scope.$digest();
                });

                it('should show ID icon instead of checkbox', () => {
                    expect(columnConfig.callbacks.userIdVerification).toHaveBeenCalledWith(
                        scope.adminCohortStudentsTableHelper.students[0],
                    );
                    SpecHelper.expectNoElement(
                        elem,
                        `editable-things-list tbody tr[data-id="${student.id}"] td[data-id="${columnConfig.id}"] input[type="checkbox"]`,
                    );
                    SpecHelper.expectElement(
                        elem,
                        `editable-things-list tbody tr[data-id="${student.id}"] td[data-id="${columnConfig.id}"] .fa-history`,
                    );
                });
            });

            describe('when userIdVerification verification_method is verified_by_admin', () => {
                let mockUserIdVerification;

                beforeEach(() => {
                    mockUserIdVerification = {
                        verification_method: 'verified_by_admin',
                    };
                    jest.spyOn(columnConfig.callbacks, 'userIdVerification').mockReturnValue(mockUserIdVerification);
                    scope.$digest();

                    expect(columnConfig.callbacks.userIdVerification).toHaveBeenCalledWith(
                        scope.adminCohortStudentsTableHelper.students[0],
                    );
                });

                it('should show checked checkbox instead of ID icon', () => {
                    SpecHelper.expectNoElement(
                        elem,
                        `editable-things-list tbody tr[data-id="${student.id}"] td[data-id="${columnConfig.id}"] .fa-id-card`,
                    );
                    SpecHelper.expectCheckboxChecked(
                        elem,
                        `editable-things-list tbody tr[data-id="${student.id}"] td[data-id="${columnConfig.id}"] input[type="checkbox"]`,
                    );
                });

                it('should disable checkbox while isSaving', () => {
                    SpecHelper.expectElementEnabled(
                        elem,
                        `editable-things-list tbody tr[data-id="${student.id}"] td[data-id="${columnConfig.id}"] input[type="checkbox"]`,
                    );
                    jest.spyOn(columnConfig.callbacks, 'isSaving').mockReturnValue(true);
                    scope.$digest();
                    SpecHelper.expectElementDisabled(
                        elem,
                        `editable-things-list tbody tr[data-id="${student.id}"] td[data-id="${columnConfig.id}"] input[type="checkbox"]`,
                    );
                });

                it('should disable checkbox if userIdVerification record has already been verified', () => {
                    SpecHelper.expectElementEnabled(
                        elem,
                        `editable-things-list tbody tr[data-id="${student.id}"] td[data-id="${columnConfig.id}"] input[type="checkbox"]`,
                    );

                    mockUserIdVerification.verified_at = new Date().getTime() / 1000;
                    scope.$digest();

                    SpecHelper.expectElementDisabled(
                        elem,
                        `editable-things-list tbody tr[data-id="${student.id}"] td[data-id="${columnConfig.id}"] input[type="checkbox"]`,
                    );
                });
            });
        });

        describe('config', () => {
            it('should have the verificationPeriod attached to it', () => {
                scope.cohort.id_verification_periods.forEach((verificationPeriod, i) => {
                    columnConfig = _.find(scope.columns, column => column.id === `verification-period-${i}`);
                    expect(columnConfig.verificationPeriod).toBe(verificationPeriod);
                });
            });

            it('should have isIdVerificationPeriodColumn flag set', () => {
                for (let i = 0; i < scope.cohort.id_verification_periods.length; i++) {
                    columnConfig = _.find(scope.columns, column => column.id === `verification-period-${i}`);
                    expect(columnConfig.isIdVerificationPeriodColumn).toBe(true);
                }
            });

            describe('userIdVerification callback', () => {
                it('should get userIdVerificationForPeriod for passed in user', () => {
                    const mockUserIdVerification = {};
                    jest.spyOn(student, 'userIdVerificationForPeriod').mockReturnValue(mockUserIdVerification);
                    for (let i = 0; i < scope.cohort.id_verification_periods.length; i++) {
                        columnConfig = _.find(scope.columns, column => column.id === `verification-period-${i}`);
                        const userIdVerification = columnConfig.callbacks.userIdVerification(student);
                        expect(student.userIdVerificationForPeriod).toHaveBeenCalledWith(
                            columnConfig.verificationPeriod,
                        );
                        expect(userIdVerification).toBe(mockUserIdVerification);
                    }
                });
            });

            describe('isSaving callback', () => {
                it('should derive its return value from adminCohortStudentsTableHelper.saving', () => {
                    scope.adminCohortStudentsTableHelper.saving = true;
                    for (let i = 0; i < scope.cohort.id_verification_periods.length; i++) {
                        columnConfig = _.find(scope.columns, column => column.id === `verification-period-${i}`);
                        expect(columnConfig.callbacks.isSaving()).toBe(true);
                    }

                    scope.adminCohortStudentsTableHelper.saving = false;
                    for (let i = 0; i < scope.cohort.id_verification_periods.length; i++) {
                        columnConfig = _.find(scope.columns, column => column.id === `verification-period-${i}`);
                        expect(columnConfig.callbacks.isSaving()).toBe(false);
                    }
                });
            });

            describe('getFormattedColumnValueForCSV callback', () => {
                it('should attempt to return the verification_method for the userIdVerificationForPeriod', () => {
                    jest.spyOn(student, 'userIdVerificationForPeriod').mockReturnValue({
                        verification_method: 'some_verification_method',
                    });
                    for (let i = 0; i < scope.cohort.id_verification_periods.length; i++) {
                        columnConfig = _.find(scope.columns, column => column.id === `verification-period-${i}`);
                        expect(columnConfig.callbacks.getFormattedColumnValueForCSV(student)).toEqual(
                            'some_verification_method',
                        );
                        expect(student.userIdVerificationForPeriod).toHaveBeenCalledWith(
                            columnConfig.verificationPeriod,
                        );
                    }
                });
            });

            describe('sort callback', () => {
                it('should return 0 if no userIdVerificationForPeriod is found', () => {
                    jest.spyOn(student, 'userIdVerificationForPeriod').mockReturnValue(undefined);
                    for (let i = 0; i < scope.cohort.id_verification_periods.length; i++) {
                        columnConfig = _.find(scope.columns, column => column.id === `verification-period-${i}`);
                        expect(columnConfig.callbacks.sort(student)).toEqual(0);
                        expect(student.userIdVerificationForPeriod).toHaveBeenCalledWith(
                            columnConfig.verificationPeriod,
                        );
                    }
                });

                it('should return 0 if userIdVerificationForPeriod has no been verified yet', () => {
                    jest.spyOn(student, 'userIdVerificationForPeriod').mockReturnValue({
                        verified_at: null,
                    });
                    for (let i = 0; i < scope.cohort.id_verification_periods.length; i++) {
                        columnConfig = _.find(scope.columns, column => column.id === `verification-period-${i}`);
                        expect(columnConfig.callbacks.sort(student)).toEqual(0);
                        expect(student.userIdVerificationForPeriod).toHaveBeenCalledWith(
                            columnConfig.verificationPeriod,
                        );
                    }
                });

                describe('when userIdVerificationForPeriod has been verified', () => {
                    beforeEach(() => {
                        jest.spyOn(student, 'userIdVerificationForPeriod').mockReturnValue({
                            verified_at: new Date().getTime() / 1000,
                            verification_method: 'some_verification_method',
                        });
                    });

                    it('should return the verification_method for the userIdVerificationForPeriod', () => {
                        for (let i = 0; i < scope.cohort.id_verification_periods.length; i++) {
                            columnConfig = _.find(scope.columns, column => column.id === `verification-period-${i}`);
                            expect(columnConfig.callbacks.sort(student)).toEqual('some_verification_method');
                            expect(student.userIdVerificationForPeriod).toHaveBeenCalledWith(
                                columnConfig.verificationPeriod,
                            );
                        }
                    });
                });
            });
        });
    });

    describe('Uploaded ID? column', () => {
        it('should be present if cohort requiresIdUpload', () => {
            jest.spyOn(cohort, 'requiresIdUpload', 'get').mockReturnValue(true);
            render();
            const uploadedIDColumn = _.findWhere(scope.columns, {
                id: 'has-uploaded-identification',
            });
            expect(uploadedIDColumn).toEqual({
                $$hashKey: expect.anything(),
                id: 'has-uploaded-identification',
                prop: 'hasUploadedIdentification',
                type: 'checkIfTrue',
                label: 'Uploaded ID?',
            });
        });

        it('should not be present if cohort !requiresIdUpload', () => {
            jest.spyOn(cohort, 'requiresIdUpload', 'get').mockReturnValue(false);
            render();
            const uploadedIDColumn = _.findWhere(scope.columns, {
                id: 'has-uploaded-identification',
            });
            expect(uploadedIDColumn).toBeUndefined();
        });
    });

    describe('Identity Verified? column', () => {
        const specColumnConfig = {
            columnId: 'identity-verified',
            columnProp: 'identifiedForEnrollment',
        };

        it(`should show a checkmark if user is ${specColumnConfig.columnProp}`, () => {
            render();
            const firstStudentPropSpy = jest
                .spyOn(scope.adminCohortStudentsTableHelper.students[0], specColumnConfig.columnProp, 'get')
                .mockReturnValue(true);
            const secondStudentPropSpy = jest
                .spyOn(scope.adminCohortStudentsTableHelper.students[1], specColumnConfig.columnProp, 'get')
                .mockReturnValue(false);
            scope.$digest();

            SpecHelper.expectElement(
                elem,
                `editable-things-list tbody tr:eq(0) td[data-id="${specColumnConfig.columnId}"] .fa-check`,
            );
            SpecHelper.expectNoElement(
                elem,
                `editable-things-list tbody tr:eq(1) td[data-id="${specColumnConfig.columnId}"] .fa-check`,
            );

            firstStudentPropSpy.mockReturnValue(false);
            secondStudentPropSpy.mockReturnValue(true);
            scope.$digest();

            SpecHelper.expectNoElement(
                elem,
                `editable-things-list tbody tr:eq(0) td[data-id="${specColumnConfig.columnId}"] .fa-check`,
            );
            SpecHelper.expectElement(
                elem,
                `editable-things-list tbody tr:eq(1) td[data-id="${specColumnConfig.columnId}"] .fa-check`,
            );
        });
    });

    _.each(
        [
            {
                columnId: 'has-uploaded-english-language-proficiency-documents',
                docsVerifiedProp: 'english_language_proficiency_documents_approved',
                hasUploadedDocsProp: 'hasUploadedEnglishLanguageProficiencyDocuments',
                indicatesUserShouldUploadProp:
                    'careerProfileIndicatesUserShouldUploadEnglishLanguageProficiencyDocuments',
            },
        ],
        specColumnConfig => {
            describe(`${specColumnConfig.columnId} column`, () => {
                let columnConfig;

                beforeEach(() => {
                    render();
                    columnConfig = _.find(scope.columns, column => column.id === specColumnConfig.columnId);
                });

                it('should show a checkmark if isVisible column callback returns true', () => {
                    jest.spyOn(columnConfig.callbacks, 'isVisible').mockReturnValue(true);
                    scope.$digest();
                    SpecHelper.expectElement(
                        elem,
                        `editable-things-list tbody tr:eq(0) .${specColumnConfig.columnId} .fa-check`,
                    );
                });

                describe('config', () => {
                    let student;
                    let hasUploadedDocsPropSpy;
                    let indicatesUserShouldUploadPropSpy;

                    beforeEach(() => {
                        student = scope.adminCohortStudentsTableHelper.students[0];
                        hasUploadedDocsPropSpy = jest.spyOn(student, specColumnConfig.hasUploadedDocsProp, 'get');
                        indicatesUserShouldUploadPropSpy = jest.spyOn(
                            student,
                            specColumnConfig.indicatesUserShouldUploadProp,
                            'get',
                        );
                    });

                    describe('isVisible callback', () => {
                        it(`should be true if user.${specColumnConfig.hasUploadedDocsProp}`, () => {
                            hasUploadedDocsPropSpy.mockReturnValue(true);
                            expect(columnConfig.callbacks.isVisible(student)).toBe(true);
                        });

                        it(`should be true if !user.${specColumnConfig.indicatesUserShouldUploadProp}`, () => {
                            indicatesUserShouldUploadPropSpy.mockReturnValue(false);
                            expect(columnConfig.callbacks.isVisible(student)).toBe(true);
                        });

                        it(`should be false if !user.${specColumnConfig.hasUploadedDocsProp} and user.${specColumnConfig.indicatesUserShouldUploadProp}`, () => {
                            hasUploadedDocsPropSpy.mockReturnValue(false);
                            indicatesUserShouldUploadPropSpy.mockReturnValue(true);
                            expect(columnConfig.callbacks.isVisible(student)).toBe(false);
                        });
                    });

                    describe('isDisabled callback', () => {
                        it(`should be true if !user.${specColumnConfig.docsVerifiedProp} and !user.${specColumnConfig.indicatesUserShouldUploadProp}`, () => {
                            student[specColumnConfig.docsVerifiedProp] = false;
                            indicatesUserShouldUploadPropSpy.mockReturnValue(false);
                            expect(columnConfig.callbacks.isDisabled(student)).toBe(true);
                        });

                        it(`should be false if user.${specColumnConfig.docsVerifiedProp}`, () => {
                            student[specColumnConfig.docsVerifiedProp] = true;
                            expect(columnConfig.callbacks.isDisabled(student)).toBe(false);
                        });

                        it(`should be false if !user.${specColumnConfig.docsVerifiedProp} and user.${specColumnConfig.indicatesUserShouldUploadProp}`, () => {
                            student[specColumnConfig.docsVerifiedProp] = false;
                            indicatesUserShouldUploadPropSpy.mockReturnValue(true);
                            expect(columnConfig.callbacks.isDisabled(student)).toBe(false);
                        });
                    });

                    describe('sort callback', () => {
                        it(`should return 1 if user.${specColumnConfig.hasUploadedDocsProp}`, () => {
                            hasUploadedDocsPropSpy.mockReturnValue(false);
                            expect(columnConfig.callbacks.sort(student)).toEqual(0);
                            hasUploadedDocsPropSpy.mockReturnValue(true);
                            expect(columnConfig.callbacks.sort(student)).toEqual(1);
                        });
                    });

                    describe('getFormattedColumnValueForCSV callback', () => {
                        it(`should return true if user.${specColumnConfig.hasUploadedDocsProp}`, () => {
                            hasUploadedDocsPropSpy.mockReturnValue(false);
                            expect(columnConfig.callbacks.getFormattedColumnValueForCSV(student)).not.toBe(true);
                            hasUploadedDocsPropSpy.mockReturnValue(true);
                            expect(columnConfig.callbacks.getFormattedColumnValueForCSV(student)).toBe(true);
                        });

                        it(`should return 'Not required' if !user.${specColumnConfig.hasUploadedDocsProp} but !user.${specColumnConfig.indicatesUserShouldUploadProp}`, () => {
                            const expectedValue = 'Not required';
                            hasUploadedDocsPropSpy.mockReturnValue(false);
                            indicatesUserShouldUploadPropSpy.mockReturnValue(true);
                            expect(columnConfig.callbacks.getFormattedColumnValueForCSV(student)).not.toEqual(
                                expectedValue,
                            );
                            indicatesUserShouldUploadPropSpy.mockReturnValue(false);
                            expect(columnConfig.callbacks.getFormattedColumnValueForCSV(student)).toEqual(
                                expectedValue,
                            );
                        });

                        it(`should return false if !user.${specColumnConfig.hasUploadedDocsProp} and user.${specColumnConfig.indicatesUserShouldUploadProp}`, () => {
                            hasUploadedDocsPropSpy.mockReturnValue(false);
                            indicatesUserShouldUploadPropSpy.mockReturnValue(false);
                            expect(columnConfig.callbacks.getFormattedColumnValueForCSV(student)).not.toEqual(false);
                            indicatesUserShouldUploadPropSpy.mockReturnValue(true);
                            expect(columnConfig.callbacks.getFormattedColumnValueForCSV(student)).toEqual(false);
                        });
                    });

                    describe('emptyToEnd callback', () => {
                        it('should be return whether the user is required to upload', () => {
                            indicatesUserShouldUploadPropSpy.mockReturnValue('value');
                            expect(columnConfig.callbacks.emptyToEnd(student)).toEqual('value');
                        });
                    });
                });

                describe('when isVisible', () => {
                    beforeEach(() => {
                        jest.spyOn(columnConfig.callbacks, 'isVisible').mockReturnValue(true);
                        scope.$digest();
                    });

                    it('should show a disabled checkmark if isDisabled column callback returns true', () => {
                        const isDisabledCallbackSpy = jest
                            .spyOn(columnConfig.callbacks, 'isDisabled')
                            .mockReturnValue(false);
                        scope.$digest();
                        SpecHelper.expectDoesNotHaveClass(
                            elem,
                            `editable-things-list tbody tr:eq(0) .${specColumnConfig.columnId}`,
                            'disabled',
                        );

                        isDisabledCallbackSpy.mockReturnValue(true);
                        scope.$digest();
                        SpecHelper.expectHasClass(
                            elem,
                            `editable-things-list tbody tr:eq(0) .${specColumnConfig.columnId}`,
                            'disabled',
                        );
                    });
                });
            });
        },
    );

    _.each(
        [
            {
                columnId: 'english-proficiency-documents-approved',
                docsVerifiedProp: 'english_language_proficiency_documents_approved',
                indicatesUserShouldUploadProp:
                    'careerProfileIndicatesUserShouldUploadEnglishLanguageProficiencyDocuments',
                originalValuesCache: 'originalEnglishLanguageProficiencyDocumentsApprovedValuesByStudentId',
            },
        ],
        specColumnConfig => {
            describe(`${specColumnConfig.columnId} column`, () => {
                let columnConfig;

                beforeEach(() => {
                    render();
                    columnConfig = _.find(scope.columns, _column => _column.id === specColumnConfig.columnId);
                });

                it('should show a checkbox', () => {
                    SpecHelper.expectElement(
                        elem,
                        `editable-things-list tbody tr:eq(0) input[type="checkbox"][name="${specColumnConfig.docsVerifiedProp}"]`,
                    );
                });

                describe('checkbox', () => {
                    it('should be checked if isChecked column callback returns true', () => {
                        jest.spyOn(columnConfig.callbacks, 'isChecked').mockReturnValue(false);
                        scope.$digest();
                        _.each(students, () => {
                            SpecHelper.expectCheckboxUnchecked(
                                elem,
                                `editable-things-list tbody tr:eq(0) input[type="checkbox"][name="${specColumnConfig.docsVerifiedProp}"]`,
                            );
                        });

                        columnConfig.callbacks.isChecked.mockReturnValue(true);
                        scope.$digest();
                        _.each(students, () => {
                            SpecHelper.expectCheckboxChecked(
                                elem,
                                `editable-things-list tbody tr:eq(0) input[type="checkbox"][name="${specColumnConfig.docsVerifiedProp}"]`,
                            );
                        });
                    });

                    it('should trigger onClick callback when clicked', () => {
                        jest.spyOn(columnConfig.callbacks, 'isDisabled').mockReturnValue(false); // ensure the checkbox is clickable
                        scope.$digest();
                        jest.spyOn(columnConfig.callbacks, 'onClick').mockImplementation(() => {});
                        SpecHelper.toggleCheckbox(
                            elem,
                            `editable-things-list tbody tr:eq(0) input[type="checkbox"][name="${specColumnConfig.docsVerifiedProp}"]`,
                        );
                        expect(columnConfig.callbacks.onClick).toHaveBeenCalledWith(
                            scope.adminCohortStudentsTableHelper.students[0],
                        );
                    });
                });

                describe('config', () => {
                    let student;
                    let indicatesUserShouldUploadPropSpy;

                    beforeEach(() => {
                        student = scope.adminCohortStudentsTableHelper.students[0];
                        indicatesUserShouldUploadPropSpy = jest.spyOn(
                            student,
                            specColumnConfig.indicatesUserShouldUploadProp,
                            'get',
                        );
                    });

                    describe('onClick callback', () => {
                        it('should set property on student.$$proxy appropriately', () => {
                            const originalValue = student[specColumnConfig.docsVerifiedProp];
                            expect(originalValue).toBeUndefined();

                            columnConfig.callbacks.onClick(student);
                            expect(student.$$proxy[specColumnConfig.docsVerifiedProp]).toEqual(!originalValue);
                            columnConfig.callbacks.onClick(student);

                            // check that the value gets set back to undefined rather than set to false
                            expect(student[specColumnConfig.docsVerifiedProp]).toEqual(originalValue);
                        });
                    });

                    describe('isChecked callback', () => {
                        it(`should return true if ${specColumnConfig.docsVerifiedProp}`, () => {
                            indicatesUserShouldUploadPropSpy.mockReturnValue(true);
                            student.$$proxy[specColumnConfig.docsVerifiedProp] = true;
                            expect(columnConfig.callbacks.isChecked(student)).toBe(true);
                        });

                        it(`should return true if student is not ${specColumnConfig.indicatesUserShouldUploadProp}`, () => {
                            indicatesUserShouldUploadPropSpy.mockReturnValue(false);
                            expect(columnConfig.callbacks.isChecked(student)).toBe(true);
                        });

                        it(`should return false if !${specColumnConfig.docsVerifiedProp} and ${specColumnConfig.indicatesUserShouldUploadProp}`, () => {
                            indicatesUserShouldUploadPropSpy.mockReturnValue(true);
                            expect(columnConfig.callbacks.isChecked(student)).toBe(false);
                        });
                    });

                    describe('isDisabled callback', () => {
                        it(`should be true if !user.${specColumnConfig.indicatesUserShouldUploadProp} and false otherwise`, () => {
                            indicatesUserShouldUploadPropSpy.mockReturnValue(false);
                            expect(columnConfig.callbacks.isDisabled(student)).toBe(true);
                            indicatesUserShouldUploadPropSpy.mockReturnValue(true);
                            expect(columnConfig.callbacks.isDisabled(student)).toBe(false);
                        });
                    });

                    describe('sort callback', () => {
                        it(`should return 1 if user.${specColumnConfig.docsVerifiedProp}`, () => {
                            student[specColumnConfig.docsVerifiedProp] = false;
                            expect(columnConfig.callbacks.sort(student)).toEqual(0);
                            student[specColumnConfig.docsVerifiedProp] = true;
                            expect(columnConfig.callbacks.sort(student)).toEqual(1);
                        });
                    });

                    describe('getFormattedColumnValueForCSV callback', () => {
                        it(`should return true if user.${specColumnConfig.docsVerifiedProp}`, () => {
                            student[specColumnConfig.docsVerifiedProp] = false;
                            expect(columnConfig.callbacks.getFormattedColumnValueForCSV(student)).not.toBe(true);
                            student[specColumnConfig.docsVerifiedProp] = true;
                            expect(columnConfig.callbacks.getFormattedColumnValueForCSV(student)).toBe(true);
                        });

                        it(`should return 'Not required' if !user.${specColumnConfig.docsVerifiedProp} but !user.${specColumnConfig.indicatesUserShouldUploadProp}`, () => {
                            const expectedValue = 'Not required';
                            student[specColumnConfig.docsVerifiedProp] = false;
                            indicatesUserShouldUploadPropSpy.mockReturnValue(true);
                            expect(columnConfig.callbacks.getFormattedColumnValueForCSV(student)).not.toEqual(
                                expectedValue,
                            );
                            indicatesUserShouldUploadPropSpy.mockReturnValue(false);
                            expect(columnConfig.callbacks.getFormattedColumnValueForCSV(student)).toEqual(
                                expectedValue,
                            );
                        });

                        it(`should return value of user.${specColumnConfig.docsVerifiedProp} if !user.${specColumnConfig.docsVerifiedProp} and user.${specColumnConfig.indicatesUserShouldUploadProp}`, () => {
                            student[specColumnConfig.docsVerifiedProp] = false;
                            indicatesUserShouldUploadPropSpy.mockReturnValue(true);
                            expect(columnConfig.callbacks.getFormattedColumnValueForCSV(student)).toBe(false);
                            student[specColumnConfig.docsVerifiedProp] = null;
                            expect(columnConfig.callbacks.getFormattedColumnValueForCSV(student)).toBeNull();
                        });
                    });

                    describe('emptyToEnd callback', () => {
                        it('should be return whether the user is required to upload', () => {
                            indicatesUserShouldUploadPropSpy.mockReturnValue('value');
                            expect(columnConfig.callbacks.emptyToEnd(student)).toEqual('value');
                        });
                    });
                });
            });
        },
    );

    describe('Agreement Signed? column', () => {
        it('should show download link if agreement has been signed', () => {
            render();
            const student = _.first(scope.adminCohortStudentsTableHelper.students);
            const cohortApplication = student.cohortApplicationsByCohortId[scope.cohort.id];
            const cell = SpecHelper.expectElement(
                elem,
                `editable-things-list tr[data-id=${student.id}] [data-id="has-signed-enrollment-agreement"]`,
            );

            jest.spyOn(cohortApplication, 'hasSignedEnrollmentAgreement', 'get').mockReturnValue(true);
            jest.spyOn(cohortApplication, 'downloadEnrollmentAgreement').mockImplementation();
            scope.$digest();

            // disable while saving
            scope.adminCohortStudentsTableHelper.saving = true;
            scope.$digest();
            SpecHelper.expectElementDisabled(cell, 'button');

            // clicking should download
            scope.adminCohortStudentsTableHelper.saving = false;
            scope.$digest();
            SpecHelper.click(cell, 'button');
            expect(cohortApplication.downloadEnrollmentAgreement).toHaveBeenCalled();
        });

        describe('uploadLink', () => {
            let student;
            let cell;
            let onDocumentSelect;
            let frontRoyalUpload;
            let $q;

            beforeEach(() => {
                frontRoyalUpload = $injector.get('frontRoyalUpload');
                $q = $injector.get('$q');
                render();
                student = _.first(scope.adminCohortStudentsTableHelper.students);
                cell = SpecHelper.expectElement(
                    elem,
                    `editable-things-list tr[data-id=${student.id}] [data-id="has-signed-enrollment-agreement"]`,
                );
                const columnConfig = _.find(scope.columns, column => column.id === 'has-signed-enrollment-agreement');
                onDocumentSelect = function (student, file, errFiles) {
                    columnConfig.callbacks.onDocumentSelect(student, file, errFiles);
                    scope.$digest();
                };
                jest.spyOn(DialogModal, 'alert');
            });

            it('should be disabled while saving', () => {
                scope.adminCohortStudentsTableHelper.saving = true;
                scope.$digest();
                SpecHelper.expectElementDisabled(cell, 'button');
            });

            it('should show an upload error', () => {
                const DialogModal = $injector.get('DialogModal');
                jest.spyOn(DialogModal, 'alert').mockImplementation();

                jest.spyOn(frontRoyalUpload, 'handleNgfSelect').mockImplementation(() =>
                    $q.reject({
                        message: 'You know you done messed up right?',
                    }),
                );

                onDocumentSelect(student, 'file', 'errFiles');
                $timeout.flush();
                expect(DialogModal.alert).toHaveBeenCalledWith({
                    content: 'You know you done messed up right?',
                });
            });

            it('should successfully upload a file', () => {
                let resolve;
                jest.spyOn(frontRoyalUpload, 'handleNgfSelect').mockImplementation((file, errFiles, fn) => {
                    const opts = fn(file);
                    return $q(_resolve => {
                        resolve = _resolve.bind(null, {
                            status: 200,
                            data: {
                                contents: {
                                    signable_documents: [
                                        {
                                            id: 'newDocument',
                                            signed_at: new Date().getTime() / 1000,
                                            document_type: opts.data.record.document_type,
                                            user_id: opts.data.record.user_id,
                                            metadata: opts.data.record.metadata,
                                        },
                                    ],
                                },
                            },
                        });
                    });
                });

                onDocumentSelect(student, 'file');

                // disabled button while uploading
                SpecHelper.expectElementDisabled(cell, 'button');

                // resolve and flush the upload
                resolve();
                $timeout.flush();
                expect(frontRoyalUpload.handleNgfSelect).toHaveBeenCalled();
                const doc = student.signable_documents[0];
                expect(doc.id).toEqual('newDocument');
                SpecHelper.expectNoElement(cell, '[name="upload"]');
                SpecHelper.expectElement(cell, '[name="download"]');
                SpecHelper.expectElementEnabled(cell, 'button');
            });
        });

        it('should show checkmark if the cohort is considered legacy', () => {
            render();

            // The index call loads accepted and pre_accepted students, but we default to only
            // showing accepted
            const acceptedStudents = _.filter(students, student => student.isAccepted);
            for (let i = 0; i < acceptedStudents.length; i++) {
                SpecHelper.expectNoElement(
                    elem,
                    `editable-things-list tr:eq(${i + 1}) [data-id="has-signed-enrollment-agreement"] .fa-check`,
                );
            }
            jest.spyOn(scope.cohort, 'isLegacyEnrollmentCohort', 'get').mockReturnValue(true);
            scope.$digest();
            for (let j = 0; j < acceptedStudents.length; j++) {
                SpecHelper.expectElement(
                    elem,
                    `editable-things-list  tr:eq(${j + 1}) [data-id="has-signed-enrollment-agreement"] .fa-check`,
                );
            }
        });

        describe('config', () => {
            let columnConfig;

            beforeEach(() => {
                render();
                columnConfig = _.find(scope.columns, column => column.id === 'has-signed-enrollment-agreement');
            });

            describe('getFormattedColumnValueForCSV callback', () => {
                it("should return 'Not required' if cohort is considered legacy", () => {
                    const student = _.first(scope.adminCohortStudentsTableHelper.students);
                    const isLegacyEnrollmentCohortSpy = jest
                        .spyOn(scope.cohort, 'isLegacyEnrollmentCohort', 'get')
                        .mockReturnValue(false);
                    expect(columnConfig.callbacks.getFormattedColumnValueForCSV(student)).not.toEqual('Not required');

                    isLegacyEnrollmentCohortSpy.mockReturnValue(true);
                    expect(columnConfig.callbacks.getFormattedColumnValueForCSV(student)).toEqual('Not required');
                });

                describe('when cohort is not considered legacy', () => {
                    beforeEach(() => {
                        jest.spyOn(scope.cohort, 'isLegacyEnrollmentCohort', 'get').mockReturnValue(false);
                    });

                    it("should return value of hasSignedEnrollmentAgreement flag on the student's (not the $$proxy's) cohort application for the cohort in question", () => {
                        const student = _.first(scope.adminCohortStudentsTableHelper.students);
                        const cohortApplication = student.cohortApplicationsByCohortId[scope.cohort.id];
                        jest.spyOn(cohortApplication, 'hasSignedEnrollmentAgreement', 'get').mockReturnValue(false);
                        expect(columnConfig.callbacks.getFormattedColumnValueForCSV(student)).toBe(false);

                        jest.spyOn(cohortApplication, 'hasSignedEnrollmentAgreement', 'get').mockReturnValue(true);
                        expect(columnConfig.callbacks.getFormattedColumnValueForCSV(student)).toBe(true);
                    });
                });
            });
        });
    });

    describe('saveAll button', () => {
        it('should work', () => {
            render();
            let disableSave = true;
            jest.spyOn(scope.adminCohortStudentsTableHelper, 'disableSave', 'get').mockImplementation(
                () => disableSave,
            );
            SpecHelper.expectElementDisabled(elem, 'button[name="save-all"]');
            disableSave = false;
            scope.$digest();
            jest.spyOn(scope.adminCohortStudentsTableHelper, 'saveAll').mockImplementation(() => {});
            SpecHelper.click(elem, 'button[name="save-all"]');
            expect(scope.adminCohortStudentsTableHelper.saveAll).toHaveBeenCalled();
        });
    });

    // https://trello.com/c/9FhiQuwT
    describe('filtering', () => {
        it('should not hide users as they are edited inline', () => {
            render();
            scope.clientFilters.missingRequiredDocuments = 'true';
            scope.$digest();
            const row = elem.find('editable-things-list tr:eq(1)');
            const userId = row.attr('data-id');
            expect(userId).not.toBeUndefined();

            row.find('input[type="checkbox"]').each(i => {
                SpecHelper.checkCheckbox(row, 'input[type="checkbox"]', i);
            });

            // make sure the row did not get filtered out
            SpecHelper.expectElement(elem, `[data-id="${userId}"]`);
        });
    });

    describe('legacy vs current enrollment handling', () => {
        it('should provide the legacy user fields for filtering if the cohort is legacy', () => {
            jest.spyOn(cohort, 'isLegacyEnrollmentCohort', 'get').mockReturnValue(true);
            render();

            expect(scope.missingRequiredDocumentsKey).toEqual('missingRequiredDocumentsLegacy');
            expect(scope.readyForApprovalKey).toEqual('readyForApprovalLegacy');

            expect(scope.clientFilters.missingRequiredDocumentsLegacy).toBeUndefined();
            SpecHelper.updateSelectize(elem, '[name="missingRequiredDocuments"]', 'true');
            expect(scope.clientFilters.missingRequiredDocumentsLegacy).toBe('true');

            expect(scope.clientFilters.readyForApprovalLegacy).toBeUndefined();
            SpecHelper.updateSelectize(elem, '[name="readyForApproval"]', 'true');
            expect(scope.clientFilters.readyForApprovalLegacy).toBe('true');
        });

        it('should provide the standard user fields for filtering if the cohort is not legacy', () => {
            jest.spyOn(cohort, 'isLegacyEnrollmentCohort', 'get').mockReturnValue(false);
            render();

            expect(scope.missingRequiredDocumentsKey).toEqual('missingRequiredDocuments');
            expect(scope.readyForApprovalKey).toEqual('readyForApproval');

            expect(scope.clientFilters.missingRequiredDocuments).toBeUndefined();
            SpecHelper.updateSelectize(elem, '[name="missingRequiredDocuments"]', 'true');
            expect(scope.clientFilters.missingRequiredDocuments).toBe('true');

            expect(scope.clientFilters.readyForApproval).toBeUndefined();
            SpecHelper.updateSelectize(elem, '[name="readyForApproval"]', 'true');
            expect(scope.clientFilters.readyForApproval).toBe('true');
        });

        it('should clear standard field clientFilter values if legacy', () => {
            jest.spyOn(cohort, 'isLegacyEnrollmentCohort', 'get').mockReturnValue(true);
            render();
            scope.clientFilters = {
                missingRequiredDocuments: 'true',
                readyForApproval: 'true',
            };
            scope.$digest();

            expect(scope.clientFilters.missingRequiredDocuments).toBeUndefined();
            expect(scope.clientFilters.readyForApproval).toBeUndefined();
        });

        it('should clear legacy field clientFilter values if not legacy', () => {
            jest.spyOn(cohort, 'isLegacyEnrollmentCohort', 'get').mockReturnValue(false);
            render();
            scope.clientFilters = {
                missingRequiredDocumentsLegacy: 'true',
                readyForApprovalLegacy: 'true',
            };
            scope.$digest();

            expect(scope.clientFilters.missingRequiredDocumentsLegacy).toBeUndefined();
            expect(scope.clientFilters.readyForApprovalLegacy).toBeUndefined();
        });
    });

    describe('cohort application status filtering', () => {
        it("should only show accepted cohort applications when clientFilters.isAccepted is 'true'", () => {
            render();
            expect(scope.clientFilters.isAccepted).toEqual('true');
            expect(scope.clientFilters.needsToRegister).toBeUndefined();
            SpecHelper.expectElements(elem, 'editable-things-list tbody tr', 2);
            SpecHelper.expectElement(
                elem,
                `editable-things-list tr[data-id=${students[0].id}] [data-id="is-accepted"] .fa-check`,
            );
            SpecHelper.expectElement(
                elem,
                `editable-things-list tr[data-id=${students[1].id}] [data-id="is-accepted"] .fa-check`,
            );
        });

        it("should show pre_accepted cohort applications when clientFilters.isAccepted is 'false'", () => {
            render();
            expect(scope.clientFilters.needsToRegister).toBeUndefined();
            scope.clientFilters.isAccepted = 'false';
            scope.$digest();
            // should change needsToRegister to false
            expect(scope.clientFilters.needsToRegister).toEqual(false);
            SpecHelper.expectElements(elem, 'editable-things-list tbody tr', 2);
            SpecHelper.expectNoElement(
                elem,
                `editable-things-list tr[data-id=${students[0].id}] [data-id="is-accepted"] .fa-check`,
            );
            SpecHelper.expectNoElement(
                elem,
                `editable-things-list tr[data-id=${students[1].id}] [data-id="is-accepted"] .fa-check`,
            );
        });
    });
});
