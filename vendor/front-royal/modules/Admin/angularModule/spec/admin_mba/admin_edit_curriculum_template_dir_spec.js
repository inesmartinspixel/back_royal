import 'AngularSpecHelper';
import 'Admin/angularModule';

describe('Admin::AdminEditCurriculumTemplate', () => {
    let renderer;
    let elem;
    let SpecHelper;
    let CurriculumTemplate;
    let curriculumTemplate;
    let goBack;
    let scope;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Admin', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            CurriculumTemplate = $injector.get('CurriculumTemplate');
        });
        curriculumTemplate = CurriculumTemplate.new({
            id: 'id',
            name: 'test template',
        });

        SpecHelper.stubCurrentUser('admin');

        SpecHelper.stubDirective('importCurriculumTemplate');
        SpecHelper.stubDirective('editSchedule');
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.curriculumTemplate = curriculumTemplate;
        goBack = renderer.scope.goBack = jest.fn();
        renderer.render(
            '<admin-edit-curriculum-template thing="curriculumTemplate" go-back="goBack()" created="created($thing)" destroyed="destroyed($thing)"></admin-edit-curriculum-template>',
        );
        scope = renderer.childScope;
        elem = renderer.elem;
    }

    it('should have a working goBack button', () => {
        render();
        SpecHelper.click(elem, '[name="back-to-list"]');
        expect(goBack).toHaveBeenCalled();
    });

    it('should disable the save button until a program type and name are provided', () => {
        curriculumTemplate.name = curriculumTemplate.program_type = undefined;
        render();
        SpecHelper.expectElementDisabled(elem, 'button[name="save"]');

        scope.proxy.name = 'some-template';
        scope.$digest();
        SpecHelper.expectElementDisabled(elem, 'button[name="save"]');

        scope.proxy.program_type = 'emba';
        scope.$digest();
        SpecHelper.expectElementEnabled(elem, 'button[name="save"]');
    });
});
