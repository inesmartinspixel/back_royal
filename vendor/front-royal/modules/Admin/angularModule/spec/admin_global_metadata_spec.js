import 'AngularSpecHelper';
import 'Admin/angularModule';

describe('Authentication::AdminGlobalMetadataDir', () => {
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let $injector;
    let $window;
    let $auth;
    let ngToast;
    let GlobalMetadata;
    let globalMetadata;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Admin', 'SpecHelper');

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;

            SpecHelper = $injector.get('SpecHelper');
            $window = $injector.get('$window');
            ngToast = $injector.get('ngToast');
            GlobalMetadata = $injector.get('GlobalMetadata');
            $auth = $injector.get('$auth');
        });

        jest.spyOn($auth, 'validateToken').mockImplementation(() => {});

        SpecHelper.stubCurrentUser('admin');

        globalMetadata = [
            {
                site_name: 'smartly',
                default_title: 'smartly is awesome mmkay',
                default_description: 'get smarter at work',
                destroyable: true,
                created_at: 0,
                updated_at: 0,
            },
            {
                site_name: 'blue ocean',
                default_title: 'blue ocean is a book',
                default_description: 'read this book',
                destroyable: true,
                created_at: 0,
                updated_at: 0,
            },
        ];

        GlobalMetadata.expect('index').returns({
            result: globalMetadata,
            meta: {
                available_actions: ['home#index', 'marketing#index'],
            },
        });
    });

    describe('header', () => {
        it('should set the text to Global Metadata Admin', () => {
            const AppHeaderViewModel = $injector.get('Navigation.AppHeader.AppHeaderViewModel');

            jest.spyOn(AppHeaderViewModel, 'setTitleHTML').mockImplementation(() => {});
            renderAndFlush();

            expect(AppHeaderViewModel.setTitleHTML).toHaveBeenCalledWith('GLOBAL METADATA<br>ADMIN');
        });
    });

    describe('create', () => {
        it('should allow for creating new global metadata', () => {
            renderAndFlush();

            const createButton = SpecHelper.expectElementText(
                elem,
                'button[name="create_global_metadata"]',
                'Create Global Metadata',
                0,
            );

            // would like to use assertSelectizeOptions, but could
            // not get it to work
            expect(scope.pageOptions).toEqual([
                {
                    value: 'home#index',
                    text: 'home#index',
                    $order: 1,
                },
                {
                    value: 'marketing#index',
                    text: 'marketing#index',
                    $order: 2,
                },
            ]);
            SpecHelper.updateSelectize(elem, '[name="new_global_metadata"]', 'home#index');
            GlobalMetadata.expect('create').returns({
                site_name: 'home#index',
                updated_at: 0,
                created_at: 0,
            });

            createButton.click();

            jest.spyOn(ngToast, 'create').mockImplementation(() => {});
            GlobalMetadata.flush('create');

            expect(scope.pageOptions).toEqual([
                {
                    value: 'marketing#index',
                    text: 'marketing#index',
                    $order: 3,
                },
            ]);

            expect(ngToast.create).toHaveBeenCalledWith({
                content: 'home#index successfully created.',
                className: 'success',
            });
        });
    });

    describe('update', () => {
        it('should allow for updating existing global metadata', () => {
            renderAndFlush();

            const updateButton = SpecHelper.expectElementText(
                elem,
                'button[name="update_global_metadata"]',
                'Update',
                0,
            );
            GlobalMetadata.expect('save').returns({
                site_name: 'smartly',
                default_title: 'smarter at work',
            });

            updateButton.click();

            jest.spyOn(ngToast, 'create').mockImplementation(() => {});
            GlobalMetadata.flush('save');

            expect(ngToast.create).toHaveBeenCalledWith({
                content: 'smartly successfully updated.',
                className: 'success',
            });
        });
    });

    describe('delete', () => {
        it('should delete global metadata if confirmed', () => {
            renderAndFlush();

            const orig = scope.globalMetadata.length;
            const deleteButton = SpecHelper.expectElements(
                elem,
                'button[name="delete_global_metadata"]',
                scope.globalMetadata.length,
            ).eq(0);
            expect(scope.globalMetadata.length).toBe(2);

            jest.spyOn($window, 'confirm').mockReturnValue(true);
            GlobalMetadata.expect('destroy');
            jest.spyOn(ngToast, 'create').mockImplementation(() => {});
            deleteButton.click();
            GlobalMetadata.flush('destroy');

            expect(ngToast.create).toHaveBeenCalledWith({
                content: 'smartly successfully deleted.',
                className: 'success',
            });

            expect(scope.globalMetadata.length).toBe(orig - 1);
        });

        it('should not delete if not confirmed', () => {
            renderAndFlush();

            const deleteButton = SpecHelper.expectElements(
                elem,
                'button[name="delete_global_metadata"]',
                scope.globalMetadata.length,
            ).eq(0);
            expect(scope.globalMetadata.length).toBe(2);

            jest.spyOn($window, 'confirm').mockReturnValue(false);
            jest.spyOn(ngToast, 'create').mockImplementation(() => {});
            deleteButton.click();

            expect(ngToast.create).not.toHaveBeenCalled();
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function renderAndFlush() {
        renderer = SpecHelper.renderer();
        renderer.render('<admin-global-metadata></admin-global-metadata>');
        elem = renderer.elem;
        scope = elem.isolateScope();
        GlobalMetadata.flush('index');
    }
});
