import 'AngularSpecHelper';
import 'Admin/angularModule';

describe('Admin::AdminEditService', () => {
    let SpecHelper;
    let $injector;
    let $rootScope;
    let scope;
    let guid;
    let AdminEditService;
    let Group;
    let thing;
    let ngToast;
    let $window;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Admin', 'SpecHelper');

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            SpecHelper = $injector.get('SpecHelper');
            $rootScope = $injector.get('$rootScope');
            guid = $injector.get('guid');
            AdminEditService = $injector.get('AdminEditService');
            Group = $injector.get('Group');
            ngToast = $injector.get('ngToast');
            $window = $injector.get('$window');

            SpecHelper.stubDirective('localePackSelectize');
            thing = Group.new({
                id: guid.generate(),
            });
        });
    });

    describe('onLink', () => {
        it('should initialize proxy', () => {
            scope = $rootScope;
            scope.thing = thing;
            AdminEditService.onLink(scope, Group);

            expect(angular.equals(scope.proxy, scope.thing)).toBe(true);
        });

        it('should set isNew if thing.id is undefined', () => {
            scope = $rootScope;
            thing.id = undefined;
            scope.thing = thing;
            AdminEditService.onLink(scope, Group);

            expect(scope.isNew).toBe(true);
        });

        it('should call getResetProxy when the thing changes', () => {
            jest.spyOn(AdminEditService, 'getResetProxy').mockImplementation(() => {});
            scope = $rootScope;
            scope.thing = thing;
            AdminEditService.onLink(scope, Group);

            scope.$digest();
            scope.thing = Group.new();
            scope.$digest();
            expect(AdminEditService.getResetProxy).toHaveBeenCalledTimes(2);
        });

        it('should call isProxyValid when the proxy changes', () => {
            jest.spyOn(AdminEditService, 'isProxyValid').mockImplementation(() => {});
            scope = $rootScope;
            scope.thing = thing;
            AdminEditService.onLink(scope, Group);
            scope.$digest();
            scope.proxy = {
                foo: true,
            };
            scope.$digest();

            expect(AdminEditService.isProxyValid).toHaveBeenCalledTimes(2);
        });

        it('should set save and delete unless options say not to', () => {
            scope = $rootScope;
            scope.thing = thing;
            AdminEditService.onLink(scope, Group);

            expect(scope.save).toBeDefined();
            expect(scope.delete).toBeDefined();
        });

        it('should not set save and delete if options say not to', () => {
            scope = $rootScope;
            scope.thing = thing;
            AdminEditService.onLink(scope, Group, {
                foo: true,
            });
            expect(scope.save).toBeUndefined();
            expect(scope.delete).toBeUndefined();
        });

        describe('save', () => {
            let scope;

            beforeEach(() => {
                scope = $rootScope;
                scope.thing = thing;
                AdminEditService.onLink(scope, Group);
            });

            describe('success', () => {
                it('should copy the proxy into the thing', () => {
                    Group.expect('save');

                    scope.proxy.name = 'the proxy';
                    scope.save();
                    Group.flush('save');

                    expect(thing.name).toBe('the proxy');
                });

                it('should reset the proxy', () => {
                    jest.spyOn(AdminEditService, 'getResetProxy').mockImplementation(() => {});
                    Group.expect('save');

                    scope.save();
                    Group.flush('save');

                    expect(AdminEditService.getResetProxy).toHaveBeenCalled();
                });

                it('should call created callback if creating a new thing', () => {
                    scope.created = jest.fn();

                    thing.id = undefined;

                    Group.expect('save');
                    scope.save();
                    Group.flush('save');

                    expect(scope.created).toHaveBeenCalled();
                });

                it('should show toast', () => {
                    jest.spyOn(ngToast, 'create').mockImplementation(() => {});
                    Group.expect('save');

                    scope.proxy.name = 'the proxy';
                    scope.save();
                    Group.flush('save');

                    expect(ngToast.create).toHaveBeenCalledWith({
                        content: 'the proxy saved',
                        className: 'success',
                    });
                });
            });
        });
    });

    describe('getResetProxy', () => {
        it('should return a copy of the thing', () => {
            const proxy = AdminEditService.getResetProxy(thing, Group);
            expect(proxy.name).toEqual(thing.name);
        });

        it('should return an empty object if there is no thing', () => {
            const proxy = AdminEditService.getResetProxy(null, Group);
            expect(proxy).toEqual({});
        });
    });

    describe('isProxyValid', () => {
        it('should return true if thing and proxy are not null and not equal', () => {
            const valid = AdminEditService.isProxyValid(Group.new(), thing);
            expect(valid).toBe(true);
        });

        it('should return false if thing or proxy is null', () => {
            let valid = AdminEditService.isProxyValid(null, thing);
            expect(!!valid).toBe(false);
            valid = AdminEditService.isProxyValid(Group.new(), null);
            expect(!!valid).toBe(false);
        });

        it('should return false if thing and proxy are equal', () => {
            const valid = AdminEditService.isProxyValid(thing, thing);
            expect(valid).toBe(false);
        });
    });

    describe('delete', () => {
        it('should show a confirm', () => {
            jest.spyOn($window, 'confirm').mockImplementation(() => {});
            AdminEditService.delete(thing);
            expect($window.confirm).toHaveBeenCalledWith(`Are you sure you want to delete ${thing.name}`);
        });

        describe('success', () => {
            it('should show a toast', () => {
                jest.spyOn($window, 'confirm').mockReturnValue(true);
                jest.spyOn(ngToast, 'create').mockImplementation(() => {});
                Group.expect('destroy');
                AdminEditService.delete(thing);
                Group.flush('destroy');
                expect(ngToast.create).toHaveBeenCalledWith({
                    content: `${thing.name} deleted`,
                    className: 'success',
                });
            });

            it('should call goBack and destroyed callbacks', () => {
                jest.spyOn($window, 'confirm').mockReturnValue(true);
                jest.spyOn(ngToast, 'create').mockImplementation(() => {});
                Group.expect('destroy');

                const goBack = jest.fn();
                const destroyed = jest.fn();
                AdminEditService.delete(thing, {
                    goBack,
                    destroyed,
                });
                Group.flush('destroy');
                expect(ngToast.create).toHaveBeenCalledWith({
                    content: `${thing.name} deleted`,
                    className: 'success',
                });
            });
        });
    });
});
