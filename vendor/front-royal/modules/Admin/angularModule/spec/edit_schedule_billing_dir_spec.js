import 'AngularSpecHelper';
import 'Admin/angularModule';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';

describe('Admin::EditScheduleBilling', () => {
    let $injector;
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let Cohort;
    let schedulableItem;
    let stripePlans;
    let scholarshipLevels;
    let stripeCoupons;
    let CurriculumTemplate;
    let schedulableItemTemplate;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Admin', 'SpecHelper');

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            SpecHelper = $injector.get('SpecHelper');
            Cohort = $injector.get('Cohort');
            CurriculumTemplate = $injector.get('CurriculumTemplate');
            $injector.get('CohortFixtures');
        });

        const defaults = {
            program_type: 'emba',
        };

        stripePlans = [
            {
                id: 'some_plan_id',
                name: 'Some Plan 1',
                amount: 80000,
                frequency: 'monthly',
            },
            {
                id: 'another_plan_id',
                name: 'Some Plan 2',
                amount: 480000,
                frequency: 'bi_annual',
            },
            {
                id: 'yet_another_plan_id',
                name: 'Some Plan 3',
                amount: 480000,
                frequency: 'bi_annual',
            },
            {
                id: 'single_payment',
                name: 'Some Plan 4',
                amount: 480000,
                frequency: 'once',
            },
        ];

        stripeCoupons = [
            {
                id: 'none',
                amount_off: 0,
                percent_off: 0,
            },
            {
                id: 'coupon1',
                amount_off: 10000,
                percent_off: 0,
            },
            {
                id: 'coupon2',
                amount_off: 150000,
                percent_off: 0,
            },
            {
                id: 'coupon3',
                amount_off: 200000,
                percent_off: 0,
            },
            {
                id: '100_percent',
                amount_off: 0,
                percent_off: 100,
            },
        ];

        scholarshipLevels = [
            {
                name: 'Level 1',
                standard: {
                    some_plan_id: _.clone(stripeCoupons[0]),
                    another_plan_id: _.clone(stripeCoupons[0]),
                    yet_another_plan_id: _.clone(stripeCoupons[0]),
                },
                early: {
                    some_plan_id: _.clone(stripeCoupons[0]),
                    another_plan_id: _.clone(stripeCoupons[0]),
                    yet_another_plan_id: _.clone(stripeCoupons[0]),
                },
            },
            {
                name: 'Level 2',
                standard: {
                    some_plan_id: _.clone(stripeCoupons[1]),
                    another_plan_id: _.clone(stripeCoupons[2]),
                    yet_another_plan_id: _.clone(stripeCoupons[3]),
                },
                early: {
                    some_plan_id: _.clone(stripeCoupons[1]),
                    another_plan_id: _.clone(stripeCoupons[2]),
                    yet_another_plan_id: _.clone(stripeCoupons[3]),
                },
            },
            {
                name: 'Level 3',
                standard: {
                    some_plan_id: _.clone(stripeCoupons[4]),
                    another_plan_id: _.clone(stripeCoupons[4]),
                    yet_another_plan_id: _.clone(stripeCoupons[4]),
                },
                early: {
                    some_plan_id: _.clone(stripeCoupons[4]),
                    another_plan_id: _.clone(stripeCoupons[4]),
                    yet_another_plan_id: _.clone(stripeCoupons[4]),
                },
            },
        ];

        schedulableItem = Cohort.fixtures.getInstance(defaults);
        schedulableItemTemplate = CurriculumTemplate.new(defaults);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render(klass) {
        renderer = SpecHelper.renderer();

        renderer.scope.schedulableItem = klass === CurriculumTemplate ? schedulableItemTemplate : schedulableItem;
        renderer.scope.stripePlans = stripePlans;
        renderer.scope.stripeCoupons = stripeCoupons;
        renderer.render(
            '<edit-schedule-billing schedulable-item="schedulableItem" stripe-plans="stripePlans" stripe-coupons="stripeCoupons"></edit-schedule-billing>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    it('should allow for opening and closing', () => {
        render();
        scope.$$editingDates = false;

        scope.$digest(); // mock out any logic about whether this is initially expanded
        SpecHelper.expectDoesNotHaveClass(elem, '.unlockable-admin-section', 'editing');
        SpecHelper.click(elem, '.unlockable-admin-section');
        SpecHelper.expectHasClass(elem, '.unlockable-admin-section', 'editing');
        SpecHelper.click(elem, '[name="close-billing"]');
        SpecHelper.expectDoesNotHaveClass(elem, '.unlockable-admin-section', 'editing');
    });

    it('should toggle readonly mode correctly', () => {
        jest.spyOn(schedulableItem, 'supportsRecurringPayments', 'get').mockReturnValue(true);
        render();
        SpecHelper.expectElement(elem, '[name="billing"].unlockable-admin-section:not(.editing)');
        SpecHelper.expectNoElement(elem, '[name="billing"].unlockable-admin-section.editing');
        SpecHelper.click(elem, '[name="billing"].unlockable-admin-section:not(.editing)');
        SpecHelper.expectNoElement(elem, '[name="billing"].unlockable-admin-section:not(.editing)');
        SpecHelper.expectElement(elem, '[name="billing"].unlockable-admin-section.editing');

        // Close and check again
        SpecHelper.click(elem, '[name="close-billing"]');
        SpecHelper.expectElement(elem, '[name="billing"].unlockable-admin-section:not(.editing)');
        SpecHelper.expectNoElement(elem, '[name="billing"].unlockable-admin-section.editing');
    });

    describe('editing plans', () => {
        beforeEach(() => {
            jest.spyOn(schedulableItem, 'supportsRecurringPayments', 'get').mockReturnValue(true);
            render();
        });

        it('should support adding new plans', () => {
            expect(schedulableItem.stripe_plans).toBeUndefined();
            SpecHelper.click(elem, '[name="billing"].unlockable-admin-section:not(.editing)');
            SpecHelper.updateSelectize(elem, '[name="add_stripe_plan"]', scope.stripePlans[0].id);
            SpecHelper.updateSelectize(elem, '[name="add_stripe_plan"]', scope.stripePlans[1].id);
            expect(schedulableItem.stripe_plans[0].id).toEqual('some_plan_id');
            expect(schedulableItem.stripe_plans[0].name).toEqual('Some Plan 1');
            expect(schedulableItem.stripe_plans[0].amount).toEqual(80000);
            expect(schedulableItem.stripe_plans[0].frequency).toEqual('monthly');
            expect(schedulableItem.stripe_plans[1].id).toEqual('another_plan_id');
            expect(schedulableItem.stripe_plans[1].name).toEqual('Some Plan 2');
            expect(schedulableItem.stripe_plans[1].amount).toEqual(480000);
            expect(schedulableItem.stripe_plans[1].frequency).toEqual('bi_annual');
        });

        it('should support adding a plan and immediately changing scholarship levels', () => {
            const somePlan = _.findWhere(stripePlans, {
                id: 'some_plan_id',
            });
            schedulableItem.stripe_plans = [somePlan];
            schedulableItem.scholarship_levels = [];
            _.each(scholarshipLevels, level => {
                const _level = {
                    name: level.name,
                    standard: {
                        some_plan_id: _.clone(stripeCoupons[0]),
                    },
                    early: {
                        some_plan_id: _.clone(stripeCoupons[0]),
                    },
                };
                schedulableItem.scholarship_levels.push(_level);
            });
            render();
            SpecHelper.click(elem, '[name="billing"].unlockable-admin-section:not(.editing)');
            SpecHelper.updateSelectize(elem, '[name="add_stripe_plan"]', 'another_plan_id');

            // Initially, no coupons for the new plan
            expect(schedulableItem.scholarship_levels[0].standard.another_plan_id.id).toEqual('none');
            expect(schedulableItem.scholarship_levels[0].early.another_plan_id.id).toEqual('none');
            expect(schedulableItem.scholarship_levels[1].standard.another_plan_id.id).toEqual('none');
            expect(schedulableItem.scholarship_levels[1].early.another_plan_id.id).toEqual('none');

            // Select a coupon for the new plan and the first level
            SpecHelper.updateSelect(
                elem,
                '.level-details:eq(0) .registration-period:eq(0) select:eq(1)',
                stripeCoupons[1].id,
            );
            expect(schedulableItem.scholarship_levels[0].standard.another_plan_id.id).toEqual(stripeCoupons[1].id);

            SpecHelper.updateSelect(
                elem,
                '.level-details:eq(0) .registration-period:eq(1) select:eq(1)',
                stripeCoupons[2].id,
            );
            expect(schedulableItem.scholarship_levels[0].early.another_plan_id.id).toEqual(stripeCoupons[2].id);

            expect(schedulableItem.scholarship_levels[1].standard.another_plan_id.id).toEqual('none');
            expect(schedulableItem.scholarship_levels[1].early.another_plan_id.id).toEqual('none');
        });

        it('should display a special message when all plans are already selected', () => {
            expect(schedulableItem.stripe_plans).toBeUndefined();
            SpecHelper.click(elem, '[name="billing"].unlockable-admin-section:not(.editing)');
            _.each(scope.stripePlans, (plan, i) => {
                SpecHelper.updateSelectize(elem, '[name="add_stripe_plan"]', plan.id);
                if (i === scope.stripePlans.length - 1) {
                    SpecHelper.expectElementText(elem, '.no-plans', 'All available Stripe plans already selected!');
                } else {
                    SpecHelper.expectNoElement(elem, 'no-plans');
                }
            });
        });

        it('support removing existing plans', () => {
            const $window = $injector.get('$window');
            jest.spyOn($window, 'confirm').mockReturnValue(true);

            schedulableItem.stripe_plans = scope.stripePlans;
            scope.$digest();
            SpecHelper.click(elem, '[name="billing"].unlockable-admin-section:not(.editing)');

            expect(_.chain(schedulableItem.stripe_plans).map('id').contains(scope.stripePlans[2].id).value()).toBe(
                true,
            );
            SpecHelper.click(elem, '.plan-details .remove', 2);
            expect(_.chain(schedulableItem.stripe_plans).map('id').contains(scope.stripePlans[2].id).value()).toBe(
                false,
            );

            expect(_.chain(schedulableItem.stripe_plans).map('id').contains(scope.stripePlans[1].id).value()).toBe(
                true,
            );
            SpecHelper.click(elem, '.plan-details .remove', 1);
            expect(_.chain(schedulableItem.stripe_plans).map('id').contains(scope.stripePlans[1].id).value()).toBe(
                false,
            );

            expect(_.chain(schedulableItem.stripe_plans).map('id').contains(scope.stripePlans[0].id).value()).toBe(
                true,
            );
            SpecHelper.click(elem, '.plan-details .remove', 0);
            expect(_.chain(schedulableItem.stripe_plans).map('id').contains(scope.stripePlans[0].id).value()).toBe(
                false,
            );
        });

        it('support linking to existing plans', () => {
            schedulableItem.stripe_plans = scope.stripePlans;
            scope.$digest();
            SpecHelper.click(elem, '[name="billing"].unlockable-admin-section:not(.editing)');

            const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
            jest.spyOn(NavigationHelperMixin, 'loadUrl').mockImplementation(() => {});
            SpecHelper.click(elem, '.plan-details .link', 0);
            expect(NavigationHelperMixin.loadUrl).toHaveBeenCalledWith(
                `https://dashboard.stripe.com/plans/${schedulableItem.stripe_plans[0].id}`,
                '_blank',
            );
        });

        it('should work for the supportsOnlyOneTimePayment programs', () => {
            // sanity check. before we switch to supportsOnlyOneTimePayment, all plans
            // should be available
            expect(scope.filteredStripePlans.length > 1).toBe(true);

            jest.spyOn(schedulableItem, 'supportsOnlyOneTimePayment', 'get').mockReturnValue(true);
            jest.spyOn(schedulableItem, 'supportsScholarshipLevels', 'get').mockReturnValue(false);

            scope.stripePlans = _.clone(scope.stripePlans); // force the watcher to rebuild filteredStripePlans
            scope.$digest();
            SpecHelper.expectNoElement(elem, '.scholarships-section');
            expect(_.chain(scope.filteredStripePlans).pluck('value').value()).toEqual(['single_payment']);
        });
    });

    describe('editing scholarship levels', () => {
        beforeEach(() => {
            jest.spyOn(schedulableItem, 'supportsRecurringPayments', 'get').mockReturnValue(true);
            render();
        });

        it('should support adding new scholarship levels', () => {
            expect(schedulableItem.stripe_plans).toBeUndefined();
            expect(schedulableItem.scholarship_levels).toBeUndefined();

            schedulableItem.stripe_plans = scope.stripePlans;
            scope.$digest();

            SpecHelper.click(elem, '[name="billing"].unlockable-admin-section:not(.editing)');
            SpecHelper.updateTextInput(elem, '[name="add_scholarship_level"]', 'New Level');
            SpecHelper.click(elem, '[name="add_scholarship_level_button"]');

            expect(schedulableItem.scholarship_levels[0].name).toEqual('New Level');
            _.each(scope.stripePlans, plan => {
                expect(schedulableItem.scholarship_levels[0].standard[plan.id].id).toEqual('none');
                expect(schedulableItem.scholarship_levels[0].standard[plan.id].amount_off).toEqual(0);
                expect(schedulableItem.scholarship_levels[0].standard[plan.id].percent_off).toEqual(0);

                expect(schedulableItem.scholarship_levels[0].early[plan.id].id).toEqual('none');
                expect(schedulableItem.scholarship_levels[0].early[plan.id].amount_off).toEqual(0);
                expect(schedulableItem.scholarship_levels[0].early[plan.id].percent_off).toEqual(0);
            });
        });

        it('should support editing scholarship levels', () => {
            expect(schedulableItem.stripe_plans).toBeUndefined();
            expect(schedulableItem.scholarship_levels).toBeUndefined();

            schedulableItem.stripe_plans = scope.stripePlans;
            schedulableItem.scholarship_levels = scholarshipLevels;
            scope.$digest();

            SpecHelper.click(elem, '[name="billing"].unlockable-admin-section:not(.editing)');

            expect(schedulableItem.scholarship_levels[0].standard.some_plan_id.id).toEqual('none');
            expect(schedulableItem.scholarship_levels[0].early.some_plan_id.id).toEqual('none');

            SpecHelper.updateSelect(
                elem,
                '.level-details:eq(0) .registration-period:eq(0) select:eq(0)',
                stripeCoupons[1].id,
            );
            expect(schedulableItem.scholarship_levels[0].standard.some_plan_id.id).toEqual('coupon1');

            SpecHelper.updateSelect(
                elem,
                '.level-details:eq(0) .registration-period:eq(1) select:eq(0)',
                stripeCoupons[1].id,
            );
            expect(schedulableItem.scholarship_levels[0].early.some_plan_id.id).toEqual('coupon1');
        });

        it('support removing existing scholarship levels', () => {
            schedulableItem.stripe_plans = scope.stripePlans;
            schedulableItem.scholarship_levels = scholarshipLevels;

            scope.$digest();
            SpecHelper.click(elem, '[name="billing"].unlockable-admin-section:not(.editing)');

            expect(
                _.chain(schedulableItem.scholarship_levels).map('name').contains(scholarshipLevels[2].name).value(),
            ).toBe(true);
            SpecHelper.click(elem, '.level-details .remove', 2);
            expect(
                _.chain(schedulableItem.scholarship_levels).map('name').contains(scholarshipLevels[2].name).value(),
            ).toBe(false);

            expect(
                _.chain(schedulableItem.scholarship_levels).map('name').contains(scholarshipLevels[1].name).value(),
            ).toBe(true);
            SpecHelper.click(elem, '.level-details .remove', 1);
            expect(
                _.chain(schedulableItem.scholarship_levels).map('name').contains(scholarshipLevels[1].name).value(),
            ).toBe(false);

            expect(
                _.chain(schedulableItem.scholarship_levels).map('name').contains(scholarshipLevels[0].name).value(),
            ).toBe(true);
            SpecHelper.click(elem, '.level-details .remove', 0);
            expect(
                _.chain(schedulableItem.scholarship_levels).map('name').contains(scholarshipLevels[0].name).value(),
            ).toBe(false);
        });
    });

    describe('program guide', () => {
        it('allow for editing of the program guide for cohorts only', () => {
            jest.spyOn(schedulableItem, 'supportsRecurringPayments', 'get').mockReturnValue(true);
            jest.spyOn(schedulableItem, 'supportsProgramGuide', 'get').mockReturnValue(true);
            render();
            scope.$digest();
            SpecHelper.click(elem, '[name="billing"].unlockable-admin-section:not(.editing)');

            SpecHelper.updateTextInput(elem, '[name="program_guide_url"]', 'some_program_guide_url');
            expect(schedulableItem.program_guide_url).toEqual('some_program_guide_url');

            jest.spyOn(schedulableItemTemplate, 'supportsRecurringPayments', 'get').mockReturnValue(true);
            render(CurriculumTemplate);
            scope.$digest();
            SpecHelper.click(elem, '[name="billing"].unlockable-admin-section:not(.editing)');
            SpecHelper.expectNoElement(elem, '[name="program_guide_url"]');
        });

        it("not allow editing if the program guide type doesn't support it", () => {
            jest.spyOn(schedulableItem, 'supportsRecurringPayments', 'get').mockReturnValue(true);
            jest.spyOn(schedulableItem, 'supportsProgramGuide', 'get').mockReturnValue(false);
            render();
            scope.$digest();
            SpecHelper.click(elem, '[name="billing"].unlockable-admin-section:not(.editing)');
            SpecHelper.expectNoElement(elem, '[name="program_guide_url"]');
        });
    });
});
