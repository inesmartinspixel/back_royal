import 'AngularSpecHelper';
import 'Admin/angularModule';

describe('Admin::AddUserDir', () => {
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let User;
    let Lesson;
    let ngToast;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Admin', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            ngToast = $injector.get('ngToast');
            User = $injector.get('User');
            Lesson = $injector.get('Lesson');
            SpecHelper.stubCurrentUser('admin');
            Lesson.expect('index');
            SpecHelper.stubCurrentUser('admin');
            SpecHelper.stubConfig();
            SpecHelper.stubDirective('itemGroupsEditor');
            render();
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('form submission', () => {
        it('should create new user when submit button is clicked', () => {
            User.expect('save');
            fillInAddUserForm();
            jest.spyOn(ngToast, 'create').mockImplementation(() => {});
            SpecHelper.expectElement(elem, '.form-group:eq(1) button[type="submit"]');
            // submitButton.click();
            SpecHelper.submitForm(elem);
            User.flush('save');
            expect(scope.newUser.sign_up_code).toEqual('ADMIN');
            expect(ngToast.create).toHaveBeenCalledWith({
                content: 'joeuser@pedago.com created.',
                className: 'success',
            });
        });

        it('should disable submission until all required fields are completed', () => {
            SpecHelper.expectElementDisabled(elem, '.form-group:eq(1) button[type="submit"]');

            SpecHelper.updateTextInput(elem, '.form-group:eq(0) input[name="name"]', 'Joe');
            SpecHelper.expectElementDisabled(elem, '.form-group:eq(1) button[type="submit"]');

            SpecHelper.updateTextInput(elem, '.form-group:eq(0) input[name="email"]', 'joeuser@pedago.com');
            SpecHelper.expectElementDisabled(elem, '.form-group:eq(1) button[type="submit"]');

            SpecHelper.updateTextInput(elem, '.form-group:eq(0) input[name="password"]', 'password');
            SpecHelper.expectElementEnabled(elem, '.form-group:eq(1) button[type="submit"]');
        });

        // see https://trello.com/c/Jmv4kaDa/1325-bug-when-creating-a-new-user-from-the-admin-page-institution-group-preferences-aren-t-saving
        it('should save changes that are set inside of edit-user-details', () => {
            fillInAddUserForm();
            SpecHelper.updateTextInput(elem, '[name="job_title"]', 'JobTitle');
            User.expect('save').toBeCalledWith(
                _.extend(scope.newUser.asJson(), {
                    job_title: 'JobTitle',
                }),
            );
            // SpecHelper.click(elem, '.form-group:eq(1) button[type="submit"]');
            SpecHelper.submitForm(elem);
            User.flush('save');
        });

        function fillInAddUserForm() {
            SpecHelper.updateTextInput(elem, '.form-group:eq(0) input[name="name"]', 'Joe');
            SpecHelper.updateTextInput(elem, '.form-group:eq(0) input[name="email"]', 'joeuser@pedago.com');
            SpecHelper.updateTextInput(elem, '.form-group:eq(0) input[name="password"]', 'password');
        }
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.users = [
            User.fixtures.getInstance(),
            User.fixtures.getInstance(),
            User.fixtures.getInstance(),
            User.fixtures.getInstance(),
            User.fixtures.getInstance(),
        ];
        renderer.scope.user = renderer.scope.users[1];
        renderer.scope.groupOptions = ['abc', 'def', 'ghi'];
        renderer.scope.globalRoleOptions = [
            {
                id: 1,
                name: 'admin',
                resource_id: null,
                resource_type: null,
                created_at: '2014-01-14T22:59:56.678Z',
                updated_at: '2014-01-14T22:59:56.678Z',
            },
            {
                id: 2,
                name: 'learner',
                resource_id: null,
                resource_type: null,
                created_at: '2014-01-14T23:30:53.412Z',
                updated_at: '2014-01-14T23:30:53.412Z',
            },
            {
                id: 3,
                name: 'editor',
                resource_id: null,
                resource_type: null,
                created_at: '2014-01-14T23:30:53.462Z',
                updated_at: '2014-01-14T23:30:53.462Z',
            },
        ];
        renderer.scope.editorRoleOptions = [
            {
                id: 1,
                name: 'reviewer',
                resource_id: 'some_guid',
                resource_type: 'Lesson',
                created_at: '2014-01-14T22:59:56.678Z',
                updated_at: '2014-01-14T22:59:56.678Z',
            },
        ];
        renderer.render(
            '<add-user users="users" group-options="groupOptions" global-role-options="globalRoleOptions" editor-role-options="editorRoleOptions"></add-user>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
    }
});
