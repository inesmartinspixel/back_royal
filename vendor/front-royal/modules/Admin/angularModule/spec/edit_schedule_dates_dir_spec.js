import 'AngularSpecHelper';
import 'Admin/angularModule';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';

describe('Admin::EditScheduleDates', () => {
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let Cohort;
    let schedulableItem;
    let dateHelper;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Admin', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            Cohort = $injector.get('Cohort');
            $injector.get('CohortFixtures');
            dateHelper = $injector.get('dateHelper');
        });

        schedulableItem = Cohort.fixtures.getInstance({
            start_date: 1,
            diploma_generation_days_offset_from_end: 7,
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.schedulableItem = schedulableItem;
        renderer.render('<edit-schedule-dates schedulable-item="schedulableItem"></edit-schedule-dates>');
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    it('should allow for opening and closing', () => {
        render();
        scope.$$editingDates = false;
        scope.$digest(); // mock out any logic about whether this is initially expanded
        SpecHelper.expectDoesNotHaveClass(elem, '.unlockable-admin-section', 'editing');
        SpecHelper.click(elem, '.unlockable-admin-section');
        SpecHelper.expectHasClass(elem, '.unlockable-admin-section', 'editing');
        SpecHelper.click(elem, '[name="close-dates"]');
        SpecHelper.expectDoesNotHaveClass(elem, '.unlockable-admin-section', 'editing');
    });

    it('should allow for editing start date', () => {
        renderAndStartEditing();
        SpecHelper.expectElement(elem, 'admin-datetimepicker[ng-model="schedulableItem.startDate"]');
    });

    it('should allow for editing registration deadline date only if it is supported', () => {
        renderAndStartEditing();
        Object.defineProperty(schedulableItem, 'supportsRecurringPayments', {
            value: true,
            configurable: true,
        });
        scope.$digest();
        SpecHelper.expectElement(elem, 'admin-datetimepicker[ng-model="schedulableItem.registrationDeadline"]');
        Object.defineProperty(schedulableItem, 'supportsRecurringPayments', {
            value: false,
        });
        scope.$digest();
        SpecHelper.expectNoElement(elem, 'admin-datetimepicker[ng-model="schedulableItem.registrationDeadline"]');
    });

    it('should allow for editing early registration deadline date only if it is supported', () => {
        renderAndStartEditing();
        Object.defineProperty(schedulableItem, 'supportsRecurringPayments', {
            value: true,
            configurable: true,
        });
        scope.$digest();
        SpecHelper.expectElement(elem, 'admin-datetimepicker[ng-model="schedulableItem.earlyRegistrationDeadline"]');
        Object.defineProperty(schedulableItem, 'supportsRecurringPayments', {
            value: false,
        });
        scope.$digest();
        SpecHelper.expectNoElement(elem, 'admin-datetimepicker[ng-model="schedulableItem.earlyRegistrationDeadline"]');
    });

    it('should allow for editing enrollment deadline date only if it is supported', () => {
        renderAndStartEditing();
        Object.defineProperty(schedulableItem, 'supportsEnrollmentDeadline', {
            value: true,
            configurable: true,
        });
        scope.$digest();
        SpecHelper.expectElement(elem, 'admin-datetimepicker[ng-model="schedulableItem.enrollmentDeadline"]');
        Object.defineProperty(schedulableItem, 'supportsEnrollmentDeadline', {
            value: false,
        });
        scope.$digest();
        SpecHelper.expectNoElement(elem, 'admin-datetimepicker[ng-model="schedulableItem.enrollmentDeadline"]');
    });

    it('should allow for editing enrollment diploma generation date only if it is supported', () => {
        renderAndStartEditing();
        Object.defineProperty(schedulableItem, 'supportsDiplomaGeneration', {
            value: true,
            configurable: true,
        });
        scope.$digest();
        SpecHelper.expectElement(elem, 'admin-datetimepicker[ng-model="schedulableItem.diplomaGeneration"]');
        Object.defineProperty(schedulableItem, 'supportsDiplomaGeneration', {
            value: false,
        });
        scope.$digest();
        SpecHelper.expectNoElement(elem, 'admin-datetimepicker[ng-model="schedulableItem.diplomaGeneration"]');
    });

    it('should allow for editing graduation date only if it is supported', () => {
        renderAndStartEditing();
        Object.defineProperty(schedulableItem, 'supportsSchedule', {
            value: true,
            configurable: true,
        });
        scope.$digest();
        SpecHelper.expectElement(elem, 'admin-datetimepicker[ng-model="schedulableItem.graduationDate"]');
        Object.defineProperty(schedulableItem, 'supportsSchedule', {
            value: false,
        });
        scope.$digest();
        SpecHelper.expectNoElement(elem, 'admin-datetimepicker[ng-model="schedulableItem.graduationDate"]');
    });

    it('should show basic details when closed', () => {
        render();
        scope.$$editingDates = false;
        jest.spyOn(dateHelper, 'formattedUserFacingMonthDayYearLong').mockImplementation(date => {
            if (!date) {
                return '';
            }
            return date.toDate ? date.toDate().getTime() : date.getTime();
        });
        jest.spyOn(dateHelper, 'formattedUserFacingDateTime').mockImplementation(date => {
            if (!date) {
                return '';
            }
            return date.toDate ? date.toDate().getTime() : date.getTime();
        });
        scope.$digest();

        SpecHelper.expectElementText(
            elem,
            '.unlockable-admin-section [name="start_date"]',
            `Start Date: ${schedulableItem.startDate.getTime()}`,
        );
        SpecHelper.expectElementText(
            elem,
            '.unlockable-admin-section [name="end_date"]',
            `End Date: ${schedulableItem.endDate.getTime()}`,
        );

        // registration deadline only sometimes supported
        Object.defineProperty(scope.schedulableItem, 'supportsRecurringPayments', {
            value: false,
            configurable: true,
        });
        scope.$digest();
        SpecHelper.expectNoElement(elem, '.unlockable-admin-section [name="registration_deadline"]');
        Object.defineProperty(scope.schedulableItem, 'supportsRecurringPayments', {
            value: true,
            configurable: true,
        });
        scope.schedulableItem.registrationDeadline = new Date();
        scope.$digest();
        SpecHelper.expectElementText(
            elem,
            '.unlockable-admin-section [name="registration_deadline"]',
            `Registration Deadline: ${schedulableItem.registrationDeadline.getTime()}`,
        );

        // enrollment deadline dates only sometimes supported
        Object.defineProperty(scope.schedulableItem, 'supportsEnrollmentDeadline', {
            value: false,
            configurable: true,
        });
        scope.$digest();
        SpecHelper.expectNoElement(elem, '.unlockable-admin-section [name="enrollment_deadline"]');
        Object.defineProperty(scope.schedulableItem, 'supportsEnrollmentDeadline', {
            value: true,
            configurable: true,
        });
        scope.$digest();
        SpecHelper.expectElementText(
            elem,
            '.unlockable-admin-section [name="enrollment_deadline"]',
            `Enrollment Deadline: ${schedulableItem.enrollmentDeadline.getTime()}`,
        );

        // enrollment deadline dates only sometimes supported
        Object.defineProperty(scope.schedulableItem, 'supportsDiplomaGeneration', {
            value: false,
            configurable: true,
        });
        scope.$digest();
        SpecHelper.expectNoElement(elem, '.unlockable-admin-section [name="diploma_generation"]');
        Object.defineProperty(scope.schedulableItem, 'supportsDiplomaGeneration', {
            value: true,
            configurable: true,
        });
        scope.$digest();
        SpecHelper.expectElementText(
            elem,
            '.unlockable-admin-section [name="diploma_generation"]',
            `Diploma Generation: ${schedulableItem.diplomaGeneration.getTime()}`,
        );

        // admission rounds only sometimes supports
        Object.defineProperty(scope.schedulableItem, 'supportsAdmissionRounds', {
            value: false,
            configurable: true,
        });
        scope.$digest();
        SpecHelper.expectNoElement(elem, '.unlockable-admin-section [name="admission_round"]:eq(0)');
        Object.defineProperty(scope.schedulableItem, 'supportsAdmissionRounds', {
            value: true,
            configurable: true,
        });
        scope.schedulableItem.registrationDeadline = new Date();
        scope.$digest();
        SpecHelper.expectElementText(
            elem,
            '.unlockable-admin-section [name="admission_round"]:eq(0)',
            `Admission Round 1: Deadline: ${schedulableItem.admission_rounds[0].applicationDeadline.getTime()} – Decision: ${schedulableItem.admission_rounds[0].decisionDate.getTime()}`,
        );
    });

    describe('admission rounds', () => {
        it('should support adding an admission round', () => {
            renderAndStartEditing();
            const origLength = schedulableItem.admission_rounds.length;
            SpecHelper.click(elem, '[name="add-admission-round"]');
            expect(schedulableItem.admission_rounds.length).toBe(origLength + 1);
        });
        it('should support removing an admission round', () => {
            renderAndStartEditing();
            const origLength = schedulableItem.admission_rounds.length;
            expect(origLength > 0).toBe(true);
            SpecHelper.click(elem, '[name="remove-admission-round"]', 0);
            expect(schedulableItem.admission_rounds.length).toBe(origLength - 1);
        });
    });

    describe('id verification periods', () => {
        it('should allow for editing id verification periods only if it is supported', () => {
            renderAndStartEditing();
            Object.defineProperty(schedulableItem, 'supportsIdVerificationPeriods', {
                value: true,
                configurable: true,
            });
            scope.$digest();
            SpecHelper.expectElement(elem, '.id-verification-periods');
            Object.defineProperty(schedulableItem, 'supportsIdVerificationPeriods', {
                value: false,
            });
            scope.$digest();
            SpecHelper.expectNoElement(elem, '.id-verification-periods');
        });

        it('should support adding an id verification period', () => {
            Object.defineProperty(schedulableItem, 'supportsIdVerificationPeriods', {
                value: true,
                configurable: true,
            });
            renderAndStartEditing();
            const origLength = schedulableItem.id_verification_periods.length;
            SpecHelper.click(elem, '[name="add-id-verification-period"]');
            expect(schedulableItem.id_verification_periods.length).toBe(origLength + 1);
        });

        it('should support removing an id verification period', () => {
            Object.defineProperty(schedulableItem, 'supportsIdVerificationPeriods', {
                value: true,
                configurable: true,
            });
            renderAndStartEditing();
            SpecHelper.click(elem, '[name="add-id-verification-period"]');
            const origLength = schedulableItem.id_verification_periods.length;
            expect(origLength > 0).toBe(true);
            SpecHelper.click(elem, '[name="remove-id-verification-period"]', 0);
            expect(schedulableItem.id_verification_periods.length).toBe(origLength - 1);
        });
    });

    function renderAndStartEditing() {
        render();
        scope.$$editingDates = true;
        scope.$digest();
    }
});
