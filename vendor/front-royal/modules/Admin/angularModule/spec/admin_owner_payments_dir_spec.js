import 'AngularSpecHelper';
import 'Admin/angularModule';

describe('Admin::adminOwnerPayments', () => {
    let $injector;
    let renderer;
    let elem;
    let SpecHelper;
    let owner;
    let BillingTransaction;
    let billingTransactions;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Admin', 'SpecHelper');

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            SpecHelper = $injector.get('SpecHelper');
            BillingTransaction = $injector.get('BillingTransaction');
        });

        owner = {
            id: 'owner_id',
        };
    });

    describe('amount', () => {
        it('should render as expected in different circumstances', () => {
            billingTransactions = [
                {
                    id: '1',
                    amount: 200,
                    amount_refunded: 0,
                },
                {
                    id: '2',
                    amount: 200,
                    amount_refunded: 100,
                },
            ];
            render();
            SpecHelper.expectElementText(elem, 'tr[data-id=1] td[data-id=amount]', '$200');
            SpecHelper.expectElementText(elem, 'tr[data-id=2] td[data-id=amount]', '$200 ( - $100)');
        });
    });

    function render() {
        BillingTransaction.expect('index').returns(billingTransactions);
        renderer = SpecHelper.renderer();
        renderer.scope.owner = owner;
        renderer.render('<admin-owner-payments owner="owner"></admin-owner-payments>');
        elem = renderer.elem;
        // scope = elem.isolateScope();
        $injector.get('$timeout').flush(); // make sure index call is sent
        BillingTransaction.flush('index');
    }
});
