import 'AngularSpecHelper';
import 'Admin/angularModule';
import 'Users/angularModule/spec/_mock/fixtures/users';

describe('Admin::ActivityTimelineDir', () => {
    let renderer;
    let elem;
    let SpecHelper;
    let $injector;
    let UserTimeline;
    let timeline;
    let scope;
    let PersistedTimelineEvent;
    let $rootScope;
    let user;
    let User;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Admin', 'SpecHelper');

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            SpecHelper = $injector.get('SpecHelper');
            UserTimeline = $injector.get('UserTimeline');
            PersistedTimelineEvent = $injector.get('PersistedTimelineEvent');
            $rootScope = $injector.get('$rootScope');
            User = $injector.get('User');
            $injector.get('UserFixtures');
        });

        timeline = UserTimeline.new({
            events: [],
        });
        user = User.fixtures.getInstance();

        SpecHelper.stubCurrentUser();
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('events', () => {
        it('should render shared elements for all events', () => {
            timeline.events = [
                {
                    category: 'something',
                    event: 'unknown',
                    time: new Date('January 1, 2018').getTime() / 1000,
                    labels: ['label1', 'label2'],
                    editor_name: 'Ed. E. Ter',
                    editor_id: 'editor_id',
                },
            ];

            render();
            const item = SpecHelper.expectElement(elem, '.item');
            SpecHelper.expectElementText(item, '.info', 'January 1, 2018 12:00am');
            SpecHelper.expectElements(item, '.label', 2);
            SpecHelper.expectElementText(item, '.label', 'label1', 0);
            SpecHelper.expectElementText(item, '.label', 'label2', 1);
            SpecHelper.expectElementText(item, '.editor', 'Ed. E. Ter');
            SpecHelper.expectNoElement(item, '.editor button'); // Only notes show this image
        });

        it('should show the editor as Applicant if necessary', () => {
            timeline.events = [
                {
                    category: 'something',
                    event: 'unknown',
                    time: new Date('January 1, 2018').getTime() / 1000,
                    editor_name: 'App. Le. Kant',
                    editor_id: user.id,
                },
            ];

            render();
            const item = SpecHelper.expectElement(elem, '.item');
            SpecHelper.expectElementText(item, '.editor', 'Applicant');
        });

        it('should sort events using the time and secondary_sort', () => {
            timeline.events = [
                {
                    category: 'a',
                    event: 'unknown',
                    time: new Date('January 1, 2018').getTime() / 1000,
                    secondary_sort: 0,
                },
                {
                    category: 'b',
                    event: 'unknown',
                    time: new Date('January 2, 2018').getTime() / 1000,
                    secondary_sort: 1,
                },
                {
                    category: 'c',
                    event: 'unknown',
                    time: new Date('January 2, 2018').getTime() / 1000,
                    secondary_sort: 0,
                },
            ];

            render();
            SpecHelper.expectElementHasClass(elem.find('.item:eq(0)'), 'b');
            SpecHelper.expectElementHasClass(elem.find('.item:eq(1)'), 'c');
            SpecHelper.expectElementHasClass(elem.find('.item:eq(2)'), 'a');
        });

        it('should render creation event', () => {
            timeline.events = [
                {
                    category: 'something',
                    event: 'creation',
                },
            ];

            render();
            SpecHelper.expectElementText(elem, '.item .for-event', 'created');
        });

        it('should render deletion event', () => {
            timeline.events = [
                {
                    category: 'something',
                    event: 'deletion',
                },
            ];

            render();
            SpecHelper.expectElementText(elem, '.item .for-event', 'deleted');
        });

        it('should render modification event', () => {
            timeline.events = [
                {
                    category: 'something',
                    event: 'modification',

                    // For now, we only support a single change in an event.  Eventually,
                    // we may want to change this, so the data model is already setup for
                    // multiple changes in a single event
                    changes: [
                        {
                            attr: 'my-attr',
                            from: 'a',
                            to: 'b',
                        },
                    ],
                },
            ];

            render();
            SpecHelper.expectElementText(elem, '.item .for-event', 'my-attr changed from a to b');
        });

        it('should render action event', () => {
            timeline.events = [
                {
                    category: 'something',
                    event: 'performed_action',
                    text: 'ate 42 hot dogs',
                    subtext: 'with reason: I could not tell you',
                },
            ];

            render();
            SpecHelper.expectElementText(
                elem,
                '.item .for-event',
                'performed action ate 42 hot dogs with reason: I could not tell you',
            );
        });

        it('should render note event', () => {
            timeline.events = [
                {
                    category: 'something',
                    event: 'note',
                    text: 'lemme tell you what happened',
                },
            ];

            render();
            SpecHelper.expectElementText(elem, '.item .for-event', 'lemme tell you what happened');
            SpecHelper.expectElement(elem, '.editor button'); // Only notes show this image
        });
    });

    describe('notes', () => {
        it('should support adding a note', () => {
            render();
            PersistedTimelineEvent.expect('create').returns({
                id: 'newId',
            });
            SpecHelper.updateTextArea(elem, '[name="note"]', 'my note');
            SpecHelper.click(elem, '[name="add-note"]');

            const newEvent = timeline.events[0];
            expect(newEvent.category).toEqual('note');
            expect(newEvent.event).toEqual('note');
            expect(newEvent.text).toEqual('my note');
            expect(newEvent.editor_name).toEqual($rootScope.currentUser.name);
            expect(new Date().getTime() / 1000 - newEvent.time < 1).toBe(true);

            // assert that it has shown up on the screen
            SpecHelper.expectElementText(elem, '.item .for-event', 'my note');

            expect(newEvent.id).toBeUndefined();
            SpecHelper.expectElementDisabled(elem, '.editor button');
            PersistedTimelineEvent.flush('create');
            SpecHelper.expectElementEnabled(elem, '.editor button');
            expect(newEvent.id).toEqual('newId');
        });

        it('should support adding a note at a specific time', () => {
            render();
            PersistedTimelineEvent.expect('create');
            SpecHelper.updateTextArea(elem, '[name="note"]', 'my note');
            scope.noteTime = new Date('2018-01-01 12:42');
            SpecHelper.click(elem, '[name="add-note"]');

            const newEvent = timeline.events[0];
            expect(newEvent.time).toEqual(new Date('2018-01-01 12:42').getTime() / 1000);
        });

        it('should support removing notes', () => {
            timeline.events = [
                {
                    id: 'id',
                    category: 'something',
                    event: 'note',
                    text: 'lemme tell you what happened',
                },
            ];
            render();
            PersistedTimelineEvent.expect('destroy');
            SpecHelper.click(elem, '.editor button');

            expect(timeline.events).toEqual([]);

            // assert that it has been removed from the screen
            SpecHelper.expectNoElement(elem, '.item .for-event');
        });
    });

    describe('filters', () => {
        it('should support filtering by category', () => {
            timeline.events = [
                {
                    category: 'cohort_application',
                    event: 'creation',
                },
                {
                    category: 'note',
                    event: 'note',
                },
            ];

            render();
            SpecHelper.expectElement(elem, '.item.cohort_application');
            SpecHelper.expectElement(elem, '.item.note');

            SpecHelper.updateSelect(elem, '[name="filter"]', 'note');
            SpecHelper.expectNoElement(elem, '.item.cohort_application');
            SpecHelper.expectElement(elem, '.item.note');
        });
    });

    function render() {
        UserTimeline.expect('show').returns(timeline);
        renderer = SpecHelper.renderer();
        renderer.scope.user = user;
        renderer.render('<activity-timeline user="user"></activity-timeline>');
        elem = renderer.elem;
        scope = elem.isolateScope();

        SpecHelper.expectElement(elem, 'front-royal-spinner');
        UserTimeline.flush('show');
        SpecHelper.expectNoElement(elem, 'front-royal-spinner');
        timeline = scope.timeline; // grab the clone off of the scope
    }
});
