import 'AngularSpecHelper';
import 'Admin/angularModule';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';

describe('Admin::EditScheduleBasicInfo', () => {
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let Cohort;
    let schedulableItem;
    beforeEach(() => {
        angular.mock.module('FrontRoyal.Admin', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            Cohort = $injector.get('Cohort');

            $injector.get('CohortFixtures');
        });

        schedulableItem = Cohort.fixtures.getInstance();

        SpecHelper.stubCurrentUser('admin');
        SpecHelper.stubDirective('adminDatetimepicker');
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.schedulableItem = schedulableItem;
        renderer.render('<edit-schedule-basic-info schedulable-item="schedulableItem"></edit-schedule-basic-info>');
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    it('should be expanded initially if there is no name', () => {
        schedulableItem.name = undefined;
        render();
        SpecHelper.expectHasClass(elem, '.unlockable-admin-section', 'editing');
    });

    it('should show as incomplete if it is', () => {
        Object.defineProperty(schedulableItem, 'incomplete', {
            value: true,
        });
        render();
        SpecHelper.expectHasClass(elem, '.unlockable-admin-section', 'incomplete');
    });

    it('should allow for opening and closing', () => {
        render();
        scope.$$editingBasics = false;
        scope.$digest(); // mock out any logic about whether this is initially expanded
        SpecHelper.expectDoesNotHaveClass(elem, '.unlockable-admin-section', 'editing');
        SpecHelper.click(elem, '.unlockable-admin-section');
        SpecHelper.expectHasClass(elem, '.unlockable-admin-section', 'editing');
        SpecHelper.click(elem, '[name="close-basics"]');
        SpecHelper.expectDoesNotHaveClass(elem, '.unlockable-admin-section', 'editing');
    });

    it('should allow for editing', () => {
        render();
        scope.$$editingBasics = true;
        scope.$digest();

        SpecHelper.updateInput(elem, '.unlockable-admin-section [name="name"]', 'new name');
        expect(schedulableItem.name).toBe('new name');

        SpecHelper.updateSelect(elem, '.unlockable-admin-section [name="program_type"]', 'emba');
        expect(schedulableItem.program_type).toBe('emba');
        SpecHelper.updateSelect(elem, '.unlockable-admin-section [name="program_type"]', 'mba');
        expect(schedulableItem.program_type).toBe('mba');

        SpecHelper.updateInput(elem, '.unlockable-admin-section [name="description"]', 'new description');
        expect(schedulableItem.description).toBe('new description');
    });

    it('should only allow for editing title if supported', () => {
        Object.defineProperty(schedulableItem, 'supportsTitle', {
            value: false,
            configurable: true,
        });

        render();
        scope.$$editingBasics = true;
        scope.$digest();

        SpecHelper.expectNoElement(elem, '.unlockable-admin-section [name="title"]');

        Object.defineProperty(schedulableItem, 'supportsTitle', {
            value: true,
            configurable: true,
        });
        scope.$digest();
        SpecHelper.updateInput(elem, '.unlockable-admin-section [name="title"]', 'new title');
        expect(schedulableItem.title).toBe('new title');
    });

    it('should only allow for editing enrollment_agreement_template if supported', () => {
        Object.defineProperty(schedulableItem, 'supportsEnrollmentAgreement', {
            value: false,
            configurable: true,
        });

        render();
        scope.$$editingBasics = true;
        scope.$digest();

        SpecHelper.expectNoElement(elem, '.unlockable-admin-section [name="enrollment_agreement_template_id"]');
        expect(scope.signNowTemplateUrl).toBeUndefined();

        Object.defineProperty(schedulableItem, 'supportsEnrollmentAgreement', {
            value: true,
            configurable: true,
        });
        scope.$digest();

        SpecHelper.updateInput(
            elem,
            '.unlockable-admin-section [name="enrollment_agreement_template_id"]',
            'new_template_id',
        );
        expect(schedulableItem.enrollment_agreement_template_id).toBe('new_template_id');
        expect(scope.signNowTemplateUrl).toBe('https://app.signnow.com/webapp/document/new_template_id');
    });

    it('should only allow for editing isolated_network if supported', () => {
        jest.spyOn(schedulableItem, 'supportsIsolatedNetwork', 'get').mockReturnValue(false);
        render();
        scope.$$editingBasics = true;
        scope.$digest();
        SpecHelper.expectNoElement(elem, '.unlockable-admin-section [name="isolated_network"]');

        jest.spyOn(schedulableItem, 'supportsIsolatedNetwork', 'get').mockReturnValue(true);
        scope.$digest();
        SpecHelper.checkCheckbox(elem, 'input[name="isolated_network"]');
        expect(schedulableItem.isolated_network).toBe(true);
    });

    it('should show basic details when closed', () => {
        render();
        scope.$$editingBasics = false;
        scope.signNowTemplateUrl = 'http://some.template.url';
        scope.$digest();

        SpecHelper.expectElementText(elem, '.unlockable-admin-section [name="name"]', `Name: ${schedulableItem.name}`);
        SpecHelper.expectElementText(
            elem,
            '.unlockable-admin-section [name="program_type"]',
            `Program Type: ${schedulableItem.program_type}`,
        );
        SpecHelper.expectElementText(
            elem,
            '.unlockable-admin-section [name="description"]',
            `Description: ${schedulableItem.description}`,
        );
        SpecHelper.expectElementText(
            elem,
            '.unlockable-admin-section [name="enrollment_agreement_template"]',
            'Enrollment Agreement Template: Link',
        );

        Object.defineProperty(schedulableItem, 'supportsTitle', {
            value: false,
            configurable: true,
        });
        scope.$digest();
        SpecHelper.expectNoElement(elem, '.unlockable-admin-section [name="title"]');

        Object.defineProperty(schedulableItem, 'supportsTitle', {
            value: true,
            configurable: true,
        });
        scope.$digest();
        SpecHelper.expectElementText(
            elem,
            '.unlockable-admin-section [name="title"]',
            `Title: ${schedulableItem.title}`,
        );

        Object.defineProperty(schedulableItem, 'supportsExercises', {
            value: true,
            configurable: true,
        });
    });
});
