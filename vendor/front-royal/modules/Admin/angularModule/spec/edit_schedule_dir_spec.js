import moment from 'moment-timezone';
import 'AngularSpecHelper';
import 'Admin/angularModule';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';

describe('Admin::EditSchedule', () => {
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let Cohort;
    let schedulableItem;
    let Stream;
    let Playlist;
    let Period;
    let $timeout;
    let Group;
    let $injector;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Admin', 'SpecHelper');

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            SpecHelper = $injector.get('SpecHelper');
            Cohort = $injector.get('Cohort');
            Stream = $injector.get('Lesson.Stream');
            Playlist = $injector.get('Playlist');
            Period = $injector.get('Period');
            $timeout = $injector.get('$timeout');
            Group = $injector.get('Group');

            $injector.get('CohortFixtures');
        });

        const defaults = {
            program_type: 'mba',
            start_date: 1,
            periods: [Period.new(), Period.new()],
            specializations: [],
        };

        schedulableItem = Cohort.fixtures.getInstance(defaults);

        const lessonStreams = [
            Stream.new({
                id: '1',
                locale_pack: {
                    id: 'foo_id',
                    groups: [
                        Group.new({
                            name: 'FOO',
                        }),
                    ],
                },
            }),
            Stream.new({
                id: '2',
                locale_pack: {
                    id: 'zombo_id',
                    groups: [
                        Group.new({
                            name: 'ZOMBO',
                        }),
                    ],
                },
            }),
        ];

        Stream.expect('index').returns(lessonStreams);
        Playlist.expect('index');
        SpecHelper.stubCurrentUser('admin');
        SpecHelper.stubDirective('localePackSelectize');
        SpecHelper.stubDirective('adminDatetimepicker');
        SpecHelper.stubDirective('editScheduleBasicInfo');
        SpecHelper.stubDirective('editScheduleDates');
        SpecHelper.stubDirective('itemGroupsEditor');
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.schedulableItem = schedulableItem;
        renderer.scope.calendarService = {
            initializeCalendar() {},
            gotoDate() {},
            refetchEvents() {},
            setOption() {},
        };
        renderer.scope.availableLearnerProjects = [
            {
                id: 1,
            },
            {
                id: 2,
            },
        ];
        renderer.render(
            '<edit-schedule schedulable-item="schedulableItem" calendar-service="calendarService" available-learner-projects-json="availableLearnerProjects"></edit-schedule>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    describe('cohort-level projects', () => {
        it('should include edit-project-resources and pass through projects', () => {
            render();
            expect(scope.availableLearnerProjects).not.toBeUndefined();
            const editProjectResources = SpecHelper.expectElement(elem, '.learner-projects edit-project-resources');
            expect(editProjectResources.isolateScope().availableLearnerProjects).toEqual(
                scope.availableLearnerProjects,
            );
            expect(editProjectResources.isolateScope().ngModel).toEqual(schedulableItem);
        });
    });

    describe('curriculum section', () => {
        it('should toggle readonly mode correctly', () => {
            render();
            SpecHelper.expectElement(elem, '[name="curriculum"].unlockable-admin-section:not(.editing)');
            SpecHelper.expectNoElement(elem, '[name="curriculum"].unlockable-admin-section.editing');
            SpecHelper.click(elem, '[name="curriculum"].unlockable-admin-section:not(.editing)');
            SpecHelper.expectNoElement(elem, '[name="curriculum"].unlockable-admin-section:not(.editing)');
            SpecHelper.expectElement(elem, '[name="curriculum"].unlockable-admin-section.editing');

            // Close and check again
            SpecHelper.click(elem, '[name="close-curriculum"]');
            SpecHelper.expectElement(elem, '[name="curriculum"].unlockable-admin-section:not(.editing)');
            SpecHelper.expectNoElement(elem, '[name="curriculum"].unlockable-admin-section.editing');
        });

        it('should toggle specialization section', () => {
            schedulableItem = Cohort.fixtures.getInstance({
                program_type: 'emba',
                start_date: 1,
                periods: [Period.new(), Period.new()],
                specializations: [], // we want to start with none for this test
            });

            render();
            SpecHelper.toggleCheckbox(elem, '[name="curriculum"].unlockable-admin-section');

            SpecHelper.expectNoElement(elem, '[name="specialization-playlists"]');
            SpecHelper.expectNoElement(elem, '[name="specializations-required"]');

            SpecHelper.toggleCheckbox(elem, '[name="has-specializations"]');

            SpecHelper.expectElement(elem, '[name="specialization-playlists"]');
            SpecHelper.expectElement(elem, '[name="specializations-required"]');
        });

        it('should hide specialization option for unsupported program types', () => {
            const programTypes = _.union('career_network_only', Cohort.CERTIFICATE_PROGRAM_TYPES);
            _.each(programTypes, programType => {
                assertSpecializationsOptionHiddenForProgramType(programType);
            });
        });

        function assertSpecializationsOptionHiddenForProgramType(programType) {
            schedulableItem = Cohort.fixtures.getInstance({
                program_type: programType,
                start_date: 1,
                periods: [Period.new(), Period.new()],
            });

            render();
            SpecHelper.click(elem, '[name="curriculum"].unlockable-admin-section');

            SpecHelper.expectNoElement(elem, '[name="has-specializations"]');
            SpecHelper.expectNoElement(elem, '[name="specialization-playlists"]');
            SpecHelper.expectNoElement(elem, '[name="specializations-required"]');
        }

        describe('playlist_collections section', () => {
            beforeEach(() => {
                schedulableItem.playlist_collections = [
                    {
                        title: 'Foo',
                        required_playlist_pack_ids: [],
                    },
                    {
                        title: 'Bar',
                        required_playlist_pack_ids: [],
                    },
                    {
                        title: 'Baz',
                        required_playlist_pack_ids: [],
                    },
                ];
            });

            it('should support adding a playlist collection', () => {
                render();
                SpecHelper.click(elem, '[name="curriculum"].unlockable-admin-section'); // enter into 'editing' mode

                const originalNumPlaylistCollections = schedulableItem.playlist_collections.length;
                SpecHelper.expectElements(
                    elem,
                    '[name="curriculum"] .edit-playlist-collections-container .edit-playlist-collection',
                    originalNumPlaylistCollections,
                );

                jest.spyOn(schedulableItem, 'addPlaylistCollection');
                SpecHelper.click(
                    elem,
                    '[name="curriculum"] .edit-playlist-collections-container button[name="add-playlist-collection"]',
                );
                expect(schedulableItem.addPlaylistCollection).toHaveBeenCalledWith();

                expect(schedulableItem.playlist_collections.length).toEqual(originalNumPlaylistCollections + 1);
                SpecHelper.expectElements(
                    elem,
                    '[name="curriculum"] .edit-playlist-collections-container .edit-playlist-collection',
                    originalNumPlaylistCollections + 1,
                );
            });

            it('should support removing a playlist collection', () => {
                render();
                SpecHelper.click(elem, '[name="curriculum"].unlockable-admin-section'); // enter into 'editing' mode

                SpecHelper.expectElements(
                    elem,
                    '[name="curriculum"] .edit-playlist-collections-container .edit-playlist-collection .remove-playlist-collection-container button[name="remove-playlist-collection"]',
                    3,
                );

                jest.spyOn(schedulableItem, 'removePlaylistCollection');
                const removedPlaylistCollection = schedulableItem.playlist_collections[1];
                SpecHelper.click(
                    elem,
                    '[name="curriculum"] .edit-playlist-collections-container .edit-playlist-collection:eq(1) .remove-playlist-collection-container button[name="remove-playlist-collection"]',
                );
                expect(schedulableItem.removePlaylistCollection).toHaveBeenCalledWith(removedPlaylistCollection);

                // assert that the second playlist collection was appropriately removed from the model
                expect(schedulableItem.playlist_collections.length).toEqual(2);
                expect(
                    schedulableItem.playlist_collections.find(playlistCollection => playlistCollection.title === 'Bar'),
                ).toBeUndefined();

                // assert that the UI updated appropriately to make what was the third playlist collection now the second playlist collection
                SpecHelper.expectElements(
                    elem,
                    '[name="curriculum"] .edit-playlist-collections-container .edit-playlist-collection .remove-playlist-collection-container button[name="remove-playlist-collection"]',
                    2,
                );
                SpecHelper.expectElementText(
                    elem,
                    '[name="curriculum"] .edit-playlist-collections-container .edit-playlist-collection:eq(1) .playlist-collection-header-container label',
                    '2.',
                );
                SpecHelper.expectInputValue(
                    elem,
                    '[name="curriculum"] .edit-playlist-collections-container .edit-playlist-collection:eq(1) .playlist-collection-header-container input[name="playlist-collection-title"]',
                    'Baz',
                );
                expect(schedulableItem.playlist_collections[1].title).toEqual('Baz');
            });
        });
    });

    describe('internal notes section', () => {
        let supportsExternalNotes;

        beforeEach(() => {
            supportsExternalNotes = true;
            Object.defineProperty(schedulableItem, 'supportsExternalNotes', {
                get() {
                    return supportsExternalNotes;
                },
            });
        });

        it('should be hidden if !supportsExternalNotes', () => {
            supportsExternalNotes = false;
            render();
            SpecHelper.expectNoElement(elem, '[name="internal-notes-section"]');
        });

        it('should toggle readonly mode correctly', () => {
            render();
            SpecHelper.expectElement(elem, '[name="internal-notes-section"].unlockable-admin-section:not(.editing)');
            SpecHelper.expectNoElement(elem, '[name="internal-notes-section"].unlockable-admin-section.editing');
            SpecHelper.click(elem, '[name="internal-notes-section"].unlockable-admin-section:not(.editing)');
            SpecHelper.expectNoElement(elem, '[name="internal-notes-section"].unlockable-admin-section:not(.editing)');
            SpecHelper.expectElement(elem, '[name="internal-notes-section"].unlockable-admin-section.editing');

            // Close and check again
            SpecHelper.click(elem, '[name="close-internal-notes"]');
            SpecHelper.expectElement(elem, '[name="internal-notes-section"].unlockable-admin-section:not(.editing)');
            SpecHelper.expectNoElement(elem, '[name="internal-notes-section"].unlockable-admin-section.editing');
        });

        it('should support editing notes', () => {
            render();
            SpecHelper.click(elem, '[name="internal-notes-section"].unlockable-admin-section:not(.editing)');
            SpecHelper.updateInput(elem, '[name="internal-notes-section"] [name="internal_notes"]', 'some notes');
            SpecHelper.click(elem, '[name="close-internal-notes"]');
            expect(schedulableItem.internal_notes).toEqual('some notes');
            SpecHelper.expectElementText(
                elem,
                '[name="internal-notes-section"].unlockable-admin-section:not(.editing)',
                'some notes',
            );
        });
    });

    describe('schedule section', () => {
        it('should add a period', () => {
            render();
            SpecHelper.click(elem, '.add-period-end');
            expect(schedulableItem.periods.length).toBe(3);
        });

        it('should add a period within the list', () => {
            render();
            SpecHelper.click(elem, '.add-button:eq(0)');
            expect(schedulableItem.periods.length).toBe(3);
        });

        it('should delete a period', () => {
            render();
            SpecHelper.click(elem, '.add-period-end');
            SpecHelper.click(elem, '.add-period-end');
            SpecHelper.click(elem, '.add-period-end');

            SpecHelper.click(elem, '.schedule-table tr.unlockable-admin-section:eq(0)');
            SpecHelper.click(elem, '.remove:eq(0)');
            expect(schedulableItem.periods.length).toBe(4);
        });

        it('should handle reordering periods', () => {
            schedulableItem.periods = [
                {
                    $$test: 0,
                },
                {
                    $$test: 1,
                },
                {
                    $$test: 2,
                },
            ];
            render();

            SpecHelper.click(elem, '.schedule-table tr.unlockable-admin-section:eq(1)'); // click the second period
            SpecHelper.click(elem, '.arrow-up:eq(0)');
            expect(schedulableItem.periods[0].$$test).toEqual(1); // Now $$test = 1 0 2

            SpecHelper.click(elem, '.schedule-table tr.unlockable-admin-section:eq(0)'); // click the first period
            SpecHelper.click(elem, '.arrow-down:eq(0)'); // Now $$test = 0 1 2

            SpecHelper.click(elem, '.schedule-table tr.unlockable-admin-section:eq(1)'); // click the second period
            SpecHelper.click(elem, '.arrow-down:eq(0)'); // Now $$test = 0 2 1
            expect(schedulableItem.periods[0].$$test).toEqual(0);
            expect(schedulableItem.periods[1].$$test).toEqual(2);
            expect(schedulableItem.periods[2].$$test).toEqual(1);
        });

        it('should toggle readonly on periods', () => {
            render();
            SpecHelper.expectDoesNotHaveClass(elem, '.schedule-table tr.unlockable-admin-section:eq(0)', 'editing');
            SpecHelper.click(elem, '.schedule-table tr.unlockable-admin-section:eq(0)');
            SpecHelper.expectHasClass(elem, '.schedule-table tr.unlockable-admin-section:eq(0)', 'editing');
            SpecHelper.click(elem, '[name="close-edit-period"]');
            SpecHelper.expectDoesNotHaveClass(elem, '.schedule-table tr.unlockable-admin-section:eq(0)', 'editing');
        });

        it('should toggle "Show all courses"', () => {
            schedulableItem.groups = [
                Group.new({
                    name: 'ZOMBO',
                }),
            ];

            // if we don't empty this out, the recalculateStreamFilters will blow up because we're not
            // actually mocking out the loading of playlists.  We should probably be filling this up
            // and testing it more explicitly, but punting for now
            schedulableItem.playlist_collections = [
                {
                    title: '',
                    required_playlist_pack_ids: [],
                },
            ];

            render();

            Stream.flush('index');
            scope.$digest();
            scope.recalculateStreamFilters(); // it would be nice to test this for real, but this happenes in a selectize-triggered callback, so hard in tests
            expect(scope.streamFilters[0].locale_pack_id).toEqual(['zombo_id']);

            SpecHelper.click(elem, '.schedule-table tr.unlockable-admin-section:eq(0)');
            SpecHelper.checkCheckbox(elem, '[name="show-all-courses"]');
            scope.recalculateStreamFilters();

            expect(scope.streamFilters[0].locale_pack_id).toEqual(['foo_id', 'zombo_id']);
        });

        it('should recompute date strings properly', () => {
            render();
            $timeout.flush();

            SpecHelper.click(elem, '.add-period-end');
            SpecHelper.click(elem, '.add-period-end');

            // Initial start_date
            let testDate = moment(scope.schedulableItem.start_date * 1000);

            // Default period duration is seven days
            expect(moment(scope.schedulableItem.periods[0].startDate).isSame(testDate)).toBe(true);
            expect(moment(scope.schedulableItem.periods[0].endDate).isSame(testDate.add(7, 'days'))).toBe(true);
            expect(moment(scope.schedulableItem.periods[1].startDate).isSame(testDate)).toBe(true);
            expect(moment(scope.schedulableItem.periods[1].endDate).isSame(testDate.add(7, 'days'))).toBe(true);

            // Up the first period's duration to eight
            SpecHelper.click(elem, '.schedule-table tr.unlockable-admin-section:eq(0)');
            SpecHelper.updateInput(elem, '[name="days"]:eq(0)', 8);
            testDate = moment(scope.schedulableItem.start_date * 1000).tz(scope.dateHelper.ADMIN_REF_TIMEZONE);
            expect(moment(scope.schedulableItem.periods[0].startDate).isSame(testDate)).toBe(true);
            expect(moment(scope.schedulableItem.periods[0].endDate).isSame(testDate.add(8, 'days'))).toBe(true);
            expect(moment(scope.schedulableItem.periods[1].startDate).isSame(testDate)).toBe(true);
            expect(moment(scope.schedulableItem.periods[1].endDate).isSame(testDate.add(7, 'days'))).toBe(true);

            // Increase the second period's offset by one
            SpecHelper.updateInput(elem, '[name="days-offset"]:eq(0)', 1);
            testDate = moment(scope.schedulableItem.start_date * 1000).tz(scope.dateHelper.ADMIN_REF_TIMEZONE);
            expect(moment(scope.schedulableItem.periods[0].startDate).isSame(testDate.add(1, 'days'))).toBe(true);
            expect(moment(scope.schedulableItem.periods[0].endDate).isSame(testDate.add(8, 'days'))).toBe(true);
            expect(moment(scope.schedulableItem.periods[1].startDate).isSame(testDate)).toBe(true);
            expect(moment(scope.schedulableItem.periods[1].endDate).isSame(testDate.add(7, 'days'))).toBe(true);

            // Change startDate input to +1 day
            const newStartDate = testDate.add(1, 'days');
            scope.schedulableItem.startDate = newStartDate.toDate();
            scope.$digest();
            expect(moment(scope.schedulableItem.periods[0].startDate).isSame(newStartDate.add(1, 'days'))).toBe(true);
            expect(moment(scope.schedulableItem.periods[0].endDate).isSame(newStartDate.add(8, 'days'))).toBe(true);
            expect(moment(scope.schedulableItem.periods[1].startDate).isSame(newStartDate)).toBe(true);
            expect(moment(scope.schedulableItem.periods[1].endDate).isSame(newStartDate.add(7, 'days'))).toBe(true);
        });

        it('should show date header', () => {
            render();
            $timeout.flush();

            SpecHelper.click(elem, '.add-period-end');
            SpecHelper.click(elem, '.add-period-end');

            const testDate = moment(scope.schedulableItem.start_date * 1000);
            SpecHelper.expectElementText(
                elem,
                '.period-header:eq(0)',
                `Period #1 ${testDate.format('dddd, MMMM D')} - ${testDate.add(7, 'days').format('dddd, MMMM D')}`,
            );
        });

        it('should show actions UI', () => {
            render();
            SpecHelper.click(elem, '.add-period-end');
            SpecHelper.click(elem, '.schedule-table tr.unlockable-admin-section:eq(0)');
            SpecHelper.expectElement(elem, '[name="actions-container"]');
        });

        it('should only show exam styles if period is an exam or review', () => {
            render();
            SpecHelper.click(elem, '.add-period-end');
            SpecHelper.click(elem, '.schedule-table tr.unlockable-admin-section:eq(0)');

            SpecHelper.updateSelect(elem, '[name="style-select"]', 'foo');
            SpecHelper.expectNoElement(elem, '[name="exam-style-select"]');

            SpecHelper.updateSelect(elem, '[name="style-select"]', 'exam');
            SpecHelper.expectElement(elem, '[name="exam-style-select"]');

            SpecHelper.updateSelect(elem, '[name="style-select"]', 'review');
            SpecHelper.expectElement(elem, '[name="exam-style-select"]');

            SpecHelper.updateSelect(elem, '[name="exam-style-select"]', 'final');
            expect(schedulableItem.periods[0].exam_style).toEqual('final');
        });

        it('should only show specialization styles if period is a specialization or a project', () => {
            render();
            SpecHelper.click(elem, '.add-period-end');
            SpecHelper.click(elem, '.schedule-table tr.unlockable-admin-section:eq(0)');

            SpecHelper.updateSelect(elem, '[name="style-select"]', 'foo');
            SpecHelper.expectNoElement(elem, '[name="specialization-style-select"]');

            SpecHelper.updateSelect(elem, '[name="style-select"]', 'specialization');
            SpecHelper.expectElement(elem, '[name="specialization-style-select"]');

            SpecHelper.updateSelect(elem, '[name="style-select"]', 'project');
            SpecHelper.expectElement(elem, '[name="specialization-style-select"]');

            SpecHelper.updateSelect(elem, '[name="specialization-style-select"]', 'specialization_1');
            expect(schedulableItem.periods[0].specialization_style).toEqual('specialization_1');
        });

        it('should clear exam styles when period style changes away from exam', () => {
            schedulableItem.periods[0].style = 'exam';
            schedulableItem.periods[0].exam_style = 'exam_foo';
            render();

            SpecHelper.click(elem, '.add-period-end');
            SpecHelper.click(elem, '.schedule-table tr.unlockable-admin-section:eq(0)');

            SpecHelper.updateSelect(elem, '[name="style-select"]', 'foo');
            expect(schedulableItem.periods[0].exam_style).toBeUndefined();
        });

        it('should clear project and specilization styles when period style changes away from project', () => {
            schedulableItem.periods[0].style = 'project';
            schedulableItem.periods[0].project_style = 'project_foo';
            schedulableItem.periods[0].specialization_style = 'specialization_1';
            render();

            SpecHelper.click(elem, '.add-period-end');
            SpecHelper.click(elem, '.schedule-table tr.unlockable-admin-section:eq(0)');

            SpecHelper.updateSelect(elem, '[name="style-select"]', 'foo');
            expect(schedulableItem.periods[0].project_style).toBeUndefined();
            expect(schedulableItem.periods[0].specialization_style).toBeUndefined();
        });

        it('should clear specilization styles when period style changes away from specialization', () => {
            schedulableItem.periods[0].style = 'specialization';
            schedulableItem.periods[0].specialization_style = 'specialization_1';
            render();

            SpecHelper.click(elem, '.add-period-end');
            SpecHelper.click(elem, '.schedule-table tr.unlockable-admin-section:eq(0)');

            SpecHelper.updateSelect(elem, '[name="style-select"]', 'foo');
            expect(schedulableItem.periods[0].specialization_style).toBeUndefined();
        });

        it('should recalculateStreamFilters when number of periods changes', () => {
            render();
            jest.spyOn(scope, 'recalculateStreamFilters');
            SpecHelper.click(elem, '.add-period-end');
            scope.$digest();
            expect(scope.recalculateStreamFilters).toHaveBeenCalled();
        });

        describe('exercises', () => {
            it('should allow adding and removing an exercise', () => {
                render();
                SpecHelper.click(elem, '.add-period-end');
                SpecHelper.click(elem, '.schedule-table tr.unlockable-admin-section:eq(0)');

                SpecHelper.expectElement(elem, '[name="exercises-container"]');
                SpecHelper.expectNoElement(elem, 'edit-period-exercise');
                SpecHelper.click(elem, '[name="add-exercise"]');
                SpecHelper.expectElements(elem, 'edit-period-exercise', 1);
                expect(scope.schedulableItem.periods[0].exercises.length).toBe(1);
                SpecHelper.click(elem, '[name="add-exercise"]');
                SpecHelper.expectElements(elem, 'edit-period-exercise', 2);
                expect(scope.schedulableItem.periods[0].exercises.length).toBe(2);

                SpecHelper.click(elem, '[name="remove-exercise"]:eq(0)');
                SpecHelper.expectElements(elem, 'edit-period-exercise', 1);
                expect(scope.schedulableItem.periods[0].exercises.length).toBe(1);
            });

            it('should show exercises in readonly view', () => {
                schedulableItem.periods[0].exercises = [
                    {
                        id: 'foo',
                    },
                    {
                        id: 'bar',
                    },
                ];
                render();
                jest.spyOn(scope.dateHelper, 'formattedUserFacingDateTime').mockReturnValue('fake date');
                scope.$digest();
                SpecHelper.expectElementText(elem, '.document-readonly:eq(0)', 'Exercise on fake date');
                SpecHelper.expectElements(elem, '.document-readonly', 2);
            });
        });

        describe('projects', () => {
            describe('project styles', () => {
                it('should only show project styles if period is a project', () => {
                    render();
                    SpecHelper.click(elem, '.schedule-table tr.unlockable-admin-section:eq(0)');

                    SpecHelper.updateSelect(elem, '[name="style-select"]', 'foo');
                    SpecHelper.expectNoElement(elem, '[name="period-style-select"]');

                    SpecHelper.updateSelect(elem, '[name="style-select"]', 'project');
                    SpecHelper.expectElement(elem, '[name="project-style-select"]');

                    SpecHelper.updateSelect(elem, '[name="project-style-select"]', 'foo');
                    expect(schedulableItem.periods[0].project_style).toEqual('foo');
                });

                it('should have both predefined and custom styles in the project style options', () => {
                    schedulableItem.periods[0].project_style = 'fooStyle';
                    schedulableItem.periods[1].project_style = 'barStyle';
                    render();
                    expect(_.pluck(scope.projectStyleOptions, 'title')).toContain('fooStyle');
                    expect(_.pluck(scope.projectStyleOptions, 'title')).toContain('barStyle');
                    expect(_.pluck(scope.projectStyleOptions, 'title')).toContain('mba_strategy');
                });

                it('should only show edit-project-resources if period is a project', () => {
                    const LearnerProject = $injector.get('LearnerProject');
                    render();
                    SpecHelper.click(elem, '.schedule-table tr.unlockable-admin-section:eq(0)');

                    SpecHelper.updateSelect(elem, '[name="style-select"]', 'foo');
                    SpecHelper.expectNoElement(elem, '.period-body-column edit-project-resources');

                    SpecHelper.updateSelect(elem, '[name="style-select"]', 'project');
                    const editProjectResources = SpecHelper.expectElement(
                        elem,
                        '.period-body-column edit-project-resources',
                    );
                    expect(scope.availableLearnerProjects).not.toBeUndefined();

                    const passedProjects = editProjectResources.isolateScope().availableLearnerProjects;
                    expect(_.pluck(passedProjects, 'id')).toEqual(_.pluck(scope.availableLearnerProjects, 'id'));

                    // edit-schedule is responsible for instantiating the learner projects
                    expect(passedProjects[0].isA(LearnerProject)).toBe(true);
                    expect(editProjectResources.isolateScope().ngModel).toEqual(schedulableItem.periods[0]);
                });
            });
        });

        describe('calendar', () => {
            it('should refetchEvents when period dates are recomputed', () => {
                render();
                jest.spyOn(scope.calendarService, 'refetchEvents');
                jest.spyOn($.fn, 'children').mockReturnValue($(document.createElement('div')));
                scope.computePeriodDates();
                expect(scope.calendarService.refetchEvents).toHaveBeenCalled();
            });

            it('should gotoDate when period is clicked', () => {
                render();
                jest.spyOn(scope.calendarService, 'gotoDate');
                SpecHelper.click(elem, '.schedule-table tr.unlockable-admin-section:eq(0)');
                expect(scope.calendarService.gotoDate).toHaveBeenCalled();
            });
        });
    });

    describe('billing section', () => {
        it('should not render if the item does not support billing', () => {
            jest.spyOn(schedulableItem, 'supportsRecurringPayments', 'get').mockReturnValue(false);
            render();
            SpecHelper.expectNoElement(elem, 'edit-schedule-billing');
        });

        it('should render if the item does support billing', () => {
            jest.spyOn(schedulableItem, 'supportsRecurringPayments', 'get').mockReturnValue(true);
            render();
            SpecHelper.expectElement(elem, 'edit-schedule-billing');
        });
    });
});
