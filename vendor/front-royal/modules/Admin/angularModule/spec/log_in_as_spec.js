import 'AngularSpecHelper';
import 'Admin/angularModule';
import 'Users/angularModule/spec/_mock/fixtures/users';

describe('Admin::LogInAs', () => {
    let SpecHelper;
    let $rootScope;
    let LogInAs;
    let $httpBackend;
    let $window;
    let User;
    let ngToast;
    let DialogModal;
    let HttpQueue;
    let $location;
    let ClientStorage;
    let EventLogger;
    let frontRoyalStore;
    let $timeout;
    let $q;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Admin', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            $rootScope = $injector.get('$rootScope');
            $httpBackend = $injector.get('$httpBackend');
            $window = $injector.get('$window');
            $location = $injector.get('$location');
            LogInAs = $injector.get('LogInAs');
            ngToast = $injector.get('ngToast');
            User = $injector.get('User');
            DialogModal = $injector.get('DialogModal');
            HttpQueue = $injector.get('HttpQueue');
            ClientStorage = $injector.get('ClientStorage');
            EventLogger = $injector.get('EventLogger');
            frontRoyalStore = $injector.get('frontRoyalStore');
            $timeout = $injector.get('$timeout');
            $q = $injector.get('$q');

            $injector.get('UserFixtures');

            jest.spyOn(LogInAs, 'beforeUnsettingCurrentUser').mockReturnValue($q.when());
        });
    });

    describe('when confirmed', () => {
        it('should let admins log in as another user', () => {
            SpecHelper.disableHttpQueue();
            jest.spyOn($window, 'confirm').mockReturnValue(true);
            jest.spyOn(EventLogger.prototype, 'tryToSaveBuffer').mockReturnValue($q.resolve());
            jest.spyOn(frontRoyalStore, 'flush').mockReturnValue($q.resolve());

            const user = User.fixtures.getInstance();

            $httpBackend
                .expectPOST(`${$window.ENDPOINT_ROOT}/api/users/log_in_as.json?ghost_mode=true`, {
                    record: {
                        logged_in_as_user_id: user.id,
                    },
                })
                .respond(200, {
                    data: user,
                });

            jest.spyOn(ngToast, 'create').mockImplementation(() => {});
            jest.spyOn($location, 'url').mockReturnValue('/home?foo=bar');
            LogInAs.logInAsUser(user);
            $timeout.flush(); // flush tryToSaveBuffer and flush
            $httpBackend.flush();
            expect(ngToast.create).toHaveBeenCalledWith({
                content: `Successfully logged in as ${user.email} Be careful!`,
                className: 'success',
            });
            expect($rootScope.currentUser.email).toEqual(user.email);
            expect($rootScope.currentUser.ghostMode).toEqual(true);
            expect(ClientStorage.getItem('logged_in_as')).toEqual('true');
            expect(ClientStorage.getItem('last_visited_route')).toEqual('/home?foo=bar');
            expect(LogInAs.beforeUnsettingCurrentUser).toHaveBeenCalled();
        });

        it('should hide any open dialog modal alerts', () => {
            jest.spyOn(DialogModal, 'hideAlerts').mockImplementation(() => {});
            jest.spyOn($window, 'confirm').mockReturnValue(true);
            const user = User.fixtures.getInstance();

            LogInAs.logInAsUser(user);

            expect(DialogModal.hideAlerts).toHaveBeenCalled();
        });

        it('should reset HttpQueue', () => {
            jest.spyOn(HttpQueue.prototype, 'reset').mockImplementation(() => {});
            jest.spyOn($window, 'confirm').mockReturnValue(true);
            const user = User.fixtures.getInstance();

            LogInAs.logInAsUser(user);

            expect(HttpQueue.prototype.reset).toHaveBeenCalled();
        });
    });
});
