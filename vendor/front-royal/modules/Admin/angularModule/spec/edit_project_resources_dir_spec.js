import 'AngularSpecHelper';
import 'Admin/angularModule';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';

describe('Admin::EditProjectResources', () => {
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let Cohort;
    let projects;
    let cohort;
    let LearnerProject;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Admin', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            Cohort = $injector.get('Cohort');
            $injector.get('CohortFixtures');
            LearnerProject = $injector.get('LearnerProject');
        });

        projects = [
            LearnerProject.new({
                id: '1',
            }),
            LearnerProject.new({
                id: '2',
            }),
        ];

        SpecHelper.stubDirective('multiSelect');
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render(schedulableItem) {
        renderer = SpecHelper.renderer();
        renderer.scope.ngModel = schedulableItem;
        renderer.scope.projects = projects;
        renderer.render(
            '<edit-project-resources ng-model="ngModel" available-learner-projects="projects"></edit-project-resources>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    describe('when isCohortLevel', () => {
        beforeEach(() => {
            cohort = Cohort.fixtures.getInstance({
                project_submission_email: 'project_submission@pedago.com',
            });
            Object.defineProperty(cohort, 'supportsCohortLevelProjects', {
                value: true,
            });
            render(cohort, true);
            startEditing();
        });

        it('should hide editing UI by default', () => {
            SpecHelper.expectElement(elem, '.unlockable-admin-section');
            SpecHelper.expectNoElement(elem, '.unlockable-admin-section editing');
        });

        assertSelectProjectsVisible();
    });

    describe('when !isCohortLevel', () => {
        beforeEach(() => {
            cohort = Cohort.fixtures.getInstance({
                periods: [{}],
            });
            render(cohort.periods[0], false);
            SpecHelper.expectElement(elem, '[name="project-documents-container"]');
        });

        assertSelectProjectsVisible();
    });

    function assertSelectProjectsVisible() {
        it('should allow for selecting projects', () => {
            SpecHelper.expectElement(elem, '[name="learner_project_ids"]');
        });
    }

    function startEditing() {
        scope.$$editingProjectResources = true;
        scope.$digest();
    }
});
