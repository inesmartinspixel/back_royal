import 'AngularSpecHelper';
import 'Admin/angularModule';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';
import sectionNavigationLocales from 'Navigation/locales/navigation/section_navigation-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(sectionNavigationLocales);

describe('Admin::AdminMba', () => {
    let $injector;
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let Cohort;
    let CohortPromotion;
    let cohorts;
    let $window;
    let ngToast;
    let $location;
    let searchId;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Admin', 'SpecHelper');

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            SpecHelper = $injector.get('SpecHelper');
            Cohort = $injector.get('Cohort');
            $injector.get('CohortFixtures');
            $window = $injector.get('$window');
            CohortPromotion = $injector.get('CohortPromotion');
            ngToast = $injector.get('ngToast');
            $location = $injector.get('$location');
        });

        Cohort.programTypes = [
            {
                key: 'type_a',
                label: 'Type A',
                supportsAdmissionRounds: false,
            },
            {
                key: 'type_b',
                label: 'Type B',
                supportsAdmissionRounds: false,
            },
            {
                key: 'type_c',
                label: 'Type C',
                supportsAdmissionRounds: true,
            },
        ];

        // this is normally handled by Schedulable
        Cohort.promotableProgramTypes = _.reject(
            Cohort.programTypes,
            programType => programType.supportsAdmissionRounds,
        );

        cohorts = [
            Cohort.fixtures.getInstance({
                id: 'cohort_0',
                program_type: 'type_a',
            }),
            Cohort.fixtures.getInstance({
                id: 'cohort_1',
                program_type: 'type_a',
            }),
            Cohort.fixtures.getInstance({
                id: 'cohort_2',
                program_type: 'type_b',
            }),
            Cohort.fixtures.getInstance({
                id: 'cohort_3',
                program_type: 'type_b',
            }),
        ];

        searchId = null;
        jest.spyOn($location, 'search').mockImplementation(() => ({
            id: searchId,
        }));

        SpecHelper.stubCurrentUser('admin');
        SpecHelper.stubDirective('editableThingsList');
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.render('<admin-mba section="cohorts"> </admin-mba>');
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    function renderAndFlush() {
        render();
        scope.editableThings = {
            cohorts,
            indexCallResponseMeta: {
                cohort_promotions: [
                    {
                        id: 1,
                        cohort_id: cohorts[0].id,
                        program_type: 'type_a',
                    },
                    {
                        id: 2,
                        cohort_id: cohorts[2].id,
                        program_type: 'type_b',
                    },
                ],
            },
        };
        scope.$apply();
    }

    it('should support changing the promoted cohort', () => {
        renderAndFlush();

        // there should a select for each of the promoted cohorts
        SpecHelper.expectElements(elem, '.promoted-cohort-select', 2);
        SpecHelper.assertSelectValue(elem, '[name="promoted_type_a_cohort"]', cohorts[0].id);
        SpecHelper.assertSelectValue(elem, '[name="promoted_type_b_cohort"]', cohorts[2].id);

        jest.spyOn($window, 'confirm').mockReturnValue(true);
        jest.spyOn(ngToast, 'create').mockImplementation(() => {});
        CohortPromotion.expect('save');
        SpecHelper.updateSelect(elem, '[name="promoted_type_a_cohort"]', cohorts[1].id);
        CohortPromotion.flush('save');
        expect(scope.cohortPromotionViewModels[0].cohortPromotion.cohort_id).toEqual(cohorts[1].id);
        expect(ngToast.create).toHaveBeenCalledWith({
            content: 'Promoted cohort updated',
            className: 'success',
        });

        // after creating a new cohort, it should be available for promotion
        const newCohort = Cohort.fixtures.getInstance({
            program_type: 'type_a',
        });
        scope.editableThings.cohorts.push(newCohort);
        scope.$digest();
        SpecHelper.assertSelectOptions(elem, '[name="promoted_type_a_cohort"]', [
            `string:${cohorts[0].id}`,
            `string:${cohorts[1].id}`,
            `string:${newCohort.id}`,
        ]);
        CohortPromotion.expect('save');
        SpecHelper.updateSelect(elem, '[name="promoted_type_a_cohort"]', newCohort.id);
        CohortPromotion.flush('save');
        expect(scope.cohortPromotionViewModels[0].cohortPromotion.cohort_id).toEqual(newCohort.id);
    });

    it('should rollback cohort_id if window.confirm is canceled', () => {
        renderAndFlush();
        SpecHelper.assertSelectValue(elem, '[name="promoted_type_a_cohort"]', cohorts[0].id);
        SpecHelper.assertSelectValue(elem, '[name="promoted_type_b_cohort"]', cohorts[2].id);

        jest.spyOn($window, 'confirm').mockReturnValue(false);
        jest.spyOn(ngToast, 'create').mockImplementation(() => {});
        jest.spyOn(CohortPromotion.prototype, 'save').mockImplementation(() => {});
        SpecHelper.updateSelect(elem, '[name="promoted_type_a_cohort"]', cohorts[1].id);
        expect(scope.cohortPromotionViewModels[0].cohortPromotion.cohort_id).toEqual(cohorts[0].id);
        expect(ngToast.create).not.toHaveBeenCalled();
        expect(CohortPromotion.prototype.save).not.toHaveBeenCalled();
    });
});
