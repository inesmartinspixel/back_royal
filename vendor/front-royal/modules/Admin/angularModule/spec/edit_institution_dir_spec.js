import 'AngularSpecHelper';
import 'Admin/angularModule';

describe('Admin::EditInstitutionDir', () => {
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let Institution;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Admin', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            Institution = $injector.get('Institution');
            SpecHelper.stubCurrentUser('admin');
            render();
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    it('should allow for editing a institution name', () => {
        expect(scope.institution.name).toBe('AN_INSTITUTION_NAME');
        SpecHelper.updateTextInput(elem, 'input[name="institution_name"]', 'ANOTHER_INSTITUTION_NAME');
        expect(scope.institution.name).toBe('ANOTHER_INSTITUTION_NAME');
    });

    it('should save and update the institution name when update button is clicked', () => {
        Institution.expect('save');
        SpecHelper.expectElement(elem, 'button[type="submit"]');
        // updateButton.click();
        SpecHelper.submitForm(elem);
        Institution.flush('save');
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.institution = Institution.new({
            id: 1,
            name: 'AN_INSTITUTION_NAME',
        });

        renderer.render('<edit-institution institution="institution" ></edit-institution>');
        elem = renderer.elem;
        scope = elem.isolateScope();
    }
});
