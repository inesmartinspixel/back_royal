import angularModule from 'Admin/angularModule/scripts/admin_module';
import AdminEditBillingTransaction from 'AdminEditBillingTransaction';
import { react2angular } from 'react2angular';
import template from 'Admin/angularModule/views/admin_owner_payments.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angular
    .module('FrontRoyal.Admin')
    .component(
        'adminEditBillingTransaction',
        react2angular(
            AdminEditBillingTransaction,
            ['thing', 'editableThingsListViewModel', 'passthrough'],
            ['$injector'],
        ),
    );

angularModule.directive('adminOwnerPayments', [
    '$injector',

    function factory() {
        return {
            restrict: 'E',
            templateUrl,
            scope: {
                owner: '<',
            },
            link(scope) {
                scope.$watch('owner', () => {
                    scope.billingTransactions = [];
                    if (!scope.owner) {
                        return;
                    }

                    scope.columns = [
                        {
                            prop: 'transaction_time',
                            type: 'time',
                            label: 'Time',
                        },
                        {
                            prop: 'transaction_type',
                            type: 'text',
                            label: 'Type',
                        },
                        {
                            prop: 'provider',
                            type: 'text',
                            label: 'Provider',
                        },
                        {
                            id: 'amount',
                            prop(billingTransaction) {
                                let str = `$${billingTransaction.amount}`;
                                if (billingTransaction.amount_refunded) {
                                    str = `${str} ( - $${billingTransaction.amount_refunded})`;
                                }
                                return str;
                            },
                            type: 'number',
                            label: '$',
                        },
                    ];
                });
            },
        };
    },
]);
