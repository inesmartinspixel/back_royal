import angularModule from 'Admin/angularModule/scripts/admin_module';

angularModule.factory('careerProfileCache', [
    '$injector',

    function factory($injector) {
        const CareerProfile = $injector.get('CareerProfile');

        return {
            get() {
                if (!this.promise) {
                    this._load();
                }
                return this.promise;
            },

            reset() {
                this.promise = null;
            },

            _load() {
                this.promise = CareerProfile.index({
                    view: 'career_profiles',
                    filters: {
                        can_edit_career_profile: true,
                    },
                });
            },
        };
    },
]);
