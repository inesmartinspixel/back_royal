import angularModule from 'Admin/angularModule/scripts/admin_module';
import template from 'Admin/angularModule/views/edit_schedule_billing.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('editScheduleBilling', [
    '$injector',

    function factory($injector) {
        const Cohort = $injector.get('Cohort');
        const $window = $injector.get('$window');
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                schedulableItem: '<',
                stripePlans: '<',
                stripeCoupons: '<',
            },
            link(scope, elem) {
                scope.proxy = {};

                //----------------------------
                // Program Guide
                //----------------------------

                scope.isCohortType = scope.schedulableItem && scope.schedulableItem.isA(Cohort);

                //----------------------------
                // Stripe Plans
                //----------------------------

                let stripePlansById = {};

                let existingPlansById = {};

                function updateAvailablePlans() {
                    if (!scope.stripePlans) {
                        scope.filteredStripePlans = [];
                        existingPlansById = {};
                        stripePlansById = {};
                        return;
                    }

                    // lookups
                    stripePlansById = _.indexBy(scope.stripePlans, 'id');
                    existingPlansById = _.indexBy(scope.schedulableItem.stripe_plans, 'id');

                    // filtered list
                    scope.filteredStripePlans = _.chain(scope.stripePlans)
                        .select(
                            stripePlan =>
                                !existingPlansById[stripePlan.id] &&
                                scope.schedulableItem.couldSupportStripePlan(stripePlan),
                        )
                        .map(stripePlan => ({
                            value: stripePlan.id,
                            label: scope.getStripePlanLabel(stripePlan),
                        }))
                        .value();

                    // reset selectize component
                    const selectizeElem = elem.find('[name="add_stripe_plan"]')[0];
                    if (selectizeElem) {
                        selectizeElem.selectize.clear(true); // don't trigger null vals
                    }
                }

                Object.defineProperty(scope, 'selectedStripePlanId', {
                    set(id) {
                        scope.schedulableItem.stripe_plans = scope.schedulableItem.stripe_plans || [];
                        scope.schedulableItem.stripe_plans.push(stripePlansById[id]);
                    },
                });

                scope.getStripePlanLabel = stripePlan => {
                    if (stripePlan) {
                        const friendlyFreq = stripePlan.frequency === 'bi_annual' ? 'bi-annual' : stripePlan.frequency;
                        return `${stripePlan.name} ($${stripePlan.amount / 100} ${friendlyFreq})`;
                    }
                };

                scope.removePlan = stripePlan => {
                    if (
                        $window.confirm(
                            'Are you sure you want to remove this level? It will remove any associated scholarship discounts associated with this plan.',
                        )
                    ) {
                        scope.schedulableItem.stripe_plans = _.reject(
                            scope.schedulableItem.stripe_plans,
                            plan => plan.id === stripePlan.id,
                        );
                    }
                    if (_.isEmpty(scope.schedulableItem.stripe_plans)) {
                        scope.schedulableItem.scholarship_levels = [];
                    }
                };

                scope.openPlan = stripePlan => {
                    NavigationHelperMixin.loadUrl(`https://dashboard.stripe.com/plans/${stripePlan.id}`, '_blank');
                };

                //----------------------------
                // Scholarship Levels
                //----------------------------

                const defaultCoupon = _.chain(scope.stripeCoupons)
                    .find({
                        id: 'none',
                    })
                    .clone()
                    .value();

                function updateScholarshipLevels() {
                    _.each(scope.schedulableItem.scholarship_levels, level => {
                        // remove any plan entries that no longer exist
                        _.each([level.standard, level.early], registrationPeriod => {
                            _.each(registrationPeriod, (coupon, planId) => {
                                if (!existingPlansById[planId]) {
                                    delete registrationPeriod[planId];
                                }
                            });
                        });

                        // add any entries that might be missing
                        _.each(existingPlansById, (plan, planId) => {
                            _.each([level.standard, level.early], registrationPeriod => {
                                if (!registrationPeriod[planId]) {
                                    registrationPeriod[planId] = _.clone(defaultCoupon);
                                }
                            });
                        });
                    });
                }

                scope.getPlanName = planId => {
                    const stripePlan = stripePlansById[planId];
                    return stripePlan.name;
                };

                function getPlanAndDiscountInfo(planId, coupon) {
                    const stripePlan = stripePlansById[planId];
                    let amountOffStr;
                    let amountTotal;

                    if (coupon.amount_off) {
                        amountOffStr = `$${coupon.amount_off / 100}`;
                        amountTotal = (stripePlan.amount - coupon.amount_off) / 100;
                    } else {
                        amountOffStr = `${coupon.percent_off}%`;
                        amountTotal = (stripePlan.amount - (stripePlan.amount * coupon.percent_off) / 100) / 100;
                    }

                    return {
                        name: stripePlan.name,
                        amountOffStr,
                        amountTotal,
                    };
                }

                scope.getCouponLabel = (planId, coupon, includePlan) => {
                    includePlan = angular.isDefined(includePlan) ? includePlan : true;
                    const info = getPlanAndDiscountInfo(planId, coupon);
                    return `${(includePlan ? `${info.name} = $` : '$') + info.amountTotal} per payment (${
                        info.amountOffStr
                    } off)`;
                };

                scope.filterCoupons = (planId, stripeCoupons) =>
                    _.filter(stripeCoupons, coupon => {
                        const info = getPlanAndDiscountInfo(planId, coupon);
                        return info.amountTotal >= 0;
                    });

                scope.addLevel = () => {
                    if (!scope.proxy.newLevelName) {
                        return;
                    }

                    const defaultCouponInfo = {};
                    _.each(existingPlansById, (plan, planId) => {
                        defaultCouponInfo[planId] = _.clone(defaultCoupon);
                    });

                    scope.schedulableItem.scholarship_levels = scope.schedulableItem.scholarship_levels || [];
                    scope.schedulableItem.scholarship_levels.push({
                        name: scope.proxy.newLevelName,
                        standard: defaultCouponInfo,
                        early: defaultCouponInfo,
                    });

                    scope.proxy.newLevelName = undefined;
                };

                scope.removeLevel = scholarshipLevel => {
                    scope.schedulableItem.scholarship_levels = _.reject(
                        scope.schedulableItem.scholarship_levels,
                        level => level.name === scholarshipLevel.name,
                    );
                };

                scope.reconcileCouponValues = scholarshipLevel => {
                    _.each([scholarshipLevel.standard, scholarshipLevel.early], registrationPeriod => {
                        _.each(registrationPeriod, coupon => {
                            const stripeCoupon = _.find(scope.stripeCoupons, {
                                id: coupon.id,
                            });
                            if (!stripeCoupon) {
                                throw new Error(
                                    `No coupon found for "${coupon.id}" while assigning scholarship level "${scholarshipLevel.name}"`,
                                );
                            }
                            coupon.amount_off = stripeCoupon.amount_off;
                            coupon.percent_off = stripeCoupon.percent_off;
                        });
                    });
                };

                //----------------------------
                // Watches
                //----------------------------

                scope.$watch('stripePlans', updateAvailablePlans);
                scope.$watchCollection('schedulableItem.stripe_plans', () => {
                    updateAvailablePlans();
                    updateScholarshipLevels();
                });
            },
        };
    },
]);
