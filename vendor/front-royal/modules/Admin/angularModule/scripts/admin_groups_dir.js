import angularModule from 'Admin/angularModule/scripts/admin_module';
import 'ExtensionMethods/array';
import template from 'Admin/angularModule/views/admin_groups.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('adminGroups', [
    '$injector',

    function factory($injector) {
        const Group = $injector.get('Group');
        const ngToast = $injector.get('ngToast');
        const DialogModal = $injector.get('DialogModal');
        const HasSortableColumnsMixin = $injector.get('HasSortableColumnsMixin');
        const $auth = $injector.get('$auth');
        const $window = $injector.get('$window');
        const AppHeaderViewModel = $injector.get('Navigation.AppHeader.AppHeaderViewModel');

        return {
            restrict: 'E',
            templateUrl,
            scope: {},
            link(scope) {
                HasSortableColumnsMixin.onLink(scope);

                // since we might change the group list,
                // clear the cache so it is not out of date later
                Group.clearCache();

                AppHeaderViewModel.setTitleHTML('GROUP<br>ADMIN');

                //-------------------------
                // Group Management
                //-------------------------

                scope.loadGroups = () => {
                    Group.index().then(response => {
                        const groups = response.result || [];
                        scope.groups = groups;
                    });
                };

                // invoke immediately
                scope.loadGroups();

                function rebuildCurrentUser() {
                    // authoritatively rebuild currentUser to ensure group assignment is valid
                    $auth.user.signedIn = false;
                    $auth.validateUser();
                }

                scope.createGroup = () => {
                    if (!scope.newGroup) {
                        return;
                    }

                    const groupName = scope.newGroup;

                    Group.create({
                        name: groupName,
                    }).then(response => {
                        scope.newGroup = undefined;
                        const newGroup = response.result;
                        newGroup.stream_locale_pack_ids = [];
                        scope.groups.push(newGroup);
                        ngToast.create({
                            content: `${groupName} successfully created.`,
                            className: 'success',
                        });

                        rebuildCurrentUser();
                    });
                };

                scope.editGroup = group => {
                    DialogModal.alert({
                        content: '<edit-group group="group"></edit-group>',
                        title: group.name,
                        size: 'medium',
                        scope: {
                            group,
                        },
                    });
                };

                scope.deleteGroup = group => {
                    if (!$window.confirm(`Are you sure you want to delete ${group.name}?`)) {
                        return;
                    }

                    group.destroy().then(() => {
                        Array.remove(scope.groups, group);
                        ngToast.create({
                            content: `${group.name} successfully deleted.`,
                            className: 'success',
                        });

                        rebuildCurrentUser();
                    });
                };

                scope.saveGroup = group => {
                    group.save().then(() => {
                        ngToast.create({
                            content: `${group.name} saved.`,
                            className: 'success',
                        });
                    });
                };
            },
        };
    },
]);
