import angularModule from 'Admin/angularModule/scripts/admin_module';
import template from 'Admin/angularModule/views/activity_timeline.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

import trashcanBeige from 'vectors/trashcan_beige.svg';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('activityTimeline', [
    '$injector',

    function factory($injector) {
        const $rootScope = $injector.get('$rootScope');
        const UserTimeline = $injector.get('UserTimeline');
        const PersistedTimelineEvent = $injector.get('PersistedTimelineEvent');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                section: '@',
                user: '<',
            },
            link(scope) {
                scope.trashcanBeige = trashcanBeige;

                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                scope.filter = 'all';

                scope.categories = {
                    all: 'All types',
                    cohort_application: 'Cohort Application',
                    enrollment: 'Enrollment',
                    career_profile: 'Career Profile',
                    project: 'Project',
                    student_network: 'Network',
                    note: 'Note',
                };

                scope.$watch('user', user => {
                    scope.timeline = null;
                    if (!user) {
                        return;
                    }

                    UserTimeline.show(user.id).then(response => {
                        scope.timeline = response.result;
                    });
                });

                scope.$watchCollection('timeline.events', events => {
                    scope.sortedEvents = events ? events.sort(sortEvents) : null;
                });

                scope.formattedChange = function (val) {
                    if (val === null || val === undefined) {
                        return 'blank';
                    }
                    return val;
                };

                function sortEvents(a, b) {
                    if (a.time < b.time) {
                        return 1;
                    }
                    if (a.time > b.time) {
                        return -1;
                    }
                    if (a.secondary_sort < b.secondary_sort) {
                        return 1;
                    }
                    if (a.secondary_sort > b.secondary_sort) {
                        return -1;
                    }
                    return 0;
                }
                scope.timelineClasses = function (item, nextItem) {
                    const nextTime = nextItem ? nextItem.time : item.time;
                    const daysUntilNext = (item.time - nextTime) / 60 / 60 / 24;
                    return [item.category, daysUntilNext >= 1 ? 'spacer' : undefined];
                };

                /** ********************************* */
                // notes
                /** ************************************* */
                scope.noteTime = null;

                scope.addNote = function () {
                    const obj = {
                        category: 'note',
                        event: 'note',
                        time: (scope.noteTime || new Date()).getTime() / 1000,
                        text: scope.note,

                        // editor_name is set here to populate the UI, but the server is going rely
                        // on the currently logged in user
                        editor_name: scope.currentUser.name,
                    };

                    scope.timeline.events.unshift(obj);

                    delete scope.note;
                    scope.noteTime = null;

                    PersistedTimelineEvent.create(obj, {
                        // We send up information about the user that this event relates to
                        // in the meta.  The server will create the necessary relationship
                        object: 'user',
                        object_id: scope.user.id,
                    }).then(response => {
                        obj.id = response.result.id;
                    });
                };

                scope.removeNote = function (item) {
                    scope.timeline.events = _.without(scope.timeline.events, item);

                    PersistedTimelineEvent.destroy(item.id);
                };
            },
        };
    },
]);
