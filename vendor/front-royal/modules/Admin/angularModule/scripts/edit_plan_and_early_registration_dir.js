import angularModule from 'Admin/angularModule/scripts/admin_module';
import 'FrontRoyalUiBootstrap/tooltip';
import { PROVIDERS_SELECTABLE_BY_ADMINS } from 'BillingTransaction';
import template from 'Admin/angularModule/views/edit_plan_and_early_registration.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('editPlanAndEarlyRegistration', [
    '$injector',

    function factory($injector) {
        const $window = $injector.get('$window');
        const BillingTransaction = $injector.get('BillingTransaction');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                enrollmentProxy: '<',
                stripePlans: '<',
                initiallyUnlocked: '<',
                hasPrimarySubscription: '<',
                showBillingTransactionForm: '<',
                showPaymentGracePeriodForm: '<',
                paymentGracePeriodFormRequired: '<?',

                // two-way bound since it can be edited in here
                earlyRegistrationDiscount: '=',
                stripePlanId: '=',
                billingTransaction: '=?',
                paymentGracePeriodEndAt: '=?',
            },
            link(scope) {
                scope.registeredEarlyOptions = [
                    {
                        value: 'yes',
                        label: 'Registered Early',
                    },
                    {
                        value: 'no',
                        label: 'Did not Register Early',
                    },
                ];

                scope.proxy = {
                    unlocked: scope.initiallyUnlocked || false,
                };

                scope.confirmAndUnlock = event => {
                    const message = [
                        'These values should only be changed for users who paid outside of the normal flow.',
                        'In those cases, you should choose the plan that corresponds to the price we charged them.',
                    ].join('\n\n');
                    if ($window.confirm(message)) {
                        scope.proxy.unlocked = true;
                    }
                    event.preventDefault();
                };

                // selectize cannot bind to boolean values
                Object.defineProperty(scope, 'earlyRegistrationYesNo', {
                    get() {
                        return scope.earlyRegistrationDiscount ? 'yes' : 'no';
                    },
                    set(val) {
                        scope.earlyRegistrationDiscount = val === 'yes';
                    },
                });

                scope.$watch('showBillingTransactionForm', () => {
                    if (scope.showBillingTransactionForm) {
                        // Since there are two instances of this on the parent form,
                        // we need to make sure we don't override the one that is already there.
                        scope.billingTransaction =
                            scope.billingTransaction ||
                            BillingTransaction.new({
                                provider: 'NONE',
                            });
                    } else {
                        scope.billingTransaction = undefined;
                    }
                });

                // billingTransaction properties
                Object.defineProperty(scope, 'billingTransactionAmount', {
                    get() {
                        return scope.billingTransaction && scope.billingTransaction.amount;
                    },
                    set(val) {
                        scope.billingTransaction.amount = Number(val);

                        return val;
                    },
                });

                scope.paymentProviders = [
                    {
                        name: '-- Record payment --',
                        value: 'NONE',
                    },
                ].concat(PROVIDERS_SELECTABLE_BY_ADMINS);

                scope.$watch('billingTransaction.provider', () => {
                    if (scope.billingTransaction?.provider === 'NONE') {
                        scope.billingTransaction.amount = null;
                        scope.billingTransaction.provider_transaction_id = null;
                    }
                });

                scope.clearPaymentGracePeriodEndAt = () => {
                    scope.paymentGracePeriodEndAt = null;
                };
            },
        };
    },
]);
