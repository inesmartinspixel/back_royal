import angularModule from 'Admin/angularModule/scripts/admin_module';
import template from 'Admin/angularModule/views/batch_edit_users.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('batchEditUsers', [
    '$injector',

    function factory($injector) {
        const $location = $injector.get('$location');
        const isMobileMixin = $injector.get('isMobileMixin');
        const AppHeaderViewModel = $injector.get('Navigation.AppHeader.AppHeaderViewModel');
        const BatchEditUsersViewModel = $injector.get('BatchEditUsersViewModel');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                section: '@',
            },
            link(scope) {
                isMobileMixin.onLink(scope);
                AppHeaderViewModel.setTitleHTML('Batch Edit Users');
                AppHeaderViewModel.setBodyBackground('beige');
                AppHeaderViewModel.showAlternateHomeButton = false;

                // We hide the app header on mobile on this screen
                scope.$watch('isMobile', () => {
                    AppHeaderViewModel.toggleVisibility(!scope.isMobile);
                });

                scope.viewModel = BatchEditUsersViewModel.instance;

                scope.availableSections = [
                    {
                        id: 'select-users',
                        label: '1. Select Users',
                    },
                    {
                        id: 'update-users',
                        label: '2. Update Users',
                    },
                    {
                        id: 'review-changes',
                        label: '3. Review Changes',
                    },
                ];

                function targetSectionFor(section) {
                    let targetSection;
                    const viewModel = scope.viewModel;

                    if (section === 'update-users' && !_.any(viewModel.loadedUsers)) {
                        targetSection = 'select-users';
                    } else if (
                        section === 'review-changes' &&
                        !_.any(viewModel.loadedUsers) &&
                        !_.any(viewModel.updatedUsers)
                    ) {
                        targetSection = 'select-users';
                    } else if (
                        section === 'review-changes' &&
                        !viewModel.validUpdateParams &&
                        !_.any(viewModel.updatedUsers)
                    ) {
                        targetSection = 'update-users';
                    }

                    return targetSection;
                }

                scope.isSectionActive = section => {
                    const current = $location.path().replace('/admin/batch-users/', '');
                    if (section.id === current) {
                        return true;
                    }
                };

                scope.isSectionDisabled = section => !!targetSectionFor(section.id);

                scope.$watch('section', section => {
                    if (section) {
                        scope.mobileState.expanded = false;
                    }
                });

                scope.$watchGroup(['viewModel.loadedUsers', 'viewModel.updateParams', 'section'], () => {
                    const targetSection = targetSectionFor(scope.section);
                    if (targetSection && targetSection !== scope.section) {
                        scope.viewModel.gotoSection(targetSection);
                    }
                });

                scope.mobileState = {
                    expanded: false,
                };

                scope.$on('$destroy', () => {
                    AppHeaderViewModel.toggleVisibility(true);
                });
            },
        };
    },
]);
