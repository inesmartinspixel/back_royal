import angularModule from 'Admin/angularModule/scripts/admin_module';
import template from 'Admin/angularModule/views/edit_group.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('editGroup', [
    '$injector',

    function factory($injector) {
        const ngToast = $injector.get('ngToast');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                group: '<',
            },
            link(scope) {
                scope.updateGroup = () => {
                    scope.preventUpdate = true;
                    scope.group.save().then(() => {
                        ngToast.create({
                            content: `${scope.group.name} updated.`,
                            className: 'success',
                        });

                        scope.preventUpdate = false;
                    });
                };
            },
        };
    },
]);
