import angularModule from 'Admin/angularModule/scripts/admin_module';
import template from 'Admin/angularModule/views/admin_careers.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('adminCareers', [
    '$injector',

    function factory($injector) {
        const $location = $injector.get('$location');
        const isMobileMixin = $injector.get('isMobileMixin');
        const AppHeaderViewModel = $injector.get('Navigation.AppHeader.AppHeaderViewModel');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                section: '@',
            },
            link(scope) {
                isMobileMixin.onLink(scope);
                AppHeaderViewModel.setTitleHTML('Manage Careers');
                AppHeaderViewModel.setBodyBackground('beige');
                AppHeaderViewModel.showAlternateHomeButton = false;
                scope.containerViewModel = {
                    header: '',
                };

                // We hide the app header on mobile on this screen
                scope.$watch('isMobile', () => {
                    AppHeaderViewModel.toggleVisibility(!scope.isMobile);
                });

                scope.availableSections = [
                    {
                        id: 'hiring_applications',
                        label: 'Hiring Managers',
                    },
                    {
                        id: 'candidate_lists',
                        label: 'Candidate Lists',
                    },
                    {
                        id: 'teams',
                        label: 'Teams',
                    },
                    {
                        id: 'organizations',
                        label: 'Organizations',
                    },
                    {
                        id: 'open_positions',
                        label: 'Positions',
                    },
                ];

                scope.gotoSection = section => {
                    $location.url(`/admin/careers/${section}`);
                };

                scope.isSectionActive = section => {
                    const current = $location.path().replace('/admin/careers/', '');
                    if (section.id === current) {
                        return true;
                    }
                };

                scope.getSectionLabel = () => {
                    const section = _.findWhere(scope.availableSections, {
                        id: scope.section,
                    });
                    return section.label;
                };

                scope.getSectionHeader = () => {
                    if (scope.containerViewModel && scope.containerViewModel.headerOverride) {
                        return scope.containerViewModel.headerOverride;
                    }
                    return scope.getSectionLabel();
                };

                scope.getMailtoLink = () => `mailto:${scope.containerViewModel.email}`;

                scope.getResumeLink = () => scope.containerViewModel.resumeUrl;

                scope.getLinkedInLink = () => scope.containerViewModel.linkedInUrl;

                scope.$on('$destroy', () => {
                    AppHeaderViewModel.toggleVisibility(true);
                });
            },
        };
    },
]);
