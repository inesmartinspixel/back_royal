import angularModule from 'Admin/angularModule/scripts/admin_module';
import template from 'Admin/angularModule/views/edit_schedule_basic_info.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('editScheduleBasicInfo', [
    '$injector',

    function factory($injector) {
        const Cohort = $injector.get('Cohort');
        const $rootScope = $injector.get('$rootScope');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                schedulableItem: '<',
            },
            link(scope) {
                // pre-open the editing for the basics if no name is specified
                scope.$watch('proxy.name', () => {
                    if (angular.isUndefined(scope.schedulableItem.name) || scope.schedulableItem.name.length === 0) {
                        scope.$$editingBasics = true;
                    }
                });

                scope.$watch('schedulableItem', schedulableItem => {
                    scope.isACohort = schedulableItem && schedulableItem.isA(Cohort);
                });

                scope.$watch('schedulableItem.enrollment_agreement_template_id', enrollment_agreement_template_id => {
                    if (enrollment_agreement_template_id) {
                        scope.signNowTemplateUrl = `https://app.signnow.com/webapp/document/${enrollment_agreement_template_id}`;
                    } else {
                        scope.signNowTemplateUrl = undefined;
                    }
                });

                scope.programTypeOptions = Cohort.programTypes;

                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });
            },
        };
    },
]);
