import angularModule from 'Admin/angularModule/scripts/admin_module';
import template from 'Admin/angularModule/views/edit_schedule_dates.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('editScheduleDates', [
    '$injector',

    function factory($injector) {
        const dateHelper = $injector.get('dateHelper');
        const Cohort = $injector.get('Cohort');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                schedulableItem: '<',
            },
            link(scope) {
                scope.dateHelper = dateHelper;
                scope.dateHelper = $injector.get('dateHelper');

                scope.$watch('schedulableItem', () => {
                    scope.isCohort = scope.schedulableItem && scope.schedulableItem.isA(Cohort);
                });
            },
        };
    },
]);
