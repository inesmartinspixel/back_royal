import angularModule from 'Admin/angularModule/scripts/admin_module';
import template from 'Admin/angularModule/views/add_user.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('addUser', [
    '$injector',

    function factory($injector) {
        const User = $injector.get('User');
        const ngToast = $injector.get('ngToast');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                users: '<',
                groupOptions: '<',
                globalRoleOptions: '<',
                editorRoleOptions: '<',
                institutionOptions: '<',
            },
            link(scope) {
                scope.initializeEmptyUser = () => {
                    scope.newUser = User.new();
                    scope.newUser.roles = [];
                    scope.newUser.institutions = [];
                    scope.newUser.sign_up_code = 'ADMIN';
                    scope.newUser.fallback_program_type = 'demo';

                    // set up defaults for edit_user_details_dir
                    scope.globalRoleOptions.forEach(globalRole => {
                        if (globalRole.name === 'learner') {
                            scope.newUser.globalRole = globalRole;
                        }
                    });
                };

                scope.initializeEmptyUser();

                scope.createUser = () => {
                    if (scope.newUser.institution) {
                        scope.newUser.institutions = [
                            {
                                name: scope.newUser.institution,
                            },
                        ];
                    }

                    scope.newUser.save().then(response => {
                        // update the containing list if it's available
                        if (scope.users) {
                            scope.users.push(response.result);
                        }
                        const email = scope.newUser.email;
                        ngToast.create({
                            content: `${email} created.`,
                            className: 'success',
                        });
                        scope.initializeEmptyUser();
                    });
                };
            },
        };
    },
]);
