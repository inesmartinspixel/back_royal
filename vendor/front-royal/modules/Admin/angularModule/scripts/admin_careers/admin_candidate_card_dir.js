import angularModule from 'Admin/angularModule/scripts/admin_module';
import template from 'Admin/angularModule/views/admin_careers/admin_candidate_card.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('adminCandidateCard', [
    function factory() {
        return {
            restrict: 'E',
            templateUrl,
            scope: {
                careerProfile: '<',
            },
            link() {},
        };
    },
]);
