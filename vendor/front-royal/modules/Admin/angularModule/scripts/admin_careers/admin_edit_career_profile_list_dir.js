import angularModule from 'Admin/angularModule/scripts/admin_module';
import Papa from 'papaparse';
import template from 'Admin/angularModule/views/admin_careers/admin_edit_career_profile_list.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('adminEditCareerProfileList', [
    '$injector',

    function factory($injector) {
        const User = $injector.get('User');
        const CareerProfileList = $injector.get('CareerProfileList');
        const $window = $injector.get('$window');
        const ngToast = $injector.get('ngToast');
        const CareerProfile = $injector.get('CareerProfile');
        const $filter = $injector.get('$filter');
        const listOffsetHelper = $injector.get('listOffsetHelper');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                careerProfileList: '=thing',
                goBack: '&',
                created: '&',
                destroyed: '&',
            },
            link(scope) {
                const candidateListHidden = 'Show Candidate List';
                const candidateListVisible = 'Hide Candidate List';
                scope.toggleCandidateListText = candidateListHidden;
                scope.showCandidateList = false;

                scope.listLimit = 1;
                scope.offset = 0;

                scope.setOffset = (offset, action, listLimit) => {
                    scope.offset = listOffsetHelper.setOffset(offset, action, listLimit);
                };

                const columnsForCSV = [
                    {
                        columnLabel: 'Name',
                        prop: 'name',
                    },
                    {
                        columnLabel: 'Email',
                        prop: 'email',
                    },
                    {
                        columnLabel: 'MBA Cohort',
                        prop: 'candidate_cohort_name',
                    },
                ];

                // we index the users controller to quickly get a list of all candidates available
                // to be added to the list
                const userParams = {
                    can_edit_career_profile: true,
                    select_career_profile: true,
                    do_not_create_relationships: false,
                    per_page: 99999999999,
                };
                User.index(userParams).then(response => {
                    // Extend the candidate's career profile to more accurately reflect what should be expected in the
                    // career profile JSON and for the career profile longLabel property.
                    scope.careerProfiles = _.map(response.result, candidate =>
                        _.extend(candidate.career_profile, {
                            name: candidate.name,
                            candidate_cohort_name: candidate.acceptedCohortName,
                        }),
                    );
                });

                // we index the career profile lists controller to get all of the career profiles
                // for the candidates already in the list
                const listParams = {
                    filters: {
                        name: scope.careerProfileList.name,
                    },
                };
                CareerProfileList.index(listParams).then(response => {
                    const list = _.first(response.result);
                    if (list && list.career_profiles) {
                        scope.listProfiles = list.career_profiles;
                    } else {
                        scope.listProfiles = [];
                    }
                });

                // If the order of the candidates in the list changes, update the listProfiles array
                // to reflect the new ordering of the candidates.
                scope.$watchCollection('careerProfileList.career_profile_ids', (newValue, oldValue) => {
                    if (newValue && oldValue && newValue !== oldValue) {
                        if (newValue.length > oldValue.length) {
                            const selectedProfileId = _.difference(newValue, oldValue)[0];
                            // We need to retrieve the candidate's profile because up until this point we don't
                            // don't have all of the necessary information to show their entier candidate card
                            CareerProfile.show(selectedProfileId, {
                                view: 'editable',
                            }).then(response => {
                                scope.listProfiles.push(response.result);
                            });
                        } else if (newValue.length < oldValue.length) {
                            const removedProfileId = _.difference(oldValue, newValue)[0];
                            scope.listProfiles = _.reject(
                                scope.listProfiles,
                                careerProfile => careerProfile.id === removedProfileId,
                            );
                        } else {
                            updateCandidateListOrdering();
                        }
                    }
                });

                function updateCandidateListOrdering() {
                    scope.listProfiles = _.sortBy(scope.listProfiles, careerProfile =>
                        _.indexOf(scope.careerProfileList.career_profile_ids, careerProfile.id),
                    );
                }

                scope.toggleCandidateList = () => {
                    scope.showCandidateList = !scope.showCandidateList;
                    scope.toggleCandidateListText = scope.showCandidateList
                        ? candidateListVisible
                        : candidateListHidden;
                };

                scope.exportCSV = () => {
                    if (!scope.listProfiles) {
                        return;
                    }
                    const data = [];
                    data.push(_.pluck(columnsForCSV, 'columnLabel'));
                    _.each(scope.listProfiles, careerProfile => {
                        data.push(_.map(columnsForCSV, column => careerProfile[column.prop] || ''));
                    });

                    const a = document.createElement('a');
                    document.body.appendChild(a);
                    a.style = 'display: none';

                    const blob = new Blob([Papa.unparse(data)], {
                        type: 'data:text/csv;charset=utf-8',
                    });

                    const url = window.URL.createObjectURL(blob);
                    a.href = url;
                    a.download = `${scope.careerProfileList.__iguana_type}_${scope.careerProfileList.name}_${$filter(
                        'amDateFormat',
                    )(new Date(), 'YYYY.MM.DD.HH.mm')}.csv`;
                    a.click();
                    window.URL.revokeObjectURL(url);
                };

                scope.save = () => {
                    const isNew = !scope.careerProfileList.id;
                    scope.careerProfileList.save().then(() => {
                        if (isNew && scope.created) {
                            scope.created({
                                $thing: scope.careerProfileList,
                            });
                        }

                        ngToast.create({
                            content: 'List saved',
                            className: 'success',
                        });
                    });
                };

                scope.destroy = () => {
                    if ($window.confirm('Are you sure you want to delete this list?')) {
                        scope.careerProfileList.destroy().then(() => {
                            ngToast.create({
                                content: 'List deleted',
                                className: 'success',
                            });
                            if (scope.goBack) {
                                scope.goBack();
                            }
                            if (scope.destroyed) {
                                scope.destroyed({
                                    $thing: scope.careerProfileList,
                                });
                            }
                        });
                    }
                };
            },
        };
    },
]);
