import angularModule from 'Admin/angularModule/scripts/admin_module';
import template from 'Admin/angularModule/views/admin_careers/list_hiring_managers.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('listHiringManagers', [
    '$injector',

    function factory($injector) {
        const editContentItemListMixin = $injector.get('editContentItemListMixin');

        return {
            restrict: 'E',
            templateUrl,
            scope: {},
            link(scope) {
                //-------------------------
                // Columns
                //-------------------------

                scope.displayColumns = [
                    {
                        prop: 'applied_at',
                        type: 'time',
                        label: 'Applied',
                    },
                    {
                        prop: 'updated_at',
                        type: 'time',
                        label: 'Updated',
                    },
                    {
                        prop: 'name',
                        type: 'text',
                        label: 'Name',
                    },
                    {
                        prop: 'email',
                        type: 'text',
                        label: 'Email',
                    },
                    {
                        prop: 'company_name',
                        type: 'text',
                        label: 'Company',
                    },
                    {
                        prop: 'locationString',
                        type: 'text',
                        label: 'Location',
                    },
                    {
                        prop: 'last_calculated_complete_percentage',
                        type: 'perc100',
                        label: '%',
                    },
                    {
                        prop: 'status',
                        type: 'acceptedRejected',
                        label: 'Status',
                    },
                ];

                //-------------------------
                // Filters
                //-------------------------

                scope.statusOptions = [
                    {
                        label: 'Accepted',
                        value: 'accepted',
                    },
                    {
                        label: 'Rejected',
                        value: 'rejected',
                    },
                    {
                        label: 'Pending',
                        value: 'pending',
                    },
                ];

                scope.quickFilterProperties = ['name', 'email', 'company_name', 'locationString'];

                const booleanProperties = [];
                const defaultFilters = [];

                // wire up filtering support
                editContentItemListMixin.onLink(scope, 'listHiringManagers', defaultFilters, booleanProperties);
            },
        };
    },
]);
