import angularModule from 'Admin/angularModule/scripts/admin_module';
import copyToClipboard from 'copyToClipboard';
import template from 'Admin/angularModule/views/admin_careers/admin_edit_hiring_relationship.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('adminEditHiringRelationship', [
    '$injector',

    function factory($injector) {
        const HiringRelationship = $injector.get('HiringRelationship');
        const ngToast = $injector.get('ngToast');
        const LogInAs = $injector.get('LogInAs');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                // since scope.hiringRelationship gets replaced below, it seems to make sense for this to be two-way,
                // though the way we're using this directive it doesn't really seem to matter
                hiringRelationship: '=',
                existingHiringRelationships: '<', // should be existing hiring relationships indexed in an object by the candidate's id
                allowEdit: '<?',
            },
            link(scope) {
                scope.allowEdit = angular.isDefined(scope.allowEdit) ? scope.allowEdit : true;

                resetProxy();

                scope.update = () => {
                    scope.hiringRelationshipProxy.save().then(response => {
                        scope.hiringRelationship = response.result;
                        scope.existingHiringRelationships[scope.hiringRelationship.candidate_id] =
                            scope.hiringRelationship;
                        resetProxy();
                        ngToast.create({
                            content: 'Hiring Relationship Updated',
                            className: 'success',
                        });
                    });
                };

                scope.$watch('hiringRelationship', () => {
                    scope.mailtoLink = `mailto:${scope.hiringRelationship.candidateEmail}`;
                    scope.resumeLink =
                        scope.hiringRelationship.career_profile.resume &&
                        scope.hiringRelationship.career_profile.resume.formats.original.url;
                    scope.linkedInProfileLink = scope.hiringRelationship.career_profile.li_profile_url;
                });

                scope.logInAsUser = hiringRelationship => {
                    LogInAs.logInAsUser({
                        id: hiringRelationship.candidate_id,
                        email: hiringRelationship.candidateEmail,
                    });
                };

                scope.copyCareerProfileDeepLinkUrl = () => {
                    copyToClipboard(scope.hiringRelationship.career_profile.deepLinkUrl, function onSuccess() {
                        ngToast.create({
                            content: 'Link copied to clipboard!',
                            className: 'success',
                        });
                    });
                };

                function resetProxy() {
                    scope.hiringRelationshipProxy = HiringRelationship.new(scope.hiringRelationship.asJson());
                }
            },
        };
    },
]);
