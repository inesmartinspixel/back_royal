import angularModule from 'Admin/angularModule/scripts/admin_module';
import template from 'Admin/angularModule/views/admin_careers/list_candidate_lists.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('listCandidateLists', [
    '$injector',

    function factory($injector) {
        const editContentItemListMixin = $injector.get('editContentItemListMixin');

        return {
            restrict: 'E',
            templateUrl,
            scope: {},
            link(scope) {
                //-------------------------
                // Config
                //-------------------------

                scope.fieldsForCareerProfileLists = ['id', 'name', 'role_descriptors', 'career_profile_ids'];

                //-------------------------
                // Columns
                //-------------------------

                scope.displayColumns = [
                    {
                        prop: 'created_at',
                        type: 'time',
                        label: 'Created',
                    },
                    {
                        prop: 'updated_at',
                        type: 'time',
                        label: 'Updated',
                    },
                    {
                        prop: 'name',
                        type: 'text',
                        label: 'Name',
                    },
                    {
                        prop: 'length',
                        type: 'text',
                        label: 'Length',
                    },
                    {
                        prop: 'roleNames',
                        type: 'commaSeparatedList',
                        label: 'Roles',
                    },
                ];

                //-------------------------
                // Filters
                //-------------------------

                scope.quickFilterProperties = ['name'];

                const booleanProperties = [];
                const defaultFilters = [];

                // wire up filtering support
                editContentItemListMixin.onLink(scope, 'listCandidateLists', defaultFilters, booleanProperties);
            },
        };
    },
]);
