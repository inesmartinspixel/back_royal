import angularModule from 'Admin/angularModule/scripts/admin_module';
import template from 'Admin/angularModule/views/admin_careers/admin_organization_options.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('adminOrganizationOptions', [
    '$injector',

    function factory($injector) {
        const DialogModal = $injector.get('DialogModal');
        const FormHelper = $injector.get('FormHelper');
        const HasSortableColumnsMixin = $injector.get('HasSortableColumnsMixin');
        const $location = $injector.get('$location');
        const $rootScope = $injector.get('$rootScope');
        const Locale = $injector.get('Locale');

        return {
            restrict: 'E',
            templateUrl,
            scope: {},
            link(scope) {
                //-------------------------
                // Initialization
                //-------------------------

                HasSortableColumnsMixin.onLink(scope);
                scope.params = {};
                scope.availableOrganizations = null;
                scope.$location = $location;
                scope.organizationTypeOptions = [
                    {
                        title: 'Professional',
                        identifier: 'professional_organization',
                    },
                    {
                        title: 'Educational',
                        identifier: 'educational_organization',
                    },
                ];

                scope.localeOptions = Locale.availablePrefLocales.map(locale => ({
                    label: locale.code,
                }));

                scope.params.locale = scope.localeOptions[0];

                // FIXME: Before you copy and paste this as a model for how to do server-side filtering
                // of an admin list, or any filtering at all, really.  See editable_things_list_dir.  Really,
                // we should extend editable_things_list_dir to handle the filtering complexity that this
                // and other similar directives need
                FormHelper.supportAutoSuggestOptions(scope);

                scope.search = () => {
                    scope.searching = true;

                    const limit = 100;
                    scope.availableOrganizations = null;
                    if (!scope.params.organization_type || !scope.params.locale) {
                        return;
                    }
                    scope
                        .getOptionsForType(
                            scope.params.organization_type.identifier,
                            scope.params.freetext_search_param,
                            scope.params.locale.label,
                            limit,
                            false,
                        )
                        .then(response => {
                            scope.availableOrganizations = response;
                        })
                        .catch(() => {
                            scope.availableOrganizations = null;
                        })
                        .finally(() => {
                            scope.searching = false;
                        });
                };

                // See create method in admin_edit_organization_option_dir.js
                const searchListener = $rootScope.$on('admin-organization-options:search', () => {
                    scope.search();
                });
                scope.$on('$destroy', searchListener);

                scope.editOrganization = organization => {
                    DialogModal.alert({
                        content:
                            '<admin-edit-organization-option organization="organization" available-organizations="availableOrganizations"></admin-edit-organization-option>',
                        title: 'Edit Organization',
                        size: 'normal',
                        scope: {
                            organization,
                            availableOrganizations: scope.availableOrganizations,
                        },
                    });
                };

                scope.createOrganization = () => {
                    DialogModal.alert({
                        content:
                            '<admin-edit-organization-option organization="organization" available-organizations="availableOrganizations" create-mode="true"></admin-edit-organization-option>',
                        title: 'Create Organization',
                        size: 'normal',
                        scope: {
                            organization: {
                                text: scope.params.freetext_search_param,
                                type: scope.params.organization_type.identifier,
                                locale: scope.params.locale.label,
                                suggest: true,
                            },
                            availableOrganizations: scope.availableOrganizations,
                            createMode: true,
                        },
                    });
                };

                // scope.deleteOrganization = function(organization) {

                //     if (!window.confirm('Are you sure you want to delete ' + organization.text + '?')) {
                //         return;
                //     }

                //     TODO: handle the deletion of the organization
                //     scope.deleteOganization.then(function(response) {
                //         Array.remove(scope.availableOrganizations, organization);
                //         ngToast.create({
                //             content: organization.text + ' successfully deleted.',
                //             className: 'success'
                //         });
                //     }).catch(function(response) {
                //         console.error(response);
                //     });
                // };
            },
        };
    },
]);
