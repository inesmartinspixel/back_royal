import angularModule from 'Admin/angularModule/scripts/admin_module';
import template from 'Admin/angularModule/views/admin_careers/list_open_positions.html';
import listOpenPositionsCustomActionsTemplate from 'Admin/angularModule/views/admin_careers/list_open_positions_custom_actions.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);
const listOpenPositionsCustomActionsTemplateUrl = cacheAngularTemplate(
    angularModule,
    listOpenPositionsCustomActionsTemplate,
);

angularModule.directive('listOpenPositions', [
    '$injector',

    function factory($injector) {
        const editContentItemListMixin = $injector.get('editContentItemListMixin');
        const $window = $injector.get('$window');
        const ngToast = $injector.get('ngToast');

        return {
            restrict: 'E',
            templateUrl,
            scope: {},
            link(scope) {
                scope.proxy = {
                    mode: 'list',
                };
                scope.openPositions = [];

                // We need to set this here rather than passing in true directly from the template so it's available
                // in the editContentItemListMixin, which uses this value to determine what filters should be passed
                // along to the server.
                scope.usingServerPagination = true;

                scope.indexParamsForOpenPositions = {
                    limit: 50,
                    fields: ['ADMIN_FIELDS'],
                };

                //-------------------------
                // Columns
                //-------------------------

                scope.klassName = 'OpenPosition';
                scope.displayColumns = [
                    {
                        prop: 'posted_at',
                        type: 'time',
                        label: 'Posted',
                    },
                    {
                        prop: 'last_curated_at',
                        type: 'time',
                        label: 'Last Curated',
                    },
                    {
                        prop: 'hiring_manager_email',
                        type: 'text',
                        label: 'Email',
                    },
                    {
                        prop: 'hiring_manager_company_name',
                        type: 'text',
                        label: 'Company',
                    },
                    {
                        prop: 'title',
                        type: 'text',
                        label: 'Title',
                    },
                    {
                        prop: 'locationString',
                        type: 'text',
                        label: 'Location',
                        sortable: false,
                    },
                    {
                        prop: 'num_unreviewed_and_unrejected_interests',
                        type: 'text',
                        label: 'Unreviewed',
                    },
                    {
                        prop: 'status',
                        type: 'text',
                        label: 'Status',
                    },
                    {
                        prop: 'editorAbilities',
                        type: 'custom',
                        label: 'Actions',
                        sortable: false,
                        templateUrl: listOpenPositionsCustomActionsTemplateUrl,
                        callbacks: {
                            toggleArchived: function toggleArchived(openPosition) {
                                openPosition.archived = !openPosition.archived;
                                openPosition.save().then(() => {
                                    ngToast.create({
                                        content: openPosition.archived ? 'Position archived' : 'Position unarchived',
                                        className: 'success',
                                    });
                                });
                            },
                            deleteOpenPosition: function deleteOpenPosition(openPosition) {
                                if ($window.confirm('Are you sure you want to delete this position?')) {
                                    openPosition.destroy().then(() => {
                                        ngToast.create({
                                            content: `Successfully deleted ${openPosition.title}`,
                                            className: 'success',
                                        });

                                        scope.$broadcast(`admin:${scope.klassName}Destroyed`, openPosition);
                                    });
                                }
                            },
                        },
                    },
                ];

                //-------------------------
                // Filters
                //-------------------------

                scope.statusOptions = [
                    {
                        label: 'Live',
                        value: 'live',
                    },
                    {
                        label: 'Drafted',
                        value: 'drafted',
                    },
                    {
                        label: 'Archived',
                        value: 'archived',
                    },
                ];

                const booleanProperties = [];
                const defaultFilters = [];

                // wire up filtering support
                editContentItemListMixin.onLink(
                    scope,
                    'listOpenPositions',
                    defaultFilters,
                    booleanProperties,
                    undefined,
                    true,
                );
            },
        };
    },
]);
