import angularModule from 'Admin/angularModule/scripts/admin_module';
import template from 'Admin/angularModule/views/admin_careers/admin_review_open_position.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('adminReviewOpenPosition', [
    '$injector',

    function factory($injector) {
        const $location = $injector.get('$location');
        const ClientStorage = $injector.get('ClientStorage');
        const ngToast = $injector.get('ngToast');
        const CandidatePositionInterest = $injector.get('CandidatePositionInterest');
        const listOffsetHelper = $injector.get('listOffsetHelper');
        const LogInAs = $injector.get('LogInAs');
        const $http = $injector.get('$http');
        const $window = $injector.get('$window');
        const $q = $injector.get('$q');
        const User = $injector.get('User');
        const HiringTeam = $injector.get('HiringTeam');
        const DialogModal = $injector.get('DialogModal');
        const $filter = $injector.get('$filter');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                openPosition: '=thing',
                openPositions: '=thingsOnCurrentPage',
                goBack: '&',
                gotoThing: '&',
                destroyed: '&',
            },
            link(scope) {
                scope.proxy = {
                    mode: 'review',
                    hiringManagerIdForCurationEmail: scope.openPosition.hiring_manager_id,
                };
                scope.hiringRelationshipsLoaded = false;
                scope.proxy.currentTab = 'unreviewed';

                scope.$watch('proxy.currentTab', currentTab => {
                    if (currentTab) {
                        $location.search('page', null);
                        ClientStorage.setItem('adminReviewOpenPosition_currentTab', currentTab);
                    }
                });

                scope.setProxyMode = mode => {
                    if (mode === 'edit') {
                        scope.settingProxyModeToEdit = true;
                        User.show(scope.openPosition.hiring_manager_id).then(response => {
                            scope.settingProxyModeToEdit = false;
                            scope.hiringManager = response.result;
                            scope.proxy.mode = 'edit';
                        });
                    }
                };

                scope.onSavePositionFormCallback = () => {
                    scope.proxy.mode = 'review';
                    ngToast.create({
                        content: 'Position saved',
                        className: 'success',
                    });
                };

                scope.logInAsUser = openPosition => {
                    LogInAs.logInAsUser({
                        id: openPosition.hiring_manager_id,
                        email: openPosition.hiring_manager_email,
                    });
                };

                scope.toggleArchived = openPosition => {
                    openPosition.archived = !openPosition.archived;
                    openPosition.save().then(() => {
                        ngToast.create({
                            content: openPosition.archived ? 'Position archived' : 'Position unarchived',
                            className: 'success',
                        });
                    });
                };

                scope.deleteOpenPosition = openPosition => {
                    if ($window.confirm('Are you sure you want to delete this position?')) {
                        openPosition.destroy().then(() => {
                            ngToast.create({
                                content: `Successfully deleted ${openPosition.title}`,
                                className: 'success',
                            });

                            if (scope.goBack) {
                                scope.goBack();
                            }
                            if (scope.destroyed) {
                                scope.destroyed({
                                    $thing: scope.openPosition,
                                });
                            }
                        });
                    }
                };

                scope.sendCurationEmail = () => {
                    const hiringManagerName = scope.hiringTeam
                        ? _.findWhere(scope.hiringTeam.hiring_managers, {
                              id: scope.proxy.hiringManagerIdForCurationEmail,
                          }).name
                        : scope.openPosition.hiring_manager_name;

                    if (
                        $window.confirm(`Trigger email to ${hiringManagerName} detailing interest for this position?`)
                    ) {
                        scope.disableSendCurationEmail = true;
                        const params = {
                            hiring_manager_id: scope.proxy.hiringManagerIdForCurationEmail,
                        };
                        $http
                            .get(
                                `${$window.ENDPOINT_ROOT}/api/open_positions/send_curation_email/${scope.openPosition.id}.json`,
                                {
                                    params,
                                },
                            )
                            .then(
                                response => {
                                    ngToast.create({
                                        content: 'Triggered curation email',
                                        className: 'success',
                                    });
                                    scope.openPosition.curationEmailLastTriggeredAt = new Date(
                                        response.data.contents.created_at * 1000,
                                    );
                                    scope.disableSendCurationEmail = false;
                                },
                                () => {
                                    scope.disableSendCurationEmail = false;
                                },
                            );
                    }
                };

                function rebuildPriorityByIndex(interestGroup) {
                    _.each(interestGroup, (interest, i) => {
                        interest.hiring_manager_priority = i;
                    });
                }

                function batchUpdateInterests(interestGroup, fields, meta) {
                    // We only send up a subset of fields that might have changed to reduce the chance of
                    // conflicts between admins and hiring managers, since usually this method is being
                    // called just to update the hiring_manager_priority and we do not have proper conflict
                    // handling yet. Also this makes the network request much smaller.
                    return $http.put(`${$window.ENDPOINT_ROOT}/api/candidate_position_interests/batch_update.json`, {
                        records: _.map(interestGroup, interest => _.pick(interest, fields)),
                        meta,
                    });
                }

                scope.moveAllToApproved = interests => {
                    if ($window.confirm(`Move all candidates in ${scope.proxy.currentTab} to approved?`)) {
                        scope.disableMoveAllToApproved = true;
                        const lastCuratedAt = new Date();

                        // Mark interests as reviewed and change to unseen if hidden (which unreviewed interests should be)
                        _.each(interests, interest => {
                            interest.admin_status = 'reviewed';

                            if (interest.hiring_manager_status === 'hidden') {
                                interest.hiring_manager_status = 'unseen';
                            }
                        });

                        // Recalculate the interest groups
                        getInterestGroups();

                        // Recalculate the priorities in the approved group
                        rebuildPriorityByIndex(scope.interestGroups.approved);

                        // Trigger a batch update
                        const fields = [
                            'id',
                            'updated_at',
                            'admin_status',
                            'hiring_manager_status',
                            'hiring_manager_priority',
                            'last_updated_at_by_non_admin',
                        ];
                        batchUpdateInterests(scope.interestGroups.approved, fields, {
                            last_curated_at: lastCuratedAt.getTime() / 1000,
                        }).then(
                            () => {
                                ngToast.create({
                                    content: `Moved all ${scope.proxy.currentTab} to approved`,
                                    className: 'success',
                                });
                                scope.disableMoveAllToApproved = false;
                            },
                            response => {
                                if (response.status === 409) {
                                    scope.handlePositionConflict(response);
                                }
                            },
                        );

                        // Update the position's last_curated_at
                        scope.openPosition.lastCuratedAt = lastCuratedAt;
                    }
                };

                function switchBucket(interest, from, to) {
                    // Remove the element from the interest bucket it was in
                    const fromIndex = _.findIndex(scope.interestGroups[from], _interest => interest === _interest);
                    scope.interestGroups[from].splice(fromIndex, 1);

                    // Add the element to the interest bucket it is now in
                    if (scope.interestGroups[to]) {
                        scope.interestGroups[to].push(interest);
                    } else {
                        scope.interestGroups[to] = [interest];
                    }
                }

                // On the scope for testing
                scope.moveInterest = (interest, from, to) => {
                    const lastCuratedAt = new Date();

                    // Reset priority when moving to another group
                    interest.hiring_manager_priority = null;

                    // Update the position's last_curated_at
                    scope.openPosition.lastCuratedAt = lastCuratedAt;

                    switchBucket(interest, from, to);

                    // Save the interest
                    const originalHiringManagerStatus = interest.hiring_manager_status;
                    interest
                        .save({
                            last_curated_at: lastCuratedAt.getTime() / 1000,
                        })
                        .then(
                            response => {
                                // The interest can be hidden on the server if there is a hiring relationship conflict.
                                // Detect this situation, move the interest back to hidden, and let the admin user know.
                                // See hide_if_hiring_relationship_conflict in candidate_position_interest.rb.
                                if (
                                    originalHiringManagerStatus !== 'hidden' &&
                                    response.result.hiring_manager_status === 'hidden'
                                ) {
                                    switchBucket(interest, to, 'hidden');
                                    ngToast.create({
                                        content: 'Interest hidden by server due to hiring relationship conflict',
                                        className: 'warning',
                                    });
                                    return $q.when();
                                }

                                // Recalculate and save priority if outstanding and / or approved groups changed
                                const shouldRecalculatePriorityForOutstanding =
                                    _.include([from, to], 'outstanding') &&
                                    scope.interestGroups.outstanding &&
                                    scope.interestGroups.outstanding.length > 0;
                                const shouldRecalculatePriorityForApproved =
                                    _.include([from, to], 'approved') &&
                                    scope.interestGroups.approved &&
                                    scope.interestGroups.approved.length > 0;

                                const fields = ['id', 'updated_at', 'hiring_manager_priority'];
                                if (shouldRecalculatePriorityForOutstanding && shouldRecalculatePriorityForApproved) {
                                    rebuildPriorityByIndex(scope.interestGroups.outstanding);
                                    rebuildPriorityByIndex(scope.interestGroups.approved);
                                    return batchUpdateInterests(
                                        scope.interestGroups.outstanding.concat(scope.interestGroups.approved),
                                        fields,
                                    );
                                }
                                if (shouldRecalculatePriorityForOutstanding) {
                                    rebuildPriorityByIndex(scope.interestGroups.outstanding);
                                    return batchUpdateInterests(scope.interestGroups.outstanding, fields);
                                }
                                if (shouldRecalculatePriorityForApproved) {
                                    rebuildPriorityByIndex(scope.interestGroups.approved);
                                    return batchUpdateInterests(scope.interestGroups.approved, fields);
                                }
                                return $q.when();
                            },
                            response => {
                                if (response.status === 409) {
                                    scope.handlePositionConflict(response);
                                }
                            },
                        );
                };

                // Updating any attributes on a CandidatePositionInterest that can also be updated
                // by the hiring manager or candidate has the potential to conflict. We are updating
                // `hiring_manager_status` in moveAllToApproved and moveInterest.
                // NOTE: this is on the scope for testing
                scope.handlePositionConflict = response => {
                    const updatedInterest = CandidatePositionInterest.new(
                        response.data.meta.candidate_position_interest,
                    );

                    const bucketName = getInterestBucketName(updatedInterest);
                    DialogModal.alert({
                        content: `Candidate moved to ${bucketName} because a hiring manager or candidate has already taken action`,
                    });

                    // Replace the interest with the newly updated one
                    const index = _.findIndex(scope.interests, interest => interest.id === updatedInterest.id);

                    // Hiring managers can only change hiring_manager_status, so let's avoid the complications
                    // of blowing away the current interest or any of its properties with _.extend and instead just
                    // set the three statuses that control visibility.
                    scope.interests[index].hiring_manager_status = updatedInterest.hiring_manager_status;
                    scope.interests[index].admin_status = updatedInterest.admin_status;
                    scope.interests[index].candidate_status = updatedInterest.candidate_status;

                    // Refresh the groups
                    getInterestGroups();
                };

                // On the scope for testing
                scope.setInterestPriority = (shiftingInterest, group, priority) => {
                    const indexOfShiftingInterest = _.findIndex(
                        scope.interestGroups[group],
                        _interest => shiftingInterest === _interest,
                    );

                    let moved = false;
                    if (Number.isInteger(priority) && priority !== indexOfShiftingInterest) {
                        scope.interestGroups[group].splice(indexOfShiftingInterest, 1);
                        scope.interestGroups[group].splice(priority, 0, shiftingInterest);
                        moved = true;
                    } else if (priority === 'higherPriority' && indexOfShiftingInterest > 0) {
                        scope.interestGroups[group].splice(indexOfShiftingInterest, 1);
                        scope.interestGroups[group].splice(indexOfShiftingInterest - 1, 0, shiftingInterest);
                        moved = true;
                    } else if (
                        priority === 'lowerPriority' &&
                        indexOfShiftingInterest < scope.interestGroups[group].length - 1
                    ) {
                        scope.interestGroups[group].splice(indexOfShiftingInterest, 1);
                        scope.interestGroups[group].splice(indexOfShiftingInterest + 1, 0, shiftingInterest);
                        moved = true;
                    }

                    if (moved) {
                        rebuildPriorityByIndex(scope.interestGroups[group]);
                        batchUpdateInterests(scope.interestGroups[group], [
                            'id',
                            'updated_at',
                            'hiring_manager_priority',
                        ]);
                    }
                };

                scope.$on('admin:interestAdminAction', (event, payload) => {
                    const interest = payload.interest;

                    switch (payload.action) {
                        case 'sendToOutstanding':
                            interest.admin_status = 'outstanding';
                            if (interest.hiring_manager_status === 'hidden') {
                                interest.hiring_manager_status = 'unseen';
                            }
                            scope.moveInterest(interest, payload.group, interest.admin_status);
                            break;
                        case 'sendToApproved':
                            interest.admin_status = 'reviewed';
                            if (interest.hiring_manager_status === 'hidden') {
                                interest.hiring_manager_status = 'unseen';
                            }
                            scope.moveInterest(interest, payload.group, 'approved');
                            break;
                        case 'sendToHidden':
                            interest.admin_status = 'reviewed';
                            interest.hiring_manager_status = 'hidden';
                            scope.moveInterest(interest, payload.group, 'hidden');
                            break;
                        case 'sendToUnreviewed':
                            // This makes the assumption that the interest is able to be seen again by the
                            // hiring manager since it will go back through the curation flow. In the event that
                            // this assumption is wrong, such as when someone on the hiring manager's team has a relationship
                            // with this candidate already, then the interest will just be re-marked as reviewed and hidden.
                            // See hide_if_hiring_relationship_conflict in candidate_position_interest.rb.
                            interest.admin_status = 'unreviewed';
                            interest.hiring_manager_status = 'hidden';
                            scope.moveInterest(interest, payload.group, interest.admin_status);
                            break;
                        case 'setInterestPriority':
                            scope.setInterestPriority(interest, payload.group, payload.priority);
                            break;
                        default:
                            throw 'Unsupported action';
                    }
                });

                // Interest grouping
                scope.tabs = ['unreviewed', 'outstanding', 'approved', 'saved', 'connected', 'passed', 'hidden'];

                function getInterestBucketName(interest) {
                    if (interest.candidate_status === 'rejected') {
                        // We do not load up interests that are rejected by the candidate by default. That said,
                        // there is a possibility that a candidate could 'hide' their own interest after 'liking'
                        // a position. If this happens after we've already loaded up the interest here, we'll get
                        // a 409 conflict, which is expected.
                        // We're only special casing candidate_status => 'rejected' here for the error message that
                        // happens on a conflict.
                        return 'rejected';
                    }
                    if (interest.admin_status === 'not_applicable') {
                        // This bucket is not currently used for anything. It represents interests that have been hidden
                        // due to hiring relationship conflict by backend hooks. We could consider adding
                        // an `admin_status_not` filter in the future to just not return these.
                        return 'not_applicable';
                    }
                    if (interest.admin_status === 'unreviewed') {
                        return 'unreviewed';
                    }
                    if (interest.admin_status === 'outstanding' && !interest.hiddenOrReviewedByHiringManager) {
                        return 'outstanding';
                    }
                    if (interest.admin_status === 'reviewed' && !interest.hiddenOrReviewedByHiringManager) {
                        return 'approved';
                    }
                    if (
                        _.includes(['outstanding', 'reviewed'], interest.admin_status) &&
                        interest.hiddenOrReviewedByHiringManager
                    ) {
                        if (interest.hiring_manager_status === 'saved_for_later') {
                            return 'saved';
                        }
                        if (_.includes(['invited', 'accepted'], interest.hiring_manager_status)) {
                            return 'connected';
                        }
                        if (interest.hiring_manager_status === 'rejected') {
                            return 'passed';
                        }
                        if (interest.hiring_manager_status === 'hidden') {
                            return 'hidden';
                        }
                    }
                }

                function getInterestGroups() {
                    scope.interestGroups = _.groupBy(scope.interests, interest => getInterestBucketName(interest));

                    // Be sure to order these groups like they would be ordered for the
                    // user (see candidate_list_dir.js orderBy property). This is to ensure that the
                    // priority is set correctly when being changed by admins. We only need the subset
                    // that we use here since anonymizing does not matter and we have already bucketed them
                    // based on admin_status.
                    _.each(scope.interestGroups, (interestGroup, name) => {
                        scope.interestGroups[name] = $filter('orderBy')(interestGroup, [
                            'hiring_manager_priority',
                            '-created_at',
                        ]);
                    });
                }

                // Load interests
                scope.interestsLoading = true;
                CandidatePositionInterest.index({
                    filters: {
                        open_position_id: scope.openPosition.id,
                        candidate_status_not: ['rejected'],
                    },
                    'fields[]': ['ADMIN_FIELDS'],
                }).then(response => {
                    scope.interests = response.result;
                    // add $new flag for using the orderBy in candidate_list_dir.js later. see also position_review_dir.js#setUnseenToNew
                    _.chain(scope.interests)
                        .where({
                            hiring_manager_status: 'unseen',
                            anonymized: false,
                        })
                        .each(interest => {
                            interest.$new = true;
                        });
                    getInterestGroups();
                    scope.interestsLoading = false;
                });

                // Load the hiring team if the position's hiring manager has one
                if (scope.openPosition.hiring_manager_hiring_team_id) {
                    scope.disableSendCurationEmail = true;
                    HiringTeam.show(scope.openPosition.hiring_manager_hiring_team_id, {
                        'fields[]': ['hiring_managers'],
                    }).then(response => {
                        scope.hiringTeam = response.result;
                        scope.hiringManagerOptions = _.map(scope.hiringTeam.hiring_managers, hiringManager => ({
                            value: hiringManager.id,
                            label: hiringManager.name,
                        }));
                        scope.disableSendCurationEmail = false;
                    });
                }

                // Interest list pagination
                scope.listLimit = 10;
                scope.offset = 0;
                scope.$watch('proxy.currentTab', (newVal, oldVal) => {
                    if (newVal !== oldVal) {
                        scope.listLimit = 10;
                        scope.offset = 0;
                    }
                });
                scope.getProfilesRelativeToOffset = (offset, action, listLimit) => {
                    scope.offset = listOffsetHelper.setOffset(offset, action, listLimit);
                };
            },
        };
    },
]);
