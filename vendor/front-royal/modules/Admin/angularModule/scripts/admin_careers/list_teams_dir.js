import angularModule from 'Admin/angularModule/scripts/admin_module';
import template from 'Admin/angularModule/views/admin_careers/list_teams.html';
import adminEditHiringTeamCustomActionsTemplate from 'Admin/angularModule/views/admin_careers/admin_edit_hiring_team_custom_actions.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);
const adminEditHiringTeamCustomActionsTemplateUrl = cacheAngularTemplate(
    angularModule,
    adminEditHiringTeamCustomActionsTemplate,
);

angularModule.directive('listTeams', [
    '$injector',

    function factory($injector) {
        const editContentItemListMixin = $injector.get('editContentItemListMixin');
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');

        return {
            restrict: 'E',
            templateUrl,
            scope: {},
            link(scope) {
                function goToStripe(team) {
                    const htmlElem = $('html');
                    const isProduction = !htmlElem.hasClass('local') && !htmlElem.hasClass('staging');
                    NavigationHelperMixin.loadUrl(
                        `https://dashboard.stripe.com/${isProduction ? '' : 'test/'}customers/${team.id}`,
                        '_blank',
                    );
                }

                //-------------------------
                // Columns
                //-------------------------

                scope.displayColumns = [
                    {
                        prop: 'created_at',
                        type: 'time',
                        label: 'Created',
                    },
                    {
                        prop: 'updated_at',
                        type: 'time',
                        label: 'Updated',
                    },
                    {
                        prop: 'title',
                        type: 'text',
                        label: 'Title',
                    },
                    {
                        prop: 'domain',
                        type: 'text',
                        label: 'Domain',
                    },
                    {
                        prop: 'memberCount',
                        type: 'text',
                        label: 'Member Count',
                    },
                    {
                        prop: 'primary_subscription',
                        type: 'checkIfTrue',
                        label: 'Active Subscription',
                    },
                    {
                        prop: 'activeStripePlan',
                        type: 'custom',
                        label: 'Plan Name/Open in Stripe',
                        templateUrl: adminEditHiringTeamCustomActionsTemplateUrl,
                        callbacks: {
                            goToStripe,
                            hiringTeamStripeButtonText: hiringTeam => {
                                if (hiringTeam.subscription_required) {
                                    return hiringTeam.hiring_plan;
                                }
                                return 'Free access';
                            },
                        },
                    },
                ];

                const booleanProperties = [];
                const defaultFilters = [];

                // wire up filtering support
                editContentItemListMixin.onLink(scope, 'listHiringTeams', defaultFilters, booleanProperties);
            },
        };
    },
]);
