import angularModule from 'Admin/angularModule/scripts/admin_module';
import template from 'Admin/angularModule/views/admin_careers/admin_edit_candidate_position_interest.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('adminEditCandidatePositionInterest', [
    '$injector',

    function factory($injector) {
        const ngToast = $injector.get('ngToast');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                candidatePositionInterest: '<',
            },
            link(scope) {
                scope.updateCandidatePositionInterest = params => {
                    _.extend(scope.candidatePositionInterest, params);
                    return scope.candidatePositionInterest.save(params).then(() => {
                        ngToast.create({
                            content: 'Candidate Position Interest Updated',
                            className: 'success',
                        });
                    });
                };
            },
        };
    },
]);
