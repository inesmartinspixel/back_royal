import angularModule from 'Admin/angularModule/scripts/admin_module';
import template from 'Admin/angularModule/views/admin_careers/admin_edit_hiring_team_members.html';
import adminHiringTeamStatusCustomFieldsTemplate from 'Admin/angularModule/views/admin_careers/admin_hiring_team_status_custom_fields.html';

import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);
const adminHiringTeamStatusCustomFieldsTemplateUrl = cacheAngularTemplate(
    angularModule,
    adminHiringTeamStatusCustomFieldsTemplate,
);

angularModule.directive('adminEditHiringTeamMembers', [
    '$injector',

    function factory($injector) {
        const editContentItemListMixin = $injector.get('editContentItemListMixin');
        const ngToast = $injector.get('ngToast');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                hiringTeam: '<',
            },
            link(scope) {
                // We need to set this here rather than passing in true directly from the template so it's available
                // in the editContentItemListMixin, which uses this value to determine what filters should be passed
                // along to the server.
                scope.usingServerPagination = true;

                scope.displayColumns = [
                    {
                        prop: 'name',
                        type: 'text',
                        label: 'Name',
                    },
                    {
                        prop: 'email',
                        type: 'text',
                        label: 'Email',
                    },
                    {
                        id: 'is-member',
                        prop: 'hiring_team_id',
                        type: 'custom',
                        label: 'Is member?',
                        templateUrl: adminHiringTeamStatusCustomFieldsTemplateUrl,
                        callbacks: getCallbacksForCheckboxColumn(),
                    },
                ];

                const defaultFilters = {
                    has_hiring_application: true,
                    hiring_application_status: ['pending', 'accepted'],
                };
                scope.$watch('hiringTeam.id', () => {
                    if (scope.hiringTeam && scope.hiringTeam.id) {
                        scope.indexParams = {
                            filters: defaultFilters,
                            available_for_hiring_team: scope.hiringTeam.id, // this needs to be top-level so it's not cached by editContentItemListMixin
                            per_page: 100,
                        };
                    }
                });

                function getCallbacksForCheckboxColumn() {
                    const callbacks = {};

                    callbacks.onClick = user => {
                        const originalValue = user.hiring_team_id;
                        user.hiring_team_id = user.hiring_team_id === scope.hiringTeam.id ? null : scope.hiringTeam.id;

                        user.save()
                            .then(() => {
                                // Update the hiring_manager_ids property of scope.hiringTeam to reflect the
                                // addition or removal of teammates elsewhere.
                                if (
                                    angular.isDefined(user.hiring_team_id) &&
                                    user.hiring_team_id === scope.hiringTeam.id
                                ) {
                                    scope.hiringTeam.hiring_manager_ids.push(user.id);
                                } else {
                                    scope.hiringTeam.hiring_manager_ids = _.reject(
                                        scope.hiringTeam.hiring_manager_ids,
                                        id => id === user.id,
                                    );
                                }

                                ngToast.create({
                                    content: `${user.email} has been updated`,
                                    className: 'success',
                                });
                            })
                            .catch(() => {
                                // If we errored, revert the change so the UI reflects the correct state.
                                user.hiring_team_id = originalValue;
                            });
                    };

                    callbacks.isChecked = user => user.hiring_team_id === scope.hiringTeam.id;

                    callbacks.isDisabled = user => !!user.$$saving;

                    return callbacks;
                }

                //------------------------------
                // Editable things list support
                //------------------------------
                function getDefaultFilters(filters) {
                    return _.map(filters, (filterValue, filterKey) => {
                        const filter = {
                            server: true,
                            default: true,
                            value: {},
                        };
                        filter.value[filterKey] = filterValue;
                        return filter;
                    });
                }
                const booleanProperties = [];
                const customFilterCallback = null;
                const manualFiltering = true;

                editContentItemListMixin.onLink(
                    scope,
                    'adminEditHiringTeamMembers',
                    getDefaultFilters(defaultFilters),
                    booleanProperties,
                    customFilterCallback,
                    manualFiltering,
                );
            },
        };
    },
]);
