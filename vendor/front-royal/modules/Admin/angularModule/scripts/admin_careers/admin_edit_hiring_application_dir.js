import angularModule from 'Admin/angularModule/scripts/admin_module';
import template from 'Admin/angularModule/views/admin_careers/admin_edit_hiring_application.html';
import customActionsTemplate from 'Admin/angularModule/views/admin_careers/admin_edit_hiring_application_custom_actions.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);
const customActionsTemplateUrl = cacheAngularTemplate(angularModule, customActionsTemplate);

angularModule.directive('adminEditHiringApplication', [
    '$injector',

    function factory($injector) {
        const User = $injector.get('User');
        const HiringApplication = $injector.get('HiringApplication');
        const HiringRelationship = $injector.get('HiringRelationship');
        const ngToast = $injector.get('ngToast');
        const CareerProfileList = $injector.get('CareerProfileList');
        const editContentItemListMixin = $injector.get('editContentItemListMixin');
        const CareerProfile = $injector.get('CareerProfile');
        const HiringTeam = $injector.get('HiringTeam');
        const DialogModal = $injector.get('DialogModal');
        const TranslationHelper = $injector.get('TranslationHelper');
        const $http = $injector.get('$http');
        const HttpQueue = $injector.get('HttpQueue');
        const LogInAs = $injector.get('LogInAs');
        const scopeTimeout = $injector.get('scopeTimeout');
        const dateHelper = $injector.get('dateHelper');
        const $q = $injector.get('$q');
        const OpenPosition = $injector.get('OpenPosition');
        const $location = $injector.get('$location');
        const CareerProfileFilterSet = $injector.get('CareerProfileFilterSet');
        const ClientStorage = $injector.get('ClientStorage');
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                hiringApplication: '=thing',
                goBack: '&',
            },
            link(scope) {
                NavigationHelperMixin.onLink(scope);

                scope.proxy = {};
                scope.hiringRelationshipsLoaded = false;

                // initialize the tabs
                scope.currentTab = ClientStorage.getItem('adminEditHiringApplication_currentTab') || 'career-network';

                scope.$watch('currentTab', currentTab => {
                    if (currentTab) {
                        $location.search('page', null);
                        ClientStorage.setItem('adminEditHiringApplication_currentTab', currentTab);

                        if (currentTab === 'career-network' && scope.hiringRelationshipsLoaded !== true) {
                            loadExistingHiringRelationships();
                        }
                    }
                });

                scope.batchCreatingRelationships = null;
                scope.usingServerPagination = true;
                scope.rolePlaceholderText = 'Interest Areas...';

                scope.indexParamsForUsers = {
                    select_career_profile: true,
                    available_for_relationships_with_hiring_manager: scope.hiringApplication.user_id,
                    per_page: 20,
                };

                scope.HiringApplication = HiringApplication;

                scope.dateHelper = dateHelper;

                // ------------------
                // User and Hiring Application Setup
                // ------------------

                scope.$watch('hiringApplication', onHiringApplicationChanged);
                scope.$watch('userLoaded', userLoaded => {
                    if (userLoaded && scope.currentTab === 'edit-profile') {
                        scope.gotoEditHiringApplication();
                    }
                });

                // will be called initially via the $watch above
                function onHiringApplicationChanged() {
                    setHiringApplicationProxy();

                    // we also need to load the full user here
                    if (!scope.userLoaded) {
                        refreshUser(scope.hiringApplication.user_id);
                    }
                }

                function refreshCard() {
                    scope.refreshing = true;
                    scopeTimeout(
                        scope,
                        () => {
                            scope.refreshing = false;
                        },
                        10,
                    );
                }

                function setUserProxy() {
                    if (scope.user) {
                        scope.userProxy = User.new(scope.user.asJson());
                    } else {
                        scope.userProxy = {};
                    }
                }

                function refreshUser(user_id) {
                    scope.userLoaded = false;
                    User.show(user_id).then(response => {
                        scope.user = response.result;
                        _.extend(scope.user.hiring_application, scope.hiringApplication);
                        setUserProxy();
                        scope.userLoaded = true;
                    });
                }

                function setHiringApplicationProxy() {
                    // hiringApplicationProxy is an object which holds the
                    // changes we want to make to the hiring application before we have
                    // committed them by pressing save
                    if (scope.hiringApplication) {
                        scope.hiringApplicationProxy = HiringApplication.new(scope.hiringApplication.asJson());
                    } else {
                        scope.hiringApplicationProxy = {};
                    }
                }

                scope.$watchCollection('proxy.primaryAreasOfInterest', primaryAreasOfInterest => {
                    scope.clientFilters.career_profile_roles = CareerProfileFilterSet.buildRolesFilter(
                        primaryAreasOfInterest,
                    );
                });

                scope.$watchCollection('hiringApplicationProxy', () => {
                    const hiringApplicationProxy = scope.hiringApplicationProxy;
                    const hiringApplication = scope.hiringApplication;
                    if (!hiringApplication || !hiringApplicationProxy) {
                        scope.hiringApplicationProxyIsValid = false;
                        return;
                    }

                    if (hiringApplicationProxy.status !== hiringApplication.status) {
                        scope.hiringApplicationProxyIsValid = true;
                    } else {
                        scope.hiringApplicationProxyIsValid = false;
                    }
                });

                //-----------------------
                // Edit Hiring Application Data
                //-----------------------

                scope.gotoEditHiringApplication = () => {
                    scope.applicationFormSteps =
                        scope.applicationFormSteps ||
                        _.chain($injector.get('HIRING_MANAGER_FORM_STEPS'))
                            .pluck('stepName')
                            .reject(stepName => stepName === 'preview')
                            .value();
                    scope.currentTab = 'edit-profile';

                    // wait for section to instantiate before navigating to first page
                    scopeTimeout(
                        scope,
                        () => {
                            scope.gotoEditHiringApplicationStep(0);
                        },
                        10,
                    );
                };

                scope.gotoEditHiringApplicationStep = $index => {
                    scope.activePageIndex = $index;
                    scope.$broadcast('gotoFormStep', scope.activePageIndex);
                    scope.currentStepName = scope.applicationFormSteps[scope.activePageIndex];
                };

                // listen for save events from the hiring manager profile
                scope.$on('savedHiringManagerProfile', () => {
                    // update the user and hiring applications to keep them in sync
                    _.extend(scope.user, scope.userProxy);
                    _.extend(scope.hiringApplication, scope.user.hiring_application);

                    // update the proxies
                    setUserProxy();
                    setHiringApplicationProxy();

                    // and then refresh the card
                    refreshCard();
                });

                //-----------------------
                // Hiring Relationships and Hiring Teams
                //-----------------------

                let existingRelationshipsByCareerProfileId;

                function loadExistingHiringRelationships() {
                    if (scope.hiringApplication.status !== 'rejected') {
                        HiringRelationship.index({
                            filters: {
                                hiring_manager_id: scope.hiringApplication.user_id,
                            },
                            'except[]': ['hiring_application', 'conversation'],
                        }).then(response => {
                            scope.hiringRelationshipsLoaded = true;
                            scope.existingHiringRelationshipsByCandidateId = _.indexBy(response.result, 'candidate_id');
                            existingRelationshipsByCareerProfileId = _.indexBy(
                                _.pluck(response.result, 'career_profile'),
                                'id',
                            );
                        });
                    }
                }

                function resetOriginalHiringTeams() {
                    scope.originalHiringTeams = _.filter(
                        scope.hiringTeamsProxy.hiringTeams,
                        hiringTeam => hiringTeam.id === scope.hiringApplication.hiring_team_id,
                    );
                }

                scope.hiringTeamsProxy = {};
                HiringTeam.index().then(response => {
                    scope.hiringTeamsProxy.hiringTeams = response.result;
                    resetOriginalHiringTeams();
                    scope.hiringTeamsProxy.addedHiringTeams = Array.from(scope.originalHiringTeams);
                });

                scope.$watchCollection('hiringTeamsProxy.addedHiringTeams', (newValue, oldValue) => {
                    scope.hiringTeamsProxyIsValid = false;

                    // Only take action if the user changed something; not from initialization of the collection.
                    if (
                        newValue &&
                        oldValue &&
                        !_.isEqual(scope.originalHiringTeams, scope.hiringTeamsProxy.addedHiringTeams)
                    ) {
                        const originalHiringTeams = scope.originalHiringTeams;
                        const addedHiringTeams = scope.hiringTeamsProxy.addedHiringTeams;

                        if (originalHiringTeams.length !== addedHiringTeams.length) {
                            // a hiring team was selected or removed
                            scope.hiringTeamsProxyIsValid = true;
                        } else if (addedHiringTeams[0] && originalHiringTeams[0]) {
                            // a new hiring team was created or a different hiring team was selected
                            if (
                                typeof addedHiringTeams[0] === 'string' ||
                                addedHiringTeams[0].id !== originalHiringTeams[0].id
                            ) {
                                scope.hiringTeamsProxyIsValid = true;
                            }
                        }
                    }
                });

                const careerProfileListParams = {
                    // annoying, but we need to gather up all of the career profiles as well, just so we can have access to their user id
                    'fields[]': ['id', 'name', 'career_profile_ids'],
                };
                CareerProfileList.index(careerProfileListParams).then(response => {
                    scope.careerProfileLists = response.result;
                });

                // ------------------
                // Log In As View Helper
                // ------------------

                scope.logInAsUser = hiringApplication => {
                    LogInAs.logInAsUser({
                        id: hiringApplication.user_id,
                        email: hiringApplication.email,
                    });
                };

                // ------------------
                // Batch Add Candidates View Helper
                // ------------------

                scope.batchAddCandidates = () => {
                    if (!scope.proxy.careerProfileList) {
                        return;
                    }

                    scope.batchCreatingRelationships = true;

                    const data = {
                        career_profile_list_id: scope.proxy.careerProfileList.id,
                        career_profile_ids: scope.proxy.careerProfileList.career_profile_ids,
                        hiring_manager_id: scope.hiringApplication.user_id,
                    };
                    const config = {
                        'FrontRoyal.ApiErrorHandler': {
                            skip: true,
                        },
                    };
                    $http.post(`${window.ENDPOINT_ROOT}/api/hiring_relationships/batch_create.json`, data, config).then(
                        response => {
                            _.each(response.data.contents.hiring_relationships, hiringRelationshipJson => {
                                const hiringRelationship = HiringRelationship.new(hiringRelationshipJson);
                                scope.existingHiringRelationshipsByCandidateId[
                                    hiringRelationship.candidate_id
                                ] = hiringRelationship;
                                existingRelationshipsByCareerProfileId[
                                    hiringRelationship.career_profile.id
                                ] = hiringRelationship;
                            });
                            scope.batchCreatingRelationships = false;
                        },
                        response => {
                            scope.batchCreatingRelationships = false;
                            HttpQueue.unfreezeAfterError(response.config);
                        },
                    );
                };

                // ------------------
                // Hiring Application View Helpers
                // ------------------

                scope.save = () => {
                    save().catch(catchHandler);
                };

                scope.saveAndReturn = () =>
                    save()
                        .then(() => {
                            scope.goBack();
                        })
                        .catch(catchHandler);

                function catchHandler(result) {
                    if (result instanceof Error) {
                        throw result;
                    } else if (result instanceof Object) {
                        throw new Error(JSON.stringify(result));
                    } else if (result !== 'aborted') {
                        throw new Error(result);
                    }
                }

                function save() {
                    const meta = prepareHiringTeamMeta();
                    return updateHiringApplication(meta);
                }

                function updateHiringApplication(meta) {
                    if (!scope.hiringApplicationProxyIsValid && !scope.hiringTeamsProxyIsValid) {
                        return $q.resolve(); // resolve immediately if no changes need to be made to hiring application
                    }
                    return scope.hiringApplicationProxy.save(meta).then(response => {
                        // update the original models
                        _.extend(scope.hiringApplication, response.result);
                        resetOriginalHiringTeams();
                        scope.hiringTeamsProxyIsValid = false;

                        if (scope.user) {
                            _.extend(scope.user.hiring_application, scope.hiringApplication);
                        }

                        // then update the proxies
                        setHiringApplicationProxy();
                        setUserProxy();

                        if (scope.hiringApplicationProxy.status === 'accepted') {
                            // we're batch creating the hiring relationships for the hiring manager upon
                            // acceptance, so we need to retrieve these newly created hiring relationships.
                            loadExistingHiringRelationships();
                        }
                        ngToast.create({
                            content: 'Hiring application saved.',
                            className: 'success',
                        });
                    });
                }

                // ------------------
                // Team View Helpers
                // ------------------

                function prepareHiringTeamMeta() {
                    const addedHiringTeams = scope.hiringTeamsProxy.addedHiringTeams;
                    const meta = {};

                    if (addedHiringTeams.length === 0) {
                        meta.hiring_team_id = null;
                    } else if (typeof addedHiringTeams[0] === 'string') {
                        // the hiring manager was added to a team that doesn't exist yet
                        // create a new hiring team and automatically assign the hiring manager as a team member
                        meta.hiring_team_title = addedHiringTeams[0];
                    } else {
                        meta.hiring_team_id = addedHiringTeams[0].id;
                    }

                    scope.hiringApplicationProxy.hiring_team_id = meta.hiring_team_id;

                    return meta;
                }

                // ------------------
                // Hiring Relationship View Helpers
                // ------------------

                scope.isVisibleToCandidate = relationship => relationship.isVisibleToCandidate;

                function hasHiringRelationshipWithHiringManager(user) {
                    if (scope.existingHiringRelationshipsByCandidateId) {
                        return !!scope.existingHiringRelationshipsByCandidateId[user.id];
                    }
                }

                function editHiringRelationship(user) {
                    showHiringRelationshipModal(user);
                }

                function createHiringRelationship(user) {
                    // ensure the user doesn't have already have a hiring relationship with this hiring manager
                    if (!hasHiringRelationshipWithHiringManager(user)) {
                        // It's important that we don't set the hiring_manager_status here because if a hiring
                        // relationship already exists between this candidate and the hiring manager we could
                        // accidentally override the hiring_manager_status on the pre-existing relationship.
                        const hiringRelationship = HiringRelationship.new({
                            candidate_id: user.id,
                            hiring_manager_id: scope.hiringApplication.user_id,
                        });
                        hiringRelationship.save().then(response => {
                            if (scope.existingHiringRelationshipsByCandidateId) {
                                // add the newly created hiring relationship to the map of existing relationships
                                scope.existingHiringRelationshipsByCandidateId[user.id] = response.result;
                                ngToast.create({
                                    content: 'Successfully Created Hiring Relationship',
                                    className: 'success',
                                });
                            }
                        });
                    }
                }

                function viewCareerProfile(user) {
                    const allowEdit = false;
                    showHiringRelationshipModal(user, allowEdit);
                }

                function showHiringRelationshipModal(user, allowEdit) {
                    // We use the same directive inside the modal for editing an existing hiring relationship
                    // as well as for viewing just a candidate's career profile, but in the latter case there's
                    // no hiring relationship to edit so we disable any editing functionality.
                    allowEdit = angular.isDefined(allowEdit) ? allowEdit : true;

                    // We make a call to the career profiles controller to get all of the career profile data
                    // because what's passed to the client from the server from the initial users controller index
                    // call doesn't contain everything required for the candidate card to be displayed properly.
                    CareerProfile.show(user.career_profile.id, {
                        view: 'editable',
                    }).then(response => {
                        // get the existing hiring relationship if it exists (if not, instantiate a new one)
                        // and attach the career profile to it.
                        let hiringRelationship = scope.existingHiringRelationshipsByCandidateId[user.id];
                        if (!hiringRelationship) {
                            hiringRelationship = HiringRelationship.new({
                                candidate_id: user.id,
                                hiring_manager_id: scope.hiringApplication.user_id,
                                candidate_display_name: user.name,
                                hiring_manager_display_name: scope.hiringApplication.name,
                            });
                            hiringRelationship.career_profile = CareerProfile.new({
                                user_id: user.id,
                            });
                        }
                        _.extend(hiringRelationship.career_profile, response.result);
                        DialogModal.alert({
                            content:
                                '<admin-edit-hiring-relationship hiring-relationship="hiringRelationship" existing-hiring-relationships="existingHiringRelationships" allow-edit="allowEdit"></admin-edit-hiring-relationship>',
                            title: allowEdit ? 'Edit Hiring Relationship' : user.email,
                            size: allowEdit ? 'large' : 'normal',
                            scope: {
                                hiringRelationship,
                                existingHiringRelationships: scope.existingHiringRelationshipsByCandidateId,
                                allowEdit,
                            },
                        });
                    });
                }

                function getHiringManagerPriority(user) {
                    return hasHiringRelationshipWithHiringManager(user)
                        ? scope.existingHiringRelationshipsByCandidateId[user.id].hiring_manager_priority
                        : null;
                }

                // ------------------
                // Editable Item List of Relationships
                // ------------------

                const translationHelper = new TranslationHelper('careers.field_options');
                const locationsOfInterestKeys = $injector.get('CAREERS_LOCATIONS_OF_INTEREST_KEYS');
                scope.locationsOfInterestOptions = _.chain(locationsOfInterestKeys)
                    .map(key => ({
                        label: translationHelper.get(key),
                        value: key,
                    }))
                    .sortBy(option => option.label)
                    .value();

                const yearsOfExperienceKeys = $injector.get('CAREERS_YEARS_EXPERIENCE_KEYS');
                scope.yearsOfExperienceOptions = _.map(yearsOfExperienceKeys, key => ({
                    label: translationHelper.get(key),
                    value: key,
                }));

                scope.hiringRelationshipStatusOptions = [
                    {
                        label: 'Matched',
                        value: ['accepted', 'accepted'],
                    },
                    {
                        label: 'Saved for Later',
                        value: ['saved_for_later', 'hidden'],
                    },
                    {
                        label: 'Waiting on Hiring Manager',
                        value: ['pending', 'hidden'],
                    },
                    {
                        label: 'Rejected by Hiring Manager',
                        value: ['rejected', 'hidden'],
                    },
                    {
                        label: 'Rejected by Candidate',
                        value: ['accepted', 'rejected'],
                    },
                    {
                        label: 'Hidden from Candidate',
                        value: ['accepted', 'hidden'],
                    },
                    {
                        label: 'Waiting on Candidate',
                        value: ['accepted', 'pending'],
                    },
                    {
                        label: 'Hidden from Hiring Manager',
                        value: ['hidden', 'hidden'],
                    },
                ];

                scope.tableColumns = [
                    {
                        prop: 'careerProfileCreatedAt',
                        type: 'time',
                        label: 'Created',
                    },
                    {
                        prop: 'name',
                        type: 'text',
                        label: 'Name',
                    },
                    {
                        prop: 'email',
                        type: 'text',
                        label: 'Email',
                    },
                    {
                        prop: 'careerProfileLocation',
                        type: 'text',
                        label: 'Location',
                    },
                    {
                        prop: 'cohortApplicationsStatusesString',
                        type: 'text',
                        label: 'Applications',
                    },
                    {
                        prop: 'hiring_relationships.hiring_manager_priority',
                        type: 'custom',
                        label: 'Priority',
                        templateUrl: customActionsTemplateUrl,
                        callbacks: {
                            getValue: getHiringManagerPriority,
                            sort: getHiringManagerPriority,
                        },
                    },
                    {
                        prop: 'editorAbilities',
                        type: 'custom',
                        label: 'Actions',
                        templateUrl: customActionsTemplateUrl,
                        callbacks: {
                            canShowAction: hasHiringRelationshipWithHiringManager,
                            editCallback: editHiringRelationship,
                            addCallback: createHiringRelationship,
                            viewCallback: viewCareerProfile,
                        },
                    },
                ];

                // ------------------
                // Open Positions
                // ------------------
                scope.setOpenPositionMode = (mode, openPosition) => {
                    if (mode === 'create' && !openPosition) {
                        throw 'Must pass in openPosition to create or edit';
                    }

                    if (mode === 'create') {
                        scope.tempOpenPosition = openPosition;
                    } else if (mode === 'list') {
                        // Remove the page parameter that position-form might have added
                        $location.search('page', null);

                        // Remove the positionId parameter that the position-form might have added
                        $location.search('positionId', null);
                    }
                    scope.openPositionMode = mode;
                };

                scope.onSavePositionFormCallback = () => {
                    scope.setOpenPositionMode('list');
                };

                scope.createOpenPosition = () => {
                    scope.setOpenPositionMode(
                        'create',
                        OpenPosition.new({
                            hiring_manager_id: scope.hiringApplication.user_id,
                            hiring_application: scope.hiringApplication,
                        }),
                    );
                };

                scope.setOpenPositionMode('list'); // initialize to list mode

                scope.indexParamsForOpenPositions = {
                    filters: {
                        hiring_manager_id: scope.hiringApplication.user_id,
                    },
                    'except[]': ['hiring_application'],
                    'fields[]': ['ADMIN_FIELDS'],
                };

                scope.openPositionsTableColumns = [
                    {
                        prop: 'title',
                        type: 'text',
                        label: 'Title',
                    },
                    {
                        prop: 'description',
                        type: 'text',
                        label: 'Description',
                    },
                    {
                        prop: 'num_interested_candidates',
                        type: 'number',
                        label: '# Interested',
                    },
                ];

                scope.onOpenPositionClick = openPosition =>
                    scope.loadUrl(`/admin/careers/open_positions?id=${openPosition.id}`);

                scope.candidatePositionInterestsTableColumns = [
                    {
                        prop: 'career_profile.name',
                        type: 'text',
                        label: 'Name',
                    },
                    {
                        prop: 'career_profile.email',
                        type: 'text',
                        label: 'Email',
                    },
                    {
                        prop: 'candidate_status',
                        type: 'text',
                        label: 'Candidate Status',
                    },
                    {
                        prop: 'hiring_manager_status',
                        type: 'text',
                        label: 'Hiring Manager Status',
                    },
                    {
                        prop: 'created_at',
                        type: 'time',
                        label: 'Created At',
                    },
                ];

                let listKey;
                const defaultFilters = [];
                const booleanProperties = [];

                editContentItemListMixin.onLink(scope, listKey, defaultFilters, booleanProperties);
            },
        };
    },
]);
