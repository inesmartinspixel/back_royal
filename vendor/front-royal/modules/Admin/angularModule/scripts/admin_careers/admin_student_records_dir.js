import angularModule from 'Admin/angularModule/scripts/admin_module';
import template from 'Admin/angularModule/views/admin_careers/admin_student_records.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

import progressBadgeCompleteTurquoise from 'images/progress_badge_complete_turquoise.png';
import trashcanBeige from 'vectors/trashcan_beige.svg';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('adminStudentRecords', [
    '$injector',

    function factory($injector) {
        const User = $injector.get('User');
        const ngToast = $injector.get('ngToast');
        const PrivateUserDocumentsHelper = $injector.get('PrivateUserDocumentsHelper');
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
        const DialogModal = $injector.get('DialogModal');
        const S3TranscriptAsset = $injector.get('S3TranscriptAsset');
        const frontRoyalUpload = $injector.get('frontRoyalUpload');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                user: '<',
            },
            link(scope) {
                scope.progressBadgeCompleteTurquoise = progressBadgeCompleteTurquoise;
                scope.trashcanBeige = trashcanBeige;

                scope.userProxy = User.new(scope.user.asJson());

                NavigationHelperMixin.onLink(scope);
                PrivateUserDocumentsHelper.onLinkWhenUsingProxy(scope);

                scope.$watchGroup(['userProxy', 'userProxy.s3_identification_asset'], () => {
                    if (scope.userProxy) {
                        scope.showAnyDocumentSections =
                            scope.userProxy.recordsIndicateUserShouldUploadIdentificationDocument ||
                            scope.userProxy.recordsIndicateUserShouldUploadEnglishLanguageProficiencyDocuments ||
                            scope.userProxy.careerProfileIndicatesUserShouldUploadEnglishLanguageProficiencyDocuments;

                        // Note: I think we disable when an identification asset is present to force deleting
                        // before reuploading, as we really don't want to have orphaned sensitive documents
                        // lingering around.
                        scope.identificationUploadIsDisabled =
                            scope.uploadingIdentification ||
                            scope.userProxy.s3_identification_asset ||
                            !scope.userProxy.recordsIndicateUserShouldUploadIdentificationDocument;

                        scope.identificationUploadInstructionsLocale = scope.userProxy
                            .recordsIndicateUserShouldUploadIdentificationDocument
                            ? 'identification_desc_when_can_upload'
                            : 'identification_desc_when_cannot_upload';

                        // Special messaging when in the admin to explain why the user does not need to upload English
                        // language proficiency documents and does not need document approval for that section.
                        if (
                            !scope.userProxy.careerProfileIndicatesUserShouldUploadEnglishLanguageProficiencyDocuments
                        ) {
                            if (scope.userProxy.career_profile.native_english_speaker) {
                                scope.englishLanguageProficiencySectionAdminMessageLocale =
                                    'english_language_proficiency_section_admin_message_for_native_english_speaker';
                            } else if (scope.userProxy.career_profile.earned_accredited_degree_in_english) {
                                scope.englishLanguageProficiencySectionAdminMessageLocale =
                                    'english_language_proficiency_section_admin_message_for_accredited_degree_holder';
                            } else {
                                scope.englishLanguageProficiencySectionAdminMessageLocale =
                                    'english_language_proficiency_section_admin_message_for_exempt_user';
                            }
                        }

                        // For dirty checking
                        scope.tempIdentityVerified = scope.userProxy.identity_verified;
                        scope.tempEnglishLanguageProficiencyDocumentsApproved =
                            scope.userProxy.english_language_proficiency_documents_approved;
                        scope.tempTranscriptsVerified = scope.userProxy.transcripts_verified;

                        // For overriding the experience that requires an official transcript
                        const educationExperiences =
                            scope.userProxy.career_profile.educationExperiencesIndicatingTranscriptRequired;
                        scope.transcriptOverrideOptions = educationExperiences.map(e => ({
                            label: e.degreeAndOrgNameString,
                            value: e.id,
                        }));
                        scope.transcriptOverrideOptions.unshift({
                            label: `🤖 Automatically choose`,
                            value: -1,
                        });
                        scope.transcriptOverride =
                            educationExperiences.find(e => e.official_transcript_required_override)?.id || -1;
                    }
                });

                scope.onTranscriptOverrideChange = newVal => {
                    const educationExperiences =
                        scope.userProxy.career_profile.educationExperiencesIndicatingTranscriptRequired;

                    educationExperiences.forEach(e => {
                        e.official_transcript_required_override = false;
                    });

                    if (newVal !== -1) {
                        educationExperiences.find(e => e.id === newVal).official_transcript_required_override = true;
                    }
                };

                scope.resetProxy = userData => {
                    // Note that a few things can happen on the server that need to be reflected back
                    // to the original user object, in addition to the direct form changes.
                    // - When a user has their identity verified we automatically delete their s3_identification_asset
                    // - When an admin has approved or waived all transcripts for a user, that user's
                    //      transcripts_verified flag becomes true
                    // - The updated_at changed, so we don't want a 406 to be triggered later
                    scope.user.copyAttrs(userData);
                    scope.userProxy = User.new(scope.user.asJson());
                };

                scope.onTranscriptSelect = ($file, $invalidFiles, educationExperienceProxy) => {
                    educationExperienceProxy.$$transcriptErrMessage = null;
                    educationExperienceProxy.$$uploadingTranscript = true;

                    frontRoyalUpload
                        .handleNgfSelect($file, $invalidFiles, file => ({
                            url: S3TranscriptAsset.UPLOAD_URL,
                            data: {
                                record: {
                                    file,
                                    education_experience_id: educationExperienceProxy.id,
                                },
                            },
                            supportedFormatsForErrorMessage: '.pdf, .doc, .docx, .jpg, .png',
                        }))
                        .then(response => {
                            const transcriptJson = _.first(response.data.contents.s3_transcript_assets);

                            // Since this change happens independently of a save button, reflect
                            // it on the original object now.
                            const originalEducationExperience = scope.user.career_profile.education_experiences.find(
                                e => e.id === educationExperienceProxy.id,
                            );
                            originalEducationExperience.transcripts.push(S3TranscriptAsset.new(transcriptJson));

                            educationExperienceProxy.transcripts.push(S3TranscriptAsset.new(transcriptJson));
                        })
                        .catch(err => {
                            educationExperienceProxy.$$transcriptErrMessage = err && err.message;
                        })
                        .finally(() => {
                            educationExperienceProxy.$$uploadingTranscript = false;
                        });
                };

                scope.deleteTranscript = (educationExperienceProxy, index) => {
                    const fileName = educationExperienceProxy.transcripts[index].file_file_name;

                    DialogModal.confirm({
                        text: `Are you sure you want to delete transcript '${fileName}'?`,
                        confirmCallback: () => {
                            educationExperienceProxy.$$deletingTranscript = true;
                            educationExperienceProxy.transcripts[index]
                                .destroy()
                                .then(
                                    () => {
                                        // Since this change happens independently of a save button, reflect
                                        // it on the original object now.
                                        const originalEducationExperience = scope.user.career_profile.education_experiences.find(
                                            e => e.id === educationExperienceProxy.id,
                                        );
                                        originalEducationExperience.transcripts.splice(index, 1);

                                        educationExperienceProxy.transcripts.splice(index, 1);
                                    },
                                    err => {
                                        throw err;
                                    },
                                )
                                .finally(() => {
                                    educationExperienceProxy.$$deletingTranscript = false;
                                });
                        },
                    });
                };

                // FIXME: Having issues undoing the checkbox change on cancel, so not using this at the moment.
                scope.toggleIdentificationApproveCheckbox = () => {
                    // If we are toggling to true and the user has an identification document then we need
                    // to let the user know that the server is going to delete the document for privacy
                    // reasons.
                    if (!scope.userProxy.identity_verified && scope.userProxy.s3_identification_asset) {
                        DialogModal.confirm({
                            text: `Are you sure you want to verify identification for ${scope.userProxy.email}? This will delete their identification from our system.`,
                            confirmCallback: () => {
                                scope.toggleApproveCheckbox('identity_verified');
                            },
                            cancelCallback: () => {},
                        });
                    } else {
                        scope.toggleApproveCheckbox('identity_verified');
                    }
                };

                scope.toggleApproveCheckbox = docsVerifiedProp => {
                    // this is more complex than just userProxy[docsVerifiedProp] = !userProxy[docsVerifiedProp]
                    // because when unchecking, we need to know if it should go back to `false` or `null`
                    scope.userProxy[docsVerifiedProp] =
                        scope.userProxy[docsVerifiedProp] === scope.user[docsVerifiedProp]
                            ? !scope.user[docsVerifiedProp]
                            : scope.user[docsVerifiedProp];
                };

                scope.onWaiveChanged = experience => {
                    // If changing $$transcript_waived to false then nullify the waiver reason
                    if (!experience.$$transcript_waived) {
                        experience.transcript_waiver = null;
                    }
                };

                scope.save = () => {
                    scope.saving = true;

                    scope.userProxy
                        .save({
                            // FIXME: I should have just read these off the career_profile
                            // param on the server in users_controller rather than sending through
                            // the meta like this. I guess I did it like this so that it was explicit
                            // that we were only saving the experiences that require a transcript.
                            education_experiences:
                                scope.userProxy.career_profile.educationExperiencesIndicatingTranscriptRequired,
                        })
                        .then(response => {
                            scope.resetProxy(response.result);

                            ngToast.create({
                                content: 'Student Records Saved',
                                className: 'success',
                            });
                        })
                        .finally(() => {
                            scope.saving = false;
                            scope.editDocuments.$setPristine();
                        });
                };
            },
        };
    },
]);
