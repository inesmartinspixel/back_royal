import angularModule from 'Admin/angularModule/scripts/admin_module';
import template from 'Admin/angularModule/views/admin_careers/admin_edit_organization_option.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('adminEditOrganizationOption', [
    '$injector',

    function factory($injector) {
        const ngToast = $injector.get('ngToast');
        const Upload = $injector.get('Upload');
        const $http = $injector.get('$http');
        const $window = $injector.get('$window');
        const DialogModal = $injector.get('DialogModal');
        const $rootScope = $injector.get('$rootScope');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                // Since scope.organization is replaced below, two-way binding seems to make sense,
                // though the way we're using this it doesn't seem to matter
                organization: '=',
                availableOrganizations: '<?',
                createMode: '<?',
            },
            link(scope) {
                // make a copy of the organization
                scope.organizationProxy = {
                    id: scope.organization.id,
                    created_at: scope.organization.created_at,
                    updated_at: scope.organization.updated_at,
                    type: scope.organization.type,
                    text: scope.organization.text,
                    locale: scope.organization.locale,
                    suggest: scope.organization.suggest,
                };

                // source_id and image_url may be left undefined depending on the type or organization
                // see auto_suggest_option_mixin.rb#as_json
                if (scope.organization.source_id) {
                    scope.organizationProxy.source_id = scope.organization.source_id;
                }

                if (scope.organization.image_url) {
                    scope.organizationProxy.image_url = scope.organization.image_url;
                }

                scope.onFileSelect = (file, errFiles) => {
                    const errFile = errFiles && errFiles[0];

                    if (file && !errFile) {
                        scope.uploading = true;
                        scope.saving = true;

                        const params = {
                            record: scope.organizationProxy,
                            organization_icon: file,
                        };

                        Upload.upload({
                            url: `${$window.ENDPOINT_ROOT}/api/auto_suggest_options/upload_icon.json`,
                            data: params,
                        }).then(
                            response => {
                                scope.organizationProxy.source_id = response.data.contents.organization_icon.id;
                                scope.organizationProxy.image_url =
                                    response.data.contents.organization_icon.formats.original.url;
                                scope.uploading = false;
                                scope.saving = false;
                                ngToast.create({
                                    content: 'Image successfully uploaded',
                                    className: 'success',
                                });
                            },
                            err => {
                                scope.uploading = false;
                                scope.saving = false;
                                throw new Error(err);
                            },
                        );
                    }
                };

                scope.update = () => {
                    scope.saving = true;

                    const params = {
                        record: scope.organizationProxy,
                    };

                    $http
                        .put(`${$window.ENDPOINT_ROOT}/api/auto_suggest_options.json`, params)
                        .then(response => {
                            if (
                                response &&
                                response.data &&
                                response.data.contents &&
                                response.data.contents[`${scope.organizationProxy.type}_options`] &&
                                response.data.contents[`${scope.organizationProxy.type}_options`].length > 0
                            ) {
                                scope.organization = response.data.contents[`${scope.organization.type}_options`][0];
                                scope.organizationProxy = scope.organization;
                                // update the organization in the table view if it's available
                                if (scope.availableOrganizations) {
                                    const index = _.pluck(scope.availableOrganizations, 'id').indexOf(
                                        scope.organization.id,
                                    );
                                    scope.availableOrganizations[index] = scope.organization;
                                }
                                ngToast.create({
                                    content: `${scope.organization.text} successfully updated`,
                                    className: 'success',
                                });
                                scope.saving = false;
                            }
                        })
                        .catch(response => {
                            scope.saving = false;
                            console.error(response);
                        });
                };

                scope.create = () => {
                    scope.saving = true;

                    const params = {
                        record: scope.organizationProxy,
                    };
                    const config = {
                        'FrontRoyal.ApiErrorHandler': {
                            skip: true,
                        },
                    };

                    $http
                        .post(`${$window.ENDPOINT_ROOT}/api/auto_suggest_options.json`, params, config)
                        .then(() => {
                            ngToast.create({
                                content: `${scope.organizationProxy.text} successfully created`,
                                className: 'success',
                            });
                            scope.saving = false;
                            DialogModal.hideAlerts();
                            $rootScope.$broadcast('admin-organization-options:search');
                        })
                        .catch(response => {
                            scope.saving = false;
                            console.error(response);
                        });
                };
            },
        };
    },
]);
