import angularModule from 'Admin/angularModule/scripts/admin_module';
import template from 'Admin/angularModule/views/admin_careers/admin_edit_hiring_team.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('adminEditHiringTeam', [
    '$injector',

    function factory($injector) {
        const $location = $injector.get('$location');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                hiringTeam: '<thing',
                goBack: '&',
                destroyed: '&',
                created: '&',
            },
            link(scope) {
                scope.$location = $location;

                scope.$watch('$location.search().tab', tab => {
                    if (!tab) {
                        return;
                    }

                    if (scope.currentTab !== tab) {
                        scope.currentTab = tab;
                    }
                });

                scope.$watch('currentTab', tab => {
                    if (!tab) {
                        tab = 'edit';
                    }

                    $location.search('tab', tab);
                });

                scope.$on('$destroy', () => {
                    $location.search('tab', null);
                });
            },
        };
    },
]);
