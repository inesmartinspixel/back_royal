import angularModule from 'Admin/angularModule/scripts/admin_module';
import template from 'Admin/angularModule/views/admin_careers/admin_edit_hiring_team_details.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('adminEditHiringTeamDetails', [
    '$injector',

    function factory($injector) {
        const $window = $injector.get('$window');
        const ngToast = $injector.get('ngToast');
        const User = $injector.get('User');
        const HiringTeam = $injector.get('HiringTeam');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                hiringTeam: '<',
                destroyed: '&',
                created: '&',
                goBack: '&',
            },
            link(scope) {
                scope.HiringTeam = HiringTeam;
                scope.hiringPlans = HiringTeam.hiringPlans();

                scope.$watchGroup(['hiringTeam', 'hiringTeam.id'], () => {
                    // If we're editing an existing team, issue an index call and then
                    // load up everything else we may need.
                    if (scope.hiringTeam && scope.hiringTeam.id) {
                        // load up hiring managers
                        const userIndexParams = {
                            hiring_team_id: scope.hiringTeam.id,
                            per_page: 999999999999,
                        };

                        User.index(userIndexParams).then(response => {
                            scope.teamMembers = response.result;
                            setProxy();
                            setProxyTeamMemberDetails();
                        });
                    }
                    // If we're creating a new team, skip the index call and set an
                    // empty proxy.
                    else {
                        setProxy();
                    }
                });

                function setProxy() {
                    scope.proxy = HiringTeam.new(scope.hiringTeam.asJson());
                }

                function setProxyTeamMemberDetails() {
                    scope.containsAccepted = !!_.findWhere(scope.teamMembers, {
                        hiringApplicationStatus: 'accepted',
                    });
                }

                scope.shouldRequireTeamMemberSelect = () => scope.proxy && scope.proxy.hiring_manager_ids.length > 0;

                scope.getHiringManagerLabel = hiringManager =>
                    `${hiringManager.name} [${hiringManager.email}] (${hiringManager.hiringApplicationStatus})`;

                scope.save = () => {
                    const isNew = !scope.proxy.id;
                    if (!scope.proxy.subscription_required) {
                        scope.proxy.subscription_required = false;

                        if (scope.proxy.hiring_plan !== HiringTeam.HIRING_PLAN_LEGACY) {
                            scope.proxy.hiring_plan = HiringTeam.HIRING_PLAN_UNLIMITED_WITH_SOURCING;
                        }
                    }
                    scope.proxy.save().then(response => {
                        _.extend(scope.hiringTeam, response.result);
                        if (isNew && scope.created) {
                            scope.created({
                                $thing: scope.hiringTeam,
                            });
                        }
                        setProxy();
                        ngToast.create({
                            content: 'Team saved',
                            className: 'success',
                        });
                    });
                };

                scope.destroy = () => {
                    if ($window.confirm('Are you sure you want to delete this team?')) {
                        scope.proxy.destroy().then(() => {
                            ngToast.create({
                                content: 'Team deleted',
                                className: 'success',
                            });
                            if (scope.goBack) {
                                scope.goBack();
                            }
                            if (scope.destroyed) {
                                scope.destroyed({
                                    $thing: scope.hiringTeam,
                                });
                            }
                        });
                    }
                };
            },
        };
    },
]);
