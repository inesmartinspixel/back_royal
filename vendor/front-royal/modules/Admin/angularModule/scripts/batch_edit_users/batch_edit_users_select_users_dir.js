import angularModule from 'Admin/angularModule/scripts/admin_module';
import template from 'Admin/angularModule/views/batch_edit_users/batch_edit_users_select_users.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('batchEditUsersSelectUsers', [
    '$injector',

    function factory($injector) {
        const User = $injector.get('User');
        const CohortApplication = $injector.get('CohortApplication');
        const $http = $injector.get('$http');
        const $window = $injector.get('$window');
        const DialogModal = $injector.get('DialogModal');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                viewModel: '<',
            },
            link(scope) {
                scope.searchParams = {};
                scope.searchParams.application_registered = undefined;

                scope.loadingUsers = false;
                scope.showNAccountIds = 40;
                const cohortApplicationStatusOptions = CohortApplication.VALID_STATUS_OPTIONS;
                scope.graduationStatusOptions = CohortApplication.VALID_GRADUATION_STATUS_OPTIONS;

                scope.$watch('viewModel.userIdentifierText', text => {
                    text = text || '';
                    scope.userIdentifiers = _.compact(text.split(/[\s\,\;]+/));
                });

                scope.$watch('viewModel.selectStrategy', () => {
                    if (scope.viewModel && !scope.viewModel.selectStrategy) {
                        scope.viewModel.selectStrategy = 'listIdentifiers';
                    }

                    if (scope.viewModel.selectStrategy !== 'listIdentifiers') {
                        scope.viewModel.userIdentifierText = null;
                    }
                });

                scope.$watch('searchParams.cohort', () => {
                    if (scope.searchParams.cohort) {
                        scope.viewModel.cohortId = scope.searchParams.cohort.id;

                        if (!scope.searchParams.cohort.supportsRecurringPayments) {
                            scope.searchParams.application_registered = undefined;
                        }
                    }
                });

                function validateSearchParams() {
                    if (scope.viewModel.selectStrategy === 'listIdentifiers') {
                        scope.searchParamsInvalid = _.size(scope.userIdentifiers) === 0;
                    } else if (scope.viewModel.selectStrategy === 'cohortStatus') {
                        scope.cohortApplicationStatusOptions = cohortApplicationStatusOptions;
                        scope.searchParamsInvalid =
                            !scope.searchParams.cohort ||
                            !scope.searchParams.cohortStatus ||
                            !scope.searchParams.graduationStatus;
                    } else {
                        scope.searchParamsInvalid = true;
                    }
                }

                scope.$watchCollection('searchParams', validateSearchParams);
                scope.$watchGroup(['viewModel.selectStrategy', 'userIdentifiers'], validateSearchParams);

                scope.loadUsers = () => {
                    const params = {
                        per_page: 9999999999,
                    };

                    if (scope.viewModel.selectStrategy === 'listIdentifiers') {
                        params.identifiers = JSON.stringify(scope.userIdentifiers);
                    } else if (scope.viewModel.selectStrategy === 'cohortStatus') {
                        params.cohort_id = scope.searchParams.cohort.id;
                        params.cohort_status = scope.searchParams.cohortStatus;
                        params.graduation_status = scope.searchParams.graduationStatus;

                        // Since application_registered is optional, we don't want to send
                        // it down in the params if it's not defined.
                        if (angular.isDefined(scope.searchParams.application_registered)) {
                            params.application_registered = scope.searchParams.application_registered;
                        }
                    } else {
                        throw new Error('Invalid selectStrategy');
                    }

                    scope.loadingUsers = true;
                    scope.viewModel.loadedUsers = [];
                    scope.viewModel.updatedUsers = [];
                    scope.viewModel.errors = {};
                    scope.viewModel.unfoundIdentifiers = [];
                    scope.viewModel.noUsersFound = false;

                    // arguable whether we should reset this here, but seems safest so someone
                    // doesn't not realize that something they selected before is still selected
                    scope.viewModel.updateParams = {};

                    // The batch edit users feature needs a POST endpoint to avoid Http parse errors from being thrown.
                    // Using a GET request and passing in too many ids/emails will exceed the query string character limit,
                    // but by using a POST request we can bypass this issue by using POST data and not query parameters.
                    return $http
                        .post(`${$window.ENDPOINT_ROOT}/api/users/batch_edit_users_index.json`, params)
                        .then(response => {
                            scope.loadingUsers = false;
                            _.each(response.data.contents.users, user => {
                                scope.viewModel.loadedUsers.push(User.new(user));
                            });

                            if (scope.viewModel.loadedUsers.length === 0) {
                                scope.viewModel.noUsersFound = scope.viewModel.loadedUsers.length === 0;
                            } else if (scope.viewModel.loadedUsers.length > 1000) {
                                // We saw issues on the server from batch updating too many people in a
                                // transaction. We have removed the transaction, but let's also place a
                                // client-side limit.
                                DialogModal.alert({
                                    content: `Please update 1000 or less users (currently ${scope.viewModel.loadedUsers.length})`,
                                });
                                scope.viewModel.loadedUsers = undefined;
                            } else {
                                if (scope.viewModel.selectStrategy === 'listIdentifiers') {
                                    scope.viewModel.unfoundIdentifiers = _.difference(
                                        scope.userIdentifiers,
                                        getLoadedIdentifiers(),
                                    );
                                }

                                if (!_.any(scope.viewModel.unfoundIdentifiers)) {
                                    scope.viewModel.gotoSection('update-users');
                                }
                            }
                        });
                };

                function getLoadedIdentifiers() {
                    const loadedIds = _.pluck(scope.viewModel.loadedUsers, 'id');
                    const loadedEmails = _.pluck(scope.viewModel.loadedUsers, 'email');
                    return _.union(loadedIds, loadedEmails);
                }
            },
        };
    },
]);
