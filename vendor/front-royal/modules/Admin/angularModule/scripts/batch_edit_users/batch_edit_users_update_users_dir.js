import angularModule from 'Admin/angularModule/scripts/admin_module';
import template from 'Admin/angularModule/views/batch_edit_users/batch_edit_users_update_users.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('batchEditUsersUpdateUsers', [
    '$injector',

    function factory($injector) {
        const CohortApplication = $injector.get('CohortApplication');
        const FormHelper = $injector.get('FormHelper');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                viewModel: '<',
            },
            link(scope) {
                FormHelper.supportForm(scope, scope.updateUsersForm);
                scope.graduationStatusOptions = CohortApplication.VALID_GRADUATION_STATUS_OPTIONS;
                scope.cohortApplicationStatusOptions = _.map(CohortApplication.VALID_STATUS_OPTIONS, option => ({
                    text: option.value === 'rejected' ? 'REJECTED (EXPELLED WHEN APPROPRIATE)' : option.text,
                    value: option.value,
                }));

                scope.formProxy = {};
                scope.$watch('formProxy', updateViewModelUpdateParams, true);

                let cohorts;
                scope.$watch('viewModel.cohorts', _cohort => {
                    cohorts = _.indexBy(_cohort, 'id');
                    updateViewModelUpdateParams();
                });

                scope.reviewChanges = () => {
                    scope.viewModel.resetForReview();
                    scope.viewModel.gotoSection('review-changes');
                };

                function updateViewModelUpdateParams() {
                    const params = scope.viewModel.updateParams;
                    // If the chosen selectStrategy on the Select Users page was cohortStatus and 'No Change To Cohort'
                    // is selected, set the cohort_id in viewModel.updateParams to the id of the cohort select on the
                    // Select Users page.
                    if (scope.viewModel.selectStrategy === 'cohortStatus' && !scope.formProxy.cohort_id) {
                        params.cohort_id = scope.viewModel.cohortId;
                    } else {
                        params.cohort_id = scope.formProxy.cohort_id;
                    }

                    params.cohort_status = scope.formProxy.cohort_status;
                    params.graduation_status = scope.formProxy.graduation_status;
                    params.can_edit_career_profile = scope.formProxy.can_edit_career_profile;

                    const cohort = cohorts && cohorts[params.cohort_id];
                    if (
                        params.can_edit_career_profile === true &&
                        params.cohort_status === 'accepted' &&
                        cohort &&
                        cohort.supportsDelayedCareerNetworkAccessOnAcceptance
                    ) {
                        scope.errors = [
                            'We generally do not put students from degreee programs into the Career Network immediately upon acceptance.  Are you sure you want to do this?',
                        ];
                    } else {
                        scope.errors = [];
                    }
                }
                updateViewModelUpdateParams();
            },
        };
    },
]);
