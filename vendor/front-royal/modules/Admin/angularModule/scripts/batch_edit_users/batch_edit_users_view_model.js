import angularModule from 'Admin/angularModule/scripts/admin_module';

angularModule.factory('BatchEditUsersViewModel', [
    '$injector',

    function factory($injector) {
        const SuperModel = $injector.get('SuperModel');
        const $location = $injector.get('$location');
        const Cohort = $injector.get('Cohort');
        const Singleton = $injector.get('Singleton');

        const BatchEditUsersViewModel = SuperModel.subclass(function () {
            this.include(Singleton);

            Object.defineProperty(this.prototype, 'hasErrors', {
                get() {
                    return _.any(this.errors);
                },
            });

            Object.defineProperty(this.prototype, 'errorCount', {
                get() {
                    return _.size(this.errors);
                },
            });

            Object.defineProperty(this.prototype, 'validUpdateParams', {
                get() {
                    return this.updatesCohortStatus || this.updatesGraduationStatus || this.updatesCanEditCareerProfile;
                },
            });

            Object.defineProperty(this.prototype, 'updatesCohortStatus', {
                get() {
                    return !!this.updateParams.cohort_id && !!this.updateParams.cohort_status;
                },
            });

            Object.defineProperty(this.prototype, 'updatesGraduationStatus', {
                get() {
                    return !!this.updateParams.cohort_id && !!this.updateParams.graduation_status;
                },
            });

            Object.defineProperty(this.prototype, 'updatesCanEditCareerProfile', {
                get() {
                    return _.contains([true, false], this.updateParams.can_edit_career_profile);
                },
            });

            return {
                initialize() {
                    const self = this;
                    self.loadedUsers = [];
                    self.id = new Date().getTime();
                    self.updateParams = {};
                    self.errors = {};

                    this.cohortNames = {};
                    Cohort.index({
                        fields: ['BASIC_FIELDS'],
                    }).then(response => {
                        self.cohorts = response.result;
                        _.each(self.cohorts, cohort => {
                            self.cohortNames[cohort.id] = cohort.name;
                        });
                    });
                },

                gotoSection(section) {
                    const label = typeof section === 'string' ? section : section.id;
                    $location.url(`/admin/batch-users/${label}`);
                },

                getCohortName(cohortId) {
                    return this.cohortNames && this.cohortNames[cohortId];
                },

                removeUser(user) {
                    this.loadedUsers = _.without(this.loadedUsers, user);
                },

                hasErrorForUser(user) {
                    return !!(this.errors && this.errors[user.id]);
                },

                resetForReview() {
                    this.updatedUsers = [];
                    this.errors = {};
                },
            };
        });

        return BatchEditUsersViewModel;
    },
]);
