import angularModule from 'Admin/angularModule/scripts/admin_module';
import template from 'Admin/angularModule/views/batch_edit_users/batch_edit_users_review_changes.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('batchEditUsersReviewChanges', [
    '$injector',

    function factory($injector) {
        const $http = $injector.get('$http');
        const User = $injector.get('User');
        const HttpQueue = $injector.get('HttpQueue');
        const $q = $injector.get('$q');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                viewModel: '<',
            },
            link(scope) {
                scope.perPage = 20;
                scope.showingUpdatedUsers = false;

                scope.$watchGroup(['viewModel.loadedUsers', 'viewModel.updatedUsers', 'showOnlyErroredUsers'], () => {
                    if (scope.showOnlyErroredUsers && _.any(scope.viewModel.errors)) {
                        scope.showingUpdatedUsers = false;
                        const usersById = _.indexBy(scope.viewModel.loadedUsers, 'id');
                        scope.usersToShow = _.chain(scope.viewModel.errors)
                            .keys()
                            .map(userId => usersById[userId])
                            .value();
                    } else if (_.any(scope.viewModel.updatedUsers)) {
                        scope.showingUpdatedUsers = true;
                        scope.usersToShow = scope.viewModel.updatedUsers;
                    } else {
                        scope.showingUpdatedUsers = false;
                        scope.usersToShow = scope.viewModel.loadedUsers;
                    }
                    scope.showingLoadedUsers = !scope.showingUpdatedUsers;
                });

                scope.$watch('viewModel.hasErrors', () => {
                    if (scope.viewModel.hasErrors) {
                        scope.showOnlyErroredUsers = true;
                    }
                });

                scope.save = () => {
                    scope.saving = true;
                    scope.showOnlyErroredUsers = false;
                    scope.viewModel.errors = {};

                    const allIds = _.pluck(scope.viewModel.loadedUsers, 'id');
                    const config = {
                        // If this request retries, bad things can happen server-side.
                        // in particular, the batch update from the first request could continue to run, while
                        // a second retried request is triggered; the secone one will try to write its first batch
                        // of updates, probably lock waiting for the first batch update to finish, then complete,
                        // possibly resulting in duplicate events triggering (this happened once).
                        //
                        // Because this isn't an iguana request, we don't need to disable timeouts. If it had been,
                        // we would need to include the following argument here:
                        // timeout: undefined,
                        httpQueueOptions: {
                            disable_retry: true,
                        },
                        'FrontRoyal.ApiErrorHandler': {
                            skip: true,
                        },
                    };

                    // Construct a sequential promise chain to call batches in chunks and collect viewModel info for
                    // completed rendering once chain is complete.
                    let viewErrors = {};
                    let updatedUsers = [];
                    const batchChunks = _.chain(allIds).chunk(50).value();
                    let promiseChain = $q.when();
                    // eslint-disable-next-line no-restricted-syntax
                    for (const batchChunk of batchChunks) {
                        // eslint-disable-next-line no-loop-func
                        promiseChain = promiseChain.then(() => {
                            const data = _.extend(scope.viewModel.updateParams, {
                                user_ids: batchChunk,
                            });
                            return $http.put(`${window.ENDPOINT_ROOT}/api/users/batch_update.json`, data, config).then(
                                response => {
                                    updatedUsers = updatedUsers.concat(
                                        _.map(response.data.contents.users, user => User.new(user)),
                                    );
                                },
                                response => {
                                    viewErrors = Object.assign(viewErrors, response.data.errors);
                                    HttpQueue.unfreezeAfterError(response.config);
                                },
                            );
                        });
                    }

                    promiseChain.finally(() => {
                        // Reset form and update completed state
                        scope.saving = false;
                        scope.viewModel.errors = viewErrors;
                        scope.viewModel.updatedUsers = updatedUsers;

                        // Prune successfully updated users from the loadedUsers collection, only allowing errored users to be retried with different options.
                        const updatedUsersIds = _.map(updatedUsers, 'id');
                        scope.viewModel.loadedUsers = _.filter(
                            scope.viewModel.loadedUsers,
                            loadedUser => !updatedUsersIds.includes(loadedUser.id),
                        );
                    });
                };
            },
        };
    },
]);
