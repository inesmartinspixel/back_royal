import angularModule from 'Admin/angularModule/scripts/admin_module';
import template from 'Admin/angularModule/views/admin_users/admin_users_legacy_pagination.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('adminUsersLegacyPagination', [
    '$injector',

    function factory() {
        return {
            restrict: 'E',
            templateUrl,
            scope: {
                totalPages: '<',
                page: '<',
                maxDisplay: '<',
                updatePage: '<',
            },
            link(scope) {
                scope.onClick = (page, $event) => {
                    if ($($event.target).parent().hasClass('disabled')) {
                        return;
                    }
                    scope.updatePage(page);
                };

                scope.$watch('totalPages', () => {
                    // build out an indexed array based on totalPages size
                    const pagesList = [];
                    for (let i = 0; i < scope.totalPages; i++) {
                        pagesList.push(i + 1);
                    }

                    // modify the list if it exceeds the max display size
                    let tmp = pagesList;
                    if (pagesList.length > scope.maxDisplay) {
                        const padding = scope.maxDisplay / 2;
                        tmp = tmp
                            .slice(0, padding - 1)
                            .concat(['...'])
                            .concat(tmp.slice(scope.totalPages - padding, scope.totalPages + padding));
                    }

                    scope.pagesList = tmp;
                });
            },
        };
    },
]);
