import angularModule from 'Admin/angularModule/scripts/admin_module';
import 'FrontRoyalUiBootstrap/tooltip';
import copyToClipboard from 'copyToClipboard';
import template from 'Admin/angularModule/views/admin_users/edit_user_details_enrollment.html';
import cacheAngularTemplate from 'cacheAngularTemplate';
import moment from 'moment-timezone';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('editUserDetailsEnrollment', [
    '$injector',

    function factory($injector) {
        const $window = $injector.get('$window');
        const CohortApplication = $injector.get('CohortApplication');
        const Cohort = $injector.get('Cohort');
        const User = $injector.get('User');
        const $timeout = $injector.get('$timeout');
        const $http = $injector.get('$http');
        const ngToast = $injector.get('ngToast');
        const dateHelper = $injector.get('dateHelper');
        const DeferralLink = $injector.get('DeferralLink');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                user: '<',
                notProxyUser: '<', // would prefer this be user and the above be userProxy
                cohortOptions: '<',
                cohortPromotions: '<',
            },
            link(scope, elem) {
                //-------------------------
                // Cohort Options
                //-------------------------

                scope.programTypes = User.programTypes;
                scope.dateHelper = dateHelper;
                let targetableProgramTypeOptionsMap = {};

                scope.getCohortName = id => {
                    const cohort = findCohort(id);
                    return cohort ? cohort.name : null;
                };

                function updateFilteredCohortOptions() {
                    scope.allCohortOptions = [];
                    scope.transferrableCohortOptions = [];
                    if (!scope.cohortOptions || !scope.user) {
                        return;
                    }

                    // normalized for selectize-friendly option set
                    scope.cohortOptionsByCohortIdMap = {};
                    targetableProgramTypeOptionsMap = {};

                    // generate selection lists
                    scope.cohortOptions.forEach(cohortOption => {
                        scope.cohortOptionsByCohortIdMap[cohortOption.id] = cohortOption;

                        // add to the list of all options
                        scope.allCohortOptions.push(cohortOption);

                        // add to the filtered list for deferral / transferring
                        if (
                            scope.user.lastCohortApplication &&
                            cohortOption.program_type === scope.user.lastCohortApplication.program_type &&
                            !_.contains(_.pluck(scope.user.cohort_applications, 'cohort_id'), cohortOption.id)
                        ) {
                            scope.transferrableCohortOptions.push(cohortOption);
                        }
                    });
                }

                function fetchRefundDetails(user) {
                    $http
                        .get(`${$window.ENDPOINT_ROOT}/api/cohort_applications/refund_details.json`, {
                            params: {
                                filters: {
                                    user_id: user.id,
                                },
                            },
                        })
                        .then(response => {
                            scope.refundDetails = response.data.contents.refund_details;
                        });
                }

                function findCohort(id) {
                    return scope.cohortOptionsByCohortIdMap[id];
                }
                scope.findCohort = findCohort;

                const slackRoomOptionsMap = {};
                scope.slackRoomOptions = function (cohortId) {
                    if (!slackRoomOptionsMap[cohortId]) {
                        const cohort = findCohort(cohortId);
                        const slackRooms = _.clone(cohort.slack_rooms);

                        // Adding this auto-assign option is a bit hacking.
                        // See comment in cohort_application.rb#merge_hash
                        if (cohort.supportsSlackRooms && _.any(slackRooms) && cohort.startDate < new Date()) {
                            slackRooms.unshift({
                                id: 'AUTO_ASSIGN',
                                title: 'Auto-assign',
                            });
                        }
                        slackRoomOptionsMap[cohortId] = slackRooms;
                    }
                    return slackRoomOptionsMap[cohortId];
                };

                scope.cohortApplicationGraduationStatusOptions = CohortApplication.VALID_GRADUATION_STATUS_OPTIONS;

                scope.cohortProgramTypes = Cohort.programTypes;
                scope.cohortProgramTypesWithNullOption = [
                    {
                        key: 'NULL', // must be a string for Selectize
                        label: 'NONE',
                    },
                ].concat(Cohort.programTypes);

                scope.ensureValidRetargetValues = application => {
                    if (application.retargeted_from_program_type === 'NULL') {
                        // sanitize 'NULL' string values (see above)
                        application.retargeted_from_program_type = null;
                    }
                };

                scope.$watch('user', user => {
                    updateFilteredCohortOptions();
                    if (user?.lastCohortApplication) {
                        fetchRefundDetails(user);
                    }
                });

                scope.$watch('cohortOptions', updateFilteredCohortOptions);

                //-------------------------
                // Enrollment Actions
                //-------------------------
                scope.enrollmentProxy = {};
                const rejectExpel = {
                    supportsCohort: false,
                    registered: false,
                    cancellationReasons: ['Requested to leave program', 'Inability to collect', 'Violation of terms'],
                };

                scope.PAYMENT_OUTSIDE_PRODUCT = 'Payment outside product';
                const enrollmentActions = _.indexBy(
                    [
                        _.extend(
                            {
                                label: 'Reject/Expel',
                                type: 'reject',
                                existingAppStatus: 'rejected_or_expelled',
                            },
                            rejectExpel,
                        ),

                        _.extend(
                            {
                                label: 'Reject/Expel and Refund',
                                type: 'rejectAndRefund',
                                existingAppStatus: 'rejected_or_expelled',
                                refund: true,
                            },
                            rejectExpel,
                        ),

                        _.extend(
                            {
                                label: 'Expel',
                                type: 'expel',
                                existingAppStatus: 'expelled',
                            },
                            rejectExpel,
                        ),

                        _.extend(
                            {
                                label: 'Expel and Refund',
                                type: 'expelAndRefund',
                                existingAppStatus: 'expelled',
                                refund: true,
                            },
                            rejectExpel,
                        ),

                        {
                            type: 'refund',
                            label: 'Issue Refund',
                            refund: true,
                            registered: false,
                        },
                        {
                            type: 'deferIndefinitely',
                            label: 'Defer Indefinitely',
                            existingAppStatus: 'deferred',
                            supportsCohort: false,
                            registered: false,
                            cancellationReasons: ['Deferral'],
                        },
                        {
                            type: 'deferCohort',
                            label: 'Defer into cohort',
                            existingAppStatus: 'deferred',
                            supportsCohort: true,
                            registered: false,
                            cancellationReasons: undefined,
                        },
                        {
                            type: 'deferCohortCancel',
                            label: 'Defer into cohort and cancel subscription',
                            existingAppStatus: 'deferred',
                            supportsCohort: true,
                            registered: false,
                            cancellationReasons: ['Deferral'],
                        },
                        {
                            type: 'deferCohortMaintain',
                            label: 'Defer into cohort and maintain subscription',
                            existingAppStatus: 'deferred',
                            supportsCohort: true,
                            registered: true,
                            cancellationReasons: undefined,
                        },
                        {
                            type: 'cancelFullyPaid',
                            label: 'Cancel subscription and mark as fully paid',
                            existingAppStatus: undefined,
                            supportsCohort: false,
                            registered: true,
                            editPlanAndEarlyRegistration: true,
                            showBillingTransactionForm: true,
                            cancellationReasons: [scope.PAYMENT_OUTSIDE_PRODUCT],
                            confirmIfNoBillingTransaction: true,
                        },
                        {
                            type: 'registerFullyPaid',
                            label: 'Register the user with a one-time out of product payment',
                            existingAppStatus: undefined,
                            supportsCohort: false,
                            registered: true, // implicit
                            editPlanAndEarlyRegistration: true,
                            cancellationReasons: undefined,
                            showBillingTransactionForm: true,
                            confirmIfNoBillingTransaction: true,
                        },
                        {
                            type: 'registerWithOutsidePaymentGracePeriod',
                            label: 'Register user with outside payment grace period',
                            existingAppStatus: undefined,
                            supportsCohort: false,
                            registered: true, // implicit
                            editPlanAndEarlyRegistration: true,
                            cancellationReasons: undefined,
                            defaultStripePlanFrequency: 'once',
                            supportedStripePlanFrequencies: ['once'],
                            showPaymentGracePeriodForm: true,
                            paymentGracePeriodDaysOffset: 30,
                            confirmIfNoBillingTransaction: false,
                        },
                    ],
                    'type',
                );

                const deferralActionsWhenHasSubscription = [
                    enrollmentActions.deferIndefinitely,
                    enrollmentActions.deferCohortCancel,
                    enrollmentActions.deferCohortMaintain,
                    enrollmentActions.cancelFullyPaid,
                ];
                const deferralActionsWhenNoSubscription = [
                    enrollmentActions.deferIndefinitely,
                    enrollmentActions.deferCohort,
                ];

                // Note: We check for not willNeedSubscription because we don't currently
                // handle creating a new Stripe subscription with a deferral link
                // See https://trello.com/c/yZKCV3E3
                const relevantCohort = scope.cohortOptions?.find(o => o.id === scope.user.relevant_cohort_id);
                if (
                    relevantCohort?.supportsDeferralLink &&
                    CohortApplication.VALID_DEFERRAL_STATUSES.includes(scope.user.lastCohortApplication?.status) &&
                    !scope.user.active_deferral_link &&
                    !User.willNeedSubscription(scope.user, relevantCohort, scope.user.lastCohortApplication)
                ) {
                    scope.showGenerateDeferralLink = true;
                }

                scope.createDeferralLink = user => {
                    DeferralLink.create({
                        user_id: user.id,
                        from_cohort_application_id: user.lastCohortApplication.id,
                    }).then(response => {
                        // We can infer this is active if we just made it
                        scope.user.active_deferral_link = response.result;
                        scope.showGenerateDeferralLink = false;

                        // Reflect this back to the original user object
                        // Note: I don't like doing this pattern generally, but
                        // creating the deferral link immediately seemed to be the
                        // most appropriate UX for admins
                        scope.notProxyUser.active_deferral_link = response.result;

                        ngToast.create({
                            content: 'Deferral link generated!',
                            className: 'success',
                        });
                    });
                };

                scope.copyDeferralLinkUrl = (url, event) => {
                    copyToClipboard(url, function onSuccess() {
                        ngToast.create({
                            content: 'Deferral link copied to clipboard!',
                            className: 'success',
                        });

                        if (event) {
                            event.currentTarget.blur();
                        }
                    });
                };

                scope.$watchGroup(['user', 'refundDetails'], () => {
                    scope.enrollmentActions = null;
                    const status = scope.user && scope.user.lastCohortApplicationStatus;
                    if (!status) {
                        return;
                    }

                    const hasSubscription = !!scope.user.primarySubscription;
                    const currentCohort = findCohort(scope.user.lastCohortApplication.cohort_id);
                    const hasRefundDetails = !!scope.refundDetails;

                    let removeActions = [];
                    if (status === 'pre_accepted') {
                        removeActions = [
                            enrollmentActions.reject,
                            hasRefundDetails ? enrollmentActions.rejectAndRefund : null,
                        ];
                    } else if (status === 'accepted') {
                        removeActions = [
                            enrollmentActions.expel,
                            hasRefundDetails ? enrollmentActions.expelAndRefund : null,
                        ];
                    } else if (status === 'expelled') {
                        removeActions = [hasRefundDetails ? enrollmentActions.refund : null];
                    }

                    let deferralActions = [];
                    // students who are accepted/preAccepted into a cohort that
                    // supports a schedule can be deferred
                    if (currentCohort.supportsSchedule && _.contains(['pre_accepted', 'accepted'], status)) {
                        deferralActions = hasSubscription
                            ? deferralActionsWhenHasSubscription
                            : deferralActionsWhenNoSubscription;
                    }

                    let registerActions = [];
                    if (
                        _.contains(['pre_accepted', 'accepted'], status) &&
                        !scope.user.lastCohortApplication.registered &&
                        !scope.user.lastCohortApplication.hasFullScholarship &&
                        currentCohort.supportsRegistrationDeadline
                    ) {
                        registerActions = [
                            enrollmentActions.registerFullyPaid,
                            status === 'pre_accepted' ? enrollmentActions.registerWithOutsidePaymentGracePeriod : null,
                        ];
                    }

                    scope.enrollmentActions = _.chain(removeActions.concat(deferralActions).concat(registerActions))
                        .compact()
                        .each((action, i) => {
                            // I could not figure out how to get selectize to just
                            // use the order that the actions are in for its sorting
                            action.sortKey = i;
                        })
                        .value();

                    // we just set this to null so we can use
                    // `enrollmentActions` instead of `enrollmentActions.length > 0`
                    // in the template
                    if (!_.any(scope.enrollmentActions)) {
                        scope.enrollmentActions = null;
                    }
                });

                scope.$watchGroup(['cohortOptions', 'user.lastCohortApplication'], () => {
                    const application = scope.user && scope.user.lastCohortApplication;
                    if (!application) {
                        return;
                    }
                    scope.currentCohortStripePlans = _.sortBy(scope.user.lastCohortApplication.stripe_plans, 'amount');
                });

                Object.defineProperty(scope, 'enrollmentOptionsValid', {
                    get() {
                        return elem.find('.ng-invalid').length === 0;
                    },
                    configurable: true,
                });

                scope.$watch('enrollmentProxy.selectedActionType', actionType => {
                    const action = enrollmentActions[actionType];
                    scope.enrollmentProxy.selectedAction = enrollmentActions[actionType];
                    scope.enrollmentProxy.showRefund = action && action.refund;
                    scope.enrollmentProxy.supportedStripePlans = scope.currentCohortStripePlans;

                    // If we need to set the registered_early and stripe_plan_id dropdowns, we
                    // want to set the initial values appropriately.
                    if (action && action.editPlanAndEarlyRegistration) {
                        scope.enrollmentProxy.earlyRegistrationDiscount =
                            scope.user.lastCohortApplication.registered_early;

                        if (action.supportedStripePlanFrequencies) {
                            scope.enrollmentProxy.supportedStripePlans = scope.currentCohortStripePlans.filter(plan =>
                                action.supportedStripePlanFrequencies.includes(plan.frequency),
                            );
                        }

                        let defaultStripePlan;
                        if (action.defaultStripePlanFrequency) {
                            defaultStripePlan = scope.enrollmentProxy.supportedStripePlans.find(
                                plan => plan.frequency === action.defaultStripePlanFrequency,
                            );
                        }
                        scope.enrollmentProxy.stripePlanId =
                            scope.user.lastCohortApplication.stripe_plan_id || defaultStripePlan?.id;

                        if (action.showPaymentGracePeriodForm) {
                            scope.enrollmentProxy.paymentGracePeriodEndAt =
                                scope.user.lastCohortApplication.paymentGracePeriodEndAt ||
                                moment()
                                    .add(action.paymentGracePeriodDaysOffset || 0, 'days')
                                    .startOf('day')
                                    .toDate();
                        } else {
                            delete scope.enrollmentProxy.paymentGracePeriodEndAt;
                        }
                    } else {
                        delete scope.enrollmentProxy.earlyRegistrationDiscount;
                        delete scope.enrollmentProxy.stripePlanId;
                        delete scope.enrollmentProxy.paymentGracePeriodEndAt;
                    }
                });

                scope.handleSelectedAction = () => {
                    const proxy = scope.enrollmentProxy;
                    const action = proxy.selectedAction;

                    if (!action) {
                        return;
                    }

                    if (
                        action.confirmIfNoBillingTransaction &&
                        (!proxy.billingTransaction || proxy.billingTransaction.provider === 'NONE')
                    ) {
                        if (!$window.confirm('Are you sure you want to proceed without recording a transaction?')) {
                            return;
                        }
                    }

                    // handle existing application
                    const existingApplication = scope.user.lastCohortApplication;
                    if (action.existingAppStatus) {
                        existingApplication.status = action.existingAppStatus;
                    }

                    // Set some properties on the application that can be set in the form
                    if (angular.isDefined(scope.enrollmentProxy.earlyRegistrationDiscount)) {
                        existingApplication.registered_early = scope.enrollmentProxy.earlyRegistrationDiscount;
                    }
                    if (angular.isDefined(scope.enrollmentProxy.stripePlanId)) {
                        existingApplication.stripe_plan_id = scope.enrollmentProxy.stripePlanId;
                    }

                    // handle special-condition options
                    const options = {};
                    if (action.type === 'cancelFullyPaid') {
                        existingApplication.total_num_required_stripe_payments = 0;
                        existingApplication.paymentGracePeriodEndAt = null;
                        existingApplication.locked_due_to_past_due_payment = false;
                        existingApplication.notes = `NOTE: This application was marked as fully paid outside of the product at time of cancellation.${
                            existingApplication.notes ? `\n${existingApplication.notes}` : ''
                        }`;
                    } else if (
                        action.type === 'registerFullyPaid' ||
                        action.type === 'registerWithOutsidePaymentGracePeriod'
                    ) {
                        existingApplication.total_num_required_stripe_payments = 0;
                        existingApplication.num_charged_payments = 0;
                        existingApplication.num_refunded_payments = 0;
                        existingApplication.registered = true;
                        existingApplication.locked_due_to_past_due_payment = false;
                        if (action.type === 'registerWithOutsidePaymentGracePeriod') {
                            existingApplication.notes = `NOTE: This application was marked as registered on the promise of payment in the future outside of the product. ${
                                existingApplication.notes ? `\n${existingApplication.notes}` : ''
                            }`;
                            existingApplication.paymentGracePeriodEndAt = proxy.paymentGracePeriodEndAt;
                        } else {
                            existingApplication.paymentGracePeriodEndAt = null;
                            existingApplication.notes = `NOTE: This application was marked as fully paid outside of the product at time of registration.${
                                existingApplication.notes ? `\n${existingApplication.notes}` : ''
                            }`;
                        }
                        if (existingApplication.status !== 'accepted') {
                            existingApplication.status = 'pre_accepted';
                        }
                    } else if (action.type === 'deferCohortMaintain') {
                        options.status = 'accepted';

                        // It's possible for the user to be deferred into a cohort while marked as past_due on payments.
                        // For users behind of payments, they're given a grace period to provide those payments. If they
                        // fail to do so, their content access is locked. Since we're carrying over the user's subscription,
                        // we also want to carry over their billing locked info so that we still lock their content access
                        // for their new cohort at the time that we would have for the cohort they deferred out of.
                        options.payment_grace_period_end_at = existingApplication.payment_grace_period_end_at;
                        options.locked_due_to_past_due_payment = existingApplication.locked_due_to_past_due_payment;
                    }

                    // handle new applications
                    if (action.supportsCohort && proxy.selectedCohortId) {
                        const targetCohort = findCohort(proxy.selectedCohortId);

                        // set registration default
                        options.registered = action.registered;

                        // Handle situations where the existing application may be fully paid
                        // or registered by us on the promise of payment outside of the product.
                        if (existingApplication.registered && !scope.user.primarySubscription) {
                            if (targetCohort.startDate < new Date()) {
                                // for ongoing cohort (start date in the past), always set to accepted / registered
                                options.status = 'accepted';
                                options.registered = true;

                                // We're marking their new application as registered, so we should also transfer
                                // their billing locked info, so that their content access experience doesn't change
                                // just because they deferred into a different cohort.
                                options.payment_grace_period_end_at = existingApplication.payment_grace_period_end_at;
                                options.locked_due_to_past_due_payment =
                                    existingApplication.locked_due_to_past_due_payment;
                            } else {
                                // for a future cohort (start date in the future) always set to pre_accepted / un-registered
                                options.status = 'pre_accepted'; // should be defaulted already
                                options.registered = false;

                                // billing locked only applies for registered users
                                options.payment_grace_period_end_at = null;
                                options.locked_due_to_past_due_payment = false;
                            }
                        }

                        // create new application (passing overrides) and auto-expand
                        const newApplication = scope.addCohortApplication(targetCohort, options);
                        newApplication.$$expanded = true;

                        // de-register old application
                        existingApplication.registered = false;

                        updateFilteredCohortOptions();
                    } else {
                        existingApplication.registered = action.registered;
                    }

                    // handle any cancellations
                    if (proxy.selectedCancellationReason) {
                        scope.user.$$subscriptionCancellationReason = proxy.selectedCancellationReason;

                        // Assuming that the canceled subscription was the only subscription.  At least
                        // for now, this is valid for students, and so valid for everything this UI
                        // deals with.
                        scope.user.subscriptions = [];
                    }

                    scope.user.$$issueRefund = action.refund && scope.refundDetails.can_issue_refund;
                    scope.user.$$enrollmentActionLabel = action.label;
                    scope.user.$$billingTransactionToRecord = angular.copy(proxy.billingTransaction);

                    // prevent any further modifications
                    proxy.changesApplied = true;
                };

                //-------------------------
                // Application Helpers
                //-------------------------

                function getPreviousApplication(application) {
                    return _.chain(scope.user.cohort_applications)
                        .sortBy('applied_at')
                        .reverse()
                        .find(app => app.applied_at < application.applied_at)
                        .value();
                }

                scope.addCohortApplication = (cohort, opts = {}) => {
                    const cohortApplication = CohortApplication.new(
                        _.extend(
                            {
                                cohort_id: cohort.id,
                                status: 'pending',
                            },
                            opts,
                        ),
                    );
                    cohortApplication.cohort = cohort;
                    cohortApplication.appliedAt = new Date();

                    // If auto-assigning the slack room is an option, then we
                    // should go ahead and set that as the default. (NOTE: we should
                    // really only do this if the status is accepted or pre_accepted,
                    // but we don't know at this point in the code what the status
                    // is, and we would never really create an application for an ongoing
                    // cohort in any other status, and even if we did add a slack room to
                    // a non-accepted user, it wouldn't hurt anything, so it's fine.)
                    const options = scope.slackRoomOptions(cohort.id);
                    const canAutoAssign = _.findWhere(options, {
                        id: 'AUTO_ASSIGN',
                    });
                    if (canAutoAssign) {
                        cohortApplication.cohort_slack_room_id = 'AUTO_ASSIGN';
                    }

                    const lastCohortApplication = scope.user.lastCohortApplication;

                    // general deferral handling
                    if (lastCohortApplication && lastCohortApplication.status === 'deferred') {
                        // don't default to pending when deferring
                        if (!opts.status) {
                            cohortApplication.status = 'pre_accepted';
                        }
                    }

                    // special-case billing handling
                    if (cohort.supportsRecurringPayments) {
                        // if the user had been deferrred or if they've been expelled (auto-expulsion likely), we should transfer payment status
                        const transferPaymentInfo =
                            lastCohortApplication &&
                            _.contains(['deferred', 'expelled'], lastCohortApplication.status) &&
                            lastCohortApplication.stripe_plans;

                        // backfill payment status to newly deferred-to cohort application
                        if (transferPaymentInfo) {
                            // we don't really know how to deal with two cohorts being of different program types when stripe_plans are supported
                            if (lastCohortApplication.program_type !== cohort.program_type) {
                                $window.alert(
                                    'Unable to transfer students deferred from one paid plan into a paid plan of a different program type.',
                                );
                                return undefined;
                            }

                            // pass along payment information
                            // This should be duplicated on the server in CohortApplication#transfer_payment_info_from_previous_application
                            cohortApplication.stripe_plan_id = lastCohortApplication.stripe_plan_id;
                            cohortApplication.total_num_required_stripe_payments =
                                lastCohortApplication.total_num_required_stripe_payments;
                            cohortApplication.num_charged_payments = lastCohortApplication.num_charged_payments;
                            cohortApplication.num_refunded_payments = lastCohortApplication.num_refunded_payments;
                            cohortApplication.stripe_plans = lastCohortApplication.stripe_plans;
                            cohortApplication.scholarship_levels = lastCohortApplication.scholarship_levels;
                            cohortApplication.scholarship_level = lastCohortApplication.scholarship_level;
                            cohortApplication.registered_early = lastCohortApplication.registered_early;
                        } else {
                            cohortApplication.total_num_required_stripe_payments = null; // default to null
                        }
                    }

                    cohortApplication.$$embeddedIn = scope.user;
                    scope.user.cohort_applications.push(cohortApplication);

                    scope.user.fallback_program_type = cohort.program_type;

                    return cohortApplication;
                };

                scope.removeCohortApplication = application => {
                    if (
                        $window.confirm(
                            `Are you sure you want to remove this user's application for ${application.cohort_name}?`,
                        )
                    ) {
                        scope.user.cohort_applications = _.without(scope.user.cohort_applications, application);
                        updateFilteredCohortOptions();
                    }
                };

                scope.getClassesForCohort = (application, index) => {
                    const classes = [application.status];
                    if (index !== 0 || scope.enrollmentProxy.changesApplied) {
                        classes.push('disabled');
                    }
                    return classes;
                };

                scope.previousApplicationIsDeferred = application => {
                    const previousApplication = getPreviousApplication(application);
                    return previousApplication && previousApplication.status === 'deferred';
                };

                scope.cohortApplicationStatusOptions = application => {
                    // only perform the below determination on new assignments
                    if (!application.isNew()) {
                        return CohortApplication.VALID_STATUS_OPTIONS;
                    }

                    // determine if a previous application existes and was deferred, and limit if so
                    if (scope.previousApplicationIsDeferred(application)) {
                        return CohortApplication.VALID_INITIAL_DEFERRED_STATUS_OPTIONS;
                    }

                    // default
                    return CohortApplication.VALID_STATUS_OPTIONS;
                };

                // watch for new cohorts selection
                scope.$watch('addCohortApplicationIds', newIds => {
                    const id = newIds || undefined;

                    if (!id) {
                        return;
                    }

                    const cohort = findCohort(id);

                    // Only when creating a new application from scratch (e.g. not in
                    // handleSelectedAction, where we also call addCohortApplication()), we
                    // initialize the stripe plans and scholarship levels to
                    // those on the cohort
                    const newApplication = scope.addCohortApplication(cohort, {
                        scholarship_levels: cohort.scholarship_levels,
                        stripe_plans: cohort.stripe_plans,
                    });
                    newApplication.$$expanded = true;
                    updateFilteredCohortOptions();
                    scope.addCohort = undefined;
                });

                // keep track of initial status
                scope.$watchCollection('user.cohort_applications', () => {
                    if (!scope.user) {
                        return;
                    }
                    scope.scholarshipLevelOptions = {};

                    _.each(scope.user.cohort_applications, cohortApplication => {
                        scope.scholarshipLevelOptions[cohortApplication.id] = getScholarshipLevelOptions(
                            cohortApplication,
                        );
                        cohortApplication.$$initialStatus = cohortApplication.status;

                        // map the cohortApplication's level to a cohort level by value
                        cohortApplication.$$scholarshipLevelName = getScholarshipLevelOptionValueFromApplication(
                            cohortApplication,
                        );
                    });
                });

                //-------------------------
                // Retargeting
                //-------------------------

                scope.targetableProgramTypeOptions = application => {
                    const cohort = findCohort(application.cohort_id);

                    if (!cohort) {
                        return;
                    }

                    const programType = cohort.program_type;
                    if (!targetableProgramTypeOptionsMap[programType]) {
                        // For each program type that is different from
                        // the one in the application and that has a cohort in
                        // the future, get the next starting cohort.
                        targetableProgramTypeOptionsMap[programType] = scope.cohortProgramTypes
                            .map(programTypeOption => {
                                // filter out the program type that is already selected
                                if (programTypeOption.key === programType) {
                                    return undefined;
                                }

                                let newCohort;

                                const promotable = _.chain(Cohort.promotableProgramTypes)
                                    .pluck('key')
                                    .contains(programTypeOption.key)
                                    .value();
                                if (promotable) {
                                    const cohortPromotion = _.findWhere(scope.cohortPromotions, {
                                        program_type: programTypeOption.key,
                                    });
                                    newCohort =
                                        cohortPromotion &&
                                        _.findWhere(scope.cohortOptions, {
                                            id: cohortPromotion.cohort_id,
                                        });
                                } else {
                                    // find the next cohort in the future for this program type
                                    newCohort = _.chain(scope.cohortOptions)
                                        .where({
                                            program_type: programTypeOption.key,
                                        })
                                        .sortBy('startDate')
                                        .detect(o => o.startDate > new Date())
                                        .value();
                                }

                                if (newCohort) {
                                    return {
                                        label: programTypeOption.label,
                                        cohortId: newCohort.id,
                                    };
                                }

                                return undefined;
                            })
                            ?.filter(Boolean);
                    }

                    return targetableProgramTypeOptionsMap[programType];
                };

                scope.retargetApplication = (index, application, retargetedCohortId) => {
                    const oldCohort = findCohort(application.cohort_id);
                    const newCohort = findCohort(retargetedCohortId);

                    application.cohort_id = newCohort.id;
                    application.retargeted_from_program_type = oldCohort.program_type;

                    const applicationElement = elem.find(`[cohort-application][data-index="${index}"]`);

                    // For anything that is changing because we just
                    // picked 'retarget', flash a border to make
                    // it clear what just changed
                    const namesOfInputsToFlash = ['retargeted-from-program-type', 'cohort_id'];

                    // When retargeting someone into the business certificate,
                    // we generally want to accept them right away.  This is not
                    // true when retargeting someone to emba.  In that case they
                    // either stay as pending or become pre_accepted (if they have
                    // a full scholarship)
                    if (_.contains(Cohort.CERTIFICATE_PROGRAM_TYPES, newCohort.program_type)) {
                        application.status = 'accepted';
                        namesOfInputsToFlash.push('status');
                    }

                    const flashingElements = _.map(namesOfInputsToFlash, name => {
                        const el = applicationElement.find(`[name="${name}"]`).next().find('.selectize-input');
                        el.addClass('flash-blue-border');
                        return el;
                    });

                    $timeout(5000).then(() => {
                        _.invoke(flashingElements, 'removeClass', 'flash-blue-border');
                    });
                };

                //-------------------------
                // Payment Helpers
                //-------------------------

                function getScholarshipLevelOptionValueFromApplication(cohortApplication) {
                    if (!cohortApplication.scholarship_level) {
                        return;
                    }
                    const scholarshipLevelOption = _.find(scope.scholarshipLevelOptions[cohortApplication.id], {
                        value: cohortApplication.scholarship_level.name,
                    });
                    return scholarshipLevelOption && scholarshipLevelOption.value;
                }

                function getScholarshipPeriodDiscountLabel(scholarshipPeriod, application) {
                    return _.map(scholarshipPeriod, (coupon, planId) => {
                        const stripePlan = _.find(application.stripe_plans, {
                            id: planId,
                        });

                        if (!stripePlan && $window.RUNNING_IN_TEST_MODE) {
                            return;
                        }

                        let amountTotal;
                        if (coupon.amount_off) {
                            amountTotal = `$${(stripePlan.amount - coupon.amount_off) / 100}`;
                        } else {
                            amountTotal = `$${
                                (stripePlan.amount - (stripePlan.amount * coupon.percent_off) / 100) / 100
                            }`;
                        }

                        return amountTotal;
                    }).join(' / ');
                }

                // label function for select dropdown
                scope.getScholarshipLevelLabel = (application, level) => {
                    if (!application || !level) {
                        return;
                    }

                    const standardDiscountDetailsLabel = getScholarshipPeriodDiscountLabel(level.standard, application);
                    const earlyDiscountDetailsLabel = getScholarshipPeriodDiscountLabel(level.early, application);

                    return `${level.name} - ${standardDiscountDetailsLabel} (${earlyDiscountDetailsLabel} early)`;
                };

                // produces data provider options for select dropdown
                function getScholarshipLevelOptions(application) {
                    // generate value / label
                    const levelOptions = _.map(application.scholarship_levels, level => ({
                        value: level.name,
                        label: scope.getScholarshipLevelLabel(application, level),
                    }));

                    return levelOptions;
                }

                //-------------------------
                // Validation / Etc.
                //-------------------------

                scope.reconcileApplication = (application, index) => {
                    if (!application || !application.cohort_id) {
                        return;
                    }

                    // ensure we clean up any formerly-paid application billing fields, if we are
                    // toggling from a paid to non-paid cohort, since we allow changing ref's cohort_id values
                    // ensure we have a default for total_num_required_stripe_payments, if changing from a non-paid

                    const cohort = findCohort(application.cohort_id);

                    // Occassionally, applicants to certain program types (e.g. biz cert) are rejected and then immediately converted
                    // to the promoted EMBA cohort. Should that EMBA application ever be rejected for whatever reason, we want to make
                    // sure that we reinstate the user's previous cohort application that was rejected for the EMBA conversion. There's
                    // analogous logic on the server that handles this for normal rejection flows, but because of REST and how the user
                    // is updated in this context, we need to make sure that the previous cohort application's status has been set to
                    // 'accepted' to prevent the save from overriding the analogous logic on the server. See https://trello.com/c/BsQClzLk.
                    const previousCohortApplication = getPreviousApplication(application);
                    if (
                        application.status === 'rejected' &&
                        application.program_type === 'emba' &&
                        previousCohortApplication &&
                        previousCohortApplication.rejected_and_converted_to_emba
                    ) {
                        if (
                            $window.confirm(
                                "Are you sure you want to update this user's status to 'rejected'? Doing so will update their previous cohort application to 'accepted'.",
                            )
                        ) {
                            previousCohortApplication.status = 'accepted';
                        } else {
                            application.status = application.$$initialStatus;
                            elem.find('selectize[name="status"]')[index].selectize.setValue(application.status);
                        }
                    }

                    // Be sure to set the stripe plans and scholarship levels if the application
                    // does not have any but the cohort does. This can happen if, for example, the
                    // admin user is changing the cohort directly from a non-emba
                    // to emba cohort. This should only happen pre-registration. Otherwise,
                    // the admin should be going through the actions dropdown.
                    //
                    // See cohort_application.rb#set_stripe_plans_and_scholarship_levels
                    if (cohort.scholarship_levels && !application.scholarship_levels) {
                        application.stripe_plans = cohort.stripe_plans;
                        application.scholarship_levels = cohort.scholarship_levels;
                    }

                    if (cohort.supportsScholarshipLevels) {
                        // hunt down the appropriate scholarship level based on the value
                        // of $$scholarshipLevelName on the application
                        if (application.$$scholarshipLevelName) {
                            application.scholarship_level = _.find(
                                application.scholarship_levels,
                                level => level.name === application.$$scholarshipLevelName,
                            );
                        }

                        // if there is no scholarship_level, then set it to the cohort's first scholarship level,
                        // which should be 'No Scholarship', and reset $$scholarshipLevelName
                        if (!application.scholarship_level) {
                            application.scholarship_level = _.first(application.scholarship_levels);
                        }
                    } else {
                        application.scholarship_level = null;
                    }

                    // if you are re-targeting a cohort with a different program type
                    // these may be applicable. Otherwise, these should have been set on
                    // pre-validation of the CohortApplication, or they will otherwise be set
                    // when passing up a new CohortApplication
                    if (!cohort.scholarship_levels || !cohort.stripe_plans) {
                        application.scholarship_levels = null;
                        application.stripe_plans = null;
                    }

                    if (application.scholarship_levels) {
                        scope.scholarshipLevelOptions[application.id] = getScholarshipLevelOptions(application);
                        application.$$scholarshipLevelName =
                            application.$$scholarshipLevelName ||
                            getScholarshipLevelOptionValueFromApplication(application);
                    }

                    if (cohort.supportsPayments) {
                        // application.status special-case handling
                        if (
                            application.registered &&
                            _.contains(['expelled', 'rejected', 'deferred'], application.status)
                        ) {
                            if (
                                application.hasFullScholarship ||
                                (_.contains(['expelled', 'deferred'], application.status) &&
                                    application.total_num_required_stripe_payments === 0)
                            ) {
                                // if the user has full scholarship, there's nothing backing this record in Stripe. Okay to update (with confirmation), otherwise revert
                                if (
                                    $window.confirm(
                                        "Are you sure you want to update this registered user's status? This will automatically unregister them.",
                                    )
                                ) {
                                    application.registered = false;
                                } else {
                                    application.status = application.$$initialStatus;
                                    elem.find('selectize[name="status"]')[index].selectize.setValue(application.status);
                                }
                            } else {
                                // the user is registered with a Stripe plan and should be manually cleaned up first. revert.
                                $window.alert(
                                    `The user cannot be ${application.status} once accepted and registered. Please cancel the subscription in Stripe. If the user has remaining payments they will be unregistered and can have their status updated to ${application.status}.`,
                                );
                                application.status = application.$$initialStatus;
                                elem.find('selectize[name="status"]')[index].selectize.setValue(application.status);
                            }
                        }

                        // reset the $$initialStatus
                        application.$$initialStatus = application.status;
                    } else {
                        application.total_num_required_stripe_payments = null;
                    }
                };

                scope.showSlackRoomSelect = application => {
                    const cohort = findCohort(application.cohort_id);

                    if (
                        !cohort ||
                        !cohort.supportsSlackRooms ||
                        (application.status !== 'pre_accepted' && application.status !== 'accepted')
                    ) {
                        return false;
                    }

                    return _.any(cohort.slack_rooms);
                };

                scope.$on('editUserResetForm', () => {
                    scope.enrollmentProxy = {};
                    delete scope.user.$$subscriptionCancellationReason;
                    delete scope.user.$$issueRefund;
                    delete scope.user.$$enrollmentActionLabel;
                    delete scope.user.$$billingTransactionToRecord;
                });
            },
        };
    },
]);
