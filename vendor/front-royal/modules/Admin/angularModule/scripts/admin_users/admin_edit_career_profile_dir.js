import angularModule from 'Admin/angularModule/scripts/admin_module';
import copyToClipboard from 'copyToClipboard';
import template from 'Admin/angularModule/views/admin_users/admin_edit_career_profile.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('adminEditCareerProfile', [
    '$injector',

    function factory($injector) {
        const HasSortableColumnsMixin = $injector.get('HasSortableColumnsMixin');
        const HiringRelationship = $injector.get('HiringRelationship');
        const $rootScope = $injector.get('$rootScope');
        const $location = $injector.get('$location');
        const ngToast = $injector.get('ngToast');
        const CareerProfile = $injector.get('CareerProfile');
        const User = $injector.get('User');
        const scopeTimeout = $injector.get('scopeTimeout');
        const CohortApplication = $injector.get('CohortApplication');
        const CareerProfileList = $injector.get('CareerProfileList');
        const dateHelper = $injector.get('dateHelper');
        const LogInAs = $injector.get('LogInAs');
        const EditCareerProfileHelper = $injector.get('EditCareerProfileHelper');
        const TranslationHelper = $injector.get('TranslationHelper');
        const TranscriptHelper = $injector.get('TranscriptHelper');
        const ClientStorage = $injector.get('ClientStorage');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                user: '=thing',
                users: '=thingsOnCurrentPage',
                goBack: '&',
                gotoThing: '&',
                containerViewModel: '<',
            },
            link(scope) {
                scope.transcriptHelper = new TranscriptHelper();

                // initialize the tabs, remembering previous tab and checking for specified tab in search params
                const clientStorageTab = ClientStorage.getItem('adminEditCareerProfile_currentTab');

                const paramTab = $location.search().tab;
                scope.currentTab = paramTab || clientStorageTab || 'career-network';

                scope.$watch('currentTab', currentTab => {
                    if (currentTab) {
                        $location.search('page', null);
                        $location.search('tab', currentTab);
                        ClientStorage.setItem('adminEditCareerProfile_currentTab', currentTab);
                    }
                });

                const companyInterestLevels = $injector.get('NEW_COMPANY_INTEREST_LEVELS');
                const cardTranslationHelper = new TranslationHelper('careers.preview_candidate_card');

                scope.$on('$destroy', () => {
                    scope.containerViewModel.headerOverride = undefined;
                    scope.containerViewModel.email = undefined;
                    scope.containerViewModel.resumeUrl = undefined;
                    scope.containerViewModel.linkedInUrl = undefined;
                    scope.containerViewModel.onApplicantDetailPage = undefined;
                    $location.search('tab', null);
                });

                scope.containerViewModel.headerOverride = scope.user.email;
                scope.containerViewModel.email = scope.user.email;
                scope.containerViewModel.onApplicantDetailPage = true;
                scope.toggleDisableText = scope.user.career_profile.do_not_create_relationships
                    ? 'Disabled'
                    : 'Enabled';

                scope.dateHelper = dateHelper;

                HasSortableColumnsMixin.onLink(scope);
                scope.sort = {
                    column: 'hiring_manager_priority',
                    descending: false,
                };

                // The career profile attach to the user that's passed into the scope of the directive doesn't have all
                // of the necessary fields/information required for this directive, so we have to make an API call to
                // retrieve this data.
                CareerProfile.show(scope.user.career_profile.id, {
                    view: 'editable',
                }).then(response => {
                    _.extend(scope.user.career_profile, response.result);
                    scope.user.avatar_url = scope.user.career_profile.avatar_url;
                    scope.containerViewModel.resumeUrl =
                        scope.user.career_profile.resume &&
                        scope.user.career_profile.resume.formats &&
                        scope.user.career_profile.resume.formats.original &&
                        scope.user.career_profile.resume.formats.original.url;
                    scope.containerViewModel.linkedInUrl = scope.user.career_profile.li_profile_url;
                    setUserProxy();

                    // force a refresh of the card, since there are bind-once pieces to its UI
                    refreshCard();
                    setStatusOptions();

                    scope.careerProfileLoaded = true;
                });

                CareerProfileList.index({
                    'fields[]': ['id', 'name', 'career_profile_ids'],
                }).then(response => {
                    scope.careerProfileLists = sortCareerProfileLists(response.result);
                    setSelectedCareerProfileLists();
                    watchSelectedCareerProfileLists(); // initialize the watch on the selectedCareerProfileLists array now
                });

                HiringRelationship.index({
                    filters: {
                        candidate_id: scope.user.id,
                    },
                    'except[]': ['career_profile', 'hiring_application', 'conversation'],
                }).then(response => {
                    scope.hiringRelationships = response.result;
                });

                function setStatusOptions() {
                    // set the available status update options if we're on an applicant detail page
                    if (scope.containerViewModel.section === 'applicants') {
                        const lastCohortApplication = scope.userProxy.lastCohortApplication;
                        // filter for valid status options depending on the applied cohort program type and their current status
                        scope.statusOptions = _.filter(CohortApplication.VALID_STATUS_OPTIONS, statusOption => {
                            let isValidStatusOption = true;
                            // Make sure that we include their current status as an option even if it's one of these blacklisted statuses.
                            // Currently we don't allow admin users to update the status to expelled, deferred, or pre_accepted. This may
                            // be subject to change in the future. Just remove the status from the blacklisted array to include it as a
                            // valid option for selection.
                            if (
                                statusOption.value !== lastCohortApplication.status &&
                                _.contains(['expelled', 'deferred', 'pre_accepted'], statusOption.value)
                            ) {
                                isValidStatusOption = false;
                            }
                            return isValidStatusOption;
                        });

                        scope.careerProfileActiveInterestLevels = _.map(companyInterestLevels, interestLevel => ({
                            value: interestLevel,
                            text: cardTranslationHelper.get(`${interestLevel}_label`),
                            caption: cardTranslationHelper.get(`${interestLevel}_caption`),
                        }));
                    }
                }

                function refreshCard() {
                    scope.refreshing = true;
                    scopeTimeout(
                        scope,
                        () => {
                            scope.refreshing = false;
                        },
                        10,
                    );
                }

                //-----------------------
                // Edit Career Profile
                //-----------------------

                scope.gotoEditCareerProfile = () => {
                    scope.currentTab = 'edit-profile';

                    if (!scope.applicationFormSteps) {
                        scope.applicationFormSteps = EditCareerProfileHelper.getStepsForApplicantEditorForm(scope.user);
                    }

                    // wait for section to instantiate before navigating to first page
                    scopeTimeout(
                        scope,
                        () => {
                            scope.gotoEditCareerProfileStep($location.search().page - 1 || 0);
                        },
                        10,
                    );
                };

                // if we see that we're initially loading the page with the edit-profile tab open,
                // immediately call gotoEditCareerProfile to ensure the applicationFormSteps are set
                if (scope.currentTab === 'edit-profile') {
                    scope.gotoEditCareerProfile();
                }

                scope.gotoEditCareerProfileStep = $index => {
                    scope.activePageIndex = $index;
                    scope.$broadcast('gotoFormStep', scope.activePageIndex);
                    scope.applicationActiveStep = scope.applicationFormSteps[scope.activePageIndex];
                    scope.currentStepName = scope.applicationActiveStep.stepName;
                };

                //-----------------------
                // Copy URL link
                //-----------------------

                scope.copyCareerProfileDeepLinkUrl = () => {
                    copyToClipboard(scope.user.career_profile.deepLinkUrl, function onSuccess() {
                        ngToast.create({
                            content: 'Link copied to clipboard!',
                            className: 'success',
                        });
                    });
                };

                //------------------------
                // Log in as user
                //------------------------

                scope.logInAsUser = user => {
                    LogInAs.logInAsUser(user);
                };

                //------------------------
                // Career Profile Lists
                //------------------------

                scope.careerProfileListsPlaceholder = 'Add user to candidate list';

                // Returns a string for the career profile list option label. If the career profile list
                // is not a selected option, the returned string is the career profile list name. If the
                // list is selected, the returned string is the name of the list followed by the order
                // number of the career profile in the list.
                scope.getCareerProfileListLabel = careerProfileList => {
                    if (_.contains(scope.selectedCareerProfileLists, careerProfileList)) {
                        const index = _.findIndex(
                            careerProfileList.career_profile_ids,
                            careerProfileId => careerProfileId === scope.user.career_profile.id,
                        );
                        return `${careerProfileList.name} (#${index + 1} of ${
                            careerProfileList.career_profile_ids.length
                        } candidates)`;
                    }
                    return careerProfileList.name;
                };

                // Initializes a watch on the selectedCareerProfileLists array. This watch is wrapped in a function so we can have
                // better control over when we actually want to initialize it. If it's not wrapped in a function, this watch triggers
                // before the multi-select actually renders in the DOM, so we wait to call this function until after it renders.
                function watchSelectedCareerProfileLists() {
                    // Watch the selectedCareerProfileLists array for changes. If a career profile list is
                    // added to the array, the displayed user's career profile id is added to the added list's
                    // career_profile_ids array and then the list is saved. If a career profile list is removed
                    // from the array, the displayed users career profile id is removed from the list's career_profile_ids
                    // array and then the list is saved.
                    scope.$watchCollection('selectedCareerProfileLists', (newValue, oldValue) => {
                        // only save the career profile list when the selectedCareerProfileLists array
                        // is changed, not when it is initially set.
                        if (newValue && oldValue) {
                            if (newValue.length > oldValue.length) {
                                // new career profile list was added to queue
                                const newCareerProfileList = _.difference(newValue, oldValue)[0];
                                newCareerProfileList.career_profile_ids.push(scope.user.career_profile.id);
                                newCareerProfileList.save().then(() => {
                                    ngToast.create({
                                        content: `Candidate added to ${newCareerProfileList.name}`,
                                        className: 'success',
                                    });
                                });
                            } else if (newValue.length < oldValue.length) {
                                // previously selected career profile list was removed
                                const removedCareerProfileList = _.difference(oldValue, newValue)[0];
                                if (removedCareerProfileList) {
                                    // remove the displayed user's career profile id from the list's career_profile_ids array
                                    removedCareerProfileList.career_profile_ids = _.without(
                                        removedCareerProfileList.career_profile_ids,
                                        scope.user.career_profile.id,
                                    );
                                    removedCareerProfileList.save().then(() => {
                                        ngToast.create({
                                            content: `Candidate removed from ${removedCareerProfileList.name}`,
                                            className: 'success',
                                        });
                                    });
                                }
                            }
                        }
                    });
                }

                //-----------------------------------
                // Saving the user and career profile
                //-----------------------------------

                function refreshUserProxy(user) {
                    _.extend(scope.user, user);
                    setUserProxy();
                    setStatusOptions();
                }

                scope.toggleDisable = () => {
                    scope.userProxy.career_profile.do_not_create_relationships = !scope.userProxy.career_profile
                        .do_not_create_relationships;
                    scope.userProxy.career_profile
                        .save()
                        .then(response => {
                            _.extend(scope.user.career_profile, response.result);
                            setUserProxy();

                            let contentString;
                            if (scope.user.career_profile.do_not_create_relationships) {
                                scope.toggleDisableText = 'Disabled';
                                contentString = 'disabled';
                            } else {
                                scope.toggleDisableText = 'Enabled';
                                contentString = 'enabled';
                            }
                            ngToast.create({
                                content: `Career Profile ${contentString}`,
                                className: 'success',
                            });
                        })
                        .catch(() => {
                            // if error occurred, revert the career profile back to it's previous state
                            scope.userProxy.career_profile.do_not_create_relationships = !scope.userProxy.career_profile
                                .do_not_create_relationships;
                        });
                };

                scope.saveAndSendFeedback = () => {
                    const meta = {
                        send_feedback_email: true,
                    };
                    scope.save(meta);
                };

                scope.confirmProfileContentAndStatus = () => {
                    const meta = {
                        confirmed_by_internal: true,
                        confirmed_internally_by: $rootScope.currentUser.name,
                    };
                    scope.userProxy.career_profile.save(meta).then(response => {
                        refreshUserProxy({
                            career_profile: response.result,
                        });
                        ngToast.create({
                            content: 'Profile Content and Status Confirmed',
                            className: 'success',
                        });
                    });
                };

                scope.save = (meta = {}) => {
                    // only save if either one of the proxies is valid or if an admin is confirming the profile content and status
                    if (scope.userProxyIsValid || scope.careerProfileProxyIsValid || meta.confirmed_by_internal) {
                        scope.userProxy.save(meta).then(response => {
                            refreshUserProxy(response.result);
                            ngToast.create({
                                content: 'Applicant Saved',
                                className: 'success',
                            });
                        });
                    }
                };

                // ------------------
                // Proxy handling
                // ------------------

                scope.$watch('user', setUserProxy);

                scope.$on('savedCareerProfile', () => {
                    refreshUserProxy(scope.userProxy);
                    refreshCard();
                });

                // resets the user proxy based on scope.user or sets it to an empty object if no scope.user
                function setUserProxy() {
                    if (scope.user) {
                        scope.userProxy = User.new(scope.user.asJson());
                    } else {
                        scope.userProxy = {};
                    }
                }

                function setSelectedCareerProfileLists() {
                    if (scope.careerProfileLists) {
                        // set the selectedCareerProfileLists to an array of career profile lists that contain the displayed user's
                        // career profile id in the list's career_profile_ids property.
                        scope.selectedCareerProfileLists = _.filter(scope.careerProfileLists, careerProfileList =>
                            _.contains(careerProfileList.career_profile_ids, scope.user.career_profile.id),
                        );
                    } else {
                        scope.selectedCareerProfileLists = [];
                    }
                }

                // returns a new array of the passed in candidate lists sorted by name
                function sortCareerProfileLists(careerProfileLists) {
                    return _.sortBy(careerProfileLists, careerProfileList => careerProfileList.name);
                }

                // Proxy dirty checking validations for last cohort application status
                scope.$watchCollection('userProxy.lastCohortApplication', () => {
                    scope.userProxyIsValid =
                        scope.user.lastCohortApplicationStatus !== scope.userProxy.lastCohortApplicationStatus;
                });

                // Proxy dirty checking validations for career profile
                scope.$watchCollection('userProxy.career_profile', () => {
                    const careerProfileProxy = scope.userProxy.career_profile;
                    const careerProfile = scope.user.career_profile;

                    if (!careerProfile || !careerProfileProxy) {
                        scope.careerProfileProxyIsValid = false;
                        return;
                    }

                    // dirty checking
                    if (
                        careerProfileProxy.do_not_create_relationships !== careerProfile.do_not_create_relationships ||
                        careerProfileProxy.profile_feedback !== careerProfile.profile_feedback ||
                        careerProfileProxy.interested_in_joining_new_company !==
                            careerProfile.interested_in_joining_new_company
                    ) {
                        scope.careerProfileProxyIsValid = true;
                    } else {
                        scope.careerProfileProxyIsValid = false;
                    }
                });
            },
        };
    },
]);
