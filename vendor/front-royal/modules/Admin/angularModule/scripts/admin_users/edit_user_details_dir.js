import angularModule from 'Admin/angularModule/scripts/admin_module';
import countries from 'countries';
import template from 'Admin/angularModule/views/admin_users/edit_user_details.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('editUserDetails', [
    '$injector',

    function factory($injector) {
        const User = $injector.get('User');
        const LessonsCtrlBase = $injector.get('LessonsCtrlBase');
        const HasSortableColumnsMixin = $injector.get('HasSortableColumnsMixin');
        const ngToast = $injector.get('ngToast');
        const $auth = $injector.get('$auth');
        const $window = $injector.get('$window');
        const $rootScope = $injector.get('$rootScope');
        const DialogModal = $injector.get('DialogModal');
        const ConfigFactory = $injector.get('ConfigFactory');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                users: '<?',
                user: '<',
                globalRoleOptions: '<',
                editorRoleOptions: '<',
                institutionOptions: '<',
                cohortOptions: '<',
                cohortPromotions: '<',
                enableUpdate: '<',
                dupeUser: '<', // if not false, then edit a clone of the user that is passed in until save is pressed
            },
            link(scope) {
                HasSortableColumnsMixin.onLink(scope);

                scope.EDITOR_FORM_CLASSES = $injector.get('EDITOR_FORM_CLASSES');

                scope.Locale = $injector.get('Locale');
                scope.countryOptions = countries;

                scope.form_errors = {};
                scope.$watch('editUser.phone', (newVal, oldVal) => {
                    if (
                        (scope.editUser && scope.editUser.phone && scope.form_errors.phone) ||
                        (newVal === '' && oldVal)
                    ) {
                        scope.form_errors.phone = 'Invalid phone number';
                        scope.preventUpdate = true;
                    } else {
                        scope.preventUpdate = false;
                    }
                });

                // We want to ensure that the user's associated professional organization locale also
                // gets updated if the edited user's locale changes in the form.
                scope.$watchGroup(['editUser.professional_organization', 'editUser.pref_locale'], () => {
                    if (scope.editUser && scope.editUser.professional_organization && scope.editUser.pref_locale) {
                        _.extend(scope.editUser.professional_organization, {
                            locale: scope.editUser.pref_locale,
                        });
                    }
                });

                // by default, sort columns by tag
                scope.sort = {
                    column: 'tag',
                    descending: false,
                };

                // Get a list of all the lessons
                scope.archived = false;

                scope.$watch('editUser.globalRole.name', roleName => {
                    if (roleName === 'editor') {
                        // eslint-disable-next-line no-new
                        new LessonsCtrlBase(scope);
                    }
                });

                // default to display assigned tab if available
                scope.currentTab = 'assigned';

                //-------------------------
                // User
                //-------------------------

                function rebuildCurrentUserIfNeeded() {
                    if (scope.user.id !== $rootScope.currentUser.id) {
                        return;
                    }

                    // authoritatively rebuild currentUser to ensure group assignment is valid
                    $auth.user.signedIn = false;
                    $auth.validateUser();
                }

                scope.$watch('user', () => {
                    scope.globalRoleId = null;
                    scope.stripeUrl = null;
                    if (!scope.user) {
                        return;
                    }
                    setEditUser(scope.user);

                    ConfigFactory.getConfig().then(config => {
                        const isProduction = config.appEnvType() === 'production';
                        scope.globalRoleId = scope.user.globalRole.id;
                        scope.stripeUrl = `https://dashboard.stripe.com/${isProduction ? '' : 'test/'}customers/${
                            scope.user.id
                        }`;
                    });
                });

                function setEditUser(user) {
                    if (scope.dupeUser !== false) {
                        // we need to duplicate the user to ensure that, if changes are never saved,
                        // then they do not get persisted in the UI.
                        // see also: https://trello.com/c/xa4zY5EU/232-0-5-iguana-provisional-changes
                        scope.editUser = User.new(user.asJson());
                    } else {
                        scope.editUser = user;
                    }
                }

                scope.sortOverride = lesson => {
                    if (scope.sort.column === 'tag' || scope.sort.column === 'title') {
                        return lesson[scope.sort.column];
                    }
                    return scope.editUser.lessonPermissions[lesson.id] === scope.sort.column;
                };

                scope.resetForm = () => {
                    scope.initializeLessons();

                    updateFilteredInstitutionOptions();

                    // re-clone the user so that subsequent changes are not
                    // visible outside this form
                    setEditUser(scope.user);

                    scope.$broadcast('editUserResetForm');

                    scope.editUserForm.$setPristine(true);
                };

                scope.updateUserDetails = () => {
                    scope.preventUpdate = true;

                    scope.editUser
                        .save({
                            subscription_cancellation_reason: scope.editUser.$$subscriptionCancellationReason,
                            issue_refund: scope.editUser.$$issueRefund,
                            enrollment_action_label: scope.editUser.$$enrollmentActionLabel,
                            billing_transaction_to_record:
                                scope.editUser.$$billingTransactionToRecord &&
                                scope.editUser.$$billingTransactionToRecord.asJson(),
                        })
                        .then(response => {
                            scope.user = response.result;

                            // update the containing list if it's available
                            // see https://trello.com/c/xa4zY5EU/260-0-5-iguana-provisional-changes
                            // for maybe a nicer way of handling this
                            if (scope.users) {
                                const userIndex = _.pluck(scope.users, 'id').indexOf(scope.user.id);
                                scope.users[userIndex] = scope.editUser;
                            }

                            ngToast.create({
                                content: `${scope.editUser.accountId} updated.`,
                                className: 'success',
                            });

                            rebuildCurrentUserIfNeeded();

                            scope.resetForm();

                            scope.preventUpdate = false;

                            if (response.meta && response.meta.error_issuing_refund) {
                                DialogModal.alert({
                                    title: 'Error issuing refund',
                                    content: response.meta.error_issuing_refund,
                                });
                            }
                        });
                };

                scope.cancellationReasons = [
                    'Deferral',
                    'Requested to leave program',
                    'Inability to collect',
                    'Payment outside product',
                ];

                //-------------------------
                // Roles
                //-------------------------

                function updateFilteredRoleOptions() {
                    if (!scope.globalRoleOptions || !scope.editUser) {
                        scope.filteredRoleOptions = [];
                        return;
                    }

                    const filtered = [];
                    let defaultRole;

                    scope.globalRoleOptions.forEach(roleOption => {
                        filtered.push({
                            value: roleOption.id,
                            text: roleOption.name.toUpperCase(),
                        });

                        if (roleOption.id === scope.editUser.globalRole.id) {
                            defaultRole = roleOption;
                        }
                    });
                    scope.filteredRoleOptions = filtered;

                    if (!scope.addRole) {
                        scope.addRole = defaultRole.id;
                    }
                }

                scope.$watch('globalRoleOptions', updateFilteredRoleOptions);

                // watch for new role selection
                scope.$watch('addRole', newIds => {
                    const id = newIds || undefined;

                    if (!id) {
                        return;
                    }

                    // eslint-disable-next-line no-restricted-syntax
                    for (const role of scope.globalRoleOptions) {
                        if (String(role.id) === id) {
                            scope.editUser.globalRole = role;
                        }
                    }
                });

                scope.canCreateLessonClasses = () => {
                    if (scope.editUser.canCreateLessons) {
                        return 'btn btn-success';
                    }
                    return 'btn btn-danger';
                };

                scope.canCreateLessonMessage = () => {
                    if (scope.editUser.canCreateLessons) {
                        return 'Lesson Creation Enabled';
                    }
                    return 'Lesson Creation Disabled';
                };

                //-------------------------
                // Institutions
                //-------------------------

                function updateFilteredInstitutionOptions() {
                    if (!scope.institutionOptions || !scope.editUser) {
                        scope.filteredInstitutionOptions = [];
                        return;
                    }

                    const filtered = [];
                    const existingInstitutionNames = scope.editUser.institutionNames();
                    scope.institutionOptions.forEach(institutionOption => {
                        if (!existingInstitutionNames.includes(institutionOption)) {
                            filtered.push({
                                value: institutionOption.id,
                                text: institutionOption.name,
                            });
                        }
                    });
                    scope.filteredInstitutionOptions = filtered;
                }

                scope.$watch('institutionOptions', updateFilteredInstitutionOptions);

                // watch for new institions selection
                scope.$watch('addInstitution', newIds => {
                    const id = newIds || undefined;

                    if (!id) {
                        return;
                    }

                    // eslint-disable-next-line no-restricted-syntax
                    for (const institution of scope.institutionOptions) {
                        if (String(institution.id) === id) {
                            scope.editUser.addInstitution(institution);
                        }
                    }

                    updateFilteredInstitutionOptions();

                    scope.addInstitution = undefined;
                });

                scope.removeInstitution = institution => {
                    if ($window.confirm(`Are you sure you want to remove the user from ${institution.name}?`)) {
                        scope.editUser.removeInstitution(institution);
                        updateFilteredInstitutionOptions();
                    }
                };

                function updateActiveInstitutionOptions() {
                    if (!scope.editUser) {
                        scope.activeInstitutionOptions = [];
                        return;
                    }

                    if (!scope.editUser.institutions) {
                        scope.editUser.institutions = [];
                    }

                    const institutionIds = scope.editUser.institutions.map(i => i.id);
                    scope.activeInstitutionOptions = scope.filteredInstitutionOptions.filter(o =>
                        institutionIds.includes(o.value),
                    );

                    scope.tempActiveInstitutionId = scope.editUser.active_institution?.id;
                }
                scope.$watchCollection('scope.editUser.institutions', updateActiveInstitutionOptions);
                scope.$watchGroup(
                    ['editUser.active_institution.id', 'filteredInstitutionOptions'],
                    updateActiveInstitutionOptions,
                );

                scope.updateActiveInstitutionOnUser = () => {
                    scope.editUser.active_institution = scope.institutionOptions.find(
                        i => i.id === scope.tempActiveInstitutionId,
                    );
                };

                // Once active_institution is set, we don't want to allow unsetting.
                // So if the institution is deleted from the user, try to default it
                // to the first institution.
                scope.$watch('tempActiveInstitutionId', (newVal, oldVal) => {
                    if (angular.isDefined(oldVal) && !newVal) {
                        scope.tempActiveInstitutionId = scope.editUser?.institutions[0]?.id;
                        scope.updateActiveInstitutionOnUser();
                    }
                });

                //-------------------------
                // Hiring managers
                //-------------------------

                scope.hiringApplicationOptions = [
                    {
                        value: '__NONE__',
                        text: 'None',
                    },
                    {
                        value: 'pending',
                        text: 'Pending',
                    },
                    {
                        value: 'accepted',
                        text: 'Accepted',
                    },
                    {
                        value: 'rejected',
                        text: 'Rejected',
                    },
                ];

                //-------------------------
                // Lesson Permissions
                //-------------------------

                function updateAllLessonPermissions(tabType, roleName) {
                    const lessons = scope.lessonsByTab[tabType];
                    if (lessons) {
                        lessons.forEach(lesson => {
                            if (scope.editUser.id !== lesson.author.id) {
                                scope.updateLessonPermissions(lesson, roleName);
                            }
                        });
                    }
                }

                scope.initializeLessons = () => {
                    if (!scope.lessons) {
                        return;
                    }

                    // create a lookup hash for our tabbed interface
                    scope.lessonsByTab = {
                        assigned: [],
                        unassigned: [],
                    };

                    scope.editUser.$$lessonPermissions = undefined;

                    // build out lookup hash and ensure authors always retain edit permissions
                    scope.lessons.forEach(lesson => {
                        if (lesson.id) {
                            // find and set editor permissions on any lessons where the editUser is the author
                            if (lesson.author && lesson.author.id === scope.editUser.id) {
                                scope.updateLessonPermissions(lesson, 'lesson_editor');
                            }

                            // place in the right tab filtering
                            if (scope.editUser.lessonPermissions[lesson.id]) {
                                scope.lessonsByTab.assigned.push(lesson);
                            } else {
                                scope.lessonsByTab.unassigned.push(lesson);
                            }
                        }
                    });
                };

                // initialize lesson permissions on update
                scope.$watch('lessons', scope.initializeLessons);

                scope.onAllClicked = (tabType, roleName) => {
                    updateAllLessonPermissions(tabType, roleName);
                };

                scope.clearAllClicked = tabType => {
                    $(`[data-id="${tabType}"] input:radio[name="all"]`).attr('checked', false);
                    scope.allSelectedValue = undefined;
                };

                scope.updateLessonPermissions = (lesson, roleName) => {
                    let i = 0;
                    const indicesToRemove = [];
                    // find indices of all other lesson override roles for this lessonId
                    scope.editUser.roles.forEach(role => {
                        if (
                            role.resource_id === lesson.id &&
                            (!roleName || scope.editUser.isScopedlessonPermission(role.name))
                        ) {
                            indicesToRemove.push(i);
                        }
                        i += 1;
                    });

                    // remove all indicesToRemove from lesson override roles
                    for (i = indicesToRemove.length - 1; i >= 0; i--) {
                        scope.editUser.roles.splice(indicesToRemove[i], 1);
                    }

                    // add this lesson override role if supplied (otherwise defaults to hidden)
                    if (roleName) {
                        const newRole = {
                            resource_id: lesson.id,
                            resource_type: 'Lesson',
                            name: roleName,
                        };
                        scope.editUser.roles.push(newRole);
                    }

                    // clear out the cached version of lessonPermissions so the next getter refreshes it
                    scope.editUser.$$lessonPermissions = undefined;
                };
            },
        };
    },
]);
