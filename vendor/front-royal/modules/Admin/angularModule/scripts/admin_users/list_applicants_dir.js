import angularModule from 'Admin/angularModule/scripts/admin_module';
import template from 'Admin/angularModule/views/admin_users/list_applicants.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('listApplicants', [
    '$injector',

    function factory($injector) {
        const editContentItemListMixin = $injector.get('editContentItemListMixin');
        const Cohort = $injector.get('Cohort');
        const CohortApplication = $injector.get('CohortApplication');
        const TranslationHelper = $injector.get('TranslationHelper');
        const CareerProfileFilterSet = $injector.get('CareerProfileFilterSet');
        const locationPlaceDetails = $injector.get('LOCATION_PLACE_DETAILS');
        const $timeout = $injector.get('$timeout');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                containerViewModel: '<',
            },
            link(scope) {
                scope.containerViewModel.section = 'applicants';

                // We need to set this here rather than passing in true directly from the template so it's available
                // in the editContentItemListMixin, which uses this value to determine what filters should be passed
                // along to the server.
                scope.usingServerPagination = true;

                //-------------------------
                // Config
                //-------------------------

                scope.indexParamsForUsers = {
                    has_cohort_application: true,
                    select_career_profile: true,
                    per_page: 100,
                };

                //-------------------------
                // Columns
                //-------------------------

                scope.displayColumns = [
                    {
                        prop: 'careerProfileCreatedAt',
                        type: 'time',
                        label: 'Created',
                        classes: ['hidden-xs', 'hidden-sm', 'hidden-md'],
                    },
                    {
                        prop: 'careerProfileUpdatedAt',
                        type: 'time',
                        label: 'Updated',
                        classes: ['hidden-xs', 'hidden-sm', 'hidden-md'],
                    },
                    {
                        prop: 'careerProfileLastConfirmedAtByStudent',
                        type: 'time',
                        label: 'Last Confirmed',
                        classes: ['hidden-xs', 'hidden-sm', 'hidden-md'],
                    },
                    {
                        prop: 'name',
                        type: 'text',
                        label: 'Name',
                    },
                    {
                        prop: 'email',
                        type: 'text',
                        label: 'Email',
                    },
                    {
                        prop: 'careerProfileLocation',
                        type: 'text',
                        label: 'Location',
                        classes: ['hidden-xs'],
                    },
                    {
                        prop: 'cohortApplicationsStatusesString',
                        type: 'text',
                        label: 'Applications',
                    },
                    {
                        prop: 'careerProfileLastCalculatedCompletePercentage',
                        type: 'perc100',
                        label: '%',
                        classes: ['hidden-xs'],
                    },
                    {
                        prop: 'careerProfileStatus',
                        type: 'text',
                        label: 'Status',
                        classes: ['hidden-xs', 'hidden-sm', 'hidden-md'],
                    },
                    {
                        prop: 'careerProfileFeedback',
                        type: 'checkIfTrue',
                        label: 'Fix Card?',
                        classes: ['hidden-xs', 'hidden-sm', 'hidden-md'],
                    },
                ];

                //-------------------------
                // Filters
                //-------------------------

                scope.proxy = {};

                // there are some cases when users don't have a relevant_cohort set on their user,
                // depending on how this directive was rendered i.e. via a hard link to a specific
                // applicant's details page or landing on this page via the Manage Careers link in
                // the Admin tab. Either way, we want to make sure that the user has a relevant_cohort
                // because other directives may depend on it being present.
                scope.$watchGroup(['cohortOptions', 'users'], () => {
                    if (!scope.cohortOptions || !scope.users) {
                        return;
                    }

                    const cohorts = _.indexBy(scope.cohortOptions, 'id');
                    _.each(scope.users, user => {
                        user.relevant_cohort = cohorts[user.relevant_cohort_id];
                    });
                });

                // cohort related filters
                let cohortOptionsPromise;
                scope.$watch('isEditing', isEditing => {
                    // In order to make things fast when loading up a page for a particular
                    // applicant, do not load the cohorts until we are on the list page.
                    // Wait a beat so that the users will start loading first
                    // Note: We've since improved how slow this was originally
                    if (isEditing === false && !cohortOptionsPromise) {
                        cohortOptionsPromise = $timeout()
                            .then(() =>
                                Cohort.index({
                                    fields: ['BASIC_FIELDS'],
                                }),
                            )
                            .then(response => {
                                scope.cohortOptions = response.result;
                            });
                    }
                });

                const statusOptions = CohortApplication.VALID_STATUS_OPTIONS.concat(
                    CohortApplication.VALID_GRADUATION_STATUS_OPTIONS,
                );
                const pendingStatusIndex = _.findIndex(statusOptions, statusOption => statusOption.value === 'pending');
                statusOptions.splice(pendingStatusIndex, pendingStatusIndex + 1);
                scope.cohortStatusOptions = statusOptions;
                scope.lastCohortStatusOptions = CohortApplication.VALID_STATUS_OPTIONS;

                // boolean filters
                scope.careerProfileCompleteOptions = getBooleanFilterOptions('Complete', 'Incomplete');
                scope.careerProfileDisabledOptions = getBooleanFilterOptions('Disabled', 'Enabled');
                scope.canEditCareerProfileOptions = getBooleanFilterOptions('Can Edit Profile', 'Cannot Edit Profile');
                scope.careerProfileFeedbackOptions = getBooleanFilterOptions(
                    'Has Profile Feedback',
                    'No Profile Feedback',
                );
                scope.careerProfileUpdatedAfterFeedbackSentOptions = getBooleanFilterOptions(
                    'Has Been Updated',
                    'Has Not Been Updated',
                );
                scope.applicationRegisteredOptions = getBooleanFilterOptions('Registered', 'Unregistered');

                // last confirmed by internal or student filters
                scope.lastConfirmedAtByStudentOptions = getLastConfirmedFilterOptions();
                scope.lastConfirmedAtByInternalOptions = getLastConfirmedFilterOptions();

                // filters from keys
                const fieldOptionsTranslationHelper = new TranslationHelper('careers.field_options');
                const skillsTranslationHelper = new TranslationHelper('careers.edit_career_profile.select_skills_form');
                const jobPreferencesTranslationHelper = new TranslationHelper(
                    'careers.edit_career_profile.job_preferences_form',
                );
                const skillsKeys = _.flatten(
                    _.values(_.chain(_.omit($injector.get('CAREERS_SKILLS_KEYS'), 'languages')).value()),
                );
                const locationsOfInterestKeys = _.without(
                    $injector.get('CAREERS_LOCATIONS_OF_INTEREST_KEYS'),
                    'flexible',
                    'show_all_locations',
                );
                scope.careerProfileSkillsOptions = getSelectizeOptionsForKeys(skillsKeys, skillsTranslationHelper);
                scope.careerProfileLocationsOfInterestOptions = getSelectizeOptionsForKeys(
                    locationsOfInterestKeys,
                    fieldOptionsTranslationHelper,
                );
                scope.careerProfileEmploymentTypesOfInterestOptions = getSelectizeOptionsForKeys(
                    ['permanent', 'part_time', 'contract', 'internship'],
                    jobPreferencesTranslationHelper,
                );
                scope.careerProfileYearsOfExperienceOptions = getSelectizeOptionsForKeys(
                    $injector.get('CAREERS_YEARS_EXPERIENCE_KEYS'),
                    fieldOptionsTranslationHelper,
                );
                scope.careerProfileIndustryOptions = getSelectizeOptionsForKeys(
                    $injector.get('CAREERS_INDUSTRY_KEYS'),
                    fieldOptionsTranslationHelper,
                );
                scope.careerProfileActiveOptions = getSelectizeOptionsForKeys(
                    $injector.get('NEW_COMPANY_INTEREST_LEVELS'),
                );

                scope.$watchCollection('proxy.primaryAreasOfInterest', primaryAreasOfInterest => {
                    scope.clientFilters.career_profile_roles = CareerProfileFilterSet.buildRolesFilter(
                        primaryAreasOfInterest,
                    );
                });

                scope.$watchCollection('proxy.careerProfileLocationKeys', locationKeys => {
                    scope.clientFilters.career_profile_places = _.map(locationKeys, key => locationPlaceDetails[key]);
                });

                //-------------------------
                // Helpers
                //-------------------------

                function getLastConfirmedFilterOptions() {
                    return [
                        {
                            label: 'Never Confirmed',
                            value: 'NULL',
                        },
                        {
                            label: 'Last Day',
                            value: getLastConfirmedAtByStudentDate(1),
                        },
                        {
                            label: 'Last 7 Days',
                            value: getLastConfirmedAtByStudentDate(7),
                        },
                        {
                            label: 'Last 2 Weeks',
                            value: getLastConfirmedAtByStudentDate(14),
                        },
                        {
                            label: 'Last 3 Weeks',
                            value: getLastConfirmedAtByStudentDate(21),
                        },
                        {
                            label: 'Last 4 Weeks',
                            value: getLastConfirmedAtByStudentDate(28),
                        },
                        {
                            label: 'Ever Confirmed',
                            value: 'NOT NULL',
                        },
                    ];
                }

                // Returns a string representation of a Date object based on the daysOffset
                // relative to today at midnight.
                // @param daysOffset - an integer value that represents the number days in the past relative to today
                // @return string representation of Date object (works with Postgresql date comparison)
                function getLastConfirmedAtByStudentDate(daysOffset) {
                    const date = new Date();
                    date.setHours(0, 0, 0, 0);
                    date.setDate(date.getDate() - daysOffset);
                    return date.toDateString();
                }

                function getBooleanFilterOptions(trueLabel, falseLabel) {
                    return [
                        {
                            label: trueLabel,
                            value: 'true',
                        },
                        {
                            label: falseLabel,
                            value: 'false',
                        },
                    ];
                }

                function getSelectizeOptionsForKeys(keys, translationHelper) {
                    return _.map(keys, key => ({
                        value: key,
                        label: (translationHelper && translationHelper.get(key)) || key,
                    }));
                }

                //------------------------------
                // Editable things list support
                //------------------------------

                const booleanProperties = [];
                const defaultFilters = [];
                const customFilterCallback = null;
                const manualFiltering = true;

                // wire up filtering support
                editContentItemListMixin.onLink(
                    scope,
                    'listApplicants',
                    defaultFilters,
                    booleanProperties,
                    customFilterCallback,
                    manualFiltering,
                );
            },
        };
    },
]);
