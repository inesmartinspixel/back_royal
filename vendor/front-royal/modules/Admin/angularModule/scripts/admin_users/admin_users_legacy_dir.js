import angularModule from 'Admin/angularModule/scripts/admin_module';
import 'ExtensionMethods/array';
import template from 'Admin/angularModule/views/admin_users/admin_users_legacy.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('adminUsersLegacy', [
    '$injector',

    function factory($injector) {
        const User = $injector.get('User');
        const ngToast = $injector.get('ngToast');
        const DialogModal = $injector.get('DialogModal');
        const HasSortableColumnsMixin = $injector.get('HasSortableColumnsMixin');
        const AppHeaderViewModel = $injector.get('Navigation.AppHeader.AppHeaderViewModel');
        const LogInAs = $injector.get('LogInAs');
        const Cohort = $injector.get('Cohort');
        const dateHelper = $injector.get('dateHelper');
        const CohortApplication = $injector.get('CohortApplication');
        const guid = $injector.get('guid');

        return {
            restrict: 'E',
            templateUrl,
            scope: {},
            link(scope) {
                HasSortableColumnsMixin.onLink(scope);
                AppHeaderViewModel.setTitleHTML('USER<br>ADMIN');
                scope.dateHelper = dateHelper;
                scope.showCreateUser = false;
                scope.cohortStatuses = CohortApplication.VALID_STATUSES;

                scope.getCohortName = id => {
                    const option = _.find(scope.cohortOptions, cohortOption => cohortOption.id === id);

                    return option ? option.name : null;
                };

                scope.getCohortGroups = user => {
                    const cohortIds = [];

                    if (user.hasAcceptedCohort) {
                        cohortIds.push(user.acceptedCohortApplication.cohort_id);
                    } else if (user.hasPendingOrPreAcceptedCohortApplication) {
                        cohortIds.push(user.pendingOrPreAcceptedCohortApplication.cohort_id);
                    }

                    return _.chain(scope.cohortOptions)
                        .select(cohortOption => _.contains(cohortIds, cohortOption.id))
                        .pluck('groups')
                        .flatten()
                        .uniq(group => group.name)
                        .value();
                };

                //-------------------------
                // Filtering
                //-------------------------

                scope.params = {
                    select_professional_organization: true,
                    select_subscriptions: true,
                    select_active_deferral_link: true,
                };

                //------------------------
                // Pagination
                //------------------------

                scope.$watchCollection('sort', (newValue, oldValue) => {
                    scope.params.sort = scope.sort.column;
                    scope.params.direction = scope.sort.descending ? 'desc' : 'asc';
                    if (oldValue && oldValue !== newValue) {
                        scope.search();
                    }
                });

                scope.$watch('page', () => {
                    scope.params.page = scope.page;
                });

                scope.updatePage = page => {
                    scope.page = page;
                    scope.params.page = page;
                    scope.loadUsers();
                };

                function updatePageInfo(meta) {
                    scope.totalCount = meta.total_count;
                    scope.perPage = meta.per_page;
                    scope.page = meta.page;
                    scope.totalPages = Math.ceil(scope.totalCount / scope.perPage);
                }

                //-------------------------
                // Data Management
                //-------------------------

                scope.search = () => {
                    scope.params.page = 1;
                    scope.loadUsers();
                };

                let extraDataLoaded;
                let institutionLookup;
                scope.loadUsers = extraParams => {
                    scope.users = undefined;
                    let params = angular.extend({}, scope.params, extraParams);

                    // We only need to load the cohorts, groups, etc. once
                    if (extraDataLoaded) {
                        params = angular.extend(params, {
                            do_not_load_extra_data: true,
                        });
                    }
                    scope.loading = true;
                    User.index(params).then(response => {
                        scope.loading = false;
                        if (!extraDataLoaded) {
                            // build out data providers based on response
                            scope.groupOptions = response.meta.available_groups;
                            scope.globalRoleOptions = response.meta.global_roles;
                            scope.editorRoleOptions = response.meta.scoped_roles;
                            scope.institutionOptions = response.meta.institutions;
                            scope.cohortPromotions = response.meta.cohort_promotions;

                            scope.cohortOptions = _.map(response.meta.cohorts, attrs => Cohort.new(attrs));

                            // stitch together institutions' additional info
                            institutionLookup = {};
                            scope.institutionOptions.forEach(institution => {
                                institutionLookup[institution.id] = institution;
                            });
                        }
                        extraDataLoaded = true;

                        const users = response.result;
                        users.forEach(user => {
                            // We only send back the ids in the SQL amalgamation in users_controller,
                            // so stitch up all the ids to actual institution values.
                            if (user.institutions) {
                                user.institutions = user.institutions.map(
                                    institution => institutionLookup[institution.id],
                                );
                            }
                            if (user.active_institution) {
                                user.active_institution = institutionLookup[user.active_institution.id];
                            }
                        });

                        // complete page update
                        scope.users = users;

                        // this just allows us to do ng-if="users" rather than ng-if="users.length > 0" in the template
                        if (!_.any(scope.users)) {
                            scope.users = null;
                        }
                        updatePageInfo(response.meta);
                    });
                };

                // This is a hack.  We are doing a query for an id we know does not exist.
                // This allows for the cohorts, groups, etc. to start loading while the user
                // starts to fill out the search form.
                scope.loadUsers({
                    freetext_search_param: guid.generate(),
                });

                scope.deleteUser = user => {
                    scope.__onDeleteConfirm = () => {
                        user.destroy().then(() => {
                            if (scope.users) {
                                Array.remove(scope.users, user);
                            }
                            ngToast.create({
                                content: `${user.email} successfully deleted.`,
                                className: 'success',
                            });
                        });
                    };

                    DialogModal.confirm({
                        confirmDeleteThing: user.name,
                        confirmDeleteByTyping: user.email,
                        confirmCallback: scope.__onDeleteConfirm,
                    });
                };

                scope.logInAsUser = user => {
                    LogInAs.logInAsUser(user);
                };

                scope.$watch('showCreateUser', () => {
                    scope.showingPopupForm = scope.showCreateUser;
                });

                scope.editUser = user => {
                    DialogModal.alert({
                        content:
                            '<edit-user-details enable-update="true" users="users" user="user" group-options="groupOptions" institution-options="institutionOptions" cohort-options="cohortOptions" global-role-options="globalRoleOptions" editor-role-options="editorRoleOptions" cohort-promotions="cohortPromotions"></edit-user-details>',
                        title: user.email,
                        size: 'large',
                        scope: {
                            users: scope.users,
                            user,
                            groupOptions: scope.groupOptions,
                            globalRoleOptions: scope.globalRoleOptions,
                            editorRoleOptions: scope.editorRoleOptions,
                            institutionOptions: scope.institutionOptions,
                            cohortOptions: scope.cohortOptions,
                            cohortPromotions: scope.cohortPromotions,
                        },
                    });
                };

                //-------------------------
                // Styling
                //-------------------------

                scope.getRoleClasses = user => {
                    const classes = ['label'];
                    classes.push(`label-${user && user.roleName() === 'learner' ? 'info' : 'success'}`);
                    return classes;
                };
            },
        };
    },
]);
