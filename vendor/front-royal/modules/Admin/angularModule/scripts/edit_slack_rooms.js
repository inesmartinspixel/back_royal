import angularModule from 'Admin/angularModule/scripts/admin_module';
import template from 'Admin/angularModule/views/edit_slack_rooms.html';
import cacheAngularTemplate from 'cacheAngularTemplate';
import getOAuthUrl from 'Slack';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('editSlackRooms', [
    '$injector',

    function factory($injector) {
        const $window = $injector.get('$window');
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
        const $location = $injector.get('$location');
        const DialogModal = $injector.get('DialogModal');
        const ngToast = $injector.get('ngToast');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                schedulableItem: '<',
            },
            link(scope) {
                NavigationHelperMixin.onLink(scope);

                scope.$watchGroup(['schedulableItem', () => $location.search()], () => {
                    if (scope.schedulableItem) {
                        const searchParams = $location.search();

                        if (searchParams.slack_oauth_success) {
                            const slackRoom = scope.schedulableItem.slack_rooms.find(
                                _slackRoom => _slackRoom.id === searchParams.slack_oauth_success,
                            );
                            if (slackRoom) {
                                ngToast.create({
                                    content: `Successfully added Quantic Slack app to Slack room '${slackRoom.title}'`,
                                    className: 'success',
                                });
                            }
                            $location.search('slack_oauth_success', null);
                        } else if (searchParams.slack_oauth_error) {
                            DialogModal.alert({
                                title: 'Slack OAuth Error',
                                content: searchParams.slack_oauth_error,
                            });
                            $location.search('slack_oauth_error', null);
                        }
                    }
                });

                scope.addToSlack = slackRoom => {
                    // Do nothing if the slackRoom record hasn't been persisted to the database yet.
                    if (scope.addToSlackFeatureDisabled(slackRoom)) {
                        return;
                    }

                    if (
                        !$window.confirm(
                            'Adding the Quantic Slack app to the Slack room will navigate you away, discarding any unsaved changes. Are you sure you want to continue?',
                        )
                    ) {
                        return;
                    }

                    const url = getOAuthUrl(slackRoom);
                    scope.loadUrl(url);
                };

                scope.addToSlackFeatureDisabled = slackRoom => {
                    return !slackRoom.created_at;
                };
            },
        };
    },
]);
