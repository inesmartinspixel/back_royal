import angularModule from 'Admin/angularModule/scripts/admin_module';
import template from 'Admin/angularModule/views/admin_mba/admin_edit_group.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('adminEditGroup', [
    '$injector',

    function factory($injector) {
        const Group = $injector.get('Group');
        const AdminService = $injector.get('AdminEditService');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                thing: '<',
                goBack: '&',
                created: '&',
                destroyed: '&',
            },
            link(scope) {
                AdminService.onLink(scope, Group);
            },
        };
    },
]);
