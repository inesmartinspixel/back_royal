import angularModule from 'Admin/angularModule/scripts/admin_module';
import template from 'Admin/angularModule/views/admin_mba/admin_cohort_gradebook.html';
import projectScoringModalTemplate from 'Admin/angularModule/views/admin_mba/admin_cohort_gradebook_project_scoring_modal.html';
import cacheAngularTemplate from 'cacheAngularTemplate';
import customFieldsTemplate from 'Admin/angularModule/views/admin_mba/admin_cohort_gradebook_custom_fields.html';

const templateUrl = cacheAngularTemplate(angularModule, template);
const customFieldsTemplateUrl = cacheAngularTemplate(angularModule, customFieldsTemplate);
const projectScoringModalTemplateUrl = cacheAngularTemplate(angularModule, projectScoringModalTemplate);

angularModule.directive('adminCohortGradebook', [
    '$injector',

    function factory($injector) {
        const $window = $injector.get('$window');
        const AdminCohortStudentsTableHelper = $injector.get('AdminCohortStudentsTableHelper');
        const Stream = $injector.get('Lesson.Stream');
        const Playlist = $injector.get('Playlist');
        const editContentItemListMixin = $injector.get('editContentItemListMixin');
        const LearnerProject = $injector.get('LearnerProject');
        const ProjectProgress = $injector.get('ProjectProgress');
        const DialogModal = $injector.get('DialogModal');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                cohort: '<',
                listMeta: '<',
            },
            link(scope) {
                const clientPaginatePerPageOptions = [10, 25, 50, 100];
                const adminCohortStudentsTableHelper = new AdminCohortStudentsTableHelper(
                    scope.cohort,
                    `${$window.ENDPOINT_ROOT}/api/users/batch_update_grades.json`,
                    {
                        indexParams: {
                            select_cohort_user_progress_record: true,
                            select_project_progress: true,
                            select_exam_progress: true,
                        },
                        batchSaveAttributes: ['id', 'project_progress'],
                        saveAllInBatches: true,
                        saveAllBatchSize: 50,
                        clientPaginatePerPage: clientPaginatePerPageOptions[0],
                        clientPaginatePerPageOptions,
                    },
                );
                scope.adminCohortStudentsTableHelper = adminCohortStudentsTableHelper;

                let projectWeights;

                const projectProgressAttrs = ['scoreOptionValue', 'status', 'id_verified'];
                _.each(projectProgressAttrs, attr => {
                    // This works becuase we can never have a cohort with more than one project with the same requirement_identifier
                    // NOTE: We need to account for falsey values
                    adminCohortStudentsTableHelper.trackStudentChanges(student =>
                        _.chain(student.project_progress)
                            .map(progress => {
                                // score can be set to 0,
                                if (attr === 'scoreOptionValue' && progress[attr] === 0) {
                                    return progress[attr].toString() + progress.requirement_identifier;
                                }
                                // and id_veriried can se set to false,
                                if (attr === 'id_verified' && progress[attr] === false) {
                                    return progress[attr].toString() + progress.requirement_identifier;
                                }
                                // otherwise ensure it's truthy
                                return progress[attr] && progress[attr].toString() + progress.requirement_identifier;
                            })
                            .compact()
                            .value(),
                    );
                });

                // When the cohort is set, fetch playlists and exam streams
                // in order to set requiredExams and specializationExams
                scope.$watch('cohort', cohort => {
                    scope.requiredExams = null;
                    scope.specializationExams = null;
                    if (!cohort) {
                        return;
                    }
                    let playlists;
                    // Grab all the playlists for the cohort
                    Playlist.index({
                        filters: {
                            locale_pack_id: cohort.playlistPackIds,
                        },
                    })
                        .then(response => {
                            playlists = response.result;
                            const streamLocalePackIdsFromPlaylists = _.chain(playlists)
                                .pluck('streamLocalePackIds')
                                .flatten()
                                .uniq()
                                .value();

                            // Grab any streams that are
                            // 1. in a required playlist
                            // 2. required in a period
                            // 3. in a specialization playlist
                            return Stream.index({
                                filters: {
                                    exam: true,
                                    locale: 'en',
                                    locale_pack_id: _.union(
                                        streamLocalePackIdsFromPlaylists,
                                        _.keys(cohort.requiredStreamPackIdsCache),
                                    ),
                                },
                            });
                        })
                        .then(response => {
                            // group the exams into required and specialization, and
                            // put the in the desired order
                            const exams = _.indexBy(response.result, 'localePackId');
                            scope.requiredExams = _.chain(scope.cohort.requiredStreamPackIdsCache)
                                .keys()
                                .flatten()
                                .map(localePackId => exams[localePackId])
                                .compact()
                                .value();

                            scope.specializationExams = _.chain(scope.cohort.getSpecializationPlaylists(playlists))
                                .pluck('streamLocalePackIds')
                                .flatten()
                                .map(localePackId => exams[localePackId])
                                .compact()
                                .value();
                        });
                });

                scope.$watchGroup(['cohort', 'listMeta.learner_projects', 'students'], () => {
                    if (!scope.cohort || !scope.listMeta || !scope.listMeta.learner_projects) {
                        return;
                    }

                    scope.learnerProjects = _.map(scope.cohort.allLearnerProjectIds, learnerProjectId => {
                        const projectJson = _.findWhere(scope.listMeta.learner_projects, {
                            id: learnerProjectId,
                        });
                        if (!projectJson) {
                            throw new Error('No project found');
                        }
                        return LearnerProject.new(projectJson);
                    });
                    scope.presentationProjects = _.filter(
                        scope.learnerProjects,
                        project => project.isPresentationProject,
                    );
                });

                function ensureProjectProgress(user, projectOrRequirementIdentifier) {
                    const requirementIdentifier =
                        typeof projectOrRequirementIdentifier === 'string'
                            ? projectOrRequirementIdentifier
                            : projectOrRequirementIdentifier.requirement_identifier;
                    let record = _.findWhere(user.$$proxy.project_progress, {
                        requirement_identifier: requirementIdentifier,
                    });
                    if (!record) {
                        record = ProjectProgress.new({
                            requirement_identifier: requirementIdentifier,
                        });
                        user.$$proxy.project_progress.push(record);
                    }
                    return record;
                }

                let projectScoreCache;
                let totalProjectWeight;

                // duplicated in FinalScoreHelper#get_project_score
                // See comment there about what the rules are here for
                // calculating a score\
                function getProjectScoreInfo(user) {
                    let scoresKey = _.pluck(user.$$proxy.project_progress, 'scoreOptionValue').join('-');
                    scoresKey = `${scoresKey}-${user.project_score_override}`;

                    // If the scores for this user have not changed since the last time
                    // we figured all this out, return the cached value.
                    let cachedValue = projectScoreCache[user.id];
                    if (cachedValue && cachedValue.scoresKey === scoresKey) {
                        return cachedValue;
                    }

                    let totalScore = 0;
                    totalProjectWeight = _.reduce(
                        projectWeights,
                        (memo, weight, requirementIdentifier) => {
                            const projectProgress = ensureProjectProgress(user, requirementIdentifier);
                            if (projectProgress.waived) {
                                return memo;
                            }
                            return memo + weight;
                        },
                        0,
                    );

                    let hasScoreSet = false;
                    const scores = _.chain(scope.learnerProjects)
                        .map(project => {
                            const projectProgress = ensureProjectProgress(user, project);
                            if (projectProgress.hasScoreSet) {
                                hasScoreSet = true;
                            }
                            if (projectProgress.waived) {
                                return null;
                            }
                            const weight = project.getScoringWeight(scope.cohort.program_type) / totalProjectWeight;
                            let score = projectProgress.score || 0;
                            const passingScore = project.getPassingScore(scope.cohort);
                            if (projectProgress.marked_as_passed) {
                                score = passingScore;
                            }
                            const passed = score >= passingScore;
                            const weightedScore = weight * (passed ? 1 : 0);
                            totalScore += weightedScore;
                            return Math.round(100 * weightedScore) / 100;
                        })
                        .without(null)
                        .value();
                    const roundedTotalScore = Math.round(100 * totalScore) / 100;

                    // We want to warn admins if they are replacing the score
                    // in user_project_records by setting a project_progress score
                    const hasOverride = typeof user.project_score_override === 'number';
                    let showWarning = hasScoreSet && hasOverride;

                    let replacedScore;
                    if (!hasScoreSet && hasOverride) {
                        totalScore = user.project_score_override;
                    } else if (hasScoreSet && hasOverride) {
                        totalScore = roundedTotalScore;
                        showWarning = true;
                        replacedScore = user.project_score_override;
                    } else {
                        totalScore = roundedTotalScore;
                    }

                    let rolloverText = `${scores.join(' + ')} = ${roundedTotalScore}`;
                    if (showWarning) {
                        rolloverText = `${rolloverText}   (By setting a score for this user, you will replace the old way of setting project scores.)`;
                    } else if (hasOverride) {
                        rolloverText =
                            'Cumulative project score was set directly in the database using the old method of setting scores.';
                    }

                    cachedValue = { totalScore, replacedScore, rolloverText, scoresKey, showWarning };
                    projectScoreCache[user.id] = cachedValue;
                    return cachedValue;
                }

                function updateColumns() {
                    if (!scope.cohort || !scope.requiredExams || !scope.specializationExams) {
                        return;
                    }
                    projectScoreCache = {};

                    scope.columns = [
                        adminCohortStudentsTableHelper.getNameColumn(),
                        adminCohortStudentsTableHelper.getEmailColumn(),
                        {
                            id: 'graduation_status',
                            prop: 'graduationStatus',
                            type: 'text',
                            label: 'Graduated',
                        },
                        {
                            id: 'final_score',
                            prop: 'acceptedCohortApplication.final_score',
                            type: 'perc1',
                            label: 'Final Score',
                            sortEmptyAs0: true,
                        },
                    ];

                    projectWeights = {};
                    _.each(scope.learnerProjects, project => {
                        projectWeights[project.requirement_identifier] = project.getScoringWeight(
                            scope.cohort.program_type,
                        );

                        let projectScoreOptions = [
                            {
                                value: null,
                                label: '',
                            },
                        ];
                        for (let i = 0; i <= 5; i++) {
                            projectScoreOptions.push({
                                value: i,
                                label: i + (project.getPassingScore(scope.cohort) <= i ? '*' : ''),
                            });
                        }
                        projectScoreOptions = projectScoreOptions.concat([
                            {
                                value: 'waived',
                                label: 'N/A',
                            },
                            {
                                value: 'marked_as_passed',
                                label: 'Passed',
                            },
                        ]);

                        const projectStatusOptions = [
                            {
                                value: undefined,
                                label: '',
                            },
                        ];
                        project.validStatusesForProgressRecords.forEach(status => {
                            projectStatusOptions.push({ value: status, label: status.replace(/_/g, ' ') });
                        });

                        const idVerifiedOptions = [
                            {
                                value: undefined,
                                label: '',
                            },
                            {
                                value: true,
                                label: 'ID verified',
                            },
                            {
                                value: false,
                                label: 'ID NOT verified',
                            },
                        ];

                        scope.columns.push({
                            id: `project-${project.id}`,
                            label: project.title,
                            type: 'custom',
                            project,
                            templateUrl: customFieldsTemplateUrl,
                            classes: 'project',
                            projectScoreOptions, // score column
                            projectStatusOptions, // status column
                            idVerifiedOptions, // id_verifed column
                            isPresentationProject: project.isPresentationProject,
                            prop: user => ensureProjectProgress(user, project).score,
                            ensureProjectProgress: user => ensureProjectProgress(user, project),
                            doNotExport: true,
                        });

                        // Add each sub-column as its own column in the CSV export
                        const projectProgressAttrsToExport = project.isPresentationProject
                            ? projectProgressAttrs
                            : _.without(projectProgressAttrs, 'id_verified');
                        _.each(projectProgressAttrsToExport, attr => {
                            const attrLabel = attr === 'scoreOptionValue' ? 'score' : attr.replace(/_/g, ' ');
                            scope.columns = _.union(scope.columns, [
                                {
                                    id: `project-${project.id}-${attr}`,
                                    prop: user => ensureProjectProgress(user, project)[attr],
                                    type: 'text',
                                    label: `${project.title} - ${attrLabel}`,
                                    csvOnly: true,
                                },
                            ]);
                        });
                    });

                    if (_.any(scope.cohort.allLearnerProjectIds)) {
                        scope.columns = _.union(scope.columns, [
                            {
                                id: 'project_score',
                                prop: user => getProjectScoreInfo(user).totalScore,
                                type: 'custom',
                                templateUrl: customFieldsTemplateUrl,
                                label: 'Project Score',
                                sortEmptyAs0: true,

                                // content that displays when rolling over a cell in the table
                                getRolloverText(user) {
                                    return getProjectScoreInfo(user).rolloverText;
                                },

                                getProjectScore(user) {
                                    return getProjectScoreInfo(user).totalScore;
                                },

                                getReplacedScore(user) {
                                    return getProjectScoreInfo(user).replacedScore;
                                },

                                showWarning(user) {
                                    return getProjectScoreInfo(user).showWarning;
                                },

                                // content that pops up when clicking on the tooltip in the head
                                tooltip: {
                                    anchorText: '?',
                                    click() {
                                        DialogModal.alert({
                                            title: 'Project Scoring',
                                            content: `<ng-include src="'${projectScoringModalTemplateUrl}'"></ng-include>`,
                                        });
                                    },
                                },
                            },
                        ]);
                    }

                    if (
                        scope.cohort.supportsPresentationProjecs &&
                        scope.presentationProjects &&
                        scope.presentationProjects.length >= 1
                    ) {
                        scope.columns = _.union(scope.columns, [
                            {
                                id: 'presentation_projects_complete',
                                prop(user) {
                                    let complete = false;
                                    if (user.$$proxy) {
                                        complete =
                                            _.map(scope.presentationProjects, project =>
                                                ensureProjectProgress(
                                                    user,
                                                    project.requirement_identifier,
                                                ).unpassedForProject(project, scope.cohort),
                                            ).filter(value => !!value).length === 0;
                                    }
                                    return complete;
                                },
                                type: 'checkIfTrue',
                                label: 'Presentation Projects Complete',
                            },
                        ]);
                    }

                    if (scope.cohort.supportsSpecializations) {
                        scope.columns = _.union(scope.columns, [
                            {
                                id: 'specialization_playlists_complete',
                                prop: 'cohort_user_progress_record.specialization_playlists_complete',
                                type: 'text',
                                label: 'Specializations Complete',
                            },
                        ]);
                    }

                    // Old cohorts used to have participation scores.
                    if (scope.cohort.startDate < new Date('2017/09/01')) {
                        scope.columns = _.union(scope.columns, [
                            {
                                id: 'participation_score',
                                prop: 'cohort_user_progress_record.participation_score',
                                type: 'perc1',
                                label: 'Participation Score',
                                sortEmptyAs0: true,
                            },
                        ]);
                    }

                    scope.columns = _.union(scope.columns, [
                        {
                            id: 'average_assessment_score_best',
                            prop: 'cohort_user_progress_record.average_assessment_score_best',
                            type: 'perc1',
                            label: 'Smartcase',
                            sortEmptyAs0: true,
                        },
                        {
                            id: 'avg_test_score',
                            prop: 'cohort_user_progress_record.avg_test_score',
                            type: 'perc1',
                            label: 'Exam',
                            sortEmptyAs0: true,
                        },
                    ]);

                    // Add a column for each exam
                    _.chain(scope.requiredExams)
                        .union(scope.specializationExams)
                        .each(exam => {
                            const scoresCache = {};
                            scope.columns.push({
                                id: `exam-${exam.localePackId}`,
                                prop(user) {
                                    if (!scoresCache[user.id]) {
                                        const streamProgress = _.findWhere(user.exam_progress, {
                                            locale_pack_id: exam.localePackId,
                                        });
                                        scoresCache[user.id] = streamProgress && streamProgress.official_test_score;
                                    }
                                    return scoresCache[user.id];
                                },
                                type: 'perc1',
                                label: exam.title,
                                sortEmptyAs0: true,
                            });
                        });

                    scope.columns = _.union(scope.columns, [
                        {
                            id: 'meets_graduation_requirements',
                            prop: 'acceptedCohortApplication.meets_graduation_requirements',
                            type: 'checkIfTrue',
                            label: 'Meets Grad Requirements',
                            csvOnly: true,
                        },
                        {
                            id: 'all_required_streams_complete',
                            prop(user) {
                                return user.cohort_user_progress_record?.required_streams_perc_complete === 1;
                            },
                            type: 'checkIfTrue',
                            label: 'Required Courses Complete',
                            csvOnly: true,
                        },
                    ]);

                    if (scope.cohort.supportsSpecializations) {
                        scope.columns = _.union(scope.columns, [
                            {
                                id: 'all_required_specializations_complete',
                                prop(user) {
                                    return (
                                        user.cohort_user_progress_record?.specialization_playlists_complete >=
                                        scope.cohort.num_required_specializations
                                    );
                                },
                                type: 'checkIfTrue',
                                label: 'Required Spec. Complete',
                                csvOnly: true,
                            },
                        ]);
                    }

                    scope.columns = _.union(scope.columns, [
                        {
                            id: 'required_lessons_complete',
                            prop: 'cohort_user_progress_record.required_lessons_complete',
                            type: 'text',
                            label: 'Required Lessons Complete',
                            csvOnly: true,
                        },
                        {
                            id: 'test_lessons_complete',
                            prop: 'cohort_user_progress_record.test_lessons_complete',
                            type: 'text',
                            label: 'Test Lessons Complete',
                            csvOnly: true,
                        },
                        {
                            id: 'elective_lessons_complete',
                            prop: 'cohort_user_progress_record.elective_lessons_complete',
                            type: 'text',
                            label: 'Elective Lessons Complete',
                            csvOnly: true,
                        },
                        {
                            id: 'average_assessment_score_first',
                            prop: 'cohort_user_progress_record.average_assessment_score_first',
                            type: 'perc1',
                            label: 'Smartcase (First scores)',
                            csvOnly: true,
                        },
                    ]);

                    scope.columns.push(adminCohortStudentsTableHelper.getSaveColumn());

                    scope.csvExportColumns = _.reject(
                        scope.columns,
                        column => column.id === 'editorAbilities' || !!column.doNotExport,
                    );
                }

                scope.$watchGroup(['cohort', 'requiredExams', 'specializationExams'], updateColumns);

                //-------------------------
                // Filters
                //-------------------------

                scope.quickFilterProperties = ['name', 'email'];

                scope.meetsGraduationRequirementsOptions = getBooleanFilterOptions(
                    'Meets Graduation Requirements',
                    'Missing Graduation Requirements',
                );
                scope.allRequiredStreamsCompleteOptions = getBooleanFilterOptions(
                    'Required Courses Complete',
                    'Missing Required Courses',
                );
                scope.allRequiredSpecializationsCompleteOptions = getBooleanFilterOptions(
                    'Required Specializations Complete',
                    'Missing Required Specializations',
                );

                function getBooleanFilterOptions(trueLabel, falseLabel) {
                    return [
                        {
                            label: trueLabel,
                            value: 'true',
                        },
                        {
                            label: falseLabel,
                            value: 'false',
                        },
                    ];
                }

                const booleanProperties = [
                    scope.meetsGraduationRequirementsOptions,
                    scope.allRequiredStreamsCompleteOptions,
                    scope.allRequiredSpecializationsCompleteOptions,
                ];

                // default filters
                const defaultFilters = _.map(
                    adminCohortStudentsTableHelper.indexParams.filters,
                    (filterValue, filterKey) => {
                        const filter = {
                            server: true,
                            default: true,
                            value: {},
                        };
                        filter.value[filterKey] = filterValue;
                        return filter;
                    },
                );

                // wire up filtering support
                editContentItemListMixin.onLink(scope, 'adminCohortGradebook', defaultFilters, booleanProperties);

                // Since filters are stored in client storage, it is possible that when we first
                // come to this page, an unsupported filter will be set.  For example, you could be
                // looking at an EMBA cohort and set the allRequiredSpecializationsCompleteOptions
                // filter to false.  Then, if you looked at an MBA page, all the records would be hidden,
                // since the cohort has no specializations.  So, we need to remove irrelevant filters.
                // It is ok for the watch to just rely on an watch on filters
                // since we can assume that there is no way to add an unsupported filter in the UI.  So,
                // we only need this to fire once when the filters object is first initialized from ClientStorage.
                function removeUnsupportedFilters() {
                    if (scope.clientFilters && !scope.cohort.supportsSpecializations) {
                        delete scope.clientFilters.all_required_specializations_complete;
                    }
                }
                scope.$watch('cohort.supportsSpecializations', removeUnsupportedFilters);
                scope.$watchCollection('clientFilters', removeUnsupportedFilters);
            },
        };
    },
]);
