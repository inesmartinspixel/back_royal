import angularModule from 'Admin/angularModule/scripts/admin_module';
import moment from 'moment-timezone';
import template from 'Admin/angularModule/views/admin_mba/admin_view_calendar.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('adminViewCalendar', [
    '$injector',

    function factory($injector) {
        const $rootScope = $injector.get('$rootScope');
        const Cohort = $injector.get('Cohort');
        const colorHelper = $injector.get('colorHelper');
        const ClientStorage = $injector.get('ClientStorage');
        const dateHelper = $injector.get('dateHelper');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                calendarService: '<?',
            },
            link(scope) {
                const colors = [
                    $injector.get('COLOR_V3_CORAL'),
                    $injector.get('COLOR_V3_ORANGE'),
                    $injector.get('COLOR_V3_YELLOW'),
                    $injector.get('COLOR_V3_GREEN'),
                    $injector.get('COLOR_V3_TURQUOISE'),
                    $injector.get('COLOR_V3_BLUE'),
                    $injector.get('COLOR_V3_PURPLE'),
                ];

                scope.calendarService = scope.calendarService || {
                    initializeCalendar(destroy) {
                        // Destroy the old calendar if recreating. See http://stackoverflow.com/a/26900059/1747491
                        if (destroy) {
                            $('#overall-calendar').fullCalendar('destroy');
                        }

                        // Initialize variables
                        let cohorts = _.sortBy(scope.cohorts, 'startDate');
                        const filters = scope.filters;

                        // For displaying events in the calendar we will cycle through a list of colors for each program_type, using a
                        // desaturated version for EMBA. Add the color as a transient property before applying filters so they don't change
                        // as filters are applied.
                        _.each(Cohort.programTypes, programType => {
                            if (programType.supportsAdmissionRounds) {
                                const cohortsOfThisType = _.where(cohorts, {
                                    program_type: programType.key,
                                });

                                _.each(cohortsOfThisType, (cohort, i) => {
                                    // The +4 offset comes from the fact that we want to keep related MBA and EMBA programs aligned color-wise,
                                    // with the first pair being MBA5 / EMBA1, then MBA6 / EMBA2, and so on.
                                    const rotatingColor = cohort.isEMBA
                                        ? colors[(i + 4) % colors.length]
                                        : colors[i % colors.length];
                                    const color = cohort.isEMBA
                                        ? colorHelper.desaturateColor(40, rotatingColor)
                                        : rotatingColor;
                                    cohort.$$color = color;
                                });
                            }
                        });

                        // Filter on programType if applicable
                        if (filters.programType) {
                            cohorts = _.filter(cohorts, cohort => cohort.program_type === filters.programType);
                        }

                        // Filter on cohortName if applicable
                        if (filters.cohortName) {
                            cohorts = _.filter(cohorts, cohort => cohort.name === filters.cohortName);
                        }

                        // Each cohort will be a source, seems easiest for how the fullcalendar API works. Let's iterate through every cohort
                        // that is left after filters are applied and create the appropriate event sources for them.
                        const eventSources = _.map(cohorts, cohort => ({
                            events(start, end, timezone, callback) {
                                const events = [];

                                // Start Date
                                if (filters.startDates) {
                                    events.push({
                                        title: `${cohort.name} starts`,
                                        start: cohort.startDate,
                                        end: cohort.startDate,
                                    });
                                }

                                _.each(['exam', 'review', 'break', 'specialization'], style => {
                                    const prop = `${style}Periods`;
                                    if (filters[prop]) {
                                        _.each(cohort[prop], (period, i) => {
                                            events.push({
                                                title: `${cohort.name} ${style} ${i + 1}`,
                                                start: period.startDate,
                                                end: period.endDate,
                                            });
                                        });
                                    }
                                });

                                if (filters.projectPeriods) {
                                    _.each(cohort.projectPeriods, period => {
                                        events.push({
                                            title: `${cohort.name} ${period.project_style} project`,
                                            start: period.startDate,
                                            end: period.endDate,
                                        });
                                    });
                                }

                                // Create admission round events. Note that this is tricky because it sometimes needs to look at other cohorts
                                // since a cohort's first admission round starts a day after the last admission round of the previous
                                // cohort of the same type ends.
                                // See cohort.js#getPreviousAdmissionRound
                                if (filters.admissionRounds) {
                                    _.each(cohort.admission_rounds, (admissionRound, i) => {
                                        const previousEndDate = Cohort.getPreviousAdmissionRound(
                                            admissionRound,
                                            cohort,
                                            scope.cohorts,
                                        );
                                        if (previousEndDate) {
                                            let end = admissionRound.applicationDeadline;
                                            if (!filters.admissionDeadlines) {
                                                end = dateHelper.addInDefaultTimeZone(end, 1, 'days');
                                            }

                                            const oneDayAfterPreviousEndDate = dateHelper.addInDefaultTimeZone(
                                                previousEndDate,
                                                1,
                                                'days',
                                            );
                                            events.push({
                                                title: `${cohort.name} admission round ${i + 1}`,
                                                start: dateHelper.shiftMonthDayForThreshold(
                                                    moment(oneDayAfterPreviousEndDate),
                                                ),

                                                // So you may wonder how this event ends a day earlier than the application deadline
                                                // when the end dates are the same. Well, that is a fantastic question! The answer is that
                                                // here we are both subtracting a day and the fullcalendar itself has a threshold that substracts a day
                                                // (see the nextDayThreshold option). This gives us
                                                // what we actually want in that the round span and the deadline don't overlap (since that would be redundant),
                                                // whereas below the fullcalendar can't do a threshold shift because the start and end dates are at the
                                                // same time.
                                                end: dateHelper.shiftMonthDayForThreshold(end),
                                            });
                                        }
                                    });
                                }

                                if (filters.admissionDeadlines) {
                                    _.each(cohort.admission_rounds, (admissionRound, i) => {
                                        events.push({
                                            title: `${cohort.name} application deadline ${i + 1}`,
                                            start: dateHelper.shiftMonthDayForThreshold(
                                                admissionRound.applicationDeadline,
                                            ),
                                            end: dateHelper.shiftMonthDayForThreshold(
                                                admissionRound.applicationDeadline,
                                            ),
                                        });
                                    });
                                }

                                if (filters.admissionDecisions) {
                                    _.each(cohort.admission_rounds, (admissionRound, i) => {
                                        events.push({
                                            title: `${cohort.name} decision date ${i + 1}`,
                                            start: dateHelper.shiftMonthDayForThreshold(admissionRound.decisionDate),
                                            end: dateHelper.shiftMonthDayForThreshold(admissionRound.decisionDate),
                                        });
                                    });
                                }

                                if (filters.enrollmentDeadline) {
                                    if (cohort.supportsEnrollmentDeadline) {
                                        events.push({
                                            title: `${cohort.name} enrollment deadline`,
                                            start: cohort.enrollmentDeadline,
                                            end: cohort.enrollmentDeadline,
                                        });
                                    }
                                }

                                // Registration Periods
                                if (
                                    filters.registrationPeriod &&
                                    cohort.registrationDeadline &&
                                    cohort.lastDecisionDate
                                ) {
                                    events.push({
                                        title: `${cohort.name} registration period`,
                                        start: dateHelper.shiftMonthDayForThreshold(cohort.lastDecisionDate),
                                        end: cohort.registrationDeadline,
                                    });
                                }

                                // Graduation Dates
                                if (filters.graduationDates) {
                                    events.push({
                                        title: `${cohort.name} graduates`,
                                        start: cohort.graduationDate,
                                        end: cohort.graduationDate,
                                    });
                                }

                                // Diploma Generation Dates
                                if (filters.diplomaGenerationDates) {
                                    events.push({
                                        title: `${cohort.name} diplomas`,
                                        start: cohort.diplomaGeneration,
                                        end: cohort.diplomaGeneration,
                                    });
                                }

                                callback(events);
                            },

                            color: cohort.$$color,
                            textColor: '#FFFFFF',
                        }));

                        // There is an angular wrapper for fullcalendar from the angular-ui folks, but it doesn't seem to be
                        // up-to-date unfortunately. See http://angular-ui.github.io/ui-calendar/
                        $('#overall-calendar').fullCalendar({
                            timezone: scope.dateHelper.ADMIN_REF_TIMEZONE,
                            displayEventTime: false,
                            height: 720,
                            // displayEventEnd: false,
                            // nextDayThreshold: '00:00:00', // the default is 09:00 AM
                            eventSources,
                        });
                    },
                    gotoDate(date) {
                        $('#overall-calendar').fullCalendar('gotoDate', date);
                    },
                    refetchEvents() {
                        $('#overall-calendar').fullCalendar('refetchEvents');
                    },
                    setOption(key, val) {
                        $('#overall-calendar').fullCalendar(key, val);
                    },
                };

                // Setup filters
                const filtersKey = 'adminViewCalendarFilters';
                const defaultFilters = {
                    programType: undefined,
                    cohortName: undefined,
                    startDates: true,
                    examPeriods: true,
                    reviewPeriods: true,
                    breakPeriods: true,
                    admissionRounds: true,
                    enrollmentDeadline: true,
                };
                const existingFiltersString = ClientStorage.getItem(filtersKey);

                if (existingFiltersString) {
                    scope.filters = JSON.parse(existingFiltersString);
                } else {
                    scope.filters = defaultFilters;
                }

                scope.dateHelper = $injector.get('dateHelper');
                scope.programTypeOptions = Cohort.calendarProgramTypes;

                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                Cohort.index().then(response => {
                    scope.cohorts = _.sortBy(response.result, 'startDate');

                    // Always filter on supportsAdmissionRounds
                    scope.cohorts = _.where(scope.cohorts, {
                        supportsAdmissionRounds: true,
                    });

                    // Build out the filter options
                    buildCohortNameOptions();
                });

                scope.calendarService.initializeCalendar();

                scope.$watch('cohorts', () => {
                    scope.calendarService.initializeCalendar(true);
                });

                scope.$watchCollection('filters', () => {
                    scope.calendarService.initializeCalendar(true);
                });

                scope.$watch('filters.programType', () => {
                    buildCohortNameOptions();
                });

                scope.$watchCollection('filters', () => {
                    ClientStorage.setItem(filtersKey, JSON.stringify(scope.filters));
                });

                scope.resetFilters = () => {
                    Object.assign(scope.filters, defaultFilters);
                };

                function buildCohortNameOptions() {
                    if (scope.filters.programType) {
                        scope.cohortNameOptions = _.map(
                            _.where(scope.cohorts, {
                                program_type: scope.filters.programType,
                            }),
                            cohort => ({
                                key: cohort.name,
                                label: cohort.name,
                            }),
                        );
                    } else {
                        scope.cohortNameOptions = _.map(scope.cohorts, cohort => ({
                            key: cohort.name,
                            label: cohort.name,
                        }));
                    }
                }
            },
        };
    },
]);
