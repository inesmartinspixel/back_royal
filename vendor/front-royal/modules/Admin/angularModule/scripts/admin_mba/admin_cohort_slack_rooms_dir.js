import angularModule from 'Admin/angularModule/scripts/admin_module';
import template from 'Admin/angularModule/views/admin_mba/admin_cohort_slack_rooms.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('adminCohortSlackRooms', [
    '$injector',

    function factory() {
        return {
            restrict: 'E',
            templateUrl,
            scope: {
                cohort: '<',
                students: '<',
            },
            link(scope) {
                function initializeRow(title) {
                    return {
                        title: [title],
                        counts: {},
                    };
                }

                scope.reset = () => {
                    const students = scope.students;
                    const cohort = scope.cohort;
                    const rows = {};

                    scope.results = [
                        {
                            title: 'Gender',
                            rows: [
                                (rows.male = initializeRow('Male')),
                                (rows.female = initializeRow('Female')),
                                (rows.unknownGender = initializeRow('Unknown')),
                            ],
                        },
                    ];

                    if (cohort.supportsScholarshipLevels) {
                        scope.results.push({
                            title: 'Scholarship',
                            rows: [
                                (rows.fullScholarship = initializeRow('Full Scholarship')),
                                (rows.noFullScholarship = initializeRow('Other')),
                            ],
                        });
                    }

                    const locationResult = {
                        title: 'Locations',
                        rows: [],
                    };
                    scope.results.push(locationResult);

                    const locationRows = {};

                    // there might not be a slack_room_assignment at the moment.  If that's
                    // the case, we can't show location info
                    _.chain(cohort.slack_room_assignment && cohort.slack_room_assignment.location_assignments)
                        .values()
                        .uniq()
                        .each(locationPair => {
                            const location = locationPair.join(', '); // country, city
                            if (!locationRows[location]) {
                                const row = {
                                    title: locationPair,
                                    counts: {},
                                };
                                locationResult.rows.push(row);
                                locationRows[location] = row;
                            }
                        });

                    _.chain(scope.results)
                        .pluck('rows')
                        .flatten()
                        .each(row => {
                            cohort.slack_rooms.forEach(slackRoom => {
                                row.counts[slackRoom.id] = 0;
                            });
                        });

                    _.each(students, student => {
                        if (!student.cohort_slack_room_id) {
                            return;
                        }

                        if (student.sex === 'male') {
                            rows.male.counts[student.cohort_slack_room_id]++;
                        } else if (student.sex === 'female') {
                            rows.female.counts[student.cohort_slack_room_id]++;
                        } else {
                            rows.unknownGender.counts[student.cohort_slack_room_id]++;
                        }

                        if (rows.fullScholarship) {
                            if (student.has_full_scholarship) {
                                rows.fullScholarship.counts[student.cohort_slack_room_id]++;
                            } else {
                                rows.noFullScholarship.counts[student.cohort_slack_room_id]++;
                            }
                        }

                        if (cohort.slack_room_assignment) {
                            const location = cohort.slack_room_assignment.location_assignments[student.user_id].join(
                                ', ',
                            );
                            locationRows[location].counts[student.cohort_slack_room_id]++;
                        }
                    });

                    // Sort locations alphabetically
                    locationResult.rows = _.sortBy(locationResult.rows, row => {
                        // move 'Other Countries' to the very end, and within
                        // each country, move '(other cities)' to the end
                        if (row.title[0] === 'Other Countries') {
                            return 'ZZZZZZZZZZZZZZ';
                        }
                        return row.title.join().replace('(', 'ZZZ');
                    });
                };

                scope.$watchGroup(['students', 'cohort'], scope.reset);
                scope.$on('slackRoomStatsRequireReset', () => {
                    scope.reset();
                });
            },
        };
    },
]);
