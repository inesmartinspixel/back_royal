import angularModule from 'Admin/angularModule/scripts/admin_module';
import template from 'Admin/angularModule/views/admin_mba/import_curriculum_template.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('importCurriculumTemplate', [
    '$injector',

    function factory($injector) {
        const CurriculumTemplate = $injector.get('CurriculumTemplate');
        const $window = $injector.get('$window');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                schedulableItem: '<',
            },
            link(scope) {
                // FIXME: Would be nice to hook this into a cache like careerProfileCache
                CurriculumTemplate.index().then(response => {
                    scope.curriculumTemplates = response.result;
                    scope.curriculumTemplateOptions = scope.curriculumTemplates.map(curriculumTemplate => ({
                        value: curriculumTemplate,
                        label: curriculumTemplate.name,
                    }));
                });

                scope.importCurriculumTemplate = curriculumTemplate => {
                    if (
                        curriculumTemplate &&
                        $window.confirm(
                            `Are you sure you want to replace the current schedule with the ${curriculumTemplate.name} template?`,
                        )
                    ) {
                        // program_type has to be first, since it defines what else you can set
                        scope.schedulableItem.program_type = curriculumTemplate.program_type;

                        const curriculumTemplateAttrs = curriculumTemplate.asJson();
                        const attrs = _.pick(curriculumTemplateAttrs, [
                            'program_type',
                            'description',
                            'groups',
                            'periods',
                            'specialization_playlist_pack_ids',
                            'num_required_specializations',
                            'registration_deadline_days_offset',
                            'early_registration_deadline_days_offset',
                            'enrollment_deadline_days_offset',
                            'graduation_days_offset_from_end',
                            'admission_rounds',
                            'id_verification_periods',
                            'stripe_plans',
                            'scholarship_levels',
                            'project_submission_email',
                            'learner_project_ids',
                            'diploma_generation_days_offset_from_end',
                            'enrollment_agreement_template_id',
                            'playlist_collections',
                        ]);

                        scope.schedulableItem.copyAttrs(attrs);
                    }
                };
            },
        };
    },
]);
