import angularModule from 'Admin/angularModule/scripts/admin_module';
import template from 'Admin/angularModule/views/admin_mba/admin_cohort_slack_assignments.html';
import studentSelectSlackRoomTemplate from 'Admin/angularModule/views/admin_mba/student_select_slack_room.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);
const studentSelectSlackRoomTemplateUrl = cacheAngularTemplate(angularModule, studentSelectSlackRoomTemplate);

angularModule.directive('adminCohortSlackAssignments', [
    '$injector',

    function factory($injector) {
        const $http = $injector.get('$http');
        const $window = $injector.get('$window');
        const ngToast = $injector.get('ngToast');
        const DialogModal = $injector.get('DialogModal');
        const scrollHelper = $injector.get('scrollHelper');
        const $q = $injector.get('$q');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                cohort: '<',
            },
            link(scope) {
                const columns = [
                    {
                        prop: 'name',
                        type: 'text',
                        label: 'Name',
                    },
                    {
                        prop: 'cohort_status',
                        type: 'text',
                        label: 'Status',
                    },
                    {
                        prop: 'sex',
                        type: 'text',
                        label: 'M/F',
                    },
                    {
                        prop: 'has_full_scholarship',
                        type: 'checkIfTrue',
                        label: 'Full Schol.',
                    },
                    {
                        prop: 'locationString',
                        type: 'text',
                        label: 'Location',
                    },
                    {
                        prop: 'cohort_slack_room_id',
                        type: 'custom',
                        label: 'Slack Room',
                        templateUrl: studentSelectSlackRoomTemplateUrl,
                        slackRoomTitle(student) {
                            const slackRoom = _.findWhere(this.slackRooms, {
                                id: student.cohort_slack_room_id,
                            });
                            return slackRoom && slackRoom.title;
                        },
                        onChange() {
                            setHasChanges();
                        },
                        canUpdateStudent(student) {
                            // Once a student is accepted and has a slack room, that room cannot be changed.
                            // It is not expected for an accepted student to have no slack room, but
                            // it used to be possible.  See https://trello.com/c/3Jb6hoS3
                            return student.cohort_status !== 'accepted' || !student.cohort_slack_room_id;
                        },
                    },
                ];

                scope.proxy = {};

                scope.$watchGroup(['cohort.slack_rooms', 'columns'], () => {
                    if (!scope.cohort || !scope.columns) {
                        return;
                    }

                    const slackRooms = scope.cohort.slack_rooms;
                    const column = _.findWhere(scope.columns, {
                        prop: 'cohort_slack_room_id',
                    });
                    column.slackRooms = slackRooms;
                });

                scope.$watch('cohort.id', () => {
                    setIndexParams();

                    if (!scope.cohort) {
                        return;
                    }

                    // remove the full scholarship column if this cohort does
                    // not support scholarships
                    if (scope.cohort && scope.cohort.supportsScholarshipLevels) {
                        scope.columns = columns;
                    } else {
                        scope.columns = _.reject(columns, column => column.prop === 'has_full_scholarship');
                    }
                });

                let originalAssignments;
                scope.$watch('proxy.students', setOriginalAssignments);

                function setOriginalAssignments() {
                    const students = scope.proxy.students;
                    originalAssignments = {};
                    _.each(students, student => {
                        originalAssignments[student.last_application_id] = student.cohort_slack_room_id;
                    });
                    setHasChanges();
                }

                function setHasChanges() {
                    scope.hasChanges = false;
                    if (!originalAssignments) {
                        return;
                    }

                    _.each(scope.proxy.students, student => {
                        if (originalAssignments[student.last_application_id] !== student.cohort_slack_room_id) {
                            scope.hasChanges = true;
                        }
                    });
                }

                scope.save = () => {
                    scope.saving = true;

                    const config = {
                        // In general, batch update requests can take a long time, and we don't want retry to avoid
                        // them piling up and causing DB contention.
                        //
                        // Because this isn't an iguana request, we don't need to disable timeouts. If it had been,
                        // we would need to include the following argument here:
                        // timeout: undefined,
                        httpQueueOptions: {
                            disable_retry: true,
                        },
                    };

                    // We filter out unchanged students for performance reasons
                    const records = _.chain(scope.proxy.students)
                        .select(
                            student =>
                                student.cohort_slack_room_id !== originalAssignments[student.last_application_id],
                        )
                        .map(student => ({
                            id: student.last_application_id,
                            cohort_slack_room_id: student.cohort_slack_room_id,
                        }))
                        .value();

                    const batchChunks = _.chain(records).chunk(50).value();
                    let promiseChain = $q.when();

                    // eslint-disable-next-line no-restricted-syntax
                    for (const batchChunk of batchChunks) {
                        // eslint-disable-next-line no-loop-func
                        promiseChain = promiseChain.then(() => {
                            return $http.put(
                                `${$window.ENDPOINT_ROOT}/api/cohort_applications/batch_update.json`,
                                {
                                    records: batchChunk,
                                },
                                config,
                            );
                        });
                    }

                    promiseChain.finally(() => {
                        scope.saving = false;
                        ngToast.create({
                            content: 'Students saved',
                            className: 'success',
                        });
                        setOriginalAssignments();
                    });

                    return promiseChain;
                };

                scope.autoAssignSlackRooms = () => {
                    if (!scope.cohort.slack_room_assignment) {
                        DialogModal.alert({
                            content:
                                'Slack room auto-assignment has not yet been created.  Try refreshing.  If that does not work, try again in a few minutes.',
                        });
                        return;
                    }

                    const studentByApplicationId = _.indexBy(scope.proxy.students, 'last_application_id');
                    _.detect(scope.cohort.slack_room_assignment.slack_room_assignments, entry => {
                        const slackRoomId = entry.slack_room_id;
                        _.each(entry.cohort_application_ids, cohortApplicationId => {
                            const student = studentByApplicationId[cohortApplicationId];
                            if (student) {
                                student.cohort_slack_room_id = slackRoomId;
                            }
                        });
                    });
                    setHasChanges();

                    // Since this button can be pushed while we're looking at
                    // room stats, we need to reset it
                    resetSlackRoomStats();
                };

                scope.reset = () => {
                    _.each(scope.proxy.students, student => {
                        student.cohort_slack_room_id = originalAssignments[student.last_application_id];
                    });
                    scope.hasChanges = false;

                    // Since this button can be pushed while we're looking at
                    // room stats, we need to reset it
                    resetSlackRoomStats();
                };

                scope.toggleStats = () => {
                    scope.proxy.showStats = !scope.proxy.showStats;
                    scrollHelper.scrollToTop();
                };

                function setIndexParams() {
                    scope.indexParams = null;
                    if (!scope.cohort) {
                        return;
                    }
                    scope.indexParams = {
                        view: 'editable',
                        filters: {
                            accepted_or_pre_accepted_in: scope.cohort.id,
                        },
                    };

                    // FIXME: This means that accepted, un-registered students will
                    // not be included.  There are only 3 such students as of 6/29/2018
                    // and it does not affect assigning cohort rooms, so I'm not worrying
                    // about this for now.
                    if (scope.cohort.supportsPayments) {
                        scope.indexParams.filters.registered = true;
                    }
                }

                function resetSlackRoomStats() {
                    scope.$broadcast('slackRoomStatsRequireReset');
                }
            },
        };
    },
]);
