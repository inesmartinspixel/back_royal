import angularModule from 'Admin/angularModule/scripts/admin_module';
import template from 'Admin/angularModule/views/admin_mba/admin_cohort_enrollment_status.html';
import customFieldsTemplate from 'Admin/angularModule/views/admin_mba/admin_cohort_enrollment_status_custom_fields.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);
const customTemplatePath = cacheAngularTemplate(angularModule, customFieldsTemplate);

angularModule.directive('adminCohortEnrollmentStatus', [
    '$injector',

    function factory($injector) {
        const $window = $injector.get('$window');
        const editContentItemListMixin = $injector.get('editContentItemListMixin');
        const AdminCohortStudentsTableHelper = $injector.get('AdminCohortStudentsTableHelper');
        const SignableDocument = $injector.get('SignableDocument');
        const frontRoyalUpload = $injector.get('frontRoyalUpload');
        const DialogModal = $injector.get('DialogModal');
        const dateHelper = $injector.get('dateHelper');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                cohort: '<',
            },
            link(scope) {
                const adminCohortStudentsTableHelper = new AdminCohortStudentsTableHelper(
                    scope.cohort,
                    `${$window.ENDPOINT_ROOT}/api/users/batch_update_cohort_enrollment_status.json`,
                    {
                        indexParams: {
                            select_career_profile: true,
                            select_education_experiences: true,
                            select_transcripts: true,
                            select_signable_documents: true,
                        },
                    },
                );
                scope.adminCohortStudentsTableHelper = adminCohortStudentsTableHelper;

                adminCohortStudentsTableHelper.trackStudentChanges(student =>
                    student.user_id_verifications ? student.user_id_verifications.length : 0,
                );
                adminCohortStudentsTableHelper.trackStudentChanges(student => student.transcripts_verified);
                adminCohortStudentsTableHelper.trackStudentChanges(
                    student => student.english_language_proficiency_documents_approved,
                );

                scope.$on('$destroy', () => {
                    adminCohortStudentsTableHelper.destroy();
                });

                scope.setColumns = () => {
                    scope.columns = [
                        adminCohortStudentsTableHelper.getNameColumn(),
                        adminCohortStudentsTableHelper.getEmailColumn(),
                        {
                            id: 'identity-verified',
                            prop: 'identifiedForEnrollment',
                            type: 'checkIfTrue',
                            label: 'ID Verified?',
                            classes: ['text-center'],
                        },
                        {
                            id: 'transcripts-uploaded',
                            prop: 'requiredTranscriptsUploadedColumnString',
                            type: 'string',
                            label: 'Education with Transcripts Uploaded',
                            classes: ['text-center'],
                            classesCallback: (column, careerProfile) => {
                                if (careerProfile.missingTranscripts) {
                                    return ['beige'];
                                }
                            },
                            doNotExport: true,
                        },
                        {
                            id: 'transcripts-approved',
                            prop: 'requiredTranscriptsApprovedColumnString',
                            type: 'string',
                            label: 'Education with Transcripts Approved',
                            classes: ['text-center'],
                            classesCallback: (column, careerProfile) => {
                                if (careerProfile.missingTranscriptApprovals) {
                                    return ['beige'];
                                }
                            },
                            doNotExport: true,
                        },
                        {
                            id: 'has-uploaded-english-language-proficiency-documents',
                            prop: 'hasUploadedEnglishLanguageProficiencyDocuments',
                            type: 'custom',
                            label: 'Uploaded English Proficiency?',
                            classes: ['text-center'],
                            templateUrl: customTemplatePath,
                            callbacks: getCallbacksForCheckmarkColumn(
                                'english_language_proficiency_documents_approved',
                                'hasUploadedEnglishLanguageProficiencyDocuments',
                                'careerProfileIndicatesUserShouldUploadEnglishLanguageProficiencyDocuments',
                            ),
                        },
                        {
                            id: 'english-proficiency-documents-approved',
                            prop: 'english_language_proficiency_documents_approved',
                            type: 'custom',
                            label: 'English Proficiency Approved?',
                            classes: ['text-center'],
                            templateUrl: customTemplatePath,
                            callbacks: getCallbacksForCheckboxColumn(
                                'english_language_proficiency_documents_approved',
                                'careerProfileIndicatesUserShouldUploadEnglishLanguageProficiencyDocuments',
                            ),
                        },
                        {
                            id: 'has-signed-enrollment-agreement',
                            prop: 'has_signed_enrollment_agreement',
                            type: 'custom',
                            label: 'Agreement Signed?',
                            templateUrl: customTemplatePath,
                            cohortId: scope.cohort.id, // needed inside of admin_cohort_enrollment_status_custom_fields.html
                            classes: ['text-center'],
                            callbacks: {
                                sort(u) {
                                    return (
                                        scope.cohort.isLegacyEnrollmentCohort ||
                                        u.cohortApplicationsByCohortId[scope.cohort.id].hasSignedEnrollmentAgreement
                                    );
                                },
                                application(student) {
                                    return student.cohortApplicationsByCohortId[scope.cohort.id];
                                },
                                status(student) {
                                    const app = student.cohortApplicationsByCohortId[scope.cohort.id];
                                    if (scope.cohort.isLegacyEnrollmentCohort) {
                                        return 'legacyCheckmark';
                                    }
                                    if (app.hasSignedEnrollmentAgreement) {
                                        return 'downloadLink';
                                    }
                                    return 'uploadLink';
                                },
                                onDocumentSelect(student, file, errFiles) {
                                    const app = student.cohortApplicationsByCohortId[scope.cohort.id];
                                    student.$$uploadingEnrollmentAgreement = true;

                                    frontRoyalUpload
                                        .handleNgfSelect(file, errFiles, _file => ({
                                            url: SignableDocument.UPLOAD_URL,
                                            data: {
                                                record: {
                                                    file: _file,
                                                    user_id: student.id,
                                                    document_type: 'enrollment_agreement',
                                                    metadata: {
                                                        cohort_id: app.cohort_id,
                                                        program_type: app.program_type,
                                                    },
                                                },
                                            },
                                            supportedFormatsForErrorMessage: '.pdf, .doc, .docx, .jpg, .png',
                                        }))
                                        .then(response => {
                                            const signableDocument = _.first(response.data.contents.signable_documents);
                                            student.signable_documents = student.signable_documents || [];
                                            student.signable_documents.push(SignableDocument.new(signableDocument));
                                        })
                                        .catch(err => {
                                            const message = err && err.message;
                                            DialogModal.alert({
                                                content: message || 'Something went wrong',
                                            });
                                        })
                                        .finally(() => {
                                            student.$$uploadingEnrollmentAgreement = false;
                                        });
                                },
                                isDisabled(student) {
                                    return (
                                        scope.cohort.isLegacyEnrollmentCohort ||
                                        adminCohortStudentsTableHelper.saving ||
                                        student.$$uploadingEnrollmentAgreement
                                    );
                                },
                                getFormattedColumnValueForCSV(student) {
                                    return scope.cohort.isLegacyEnrollmentCohort
                                        ? 'Not required'
                                        : student.cohortApplicationsByCohortId[scope.cohort.id]
                                              .hasSignedEnrollmentAgreement;
                                },
                            },
                        },
                        {
                            id: 'is-accepted',
                            prop: 'isAccepted',
                            type: 'checkIfTrue',
                            label: 'Accepted?',
                            classes: ['text-center'],
                        },
                        adminCohortStudentsTableHelper.getSaveColumn(),
                    ];

                    // if the cohort requiresIdUpload, then add a column just before the identity-verified column
                    // that shows if the student hasUploadedIdentification, which makes it more clear for the admin
                    // user when they're using filters if they are missingRequiredDocuments and/or readyForApproval
                    if (scope.cohort.requiresIdUpload) {
                        const idVerifiedColumnIndex = _.findIndex(
                            scope.columns,
                            column => column.id === 'identity-verified',
                        );
                        scope.columns.splice(idVerifiedColumnIndex, 0, {
                            id: 'has-uploaded-identification',
                            prop: 'hasUploadedIdentification',
                            type: 'checkIfTrue',
                            label: 'Uploaded ID?',
                        });
                    }

                    //----------------------------------------------------------
                    // Special-Case Handling for ID Verification Period Columns
                    //----------------------------------------------------------

                    const verificationPeriodPrefix = 'verification-period-';

                    const getIdVerificationPeriodColumn = (verificationPeriod, periodIndex) => ({
                        id: `${verificationPeriodPrefix}${periodIndex}`,
                        prop: `id_verification_period_${periodIndex}`,
                        type: 'custom',
                        label: `ID Verification ${dateHelper.formattedUserFacingMonthDayYearShort(
                            verificationPeriod.dueDate,
                            false,
                        )}`,
                        classes: ['text-center'],
                        templateUrl: customTemplatePath,
                        isIdVerificationPeriodColumn: true,
                        verificationPeriod,
                        callbacks: {
                            userIdVerification(user) {
                                return user.userIdVerificationForPeriod(verificationPeriod);
                            },
                            isSaving() {
                                return adminCohortStudentsTableHelper.saving;
                            },
                            getFormattedColumnValueForCSV(user) {
                                const verification = user.userIdVerificationForPeriod(verificationPeriod);
                                return verification && verification.verification_method;
                            },
                            sort(user) {
                                const verification = user.userIdVerificationForPeriod(verificationPeriod);

                                // when you first check a checkbox, before it is saved,
                                // we don't want the record to move in the list
                                if (!verification || !verification.verified_at) {
                                    return 0;
                                }
                                return verification.verification_method;
                            },
                        },
                    });

                    // construct the ID verification period columns
                    const idVerificationColumns = [];
                    (scope.cohort.id_verification_periods || []).forEach((verificationPeriod, periodIndex) =>
                        idVerificationColumns.push(getIdVerificationPeriodColumn(verificationPeriod, periodIndex)),
                    );

                    // insert the ID verification period columns after the email column
                    const emailColumnIndex = scope.columns.findIndex(
                        column => column.id === AdminCohortStudentsTableHelper.EMAIL_COLUMN_ID,
                    );
                    scope.columns.splice(emailColumnIndex + 1, 0, ...idVerificationColumns);

                    // Remove and add columns specifically for the CSV export
                    scope.csvExportColumns = _.reject(
                        scope.columns,
                        column => column.id === 'editorAbilities' || !!column.doNotExport,
                    );
                    scope.csvExportColumns = scope.csvExportColumns.concat([
                        {
                            label: 'Education with Transcripts Required',
                            prop: 'career_profile.numRequiredTranscripts',
                            type: 'string',
                        },
                        {
                            label: 'Education with Transcripts Uploaded',
                            prop: 'career_profile.numRequiredTranscriptsUploaded',
                            type: 'string',
                        },
                        {
                            label: 'Education with Transcripts Waived',
                            prop: 'career_profile.numRequiredTranscriptsWaived',
                            type: 'string',
                        },
                        {
                            label: 'Education with Transcripts Approved',
                            prop: 'career_profile.numRequiredTranscriptsApproved',
                            type: 'string',
                        },
                    ]);
                };

                scope.$watch('cohort', scope.setColumns);

                function getCallbacksForCheckmarkColumn(
                    docsVerifiedProp,
                    hasUploadedProp,
                    indicatesUserShouldUploadProp,
                ) {
                    const callbacks = {};

                    // isVisible uses the student instead of the $$proxy because it is not changeable
                    callbacks.isVisible = function isVisible(student) {
                        return student[hasUploadedProp] || !student[indicatesUserShouldUploadProp];
                    };

                    // isDisabled uses the student instead of the $$proxy because it is not changeable
                    callbacks.isDisabled = function isDisabled(student) {
                        return !student[docsVerifiedProp] && !student[indicatesUserShouldUploadProp];
                    };

                    // sort uses the student instead of the proxy so that if we edit a user inline after
                    // sorting, the row will not move.  Unfortunately, this also means that if you edit a row
                    // and then sort (without saving), the sort will not be based on the changes you made, but
                    // rather on the underlying values.  Still seemed, though, that this is the best implementation.
                    callbacks.sort = function sort(student) {
                        if (student[hasUploadedProp]) {
                            return 1;
                        }
                        return 0;
                    };
                    callbacks.getFormattedColumnValueForCSV = student => {
                        if (student[hasUploadedProp]) {
                            return true;
                        }
                        if (!student[indicatesUserShouldUploadProp]) {
                            return 'Not required';
                        }
                        return false;
                    };

                    // When sorting, we always want to move users who do not need to upload this thing
                    // anyway to the end
                    callbacks.emptyToEnd = student => student[indicatesUserShouldUploadProp];
                    return callbacks;
                }

                function getCallbacksForCheckboxColumn(docsVerifiedProp, indicatesUserShouldUploadProp) {
                    const callbacks = {};

                    callbacks.onClick = student => {
                        student.$$proxy[docsVerifiedProp] = !student.$$proxy[docsVerifiedProp];
                    };

                    // isChecked is based on the proxy because the status of the checkbox should show the value
                    // on the proxy if it has been edited
                    callbacks.isChecked = student =>
                        student.$$proxy[docsVerifiedProp] || !student[indicatesUserShouldUploadProp];

                    // isDisabled is based on the student rather than the proxy because it is only based
                    // on things that cannot change.
                    callbacks.isDisabled = student =>
                        adminCohortStudentsTableHelper.saving || !student[indicatesUserShouldUploadProp];

                    // See comment above about why sort is based on the student instead of the proxy.
                    callbacks.sort = student => {
                        if (student[docsVerifiedProp]) {
                            return 1;
                        }
                        return 0;
                    };
                    callbacks.getFormattedColumnValueForCSV = student => {
                        if (student[docsVerifiedProp]) {
                            return true;
                        }
                        if (!student[indicatesUserShouldUploadProp]) {
                            return 'Not required';
                        }
                        return student[docsVerifiedProp];
                    };
                    callbacks.emptyToEnd = student => student[indicatesUserShouldUploadProp];
                    return callbacks;
                }

                //-------------------------
                // Filters
                //-------------------------

                scope.quickFilterProperties = ['name', 'email'];

                scope.missingRequiredDocumentsOptions = getBooleanFilterOptions(
                    'Missing Required Documents',
                    'Not Missing Required Documents',
                );
                scope.readyForApprovalOptions = getBooleanFilterOptions('Ready for Approval', 'Not Ready for Approval');
                scope.isAcceptedOptions = getBooleanFilterOptions(
                    'Accepted',
                    scope.cohort.supportsPayments ? 'Pre-Accepted (Registered)' : 'Pre-Accepted',
                );

                function getBooleanFilterOptions(trueLabel, falseLabel) {
                    return [
                        {
                            label: trueLabel,
                            value: 'true',
                        },
                        {
                            label: falseLabel,
                            value: 'false',
                        },
                    ];
                }

                // special handling for legacy / non-legacy filtering
                if (scope.cohort.isLegacyEnrollmentCohort) {
                    scope.missingRequiredDocumentsKey = 'missingRequiredDocumentsLegacy';
                    scope.readyForApprovalKey = 'readyForApprovalLegacy';
                } else {
                    scope.missingRequiredDocumentsKey = 'missingRequiredDocuments';
                    scope.readyForApprovalKey = 'readyForApproval';
                }
                const booleanProperties = [
                    scope.missingRequiredDocumentsKey,
                    scope.readyForApprovalKey,
                    'isAccepted',
                    'needsToRegister',
                ];

                // default filters
                const defaultFilters = _.map(
                    adminCohortStudentsTableHelper.indexParams.filters,
                    (filterValue, filterKey) => {
                        const filter = {
                            server: true,
                            default: true,
                            value: {},
                        };

                        // We collect enrollment-related data from both accepted and pre_accepted users.
                        // Therefore, we need to allow admins to view both accepted/pre_accepted users
                        // in this directive.
                        if (filterKey === 'cohort_status') {
                            // For cohorts that support payments, we also want to restrict the users to users
                            // that aren't just pre_accepted, but have also registered. This way we ignore users
                            // that are pre-accepted but haven't registered yet since the admins using this page
                            // really don't care about those users anyway.
                            filter.value[filterKey] = scope.cohort.supportsPayments
                                ? ['accepted_or_pre_accepted_registered']
                                : ['accepted', 'pre_accepted'];
                        } else {
                            filter.value[filterKey] = filterValue;
                        }
                        return filter;
                    },
                );

                // wire up filtering support
                editContentItemListMixin.onLink(
                    scope,
                    'adminEditCohortEnrollmentStatus',
                    defaultFilters,
                    booleanProperties,
                );

                // don't attempt to persist legacy / non-legacy filters if transitioning between cohorts
                scope.$watch('clientFilters', () => {
                    if (scope.cohort.isLegacyEnrollmentCohort) {
                        delete scope.clientFilters.missingRequiredDocuments;
                        delete scope.clientFilters.readyForApproval;
                    } else {
                        delete scope.clientFilters.missingRequiredDocumentsLegacy;
                        delete scope.clientFilters.readyForApprovalLegacy;
                    }

                    // Always revert to showing only accepted applications
                    scope.clientFilters.isAccepted = 'true';
                    delete scope.clientFilters.needsToRegister;
                });

                scope.$watch('clientFilters.isAccepted', () => {
                    // While we want to show pre_accepted users in this directive, we want
                    // to clamp it down to pre_accepted users that do not need to register.
                    if (scope.clientFilters.isAccepted === 'false') {
                        scope.clientFilters.needsToRegister = false;
                    } else {
                        delete scope.clientFilters.needsToRegister;
                    }
                });
            },
        };
    },
]);
