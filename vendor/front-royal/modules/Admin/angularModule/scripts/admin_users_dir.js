import angularModule from 'Admin/angularModule/scripts/admin_module';
import template from 'Admin/angularModule/views/admin_users.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('adminUsers', [
    '$injector',

    function factory($injector) {
        const $location = $injector.get('$location');
        const AppHeaderViewModel = $injector.get('Navigation.AppHeader.AppHeaderViewModel');
        const $rootScope = $injector.get('$rootScope');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                section: '@',
            },
            link(scope) {
                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                AppHeaderViewModel.setTitleHTML('Manage Users');
                AppHeaderViewModel.setBodyBackground('beige');
                AppHeaderViewModel.showAlternateHomeButton = false;
                scope.containerViewModel = {
                    header: '',
                };

                scope.availableSections = [
                    {
                        id: 'applicants',
                        label: 'Applicants',
                    },
                    {
                        id: 'all',
                        label: 'Users',
                    },
                ];

                // We don't give interviewers edit permissions on users, so might as
                // well hide the Users admin.
                if (scope.currentUser.roleName() === 'interviewer') {
                    const index = scope.availableSections.findIndex(s => s.id === 'all');
                    scope.availableSections.splice(index, 1);
                }

                scope.gotoSection = section => {
                    $location.url(`/admin/users/${section}`);
                };

                scope.isSectionActive = section => {
                    const current = $location.path().replace('/admin/users/', '');
                    if (section.id === current) {
                        return true;
                    }
                };

                scope.getSectionLabel = () => {
                    const section = _.findWhere(scope.availableSections, {
                        id: scope.section,
                    });
                    return section.label;
                };

                scope.getSectionHeader = () => {
                    if (scope.containerViewModel && scope.containerViewModel.headerOverride) {
                        return scope.containerViewModel.headerOverride;
                    }
                    return scope.getSectionLabel();
                };

                scope.getMailtoLink = () => `mailto:${scope.containerViewModel.email}`;

                scope.getResumeLink = () => scope.containerViewModel.resumeUrl;

                scope.getLinkedInLink = () => scope.containerViewModel.linkedInUrl;

                scope.$on('$destroy', () => {
                    AppHeaderViewModel.toggleVisibility(true);
                });
            },
        };
    },
]);
