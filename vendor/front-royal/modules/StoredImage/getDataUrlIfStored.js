export default async function getDataUrlIfStored(url, db) {
    const storedImage = await db.images.where('url').equals(url).first();

    if (storedImage) {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(storedImage.data);
            reader.onload = e => {
                resolve(e.target.result);
            };
            reader.onerror = reject;
            reader.onabort = reject;
        });
    }

    return url;
}
