// NOTE: This stuff is duplicated in hybrid/www/index.html.  Tried to import
// this over there, but ran into various issues.

// Generate a script tag for the passed in asset path and append it to the body
function injectScriptTagForAsset(path) {
    const script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = path.replace(/^\//, ''); // remove any prepended forward slash
    script.async = false; // add to execution queue
    script.defer = true; // don't block rendering
    document.getElementsByTagName('body')[0].appendChild(script);
}

// Get all of the paths for JS assets in the webpack manifest that have code used
// by the targetModule and inject the script tags for those assets into the HTML,
// except for assets that already had their script tags injected.
export default function injectScriptTagsForModule(targetModule, injectedScriptTags, delay) {
    setTimeout(() => {
        const targetModuleRegex = new RegExp(`${targetModule}.*.js$`);

        // Do not use Object.keys here, since we don't have corejs
        // in inlined code
        Object.keys(window.webpackManifest).forEach(key => {
            const path = window.webpackManifest[key];
            if (!injectedScriptTags[path] && targetModuleRegex.test(path)) {
                injectedScriptTags[path] = true;
                injectScriptTagForAsset(path);
            }
        });
    }, delay);
}
