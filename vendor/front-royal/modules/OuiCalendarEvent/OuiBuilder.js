// See comment in https://stackoverflow.com/a/21653600/1747491
function formatDateToParam(date) {
    return date ? date.toISOString().replace(/-|:|\.\d+/g, '') : '';
}

// See https://tools.ietf.org/html/rfc5545#section-3.3.11
//  The "TEXT" property values may also contain special characters
//  that are used to signify delimiters, such as a COMMA character for
//  lists of values or a SEMICOLON character for structured values.
//  In order to support the inclusion of these special characters in
//  "TEXT" property values, they MUST be escaped with a BACKSLASH
//  character.  A BACKSLASH character in a "TEXT" property value MUST
//  be escaped with another BACKSLASH character.  A COMMA character in
//  a "TEXT" property value MUST be escaped with a BACKSLASH
//  character.  A SEMICOLON character in a "TEXT" property value MUST
//  be escaped with a BACKSLASH character.  However, a COLON character
//  in a "TEXT" property value SHALL NOT be escaped with a BACKSLASH
//  character.
//
//  DESCRIPTION - https://tools.ietf.org/html/rfc5545#section-3.8.1.5
//  LOCATION - https://tools.ietf.org/html/rfc5545#section-3.8.1.7
//  SUMMARY - https://tools.ietf.org/html/rfc5545#section-3.8.1.12
//
// Also note the trickyness regarding newlines. The spec states that
// newlines should be encoded as "\n". But you have to remember that
// JavaScript will consume that as a newline character. So to get "\n"
// to the ICS field as a literal string you need to use "\\n". But then you
// have to make sure you aren't doubly escaping, since backslashes are themselves
// supposed to be escaped. Tricky tricky.
// See https://stackoverflow.com/a/667274/1747491
function escapeForICS(value) {
    if (!value) {
        return null;
    }

    // First escape backslash, comma, or semicolon characters
    let escapedValue = value.replace(/\\|,|;/g, char => `\\${char}`);

    // Then escape any newline characters
    escapedValue = escapedValue.replace(/\n/g, '\\n');
    return escapedValue;
}

// TODO: Consider switching to the official gapi library?
// For unofficial documentation for the HTTP API see:
//  https://stackoverflow.com/a/23495015/1747491
//  https://stackoverflow.com/a/21653600/1747491
function google(event) {
    let startDateParam;
    let endDateParam;

    if (event.allday) {
        // TODO: support allday
    } else {
        startDateParam = formatDateToParam(event.startDate);
        endDateParam = formatDateToParam(event.endDate);
    }

    // This http API supports a timezone param, `ctz`, but since
    // the browser should handle converting the UTC date I'm not
    // going to bother supporting it right now.
    return window.encodeURI(
        [
            'https://www.google.com/calendar/render',
            '?action=TEMPLATE',
            `&text=${event.title}`,
            `&dates=${startDateParam}`,
            `/${endDateParam}`,
            `&details=${event.description}`,
            `&location=${event.address}`,
            '&sprop=&sprop=name:',
        ].join(''),
    );
}

// TODO: See if I can find more documentation about the /owa request used
// Note that OWA stands for Outlook Web Access
function outlook(event) {
    // TODO: DRY some of this up with the other builder methods?
    let startDateParam;
    let endDateParam;

    if (event.allday) {
        // TODO: support allday
    } else {
        startDateParam = formatDateToParam(event.startDate);
        endDateParam = formatDateToParam(event.endDate);
    }

    return window.encodeURI(
        [
            'https://outlook.office365.com/owa/',
            '?path=/calendar/action/compose',
            '&rru=addevent',
            `&subject=${event.title}`,
            `&startdt=${startDateParam}`,
            `&enddt=${endDateParam}`,
            `&body=${event.description}`,
            `&location=${event.address}`,
            `&allday=${event.allday}`,
        ].join(''),
    );
}

function yahoo() {
    // TODO: Maybe?
    // See https://git.io/JeG0C
    throw new Error('Yahoo not implemented');
}

/**
 * Creates an ICS file
 *
 * @param {OuiEvent} event
 * @param {'string' | 'blob' | 'uri'} returnType - The desired return type of the ICS file
 *
 */
function ics(event, prodid, returnType = 'string') {
    let startDateParam;
    let endDateParam;
    let stampDateParam;

    if (event.allday) {
        // TODO: Support allday
    } else if (event.timezone) {
        // TODO: You can specify timezone information in the ICS, though it
        // looks to be a pain (see https://stackoverflow.com/a/35649729/1747491).
        // I'm not sure that we need to support it though, as all calendars I've
        // tested in seem to take a UTC date and properly format it for the
        // client's timezone.
    }

    startDateParam = formatDateToParam(event.startDate);
    endDateParam = formatDateToParam(event.endDate);
    stampDateParam = formatDateToParam(new Date());

    // TODO: See if there is a minimal library for generating this. Seems like
    // there should be.
    // See this handy validator -- https://icalendar.org/validator.html
    const fileContents = [
        'BEGIN:VCALENDAR',
        `PRODID:${prodid}`,
        'VERSION:2.0',
        'BEGIN:VEVENT',
        `URL:${event.url}`,
        `DTSTART:${startDateParam}`,
        `DTEND:${endDateParam}`,
        `DTSTAMP:${stampDateParam}`,
        `SUMMARY:${escapeForICS(event.title)}`,
        `DESCRIPTION:${escapeForICS(event.description)}`,
        `LOCATION:${escapeForICS(event.address)}`,
        `UID:${event.id}`,
        'END:VEVENT',
        'END:VCALENDAR',
    ].join('\n');

    // FIXME: There's currently an issue in Firefox mobile, at least on Android
    // See https://bugzilla.mozilla.org/show_bug.cgi?id=1429362
    if (returnType === 'string') {
        return fileContents;
    }
    if (returnType === 'blob') {
        return new Blob([fileContents], {
            type: 'text/calendar',
        });
    }
    if (returnType === 'uri') {
        return window.encodeURI(`data:text/calendar;charset=utf8,${fileContents}`);
    }
}

const OuiBuilder = {
    google,
    outlook,
    yahoo,
    ics,
};

export default OuiBuilder;
