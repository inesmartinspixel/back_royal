import 'AngularSpecHelper';
import 'ItemGroupsEditor/angularModule';

describe('Admin::ItemGroupsEditorDir', () => {
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let $rootScope;
    let groups;
    let item;
    let Group;
    let Iguana;
    let Item;
    let $timeout;

    angular.mock.module.sharedInjector();

    beforeAll(() => {
        angular.mock.module('ItemGroupsEditor', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            $rootScope = $injector.get('$rootScope');
            Group = $injector.get('Group');
            Iguana = $injector.get('Iguana');
            $timeout = $injector.get('$timeout');
        });

        groups = [
            Group.new({
                id: 1,
                name: 'Group1',
            }),
            Group.new({
                id: 2,
                name: 'Group2',
            }),
        ];

        Group.expect('index').returns(groups);

        Item = Iguana.subclass(function () {
            this.collection = 'items';
        });

        item = Item.new({
            groups: [groups[0]],
        });

        render();
    });

    it('should load groups', () => {
        expect(_.pluck(scope.groups, 'id')).toEqual(_.pluck(groups, 'id'));
    });

    it('should allow for updating the groups for the item', () => {
        SpecHelper.updateSelectize(elem, '[name="groups"]', [groups[0].id, groups[1].id]);
        expect(_.pluck(item.groups, 'id')).toEqual(_.pluck(groups, 'id'));
    });

    it('should share a group list between two instances', () => {
        // Any instances of this should share the same array
        // that is returned from Group.cachedIndex.  This ensures that
        // if a new group is added, it is shared globally.
        const scope1 = scope;
        render(true);
        const scope2 = scope;

        expect(scope1.groups).toBe(scope2.groups);

        const assertion = jest.fn().mockImplementation(_groups => {
            expect(scope1.groups).toBe(_groups);
        });
        Group.cachedIndex().then(assertion);
        $timeout.flush();
        expect(assertion).toHaveBeenCalled();
    });

    afterAll(() => {
        SpecHelper.cleanup();
    });

    function render(noFlush) {
        renderer = SpecHelper.renderer();
        renderer.scope.item = item;
        renderer.scope.groups = groups;
        renderer.render('<item-groups-editor item="item"></item-groups-editor>');
        elem = renderer.elem;
        scope = elem.isolateScope();
        $rootScope.$digest();

        if (noFlush !== true) {
            Group.flush('index');
        }
    }
});
