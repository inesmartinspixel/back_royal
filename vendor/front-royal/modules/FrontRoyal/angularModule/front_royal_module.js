import 'ng-toast';
import 'oclazyload';
import 'ng-file-upload';
import 'angular-animate';
import 'ng-token-auth';
import 'angular-cache-buster';

/*
    Due to the nature of angular modules and how they're simply statements that "register" a module
    with the app and any directives, services, etc. that the angular module may have, we can simply
    import the modules here just for their side-effects, meaning the module's global code gets run,
    effectively "registering" the angular module with the app, but without importing any values.
    We import these ES6 angular modules here rather than in the angular modules that actually list
    the angular module as a dependency to prevent the angular module from being "registered" more
    than once with the app. This strategy for importing modules is unique to our angular modules
    that have been ported over to ES6 module form. Traditional ES6 modules should be imported in
    the places they're needed as usual.
*/
import 'Compile/angularModule';
import 'ErrorLogging/angularModule';
import 'FrontRoyalStore/angularModule';
import 'FrontRoyalWrapper/angularModule';
import 'Groups/angularModule';
import 'IguanaExtensions/angularModule';
import 'Injector/angularModule';
import 'LearnerProjects/angularModule';
import 'NoApply/angularModule';
import 'OfflineMode/angularModule';
import 'Onetrust/angularModule';
import 'Telephone/angularModule';
import 'PrioritizedInterceptors/angularModule';
import 'Translation/angularModule';
import 'AngularHttpQueueAndRetry/angularModule';
import 'IsMobile/angularModule';
import 'RouteAnimationHelper/angularModule';
import 'ScrollHelper/angularModule';
import 'Status/angularModule';
import 'MobileAppRateHelper/angularModule';
import 'FrontRoyalAssetPreloader/angularModule';
import 'Onboarding/angularModule';
import 'StudentNetwork/angularModule';
import 'Settings/angularModule';

// define all application dependencies (may be ordered)
const frontRoyalDependencies = [
    'prioritizedInterceptors',
    'EventLogger',
    'Translation',
    'scrollHelper',
    'FrontRoyal.ApiErrorHandler',
    'FrontRoyal.AssetPreloader',
    'FrontRoyal.Authentication',
    'FrontRoyal.UiBootstrap',
    'FrontRoyal.ContentItem',
    'FrontRoyal.Mailbox',
    'FrontRoyal.ErrorLogService',
    'FrontRoyal.Groups',
    'FrontRoyal.iguanaExtensions',
    'FrontRoyal.Institutions',
    'FrontRoyal.LearnerProjects',
    'FrontRoyal.Lessons',
    'FrontRoyal.Linkedin',
    'FrontRoyal.Navigation',
    'FrontRoyal.Onboarding',
    'FrontRoyal.Careers',
    'FrontRoyal.StudentNetwork',
    'FrontRoyal.Payments',
    'FrontRoyal.Playlists',
    'FrontRoyal.Status',
    'FrontRoyal.Users',
    'FrontRoyal.RouteAnimationHelper',
    'FrontRoyal.Settings',
    'FrontRoyal.Wrapper',
    'HelpScoutBeacon',
    'HttpQueueAndRetry',
    'Iguana',
    'Iguana.Adapters.RestfulIdStyle',
    'ClientStorage',
    'OfflineMode',
    'compileDir',
    'guid',
    'ng-token-auth',
    'ngAnimate',
    'ngCacheBuster',
    'ngResource',
    'ngRoute',
    'ngToast',
    'noApply',
    'isMobile',
    'oc.lazyLoad',
    'segmentio',
    'FrontRoyalStore',
    'ngFileUpload',
    'onetrustCookieHelper',
    'MobileAppRateHelper',
    'Injector',
];

const frontRoyalAngularModule = angular.module('FrontRoyal', frontRoyalDependencies);
export default frontRoyalAngularModule;
