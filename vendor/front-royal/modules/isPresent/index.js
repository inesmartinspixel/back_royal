export default function isPresent(val) {
    return val || val === 0;
}
