import 'AngularSpecHelper';
import 'Dynamic-node/angularModule';

describe('dynamicNode', () => {
    let SpecHelper;
    let elem;
    let scope;
    let $injector;

    angular.mock.module.sharedInjector();

    beforeAll(() => {
        angular.mock.module('dynamicNode', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                render();
            },
        ]);
    });

    afterAll(() => {
        SpecHelper.cleanup();
    });

    it('should render nothing if dynamic-node is undefined', () => {
        SpecHelper.expectEqual('<!-- dynamicNode: dir -->', elem.html());
    });

    it('should render an element when dynamic-node is defined', () => {
        scope.dir = 'my-element';
        scope.$digest();
        SpecHelper.expectElement(elem, 'my-element');
    });

    it('should pass along attributes', () => {
        scope.dir = 'my-element';
        scope.$digest();
        SpecHelper.expectEqual('value', elem.find('my-element').attr('my-attr'));
    });

    it('should re-render when dynamic-node changes', () => {
        scope.dir = 'my-element';

        scope.$digest();

        const el1 = SpecHelper.expectElement(elem, 'my-element');
        const createdScope = el1.scope();
        jest.spyOn(createdScope, '$destroy').mockImplementation(() => {});
        scope.dir = 'my-other-element';
        expect(el1.parent()[0]).not.toBeUndefined(); // sanity check

        scope.$digest();

        expect(createdScope.$destroy).toHaveBeenCalled();
        expect(el1.parent()[0]).toBeUndefined();
        SpecHelper.expectElement(elem, 'my-other-element');
    });

    it('should re-render when observe changes', () => {
        scope.dir = 'my-element';

        scope.$digest();

        const el1 = SpecHelper.expectElement(elem, 'my-element');
        const createdScope1 = el1.scope();
        jest.spyOn(createdScope1, '$destroy').mockImplementation(() => {});

        scope.a = 'newValue';
        expect(el1.parent()[0]).not.toBeUndefined(); // sanity check

        scope.$digest();

        expect(createdScope1.$destroy).toHaveBeenCalled();
        expect(el1.parent()[0]).toBeUndefined();
        const el2 = SpecHelper.expectElement(elem, 'my-element');
        const createdScope2 = el2.scope();
        jest.spyOn(createdScope2, '$destroy').mockImplementation(() => {});

        scope.a = 'another newValue';
        expect(el2.parent()[0]).not.toBeUndefined(); // sanity check

        scope.$digest();

        expect(createdScope2.$destroy).toHaveBeenCalled();
        expect(el2.parent()[0]).toBeUndefined();
        const el3 = SpecHelper.expectElement(elem, 'my-element');
        const createdScope3 = el3.scope();
        jest.spyOn(createdScope3, '$destroy').mockImplementation(() => {});

        scope.b = 'newValue';
        expect(el3.parent()[0]).not.toBeUndefined(); // sanity check

        scope.$digest();

        expect(createdScope3.$destroy).toHaveBeenCalled();
        expect(el3.parent()[0]).toBeUndefined();
        SpecHelper.expectElement(elem, 'my-element');
    });

    // does not work because animate is not really set up?
    // it('should implement after-animate event', function() {
    //     scope.dir = 'my-element';
    //     scope.afterAnimate = jest.fn();
    //     scope.$digest();
    //     expect(scope.afterAnimate).toHaveBeenCalled();
    //     expect(scope.afterAnimate.mock.calls[0][0].nodeName.toLowerCase()).toBe('my-element');
    // });

    function render() {
        const renderer = SpecHelper.renderer();
        renderer.render(
            '<div><div dynamic-node="dir" observe="a,b" my-attr="value" after-animate="afterAnimate($element)"></div></div>',
        );
        elem = renderer.elem;
        scope = renderer.scope.$$childTail;
    }
});
