import 'AngularSpecHelper';
import 'SoundManager/angularModule';

describe('SoundManager::SoundManager', () => {
    let $httpBackend;
    let SoundManager;
    let ErrorLogService;

    beforeEach(() => {
        angular.mock.module('SoundManager', 'SpecHelper');

        angular.mock.inject($injector => {
            $httpBackend = $injector.get('$httpBackend');
            SoundManager = $injector.get('SoundManager');
            ErrorLogService = $injector.get('ErrorLogService');
        });
    });

    describe('loadUrl', () => {
        it('should work', () => {
            const fooUrl = 'http://foo';
            const fakeAudioContext = {
                decodeAudioData() {},
            };
            jest.spyOn(ErrorLogService, 'notify').mockImplementation(() => {});
            SoundManager._context = fakeAudioContext;
            jest.spyOn(fakeAudioContext, 'decodeAudioData').mockImplementation(() => {});
            $httpBackend.when('GET', fooUrl).respond(200, {
                byteLength: 123,
            });

            SoundManager.loadUrl(fooUrl);
            $httpBackend.flush();

            expect(ErrorLogService.notify).not.toHaveBeenCalled();
            expect(fakeAudioContext.decodeAudioData).toHaveBeenCalled();
        });

        it('should handle and log $http error', () => {
            const fooUrl = 'http://foo';
            jest.spyOn(ErrorLogService, 'notify').mockImplementation(() => {});
            $httpBackend.when('GET', fooUrl).respond(503, {
                foo: 'bar',
            });
            SoundManager.loadUrl(fooUrl);
            $httpBackend.flush();
            expect(ErrorLogService.notify).toHaveBeenCalledWith(
                'Reached max attempts when trying to load audio',
                null,
                { url: fooUrl, byteLength: undefined, status: undefined },
            );
        });

        it('should handle and log decodeAudioData errors', () => {
            const fooUrl = 'http://foo';
            const fakeAudioContext = {
                decodeAudioData() {},
            };
            jest.spyOn(ErrorLogService, 'notify').mockImplementation(() => {});
            SoundManager._context = fakeAudioContext;
            jest.spyOn(fakeAudioContext, 'decodeAudioData').mockImplementation((data, success, failure) => {
                failure('Could not decode audio');
            });
            $httpBackend.when('GET', fooUrl).respond(200, { byteLength: 123 }, { 'some-header': '12345' });

            SoundManager.loadUrl(fooUrl);
            $httpBackend.flush();

            expect(fakeAudioContext.decodeAudioData).toHaveBeenCalled();
            expect(ErrorLogService.notify).toHaveBeenCalledWith('Could not decode audio', null, {
                url: fooUrl,
                byteLength: 123,
                status: 200,
                requestHeaders: {
                    Accept: 'application/json, text/plain, */*',
                },
                responseHeaders: { 'some-header': '12345' },
            });
        });
    });
});
