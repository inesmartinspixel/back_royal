import 'AngularSpecHelper';
import 'DeepExtend/angularModule';

describe('DeepExtend', () => {
    let DeepExtend;

    angular.mock.module.sharedInjector();

    beforeAll(() => {
        angular.mock.module('DeepExtend');

        angular.mock.inject(_DeepExtend_ => {
            DeepExtend = _DeepExtend_;
        });
    });

    it('should copy non-objects that only exist on src', () => {
        const target = {
            prop: 1,
        };
        const src = {
            a: 1,
            b: 'b',
            c: ['c'],
        };
        DeepExtend.extend(target, src);
        expect(target.prop).toBe(1);
        expect(target.a).toBe(src.a);
        expect(target.b).toBe(src.b);
        expect(target.c).toBe(src.c);
    });

    it('should override existing non-objects', () => {
        const target = {
            a: 2,
            b: 'orig',
            c: [],
        };
        const src = {
            a: 1,
            b: 'str',
            c: [],
        };
        DeepExtend.extend(target, src);
        expect(target.a).toBe(src.a);
        expect(target.b).toBe(src.b);
        expect(target.c).toBe(src.c);
    });

    // I had to make this be the case to avoid maximum call stack errors in frame.js
    it('should copy references of objects that do not exist on target', () => {
        const target = {};
        const src = {
            a: {
                a: 1,
            },
        };
        DeepExtend.extend(target, src);
        expect(target.a).toBe(src.a);
    });

    it('should deep merge objects', () => {
        const target = {
            a: {
                a: 'target',
                b: 'target',
            },
        };

        const src = {
            a: {
                a: 'src',
            },
        };

        DeepExtend.extend(target, src);
        expect(target.a.a).toBe('src');
        expect(target.a.b).toBe('target');
    });
});
