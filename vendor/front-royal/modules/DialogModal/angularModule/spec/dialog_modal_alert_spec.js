import 'AngularSpecHelper';
import 'DialogModal/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import dialogModalAlertLocales from 'DialogModal/locales/dialog_modals/dialog_modal_alert-en.json';
import apiErrorHandlerLocales from 'FrontRoyalApiErrorHandler/locales/front_royal_api_error_handler/api_error_handler-en.json';
import fatalLocales from 'FrontRoyalApiErrorHandler/locales/front_royal_api_error_handler/fatal-en.json';

setSpecLocales(dialogModalAlertLocales, apiErrorHandlerLocales, fatalLocales);

describe('Dialog::DialogModalAlert', () => {
    let SpecHelper;
    let DialogModal;
    let $document;
    let $rootScope;
    let $timeout;
    let dialogModalAlert;
    let dialogModalConfirm;
    let transitionsOriginallyEnabled;

    const testApp = angular.module('DialogModalTestApp', ['DialogModal']);
    testApp.directive('isolateScope', [
        function factory() {
            return {
                scope: {
                    prop: '=?',
                },
                restrict: 'E',
            };
        },
    ]);

    angular.mock.module.sharedInjector();

    beforeAll(() => {
        angular.mock.module('DialogModalTestApp', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            DialogModal = $injector.get('DialogModal');
            $document = $injector.get('$document');
            $rootScope = $injector.get('$rootScope');
            $timeout = $injector.get('$timeout');
        });

        // this will disable bootstrap transitions, making it much easier to
        // test the close button
        transitionsOriginallyEnabled = $.support.transition;
        $.support.transition = false;
    });

    afterAll(() => {
        $.support.transition = transitionsOriginallyEnabled;
        SpecHelper.cleanup();
    });

    beforeEach(() => {
        SpecHelper.stubConfig();
        SpecHelper.stubEventLogging();
    });

    describe('alert', () => {
        it('should render contents', () => {
            render({
                content: '<div id="content"></div>',
            });
            expect(dialogModalAlert.find('#content').length).toBe(1);
        });

        it('should render title if provided', () => {
            render({
                title: 'A title',
            });
            const modalTitle = dialogModalAlert.find('.modal-title');
            SpecHelper.expectEqual(1, modalTitle.length, 'modal title size');
            SpecHelper.expectEqual('A title', modalTitle.text().trim(), 'modal title content');
        });

        it('should not render title if not provided', () => {
            render();
            const modalTitle = dialogModalAlert.find('.modal-title');
            SpecHelper.expectEqual(0, modalTitle.length, 'modal title size');
        });

        it('should reset size if provided', () => {
            render({
                size: 'large',
            });
            SpecHelper.expectEqual(1, dialogModalAlert.find('.large').length, 'large sized modal');
        });

        it('should default to normal size', () => {
            render();
            SpecHelper.expectEqual(1, dialogModalAlert.find('.normal').length, 'normal sized modal');
        });

        it('should fire close callback', () => {
            let closeCalled = false;
            render({
                close() {
                    closeCalled = true;
                },
            });
            $timeout.flush();
            closeDialogModalAlert();
            SpecHelper.expectEqual(true, closeCalled, 'close callback called');
        });

        it('should pass properties down through the scope', () => {
            const prop = {};
            render({
                content: '<isolate-scope prop="prop"></isolate-scope>',
                scope: {
                    prop,
                },
            });

            const createdScope = getChildScope();
            expect(createdScope.prop).toBe(prop);
        });

        it('should destroy a scope that is created within the content', () => {
            render({
                content: '<isolate-scope></isolate-scope>',
            });

            const createdScope = getChildScope();
            let createdScopeDestroyed = false;
            createdScope.$on('$destroy', () => {
                createdScopeDestroyed = true;
            });
            $timeout.flush();
            closeDialogModalAlert();
            SpecHelper.expectEqual(true, createdScopeDestroyed, 'createdScopeDestroyed');
        });

        it('should render an indestructible modal', () => {
            render({
                indestructible: true,
            });

            DialogModal.removeAlerts();
            SpecHelper.expectElement($document, 'dialog-modal-alert .modal');
            DialogModal.removeAlerts(true);
            SpecHelper.expectNoElement($document, 'dialog-modal-alert .modal');
        });
    });

    describe('confirm', () => {
        let options;

        beforeEach(() => {
            options = {
                text: '',
            };
        });

        afterEach(() => {
            DialogModal.removeAlerts();
        });

        it('should render on top of another dialog modal', () => {
            render();
            const alert = $document.find('dialog-modal-alert:last-of-type');

            renderConfirm();
            const confirm = $document.find('dialog-modal-alert:last-of-type');

            SpecHelper.expectElements($document, 'dialog-modal-alert', 2);
            expect(alert.scope().$id).not.toEqual(confirm.scope().id);
        });

        it('should render text', () => {
            options.text = 'foobar';
            renderConfirm(options);
            SpecHelper.expectElementText(dialogModalConfirm, '.message', 'foobar');
        });

        it('should render cancel and ok buttons', () => {
            renderConfirm(options);
            SpecHelper.expectElements(dialogModalConfirm, '.modal-action-button', 2);
        });

        describe('onActionClick', () => {
            let result;
            let callback;

            beforeEach(() => {
                result = 'raboof';
                callback = () => {
                    result = 'foobar';
                };
            });

            it('should fire confirmCallback', () => {
                options.confirmCallback = callback;
                jest.spyOn(options, 'confirmCallback');
                expect(result).toEqual('raboof');

                renderAndHideDialogModalConfirm(1, options); // confirm button is at index 1

                expect(options.confirmCallback).toHaveBeenCalled();
                expect(result).toEqual('foobar');
            });

            it('should hide modal on confirm', () => {
                renderAndHideDialogModalConfirm(1, options); // confirm button is at index 1
                SpecHelper.expectNoElement($document, 'dialog-modal-alert');
            });

            it('should hide confirm modal on confirm if another dialog-modal-alert is visible', () => {
                assertDialogModalAlertIsPresentAfterConfirmModalIsHidden(1); // confirm button is at index 1
            });

            it('should fire cancelCallback', () => {
                options.cancelCallback = callback;
                jest.spyOn(options, 'cancelCallback');
                expect(result).toEqual('raboof');

                renderAndHideDialogModalConfirm(0, options); // cancel button is at index 0

                expect(options.cancelCallback).toHaveBeenCalled();
                expect(result).toEqual('foobar');
            });

            it('should hide modal on cancel', () => {
                renderAndHideDialogModalConfirm(0, options); // cancel button is at index 0
                SpecHelper.expectNoElement($document, 'dialog-modal-alert');
            });

            it('should hide confirm modal on cancel if another dialog-modal-alert is visible', () => {
                assertDialogModalAlertIsPresentAfterConfirmModalIsHidden(0); // cancel button is at index 0
            });

            describe('when options.confirmDeleteByTyping', () => {
                beforeEach(() => {
                    options.confirmDeleteByTyping = 'vorpal blade';
                    options.confirmDeleteThing = 'the jabberwocky';
                });

                it('should show proper text', () => {
                    renderConfirm(options);
                    $timeout.flush();

                    SpecHelper.expectElementText(
                        dialogModalConfirm,
                        'p',
                        'Are you sure you wish to delete the jabberwocky? Type vorpal blade below to confirm.',
                    );
                });

                it('should allow cancel before typing something', () => {
                    options.cancelCallback = callback;
                    jest.spyOn(options, 'cancelCallback');
                    expect(result).toEqual('raboof');

                    renderAndHideDialogModalConfirm(0, options); // cancel button is at index 0

                    expect(options.cancelCallback).toHaveBeenCalled();
                    expect(result).toEqual('foobar');
                });

                it('should disable confirm until typing matches, then properly cancel', () => {
                    options.confirmCallback = callback;
                    jest.spyOn(options, 'confirmCallback');
                    expect(result).toEqual('raboof');

                    renderConfirm(options);
                    $timeout.flush();

                    SpecHelper.expectElementDisabled(dialogModalConfirm, '.modal-action-button', true, 1);
                    SpecHelper.updateTextInput(dialogModalConfirm, 'input', 'vorpal blade');
                    SpecHelper.expectElementDisabled(dialogModalConfirm, '.modal-action-button', false, 1);

                    performActionForConfirmationDialogModal(0); // cancel button is at index 0
                    $timeout.flush();

                    expect(options.confirmCallback).not.toHaveBeenCalled();
                    expect(result).toEqual('raboof');

                    SpecHelper.expectNoElement($document, 'dialog-modal-alert');
                });

                it('should disable confirm until typing matches, then hide modal on confirm', () => {
                    options.confirmCallback = callback;
                    jest.spyOn(options, 'confirmCallback');
                    expect(result).toEqual('raboof');

                    renderConfirm(options);
                    $timeout.flush();

                    SpecHelper.expectElementDisabled(dialogModalConfirm, '.modal-action-button', true, 1);
                    SpecHelper.updateTextInput(dialogModalConfirm, 'input', 'vorpal blade');
                    SpecHelper.expectElementDisabled(dialogModalConfirm, '.modal-action-button', false, 1);

                    performActionForConfirmationDialogModal(1); // confirm button is at index 1
                    $timeout.flush();

                    expect(options.confirmCallback).toHaveBeenCalled();
                    expect(result).toEqual('foobar');

                    SpecHelper.expectNoElement($document, 'dialog-modal-alert');
                });
            });

            describe('when options.showThanks', () => {
                beforeEach(() => {
                    options.showThanks = true;
                });

                describe('cancel', () => {
                    it('should not show "Thank you" message', () => {
                        options.cancelCallback = callback;
                        jest.spyOn(options, 'cancelCallback');
                        expect(result).toEqual('raboof');

                        renderConfirm(options);
                        $timeout.flush();
                        const scope = SpecHelper.expectElement($document, 'dialog-modal-alert:last-of-type').scope();
                        expect(scope.showThanks).not.toBe(true);
                        performActionForConfirmationDialogModal(0); // cancel button is at index 0
                        expect(scope.showThanks).not.toBe(true);
                        SpecHelper.expectNoElement(dialogModalConfirm, '.thanks');
                        expect(options.cancelCallback).not.toHaveBeenCalled();
                        SpecHelper.expectElement($document, 'dialog-modal-alert:last-of-type');
                        $timeout.flush();
                    });
                });

                describe('confirm', () => {
                    it('should show "Thank you" message on confirm', () => {
                        renderConfirm(options);
                        $timeout.flush();
                        const scope = SpecHelper.expectElement($document, 'dialog-modal-alert:last-of-type').scope();
                        expect(scope.showThanks).not.toBe(true);
                        performActionForConfirmationDialogModal(1); // confirm button is at index 1
                        expect(scope.showThanks).toBe(true);
                        SpecHelper.expectElementText(dialogModalConfirm, '.thanks', 'Thanks for letting us know.');
                        $timeout.flush();
                    });

                    it('should delay firing the confirmCallback and hiding the modal', () => {
                        options.confirmCallback = callback;
                        jest.spyOn(options, 'confirmCallback');
                        expect(result).toEqual('raboof');

                        renderConfirm(options);
                        $timeout.flush();
                        const scope = SpecHelper.expectElement($document, 'dialog-modal-alert:last-of-type').scope();
                        expect(scope.showThanks).not.toBe(true);
                        performActionForConfirmationDialogModal(1); // confirm button is at index 1
                        expect(scope.showThanks).toBe(true);
                        SpecHelper.expectElementText(dialogModalConfirm, '.thanks', 'Thanks for letting us know.');
                        expect(options.confirmCallback).not.toHaveBeenCalled();
                        SpecHelper.expectElement($document, 'dialog-modal-alert:last-of-type');
                        $timeout.flush();

                        SpecHelper.expectNoElement($document, 'dialog-modal-alert:last-of-type');
                        expect(options.confirmCallback).toHaveBeenCalled();
                    });
                });
            });

            function assertDialogModalAlertIsPresentAfterConfirmModalIsHidden(actionButtonIndex) {
                render();
                renderAndHideDialogModalConfirm(actionButtonIndex, options);
                // we expect only one dialog-modal-alert element to be in existence
                // and we expect it to be the alert modal, not the confirm modal
                expect(dialogModalAlert).toEqual(SpecHelper.expectElement($document, 'dialog-modal-alert'));
            }
        });
    });

    describe('showFatalError', () => {
        it('should render and reload the page when closed', () => {
            DialogModal.showFatalError();
            $rootScope.$digest();
            dialogModalAlert = $document.find('dialog-modal-alert');

            SpecHelper.expectElementText(
                dialogModalAlert,
                '.message',
                "Something went wrong. It's not your fault. Our engineers have been notified of the issue. Click to return to the dashboard.",
            );
            jest.spyOn(DialogModal, '_reloadWindow').mockImplementation(() => {});
            SpecHelper.click(dialogModalAlert, '.modal-action-button');
            expect(DialogModal._reloadWindow).toHaveBeenCalled();
        });
    });

    describe('hidden.bs.modal event listener', () => {
        it('should ensure modal-open class is present on document body if another modal is visible', () => {
            render();
            renderAndHideDialogModalConfirm(1, {
                mockModalIsVisible: true,
            });

            SpecHelper.expectElement($document, 'dialog-modal-alert');
            const bodyEl = $($document[0].body);
            SpecHelper.expectElementHasClass(bodyEl, 'modal-open');
        });
    });

    function getChildScope() {
        const createdScope = dialogModalAlert.find('isolate-scope').isolateScope();
        SpecHelper.expectEqual(true, !!createdScope, 'createdScope exists');
        return createdScope;
    }

    function closeDialogModalAlert() {
        const close = dialogModalAlert.find('.close');
        SpecHelper.expectEqual(1, close.length, 'close buttons');
        close.click();
    }

    function renderAndHideDialogModalConfirm(actionButtonIndex, options) {
        renderConfirm(options);

        if (options.mockModalIsVisible) {
            const dialogModalConfirmScope = dialogModalConfirm.scope();
            jest.spyOn(dialogModalConfirmScope, 'modalIsVisible').mockReturnValue(true);
        }

        $timeout.flush();
        performActionForConfirmationDialogModal(actionButtonIndex);
        $timeout.flush();
    }

    function performActionForConfirmationDialogModal(actionButtonIndex) {
        SpecHelper.click(dialogModalConfirm, `.modal-action-button:eq(${actionButtonIndex})`);
    }

    function renderConfirm(options) {
        DialogModal.confirm(options);

        // the $compile will not actually run until a digest
        $rootScope.$digest();
        dialogModalConfirm = $document.find('dialog-modal-alert:last-of-type');
    }

    function render(options) {
        options = angular.extend(
            {
                content: '<div>Some content</div>',
            },
            options || {},
        );

        DialogModal.alert(options);

        // the $compile will not actually run until a digest
        $rootScope.$digest();

        dialogModalAlert = $document.find('dialog-modal-alert');
        expect(dialogModalAlert.length).toBe(1);
        const contents = dialogModalAlert.find('.modal');
        expect(contents.length).toBe(1);
    }
});
