import 'AngularSpecHelper';
import 'RouteAnimationHelper/angularModule';

describe('FrontRoyal.RouteAnimationHelper', () => {
    let SpecHelper;
    let $rootScope;
    let RouteAnimationHelper;
    let ngView;
    let $location;
    let $timeout;
    let Capabilities;
    let scrollHelper;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.RouteAnimationHelper', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                SpecHelper = $injector.get('SpecHelper');
                $rootScope = $injector.get('$rootScope');
                RouteAnimationHelper = $injector.get('RouteAnimationHelper');
                $location = $injector.get('$location');
                $timeout = $injector.get('$timeout');
                Capabilities = $injector.get('Capabilities');
                scrollHelper = $injector.get('scrollHelper');
            },
        ]);

        jest.spyOn($location, 'url').mockImplementation(() => {});
        jest.spyOn(scrollHelper, 'scrollToTop').mockImplementation(() => {});

        $(document.body).append('<div ng-view id="ng-view"></div>');
        ngView = $('[ng-view]');

        Object.defineProperty(Capabilities, 'cssTransitions', {
            value: true,
        });
    });

    afterEach(() => {
        $(document).find('[ng-view]').remove();
        SpecHelper.cleanup();
    });

    describe('animatePathChange', () => {
        it('should animate two path changes', () => {
            assertAnimatePathChange('my-class-1');
            assertAnimatePathChange('my-class-2');

            // make sure the first class was removed before the second animation
            SpecHelper.expectElementDoesNotHaveClass(ngView, 'my-class-1');
        });

        it('should animate one path change and then remove the class before a subsequent un-animated path change', () => {
            assertAnimatePathChange('my-class');
            SpecHelper.expectElementHasClass(ngView, 'my-class');

            // When a subsequent, un-animated route change happens,
            // the animation classes should be removed
            $rootScope.$broadcast('$routeChangeStart');
            SpecHelper.expectElementDoesNotHaveClass(ngView, 'my-class');
        });

        it('should remove an old ng-view that is still animating away before animating the current view away', () => {
            $(document.body).append('<div ng-view id="ng-view" class="ng-leave"></div>');

            SpecHelper.expectElement($(document), '[ng-view].ng-leave');
            assertAnimatePathChange('my-class');
            SpecHelper.expectNoElement($(document), '[ng-view].ng-leave');
        });

        it('should switch routes immediately if no transitions are avialable', () => {
            Object.defineProperty(Capabilities, 'cssTransitions', {
                value: false,
            });
            assertAnimatePathChange('my-class', 0);
        });
    });

    function assertAnimatePathChange(cssClass, duration) {
        duration = angular.isDefined(duration) ? duration : 1000;
        $location.url.mockClear();
        scrollHelper.scrollToTop.mockClear();
        RouteAnimationHelper.animatePathChange('newLocation', cssClass, 1000);
        SpecHelper.expectElementHasClass(ngView, cssClass);
        SpecHelper.expectElementHasClass(ngView, 'route-leave');
        if (duration > 0) {
            $timeout.flush(duration - 1);
        }
        expect($location.url).not.toHaveBeenCalled();

        $timeout.flush(1);
        expect($location.url).toHaveBeenCalled();
        expect(scrollHelper.scrollToTop).toHaveBeenCalledWith();
        $rootScope.$broadcast('$routeChangeStart');
        $rootScope.$broadcast('$routeChangeSuccess');
    }
});
