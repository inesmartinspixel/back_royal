export default angular
    .module('HelpScoutBeacon', [])
    .constant('HELPSCOUT_BEACON_EMBA_ID', '1ef24f81-9517-4620-adda-1cd67704dc58')
    .constant('HELPSCOUT_BEACON_HYBRID_ID', 'a3df8a38-e8c3-49c2-b21f-59ff7c7dce33');
