import 'AngularSpecHelper';
import 'HelpScoutBeacon/angularModule';

describe('helpScoutBeacon', () => {
    let $injector;
    let SpecHelper;
    let helpScoutBeacon;
    let $location;
    let $rootScope;
    let $window;

    beforeEach(() => {
        angular.mock.module('HelpScoutBeacon', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            __$injector => {
                $injector = __$injector;
                SpecHelper = $injector.get('SpecHelper');
                helpScoutBeacon = $injector.get('helpScoutBeacon');
                $location = $injector.get('$location');
                $rootScope = $injector.get('$rootScope');
                $window = $injector.get('$window');
                $window.Beacon = jest.fn();
                const $q = $injector.get('$q');

                jest.spyOn(helpScoutBeacon, '_loadScript').mockReturnValue($q.when());
            },
        ]);

        helpScoutBeacon.startWatching();
    });

    afterEach(() => {
        helpScoutBeacon.stopWatching();
        SpecHelper.cleanup();
    });

    it('should enable on application/settings', () => {
        $rootScope.currentUser = {
            programType: 'emba',
        };

        let path = '';
        jest.spyOn($location, 'path').mockImplementation(() => path);
        $rootScope.$digest();
        expect($window.Beacon).not.toHaveBeenCalled();

        path = '/settings/application';
        $rootScope.$digest();
        expect($window.Beacon).toHaveBeenCalledWith('init', $injector.get('HELPSCOUT_BEACON_EMBA_ID'));
        $window.Beacon.mockClear();

        path = '/somewhere/else';
        $rootScope.$digest();
        expect($window.Beacon).toHaveBeenCalledWith('destroy', $injector.get('HELPSCOUT_BEACON_EMBA_ID'));
    });

    it('should enable for emba and mba program types', () => {
        jest.spyOn($location, 'path').mockReturnValue('/settings/application');

        $rootScope.currentUser = {
            programType: 'career_network_only',
        };
        $rootScope.$digest();
        expect($window.Beacon).not.toHaveBeenCalled();

        $rootScope.currentUser.programType = 'emba';
        $rootScope.$digest();
        expect($window.Beacon).toHaveBeenCalledWith('init', $injector.get('HELPSCOUT_BEACON_EMBA_ID'));
        $window.Beacon.mockClear();

        $rootScope.currentUser.programType = 'mba';
        $rootScope.$digest();
        expect($window.Beacon).toHaveBeenCalledWith('destroy', $injector.get('HELPSCOUT_BEACON_EMBA_ID'));
        expect($window.Beacon).toHaveBeenCalledWith('init', $injector.get('HELPSCOUT_BEACON_HYBRID_ID'));
        $window.Beacon.mockClear();

        $rootScope.currentUser.programType = 'career_network_only';
        $rootScope.$digest();
        expect($window.Beacon).toHaveBeenCalledWith('destroy', $injector.get('HELPSCOUT_BEACON_HYBRID_ID'));
    });

    it('should call suggest when navigating', () => {
        jest.spyOn($location, 'path').mockReturnValue('/settings/application');
        let url = '/settings/application>page=1';
        jest.spyOn($location, 'url').mockImplementation(() => url);

        $rootScope.currentUser = {
            programType: 'emba',
        };
        $rootScope.$digest();
        expect($window.Beacon).toHaveBeenCalledWith('init', $injector.get('HELPSCOUT_BEACON_EMBA_ID'));
        $window.Beacon.mockClear();

        url = '/settings/application>page=2';
        $rootScope.$digest();
        expect($window.Beacon).toHaveBeenCalledWith('suggest');
    });
});
