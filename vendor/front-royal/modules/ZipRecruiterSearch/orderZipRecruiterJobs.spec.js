import orderZipRecruiterJobs from './orderZipRecruiterJobs';

describe('orderZipRecruiterJobs', () => {
    let jobs;

    beforeEach(() => {
        jobs = [
            {
                has_non_zr_url: true,
                salary_source: '',
                salary_max: 99999,
            },
            {
                has_non_zr_url: true,
                salary_source: '',
                salary_max: 99999,
            },
            {
                has_non_zr_url: true,
                salary_source: '',
                salary_max: 99999,
            },
        ];
    });

    it('should order by has_non_zr_url ascending', () => {
        jobs[2].has_non_zr_url = false;
        const ordered = orderZipRecruiterJobs(jobs);
        expect(ordered[0]).toEqual(jobs[2]);
    });

    it('should order by salary source weight mappings descending', () => {
        jobs[2].salary_source = 'provided';
        jobs[1].salary_source = 'parsed';
        jobs[0].salary_source = 'predicted';
        const ordered = orderZipRecruiterJobs(jobs);
        expect(ordered[0]).toEqual(jobs[2]);
        expect(ordered[1]).toEqual(jobs[1]);
        expect(ordered[2]).toEqual(jobs[0]);
    });

    it('should order by salary_max descending', () => {
        jobs[2].salary_max = '999';
        jobs[1].salary_max = '99';
        jobs[0].salary_max = '9';
        const ordered = orderZipRecruiterJobs(jobs);
        expect(ordered[0]).toEqual(jobs[2]);
        expect(ordered[1]).toEqual(jobs[1]);
        expect(ordered[2]).toEqual(jobs[0]);
    });
});
