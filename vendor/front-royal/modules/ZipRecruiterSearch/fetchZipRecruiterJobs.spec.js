import axios from 'axios';
import fetchZipRecruiterJobs from './fetchZipRecruiterJobs';
import buildUniqueApiFilterObjects from './buildUniqueApiFilterObjects';

jest.mock('axios');

let mockbuildUniqueApiFilterObjectsReturnValue;
jest.mock('./buildUniqueApiFilterObjects', () =>
    jest.fn().mockImplementation(() => mockbuildUniqueApiFilterObjectsReturnValue),
);

describe('fetchZipRecruiterJobs', () => {
    let $injector;
    let passedInFilters;
    let mockCountry;

    beforeEach(() => {
        mockCountry = 'US';
        const currentUser = {
            country: mockCountry,
        };

        const configValues = {
            ziprecruiter_search_url: 'https://foobar.com',
            ziprecruiter_api_key_us: 'api_key_us',
            ziprecruiter_api_key_de: 'api_key_de',
        };

        $injector = {
            get: key =>
                ({
                    ConfigFactory: {
                        getSync: () => {
                            return {
                                ...configValues,

                                // copied straight from config.zipRecruiterApiKeyForCountry
                                zipRecruiterApiKeyForCountry: (countryCode, useFallback) => {
                                    const keyByCountry =
                                        configValues[`ziprecruiter_api_key_${countryCode.toLowerCase()}`];
                                    return useFallback
                                        ? keyByCountry || configValues.ziprecruiter_api_key_us
                                        : keyByCountry;
                                },
                            };
                        },
                    },
                    $rootScope: { currentUser },
                }[key]),
        };

        axios.mockResolvedValue([]);
        axios.mockReset();

        passedInFilters = {
            role: [],
            places: [],
        };
    });

    it('should make an API request for every filter returned from buildUniqueApiFilterObjects', async () => {
        mockbuildUniqueApiFilterObjectsReturnValue = [
            {
                foo: 'bar',
                country: 'US',
            },
            {
                bar: 'foo',
                country: 'DE',
            },
        ];

        await fetchZipRecruiterJobs($injector, passedInFilters);
        expect(buildUniqueApiFilterObjects).toHaveBeenCalledWith($injector, passedInFilters);
        expect(axios).toHaveBeenCalledTimes(2);
        expect(axios.mock.calls[0]).toEqual([
            {
                method: 'get',
                url: 'https://foobar.com',
                params: {
                    foo: 'bar',
                    api_key: 'api_key_us',
                },
                headers: {
                    'Content-Type': 'application/json',
                },
            },
        ]);
        expect(axios.mock.calls[1]).toEqual([
            {
                method: 'get',
                url: 'https://foobar.com',
                params: {
                    bar: 'foo',
                    api_key: 'api_key_de',
                },
                headers: {
                    'Content-Type': 'application/json',
                },
            },
        ]);
    });

    it('should exclude requests containing filters for which we do not have a corresponding API key', async () => {
        mockbuildUniqueApiFilterObjectsReturnValue = [
            {
                foo: 'bar',
                country: 'US',
            },
            {
                bar: 'foo',
                country: 'GB',
            },
        ];

        await fetchZipRecruiterJobs($injector, passedInFilters);
        expect(buildUniqueApiFilterObjects).toHaveBeenCalledWith($injector, passedInFilters);
        expect(axios).toHaveBeenCalledTimes(1);
        expect(axios.mock.calls[0]).toEqual([
            {
                method: 'get',
                url: 'https://foobar.com',
                params: {
                    foo: 'bar',
                    api_key: 'api_key_us',
                },
                headers: {
                    'Content-Type': 'application/json',
                },
            },
        ]);
    });

    it('should use country of residence for API key if the country is not specified in the filters', async () => {
        mockbuildUniqueApiFilterObjectsReturnValue = [
            {
                foo: 'bar',
            },
        ];

        await fetchZipRecruiterJobs($injector, passedInFilters);
        expect(buildUniqueApiFilterObjects).toHaveBeenCalledWith($injector, passedInFilters);
        expect(axios).toHaveBeenCalledTimes(1);
        expect(axios.mock.calls[0]).toEqual([
            {
                method: 'get',
                url: 'https://foobar.com',
                params: {
                    foo: 'bar',
                    api_key: 'api_key_us',
                },
                headers: {
                    'Content-Type': 'application/json',
                },
            },
        ]);
    });

    it('should fallback to US API key if no country specified in filters and no API key for country of residence', async () => {
        mockbuildUniqueApiFilterObjectsReturnValue = [
            {
                foo: 'bar',
            },
        ];
        $injector.get('$rootScope').currentUser.country = 'GB';

        await fetchZipRecruiterJobs($injector, passedInFilters);
        expect(buildUniqueApiFilterObjects).toHaveBeenCalledWith($injector, passedInFilters);
        expect(axios).toHaveBeenCalledTimes(1);
        expect(axios.mock.calls[0]).toEqual([
            {
                method: 'get',
                url: 'https://foobar.com',
                params: {
                    foo: 'bar',
                    api_key: 'api_key_us',
                },
                headers: {
                    'Content-Type': 'application/json',
                },
            },
        ]);
    });
});
