import { isEmpty, find } from 'lodash/fp';
import filterHelpers from './filterHelpers';

/*

Description of client filtering from https://trello.com/c/5cH9Mhwu:

Beyond using the ZipRecruiter API directly to fetch jobs, we may need to do some additional
post-filtering on the results before showing data to students.

One example of this is salary filtering: ZipRecruiter returns any jobs whose max salary
is greater than the `refine_by_salary` value passed in. We will want to filter jobs whose
min_salary is greater than 10k less than the refine_by_salary value, to avoid showing
jobs that are hacking the system with enormously wide salary ranges (I've seen things like
a executive assistant position with a salary range of 30k to 105k!).

The API doesn't provide a way to filter by job type; but, it does include a `salary_interval`
which has values like `yearly`, etc. that we could use to infer it.

The API doesn't provide a way to filter by industry, but it does include an industry field.
We can use this to filter when industries are selected in the dropdown.

The API doesn't provide a years experience filter. I don't know the best way to handle this.
One option would be to append some kind of string to the `search` param to try to guess at it.
This would mean we'd have to do multiple searches to cover multiple years of experience ranges,
though. Another option would be to simply ignore it for now...

There are probably more filters we'll want to apply here. Since our goal is to bolster our job results with
ones we would feel confident are appealing to students, we might end up wanting to proactively filter out jobs
that contain certain exact phrases, such as "Executive Assistant" (sometimes you can accidentally find these
jobs when searching for a higher level position, since the description will include the higher level
position within it). We'll want to experiment with the results we get for various roles and come up with
heuristics that make sense.

*/

const filterZipRecruiterJobs = ($injector, jobs, filters) => {
    // We make use of ZipRecruiter's `refine_by_salary` filter to show
    // users results with a `salary_max` greater than or equal to the
    // upper bound of the user's current salary (or 100000 if they don't
    // have a current salary).
    // We also want to filter out jobs that may have large salary ranges,
    // but only if the lower bound of that range is less than the lower
    // bound of the user's current salary.
    const minSalaryCap = $injector.get('$rootScope').currentUser?.career_profile?.currentSalaryRange?.lower || 90000;

    return jobs.filter(job => {
        // Filter out any results with a `salary_min` less than minSalaryCap
        // and a difference of more than 40000 between `salary_min` and `salary_max`.
        // The 40000 is arbitrary, and seems to work fairly well in practice, but we
        // may want or need to tweak it going forward.
        if (job?.salary_min < minSalaryCap && job?.salary_max - job.salary_min >= 40000) {
            return false;
        }

        // Filter out anything that doesn't match defined expressions
        // for selected roles
        if (!isEmpty(filters.role)) {
            const isPotentiallyRoleMatch = find(role => {
                const expressionMatch = filterHelpers.roles[role]?.expressionMatch;
                if (expressionMatch) {
                    return expressionMatch.test(job.name) || expressionMatch.test(job.snippet);
                }
                return true; // fallback to true if we don't find an expressionMatch
            }, filters.role);

            if (!isPotentiallyRoleMatch) {
                return false;
            }
        }

        // Filter out any element that doesn't pass a single truth predicate
        // for any selected position_descriptors
        if (!isEmpty(filters.position_descriptors)) {
            const isPotentiallyDescriptorMatch = find(descriptor => {
                const truthPredicate = filterHelpers.position_descriptors[descriptor]?.truthPredicate;
                if (truthPredicate) {
                    return truthPredicate(job);
                }
                return true; // fallback to true if we don't find a truthPredicate
            }, filters.position_descriptors);

            if (!isPotentiallyDescriptorMatch) {
                return false;
            }
        }

        // Filter out everything that matches certain phrases
        if (/(restaurant|executive assistant)/gi.test(job.name)) {
            return false;
        }

        // Filter out everything from certain industries we don't intend to show
        if (/(food|retail)/gi.test(job.industry_name)) {
            return false;
        }

        // Filter out anything matching "cold call"(ing).
        // See also: https://trello.com/c/OCdoHMsN
        if (/cold call/gi.test(job.name) || /cold call/gi.test(job.snippet)) {
            return false;
        }

        // Even when using ZipRecruiter's `location` filter, some
        // results don't have either a city or location, i.e.
        // {
        //    city: "",
        //    location: "us"
        // }
        // This is no good when we're filtering for specific locations,
        // to let's remove anything that doens't have a city OR location.
        if (!job.city || !job.location) {
            return false;
        }

        return true;
    });
};

export default filterZipRecruiterJobs;
