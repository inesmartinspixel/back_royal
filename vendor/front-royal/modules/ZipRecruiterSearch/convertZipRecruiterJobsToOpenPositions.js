export default function convertZipRecruiterJobsToOpenPositions($injector, jobs) {
    const OpenPosition = $injector.get('OpenPosition');

    return jobs.map(job => {
        return OpenPosition.new({
            ...job,
            external: true,
            hiring_application: {
                company_name: job?.hiring_company?.name,
                website_url: job?.hiring_company?.url,
            },
            title: job?.name,
            job_post_url: job?.url,
            short_description: job?.snippet,

            // Round down to the nearest thousand for display
            salary: job.salary_max ? Math.floor(job.salary_max / 1000) * 1000 : undefined,
        });
    });
}
