/* eslint-disable class-methods-use-this */
import getExpandedIterableFilters from './getExpandedIterableFilters';

describe('buildUniqueApiFilterObjects', () => {
    let $injector;
    let mockErrorLogService;

    beforeEach(() => {
        class mockTranslationHelper {
            initialize() {}

            // eslint-disable-next-line lodash-fp/prefer-identity
            get(value) {
                return value;
            }
        }
        mockErrorLogService = {
            notifyInProd: jest.fn(),
        };
        $injector = {
            get: key => ({ TranslationHelper: mockTranslationHelper, ErrorLogService: mockErrorLogService }[key]),
        };
    });

    it('should expand iterable filters', () => {
        const expandedFilters = getExpandedIterableFilters($injector, {
            role: ['aerospace', 'mechanical'],
            position_descriptors: ['permanent', 'internship'],
            places: [
                {
                    administrative_area_level_1: { short: 'VA', long: 'Virginia' },
                    country: { short: 'US', long: 'United States' },
                    formatted_address: 'Harrisonburg, VA, USA',
                    locality: { short: 'Harrisonburg', long: 'Harrisonburg' },
                },
                {
                    administrative_area_level_1: { short: 'DC', long: 'District of Columbia' },
                    country: { short: 'US', long: 'United States' },
                    formatted_address: 'Washington, DC, USA',
                    locality: { short: 'Washington', long: 'Washington' },
                },
            ],
        });
        expect(expandedFilters).toEqual(
            expect.arrayContaining([
                {
                    search: '"engineer" aerospace -sales',
                    location: 'Harrisonburg, VA, USA',
                    country: 'US',
                    radius_miles: 25,
                },
                {
                    search: '"engineer" mechanical -sales',
                    location: 'Harrisonburg, VA, USA',
                    country: 'US',
                    radius_miles: 25,
                },
                {
                    search: '"engineer" aerospace -sales',
                    location: 'Washington, DC, USA',
                    country: 'US',
                    radius_miles: 25,
                },
                {
                    search: '"engineer" mechanical -sales',
                    location: 'Washington, DC, USA',
                    country: 'US',
                    radius_miles: 25,
                },
                {
                    search: '"internship" "engineer" aerospace -sales',
                    location: 'Harrisonburg, VA, USA',
                    country: 'US',
                    radius_miles: 25,
                },
                {
                    search: '"internship" "engineer" mechanical -sales',
                    location: 'Harrisonburg, VA, USA',
                    country: 'US',
                    radius_miles: 25,
                },
                {
                    search: '"internship" "engineer" aerospace -sales',
                    location: 'Washington, DC, USA',
                    country: 'US',
                    radius_miles: 25,
                },
                {
                    search: '"internship" "engineer" mechanical -sales',
                    location: 'Washington, DC, USA',
                    country: 'US',
                    radius_miles: 25,
                },
            ]),
        );
    });

    it('should work when only one iterable filter is passed in', () => {
        let expandedFilters = getExpandedIterableFilters($injector, {
            role: ['aerospace', 'mechanical'],
        });
        expect(expandedFilters).toEqual(
            expect.arrayContaining([
                {
                    search: '"engineer" aerospace -sales',
                },
                {
                    search: '"engineer" mechanical -sales',
                },
            ]),
        );

        expandedFilters = getExpandedIterableFilters($injector, {
            places: [
                {
                    administrative_area_level_1: { short: 'VA', long: 'Virginia' },
                    country: { short: 'US', long: 'United States' },
                    formatted_address: 'Harrisonburg, VA, USA',
                    locality: { short: 'Harrisonburg', long: 'Harrisonburg' },
                },
                {
                    administrative_area_level_1: { short: 'DC', long: 'District of Columbia' },
                    country: { short: 'US', long: 'United States' },
                    formatted_address: 'Washington, DC, USA',
                    locality: { short: 'Washington', long: 'Washington' },
                },
            ],
        });
        expect(expandedFilters).toEqual(
            expect.arrayContaining([
                {
                    location: 'Harrisonburg, VA, USA',
                    country: 'US',
                    radius_miles: 25,
                },
                {
                    location: 'Harrisonburg, VA, USA',
                    country: 'US',
                    radius_miles: 25,
                },
                {
                    location: 'Washington, DC, USA',
                    country: 'US',
                    radius_miles: 25,
                },
                {
                    location: 'Washington, DC, USA',
                    country: 'US',
                    radius_miles: 25,
                },
            ]),
        );

        expandedFilters = getExpandedIterableFilters($injector, {
            position_descriptors: ['internship', 'part_time'],
        });
        expect(expandedFilters).toEqual(
            expect.arrayContaining([
                {
                    search: '"internship"',
                },
                {
                    search: '"part time"',
                },
            ]),
        );
    });

    it('should notifyInProd and translate roles we cannot map to roleHelpers', () => {
        const expandedFilters = getExpandedIterableFilters($injector, {
            role: ['foo'],
        });
        expect(mockErrorLogService.notifyInProd).toHaveBeenCalledWith(
            'Could not find ZipRecruiter searchTerm for selected role',
            null,
            {
                role: 'foo',
            },
        );
        expect(expandedFilters).toEqual(
            expect.arrayContaining([
                {
                    search: 'foo',
                },
            ]),
        );
    });

    it('should not blow up if no iterable filters are passed in', () => {
        const expandedFilters = getExpandedIterableFilters($injector, {});
        expect(expandedFilters).toEqual([]);
    });
});
