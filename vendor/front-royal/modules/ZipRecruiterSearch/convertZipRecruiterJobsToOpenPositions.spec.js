import convertZipRecruiterJobsToOpenPositions from './convertZipRecruiterJobsToOpenPositions';

describe('convertZipRecruiterJobsToOpenPositions', () => {
    let $injector;
    let jobs;

    beforeEach(() => {
        $injector = {
            get: key =>
                ({
                    OpenPosition: {
                        new: options => options,
                    },
                }[key]),
        };
        jobs = [
            {
                id: 'zip_id_1',
                hiring_company: {
                    name: 'foobar company',
                    url: 'https://foobarcompany.com',
                },
                name: 'Some job',
                location: 'Some City, USA',
                salary_max: 99999,
                snippet: 'snippet',
                url: 'https://foobarcompany.com/somejob',
            },
        ];
    });

    it('should assign various OpenPosition properties', () => {
        const first = convertZipRecruiterJobsToOpenPositions($injector, jobs)[0];
        ['external', 'hiring_application', 'title', 'job_post_url', 'short_description', 'salary'].forEach(key => {
            expect(first[key]).not.toBeUndefined();
        });
    });

    it('shoudl round salary down to nearest thousand', () => {
        const first = convertZipRecruiterJobsToOpenPositions($injector, jobs)[0];
        expect(first.salary).toEqual(99000);
    });
});
