import getZipRecruiterJobs from './getZipRecruiterJobs';
import fetchZipRecruiterJobs from './fetchZipRecruiterJobs';
import filterZipRecruiterJobs from './filterZipRecruiterJobs';
import orderZipRecruiterJobs from './orderZipRecruiterJobs';
import convertZipRecruiterJobsToOpenPositions from './convertZipRecruiterJobsToOpenPositions';

let mockFetchZipRecruiterJobsReturnValue = Promise.resolve([{}]);
jest.mock('./fetchZipRecruiterJobs', () => jest.fn().mockImplementation(() => mockFetchZipRecruiterJobsReturnValue));

let mockFilterZipRecruiterJobsReturnValue = [];
jest.mock('./filterZipRecruiterJobs', () => jest.fn().mockImplementation(() => mockFilterZipRecruiterJobsReturnValue));

let mockOrderZipRecruiterJobsReturnValue = [];
jest.mock('./orderZipRecruiterJobs', () => jest.fn().mockImplementation(() => mockOrderZipRecruiterJobsReturnValue));

let mockConvertZipRecruiterJobsToOpenPositionsReturnValue = [];
jest.mock('./convertZipRecruiterJobsToOpenPositions', () =>
    jest.fn().mockImplementation(() => mockConvertZipRecruiterJobsToOpenPositionsReturnValue),
);

describe('fetchZipRecruiterJobs', () => {
    let $injector;
    let mockCountry;
    let mockZipRecruiterSearchActivated;

    beforeEach(() => {
        mockCountry = 'US';
        mockZipRecruiterSearchActivated = true;

        $injector = {
            get: key =>
                ({
                    ConfigFactory: {
                        getSync: () => {
                            return {
                                zipRecruiterSearchActivated: () => mockZipRecruiterSearchActivated,
                            };
                        },
                    },
                    $rootScope: {
                        currentUser: {
                            country: mockCountry,
                        },
                    },
                }[key]),
        };
    });

    it('should do nothing if !zipRecruiterSearchActivated', async () => {
        mockZipRecruiterSearchActivated = false;
        await getZipRecruiterJobs($injector, {});
        expect(fetchZipRecruiterJobs).not.toHaveBeenCalled();
    });

    it('should call fetchZipRecruiterJobs', async () => {
        await getZipRecruiterJobs($injector, {});
        expect(fetchZipRecruiterJobs).toHaveBeenCalled();
    });

    it('should call filterZipRecruiterJobs with unique jobs from fetchZipRecruiterJobs response', async () => {
        mockFetchZipRecruiterJobsReturnValue = Promise.resolve([
            {
                data: {
                    jobs: [{ id: '1' }, { id: '2' }],
                },
            },
            {
                data: {
                    jobs: [{ id: '2' }, { id: '3' }],
                },
            },
            {
                data: {
                    jobs: [{ id: '3' }],
                },
            },
        ]);
        await getZipRecruiterJobs($injector, {});
        expect(filterZipRecruiterJobs).toHaveBeenCalledWith($injector, [{ id: '1' }, { id: '2' }, { id: '3' }], {});
    });

    it('should call orderZipRecruiterJobs with return value from filterZipRecruiterJobs', async () => {
        mockFilterZipRecruiterJobsReturnValue = 'mockFilterZipRecruiterJobsReturnValue';
        await getZipRecruiterJobs($injector, {});
        expect(orderZipRecruiterJobs).toHaveBeenCalledWith(mockFilterZipRecruiterJobsReturnValue);
    });

    it('should call convertZipRecruiterJobsToOpenPositions with return value from orderZipRecruiterJobs', async () => {
        mockOrderZipRecruiterJobsReturnValue = 'mockOrderZipRecruiterJobsReturnValue';
        await getZipRecruiterJobs($injector, {});
        expect(convertZipRecruiterJobsToOpenPositions).toHaveBeenCalledWith(
            $injector,
            mockOrderZipRecruiterJobsReturnValue,
        );
    });

    it('should return convertZipRecruiterJobsToOpenPositions return value', async () => {
        mockConvertZipRecruiterJobsToOpenPositionsReturnValue = 'mockConvertZipRecruiterJobsToOpenPositionsReturnValue';
        const returned = await getZipRecruiterJobs($injector, {});
        expect(returned).toEqual('mockConvertZipRecruiterJobsToOpenPositionsReturnValue');
    });
});
