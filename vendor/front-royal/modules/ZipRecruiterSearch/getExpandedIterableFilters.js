import { isEmpty } from 'lodash/fp';
import filterHelpers from './filterHelpers';

/*
  Builds an array of unique filter objects.
 
  Example input:
    {
      role: [
          'Aerospace Engineering',
          'Mechanical Engineering'
      ],
      places: [
         {
            administrative_area_level_1: {short: "VA", long: "Virginia"},
            country: {short: "US", long: "United States"},
            formatted_address: "Harrisonburg, VA, USA",
            locality: {short: "Harrisonburg", long: "Harrisonburg"},
            ...
        },
        {
            administrative_area_level_1: {short: "DC", long: "District of Columbia"},
            country: {short: "US", long: "United States"},
            formatted_address: "Washington, DC, USA",
            locality: {short: "Washington", long: "Washington"},
            ...
        }
      ]
    }
 
   Example output:
    [
      {
          search: '"engineer" aerospace',
          location: 'Harrisonburg, VA',
          country: 'US',
          radius_miles: 25
      },
      {
          search: '"engineer" aerospace',
          location: 'Washington, DC',
          country: 'US',
          radius_miles: 25
      },
      {
          search: '"engineer" mechanical',
          location: 'Harrisonburg, VA',
          country: 'US',
          radius_miles: 25
      },
      {
          search: '"engineer" mechanical',
          location: 'Washington, DC',
          country: 'US',
          radius_miles: 25
      }
    ]
 */

const ITERABLE_FILTER_MAPPINGS = [
    {
        internalPropName: 'role',
        transformedFilterObject: (value, $injector) => {
            // The ZipSearch API expects human readable search terms. Therefore,
            // we need to get the human readable description for any role filers.
            // We have defined a set of helpers to get the most effective search
            // terms, but fall back to translating the value if we can't find
            // a term for the value being search for.
            const filterObject = {};
            const TranslationHelper = $injector.get('TranslationHelper');
            const roleTranslationHelper = new TranslationHelper('careers.field_options');

            let transformed;
            if (filterHelpers.roles[value]?.searchTerm) {
                transformed = filterHelpers.roles[value]?.searchTerm;
            } else {
                $injector
                    .get('ErrorLogService')
                    .notifyInProd('Could not find ZipRecruiter searchTerm for selected role', null, {
                        role: value,
                    });

                // NOTE: the API breaks down for non-EN locales, so we need to force
                // english for any fallback translations.
                transformed = roleTranslationHelper.get(value, undefined, undefined, 'fetchZipRecruiterJobs', 'en');
            }

            if (transformed) {
                filterObject.search = transformed;
            }
            return filterObject;
        },
    },
    {
        internalPropName: 'industry',
        transformedFilterObject: value => {
            const filterObject = {};
            const transformed = filterHelpers.industry[value]?.searchTerm;
            if (transformed) {
                filterObject.search = transformed;
            }
            return filterObject;
        },
    },
    {
        internalPropName: 'position_descriptors',
        transformedFilterObject: value => {
            const filterObject = {};
            const transformed = filterHelpers.position_descriptors[value]?.searchTerm;
            if (transformed) {
                filterObject.search = transformed;
            }
            return filterObject;
        },
    },
    {
        internalPropName: 'places',
        // The ZipSearch API expects human readable locations, i.e. Nashville, TN.
        // The location input that corresponds to these filters in the client only
        // allows the user to select cities, and the `formatted_address` returned by
        // the Google Places API seems to be a pretty reliable way to get a human
        // readable representation of the selected city, without having to construct
        // said representation from other properties in that object.
        // NOTE: `location` and `radius_miles` are both passed to ZipRecruiter's API
        // while `country` is not. However, we need `country` to determine which API
        // key to use.
        transformedFilterObject: value => {
            return {
                location: value?.formatted_address,
                radius_miles: 25,
                country: value?.country?.short,
            };
        },
    },
];

const getExpandedIterableFilters = ($injector, filters) => {
    let expandedFilters = [];

    ITERABLE_FILTER_MAPPINGS.forEach(mapping => {
        const values = filters[mapping.internalPropName] || [];
        const tmpExpandedFilters = [];

        values.forEach(value => {
            const filterObject = mapping.transformedFilterObject(value, $injector, expandedFilters);

            if (filterObject) {
                // If we already have expandedFilters, we need to create a new
                // unique object for each existing filter + value combination.
                if (!isEmpty(expandedFilters)) {
                    expandedFilters.forEach(expandedFilter => {
                        const combinedFilter = { ...expandedFilter, ...filterObject };

                        // Multiple internal properties map to the `search`
                        // paramater. If the `search` property already exists
                        // on the expanded filter, append the new value.
                        if (expandedFilter.search && filterObject.search) {
                            combinedFilter.search = filterObject.search.concat(` ${expandedFilter.search}`);
                        }

                        tmpExpandedFilters.push(combinedFilter);
                    });
                } else {
                    tmpExpandedFilters.push(filterObject);
                }
            }
        });

        // If we constructed new expanded filters, override
        // the existing expanded filters.
        if (!isEmpty(tmpExpandedFilters)) {
            expandedFilters = tmpExpandedFilters;
        }
    });

    return expandedFilters;
};

export default getExpandedIterableFilters;
