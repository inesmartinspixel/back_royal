import buildUniqueApiFilterObjects from './buildUniqueApiFilterObjects';
import getExpandedIterableFilters from './getExpandedIterableFilters';

let mockGetExpandedIterableFiltersReturnValue = [{}];
jest.mock('./getExpandedIterableFilters', () =>
    jest.fn().mockImplementation(() => mockGetExpandedIterableFiltersReturnValue),
);

describe('buildUniqueApiFilterObjects', () => {
    let $injector;
    let currentUser;

    beforeEach(() => {
        currentUser = {
            careerProfilePrimaryAreasOfInterest: ['foo', 'bar', 'foobar'],
            career_profile: {
                currentSalaryRange: {
                    lower: 90000,
                    upper: 99999,
                },
            },
        };
        $injector = {
            get: key => ({ $rootScope: { currentUser } }[key]),
        };
        getExpandedIterableFilters.mockClear();
    });

    describe('role', () => {
        it('should add primary_areas_of_interest to filters before expansion if no role filters passed in', () => {
            buildUniqueApiFilterObjects($injector, {});
            expect(getExpandedIterableFilters).toHaveBeenCalledTimes(1);
            expect(getExpandedIterableFilters).toHaveBeenCalledWith($injector, {
                role: currentUser.careerProfilePrimaryAreasOfInterest,
            });
        });
        it('should default to `general_management` if no roles or primary_areas_of_interest', () => {
            currentUser.careerProfilePrimaryAreasOfInterest = ['other'];
            buildUniqueApiFilterObjects($injector, {});
            expect(getExpandedIterableFilters).toHaveBeenCalledTimes(1);
            expect(getExpandedIterableFilters).toHaveBeenCalledWith($injector, {
                role: ['general_management'],
            });
        });
    });

    it('should return one object if getExpandedIterableFilters returns an empty array', () => {
        mockGetExpandedIterableFiltersReturnValue = [];
        const filters = buildUniqueApiFilterObjects($injector, {});
        expect(filters.length).toBe(1);
        expect(typeof filters[0]).toBe('object');
    });

    it('should set refine_by_salary to career_profile.currentSalaryRange.upper', () => {
        const filters = buildUniqueApiFilterObjects($injector, {});
        filters.forEach(filter => {
            expect(filter.refine_by_salary).toEqual(99999);
        });
    });

    it('should base jobs_per_page on MAX_RESPONSE_LENGTH and expanded filter length', () => {
        mockGetExpandedIterableFiltersReturnValue = [{}, {}, {}, {}, {}];
        let filters = buildUniqueApiFilterObjects($injector, {});
        filters.forEach(filter => {
            expect(filter.jobs_per_page).toEqual(100);
        });

        mockGetExpandedIterableFiltersReturnValue = [{}, {}, {}];
        filters = buildUniqueApiFilterObjects($injector, {});
        filters.forEach(filter => {
            expect(filter.jobs_per_page).toEqual(166);
        });
    });
});
