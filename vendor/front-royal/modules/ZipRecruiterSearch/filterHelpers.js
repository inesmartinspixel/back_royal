import { includes } from 'lodash/fp';

/*
    We setup the actual search terms we pass to the ZipSearch API in this file.
    Here is a note from ZipRecruiter regarding the `search` api parameter (undocumented):

    ----

    I believe the API will accept multiple search terms separated by commas, but that won't produce the best results. 
    You'd be better off using boolean logic. The Search API supports AND and NOT operators.
    
    For example:

    - If you pass us “Sales” “Medical” we take that as an AND relationship and return the most relevant jobs with both 
    Sales AND Medical, though it is not a requirement.

    - If you pass us “Sales Medical” with the entire phrase in quotations, we look for an exact match to the phrase 
    ‘sales medical’.

    - If you pass us Sales Medical without use of quotations, we take that as a soft AND relationship. Soft AND means 
    that results with all words in the query are preferred, but depending on the query, missing words are sometimes 
    allowed (in particular, queries with 4+ words). We do the soft-AND rather than an OR because an AND is usually 
    more relevant. Having dozens of OR terms is usually going down the wrong path.

    - If you pass us “Sales” -Medical we take that as a NOT relationship and return jobs that include Sales but 
    NOT Medical. Please note that if you pass us "Sales -Medical" in quotations, we look for an exact match to 
    the phrase ‘sales -medical’.
*/

export default {
    // On the featured positions page, we allow users to filter by roles. Some of
    // our internal roles are a bit too generic to pass to ZipRecruiter and receive
    // anything meaningful in the response. Additionally, we perform client-side
    // filtering of ZipRecruiter results that we don't think are particularly enticing
    // or just outright wrong. This helper aids in both of those ventures by providing
    // search terms and matching expressions for all known internal roles.
    //
    // NOTE: for top-level roles, we include all of the children in the expression matcher.
    roles: {
        // Top level
        // "Business Analysis / Intelligence"
        data_analysis: {
            searchTerm: '"business analyst"',
            expressionMatch: /business.*(analyst|analysis)/gi,
        },
        // Top level
        // "Management Consulting / Strategy"
        consultant: {
            searchTerm: '"management consultant"',
            expressionMatch: /management.*(consultant|strategist)/gi,
        },
        // Top level
        // "Data Science"
        data_science: {
            searchTerm: '"data scientist"',
            expressionMatch: /data.*(scientist|engineer|analytics)/gi,
        },
        // Top level (with children)
        // "Design"
        design: {
            searchTerm: '"designer"',
            expressionMatch: /(ux|ui|architecture|art|graphic design)/gi,
        },
        // "UX / UI"
        ux_ui: {
            searchTerm: '"ux" "ui" "designer" -engineer',
            expressionMatch: /(ux|ui)/gi,
        },
        // "Architecture / Art"
        art_creative: {
            searchTerm: '"art" -sales -state', // Attempting to remove "state of the art" with -state
            expressionMatch: /(architecture|art)/gi,
        },
        // "Graphic Design"
        graphic_design: {
            searchTerm: '"graphic designer"',
            expressionMatch: /graphic design/gi,
        },
        // Top level (with children)
        // "Engineering"
        engineering: {
            searchTerm: '"engineer" -sales',
            expressionMatch: /(software|qa|devops|electrical|hardware|aerospace|biomedical|chemical|civil|mechanical).*engineer/gi,
        },
        // "Software Development (front-end)"
        software_dev_front_end: {
            searchTerm: '"software" "engineer" frontend front-end -test -automation -sales',
            expressionMatch: /(engineer|develop|front.*end)/gi,
        },
        // "Software Development (back-end)"
        software_dev_back_end: {
            searchTerm: '"software" "engineer" backend back-end -test -automation -sales',
            expressionMatch: /(engineer|develop|back.*end)/gi,
        },
        // "Software Development (full-stack)"
        software_dev_full_stack: {
            searchTerm: '"software" "engineer" fullstack full-stack -test -automation -sales',
            expressionMatch: /(engineer|develop|full.*stack)/gi,
        },
        // "QA"
        qa: {
            searchTerm: '"qa" "engineer" test automation -sales',
            expressionMatch: /(qa|test).*(engineer|develop)/gi,
        },
        // "DevOps"
        devops: {
            searchTerm: '"devops" -sales',
            expressionMatch: /(devops|automation).*(engineer|develop)/gi,
        },
        // "Electrical Engineering"
        hardware: {
            searchTerm: '"engineer" electrical electronic -sales',
            expressionMatch: /(hardware|electrical|electronic).*engineer/gi,
        },
        // "Aerospace Engineering"
        aerospace: {
            searchTerm: '"engineer" aerospace -sales',
            expressionMatch: /aerospace.*engineer/gi,
        },
        // "Biomedical Engineering"
        biomedical: {
            searchTerm: '"engineer" biomedical -sales',
            expressionMatch: /biomedical.*engineer/gi,
        },
        // "Chemical Engineering"
        chemical: {
            searchTerm: '"engineer" chemical -sales',
            expressionMatch: /chemical.*engineer/gi,
        },
        // "Civil Engineering"
        civil: {
            searchTerm: '"engineer" civil construction structure structural -sales',
            expressionMatch: /(civil|construction|structur).*engineer/gi,
        },
        // "Mechanical Engineering"
        mechanical: {
            searchTerm: '"engineer" mechanical -sales',
            expressionMatch: /(hardware|mechanical).*engineer/gi,
        },
        // Top level (with children)
        // "Finance"
        finance: {
            searchTerm: '"finance"',
            expressionMatch: /(corporate banking|investing|trading|financial planning|accounting|wealth management|risk audit)/gi,
        },
        // "Investment Banking"
        corporate_banking: {
            searchTerm: '"finance" "investment" analyst banker -sales -retail',
            expressionMatch: /(financ|invest|analyst|banker)/gi,
        },
        // "Investing"
        investment: {
            searchTerm: '"finance" "investment" manager officer researcher -sales -retail',
            expressionMatch: /(finance|invest|manager|officer|researcher)/gi,
        },
        // "Trading"
        trading: {
            searchTerm: '"trade" stocks options trader',
            expressionMatch: /(trading|trader)/gi,
        },
        // "FP&A / Accounting / Control"
        fpa_accounting_audit: {
            searchTerm: '"accountant" cpa',
            expressionMatch: /(financial planning|cpa|accountant|accounting)/gi,
        },
        // "Wealth Management"
        wealth_management: {
            searchTerm: '"management" wealth capital fund -retail',
            expressionMatch: /(wealth|capital|fund).*management/gi,
        },
        // "Risk / Audit"
        risk_audit: {
            searchTerm: '"risk" "audit" finance',
            expressionMatch: /(risk|audit)/gi,
        },
        // Top level
        information_technology: {
            searchTerm: '"information technology"',
            expressionMatch: /information technology/gi,
        },
        // Top level (with children)
        general_management: {
            searchTerm: '"manager" project operations hr talen legal admin logistics supply -retail -food -assistant',
            expressionMatch: /(senior manage|project manage|operations|human resources|(?:^|\W)hr(?:$|\W)|talent acquisition|legal|administrative|logistics|supply chain)/gi,
        },
        // "Senior Management"
        senior_management: {
            searchTerm: 'ceo cfo cto vice president chief officer director -retail -food',
            expressionMatch: /(ceo|cfo|cto|vice president|chief officer|director)/gi,
        },
        // "Operations / Project Management"
        operations: {
            searchTerm: '"project manager"',
            expressionMatch: /(operations|project manage)/gi,
        },
        // "Human Resources / Talent Acquisition"
        human_resources: {
            searchTerm: '"human resources"',
            expressionMatch: /(human resources|(?:^|\W)hr(?:$|\W)|talent acquisition)/gi,
        },
        // "Legal"
        legal: {
            searchTerm: '"legal" attorney partner counsel',
            expressionMatch: /(legal|attorney|counsel)/gi,
        },
        // "Administrative"
        administrative: {
            searchTerm: '"administrative" -assistant',
            expressionMatch: /administrat(or|ive|ion)/gi,
        },
        // "Logistics / Supply Chain"
        logistics_supply_chain: {
            searchTerm: 'logistics supply',
            expressionMatch: /(logistics|supply chain)/gi,
        },
        // Top level
        // "Marketing"
        marketing: {
            searchTerm: '"marketing"',
            expressionMatch: /marketing/gi,
        },
        // Top level
        // "Medical Services"
        medical_services: {
            searchTerm: '"medical" nurse physician therapist pharmacist -sales',
            expressionMatch: /(medical services|health services|nurse|physician|therapist|pharmacist)/gi,
        },
        // Top level
        // "Product Management"
        product_management: {
            searchTerm: '"product manager"',
            expressionMatch: /product manage/gi,
        },
        // Top level
        // "Research"
        research: {
            searchTerm: '"research"',
            expressionMatch: /research/gi,
        },
        // Top level (with children)
        // "Sales / Business Development"
        sales: {
            searchTerm: '"sales" executive manager success sdr inside outside -retail -food',
            expressionMatch: /(account executive|account manage|customer success|sales engineer|sales develop|sdr|inside sales|outside sales)/gi,
        },
        // "Account Executive"
        account_executive: {
            searchTerm: '"account executive"',
            expressionMatch: /account executive/gi,
        },
        // "Account Manager"
        account_manager: {
            searchTerm: '"account manager"',
            expressionMatch: /account manage/gi,
        },
        // "Customer Success"
        customer_success: {
            searchTerm: '"customer success"',
            expressionMatch: /customer success/gi,
        },
        // "Sales Engineer"
        sales_engineer: {
            searchTerm: '"sales engineer"',
            expressionMatch: /sales engineer/gi,
        },
        // "Sales Development Representative"
        sales_development_rep: {
            searchTerm: '"sales" development sdr inside',
            expressionMatch: /(sales develop|sdr|inside sales)/gi,
        },
        // Top level
        // "Teaching / Education"
        teaching_education: {
            searchTerm: '"teacher" education',
            expression: /(teach|educat)/gi,
        },
        // Top level
        // "Communications / Writing"
        writing: {
            searchTerm: '"writer"',
            expression: /(writing|journali)/gi,
        },
    },
    // We also allow users to filter by specific descriptors. ZipRecruiter doesn't
    // have an analagous API feature, but we can append relevant search terms like
    // "contractor" and/or "intern" to the `search parameter`. Similarly, we can
    // use an `expressionMatch` to validate the results we get, to a degree.
    position_descriptors: {
        permanent: {
            searchTerm: '',
            truthPredicate: job => job?.salary_interval === 'yearly',
        },
        part_time: {
            searchTerm: '"part time"',
            truthPredicate: job => includes(job?.salary_interval, ['hourly', 'weekly']),
        },
        contract: {
            searchTerm: '"contractor"',
            truthPredicate: job => /contractor/gi.test(job?.snippet) || /contractor/gi.test(job?.name),
        },
        internship: {
            searchTerm: '"internship"',
            truthPredicate: job =>
                /(intern|internship)/gi.test(job?.snippet) || /(intern|internship)/gi.test(job?.name),
        },
    },
    // Additionally, we allow users to filter by industry. Again, ZipRecruiter doesn't
    // give us a way to do the same with their API. However, we can append a relevant
    // search terms like "healthcare" and/or "nonprofit" to the `search` parameter.
    industry: {
        consulting: {
            searchTerm: '"consulting"',
        },
        consumer_products: {
            searchTerm: '"consumer products"',
        },
        media: {
            searchTerm: '"media"',
        },
        financial: {
            searchTerm: '"finance"',
        },
        health_care: {
            searchTerm: '"healthcare"',
        },
        manufacturing: {
            searchTerm: '"manufacturing"',
        },
        nonprofit: {
            searchTerm: '"nonprofit"',
        },
        retail: {
            searchTerm: '"retail"',
        },
        real_estate: {
            searchTerm: '"real estate"',
        },
        technology: {
            searchTerm: '"technology"',
        },
        education: {
            searchTerm: '"education"',
        },
        energy: {
            searchTerm: '"energy"',
        },
        government: {
            searchTerm: '"government"',
        },
        military: {
            searchTerm: '"military"',
        },
    },
};
