// For information on the ZipSearch REST API, see the API reference here:
// https://pedago.atlassian.net/wiki/spaces/TECH/pages/848396297/ZipRecruiter

import axios from 'axios';
import buildUniqueApiFilterObjects from './buildUniqueApiFilterObjects';

const fetchZipRecruiterJobs = ($injector, _filters) => {
    const config = $injector.get('ConfigFactory').getSync();
    const currentUser = $injector.get('$rootScope').currentUser;
    const promises = [];

    // While our OpenPositions API supports iterable filters,
    // ZipRecruiter's API does not. We will need to issue an
    // API request for each unique combination of filters.
    buildUniqueApiFilterObjects($injector, _filters).forEach(filters => {
        let api_key;
        if (filters?.country) {
            // If we have selected a country in the filter, we should only
            // try to use an API key for that specific country. If we don't
            // find one, we won't make a request.
            api_key = config.zipRecruiterApiKeyForCountry(filters.country);
            delete filters.country;
        } else {
            // If we don't have a selected country, we should try to show
            // users jobs for their specific country. If we don't have an
            // api key for their country of residence, we will show them
            // US results by default.
            api_key = config.zipRecruiterApiKeyForCountry(currentUser?.country, true);
        }

        if (api_key) {
            promises.push(
                axios({
                    method: 'get',
                    url: config.ziprecruiter_search_url,
                    params: {
                        api_key,
                        ...filters,
                    },
                    headers: {
                        'Content-Type': 'application/json',
                    },
                }),
            );
        }
    });

    return Promise.all(promises);
};

export default fetchZipRecruiterJobs;
