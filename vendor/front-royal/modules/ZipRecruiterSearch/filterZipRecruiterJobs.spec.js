import { map } from 'lodash/fp';
import filterZipRecruiterJobs from './filterZipRecruiterJobs';

describe('filterZipRecruiterJobs', () => {
    let $injector;
    let jobs;

    beforeEach(() => {
        $injector = {
            get: key =>
                ({
                    $rootScope: {
                        currentUser: {
                            career_profile: {
                                currentSalaryRange: {
                                    lower: 90000,
                                    upper: 99999,
                                },
                            },
                        },
                    },
                }[key]),
        };
        jobs = [
            {
                id: 'zip_id_1',
                hiring_company: {
                    name: 'foobar company',
                    url: 'https://foobarcompany.com',
                },
                name: 'Some job',
                city: 'Some City',
                location: 'Some City, USA',
                salary_min: 99999,
                salary_max: 99999,
                salary_interval: 'yearly',
                snippet: '',
                salary_source: 'provided',
                url: 'https://foobarcompany.com/somejob',
            },
            {
                id: 'zip_id_2',
                hiring_company: {
                    name: 'foobar company',
                    url: 'https://foobarcompany.com',
                },
                name: 'Some job',
                city: 'Some City',
                location: 'Some City, USA',
                salary_min: 99999,
                salary_max: 99999,
                salary_interval: 'yearly',
                snippet: '',
                salary_source: 'parsed',
                url: 'https://foobarcompany.com/somejob',
            },
            {
                id: 'zip_id_3',
                hiring_company: {
                    name: 'foobar company',
                    url: 'https://foobarcompany.com',
                },
                name: 'Some job',
                city: 'Some City',
                location: 'Some City, USA',
                has_non_zr_url: true,
                salary_min: 99999,
                salary_max: 99999,
                salary_interval: 'yearly',
                snippet: '',
                salary_source: 'predicted',
                url: 'https://foobarcompany.com/somejob',
            },
            {
                id: 'zip_id_4',
                hiring_company: {
                    name: 'foobar company',
                    url: 'https://foobarcompany.com',
                },
                name: 'Some job',
                city: 'Some City',
                location: 'Some City, USA',
                has_non_zr_url: true,
                salary_min: 99999,
                salary_max: 99999,
                salary_interval: 'yearly',
                snippet: '',
                salary_source: 'provided',
                url: 'https://foobarcompany.com/somejob',
            },
            {
                id: 'zip_id_5',
                hiring_company: {
                    name: 'foobar company',
                    url: 'https://foobarcompany.com',
                },
                name: 'Some job',
                city: 'Some City',
                location: 'Some City, USA',
                has_non_zr_url: false,
                salary_min: 99999,
                salary_max: 99999,
                salary_interval: 'yearly',
                snippet: '',
                salary_source: 'foobar',
                url: 'https://foobarcompany.com/somejob',
            },
        ];
    });

    describe('filtering', () => {
        it('should filter out jobs with large salary ranges when salary_min is less than refineBySalary', () => {
            const salaries = [79999, 89999, 99999, 109999, 119999];

            for (let i = 0; i < jobs.length; i++) {
                const job = jobs[i];
                job.salary_min = salaries[i];
                job.salary_max = job.salary_min + 40000;
            }

            const filtered = map('id')(filterZipRecruiterJobs($injector, jobs, {}));
            expect(filtered).not.toEqual(expect.arrayContaining([jobs[0].id, jobs[1].id]));
            expect(filtered).toEqual(expect.arrayContaining([jobs[2].id, jobs[3].id, jobs[4].id]));
        });

        it('should filter out jobs that do not match position_descriptors', () => {
            const salaryIntervals = ['yearly', 'weekly', 'hourly', 'minutely', 'secondly'];

            for (let i = 0; i < jobs.length; i++) {
                const job = jobs[i];
                job.salary_interval = salaryIntervals[i];
            }

            let filtered = map('id')(
                filterZipRecruiterJobs($injector, jobs, { position_descriptors: ['permanent', 'part_time'] }),
            );
            expect(filtered).toEqual(expect.arrayContaining([jobs[0].id, jobs[1].id, jobs[2].id]));
            expect(filtered).not.toEqual(expect.arrayContaining([[jobs[3].id, jobs[4].id]]));

            jobs[1].snippet = 'something something contractor something something';
            filtered = map('id')(filterZipRecruiterJobs($injector, jobs, { position_descriptors: ['contract'] }));
            expect(filtered).toEqual(expect.arrayContaining([jobs[1].id]));
            expect(filtered).not.toEqual(expect.arrayContaining([jobs[0].id, jobs[2].id, jobs[3].id, jobs[4].id]));
        });

        it('should filter out jobs that match certain phrases', () => {
            jobs[0].name = 'Restaurant Manager';
            jobs[1].name = 'Executive Assistant';
            jobs[2].name = 'Something something cold calling somethign something darkside';

            const filtered = map('id')(filterZipRecruiterJobs($injector, jobs, {}));
            expect(filtered).not.toEqual(expect.arrayContaining([jobs[0].id, jobs[1].id], jobs[2].id));
            expect(filtered).toEqual(expect.arrayContaining([jobs[3].id, jobs[4].id]));
        });

        it('should filter out jobs in specific industries', () => {
            jobs[0].industry_name = 'Food';
            jobs[1].industry_name = 'Retail';

            const filtered = map('id')(filterZipRecruiterJobs($injector, jobs, {}));
            expect(filtered).not.toEqual(expect.arrayContaining([jobs[0].id, jobs[1].id]));
            expect(filtered).toEqual(expect.arrayContaining([jobs[2].id, jobs[3].id, jobs[4].id]));
        });

        it('should filter out jobs that do not have a city or location', () => {
            jobs[0].city = undefined;
            jobs[1].location = undefined;

            const filtered = map('id')(filterZipRecruiterJobs($injector, jobs, {}));
            expect(filtered).not.toEqual(expect.arrayContaining([jobs[0].id, jobs[1].id]));
            expect(filtered).toEqual(expect.arrayContaining([jobs[2].id, jobs[3].id, jobs[4].id]));
        });

        it('should filter out jobs that do not match defined role expressions', () => {
            const snippets = ['aerospace engineering...', 'biomedical engineering...', 'foo', 'bar', 'baz'];
            const names = ['foo', 'bar', 'aerospace engineer', 'baz', 'qux'];

            for (let i = 0; i < jobs.length; i++) {
                const job = jobs[i];
                job.snippet = snippets[i];
                job.name = names[i];
            }

            const filtered = map('id')(filterZipRecruiterJobs($injector, jobs, { role: ['aerospace', 'biomedical'] }));
            expect(filtered).toEqual(expect.arrayContaining([jobs[0].id, jobs[1].id, jobs[2].id]));
            expect(filtered).not.toEqual(expect.arrayContaining([jobs[3].id, jobs[4].id]));
        });
    });
});
