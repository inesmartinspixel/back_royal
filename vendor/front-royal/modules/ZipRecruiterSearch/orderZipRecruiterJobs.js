import { orderBy } from 'lodash/fp';

/*

Description of ordering from https://trello.com/c/5cH9Mhwu:

There's also an open question about the quality of jobs that are posted on ZipRecruiter directly vs.
externally (i.e.: whether the link will lead to a ZipRecruiter apply form or an apply form an  external
site. This is indicated by the presence of the `has_zipapply` attribute, as far as I can tell - when it's
false, it means that following the `url` will lead to an external site. It might also be related to the `has_non_zr_url`
attribute - maybe it's possible to not have zip apply and still be on zip recruiter's site? We should experiment to
see if jobs hosted external to zip recruiter tend to be higher quality; I wouldn't be surprised if they are, since it
means the poster is a company that's mature enough to be running their own ATS / hosting jobs on their own site. If so,
we might want to prioritize them.

I've also noticed that `salary_source` is sometimes `predicted` and sometimes `provided`. It might be the case that we
should prioritize those that are `provided`, since they're likely to be much more accurate.

*/

export default function orderZipRecruiterJobs(jobs) {
    const salarySourceWeightMappings = {
        provided: 3,
        parsed: 2,
        predicted: 1,
    };

    // As described above, we want to prioritize jobs that:
    // 1. Are hosted by ZipRecruiter instead of a 3rd part
    // 2. Have a `salary_source` of "provided" or "parsed" vs. "predicted" (more accurate)
    // 3. Have a higher salary
    return orderBy(
        ['has_non_zr_url', job => salarySourceWeightMappings[job?.salary_source] || 0, 'salary_max'],
        ['asc', 'desc', 'desc'],
        jobs,
    );
}
