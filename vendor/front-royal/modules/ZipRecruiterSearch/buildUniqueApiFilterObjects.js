import { isEmpty, without } from 'lodash/fp';
import getExpandedIterableFilters from './getExpandedIterableFilters';

const MAX_RESPONSE_LENGTH = 500;

const buildUniqueApiFilterObjects = ($injector, filters) => {
    const currentUser = $injector.get('$rootScope').currentUser;

    // The ZipSearch API is kind of all over the place if you don't
    // provide search terms. If we don't have specific role(s) in the
    // filters, we should try to use `primary_areas_of_interest` from the
    // user's career profile.
    // Note that `primary_areas_of_interest` is not nullable so at the very
    // least, we can rely on it being an empty array.
    filters.role = !isEmpty(filters.role) ? filters.role : currentUser?.careerProfilePrimaryAreasOfInterest;

    // Internally, we support an "other" role in both filters and
    // `primary_areas_of_interest`. We don't want to pass that to
    // ZipRecruiter because the reuslts are all over the place.
    filters.role = without(['other'], filters.role);

    // In the off chance that we ended up with an empty array here,
    // we should default to `general_management` because the ZipRecruiter
    // API doesn't return desirable results without a search term.
    if (isEmpty(filters.role)) {
        filters.role = ['general_management'];
    }

    let filterObjects = getExpandedIterableFilters($injector, filters);

    // If no filters are passed in, then we should generate
    // one filter object with the default filters assigned below
    // so that we make one request to zip recruiter with those
    // default filters.
    if (isEmpty(filterObjects)) {
        filterObjects = [{}];
    }

    filterObjects = filterObjects.map(filterObject => {
        return {
            ...filterObject,

            // If the user has selected a salary range on their career profile,
            // filter for jobs with a salary greater than the range's max. Otherwise,
            // just default to 100000.
            refine_by_salary: currentUser?.career_profile?.currentSalaryRange?.upper || 100000,

            // When we have multiple requests going out, it makes sense to limit
            // the amount of results each request can potentially return.
            jobs_per_page: Math.floor(MAX_RESPONSE_LENGTH / filterObjects.length),
        };
    });

    return filterObjects;
};

export default buildUniqueApiFilterObjects;
