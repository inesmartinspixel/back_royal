import { uniqBy, flatten } from 'lodash/fp';
import fetchZipRecruiterJobs from './fetchZipRecruiterJobs';
import filterZipRecruiterJobs from './filterZipRecruiterJobs';
import orderZipRecruiterJobs from './orderZipRecruiterJobs';
import convertZipRecruiterJobsToOpenPositions from './convertZipRecruiterJobsToOpenPositions';

export default async function getZipRecruiterJobs($injector, filters) {
    const config = $injector.get('ConfigFactory').getSync();

    if (config.zipRecruiterSearchActivated()) {
        const responses = await fetchZipRecruiterJobs($injector, filters);
        const allJobs = uniqBy('id')(flatten(responses.map(response => response.data?.jobs || [])));
        const filteredJobs = filterZipRecruiterJobs($injector, allJobs, filters);
        const orderedFilteredJobs = orderZipRecruiterJobs(filteredJobs);
        return convertZipRecruiterJobsToOpenPositions($injector, orderedFilteredJobs);
    }

    return Promise.resolve([]);
}
