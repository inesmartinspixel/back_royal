import React, { useContext, useEffect, useState } from 'react';
import { flow, filter, sortBy, map, compact, keyBy } from 'lodash/fp';
import { Select, SubText } from 'FrontRoyalMaterialUiForm';
import AngularContext from 'AngularContext';
import Chip from '@material-ui/core/Chip';
import ReactIf from 'ReactIf';
import { makeStyles } from '@material-ui/core/styles';

/* Use formControl spacing so the left side of
the chips are aligned with the input */
const useStyles = makeStyles(theme => ({
    chipWrapper: theme.formControlSpacing(),
}));

const baseOptions = [
    {
        label: 'Current Degree Students',
        value: 'visible_to_current_degree_students',
    },
    {
        label: 'Graduated Degree Students',
        value: 'visible_to_graduated_degree_students',
    },
    {
        label: 'Non-Degree Students',
        value: 'visible_to_non_degree_users',
    },
];

const renderValueLabelsMap = {
    visible_to_current_degree_students: 'Current',
    visible_to_graduated_degree_students: 'Graduated',
    visible_to_non_degree_users: 'Non-Degree',
};

function renderValue(selected) {
    const formatted = [];

    Object.keys(renderValueLabelsMap).forEach(col => {
        if (selected.includes(col)) {
            formatted.push(renderValueLabelsMap[col]);
        }
    });

    // Any other values in the array are cohort ids
    const cohortCount = selected.length - formatted.length;
    const plus = formatted.length > 0 ? '+' : '';
    if (cohortCount === 1) {
        formatted.push(`${plus}1 cohort`);
    } else if (cohortCount > 2) {
        formatted.push(`${plus}${cohortCount} cohorts`);
    }

    return formatted.join(', ');
}

export default function VisibleToInput(props) {
    const $injector = useContext(AngularContext);
    const Cohort = $injector.get('Cohort');
    const [options, setOptions] = useState(baseOptions);
    const [cohortsMap, setCohortsMap] = useState();
    const [selectedCohorts, setSelectedCohorts] = useState([]);
    const {
        field: { value },
    } = props;

    const classes = useStyles();

    useEffect(() => {
        let canceled = false;
        Cohort.index({
            fields: ['BASIC_FIELDS'],
        }).then(response => {
            if (canceled) {
                return;
            }

            // Goal: only include programs with network access whose network is not isolated
            const _cohorts = flow(
                filter(c => c.supportsNetworkAccess && !c.isolated_network),
                sortBy(c => -`${c.program_type === 'emba' ? 2 : 1}${c.start_date}`), // put emba first, start date desc
            )(response.result);

            setOptions(baseOptions.concat(_cohorts));
            setCohortsMap(keyBy('id')(_cohorts));
        });

        return () => {
            canceled = true;
        };
    }, [$injector, Cohort]);

    function optionLabel(opt) {
        // if this is a cohort, use the id and name
        if (opt.id) {
            return opt.name;
        }
        return opt.label;
    }

    function optionValue(opt) {
        // if this is a cohort, use the id and name
        if (opt.id) {
            return opt.id;
        }
        return opt.value;
    }

    useEffect(() => {
        if (cohortsMap) {
            setSelectedCohorts(
                flow(
                    map(entry => cohortsMap[entry]),
                    compact,
                )(value),
            );
        }
    }, [value, cohortsMap]);

    const { field } = props;
    return (
        <>
            <ReactIf if={cohortsMap}>
                <Select
                    field={field}
                    label="Visible To"
                    multiple
                    options={options}
                    optionLabel={optionLabel}
                    optionValue={optionValue}
                    renderValue={renderValue}
                />
                <div className={classes.chipWrapper}>
                    {selectedCohorts.map(cohort => (
                        <Chip key={cohort.id} label={cohort.name} variant="outlined" color="primary" />
                    ))}
                </div>
            </ReactIf>
            {!cohortsMap && <SubText>Loading cohorts...</SubText>}
        </>
    );
}
