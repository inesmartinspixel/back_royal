import React, { useEffect, useState } from 'react';
import ReactIf from 'ReactIf';
import { Formik, Form } from 'formik';
import { toForm, fromForm, validationSchema, EVENT_TYPE_CONFIGS, mappable } from 'StudentNetworkEvent';
import { Field } from 'FrontRoyalForm';
import {
    DatePicker,
    TextField,
    SingleLocationInput,
    Checkbox,
    Select,
    FileInput,
    materialUiTheme as theme,
} from 'FrontRoyalMaterialUiForm';
import { ThemeProvider } from '@material-ui/styles';
import { makeStyles } from '@material-ui/core/styles';
import { useTranslationHelper } from 'ngTranslate';
import { identity, isEqual, some, omit, map } from 'lodash/fp';
import { useConfig } from 'FrontRoyalConfig';
import { useRecordFromAdminTable, BackButton } from 'AdminTable';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import copy from 'clipboard-copy';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import FileCopy from '@material-ui/icons/FileCopy';
import moment from 'moment-timezone';
import StudentNetworkEventPreview from './StudentNetworkEventPreview';
import VisibleToInput from './VisibleToInput';

const useStyles = makeStyles(targetTheme => ({
    errorBox: {
        padding: targetTheme.spacing(3, 2),
        marginLeft: 'auto',
        marginRight: 'auto',
        marginTop: 20,
        color: 'red',
    },
}));

function FlexField(props) {
    return (
        <div style={{ flexGrow: '1', flexBasis: '50%', minWidth: '200px', paddingBottom: '12px' }}>
            {/* eslint-disable-next-line react/jsx-props-no-spreading */}
            <Field {...props} />
        </div>
    );
}

function FlexButton(props) {
    const { children } = props;
    const passthrough = omit(['style', 'children'])(props);
    return (
        <Button
            variant="contained"
            style={{ flex: '0 1 0', marginRight: theme.spacing(), ...props.style }}
            type="submit"
            color="primary"
            {...passthrough}
        >
            {children}
        </Button>
    );
}

function LineBreak() {
    return <div style={{ flex: '0 0 100%' }} />;
}

function useSetTimezoneOnLocationChange(formik) {
    const [latLng, setlatLng] = useState([
        formik.values.place && formik.values.place.details.lat,
        formik.values.place && formik.values.place.details.lng,
    ]);
    const config = useConfig();

    useEffect(() => {
        const placeDetails = formik.values.place && formik.values.place.details;
        if (!placeDetails || !config) {
            return;
        }

        const newLatLng = [placeDetails.lat, placeDetails.lng];
        if (!isEqual(newLatLng, latLng)) {
            setlatLng([placeDetails.lat, placeDetails.lng]);

            // set the timezone to empty so the form will be invalid until this
            // request returns
            formik.setFieldValue('timezone', '');
            const time = formik.values.start_time || moment();
            const apiKey = config.google_api_key;
            fetch(
                `https://maps.googleapis.com/maps/api/timezone/json?key=${apiKey}&location=${newLatLng.join(
                    ',',
                )}&timestamp=${time.toDate().getTime() / 1000}`,
            )
                .then(response => response.json())
                .then(data => {
                    formik.setFieldValue('timezone', data.timeZoneId);
                });
        }
    }, [formik, config, latLng]);
}

function FormFields({ formik, destroy, duplicate }) {
    const style = {
        display: 'flex',
        flexWrap: 'wrap',
        flexDirection: 'row',
        alignItems: 'flex-start',
        alignContent: 'flex-start',
        maxWidth: '680px',
        paddingLeft: '12px',
    };

    useSetTimezoneOnLocationChange(formik);

    const translationHelper = useTranslationHelper('student_network.student_network_event');

    useEffect(() => {
        const startTime = formik.values.start_time;
        const endTime = formik.values.end_time;

        // If the startTime changed and we now have an endTime
        // that's before our startTime, auto update it to be an
        // hour after startTime.
        if (startTime >= endTime) {
            formik.setFieldValue('end_time', startTime.clone().add(1, 'hours'));
        }

        /*
        Ignoring exhaustive-deps here is the right thing to do
        because we only want this to run when the start_time changes,
        not when the end_time changes.
    */
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [formik.values.start_time]);

    useEffect(() => {
        const id = formik.values.id;
        const published = formik.values.published;

        if (id && published) {
            formik.setFieldValue('shareable_url', `${window.ENDPOINT_ROOT}/student-network?event-id=${id}`);
        }

        /*
        Ignoring exhaustive-deps here is the right thing to do
        because we only want this to run when id or published
        changes.
    */
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [formik.values.id, formik.values.published]);

    const classes = useStyles();

    const rsvpOptions = [
        {
            key: 'not_required',
            label: 'RSVP Not Required',
        },
        {
            key: 'required',
            label: 'RSVP Required',
        },
        {
            key: 'closed',
            label: 'RSVP Closed',
        },
    ];

    return (
        <>
            <div {...{ style }}>
                <FlexField component={FileInput} name="image" label="Upload Image" />

                <LineBreak />
                <FlexField name="title" label="Title" component={TextField} />
                <FlexField
                    name="event_type"
                    component={Select}
                    label="Type"
                    options={EVENT_TYPE_CONFIGS}
                    optionValue={opt => opt.key}
                    optionLabel={opt => translationHelper.get(opt.key)}
                />

                <ReactIf if={mappable(formik.values)}>
                    <LineBreak />
                    <FlexField name="place" label="Location" component={SingleLocationInput} placeType="address" />
                    <FlexField name="location_name" label="Location Name" component={TextField} />
                </ReactIf>

                <LineBreak />

                <ReactIf if={!formik.values.date_tbd}>
                    <FlexField
                        name="start_time"
                        label="Start"
                        component={DatePicker}
                        format="YYYY/MM/DD h:mm A z"
                        timezone={
                            mappable(formik.values) ? formik.values.timezone || moment.tz.guess() : moment.tz.guess()
                        }
                    />
                    <FlexField
                        name="end_time"
                        label="End"
                        component={DatePicker}
                        format="YYYY/MM/DD h:mm A z"
                        timezone={
                            mappable(formik.values) ? formik.values.timezone || moment.tz.guess() : moment.tz.guess()
                        }
                    />
                </ReactIf>
                <FlexField name="date_tbd" label="Exact Date TBD" component={Checkbox} />
                {formik.values.date_tbd && (
                    <FlexField name="date_tbd_description" label="Date Description" component={TextField} />
                )}

                <LineBreak />
                <FlexField name="description" label="Description (Markdown)" component={TextField} multiline rows={3} />

                <LineBreak />
                <FlexField
                    name="rsvp_status"
                    component={Select}
                    label="RSVP Options"
                    options={rsvpOptions}
                    optionValue={opt => opt.key}
                    optionLabel={opt => opt.label} // Not bothering with translations since we're in the admin
                />
                <FlexField name="external_rsvp_url" label="External RSVP URL" component={TextField} />

                <LineBreak />
                <FlexField name="internal_notes" label="Internal Notes" component={TextField} multiline rows={3} />

                <LineBreak />
                <FlexField name="visible_to" component={VisibleToInput} />
                <FlexField name="published" label="Published" component={Checkbox} />

                <ReactIf if={formik.values.shareable_url}>
                    <LineBreak />
                    <FlexField
                        name="shareable_url"
                        label="Shareable URL"
                        component={TextField}
                        disabled
                        InputProps={{
                            endAdornment: (
                                <InputAdornment position="end">
                                    <IconButton
                                        aria-label="Copy"
                                        onClick={() => {
                                            copy(formik.values.shareable_url);
                                        }}
                                    >
                                        <FileCopy />
                                    </IconButton>
                                </InputAdornment>
                            ),
                        }}
                    />
                </ReactIf>

                <LineBreak />

                <div style={{ display: 'flex', justifyContent: 'space-between', flexGrow: 1 }}>
                    <FlexButton
                        type="button"
                        name="duplicate"
                        color="default"
                        onClick={() => {
                            duplicate(formik);
                            formik.setFieldValue('published', false);
                            formik.setFieldValue('title', `Duplicate of ${formik.values.title}`);
                        }}
                        style={{ flexBasis: 'auto' }}
                        disabled={formik.isSubmitting || !formik.values.id}
                    >
                        Duplicate
                    </FlexButton>

                    {/* marginLeft: auto pushes these buttons to the right */}
                    <FlexButton
                        type="button"
                        name="destroy"
                        color="secondary"
                        onClick={destroy}
                        style={{ marginLeft: 'auto' }}
                        disabled={formik.isSubmitting || !formik.values.id}
                    >
                        Delete
                    </FlexButton>

                    <FlexButton type="submit" color="primary" disabled={!formik.isValid || formik.isSubmitting}>
                        Save
                    </FlexButton>
                </div>

                {/* HACK: this is temporary until we refactor error handling */}
                <LineBreak />
                {some(identity)(formik.errors) && (
                    <Paper className={classes.errorBox}>
                        Please correct the following errors:
                        <br />
                        <ul style={{ marginBottom: 0 }}>{map(err => <li key={err}>{err}</li>)(formik.errors)}</ul>
                    </Paper>
                )}
            </div>
        </>
    );
}

export default function AdminEditStudentNetworkEvent({ record: studentNetworkEvent }) {
    const [renderFormFields, setRenderFormFields] = React.useState(false);

    // We have to wait a beat before adding the form in
    //  because of an issue in material-ui outlined
    // text fields.  If those fields are created when they are not visible
    // on the screen, then the label will not show up appropriately on
    // top of the border.  The reason this is not immediately visible on the scren seems
    // to be because it is being rendered from dynamic-node, which compiles the element
    // before inserting it into the DOM.
    // We may need to generalize this, or, it might get fixed in material-ui.
    // There are a number of related issues.
    //
    // * https://github.com/mui-org/material-ui/issues/14369
    // * https://github.com/mui-org/material-ui/issues/16465
    // * https://github.com/mui-org/material-ui/issues/17355
    //
    // * UPDATE: Looks like there is a fix for this in the works in material-ui: https://github.com/mui-org/material-ui/pull/17680
    useEffect(() => {
        setRenderFormFields(true);
    }, []);

    const { initialValues, onSubmit, destroy, duplicate } = useRecordFromAdminTable({
        fromForm,
        toForm,
    });

    return (
        <ThemeProvider theme={theme}>
            {renderFormFields && (
                <div className="admin-edit-student-network-event">
                    <Formik
                        initialValues={initialValues}
                        validationSchema={validationSchema}
                        onSubmit={onSubmit}
                        render={formik => (
                            <>
                                <BackButton home="Events" detail={formik.values.title || 'New Event'} />
                                <Form>
                                    <div style={{ display: 'flex' }}>
                                        <div style={{ width: '360px' }}>
                                            <StudentNetworkEventPreview formValues={formik.values} />
                                        </div>
                                        <FormFields {...{ formik, studentNetworkEvent, destroy, duplicate }} />
                                    </div>
                                </Form>
                            </>
                        )}
                    />
                </div>
            )}
        </ThemeProvider>
    );
}
