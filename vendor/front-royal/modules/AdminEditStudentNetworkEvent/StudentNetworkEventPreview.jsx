import React, { useState, useContext, useEffect } from 'react';
import AngularContext from 'AngularContext';
import { angularDirectiveToReact } from 'Angular2reactHelper';
import { fromForm } from 'StudentNetworkEvent';
import debounceRender from 'react-debounce-render';
import ReactIf from 'ReactIf';

const StudentNetworkEventDetails = angularDirectiveToReact('FrontRoyal.Admin', 'studentNetworkEventDetails', {
    event: '<',
    containerSelector: '<?',
    fadeInImage: '<?',
});

const StudentNetworkEventListItem = angularDirectiveToReact('FrontRoyal.Admin', 'studentNetworkEventListItem', {
    event: '<',
});

function ShadowBox({ children }) {
    return (
        <div
            style={{
                boxShadow: '2px 3px 9px 0px rgba(0,0,0,0.75)',
                borderRadius: '5px',
                margin: '0px 12px 12px',
                overflow: 'hidden',
            }}
        >
            {children}
        </div>
    );
}

function Preview({ studentNetworkEvent }) {
    return (
        <ReactIf if={studentNetworkEvent}>
            <ShadowBox>
                <StudentNetworkEventListItem event={studentNetworkEvent} />
            </ShadowBox>
            <ShadowBox>
                <StudentNetworkEventDetails event={studentNetworkEvent} fadeInImage={false} />
            </ShadowBox>
        </ReactIf>
    );
}

const MemoizedPreview = React.memo(
    Preview,
    (prevProps, nextProps) => prevProps.studentNetworkEvent === nextProps.studentNetworkEvent,
);

function StudentNetworkEventPreview({ formValues }) {
    const $injector = useContext(AngularContext);
    const [studentNetworkEvent, setStudentNetworkEvent] = useState();

    useEffect(() => {
        const StudentNetworkEvent = $injector.get('StudentNetworkEvent');
        const _studentNetworkEvent = studentNetworkEvent || StudentNetworkEvent.new();
        _studentNetworkEvent.copyAttrs(fromForm(formValues));

        // FIXME: it kind of sucks that we have to do this explicitly here.
        // Maybe we could have two different `fromForm` definitions, one
        // for converting for the api and one for converting for preview.
        // We can come back to it once we're doing this again
        _studentNetworkEvent.image = {
            formats: {
                original: {
                    url: formValues.image.getUrl && formValues.image.getUrl(),
                },
            },
        };
        setStudentNetworkEvent(_studentNetworkEvent);
        $injector.get('$rootScope').$apply();
    }, [formValues, $injector, studentNetworkEvent]);

    return <MemoizedPreview {...{ studentNetworkEvent }} />;
}

// Use debounce render so we're not updating on every keystroke to the title field
export default debounceRender(StudentNetworkEventPreview, 500);
