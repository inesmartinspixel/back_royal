import { allForUser, progressIsFetchedForUser } from 'StoredProgress';
import { difference, last } from 'lodash/fp';
import { setStreamsWithStoredContent } from 'StoredContent';
import { EventEmitter } from 'events';
import { DisconnectedError, dbHasPersistentStorage } from 'FrontRoyalStore';
import allTruthy from 'allTruthy';
import NetworkConnection from 'NetworkConnection';
import { TURNING_OFF_OFFLINE_MODE, TURNING_ON_OFFLINE_MODE } from './constants';
import resolveRouteInOfflineMode from './resolveRouteInOfflineMode';
import showOfflineModal from './showOfflineModal';

async function atLeastOneStreamWithContentStored(db) {
    const ct = await db.publishedStreams.where({ all_content_stored: 1 }).count();
    return ct > 0;
}

export default class OfflineModeManager extends EventEmitter {
    constructor(injector) {
        super();
        this.frontRoyalStore = injector.get('frontRoyalStore');
        this.injector = injector;

        // This will be set to true asynchronously
        // once a request is attempted
        this.inOfflineMode = false;

        // Note that this is asynchronous.  It takes a short
        // time for us to read the store and confirm that we
        // can enter offline mode.
        this.updateInOfflineMode();
    }

    destroy() {
        this.removeAllListeners();
    }

    // Resolves with true if the store is enabled for the
    // current user, offline mode is enabled in the config,
    // and offline mode is supported on this device.
    async isOfflineModeSupported() {
        const db = await this.frontRoyalStore.getDb();

        if (!dbHasPersistentStorage(db)) {
            return false;
        }

        const configRecord = await this.frontRoyalStore.getConfig();
        if (window.CORDOVA && configRecord?.enable_offline_mode === 'true') {
            return true;
        }

        // We would only really ever set it to this in local development
        if (configRecord?.enable_offline_mode === 'even_on_web') {
            return true;
        }

        return false;
    }

    // Resolves with true if isOfflineModeSupported() and
    // the things that are need in the database before going
    // into offline mode have been stored
    async canEnterOfflineMode() {
        if (!(await this.isOfflineModeSupported())) {
            return false;
        }

        const currentUser = await this.frontRoyalStore.getCurrentUser();

        // If the front royal store is disabled, frontRoyalStore.getCurrentUser()
        // will not return a currentUser.
        if (!currentUser) {
            return false;
        }

        // In order to enter offline mode we need
        const stuffLoaded = await this.frontRoyalStore.retryAfterHandledError(db =>
            allTruthy([
                // a currentUser whose progress is fetched
                progressIsFetchedForUser(currentUser.id, db),

                // at least one offline stream to put on the dashboard
                atLeastOneStreamWithContentStored(db),
            ]),
        );

        return stuffLoaded;
    }

    /*
        Normally, api calls that fail because the user has gone
        offline never complete. The promise they generate is never
        resolved or rejected.  You can use this method to wrap an
        api call when you need to know if the user switched into
        offline mode while the call was in flight.

        The function, `fn`, provided as an argument to this method
        should make an api call and return a promise.  If, before
        the api call completes, we switch to offline mode, then the
        promise will be rejected with a DisconnectedError.  If we
        are already in offline mode then we will just throw a
        DisconnectedError immediately without ever making an api call.

        Flagging a request as a `background` request has a similar effect
        to this. The difference is that rejectInOfflineMode preserves the
        normal disconnected handling if the user loses an internet connection
        but cannot enter offline mode.  So, `background` is used when the
        user is not waiting on the results of a request and does not need to
        know if the request fails.  rejectInOfflineMode is used if you have
        special handling in the case that offline mode has been entered,
        but without offline mode you want network errors to be handled normally.
    */
    rejectInOfflineMode(fn) {
        if (this.inOfflineMode) {
            return Promise.reject(new DisconnectedError());
        }

        const promise = fn();

        return new Promise((resolve, reject) => {
            // Listen for us to enter offline mode.  If we
            // do, reject this promise with a DisconnectedError
            const rejectWithDisconnectedError = () => reject(new DisconnectedError());
            this.once(TURNING_ON_OFFLINE_MODE, () => {
                rejectWithDisconnectedError();
            });

            // If the promise completes normally (successfully
            // or not), return its result and cancel
            // the listener.
            promise
                .then(result => {
                    resolve(result);
                }, reject)
                .finally(() => {
                    this.removeListener(TURNING_ON_OFFLINE_MODE, rejectWithDisconnectedError);
                });
        });
    }

    async shouldBeInOfflineMode() {
        const offline = NetworkConnection.offline;
        return offline ? this.canEnterOfflineMode() : false;
    }

    get inOfflineMode() {
        return this._inOfflineMode || false;
    }

    set inOfflineMode(val) {
        const oldValue = this._inOfflineMode || false;
        this._inOfflineMode = val;

        if (oldValue && !this._inOfflineMode) {
            this.emit(TURNING_OFF_OFFLINE_MODE);
            const EventLogger = this.injector.get('EventLogger');

            // label is not really important, but since we're required to have one, why not?
            EventLogger.log('offline_mode:turned_off', { label: 'off' });
        }

        if (!oldValue && this._inOfflineMode) {
            const EventLogger = this.injector.get('EventLogger');
            this.emit(TURNING_ON_OFFLINE_MODE);
            // label is not really important, but since we're required to have one, why not?
            EventLogger.log('offline_mode:turned_on', { label: 'on' });
        }
    }

    // If there is no network connection and offline mode is
    // supported, then this method will switch inOfflineMode to true.
    // Otherwise, it will switch inOfflineMode to false.
    async updateInOfflineMode() {
        this.inOfflineMode = await this.shouldBeInOfflineMode();

        return this.inOfflineMode;
    }

    // Called by the api error handler after a network
    // error is triggered by an api request.
    async showOfflineModalAfterDisconnectedError() {
        if (this.inOfflineMode) {
            return true;
        }

        if (await this.shouldBeInOfflineMode()) {
            await this.showOfflineModalThenEnterOfflineMode(this.injector);

            const $route = this.injector.get('$route');

            // If we are in the middle of resolving a route,
            // then the route handling will handle redirecting.
            // Otherwise, we do it here
            if (!$route.frontRoyalIsResolvingRoute) {
                this.injector.get('$rootScope').goHome();
            }
        }

        return this.inOfflineMode;
    }

    async resolveRoute() {
        const shouldBeInOfflineMode = await this.shouldBeInOfflineMode();
        const shouldEnterOfflineMode = !this.inOfflineMode && shouldBeInOfflineMode;

        if (shouldEnterOfflineMode) {
            // after showOfflineModal returns, inOfflineMode will be true
            await this.showOfflineModalThenEnterOfflineMode(this.injector);
        }

        if (!shouldBeInOfflineMode) {
            this.inOfflineMode = false;
        }

        if (this.inOfflineMode) {
            return resolveRouteInOfflineMode(this.injector);
        }

        return null;
    }

    async showOfflineModalThenEnterOfflineMode() {
        await showOfflineModal(this.injector);
        this.inOfflineMode = true;
    }

    // Called by the validation responder after an auth call fails
    async attemptOfflineAuthentication() {
        const inOfflineMode = await this.updateInOfflineMode();
        if (!inOfflineMode) {
            return null;
        }

        // We always expect to find a currentUser here, since we would
        // not have entered offline mode otherwise, ValidationResponder
        // seems like it would handle it fine even if we ended up returning
        // undefined here.
        const currentUser = await this.frontRoyalStore.getCurrentUser();
        return currentUser;
    }

    // FIXME: once all streams are coming from the store, this can just be a synchronous
    // check on `stream.all_content_stored`
    async streamIsAvailableOffline(stream) {
        return this.streamIdIsAvailableOffline(stream.id);
    }

    async streamIdIsAvailableOffline(streamId) {
        const canEnterOfflineMode = await this.canEnterOfflineMode();

        if (canEnterOfflineMode) {
            const storedStream = await this.frontRoyalStore.retryAfterHandledError(db =>
                db.publishedStreams.where({ id: streamId }).first(),
            );
            return storedStream?.all_content_stored === 1;
        }

        return Promise.resolve(false);
    }

    /*
        We make the last completed stream and the next 10 streams
        in the curriculum that are not completed available offline
    */
    async ensureDefaultStreamsAvailableOffline(user) {
        const isOfflineModeSupported = await this.isOfflineModeSupported();

        if (!isOfflineModeSupported) {
            return undefined;
        }

        const streamProgressRecords = await this.frontRoyalStore.retryAfterHandledError(db =>
            allForUser(db.streamProgress, user.id).toArray(),
        );
        const completedStreamLocalePackIds = streamProgressRecords
            .filter(record => record.completed_at)
            .map(record => record.locale_pack_id);

        const requiredStreamLocalePackIds = user.relevant_cohort.getRequiredStreamPackIdsFromPeriods();

        // Get the next 10 incomplete streams
        const incompleteStreamLocalePackIds = difference(requiredStreamLocalePackIds, completedStreamLocalePackIds);
        const availableStreamLocalePackIds = incompleteStreamLocalePackIds.slice(0, 10);

        // Get the last completed stream
        if (last(completedStreamLocalePackIds)) {
            availableStreamLocalePackIds.push(last(completedStreamLocalePackIds));
        }

        try {
            return await setStreamsWithStoredContent(availableStreamLocalePackIds, user.pref_locale, this.injector);
        } catch (err) {
            // If we go offline while trying to do this, just resolve.
            // We will try again later
            if (err.constructor === DisconnectedError) {
                return undefined;
            }

            // See fetchImageBlobsForLesson. We may want to handle this a bit different in https://trello.com/c/Cw5ajLHA
            if (err?.config?.imageRequest) {
                return undefined;
            }

            throw err;
        }
    }
}
