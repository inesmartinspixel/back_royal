import canNavigateToCurrentRouteInOfflineMode from './canNavigateToCurrentRouteInOfflineMode';

describe('canNavigateToCurrentRouteInOfflineMode', () => {
    let injector;
    let $route;
    let offlineModeManager;

    beforeEach(() => {
        offlineModeManager = {
            streamIdIsAvailableOffline: jest.fn(),
        };
        $route = {
            current: { $$route: { directive: 'some-directive' } },
        };
        injector = {
            get: key =>
                ({
                    offlineModeManager,
                    $route,
                }[key]),
        };
    });

    it('should be true if going to the student-dashboard', async () => {
        setDirective('student-dashboard');
        expect(await canNavigateToCurrentRouteInOfflineMode(injector)).toBe(true);
    });

    it('should be true if going to show-stream for a stream that is available offline', async () => {
        const streamId = setupNavigationToStream({ availableOffline: true, directive: 'show-stream' });
        expect(await canNavigateToCurrentRouteInOfflineMode(injector)).toBe(true);
        expect(offlineModeManager.streamIdIsAvailableOffline).toHaveBeenCalledWith(streamId);
    });

    it('should be false if going to show-stream for a stream that is available offline', async () => {
        const streamId = setupNavigationToStream({ availableOffline: false, directive: 'show-stream' });
        expect(await canNavigateToCurrentRouteInOfflineMode(injector)).toBe(false);
        expect(offlineModeManager.streamIdIsAvailableOffline).toHaveBeenCalledWith(streamId);
    });

    it('should be true if going to stream-dashboard for a stream that is available offline', async () => {
        const streamId = setupNavigationToStream({ availableOffline: true, directive: 'stream-dashboard' });
        expect(await canNavigateToCurrentRouteInOfflineMode(injector)).toBe(true);
        expect(offlineModeManager.streamIdIsAvailableOffline).toHaveBeenCalledWith(streamId);
    });

    it('should be false if going to stream-dashboard for a stream that is available offline', async () => {
        const streamId = setupNavigationToStream({ availableOffline: false, directive: 'stream-dashboard' });
        expect(await canNavigateToCurrentRouteInOfflineMode(injector)).toBe(false);
        expect(offlineModeManager.streamIdIsAvailableOffline).toHaveBeenCalledWith(streamId);
    });

    it('should be false for any other directive', async () => {
        expect(await canNavigateToCurrentRouteInOfflineMode(injector)).toBe(false);
    });

    function setDirective(directive) {
        $route.current.$$route.directive = directive;
    }

    function setupNavigationToStream({ availableOffline, directive }) {
        const streamId = 'some-stream';
        $route.current.params = { stream_id: streamId };
        offlineModeManager.streamIdIsAvailableOffline.mockResolvedValue(availableOffline);
        setDirective(directive);
        return streamId;
    }
});
