import showOfflineModal from './showOfflineModal';
import canNavigateToCurrentRouteInOfflineMode from './canNavigateToCurrentRouteInOfflineMode';

jest.mock('./canNavigateToCurrentRouteInOfflineMode', () => jest.fn());

describe('showOfflineModal', () => {
    let DialogModal;
    let injector;
    let $q;
    let offlineModeManager;

    beforeEach(() => {
        canNavigateToCurrentRouteInOfflineMode.mockClear();

        DialogModal = {
            alert: jest.fn().mockImplementation(opts => {
                // close the modal once it opens so that the
                // promise resolves
                opts.close();
            }),
        };

        $q = fn => new Promise(fn);

        offlineModeManager = {
            streamIdIsAvailableOffline: jest.fn(),
        };
        injector = {
            get: key =>
                ({
                    $q,
                    DialogModal,
                    offlineModeManager,
                }[key]),
        };
    });

    it('should show a modal with expected message when canNavigateToCurrentRouteInOfflineMode is true', async () => {
        canNavigateToCurrentRouteInOfflineMode.mockResolvedValue(true);
        await showOfflineModal(injector);
        expect(DialogModal.alert).toHaveBeenCalled();

        // The method resolves when the modal is closed, which is triggered automatically
        // up in the mock of DialogModal.alert
        const { scope } = DialogModal.alert.mock.calls[0][0];
        expect(scope.messageKey).toEqual('OfflineMode.showOfflineModal.only_offline_courses_available');
    });

    it('should show a modal with expected message when canNavigateToCurrentRouteInOfflineMode is false', async () => {
        canNavigateToCurrentRouteInOfflineMode.mockResolvedValue(false);
        await showOfflineModal(injector);
        expect(DialogModal.alert).toHaveBeenCalled();

        // The method resolves when the modal is closed, which is triggered automatically
        // up in the mock of DialogModal.alert
        const { scope } = DialogModal.alert.mock.calls[0][0];
        expect(scope.messageKey).toEqual('OfflineMode.showOfflineModal.click_to_continue_to_dashboard');
    });
});
