import { TURNING_OFF_OFFLINE_MODE, TURNING_ON_OFFLINE_MODE } from './constants';

// The exports may seem strange, but the real thing
// that this module makes available to the outside
// world is OfflineModeManager, which is made available through
// the angularModule

// eslint-disable-next-line import/prefer-default-export
export { TURNING_OFF_OFFLINE_MODE, TURNING_ON_OFFLINE_MODE };
