import 'FrontRoyalStore/angularModule';
import 'Injector/angularModule';
import OfflineModeManager from './OfflineModeManager';

angular.module('OfflineMode', ['FrontRoyalStore', 'Injector']).factory('offlineModeManager', [
    '$injector',
    $injector => {
        const frontRoyalStore = $injector.get('frontRoyalStore');
        const injector = $injector.get('injector');
        const $rootScope = $injector.get('$rootScope');
        const $timeout = $injector.get('$timeout');

        const offlineModeManager = new OfflineModeManager(injector);

        let ensureDefaultStreamsTimeout;
        const oneMinute = 60 * 1000;
        function ensureDefaultStreamsAvailableOffline(delay) {
            $timeout.cancel(ensureDefaultStreamsTimeout);
            if ($rootScope.currentUser && frontRoyalStore.enabled) {
                ensureDefaultStreamsTimeout = $timeout(delay);

                // After running ensureDefaultStreamsAvailableOffline, we run it
                // again every so often, so that we're always responding to
                // changes in the user's progress.
                ensureDefaultStreamsTimeout
                    .then(() => offlineModeManager.ensureDefaultStreamsAvailableOffline($rootScope.currentUser))
                    .then(() => ensureDefaultStreamsAvailableOffline(5 * oneMinute))
                    .catch(err => {
                        // prevent unhandled rejection error when $rootScope is destroyed
                        if (err !== 'canceled') {
                            throw err;
                        }
                    });
            }
        }

        $rootScope.$watchGroup(['currentUser', 'currentUser.pref_locale', () => frontRoyalStore.enabled], () => {
            /*
                This initial 5 second timeout is a hack.  The problem is that
                the request for streams with full lesson content in
                setStreamsWithStoredContent can be pretty slow.  There is a good chance that there are other, more
                urgent requests about to get sent.  We can set the priority of that other request to low, but once
                it is in flight, it will still block requests that come in after it.  Se we delay here.

                The real solution is to be able to mark some requests as not to be queued.
            */
            ensureDefaultStreamsAvailableOffline(5000);
        });
        $rootScope.$on('$destroy', () => {
            $timeout.cancel(ensureDefaultStreamsTimeout);
            offlineModeManager.destroy();
        });
        return offlineModeManager;
    },
]);
