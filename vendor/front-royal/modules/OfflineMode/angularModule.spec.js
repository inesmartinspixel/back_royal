import 'angular';
import 'angular-mocks';
import 'OfflineMode/angularModule';

describe('OfflineMode angularModule', () => {
    let $injector;
    let $timeout;
    let frontRoyalStore;
    let $rootScope;
    let $q;
    let offlineModeManager;
    const oneMinute = 60 * 1000;

    beforeEach(() => {
        angular.mock.module('OfflineMode');

        angular.mock.inject([
            '$injector',

            _$injector_ => {
                $injector = _$injector_;
                $timeout = $injector.get('$timeout');
                frontRoyalStore = $injector.get('frontRoyalStore');
                $rootScope = $injector.get('$rootScope');
                $q = $injector.get('$q');

                $rootScope.currentUser = {};
                frontRoyalStore.enabled = true;

                offlineModeManager = $injector.get('offlineModeManager');
                jest.spyOn(offlineModeManager, 'ensureDefaultStreamsAvailableOffline').mockReturnValue($q.resolve());
                $rootScope.$digest();
            },
        ]);
    });

    it('should call ensureDefaultStreamsAvailableOffline after 5 seconds and then repeatedly every 5 minutes', async () => {
        // Wait 5 seconds and then call ensureDefaultStreamsAvailableOffline
        $timeout.flush(5000 - 1);
        expect(offlineModeManager.ensureDefaultStreamsAvailableOffline).not.toHaveBeenCalled();
        $timeout.flush(1);
        expect(offlineModeManager.ensureDefaultStreamsAvailableOffline).toHaveBeenCalled();

        // Wait another 5 minutes and then call it again
        offlineModeManager.ensureDefaultStreamsAvailableOffline.mockClear();
        $timeout.flush(5 * oneMinute - 1);
        expect(offlineModeManager.ensureDefaultStreamsAvailableOffline).not.toHaveBeenCalled();
        $timeout.flush(1);
        expect(offlineModeManager.ensureDefaultStreamsAvailableOffline).toHaveBeenCalled();
    });

    it('should call ensureDefaultStreamsAvailableOffline when the current user changes', () => {
        assertEnsureCallTriggered(() => {
            $rootScope.currentUser = {};
        });
    });

    it('should call ensureDefaultStreamsAvailableOffline when pref_locale changes', () => {
        assertEnsureCallTriggered(() => {
            $rootScope.currentUser.pref_locale = 'changed';
        });
    });

    it('should call ensureDefaultStreamsAvailableOffline when frontRoyalStore enabled', () => {
        frontRoyalStore.enabled = false;
        assertEnsureCallTriggered(() => {
            frontRoyalStore.enabled = true;
        });
    });

    it('should catch canceled error', () => {
        offlineModeManager.ensureDefaultStreamsAvailableOffline.mockImplementation(() => {
            // eslint-disable-next-line no-throw-literal
            throw 'canceled';
        });

        // Wait 5 seconds and then call ensureDefaultStreamsAvailableOffline
        expect(() => {
            $timeout.flush(5000);
            expect(offlineModeManager.ensureDefaultStreamsAvailableOffline).toHaveBeenCalled();
        }).not.toThrow();
    });

    it("should throw error if it's not a canceled error", () => {
        offlineModeManager.ensureDefaultStreamsAvailableOffline.mockImplementation(() => {
            // eslint-disable-next-line no-throw-literal
            throw 'not canceled';
        });

        // Wait 5 seconds and then call ensureDefaultStreamsAvailableOffline
        expect(() => {
            $timeout.flush(5000);
            expect(offlineModeManager.ensureDefaultStreamsAvailableOffline).toHaveBeenCalled();
        }).toThrow('not canceled');
    });

    it('should destroy offlineModeManager on rootScope destroy', () => {
        jest.spyOn(offlineModeManager, 'destroy').mockImplementation();
        $rootScope.$destroy();
        expect(offlineModeManager.destroy).toHaveBeenCalled();
    });

    function assertEnsureCallTriggered(fn) {
        // Wait 5 seconds and then call ensureDefaultStreamsAvailableOffline
        $timeout.flush(5000);
        expect(offlineModeManager.ensureDefaultStreamsAvailableOffline).toHaveBeenCalled();

        offlineModeManager.ensureDefaultStreamsAvailableOffline.mockClear();
        fn();
        $rootScope.$digest();
        $timeout.flush(5000);
        expect(offlineModeManager.ensureDefaultStreamsAvailableOffline).toHaveBeenCalled();
    }
});
