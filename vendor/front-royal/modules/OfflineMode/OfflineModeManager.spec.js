import openTestDb from 'FrontRoyalStore/openTestDb';
import { setStreamsWithStoredContent } from 'StoredContent';
import { DisconnectedError } from 'FrontRoyalStore';
import dbHasPersistentStorage from 'FrontRoyalStore/dbHasPersistentStorage';
import NetworkConnection from 'NetworkConnection';
import OfflineModeManager from './OfflineModeManager';
import resolveRouteInOfflineMode from './resolveRouteInOfflineMode';
import showOfflineModal from './showOfflineModal';
import { TURNING_OFF_OFFLINE_MODE, TURNING_ON_OFFLINE_MODE } from './constants';

import 'jest-extended';

jest.mock('StoredContent/setStreamsWithStoredContent', () => jest.fn());
jest.mock('./resolveRouteInOfflineMode', () => jest.fn());
jest.mock('./showOfflineModal', () => jest.fn());
jest.mock('FrontRoyalStore/dbHasPersistentStorage', () => jest.fn());

describe('OfflineModeManager', () => {
    const db = openTestDb();
    const mockedGetDbReturnValue = 'mockedGetDbReturnValue';

    let config;
    const EventLogger = { log: jest.fn() };

    const requiredStreamLocalePackIds = [];
    for (let i = 1; i < 42; i++) {
        requiredStreamLocalePackIds.push(`stream_${i}`);
    }

    const user = {
        id: 'userId',
        pref_locale: 'xx',
        relevant_cohort: { getRequiredStreamPackIdsFromPeriods: () => requiredStreamLocalePackIds },
    };
    let offlineModeManager;
    const frontRoyalStore = {
        retryAfterHandledError: fn => fn(db),
        getConfig: () => Promise.resolve(config),
        getCurrentUser: async () => (await db.currentUsers.toArray())[0],
        getDb: () => Promise.resolve(mockedGetDbReturnValue),
    };
    const injector = { get: key => ({ frontRoyalStore, EventLogger }[key]) };

    beforeEach(() => {
        setStreamsWithStoredContent.mockClear();
        resolveRouteInOfflineMode.mockClear();
        showOfflineModal.mockClear();
        dbHasPersistentStorage.mockClear();
        EventLogger.log.mockClear();
        offlineModeManager = new OfflineModeManager(injector);
        offlineModeManager.offlineModeAvailable = true;
    });

    afterEach(() => {
        offlineModeManager.destroy();
    });

    describe('isOfflineModeSupported', () => {
        let dbHasPersistentStorageReturnValue;

        beforeEach(() => {
            dbHasPersistentStorage.mockImplementation(() => dbHasPersistentStorageReturnValue);
        });

        afterEach(() => {
            delete window.CORDOVA;
        });

        it('should be true if all requirements met', async () => {
            await setItAllUp();
            expect(await offlineModeManager.isOfflineModeSupported()).toBe(true);
        });

        it('should be false if not cordova', async () => {
            await setItAllUp();
            delete window.CORDOVA;
            expect(await offlineModeManager.isOfflineModeSupported()).toBe(false);
        });

        it('should be false if no config record', async () => {
            await setItAllUp();
            config = null;
            expect(await offlineModeManager.isOfflineModeSupported()).toBe(false);
        });

        it('should be false if not enable_offline_mode', async () => {
            await setItAllUp();
            config = { id: 'frontRoyalConfig', enable_offline_mode: 'false' };
            expect(await offlineModeManager.isOfflineModeSupported()).toBe(false);
        });

        it('should be false if db does not have persistent storage', async () => {
            await setItAllUp();
            dbHasPersistentStorageReturnValue = false;
            expect(await offlineModeManager.isOfflineModeSupported()).toBe(false);
            expect(dbHasPersistentStorage).toHaveBeenCalledWith(mockedGetDbReturnValue);
        });

        async function setItAllUp() {
            window.CORDOVA = {};
            dbHasPersistentStorageReturnValue = true;
            config = {
                id: 'frontRoyalConfig',
                enable_offline_mode: 'true',
            };
        }
    });

    describe('canEnterOfflineMode', () => {
        const streamId = 'streamId';
        let isOfflineModeSupported;

        beforeEach(() => {
            jest.spyOn(offlineModeManager, 'isOfflineModeSupported').mockImplementation(() => isOfflineModeSupported);
        });

        it('should be true if all requirements are met', async () => {
            await setItAllUp();
            expect(await offlineModeManager.canEnterOfflineMode()).toBe(true);
        });

        it('should be false if !isOfflineModeSupported', async () => {
            await setItAllUp();
            isOfflineModeSupported = false;
            expect(await offlineModeManager.canEnterOfflineMode()).toBe(false);
        });

        it('should be false if no current user', async () => {
            await setItAllUp();
            await db.currentUsers.clear();
            expect(await offlineModeManager.canEnterOfflineMode()).toBe(false);
        });

        it('should be false if progress is not yet fetched for user', async () => {
            await setItAllUp();
            await db.progressFetches.clear();
            expect(await offlineModeManager.canEnterOfflineMode()).toBe(false);
        });

        it('should be false if there are no streams with all_content_stored', async () => {
            await setItAllUp();
            await db.publishedStreams.put({ id: streamId, all_content_stored: 0 });
            expect(await offlineModeManager.canEnterOfflineMode()).toBe(false);
        });

        async function setItAllUp() {
            frontRoyalStore.enabled = true;
            isOfflineModeSupported = true;
            await db.currentUsers.put({ id: user.id });
            await db.progressFetches.put({ user_id: user.id });
            await db.publishedStreams.put({ id: streamId, all_content_stored: 1 });
        }
    });

    describe('rejectInOfflineMode', () => {
        let onLine;
        beforeEach(() => {
            onLine = true;
            jest.spyOn(NetworkConnection, 'offline', 'get').mockImplementation(() => !onLine);
            // make it so updateInOfflineMode will actually set the value
            jest.spyOn(offlineModeManager, 'canEnterOfflineMode').mockReturnValue(Promise.resolve(true));
        });

        it('should reject if already in offline mode', async () => {
            offlineModeManager.inOfflineMode = true;
            let err;
            try {
                await offlineModeManager.rejectInOfflineMode(() => {});
            } catch (_err) {
                err = _err;
            }
            expect(err?.constructor).toBe(DisconnectedError);
        });

        it('should reject if offline mode is turned on before the promise resolves', async () => {
            let err;
            const promise = offlineModeManager.rejectInOfflineMode(() => {
                return new Promise(() => {});
            });
            onLine = false;
            offlineModeManager.updateInOfflineMode();
            try {
                await promise;
            } catch (_err) {
                err = _err;
            }
            expect(err?.constructor).toBe(DisconnectedError);
        });

        it('should resolve if promise resolves', async () => {
            expect(await offlineModeManager.rejectInOfflineMode(async () => 'returnValue')).toEqual('returnValue');
        });

        it('should reject if promise rejects', async () => {
            // eslint-disable-next-line prefer-promise-reject-errors
            const promise = Promise.reject('rejection');
            let err;
            try {
                await offlineModeManager.rejectInOfflineMode(() => promise);
            } catch (_err) {
                err = _err;
            }
            expect(err).toEqual('rejection');
        });
    });

    describe('attemptOfflineAuthentication', () => {
        it('should return null if not in offline mode', async () => {
            jest.spyOn(offlineModeManager, 'updateInOfflineMode').mockReturnValue(Promise.resolve(false));

            // enable the store so we can assert that there is no request to getCurrentUser anyway
            frontRoyalStore.enabled = true;
            jest.spyOn(offlineModeManager.frontRoyalStore, 'getCurrentUser');
            expect(await offlineModeManager.attemptOfflineAuthentication()).toBe(null);
            expect(offlineModeManager.frontRoyalStore.getCurrentUser).not.toHaveBeenCalled();
        });

        it('should return the currentUser if there is one', async () => {
            jest.spyOn(offlineModeManager, 'updateInOfflineMode').mockReturnValue(Promise.resolve(true));

            frontRoyalStore.enabled = true;
            jest.spyOn(offlineModeManager.frontRoyalStore, 'getCurrentUser').mockReturnValue(
                Promise.resolve({ id: user.id }),
            );
            const result = await offlineModeManager.attemptOfflineAuthentication();
            expect(result.id).toEqual(user.id);
        });
    });

    describe('ensureDefaultStreamsAvailableOffline', () => {
        beforeEach(() => {
            jest.spyOn(offlineModeManager, 'isOfflineModeSupported').mockReturnValue(Promise.resolve(true));
        });

        it('should make available the last completed stream and the next 10 incomplete ones', async () => {
            await db.streamProgress.bulkPut([
                {
                    user_id: user.id,
                    locale_pack_id: 'stream_1',
                    completed_at: 1,
                },
                {
                    user_id: user.id,
                    locale_pack_id: 'stream_2',
                    completed_at: 2,
                },
                {
                    user_id: user.id,
                    locale_pack_id: 'stream_3',
                },
            ]);
            await offlineModeManager.ensureDefaultStreamsAvailableOffline(user);
            expect(setStreamsWithStoredContent).toHaveBeenCalledWith(
                [
                    // the next 10 incomplete streams
                    'stream_3',
                    'stream_4',
                    'stream_5',
                    'stream_6',
                    'stream_7',
                    'stream_8',
                    'stream_9',
                    'stream_10',
                    'stream_11',
                    'stream_12',

                    // and the last completed one
                    'stream_2',
                ],
                user.pref_locale,
                offlineModeManager.injector,
            );
        });
        it('should ignore progress from another user', async () => {
            await db.streamProgress.bulkPut([
                {
                    user_id: 'anotherUser',
                    locale_pack_id: 'stream_1',
                    completed_at: 1,
                },
                {
                    user_id: 'anotherUser',
                    locale_pack_id: 'stream_2',
                    completed_at: 2,
                },
            ]);
            await offlineModeManager.ensureDefaultStreamsAvailableOffline(user);
            const result = setStreamsWithStoredContent.mock.calls[0][0];
            expect(result).toIncludeAllMembers(['stream_1', 'stream_2']);
        });
    });

    describe('resolveRoute', () => {
        it('should show modal and call resolveRouteInOfflineMode if not in offline mode and should be entering offline mode', async () => {
            showOfflineModal.mockResolvedValue();
            offlineModeManager.inOfflineMode = false;
            jest.spyOn(offlineModeManager, 'shouldBeInOfflineMode').mockReturnValue(true);
            await offlineModeManager.resolveRoute();
            expect(showOfflineModal).toHaveBeenCalled();
            expect(resolveRouteInOfflineMode).toHaveBeenCalled();
            expect(offlineModeManager.inOfflineMode).toBe(true);
        });

        it('should turn off offline mode if should not be in offline mode', async () => {
            offlineModeManager.inOfflineMode = true;
            jest.spyOn(offlineModeManager, 'shouldBeInOfflineMode').mockReturnValue(false);
            await offlineModeManager.resolveRoute();
            expect(showOfflineModal).not.toHaveBeenCalled();
            expect(resolveRouteInOfflineMode).not.toHaveBeenCalled();
            expect(offlineModeManager.inOfflineMode).toBe(false);
        });

        it('should call resolveRouteInOfflineMode if already in offline mode', async () => {
            offlineModeManager.inOfflineMode = true;
            jest.spyOn(offlineModeManager, 'shouldBeInOfflineMode').mockReturnValue(true);
            await offlineModeManager.resolveRoute();
            expect(showOfflineModal).not.toHaveBeenCalled();
            expect(resolveRouteInOfflineMode).toHaveBeenCalled();
            expect(offlineModeManager.inOfflineMode).toBe(true);
        });
    });

    describe('updateInOfflineMode', () => {
        it('should work', async () => {
            let shouldBeInOfflineMode;
            jest.spyOn(offlineModeManager, 'shouldBeInOfflineMode').mockImplementation(() => shouldBeInOfflineMode);
            const turnedOff = jest.fn();
            const turnedOn = jest.fn();
            offlineModeManager.on(TURNING_OFF_OFFLINE_MODE, turnedOff);
            offlineModeManager.on(TURNING_ON_OFFLINE_MODE, turnedOn);

            function clearSpies() {
                turnedOff.mockClear();
                turnedOn.mockClear();
                EventLogger.log.mockClear();
            }

            // was false / still false
            shouldBeInOfflineMode = false;
            offlineModeManager.inOfflineMode = false;
            expect(await offlineModeManager.updateInOfflineMode()).toBe(false);
            expect(offlineModeManager.inOfflineMode).toBe(false);
            expect(turnedOff).not.toHaveBeenCalled();
            expect(turnedOn).not.toHaveBeenCalled();
            expect(EventLogger.log).not.toHaveBeenCalled();
            clearSpies();

            // was false / now true
            shouldBeInOfflineMode = true;
            expect(await offlineModeManager.updateInOfflineMode()).toBe(true);
            expect(offlineModeManager.inOfflineMode).toBe(true);
            expect(turnedOff).not.toHaveBeenCalled();
            expect(turnedOn).toHaveBeenCalled();
            expect(EventLogger.log).toHaveBeenCalledWith('offline_mode:turned_on', { label: 'on' });
            clearSpies();

            // was true / still true
            expect(await offlineModeManager.updateInOfflineMode()).toBe(true);
            expect(offlineModeManager.inOfflineMode).toBe(true);
            expect(turnedOff).not.toHaveBeenCalled();
            expect(turnedOn).not.toHaveBeenCalled();
            expect(EventLogger.log).not.toHaveBeenCalled();
            clearSpies();

            // was true / now false
            shouldBeInOfflineMode = false;
            expect(await offlineModeManager.updateInOfflineMode()).toBe(false);
            expect(offlineModeManager.inOfflineMode).toBe(false);
            expect(turnedOff).toHaveBeenCalled();
            expect(turnedOn).not.toHaveBeenCalled();
            expect(EventLogger.log).toHaveBeenCalledWith('offline_mode:turned_off', { label: 'off' });
            clearSpies();
        });
    });

    describe('shouldBeInOfflineMode', () => {
        it('should work', async () => {
            let canEnterOfflineMode;
            let onLine;
            jest.spyOn(offlineModeManager, 'canEnterOfflineMode').mockImplementation(() => canEnterOfflineMode);
            jest.spyOn(NetworkConnection, 'online', 'get').mockImplementation(() => onLine);

            onLine = true;
            canEnterOfflineMode = true;
            expect(await offlineModeManager.shouldBeInOfflineMode()).toBe(false);

            onLine = true;
            canEnterOfflineMode = false;
            expect(await offlineModeManager.shouldBeInOfflineMode()).toBe(false);

            onLine = false;
            canEnterOfflineMode = false;
            expect(await offlineModeManager.shouldBeInOfflineMode()).toBe(false);

            onLine = false;
            canEnterOfflineMode = true;
            expect(await offlineModeManager.shouldBeInOfflineMode()).toBe(true);
        });
    });
});
