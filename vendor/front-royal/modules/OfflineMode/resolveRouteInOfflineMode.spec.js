import resolveRouteInOfflineMode from './resolveRouteInOfflineMode';
import canNavigateToCurrentRouteInOfflineMode from './canNavigateToCurrentRouteInOfflineMode';

jest.mock('./canNavigateToCurrentRouteInOfflineMode', () => jest.fn());

describe('resolveRouteInOfflineMode', () => {
    let injector;
    let $q;
    let $rootScope;

    beforeEach(() => {
        $rootScope = {
            redirectHomeWhileRouting: jest.fn(),
        };
        canNavigateToCurrentRouteInOfflineMode.mockClear();
        $q = fn => new Promise(fn);
        $q.resolve = Promise.resolve;

        injector = {
            get: key =>
                ({
                    $q,
                    $rootScope,
                }[key]),
        };
    });

    it('should just resolve when canNavigateToCurrentRouteInOfflineMode is true', async () => {
        canNavigateToCurrentRouteInOfflineMode.mockResolvedValue(true);
        await resolveRouteInOfflineMode(injector);
        expect($rootScope.redirectHomeWhileRouting).not.toHaveBeenCalled();
    });

    it('should redirect when canNavigateToCurrentRouteInOfflineMode is false', async () => {
        canNavigateToCurrentRouteInOfflineMode.mockResolvedValue(false);
        await resolveRouteInOfflineMode(injector);
        expect($rootScope.redirectHomeWhileRouting).toHaveBeenCalled();
    });
});
