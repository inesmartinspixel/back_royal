# Offline Mode

## Requirements for offline mode

The requirements for offline mode are defined in `isOfflineModeSupported()` and `canEnterOfflineMode()`.

Offline mode is supported if

1. we are in a cordova client
1. the `currentUser` has `enable_front_royal_store=true`
1. the `enable_offline_mode` config setting has been set to `'true'`.

If offline mode is supported, a user can enter offline mode if a few things have been saved to the store:

1. the site config, which is stored in `configRecords`.
1. a `currentUser` record, which is stored in `currentUsers`
1. all of the progress for the `currentUser`
1. at least one stream with all of the content needed to launch the player for any lesson in the stream

## Launching without a network connection

If the cordova app is launched without a network connection, a user may be able to enter offline mode. Here is how it happens:

1. In `app.js#networkReadyBootstrap` a call to `ConfigFactory.getConfig` will trigger an api request to load the app config. That request will fail, and `ConfigFactory#_loadFromStore` will attempt to load the config from the store.
1. In `app.js#networkReadyBootstrap` a call to `ValidationResponder.initialize` will call `$auth.validateUser()`. That call will fail and trigger`OfflineModeManager#attemptOfflineAuthentication()`. If all the requirements for entering offline mode are met, then the `currentUser` will be loaded from the store and offline mode will be turned on.
1. The user will be routed onto a page in the app in offline mode
