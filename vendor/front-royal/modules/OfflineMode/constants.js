const TURNING_OFF_OFFLINE_MODE = 'turning off offline mode';
const TURNING_ON_OFFLINE_MODE = 'turning on offline mode';

// eslint-disable-next-line import/prefer-default-export
export { TURNING_OFF_OFFLINE_MODE, TURNING_ON_OFFLINE_MODE };
