import React, { useRef } from 'react';
import AdminEditStudentNetworkEvent from 'AdminEditStudentNetworkEvent';
import { TextField, Select, MultiLocationInput } from 'FrontRoyalMaterialUiForm';
import { useTranslationHelper } from 'ngTranslate';
import AdminTable, { CheckmarkCell, TimeCell } from 'AdminTable';
import { EVENT_TYPE_CONFIGS, eventTypeIcon } from 'StudentNetworkEvent';
import { useTheme } from '@material-ui/core/styles';
import { omit } from 'lodash/fp';
import Oreo from 'Oreo';

export default function AdminStudentNetworkEventsTable({ $injector }) {
    const translationHelper = useTranslationHelper('student_network.student_network_event', $injector);

    // see the `input` styles at https://github.com/mui-org/material-ui/blob/master/packages/material-ui/src/OutlinedInput/OutlinedInput.js
    // I tried to import that here, but it didn't work, so just hardcoding.
    const paddingLeftFromOutlinedInput = 14;

    const eventTypeRenderValueStyle = {
        position: 'absolute',
        top: '50%',

        // the `left` value here should be the same as the
        left: paddingLeftFromOutlinedInput,
        transform: 'translateY(-50%)',
        width: `calc(100% - ${2 * paddingLeftFromOutlinedInput}px)`,
        overflow: 'hidden',
    };

    function stringForPlace(studentNetworkEvent) {
        if (!studentNetworkEvent.placeCity) {
            return '';
        }
        return `${studentNetworkEvent.placeCity}${studentNetworkEvent.placeCity && ', '}${
            studentNetworkEvent.placeCountry
        }`;
    }

    const columns = useRef([
        {
            Header: 'Title',
            accessor: 'title',
            Cell: function Title({ row: { original } }) {
                const image = original.image;
                const src = image && image.formats && image.formats.original && image.formats.original.url;
                const title = original.title;
                return (
                    <div style={{ whiteSpace: 'nowrap' }}>
                        <div
                            style={{
                                backgroundColor: Oreo.COLOR_V3_MAP_BLUE_MID,
                                width: 40,
                                height: 30,
                                display: 'inline-block',
                                verticalAlign: 'middle',
                                borderRadius: 3,
                                marginRight: 10,
                                overflow: 'hidden',
                            }}
                        >
                            {src && (
                                <img
                                    src={src}
                                    style={{
                                        width: '100%',
                                        height: '100%',
                                        objectFit: 'cover',
                                    }}
                                />
                            )}
                        </div>
                        {title}
                    </div>
                );
            },
        },
        {
            Header: 'Start',
            accessor: 'start_time',
            Cell: TimeCell,
        },
        {
            Header: 'Type',
            accessor: 'event_type',
            Cell: function EventType({ row: { original } }) {
                const eventType = original.event_type;
                const theme = useTheme();
                return (
                    <span style={{ whiteSpace: 'nowrap' }}>
                        <img {...eventTypeIcon(eventType)} style={{ marginRight: theme.spacing() }} />
                        {translationHelper.get(eventType)}
                    </span>
                );
            },
        },
        {
            Header: 'Location Name',
            accessor: 'location_name',
        },
        {
            Header: 'City',
            accessor: 'place_details',
            Cell: function City({ row: { original } }) {
                return <>{stringForPlace(original)}</>;
            },
            sortType(a, b) {
                const strA = stringForPlace(a.original);
                const strB = stringForPlace(b.original);
                if (strA > strB) {
                    return 1;
                }
                if (strA < strB) {
                    return -1;
                }
                return 0;
            },
        },
        {
            Header: 'Published',
            accessor: 'published',
            Cell: CheckmarkCell,
        },
        {
            Header: 'Author',
            accessor: 'author_name',
        },
    ]).current;

    const filterFields = useRef([
        {
            name: 'keyword_search',
            component: TextField,
            label: 'Search',
        },
        {
            name: 'event_type',
            component: Select,
            label: 'Type',
            multiple: true,
            options: EVENT_TYPE_CONFIGS,
            optionValue: opt => opt.key,
            optionLabel: opt => translationHelper.get(opt.key),
            renderValue: function RenderEventTypeSelection(selected) {
                return (
                    <>
                        {/* The nbsp is necessary to give the element one line of height */}
                        &nbsp;
                        {/* Then we absolutely position the icons so that their height
                        does not effect the height of the element */}
                        <div style={eventTypeRenderValueStyle}>
                            {selected.map(key => (
                                <img key={key} {...eventTypeIcon(key)} />
                            ))}
                        </div>
                    </>
                );
            },
        },
        {
            name: 'published',
            component: Select,
            options: [
                { label: 'All', value: '' },
                { label: 'Published', value: true },
                { label: 'Unpublished', value: false },
            ],
            label: 'Published',
        },
        {
            name: 'time_filter',
            component: Select,
            options: [
                { label: 'All', value: '' },
                { label: 'Upcoming Events', value: 'upcoming' },
                { label: 'Past Events', value: 'past' },
                { label: 'TBD', value: 'tbd' },
            ],
            label: 'Time Filter',
        },
        {
            name: 'places',
            label: 'Locations',
            component: MultiLocationInput,
            getLabelsForSelectFilterChips: placeDetails => placeDetails.formatted_address,
        },
        {
            name: 'editor',
            hidden: true,
            default: true,
        },
    ]).current;

    const StudentNetworkEvent = $injector.get('StudentNetworkEvent');

    const fetchData = useRef(filters => {
        const queryFilters = omit(['start_time', 'end_time', 'include_tbd', 'only_tbd'])({ ...filters });

        // Tricky: handle our custom Time Filter here
        if (queryFilters.time_filter === 'past') {
            queryFilters.start_time = 0;
            queryFilters.end_time = new Date().getTime() / 1000;
        } else if (queryFilters.time_filter === 'upcoming') {
            queryFilters.start_time = new Date().getTime() / 1000;
            queryFilters.end_time = new Date('2099/01/01').getTime() / 1000;
            queryFilters.include_tbd = true;
        } else if (queryFilters.time_filter === 'tbd') {
            queryFilters.only_tbd = true;
        } else {
            queryFilters.start_time = 0;
            queryFilters.end_time = new Date('2099/01/01').getTime() / 1000;
            queryFilters.include_tbd = true;
        }

        return StudentNetworkEvent.index({
            fields: ['ADMIN_FIELDS'],
            filters: queryFilters,
        }).then(response => response.result);
    }).current;

    const getBlankRecord = useRef(() => StudentNetworkEvent.new()).current;

    return (
        <AdminTable
            {...{
                $injector,
                idParam: 'studentnetworkevent-id',
                collection: $injector.get('StudentNetworkEvent').collection,
                columns,
                fetchData,
                filterFields,
                EditComponent: AdminEditStudentNetworkEvent,
                getBlankRecord,
            }}
        />
    );
}
