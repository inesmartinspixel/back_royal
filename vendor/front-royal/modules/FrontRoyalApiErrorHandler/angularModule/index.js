import './scripts/front_royal_api_error_handler';

import './scripts/logged_out_dir';
import './scripts/fatal_dir';
import './scripts/disconnected_dir';
import './scripts/disconnected_mobile_init_dir';
import './scripts/failed_validation_dir';
import './scripts/internal_server_error_dir';
import 'FrontRoyalApiErrorHandler/locales/front_royal_api_error_handler/api_error_handler-en.json';
import 'FrontRoyalApiErrorHandler/locales/front_royal_api_error_handler/disconnected-en.json';
import 'FrontRoyalApiErrorHandler/locales/front_royal_api_error_handler/disconnected_mobile_init-en.json';
import 'FrontRoyalApiErrorHandler/locales/front_royal_api_error_handler/fatal-en.json';
import 'FrontRoyalApiErrorHandler/locales/front_royal_api_error_handler/internal_server_error-en.json';
import 'FrontRoyalApiErrorHandler/locales/front_royal_api_error_handler/logged_out-en.json';
