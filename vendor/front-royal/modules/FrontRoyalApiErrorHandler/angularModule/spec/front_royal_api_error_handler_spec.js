import 'AngularSpecHelper';
import 'FrontRoyalApiErrorHandler/angularModule';

import { constant } from 'lodash/fp';
import NetworkConnection from 'NetworkConnection';
import apiErrorHandlerLocales from 'FrontRoyalApiErrorHandler/locales/front_royal_api_error_handler/api_error_handler-en.json';
import disconnectedLocales from 'FrontRoyalApiErrorHandler/locales/front_royal_api_error_handler/disconnected-en.json';
import fatalLocales from 'FrontRoyalApiErrorHandler/locales/front_royal_api_error_handler/fatal-en.json';
import signInFormLocales from 'Authentication/locales/authentication/sign_in_form-en.json';
import loggedOutLocales from 'FrontRoyalApiErrorHandler/locales/front_royal_api_error_handler/logged_out-en.json';
import internalServerErrorLocales from 'FrontRoyalApiErrorHandler/locales/front_royal_api_error_handler/internal_server_error-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(
    apiErrorHandlerLocales,
    disconnectedLocales,
    fatalLocales,
    signInFormLocales,
    loggedOutLocales,
    internalServerErrorLocales,
);

describe('FrontRoyal.ApiErrorHandler', () => {
    let SpecHelper;
    let $rootScope;
    let ApiErrorHandler;
    let elem;
    let $timeout;
    let HttpQueue;
    let originalPromise;
    let response;
    let $q;
    let originalPromiseResolved;
    let $auth;
    let originalPromiseRejected;
    let originalPromiseRejectedWith;
    let ErrorLogService;
    let $window;
    let EventLogger;
    let Hippos;
    let DialogModal;
    let $location;
    let modalScope;
    let $injector;
    let offlineModeManager;
    let shouldEnterOfflineMode;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.ApiErrorHandler', 'SpecHelper');

        const confirm = constant(true);
        angular.mock.module($provide => {
            $window = {
                location: {
                    href: '',
                },
                document: window.document,
                markdown: window.markdown,
                confirm,
                open() {},
                navigator: {
                    onLine: true,
                },
                performance: window.performance,
            };
            $provide.value('$window', $window);
        });

        angular.mock.inject([
            '$injector',
            _$injector => {
                $injector = _$injector;
                SpecHelper = $injector.get('SpecHelper');
                $rootScope = $injector.get('$rootScope');
                ApiErrorHandler = $injector.get('ApiErrorHandler');
                $timeout = $injector.get('$timeout');
                HttpQueue = $injector.get('HttpQueue');
                $q = $injector.get('$q');
                ErrorLogService = $injector.get('ErrorLogService');
                EventLogger = $injector.get('EventLogger');
                Hippos = $injector.get('Hippos');
                $auth = $injector.get('$auth');
                DialogModal = $injector.get('DialogModal');
                $location = $injector.get('$location');
                offlineModeManager = $injector.get('offlineModeManager');

                SpecHelper.mockCapabilities();

                jest.spyOn(ErrorLogService, 'notify').mockImplementation(() => {});
                jest.spyOn(EventLogger.prototype, 'log');
                SpecHelper.stubEventLogging();
                originalPromiseResolved = false;
                originalPromiseRejected = false;

                shouldEnterOfflineMode = false;
                jest.spyOn(offlineModeManager, 'showOfflineModalAfterDisconnectedError').mockImplementation(() =>
                    $q.resolve(shouldEnterOfflineMode),
                );

                SpecHelper.stubConfig();
            },
        ]);
    });

    afterEach(() => {
        DialogModal.removeAlerts(); // I don't understand why, but there are strange errors if we don't do this before the cleanup
        SpecHelper.cleanup();
    });

    it('should log to ErrorLogger if notifySentry on network unavailable', () => {
        handleErrorAndAssertModal('public', {
            status: 0,
        });

        expect(EventLogger.prototype.log).toHaveBeenCalled();
        const args = EventLogger.prototype.log.mock.calls[0];
        expect(args[0]).toBe('error');
        expect(args[1]).toEqual({
            label: '/api/',
            server_error: angular.extend(
                {
                    path: '/api/',
                    message: undefined,
                },
                _.omit(response, 'handledByFrontRoyalApiErrorHandler'),
            ),
        });
    });

    it('should log to ErrorLogger if notifySentry on timeouts / promise cancels', () => {
        handleErrorAndAssertModal('public', {
            status: -1,
        });

        expect(EventLogger.prototype.log).toHaveBeenCalled();
        const args = EventLogger.prototype.log.mock.calls[0];
        expect(args[0]).toBe('error');
        expect(args[1]).toEqual({
            label: '/api/',
            server_error: angular.extend(
                {
                    path: '/api/',
                    message: undefined,
                },
                _.omit(response, 'handledByFrontRoyalApiErrorHandler'),
            ),
        });
    });

    it('should not handle urls that do not match /api/', () => {
        jest.spyOn(ApiErrorHandler, '_switchToOfflineModeOrHandleError').mockImplementation(() => {});
        ApiErrorHandler.onResponseError({
            config: {
                url: 'do not handle',
            },
        });
        expect(ApiErrorHandler._switchToOfflineModeOrHandleError).not.toHaveBeenCalled();
    });

    [0, 502, 504].forEach(statusCode => {
        let expectedMessageText;

        describe(`with status_code ${statusCode}`, () => {
            beforeEach(() => {
                expectedMessageText =
                    statusCode === 0
                        ? 'A message failed to reach the server.'
                        : 'We were unable to contact our servers.';

                handleErrorAndAssertModal('public', {
                    status: statusCode,
                });
            });

            it('should show disconnected modal', () => {
                SpecHelper.expectElementText(elem, '.disconnected .explanation', expectedMessageText);
            });
        });
    });

    describe('entering offline mode', () => {
        it('should return a promise that never resolves', () => {
            jest.spyOn(DialogModal, 'alert');
            shouldEnterOfflineMode = true;
            jest.spyOn(HttpQueue.prototype, 'unfreezeAfterError').mockImplementation(() => {});
            handleError('public', { status: 0 });
            $timeout.flush();
            expect(HttpQueue.prototype.unfreezeAfterError).toHaveBeenCalled();
            expect(DialogModal.alert).not.toHaveBeenCalled();
            expect(originalPromiseResolved).toBe(false);
            expect(originalPromiseRejected).toBe(false);
        });
    });

    describe('disconnected modal', () => {
        let resolveRetry;
        let rejectRetry;
        let retryPromise;

        beforeEach(() => {
            retryPromise = $q((_resolve, _reject) => {
                resolveRetry = _resolve;
                rejectRetry = _reject;
            });
            jest.spyOn(HttpQueue.prototype, 'retry').mockReturnValue(retryPromise);
            handleDisconnectedError();
        });

        afterEach(() => {
            delete $window.CORDOVA;
            delete navigator.splashscreen;
        });

        it('should retry after 10 seconds if online=true initially', () => {
            // after 9 seconds timeout has not been called
            $timeout.flush(9000);
            expect(HttpQueue.prototype.retry).not.toHaveBeenCalled();

            // after 10 seconds it has
            $timeout.flush(1000);
            expect(HttpQueue.prototype.retry).toHaveBeenCalled();
        });

        it('should retry after 2 seconds if going from offline to online', () => {
            let isOnline = false;
            jest.spyOn(NetworkConnection, 'online', 'get').mockImplementation(() => isOnline);
            handleDisconnectedError();
            $timeout.flush();
            SpecHelper.expectElementText(elem, '.disconnected button', 'Reconnecting...');
            SpecHelper.expectElementText(
                elem,
                '.disconnected .message',
                'A message failed to reach the server. We will retry once you are back online.',
            );

            isOnline = true;
            $rootScope.$apply();
            $timeout.flush(1000);

            // after 1 second, retry has still not been called
            expect(HttpQueue.prototype.retry).not.toHaveBeenCalled();
            $timeout.flush(1000);

            // after 2 seconds, it has
            expect(HttpQueue.prototype.retry).toHaveBeenCalled();
        });

        it('should cancel an in-flight retry and show reconnecting message when switching from online to offline', () => {
            $timeout.flush(10000);
            expect(HttpQueue.prototype.retry).toHaveBeenCalled();
            jest.spyOn(NetworkConnection, 'online', 'get').mockReturnValue(false);
            const cancelSpy = jest.spyOn(modalScope, 'cancelRetryRequest');
            modalScope.$digest();
            expect(cancelSpy).toHaveBeenCalled();

            SpecHelper.expectElementText(elem, '.disconnected button', 'Reconnecting...');
            SpecHelper.expectElementText(
                elem,
                '.disconnected .message',
                'A message failed to reach the server. We will retry once you are back online.',
            );
        });

        it('should be possible to force an immediate retry by clicking button', () => {
            expect(HttpQueue.prototype.retry).not.toHaveBeenCalled();
            SpecHelper.expectElementText(elem, '.disconnected button', 'Retry now');
            SpecHelper.expectElementText(
                elem,
                '.disconnected .message',
                'A message failed to reach the server. We will retry in 10 seconds.',
            );
            SpecHelper.click(elem, '.disconnected button');
            expect(HttpQueue.prototype.retry).toHaveBeenCalled();
        });

        it('should be possible to cancel an in-flight retry by clicking button, and then to retry again by clicking again', () => {
            $timeout.flush(10000);
            expect(HttpQueue.prototype.retry).toHaveBeenCalled();
            HttpQueue.prototype.retry.mockClear();
            const cancelSpy = jest.spyOn(modalScope, 'cancelRetryRequest');

            SpecHelper.expectElementText(elem, '.disconnected button', 'Cancel');
            SpecHelper.expectElementText(elem, '.disconnected .message', 'We are sending the message again.');
            SpecHelper.click(elem, '.disconnected button');

            expect(cancelSpy).toHaveBeenCalled();
            expect(HttpQueue.prototype.retry).not.toHaveBeenCalled();

            SpecHelper.expectElementText(elem, '.disconnected button', 'Retry now');
            SpecHelper.expectElementText(elem, '.disconnected .message', 'A message failed to reach the server.');
            SpecHelper.click(elem, '.disconnected button');
            expect(HttpQueue.prototype.retry).toHaveBeenCalled();
        });

        it('should retry again after a failure', () => {
            $timeout.flush(10000);
            expect(HttpQueue.prototype.retry).toHaveBeenCalled();
            HttpQueue.prototype.retry.mockClear();
            rejectRetry({
                status: 0,
                config: {
                    url: '/api/',
                },
            });
            $timeout.flush(0);

            // after 9 seconds it should not have been retried again
            $timeout.flush(9000);
            expect(HttpQueue.prototype.retry).not.toHaveBeenCalled();

            // after 10 seconds it should be retried
            $timeout.flush(1000);
            expect(HttpQueue.prototype.retry).toHaveBeenCalled();
        });

        it('should resolve if the retry succeeds', () => {
            jest.spyOn(modalScope, 'resolve').mockImplementation(() => {});
            $timeout.flush(10000);
            resolveRetry({
                config: {
                    url: '/api/',
                },
            });
            $timeout.flush(0);
            expect(modalScope.resolve).toHaveBeenCalled();
        });

        it('should apply special skin and hide the splash in Cordova if the app has not yet initialized', () => {
            const isOnline = false;
            $window.CORDOVA = {};
            jest.spyOn(NetworkConnection, 'online', 'get').mockImplementation(() => isOnline);

            navigator.splashscreen = {
                hide: jest.fn(),
            };
            jest.spyOn($location, 'path').mockImplementation(() => {});

            handleDisconnectedError();

            const modalElem = $('.server-error-modal');
            expect(modalElem.hasClass('disconnected-modal-not-initialized')).toBe(true);
            expect(navigator.splashscreen.hide).toHaveBeenCalled();
            expect($location.path).toHaveBeenCalledWith('/disconnected-mobile-init');
        });

        function handleDisconnectedError() {
            DialogModal.removeAlerts();

            HttpQueue.prototype.retry.mockClear();
            handleErrorAndAssertModal('public', {
                status: 0,
            });
        }
    });

    describe('with an unhandled status code', () => {
        it('should force back to the homepage when the button is clicked', () => {
            handleErrorAndAssertModal('public', {
                status: 500,
            });
            expectFatalError();
        });

        it('should log to ErrorLogService.notify', () => {
            handleErrorAndAssertModal('public', {
                status: 500,
                config: {
                    url: 'https://abc.com/api/endpoint/id?asdasdasdasd',
                    method: 'PUT',
                },
            });

            expect(ErrorLogService.notify).toHaveBeenCalled();
            const args = ErrorLogService.notify.mock.calls[0];
            expect(args[0].message).toBe('Received status code 500 on a PUT to api/endpoint');
            expect(args[1]).toBeUndefined(); // cause
            expect(args[2]).toEqual({
                label: 'https://abc.com/api/endpoint/id',
                fingerprint: ['api/endpoint', 'PUT', 500],
                server_error: angular.extend(
                    {
                        path: 'https://abc.com/api/endpoint/id',
                        message: undefined,
                    },
                    _.omit(response, 'handledByFrontRoyalApiErrorHandler'),
                ),
            });
        });
    });

    describe('with a 503', () => {
        it('should force a refresh when the button is clicked', () => {
            handleErrorAndAssertModal('public', {
                status: 503,
            });
            expectFatalError(
                'Our servers are currently down. Engineers are working to fix the issue. We apologize for the inconvenience.',
            );
        });
    });

    describe('with a 409', () => {
        it('should reject the promise for handling at the site of the request call', () => {
            jest.spyOn(HttpQueue.prototype, 'unfreezeAfterError').mockImplementation(() => {});
            handleError('public', {
                status: 409,
            });
            expect(originalPromiseRejected).toBe(true);
            expect(originalPromiseRejectedWith).not.toBeUndefined(); // should reject with the response
            expect(HttpQueue.prototype.unfreezeAfterError).toHaveBeenCalled();
        });
    });

    describe('with a not_logged_in', () => {
        it('should use Hippos to attempt to log back in and do nothing if successful', () => {
            jest.spyOn(Hippos.prototype, 'go').mockReturnValue($q.when());

            response = handleErrorAndExpectNoModal('public', {
                status: 401,
                data: {
                    not_logged_in: true,
                },
            });

            expect(Hippos.prototype.go).toHaveBeenCalledWith(response);
        });

        it('should use Hippos to attempt to log back in and handle the error if the login succeeds but the retried request fails', () => {
            jest.spyOn(Hippos.prototype, 'go').mockReturnValue(
                $q((respond, reject) => {
                    reject({
                        failureType: 'retry',
                        response: {
                            status: 500,
                            config: {
                                url: '/api/',
                            },
                        },
                    });
                }),
            );

            handleErrorAndAssertModal('public', {
                status: 401,
                data: {
                    not_logged_in: true,
                },
            });

            // the initial failure was a 401, but the 500 is shown, because after the
            // auto-login there was a retry, and it failed with a 500
            expectFatalError();
        });

        describe('with failed autologin', () => {
            beforeEach(() => {
                jest.spyOn(Hippos.prototype, 'go').mockReturnValue(
                    $q((respond, reject) => {
                        reject({
                            failureType: 'login',
                            response: {},
                        });
                    }),
                );
            });

            describe('on a public page', () => {
                it('should attempt a sign-in when the button is clicked', () => {
                    jest.spyOn($location, 'search');
                    handleErrorAndAssertModal('public', {
                        status: 401,
                        data: {
                            not_logged_in: true,
                        },
                    });
                    expectAuthError();
                    expect($location.search).toHaveBeenCalledWith('target', 'NONE');
                });
            });

            describe('with an institutional subdomain', () => {
                it('should show a link to loginWithProvider', () => {
                    $injector.get('institutionalSubdomain').id = 'some-institution';
                    const omniauth = $injector.get('omniauth');

                    handleErrorAndAssertModal('public', {
                        status: 401,
                        data: {
                            not_logged_in: true,
                        },
                    });

                    jest.spyOn(omniauth, 'loginWithProvider').mockImplementation(() => {});
                    SpecHelper.click(elem, 'a');
                    expect(omniauth.loginWithProvider).toHaveBeenCalledWith('some-institution');
                });
            });

            ['editor', 'admin', 'reports'].forEach(site => {
                describe(`on the ${site} pages`, () => {
                    it('should retry if the login-success event is observed', () => {
                        handleErrorAndAssertModal(site, {
                            status: 401,
                            data: {
                                not_logged_in: true,
                            },
                        });
                        assertRetried(() => {
                            SpecHelper.click(elem, 'button[type="submit"]');
                        });
                    });
                });
            });

            function assertRetried(func) {
                const deferred = $q.defer();
                jest.spyOn($auth, 'submitLogin').mockReturnValue(deferred.promise);

                SpecHelper.expectElementText(elem, 'button[type="submit"]', 'Login');
                SpecHelper.expectElementDisabled(elem, 'button[type="submit"]');

                SpecHelper.updateTextInput(elem, 'input[name="email"]', 'someuser@email.com');
                SpecHelper.updateTextInput(elem, 'input[name="password"]', 'password');
                func();
                SpecHelper.expectElementDisabled(elem, 'button[type="submit"]');
                deferred.resolve();

                let resolveRetry;
                const retryPromise = $q(_resolve => {
                    resolveRetry = _resolve;
                });
                jest.spyOn(HttpQueue.prototype, 'retry').mockReturnValue(retryPromise);

                $rootScope.$emit('validation-responder:login-success');
                expect(HttpQueue.prototype.retry).toHaveBeenCalledWith(response.config);

                // when the retry succeeds, the original promise should be resolved
                resolveRetry();
                $timeout.flush();
                expect(originalPromiseResolved).toBe(true);
            }
        });
    });

    describe('with an unauthorized error', () => {
        describe('on public site', () => {
            it('should got to homepage when the button is clicked', () => {
                handleErrorAndAssertModal('public', {
                    status: 401,
                });
                expectFatalError(
                    'You do not have permission to view this content. Please click to return to the dashboard.',
                    $rootScope.homePath,
                );
            });
        });
        describe('on the editor page', () => {
            it('should dismiss the modal with button is clicked', () => {
                handleErrorAndAssertModal('editor', {
                    status: 401,
                });
                jest.spyOn(HttpQueue.prototype, 'unfreezeAfterError').mockImplementation(() => {});
                jest.spyOn($location, 'url').mockImplementation(() => {});
                SpecHelper.expectElementText(
                    elem,
                    '.message',
                    'You do not have permission to do that. Please click to continue.',
                );
                SpecHelper.click(elem, 'button[name="dismiss"]');
                expect(originalPromiseRejected).toBe(true);
                expect($location.url).not.toHaveBeenCalled();
                expect(HttpQueue.prototype.unfreezeAfterError).toHaveBeenCalled();
            });
        });
    });

    describe('with a 406 on the editor page', () => {
        it('should be dismissed when the button is clicked', () => {
            handleErrorAndAssertModal('editor', {
                status: 406,
                data: {
                    message: 'Validation failed',
                },
            });
            jest.spyOn(HttpQueue.prototype, 'unfreezeAfterError').mockImplementation(() => {});
            SpecHelper.click(elem, 'button[name="dismiss"]');
            expect(originalPromiseRejected).toBe(true);
            expect(HttpQueue.prototype.unfreezeAfterError).toHaveBeenCalled();
        });
    });

    describe('with a 500 on the editor page', () => {
        it('should not force back to the homepage', () => {
            handleErrorAndAssertModal('editor', {
                status: 500,
                data: {
                    message: 'Internal Server Error',
                },
            });
            jest.spyOn(HttpQueue.prototype, 'unfreezeAfterError').mockImplementation(() => {});
            SpecHelper.click(elem, 'button[name="dismiss"]');
            expect(originalPromiseRejected).toBe(true);
            expect(HttpQueue.prototype.unfreezeAfterError).toHaveBeenCalled();
        });
    });

    describe('skip', () => {
        it('should be possible to disable with the skip param', () => {
            response = {};
            response.config = {
                url: '/api/',
                'FrontRoyal.ApiErrorHandler': {
                    skip: true,
                },
            };
            $rootScope.site = () => 'public';
            originalPromise = ApiErrorHandler.onResponseError(response);
            originalPromise.then(
                () => {
                    originalPromiseResolved = true;
                },
                () => {
                    originalPromiseRejected = true;
                },
            );
            $timeout.flush();

            expect(originalPromiseRejected).toBe(true);
            expect(originalPromiseResolved).toBe(false);
            elem = $('.server-error-modal');
            expect(elem.length).toBe(0);
        });
    });

    describe('redirectOnLoggedOut', () => {
        it('should be possible to redirect on logged out with a param', () => {
            response = {
                status: 401,
                data: {
                    not_logged_in: true,
                },
            };
            response.config = {
                url: '/api/',
                'FrontRoyal.ApiErrorHandler': {
                    redirectOnLoggedOut: true,
                },
            };
            $rootScope.site = () => 'public';

            jest.spyOn(Hippos.prototype, 'go').mockReturnValue(
                $q((respond, reject) => {
                    reject({
                        failureType: 'login',
                        response: {},
                    });
                }),
            );

            jest.spyOn($location, 'url').mockImplementation(() => 'foobar');
            jest.spyOn(HttpQueue.prototype, 'unfreezeAfterError').mockImplementation(() => {});

            originalPromise = ApiErrorHandler.onResponseError(response);
            originalPromise.then(
                () => {
                    originalPromiseResolved = true;
                },
                () => {
                    originalPromiseRejected = true;
                },
            );
            $timeout.flush();

            expect(originalPromiseRejected).toBe(true);
            expect(originalPromiseResolved).toBe(false);
            elem = $('.server-error-modal');
            expect(elem.length).toBe(0);

            expect($location.url).toHaveBeenCalledWith('/sign-in');
            expect(HttpQueue.prototype.unfreezeAfterError).toHaveBeenCalled();
        });
    });

    describe('showFatalError', () => {
        it('should show a fatal error with the supplied message', () => {
            ApiErrorHandler.showFatalError(
                {
                    config: {
                        url: '/api/',
                    },
                },
                'a message',
            );
            $timeout.flush();
            expectFatalError('a message');
        });
    });

    function handleErrorAndExpectNoModal(site, _response) {
        response = _response;
        response.config = {
            url: '/api/',
        };
        $rootScope.site = () => site;
        originalPromise = ApiErrorHandler.onResponseError(response);
        originalPromise.then(
            () => {
                originalPromiseResolved = true;
            },
            () => {
                originalPromiseRejected = true;
            },
        );
        $timeout.flush();
        return response;
    }

    function handleError(site, _response) {
        response = _response;
        response.config = _.extend(
            {
                url: '/api/',
                method: undefined,
                httpQueue: undefined,
            },
            response.config || {},
        );
        $rootScope.site = () => site;
        originalPromise = ApiErrorHandler.onResponseError(response);
        originalPromise.then(
            () => {
                originalPromiseResolved = true;
            },
            rejectedWith => {
                originalPromiseRejected = true;
                originalPromiseRejectedWith = rejectedWith;
            },
        );
        $timeout.flush(50);
    }

    function handleErrorAndAssertModal(site, _response) {
        handleError(site, _response);
        assertModal();
        return response;
    }

    function assertModal() {
        elem = $('.server-error-modal');
        expect(elem.length).toBe(1, 'error modal not found');
        modalScope = elem.find('[data-id="contents"] > *').isolateScope();
    }

    function expectFatalError(message, target) {
        assertModal();
        jest.spyOn(modalScope, 'reload').mockImplementation(() => {});
        if (message) {
            SpecHelper.expectElementText(elem, '.message', message);
        }

        // see comment in the directive code
        expect(originalPromiseRejected).toBe(false);
        SpecHelper.click(elem, 'button[name="continue"]');
        expect(modalScope.reload).toHaveBeenCalledWith(target);
    }

    function expectAuthError() {
        const deferred = $q.defer();
        jest.spyOn($auth, 'submitLogin').mockReturnValue(deferred.promise);

        SpecHelper.updateTextInput(elem, 'input[name="email"]', 'someuser@email.com');
        SpecHelper.updateTextInput(elem, 'input[name="password"]', 'password');
        SpecHelper.click(elem, 'button[type="submit"]');

        // see comment in the directive code
        expect(originalPromiseRejected).toBe(false);

        expect($auth.submitLogin).toHaveBeenCalled();
        deferred.resolve();
    }
});
