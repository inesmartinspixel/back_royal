import 'AngularSpecHelper';
import 'FrontRoyalApiErrorHandler/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import disconnectedMobileInitLocales from 'FrontRoyalApiErrorHandler/locales/front_royal_api_error_handler/disconnected_mobile_init-en.json';

setSpecLocales(disconnectedMobileInitLocales);

// see also: `front_royal_api_error_handler_spec.js` for more comprehensive test with interplay between retry / queue / logging / etc
describe('FrontRoyal.ApiErrorHandler.disconnectedMobileInit', () => {
    let SpecHelper;
    let elem;
    let $rootScope;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.ApiErrorHandler', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            $rootScope = $injector.get('$rootScope');
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render(disconnectedMessages) {
        const renderer = SpecHelper.renderer();
        renderer.scope.disconnectedMessages = disconnectedMessages;
        renderer.render(
            '<disconnected-mobile-init disconnected-messages="disconnectedMessages"></disconnected-mobile-init>',
        );
        elem = renderer.elem;
    }

    it('should display a message', () => {
        render();
        SpecHelper.expectElementText(
            elem,
            '.message',
            'Your network appears to be offline. Please enable a Wi-Fi or cellular connection to continue.',
        );
    });

    it('should support alternative messages', () => {
        render(['message 1', 'message2']);
        SpecHelper.expectElementText(elem, '.message', 'message 1message2');
    });

    it('should monitor networkBootstrapStarted status', () => {
        render();
        SpecHelper.expectElementText(
            elem,
            '.message',
            'Your network appears to be offline. Please enable a Wi-Fi or cellular connection to continue.',
        );

        $rootScope.networkBootstrapStarted = true;
        $rootScope.$digest();

        SpecHelper.expectElementText(elem, '.message', 'Connection detected!Loading ...');
    });
});
