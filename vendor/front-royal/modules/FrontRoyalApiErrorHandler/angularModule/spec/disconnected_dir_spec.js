import 'AngularSpecHelper';
import 'FrontRoyalApiErrorHandler/angularModule';

import NetworkConnection from 'NetworkConnection';
import setSpecLocales from 'Translation/setSpecLocales';
import disconnectedLocales from 'FrontRoyalApiErrorHandler/locales/front_royal_api_error_handler/disconnected-en.json';

setSpecLocales(disconnectedLocales);

// see also: `front_royal_api_error_handler_spec.js` for more comprehensive test with interplay between retry / queue / logging / etc
describe('FrontRoyal.ApiErrorHandler.apiErrorDisconnected', () => {
    let SpecHelper;
    let elem;
    let scope;
    let $window;
    let $rootScope;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.ApiErrorHandler', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            $window = $injector.get('$window');
            $rootScope = $injector.get('$rootScope');
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        const renderer = SpecHelper.renderer();
        renderer.render('<api-error-disconnected></api-error-disconnected>');
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    describe('appInitialized', () => {
        afterEach(() => {
            delete $window.CORDOVA;
        });

        it('should set appInitialized based on Cordova app initialization status', () => {
            const isOnline = false;
            $window.CORDOVA = {};
            jest.spyOn(NetworkConnection, 'online', 'get').mockImplementation(() => isOnline);
            $rootScope.networkBootstrapCompleted = false;

            render();
            expect(scope.appInitialized).toBe(false);
            SpecHelper.expectElement(elem, 'disconnected-mobile-init');

            $rootScope.networkBootstrapCompleted = true;
            render();
            expect(scope.appInitialized).toBe(true);
            SpecHelper.expectNoElement(elem, 'disconnected-mobile-init');
        });
    });
});
