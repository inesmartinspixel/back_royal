import { angular2react } from 'angular2react';

// https://github.com/bcherny/angular2react-demos/blob/master/multi-file/src/lazyInjector.js

/**
 * Angular2react needs an $injector, but it doesn't actually invoke $injector.get
 * until we invoke ReactDOM.render. We can take advantage of this to provide the
 * "lazy" injector below to React components created with angular2react, as a way
 * to avoid component ordering issues.
 *
 * @see https://github.com/coatue-oss/angular2react/issues/12
 */

// state

function getLazyInjector() {
    let $injector;

    const lazyInjector = {
        get $injector() {
            return {
                get get() {
                    return $injector.get;
                },
            };
        },
        set $injector(_$injector) {
            $injector = _$injector;
        },
    };
    return lazyInjector;
}

function angularDirectiveToReact(moduleName, directiveName, directiveDefinitionOrBindings) {
    const bindings = directiveDefinitionOrBindings.scope || directiveDefinitionOrBindings;
    const mockComponent = {
        bindings,
    };
    const lazyInjector = getLazyInjector();

    angular.module(moduleName).run([
        '$injector',
        function (_$injector) {
            lazyInjector.$injector = _$injector;
        },
    ]);

    return angular2react(directiveName, mockComponent, lazyInjector.$injector);
}

export { angularDirectiveToReact };
