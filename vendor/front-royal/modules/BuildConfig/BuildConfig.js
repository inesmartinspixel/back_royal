const config = {
    build_number: 'APPLICATION_BUILD_NUMBER_CI_STAMPED',
    build_timestamp: 'APPLICATION_BUILD_TIMESTAMP_CI_STAMPED',
    build_commit: 'APPLICATION_COMMIT_CI_STAMPED',
};

// locally, the build number is always 4122 (which allows us to load non-English content locally)
if (Number.isNaN(parseInt(config.build_number, 10))) {
    config.build_number = '4122';
}

export default config;
