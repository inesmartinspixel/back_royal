import {
    formattedTelephoneNumber,
    formattedTelephonePlaceholder,
    formattedTelephoneOrText,
} from './TelephoneFormatter';

import PhoneFormat from './PhoneFormat';

export { formattedTelephoneNumber, formattedTelephonePlaceholder, formattedTelephoneOrText, PhoneFormat };
