import 'AngularSpecHelper';
import 'Telephone/angularModule';
import telInputLocales from 'Telephone/locales/telephone/tel_input-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(telInputLocales);

describe('FrontRoyal.Telephone.TelInputDir', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let $timeout;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Telephone', 'SpecHelper');

        angular.mock.inject([
            '$injector',

            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                $timeout = $injector.get('$timeout');
            },
        ]);
    });

    function render(model) {
        renderer = SpecHelper.renderer();

        const mockForm = {
            ngModel: model,
        };

        renderer.scope.errors = {};
        renderer.scope.ngModel = mockForm.ngModel;
        renderer.render('<form name="spec_form"><tel-input ng-model="ngModel" errors="errors"></tel-input></form>');
        elem = renderer.elem;

        // the onchange occurs after a timeout
        $timeout.flush();
    }

    afterEach(() => {
        SpecHelper.cleanup();
    });

    it('should render with defaults', () => {
        render();
        SpecHelper.updateSelect(elem, 'select[name="countryCode"]', 'US');
        const input = SpecHelper.expectElement(elem, 'input[name="phone"]');
        expect(input.attr('placeholder')).toBe('(201) 555-0123');
        expect(input.val()).toBe('');
        SpecHelper.assertSelectValue(elem, 'select', 'US');
        SpecHelper.expectElementText(elem, '.selected-prefix', '+1');
    });

    it('should initialize correctly given a value', () => {
        render('+27119785313');
        SpecHelper.expectElement(elem, 'select[name="countryCode"]');
        const input = SpecHelper.expectElement(elem, 'input[name="phone"]');
        expect(input.attr('placeholder')).toBe('071 123 4567');
        expect(input.val()).toBe('011 978 5313');
        SpecHelper.assertSelectValue(elem, 'select', 'ZA');
        SpecHelper.expectElementText(elem, '.selected-prefix', '+27');
    });

    it('should have multiple country codes available for selection', () => {
        render();
        const options = SpecHelper.expectElements(elem, 'option', 249);
        expect(options.eq(1).attr('value')).toBe('string:AF');
        expect(options.eq(1).text()).toBe('Afghanistan (+93)');
        expect(options.eq(247).attr('value')).toBe('string:ZM');
        expect(options.eq(247).text()).toBe('Zambia (+260)');
    });

    it('should update the placeholder on selection', () => {
        render();
        SpecHelper.updateSelect(elem, 'select[name="countryCode"]', 'FR');
        const input = SpecHelper.expectElement(elem, 'input[name="phone"]');
        expect(input.attr('placeholder')).toBe('06 12 34 56 78');
        SpecHelper.assertSelectValue(elem, 'select', 'FR');
        SpecHelper.expectElementText(elem, '.selected-prefix', '+33');
    });

    it('should validate on change', () => {
        render();
        const controller = elem.find('tel-input').controller('ngModel');
        expect(controller.$valid).toBe(true);
        SpecHelper.updateSelect(elem, 'select[name="countryCode"]', 'US');
        SpecHelper.updateInput(elem, 'input[name="phone"]', '(201) 555-');
        expect(controller.$valid).toBe(false);
        SpecHelper.updateInput(elem, 'input[name="phone"]', '(201) 555-0123');
        expect(controller.$valid).toBe(true);
    });

    it('should should update errors if provided and invalid', () => {
        render();
        const controller = elem.find('tel-input').controller('form');
        SpecHelper.updateInput(elem, 'input[name="phone"]', '(201) 555-');
        expect(controller.$valid).toBe(false);
        expect(renderer.scope.errors).toEqual({
            phone: 'Invalid phone number',
        });
    });
});
