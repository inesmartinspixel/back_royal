import telephoneValidation from './TelephoneValidation';
import telephoneInput from './TelephoneInput';
import 'Translation/angularModule';

const telephoneModule = angular.module('FrontRoyal.Telephone', ['Translation']);
telephoneModule.directive('telInput', ['$injector', telephoneInput]);
telephoneModule.directive('telValidation', ['$injector', telephoneValidation]);

export default telephoneModule;
