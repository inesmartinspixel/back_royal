import { formattedTelephoneNumber } from './TelephoneFormatter';

describe('TelephoneFormatter', () => {
    describe('formattedTelephoneNumber', () => {
        it('should return undefined if undefined is being filtered', () => {
            expect(formattedTelephoneNumber(undefined)).toBe(undefined);
        });

        it('should return null if null is being filtered', () => {
            expect(formattedTelephoneNumber(null)).toBe(null);
        });
    });
});
