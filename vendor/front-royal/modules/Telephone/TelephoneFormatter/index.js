import {
    formattedTelephoneNumber,
    formattedTelephonePlaceholder,
    formattedTelephoneOrText,
    // eslint-disable-next-line import/extensions
} from './TelephoneFormatter.js';

export { formattedTelephoneNumber, formattedTelephonePlaceholder, formattedTelephoneOrText };
