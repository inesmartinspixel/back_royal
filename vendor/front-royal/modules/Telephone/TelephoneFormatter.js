import PhoneFormat from './PhoneFormat';

// can filter in either local (for intuitive form input) or international formats (reporting)
function formattedTelephoneNumber(number, countryCode, internationalFormat) {
    if (!number) {
        return number;
    }

    // Examples:
    // United States: 8646978257
    // United States: +18646978257
    // South Africa: +27119785313

    const cleanNumber = PhoneFormat.cleanPhone(number);

    // attempt to discern the country code from the number if not explicitly provided.
    // this will probably only happen in a display context, since all input would
    // be more intuitive in local input mode.
    if (!countryCode) {
        // default to US
        countryCode = 'US';

        // if the cleaned number had a + (required for E164 format) then get the
        // country code from it
        if (cleanNumber.includes('+')) {
            countryCode = PhoneFormat.countryForE164Number(cleanNumber);
        }
    }

    // handle conditional formatting
    if (internationalFormat) {
        return PhoneFormat.formatInternational(countryCode, cleanNumber);
    }
    return PhoneFormat.formatLocal(countryCode, cleanNumber);
}

function formattedTelephonePlaceholder(countryCode) {
    if (!countryCode) {
        return undefined;
    }

    const exampleNumber = PhoneFormat.exampleMobileNumber(countryCode);
    return PhoneFormat.formatLocal(countryCode, exampleNumber);
}

function formattedTelephoneOrText(input) {
    // If null or undefined input
    if (!input) {
        return input;
    }

    // If no country code is found
    const countryCode = PhoneFormat.countryForE164Number(input);
    if (!countryCode) {
        return input;
    }

    // If number is not valid
    const valid = PhoneFormat.isValidNumber(input, countryCode);
    if (!valid) {
        return input;
    }

    // Formatted number
    return PhoneFormat.formatInternational(countryCode, input);
}

export { formattedTelephoneNumber, formattedTelephonePlaceholder, formattedTelephoneOrText };
