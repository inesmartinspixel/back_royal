import { fetch as fetchPolyfill } from 'whatwg-fetch'; // IE11

const fetch = window.fetch && typeof window.fetch === 'function' ? window.fetch : fetchPolyfill;

export default fetch;
