import angularModule from 'Authentication/angularModule/scripts/authentication_module';
import template from 'Authentication/angularModule/views/forgot_password.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('forgotPassword', [
    '$injector',

    function factory($injector) {
        const $auth = $injector.get('$auth');
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
        const TranslationHelper = $injector.get('TranslationHelper');
        return {
            restrict: 'E',
            templateUrl,
            scope: {
                urlPrefix: '@',
            },
            link(scope) {
                const translationHelper = new TranslationHelper('authentication.forgot_password');

                NavigationHelperMixin.onLink(scope);

                scope.preventSubmit = false;
                scope.form = {};
                scope.form_errors = {};
                scope.submitted = false;

                scope.requestPasswordReset = () => {
                    scope.preventSubmit = true;

                    $auth.requestPasswordReset(scope.form).then(
                        () => {
                            scope.preventSubmit = false;
                            scope.submitted = true;
                        },
                        resp => {
                            scope.preventSubmit = false;
                            scope.submitted = false;

                            if (resp && resp.data && resp.data.errors && resp.data.errors.length > 0) {
                                scope.form_errors.email = resp.data.errors[0];
                            } else {
                                scope.form_errors.email = translationHelper.get('unable_to_process_your_request');
                            }
                        },
                    );
                };
            },
        };
    },
]);
