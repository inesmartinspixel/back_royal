import angularModule from 'Authentication/angularModule/scripts/authentication_module';
import { getBrandEmail } from 'AppBrandMixin';
import moment from 'moment-timezone';

angularModule.factory('AuthFormHelperMixin', [
    '$injector',

    function factory($injector) {
        const $q = $injector.get('$q');
        const $rootScope = $injector.get('$rootScope');
        const $http = $injector.get('$http');
        const $auth = $injector.get('$auth');
        const $window = $injector.get('$window');
        const omniauth = $injector.get('omniauth');
        const ConfigFactory = $injector.get('ConfigFactory');
        const blockAuthenticatedAccess = $injector.get('blockAuthenticatedAccess');
        const $location = $injector.get('$location');
        const Hippos = $injector.get('Hippos');
        const DialogModal = $injector.get('DialogModal');
        const TranslationHelper = $injector.get('TranslationHelper');
        const ErrorLogService = $injector.get('ErrorLogService');
        const ClientStorage = $injector.get('ClientStorage');
        const EventLogger = $injector.get('EventLogger');
        const preSignupValues = $injector.get('preSignupValues');
        const $timeout = $injector.get('$timeout');

        const isCustomCodeError = error => _.contains(['error_account_deactivated', 'error_hiring_web_only'], error);
        const translationHelper = new TranslationHelper('authentication.auth_form_helper_mixin');

        return {
            onLink(scope, onOmniauthSuccess, onOmniauthFailure) {
                scope.preventSubmit = false;

                scope.form_errors = {};

                if (blockAuthenticatedAccess.block) {
                    scope.form_errors.general = blockAuthenticatedAccess.errorMessage;
                    scope.preventSubmit = true;
                }

                // registers the user with ng-token-auth and signal login success
                function handleAuthPassthrough(user, resolve) {
                    $auth.initDfd();
                    $auth.handleValidAuth(user, true).then(() => {
                        $rootScope.$broadcast('auth:login-success', user);
                        resolve();
                    });
                }

                function showDialogModalWithErrorMessage(errorMessage) {
                    if (!angular.isDefined(errorMessage)) {
                        return;
                    }

                    DialogModal.alert({
                        content: errorMessage, // should already be localized
                        classes: ['server-error-modal'],
                        title: translationHelper.get('server_error'),
                    });
                }

                // If there are errors during omniauth login, then
                // the will be included as a queryparam.  I would like
                // to remove the query param, but for some reason
                // I saw this directive rendered twice consecutively.  If
                // the first one deletes the query param, then there will
                // be no message. (We need to uniq because repeated failures dupe the message)
                if ($location.search().error) {
                    const error = $location.search().error;

                    if (isCustomCodeError(error)) {
                        showDialogModalWithErrorMessage(
                            translationHelper.get(error, {
                                brandEmail: getBrandEmail(undefined, 'support', ConfigFactory.getSync()),
                            }),
                        );
                    } else {
                        scope.form_errors = scope.form_errors || {};
                        scope.form_errors.general = Array.isArray(error) ? _.uniq(error) : error;
                    }
                }

                function isIgnorableError(callbackError) {
                    return (
                        callbackError &&
                        callbackError.data &&
                        _.contains(['error_account_deactivated', 'error_hiring_web_only'], callbackError.data.message)
                    );
                }

                function handleApplicationAuth(provider, params, resolve, reject) {
                    const receivedMalformedButSuccessfulErrorMessage =
                        'Received malformed but successful callback response (native)';
                    const failedToRetrieveUserRecordErrorMessage = 'Failed to retrieve user record with token (native)';

                    // make endpoint call to update user and return auth_token values
                    $http({
                        url: `${window.ENDPOINT_ROOT}/native_oauth/${provider}/callback.json`,
                        method: 'GET',
                        params,
                    })
                        .then(response => {
                            let user;

                            // ensure we don't have a malformed / invalid 200 response
                            if (response && response.data && response.data.data) {
                                user = response.data.data;
                            } else {
                                ErrorLogService.notify(receivedMalformedButSuccessfulErrorMessage, undefined, {
                                    response: JSON.stringify(response),
                                    provider,
                                });
                                reject('Missing user record');
                                return;
                            }

                            // finish ng-token-auth authentication
                            handleAuthPassthrough(user, resolve);
                        })
                        .catch(callbackError => {
                            if (!isIgnorableError(callbackError)) {
                                ErrorLogService.notify(failedToRetrieveUserRecordErrorMessage, undefined, {
                                    callbackError: JSON.stringify(callbackError),
                                    provider,
                                });
                            }
                            reject(callbackError);
                        });
                }

                // needed on scope for tests
                scope.getProviderPromise = (provider, signUpCode, programType) => {
                    // programType is only passed through to omniauth login, not to native
                    // login.  This is ok, since it is only set on marketing pages and so
                    // will never be set in the native situation.

                    const failedToLoginErrorMessage = 'Failed to login (native)';
                    const failedToRetrieveAuthTokenErrorMessage = 'Failed to retrieve auth token from (native)';

                    let parsedError;

                    if (provider === 'facebook' && window.facebookConnectPlugin) {
                        return $q((resolve, reject) => {
                            // FIXME: https://trello.com/c/1MtttaEm/926-chore-investigate-double-callback-situation-in-authformhelpermixin
                            let loginCompleted = false;

                            // first validate login
                            window.facebookConnectPlugin.login(
                                ['public_profile', 'email'],
                                () => {
                                    // handle duplicate callback oddity
                                    if (loginCompleted) {
                                        return;
                                    }
                                    loginCompleted = true;

                                    // now get the token
                                    window.facebookConnectPlugin.getAccessToken(
                                        accessToken => {
                                            const params = {
                                                access_token: accessToken,
                                                sign_up_code: signUpCode,
                                                timezone: moment.tz.guess(),
                                            };
                                            handleApplicationAuth(provider, params, resolve, reject);
                                        },
                                        tokenError => {
                                            ErrorLogService.notify(failedToRetrieveAuthTokenErrorMessage, undefined, {
                                                tokenError: JSON.stringify(tokenError),
                                                provider,
                                            });
                                            reject(tokenError);
                                        },
                                    );
                                },
                                loginError => {
                                    parsedError = JSON.stringify(loginError);
                                    if (!parsedError.match('cancel')) {
                                        // reports as "cancelled" and "canceled" depending on version
                                        ErrorLogService.notify(failedToLoginErrorMessage, undefined, {
                                            parsedError,
                                            provider,
                                        });
                                    }
                                    reject(loginError);
                                },
                            );
                        });
                    }

                    if (provider === 'google_oauth2' && window.plugins && window.plugins.googleplus) {
                        return $q((resolve, reject) => {
                            // get the clientId required to get idToken values in the response
                            const webClientId = ConfigFactory.getSync().google_oauth_id;

                            // Google is a little annoying in that the webClient is needed, as well as the plugin configuration pointing to a
                            // SHA-1 mapped token generated from production certificates. ie - the webClientId needs to match the same "production"
                            // environment. Be sure you account for this in application.yml if testing native Google Sign-In
                            window.plugins.googleplus.login(
                                {
                                    // NOTE: this is only used by Android and will be reflected as the aud in identiy token
                                    webClientId,
                                },
                                providerResponse => {
                                    const params = {
                                        provider_data: providerResponse,
                                        sign_up_code: signUpCode,
                                        timezone: moment.tz.guess(),
                                    };
                                    handleApplicationAuth(provider, params, resolve, reject);
                                },
                                loginError => {
                                    parsedError = JSON.stringify(loginError);
                                    if (!parsedError.match('cancel')) {
                                        // reports as "cancelled" and "canceled" depending on version
                                        ErrorLogService.notify(failedToLoginErrorMessage, undefined, {
                                            parsedError,
                                            provider,
                                        });
                                    }
                                    reject(loginError);
                                },
                            );
                        });
                    }

                    if (
                        provider === 'apple' &&
                        window.cordova &&
                        window.cordova.plugins &&
                        window.cordova.plugins.SignInWithApple &&
                        window.device &&
                        window.device.platform === 'iOS' &&
                        parseFloat(window.device.version) >= 13
                    ) {
                        return $q((resolve, reject) => {
                            window.cordova.plugins.SignInWithApple.signin(
                                null,
                                providerResponse => {
                                    const params = {
                                        provider_data: providerResponse,
                                        sign_up_code: signUpCode,
                                        timezone: moment.tz.guess(),
                                    };
                                    handleApplicationAuth(provider, params, resolve, reject);
                                },
                                loginError => {
                                    parsedError = JSON.stringify(loginError);
                                    if (!parsedError.match('cancel')) {
                                        // reports as "cancelled" and "canceled" depending on version
                                        ErrorLogService.notify(failedToLoginErrorMessage, undefined, {
                                            parsedError,
                                            provider,
                                        });
                                    }
                                    reject(loginError);
                                },
                            );
                        });
                    }

                    // default to omniauth
                    return omniauth.loginWithProvider(provider, signUpCode, programType);
                };

                scope.logPreSignupForm = () => {
                    // the pixelmatter forms at the dynamic_landing_page may populate
                    // a $window.preSignupForm object
                    if (_.any($window.preSignupForm)) {
                        // The event that gets logged below gets sent to Facebook,
                        // which expects the event property values to be in human
                        // readable form.
                        const formPayload = {};
                        _.each(
                            preSignupValues.DYNAMIC_LANDING_PAGE_MULTI_STEP_FORM_KEYS_TO_VALUES_MAP,
                            (keyToValuesMap, dbColumn) => {
                                const dbValue = $window.preSignupForm[dbColumn];
                                formPayload[dbColumn] = keyToValuesMap[dbValue]?.text;
                            },
                        );
                        EventLogger.log(
                            'registration:completed_pre_signup_form',
                            angular.extend(
                                {
                                    label: 'registration:completed_pre_signup_form',
                                },
                                formPayload,
                            ),
                            {
                                segmentioType: 'registration:completed_pre_signup_form',
                                segmentioLabel: 'registration:completed_pre_signup_form',
                            },
                        );
                    }
                };

                scope.loginWithProvider = (provider, signUpCode, programType) => {
                    // We know that it is possible for the signUpCode to not be set yet in
                    // CORDOVA, but that shouldn't be a problem as we default the signUpCode on
                    // the server-side if it doesn't exist. See - https://trello.com/c/JDHMbJQr
                    if (!signUpCode && !$window.CORDOVA) {
                        ErrorLogService.notify('loginWithProvider called with no signUpCode');
                    }

                    scope.preventSubmit = true;

                    EventLogger.log('auth:login_with_provider', { provider });
                    scope.logPreSignupForm();

                    // Since logging in with oauth will send us away from the page,
                    // try to make extra sure these events get logged.
                    EventLogger.tryToSaveBuffer();

                    // OAuth providers have mixed whitelisting allowances, and in some instances will
                    // not allow us to suport dynamic query strings. As such, we'll translate redirect
                    // target requests into a form that will persist throughout a desktop browser OAuth
                    // authentication flow (redirects to provider / redirects back)
                    // Note that the marketing_signup_form may override this.
                    if (!$window.CORDOVA) {
                        // the sign up forms on the dynamic_landing_page override the redirect target so that
                        // the user ends up back on that page viewing the last step of
                        // the form with the success message
                        const redirectTarget = scope.oauthRedirectTarget || $location.search().target;
                        if (redirectTarget) {
                            ClientStorage.setItem('oauth_redirect_target', redirectTarget);
                        }
                    }

                    // Note: In a browser, the callbacks never get hit because
                    // you have been forwarded away to a different page.  In cordova,
                    // behavior may be different. (I'm not sure right now)
                    // -----
                    // Note: we timeout for a beat so that tryToSaveBuffer will succeed in
                    // sending off all events (we could wait for it to finish, but that
                    // seems unnecessary).  Specifically, we want to login_with_provider event
                    // and the registration:completed_pre_signup_form event.
                    $timeout(100)
                        .then(() => scope.getProviderPromise(provider, signUpCode, programType))
                        .then(function providerSuccess() {
                            scope.preventSubmit = false;
                            scope.form_errors = {};
                            if (onOmniauthSuccess) {
                                onOmniauthSuccess.call(this);
                            }
                        })
                        .catch(function providerFailure(errorMessage) {
                            scope.preventSubmit = false;

                            // clear any temporary redirect targets
                            ClientStorage.removeItem('oauth_redirect_target');

                            // validate this isn't just the user cancelling the dialog
                            const errorJSON = JSON.stringify(errorMessage);
                            if (errorJSON && errorJSON.toLowerCase().includes('user cancel')) {
                                return;
                            }

                            if (onOmniauthFailure) {
                                onOmniauthFailure.call(this, errorMessage);
                            }
                        });
                };

                scope.formErrorClasses = fieldName => {
                    if (scope.form_errors[fieldName]) {
                        return ['form-error', 'active'];
                    }
                    return ['form-error'];
                };

                scope.formValidationClasses = fieldName => {
                    if (scope.form_errors[fieldName]) {
                        return ['form-control', 'ng-invalid'];
                    }
                    return ['form-control'];
                };

                scope.uniqueErrors = errors => {
                    if (_.isArray(errors)) {
                        errors = _.uniq(errors).join('\n');
                    }
                    return errors;
                };

                scope.submitLogin = signInInfo => {
                    scope.preventSubmit = true;
                    scope.form_errors = {};

                    // when coming from the marketing sign up form, this
                    // will already be set to hiring_team_invite
                    if (!signInInfo.provider) {
                        signInInfo.provider = scope.urlConfig.provider;
                    }

                    if (window.CORDOVA?.miyaMiya) {
                        signInInfo.client = 'miya_miya';
                    }

                    return $auth
                        .submitLogin(signInInfo)
                        .then(() => {
                            Hippos.save(signInInfo);
                            scope.preventSubmit = false;
                        })
                        .catch(() => {
                            scope.preventSubmit = false;
                        });
                };

                // ng-token-auth sanitizes all sign-in errors as "Invalid Credentials"
                // but we have a specific use-case where we want to suggest OAuth
                // providers. It's leaky, but provides a nice UX.
                const errorListener = $rootScope.$on('auth:login-error', (event, eventResponse) => {
                    // these should all be localized at this point
                    if (eventResponse && eventResponse.errors && eventResponse.errors[0]) {
                        // Don't set the general error message if it's a custom response
                        if (!isCustomCodeError(eventResponse.errors[0])) {
                            scope.form_errors.general = eventResponse.errors[0];
                        }
                    } else if (eventResponse) {
                        // On a failed invalid user login attempt the server will return a message string for
                        // eventResponse rather than an object. This distinction allows us to differentiate between
                        // the different server responses.

                        if (isCustomCodeError(eventResponse)) {
                            eventResponse = translationHelper.get(eventResponse, {
                                brandEmail: getBrandEmail(undefined, 'support', ConfigFactory.getSync()),
                            });
                        }

                        // The listener on 'auth:login-error' events overrides front-royal-api-error-handler,
                        // so we need to explicity create the DialogModal alert.
                        showDialogModalWithErrorMessage(eventResponse);
                    }
                });

                scope.$on('$destroy', () => {
                    if (errorListener) {
                        errorListener();
                    }
                });
            },

            // Provides an error responder capable of dealing with our special-case error codes
            getCustomOmniauthErrorResponder(scope, fieldName) {
                fieldName = fieldName || 'general';

                const getCustomErrorMessage = errorResponse => {
                    if (!errorResponse) {
                        return null;
                    }
                    if (errorResponse.data && errorResponse.data.message) {
                        return errorResponse.data.message;
                    }
                    if (errorResponse.errors && errorResponse.errors[0]) {
                        return errorResponse.errors[0];
                    }
                    if (typeof errorResponse === 'string') {
                        return errorResponse;
                    }
                    return null;
                };

                return errorResponse => {
                    // handle custom error code with modal support, defaulting to known OAuth / traditional sign-in formats
                    const errorMessage = getCustomErrorMessage(errorResponse);
                    if (isCustomCodeError(errorMessage)) {
                        DialogModal.alert({
                            content: translationHelper.get(errorMessage, {
                                brandEmail: getBrandEmail(undefined, 'support', ConfigFactory.getSync()),
                            }), // should already be localized
                            classes: ['server-error-modal'],
                            title: translationHelper.get('server_error'),
                        });
                    } else {
                        scope.form_errors[fieldName] = errorMessage;
                    }
                };
            },
        };
    },
]);
