import angularModule from 'Authentication/angularModule/scripts/authentication_module';
import smartlyLogoRed from 'vectors/logo-red.svg';
import quanticLogoRed from 'vectors/logo-red_quantic.svg';

angularModule.factory('JoinConfig', [
    '$injector',
    $injector => {
        const ConfigFactory = $injector.get('ConfigFactory');
        const $window = $injector.get('$window');
        const ErrorLogService = $injector.get('ErrorLogService');
        const $rootScope = $injector.get('$rootScope');
        const SuperModel = $injector.get('SuperModel');

        const defaultUrlPrefix = 'default';

        const JoinConfig = SuperModel.subclass(function () {
            this.extend({
                // return the url config data structure based on url prefix, falling back to specified default
                getConfig(urlPrefix) {
                    return this._getJoinConfig().then(joinConfig => {
                        if ($window.CORDOVA) {
                            if ($window.CORDOVA.forcedUrlPrefix) {
                                urlPrefix = $window.CORDOVA.forcedUrlPrefix;
                            }
                        }

                        // We could have a current user when we are on a sign-in page
                        // if the user was logged out in the middle of a session.  In that
                        // case, check the user's sign up code to see if we should use
                        // a particular configuration.
                        if (!urlPrefix && joinConfig.configForUser($rootScope.currentUser)) {
                            return joinConfig.configForUser($rootScope.currentUser);
                        }

                        if (!urlPrefix) {
                            urlPrefix = defaultUrlPrefix;
                        }

                        return joinConfig.configForUrlPrefix(urlPrefix);
                    });
                },

                _getJoinConfig() {
                    // Load up the join_config from the server,
                    // and then merge in values that only the client
                    // needs.
                    if (!this._joinConfigPromise) {
                        this._joinConfigPromise = ConfigFactory.getConfig().then(
                            config => new JoinConfig(config.join_config),
                        );
                    }
                    return this._joinConfigPromise;
                },
            });

            return {
                initialize(configs) {
                    // clone it
                    configs = JSON.parse(JSON.stringify(configs));

                    // default configuration
                    const _default = {
                        title_message: 'authentication.institution_register.sign_up',
                        sign_in_title_message: 'authentication.sign_in.login',
                        show_testimonials: false,
                        included_field_ids: ['name', 'email', 'password'],
                        provider: 'email',
                        smartly_logo_img: smartlyLogoRed,
                        quantic_logo_img: quanticLogoRed,
                        // sign_up_message: 'authentication.sign_up_form.sign_up_for_free_using',
                        submit_button_text: 'authentication.sign_up_form.join',
                    };

                    const _institutionDemoDefault = _.extend({}, _default, {
                        show_testimonials: true,
                        institutional_demo: true,
                        sign_up_message: 'authentication.sign_up_form.experience_a_demo',
                        included_field_ids: ['name', 'company', 'job_title', 'phone', 'country', 'email', 'password'],
                        disable_oauth: true,
                        submit_button_text: 'authentication.sign_up_form.join_demo',
                    });

                    // handle standard vs institution configration defaults
                    _.each(configs, entry => {
                        const defaultForEntry = entry.institutional_demo ? _institutionDemoDefault : _default;
                        _.extend(entry, defaultForEntry);
                    });

                    // handle any special-case client-only configuration schemes
                    const clientSideOverrides = {
                        hiring: {
                            title_message: 'authentication.institution_register.sign_up_to_discover_candidates',
                            show_title_message_on_mobile: true,
                            show_testimonials: true,
                            disable_oauth: true,
                            sign_up_message: null,
                            submit_button_text: 'authentication.sign_up_form.sign_up_its_free',
                            require_company_email: true,
                            included_field_ids: [
                                'email',
                                'name',
                                'job_title',
                                'company',
                                'location',
                                'phone',
                                'password',
                            ],
                            field_overrides: {
                                phone: {
                                    defaultCountryCode: 'US',
                                },
                            },
                        },
                        math101a: {
                            subtitle_message: 'authentication.institution_register.subtitle_math101a',
                            sign_up_message: 'authentication.sign_up_form.sign_up_using_emu_email',
                            disable_oauth: true,
                            email_placeholder: 'you@emu.edu',
                        },
                        math101b: {
                            subtitle_message: 'authentication.institution_register.subtitle_math101b',
                            sign_up_message: 'authentication.sign_up_form.sign_up_using_emu_email',
                            disable_oauth: true,
                            email_placeholder: 'you@emu.edu',
                        },
                        gwstatistics: {
                            disable_oauth: true,
                        },
                        jmu: {
                            disable_oauth: true,
                        },
                        emiratesdb: {
                            disable_oauth: true,
                        },
                        georgetownmsb: {
                            sign_up_message: 'authentication.sign_up_form.sign_up_using_georgetown_email',
                            disable_oauth: true,
                            email_placeholder: 'you@georgetown.edu',
                        },
                        nyu: {
                            sign_up_message: 'authentication.sign_up_form.sign_up_using_nyu_email',
                            disable_oauth: true,
                            email_placeholder: 'you@stern.nyu.edu',
                        },
                        iebusiness: {
                            disable_oauth: true,
                            email_placeholder: 'ie.edu email',
                            show_title_message_on_mobile: true,
                            subtitle_message: 'authentication.institution_register.subtitle_iebusiness',
                        },
                        dubaipolice: {
                            disable_oauth: true,
                        },
                        'research-study17': {
                            subtitle_message: 'authentication.institution_register.subtitle_research_study17',
                            show_title_message_on_mobile: true,
                            disable_oauth: true,
                        },
                        intuit: {
                            disable_oauth: true,
                        },
                        advancedplacement: {
                            disable_oauth: true,
                        },
                        inframark: {
                            sign_up_message: 'authentication.sign_up_form.sign_up_using_inframark_email',
                            disable_oauth: true,
                            email_placeholder: 'you@inframark.com',
                        },
                        georgetownmim: {
                            sign_up_message: 'authentication.sign_up_form.sign_up_using_georgetown_email',
                            subtitle_message: 'authentication.institution_register.subtitle_georgetownmim',
                            disable_oauth: true,
                            email_placeholder: 'you@georgetown.edu',
                        },
                        miyamiya: {
                            title_message: 'authentication.sign_up_form.miyamiya_title_message',
                            sign_in_title_message: 'authentication.sign_in.login_to_miyamiya',
                            disable_oauth: true,
                        },
                    };

                    // apply overrides if found in configs
                    _.each(clientSideOverrides, (value, key) => {
                        if (configs[key]) {
                            // do not use a deep merge, or odd things will happen
                            // to fields like the `included_field_ids` array
                            angular.extend(configs[key], value);
                        }
                    });

                    this.default = configs.default;
                    this._configs = configs;
                },

                configForUrlPrefix(urlPrefix) {
                    const config = this._configs[urlPrefix];
                    if (!config) {
                        ErrorLogService.notify(`No urlConfig for '${urlPrefix}'`);
                        urlPrefix = defaultUrlPrefix;
                    }

                    return this._configs[urlPrefix];
                },

                configForUser(user) {
                    if (!user) {
                        return undefined;
                    }

                    return _.findWhere(this._configs, {
                        signup_code: user.sign_up_code,
                    });
                },
            };
        });

        return JoinConfig;
    },
]);
