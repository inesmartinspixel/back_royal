import 'ng-toast';
import 'ng-token-auth';
import 'angular-bind-html-compile';

import 'EventLogger/angularModule';
import 'Capabilities/angularModule';
import 'CaptchaHelper/angularModule';
import 'ClientStorage/angularModule';
import 'ErrorLogging/angularModule';
import 'Injector/angularModule';
import 'InstitutionalSubdomain/angularModule';
import 'PreSignupTracking/angularModule';
import 'PrioritizedInterceptors/angularModule';
import 'RemoteNotificationHelper/angularModule';
import 'SafeApply/angularModule';
import 'Telephone/angularModule';
import 'Translation/angularModule';
import 'AngularHttpQueueAndRetry/angularModule';
import 'Careers/angularModule';
import 'EmailInput/angularModule';
import 'Navigation/angularModule';
import 'OnImageLoad/angularModule';
import 'ScrollHelper/angularModule';
import 'Segmentio/angularModule';
import 'TimeoutHelpers/angularModule';
import 'Users/angularModule';

export default angular
    .module('FrontRoyal.Authentication', [
        'angular-bind-html-compile',
        'remoteNotificationHelper',
        'timeoutHelpers',
        'scrollHelper',
        'Translation',
        'Capabilities',
        'EventLogger',
        'FrontRoyal.Careers',
        'FrontRoyal.Users',
        'FrontRoyal.Navigation',
        'FrontRoyal.EmailInput',
        'ClientStorage',
        'ng-token-auth',
        'ngToast',
        'onImageLoad',
        'prioritizedInterceptors',
        'segmentio',
        'HttpQueueAndRetry',
        'ipCookie',
        'institutionalSubdomain',
        'FrontRoyal.ErrorLogService',
        'FrontRoyal.Telephone',
        'safeApply',
        'captchaHelper',
        'preSignupTracking',
        'Injector',
    ])
    .config([
        '$authProvider',
        'CapabilitiesProvider',
        ($authProvider, CapabilitiesProvider) => {
            //-----------------------------
            // Configure $authProvider
            // https://github.com/lynndylanhurley/ng-token-auth
            //-----------------------------

            // Hack that lets us hard refresh to get back out of logged-in-as-user mode into
            // the user we were previously logged in as: prefill old auth creds if available
            const Capabilities = CapabilitiesProvider.$get();
            const supportsLocalStorage = Capabilities.localStorage;

            if (supportsLocalStorage) {
                if (window.localStorage.prev_auth_headers) {
                    window.localStorage.auth_headers = window.localStorage.prev_auth_headers;
                    window.localStorage.removeItem('prev_auth_headers');
                }
                if (window.localStorage.logged_in_as_user_id) {
                    window.localStorage.removeItem('logged_in_as_user_id');
                }
            }

            $authProvider.configure({
                // every API request originating from the ENDPOINT_ROOT
                apiUrl: `${window.ENDPOINT_ROOT}/api`,

                // defer validation for explicit validation once auth listeners are setup
                validateOnPageLoad: false,

                // required for cordova hybrid
                storage: supportsLocalStorage ? 'localStorage' : 'cookies', // we need to fall back to cookies in Safari private mode

                // rely on auth events to perform redirect as necessary
                confirmationSuccessUrl: `${window.ENDPOINT_ROOT}/`,
                passwordResetSuccessUrl: `${window.ENDPOINT_ROOT}/settings/profile`,

                omniauthWindowType: window.CORDOVA ? 'inAppBrowser' : 'sameWindow',

                authProviderPaths: {
                    google_oauth2: '/auth/google_oauth2',
                    facebook: '/auth/facebook',
                    apple_quantic: '/auth/apple_quantic',
                    apple_smartly: '/auth/apple_smartly',
                    twitter: '/auth/twitter',
                    onelogin: '/auth/onelogin',
                    jll: '/auth/jll',
                },
            });
        },
    ])
    .run([
        '$injector',
        $injector => {
            const EventLogger = $injector.get('EventLogger');
            EventLogger.allowEmptyLabel(['user:login', 'user:logout', 'anonymous_user:login']);
            EventLogger.setLabelProperty('sign_up_code', ['viewed_join_form', 'viewed_join_form_step_2']);
            EventLogger.setLabelProperty('provider', ['auth:login_with_provider', 'auth:submit_registration']);
        },
    ]);
