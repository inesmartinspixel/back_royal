import angularModule from 'Authentication/angularModule/scripts/authentication_module';
import { setupBrandNameProperties } from 'AppBrandMixin';
import template from 'Authentication/angularModule/views/confirm_profile_info_form.html';
import signupFormFieldTemplate from 'Authentication/angularModule//views/sign_up_form_field.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('confirmProfileInfoForm', [
    '$injector',

    function factory($injector) {
        const $auth = $injector.get('$auth');
        const $rootScope = $injector.get('$rootScope');
        const ValidationResponder = $injector.get('ValidationResponder');
        const TranslationHelper = $injector.get('TranslationHelper');
        const AuthFormHelperMixin = $injector.get('AuthFormHelperMixin');
        const ClientStorage = $injector.get('ClientStorage');

        return {
            restrict: 'E',
            templateUrl,
            scope: {},
            link(scope) {
                scope.signupFormFieldTemplate = signupFormFieldTemplate;
                const translationHelper = new TranslationHelper('authentication.confirm_profile_info_form');
                AuthFormHelperMixin.onLink(scope);
                setupBrandNameProperties($injector, scope);

                scope.includedFields = [
                    {
                        type: 'text',
                        name: 'name',
                        placeholder: 'name_short',
                    },
                    {
                        type: 'email',
                        name: 'email',
                    },
                ];

                scope.emailPlaceholder = translationHelper.get('email');

                scope.form = {};
                scope.form_errors = {};

                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                scope.$watch('currentUser', currentUser => {
                    scope.form = {};
                    scope.form_errors = {};

                    if (!currentUser) {
                        return;
                    }

                    let provider = currentUser.provider;

                    if (['apple_quantic', 'apple_smartly'].includes(provider)) {
                        provider = 'apple';
                    }

                    if (_.contains(['google_oauth2', 'facebook', 'apple'], provider)) {
                        scope.providerName = translationHelper.get(provider);
                    }

                    scope.form.email = $rootScope.currentUser.email;
                    scope.form.name = $rootScope.currentUser.name;
                });

                scope.submitProfileConfirmation = () => {
                    scope.preventSubmit = true;
                    scope.form_errors = {};

                    // Keep sign in form simple and let users put in a single name field.
                    // Store it in name- rest of app will be resiliant to empty last name
                    scope.form.confirmed_profile_info = true;

                    // persist any changes to the user here, like we would in settings, check for errors, etc.
                    $auth.updateAccount(scope.form).then(
                        () => {
                            // see https://trello.com/c/xa4zY5EU/260-0-5-iguana-provisional-changes
                            // for maybe a nicer way of handling this
                            $rootScope.currentUser.email = scope.form.email;
                            $rootScope.currentUser.name = scope.form.name;
                            $rootScope.currentUser.school = scope.form.school;
                            $rootScope.currentUser.confirmed_profile_info = scope.form.confirmed_profile_info;

                            // re-identify
                            ValidationResponder.identifyCurrentUser();

                            // clear last_visited_route to prevent re-directing to the current URL in Cordova
                            ClientStorage.removeItem('last_visited_route');

                            // move on to next page
                            ValidationResponder.forwardToTargetUrl();
                        },
                        response => {
                            scope.preventSubmit = false;
                            scope.form.confirmed_profile_info = false;

                            // Copy-pasted and adapted from institution_register_dir.js
                            if (response && response.data && response.data.errors) {
                                if (response.data.errors.name) {
                                    scope.form_errors.name = `Name ${response.data.errors.name}`;
                                }
                                if (response.data.errors.email) {
                                    scope.form_errors.email = response.data.errors.full_messages[0];
                                }
                            } else {
                                scope.form_errors.general = translationHelper.get('a_server_error_occurred', {
                                    brandName: scope.brandNameShort,
                                });
                            }
                        },
                    );
                };
            },
        };
    },
]);
