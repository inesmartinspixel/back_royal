import angularModule from 'Authentication/angularModule/scripts/authentication_module';
import { setupBrandNameProperties } from 'AppBrandMixin';
import template from 'Authentication/angularModule/views/demo_join.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('demoJoin', [
    '$injector',

    function factory($injector) {
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
        const ValidationResponder = $injector.get('ValidationResponder');
        const ConfigFactory = $injector.get('ConfigFactory');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                urlPrefix: '@',
            },
            link(scope) {
                NavigationHelperMixin.onLink(scope);
                setupBrandNameProperties($injector, scope, ConfigFactory.getSync());

                // If at the /demo route, then there is no urlPrefix.
                // Default to default_demo
                scope.urlPrefix = scope.urlPrefix || 'default_demo';

                scope.forwardToTargetUrl = () => {
                    ValidationResponder.forwardToTargetUrl();
                };
            },
        };
    },
]);
