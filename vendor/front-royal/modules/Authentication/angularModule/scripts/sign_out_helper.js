import angularModule from 'Authentication/angularModule/scripts/authentication_module';
import beforeUnsettingCurrentUser from 'beforeUnsettingCurrentUser';

angularModule.factory('SignOutHelper', [
    '$injector',

    function factory($injector) {
        const $auth = $injector.get('$auth');
        const $window = $injector.get('$window');
        const EventLogger = $injector.get('EventLogger');
        const $rootScope = $injector.get('$rootScope');

        return {
            signOut() {
                EventLogger.log('user:logout');

                // attempt to disconnect auth plugins
                if ($window.CORDOVA) {
                    // facebook plugin
                    try {
                        window.facebookConnectPlugin.logout(
                            () => {},
                            () => {},
                        );
                        // eslint-disable-next-line no-empty
                    } catch (ex) {}

                    // googleplus plugin
                    try {
                        window.plugins.googleplus.disconnect(() => {});
                        // eslint-disable-next-line no-empty
                    } catch (ex) {}
                }

                // Just show a spinner on the screen while we are signing out
                $rootScope.signingOut = true;

                // ng-token-auth signout
                this.beforeUnsettingCurrentUser()
                    .then(() => $auth.signOut())
                    .finally(() => {
                        $rootScope.signingOut = false;
                    });
            },

            // local method we can mock in specs
            beforeUnsettingCurrentUser: () => {
                return beforeUnsettingCurrentUser($injector);
            },
        };
    },
]);
