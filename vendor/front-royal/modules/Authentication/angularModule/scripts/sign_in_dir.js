import angularModule from 'Authentication/angularModule/scripts/authentication_module';
import { setupBrandNameProperties } from 'AppBrandMixin';
import template from 'Authentication/angularModule/views/sign_in.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('signIn', [
    '$injector',

    function factory($injector) {
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
        const JoinConfig = $injector.get('JoinConfig');
        const ConfigFactory = $injector.get('ConfigFactory');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                urlPrefix: '@',
            },
            link(scope) {
                $injector.get('scrollHelper').scrollToTop();

                NavigationHelperMixin.onLink(scope);
                setupBrandNameProperties($injector, scope, ConfigFactory.getSync());

                scope.joinLinkClicked = () => {
                    scope.gotoJoin();
                };

                JoinConfig.getConfig(scope.urlPrefix).then(urlConfig => {
                    scope.urlConfig = urlConfig;
                });
            },
        };
    },
]);
