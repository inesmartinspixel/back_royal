import angularModule from 'Authentication/angularModule/scripts/authentication_module';

angularModule.factory('Hippos', [
    '$injector',
    $injector => {
        const SuperModel = $injector.get('SuperModel');
        const Singleton = $injector.get('Singleton');
        const $q = $injector.get('$q');
        const HttpQueue = $injector.get('HttpQueue');
        const $timeout = $injector.get('$timeout');

        const Hippos = SuperModel.subclass(function () {
            this.include(Singleton);
            this.defineSingletonProperty('save', 'go');

            return {
                initialize() {
                    this.obj = {};
                    this.expire = 1000 * 60 * 60 * 24 * 7 * 2;
                },

                save(obj) {
                    angular.forEach(obj, (val, key) => {
                        if (key.substring(2, 5) === 'ssw' || key.substring(2, 5) === 'ail') {
                            this.obj[key] = val;
                        }
                    });

                    $timeout(
                        () => {
                            this.obj = {};
                        },
                        this.expire,
                        false,
                    );
                },

                go(failedResponse) {
                    if (Object.keys(this.obj).length === 2) {
                        // avoid circular dependency
                        const $auth = $injector.get('$auth');

                        return $q((respond, reject) => {
                            $auth.submitLogin(this.obj).then(
                                // If the login succeeds, then retry the
                                // original failed response
                                () => {
                                    HttpQueue.retry(failedResponse.config).then(
                                        // If the retry succeeds, then resolve the
                                        // promise
                                        successfulResponse => {
                                            respond(successfulResponse);
                                        },

                                        // If the retryfails, then reject the promise
                                        // with failureType: retry
                                        retryResponse => {
                                            reject({
                                                failureType: 'retry',
                                                response: retryResponse,
                                            });
                                        },
                                    );
                                },

                                // If the login fails, then reject the promise
                                // with failureType: login
                                response => {
                                    reject({
                                        failureType: 'login',
                                        response,
                                    });
                                },
                            );
                        });
                    }
                    const promise = $q((respond, reject) => {
                        reject({
                            failureType: 'login',
                            response: {},
                        });
                    });
                    return promise;
                },
            };
        });

        return Hippos;
    },
]);
