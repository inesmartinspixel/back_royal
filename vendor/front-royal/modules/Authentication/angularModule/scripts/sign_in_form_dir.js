import angularModule from 'Authentication/angularModule/scripts/authentication_module';
import template from 'Authentication/angularModule/views/sign_in_form.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('signInForm', [
    '$injector',

    function factory($injector) {
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
        const AuthFormHelperMixin = $injector.get('AuthFormHelperMixin');
        const ValidationResponder = $injector.get('ValidationResponder');
        const JoinConfig = $injector.get('JoinConfig');
        const ConfigFactory = $injector.get('ConfigFactory');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                showForgotPassword: '<',
                contained: '<',
                urlPrefix: '<?',
            },
            link(scope, elem) {
                // get signup code from config
                JoinConfig.getConfig(scope.urlPrefix).then(urlConfig => {
                    scope.urlConfig = urlConfig;
                    scope.showSocial = !urlConfig.disable_oauth;
                    scope.logoImg = ConfigFactory.getSync().isQuantic()
                        ? urlConfig.quantic_logo_img
                        : urlConfig.smartly_logo_img;
                });

                NavigationHelperMixin.onLink(scope);

                AuthFormHelperMixin.onLink(
                    scope,
                    ValidationResponder.forwardToTargetUrl,
                    AuthFormHelperMixin.getCustomOmniauthErrorResponder(scope),
                );

                scope.showPassword = false;
                scope.form = {};

                scope.submit = () => {
                    // It is possible that we will need to show a modal after
                    // submitting login.  If the user still has an input focused
                    // at that point, the keyboard might push the modal up off of
                    // the screen.  By disabling each input, we make sure you can see
                    // the whole screen after submitting.  (See the call to getCurrentUser()
                    // in ValidationResponder for an example)
                    elem.find('input').each(function () {
                        $(this).trigger('blur');
                    });
                    scope.submitLogin(scope.form);
                };

                const stopListeningForLoginSuccess = scope.$on('validation-responder:login-success', () => {
                    ValidationResponder.forwardToTargetUrl();
                });

                scope.$on('$destroy', () => {
                    stopListeningForLoginSuccess();
                });

                // overridden from AuthFormHelperMixin ... why are we doing this exactly?
                scope.formErrorClasses = () => {
                    if (scope.form_errors.general) {
                        return ['form-error', 'active'];
                    }
                    if (scope.form_errors.phone) {
                        return ['form-error', 'active'];
                    }
                    return ['form-error'];
                };

                scope.formValidationClasses = () => {
                    if (scope.form_errors.general) {
                        return ['form-control', 'ng-invalid'];
                    }
                    return ['form-control'];
                };

                scope.toggleShowPassword = evt => {
                    evt.preventDefault();
                    evt.stopImmediatePropagation();
                    scope.showPassword = !scope.showPassword;
                };
            },
        };
    },
]);
