import angularModule from 'Authentication/angularModule/scripts/authentication_module';
import { setupBrandNameProperties, setupScopeProperties } from 'AppBrandMixin';
import template from 'Authentication/angularModule/views/logged_out_page.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

import logoRed from 'vectors/logo-red.svg';
import logoRedQuantic from 'vectors/logo-red_quantic.svg';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('loggedOutPage', [
    '$injector',

    function factory($injector) {
        const institutionalSubdomain = $injector.get('institutionalSubdomain');
        const omniauth = $injector.get('omniauth');
        const $location = $injector.get('$location');
        const ConfigFactory = $injector.get('ConfigFactory');

        return {
            restrict: 'E',
            templateUrl,
            scope: {},
            link(scope) {
                const config = ConfigFactory.getSync();
                setupBrandNameProperties($injector, scope, config);
                setupScopeProperties(
                    $injector,
                    scope,
                    [{ prop: 'logoImgSrc', quantic: logoRedQuantic, fallback: logoRed }],
                    config,
                );

                scope.signIn = () => {
                    // We should never be on this page without an institutionalSubdomain,
                    // but just to be sure, support both possibilities.
                    if (institutionalSubdomain.id) {
                        omniauth.loginWithProvider(institutionalSubdomain.id);
                    } else {
                        $location.path('/sign-in');
                    }
                };
            },
        };
    },
]);
