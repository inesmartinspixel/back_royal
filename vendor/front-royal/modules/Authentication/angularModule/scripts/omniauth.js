import angularModule from 'Authentication/angularModule/scripts/authentication_module';
import Auid from 'Auid';
import moment from 'moment-timezone';

angularModule.factory('omniauth', [
    '$injector',
    $injector => {
        const $auth = $injector.get('$auth');
        const ConfigFactory = $injector.get('ConfigFactory');
        const $window = $injector.get('$window');
        const $location = $injector.get('$location');
        const Locale = $injector.get('Locale');
        const $q = $injector.get('$q');
        const ClientStorage = $injector.get('ClientStorage');
        const preSignupValues = $injector.get('preSignupValues');
        const ClientConfig = $injector.get('ClientConfig');
        const clientConfig = ClientConfig.current;
        const injector = $injector.get('injector');

        return {
            loginWithProvider(provider, signUpCode, programType) {
                return ConfigFactory.getConfig().then(config => {
                    /*
                        We never got sp-initiated auth working with JLL,
                        so we wanted to just forward to the idp-initiated flow
                        rather than going with the sp-initiated flow.
                    */
                    const idpUrl = config.forceIdpInitiatedLoginUrlFor(provider);
                    if (idpUrl) {
                        $window.location.href = idpUrl;

                        // At this point, we expect to leave the front-royal app, we return a
                        // promise that will never resolve in order to prevent the auth
                        // call from ever being complete
                        return $q(() => {});
                    }
                    const opts = {
                        params: {
                            sign_up_code: signUpCode,
                            pref_locale: Locale.defaultLocaleForSignUpCode(signUpCode),
                            timezone: moment.tz.guess(),
                        },
                    };

                    // Apple does NOT allow localhost, which Cordova uses via its WkWebview implementation in iOS
                    if (provider === 'apple' && window.CORDOVA) {
                        opts.auth_origin_url = `${window.ENDPOINT_ROOT}/sign-in`;
                    }

                    const id = Auid.get(injector);
                    if (id) {
                        opts.params.id = id;
                    }

                    if (programType) {
                        opts.params.program_type = programType;
                    }

                    // pull skip_apply from the query params (if available) and pass up to the server
                    const skip_apply = $location.search() && $location.search().skip_apply;
                    if (skip_apply) {
                        opts.params.skip_apply = skip_apply;
                    }

                    // pull the user id stored from a referral link (if available) and pass up to the server
                    const referredById = ClientStorage.getItem('referredById');
                    if (referredById) {
                        opts.params.referred_by_id = referredById;
                    }

                    // append the client config since this will be accessed via a hard link resolution
                    // and will not be subject to angular interceptors
                    opts.params.fr_client = clientConfig.identifier;

                    // add the dynamic landing page multi-step pre-signup form to the params
                    const preSignupFormRegistrationInfo = preSignupValues.getDynamicLandingPageMultiStepFormRegistrationInfo();
                    _.extend(opts.params, preSignupFormRegistrationInfo);

                    // handle conditional client-ids across domains for Sign in with Apple
                    if (provider === 'apple') {
                        const apple_provider = config.isQuantic() ? 'apple_quantic' : 'apple_smartly';
                        return $auth.authenticate(apple_provider, opts);
                    }

                    return $auth.authenticate(provider, opts);
                });
            },
        };
    },
]);
