import angularModule from 'Authentication/angularModule/scripts/authentication_module';

angularModule.directive('captchaWrapper', [
    '$injector',

    function factory($injector) {
        const $compile = $injector.get('$compile');
        const $window = $injector.get('$window');
        const ConfigFactory = $injector.get('ConfigFactory');

        return {
            restrict: 'E',
            scope: {},
            link(scope, elem) {
                // This no longer hinges on whether or not the captcha is
                // required. We are now making an assumption that if you
                // are making use of the captcha-wrapper directive somewhere,
                // you most likely  want that captcha to be required. If it
                // isn't required, this directive likely isn't being used.
                $window.captchaInvalid = () => !window.captchaComplete;

                ConfigFactory.getConfig().then(config => {
                    scope.recaptchaKey = config.recaptcha_site_key;

                    // We embed this HTML here for several reasons:
                    //
                    // 1. We don't use recaptcha in Cordova.
                    // 2. Attemping to exclude these elements from the DOM
                    //    using an ng-if introduces scoping issues that
                    //    impact the usability of the recaptcha.
                    // 3. Attempting to hide these elements in the DOM
                    //    using ng-show/ng-hide means that the recaptcha
                    //    script will be loaded (unsuccessfully) in Cordova.
                    if (scope.recaptchaKey) {
                        elem.append(
                            $compile(
                                '' +
                                    '<div name="captcha_container">' +
                                    ' <script>' +
                                    '   window.captchaComplete = false;' +
                                    '   function captchaCallback() {' +
                                    '     window.captchaComplete = true;' +
                                    '   }' +
                                    ' </script>' +
                                    ' <div class="g-recaptcha" ng-attr-data-sitekey="{{recaptchaKey}}" data-callback="captchaCallback"></div>' +
                                    ' <script  src="https://www.google.com/recaptcha/api.js" async defer></script>' +
                                    '</div>',
                            )(scope),
                        );
                    }
                });
            },
        };
    },
]);
