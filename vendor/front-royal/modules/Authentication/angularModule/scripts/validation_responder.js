import angularModule from 'Authentication/angularModule/scripts/authentication_module';
import * as userAgentHelper from 'userAgentHelper';
import { QUANTIC_DOMAIN, SMARTLY_DOMAIN, QUANTIC_DOMAIN_STAGING, SMARTLY_DOMAIN_STAGING } from 'AppBrandMixin';
import NetworkConnection from 'NetworkConnection';
import Auid from 'Auid';
import casperMode from 'casperMode';
import locales from 'Authentication/locales/authentication/validation_responder-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(locales);

angularModule.factory('ValidationResponder', [
    '$injector',
    $injector => {
        const $auth = $injector.get('$auth');
        const $rootScope = $injector.get('$rootScope');
        const $location = $injector.get('$location');
        const $window = $injector.get('$window');
        const isMobile = $injector.get('isMobile');
        const User = $injector.get('User');
        const EventLogger = $injector.get('EventLogger');
        const segmentio = $injector.get('segmentio');
        const blockAuthenticatedAccess = $injector.get('blockAuthenticatedAccess');
        const $q = $injector.get('$q');
        const omniauth = $injector.get('omniauth');
        const institutionalSubdomain = $injector.get('institutionalSubdomain');
        const $timeout = $injector.get('$timeout');
        const ClientStorage = $injector.get('ClientStorage');
        const ConfigFactory = $injector.get('ConfigFactory');
        const offlineModeManager = $injector.get('offlineModeManager');
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
        const frontRoyalStore = $injector.get('frontRoyalStore');
        const DialogModal = $injector.get('DialogModal');
        const safeApply = $injector.get('safeApply');
        const TranslationHelper = $injector.get('TranslationHelper');
        const injector = $injector.get('injector');
        const translationHelper = new TranslationHelper('authentication.validation_responder');

        // things we want to cleanup from ClientStorage / cookies upon signout
        const TRANSIENT_STORAGE_KEYS = [
            'librarySearchProgress',
            'librarySearchText',
            'librarySearchTopic',
            'librarySearchProgress',
            'toggleableCourseListLibrary',
            'toggleableCourseListLibrary',
            'toggleableCourseListStudentDashboard',
            'soundEnabled',
            'amplitude_sessionId',
            'amplitude_unsent',
            'amplitude_lastEventId',
            'amplitude_lastEventTime',
            'last_visited_route',
            'onboardingQuestions',
            'sharedCareerProfiles',
            'continueApplicationInMarketing',
            'auid',
        ];

        function identifyCurrentUser() {
            // these are standardized in segment. see also - https://segment.com/docs/spec/identify/#traits
            const payload = {};
            _.extend(payload, {
                cordova: !!$window.CORDOVA,
                email: $rootScope.currentUser.email,
                emailDomain: $rootScope.currentUser.emailDomain,
                name: $rootScope.currentUser.name,
                groups: $rootScope.currentUser.userGroupNames().join(','),
                institutions: $rootScope.currentUser.institutionNames().join(','),
                globalRole: $rootScope.currentUser.roleName(),
                plan_id: $rootScope.currentUser.plan_id,
                sign_up_code: $rootScope.currentUser.sign_up_code,
            });

            // Until we get a proper acknowledge page we need to stop sending this information to
            // analytics on Android because it is in violation of Play store terms.
            if ($window.CORDOVA && userAgentHelper.isAndroidDevice()) {
                delete payload.email;
                delete payload.emailDomain;
                delete payload.name;
            }

            segmentio.identify($rootScope.currentUser.id, payload);
        }

        function redirectToCorrectDomain(user, config) {
            if ($window.CORDOVA || config.is_alternative_staging_environment === 'true') {
                return false;
            }

            // If someone is at an institutional subdomain, do not redirect them.
            if (institutionalSubdomain?.id) {
                return false;
            }

            /* All of this is a bit hackish.  Basically, we want to have a way
                to test this on local and staging.  In other places in the app,
                however, QUANTIC_DOMAIN is always quantic.edu and SMARTLY_DOMAIN
                is always smart.ly regardless of the environment.  So we need
                a workaround just for this particular thing.  Further explanation
                in inline comments below */

            let smartlyHost;
            let quanticHost;
            let targetProtocol = 'https';
            let currentHost;

            // If the webserver is running in development mode,
            // and FORWARD_TO_CORRECT_DOMAIN_IN_DEVMODE is set to true
            // in the configuration,
            // then quantic users will be forwarded to localhost:3001
            // and smartly users will be forwarded to localhost:3000.
            if (config.appEnvType() === 'development') {
                if (config.forward_to_correct_domain_in_devmode !== 'true') {
                    return false;
                }
                quanticHost = 'localhost:3001';
                smartlyHost = 'localhost:3000';
                targetProtocol = 'http';

                // only pay attention to the port in development
                currentHost = `${$location.host()}:${$location.port()}`;
            }

            // In staging and production, we forward to the correct host
            else if (config.appEnvType() === 'staging') {
                quanticHost = QUANTIC_DOMAIN_STAGING;
                smartlyHost = SMARTLY_DOMAIN_STAGING;
                currentHost = $location.host();
            } else {
                quanticHost = QUANTIC_DOMAIN;
                smartlyHost = SMARTLY_DOMAIN;
                currentHost = $location.host();
            }

            const targetHost = user.shouldSeeQuanticBranding ? quanticHost : smartlyHost;

            if (currentHost !== targetHost) {
                const url = new URL(`${targetProtocol}://${targetHost}${$location.url()}`);
                NavigationHelperMixin.loadUrl(url.toString());
                return true;
            }

            return false;
        }

        // NOTE: `handleValidationSuccess` can and does get called multiple times in marketing
        // registration flows. This is because we explicitly broadcast the `auth:validation-success`
        // event in `SignUpFormHelper.submitRegistration`, then redirect to /home in `marketingSignUpForm`
        // which re-initializes `ValidationResponder` and calls `$auth.validateUser` (which broadcasts
        // `auth:validation-success` on success).
        function handleValidationSuccess(ev, userResponse) {
            // handle special-case single page oauth implementation
            if ($location.search() && $location.search().oauth_login) {
                $location.search('oauth_login', undefined);
            }

            const user = User.new(userResponse || $auth.user);

            // Clear out the referredById from localStorage upon successful authentication so
            // that a single browser with multiple signups doesn't continue to set the referred_by
            if (ClientStorage.getItem('referredById')) {
                ClientStorage.removeItem('referredById');
            }

            let abortLogin;
            frontRoyalStore
                .getCurrentUser()
                .then(userFromStore => {
                    if (userFromStore && userFromStore.id !== user.id) {
                        DialogModal.alert({
                            content: translationHelper.get('you_have_stored_data_for_another_user', {
                                email: userFromStore.email,
                            }),
                            close: () => {
                                $location.url('/sign-in');
                            },
                        });
                        $rootScope.authCallComplete = true;
                        abortLogin = true;
                    }
                })
                .then(() => {
                    if (abortLogin) {
                        return null;
                    }

                    // we need to ensure that prior to validation we actually have
                    // a config loaded in order for events to provide servertime info
                    return ConfigFactory.getConfig();
                })
                .then(config => {
                    if (abortLogin) {
                        return;
                    }
                    if (redirectToCorrectDomain(user, config)) {
                        return;
                    }

                    // If an anonymous user just logged in, store the fact that
                    // they did.  This lets us attach the anonymous user's events
                    // to the actual user, or just filter the anonymous user out of
                    // reports about new users.
                    const currentAuid = Auid.get(injector);
                    const anonymousUserLogin = currentAuid && currentAuid !== user.id;
                    if (anonymousUserLogin) {
                        EventLogger.log('anonymous_user:login', {
                            logged_in_user_id: user.id,
                        });
                        EventLogger.onUserChange(currentAuid);
                    }

                    $rootScope.currentUser = user;
                    $rootScope.authCallComplete = true;

                    if (casperMode()) {
                        $rootScope.originalCurrentUser = _.clone(user);
                    }

                    // Keep the user's id as then auid.  That way, any events the
                    // user logs from the marketing pages (when they are not
                    // authenticated), will have the correct id.
                    Auid.set(user.id);

                    EventLogger.log('user:login');

                    // eslint-disable-next-line no-use-before-define
                    ValidationResponder.identifyCurrentUser();

                    if ($window.CORDOVA && $rootScope.currentUser.pref_allow_push_notifications) {
                        // Since remote push services can periodically reset/refresh tokens, we need to initialize
                        // the plugin every time a user is validated and re-register for remote notifications.
                        $injector.get('remoteNotificationHelper').registerDeviceForRemoteNotifications();
                    }

                    $rootScope.currentUser.setContinueApplicationInMarketingFlag();

                    $rootScope.$broadcast('validation-responder:login-success', ev);
                })
                .finally(() => {
                    // getCurrentUser returns a native promise
                    safeApply($rootScope);
                });
        }

        function unsetCurrentUser() {
            frontRoyalStore.enabled = false;
            $rootScope.currentUser = undefined;
        }

        function forwardToTargetUrl(skipFallbackToDashboard) {
            const useLastVisitedRoute = $window.CORDOVA || !!ClientStorage.getItem('logged_in_as');

            // redirect to `last_visited_route` if targeting is applicable, otherwise default to URL query-string or OAuth localStorage values if provided
            const target = useLastVisitedRoute
                ? ClientStorage.getItem('last_visited_route')
                : $location.search().target || ClientStorage.getItem('oauth_redirect_target');

            // remove temp vars from client storage
            ClientStorage.removeItem('logged_in_as');
            ClientStorage.removeItem('oauth_redirect_target');
            if (!$window.CORDOVA) {
                ClientStorage.removeItem('last_visited_route');
            }

            if (target === 'NONE') {
                // do nothing.  This is used when a user gets logged out
                // during an active session and signs back in through the sign-in-form
                $location.search('target', null);
            } else if (target && target.slice(0, 4) === 'http') {
                // the marketing_sign_up_form might set this to an absolute url
                // if this is a sign up page with a success step that we want to return
                // to after oauth login
                if (target !== $window.location.href) {
                    // eslint-disable-next-line no-use-before-define
                    ValidationResponder.setHref(target);
                }
            } else if (target) {
                $location.url(target);
            } else if (!skipFallbackToDashboard) {
                $rootScope.goHome();
                $location.search({});
            }
        }

        function forwardToTargetUrlIfExists() {
            forwardToTargetUrl(true);
        }

        function loginWithSaml() {
            // This is hackish, but since this function is called
            // in a callback to an auth:invalid call, we can't
            // login right away.  We need to wait for ng-token-auth
            // to cleanup the .dfd property.
            if ($injector.get('$auth').dfd) {
                $timeout(loginWithSaml, 50);
                return;
            }

            omniauth
                .loginWithProvider(institutionalSubdomain.id)
                .then(forwardToTargetUrl, unsetCurrentUser)
                .finally(() => {
                    $rootScope.authCallComplete = true;
                });
        }

        const ValidationResponder = {
            initialize(initiallyDisconnected) {
                //-------------------------
                // Login / Resume / Logout
                //-------------------------

                $rootScope.$on('auth:validation-success', handleValidationSuccess);
                $rootScope.$on('auth:login-success', handleValidationSuccess);

                $rootScope.$on('auth:password-reset-confirm-success', () => {
                    $location.url('/settings/profile');
                });

                //-------------------------
                // Logout / Failure
                //-------------------------

                $rootScope.$on('auth:validation-error', unsetCurrentUser);
                $rootScope.$on('auth:session-expired', unsetCurrentUser);

                /*
                    If the initial validaton fails, then we have special
                    handling when in an institutional subdomain (like jll.smart.ly).
                    In those cases, we immediately forward to saml oauth
                    authentication.
                */
                if (institutionalSubdomain.id) {
                    $rootScope.$on('auth:invalid', loginWithSaml);
                } else {
                    $rootScope.$on('auth:invalid', unsetCurrentUser);
                }

                // auth events that are expected and require redirects
                $rootScope.$on('auth:logout-success', () => {
                    unsetCurrentUser();

                    // do selective cleanup on signout
                    TRANSIENT_STORAGE_KEYS.forEach(key => {
                        ClientStorage.removeItem(key);
                    });

                    // If we send institutional users to the homepage, they
                    // will just get automatically logged back in due to their
                    // subdomain.
                    if ($window.CORDOVA) {
                        // At one point this was an if / else if because we were building
                        // an app for the external institution Uber, which we did NOT want to
                        // touch the onboarding experience. But then we built an app for
                        // Miya Miya, which we DID want to use the onboarding experience. So
                        // if we ever need to do something similar to Uber again then we'll need
                        // to reconcile this forcedUrlPrefix and onboarding logic appropriately.
                        // if ($window.CORDOVA.forcedUrlPrefix) {
                        //     $location.path(`/${$window.CORDOVA.forcedUrlPrefix}/sign-in`);

                        if (isMobile()) {
                            $location.path('onboarding/hybrid/login');
                        } else {
                            $location.path('/sign-in');
                        }
                    } else if (institutionalSubdomain.id) {
                        $location.path('/logged-out');
                    } else if (!$window.RUNNING_IN_TEST_MODE) {
                        $window.location.replace('/');
                    }
                });

                // Setup initial state and kick off call
                $rootScope.authCallComplete = false;

                function attemptOfflineAuthentication(err) {
                    if (err.reason !== 'unauthorized') {
                        throw err;
                    }
                    return offlineModeManager.attemptOfflineAuthentication().then(userAttrs => {
                        // If we did not enter offline mode and load
                        // a user from the store, throw the original error
                        if (!userAttrs) {
                            throw err;
                        }
                        frontRoyalStore.enabled = true;
                        $rootScope.currentUser = User.new(userAttrs);

                        if (!userAttrs.enable_front_royal_store) {
                            $injector
                                .get('ErrorLogService')
                                .notifyInProd('How is this user here but not enable_front_royal_store');
                        }

                        $rootScope.authCallComplete = true;
                    });
                }

                if (!blockAuthenticatedAccess.block) {
                    offlineModeManager.canEnterOfflineMode().then(canEnterOfflineMode => {
                        // skip the auth call altogether if we're offline in hybrid mode
                        if ($window.CORDOVA && !NetworkConnection.online && !canEnterOfflineMode) {
                            unsetCurrentUser();
                            $rootScope.authCallComplete = true;
                        } else {
                            /*
                            We add the forwardToTargetUrlIfExists to handle the
                            omniauth case.  In that case we would have been forwarded
                            back to this page, and the original target query param
                            will still be in the url.  If there is no target, however,
                            we should just stay on the page we're on.
                        */
                            $auth
                                .validateUser()
                                .catch(attemptOfflineAuthentication)
                                .catch(err => {
                                    $rootScope.authCallComplete = true;
                                    if (err.reason !== 'unauthorized') {
                                        throw err;
                                    }

                                    // FIXME: something else triggers `goHome` or an equivalent unless we initially redirect
                                    // to routes in `FrontRoyal` module `run`, which we do when we're initially disconnected.
                                    // This seems like a smell that should be unified, but there probably be dragons.
                                    if (initiallyDisconnected) {
                                        $rootScope.goHome();
                                    }
                                })
                                .then(forwardToTargetUrlIfExists);
                        }
                    });
                }
            },

            setHref(href) {
                $window.location.href = href;
            },

            waitForAuthCallComplete() {
                return $q(resolve => {
                    const cancel = $rootScope.$watch('authCallComplete', authCallComplete => {
                        if (authCallComplete) {
                            resolve();
                            cancel();
                        }
                    });
                });
            },

            // see also: edit_lesson_preview_mode_dir / preview_lesson_dir
            initializePreviewMode() {
                const PreviewWindowConfigFactory = $injector.get('ConfigFactory');

                $window.registerSharedPreviewScope = (scope, currentUser, MasterWindowConfigFactory) => {
                    $rootScope.sharedPreviewScope = scope;
                    $rootScope.user = currentUser;
                    $rootScope.currentUser = currentUser;
                    $rootScope.authCallComplete = true;

                    // SiteMetadata assumes that the ConfigFactory has loaded up config
                    // before anything else happens.  This is a safe assumption because the config
                    // starts loading in app.js before anything else happens, and queuing ensures that
                    // it blocks everything else.  In this case, however, the preview mode window
                    // does not need to make any api requests before rendering the lesson, so it
                    // will blow up trying to render the lesson before the config is loaded.
                    PreviewWindowConfigFactory._config = MasterWindowConfigFactory._config;

                    // Since we're copying config, we need to be sure to copy the initTime as well
                    PreviewWindowConfigFactory._serverTime = MasterWindowConfigFactory._serverTime;

                    safeApply($rootScope);
                };
            },

            handleValidationSuccess,
            unsetCurrentUser,
            forwardToTargetUrl,
            identifyCurrentUser,
        };

        return ValidationResponder;
    },
]);
