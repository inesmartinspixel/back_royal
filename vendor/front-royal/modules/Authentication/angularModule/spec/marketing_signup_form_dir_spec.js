import 'AngularSpecHelper';
import 'Authentication/angularModule';

describe('Authentication.MarketingSignUpForm', () => {
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let $timeout;
    let SignUpFormHelper;
    let ClientStorage;
    let $window;
    let onFormError;
    let submitCallback;
    let $injector;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Authentication', 'SpecHelper');

        angular.mock.module($provide => {
            $provide.value('ipCookie', jest.fn());
        });

        angular.mock.inject([
            '$injector',
            _$injector => {
                $injector = _$injector;
                SpecHelper = $injector.get('SpecHelper');
                $timeout = $injector.get('$timeout');
                ClientStorage = $injector.get('ClientStorage');
                SignUpFormHelper = $injector.get('SignUpFormHelper');
                $window = $injector.get('$window');
                onFormError = jest.fn();

                $window.smForm = function (options) {
                    this.onFormError = onFormError;
                    submitCallback = options.submitCallback;
                };

                SpecHelper.mockCapabilities();
                SpecHelper.stubEventLogging();
                SpecHelper.stubConfig();
                SpecHelper.stubDirective('locationAutocomplete');
                $window.afterSignedUpOnDynamicLandingPage = jest.fn();
                $window.hideSuccess = jest.fn();
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
        delete $window.smForm;
        delete $window.afterSignedUpOnDynamicLandingPage;
        delete $window.hideSuccess;
    });

    function render(urlPrefix, html) {
        renderer = SpecHelper.renderer();
        const urlPrefixAttr = urlPrefix ? 'url-prefix="prefix"' : '';
        html = html || `<div marketing-sign-up-form ${urlPrefixAttr}"></div>`;
        renderer.render(html);

        elem = renderer.elem;
        scope = renderer.scope;
        scope.signUpForm = {};

        // attach smForm
        $timeout.flush();
    }

    it('should set oauth_redirect_target only if configured to show success page after registration', () => {
        render(null, '<div marketing-sign-up-form complete-dynamic-landing-form-on-registration=true></div>');
        expect(scope.oauthRedirectTarget).toEqual($window.location.href);

        render();
        expect(scope.oauthRedirectTarget).toBeUndefined();
    });

    it('should handle delayed loading properly', () => {
        $window.smForm = undefined;
        render();

        // no errors should be thrown here

        $window.smForm = function (options) {
            this.onFormError = onFormError;
            submitCallback = options.submitCallback;
        };

        $timeout.flush();
    });

    it('should call getConfig with no prefix if there is url-prefix', () => {
        jest.spyOn(SignUpFormHelper.prototype, 'initialize');
        jest.spyOn(SignUpFormHelper.prototype, 'getConfig');
        render();
        expect(SignUpFormHelper.prototype.initialize).toHaveBeenCalledWith(scope);
        expect(SignUpFormHelper.prototype.getConfig).toHaveBeenCalledWith(undefined);
    });

    it('should call getConfig with a provided url-prefix', () => {
        jest.spyOn(SignUpFormHelper.prototype, 'initialize');
        jest.spyOn(SignUpFormHelper.prototype, 'getConfig');
        render('prefix');
        expect(SignUpFormHelper.prototype.initialize).toHaveBeenCalledWith(scope);
        expect(SignUpFormHelper.prototype.getConfig).toHaveBeenCalledWith('prefix');
    });

    it('should populate the email field with the prefilledEmail in local storage', () => {
        ClientStorage.setItem('prefilledEmail', 'test@email.com');
        render(null, '<div marketing-sign-up-form><input name="email" ng-model="email"></input></div>');
        expect(elem.find('[name="email"]').val()).toEqual('test@email.com');
        expect(ClientStorage.getItem('prefilledEmail')).toBeUndefined();
    });

    it('should call afterSignedUpOnDynamicLandingPage and ensureLoginEvent if logged in', () => {
        const currentUser = SpecHelper.stubCurrentUser();
        jest.spyOn(currentUser, 'ensureLoginEvent').mockImplementation(() => {});
        render(null, '<div marketing-sign-up-form complete-dynamic-landing-form-on-registration=true></div>');
        scope.$broadcast('validation-responder:login-success');
        expect($window.afterSignedUpOnDynamicLandingPage).toHaveBeenCalled();
        expect(currentUser.ensureLoginEvent).toHaveBeenCalled();

        currentUser.ensureLoginEvent.mockClear();
        $window.afterSignedUpOnDynamicLandingPage.mockClear();

        render(null);
        scope.$broadcast('validation-responder:login-success');
        expect($window.afterSignedUpOnDynamicLandingPage).not.toHaveBeenCalled();
        expect(currentUser.ensureLoginEvent).not.toHaveBeenCalled();
    });

    it('should call hideSuccess on validation failure', () => {
        render(null, '<div marketing-sign-up-form complete-dynamic-landing-form-on-registration=true></div>');
        scope.$broadcast('auth:validation-error');
        expect($window.hideSuccess).toHaveBeenCalled();

        $window.hideSuccess.mockClear();
        render(null);
        scope.$broadcast('auth:validation-error');
        expect($window.afterSignedUpOnDynamicLandingPage).not.toHaveBeenCalled();
    });

    describe('onForwardToNextPage', () => {
        let forwardToNextPage;
        beforeEach(() => {
            SignUpFormHelper.prototype.onForwardToNextPage = fn => {
                forwardToNextPage = fn;
            };
        });

        it('should call afterSignedUpOnDynamicLandingPage and ensureLoginEvent if configured to do so', () => {
            const currentUser = SpecHelper.stubCurrentUser();
            jest.spyOn(currentUser, 'ensureLoginEvent').mockImplementation(() => {});
            render(null, '<div marketing-sign-up-form complete-dynamic-landing-form-on-registration=true></div>');
            jest.spyOn(scope, 'setHref').mockImplementation(() => {});
            forwardToNextPage();
            expect($window.afterSignedUpOnDynamicLandingPage).toHaveBeenCalled();
            expect(scope.setHref).not.toHaveBeenCalled();
            expect(currentUser.ensureLoginEvent).toHaveBeenCalled();
        });

        it('should update the window location if not configured to afterSignedUpOnDynamicLandingPage', () => {
            render();
            jest.spyOn(scope, 'setHref').mockImplementation(() => {});
            forwardToNextPage();
            expect($window.afterSignedUpOnDynamicLandingPage).not.toHaveBeenCalled();
            expect(scope.setHref).toHaveBeenCalledWith('/home');
        });
    });

    describe('server error handling', () => {
        it('should not pass errors through to small form before submit', () => {
            render();
            scope.form_errors = {
                name: 'That is not really your name.',
                password: 'Sounds like the password an idiot would have on their luggage.',
            };
            scope.signUpForm.$submitted = false;
            scope.$apply();
            expect(onFormError).not.toHaveBeenCalled();
        });

        it('should pass errors through to small form after submit', () => {
            render();
            scope.form_errors = {
                name: 'That is not really your name.',
                password: 'Sounds like the password an idiot would have on their luggage.',
            };
            scope.signUpForm.$submitted = true;
            scope.$apply();
            expect(onFormError).toHaveBeenCalledWith({
                message:
                    'That is not really your name., Sounds like the password an idiot would have on their luggage.',
                invalidFields: ['name', 'password'],
            });
        });
    });

    describe('special error handling', () => {
        it('should show phone errors', () => {
            render(
                null,
                '<div marketing-sign-up-form><div class="sm-form__group--phone-number"><input></input></div></div>',
            );
            scope.form_errors = {
                phone: 'some error',
            };
            elem.find('input').trigger('blur');
            SpecHelper.expectHasClass(elem, '.sm-form__group--phone-number', 'sm-form__group--error');

            scope.form_errors = {};
            elem.find('input').trigger('blur');
            SpecHelper.expectDoesNotHaveClass(elem, '.sm-form__group--phone-number', 'sm-form__group--error');
        });

        it('should show location errors', () => {
            render(
                null,
                '<div marketing-sign-up-form><div class="sm-form__group location"><input class="ng-touched"></input><div class="sm-form__error-message"></div></div></div>',
            );
            scope.signUpForm.location = {
                $valid: false,
            };
            elem.find('input').trigger('blur');
            SpecHelper.expectHasClass(elem, '.sm-form__group.location', 'sm-form__group--error');
            SpecHelper.expectElementText(elem, '.sm-form__error-message', 'Please select a location');

            scope.signUpForm.location = {
                $valid: true,
            };
            elem.find('input').trigger('blur');
            SpecHelper.expectDoesNotHaveClass(elem, '.sm-form__group.location', 'sm-form__group--error');
            SpecHelper.expectElementText(elem, '.sm-form__error-message', '');
        });

        it('should show querystring provided OAuth errors', () => {
            const html = `<div marketing-sign-up-form>
                                <div class="onboarding__social-auth">
                                    <div class="sm-form__group">
                                        <span class="sm-form__error-message sm-form__error-message--form-error"></span>
                                    </div>
                                </div>
                            </div>`;

            render(null, html);
            SpecHelper.expectElementText(elem, '.sm-form__error-message', '');

            const $location = $injector.get('$location');
            jest.spyOn($location, 'search').mockReturnValue({
                error: 'OAuth Error',
            });
            render(null, html);
            SpecHelper.expectElementText(elem, '.sm-form__error-message', 'OAuth Error');

            $location.search.mockReturnValue({
                error: ['OAuth Error 1', 'OAuth Error 2'],
            });
            render(null, html);
            SpecHelper.expectElementText(elem, '.sm-form__error-message', 'OAuth Error 1, OAuth Error 2');
        });
    });

    describe('submitRegistration', () => {
        it('should submit the form', () => {
            jest.spyOn(SignUpFormHelper.prototype, 'submitRegistration').mockImplementation(() => {});
            render();
            submitCallback({
                a: 1,
            });
            expect(SignUpFormHelper.prototype.submitRegistration).toHaveBeenCalledWith({
                a: 1,
            });
        });
        it('should add in special values', () => {
            jest.spyOn(SignUpFormHelper.prototype, 'submitRegistration').mockImplementation(() => {});
            render();
            scope.phone = 'phone';
            scope.place_details = 'place_details';
            scope.place_id = 'place_id';
            scope.professional_organization = {
                text: 'Pedago',
                locale: 'en',
            };
            submitCallback({});
            expect(SignUpFormHelper.prototype.submitRegistration).toHaveBeenCalledWith({
                phone: 'phone',
                place_details: 'place_details',
                place_id: 'place_id',
                professional_organization: {
                    text: 'Pedago',
                    locale: 'en',
                },
            });
        });
        it('should add program-type if in attrs', () => {
            render(null, '<div marketing-sign-up-form program-type="program_type"></div>');
            elem.attr('program-type', 'program_type');
            jest.spyOn(SignUpFormHelper.prototype, 'submitRegistration').mockImplementation(() => {});
            submitCallback({});
            expect(SignUpFormHelper.prototype.submitRegistration.mock.calls[0][0].program_type).toEqual('program_type');
        });

        it('should add program-type if in queryparams', () => {
            const $location = $injector.get('$location');
            jest.spyOn($location, 'search').mockReturnValue({
                program: 'program_type',
            });
            render(null);
            jest.spyOn(SignUpFormHelper.prototype, 'submitRegistration').mockImplementation(() => {});
            submitCallback({});
            expect(SignUpFormHelper.prototype.submitRegistration.mock.calls[0][0].program_type).toEqual('program_type');
        });
    });

    describe('provider=hiring_team_invite', () => {
        it('should work', () => {
            const ValidationResponder = $injector.get('ValidationResponder');
            const $location = $injector.get('$location');
            jest.spyOn($location, 'search').mockReturnValue({
                provider: 'hiring_team_invite',
                fixedEmail: 'invited@pedago.com',
                fixedCompanyName: 'fixedCompanyName',
            });
            render();
            jest.spyOn(SignUpFormHelper.prototype, 'submitRegistration').mockImplementation(() => {});
            jest.spyOn(SignUpFormHelper.prototype, 'attemptSignIn').mockImplementation(() => {});
            jest.spyOn(ValidationResponder, 'forwardToTargetUrl');
            submitCallback({});
            expect(SignUpFormHelper.prototype.submitRegistration).not.toHaveBeenCalled();
            expect(SignUpFormHelper.prototype.attemptSignIn).toHaveBeenCalledWith({
                email: 'invited@pedago.com',
                provider: 'hiring_team_invite',
            });
            scope.$broadcast('validation-responder:login-success');
        });
    });

    it('should show privacy notice', () => {
        SpecHelper.stubConfig({
            gdpr_user: 'true',
        });
        render(null, '<div marketing-sign-up-form><p ng-if="gdprAppliesToUser" class="privacy-notice"></p></div>');
        SpecHelper.expectElement(elem, '.privacy-notice');
    });
});
