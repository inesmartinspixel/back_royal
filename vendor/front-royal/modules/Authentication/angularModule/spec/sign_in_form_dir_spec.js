import 'AngularSpecHelper';
import 'Authentication/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import signInFormLocales from 'Authentication/locales/authentication/sign_in_form-en.json';
import authFormHelperMixinLocales from 'Authentication/locales/authentication/auth_form_helper_mixin-en.json';

setSpecLocales(signInFormLocales, authFormHelperMixinLocales);

describe('Authentication::SignInForm', () => {
    let $injector;
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let $auth;
    let $q;
    let ngToast;
    let AuthFormHelperMixin;
    let ValidationResponder;
    let $rootScope;
    let DialogModal;
    let TranslationHelper;
    let translationHelper;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Authentication', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector => {
                $injector = _$injector;
                SpecHelper = $injector.get('SpecHelper');
                $auth = $injector.get('$auth');
                $q = $injector.get('$q');
                ngToast = $injector.get('ngToast');
                AuthFormHelperMixin = $injector.get('AuthFormHelperMixin');
                ValidationResponder = $injector.get('ValidationResponder');
                $rootScope = $injector.get('$rootScope');
                DialogModal = $injector.get('DialogModal');

                SpecHelper.mockCapabilities();
                SpecHelper.stubConfig();
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.showForgotPassword = true;
        renderer.render('<sign-in-form show-forgot-password="showForgotPassword"></sign-in-form>');
        elem = renderer.elem;
        scope = renderer.childScope;
    }

    it('should use AuthFormHelperMixin', () => {
        jest.spyOn(AuthFormHelperMixin, 'onLink');
        render();
        expect(AuthFormHelperMixin.onLink).toHaveBeenCalled();
    });

    describe('form fields', () => {
        it('should have email and password inputs', () => {
            render();

            const inputs = SpecHelper.expectElements(elem, 'input', 2);
            SpecHelper.expectElementHasClass(inputs, 'form-control');

            expect(inputs.eq(0).attr('name')).toEqual('email');
            expect(inputs.eq(0).attr('type')).toEqual('email');
            expect(inputs.eq(0).attr('required')).toEqual('required');

            expect(inputs.eq(1).attr('name')).toEqual('password');
            expect(inputs.eq(1).attr('type')).toEqual('password');
            expect(inputs.eq(1).attr('required')).toEqual('required');
            expect(inputs.eq(1).attr('autocomplete')).toEqual('off');
            expect(inputs.eq(1).attr('autocorrect')).toEqual('off');
            expect(inputs.eq(1).attr('autocapitalize')).toEqual('off');
            expect(inputs.eq(1).attr('spellcheck')).toEqual('false');
        });
    });

    describe('form validation', () => {
        beforeEach(() => {
            render();
        });

        it('should disable the submit button until valid', () => {
            SpecHelper.expectElementDisabled(elem, 'button[type="submit"]');

            SpecHelper.updateTextInput(elem, 'input[name="email"]', 'someuser@email.com');
            SpecHelper.expectElementDisabled(elem, 'button[type="submit"]');

            SpecHelper.updateTextInput(elem, 'input[name="password"]', 'password');
            SpecHelper.expectElementEnabled(elem, 'button[type="submit"]');
        });
    });

    describe('omniauth', () => {
        let defaultSignupCode;

        beforeEach(() => {
            defaultSignupCode = $injector.get('ConfigFactory').getSync().join_config.default.signup_code;
            render();
            jest.spyOn(scope, 'loginWithProvider').mockImplementation(() => {});
        });

        it('should login with Facebook', () => {
            SpecHelper.click(elem, '.soc-no-block-fb-btn');
            expect(scope.loginWithProvider).toHaveBeenCalledWith('facebook', defaultSignupCode);
        });

        it('should login with Google', () => {
            SpecHelper.click(elem, '.soc-no-block-g-btn');
            expect(scope.loginWithProvider).toHaveBeenCalledWith('google_oauth2', defaultSignupCode);
        });

        it('should login with Apple', () => {
            SpecHelper.click(elem, '.soc-no-block-a-btn');
            expect(scope.loginWithProvider).toHaveBeenCalledWith('apple', defaultSignupCode);
        });
    });

    describe('form submission', () => {
        beforeEach(() => {
            render();
            SpecHelper.expectElement(elem, 'button[type="submit"]');
            SpecHelper.updateTextInput(elem, 'input[name="email"]', 'someuser@email.com');
            expect(scope.form.email).toEqual('someuser@email.com');

            SpecHelper.updateTextInput(elem, 'input[name="password"]', 'password');
            expect(scope.form.password).toEqual('password');
            SpecHelper.expectElementEnabled(elem, 'button[type="submit"]');
        });

        it('should disable the submit button during submissions', () => {
            let deferred;

            // failure
            deferred = $q.defer();
            jest.spyOn($auth, 'submitLogin').mockReturnValue(deferred.promise);
            SpecHelper.submitForm(elem);
            SpecHelper.expectElementDisabled(elem, 'button[type="submit"]');
            deferred.reject();
            scope.$digest();
            SpecHelper.expectElementEnabled(elem, 'button[type="submit"]');

            // success
            deferred = $q.defer();
            $auth.submitLogin.mockReturnValue(deferred.promise);
            SpecHelper.submitForm(elem);
            SpecHelper.expectElementDisabled(elem, 'button[type="submit"]');
            deferred.resolve();
            scope.$digest();
            SpecHelper.expectElementEnabled(elem, 'button[type="submit"]');
        });

        it('should handle valid logins', () => {
            const deferred = $q.defer();
            jest.spyOn($auth, 'submitLogin').mockReturnValue(deferred.promise);
            jest.spyOn(ngToast, 'create').mockImplementation(() => {});
            SpecHelper.submitForm(elem);
            expect($auth.submitLogin).toHaveBeenCalledWith(scope.form);
            deferred.resolve();
            scope.$digest();
            expect(ngToast.create).not.toHaveBeenCalled();
        });

        it('should show errors upon invalid logins', () => {
            jest.spyOn($auth, 'submitLogin').mockReturnValue($q.reject());
            SpecHelper.submitForm(elem);
            expect($auth.submitLogin).toHaveBeenCalledWith(scope.form);
            SpecHelper.expectElementText(elem, '.form-error', '');
        });

        it('should show errors upon invalid phone_no_password logins', () => {
            scope.urlConfig.provider = 'phone_no_password';
            scope.$digest();
            SpecHelper.expectElement(elem, 'tel-input');
            jest.spyOn($auth, 'submitLogin').mockReturnValue($q.reject());
            SpecHelper.submitForm(elem);
            expect($auth.submitLogin).toHaveBeenCalledWith(scope.form);
            SpecHelper.expectElements(elem, '.form-error', 2);
            SpecHelper.expectElementText(elem, '.form-error:eq(0)', '');
        });

        it('should alert the user if they attempted to sign in with an invalid account', () => {
            jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
            const eventResponse = 'This is an apologetic message from the server';
            $rootScope.$broadcast('auth:login-error', eventResponse);
            TranslationHelper = $injector.get('TranslationHelper');
            translationHelper = new TranslationHelper('authentication.auth_form_helper_mixin');
            expect(DialogModal.alert).toHaveBeenCalledWith({
                content: eventResponse,
                classes: ['server-error-modal'],
                title: translationHelper.get('server_error'),
            });
        });

        it('should forward to target url after valid login', () => {
            jest.spyOn(ValidationResponder, 'forwardToTargetUrl').mockImplementation(() => {});
            $rootScope.$broadcast('validation-responder:login-success');
            expect(ValidationResponder.forwardToTargetUrl).toHaveBeenCalled();

            // make sure we destroy the listener
            ValidationResponder.forwardToTargetUrl.mockClear();
            scope.$destroy();
            scope.$broadcast('validation-responder:login-success');
            expect(ValidationResponder.forwardToTargetUrl).not.toHaveBeenCalled();
        });
    });

    it('should send user to institutional forgot password route if on institutional sign-in', () => {
        render();
        jest.spyOn(scope, 'loadRoute').mockImplementation(() => {});
        jest.spyOn(scope, 'getUrlPrefix').mockReturnValue('test/'); // would rather mock out $route attributes here
        SpecHelper.click(elem, '.forgot-password-link-container a');
        expect(scope.loadRoute).toHaveBeenCalledWith('/test/forgot-password', undefined);
    });
});
