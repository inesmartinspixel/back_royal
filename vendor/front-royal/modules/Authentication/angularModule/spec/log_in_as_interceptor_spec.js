import 'AngularSpecHelper';
import 'Authentication/angularModule';

describe('LogInAsInterceptor', () => {
    let $injector;
    let SpecHelper;
    let $rootScope;
    let $window;
    let $q;
    let LogInAsInterceptor;
    let config;
    let ClientStorage;
    let requestTerminator;

    angular.mock.module.sharedInjector();

    beforeAll(() => {
        angular.mock.module('SpecHelper', 'FrontRoyal.Authentication');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                $rootScope = $injector.get('$rootScope');
                $window = $injector.get('$window');
                $q = $injector.get('$q');
                LogInAsInterceptor = $injector.get('LogInAsInterceptor');
                ClientStorage = $injector.get('ClientStorage');
            },
        ]);

        SpecHelper.stubCurrentUser();
        jest.spyOn(LogInAsInterceptor, 'request');
    });

    afterAll(() => {
        SpecHelper.cleanup();
    });

    describe('request', () => {
        beforeEach(() => {
            config = {
                foo: 'bar',
            };
            requestTerminator = {
                resolve: jest.fn(),
            };
            jest.spyOn($window, 'alert').mockImplementation(jest.fn());
        });

        afterEach(() => {
            ClientStorage.removeItem('logged_in_as');
            ClientStorage.removeItem('logged_in_as_user_id');
        });

        it('should resolve terminator and alert user when logged_in_as and user IDs do not match', () => {
            jest.spyOn($q, 'defer').mockReturnValue(requestTerminator);
            ClientStorage.setItem('logged_in_as', true);
            ClientStorage.setItem('logged_in_as_user_id', '1234567890');
            LogInAsInterceptor.request(config);
            expect(requestTerminator.resolve).toHaveBeenCalled();
            expect($window.alert).toHaveBeenCalled();
        });

        it('should not abort a request when not logged_in_as', () => {
            jest.spyOn($q, 'defer').mockReturnValue(requestTerminator);
            ClientStorage.setItem('logged_in_as', false);
            LogInAsInterceptor.request(config);
            expect(requestTerminator.resolve).not.toHaveBeenCalled();
            expect($window.alert).not.toHaveBeenCalled();
        });

        it('should not abort a request when logged_in_as and user IDs match', () => {
            jest.spyOn($q, 'defer').mockReturnValue(requestTerminator);
            ClientStorage.setItem('logged_in_as', true);
            ClientStorage.setItem('logged_in_as_user_id', $rootScope.currentUser.id);
            LogInAsInterceptor.request(config);
            expect(requestTerminator.resolve).not.toHaveBeenCalled();
            expect($window.alert).not.toHaveBeenCalled();
        });

        // See https://trello.com/c/DzVKPGJU for more information
        it('should not reset config timeout when not logged_in_as', () => {
            ClientStorage.setItem('logged_in_as', false);
            const deferred = $q.defer();
            config.timeout = deferred.promise;
            LogInAsInterceptor.request(config);
            expect(config.timeout).toBe(deferred.promise);
        });
    });
});
