import moment from 'moment-timezone';
import 'AngularSpecHelper';
import 'Authentication/angularModule';

describe('Authentication::Omniauth', () => {
    let SpecHelper;
    let ClientStorage;
    let omniauth;
    let $auth;
    let $timeout;
    let $location;
    let $window;
    let preSignupValues;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Authentication', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            ClientStorage = $injector.get('ClientStorage');
            omniauth = $injector.get('omniauth');
            $auth = $injector.get('$auth');
            $timeout = $injector.get('$timeout');
            $location = $injector.get('$location');
            $window = $injector.get('$window');
            preSignupValues = $injector.get('preSignupValues');
        });

        SpecHelper.stubConfig();
    });

    afterEach(() => {
        SpecHelper.cleanup();
        ClientStorage.removeItem('auid');
    });

    describe('loginWithProvider', () => {
        it('should assign an auid if there is one', () => {
            ClientStorage.setItem('auid', 'some_auid');
            jest.spyOn($auth, 'authenticate').mockImplementation(() => {});
            omniauth.loginWithProvider('provider');
            $timeout.flush(); // load config
            expect($auth.authenticate).toHaveBeenCalled();
            expect($auth.authenticate.mock.calls[0][1].params.id).toEqual('some_auid');
        });

        it('should not assign an auid if there is none', () => {
            ClientStorage.removeItem('auid');
            jest.spyOn($auth, 'authenticate').mockImplementation(() => {});
            omniauth.loginWithProvider('provider');
            $timeout.flush(); // load config
            expect($auth.authenticate).toHaveBeenCalled();
            expect($auth.authenticate.mock.calls[0][1].params.hasOwnProperty('id')).toBe(false);
        });

        it('should pass along the sign_up_code', () => {
            jest.spyOn($auth, 'authenticate').mockImplementation(() => {});
            omniauth.loginWithProvider('provider', 'signUpCode');
            $timeout.flush(); // load config
            expect($auth.authenticate.mock.calls[0][1].params.sign_up_code).toBe('signUpCode');
        });

        it('should pass along the timezone', () => {
            jest.spyOn($auth, 'authenticate').mockImplementation(() => {});
            jest.spyOn(moment.tz, 'guess').mockReturnValue('America/Chicago');
            omniauth.loginWithProvider('provider', 'signUpCode');
            $timeout.flush(); // load config
            expect($auth.authenticate.mock.calls[0][1].params.timezone).toBe('America/Chicago');
        });

        it('should add a program_type if there is one', () => {
            jest.spyOn($auth, 'authenticate').mockImplementation(() => {});
            omniauth.loginWithProvider('provider', 'signUpCode', 'some_program_type');
            $timeout.flush(); // load config
            expect($auth.authenticate).toHaveBeenCalled();
            expect($auth.authenticate.mock.calls[0][1].params.hasOwnProperty('program_type')).toBe(true);
            expect($auth.authenticate.mock.calls[0][1].params.program_type).toEqual('some_program_type');
        });

        it('should not add a program_type if there is none', () => {
            jest.spyOn($auth, 'authenticate').mockImplementation(() => {});
            omniauth.loginWithProvider('provider', 'signUpCode');
            $timeout.flush(); // load config
            expect($auth.authenticate).toHaveBeenCalled();
            expect($auth.authenticate.mock.calls[0][1].params.hasOwnProperty('program_type')).toBe(false);
        });

        it('should pass along skip_apply if provided', () => {
            jest.spyOn($location, 'search').mockReturnValue({
                skip_apply: true,
            });
            jest.spyOn($auth, 'authenticate').mockImplementation(() => {});
            omniauth.loginWithProvider('provider', 'signUpCode');
            $timeout.flush(); // load config
            expect($auth.authenticate).toHaveBeenCalled();
            expect($auth.authenticate.mock.calls[0][1].params.skip_apply).toBe(true);
        });

        it('should not pass along skip_apply if not provided', () => {
            jest.spyOn($location, 'search');
            jest.spyOn($auth, 'authenticate').mockImplementation(() => {});
            omniauth.loginWithProvider('provider', 'signUpCode');
            $timeout.flush(); // load config
            expect($auth.authenticate).toHaveBeenCalled();
            expect($auth.authenticate.mock.calls[0][1].params.hasOwnProperty('skip_apply')).toBe(false);
        });

        it('should add a referred_by_id if there is one', () => {
            jest.spyOn(ClientStorage, 'getItem').mockReturnValue('some_uuid');
            jest.spyOn($auth, 'authenticate').mockImplementation(() => {});
            omniauth.loginWithProvider('provider', 'signUpCode');
            $timeout.flush(); // load config
            expect($auth.authenticate).toHaveBeenCalled();
            expect($auth.authenticate.mock.calls[0][1].params.hasOwnProperty('referred_by_id')).toBe(true);
            expect($auth.authenticate.mock.calls[0][1].params.referred_by_id).toEqual('some_uuid');
        });

        it('should not add a referred_by_id if there is none', () => {
            jest.spyOn(ClientStorage, 'getItem').mockReturnValue(null);
            jest.spyOn($auth, 'authenticate').mockImplementation(() => {});
            omniauth.loginWithProvider('provider', 'signUpCode');
            $timeout.flush(); // load config
            expect($auth.authenticate).toHaveBeenCalled();
            expect($auth.authenticate.mock.calls[0][1].params.hasOwnProperty('referred_by_id')).toBe(false);
        });

        it('should getDynamicLandingPageMultiStepFormRegistrationInfo and add it to params', () => {
            jest.spyOn(preSignupValues, 'getDynamicLandingPageMultiStepFormRegistrationInfo').mockReturnValue({
                foo: 'foo',
            });
            jest.spyOn($auth, 'authenticate').mockImplementation(() => {});
            omniauth.loginWithProvider('provider', 'signUpCode');
            $timeout.flush(); // load config
            expect($auth.authenticate).toHaveBeenCalled();
            expect($auth.authenticate.mock.calls[0][1].params.hasOwnProperty('foo')).toBe(true);
            expect($auth.authenticate.mock.calls[0][1].params.foo).toEqual('foo');
        });
    });
});
