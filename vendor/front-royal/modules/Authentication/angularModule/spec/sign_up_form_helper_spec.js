import moment from 'moment-timezone';
import 'AngularSpecHelper';
import 'Authentication/angularModule';
import signUpFormLocales from 'Authentication/locales/authentication/sign_up_form-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(signUpFormLocales);

/* eslint-disable no-prototype-builtins */
describe('Authentication::SignUpFormHelper', () => {
    let $injector;
    let SpecHelper;
    let scope;
    let signUpFormHelper;
    let AuthFormHelperMixin;
    let SignUpFormHelper;
    let $q;
    let $timeout;
    let forwardToNextPage;
    let ClientStorage;
    let $auth;
    let preSignupValues;
    let ValidationResponder;
    let $location;
    let $rootScope;
    let EventBundle;
    let Config;
    let $window;
    let captchaHelper;
    let EventLogger;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Authentication', 'SpecHelper');

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            SpecHelper = $injector.get('SpecHelper');
            AuthFormHelperMixin = $injector.get('AuthFormHelperMixin');
            SignUpFormHelper = $injector.get('SignUpFormHelper');
            $q = $injector.get('$q');
            $timeout = $injector.get('$timeout');
            ClientStorage = $injector.get('ClientStorage');
            $location = $injector.get('$location');
            $auth = $injector.get('$auth');
            preSignupValues = $injector.get('preSignupValues');
            ValidationResponder = $injector.get('ValidationResponder');
            $location = $injector.get('$location');
            $rootScope = $injector.get('$rootScope');
            EventBundle = $injector.get('EventLogger.EventBundle');
            Config = $injector.get('Config');
            $window = $injector.get('$window');
            captchaHelper = $injector.get('captchaHelper');
            EventLogger = $injector.get('EventLogger');
        });

        SpecHelper.stubConfig();

        // eslint-disable-next-line no-shadow
        jest.spyOn(AuthFormHelperMixin, 'onLink').mockImplementation(scope => {
            scope.preventSubmit = false;
            scope.logPreSignupForm = jest.fn();
        });

        jest.spyOn(captchaHelper, 'resetCaptcha').mockImplementation(() => {
            // noop
        });

        scope = $rootScope.$new();
        signUpFormHelper = new SignUpFormHelper(scope);
        forwardToNextPage = jest.fn();
        signUpFormHelper.onForwardToNextPage(forwardToNextPage);
    });

    describe('initialize', () => {
        it('should use AuthFormHelperMixin', () => {
            signUpFormHelper = new SignUpFormHelper(scope);
            expect(AuthFormHelperMixin.onLink).toHaveBeenCalled();

            // FIXME: the way AuthFormHelper mixin handles onOmniAuthSuccess and
            // onOmniAuthFailure makes it really hard to test.
        });
    });

    describe('getConfig', () => {
        it('should work', () => {
            const JoinConfig = $injector.get('JoinConfig');
            const urlConfig = {
                signup_code: 'signup_code',
            };

            jest.spyOn(JoinConfig, 'getConfig').mockReturnValue($q.when(urlConfig));
            jest.spyOn(EventLogger.prototype, 'log').mockImplementation(() => {});
            signUpFormHelper.getConfig('urlPrefix');
            $timeout.flush();

            expect(scope.urlConfig).toBe(urlConfig);
            expect(scope.signUpCode).toBe(urlConfig.signup_code);
            expect(JoinConfig.getConfig).toHaveBeenCalledWith('urlPrefix');
            expect(EventLogger.prototype.log).toHaveBeenCalledWith('viewed_join_form', {
                sign_up_code: 'signup_code',
            });
        });
    });

    describe('attemptSignIn', () => {
        it('should work', () => {
            const deferred = $q.defer();
            const loginInfo = {
                email: 'user@example.com',
            };

            jest.spyOn($auth, 'submitLogin').mockReturnValue(deferred.promise);
            signUpFormHelper.attemptSignIn(loginInfo);
            expect($auth.submitLogin).toHaveBeenCalledWith(loginInfo);

            const Hippos = $injector.get('Hippos');
            jest.spyOn(Hippos.prototype, 'save').mockImplementation(() => {});
            jest.spyOn(signUpFormHelper, '_forwardToNextPage').mockImplementation(() => {});

            deferred.resolve();
            $timeout.flush();

            expect(Hippos.prototype.save).toHaveBeenCalledWith(loginInfo);
            expect(signUpFormHelper._forwardToNextPage).toHaveBeenCalled();
        });
    });

    describe('submitRegistration', () => {
        beforeEach(() => {
            SpecHelper.stubEventLogging();
        });

        const mockUserResponse = {
            data: {
                data: {},
            },
        };

        beforeEach(() => {
            // getConfig would normally set this
            scope.urlConfig = {
                provider: 'email',
            };
            scope.signUpCode = 'CODE';
        });

        afterEach(() => {
            ClientStorage.removeItem('auid');
        });

        it('should set preventSubmit during submission and reset on failure', () => {
            const deferred = submitRegistrationAndAssertPreventSubmit();
            deferred.reject();
            $timeout.flush();
            expect(scope.preventSubmit).toBe(false);
        });

        it('should set preventSubmit during submission and reset on success', () => {
            const deferred = submitRegistrationAndAssertPreventSubmit();
            deferred.resolve(mockUserResponse);
            $timeout.flush();
            expect(scope.preventSubmit).toBe(true);
        });

        it('should call logPreSignupForm', () => {
            signUpFormHelper.submitRegistration({});
            expect(signUpFormHelper.scope.logPreSignupForm).toHaveBeenCalledWith();
        });

        it('should log auth:submit_registration', () => {
            jest.spyOn(EventLogger.prototype, 'log');
            signUpFormHelper.submitRegistration({});
            expect(EventLogger.prototype.log).toHaveBeenCalledWith('auth:submit_registration', { provider: 'email' });
        });

        it('should add preSignupValues to call to $auth.submitRegistration', () => {
            jest.spyOn($auth, 'submitRegistration');
            preSignupValues.all = jest.fn().mockReturnValue({ some: 'values' });
            signUpFormHelper.submitRegistration({});
            expect($auth.submitRegistration).toHaveBeenCalled();
            expect($auth.submitRegistration.mock.calls[0][0].some).toEqual('values');
        });

        it('should add an auid if there is one', () => {
            ClientStorage.setItem('auid', 'some_auid');
            const deferred = $q.defer();
            jest.spyOn($auth, 'submitRegistration').mockReturnValue(deferred.promise);
            signUpFormHelper.submitRegistration({});
            expect($auth.submitRegistration).toHaveBeenCalled();
            expect($auth.submitRegistration.mock.calls[0][0].id).toEqual('some_auid');
        });

        it('should not add an auid if there is none', () => {
            ClientStorage.removeItem('auid');
            const deferred = $q.defer();
            jest.spyOn($auth, 'submitRegistration').mockReturnValue(deferred.promise);
            signUpFormHelper.submitRegistration({});
            expect($auth.submitRegistration).toHaveBeenCalled();
            // eslint-disable-next-line no-prototype-builtins
            expect($auth.submitRegistration.mock.calls[0][0].hasOwnProperty('id')).toBe(false);
        });

        it('should pass along skip_apply if provided', () => {
            jest.spyOn($location, 'search').mockReturnValue({
                skip_apply: true,
            });
            const deferred = $q.defer();
            jest.spyOn($auth, 'submitRegistration').mockReturnValue(deferred.promise);
            signUpFormHelper.submitRegistration({});
            expect($auth.submitRegistration).toHaveBeenCalled();
            expect($auth.submitRegistration.mock.calls[0][0].skip_apply).toBe(true);
        });

        it('should not pass along skip_apply if not provided', () => {
            jest.spyOn($location, 'search');
            const deferred = $q.defer();
            jest.spyOn($auth, 'submitRegistration').mockReturnValue(deferred.promise);
            signUpFormHelper.submitRegistration({});
            expect($auth.submitRegistration).toHaveBeenCalled();
            expect($auth.submitRegistration.mock.calls[0][0].hasOwnProperty('skip_apply')).toBe(false);
        });

        it('should add a referred_by_id if there is one', () => {
            jest.spyOn(ClientStorage, 'getItem').mockReturnValue('some_uuid');
            const deferred = $q.defer();
            jest.spyOn($auth, 'submitRegistration').mockReturnValue(deferred.promise);
            signUpFormHelper.submitRegistration({});
            expect($auth.submitRegistration).toHaveBeenCalled();
            expect($auth.submitRegistration.mock.calls[0][0].referred_by_id).toEqual('some_uuid');
        });

        it('should not add a referred_by_id if there is none', () => {
            jest.spyOn(ClientStorage, 'getItem').mockReturnValue(null);
            const deferred = $q.defer();
            jest.spyOn($auth, 'submitRegistration').mockReturnValue(deferred.promise);
            signUpFormHelper.submitRegistration({});
            expect($auth.submitRegistration).toHaveBeenCalled();
            expect($auth.submitRegistration.mock.calls[0][0].hasOwnProperty('referred_by_id')).toBe(false);
        });

        it('should add preSignupValues', () => {
            const deferred = $q.defer();
            jest.spyOn(preSignupValues, 'all').mockReturnValue({
                a: 'b',
            });
            jest.spyOn($auth, 'submitRegistration').mockReturnValue(deferred.promise);
            signUpFormHelper.submitRegistration({});
            expect($auth.submitRegistration).toHaveBeenCalled();
            expect($auth.submitRegistration.mock.calls[0][0].a).toBe('b');
        });

        it('should add sign_up_code', () => {
            const deferred = $q.defer();
            jest.spyOn($auth, 'submitRegistration').mockReturnValue(deferred.promise);
            signUpFormHelper.submitRegistration({});
            expect($auth.submitRegistration).toHaveBeenCalled();
            expect(scope.signUpCode).not.toBeUndefined(); // sanity check
            expect($auth.submitRegistration.mock.calls[0][0].sign_up_code).toBe(scope.signUpCode);
        });

        it('should add timezone', () => {
            const timezone = 'America/Chicago';
            const deferred = $q.defer();
            jest.spyOn($auth, 'submitRegistration').mockReturnValue(deferred.promise);
            jest.spyOn(moment.tz, 'guess').mockReturnValue(timezone);
            signUpFormHelper.submitRegistration({});
            expect($auth.submitRegistration).toHaveBeenCalled();
            expect($auth.submitRegistration.mock.calls[0][0].timezone).toBe(timezone);
        });

        it('should handle valid registrations', () => {
            const deferred = $q.defer();
            jest.spyOn($auth, 'submitRegistration').mockReturnValue(deferred.promise);
            jest.spyOn(ValidationResponder, 'handleValidationSuccess').mockImplementation(() => {});
            jest.spyOn(ValidationResponder, 'forwardToTargetUrl').mockImplementation(() => {});
            jest.spyOn($location, 'path').mockImplementation(() => {});
            jest.spyOn($rootScope, '$broadcast');
            EventBundle.expect('create');

            const registrationInfo = {
                a: 'b',
            };
            signUpFormHelper.submitRegistration(registrationInfo);
            deferred.resolve(mockUserResponse);
            Config.expect('index');
            scope.$digest();

            expect($auth.submitRegistration).toHaveBeenCalledWith(registrationInfo);
            expect($rootScope.$broadcast).toHaveBeenCalledWith('auth:validation-success', mockUserResponse.data.data);

            expect(forwardToNextPage).not.toHaveBeenCalled();
            $rootScope.$broadcast('validation-responder:login-success');
            scope.$digest();
            expect(forwardToNextPage).toHaveBeenCalled();
        });

        describe('recaptcha', () => {
            beforeEach(() => {
                $window.grecaptcha = {
                    // eslint-disable-next-line lodash-fp/prefer-constant
                    reset() {
                        return 'foobar';
                    },
                };
                $window.captchaComplete = true;
            });

            afterEach(() => {
                $window.CORDOVA = false;
            });

            it('should call resetCaptcha upon invalid registration', () => {
                jest.spyOn($window.grecaptcha, 'reset').mockImplementation(() => {});
                jest.spyOn($auth, 'submitRegistration').mockReturnValue($q.reject());
                signUpFormHelper.submitRegistration({});
                $timeout.flush();
                expect(captchaHelper.resetCaptcha).toHaveBeenCalled();
            });
        });

        it('should add error messages upon invalid registrations', () => {
            const errorResponse = {
                data: {
                    errors: {
                        password: 'is too short!',
                    },
                },
            };
            jest.spyOn($auth, 'submitRegistration').mockReturnValue($q.reject(errorResponse));
            signUpFormHelper.submitRegistration({});
            $timeout.flush();
            expect(scope.form_errors).toEqual({
                password: 'Password is too short!',
            });
        });

        it('should successfully login with an existing email and password', () => {
            const errorResponse = {
                data: {
                    errors: {
                        email: ['already in use'],
                    },
                },
            };

            // join request fails
            jest.spyOn($auth, 'submitRegistration').mockReturnValue($q.reject(errorResponse));

            // login request succeeds
            jest.spyOn($auth, 'submitLogin').mockReturnValue($q.resolve());
            jest.spyOn(ValidationResponder, 'forwardToTargetUrl').mockImplementation(() => {});
            signUpFormHelper.submitRegistration({});
            $timeout.flush();
            $timeout.flush();
            expect(forwardToNextPage).toHaveBeenCalled();
        });

        it('should show email in use error with an existing email and wrong password', () => {
            const errorResponse = {
                data: {
                    errors: {
                        email: ['already in use'],
                        full_messages: ['Email address is already in use'],
                    },
                },
            };

            // join request fails
            jest.spyOn($auth, 'submitRegistration').mockReturnValue($q.reject(errorResponse));

            // and login request fails
            jest.spyOn($auth, 'submitLogin').mockReturnValue($q.reject());
            signUpFormHelper.submitRegistration({});
            $timeout.flush();
            $timeout.flush();
            expect(forwardToNextPage).not.toHaveBeenCalled();
            scope.$digest();
            expect(scope.form_errors).toEqual({
                email: 'Email address is already in use',
            });
        });

        it('should log an event with answers to the onboarding questions after successful registration', () => {
            const onboardingQuestions = JSON.stringify({
                question1: 'test',
                question2: 'test2',
            });

            jest.spyOn(EventLogger.prototype, 'log').mockImplementation(() => {});
            const deferred = $q.defer();
            jest.spyOn($auth, 'submitRegistration').mockReturnValue(deferred.promise);
            jest.spyOn(ValidationResponder, 'handleValidationSuccess').mockImplementation(() => {});
            jest.spyOn($rootScope, '$broadcast');
            jest.spyOn(ClientStorage, 'getItem').mockReturnValue(onboardingQuestions);
            EventBundle.expect('create');

            signUpFormHelper.submitRegistration({});
            deferred.resolve(mockUserResponse);
            Config.expect('index');
            scope.$digest();

            expect(EventLogger.prototype.log).toHaveBeenCalledWith('auth:registration-success', {
                onboarding_questions: JSON.parse(onboardingQuestions),
            });
        });
    });

    function submitRegistrationAndAssertPreventSubmit() {
        const deferred = $q.defer();
        jest.spyOn($auth, 'submitRegistration').mockReturnValue(deferred.promise);
        expect(scope.preventSubmit).toBe(false);
        signUpFormHelper.submitRegistration({});
        expect(scope.preventSubmit).toBe(true);
        return deferred;
    }

    afterEach(() => {
        SpecHelper.cleanup();
    });
});
