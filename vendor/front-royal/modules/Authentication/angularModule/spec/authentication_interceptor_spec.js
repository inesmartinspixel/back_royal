import 'AngularSpecHelper';
import 'Authentication/angularModule';

import Auid from 'Auid';

describe('AuthenticationInterceptor', () => {
    let SpecHelper;
    let Interceptor;

    angular.mock.module.sharedInjector();

    beforeAll(() => {
        // SpecHelper has to be first so
        // that localStorage will be cleaned up before the
        // Interceptor is initialized in each test
        angular.mock.module('SpecHelper', 'FrontRoyal.Authentication');

        let ipCookie;

        angular.mock.inject([
            '$injector',
            $injector => {
                SpecHelper = $injector.get('SpecHelper');
                Interceptor = $injector.get('AuthenticationInterceptor');
                ipCookie = $injector.get('ipCookie');
            },
        ]);

        ipCookie.remove('auid');
    });

    afterAll(() => {
        SpecHelper.cleanup();
    });

    describe('request', () => {
        it('should add a consistent anonymous user id to all requests', () => {
            jest.spyOn(Auid, 'ensure').mockImplementation(() => 'test-guid');

            let config = Interceptor.request({});
            expect(config.headers).toEqual({
                auid: 'test-guid',
                'scorm-token': undefined,
            });

            // shouldn't re-generate
            config = Interceptor.request({});
            expect(config.headers).toEqual({
                auid: 'test-guid',
                'scorm-token': undefined,
            });
        });
    });
});
