import 'AngularSpecHelper';
import 'Authentication/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import forgotPasswordLocales from 'Authentication/locales/authentication/forgot_password-en.json';
import sessionsNavigationLocales from 'Navigation/locales/navigation/sessions_navigation-en.json';

setSpecLocales(forgotPasswordLocales, sessionsNavigationLocales);

describe('Authentication::ForgotPassword', () => {
    let renderer;
    let elem;
    let SpecHelper;
    let $location;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Authentication', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            $location = $injector.get('$location');
            SpecHelper.stubConfig();
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.render('<forgot-password></forgot-password>');
        elem = renderer.elem;
    }

    it('should have a link to the Sign-In page', () => {
        render();
        jest.spyOn($location, 'url').mockImplementation(() => {});
        SpecHelper.click(elem, '.sign-in-link a');
        expect($location.url).toHaveBeenCalledWith('/sign-in');
    });
});
