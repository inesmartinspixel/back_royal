import 'AngularSpecHelper';
import 'Authentication/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import signUpFormLocales from 'Authentication/locales/authentication/sign_up_form-en.json';

setSpecLocales(signUpFormLocales);

describe('Authentication.SignUpForm', () => {
    let $injector;
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let $timeout;
    let JoinConfig;
    let $q;
    let SignUpFormHelper;
    let ClientStorage;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Authentication', 'SpecHelper');

        angular.mock.module($provide => {
            $provide.value('ipCookie', jest.fn());
        });

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            SpecHelper = $injector.get('SpecHelper');
            $timeout = $injector.get('$timeout');
            $q = $injector.get('$q');
            ClientStorage = $injector.get('ClientStorage');
            SignUpFormHelper = $injector.get('SignUpFormHelper');
            JoinConfig = $injector.get('JoinConfig');

            SpecHelper.mockCapabilities();

            SpecHelper.stubConfig();

            SpecHelper.stubDirective('locationAutocomplete');

            SpecHelper.stubEventLogging();
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render(urlConfigOverrides) {
        renderer = SpecHelper.renderer();

        let defaultJoinConfig;
        JoinConfig.getConfig('default').then(config => {
            defaultJoinConfig = config;
        });
        $timeout.flush();

        jest.spyOn(JoinConfig, 'getConfig').mockReturnValue(
            $q.when(_.extend({}, defaultJoinConfig, urlConfigOverrides)),
        );

        renderer.render('<sign-up-form url-prefix="\'prefix\'"></sign-up-form>');

        elem = renderer.elem;
        scope = renderer.childScope;

        expect(JoinConfig.getConfig).toHaveBeenCalledWith('prefix');
    }

    it('should use SignUpFormHelper', () => {
        jest.spyOn(SignUpFormHelper.prototype, 'initialize');
        jest.spyOn(SignUpFormHelper.prototype, 'getConfig');
        render();
        expect(SignUpFormHelper.prototype.initialize).toHaveBeenCalledWith(scope);
        expect(SignUpFormHelper.prototype.getConfig).toHaveBeenCalledWith(scope.urlPrefix);
    });

    describe('form fields', () => {
        let $window;

        beforeEach(() => {
            $window = $injector.get('$window');
        });

        afterEach(() => {
            $window.CORDOVA = false;
            delete $window.plugins;
        });

        it('should show text-like fields (text, email, tel)', () => {
            render({
                included_field_ids: ['name', 'job_title', 'phone', 'email'],
            });

            $timeout.flush();

            const inputs = SpecHelper.expectElements(elem, 'input', 4);
            SpecHelper.expectElementHasClass(inputs, 'form-control');

            // for the first input, check all the fields
            expect(inputs.eq(0).attr('name')).toEqual('name');

            expect(inputs.eq(0).attr('type')).toEqual('text');
            expect(inputs.eq(0).attr('required')).toEqual('required');
            expect(inputs.eq(0).attr('maxlength')).toEqual('255');

            // since the code is shared, we'll assume the rest of the
            // text, email, and tel fields get all the same attrs the
            // first one did, and just check the names
            expect(inputs.eq(1).attr('name')).toEqual('job_title');

            SpecHelper.expectElement(elem, 'select[name="countryCode"]');

            expect(inputs.eq(2).attr('name')).toEqual('phone');
            expect(inputs.eq(3).attr('name')).toEqual('email');
        });

        it('should show password fields', () => {
            render({
                included_field_ids: ['password'],
            });

            const input = SpecHelper.expectElements(elem, '[name="password"]');
            SpecHelper.expectElementHasClass(input, 'form-control');
            expect(input.attr('type')).toEqual('password');
            expect(input.attr('required')).toEqual('required');
            expect(input.attr('autocomplete')).toEqual('off');
            expect(input.attr('autocorrect')).toEqual('off');
            expect(input.attr('autocapitalize')).toEqual('off');
            expect(input.attr('spellcheck')).toEqual('false');

            // click to show the password
            SpecHelper.click(elem, '.form-show-password');
            expect(input.attr('type')).toEqual('text');
        });

        it('should show selectize fields', () => {
            render({
                included_field_ids: ['country'],
            });

            SpecHelper.expectElement(elem, 'selectize[name="country"]', 'United States (USA)');
        });

        it('should populate the email field with the prefilledEmail in local storage', () => {
            ClientStorage.setItem('prefilledEmail', 'test@email.com');
            render({
                included_field_ids: ['email'],
            });
            expect(elem.find('[name="email"]').val()).toEqual('test@email.com');
            expect(ClientStorage.getItem('prefilledEmail')).toBeUndefined();
        });

        it('should show organization-autocomplete field', () => {
            render({
                included_field_ids: ['company'],
            });

            SpecHelper.expectElement(elem, 'organization-autocomplete');
        });

        it('should set a hidden client_id value if Cordova', () => {
            $window.CORDOVA = true;
            $window.googleOauthId = '1234abcd';
            $window.plugins = {
                socialsharing: {
                    canShareVia: angular.noop,
                },
            };
            render();
            expect(scope.form.client_id).toEqual('1234abcd');
            $window.googleOauthId = undefined;
        });

        it('should not set a hidden client_id value if not Cordova', () => {
            $window.CORDOVA = false;
            render();
            expect(scope.form.client_id).toBeUndefined();
        });
    });

    describe('showCaptcha', () => {
        let $window;
        let origRunningInTestMode;

        beforeEach(() => {
            $window = $injector.get('$window');
            origRunningInTestMode = $window.RUNNING_IN_TEST_MODE;
        });

        afterEach(() => {
            $window.CORDOVA = false;
            delete $window.plugins;
            $window.RUNNING_IN_TEST_MODE = origRunningInTestMode;
        });

        it('should be false in cordova', () => {
            $window.CORDOVA = true;
            $window.plugins = {
                socialsharing: {
                    canShareVia: angular.noop,
                },
            };
            render();
            expect(scope.showCaptcha).toBe(false);
        });

        it('should be false when $window.RUNNING_IN_TEST_MODE', () => {
            $window.RUNNING_IN_TEST_MODE = true;
            render();
            expect(scope.showCaptcha).toBe(false);
        });

        it('should be true otherwise', () => {
            // have to mock RUNNING_IN_TEST_MODE to false here;
            // the afterEach hook handles Cordova stuff for us.
            $window.RUNNING_IN_TEST_MODE = false;
            render();
            expect(scope.showCaptcha).toBe(true);
        });
    });

    it('should show sign-up message', () => {
        render({
            sign_up_message: 'authentication.sign_up_form.sign_up_for_free_using',
        });
        SpecHelper.expectElementText(elem, '[name="sign_up_message"]', 'Sign up for free using');
    });

    it('should set button text', () => {
        render({
            submit_button_text: 'authentication.sign_up_form.join',
        });
        SpecHelper.expectElementText(elem, 'button[type="submit"]', 'Create Account');
    });

    describe('form validation', () => {
        it('should disable the submit button until valid', () => {
            render();

            SpecHelper.expectElementDisabled(elem, 'button[type="submit"]');

            SpecHelper.updateTextInput(elem, 'input[name="name"]', 'Some');
            SpecHelper.expectElementDisabled(elem, 'button[type="submit"]');

            SpecHelper.updateTextInput(elem, 'input[name="email"]', 'someuser@email.com');
            SpecHelper.expectElementDisabled(elem, 'button[type="submit"]');

            SpecHelper.updateTextInput(elem, 'input[name="password"]', '        ');
            SpecHelper.expectElementDisabled(elem, 'button[type="submit"]');

            SpecHelper.updateTextInput(elem, 'input[name="password"]', '1234567');
            SpecHelper.expectElementDisabled(elem, 'button[type="submit"]');

            SpecHelper.updateTextInput(elem, 'input[name="password"]', 'password');
            SpecHelper.expectElementEnabled(elem, 'button[type="submit"]');
        });
    });

    describe('omniauth', () => {
        it('should login with Facebook', () => {
            render();
            jest.spyOn(scope, 'loginWithProvider').mockImplementation(() => {});
            SpecHelper.click(elem, '.soc-no-block-fb-btn');
            expect(scope.loginWithProvider).toHaveBeenCalledWith('facebook', 'FREEMBA');
        });

        it('should login with Google', () => {
            render();
            jest.spyOn(scope, 'loginWithProvider').mockImplementation(() => {});
            SpecHelper.click(elem, '.soc-no-block-g-btn');
            expect(scope.loginWithProvider).toHaveBeenCalledWith('google_oauth2', 'FREEMBA');
        });

        it('should login with Apple', () => {
            render();
            jest.spyOn(scope, 'loginWithProvider').mockImplementation(() => {});
            SpecHelper.click(elem, '.soc-no-block-a-btn');
            expect(scope.loginWithProvider).toHaveBeenCalledWith('apple', 'FREEMBA');
        });

        it('should hide social fields if configured to do so', () => {
            render({
                disable_oauth: true,
            });
            SpecHelper.expectNoElement(elem, '.soc-btns');
        });
    });

    describe('submitRegistration', () => {
        it('should delegate to signUpFormHelper', () => {
            render();
            fillOutForm();
            jest.spyOn(SignUpFormHelper.prototype, 'submitRegistration').mockImplementation(() => {});
            SpecHelper.submitForm(elem);

            expect(SignUpFormHelper.prototype.submitRegistration).toHaveBeenCalledWith({
                email: 'someuser@email.com',
                name: 'Name',
                password: 'password',
            });
        });
    });

    describe('submit button', () => {
        it('should be disabled when preventSubmit is true', () => {
            // signUpFormHelper is actually responsible for setting this property
            render();
            fillOutForm();
            scope.preventSubmit = false;
            scope.$apply();
            SpecHelper.expectElementEnabled(elem, 'button[type="submit"]');
            scope.preventSubmit = true;
            scope.$apply();
            SpecHelper.expectElementDisabled(elem, 'button[type="submit"]');
        });

        it('should be disabled when form is invalid', () => {
            render();
            scope.preventSubmit = false;
            scope.$apply();
            SpecHelper.expectElementDisabled(elem, 'button[type="submit"]');
            fillOutForm();
            SpecHelper.expectElementEnabled(elem, 'button[type="submit"]');
        });
    });

    it('should display error messages', () => {
        // scope.form_errors will be set by signUpFormHelper
        render();
        scope.form_errors = {
            name: 'some error',
        };
        scope.$apply();
        SpecHelper.expectElementText(elem, '.form-error.active', 'some error');
    });

    it('should show privacy-notice', () => {
        SpecHelper.stubConfig({
            gdpr_user: 'true',
        });
        render();
        SpecHelper.expectElement(elem, '.privacy-notice');
    });

    function fillOutForm() {
        SpecHelper.updateTextInput(elem, 'input[name="name"]', 'Name');
        expect(scope.form.name).toEqual('Name');

        SpecHelper.updateTextInput(elem, 'input[name="email"]', 'someuser@email.com');
        expect(scope.form.email).toEqual('someuser@email.com');

        SpecHelper.updateTextInput(elem, 'input[name="password"]', 'password');
        expect(scope.form.password).toEqual('password');
    }
});
