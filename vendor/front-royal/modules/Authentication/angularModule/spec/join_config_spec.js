import 'AngularSpecHelper';
import 'Authentication/angularModule';

describe('Authentication::JoinConfig', () => {
    let SpecHelper;
    let JoinConfig;
    let ConfigFactory;
    let $timeout;
    let ErrorLogService;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Authentication', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            JoinConfig = $injector.get('JoinConfig');
            ConfigFactory = $injector.get('ConfigFactory');
            $timeout = $injector.get('$timeout');
            ErrorLogService = $injector.get('ErrorLogService');
        });

        SpecHelper.stubConfig({
            join_config: {
                default: {
                    signup_code: 'DEFAULT',
                },
                acme: {
                    signup_code: 'ACME',
                },
            },
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    it('should cache the result of the first fetch', () => {
        jest.spyOn(ConfigFactory, 'getConfig');
        JoinConfig.getConfig();
        expect(ConfigFactory.getConfig.mock.calls.length).toBe(1);
        JoinConfig.getConfig();
        expect(ConfigFactory.getConfig.mock.calls.length).toBe(1);
    });

    it('should return the entry for a urlPrefix', () => {
        let config;
        JoinConfig.getConfig('acme').then(_config => {
            config = _config;
        });
        $timeout.flush();
        expect(config.signup_code).toEqual('ACME');
    });

    it('should return the default entry if there is no urlPrefix', () => {
        let config;
        JoinConfig.getConfig().then(_config => {
            config = _config;
        });
        $timeout.flush();
        expect(config.signup_code).toEqual('DEFAULT');
    });

    it('should return the entry for a currentUser if there is one', () => {
        const currentUser = SpecHelper.stubCurrentUser();
        currentUser.sign_up_code = 'ACME';
        let config;
        JoinConfig.getConfig().then(_config => {
            config = _config;
        });
        $timeout.flush();
        expect(config.signup_code).toEqual('ACME');
    });

    it('should log an error and return the default entry if an invalud urlPrefix is passed in', () => {
        let config;
        jest.spyOn(ErrorLogService, 'notify').mockImplementation(() => {});
        JoinConfig.getConfig('noGood').then(_config => {
            config = _config;
        });
        $timeout.flush();
        expect(ErrorLogService.notify).toHaveBeenCalled();
        expect(config.signup_code).toEqual('DEFAULT');
    });
});
