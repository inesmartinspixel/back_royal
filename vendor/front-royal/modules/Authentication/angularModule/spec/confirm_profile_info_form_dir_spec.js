import 'AngularSpecHelper';
import 'Authentication/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import confirmProfileInfoFormLocales from 'Authentication/locales/authentication/confirm_profile_info_form-en.json';
import signUpFormLocales from 'Authentication/locales/authentication/sign_up_form-en.json';

setSpecLocales(confirmProfileInfoFormLocales, signUpFormLocales);

describe('Auth::ConfirmProfileInfoForm', () => {
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let $rootScope;
    let ClientStorage;
    let $auth;
    let ValidationResponder;
    let $q;
    let currentUser;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Authentication', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            $auth = $injector.get('$auth');
            ClientStorage = $injector.get('ClientStorage');
            ValidationResponder = $injector.get('ValidationResponder');
            $rootScope = $injector.get('$rootScope');
            $q = $injector.get('$q');

            currentUser = SpecHelper.stubCurrentUser();
            currentUser.confirmed_profile_info = false;
            SpecHelper.stubConfig();
            SpecHelper.mockCapabilities();
        });
    });

    it('should show expected form fields', () => {
        $rootScope.currentUser.email = 'email@example.com';
        $rootScope.currentUser.name = 'Ori Ratner';
        render();

        const inputs = elem.find('input');
        SpecHelper.expectElementHasClass(inputs, 'form-control');

        expect(inputs.eq(0).attr('name')).toEqual('name');
        expect(inputs.eq(0).attr('type')).toEqual('text');
        expect(inputs.eq(0).attr('required')).toEqual('required');
        expect(inputs.eq(0).attr('placeholder')).toEqual('Name');
        expect(inputs.eq(0).val()).toEqual($rootScope.currentUser.name);

        expect(inputs.eq(1).attr('name')).toEqual('email');
        expect(inputs.eq(1).attr('type')).toEqual('email');
        expect(inputs.eq(1).attr('required')).toEqual('required');
        expect(inputs.eq(1).attr('placeholder')).toEqual('Email');
        expect(inputs.eq(1).val()).toEqual($rootScope.currentUser.email);
    });

    it('should update the user and scope on successful submit and forward', () => {
        const deferred = $q.defer();
        jest.spyOn($auth, 'updateAccount').mockReturnValue(deferred.promise);
        jest.spyOn(ValidationResponder, 'identifyCurrentUser').mockImplementation(() => {});
        jest.spyOn(ClientStorage, 'removeItem').mockImplementation(() => {});
        jest.spyOn(ValidationResponder, 'forwardToTargetUrl').mockImplementation(() => {});

        render();

        SpecHelper.updateTextInput(elem, 'input[name="name"]', 'some.user');
        expect(scope.form.name).toEqual('some.user');
        SpecHelper.updateTextInput(elem, 'input[name="email"]', 'some.user@email.com');
        expect(scope.form.email).toEqual('some.user@email.com');

        // SpecHelper.click(elem, 'button.confirm-profile-submit');
        SpecHelper.submitForm(elem);
        expect(scope.form.confirmed_profile_info).toEqual(true);
        deferred.resolve();
        scope.$digest();

        expect($rootScope.currentUser.email).toEqual('some.user@email.com');
        expect($rootScope.currentUser.name).toEqual('some.user');
        expect($rootScope.currentUser.confirmed_profile_info).toEqual(true);
        expect(ValidationResponder.identifyCurrentUser).toHaveBeenCalled();
        expect(ClientStorage.removeItem).toHaveBeenCalledWith('last_visited_route');
        expect(ValidationResponder.forwardToTargetUrl).toHaveBeenCalled();
    });

    it('should show an error if save fails', () => {
        const deferred = $q.defer();
        jest.spyOn($auth, 'updateAccount').mockReturnValue(deferred.promise);
        jest.spyOn(ValidationResponder, 'identifyCurrentUser').mockImplementation(() => {});

        render();

        SpecHelper.updateTextInput(elem, 'input[name="name"]', 'some.user');
        expect(scope.form.name).toEqual('some.user');
        SpecHelper.updateTextInput(elem, 'input[name="email"]', 'some.user@email.com');
        expect(scope.form.email).toEqual('some.user@email.com');

        // SpecHelper.click(elem, 'button.confirm-profile-submit');
        SpecHelper.submitForm(elem);
        deferred.reject();
        scope.$digest();

        expect(scope.form.confirmed_profile_info).toEqual(false);
        expect($rootScope.currentUser.confirmed_profile_info).toEqual(false);
        expect(ValidationResponder.identifyCurrentUser).not.toHaveBeenCalled();
        expect(scope.form_errors.general).toEqual('A server error occurred. Quantic engineers have been notified');
    });

    describe('oauth registration', () => {
        beforeEach(() => {
            $rootScope.currentUser.provider = 'google_oauth2';
            $rootScope.currentUser.confirmed_profile_info = false;
        });

        it('should show the correct provider info for Google', () => {
            render();
            SpecHelper.expectElementText(elem, '.form-container-inner-step2 > h2', "You're signing up using Google");
        });

        it('should show the correct provider info for Facebook', () => {
            $rootScope.currentUser.provider = 'facebook';
            render();
            SpecHelper.expectElementText(elem, '.form-container-inner-step2 > h2', "You're signing up using Facebook");
        });

        it('should show the correct provider info for Apple', () => {
            $rootScope.currentUser.provider = 'apple';
            render();
            SpecHelper.expectElementText(elem, '.form-container-inner-step2 > h2', "You're signing up using Apple");
        });

        it('should not show provider header for email provider', () => {
            $rootScope.currentUser.provider = 'email';
            render();
            SpecHelper.expectNoElement(elem, '.form-container-inner-step2 > h2');
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.render('<confirm-profile-info-form></confirm-profile-info-form>');
        elem = renderer.elem;
        scope = elem.isolateScope();
    }
});
