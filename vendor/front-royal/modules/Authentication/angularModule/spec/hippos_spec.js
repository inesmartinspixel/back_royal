import 'AngularSpecHelper';
import 'Authentication/angularModule';

describe('Authentication::Hippos', () => {
    let SpecHelper;
    let Hippos;
    let creds;
    let $auth;
    let $q;
    let $timeout;
    let HttpQueue;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Authentication', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            Hippos = $injector.get('Hippos');
            $auth = $injector.get('$auth');
            $q = $injector.get('$q');
            $timeout = $injector.get('$timeout');
            HttpQueue = $injector.get('HttpQueue');

            creds = {
                email: 'user@example.com',
                password: '12345',
            };
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    it('should login using saved credentials', () => {
        const promise = $q(() => {});
        jest.spyOn($auth, 'submitLogin').mockReturnValue(promise);
        Hippos.save(creds);
        Hippos.go({});
        expect($auth.submitLogin).toHaveBeenCalledWith(creds);
    });

    it('should immediately reject if there are no saved credentials', () => {
        const callback = jest.fn();
        Hippos.go({}).catch(callback);
        $timeout.flush();
        expect(callback).toHaveBeenCalledWith({
            failureType: 'login',
            response: {},
        });
    });

    it('should reject if the login fails', () => {
        const response = {
            reason: 'unauthorized',
            errors: ['Invalid credentials'],
        };
        $auth.submitLogin = jest.fn();
        $auth.submitLogin.mockImplementation(() =>
            $q((respond, reject) => {
                reject(response);
            }),
        );
        const callback = jest.fn();
        Hippos.save(creds);
        Hippos.go({}).catch(callback);
        $timeout.flush();
        expect($auth.submitLogin).toHaveBeenCalled();
        expect(callback).toHaveBeenCalledWith({
            failureType: 'login',
            response,
        });
    });

    it('should retry supplied response if the login succeeds', () => {
        $auth.submitLogin = jest.fn();
        $auth.submitLogin.mockImplementation(() => $q.when());
        const failedResponse = {
            config: {
                some: 'config',
            },
        };

        HttpQueue.prototype.retry = jest.fn();
        HttpQueue.prototype.retry.mockImplementation(() =>
            $q(respond => {
                respond();
            }),
        );
        const callback = jest.fn();
        Hippos.save(creds);
        Hippos.go(failedResponse).then(callback);
        $timeout.flush();
        expect($auth.submitLogin).toHaveBeenCalled();
        expect(callback).toHaveBeenCalled();
        expect(HttpQueue.prototype.retry).toHaveBeenCalledWith(failedResponse.config);
    });
});
