import 'AngularSpecHelper';
import 'Authentication/angularModule';
import confirmProfileInfoFormLocales from 'Authentication/locales/authentication/confirm_profile_info_form-en.json';
import loginRegisterFooterLocales from 'Authentication/locales/authentication/login_register_footer-en.json';
import signUpFormLocales from 'Authentication/locales/authentication/sign_up_form-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(confirmProfileInfoFormLocales, loginRegisterFooterLocales, signUpFormLocales);

describe('Authentication::CompleteRegistration', () => {
    let renderer;
    let elem;
    let SpecHelper;
    let SignOutHelper;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Authentication', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            SignOutHelper = $injector.get('SignOutHelper');
        });

        SpecHelper.stubConfig();
        SpecHelper.stubCurrentUser('learner');
    });

    // This is implemented in the login-register-footer, and should really
    // be test there
    describe('signOut', () => {
        it('should work', () => {
            render();
            jest.spyOn(SignOutHelper, 'signOut').mockImplementation(() => {});
            SpecHelper.click(elem, '[name="sign-out"]');
            expect(SignOutHelper.signOut).toHaveBeenCalled();
        });
    });

    describe('support link', () => {
        it('should work', () => {
            render();
            const loginRegisterFooterScope = elem.find('login-register-footer').isolateScope();
            jest.spyOn(loginRegisterFooterScope, 'loadUrl').mockImplementation(() => {});
            SpecHelper.click(elem, '[name="support"]');
            expect(loginRegisterFooterScope.loadUrl).toHaveBeenCalledWith('/help', '_blank');
        });
    });

    it('should have confirm profile info form', () => {
        render();
        SpecHelper.expectElement(elem, 'confirm-profile-info-form');
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.render('<complete-registration></complete-registration>');
        elem = renderer.elem;
    }
});
