import moment from 'moment-timezone';
import 'AngularSpecHelper';
import 'Authentication/angularModule';
import 'Users/angularModule/spec/_mock/fixtures/users';
import authFormHelperMixinLocales from 'Authentication/locales/authentication/auth_form_helper_mixin-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(authFormHelperMixinLocales);

describe('Authentication::AuthFormHelperMixin', () => {
    let $injector;
    let SpecHelper;
    let AuthFormHelperMixin;
    let $rootScope;
    let scope;
    let omniauth;
    let $location;
    let blockAuthenticatedAccess;
    let DialogModal;
    let $window;
    let EventLogger;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Authentication', 'SpecHelper');

        angular.mock.module($provide => {
            blockAuthenticatedAccess = {
                block: false,
                errorMessage: 'blocked, yo',
            };
            $provide.value('blockAuthenticatedAccess', blockAuthenticatedAccess);
        });

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            SpecHelper = $injector.get('SpecHelper');
            $rootScope = $injector.get('$rootScope');
            AuthFormHelperMixin = $injector.get('AuthFormHelperMixin');
            omniauth = $injector.get('omniauth');
            $location = $injector.get('$location');
            DialogModal = $injector.get('DialogModal');
            $window = $injector.get('$window');
            EventLogger = $injector.get('EventLogger');
            delete $window.preSignupForm;
        });

        SpecHelper.stubConfig();

        scope = $rootScope.$new();

        window.CORDOVA = true;

        AuthFormHelperMixin.onLink(scope);

        SpecHelper.stubEventLogging();
    });

    afterEach(() => {
        SpecHelper.cleanup();
        window.plugins = undefined;
        window.CORDOVA = undefined;
        delete $window.preSignupForm;
    });

    describe('query param errors', () => {
        // this happens when there are errors in oauth login
        it('should show errors that are in the query params', () => {
            jest.spyOn($location, 'search').mockReturnValue({
                error: 'Some Error',
            });
            AuthFormHelperMixin.onLink(scope);
            expect(scope.form_errors).toEqual({
                general: 'Some Error',
            });
        });

        it("should have special handling for 'error_account_deactivated'", () => {
            jest.spyOn($location, 'search').mockReturnValue({
                error: 'error_account_deactivated',
            });
            jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
            AuthFormHelperMixin.onLink(scope);
            expect(DialogModal.alert).toHaveBeenCalledWith({
                content:
                    'Your account has been deactivated. Contact <a href="mailto:support@quantic.edu">support@quantic.edu</a> if you wish to reactivate it.',
                classes: ['server-error-modal'],
                title: 'Server Error',
            });
        });

        it("should have special handling for 'error_hiring_web_only'", () => {
            jest.spyOn($location, 'search').mockReturnValue({
                error: 'error_hiring_web_only',
            });
            jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
            AuthFormHelperMixin.onLink(scope);
            expect(DialogModal.alert).toHaveBeenCalledWith({
                content:
                    "It looks like you're trying to sign in with a Smartly Talent employer account. Please sign in to Smartly Talent on a desktop device.",
                classes: ['server-error-modal'],
                title: 'Server Error',
            });
        });
    });

    describe('errorListener', () => {
        it('show add to form_errors if object', () => {
            const response = {
                errors: ['an_error'],
            };

            AuthFormHelperMixin.onLink(scope);

            $rootScope.$broadcast('auth:login-error', response);
            expect(scope.form_errors.general).toEqual(response.errors[0]);
        });

        it('should show DailogModal if not an object', () => {
            const response = 'not_an_object';
            jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
            $rootScope.$broadcast('auth:login-error', response);
            expect(DialogModal.alert).toHaveBeenCalledWith({
                content: 'not_an_object',
                classes: ['server-error-modal'],
                title: 'Server Error',
            });
        });

        it("should have special handling for 'error_account_deactivated'", () => {
            const response = 'error_account_deactivated';
            jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
            $rootScope.$broadcast('auth:login-error', response);
            expect(DialogModal.alert).toHaveBeenCalledWith({
                content:
                    'Your account has been deactivated. Contact <a href="mailto:support@quantic.edu">support@quantic.edu</a> if you wish to reactivate it.',
                classes: ['server-error-modal'],
                title: 'Server Error',
            });
        });

        it("should have special handling for 'error_hiring_web_only'", () => {
            const response = 'error_hiring_web_only';
            jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
            $rootScope.$broadcast('auth:login-error', response);
            expect(DialogModal.alert).toHaveBeenCalledWith({
                content:
                    "It looks like you're trying to sign in with a Smartly Talent employer account. Please sign in to Smartly Talent on a desktop device.",
                classes: ['server-error-modal'],
                title: 'Server Error',
            });
        });
    });

    describe('with blockAuthenticatedAccess', () => {
        it('should prevent submit and show a message', () => {
            blockAuthenticatedAccess.block = true;
            AuthFormHelperMixin.onLink(scope);
            expect(scope.form_errors).toEqual({
                general: 'blocked, yo',
            });
        });
    });

    describe('loginWithProvider', () => {
        let ClientStorage;
        let $timeout;

        beforeEach(() => {
            jest.spyOn(omniauth, 'loginWithProvider').mockImplementation(() => {});
            ClientStorage = $injector.get('ClientStorage');
            $timeout = $injector.get('$timeout');
        });

        it('should exist', () => {
            expect(scope.loginWithProvider).toEqual(expect.any(Function));
        });

        it('should log login_with_provider and save event buffer', () => {
            jest.spyOn(EventLogger.prototype, 'log').mockImplementation(() => {});
            jest.spyOn(EventLogger.prototype, 'tryToSaveBuffer').mockImplementation(() => {});
            scope.loginWithProvider('someProvider', 'signUpCode');
            expect(EventLogger.prototype.log).toHaveBeenCalledWith('auth:login_with_provider', {
                provider: 'someProvider',
            });
            expect(EventLogger.prototype.tryToSaveBuffer).toHaveBeenCalled();
        });

        it('should toggle preventSubmit while logging in', () => {
            const deferred = $injector.get('$q').defer();
            jest.spyOn(scope, 'getProviderPromise').mockReturnValue(deferred.promise);

            expect(scope.preventSubmit).toBe(false);

            scope.loginWithProvider('someProvider');
            expect(scope.preventSubmit).toBe(true);

            $timeout.flush();
            deferred.resolve();
            scope.$digest();

            expect(scope.preventSubmit).toBe(false);
        });

        it('should notify if no sign_up_code passed in (desktop)', () => {
            window.CORDOVA = undefined;
            const ErrorLogService = $injector.get('ErrorLogService');
            jest.spyOn(ErrorLogService, 'notify').mockImplementation(() => {});
            scope.loginWithProvider('someProvider');
            expect(ErrorLogService.notify).toHaveBeenCalledWith('loginWithProvider called with no signUpCode');
            $timeout.flush();
            expect(omniauth.loginWithProvider).toHaveBeenCalledWith('someProvider', undefined, undefined);
        });

        it('should NOT notify if no sign_up_code passed in (Cordova)', () => {
            window.CORDOVA = true;
            const ErrorLogService = $injector.get('ErrorLogService');
            jest.spyOn(ErrorLogService, 'notify').mockImplementation(() => {});
            scope.loginWithProvider('someProvider');
            $timeout.flush();
            expect(ErrorLogService.notify).not.toHaveBeenCalledWith('loginWithProvider called with no signUpCode');
            expect(omniauth.loginWithProvider).toHaveBeenCalledWith('someProvider', undefined, undefined);
        });

        it('should pass along sign_up_code and programType', () => {
            const ErrorLogService = $injector.get('ErrorLogService');
            jest.spyOn(ErrorLogService, 'notify').mockImplementation(() => {});
            scope.loginWithProvider('someProvider', 'signUpCode', 'programType');
            $timeout.flush();
            expect(ErrorLogService.notify).not.toHaveBeenCalled();
            expect(omniauth.loginWithProvider).toHaveBeenCalledWith('someProvider', 'signUpCode', 'programType');
        });

        it('should persist a oauth_redirect_target value in ClientStorage if not Cordova', () => {
            window.CORDOVA = true;
            expect(ClientStorage.getItem('oauth_redirect_target')).toBeUndefined();
            jest.spyOn($location, 'search').mockReturnValue({
                target: '/some_url',
            });

            scope.loginWithProvider('someProvider', 'signUpCode');
            $timeout.flush();
            expect(ClientStorage.getItem('oauth_redirect_target')).toBeUndefined();

            window.CORDOVA = undefined;
            scope.loginWithProvider('someProvider', 'signUpCode');
            expect(ClientStorage.getItem('oauth_redirect_target')).toEqual('/some_url');
        });

        it('should persist a oauth_redirect_target value that is saved on the scope', () => {
            window.CORDOVA = undefined;
            scope.oauthRedirectTarget = 'http://some/absolute/path';
            scope.loginWithProvider('someProvider', 'signUpCode');
            expect(ClientStorage.getItem('oauth_redirect_target')).toEqual('http://some/absolute/path');
        });

        it('should not persist a oauth_redirect_target if there is no redirect target', () => {
            window.CORDOVA = true;
            expect(ClientStorage.getItem('oauth_redirect_target')).toBeUndefined();
            jest.spyOn($location, 'search').mockReturnValue({}); // no target

            scope.loginWithProvider('someProvider', 'signUpCode');
            expect(ClientStorage.getItem('oauth_redirect_target')).toBeUndefined();

            window.CORDOVA = undefined;
            scope.loginWithProvider('someProvider', 'signUpCode');
            expect(ClientStorage.getItem('oauth_redirect_target')).toBeUndefined();
        });

        it('should log preSignupForm if it exists', () => {
            jest.spyOn(EventLogger.prototype, 'log').mockImplementation(() => {});
            const preSignupValues = $injector.get('preSignupValues');
            preSignupValues.DYNAMIC_LANDING_PAGE_MULTI_STEP_FORM_KEYS_TO_VALUES_MAP = {
                foo: {
                    bar: { text: 'Bar' },
                },
            };
            $window.preSignupForm = {
                foo: 'bar',
            };
            scope.loginWithProvider('someProvider', 'signUpCode');
            expect(EventLogger.prototype.log).toHaveBeenCalledWith(
                'registration:completed_pre_signup_form',
                {
                    label: 'registration:completed_pre_signup_form',
                    foo: 'Bar',
                },
                {
                    segmentioType: 'registration:completed_pre_signup_form',
                    segmentioLabel: 'registration:completed_pre_signup_form',
                },
            );
        });

        it('should NOT log preSignupForm if it does not exist', () => {
            const loggedEvents = [];
            jest.spyOn(EventLogger.prototype, 'log').mockImplementation(eventType => {
                loggedEvents.push(eventType);
            });
            delete $window.preSignupForm;
            scope.loginWithProvider('someProvider', 'signUpCode');
            $timeout.flush();
            expect(loggedEvents.includes('registration:completed_pre_signup_form')).toBe(false);
        });

        describe('custom Facebook support', () => {
            describe('browser flow', () => {
                it('should call through to omniauth ', () => {
                    scope.loginWithProvider('facebook', 'signUpCode', 'programType');
                    $timeout.flush();
                    expect(omniauth.loginWithProvider).toHaveBeenCalledWith('facebook', 'signUpCode', 'programType');
                });
            });

            describe('cordova flow', () => {
                beforeEach(() => {
                    window.CORDOVA = true;
                    window.facebookConnectPlugin = {
                        login: jest.fn(),
                        getAccessToken: jest.fn(),
                    };
                });

                afterEach(() => {
                    window.CORDOVA = undefined;
                    window.facebookConnectPlugin = undefined;
                });

                it('should call through to native plugin', () => {
                    scope.loginWithProvider('facebook');
                    $timeout.flush();

                    // initial login call
                    expect(window.facebookConnectPlugin.login).toHaveBeenCalled();
                    const loginArgs = window.facebookConnectPlugin.login.mock.calls[0];
                    expect(loginArgs[0]).toEqual(['public_profile', 'email']);

                    // login callback -> getAccessToken
                    const loginSuccess = loginArgs[1];
                    expect(window.facebookConnectPlugin.getAccessToken).not.toHaveBeenCalled();
                    loginSuccess();
                    expect(window.facebookConnectPlugin.getAccessToken).toHaveBeenCalled();

                    // getAccessToken callback -> backend callback
                    $injector.get('UserFixtures');
                    const $httpBackend = $injector.get('$httpBackend');
                    const $q = $injector.get('$q');
                    const $auth = $injector.get('$auth');
                    const User = $injector.get('User');
                    const userResponse = User.fixtures.getLearner();
                    jest.spyOn(moment.tz, 'guess').mockReturnValue('America/Chicago');
                    $httpBackend
                        .expectGET(
                            `${$window.ENDPOINT_ROOT}/native_oauth/facebook/callback.json?timezone=America%2FChicago`,
                        )
                        .respond(200, {
                            data: userResponse,
                        });
                    const tokenArgs = window.facebookConnectPlugin.getAccessToken.mock.calls[0];
                    const tokenSuccess = tokenArgs[0];
                    const ngTokenAuthDeferred = $q.defer();

                    jest.spyOn($auth, 'handleValidAuth').mockReturnValue(ngTokenAuthDeferred.promise);
                    tokenSuccess();
                    $httpBackend.flush();
                    expect($auth.handleValidAuth).toHaveBeenCalledWith(userResponse, true);
                });
            });
        });

        describe('custom Google support', () => {
            describe('browser flow', () => {
                it('should call through to omniauth ', () => {
                    scope.loginWithProvider('google_oauth2', 'signUpCode', 'programType');
                    $timeout.flush();
                    expect(omniauth.loginWithProvider).toHaveBeenCalledWith(
                        'google_oauth2',
                        'signUpCode',
                        'programType',
                    );
                });
            });

            describe('cordova flow', () => {
                let loginDeferred;
                let $q;

                beforeEach(() => {
                    $q = $injector.get('$q');
                    loginDeferred = $q.defer();

                    window.CORDOVA = true;
                    window.plugins = {
                        googleplus: {
                            login() {},
                        },
                    };

                    jest.spyOn(window.plugins.googleplus, 'login').mockReturnValue(loginDeferred.promise);
                });

                afterEach(() => {
                    window.CORDOVA = undefined;
                    window.plugins = undefined;
                });

                it('should call through to native plugin', () => {
                    jest.spyOn(EventLogger.prototype, 'log').mockImplementation(() => {});
                    const ConfigFactory = $injector.get('ConfigFactory');
                    jest.spyOn(ConfigFactory, 'getSync').mockReturnValue({
                        google_oauth_id: 'some_google_id',
                    });

                    scope.loginWithProvider('google_oauth2');
                    $timeout.flush();

                    // initial login call
                    expect(window.plugins.googleplus.login).toHaveBeenCalled();
                    const loginArgs = window.plugins.googleplus.login.mock.calls[0];
                    expect(loginArgs[0]).toEqual({
                        webClientId: 'some_google_id',
                    });

                    // login callback -> backend callback
                    $injector.get('UserFixtures');
                    const $httpBackend = $injector.get('$httpBackend');
                    const $auth = $injector.get('$auth');
                    const User = $injector.get('User');
                    const userResponse = User.fixtures.getLearner();
                    jest.spyOn(moment.tz, 'guess').mockReturnValue('America/Chicago');
                    $httpBackend
                        .expectGET(
                            `${$window.ENDPOINT_ROOT}/native_oauth/google_oauth2/callback.json?timezone=America%2FChicago`,
                        )
                        .respond(200, {
                            data: userResponse,
                        });

                    const loginSuccess = loginArgs[1];
                    const ngTokenAuthDeferred = $q.defer();

                    jest.spyOn($auth, 'handleValidAuth').mockReturnValue(ngTokenAuthDeferred.promise);
                    loginSuccess();
                    $httpBackend.flush();
                    expect($auth.handleValidAuth).toHaveBeenCalledWith(userResponse, true);
                });
            });
        });

        describe('submitLogin', () => {
            afterEach(() => {
                $window.CORDOVA = null;
            });

            it('should set client to miyaMiya on miyaMiya client', () => {
                $window.CORDOVA = { miyaMiya: true };
                const $auth = $injector.get('$auth');
                AuthFormHelperMixin.onLink(scope);
                scope.urlConfig = { provider: 'provider' };
                jest.spyOn($auth, 'submitLogin');
                scope.submitLogin({ email: 'me@example.com' });
                expect($auth.submitLogin).toHaveBeenCalledWith({
                    email: 'me@example.com',
                    provider: 'provider',
                    client: 'miya_miya',
                });
            });

            it('should set provider if not set', () => {
                const $auth = $injector.get('$auth');
                AuthFormHelperMixin.onLink(scope);
                scope.urlConfig = { provider: 'provider' };
                jest.spyOn($auth, 'submitLogin');
                scope.submitLogin({ email: 'me@example.com' });
                expect($auth.submitLogin).toHaveBeenCalledWith({ email: 'me@example.com', provider: 'provider' });
            });

            it('should not set provider if passed in', () => {
                const $auth = $injector.get('$auth');
                AuthFormHelperMixin.onLink(scope);
                scope.urlConfig = { provider: 'provider' };
                jest.spyOn($auth, 'submitLogin');
                scope.submitLogin({ email: 'me@example.com', provider: 'passedIn' });
                expect($auth.submitLogin).toHaveBeenCalledWith({ email: 'me@example.com', provider: 'passedIn' });
            });
        });
    });
});
