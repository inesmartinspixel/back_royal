import 'AngularSpecHelper';
import 'Authentication/angularModule';

describe('Authentication::SignOutHelper', () => {
    let SpecHelper;
    let SignOutHelper;
    let $auth;
    let EventLogger;
    let $q;
    let $timeout;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Authentication', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            SignOutHelper = $injector.get('SignOutHelper');
            $auth = $injector.get('$auth');
            EventLogger = $injector.get('EventLogger');
            $q = $injector.get('$q');
            $timeout = $injector.get('$timeout');
        });

        SpecHelper.stubConfig();
    });

    afterEach(() => {
        SpecHelper.cleanup();
        window.plugins = undefined;
        window.CORDOVA = undefined;
    });

    describe('signOut', () => {
        it('should log an event', () => {
            jest.spyOn(EventLogger.prototype, 'log').mockImplementation(() => {});
            SignOutHelper.signOut();
            expect(EventLogger.prototype.log).toHaveBeenCalledWith('user:logout');
        });

        it('should disconnect auth plugins on Cordova', () => {
            window.CORDOVA = true;
            window.facebookConnectPlugin = {
                logout: jest.fn(),
            };
            window.plugins = {
                googleplus: {
                    disconnect: jest.fn(),
                },
            };
            SignOutHelper.signOut();
            expect(window.facebookConnectPlugin.logout).toHaveBeenCalled();
            expect(window.plugins.googleplus.disconnect).toHaveBeenCalled();
        });

        it('should sign out the currentUser', () => {
            SpecHelper.stubEventLogging();
            jest.spyOn(SignOutHelper, 'beforeUnsettingCurrentUser').mockReturnValue($q.when());
            jest.spyOn($auth, 'signOut').mockImplementation(() => {});
            SignOutHelper.signOut();
            $timeout.flush();
            expect($auth.signOut).toHaveBeenCalled();
        });
    });
});
