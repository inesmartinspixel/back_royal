import 'AngularSpecHelper';
import 'Authentication/angularModule';

import * as userAgentHelper from 'userAgentHelper';
import { QUANTIC_DOMAIN, SMARTLY_DOMAIN } from 'AppBrandMixin';
import NetworkConnection from 'NetworkConnection';
import Auid from 'Auid';

describe('Authentication::ValidationResponder', () => {
    let SpecHelper;
    let $location;
    let EventLogger;
    let $rootScope;
    let $timeout;
    let $q;
    let ValidationResponder;
    let ClientStorage;
    let segmentio;
    let $window;
    let $injector;
    let remoteNotificationHelper;
    let User;
    let config;
    let NavigationHelperMixin;
    let offlineModeManager;
    let canEnterOfflineMode;
    let storedUser;

    beforeEach(() => {
        angular.mock.module(
            'FrontRoyal.Navigation',
            'FrontRoyal.Authentication',
            'SpecHelper',
            'NoUnhandledRejectionExceptions',
        );

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            SpecHelper = $injector.get('SpecHelper');
            ValidationResponder = $injector.get('ValidationResponder');
            $location = $injector.get('$location');
            EventLogger = $injector.get('EventLogger');
            $rootScope = $injector.get('$rootScope');
            $timeout = $injector.get('$timeout');
            $q = $injector.get('$q');
            ClientStorage = $injector.get('ClientStorage');
            segmentio = $injector.get('segmentio');
            $window = $injector.get('$window');
            remoteNotificationHelper = $injector.get('remoteNotificationHelper');
            User = $injector.get('User');
            NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
        });

        $rootScope.currentUser = undefined;

        SpecHelper.stubConfig();
        config = $injector.get('ConfigFactory').getSync();

        offlineModeManager = $injector.get('offlineModeManager');
        canEnterOfflineMode = false;
        jest.spyOn(offlineModeManager, 'canEnterOfflineMode').mockImplementation(() => $q.when(canEnterOfflineMode));

        const frontRoyalStore = $injector.get('frontRoyalStore');
        storedUser = undefined;
        jest.spyOn(frontRoyalStore, 'getCurrentUser').mockImplementation(() => $q.when(storedUser));
    });

    afterEach(() => {
        SpecHelper.cleanup();

        // Some of the tests below login users with a different pref_locale.
        // If we don't set back the preferredCode then assertions on localized
        // text will break only when all the tests are run together
        const Locale = $injector.get('Locale');
        Locale.preferredCode = 'en';
    });

    describe('initialize', () => {
        let blockAuthenticatedAccess;
        let blockAuthenticatedAccessValue;
        let $auth;

        beforeEach(() => {
            blockAuthenticatedAccess = $injector.get('blockAuthenticatedAccess');
            $auth = $injector.get('$auth');
            blockAuthenticatedAccessValue = false;
            jest.spyOn(blockAuthenticatedAccess, 'block', 'get').mockImplementation(
                () => blockAuthenticatedAccessValue,
            );
        });

        afterEach(() => {
            delete $window.CORDOVA;
        });

        it('should not attempt to validate if blockAuthenticatedAccess.block is set', () => {
            blockAuthenticatedAccessValue = true;
            ValidationResponder.initialize();
            expect(offlineModeManager.canEnterOfflineMode).not.toHaveBeenCalled();
        });

        it('should attempt to validate if blockAuthenticatedAccess.block is not set', () => {
            ValidationResponder.initialize();
            expect(offlineModeManager.canEnterOfflineMode).toHaveBeenCalled();
        });

        it('should call handleValidationFailure if in a Cordova context without a connection', () => {
            jest.spyOn(NetworkConnection, 'online', 'get').mockReturnValue(false);
            $window.CORDOVA = {};
            jest.spyOn($auth, 'validateUser').mockReturnValue($q.when());
            ValidationResponder.initialize();
            $timeout.flush();
            expect($auth.validateUser).not.toHaveBeenCalled();
            expect($rootScope.authCallComplete).toBe(true);
            expect($rootScope.currentUser).toBeUndefined();
        });

        it('should redirect to home if initiallyDisconnected', () => {
            SpecHelper.stubEventLogging();

            const response = {
                reason: 'unauthorized',
                errors: ['Invalid credentials'],
            };

            jest.spyOn($auth, 'validateUser').mockReturnValue($q.reject(response));
            jest.spyOn(offlineModeManager, 'attemptOfflineAuthentication').mockReturnValue($q.resolve(null));
            jest.spyOn($rootScope, 'goHome').mockImplementation(() => {});

            ValidationResponder.initialize(true); // initiallyDisconnected
            $timeout.flush();

            expect($auth.validateUser).toHaveBeenCalled();
            expect($rootScope.goHome).toHaveBeenCalled();
        });

        describe('FrontRoyalStore integration', () => {
            it('should load user from store if validation request fails and canEnterOfflineMode', () => {
                SpecHelper.stubEventLogging();
                const user = { id: 'userId' };
                jest.spyOn(offlineModeManager, 'attemptOfflineAuthentication').mockReturnValue($q.when(user));
                jest.spyOn($auth, 'validateUser').mockImplementation(() => $q.reject({ reason: 'unauthorized' }));
                jest.spyOn($rootScope, 'goHome');
                ValidationResponder.initialize();
                $timeout.flush();
                expect($rootScope.currentUser?.id).toEqual(user.id);
                expect($rootScope.authCallComplete).toEqual(true);

                // forwardToTargetUrlIfExists is called with skipFallbackToDashboard, so goHome
                // is not called.
                expect($rootScope.goHome).not.toHaveBeenCalled();
            });
        });
    });

    describe('identifyCurrentUser', () => {
        beforeEach(() => {
            SpecHelper.stubCurrentUser();
        });

        afterEach(() => {
            delete window.CORDOVA;
        });

        it('should remove personal information from the identify call if on Android in Cordova', () => {
            jest.spyOn(userAgentHelper, 'isAndroidDevice').mockReturnValue(true);
            window.CORDOVA = true;
            jest.spyOn(segmentio, 'identify').mockImplementation(() => {});
            ValidationResponder.identifyCurrentUser();
            expect(segmentio.identify).toHaveBeenCalledWith($rootScope.currentUser.id, {
                cordova: true,
                groups: 'ABC',
                institutions: 'Quantic',
                globalRole: 'learner',
                plan_id: undefined,
                sign_up_code: undefined,
            });
        });

        it('should include normal information on Android when not in Cordova', () => {
            jest.spyOn(userAgentHelper, 'isAndroidDevice').mockReturnValue(false);
            window.CORDOVA = false;
            jest.spyOn(segmentio, 'identify').mockImplementation(() => {});
            ValidationResponder.identifyCurrentUser();
            expect(segmentio.identify).toHaveBeenCalledWith($rootScope.currentUser.id, {
                cordova: false,
                email: $rootScope.currentUser.email,
                emailDomain: $rootScope.currentUser.emailDomain,
                name: $rootScope.currentUser.name,
                groups: 'ABC',
                institutions: 'Quantic',
                globalRole: 'learner',
                plan_id: undefined,
                sign_up_code: undefined,
            });
        });

        it('should include normal information on non-Android', () => {
            jest.spyOn(userAgentHelper, 'isAndroidDevice').mockReturnValue(false);
            window.CORDOVA = true;
            jest.spyOn(segmentio, 'identify').mockImplementation(() => {});
            ValidationResponder.identifyCurrentUser();
            expect(segmentio.identify).toHaveBeenCalledWith($rootScope.currentUser.id, {
                cordova: true,
                email: $rootScope.currentUser.email,
                emailDomain: $rootScope.currentUser.emailDomain,
                name: $rootScope.currentUser.name,
                groups: 'ABC',
                institutions: 'Quantic',
                globalRole: 'learner',
                plan_id: undefined,
                sign_up_code: undefined,
            });
        });
    });

    describe('handleValidationSuccess', () => {
        let userResponse;

        beforeEach(() => {
            jest.spyOn(Auid, 'set').mockImplementation(() => {});
            jest.spyOn(ValidationResponder, 'identifyCurrentUser').mockImplementation(() => {});
            jest.spyOn(EventLogger.prototype, 'log').mockImplementation(() => {});
            jest.spyOn(EventLogger.prototype, 'onUserChange').mockReturnValue($q.when());

            userResponse = {
                id: 'user_id',
                pref_locale: 'es',
            };
        });

        it('should remove oauth_login query param', () => {
            jest.spyOn($location, 'search').mockReturnValue({
                oauth_login: true,
            });
            ValidationResponder.handleValidationSuccess({}, userResponse);
            $rootScope.$digest();
            expect($location.search.mock.calls[2]).toEqual(['oauth_login', undefined]);
        });

        it('should clear out the referredById', () => {
            jest.spyOn(ClientStorage, 'getItem').mockReturnValue('some-uuid');
            jest.spyOn(ClientStorage, 'removeItem').mockImplementation(() => {});
            ValidationResponder.handleValidationSuccess({}, userResponse);
            $rootScope.$digest();
            expect(ClientStorage.removeItem).toHaveBeenCalledWith('referredById');
        });

        describe('with existing auid different from user id', () => {
            it('should log anonymous_user:login event and then set currentUser', () => {
                jest.spyOn(Auid, 'get').mockReturnValue('auid');
                ValidationResponder.handleValidationSuccess({}, userResponse);
                $rootScope.$digest();
                expect(EventLogger.prototype.log).toHaveBeenCalledWith('anonymous_user:login', {
                    logged_in_user_id: userResponse.id,
                });
                expect(EventLogger.prototype.onUserChange).toHaveBeenCalledWith('auid');
                assertCurrentUserSet();
            });
        });
        describe('with no existing auid', () => {
            it('should set currentUser', () => {
                ValidationResponder.handleValidationSuccess({}, userResponse);
                $rootScope.$digest();
                expect(EventLogger.prototype.onUserChange).not.toHaveBeenCalled();
                assertCurrentUserSet();
            });
        });
        describe('with existing auid the same as user id', () => {
            it('should set currentUser', () => {
                ValidationResponder.handleValidationSuccess({}, userResponse);
                $rootScope.$digest();
                expect(EventLogger.prototype.onUserChange).not.toHaveBeenCalled();
                assertCurrentUserSet();
            });
        });
        describe('with $window.CORDOVA', () => {
            beforeEach(() => {
                window.CORDOVA = {};
                window.facebookConnectPlugin = {
                    logEvent: jest.fn(),
                    logPurchase: jest.fn(),
                };
                jest.spyOn(remoteNotificationHelper, 'registerDeviceForRemoteNotifications').mockImplementation(
                    () => 'foobar',
                );
            });
            afterEach(() => {
                window.CORDOVA = undefined;
                window.facebookConnectPlugin = undefined;
            });
            it('should call remoteNotificationHelper.registerDeviceForRemoteNotifications if $rootScope.currentUser.pref_allow_push_notifications', () => {
                userResponse.pref_allow_push_notifications = true;
                ValidationResponder.handleValidationSuccess({}, userResponse);
                $rootScope.$digest();
                expect(remoteNotificationHelper.registerDeviceForRemoteNotifications).toHaveBeenCalled();
            });
            it('should not call remoteNotificationHelper.registerDeviceForRemoteNotifications if !$rootScope.currentUser.pref_allow_push_notifications', () => {
                userResponse.pref_allow_push_notifications = false;
                ValidationResponder.handleValidationSuccess({}, userResponse);
                $rootScope.$digest();
                expect(remoteNotificationHelper.registerDeviceForRemoteNotifications).not.toHaveBeenCalled();
            });
        });
        describe('redirectToCorrectDomain', () => {
            let shouldSeeQuanticBranding;
            const currentPath = '/currentPath';

            beforeEach(() => {
                window.CORDOVA = false;
                jest.spyOn(config, 'appEnvType').mockReturnValue('production');
                jest.spyOn(NavigationHelperMixin, 'loadUrl');
                jest.spyOn(User.prototype, 'shouldSeeQuanticBranding', 'get').mockImplementation(
                    () => shouldSeeQuanticBranding,
                );
                jest.spyOn($location, 'url').mockReturnValue(currentPath);
                jest.spyOn($location, 'port').mockReturnValue('');
            });

            it('should do nothing if cordova', () => {
                window.CORDOVA = true;
                assertNoRedirect();
            });

            it('should do nothing if on an institutional subdomain', () => {
                const institutionalSubdomain = $injector.get('institutionalSubdomain');
                jest.spyOn($location, 'host').mockReturnValue('acmeproducts.smart.ly');
                institutionalSubdomain.id = 'acmeproducts';
                assertNoRedirect();
            });

            it('should do nothing if smartly user already at smart.ly', () => {
                jest.spyOn($location, 'host').mockReturnValue(SMARTLY_DOMAIN);
                shouldSeeQuanticBranding = false;
                assertNoRedirect();
            });

            it('should do nothing if quantic user already at quantic.edu', () => {
                jest.spyOn($location, 'host').mockReturnValue(QUANTIC_DOMAIN);
                shouldSeeQuanticBranding = true;
                assertNoRedirect();
            });

            it('should forward a smartly user to smartly', () => {
                shouldSeeQuanticBranding = false;
                assertRedirect(`https://${SMARTLY_DOMAIN}${currentPath}`);
            });

            it('should forward a quantic user to quantic', () => {
                shouldSeeQuanticBranding = true;
                assertRedirect(`https://${QUANTIC_DOMAIN}${currentPath}`);
            });

            function assertNoRedirect() {
                ValidationResponder.handleValidationSuccess({}, userResponse);
                $rootScope.$digest();
                expect(NavigationHelperMixin.loadUrl).not.toHaveBeenCalled();
            }

            function assertRedirect(url) {
                ValidationResponder.handleValidationSuccess({}, userResponse);
                $rootScope.$digest();
                expect(NavigationHelperMixin.loadUrl).toHaveBeenCalledWith(url);
                expect($location.url).toHaveBeenCalledWith();
            }
        });

        describe('FrontRoyalStore', () => {
            let DialogModal;

            beforeEach(() => {
                DialogModal = $injector.get('DialogModal');
                jest.spyOn(DialogModal, 'alert');
                DialogModal.alert.who = 'me';
            });

            it('should prevent logging in as another user when there is a user in the store', () => {
                const storedUserEmail = 'storedUser@example.com';
                storedUser = { id: 'userInStore', email: storedUserEmail };
                ValidationResponder.handleValidationSuccess({}, userResponse);
                $timeout.flush();
                expect(DialogModal.alert).toHaveBeenCalled();
                const opts = DialogModal.alert.mock.calls[0][0];
                expect(opts.content).toEqual(
                    `You have stored data for ${storedUserEmail}.  Please login as that user in order to save your data to the server.`,
                );
                expect($rootScope.currentUser).toBeUndefined();
                expect($rootScope.authCallComplete).toBe(true);

                // redirect to sign-in when modal is closed
                jest.spyOn($location, 'url');
                opts.close();
                expect($location.url).toHaveBeenCalledWith('/sign-in');
            });

            it('should allow logging in as a user in the store', () => {
                storedUser = { id: userResponse.id, email: 'storedUser@example.com' };
                ValidationResponder.handleValidationSuccess({}, userResponse);
                $timeout.flush();
                expect(DialogModal.alert).not.toHaveBeenCalled();
                expect($rootScope.currentUser?.id).toEqual(userResponse.id);
                expect($rootScope.authCallComplete).toBe(true);
            });
        });

        function assertCurrentUserSet() {
            expect($rootScope.currentUser).not.toBeUndefined();
            expect($rootScope.currentUser.id).toBe('user_id');
            expect($rootScope.authCallComplete).toBe(true);
            expect(Auid.set).toHaveBeenCalledWith(userResponse.id);
            expect(EventLogger.prototype.log).toHaveBeenCalledWith('user:login');
            expect(ValidationResponder.identifyCurrentUser).toHaveBeenCalled();
        }
    });

    describe('forwardToTargetUrl', () => {
        afterEach(() => {
            delete window.CORDOVA;
            ClientStorage.removeItem('last_visited_route');
        });

        it('should clear temporary redirect targets from ClientStorage', () => {
            jest.spyOn(ClientStorage, 'removeItem').mockImplementation(() => {});
            ValidationResponder.forwardToTargetUrl();

            expect(ClientStorage.removeItem).toHaveBeenCalledWith('logged_in_as');
            expect(ClientStorage.removeItem).toHaveBeenCalledWith('oauth_redirect_target');
            expect(ClientStorage.removeItem).toHaveBeenCalledWith('last_visited_route');
        });

        it('should redirect to target found in last_visited_route if Cordova', () => {
            jest.spyOn($location, 'url').mockImplementation(() => {});

            ClientStorage.setItem('last_visited_route', '/route_a');
            ValidationResponder.forwardToTargetUrl();
            expect($location.url).not.toHaveBeenCalled();
            expect(ClientStorage.getItem('last_visited_route')).toBeUndefined(); // cleared

            window.CORDOVA = true;
            ClientStorage.setItem('last_visited_route', '/route_a');
            ValidationResponder.forwardToTargetUrl();
            expect($location.url).toHaveBeenCalledWith('/route_a');
        });

        it('should redirect to target found in querystring if not Cordova', () => {
            jest.spyOn($location, 'url').mockImplementation(() => {});
            jest.spyOn($location, 'search').mockReturnValue({
                target: '/route_b',
            });

            window.CORDOVA = true;
            ValidationResponder.forwardToTargetUrl();
            expect($location.url).not.toHaveBeenCalled();

            delete window.CORDOVA;
            ValidationResponder.forwardToTargetUrl();
            expect($location.url).toHaveBeenCalledWith('/route_b');
        });

        it('should redirect to target found in oauth_redirect_target if not Cordova', () => {
            jest.spyOn($location, 'url').mockImplementation(() => {});

            window.CORDOVA = true;
            ClientStorage.setItem('oauth_redirect_target', '/route_c');
            ValidationResponder.forwardToTargetUrl();
            expect($location.url).not.toHaveBeenCalled();
            expect(ClientStorage.getItem('oauth_redirect_target')).toBeUndefined(); // cleared

            delete window.CORDOVA;
            ClientStorage.setItem('oauth_redirect_target', '/route_c');
            ValidationResponder.forwardToTargetUrl();
            expect($location.url).toHaveBeenCalledWith('/route_c');
        });

        it('should handle an absolute url', () => {
            jest.spyOn(ValidationResponder, 'setHref').mockImplementation(() => {});
            delete window.CORDOVA;

            ClientStorage.setItem('oauth_redirect_target', 'http://some/absolute/url');
            ValidationResponder.forwardToTargetUrl();
            expect(ValidationResponder.setHref).toHaveBeenCalledWith('http://some/absolute/url');

            // It should not refresh the page if it's already on the right one
            ClientStorage.setItem('oauth_redirect_target', $window.location.href);
            ValidationResponder.setHref.mockClear();
            ValidationResponder.forwardToTargetUrl();
            expect(ValidationResponder.setHref).not.toHaveBeenCalled();
        });
    });

    describe('SAML login', () => {
        it('should be initialized when auth:invalid event is triggered', () => {
            SpecHelper.stubEventLogging();
            const $auth = $injector.get('$auth');
            const omniauth = $injector.get('omniauth');
            const institutionalSubdomain = $injector.get('institutionalSubdomain');

            jest.spyOn($rootScope, 'goHome');
            jest.spyOn(omniauth, 'loginWithProvider').mockReturnValue($q.resolve());
            $auth.dfd = 'dfd';
            institutionalSubdomain.id = 'subdomain';

            // This needs to be called in order to setup the
            // listeners on auth:invalid
            ValidationResponder.initialize();
            $rootScope.$emit('auth:invalid');

            // Initially $auth.dfd is still set, so we have to
            // wait for it to get unset
            expect(omniauth.loginWithProvider).not.toHaveBeenCalled();
            $auth.dfd = null;
            $timeout.flush();
            expect(omniauth.loginWithProvider).toHaveBeenCalledWith(institutionalSubdomain.id);

            expect($rootScope.goHome).toHaveBeenCalled(); // called from forwardToTargetUrl
            expect($rootScope.authCallComplete).toBe(true);
        });
    });
});
