import 'AngularSpecHelper';
import 'Authentication/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import institutionRegisterLocales from 'Authentication/locales/authentication/institution_register-en.json';
import sessionsNavigationLocales from 'Navigation/locales/navigation/sessions_navigation-en.json';
import signUpFormLocales from 'Authentication/locales/authentication/sign_up_form-en.json';

setSpecLocales(institutionRegisterLocales, sessionsNavigationLocales, signUpFormLocales);

describe('Authentication::InstitutionRegister', () => {
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let $location;
    let $timeout;
    let $q;
    let NavigationHelperMixin;
    let JoinConfig;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Authentication', 'SpecHelper');

        angular.mock.module($provide => {
            $provide.value('ipCookie', jest.fn());
        });

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            $location = $injector.get('$location');
            $timeout = $injector.get('$timeout');
            $q = $injector.get('$q');
            NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
            JoinConfig = $injector.get('JoinConfig');

            SpecHelper.mockCapabilities();

            SpecHelper.stubConfig({
                is_quantic: false,
            });
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render(urlPrefix, urlConfigOverrides) {
        renderer = SpecHelper.renderer();

        let defaultJoinConfig;
        JoinConfig.getConfig('default').then(config => {
            defaultJoinConfig = config;
        });
        $timeout.flush();

        const deferred = $q.defer();
        jest.spyOn(JoinConfig, 'getConfig').mockReturnValue(deferred.promise);
        deferred.resolve(_.extend({}, defaultJoinConfig, urlConfigOverrides));

        if (urlPrefix) {
            renderer.render(`<institution-register url-prefix="${urlPrefix}"></institution-register>`);
        } else {
            renderer.render('<institution-register></institution-register>');
        }

        elem = renderer.elem;
        scope = renderer.childScope;

        if (urlPrefix) {
            expect(JoinConfig.getConfig).toHaveBeenCalledWith(urlPrefix);
        } else {
            expect(JoinConfig.getConfig).toHaveBeenCalled();
        }
    }

    it('should have a link to the Sign-In page', () => {
        render();
        const signinButton = SpecHelper.expectElementText(elem, '.sign-in-link > a', 'Login', 0);
        jest.spyOn($location, 'path').mockImplementation(() => {});
        signinButton.click();
        expect($location.path).toHaveBeenCalledWith('/sign-in');
    });

    it('should have a link to sign-in page', () => {
        render();
        jest.spyOn(scope, 'loadRoute').mockImplementation(() => {});
        SpecHelper.click(elem, '.sign-in-link a');
        expect(scope.loadRoute).toHaveBeenCalledWith('/sign-in', false);
    });

    it('should use NavigationHelperMixin', () => {
        jest.spyOn(NavigationHelperMixin, 'onLink').mockImplementation(() => {});
        render();
        expect(NavigationHelperMixin.onLink).toHaveBeenCalled();
    });

    it('should show nav list', () => {
        render();
        SpecHelper.expectElementText(elem, '.nav-list', 'Login Get Started Now');
    });

    it('should show title if defined in join_config', () => {
        render('prefix', {
            title_message: 'authentication.institution_register.sign_up',
        });
        SpecHelper.expectElementText(elem, '[name="title"]', 'Sign up for Smartly');
    });

    it('should show subtitle if defined in join_config', () => {
        render('prefix', {
            subtitle_message: 'authentication.institution_register.sign_up_to_start_reviewing',
        });
        SpecHelper.expectElementText(
            elem,
            '[name="subtitle"]',
            'Sign up to start connecting with elite MBA candidates in our career network.',
        );
    });
});
