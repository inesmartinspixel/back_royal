import 'AngularSpecHelper';
import 'Authentication/angularModule';

describe('Authentication::captchaWrapper', () => {
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let $window;
    let ConfigFactory;
    let $q;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Authentication', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');

            $window = $injector.get('$window');
            $q = $injector.get('$q');
            ConfigFactory = $injector.get('ConfigFactory');

            SpecHelper.stubConfig();
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.render('<captcha-wrapper></captcha-wrapper>');
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    it('should block render before ConfigFactory.getConfig resolves', () => {
        const deferred = $q.defer();
        jest.spyOn(ConfigFactory, 'getConfig').mockReturnValue(deferred.promise);
        render();
        SpecHelper.expectNoElement(elem, '.g-recaptcha');

        deferred.resolve({
            recaptcha_site_key: 'foo_bar',
        });
        scope.$digest(); // $compile won't run until a digest
        SpecHelper.expectElement(elem, '.g-recaptcha');
    });

    describe('captchaInvalid', () => {
        beforeEach(() => {
            jest.spyOn(ConfigFactory, 'getConfig').mockImplementation(() =>
                $q.resolve({
                    recaptcha_site_key: 'foo_bar',
                }),
            );
        });

        it('should be true if !window.captchaComplete', () => {
            window.captchaComplete = false;
            render();
            expect($window.captchaInvalid()).toBe(true);
        });

        it('should be false if window.captchaComplete', () => {
            window.captchaComplete = true;
            render();
            expect($window.captchaInvalid()).toBe(false);
        });
    });
});
