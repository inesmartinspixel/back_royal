import 'AngularSpecHelper';
import 'Authentication/angularModule';
import institutionRegisterLocales from 'Authentication/locales/authentication/institution_register-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(institutionRegisterLocales);

describe('Authentication::DemoJoin', () => {
    let renderer;
    let elem;
    let SpecHelper;
    let $q;
    let NavigationHelperMixin;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Authentication', 'SpecHelper');

        angular.mock.module($provide => {
            $provide.value('ipCookie', jest.fn());
        });

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            $q = $injector.get('$q');
            NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
            const JoinConfig = $injector.get('JoinConfig');

            SpecHelper.mockCapabilities();
            SpecHelper.stubConfig();

            const deferred = $q.defer(); // use the same promise for both calls
            jest.spyOn(JoinConfig, 'getConfig').mockReturnValue(deferred.promise);
            deferred.resolve({
                institutional_demo: true,
            });
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render(urlPrefix) {
        renderer = SpecHelper.renderer();

        if (urlPrefix) {
            renderer.render(`<demo-join url-prefix="${urlPrefix}"></demo-join>`);
        } else {
            renderer.render('<demo-join></demo-join>');
        }

        elem = renderer.elem;
    }

    it('should not have any links or nav list', () => {
        render();
        SpecHelper.expectNoElement(elem, '.nav-list');
        SpecHelper.expectNoElement(elem, '.sign-in-link');
        SpecHelper.expectNoElement(elem, '.nav-elements');
    });

    it('should use NavigationHelperMixin', () => {
        jest.spyOn(NavigationHelperMixin, 'onLink').mockImplementation(() => {});
        render();
        expect(NavigationHelperMixin.onLink).toHaveBeenCalled();
    });
});
