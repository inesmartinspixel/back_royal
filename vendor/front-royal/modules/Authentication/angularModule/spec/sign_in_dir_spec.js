import 'AngularSpecHelper';
import 'Authentication/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import signInLocales from 'Authentication/locales/authentication/sign_in-en.json';
import sessionsNavigationLocales from 'Navigation/locales/navigation/sessions_navigation-en.json';

setSpecLocales(signInLocales, sessionsNavigationLocales);

describe('Authentication::SignIn', () => {
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let $timeout;
    let $injector;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Authentication', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector => {
                $injector = _$injector;
                SpecHelper = $injector.get('SpecHelper');
                $timeout = $injector.get('$timeout');

                SpecHelper.stubConfig();
                SpecHelper.stubDirective('signInForm');
            },
        ]);
    });

    describe('join page link', () => {
        it('should work', () => {
            render();
            $timeout.flush(); // get config result
            const joinLink = SpecHelper.expectElementText(elem, '.join-link', "Don't have an account? Sign Up", 0);
            expect(joinLink.css('visibility')).not.toEqual('hidden');
            jest.spyOn(scope, 'gotoJoin').mockImplementation(() => {});
            joinLink.find('a').click();
            expect(scope.gotoJoin).toHaveBeenCalled();
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.render('<sign-in></sign-in>');
        elem = renderer.elem;
        scope = renderer.childScope; // we delegate to a sub-directive
    }
});
