import 'AngularSpecHelper';
import 'Authentication/angularModule';
import forgotPasswordLocales from 'Authentication/locales/authentication/forgot_password-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(forgotPasswordLocales);

describe('Authentication.ForgotPasswordForm', () => {
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let $q;
    let $auth;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Authentication', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            $q = $injector.get('$q');
            $auth = $injector.get('$auth');
            SpecHelper.stubConfig();
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.render('<forgot-password-form></forgot-password-form>');
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    describe('form fields', () => {
        it('should have an email input', () => {
            render();

            const inputs = SpecHelper.expectElements(elem, 'input', 1);
            SpecHelper.expectElementHasClass(inputs, 'form-control');

            expect(inputs.eq(0).attr('name')).toEqual('email');
            expect(inputs.eq(0).attr('type')).toEqual('email');
            expect(inputs.eq(0).attr('required')).toEqual('required');
            expect(inputs.eq(0).attr('placeholder')).toEqual('Your email');
        });
    });

    describe('form submission', () => {
        beforeEach(() => {
            render();
            SpecHelper.expectElement(elem, 'button[type="submit"]');
            SpecHelper.updateTextInput(elem, 'input[name="email"]', 'some.user@email.com');
            expect(scope.form.email).toEqual('some.user@email.com');
        });

        it('should load notification screen after submission', () => {
            let deferred;

            deferred = $q.defer();
            jest.spyOn($auth, 'requestPasswordReset').mockReturnValue(deferred.promise);

            // submit.click();
            SpecHelper.submitForm(elem);
            deferred.resolve();
            scope.$digest();
            SpecHelper.expectElementText(
                elem,
                '.reset',
                'We sent you an email with instructions for resetting your password.',
            );
        });

        it('should display a toast message upon invalid resets', () => {
            jest.spyOn($auth, 'requestPasswordReset').mockReturnValue($q.reject());
            // submit.click();
            SpecHelper.submitForm(elem);
            expect($auth.requestPasswordReset).toHaveBeenCalledWith(scope.form);
            SpecHelper.expectElementText(
                elem,
                '.form-error',
                'Unable to process your request. Please make sure your email address is correct.',
            );
        });
    });
});
