import 'AngularSpecHelper';
import 'MobileAppRateHelper/angularModule';
import moment from 'moment-timezone';

describe('MobileAppRateHelper', () => {
    let $injector;
    let $rootScope;
    let $window;
    let $q;
    let MobileAppRateHelper;
    let User;
    let user;

    beforeEach(() => {
        angular.mock.module('MobileAppRateHelper', 'SpecHelper');

        angular.mock.module($provide => {
            User = {
                MAX_NUM_APP_RATING_PROMPTS: 42,
            };
            $provide.value('User', User);
        });

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                $rootScope = $injector.get('$rootScope');
                $window = $injector.get('$window');
                $q = $injector.get('$q');
                MobileAppRateHelper = $injector.get('MobileAppRateHelper');

                user = { progress: { getAllProgress: jest.fn() } };
            },
        ]);
    });

    describe('supportsNativeStorage', () => {
        describe('when not on CORDOVA', () => {
            beforeEach(() => {
                delete $window.CORDOVA;
            });

            it('should return false', () => {
                expect(MobileAppRateHelper.supportsNativeStorage()).toBe(false);
            });
        });

        describe('when on CORDOVA', () => {
            beforeEach(() => {
                $window.CORDOVA = true;
            });

            afterEach(() => {
                delete $window.CORDOVA;
            });

            describe('when NativeStorage is not present', () => {
                beforeEach(() => {
                    delete $window.NativeStorage;
                });

                it('should return false', () => {
                    expect(MobileAppRateHelper.supportsNativeStorage()).toBe(false);
                });
            });

            describe('when NativeStorage is present', () => {
                beforeEach(() => {
                    $window.NativeStorage = {};
                });

                afterEach(() => {
                    delete $window.NativeStorage;
                });

                it('should return true', () => {
                    expect(MobileAppRateHelper.supportsNativeStorage()).toBe(true);
                });
            });
        });
    });

    describe('meetsRequirementsForMobileAppRatingPrompt', () => {
        let defaultMocks;

        beforeEach(() => {
            defaultMocks = {
                hasExternalInstitution: false,
                isMiyaMiya: false,
                isDemo: false,
                defaultsToHiringExperience: false,
                hasEverApplied: true,
                hasEverBeenRejected: false,
                isAccepted: true,
                lastCohortApplication: {
                    can_convert_to_emba: false,
                },
            };
        });

        function mockProperties(mocks) {
            _.each(mocks, (value, key) => {
                user[key] = value;
            });
        }

        describe('when asynchronous checks are not called', () => {
            function assertWhenGetAllProgressIsNotCalled(overrides, _user, supportsNativeStorage) {
                const mocks = _.extend({}, defaultMocks, overrides);
                mockProperties(mocks);
                jest.spyOn(MobileAppRateHelper, 'supportsNativeStorage').mockReturnValue(
                    angular.isDefined(supportsNativeStorage) ? supportsNativeStorage : true,
                );
                const u = angular.isDefined(_user) ? _user : user;
                const response = MobileAppRateHelper.meetsRequirementsForMobileAppRatingPrompt(u);
                expect(response.$$state.value).toBe(false);
                expect(user.progress.getAllProgress).not.toHaveBeenCalled();
            }

            it('should return false if !supportsNativeStorage', () => {
                assertWhenGetAllProgressIsNotCalled({}, user, false);
            });

            it('should return false if no user is passed in', () => {
                assertWhenGetAllProgressIsNotCalled({}, null);
            });

            it('should return false if hasExternalInstitution', () => {
                assertWhenGetAllProgressIsNotCalled({
                    hasExternalInstitution: !defaultMocks.hasExternalInstitution,
                });
            });

            it('should return false if isMiyaMiya', () => {
                assertWhenGetAllProgressIsNotCalled({
                    hasExternalInstitution: !defaultMocks.isMiyaMiya,
                });
            });

            it('should return false if isDemo', () => {
                assertWhenGetAllProgressIsNotCalled({
                    isDemo: !defaultMocks.isDemo,
                });
            });

            it('should return false if defaultsToHiringExperience', () => {
                assertWhenGetAllProgressIsNotCalled({
                    defaultsToHiringExperience: !defaultMocks.defaultsToHiringExperience,
                });
            });

            it('should return false if !hasEverApplied', () => {
                assertWhenGetAllProgressIsNotCalled({
                    hasEverApplied: !defaultMocks.hasEverApplied,
                });
            });

            it('should return false if hasEverBeenRejected and !isAccepted', () => {
                assertWhenGetAllProgressIsNotCalled({
                    hasEverBeenRejected: !defaultMocks.hasEverBeenRejected,
                    isAccepted: !defaultMocks.isAccepted,
                });
            });

            it('should return false if lastCohortApplication.can_convert_to_emba', () => {
                assertWhenGetAllProgressIsNotCalled({
                    lastCohortApplication: {
                        can_convert_to_emba: true,
                    },
                });
            });
        });

        describe('when asynchronous checks are called', () => {
            beforeEach(() => {
                jest.spyOn(MobileAppRateHelper, 'supportsNativeStorage').mockReturnValue(true);
                mockProperties(defaultMocks);
            });

            function assertAsynchronousChecks(expectedValue, overrides = {}) {
                const mockValues = _.extend(
                    {
                        hasCompleteStreamProgress: true,
                        appRatingPromptCounter: 0,
                        lastAppRatingPromptAt: moment().subtract(30, 'days').toDate(),
                    },
                    overrides,
                );
                const deferredGetAllProgress = $q.defer();
                const deferredGetAppRatingPromptCounter = $q.defer();
                const deferredGetLastAppRatingPromptAt = $q.defer();

                user.progress.getAllProgress.mockReturnValue(deferredGetAllProgress.promise);
                jest.spyOn(MobileAppRateHelper, 'getAppRatingPromptCounter').mockReturnValue(
                    deferredGetAppRatingPromptCounter.promise,
                );
                jest.spyOn(MobileAppRateHelper, 'getLastAppRatingPromptAt').mockReturnValue(
                    deferredGetLastAppRatingPromptAt.promise,
                );

                const response = MobileAppRateHelper.meetsRequirementsForMobileAppRatingPrompt(user);

                deferredGetAllProgress.resolve({
                    streamProgress: [
                        {
                            complete: mockValues.hasCompleteStreamProgress,
                        },
                    ],
                });
                deferredGetAppRatingPromptCounter.resolve(mockValues.appRatingPromptCounter);
                deferredGetLastAppRatingPromptAt.resolve(mockValues.lastAppRatingPromptAt);
                $rootScope.$apply(); // Propagate promise resolution to 'then' functions

                expect(response.$$state.value).toBe(expectedValue);
            }

            it('should return false if user has not completed a stream', () => {
                assertAsynchronousChecks(false, {
                    hasCompleteStreamProgress: false,
                });
            });

            describe('when user has completed a stream', () => {
                it('should return false if appRatingPromptCounter is greater than or equal to MAX_NUM_APP_RATING_PROMPTS', () => {
                    assertAsynchronousChecks(false, {
                        appRatingPromptCounter: User.MAX_NUM_APP_RATING_PROMPTS,
                    });
                });

                describe('when appRatingPromptCounter is less than MAX_NUM_APP_RATING_PROMPTS', () => {
                    it('should return true if no lastAppRatingPromptAt is present', () => {
                        assertAsynchronousChecks(true, {
                            lastAppRatingPromptAt: null,
                        });
                    });

                    it('should return false if lastAppRatingPromptAt is less than 30 days ago', () => {
                        assertAsynchronousChecks(false, {
                            lastAppRatingPromptAt: moment().subtract(29, 'days').toDate(),
                        });
                    });

                    it('should return true if lastAppRatingPromptAt is at least 30 days ago', () => {
                        assertAsynchronousChecks(true);
                    });
                });
            });
        });
    });

    describe('getAppRatingPromptCounter', () => {
        describe('when supportsNativeStorage', () => {
            let successCallback;
            let errorCallback;
            let response;

            beforeEach(() => {
                jest.spyOn(MobileAppRateHelper, 'supportsNativeStorage').mockReturnValue(true);
                $window.NativeStorage = {
                    getItem: jest.fn(),
                };
                jest.spyOn($window.NativeStorage, 'getItem').mockImplementation(
                    (_key, _successCallback, _errorCallback) => {
                        successCallback = _successCallback;
                        errorCallback = _errorCallback;
                    },
                );
                response = MobileAppRateHelper.getAppRatingPromptCounter();
                expect($window.NativeStorage.getItem).toHaveBeenCalledWith(
                    MobileAppRateHelper.APP_RATING_PROMPT_COUNTER,
                    successCallback,
                    errorCallback,
                );
            });

            afterEach(() => {
                delete $window.NativeStorage;
            });

            describe('when successful', () => {
                it('should get APP_RATING_PROMPT_COUNTER and resolve the value', () => {
                    const expectedValue = 0;
                    expect(response.$$state.status).toEqual(0); // not resolved yet
                    expect(response.$$state.value).toBeUndefined();
                    successCallback(expectedValue);
                    expect(response.$$state.status).toEqual(1); // resolved
                    expect(response.$$state.value).toEqual(expectedValue);
                });
            });

            describe('when error occurred', () => {
                it('should resolve the value 0 if error code indicates ITEM_NOT_FOUND', () => {
                    const expectedValue = 0;
                    expect(response.$$state.status).toEqual(0); // not resolved yet
                    expect(response.$$state.value).toBeUndefined();
                    errorCallback({
                        code: 2, // error code 2 indicates ITEM_NOT_FOUND
                    });
                    expect(response.$$state.status).toEqual(1); // resolved
                    expect(response.$$state.value).toEqual(expectedValue);
                });

                it('should reject error if error code does not indicate ITEM_NOT_FOUND', () => {
                    expect(response.$$state.status).toEqual(0); // not rejected yet
                    expect(response.$$state.value).toBeUndefined();
                    const error = {
                        code: 'not_2', // error code 2 indicates ITEM_NOT_FOUND
                    };
                    errorCallback(error);
                    expect(response.$$state.status).toEqual(2); // rejected
                    expect(response.$$state.value).toEqual(error);
                });
            });
        });

        describe('when !supportsNativeStorage', () => {
            beforeEach(() => {
                jest.spyOn(MobileAppRateHelper, 'supportsNativeStorage').mockReturnValue(false);

                // In the wild, NativeStorage would never be present when not on CORDOVA,
                // but it's useful for our tests to mock the NativeStorage service so that
                // we can make an assertion that the getItem method doesn't get called.
                $window.NativeStorage = {
                    getItem: jest.fn(),
                };
            });

            afterEach(() => {
                delete $window.NativeStorage;
            });

            it('should reject promise immediately without attempting to get APP_RATING_PROMPT_COUNTER', () => {
                const response = MobileAppRateHelper.getAppRatingPromptCounter();
                expect($window.NativeStorage.getItem).not.toHaveBeenCalled();
                expect(response.$$state.status).toEqual(2); // rejected
            });
        });
    });

    describe('setAppRatingPromptCounter', () => {
        describe('supportsNativeStorage', () => {
            let successCallback;
            let errorCallback;
            let response;

            beforeEach(() => {
                jest.spyOn(MobileAppRateHelper, 'supportsNativeStorage').mockReturnValue(true);
                $window.NativeStorage = {
                    setItem: jest.fn(),
                };
                jest.spyOn($window.NativeStorage, 'setItem').mockImplementation(
                    (_key, _value, _successCallback, _errorCallback) => {
                        successCallback = _successCallback;
                        errorCallback = _errorCallback;
                    },
                );
                response = MobileAppRateHelper.setAppRatingPromptCounter(7);
                expect($window.NativeStorage.setItem).toHaveBeenCalledWith(
                    MobileAppRateHelper.APP_RATING_PROMPT_COUNTER,
                    7,
                    successCallback,
                    errorCallback,
                );
            });

            afterEach(() => {
                delete $window.NativeStorage;
            });

            describe('when successful', () => {
                it('should resolve the returned promise', () => {
                    expect(response.$$state.status).toEqual(0); // not resolved yet
                    successCallback();
                    expect(response.$$state.status).toEqual(1); // resolved
                });
            });

            describe('when an error occurred', () => {
                it('should reject the error', () => {
                    expect(response.$$state.status).toEqual(0); // not rejected yet
                    expect(response.$$state.value).toBeUndefined();
                    const error = 'foo';
                    errorCallback(error);
                    expect(response.$$state.status).toEqual(2); // not rejected yet
                    expect(response.$$state.value).toEqual('foo');
                });
            });
        });

        describe('when !supportsNativeStorage', () => {
            beforeEach(() => {
                jest.spyOn(MobileAppRateHelper, 'supportsNativeStorage').mockReturnValue(false);

                // In the wild, NativeStorage would never be present when not on CORDOVA,
                // but it's useful for our tests to mock the NativeStorage service so that
                // we can make an assertion that the getItem method doesn't get called.
                $window.NativeStorage = {
                    setItem: jest.fn(),
                };
            });

            afterEach(() => {
                delete $window.NativeStorage;
            });

            it('should reject the returned promise immediately without attempting to set APP_RATING_PROMPT_COUNTER', () => {
                const response = MobileAppRateHelper.setAppRatingPromptCounter(7);
                expect($window.NativeStorage.setItem).not.toHaveBeenCalled();
                expect(response.$$state.status).toEqual(2); // rejected
            });
        });
    });

    describe('incrementAppRatingPromptCounter', () => {
        beforeEach(() => {
            $q = $injector.get('$q');
            $rootScope = $injector.get('$rootScope');
        });

        it('should getAppRatingPromptCounter and then setAppRatingPromptCounter to an incremented value', () => {
            const deferredGetAppRatingPromptCounter = $q.defer();
            const deferredSetAppRatingPromptCounter = $q.defer();
            jest.spyOn(MobileAppRateHelper, 'getAppRatingPromptCounter').mockReturnValue(
                deferredGetAppRatingPromptCounter.promise,
            );
            jest.spyOn(MobileAppRateHelper, 'setAppRatingPromptCounter').mockReturnValue(
                deferredSetAppRatingPromptCounter.promise,
            );

            const response = MobileAppRateHelper.incrementAppRatingPromptCounter();
            expect(MobileAppRateHelper.getAppRatingPromptCounter).toHaveBeenCalled();
            expect(MobileAppRateHelper.setAppRatingPromptCounter).not.toHaveBeenCalled();
            expect(response.$$state.status).toEqual(0); // not resolved yet

            deferredGetAppRatingPromptCounter.resolve(7);
            $rootScope.$apply(); // Propagate promise resolution to 'then' functions
            expect(MobileAppRateHelper.setAppRatingPromptCounter).toHaveBeenCalledWith(8);
            expect(response.$$state.status).toEqual(0); // not resolved yet

            deferredSetAppRatingPromptCounter.resolve();
            $rootScope.$apply(); // Propagate promise resolution to 'then' functions
            expect(response.$$state.status).toEqual(1); // resolved
        });

        describe('when getAppRatingPromptCounter returns rejected promise', () => {
            it('should catch it and resolve', () => {
                const deferredGetAppRatingPromptCounter = $q.defer();
                jest.spyOn(MobileAppRateHelper, 'getAppRatingPromptCounter').mockReturnValue(
                    deferredGetAppRatingPromptCounter.promise,
                );
                jest.spyOn(MobileAppRateHelper, 'setAppRatingPromptCounter');

                const response = MobileAppRateHelper.incrementAppRatingPromptCounter();
                expect(MobileAppRateHelper.getAppRatingPromptCounter).toHaveBeenCalled();
                expect(MobileAppRateHelper.setAppRatingPromptCounter).not.toHaveBeenCalled();
                expect(response.$$state.status).toEqual(0); // not rejected yet

                deferredGetAppRatingPromptCounter.reject();
                $rootScope.$apply(); // Propagate promise resolution to 'catch' functions
                expect(MobileAppRateHelper.setAppRatingPromptCounter).not.toHaveBeenCalled();
                expect(response.$$state.status).toEqual(1); // resolved through catch
            });
        });

        describe('when setAppRatingPromptCounter returns rejected promise', () => {
            it('should catch it and resolve', () => {
                const deferredGetAppRatingPromptCounter = $q.defer();
                const deferredSetAppRatingPromptCounter = $q.defer();
                jest.spyOn(MobileAppRateHelper, 'getAppRatingPromptCounter').mockReturnValue(
                    deferredGetAppRatingPromptCounter.promise,
                );
                jest.spyOn(MobileAppRateHelper, 'setAppRatingPromptCounter').mockReturnValue(
                    deferredSetAppRatingPromptCounter.promise,
                );

                const response = MobileAppRateHelper.incrementAppRatingPromptCounter();
                expect(MobileAppRateHelper.getAppRatingPromptCounter).toHaveBeenCalled();
                expect(MobileAppRateHelper.setAppRatingPromptCounter).not.toHaveBeenCalled();
                expect(response.$$state.status).toEqual(0); // not resolved yet

                deferredGetAppRatingPromptCounter.resolve(7);
                $rootScope.$apply(); // Propagate promise resolution to 'then' functions
                expect(MobileAppRateHelper.setAppRatingPromptCounter).toHaveBeenCalledWith(8);
                expect(response.$$state.status).toEqual(0); // not resolved yet

                deferredSetAppRatingPromptCounter.reject();
                $rootScope.$apply(); // Propagate promise resolution to 'catch' functions
                expect(response.$$state.status).toEqual(1); // resolved through catch
            });
        });
    });

    describe('getLastAppRatingPromptAt', () => {
        describe('when supportsNativeStorage', () => {
            let successCallback;
            let errorCallback;
            let response;

            beforeEach(() => {
                jest.spyOn(MobileAppRateHelper, 'supportsNativeStorage').mockReturnValue(true);
                $window.NativeStorage = {
                    getItem: jest.fn(),
                };
                jest.spyOn($window.NativeStorage, 'getItem').mockImplementation(
                    (_key, _successCallback, _errorCallback) => {
                        successCallback = _successCallback;
                        errorCallback = _errorCallback;
                    },
                );
                response = MobileAppRateHelper.getLastAppRatingPromptAt();
                expect($window.NativeStorage.getItem).toHaveBeenCalledWith(
                    MobileAppRateHelper.LAST_APP_RATING_PROMPT_AT,
                    successCallback,
                    errorCallback,
                );
            });

            afterEach(() => {
                delete $window.NativeStorage;
            });

            describe('when successful', () => {
                it('should get LAST_APP_RATING_PROMPT_AT and resolve a date object', () => {
                    const expectedValue = new Date().getTime();
                    expect(response.$$state.status).toEqual(0); // not resolved yet
                    expect(response.$$state.value).toBeUndefined();
                    successCallback(expectedValue);
                    expect(response.$$state.status).toEqual(1); // resolved
                    expect(response.$$state.value).toEqual(new Date(expectedValue));
                });
            });

            describe('when error occurred', () => {
                it('should resolve the value null if error code indicates ITEM_NOT_FOUND', () => {
                    expect(response.$$state.status).toEqual(0); // not resolved yet
                    expect(response.$$state.value).toBeUndefined();
                    errorCallback({
                        code: 2, // error code 2 indicates ITEM_NOT_FOUND
                    });
                    expect(response.$$state.status).toEqual(1); // resolved
                    expect(response.$$state.value).toBeNull();
                });

                it('should reject error if error code does not indicate ITEM_NOT_FOUND', () => {
                    expect(response.$$state.status).toEqual(0); // not rejected yet
                    expect(response.$$state.value).toBeUndefined();
                    const error = {
                        code: 'not_2', // error code 2 indicates ITEM_NOT_FOUND
                    };
                    errorCallback(error);
                    expect(response.$$state.status).toEqual(2); // rejected
                    expect(response.$$state.value).toEqual(error);
                });
            });
        });

        describe('when !supportsNativeStorage', () => {
            beforeEach(() => {
                jest.spyOn(MobileAppRateHelper, 'supportsNativeStorage').mockReturnValue(false);

                // In the wild, NativeStorage would never be present when not on CORDOVA,
                // but it's useful for our tests to mock the NativeStorage service so that
                // we can make an assertion that the getItem method doesn't get called.
                $window.NativeStorage = {
                    getItem: jest.fn(),
                };
            });

            afterEach(() => {
                delete $window.NativeStorage;
            });

            it('should reject promise immediately without attempting to get LAST_APP_RATING_PROMPT_AT', () => {
                const response = MobileAppRateHelper.getLastAppRatingPromptAt();
                expect($window.NativeStorage.getItem).not.toHaveBeenCalled();
                expect(response.$$state.status).toEqual(2); // rejected
            });
        });
    });
});
