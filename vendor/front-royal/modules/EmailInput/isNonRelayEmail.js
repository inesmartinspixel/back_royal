// see http://stackoverflow.com/a/8809472/2823912
const INVALID_EMAIL_DOMAINS = ['privaterelay.appleid.com'];

export default function isNonRelayEmail(email) {
    const lowerEmail = email.toLowerCase();
    return INVALID_EMAIL_DOMAINS.some(domain => lowerEmail.endsWith(domain));
}
