import isNonRelayEmail from './isNonRelayEmail';

describe('isNonRelayEmail', () => {
    it('should return true if domain ends in privaterelay.appleid.com', () => {
        expect(isNonRelayEmail('anon@privaterelay.appleid.com')).toBe(true);
    });

    it('should return false if domain does not end in privaterelay.appleid.com', () => {
        expect(isNonRelayEmail('valid@valid-domain.com')).toBe(false);
    });
});
