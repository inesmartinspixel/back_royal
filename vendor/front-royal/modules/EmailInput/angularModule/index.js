import './scripts/email_input_module';

import './scripts/free_email_domains';
import './scripts/email_validation_dir';
import './scripts/free_email_domain_list';
import './scripts/require_domain_dir';

import 'EmailInput/locales/email_input/email_validation-en.json';
