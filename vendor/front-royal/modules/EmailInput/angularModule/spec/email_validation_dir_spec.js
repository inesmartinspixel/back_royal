import 'AngularSpecHelper';
import 'EmailInput/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import emailValidationLocales from 'EmailInput/locales/email_input/email_validation-en.json';

setSpecLocales(emailValidationLocales);

describe('FrontRoyal.EmailInput.freeEmailDomains', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let freeEmailDomains;
    let errors;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.EmailInput', 'SpecHelper');

        angular.mock.inject([
            '$injector',

            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                freeEmailDomains = $injector.get('freeEmailDomains');
            },
        ]);
    });

    function render(options) {
        const model = '';
        renderer = SpecHelper.renderer();

        const mockForm = {
            ngModel: model,
        };

        errors = {};
        renderer.scope.ngModel = mockForm.ngModel;
        renderer.scope.options = {
            errors,
        };
        _.extend(renderer.scope.options, options);
        renderer.render(
            '<form name="spec_form"><input type="text" name="email" email-validation="options" ng-model="ngModel"></input></form>',
        );
        elem = renderer.elem;
    }

    afterEach(() => {
        SpecHelper.cleanup();
    });

    it('should be valid only with something that looks like an email', () => {
        render();
        SpecHelper.updateTextInput(elem, 'input', 'someone');
        expect(errors).toEqual({
            email: 'Please enter a valid email address',
        });
        SpecHelper.updateTextInput(elem, 'input', 'someone@assdfsd');
        expect(errors).toEqual({
            email: 'Please enter a valid email address',
        });
        SpecHelper.updateTextInput(elem, 'input', 'someone@assdfsd.com');
        expect(errors).toEqual({});

        // make sure long TLDs are supported
        SpecHelper.updateTextInput(elem, 'input', 'someone@assdfsd.abcdefgh');
        expect(errors).toEqual({});

        // make sure capital letters are supported
        SpecHelper.updateTextInput(elem, 'input', 'someONE@assdfsd.abcdefgh');
        expect(errors).toEqual({});
    });

    describe('with requireCompanyEmail != true', () => {
        it('should not check email domain', () => {
            jest.spyOn(freeEmailDomains, 'isFreeEmail').mockImplementation(() => {});
            render();
            SpecHelper.updateTextInput(elem, 'input', 'someone@example.com');
            expect(freeEmailDomains.isFreeEmail).not.toHaveBeenCalled();
            expect(errors).toEqual({});
        });
    });

    describe('with requireCompanyEmail === true', () => {
        beforeEach(() => {
            render({
                requireCompanyEmail: true,
            });
        });

        it('should be invalid with a free email', () => {
            jest.spyOn(freeEmailDomains, 'isFreeEmail').mockReturnValue(true);
            SpecHelper.updateTextInput(elem, 'input', 'someone@example.com');
            expect(freeEmailDomains.isFreeEmail).toHaveBeenCalledWith('someone@example.com');
            expect(errors).toEqual({
                email: 'Please use your company email address',
            });
        });

        it('should be valid with a company email', () => {
            jest.spyOn(freeEmailDomains, 'isFreeEmail').mockReturnValue(false);
            SpecHelper.updateTextInput(elem, 'input', 'someone@example.com');
            expect(freeEmailDomains.isFreeEmail).toHaveBeenCalledWith('someone@example.com');
            expect(errors).toEqual({});
        });
    });
});
