import 'AngularSpecHelper';
import 'EmailInput/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import emailValidationLocales from 'EmailInput/locales/email_input/email_validation-en.json';

setSpecLocales(emailValidationLocales);

describe('FrontRoyal.EmailInput.requireDomain', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let errors;
    let requireDomain;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.EmailInput', 'SpecHelper');

        angular.mock.inject([
            '$injector',

            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
            },
        ]);

        requireDomain = 'example.com';
    });

    function render(options) {
        const model = '';
        renderer = SpecHelper.renderer();

        const mockForm = {
            ngModel: model,
        };

        errors = {};
        renderer.scope.ngModel = mockForm.ngModel;
        renderer.scope.options = {
            domain: requireDomain,
            errors,
        };
        _.extend(renderer.scope.options, options);
        renderer.render(
            '<form name="spec_form"><input type="text" name="email" require-domain="options" ng-model="ngModel"></input></form>',
        );
        elem = renderer.elem;
    }

    afterEach(() => {
        SpecHelper.cleanup();
    });

    it('should be valid only when the domain entered matches the required domain', () => {
        render();
        SpecHelper.updateTextInput(elem, 'input', 'someone');
        expect(errors).toEqual({
            email: `Please enter an email address with the domain @${requireDomain}`,
        });
        SpecHelper.updateTextInput(elem, 'input', 'someone@example');
        expect(errors).toEqual({
            email: `Please enter an email address with the domain @${requireDomain}`,
        });
        SpecHelper.updateTextInput(elem, 'input', 'someone@pedago.com');
        expect(errors).toEqual({
            email: `Please enter an email address with the domain @${requireDomain}`,
        });
        SpecHelper.updateTextInput(elem, 'input', 'someone@example.com');
        expect(errors).toEqual({});
    });
});
