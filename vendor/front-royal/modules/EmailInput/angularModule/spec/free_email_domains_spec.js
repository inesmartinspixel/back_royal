import 'AngularSpecHelper';
import 'EmailInput/angularModule';

describe('FrontRoyal.EmailInput.freeEmailDomains', () => {
    let SpecHelper;
    let freeEmailDomains;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.EmailInput', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                SpecHelper = $injector.get('SpecHelper');
                freeEmailDomains = $injector.get('freeEmailDomains');
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    it('should return false if not a free email domain', () => {
        expect(freeEmailDomains.isFreeEmail('someone@somecompany.com')).toBe(false);
    });

    it('should return false if input is empty', () => {
        expect(freeEmailDomains.isFreeEmail('')).toBe(false);
    });

    it('should return false if input is null', () => {
        expect(freeEmailDomains.isFreeEmail(null)).toBe(false);
    });

    it('should return true with a known domain', () => {
        expect(freeEmailDomains.isFreeEmail('someone@gmail.com')).toBe(true);
    });

    it('should return true with a known domain with a wildcard', () => {
        expect(freeEmailDomains.isFreeEmail('someone@aol.anything.com')).toBe(true);
    });
});
