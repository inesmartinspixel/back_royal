import 'Injector/angularModule';
import 'Onetrust/angularModule';

export default angular.module('segmentio', ['onetrustCookieHelper', 'Injector']);
