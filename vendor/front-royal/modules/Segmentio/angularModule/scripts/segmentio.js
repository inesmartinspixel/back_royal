import angularModule from 'Segmentio/angularModule/scripts/segmentio_module';
import { QUANTIC_DOMAIN, SMARTLY_DOMAIN, QUANTIC_DOMAIN_STAGING, SMARTLY_DOMAIN_STAGING } from 'AppBrandMixin';

/* eslint-disable */

angularModule.factory('segmentio', [
    '$injector',
    $injector => {
        const onetrustCookieHelper = $injector.get('onetrustCookieHelper');
        const $window = $injector.get('$window');
        const injector = $injector.get('injector');
        const linkedDomains = [QUANTIC_DOMAIN, SMARTLY_DOMAIN, QUANTIC_DOMAIN_STAGING, SMARTLY_DOMAIN_STAGING];
        let injected = false;

        // Create a wrapper around the Segmentio interface
        const segmentio = {};

        // This snippet is copied direct from the segmentio
        // docs.  We pass in a fake stubWindow object because,
        // instead of relying on a global, like segmentio suggests
        // here, we want to rely on angular's DI system,
        const snippet = () => {
            // Create a queue, but don't obliterate an existing one!
            const analytics = (window.analytics = window.analytics || []);

            // If the real analytics.js is already on the page return.
            if (analytics.initialize) return;

            // If the snippet was invoked already show an error.
            if (analytics.invoked) {
                if (window.console && console.error) {
                    console.error('Segment snippet included twice.');
                }
                return;
            }

            // Invoked flag, to make sure the snippet
            // is never invoked twice.
            analytics.invoked = true;

            // A list of the methods in Analytics.js to stub.
            analytics.methods = [
                'trackSubmit',
                'trackClick',
                'trackLink',
                'trackForm',
                'pageview',
                'identify',
                'group',
                'track',
                'ready',
                'alias',
                'page',
                'once',
                'off',
                'on',
            ];

            // Define a factory to create stubs. These are placeholders
            // for methods in Analytics.js so that you never have to wait
            // for it to load to actually record data. The `method` is
            // stored as the first argument, so we can replay the data.
            analytics.factory = method =>
                function () {
                    const args = Array.prototype.slice.call(arguments);
                    args.unshift(method);
                    analytics.push(args);
                    return analytics;
                };

            // For each of our methods, generate a queueing stub.
            for (const key of analytics.methods) {
                analytics[key] = analytics.factory(key);
            }

            // Add a version to keep track of what's in the wild.
            analytics.SNIPPET_VERSION = '3.0.1';
        };

        // In tests, this will be included over and over.  Breaking
        // DI, but segment does not give us a clean way around that,
        // since they shove analytics on the window once the
        // script is loaded
        if (!window.analytics) {
            snippet();
        }

        // Define a method to load Analytics.js from our CDN,
        // and that will be sure to only ever load it once.
        segmentio.load = key => {
            // defer this injection to prevent circular dependency
            const offlineModeManager = injector.get('offlineModeManager', { optional: true });
            if (offlineModeManager?.inOfflineMode) {
                return;
            }

            // the tag may have been injected inline
            if ($('[segmentio-include-tag]').length > 0) {
                injected = true;
            }

            if (injected) {
                return;
            }
            if (!key) {
                return;
            }
            // Create an async script element based on your key.
            const script = document.createElement('script');
            script.type = 'text/javascript';
            script.async = true;
            script.src = `${
                'https:' === document.location.protocol ? 'https://' : 'http://'
            }cdn.segment.com/analytics.js/v1/${key}/analytics.min.js`;

            // Insert our script next to the first script element.
            const first = document.getElementsByTagName('script')[0];
            first.parentNode.insertBefore(script, first);
            injected = true;
        };

        // enable cross-domain tracking. See
        // * https://segment.com/docs/connections/destinations/catalog/google-analytics/#cross-domain-tracking
        // * https://developers.google.com/analytics/devguides/collection/gtagjs/cross-domain#using_a_single_snippet_on_all_domains
        analytics.ready(function () {
            ga('require', 'linker');
            ga('linker:autoLink', linkedDomains);
        });

        // Note: we put this on the object for mocking purposes
        segmentio.onetrustCookieHelper = onetrustCookieHelper;

        // Helper to set up a method that wraps a segmentio method
        function wrapSegmentMethod(segmentMethodName, publicMethodName) {
            if (!publicMethodName) {
                publicMethodName = segmentMethodName;
            }

            Object.defineProperty(segmentio, publicMethodName, {
                get() {
                    return window.analytics[segmentMethodName];
                },

                // the setter is just implemented to allow jest.spyOn to work in tests
                set(val) {
                    window.analytics[segmentMethodName] = val;
                },
            });
        }

        // Helper to merge in disabled integrations due to OneTrust preferences
        function enforceOneTrustIntegrations(options) {
            return angular.merge(options || {}, {
                integrations: segmentio.onetrustCookieHelper.getDisabledIntegrationsObject(),
            });
        }

        // https://segment.com/docs/sources/website/analytics.js
        // "An integrations object may be passed in the options of alias, group, identify, page and track methods"
        //
        // We want to automatically disable integrations based on on user preferences in OneTrust.
        // So, we wrap calls to these methods to automatically merge in integration overrides.
        //
        // To keep the ability to jest.spyOn calls to the original identify, track, etc. calls, we
        // we wrap the original calls but expose them with names prepended with underscores. The
        // standard method names (identify, track, etc.) simply proxy these methods.
        wrapSegmentMethod('identify', '__identify');
        segmentio.identify = (userId, traits, options, callback) => {
            segmentio.__identify(userId, traits || {}, enforceOneTrustIntegrations(options), callback);
        };
        wrapSegmentMethod('track', '__track');
        segmentio.track = (event, properties, options, callback) => {
            segmentio.__track(event, properties || {}, enforceOneTrustIntegrations(options), callback);
        };
        wrapSegmentMethod('page', '__page');
        segmentio.page = (category, name, properties, options, callback) => {
            segmentio.__page(category, name, properties || {}, enforceOneTrustIntegrations(options), callback);
        };
        wrapSegmentMethod('group', '__group');
        segmentio.group = (groupId, traits, options, callback) => {
            segmentio.__group(groupId, traits || {}, enforceOneTrustIntegrations(options), callback);
        };
        wrapSegmentMethod('alias', '__alias');
        segmentio.alias = (userId, previousId, options, callback) => {
            segmentio.__alias(userId, previousId, enforceOneTrustIntegrations(options), callback);
        };

        // for the rest of the segment methods, we do a simple pass-through
        ['trackLink', 'trackForm', 'trackClick', 'trackSubmit', 'pageview', 'ab', 'ready'].forEach(meth => {
            wrapSegmentMethod(meth);
        });

        return segmentio;
    },
]);

/* eslint-enable */
