import angularModule from 'Lessons/angularModule/scripts/lessons_module';
import { ensureUserProgressFetched, getProgressMaxUpdatedAt, fetchProgressForUser } from 'StoredProgress';

angularModule.factory('UserProgressLoader', [
    '$injector',
    $injector => {
        const SuperModel = $injector.get('SuperModel');
        const StreamProgress = $injector.get('Lesson.StreamProgress');
        const Stream = $injector.get('Lesson.Stream');
        const LessonProgress = $injector.get('LessonProgress');
        const $rootScope = $injector.get('$rootScope');
        const frontRoyalStore = $injector.get('frontRoyalStore');
        const $http = $injector.get('$http');
        const $q = $injector.get('$q');

        $rootScope.$watchGroup(['currentUser', () => frontRoyalStore.enabled], (newValues, oldValues) => {
            const [newUser, newEnabled] = newValues;
            const [oldUser, oldEnabled] = oldValues;
            if ((oldUser && oldUser !== newUser) || (angular.isDefined(newEnabled) && newEnabled !== oldEnabled)) {
                oldUser?.progress.destroy();
            }
        });

        const UserProgressLoader = SuperModel.subclass(() => ({
            usesFrontRoyalStore: true,

            initialize(user) {
                this.user = user;
            },

            async getAllProgress() {
                const response = await frontRoyalStore.retryAfterHandledError(db =>
                    ensureUserProgressFetched(this.user.id, db, $http),
                );
                return {
                    streamProgress: response.streamProgress.map(r => StreamProgress.new(r)),
                    lessonProgress: response.lessonProgress.map(r => LessonProgress.new(r)),
                    favoriteStreamsSet: response.favoriteStreamsSet,
                };
            },

            destroy() {
                this.destroyed = true;
            },

            clear() {
                // do nothing. While the legacy loader caches
                // data, this loader actually does not.  The data
                // is cached in the store, and this object is not
                // responsible for clearing the store. (The only thing
                // that would clear the store is User#clearAllProgress)
            },

            async onUnfetchedUpdatesDetected(progressMaxUpdatedAt) {
                return frontRoyalStore.retryAfterHandledError(db =>
                    fetchProgressForUser(this.user.id, db, $http, {
                        updated_since: progressMaxUpdatedAt,
                    }),
                );
            },

            onPlayerViewModelDestroyed() {
                // do nothing. see comment in player-view-model.js#destroy
                // to see why we only need to do this in the legacy case
            },

            setProgressMaxUpdatedAt() {
                // do nothing.  When getProgressMaxUpdatedAt is called, we always
                // pull it from the database
            },

            async getProgressMaxUpdatedAt() {
                return frontRoyalStore.retryAfterHandledError(db => getProgressMaxUpdatedAt(this.user.id, db));
            },

            // This is called when a stream is loaded up for user in the player.
            // The api call for the stream will include progress, but we want to
            // use the progress we have in the store.
            replaceProgress(stream) {
                // since we're swapping out progress, we don't want
                // to change a stream that someone else has already grabbed,
                // so we clone it
                stream = Stream.new(stream.asJson());
                return this._getProgressForReplace(stream).then(progress => {
                    const streamProgress = progress[stream.localePackId];

                    if (streamProgress) {
                        streamProgress.$$embeddedIn = stream;
                        stream.lesson_streams_progress = progress[stream.localePackId];
                    }

                    stream.lessons.forEach(lesson => {
                        const lessonProgress = progress[lesson.localePackId];
                        if (lessonProgress) {
                            lesson.lesson_progress = lessonProgress;
                            lessonProgress.$$embeddedIn = lesson;
                        }
                    });

                    return stream;
                });
            },

            async _getProgressForReplace(stream) {
                const result = {};
                const streamProgressAttrs = await frontRoyalStore.retryAfterHandledError(db =>
                    db.streamProgress
                        .where({ user_id: $rootScope.currentUser.id, locale_pack_id: stream.localePackId })
                        .first(),
                );
                if (streamProgressAttrs) {
                    result[streamProgressAttrs.locale_pack_id] = StreamProgress.new(streamProgressAttrs);
                }

                const lessonProgressKeys = stream.lessons.map(lesson => [
                    $rootScope.currentUser.id,
                    lesson.localePackId,
                ]);
                const lessonProgressAttrs = await frontRoyalStore.retryAfterHandledError(db =>
                    db.lessonProgress.where('[user_id+locale_pack_id]').anyOf(lessonProgressKeys).toArray(),
                );
                lessonProgressAttrs.forEach(attrs => {
                    // See https://sentry.io/organizations/pedago/issues/1515889842/?project=1491374&referrer=trello_plugin
                    if (!attrs) {
                        $injector.get('ErrorLogService').notifyInProd('How could attrs be falsy here?.', null, {
                            extra: {
                                lessonProgressAttrs,
                                lessonProgressKeys,
                            },
                        });
                    }
                    result[attrs.locale_pack_id] = LessonProgress.new(attrs);
                });

                return result;
            },
        }));

        // Note that this is currently only used to provide progress to the
        // student dashboard call.  When we load up a single stream with Stream.show
        // or all the streams on the library, we still embed the progress in the streams.
        // Eventually we would like to make this all consistent, and make sure we only have
        // one cache for all content and progress that is busted in a clear way.
        const LegacyUserProgressLoader = SuperModel.subclass(() => ({
            usesFrontRoyalStore: false,

            initialize(user) {
                const self = this;
                this.user = user;

                this._deregistrationFunctions = [
                    // The list of favorites is encoded in the response to the progress call.  If it
                    // changes (because a stream was auto-favorited when it was started, for example)
                    // then we need to reload.
                    //
                    // This is actually unnecessary in practice at the moment because:
                    // 1. See PlayerViewModel destroy callback. We always clear out the UserProgress cache
                    //    when leaving the player.
                    //
                    // Since that could change, we leave this here.
                    $rootScope.$watch(
                        () => user.favorite_lesson_stream_locale_packs,
                        (_new, _old) => {
                            // we don't want this to fire the first time; only when it subsequently changes
                            // if the UserProgress object was created and the load was called in the same
                            // digest, this watcher would fire after the load call, clearing out the cache
                            if (_new !== _old) {
                                self.clear();
                            }
                        },
                        true,
                    ), // deep watch since the whole collection can be replaced with a clone
                ];
            },

            destroy() {
                this.destroyed = true;
                _.each(this._deregistrationFunctions, fn => {
                    fn();
                });
            },

            getAllProgress() {
                const self = this;

                if (!self._streamProgressPromise) {
                    self._streamProgressPromise = StreamProgress.index({
                        filters: {
                            user_id: self.user.id,
                        },
                        include_lesson_progress: true,
                        include_favorite_streams: true,
                    }).then(response => ({
                        streamProgress: response.result,

                        // NOTE: we're not actually instantiating these as
                        // instance of LessonProgress.  Seems like a bug, but I'm not
                        // gonna mess with it now since this stuff is on the way out
                        lessonProgress: response.meta.lesson_progress,

                        // this is an object whose keys are locale pack ids
                        // and whose values are always `true`
                        favoriteStreamsSet: response.meta.favorite_streams_set,
                    }));
                }
                return self._streamProgressPromise;
            },

            setProgressMaxUpdatedAt(progressMaxUpdatedAt) {
                this.progressMaxUpdatedAt = progressMaxUpdatedAt;
            },

            getProgressMaxUpdatedAt() {
                return this.progressMaxUpdatedAt;
            },

            clear() {
                this._streamProgressPromise = null;
            },

            // see comment in player-view-model.js#destroy
            onPlayerViewModelDestroyed() {
                this.clear();
            },

            onUnfetchedUpdatesDetected() {
                this.clear();
            },

            // We could replace the stream from the cached progress
            // here, and it would make sense to do so.  But since this
            // is going away, I'm not messing with it now.
            replaceProgress(stream) {
                return $q.resolve(stream);
            },
        }));

        return {
            initialize(user, useFrontRoyalStore) {
                if (useFrontRoyalStore) {
                    return new UserProgressLoader(user);
                }
                return new LegacyUserProgressLoader(user);
            },
        };
    },
]);
