import angularModule from 'Lessons/angularModule/scripts/lessons_module';
import template from 'Lessons/angularModule/views/shared/lesson_progress_bar.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('lessonProgressBar', [
    '$injector',
    $injector => ({
        scope: {
            rowThreshold: '<',
            playerViewModel: '<',
        },

        restrict: 'E',
        templateUrl,

        link(scope) {
            function setIndicatorStates() {
                const playerViewModel = scope.playerViewModel;
                if (!playerViewModel) {
                    return;
                }

                const currentIndex = playerViewModel.activeFrameIndex;
                let highWaterMark = 0;
                for (let index = scope.indicators.length - 1; index >= 0; index--) {
                    const indicator = scope.indicators[index];

                    const frame = scope.playerViewModel.lesson.frames[index];
                    const completed = scope.playerViewModel.completedFrames[frame.id];

                    // if the current frame is the highest visited
                    // one, or any completed frame is shown as filled
                    if (index === currentIndex || completed) {
                        indicator.filledState = 'filled';
                    }

                    // a frame that is before the highest visited
                    // but that is not completed is shown as
                    // partially filled. (But the filled state can
                    // never go backwards from filled to partialy filled)
                    else if (index < highWaterMark && !completed && indicator.filledState !== 'filled') {
                        indicator.filledState = 'partially-filled';
                    }

                    if (index === currentIndex) {
                        indicator.canNavigate = false;
                    } else if (scope.playerViewModel.canNavigateFreely) {
                        indicator.canNavigate = true;
                    } else if (
                        indicator.filledState === 'filled' &&
                        scope.playerViewModel.canNavigateBackToCompletedFrames
                    ) {
                        indicator.canNavigate = true;
                    } else {
                        indicator.canNavigate = false;
                    }

                    if (!highWaterMark && indicator.filledState === 'filled') {
                        highWaterMark = index;
                    }

                    // if the current frame is not the highest
                    // visited one, then it gets special styling
                    if (index === currentIndex && index < highWaterMark) {
                        indicator.activeHighlight = true;
                    } else {
                        indicator.activeHighlight = false;
                    }
                }
            }

            scope.navigateTo = (indicator, event) => {
                // remove frame history. otherwise the back
                // button gets really confusing as it jumps
                // back to the last frame you were on.
                scope.playerViewModel.frameHistory = [];
                scope.playerViewModel.gotoFrameIndex(indicator.index);
                event.stopPropagation();
            };

            scope.$watch('playerViewModel', playerViewModel => {
                scope.count = playerViewModel ? playerViewModel.frames.length : 0;

                const indicatorsByRow = [];
                const indicators = [];

                for (let i = 0; i < scope.count; i++) {
                    indicators.push({
                        index: i,
                        filledState: 'unfilled',
                        active: false,
                    });
                }

                if (indicators.length > scope.rowThreshold) {
                    const halfLength = Math.ceil(indicators.length / 2);
                    indicatorsByRow.push(indicators.slice(0, halfLength));
                    indicatorsByRow.push(indicators.slice(halfLength));
                } else {
                    indicatorsByRow.push(indicators.slice(0, indicators.length));
                }

                scope.indicatorsByRow = indicatorsByRow;
                scope.indicators = indicators;
                setIndicatorStates();
            });

            scope.rowClasses = row => {
                const classes = [];
                const rowIndex = scope.indicatorsByRow.indexOf(row);

                if (scope.count <= scope.rowThreshold) {
                    classes.push('single-row');
                }

                // we want to be able to rely on text centering because we don't
                // want to have to calculate the width of the indicator rows. this
                // offset lets us nudge things over if needed (even numbered 2nd row)
                if (rowIndex % 2 === 1) {
                    classes.push('row-even');

                    if (row.length === scope.count / 2) {
                        classes.push('row-offset');
                    }
                }

                return classes;
            };

            scope.$watch('playerViewModel.activeFrameIndex', () => {
                // wait half a second before updating fills in order
                // to line up with the frame transition.  This is a bit
                // hacky.  We could somehow tap in to the frame_visible
                // event, but it's a bit tricky to get access to that
                // from here.
                $injector.get('scopeTimeout')(scope, setIndicatorStates, 500);
            });
        },
    }),
]);
