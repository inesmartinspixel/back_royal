import angularModule from 'Lessons/angularModule/scripts/lessons_module';
import { getBrandEmail } from 'AppBrandMixin';

angularModule.factory('ContentAccessHelper', [
    '$injector',
    $injector => {
        const $rootScope = $injector.get('$rootScope');
        const Stream = $injector.get('Lesson.Stream');
        const Playlist = $injector.get('Playlist');
        const Lesson = $injector.get('Lesson');
        const Cohort = $injector.get('Cohort');
        const DialogModal = $injector.get('DialogModal');
        const $location = $injector.get('$location');
        const SuperModel = $injector.get('SuperModel');
        const TranslationHelper = $injector.get('TranslationHelper');
        const ErrorLogService = $injector.get('ErrorLogService');
        const $filter = $injector.get('$filter');
        const $sanitize = $injector.get('$sanitize');
        const ConfigFactory = $injector.get('ConfigFactory');

        const translationHelper = new TranslationHelper('lessons.shared.content_access_helper');

        const ContentAccessHelper = SuperModel.subclass(function () {
            this.extend({
                canLaunch(contentItem) {
                    return new ContentAccessHelper(contentItem).canLaunch;
                },

                // With streams and lessons, we show messaging about why a particular
                // content item is locked (for example, on a stream-link-box).  So those
                // messages are set on an instance below.  For playlists, however, we show a more
                // general message on the student dashboard about THIS USER's content is locked. So
                // that is set here in a class method.
                playlistsLockedReasonKey(user) {
                    let key;
                    if (user.academic_hold) {
                        key = 'reason_message_playlist_placed_on_academic_hold';
                    } else if (user.pastDueForIdVerification) {
                        key = 'reason_message_playlist_requires_id_verification';
                    } else {
                        // I'm not sure if you can get here without a relevant cohort, but just following
                        // the old logic that was here
                        key = user.relevant_cohort
                            ? user.relevant_cohort.playlistMapLockedKey
                            : Cohort.playlistMapLockedKey('mba');
                        if (user.isRejectedOrExpelledOrDeferredOrReApplied) {
                            key += '_rejected';
                        }
                    }
                    return key; // translated elsewhere with translate-compile because it may contain HTML
                },
            });

            Object.defineProperty(this.prototype, 'canLaunch', {
                get() {
                    const contentItem = this.contentItem;

                    if (this._canLaunch === undefined) {
                        if (!contentItem) {
                            this._canLaunch = false;
                        } else if (contentItem.isA(Playlist)) {
                            this._canLaunch = this._canLaunchPlaylist(contentItem);
                        } else if (contentItem.isA(Stream)) {
                            this._canLaunch = this._canLaunchStream(contentItem);
                        } else if (contentItem.isA(Lesson)) {
                            this._canLaunch = this._canLaunchLesson(contentItem);
                        } else {
                            throw new Error(
                                'Trying to launch a content item that is not a Lesson, Stream, or Playlist',
                            );
                        }
                    }

                    return this._canLaunch;
                },
                // specs
                configurable: true,
            });

            // Used on buttons in places like the keep learning section
            Object.defineProperty(this.prototype, 'launchText', {
                get() {
                    // we special case completed streams
                    const completedExam =
                        this.contentItem.isA(Stream) && this.contentItem.exam && this.contentItem.complete;
                    if (this.canLaunch && !completedExam) {
                        return this.contentItem.launchText;
                    }
                    if (this.contentItem.complete) {
                        return translationHelper.get('completed');
                    }
                    return this.translationHelper.get('locked');
                },
                configurable: true,
            });

            // Used in modals and tooltips to describe why the item is locked
            Object.defineProperty(this.prototype, 'reasonMessage', {
                get() {
                    if (this.canLaunch) {
                        return null;
                    }

                    // if this is a lesson, and it cannot be launched because
                    // of an issue with the stream, the show the message related
                    // to the stream
                    if (this._delegateReasonTo) {
                        return this._delegateReasonTo.reasonMessage;
                    }

                    if (!this._reasonMessage) {
                        this._reasonMessage = this._getReasonText('message');
                    }

                    return this._reasonMessage;
                },
                configurable: true,
            });

            // property that can be mocked in specs
            Object.defineProperty(this.prototype, 'reason', {
                get() {
                    // need to make sure 'canLaunch' has been triggered so that
                    // _reason will be set
                    if (this.canLaunch) {
                        return null;
                    }

                    // if this is a lesson, and it cannot be launched because
                    // of an issue with the stream, the show the reason related
                    // to the stream
                    if (this._delegateReasonTo) {
                        return this._delegateReasonTo.reason;
                    }
                    return this._reason;
                },
                configurable: true,
            });

            // used as the title of modals
            Object.defineProperty(this.prototype, 'reasonTitle', {
                get() {
                    if (this.canLaunch) {
                        return null;
                    }

                    // if this is a lesson, and it cannot be launched because
                    // of an issue with the stream, the show the message related
                    // to the stream
                    if (this._delegateReasonTo) {
                        return this._delegateReasonTo.reasonTitle;
                    }

                    if (!this._reasonTitle) {
                        this._reasonTitle = this._getReasonText('title');
                    }

                    return this._reasonTitle;
                },
                configurable: true,
            });

            return {
                REASON_NO_USER: 'no_user',
                REASON_COMING_SOON: 'coming_soon',
                REASON_LOCKED: 'locked',
                REASON_UNMET_PREREQUISITES_SCHEDULE: 'unmet_prerequisites_schedule',
                REASON_UNMET_PREREQUISITES_PLAYLIST: 'unmet_prerequisites_playlist',
                REASON_UNMET_PREREQUISITES_CONCENTRATION: 'unmet_prerequisites_concentration',
                REASON_UNMET_PREREQUISITES_SPECIALIZATION: 'unmet_prerequisites_specialization',
                REASON_BEFORE_LAUNCH_WINDOW: 'before_launch_window',
                REASON_EXAM_TIME_RUN_OUT: 'exam_time_run_out',
                REASON_COMPLETED_TEST: 'completed_test',
                REASON_BEFORE_LAUNCH_WINDOW_AND_UNMET_PREREQUISITES_SCHEDULE:
                    'before_launch_window_and_unmet_prerequisites_schedule',
                REASON_BEFORE_LAUNCH_WINDOW_AND_UNMET_PREREQUISITES_CONCENTRATION:
                    'before_launch_window_and_unmet_prerequisites_concentration',
                REASON_BEFORE_LAUNCH_WINDOW_AND_UNMET_PREREQUISITES_SPECIALIZATION:
                    'before_launch_window_and_unmet_prerequisites_specialization',
                REASON_BEFORE_LAUNCH_WINDOW_AND_UNMET_PREREQUISITES_PLAYLIST:
                    'before_launch_window_and_unmet_prerequisites_playlist',
                REASON_REQUIRES_ID_VERIFICATION: 'requires_id_verification',
                REASON_ACADEMIC_HOLD: 'placed_on_academic_hold',

                initialize(contentItem, user) {
                    this.user = user || $rootScope.currentUser;
                    this.contentItem = contentItem;

                    // put it here so it is accessible for mocking in specs
                    this.translationHelper = translationHelper;

                    if (!contentItem) {
                        this.contentType = null;
                    } else if (contentItem.isA(Playlist)) {
                        this.contentType = 'playlist';
                    } else if (contentItem.isA(Stream)) {
                        this.contentType = 'stream';
                    } else if (contentItem.isA(Lesson)) {
                        this.contentType = 'lesson';
                    }
                },

                showModalIfCannotLaunch() {
                    if (!this.canLaunch) {
                        DialogModal.alert({
                            content: `<p class="message">${this.reasonMessage}</p><button class="modal-action-button" ng-click="close()">${this.reasonTitle}</button>`,
                            size: 'small',
                            hideCloseButton: true,
                            closeOnClick: true,
                            close() {
                                $location.url('/dashboard');
                            },
                            blurTargetSelector: 'div[ng-controller]',
                        });
                    }
                },

                // This one seems a bit odd, but in order to leave the stream dashboard
                // behavior as-is, it was necessary.  The stream dashboard is normally
                // accessible for streams that cannot be launched (though there is no
                // way to navigate to it, so we don't really expect people to see it).
                // In just the unmet prereq case though, we actually force people away
                // from it.  It's maybe odd and maybe accidental that we do that in the
                // prereq case but not the before examOpen case, but not a big deal either way.
                showModalIfPrereqsNotMet() {
                    if (!this._hasCompletedPrerequisites()) {
                        this.showModalIfCannotLaunch();
                    }
                },

                _canLaunchPlaylist() {
                    if (!this.user) {
                        this._reason = this.REASON_NO_USER;
                        return false;
                    }
                    if (this.user.inGroup('SUPERVIEWER')) {
                        return true;
                    }
                    if (this._isPlaylistLocked(this.contentItem)) {
                        this._reason = this.REASON_LOCKED;
                        return false;
                    }
                    if (this.user.academic_hold) {
                        this._reason = this.REASON_ACADEMIC_HOLD;
                        return false;
                    }
                    if (this.user.pastDueForIdVerification) {
                        this._reason = this.REASON_REQUIRES_ID_VERIFICATION;
                        return false;
                    }
                    return true;
                },

                _canLaunchStream() {
                    const stream = this.contentItem;
                    if (!this.user) {
                        this._reason = this.REASON_NO_USER;
                        return false;
                    }
                    if (this.user.inGroup('SUPERVIEWER')) {
                        return true;
                    }
                    if (this._isStreamLocked(stream)) {
                        this._reason = this.REASON_LOCKED;
                        return false;
                    }
                    // NOTE: if the time limit is run out or we are after the launch window,
                    // the stream dashboard can be opened, but the lessons within it cannot be
                    // (see canLaunchLesson below).
                    if (!stream.complete && stream.beforeLaunchWindow(this.user)) {
                        this._reason = this.REASON_BEFORE_LAUNCH_WINDOW;
                        if (!this._hasCompletedPrerequisitesWhenSchedule()) {
                            this._reason = this.REASON_BEFORE_LAUNCH_WINDOW_AND_UNMET_PREREQUISITES_SCHEDULE;
                        } else if (!this._hasCompletedPrerequisitesWhenPlaylist('concentration')) {
                            this._reason = this.REASON_BEFORE_LAUNCH_WINDOW_AND_UNMET_PREREQUISITES_CONCENTRATION;
                        } else if (!this._hasCompletedPrerequisitesWhenPlaylist('specialization')) {
                            this._reason = this.REASON_BEFORE_LAUNCH_WINDOW_AND_UNMET_PREREQUISITES_SPECIALIZATION;
                        }
                        return false;
                    }
                    if (!stream.complete && !this._hasCompletedPrerequisitesWhenSchedule()) {
                        this._reason = this.REASON_UNMET_PREREQUISITES_SCHEDULE;
                        return false;
                    }
                    if (!stream.complete && !this._hasCompletedPrerequisitesWhenPlaylist('concentration')) {
                        this._reason = this.REASON_UNMET_PREREQUISITES_CONCENTRATION;
                        return false;
                    }
                    if (!stream.complete && !this._hasCompletedPrerequisitesWhenPlaylist('specialization')) {
                        this._reason = this.REASON_UNMET_PREREQUISITES_SPECIALIZATION;
                        return false;
                    }
                    if (stream.coming_soon) {
                        this._reason = this.REASON_COMING_SOON;
                        return false;
                    }
                    if (this.user.academic_hold) {
                        this._reason = this.REASON_ACADEMIC_HOLD;
                        return false;
                    }
                    if (this.user.pastDueForIdVerification) {
                        this._reason = this.REASON_REQUIRES_ID_VERIFICATION;
                        return false;
                    }
                    return true;
                },

                _canLaunchLesson() {
                    const lesson = this.contentItem;

                    if (lesson.stream()) {
                        const streamContentAccessHelper = new ContentAccessHelper(lesson.stream(), this.user);
                        if (!streamContentAccessHelper.canLaunch) {
                            this._delegateReasonTo = streamContentAccessHelper;
                            this._canLaunch = false;
                            return false;
                        }
                    }

                    if (this.user && this.user.inGroup('SUPERVIEWER')) {
                        return true;
                    }
                    if (lesson.comingSoon) {
                        this._reason = this.REASON_COMING_SOON;
                        return false;
                    }
                    if (lesson.unrestricted) {
                        return true;
                    }
                    if (!this.user) {
                        this._reason = this.REASON_NO_USER;
                        return false;
                    }
                    if (lesson.completedTest) {
                        this._reason = this.REASON_COMPLETED_TEST;
                        return false;
                    }
                    if (lesson.stream() && lesson.stream().msLeftInTimeLimit === 0) {
                        this._reason = this.REASON_EXAM_TIME_RUN_OUT;
                        return false;
                    }
                    return true;
                },

                /*
                 *  Determines if the playlist is locked or unlocked depending on the state of the user (if present).
                 *  If the user is not accepted, the first playlist in the user's relevant_cohort should always be
                 *  unlocked (the first playlist should be the Foundations playlist).
                 *  If the user is accepted, they should have access to everything, unless the user lockedDueToPastDuePayment,
                 *  in which case they should fallback to only have access to the Foundations playlist.
                 */
                _isPlaylistLocked() {
                    const playlist = this.contentItem;

                    if (!this.user) {
                        return true;
                    }
                    // always unlock the first playlist in the user's relevant_cohort
                    if (
                        this.user.relevant_cohort &&
                        _.indexOf(this.user.relevant_cohort.playlistPackIds, playlist.localePackId) === 0
                    ) {
                        return false;
                    }
                    // Content is not locked if lockable flag is set to false
                    if (!this._mbaContentLocked()) {
                        return false;
                    }

                    // Otherwise assume locked
                    return true;
                },

                /*
                 *  Determines if the stream is locked or unlocked depending on the state of the user (if present).
                 *  If the user is not accepted, streams that meet the following criteria should be unlocked:
                 *
                 *      1. streams specifically marked as unlocked_for_mba_users
                 *      2. streams associated with the user's access groups
                 *      3. the first stream in the first playlist of the user's relevant_cohort (first playlist should be the Foundations playlist)
                 *
                 *  If the user is accepted, they should have access to everything, unless the user lockedDueToPastDuePayment,
                 *  in which case they should fallback to only have access to the streams that meet the criteria above
                 *  until they've successfully provided payment.
                 */
                _isStreamLocked() {
                    const stream = this.contentItem;

                    if (!this.user) {
                        return true;
                    }
                    // A stream explicitly marked as unlocked is not locked
                    if (stream.unlocked_for_mba_users) {
                        return false;
                    }
                    // unlock any stream that is in a group associated with the user
                    if (this.user.streamIsAssociatedWithAccessGroup(stream.localePackId)) {
                        return false;
                    }
                    // always unlock the stream if it's the first stream in the first playlist
                    // of the user's relevant_cohort (the foundations playlist)
                    if (
                        this.user.relevant_cohort &&
                        this.user.relevant_cohort.isFirstStreamInFoundationsPlaylist(stream.localePackId)
                    ) {
                        return false;
                    }
                    if (!this._mbaContentLocked()) {
                        return false;
                    }

                    // Otherwise assume locked
                    return true;
                },

                _hasCompletedPrerequisites() {
                    return (
                        this._hasCompletedPrerequisitesWhenSchedule() && this._hasCompletedPrerequisitesWhenPlaylist()
                    );
                },

                _hasCompletedPrerequisitesWhenPlaylist(type) {
                    if (!this._shouldCheckPrerequisites()) {
                        return true;
                    }

                    const stream = this.contentItem;
                    const cohort = this.user.relevant_cohort;
                    let cohortPackIds = cohort.playlistPackIds;
                    if (type === 'specialization') {
                        cohortPackIds = cohort.specialization_playlist_pack_ids;
                    } else if (type === 'concentration') {
                        cohortPackIds = cohort.requiredPlaylistPackIds;
                    }
                    let requiredStreamLocalePackIds = [];

                    // Check if the exam is in a playlist
                    _.each(cohortPackIds, playlistPackId => {
                        const playlist = Playlist.getCachedForLocalePackId(playlistPackId, false);

                        // It's possible that one of the locale pack ids in specialization_playlist_pack_ids or
                        // requiredPlaylistPackIds corresponds to an unpublished playlist.  If that's the
                        // case, then the client should behave as though that playlist does not exist at all
                        // and is not attached to the cohort, so we can skip right over it.
                        // see https://trello.com/c/7OkCSP4I
                        if (!playlist) {
                            return;
                        }

                        const streamEntryLocalePackIds = _.pluck(playlist.stream_entries, 'locale_pack_id');

                        // Find the position of this stream in the playlist (if it's there at all)
                        const examEntryIndex = streamEntryLocalePackIds.indexOf(stream.localePackId);

                        // Go from the first stream entry up to (not inclusive of) the entry that is the exam.
                        if (examEntryIndex > 0) {
                            const packIdsBeforeThisOneInPlaylist = streamEntryLocalePackIds.slice(0, examEntryIndex);
                            requiredStreamLocalePackIds = requiredStreamLocalePackIds.concat(
                                packIdsBeforeThisOneInPlaylist,
                            );
                        }
                    });
                    return this._hasCompletedAllStreamsAndStartedExams(requiredStreamLocalePackIds);
                },

                _hasCompletedPrerequisitesWhenSchedule() {
                    if (!this._shouldCheckPrerequisites()) {
                        return true;
                    }

                    const stream = this.contentItem;
                    const cohort = this.user.relevant_cohort;

                    // Check if the exam is in a period
                    const examPeriodIndex = _.findIndex(cohort.periods, period =>
                        _.contains(period.requiredStreamLocalePackIds, stream.localePackId),
                    );

                    // Go from the first period up to (not inclusive of) the period that contains the exam.
                    // (The method here handles the case when the exam is not found in a period)
                    const requiredStreamLocalePackIds = cohort.getRequiredStreamPackIdsFromPeriods(examPeriodIndex - 1);
                    return this._hasCompletedAllStreamsAndStartedExams(requiredStreamLocalePackIds);
                },

                // Determines if the user has completed all required steams contained in the streamLocalePackIds param.
                // If the stream is an exam, it only checks if the exam has been started. This avoids creating situations
                // where the user has to take an exam, but can't because they haven't finished an exam required previously
                // in the schedule (see https://trello.com/c/J0GP65Qu).
                _hasCompletedAllStreamsAndStartedExams(streamLocalePackIds = []) {
                    // Search for a stream that's incomplete or is an exam that hasn't been started yet
                    const incompleteStreamOrUnstartedExam = _.detect(streamLocalePackIds, streamLocalePackId => {
                        const stream = Stream.getCachedForLocalePackId(streamLocalePackId);
                        // we only check if the stream has been started if the stream is an exam;
                        // otherwise we check if the stream has been completed
                        return stream.exam ? stream.notStarted : !stream.complete;
                    });
                    return !incompleteStreamOrUnstartedExam;
                },

                _shouldCheckPrerequisites() {
                    const stream = this.contentItem;

                    // only users in a cohort have prereqs
                    if (!this.user || !this.user.relevant_cohort) {
                        return false;
                    }

                    // only exams have prereqs
                    if (!stream.exam) {
                        return false;
                    }

                    // prereqs can be turned off with exam locking
                    if (
                        this.user.acceptedCohortApplication &&
                        this.user.acceptedCohortApplication.disable_exam_locking
                    ) {
                        return false;
                    }

                    return true;
                },

                _mbaContentLocked() {
                    if (!this.user) {
                        return true;
                    }
                    // We want to lock content for users where the lockedDueToPastDuePayment flag is true because they
                    // need to provide payment, so this takes precedence over _mbaContentLocked.
                    if (this.user.lockedDueToPastDuePayment) {
                        return true;
                    }

                    // Tricky: the mba_content_lockable flag only applies to some cohort types
                    // The idea here is that some cohorts allow us to use the mba_content_lockable flag to unlock all content.
                    // We use this primarily for legacy users that registered back in early 2016 before we had a notion of locking courses.
                    // We didn't want to take away their content when we turned on locking, so this gave us the flexibility to unlock things for them.
                    // Furthermore, we occasionally use unlocking for demo accounts (but this could likely go away in favor of builing up the demo
                    // program type experience).
                    // However, for a shorter program (e.g.: the business certificate) that also include auto-grading when you complete the entire program,
                    // we don't want to give away all the content, even for legacy users. In this case, we've decided that it's been long enough that if you
                    // return to the product and apply to the business certificate, we would prefer to honor the locking rules over continuing to honor
                    // your legacy status in the product.
                    const supportsUnlockingWithoutAcceptance =
                        !this.user.relevant_cohort ||
                        Cohort.supportsUnlockingWithoutAcceptance(this.user.programType) ||
                        this.user.programType === 'demo';

                    if (!this.user.mba_content_lockable && supportsUnlockingWithoutAcceptance) {
                        return false;
                    }

                    if (this.user.acceptedCohortApplication) {
                        return false;
                    }

                    return true;
                },

                _notifyOnMissingLaunchReason() {
                    if (this._alreadyNotifiedOnMissingLaunchReason) {
                        return;
                    }
                    this._alreadyNotifiedOnMissingLaunchReason = true;

                    ErrorLogService.notify('reason not set.', {
                        userId: this.user && this.user.id,
                        contentType: this.contentType,
                        contentItemId: this.contentItem.id,
                    });
                },

                _getReasonText(textType) {
                    // Text will use a key like
                    // reason_message_lesson_locked or reason_title_stream_unmet_prerequisites
                    let key;
                    const defaultKey = ['reason', textType, this.contentType, 'default'].join('_');
                    if (!this._reason) {
                        this._notifyOnMissingLaunchReason();
                        key = defaultKey;
                    } else {
                        key = ['reason', textType, this.contentType, this._reason].join('_');
                    }

                    const examOpenTime = this.contentItem.examOpenTime && this.contentItem.examOpenTime(this.user);

                    // try to translate the key.  If it is not found, translationHelper
                    // will notify the ErrorLogService and fall back to the default locked message
                    //
                    // NOTE: we need to return an HTML-safe string here, because some of these
                    // locales contain html.
                    return $sanitize(
                        this.translationHelper.getWithFallbackKey(key, defaultKey, {
                            // any params that will be needed for any of these translations
                            // should be included here
                            openTimeStr: $filter('amDateFormat')(examOpenTime, 'MMMM D, YYYY'),
                            brandEmail: getBrandEmail(this.user, 'support', ConfigFactory.getSync()),
                        }),
                    );
                },
            };
        });
        return ContentAccessHelper;
    },
]);
