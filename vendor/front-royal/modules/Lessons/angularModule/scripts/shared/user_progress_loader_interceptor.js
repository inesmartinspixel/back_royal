import angularModule from 'Lessons/angularModule/scripts/lessons_module';

angularModule.factory('UserProgressLoaderInterceptor', [
    '$injector',

    $injector => {
        async function setProgressMaxUpdatedAt(response) {
            const currentUser = $injector.get('$rootScope').currentUser;
            if (!currentUser) {
                return;
            }
            const userProgressLoader = currentUser.progress;
            const userProgressLoaderProgressMaxUpdatedAt = await userProgressLoader.getProgressMaxUpdatedAt();

            let progressMaxUpdatedAtBeforeChanges;
            let serverProgressMaxUpdatedAt;

            try {
                // On api requests that update progress, both of these will be
                // set in the meta. On pings, only the second one will be set (since in that
                // case we know the api request did not create any changes)
                progressMaxUpdatedAtBeforeChanges =
                    response.data.meta.push_messages.progress_max_updated_at_before_changes;
                serverProgressMaxUpdatedAt = response.data.meta.push_messages.progress_max_updated_at;

                if (serverProgressMaxUpdatedAt && !progressMaxUpdatedAtBeforeChanges) {
                    progressMaxUpdatedAtBeforeChanges = serverProgressMaxUpdatedAt;
                }
            } catch (e) {
                // eslint-disable-next-line no-empty
            }

            // if the server says that there is progress later than the last progress we
            // know about, then clear the cache.  Otherwise, just update progressMaxUpdatedAt
            if (
                angular.isDefined(progressMaxUpdatedAtBeforeChanges) &&
                angular.isDefined(userProgressLoaderProgressMaxUpdatedAt) &&
                progressMaxUpdatedAtBeforeChanges > userProgressLoaderProgressMaxUpdatedAt
            ) {
                userProgressLoader.setProgressMaxUpdatedAt(undefined);
                userProgressLoader.onUnfetchedUpdatesDetected(userProgressLoaderProgressMaxUpdatedAt);
            } else if (angular.isDefined(serverProgressMaxUpdatedAt)) {
                userProgressLoader.setProgressMaxUpdatedAt(serverProgressMaxUpdatedAt);
            }

            return response;
        }

        return {
            response(response) {
                // Some requests cannot handle async responses, so only
                // await the interceptor if this is an api request with push
                // messages in it.
                try {
                    // eslint-disable-next-line babel/no-unused-expressions
                    response.data.meta.push_messages;
                } catch (err) {
                    return response;
                }

                return setProgressMaxUpdatedAt(response).then(() => response);
            },
        };
    },
]);

angularModule.config([
    '$httpProvider',
    $httpProvider => {
        $httpProvider.interceptors.push('UserProgressLoaderInterceptor');
    },
]);
