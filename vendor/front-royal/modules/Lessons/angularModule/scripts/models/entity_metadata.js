import angularModule from 'Lessons/angularModule/scripts/lessons_module';

angular.module('FrontRoyal.EntityMetadata', []).factory('EntityMetadata', [
    'Iguana',
    Iguana =>
        Iguana.subclass(function () {
            this.setCollection('entity_metadata');
            this.alias('EntityMetadata');
            this.embeddedIn('stream');
            this.embeddedIn('lesson');
            this.setIdProperty('id'); // todo: eventually we should only publically expose guids instead of sequential IDs

            return {
                imageUploadUrl() {
                    return `${window.ENDPOINT_ROOT}/api/entity_metadata/${this.id}/images.json`;
                },
                imageSrc() {
                    return (
                        this.image &&
                        this.image.formats &&
                        this.image.formats.original &&
                        this.image.formats.original.url
                    );
                },
            };
        }),
]);
