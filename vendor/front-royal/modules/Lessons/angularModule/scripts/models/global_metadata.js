import angularModule from 'Lessons/angularModule/scripts/lessons_module';

angular.module('FrontRoyal.GlobalMetadata', []).factory('GlobalMetadata', [
    'Iguana',
    Iguana =>
        Iguana.subclass(function () {
            this.setCollection('global_metadata');
            this.alias('GlobalMetadata');
            this.setIdProperty('id'); // todo: eventually we should only publically expose guids instead of sequential IDs

            return {
                imageSrc() {
                    return (
                        this.default_image &&
                        this.default_image.formats &&
                        this.default_image.formats.original &&
                        this.default_image.formats.original.url
                    );
                },

                imageUploadUrl() {
                    return `${window.ENDPOINT_ROOT}/api/global_metadata/${this.id}/images.json`;
                },
            };
        }),
]);
