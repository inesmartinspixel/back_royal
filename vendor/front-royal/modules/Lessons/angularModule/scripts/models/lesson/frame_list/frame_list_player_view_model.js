import angularModule from 'Lessons/angularModule/scripts/lessons_module';

angularModule.factory('Lesson.FrameList.FrameListPlayerViewModel', [
    '$injector',
    $injector => {
        const PlayerViewModel = $injector.get('Lesson.PlayerViewModel');
        const PlayerViewModelWithLesson = $injector.get('PlayerViewModelWithLesson');
        const $location = $injector.get('$location');
        const PlayerViewModelWithFrames = $injector.get('PlayerViewModelWithFrames');
        const $rootScope = $injector.get('$rootScope');
        const frontRoyalStore = $injector.get('frontRoyalStore');

        return PlayerViewModel.subclass(function () {
            this.include(PlayerViewModelWithLesson);
            this.include(PlayerViewModelWithFrames);

            //-------------------------------------------------
            // Callbacks defined in PlayerViewModel
            //-------------------------------------------------

            this.setCallback('after', 'initialized', function () {
                if (this.showStartScreen && this.lesson.frames[0]) {
                    this.preloadAssetsForFrame(this.lesson.frames[0]);
                } else if (!this.showStartScreen) {
                    this.setInitialActiveFrame();
                }
            });

            //-------------------------------------------------
            // Callbacks defined in PlayerViewModelWithLesson
            //-------------------------------------------------

            this.setCallback('after', 'showed_start_screen', function () {
                this.activeFrame = undefined;
            });

            this.setCallback('after', 'hid_start_screen', function () {
                // Normally, when we are on the start screen and we set
                // showStartScreen = false, there will be no active frame,
                // and we want to go to the initial one.  However, if the active
                // frame is set in some other way (like by clicking the progress
                // hexagons, then the new activeFrame will be set before setting
                // showStartScreen to false)
                if (!this.activeFrame) {
                    this.setInitialActiveFrame();
                }
            });

            this.setCallback('after', 'showed_finish_screen', function () {
                this.activeFrame = undefined;
            });

            //-------------------------------------------------
            // Callbacks defined in PlayerViewModelWithFrames
            //-------------------------------------------------
            this.setCallback('around', 'frame_started', function (startFrame) {
                if (!this.started) {
                    this.started = true;
                }
                // when switching frames, switch back to the default locale
                this.$$showingLanguage = this.lesson.locale;

                startFrame();
                this._saveBookmarkedFrame(this.activeFrame);
                $location.search('frame', this.activeFrame.id);
            });

            this.setCallback('after', 'frame_unloaded', function () {
                $location.search('frame', undefined);
                const activeSeconds = this._lastUnloadEvent.properties.duration_total;
                const limitedActiveSeconds = Math.min(activeSeconds, 120);
                this.frameDurations[this._lastUnloadEvent.properties.frame_id] =
                    this.frameDurations[this._lastUnloadEvent.properties.frame_id] || 0;
                this.frameDurations[this._lastUnloadEvent.properties.frame_id] += limitedActiveSeconds;
            });

            this.setCallback('after', 'frame_completed', function () {
                this._trackFrameChallengeStats();

                if (
                    this.activeFrame &&
                    this.activeFrame.nextFrame() &&
                    this.activeFrame.mainUiComponent.savesProgressOnComplete
                ) {
                    this._saveBookmarkedFrame(this.activeFrame.nextFrame());
                }

                // We used to do this when moving on to the
                // finish screen, but this way we can start the
                // last lesson progress save sooner, which is important
                // because we block the finish screen until that save happens
                if (!this.activeFrame.nextFrame() && !this.editorMode) {
                    this._finishSavingProgressThenMarkAsComplete();
                }
            });

            //-------------------------------------------------
            // Publicly Accessible Properties
            //-------------------------------------------------

            Object.defineProperty(this.prototype, 'activeFrameId', {
                get() {
                    return this.activeFrame && this.activeFrame.id;
                },
                set(val) {
                    const frame = this.lesson.frameForId(val);
                    this.activeFrame = frame;
                    return val;
                },
            });

            Object.defineProperty(this.prototype, 'frames', {
                get() {
                    return this.lesson.frames;
                },
            });

            Object.defineProperty(this.prototype, 'completedFrames', {
                get() {
                    if (this.logProgress) {
                        return this.lesson.ensureLessonProgress().completed_frames;
                    }

                    if (!this._completedFrames) {
                        this._completedFrames = {};
                    }

                    return this._completedFrames;
                },
            });

            Object.defineProperty(this.prototype, 'frameDurations', {
                get() {
                    if (this.logProgress) {
                        const lessonProgress = this.lesson.ensureLessonProgress();
                        lessonProgress.frame_durations = lessonProgress.frame_durations || {};
                        return lessonProgress.frame_durations;
                    }

                    if (!this._frameDurations) {
                        this._frameDurations = {};
                    }

                    return this._frameDurations;
                },
            });

            Object.defineProperty(this.prototype, 'challengeScores', {
                get() {
                    if (this.logProgress) {
                        const lessonProgress = this.lesson.ensureLessonProgress();
                        lessonProgress.challenge_scores = lessonProgress.challenge_scores || {};
                        return lessonProgress.challenge_scores;
                    }

                    if (!this._challengeScores) {
                        this._challengeScores = {};
                    }

                    return this._challengeScores;
                },
            });

            Object.defineProperty(this.prototype, 'frameHistory', {
                get() {
                    if (this.logProgress) {
                        const lessonProgress = this.lesson.ensureLessonProgress();
                        lessonProgress.frame_history = lessonProgress.frame_history || [];
                        return lessonProgress.frame_history;
                    }

                    if (!this._frame_history) {
                        this._frame_history = [];
                    }

                    return this._frame_history;
                },
                set(val) {
                    if (this.logProgress) {
                        this.lesson.ensureLessonProgress().frame_history = val;
                    } else {
                        this._frame_history = val;
                    }
                },
            });

            Object.defineProperty(this.prototype, 'frameBookmarkId', {
                get() {
                    if (this.logProgress) {
                        const lessonProgress = this.lesson.ensureLessonProgress();
                        return lessonProgress.frame_bookmark_id;
                    }

                    return this._frame_bookmark_id;
                },
                set(val) {
                    if (this.logProgress) {
                        this.lesson.ensureLessonProgress().frame_bookmark_id = val;
                    } else {
                        this._frame_bookmark_id = val;
                    }

                    if (this.scormMode) {
                        this.rpc.call('setBookmark', val);
                    }

                    return val;
                },
            });

            Object.defineProperty(this.prototype, 'shouldShowPreviousButton', {
                get() {
                    // If there is a stream, the back button can go
                    // to the stream dashboard, if there is a previous frame,
                    // the back button can go there
                    return (
                        !this.showStartScreen && ((!this.lesson.assessment && !this.lesson.test) || this.previewMode)
                    );
                },
            });

            Object.defineProperty(this.prototype, 'canNavigateFreely', {
                get() {
                    const canEdit = $rootScope.currentUser && $rootScope.currentUser.canEditLesson(this.lesson);
                    return canEdit;
                },
                configurable: true, // specs
            });

            Object.defineProperty(this.prototype, 'canNavigateBackToCompletedFrames', {
                get() {
                    const lesson = this.lesson;
                    const isAssessmentOrTest = lesson.assessment || lesson.test;
                    return this.canNavigateFreely || !isAssessmentOrTest;
                },
                configurable: true, // specs
            });

            Object.defineProperty(this.prototype, 'streamProgress', {
                get() {
                    return this.stream ? this.stream.ensureStreamProgress() : undefined;
                },
                configurable: true, // specs
            });

            return {
                //---------------------------------
                // Lifecycle
                //---------------------------------

                setInitialActiveFrame() {
                    // check for the start frame id first in the
                    // query params, then in lesson_progress.frame_bookmark_id
                    const lesson = this.lesson;
                    let startingFrameId;

                    // We only pay attention to the frame_bookmark_id in the querystring
                    // if this user is allowed to navigate back
                    if (this.canNavigateBackToCompletedFrames) {
                        const search = $location.search();
                        startingFrameId = search ? search.frame : undefined;
                    }

                    startingFrameId = startingFrameId || lesson.frame_bookmark_id;
                    if (startingFrameId && this.logProgress) {
                        lesson.tryToSetFrameBookmarkId(startingFrameId);
                    }

                    let initialFrame;
                    if (startingFrameId) {
                        // Be defensive.  Due to changes in content, the bookmarked
                        // frame may no longer exist.
                        initialFrame = lesson.frameForId(startingFrameId, true);
                    }

                    if (!initialFrame) {
                        initialFrame = lesson.frames[0];
                    }

                    if (initialFrame) {
                        this.gotoFrame(initialFrame);
                    }
                },

                //---------------------------------
                // Navigation
                //---------------------------------

                // goto next frame
                gotoNext() {
                    // progress to next frame or set completed to true if at last
                    if (this.activeFrame) {
                        const frameHistory = this.frameHistory;
                        const lastFrameIdInHistory = _.last(frameHistory);
                        if (lastFrameIdInHistory !== this.activeFrame.id) {
                            frameHistory.push(this.activeFrame.id);
                        }

                        if (this.activeFrame.nextFrame()) {
                            this.gotoFrame(this.activeFrame.nextFrame());
                        }

                        // Moving on from last frame, so
                        // - hide the frame
                        // - ensure progress is saved
                        // - marke the lesson as complete
                        // - show the finish screeen
                        //
                        // In most cases, _finishSavingProgressThenMarkAsComplete will
                        // already have been called when the last frame was finished
                        else if (!this.editorMode) {
                            this.activeFrame = undefined;
                            this._finishSavingProgressThenMarkAsComplete().then(() => {
                                // In the normal case, activeframe will still be undefined,
                                // but if the user used the hexagons at the top to
                                // navigate in the meantime, don't skip to the finish screen.
                                if (!this.activeFrame) {
                                    this.showFinishScreen = true;
                                }
                            });
                        }
                    }
                },

                gotoPrev() {
                    let prevFrameId;
                    if (this.frameHistory.length > 0) {
                        prevFrameId = this.frameHistory.pop();

                        // be defensive against the possibility that this
                        // frame has been removed from the lesson
                        if (!this.lesson.frameForId(prevFrameId, true)) {
                            prevFrameId = undefined;
                        }
                    }

                    const showingFinishScreen = this.showFinishScreen;
                    this.showFinishScreen = false;

                    if (prevFrameId) {
                        this.gotoFrameId(prevFrameId);
                    } else if (this.activeFrame && this.activeFrame.prevFrame()) {
                        this.gotoFrame(this.activeFrame.prevFrame());
                    } else if (showingFinishScreen) {
                        this.gotoFrameIndex(this.frames.length - 1);
                    } else {
                        this.showStartScreen = true;
                    }
                },

                //---------------------------------
                // Editing
                //---------------------------------

                swapFrame(oldFrame, newFrame) {
                    // use _activeFrame instead of activeFrame so it doesn't get set automatically
                    if (oldFrame === this._activeFrame) {
                        throw new Error(
                            'Bad things will happen if you try to swap out the active frame.  See editFrameInfoPanelDirBase.',
                        );
                    }
                    const index = oldFrame.index();
                    this.removeFrame(oldFrame);
                    this.lesson.addFrame(newFrame, index);
                },

                removeFrame(frame) {
                    if (this._activeFrame === frame) {
                        let nextFrame = frame.nextFrame() || this.frames[0];
                        if (nextFrame === frame) {
                            nextFrame = undefined;
                        }
                        if (nextFrame) {
                            // do not use gotoFrame.  Since this is the editor
                            // we just want to switch without worrying about preloading
                            this.activeFrame = nextFrame;
                        } else {
                            this.activeFrame = undefined;
                        }
                    }
                    this.lesson.removeFrame(frame);
                },

                removeFrames(frames) {
                    angular.forEach(
                        frames,
                        function (frame) {
                            this.lesson.removeFrame(frame);
                        },
                        this,
                    );
                },

                setChallengeScore(challengeViewModel) {
                    const score = challengeViewModel.getScore();

                    // score will not be set for challenges with no
                    // correct answer, and also for some challenges that are not
                    // yet complete.
                    if (typeof score !== 'number') {
                        return;
                    }

                    // FIXME: once we know challenge ids are unique, just
                    // use those. See https://trello.com/c/dQ6I2HnV/403-fix-ensure-unique-component-ids-and-fix-up-lesson-progress-challenge-scores
                    const key = `${challengeViewModel.frame.id},${challengeViewModel.model.id}`;
                    const currentScore = this.challengeScores[key];
                    const hasCurrentScore = typeof currentScore === 'number';

                    // On test and assessment lessons, we preserve the very first score the user enters.
                    // On regular lessons the score can change.
                    // (Note that on assessment lessons, even though the score does not change if you do
                    // a frame over again, it could get reset back to null after you finish the lesson)
                    // This logic is duplicated server-side in merge_completed_frames
                    const shouldUpdateScore =
                        (this.testOrAssessment && !hasCurrentScore) ||
                        (!this.testOrAssessment && score !== currentScore);

                    if (!shouldUpdateScore) {
                        return;
                    }

                    this.challengeScores[key] = score;

                    // To prevent cheating, we save progress on test and assessment lessons
                    // after every challenge.  To improve performance we don't do this on other
                    // lessons. (In that case progress is saved after each frame)
                    if (this.testOrAssessment && !(this.editorMode || !this.logProgress)) {
                        this._saveLessonAndStreamProgress();
                    }
                },

                //---------------------------------
                // Language switcher
                //---------------------------------

                swapLanguage() {
                    const self = this;

                    if (self.$$showingLanguage === 'en') {
                        self._showTextFrom(this.lesson);
                    } else {
                        self.swappingLanguage = true;
                        self._loadEnglishTranslation().then(response => {
                            const englishTranslation = response.result[0];
                            if (englishTranslation) {
                                self._showTextFrom(englishTranslation);
                            } else {
                                $injector.get('$window').alert('No English translation found.');
                            }
                            self.swappingLanguage = false;
                        });
                    }
                },

                _loadEnglishTranslation() {
                    if (!this.$$loadEnglishTranslationPromise) {
                        const FrameList = $injector.get('Lesson.FrameList');
                        this.$$loadEnglishTranslationPromise = FrameList.index({
                            filters: {
                                published: true,
                                locale_pack_id: this.lesson.locale_pack.id,
                                locale: 'en',
                            },
                            'fields[]': ['id', 'frames', 'locale'],
                        });
                    }

                    return this.$$loadEnglishTranslationPromise;
                },

                _showTextFrom(lesson) {
                    const self = this;
                    self.$$showingLanguage = lesson.locale;
                    self.$$origTextMap = self.$$origTextMap || {};
                    const getTextFromFrame = lesson.frames[self.activeFrameIndex];

                    if (!getTextFromFrame) {
                        return;
                    }

                    getTextFromFrame
                        .reify()
                        .componentsForType('Text.TextModel')
                        .forEach(getTextFromComponent => {
                            const component = self.activeFrame.getComponentById(getTextFromComponent.id);
                            if (component) {
                                // Once we've swapped languages once, we've replaced the text actually in
                                // the model (which is, admittedly, a bit hackish).  So we have to keep a map
                                // of the texts for each locale so we can switch back.
                                self.$$origTextMap[component.id] =
                                    self.$$origTextMap[component.id] || component.formatted_text;

                                let formattedText;
                                if (lesson === self.lesson) {
                                    formattedText = self.$$origTextMap[component.id];
                                } else {
                                    formattedText = getTextFromComponent.formatted_text;
                                }
                                component.formatted_text = formattedText;
                            }
                        });
                },

                _setChallengeScoresXOfY() {
                    const result = [0, 0];
                    angular.forEach(this.challengeScores, score => {
                        result[1] += 1;
                        if (score === 1) {
                            result[0] += 1;
                        } else if (score !== 0) {
                            throw new Error('Cannot calculate challengeScoresXOfY unless all scores are 0 or 1');
                        }
                    });
                    this.challengeScoresXOfY = result;
                },

                _setSecondsInLesson() {
                    let time = 0;
                    angular.forEach(this.frameDurations, seconds => {
                        time += seconds;
                    });
                    this.secondsInLesson = time;
                },

                _setLessonScore() {
                    // since challenge scores might be stored here
                    // in an instance variable or in lesson progress,
                    // we cannot rely on lesson.lessonScore
                    this.scoreMetadata = {};
                    this.lessonScore = this.lesson.getScore(this.challengeScores, this.scoreMetadata);
                },

                _markAsComplete() {
                    // see comment in PlayerViewModel._logLessonComplete about this
                    // being called more than once
                    if (this.completed) {
                        return;
                    }

                    this.frameBookmarkId = null;
                    this._setChallengeScoresXOfY();
                    this._setSecondsInLesson();
                    this._setLessonScore();
                    this.frameHistory = [];
                    this.completed = true;
                },

                // this handles BOTH of: marking a lesson as started, and saving the latest frame bookmark
                _saveBookmarkedFrame(newBookmarkedFrame) {
                    // do nothing if the frame bookmark is not changing
                    if (newBookmarkedFrame.id === this.frameBookmarkId) {
                        return;
                    }

                    // once a view model has been marked as complete, the user can
                    // still navigate back to previous frames, but we don't update
                    // the frame bookmark id.  See https://trello.com/c/epxiJOP7/932-bug-going-back-from-finish-screen-undoes-frame-bookmark-reset#
                    if (this.completed) {
                        return;
                    }

                    if (this.editorMode || !this.logProgress) {
                        return;
                    }

                    this.frameBookmarkId = newBookmarkedFrame.id;

                    // this might be called before a lesson start event ever happens,
                    // so lesson_bookmark_id might not be set yet.  The server requires it
                    const streamProgress = this.streamProgress;
                    if (streamProgress) {
                        streamProgress.lesson_bookmark_id = this.lesson.id;
                    }

                    return this._saveLessonAndStreamProgress();
                },

                // this handles tracking frames seen, challenges seen, and first answers for each challenge
                // should be called after an active frame has been completed and before it changes
                _trackFrameChallengeStats() {
                    const activeFrame = this.activeFrame;

                    // don't track stats in editor or if there's no activeFrame
                    if (!activeFrame || this.editorMode) {
                        return;
                    }

                    // use a hash so we don't double-count frames that are navigated to more than once
                    // this could happen due to a back button, or branching
                    this.completedFrames[activeFrame.id] = true;
                },

                _finishSavingProgressThenMarkAsComplete() {
                    // We want to wait for all challenges to be saved to the server
                    // before we calculate the score.  This is necessary for our
                    // cheating prevention to work correctly, since external changes
                    // might be pushed down from the server when we record the final challenge.
                    return (
                        this.finishSavingProgress()
                            .then(() => frontRoyalStore.flush())
                            // eslint-disable-next-line no-unused-vars
                            .then(flushed => {
                                // FIXME: if flushed is false here, then we can set
                                // some flag on the playerViewModel that will allows us to
                                // put a warning on the finish screen indicating that progress
                                // has not been saved to the server.  We may or may not want
                                // to enter offline mode at this point.  Might be fine to
                                // just wait for that to happen during navigation as we Normally
                                // do.  See https://trello.com/c/jJ56MlMM and also
                                // the comment in OfflineModeManager#resolveRoute
                                this._markAsComplete();
                            })
                    );
                },
            };
        });
    },
]);
