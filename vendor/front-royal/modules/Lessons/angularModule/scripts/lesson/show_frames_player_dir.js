import angularModule from 'Lessons/angularModule/scripts/lessons_module';
import template from 'Lessons/angularModule/views/lesson/show_frames_player.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('showFramesPlayer', [
    '$injector',
    function factory($injector) {
        const AppHeaderViewModel = $injector.get('Navigation.AppHeader.AppHeaderViewModel');
        const scopeTimeout = $injector.get('scopeTimeout');
        const $ocLazyLoad = $injector.get('$ocLazyLoad');
        const $timeout = $injector.get('$timeout');
        const $rootScope = $injector.get('$rootScope');
        const MessagingService = $injector.get('Lesson.MessagingService');

        return {
            restrict: 'E',
            templateUrl,
            link(scope, element) {
                AppHeaderViewModel.showAlternateHomeButton = true;

                scope.animate = () => element.data('$$ngAnimateState');
                scope.MessagingService = MessagingService;

                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                /*
                        indicates what should currently be on the screen.  Can be
                        frame, startScreen, finishScreen.  Is set to
                        undefined momentarily when transitioning from one to the next
                        in order to allow the animation.
                    */
                Object.defineProperty(scope, 'show', {
                    get() {
                        return scope.$show;
                    },
                    set(val) {
                        if (val === scope.$show) {
                            return;
                        }

                        scope.$show = val;
                    },
                });

                scope.backwardsClass = '';

                Object.defineProperty(scope, 'frameDirectiveName', {
                    get() {
                        if (this.show !== 'frame') {
                            return undefined;
                        }
                        return this.activeFrameViewModel && this.activeFrameViewModel.frame.directiveName;
                    },
                });

                Object.defineProperty(scope, 'buttonDirectiveName', {
                    get() {
                        if (this.show !== 'frame') {
                            return undefined;
                        }
                        return this.activeFrameViewModel && this.activeFrameViewModel.frame.buttonDirectiveName;
                    },
                });

                function preloadAssessmentEndScreenStyles() {
                    if (scope.playerViewModel.assessment) {
                        $ocLazyLoad.load(window.webpackManifest['assessment.css'].replace(/^\//, ''));
                    }
                }

                function updateAppHeader(val) {
                    const origColor = AppHeaderViewModel.bodyBackgroundColor;

                    if (val === 'frame') {
                        scope.activeFrameViewModel.setBackgroundColor();
                        AppHeaderViewModel.showMobileMessages = true;
                    } else {
                        let bgColor = val === 'finishScreen' ? 'beige-pattern' : 'white';
                        // special hack for demo mode: use a different background
                        if ((val === 'finishScreen' || val === 'startScreen') && scope.playerViewModel.demoMode) {
                            bgColor = 'demo-pattern';
                        }
                        scope.playerViewModel.setBodyBackground(bgColor);
                        AppHeaderViewModel.showMobileMessages = false;
                    }

                    const newColor = AppHeaderViewModel.bodyBackgroundColor;

                    AppHeaderViewModel.showFrameInstructions = !!val;

                    // the background color change is not animated,
                    // because animating background-image changes
                    // does not work well (I've tried it).  The change in
                    // the app header color is animated, though, so
                    // delay half a second
                    if (newColor !== origColor) {
                        return scopeTimeout(scope, 500);
                    }
                    return undefined;
                }

                function transition(action, delayBeforeRemovingContent, delayBeforeSettingNewContent) {
                    // Even if delayBeforeRemovingContent is 0, we want a timeout here,
                    // because, since the continue button is wrapped inside of a transcluded element,
                    // we need to add the backwards class first, and then wait a tick before kicking
                    // off the ng-if
                    let newShowValue;
                    let canceled = false;
                    scope.lastTransition = {
                        cancel() {
                            canceled = true;
                        },
                    };
                    scopeTimeout(scope, delayBeforeRemovingContent)
                        .then(() => {
                            if (canceled || scope.playerViewModel.destroyed) {
                                return;
                            }
                            // In desktop, we don't want to hide the message right away,
                            // because we want it to slide off the screen along with the frame.
                            AppHeaderViewModel.showMobileMessages = false;

                            scope.show = undefined;
                            return scopeTimeout(scope, delayBeforeSettingNewContent);
                        })
                        .then(() => {
                            if (canceled || scope.playerViewModel.destroyed) {
                                return;
                            }
                            newShowValue = action();

                            return updateAppHeader(newShowValue);
                        })
                        .then(() => {
                            if (canceled || scope.playerViewModel.destroyed) {
                                return;
                            }

                            scope.show = newShowValue;

                            // Reset any vertical scroll that might have occured
                            $injector.get('scrollHelper').scrollToTop();

                            // now that we've succeeded in getting something on the screen,
                            // go ahead and prep any assessment end screen styles
                            preloadAssessmentEndScreenStyles();
                        })
                        .catch(err => {
                            // if timeout has been cancelled (do to destroy) before transition completes
                            // this will suppress (useful for specs)
                            if (err !== 'canceled') {
                                throw err;
                            }
                        });
                }

                scope.onFrameVisible = elemScope => {
                    if (elemScope) {
                        elemScope.$emit('frame_visible');
                        elemScope.$broadcast('frame_visible');
                    }
                };

                // NOTE: with watchGroup, the second argument is not really what you think it would be.  It does not show the old values
                // of each of these things the past time this watch was run.  This seems to be working anyhow though, so I'm not gonna
                // mess with it.
                scope.$watchGroup(
                    [
                        'playerViewModel.activeFrameViewModel',
                        'playerViewModel.showStartScreen',
                        'playerViewModel.showFinishScreen',
                    ],
                    (newStuff, oldStuff) => {
                        scope.cancelSlowLoadTimeouts();
                        const newFrameViewModel = newStuff[0];
                        const oldFrameViewModel = oldStuff[0];

                        // if the new frame is before the old frame play in the list,
                        // add the backwards class so the transition will go backwards
                        if (scope.playerViewModel.showStartScreen && oldFrameViewModel) {
                            scope.backwardsClass = 'backwards';
                        } else if (scope.show === 'finishScreen' && newFrameViewModel) {
                            scope.backwardsClass = 'backwards';
                        } else if (scope.show === 'startScreen') {
                            scope.backwardsClass = '';
                        } else if (oldFrameViewModel && newFrameViewModel && oldFrameViewModel !== newFrameViewModel) {
                            scope.backwardsClass = newFrameViewModel.index < oldFrameViewModel.index ? 'backwards' : '';
                        } else {
                            // just leave it as it is.
                        }

                        let delayBeforeRemovingContent;
                        let delayBeforeSettingNewContent;
                        // For the bookends, we are hacking around ng-animate, so the
                        // animation happens before scope.show is set to undefined.  The
                        // animation lasts for 1 second

                        if (['startScreen', 'finishScreen'].includes(scope.show)) {
                            delayBeforeRemovingContent = 500;
                            delayBeforeSettingNewContent = 0;
                        }
                        // not a first page load
                        // For frames the transition lasts 500ms, after scope.show is set to undefined
                        else if (scope.$show) {
                            delayBeforeRemovingContent = 0;
                            delayBeforeSettingNewContent = 500;
                        }
                        // a first page load, not transitioning in from anything, so no need to delay
                        // before animations starts
                        else {
                            delayBeforeRemovingContent = 0;
                            delayBeforeSettingNewContent = 0;
                        }

                        if (scope.lastTransition) {
                            scope.lastTransition.cancel();
                        }
                        transition(
                            () => {
                                scope.cancelSlowLoadTimeouts();

                                // once the old frame is off the screen, clear messages, set background styles,
                                // and set the new frame and triggering its animation onto the screen
                                if (oldFrameViewModel && oldFrameViewModel.playerViewModel) {
                                    oldFrameViewModel.playerViewModel.clearMessage();
                                }

                                // If we're putting a new frame on the screen, clear messages as well,
                                // just in case the old frame never finished transitioning off.
                                // See https://trello.com/c/FvrAjwim/1033-bug-answer-messages-comes-back-after-branching-frame-related-to-practice-refactor#
                                if (newFrameViewModel && newFrameViewModel.playerViewModel) {
                                    newFrameViewModel.playerViewModel.clearMessage();
                                }

                                let targetShowValue;

                                scope.activeFrameViewModel = undefined;
                                if (scope.playerViewModel.showStartScreen) {
                                    targetShowValue = 'startScreen';
                                } else if (scope.playerViewModel.showFinishScreen) {
                                    targetShowValue = 'finishScreen';
                                } else if (newFrameViewModel) {
                                    scope.activeFrameViewModel = newFrameViewModel;
                                    targetShowValue = 'frame';
                                } else {
                                    scope.slowLoadTimeouts = [
                                        $timeout(scope.showSlowLoadingMessaging, 10 * 1000),
                                        $timeout(scope.showSlowLoadingExitButton, 20 * 1000),
                                    ];
                                    // targetShowValue remains undefined
                                }
                                return targetShowValue;
                            },
                            delayBeforeRemovingContent,
                            delayBeforeSettingNewContent,
                        );
                    },
                );

                scope.cancelSlowLoadTimeouts = () => {
                    scope.showSlowLoadingMessage = false;
                    scope.showExitButtonOnSlowLoading = false;
                    if (scope.slowLoadTimeouts) {
                        scope.slowLoadTimeouts.forEach(promise => {
                            $timeout.cancel(promise);
                        });
                    }
                };

                function logSlowLoadingEvent(eventName) {
                    scope.playerViewModel.log(eventName);
                }

                scope.showSlowLoadingMessaging = () => {
                    logSlowLoadingEvent('lesson:show_slow_loading_message');
                    scope.showSlowLoadingMessage = true;
                };

                scope.showSlowLoadingExitButton = () => {
                    logSlowLoadingEvent('lesson:show_slow_loading_exit');
                    scope.showExitButtonOnSlowLoading = true;
                };

                scope.exit = () => {
                    logSlowLoadingEvent('lesson:slow_loading_exit_clicked');
                    AppHeaderViewModel.exitButtonClick();
                };

                // stop propagation of the click event as well as the mousedown
                // and touchstart events, which are handled by provideHint below
                scope.stopPropagation = evt => {
                    evt.preventDefault();
                    evt.stopImmediatePropagation();
                };

                scope.$watch('playerViewModel', playerViewModel => {
                    playerViewModel.setBodyBackground('white');
                });

                scope.$watch('playerViewModel', playerViewModel => {
                    playerViewModel.started = true;
                });

                function onDestroy() {
                    if (scope.playerViewModel) {
                        scope.playerViewModel.destroy();
                    }
                }
                // We used to do this only on scope.$destroy, but then the event would get fired
                // after navigating to a different route when a user clicked the exit button.
                // This was not a big deal, but led to unexpected 'directive' property in the
                // frame:unload event.  See https://trello.com/c/9FFXCTfA/432-chore-investigate-unexpected-directive-notifications
                $rootScope.$on('$routeChangeStart', onDestroy);
                scope.$on('$destroy', onDestroy);
            },
        };
    },
]);
