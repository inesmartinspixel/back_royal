import angularModule from 'Lessons/angularModule/scripts/lessons_module';
import { setupBrandNameProperties, setupScopeProperties } from 'AppBrandMixin';
import template from 'Lessons/angularModule/views/lesson/lesson_finish_screen.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

import lessonFinishBrain from 'vectors/lesson_finish_brain.svg';
import summaryUnlocked from 'vectors/summary_unlocked.svg';
import progressBadgeCompleteTurquoise from 'images/progress_badge_complete_turquoise.png';
import shield from 'images/shield.png';
import shieldQuantic from 'images/shield_quantic.png';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('lessonFinishScreen', [
    '$injector',
    function factory($injector) {
        const lessonBookendMixin = $injector.get('lessonBookendMixin');
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
        const SummaryHelperMixin = $injector.get('Stream.SummaryHelperMixin');
        const TranslationHelper = $injector.get('TranslationHelper');
        const $rootScope = $injector.get('$rootScope');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                playerViewModel: '<',
            },
            link(scope, elem) {
                scope.lessonFinishBrain = lessonFinishBrain;
                scope.summaryUnlocked = summaryUnlocked;
                scope.progressBadgeCompleteTurquoise = progressBadgeCompleteTurquoise;

                NavigationHelperMixin.onLink(scope);
                SummaryHelperMixin.onLink(scope);
                lessonBookendMixin(scope, elem);
                setupBrandNameProperties($injector, scope);
                setupScopeProperties($injector, scope, [
                    {
                        prop: 'shieldImgSrc',
                        quantic: shieldQuantic,
                        fallback: shield,
                    },
                ]);

                const translationHelper = new TranslationHelper('lessons.lesson.lesson_finish_screen');

                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                scope.$watchGroup(['lesson', 'currentUser'], () => {
                    // prevent error on logout
                    if (!scope.currentUser) {
                        return;
                    }

                    if (scope.currentUser.testCompleteMessageKey) {
                        scope.testCompleteMessage =
                            scope.testCompleteMessage ||
                            translationHelper.get(scope.currentUser.testCompleteMessageKey);
                    }

                    scope.keyTerms = _.uniq(scope.lesson.getKeyTermsForDisplay());

                    if (scope.keyTerms.length === 0 || scope.lesson.assessment || scope.playerViewModel.demoMode) {
                        scope.startAnimation();
                    }
                });
            },
        };
    },
]);
