import angularModule from 'Lessons/angularModule/scripts/lessons_module';

angular
    .module('FrontRoyal.Lessons')
    .factory('Lesson.FrameList.Frame.Componentized.Component.Text.Behaviors.ProcessesInlineImages', [
        'AModuleAbove',
        'Lesson.FrameList.Frame.Componentized.Component.Image.ImageModel',
        '$q',
        (AModuleAbove, ImageModel, $q) => {
            // FIXME: these two functions are duplicated in ProcessesInlineImagesEditor
            function imagesByLabel(frame) {
                const map = {};
                frame.componentsForType(ImageModel).forEach(image => {
                    if (image.label) {
                        map[image.label] = image;
                    }
                });

                return map;
            }

            function searchTextForImages(text, fn) {
                return text.replace(/\!\[(.*?)\]/g, fn);
            }

            return new AModuleAbove({
                included(TextModel) {
                    TextModel.supportBehavior('ProcessesInlineImages');

                    TextModel.imageProcessors().push(function (processImage) {
                        const imageMap = imagesByLabel(this.frame());

                        const promises = [];
                        searchTextForImages(this.text, (ignoreMe, imageLabel) => {
                            const image = imageMap[imageLabel];
                            promises.push(processImage(image, 'inline'));
                        });

                        return $q.all(promises);
                    });
                },
            });
        },
    ]);
