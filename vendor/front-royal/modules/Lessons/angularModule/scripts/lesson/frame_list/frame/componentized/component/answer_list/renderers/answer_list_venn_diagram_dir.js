import angularModule from 'Lessons/angularModule/scripts/lessons_module';
import template from 'Lessons/angularModule/views/lesson/frame_list/frame/componentized/component/answer_list/answer_list_venn_diagram.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('cfAnswerListVennDiagram', [
    () => ({
        restrict: 'E',
        templateUrl,

        link(scope) {
            scope.vennDiagramAnswerClasses = answerViewModel => ['target', 'circle'].concat(answerViewModel.cssClasses);

            scope.movableCircleClasses = function () {
                const classes = ['movable', 'prompt', 'circle'];
                if (this.viewModel.currentlySelectedAnswerViewModel()) {
                    const horizontalPosition = ['left', 'center', 'right'][
                        this.viewModel.currentlySelectedAnswerIndex()
                    ];
                    classes.push(['middle', horizontalPosition].join(''));
                } else {
                    classes.push('bottomright');
                }

                return classes;
            };

            scope.vennDiagramMessageClasses = function () {
                const classes = ['message', 'bottom'];

                // answerViewModel.currentChallengeViewModel ?
                const position = ['points_up_left', 'points_up_center', 'points_up_right'][
                    this.viewModel.currentlySelectedAnswerIndex()
                ];

                classes.push(position);

                if (this.viewModel.currentlySelectedAnswerViewModel()) {
                    if (this.viewModel.currentChallengeViewModel.complete) {
                        classes.push('correct');
                    } else if (this.viewModel.currentChallengeViewModel.showingIncorrectStyling) {
                        classes.push('incorrect');
                    } else {
                        classes.push('unvalidated');
                    }
                }

                return classes;
            };
        },
    }),
]);
