import angularModule from 'Lessons/angularModule/scripts/lessons_module';
import template from 'Lessons/angularModule/views/lesson/frame_list/frame/componentized/component/matching_challenge_button/matching_challenge_button.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('cfMatchingChallengeButton', [
    'Lesson.FrameList.Frame.Componentized.Component.UiComponent.UiComponentDirHelper',
    'Lesson.FrameList.Frame.Componentized.Component.Challenge.ChallengeBlankDirHelper',
    'Lesson.FrameList.Frame.Componentized.Component.AnswerList.AnswerButtonsDirHelper',
    (UiComponentDirHelper, ChallengeBlankDirHelper, AnswerButtonsDirHelper) =>
        UiComponentDirHelper.getOptions({
            templateUrl,
            link(scope) {
                UiComponentDirHelper.link(scope);

                // it's a little odd that we're including something called ChallengeBlankDirHelper
                // here, but it has useful stuff for us, and I didn't feel like renaming it
                ChallengeBlankDirHelper.link(scope);

                Object.defineProperty(scope, 'cssClasses', {
                    get() {
                        const classes = [];
                        classes.push(scope.model.contentType);

                        if (scope.frameViewModel.editorMode) {
                            classes.push('editable');
                        }

                        // challengeBlankCssClasses comes from ChallengeBlankDirHelper
                        return classes.concat(this.challengeBlankCssClasses);
                    },
                });

                scope.imageStyleFor = viewModel => AnswerButtonsDirHelper.imageStyleFor(scope, viewModel);
            },
        }),
]);
