import angularModule from 'Lessons/angularModule/scripts/lessons_module';

angularModule.factory(
    'Lesson.FrameList.Frame.Componentized.Component.ContinueButton.ChallengesContinueButton.ChallengesContinueButtonViewModel',

    [
        'Lesson.FrameList.Frame.Componentized.Component.ContinueButton.ContinueButtonViewModel',

        ContinueButtonViewModel =>
            ContinueButtonViewModel.subclass(() => ({
                directiveName: 'cf-challenges-continue-button',
            })),
    ],
);
