import angularModule from 'Lessons/angularModule/scripts/lessons_module';

angular
    .module('FrontRoyal.Lessons')
    .factory('Lesson.FrameList.Frame.Componentized.Component.Text.Behaviors.ProcessesStorableImages', [
        '$injector',
        $injector => {
            const AModuleAbove = $injector.get('AModuleAbove');

            return new AModuleAbove({
                included(TextModel) {
                    TextModel.supportBehavior('ProcessesStorableImages');
                },
            });
        },
    ]);
