import angularModule from 'Lessons/angularModule/scripts/lessons_module';
import casperMode from 'casperMode';
import template from 'Lessons/angularModule/views/lesson/frame_list/frame/componentized/component/interactive_cards/interactive_cards.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('cfInteractiveCards', [
    '$injector',
    $injector => {
        const UiComponentDirHelper = $injector.get(
            'Lesson.FrameList.Frame.Componentized.Component.UiComponent.UiComponentDirHelper',
        );
        const $timeout = $injector.get('$timeout');
        const Capabilities = $injector.get('Capabilities');
        const $window = $injector.get('$window');
        const safeApply = $injector.get('safeApply');
        const $interval = $injector.get('$interval');

        return UiComponentDirHelper.getOptions({
            templateUrl,
            link(scope, elem) {
                UiComponentDirHelper.link(scope);

                let activateChallengeTimeout;

                scope.getOverlayClasses = function (interactiveCardsViewModel, overlayViewModel) {
                    const classes = {
                        card: true,
                    };

                    if (overlayViewModel.model.contentType) {
                        const key = `${overlayViewModel.model.contentType}-card`;
                        classes[key] = true;
                    }

                    if (overlayViewModel.model.background_color) {
                        const key = `background-${overlayViewModel.model.background_color}`;
                        classes[key] = true;
                        classes['has-background'] = true; // see interactive_cards.scss
                    }

                    if (interactiveCardsViewModel.currentCard === overlayViewModel.model) {
                        classes.active = true;
                    }

                    if (interactiveCardsViewModel.model.force_card_height) {
                        classes['force-card-height'] = true;
                    }

                    if (interactiveCardsViewModel.model.wide) {
                        classes.wide = true;
                    }

                    return classes;
                };

                // When content is right-to-left, we reverse the carousel direction
                scope.reverseDirection =
                    scope.viewModel &&
                    scope.viewModel.playerViewModel &&
                    scope.viewModel.playerViewModel.lesson &&
                    scope.viewModel.playerViewModel.lesson.localeDirection === 'rtl';
                const reverseMultiplier = scope.reverseDirection ? -1 : 1;

                // Listen for each challenge to be completed, and when it is:
                // 1. if the next challenge's blank is visible on the current card,
                //    then activate it
                // 2. otherwise, go to the card that has the next challenge's blank
                //    and then, once the transition is complete, activate it.
                //
                // I tried to make this a behavior, but it was tricky because it needs to know when
                // the transition is done.  In any case, it seems easy enough to get the flexibility
                // we need here.
                scope.model.on('.challengesComponent.challenges:childAdded', challenge => {
                    const challengesViewModel = scope.viewModel.challengesComponentViewModel;
                    const challengeViewModel = scope.viewModel.viewModelFor(challenge);

                    const delay = {
                        ChallengeModel: 0,
                        MultipleChoiceChallengeModel: 1500,
                        UserInputChallengeModel: 0,
                    }[challenge.type];

                    if (angular.isUndefined(delay)) {
                        throw new Error(`Cannot determine delay for "${challenge.type}"`);
                    }

                    if (activateChallengeTimeout) {
                        $timeout.cancel(activateChallengeTimeout);
                    }

                    challengeViewModel.on('completed', () => {
                        // in preview mode, the other window may have already moved us on
                        // to the next challenge
                        if (challengeViewModel.completedHandledByInteractiveCardsScope) {
                            return;
                        }
                        challengeViewModel.completedHandledByInteractiveCardsScope = scope.$id;

                        const nextChallengeViewModel = challengesViewModel.nextChallengeViewModel;

                        if (!nextChallengeViewModel) {
                            return;
                        }

                        /*
                            It's important that we don't use a timeout if delay
                            is 0, because we have to activate the next challenge
                            synchronously to prevent losing the keyboard in mobile
                            safari.
                        */
                        if (delay && delay > 0) {
                            activateChallengeTimeout = $timeout(() => {
                                scope.activateChallenge(nextChallengeViewModel);
                            }, delay);
                        } else {
                            scope.activateChallenge(nextChallengeViewModel);
                        }
                    });
                });

                scope.onAfterSlideChange = () => {
                    if (scope.nextChallengeViewModel) {
                        scope.nextChallengeViewModel.active = true;
                        scope.nextChallengeViewModel = undefined;
                    } else {
                        const currentChallenge =
                            scope.viewModel.challengesComponentViewModel.currentChallengeViewModel &&
                            scope.viewModel.challengesComponentViewModel.currentChallengeViewModel.model;
                        if (currentChallenge && !scope.viewModel.challengeOnCurrentCard(currentChallenge)) {
                            scope.viewModel.activateSomeChallengeOnCurrentCard();
                        }
                    }
                    safeApply(scope);
                };

                scope.activateChallenge = nextChallengeViewModel => {
                    if (scope.viewModel.challengeOnCurrentCard(nextChallengeViewModel.model)) {
                        nextChallengeViewModel.active = true;
                    } else {
                        scope.viewModel.showCardForChallenge(nextChallengeViewModel.model);
                        scope.nextChallengeViewModel = nextChallengeViewModel;
                    }
                    safeApply(scope);
                };

                function getMaxCardHeight() {
                    const componentOverlays = elem.find('.carousel cf-component-overlay').toArray();
                    if (componentOverlays.length === 0) {
                        return null;
                    }
                    return Math.max(...componentOverlays.map(overlay => overlay.offsetHeight));
                }

                // After the directive renders, or if card content or options change,
                // programatically resize all the cards to match the
                // tallest. I tried doing this with CSS (table layout and flex), but we also need the
                // width to be fluid for the carousel to work; so I couldn't figure out a pure-CSS solution.
                function setCardHeights() {
                    // Push the resize to the next event loop to ensure that the browser has rendered
                    // the changes that triggered setCardHeights.
                    // For example, the rescale code in `component_overlay_dir` could trigger this,
                    // but the image might not actually be rendered in the browser yet, which is necessary
                    // for this function to work.
                    $timeout(() => {
                        const cardSelector = '.card-wrapper .card';

                        if (scope.model.force_card_height) {
                            const cards = elem.find(cardSelector).toArray();

                            if (cards.length === 0) {
                                return;
                            }

                            cards.forEach(card => {
                                card.style.height = null;
                            });
                        } else {
                            const maxCardHeight = getMaxCardHeight();

                            const cards = elem.find(cardSelector).toArray();

                            if (cards.length === 0) {
                                return;
                            }

                            cards.forEach(card => {
                                card.style.height = `${maxCardHeight}px`;
                            });
                        }
                    });
                }

                // We need to trigger a height calculation when...
                scope.$watch('model.force_card_height', setCardHeights); // an editor toggles force_card_height
                scope.$watchCollection('model.overlay_ids', setCardHeights); // an editor adds or removes an overlay
                scope.viewModel.on('interactiveCards:shouldSetCardHeights', setCardHeights); // an editor changes text content
                scope.$on('frame:rendered', setCardHeights); // component_overlay_dir rescales an image

                // Poll for the elements used in the maxHeight calculation to be rendered.
                // We saw an intermittent issue where all of the above setCardHeights invocations would run
                // while the elements used in the maxHeight calculation were not rendered, which resulted in
                // the cards having a height of zero.
                const hasHeightInterval = $interval(
                    () => {
                        const height = getMaxCardHeight();
                        if (height) {
                            $interval.cancel(hasHeightInterval);
                        } else {
                            setCardHeights();
                        }
                    },
                    0,
                    50,
                ); // run a max of 50 times to ensure no chance of infinite loop

                function getWidth() {
                    return elem.find('.cf-interactive-cards').width();
                }

                const carousel = elem.find('.carousel');

                function translateCarousel(panning = 0) {
                    const cardWidth = carousel.find('.card-wrapper').outerWidth(true);

                    const position = -cardWidth * scope.viewModel.currentIndex + panning;

                    const translateValue = `translateX(${reverseMultiplier * position}px)`;

                    // Scale the element using transform, so that text scales down as well
                    carousel.css({
                        '-webkit-transform': translateValue,
                        '-o-transform': translateValue,
                        transform: translateValue,
                    });
                }

                const masks = elem.find('.mask');
                translateCarousel(0); // initialize translate to 0 so it doesn't slide in

                let pannedTransition = false;

                // Hammer.js recognizes vertical panning on touch devices and touch emulators
                // (Chrome device emulator) as “panleft”. See this answer on the issue for the
                // 'pointercancel' suggestion: https://github.com/hammerjs/hammer.js/issues/1050#issuecomment-279239388
                //
                // See also an issue that was caused by only checking for this sometimes: https://trello.com/c/WDPIf6fG
                function runIfNotPointerCancel(event, callback) {
                    if (event.srcEvent.type !== 'pointercancel') {
                        callback();
                    }
                }

                if (Capabilities.touchEnabled) {
                    if (scope.viewModel.overlaysViewModels.length > 1) {
                        var mc = new $window.Hammer(carousel[0]);

                        mc.on('panstart', ev => {
                            runIfNotPointerCancel(ev, () => {
                                masks.addClass('panning');
                                carousel.addClass('panning');
                            });
                        });
                        mc.on('panend', ev => {
                            runIfNotPointerCancel(ev, () => {
                                masks.removeClass('panning');
                                carousel.removeClass('panning');
                                const width = getWidth();

                                // if the card hs been moved far enough, or if it has been flicked (velocity is high)
                                if (ev.deltaX < -0.25 * width * reverseMultiplier || ev.velocityX > 0.5) {
                                    scope.viewModel.goForward();
                                } else if (ev.deltaX > 0.25 * width * reverseMultiplier || ev.velocityX < -0.5) {
                                    scope.viewModel.goBack();
                                }

                                // hold off on the digest until the
                                // transition is done.  Makes it smoother
                                carousel.one('transitionend', () => {
                                    pannedTransition = true;
                                    safeApply(scope);
                                });

                                // even if the currentIndex did not change,
                                // we still want to translate the carousel again
                                translateCarousel();
                            });
                        });

                        mc.on('panleft panright', ev => {
                            runIfNotPointerCancel(ev, () => {
                                translateCarousel(ev.deltaX);
                            });
                        });
                    }
                }

                let cancelDisableButtonsTimeout;

                /*
                    Note: this does not perfectly handle the case where we switch the order
                    of the interactive cards in the editor.  In that case, you end up with the
                    active challenge on a card that is not active.  But it's a bit tricky
                    to fix and it's not either very common or very severe, so I'm leaving it for now.
                */
                scope.$watch('viewModel.currentIndex', () => {
                    translateCarousel();

                    // during panned transitions, we don't digest until after the transition has already completed.
                    // in that case, go ahead and perform the slide change, as the carousel won't refire the event.
                    if (pannedTransition) {
                        pannedTransition = false;
                        scope.onAfterSlideChange();
                    } else if (casperMode()) {
                        // In casperMode we have disabled transitions, so manually fire it
                        scope.onAfterSlideChange();
                    } else {
                        carousel.one('transitionend', scope.onAfterSlideChange);
                    }

                    scope.disableButtons = true;
                    cancelDisableButtonsTimeout = $timeout(() => {
                        scope.disableButtons = false;
                    }, 500);
                });

                scope.$on('$destroy', () => {
                    if (mc) {
                        mc.destroy();
                    }
                    $timeout.cancel(cancelDisableButtonsTimeout);
                });
            },
        });
    },
]);
