import angularModule from 'Lessons/angularModule/scripts/lessons_module';
import 'ExtensionMethods/array';

angularModule.factory('Lesson.FrameList.Frame.Componentized', [
    'Lesson.FrameList.Frame',
    'Lesson.FrameList.Frame.Componentized.ComponentizedFrameViewModel',
    'Lesson.FrameList.Frame.Componentized.Component.ComponentModel',
    '$injector',

    (Frame, ComponentizedFrameViewModel, ComponentModel, $injector) => {
        // Support lazy loading of injectables
        Frame.mapInjectables({
            // ui component
            'ComponentizedFrame.UiComponent':
                'Lesson.FrameList.Frame.Componentized.Component.UiComponent.UiComponentModel',

            // content providers
            'ComponentizedFrame.Challenges':
                'Lesson.FrameList.Frame.Componentized.Component.Challenges.ChallengesModel',

            // layouts
            'ComponentizedFrame.Layout': 'Lesson.FrameList.Frame.Componentized.Component.Layout.LayoutModel',
            'ComponentizedFrame.TextImageInteractive':
                'Lesson.FrameList.Frame.Componentized.Component.Layout.TextImageInteractive.TextImageInteractiveModel',

            // challenge stuff
            'ComponentizedFrame.AnswerMatcher':
                'Lesson.FrameList.Frame.Componentized.Component.AnswerMatcher.AnswerMatcherModel',
            'ComponentizedFrame.SimilarToSelectableAnswer':
                'Lesson.FrameList.Frame.Componentized.Component.AnswerMatcher.SimilarToSelectableAnswer.SimilarToSelectableAnswerModel',
            'ComponentizedFrame.MatchesExpectedText':
                'Lesson.FrameList.Frame.Componentized.Component.AnswerMatcher.MatchesExpectedText.MatchesExpectedTextModel',
            'ComponentizedFrame.AnswerList':
                'Lesson.FrameList.Frame.Componentized.Component.AnswerList.AnswerListModel',
            'ComponentizedFrame.Challenge': 'Lesson.FrameList.Frame.Componentized.Component.Challenge.ChallengeModel',
            'ComponentizedFrame.MultipleChoiceChallenge':
                'Lesson.FrameList.Frame.Componentized.Component.Challenge.MultipleChoiceChallenge.MultipleChoiceChallengeModel',
            'ComponentizedFrame.UserInputChallenge':
                'Lesson.FrameList.Frame.Componentized.Component.Challenge.UserInputChallenge.UserInputChallengeModel',
            'ComponentizedFrame.ChallengeOverlayBlank':
                'Lesson.FrameList.Frame.Componentized.Component.ChallengeOverlayBlank.ChallengeOverlayBlankModel',
            'ComponentizedFrame.MultipleChoiceMessage':
                'Lesson.FrameList.Frame.Componentized.Component.MultipleChoiceMessage.MultipleChoiceMessageModel',
            'ComponentizedFrame.UserInputMessage':
                'Lesson.FrameList.Frame.Componentized.Component.UserInputMessage.UserInputMessageModel',
            'ComponentizedFrame.ChallengeValidator':
                'Lesson.FrameList.Frame.Componentized.Component.ChallengeValidator.ChallengeValidatorModel',
            'ComponentizedFrame.SelectableAnswer':
                'Lesson.FrameList.Frame.Componentized.Component.Answer.SelectableAnswer.SelectableAnswerModel',
            'ComponentizedFrame.TilePrompt':
                'Lesson.FrameList.Frame.Componentized.Component.TilePrompt.TilePromptModel',
            'ComponentizedFrame.MatchingBoard':
                'Lesson.FrameList.Frame.Componentized.Component.MatchingBoard.MatchingBoardModel',
            'ComponentizedFrame.TilePromptBoard':
                'Lesson.FrameList.Frame.Componentized.Component.TilePromptBoard.TilePromptBoardModel',
            'ComponentizedFrame.MatchingChallengeButton':
                'Lesson.FrameList.Frame.Componentized.Component.MatchingChallengeButton.MatchingChallengeButtonModel',
            'ComponentizedFrame.SelectableAnswerNavigator':
                'Lesson.FrameList.Frame.Componentized.Component.Answer.SelectableAnswer.SelectableAnswerNavigatorModel',

            // continue buttons
            'ComponentizedFrame.ChallengesContinueButton':
                'Lesson.FrameList.Frame.Componentized.Component.ContinueButton.ChallengesContinueButton.ChallengesContinueButtonModel',
            'ComponentizedFrame.AlwaysReadyContinueButton':
                'Lesson.FrameList.Frame.Componentized.Component.ContinueButton.AlwaysReadyContinueButton.AlwaysReadyContinueButtonModel',

            // frame navigator
            'ComponentizedFrame.FrameNavigator':
                'Lesson.FrameList.Frame.Componentized.Component.FrameNavigator.FrameNavigatorModel',

            // other stuff
            'ComponentizedFrame.Image': 'Lesson.FrameList.Frame.Componentized.Component.Image.ImageModel',
            'ComponentizedFrame.Text': 'Lesson.FrameList.Frame.Componentized.Component.Text.TextModel',
            'ComponentizedFrame.ComponentOverlay':
                'Lesson.FrameList.Frame.Componentized.Component.ComponentOverlay.ComponentOverlayModel',
            'ComponentizedFrame.InteractiveCards':
                'Lesson.FrameList.Frame.Componentized.Component.InteractiveCards.InteractiveCardsModel',
        });

        // we could get this from the list, but I don't want to try to keep track of all those models
        // in the arguments array.  Easier to pull it this way
        const ContinueButtonModel = $injector.get(
            'Lesson.FrameList.Frame.Componentized.Component.ContinueButton.ContinueButtonModel',
        );
        const FrameNavigatorModel = $injector.get(
            'Lesson.FrameList.Frame.Componentized.Component.FrameNavigator.FrameNavigatorModel',
        );
        const ImageModel = $injector.get('Lesson.FrameList.Frame.Componentized.Component.Image.ImageModel');
        const TextImageInteractive = $injector.get(
            'Lesson.FrameList.Frame.Componentized.Component.Layout.TextImageInteractive.TextImageInteractiveModel',
        );
        const TextModel = $injector.get('Lesson.FrameList.Frame.Componentized.Component.Text.TextModel');
        const $q = $injector.get('$q');
        const splitOnSpecialBlock = $injector.get('Lesson.FrameList.Frame.Componentized.Component.Text.TextHelpers')
            .splitOnSpecialBlock;

        const TranslationHelper = $injector.get('TranslationHelper');

        const translationHelper = new TranslationHelper('lessons.lesson.frame_list.frame.componentized');

        const getMultipleChoiceInstructions = function () {
            if (this.mainUiComponent.challenges[0].editor_template === 'check_many') {
                return translationHelper.get('select_all');
            }
            return translationHelper.get('select_an');
        };

        return Frame.subclass(function () {
            this.alias('componentized');

            this.embedsMany('components', 'Lesson.FrameList.Frame.Componentized.Component.ComponentModel');

            this.extend({
                title: 'Componentized',
                FrameViewModel: ComponentizedFrameViewModel,
            });

            // NOTE: utilizing around callbacks were creating max callstack issues in Chrome. Perhaps this can eventually be fixed.
            // this.setCallback('before', 'save', 'removeUnreferencedComponents');

            Object.defineProperty(this.prototype, 'miniInstructions', {
                get() {
                    const key = this.mainUiComponent.editor_template;

                    if (!this.$$_miniInstructionsMap) {
                        this.$$_miniInstructionsMap = {
                            no_interaction: translationHelper.get('no_interaction'),
                            basic_multiple_choice: getMultipleChoiceInstructions,
                            multiple_card_multiple_choice: getMultipleChoiceInstructions,
                            matching: translationHelper.get('matching'),
                            this_or_that: translationHelper.get('this_or_that'),
                            fill_in_the_blanks: translationHelper.get('fill_in_the_blanks'),
                            image_hotspot: getMultipleChoiceInstructions,
                            compose_blanks: translationHelper.get('compose_blanks'),
                            compose_blanks_on_image: translationHelper.get('compose_blanks_on_image'),
                            blanks_on_image: translationHelper.get('blanks_on_image'),
                            venn_diagram: translationHelper.get('venn_diagram'),
                            multiple_choice_poll: translationHelper.get('multiple_choice_poll'),
                        };
                    }

                    const valueGetter = this.$$_miniInstructionsMap[key];

                    // should we support custom values?
                    if (!valueGetter) {
                        throw new Error(`No miniInstructions for "${key}"`);
                    } else if (typeof valueGetter === 'function') {
                        return valueGetter.apply(this);
                    } else {
                        return valueGetter;
                    }
                },
            });

            Object.defineProperty(this.prototype, 'editor_template', {
                get() {
                    return this.mainUiComponent ? this.mainUiComponent.editor_template : undefined;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'continueButton', {
                get() {
                    return this.continue_button_id && this.dereference(this.continue_button_id);
                },
                set(component) {
                    if (!component) {
                        if (this.continueButton) {
                            this.continueButton.remove();
                        }
                        this.continue_button_id = undefined;
                    } else if (component.isA && component.isA(ContinueButtonModel)) {
                        if (this.continueButton) {
                            this.continueButton.remove();
                        }
                        this.continue_button_id = component.id;
                    } else {
                        throw new Error('Cannot set continueButton to something that is not a ContinueButtonModel.');
                    }
                    return component;
                },
            });

            Object.defineProperty(this.prototype, 'frameNavigator', {
                get() {
                    return this.frame_navigator_id && this.dereference(this.frame_navigator_id);
                },
                set(component) {
                    if (!component) {
                        if (this.frameNavigator) {
                            this.frameNavigator.remove();
                        }
                        this.frame_navigator_id = undefined;
                    } else if (component.isA && component.isA(FrameNavigatorModel)) {
                        if (this.frameNavigator) {
                            this.frameNavigator.remove();
                        }
                        this.frame_navigator_id = component.id;
                    } else {
                        throw new Error('Cannot set frameNavigator to something that is not a FrameNavigatorModel.');
                    }
                    return component;
                },
            });
            Object.defineProperty(this.prototype, 'mainUiComponent', {
                get() {
                    return this.main_ui_component_id && this.dereference(this.main_ui_component_id);
                },
                set(component) {
                    if (!component) {
                        this.main_ui_component_id = undefined;
                    } else if (component.isA && component.isA(ComponentModel)) {
                        this.main_ui_component_id = component.id;
                    } else {
                        throw new Error('Cannot set mainUiComponent to something that is not a ComponentModel.');
                    }
                    return component;
                },
            });

            Object.defineProperty(this.prototype, 'mainUiComponentEditorViewModel', {
                get() {
                    return this.mainUiComponent && this.editorViewModelFor(this.mainUiComponent);
                },
            });

            // Used outside when the frame interfaces with things in the editor.
            // For switching between componentized and non-componentized
            // frame types and for populating the thunmbnails
            Object.defineProperty(this.prototype, 'text_content', {
                get() {
                    return this.mainUiComponent.text_content;
                },
                set(val) {
                    // when first initializing one of these by switching from another
                    // frame type, text_content might be set, but we might not have
                    // a mainUiComponent
                    if (this.mainUiComponent) {
                        this.mainUiComponent.text_content = val;
                    }
                    return val;
                },
            });

            Object.defineProperty(this.prototype, 'mainTextComponent', {
                get() {
                    return this.mainUiComponent ? this.mainUiComponent.mainTextComponent : undefined;
                },
            });

            // Used when  switching between componentized and non-componentized
            // frame types.  Can be removed once all non-componentized frame types
            // are gone
            Object.defineProperty(this.prototype, 'mainModalTexts', {
                get() {
                    return this.mainUiComponentEditorViewModel.mainModalTexts;
                },
                set(val) {
                    // when first initializing one of these by switching from another
                    // frame type, text_content might be set, but we might not have
                    // a mainUiComponent
                    if (this.mainUiComponentEditorViewModel) {
                        this.mainUiComponentEditorViewModel.mainModalTexts = val;
                    }
                    return val;
                },
            });

            Object.defineProperty(this.prototype, 'label', {
                get() {
                    const truncateTo = 30;
                    const text = this.text_content || '';
                    const truncatedText = text.length > truncateTo ? `${text.substring(0, truncateTo)}...` : text;
                    return [this.index() + 1, ': ', truncatedText].join('');
                },
            });

            // Used outside when the frame interfaces with things in the editor.
            // For switching between componentized and non-componentized
            // frame types and for populating the thunmbnails
            Object.defineProperty(this.prototype, 'main_image_id', {
                get() {
                    return this.mainUiComponentEditorViewModel.mainImage
                        ? this.mainUiComponentEditorViewModel.mainImage.id
                        : undefined;
                },
                set(val) {
                    // when first initializing one of these by switching from another
                    // frame type, main_image_id might be set, but we might not have
                    // a mainUiComponent
                    if (this.mainUiComponentEditorViewModel) {
                        const image = val ? this.dereference(val) : undefined;
                        this.mainUiComponentEditorViewModel.mainImage = image;
                    }
                    return val;
                },
            });

            Object.defineProperty(this.prototype, 'mainImageSrc', {
                get() {
                    return this.main_image_id
                        ? this.dereference(this.main_image_id).urlForFormat('original')
                        : undefined;
                },
            });

            Object.defineProperty(this.prototype, 'imageEditorViewModels', {
                get() {
                    const editorViewModels = [];
                    this.components.forEach(component => {
                        if (component.isA(ImageModel)) {
                            editorViewModels.push(component.editorViewModel);
                        }
                    });
                    return editorViewModels;
                },
            });

            Object.defineProperty(this.prototype, 'imageComponents', {
                get() {
                    const models = [];
                    this.components.forEach(component => {
                        if (component.isA(ImageModel)) {
                            models.push(component);
                        }
                    });
                    return models;
                },
            });

            return {
                directiveName: 'componentized',
                buttonDirectiveName: 'componentized-continue-button',
                infoPanelDirectiveName: 'componentized-info-panel',

                initialize($super, attrs) {
                    attrs.components = attrs.components || [];
                    $super(attrs);
                    this.$$editorViewModels = {};
                    this.$$_componentMap = {};
                    this.$$_componentsByType = {};
                    if (!this.frameNavigator) {
                        this.frameNavigator = FrameNavigatorModel.EditorViewModel.addComponentTo(this).model;
                    }
                },

                beforeSave() {
                    this.removeUnreferencedComponents();
                },

                // In order to speed things up, cache components in a hash
                // once they have been dereferenced once, so that we don't have
                // to loop through the components array over and over again
                dereference(id) {
                    if (!this.$$_componentMap[id]) {
                        this.components.forEach(component => {
                            if (component.id === id) {
                                this.$$_componentMap[id] = component;
                            }
                        });
                    }

                    if (!this.$$_componentMap[id]) {
                        const err = new Error('No component found for requested id.');
                        $injector.get('ErrorLogService').notify(err, null, {
                            componentId: id,
                        });
                        throw err;
                    }

                    return this.$$_componentMap[id];
                },

                getComponentById(id) {
                    let component;
                    try {
                        component = this.dereference(id);
                    } catch (e) {
                        if (!e.message.match(/No component found/)) {
                            throw e;
                        }
                    }
                    return component;
                },

                mapComponentsById() {
                    const componentMap = {};
                    this.components.forEach(c => {
                        componentMap[c.id] = c;
                    });
                    return componentMap;
                },

                addComponent(component) {
                    if (component.frame()) {
                        throw new Error('Can only add components that are not on any frame');
                    }

                    // do not use this.$$_componentMap, because it may not have everything
                    const componentMap = this.mapComponentsById();
                    const existingComponent = componentMap[component.id];

                    // If a component with this id already exists, skip it.  Theoretically,
                    // this could be dangerous.  Hopefully, it won't be a problem because
                    // we will only hit this if we try to import the same component from another
                    // frame more than once.  In those cases, we want to keep the first import,
                    // since we may have changed it.
                    if (!existingComponent) {
                        component.$$embeddedIn = this;
                        this.components.push(component);
                    }

                    this._onComponentListChanged();
                    return existingComponent || component;
                },

                /*
                    Copy a component from a different frame over to this one.

                    If options.addReferences is true, then all referenced components
                    will also be imported.

                    options.skipReferences is an array of keys, and any component
                    with that key will be skipped (see indirectlyReferencedComponents
                    for that implementation)
                */
                importComponent(component, options = {}) {
                    if (component.frame() === this) {
                        throw new Error('Cannot copy a component onto this frame if it is already on this frame.');
                    }

                    const componentsToAdd = [component];

                    if (options.addReferences) {
                        component.indirectlyReferencedComponents(options).forEach(referencedComponent => {
                            componentsToAdd.push(referencedComponent);
                        });
                    }

                    // eslint-disable-next-line no-shadow
                    const addedComponents = componentsToAdd.map(component => {
                        if (!this.components.includes(component)) {
                            // clone the components so we don't mess with the
                            // source frame
                            const clonedComponent = component.clone();
                            return this.addComponent(clonedComponent);
                        }
                        return undefined;
                    });

                    // addedComponents[0] may be a clone of the component that was
                    // passed in, or it may be the same one.
                    return addedComponents[0];
                },

                // loop through this frame's branching overrides looking for references pointing
                // to the old frame, and replace those references with references to the
                // specified new frame
                replaceFrameReferences(oldFrame, newFrame) {
                    // look for references in answer-specific overrides
                    if (this.frameNavigator && this.frameNavigator.selectableAnswerNavigators) {
                        this.frameNavigator.selectableAnswerNavigators.forEach(selectableAnswerNavigator => {
                            if (selectableAnswerNavigator.next_frame_id === oldFrame.id) {
                                selectableAnswerNavigator.next_frame_id = newFrame.id;
                            }
                        });
                    }

                    // look for references on the frameNavigator
                    if (this.frameNavigator.next_frame_id === oldFrame.id) {
                        this.frameNavigator.next_frame_id = newFrame.id;
                    }
                    return this;
                },
                /*
                    Make it so that any components that used to reference
                    the old component now reference the new one.

                    Both components should already be on the frame.
                */
                swapComponents(oldComponent, newComponent) {
                    if (oldComponent.frame() !== this) {
                        throw new Error('oldComponent is not in this frame');
                    }

                    if (newComponent.frame() !== this) {
                        throw new Error('newComponent is not in this frame');
                    }

                    this.components.forEach(component => {
                        component.editorViewModel.swapReferences(oldComponent, newComponent);
                    });
                },

                /*
                    Given an editor_template to use on the mainUiComponent,
                    create a new frame whose mainUiComponent has that editor_template
                    and replace this frame with that frame in the containing lesson.
                */
                swapMainUiComponentEditorTemplate(template) {
                    const oldFrame = this;
                    const newFrame = this.lesson().replaceFrameWithEmptyOne(oldFrame);
                    const helper = template.EditorViewModel.addComponentTo(newFrame).setup();
                    newFrame.mainUiComponent = helper.model;
                    newFrame.mainUiComponentEditorViewModel.applyTemplate(template, oldFrame);
                    newFrame.copyMainTextAndImagesFrom(oldFrame);
                    return newFrame;
                },

                /*
                    Copy all the images, the main text content, the modals,
                    and the main image from another frame onto this one. (If this
                    frame does not support a main image then that will not be set).
                */
                copyMainTextAndImagesFrom(otherFrame) {
                    if (!otherFrame) {
                        return;
                    }
                    const frame = this;
                    otherFrame.componentsForType(ImageModel).forEach(image => {
                        frame.importComponent(image);
                    });

                    // if the editor template set one of these (for example,
                    // multiple_choice_poll sets the main_image_id), do not
                    // override it.  Except with mainModalTexts.  For those, new ones
                    // get created, but they are just defaults.  Copy over the existing
                    // values.  A bit hacky, but it works.
                    ['text_content', 'mainModalTexts', 'main_image_id'].forEach(key => {
                        if (!frame[key] || key === 'mainModalTexts') {
                            frame[key] = otherFrame[key];
                        }
                    });
                },

                editorViewModelFor(model) {
                    return this.editorViewModelsFor([model])[0];
                },

                editorViewModelsFor(models) {
                    const editorViewModels = [];
                    angular.forEach(models, model => {
                        // Note: If you get a max call stack error that traces back to here,
                        // the issue is probably that you are initializing editor view models in the
                        // initialize method of other editor view models in some sort of
                        // circular manner.
                        if (!model || !model.isA || !model.isA(ComponentModel)) {
                            console.error('Not a ComponentModel: ', model);
                            throw new Error(
                                `Cannot create EditorViewModel for something which is not a ComponentModel: ${model}`,
                            );
                        }
                        if (model.frame() !== this) {
                            throw new Error('Cannot create EditorViewModel for unrelated component.');
                        }

                        // if we don't have a EditorViewModel yet, and this component supports EditorViewModels, create one
                        if (!this.$$editorViewModels[model.id] && model.constructor.EditorViewModel) {
                            let editorViewModel;
                            try {
                                editorViewModel = new model.constructor.EditorViewModel(model);
                            } catch (err) {
                                if (err.message.match(/Maximum call stack/i)) {
                                    throw new Error(
                                        `The EditorViewModel initializer for ${model.type} raised a Maximum call stack error.  Most likely it referenced model.editorViewModel.  Do not do that.`,
                                    );
                                } else {
                                    throw err;
                                }
                            }
                            this.$$editorViewModels[model.id] = editorViewModel;
                            editorViewModel.applyCurrentTemplate();
                        }
                        editorViewModels.push(this.$$editorViewModels[model.id]);
                    });
                    return editorViewModels;
                },

                editorViewModelsForType(modelKlass) {
                    return this.componentsForType(modelKlass).map(component => this.editorViewModelFor(component));
                },

                componentsForType(modelKlass) {
                    if (typeof modelKlass === 'string') {
                        modelKlass = $injector.get(`Lesson.FrameList.Frame.Componentized.Component.${modelKlass}`);
                    }

                    // cache the results so that we're not looping through
                    // the components again and again.  The cache is blown away
                    // in addComponent
                    if (!this.$$_componentsByType[modelKlass.alias()]) {
                        const components = [];
                        this.components.forEach(component => {
                            if (component.isA(modelKlass)) {
                                components.push(component);
                            }
                        });
                        this.$$_componentsByType[modelKlass.alias()] = components;
                    }
                    return this.$$_componentsByType[modelKlass.alias()];
                },

                removeComponent(component) {
                    // clone the list of components first, so that
                    // if components are removed from within the loop, it
                    // does not cause trouble (see https://trello.com/c/7ZoFhlGK/116-bug-error-when-removing-answer-from-answer-list-in-editor)
                    const components = this.components.slice(0);
                    components.forEach(_component => {
                        this.editorViewModelFor(_component).removeReferencesTo(component);
                    });

                    delete this.$$_componentMap[component.id];
                    if (this.mainUiComponent === component) {
                        this.mainUiComponent = undefined;
                    }
                    if (this.continueButton === component) {
                        this.continue_button_id = undefined;
                    }
                    if (this.frameNavigator === component) {
                        this.frame_navigator_id = undefined;
                    }

                    Array.remove(this.components, component);
                    component.$$embeddedIn = undefined;
                    this._onComponentListChanged();
                },

                applyDefaultEditorTemplate() {
                    const defaultTemplate = TextImageInteractive.EditorViewModel.templates.no_interaction;
                    const helper = defaultTemplate.EditorViewModel.addComponentTo(this).setup();
                    this.mainUiComponent = helper.model;
                    helper.applyTemplate(defaultTemplate);
                },

                /*
                    FIXME: for this to really work, we would need to call it
                    again and again until nothing is removed.  It's possible that
                    the removal of one component can cause others to get removed
                    and leave something unreferenced.  We ran into this problem
                    with this ticket: https://trello.com/c/br4OTced/379-allow-editing-a-larger-chunk-of-text-at-once-in-blanks-and-compose-blanks-modes.
                    But we fixed it in a different way.
                */
                removeUnreferencedComponents() {
                    const referencedComponentsSet = this._referencedComponentsSet();

                    // it doesn't work to remove components as we're looping
                    // through the array, so we save the removed ones for later
                    const componentsToRemove = [];

                    this.components.forEach(component => {
                        if (!referencedComponentsSet[component.id] && !component.allowUnreferenced) {
                            componentsToRemove.push(component);
                        }
                    });

                    componentsToRemove.forEach(component => {
                        component.remove();
                    });
                },

                getReferencedImages() {
                    const referencedComponentsSet = this._referencedComponentsSet();
                    const images = this.componentsForType(ImageModel);
                    const referencedImages = [];
                    images.forEach(image => {
                        if (referencedComponentsSet[image.id]) {
                            referencedImages.push(image);
                        }
                    });
                    return referencedImages;
                },

                prevFrame() {
                    if (this.index() === 0) {
                        return undefined;
                    }

                    // find all possible frames that could have pointed to this frame
                    // and go back to the first in the list
                    let prevFrame;
                    const frames = this.lesson().frames;
                    for (let i = 0; i < frames.length; i++) {
                        const frame = frames[i].reify(); // for some reason, Safari might not have reified prior frames at this point
                        if (frame !== this && frame.frameNavigator.mightNavigateTo(this)) {
                            prevFrame = frame;
                            break;
                        }
                    }
                    if (!prevFrame) {
                        throw new Error(`No frames navigate to ${this.label}`);
                    }
                    return prevFrame;
                },

                preloadImages() {
                    return this.mainUiComponent.recursivelyPreloadImages();
                },

                recursivelyGetImageUrls() {
                    return this.mainUiComponent.recursivelyGetImageUrls();
                },

                validateTextLengths() {
                    let valid = true;

                    this.componentsForType(TextModel).forEach(textModel => {
                        const result = textModel.editorViewModel.validateTextLength();
                        if (!result.valid) {
                            valid = false;
                        }
                    });

                    return {
                        valid,
                    };
                },

                getKeyTerms() {
                    const keyTerms = [];

                    function extractKeyTerms(ignore, match) {
                        // remove brackets
                        let term = match.replace(/[[\]]/g, '');

                        // trim whitespace
                        term = term.trim();

                        keyTerms.push(term);
                    }

                    function extractKeyTermsIfNotInSpecialBlock(textBlock, inSpecialBlock) {
                        if (!inSpecialBlock) {
                            textBlock.replace(/\*\*([^*]+)\*/g, extractKeyTerms);
                        }
                    }

                    const textModels = this.componentsForType(TextModel);

                    // eslint-disable-next-line no-restricted-syntax
                    for (const textModel of textModels) {
                        const text = textModel.text || ''; // guard against undefined text

                        splitOnSpecialBlock(text, false, extractKeyTermsIfNotInSpecialBlock);
                    }

                    return keyTerms;
                },

                containsText(text) {
                    if (text) {
                        const textModels = this.componentsForType(TextModel);

                        // eslint-disable-next-line no-restricted-syntax
                        for (const textModel of textModels) {
                            // TODO: we may want to update this to use plaintext from formatted_text once we do pre-formatting
                            if (textModel.text && textModel.text.toLowerCase().includes(text.toLowerCase())) {
                                return true;
                            }
                        }
                    }

                    return false;
                },

                formatAllText(force) {
                    let textModels = this.componentsForType(TextModel);

                    if (!force) {
                        textModels = _.where(textModels, {
                            formatted_text: undefined,
                        });
                    }

                    return $q.all(_.chain(textModels).pluck('editorViewModel').invoke('formatText').value());
                },

                addTransN(convertNonMathjax) {
                    const textModels = this.componentsForType(TextModel);
                    _.chain(textModels).pluck('editorViewModel').invoke('addTransN', convertNonMathjax).value();

                    // if we're changing WITHIN MathJax, we need to also potentially fix up the user input models
                    // to make sure that we're not asking users to type something like "\transn{1.234}".
                    //
                    // We also need to find instances where we've changed multiple choice answers to be something
                    // like "\transn{1.234}" and wrap them in MathJax so they display properly.
                    if (!convertNonMathjax) {
                        const UserInputChallengeModel = $injector.get(
                            'Lesson.FrameList.Frame.Componentized.Component.Challenge.UserInputChallenge.UserInputChallengeModel',
                        );
                        const userInputChallengeModels = this.componentsForType(UserInputChallengeModel);
                        _.chain(userInputChallengeModels).pluck('editorViewModel').invoke('removeTransN').value();

                        const MultipleChoiceChallenge = $injector.get(
                            'Lesson.FrameList.Frame.Componentized.Component.Challenge.MultipleChoiceChallenge.MultipleChoiceChallengeModel',
                        );
                        const multipleChoiceChallengeModels = this.componentsForType(MultipleChoiceChallenge);
                        _.chain(multipleChoiceChallengeModels).pluck('editorViewModel').invoke('wrapTransN').value();
                    }
                },

                transformMixedFractions() {
                    const textModels = this.componentsForType(TextModel);

                    _.chain(textModels).pluck('editorViewModel').invoke('transformMixedFractions').value();
                },

                _referencedComponentsSet() {
                    const referencedComponentsSet = {};
                    ['mainUiComponent', 'continueButton', 'frameNavigator'].forEach(key => {
                        const component = this[key];
                        if (!component) {
                            return;
                        }
                        referencedComponentsSet[component.id] = component;

                        component.indirectlyReferencedComponents().forEach(referencedComponent => {
                            referencedComponentsSet[referencedComponent.id] = referencedComponent;
                        });
                    });

                    return referencedComponentsSet;
                },

                _onComponentListChanged() {
                    // kill the map of componentByType, now that the lists
                    // may be invalidated (we could try to just blow away
                    // certain lists for certain types, but it seems like more
                    // trouble than it's worth, since we'd have to worry about
                    // going up the inheritance chain)
                    this.$$_componentsByType = {};
                },
            };
        });
    },
]);
