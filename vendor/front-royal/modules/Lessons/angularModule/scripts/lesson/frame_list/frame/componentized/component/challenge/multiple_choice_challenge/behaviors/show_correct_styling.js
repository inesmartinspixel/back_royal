import angularModule from 'Lessons/angularModule/scripts/lessons_module';
/*
    When this behavior is on, any time an answer is validated and determined
    to be correct, the ui component for that answer will have correct styling added to it.
*/
angular
    .module('FrontRoyal.Lessons')
    .factory(
        'Lesson.FrameList.Frame.Componentized.Component.Challenge.MultipleChoiceChallenge.Behaviors.ShowCorrectStyling',
        [
            '$injector',
            $injector => {
                const AModuleAbove = $injector.get('AModuleAbove');
                const ComponentEventListener = $injector.get(
                    'Lesson.FrameList.Frame.Componentized.Component.ComponentEventListener',
                );

                return new AModuleAbove({
                    included(MultipleChoiceChallengeModel) {
                        MultipleChoiceChallengeModel.supportBehavior('ShowCorrectStyling');

                        MultipleChoiceChallengeModel.ViewModel.setCallback('after', 'initialize', function () {
                            const challengeViewModel = this;

                            this.model.on('behavior_added:ShowCorrectStyling', () => {
                                new ComponentEventListener(challengeViewModel, 'validated', validationResult => {
                                    validationResult
                                        .filterAnswersViewModels({
                                            selected: true,
                                            correct: true,
                                        })
                                        .forEach(answerViewModel => {
                                            answerViewModel.addCorrectStyling();
                                        });
                                });
                            });
                        });
                    },
                });
            },
        ],
    );
