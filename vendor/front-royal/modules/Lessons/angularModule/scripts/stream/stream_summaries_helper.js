import angularModule from 'Lessons/angularModule/scripts/lessons_module';

angularModule.factory('Stream.StreamSummariesHelper', [
    () => {
        const StreamSummariesHelper = {
            userHasUnlocked(stream, summary) {
                if (!stream || !summary) {
                    return false;
                }

                // bail if none completed
                const lessonsCompleted = stream.lessonsCompleted;
                if (_.isEmpty(lessonsCompleted)) {
                    return false;
                }

                // see if any are found
                return lessonsCompleted.some(completedLesson => _.contains(summary.lessons, completedLesson.id));
            },

            summariesForLesson(stream, lesson) {
                if (!stream || !lesson) {
                    return;
                }
                return stream.summaries.filter(summary => _.contains(summary.lessons, lesson.id));
            },

            newUnlocksForLesson(stream, lesson) {
                if (!stream || !lesson) {
                    return;
                }

                return this.summariesForLesson(stream, lesson).filter(
                    summary => !this.userHasUnlocked(stream, summary),
                );
            },

            onLink(scope) {
                scope.userHasUnlocked = this.userHasUnlocked;
                scope.summariesForLesson = this.summariesForLesson;
                scope.newUnlocksForLesson = this.newUnlocksForLesson;
            },
        };

        return StreamSummariesHelper;
    },
]);
