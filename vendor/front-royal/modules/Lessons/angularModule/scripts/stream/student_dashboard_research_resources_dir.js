import angularModule from 'Lessons/angularModule/scripts/lessons_module';
import { setupBrandNameProperties } from 'AppBrandMixin';
import template from 'Lessons/angularModule/views/stream/student_dashboard_research_resources.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('studentDashboardResearchResources', [
    '$injector',

    function factory($injector) {
        const $rootScope = $injector.get('$rootScope');
        const DialogModal = $injector.get('DialogModal');
        const TranslationHelper = $injector.get('TranslationHelper');
        const $location = $injector.get('$location');

        return {
            restrict: 'E',
            templateUrl,
            scope: {},
            link(scope) {
                setupBrandNameProperties($injector, scope);

                // Referenced inside of AppBrandMixin
                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                scope.viewLibraryServices = () => {
                    DialogModal.alert({
                        title: new TranslationHelper('lessons.stream.student_dashboard_research_resources').get(
                            'library_services',
                        ),
                        content: '<library-services></library-services>',
                        size: 'medium-large',
                        classes: ['library-services'],
                        close() {
                            $location.search('libraryservices', null);
                        },
                    });
                };

                scope.$location = $location;

                scope.$watch('$location.search().libraryservices', val => {
                    if (val === '1') {
                        scope.viewLibraryServices();
                    }
                });
            },
        };
    },
]);
