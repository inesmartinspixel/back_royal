import angularModule from 'Lessons/angularModule/scripts/lessons_module';
import template from 'Lessons/angularModule/views/stream/student_dashboard_schedule_interview.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('studentDashboardScheduleInterview', [
    '$injector',

    function factory($injector) {
        const $rootScope = $injector.get('$rootScope');
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');

        return {
            restrict: 'E',
            templateUrl,
            scope: {},
            link(scope) {
                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                NavigationHelperMixin.onLink(scope);

                scope.toggleBox = () => {
                    $('.app-main-container > ng-include > [ng-view] > div').toggleClass('schedule-interview-open');
                    scope.boxOpen = !scope.boxOpen;
                };

                scope.interviewLink = scope.currentUser.lastCohortApplication.invited_to_interview_url;
            },
        };
    },
]);
