import angularModule from 'Lessons/angularModule/scripts/lessons_module';
import { setupBrandEmailProperties } from 'AppBrandMixin';
import template from 'Lessons/angularModule/views/stream/student_dashboard_enrollment.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('studentDashboardEnrollment', [
    '$injector',
    function factory($injector) {
        const $rootScope = $injector.get('$rootScope');
        return {
            restrict: 'E',
            templateUrl,
            scope: {
                enrollmentTodos: '<',
                showHaveQuestions: '<',
            },
            link(scope) {
                setupBrandEmailProperties($injector, scope, ['registrar']);

                // referenced inside `setupBrandEmailProperties`
                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                scope.toggleEnrollment = () => {
                    $('.app-main-container > ng-include > [ng-view] > div').toggleClass('enrollment-open');
                    scope.enrollmentOpen = !scope.enrollmentOpen;
                };

                scope.onTodoClick = enrollmentTodo => {
                    if (!enrollmentTodo || enrollmentTodo.isDisabled()) {
                        return;
                    }
                    enrollmentTodo.onClick();
                };
            },
        };
    },
]);
