import angularModule from 'Lessons/angularModule/scripts/lessons_module';
// This only shows up when there is a relevant_cohort
import { formattedBrandName, getBrandStyleClass, setupBrandEmailProperties } from 'AppBrandMixin';
import template from 'Lessons/angularModule/views/stream/student_dashboard_program_box.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('studentDashboardProgramBox', [
    '$injector',

    function factory($injector) {
        const MbaApplicationMixin = $injector.get('MbaApplicationMixin');
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
        const CertificateHelperMixin = $injector.get('Stream.CertificateHelperMixin');
        const Cohort = $injector.get('Cohort');
        const TranslationHelper = $injector.get('TranslationHelper');
        const translationHelper = new TranslationHelper('lessons.stream.student_dashboard_program_box');
        const dateHelper = $injector.get('dateHelper');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                currentUser: '<',
                foundationPlaylist: '<',
                numPlaylists: '<',
                showAcceptedMessage: '<',
                onClose: '&',
                programComplete: '<?',
            },
            link(scope) {
                MbaApplicationMixin.onLink(scope);
                NavigationHelperMixin.onLink(scope);
                CertificateHelperMixin.onLink(scope);
                scope.dateHelper = dateHelper;

                //------------------------
                // Branding
                //------------------------

                setupBrandEmailProperties($injector, scope, ['emba', 'mba']);

                Object.defineProperty(scope, 'brandEmail', {
                    get() {
                        // Unlike brandNameShort and brandStyleClass, we don't need to check if notApplied
                        // here because the locale messaging shown to the user when they haven't applied
                        // doesn't have an email address in it.
                        const programType = scope.currentUser.programType;
                        const prop = `brand${programType.charAt(0).toUpperCase()}${programType.slice(1)}Email`;
                        return scope[prop];
                    },
                });

                let notApplied; // updated in setProgramBoxStatus
                Object.defineProperty(scope, 'brandNameShort', {
                    get() {
                        // When the user hasn't applied to a cohort yet, we try to upsell the MBA programs.
                        // Once we enable Quantic, the upsell message should show Quantic instead of Smartly
                        // since the MBA programs are considered to be part of Quantic; not Smartly.
                        const brand = notApplied || scope.currentUser?.shouldSeeQuanticBranding ? 'quantic' : 'smartly';
                        return formattedBrandName('short', brand);
                    },
                });

                scope.brandStyleClass = function () {
                    // When the user hasn't applied to a cohort yet, we try to upsell the MBA programs.
                    // Once we enable Quantic, the upsell message should show Quantic instead of Smartly
                    // since the MBA programs are considered to be part of Quantic; not Smartly.
                    if (notApplied) {
                        return 'quantic';
                    }
                    return getBrandStyleClass(scope.currentUser); // delegate to the standard helper when user has applied
                };

                //---------------------------
                // Action buttons
                //---------------------------

                scope.gotoMbaSettings = () => {
                    // First check the reapplyingOrEditingApplication flag on the user.
                    // If the user ends the session, the flag will be gone, in which case we check
                    // ClientStorage for the value.
                    if (scope.currentUser.reapplyingOrEditingApplication) {
                        scope.loadRoute('/settings/application');
                    } else {
                        scope.loadRoute('/settings/application_status');
                    }
                };

                //----------------------------
                // Watches
                //----------------------------

                function setProgramBoxStatus() {
                    if (!scope.currentUser) {
                        return;
                    }

                    // We have a standardized structure for the translations to generate the appropriate subtitle.
                    //
                    // Components (applicable statuses):
                    // - program_type: self explanatory
                    // - status: status of applicable cohort application, or 'not_applied'
                    // - not_started? (accepted): whether the user has just been accepted and not started yet
                    // - foundations_complete? (pending, accepted): whether the user has finished the foundations playlist
                    // - no_reapply_cta? (rejected): whether the user should see a reapply call-to-action
                    //
                    // For EMBA, we also add the following statuses:
                    // - status: include 'pre_accepted' as a possible status
                    // - registered? (pre_accepted): whether the user is registered for the class or not
                    //
                    // from these components, generate the appropriate locale string. See the _spec for this directive
                    // for a breakout of all of the locale keys that are applicable to a given program_type.

                    const program_type = scope.currentUser.programType;
                    const relevantCohort = scope.currentUser.relevant_cohort;
                    let status = scope.currentUser.lastCohortApplicationStatus || 'not_applied';
                    const registered_last_application =
                        scope.currentUser.lastCohortApplication && scope.currentUser.lastCohortApplication.registered;

                    // special status cases:
                    //  - if you're accepted, even to an "older" application because of monkey business in the admin tools, you should be accepted
                    //  - if you're accepted and have completed all of the program, you get a special status
                    //  - if you're in one of several states, collapse the status to 'applied' for this directive's purposes
                    //  - if you're accepted and have completed all of the required courses
                    //  - if you're in the EMBA and pre_accepted
                    if (scope.currentUser.acceptedCohortApplication && !!scope.programComplete) {
                        // auto-graded cohorts can show a failure message
                        if (
                            scope.currentUser.acceptedCohortApplication.graduation_status === 'failed' &&
                            Cohort.supportsAutograding(program_type)
                        ) {
                            status = 'program_failed';
                        } else {
                            status = 'program_complete';
                        }
                    } else if (scope.currentUser.acceptedCohortApplication) {
                        status = 'accepted';
                    } else if (
                        scope.currentUser.lastCohortApplicationStatus === 'pre_accepted' &&
                        _.contains(['mba', 'emba'], program_type)
                    ) {
                        status = 'pre_accepted';
                    } else if (_.contains(['pending', 'pre_accepted'], scope.currentUser.lastCohortApplicationStatus)) {
                        status = 'applied';
                    }

                    const not_started = scope.showAcceptedMessage && status === 'accepted' ? 'not_started' : undefined;
                    const foundations_complete =
                        !!scope.foundationsComplete &&
                        (status === 'applied' || (status === 'accepted' && !!not_started))
                            ? 'foundations_complete'
                            : undefined;

                    let no_reapply_cta;
                    if (Cohort.supportsAdmissionRounds(relevantCohort.program_type)) {
                        // MBA and EMBA have slightly different re-application requirements compared to other
                        // program types. Instead of asking "is the application rejected for the currently
                        // promoted cohort", the question is "is it far enough past the applied to cohort's decision
                        // date for this user to apply for another cohort?".
                        no_reapply_cta =
                            scope.currentUser.lastCohortApplication?.status === 'rejected' &&
                            !scope.currentUser.lastCohortApplication.canReapplyTo(relevantCohort)
                                ? 'no_reapply_cta'
                                : undefined;
                    } else {
                        no_reapply_cta = scope.currentUser.isRejectedForPromotedCohort ? 'no_reapply_cta' : undefined;
                    }

                    let registered;
                    if (program_type === 'emba' && status === 'pre_accepted') {
                        registered = registered_last_application ? 'registered' : 'not_registered';
                        scope.registrationDeadline = dateHelper.formattedUserFacingMonthDayYearLong(
                            scope.currentUser.relevant_cohort.registrationDeadline,
                        );
                    }

                    if (
                        scope.currentUser.lastCohortApplication &&
                        scope.currentUser.lastCohortApplication.rejected_after_pre_accepted &&
                        relevantCohort.supportsAdmissionRounds
                    ) {
                        status = 'rejected_after_pre_accepted';
                    }

                    // decide whether to show additional UI elements
                    scope.showProgressBar =
                        status === 'applied' && relevantCohort.studentDashboardProgramBoxShowProgressBar;
                    notApplied = status === 'not_applied';
                    scope.showApplyButton = notApplied;
                    scope.showRegisterButton =
                        status === 'pre_accepted' && relevantCohort.supportsPayments && !registered_last_application;
                    scope.showCertificateDownloadArea =
                        status === 'program_complete' && Cohort.supportsCertificateDownload(program_type);
                    scope.showFaqButton =
                        !scope.showCertificateDownloadArea && (status === 'accepted' || status === 'program_complete');

                    // whether to make it taller
                    scope.taller = scope.showProgressBar || scope.showApplyButton || scope.showCertificateDownloadArea;

                    // construct the locale keys
                    scope.mobileStatusKey = _.compact(['mobile', 'status', status, no_reapply_cta, registered]).join(
                        '_',
                    );

                    if (status === 'not_applied') {
                        scope.titleHtml = translationHelper.get('mba_programs', { brandName: scope.brandNameShort });
                        scope.subtitleKey = 'subtitle_not_applied';
                    } else {
                        scope.titleHtml = relevantCohort.studentDashboardProgramBoxTitleHtml;
                        scope.subtitleKey = _.compact([
                            'subtitle',
                            relevantCohort.studentDashboardProgramBoxSubtitleKey,
                            status,
                            not_started,
                            foundations_complete,
                            no_reapply_cta,
                            registered,
                        ]).join('_');
                    }

                    // used to decide whether to show special status styling on mobile
                    scope.rejected = _.contains(
                        ['rejected', 'expelled', 'deferred', 'program_failed', 'rejected_after_pre_accepted'],
                        status,
                    );
                    scope.pre_accepted = status === 'pre_accepted';

                    scope.showPreAcceptedMessage =
                        scope.pre_accepted && !scope.showRegisterButton && !!scope.mobileStatusKey;

                    // whether to align sub-title to the right
                    scope.noRight =
                        !scope.showCertificateDownloadArea &&
                        !scope.showApplyButton &&
                        !scope.showFaqButton &&
                        !scope.showRegisterButton &&
                        !scope.showProgressBar &&
                        !scope.rejected;

                    // used in some CSS styling
                    scope.programType = program_type;
                }

                scope.$watch('currentUser.hasPendingOrPreAcceptedCohortApplication', setProgramBoxStatus);

                // canCalculateComplete will be false until all of the streams in the foundation playlist are loaded.  It is
                // a little expensive to calculate, and once it's true it should always be true, so stop watching for it once it is true
                scope.foundationsComplete = null;
                const stopWatchingCanCalculateComplete = scope.$watch(
                    'foundationPlaylist.canCalculateComplete',
                    canCalculateComplete => {
                        if (canCalculateComplete) {
                            scope.foundationsComplete = scope.foundationPlaylist.complete;
                            setProgramBoxStatus();
                            stopWatchingCanCalculateComplete();
                        }
                    },
                );
            },
        };
    },
]);
