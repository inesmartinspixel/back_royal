import angularModule from 'Lessons/angularModule/scripts/lessons_module';
import template from 'Lessons/angularModule/views/stream/keep_learning.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('keepLearning', [
    '$injector',

    function factory($injector) {
        const Stream = $injector.get('Lesson.Stream');
        const RouteAnimationHelper = $injector.get('RouteAnimationHelper');
        const FeedbackModal = $injector.get('FeedbackModal');
        const ContentAccessHelper = $injector.get('ContentAccessHelper');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                streams: '<',
                whenEmpty: '<',
            },
            link(scope) {
                let contentAccessHelper;

                //----------------------------
                // Misc Scope Logic
                //----------------------------

                scope.launchKeepLearning = () => {
                    // There are no expected cases where you can get a keep learning box
                    // with an unlaunchable lesson, but just to be sure. (To repro, start
                    // a stream and then change it to coming soon and go to the stream
                    // dashboard)
                    if (!contentAccessHelper.canLaunch) {
                        return;
                    }

                    if (!scope.streams && scope.whenEmpty === 'requestFeedback') {
                        FeedbackModal.launchFeedback();
                    } else if (!scope.streams) {
                        scope.openLibrary();
                    } else {
                        scope.keepLearning.lesson.launch(`keep_learning_${scope.keepLearning.lesson.launchText}`);
                    }
                };

                scope.formattedLessonMinutes = lesson => String.padNumber(Math.ceil(lesson.approxLessonMinutes), 2);

                function updateKeepLearning(streams) {
                    if (!streams) {
                        return;
                    }

                    scope.keepLearning = Stream.keepLearningStreamAndLesson(streams);

                    if (!scope.keepLearning) {
                        return;
                    }

                    contentAccessHelper = new ContentAccessHelper(scope.keepLearning.lesson);
                }

                scope.openLibrary = () => {
                    RouteAnimationHelper.animatePathChange('/library', 'slide-left');
                };

                //----------------------------
                // Watches
                //----------------------------

                scope.$watch('streams', () => {
                    if (_.isArray(scope.streams)) {
                        updateKeepLearning(scope.streams);
                    } else if (scope.streams) {
                        updateKeepLearning([scope.streams]);
                    } else {
                        updateKeepLearning();
                    }
                });
            },
        };
    },
]);
