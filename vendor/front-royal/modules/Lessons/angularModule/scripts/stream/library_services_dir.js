import angularModule from 'Lessons/angularModule/scripts/lessons_module';
import { setupBrandEmailProperties } from 'AppBrandMixin';
import template from 'Lessons/angularModule/views/stream/library_services.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('libraryServices', [
    '$injector',

    function factory($injector) {
        const $rootScope = $injector.get('$rootScope');
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
        const ConfigFactory = $injector.get('ConfigFactory');

        return {
            restrict: 'E',
            templateUrl,
            scope: {},
            link(scope) {
                NavigationHelperMixin.onLink(scope);
                setupBrandEmailProperties($injector, scope, ['library']);

                // referenced inside `setupBrandEmailProperties`
                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                scope.goToProQuest = () => {
                    scope.loadUrl('https://search.proquest.com/abicomplete/embedded/ENJIZ2M6M79NUV7I', '_blank');
                };

                scope.goToStatista = () => {
                    scope.loadUrl(ConfigFactory.getSync().statista_url, '_blank');
                };
            },
        };
    },
]);
