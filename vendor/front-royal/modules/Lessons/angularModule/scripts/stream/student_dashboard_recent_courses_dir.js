import angularModule from 'Lessons/angularModule/scripts/lessons_module';
import template from 'Lessons/angularModule/views/stream/student_dashboard_recent_courses.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('studentDashboardRecentCourses', [
    '$injector',

    function factory($injector) {
        const RouteAnimationHelper = $injector.get('RouteAnimationHelper');
        const EventLogger = $injector.get('EventLogger');
        const TranslationHelper = $injector.get('TranslationHelper');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                recentStreams: '<',
            },
            link(scope) {
                //---------------------------
                // Initialization
                //---------------------------

                new TranslationHelper('lessons.stream.student_dashboard_recent_courses');

                //---------------------------
                // Navigation
                //---------------------------

                scope.openStreamDashboard = stream => {
                    EventLogger.log('student-dashboard:recent-courses');
                    RouteAnimationHelper.animatePathChange(stream.streamDashboardPath, 'slide-left');
                };

                //----------------------------
                // Watches
                //----------------------------
            },
        };
    },
]);
