import angularModule from 'Lessons/angularModule/scripts/lessons_module';
import template from 'Lessons/angularModule/views/stream/student_dashboard_offline_box.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('studentDashboardOfflineBox', [
    '$injector',

    function factory() {
        return {
            restrict: 'E',
            scope: {},
            templateUrl,
            link(scope) {
                scope.proxy = {};

                scope.toggleOfflineBox = () => {
                    $('.app-main-container > ng-include > [ng-view] > div').toggleClass('offline-box-open');
                    scope.proxy.offlineBoxOpen = !scope.proxy.offlineBoxOpen;
                };
            },
        };
    },
]);
