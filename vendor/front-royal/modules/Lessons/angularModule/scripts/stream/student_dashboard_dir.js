import angularModule from 'Lessons/angularModule/scripts/lessons_module';
import template from 'Lessons/angularModule/views/stream/student_dashboard.html';
import studentDashboardSidebarTemplate from 'Lessons/angularModule/views/stream/student_dashboard_sidebar.html';
import topMessageBoxTemplate from 'Lessons/angularModule/views/stream/student_dashboard_top_message_box.html';
import coursesTopicTemplate from 'Lessons/angularModule/views/stream/student_dashboard_courses_topic.html';
import toggleableCourseListButtonsTemplate from 'Lessons/angularModule/views/stream/toggleable_course_list_buttons.html';
import coursesFlatTemplate from 'Lessons/angularModule/views/stream/student_dashboard_courses_flat.html';
import efficacySidebarTemplate from 'Lessons/angularModule/views/stream/student_dashboard_efficacy_sidebar.html';
import cacheAngularTemplate from 'cacheAngularTemplate';
import DisconnectedError from 'FrontRoyalStore/DisconnectedError';

import iconFreeCourses from 'vectors/icon-free-courses.svg';
import iconCompleteApplication from 'vectors/icon-complete-application.svg';
import iconLearnMore from 'vectors/icon-learn-more.svg';

const templateUrl = cacheAngularTemplate(angularModule, template);

cacheAngularTemplate(angularModule, 'Lessons/student_dashboard_sidebar.html', studentDashboardSidebarTemplate);

angularModule.directive('studentDashboard', [
    '$injector',

    function factory($injector) {
        const $rootScope = $injector.get('$rootScope');
        const Playlist = $injector.get('Playlist');
        const StreamDashboardDirHelper = $injector.get('Stream.StreamDashboardDirHelper');
        const $filter = $injector.get('$filter');
        const AppHeaderViewModel = $injector.get('Navigation.AppHeader.AppHeaderViewModel');
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
        const RouteAnimationHelper = $injector.get('RouteAnimationHelper');
        const ShareService = $injector.get('Navigation.ShareService');
        const SiteMetadata = $injector.get('SiteMetadata');
        const EventLogger = $injector.get('EventLogger');
        const DialogModal = $injector.get('DialogModal');
        const HasToggleableDisplayMode = $injector.get('HasToggleableDisplayMode');
        const TranslationHelper = $injector.get('TranslationHelper');
        const LearnerContentCache = $injector.get('LearnerContentCache');
        const Stream = $injector.get('Lesson.Stream');
        const ClientStorage = $injector.get('ClientStorage');
        const Cohort = $injector.get('Cohort');
        const $window = $injector.get('$window');
        const CareersNetworkViewModel = $injector.get('CareersNetworkViewModel');
        const ContentAccessHelper = $injector.get('ContentAccessHelper');
        const isMobile = $injector.get('isMobileMixin');
        const CohortApplication = $injector.get('CohortApplication');
        const UserIdVerificationViewModel = $injector.get('UserIdVerificationViewModel');
        const offlineModeManager = $injector.get('offlineModeManager');

        return {
            scope: {},
            restrict: 'E',
            templateUrl,
            controllerAs: 'controller',

            link(scope) {
                scope.topMessageBoxTemplate = topMessageBoxTemplate;
                scope.coursesTopicTemplate = coursesTopicTemplate;
                scope.toggleableCourseListButtonsTemplate = toggleableCourseListButtonsTemplate;
                scope.coursesFlatTemplate = coursesFlatTemplate;
                scope.efficacySidebarTemplate = efficacySidebarTemplate;

                isMobile.onLink(scope);

                //---------------------------
                // Initialization
                //---------------------------

                scope.offlineModeManager = offlineModeManager;
                const translationHelper = new TranslationHelper('lessons.stream.student_dashboard');
                let hasAvailableIncompleteStreams;

                // setup EventLogger label exceptions
                EventLogger.allowEmptyLabel([
                    'student-dashboard:completed-courses',
                    'student-dashboard:keep-learning',
                    'student-dashboard:recent-courses',
                    'student-dashboard:download-program-certificate',
                ]);

                // Get current user onto the scope
                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                if (scope.currentUser.hasCareersNetworkAccess) {
                    scope.careersNetworkViewModel = CareersNetworkViewModel.get('candidate');
                    scope.careersNetworkViewModel.nagIfNecessary();
                }

                StreamDashboardDirHelper.onLink(scope);
                NavigationHelperMixin.onLink(scope);
                HasToggleableDisplayMode.onLink(scope, 'toggleableCourseListStudentDashboard', true, true);

                if (
                    scope.currentUser.roleName() === 'learner' &&
                    !scope.currentUser.hasExternalInstitution &&
                    !scope.currentUser.isDemo &&
                    !scope.currentUser.hasEverApplied
                ) {
                    DialogModal.alert({
                        title: translationHelper.get('welcome_to_your_dashbaord'),
                        content:
                            `<div class="col-xs-12 col-sm-4"><img src="${iconFreeCourses}"><p translate-once="lessons.stream.student_dashboard.try_any_free_lessons" translate-compile></p></div>` +
                            `<div class="col-xs-12 col-sm-4"><img src="${iconCompleteApplication}"><p translate-once="lessons.stream.student_dashboard.complete_your_application" translate-compile></p></div>` +
                            `<div class="col-xs-12 col-sm-4"><img src="${iconLearnMore}"><p translate-once="lessons.stream.student_dashboard.need_more_info" translate-compile></p></div>`,
                        classes: ['welcome-to-dashboard', 'medium-large'],
                        scope: {
                            closeModal: () => {
                                DialogModal.hideAlerts();
                            },
                            completeApplication: () => {
                                scope.loadRoute('/settings/application');
                            },
                            learnMore: () => {
                                scope.loadUrl('/programs', '_blank');
                            },
                        },
                    });
                }

                // Set up header
                AppHeaderViewModel.setBodyBackground('beige');
                AppHeaderViewModel.showAlternateHomeButton = false;
                AppHeaderViewModel.setTitleHTML(translationHelper.get('home_title'));

                // Set up title
                SiteMetadata.updateHeaderMetadata();

                // ensure that we're at the top of the page when we navigate to here
                $injector.get('scrollHelper').scrollToTop();

                //---------------------------
                // Navigation
                //---------------------------

                scope.openLibrary = () => {
                    RouteAnimationHelper.animatePathChange('/library', 'slide-left');
                };

                scope.openSettings = () => {
                    RouteAnimationHelper.animatePathChange('/settings', 'slide-left');
                };

                scope.browseTopic = topicName => {
                    RouteAnimationHelper.animatePathChange(`/library?topics[]=${topicName}`, 'slide-left');
                };

                //---------------------------
                // Display Helpers
                //---------------------------

                // determine if user in the special blue ocean high school group
                Object.defineProperty(scope, 'blueOceanHighSchooler', {
                    get() {
                        return scope.currentUser && scope.currentUser.sign_up_code === 'BOSHIGH';
                    },
                });

                // whether to show the course little toggle button
                Object.defineProperty(scope, 'showToggleableCourseButtons', {
                    get() {
                        return !scope.blueOceanHighSchooler;
                    },
                });

                // whether to show the project resources box
                Object.defineProperty(scope, 'showCohortLevelLearnerProjectResources', {
                    get() {
                        // Return true if this cohort supports top-level projects and
                        // has both project_submission_email and projects
                        return (
                            Cohort.supportsCohortLevelProjects(scope.currentUser.programType) &&
                            scope.relevantCohort.project_submission_email &&
                            angular.isDefined(scope.relevantCohort.learner_projects) &&
                            scope.relevantCohort.learner_projects.length > 0
                        );
                    },
                });

                // whether to show the recent streams box
                Object.defineProperty(scope, 'showRecentStreams', {
                    get() {
                        return (
                            scope.recentStreams &&
                            scope.recentStreams.length > 0 &&
                            !scope.topNavInfo &&
                            !offlineModeManager.inOfflineMode
                        );
                    },
                });

                // whether to show the new user welcome box
                Object.defineProperty(scope, 'showWelcomeBox', {
                    get() {
                        //  - rejected and expelled users don't see the welcome box
                        //  - pre_accepted to something other than MBA or EMBA don't see it
                        //  - accepted to something other than Biz Cert or CNO don't see it
                        //  - external-institution users without a relevant cohort shouldn't see the welcome box
                        if (
                            scope.currentUser.isRejectedOrExpelledOrDeferred ||
                            (scope.currentUser.hasPreAcceptedCohortApplication && !scope.currentUser.mba_enabled) ||
                            (scope.currentUser.hasPreAcceptedCohortApplication &&
                                scope.currentUser.can_purchase_paid_certs) ||
                            (scope.currentUser.acceptedCohortApplication &&
                                !_.contains(
                                    ['the_business_certificate', 'career_network_only'],
                                    scope.currentUser.programType,
                                )) ||
                            (!scope.currentUser.relevant_cohort && !scope.currentUser.hasExternalInstitution)
                        ) {
                            return false;
                            //  for external-institution users, it depends on if they have started any stream, or have other messaging
                        }

                        if (scope.currentUser.hasExternalInstitution) {
                            // For external-institution users we will dismiss the welcome box after lesson progress.
                            return !scope.showRecentStreams && !scope.topNavInfo && !scope.showEfficacyStudyTips;

                            // for the rest of the cases we will continue to display the welcome box, unless special conditions are met
                        }

                        return !scope.topNavInfo && !scope.showEfficacyStudyTips;
                    },
                });

                // whether to show the efficacy study tips
                Object.defineProperty(scope, 'showEfficacyStudyTips', {
                    get() {
                        return scope.currentUser && scope.currentUser.inEfficacyStudy && !scope.topNavInfo;
                    },
                });

                Object.defineProperty(scope, 'showResearchResources', {
                    get() {
                        return (
                            scope.currentUser &&
                            !offlineModeManager.inOfflineMode &&
                            ((scope.currentUser.graduationStatus === 'pending' &&
                                Cohort.supportsResearchResourcesSidebar(scope.currentUser.programType)) ||
                                scope.currentUser.hasSuperEditorAccess ||
                                scope.currentUser.hasInterviewerAccess)
                        );
                    },
                    configurable: true,
                });

                scope.$watch('showResearchResources', () => {
                    if (angular.isDefined(scope.showResearchResources) && !scope.showResearchResources) {
                        $injector.get('$location').search('libraryservices', null);
                    }
                });

                //---------------------------
                // User Specific Display
                //---------------------------

                // Signal that we should no longer send a mobile user through onboarding after they have
                // successfully authenticated and loaded the dashboard. This prevents the user from seeing
                // the onboarding again if they log out and close the app.
                if ($window.CORDOVA) {
                    ClientStorage.setItem('skip_onboarding', true);
                }

                const stopWatchingNewlyAccepted = scope.$watch('currentUser.newlyAccepted', newlyAccepted => {
                    // Determine if the application accepted message should be displayed
                    if (newlyAccepted) {
                        if (Cohort.supportsAcceptanceMessage(scope.currentUser.programType)) {
                            DialogModal.alert({
                                content: '<application-accepted></application-accepted>',
                                size: 'fullscreen',
                                hideCloseButton: true,
                                closeOnClick: false,
                                scope: {},
                                classes: ['accepted-application-modal'],
                            });

                            // Only set the first digest so that the subsequent save does not overwrite
                            if (scope.showAcceptedMessage === undefined) {
                                scope.showAcceptedMessage = newlyAccepted;

                                if (newlyAccepted && !scope.currentUser.ghostMode) {
                                    scope.currentUser.hasSeenAccepted = true;
                                    scope.currentUser.save();
                                }
                            }
                        }

                        scope.showSchedule =
                            Cohort.supportsSchedule(scope.currentUser.programType) && scope.currentUser.isAccepted;
                        stopWatchingNewlyAccepted();
                    }
                });

                Object.defineProperty(scope, 'showScheduleInterview', {
                    get() {
                        return (
                            scope.currentUser.lastCohortApplication &&
                            scope.currentUser.lastCohortApplication.invited_to_interview === true
                        );
                    },
                    configurable: true,
                });

                //---------------------------
                // Enrollment Sidebar Box
                //---------------------------

                function getEnrollmentSidebarTodos(user) {
                    if (!user || !user.career_profile) {
                        return undefined;
                    }

                    const userIdVerificationViewModel = new UserIdVerificationViewModel();
                    userIdVerificationViewModel.launchVerificationModalIfLastIdologyVerificationFailed();

                    // For the todo_complete_student_profile enrollment todo, when the user clicks on the todo,
                    // we'd like to send them to the first incomplete step in /settings/my-profile area. Since
                    // the logic to determine the incomplete steps is somewhat computationally intensive, we
                    // perform this logic outside of the todo_complete_student_profile onCLick handler and only
                    // if we see that the todo_complete_student_profile enrollment todo is marked as incomplete.
                    let firstIncompleteStepPage = 0;
                    const completeStudentProfileTodoIsVisibleHandler = () => true;
                    const completeStudentProfileTodoIsDisabledHandler = () =>
                        user.career_profile.last_calculated_complete_percentage === 100;
                    if (
                        completeStudentProfileTodoIsVisibleHandler() &&
                        !completeStudentProfileTodoIsDisabledHandler()
                    ) {
                        const EditCareerProfileHelper = $injector.get('EditCareerProfileHelper');
                        const ProfileCompletionHelper = $injector.get('ProfileCompletionHelper');
                        const steps = EditCareerProfileHelper.getStepsForCareersForm(
                            $injector.get('CAREER_PROFILE_FORM_STEPS'),
                        );
                        const profileCompletionHelper = new ProfileCompletionHelper(steps);
                        const stepsProgressMap = profileCompletionHelper.getStepsProgressMap(user.career_profile);
                        firstIncompleteStepPage =
                            _.findIndex(steps, step => stepsProgressMap[step.stepName] === 'incomplete') + 1; // 1-indexed
                    }

                    return [
                        {
                            name: 'todo_enrollment_agreement',
                            localeKey: 'sign_enrollment_agreement',
                            isVisible: () =>
                                user.lastCohortApplication.hasSignedEnrollmentAgreement ||
                                user.lastCohortApplication.hasActiveEnrollmentAgreementSigningLink,
                            isDisabled: () => user.lastCohortApplication.hasSignedEnrollmentAgreement,
                            onClick: () =>
                                scope.loadUrl(
                                    user.lastCohortApplication.activeEnrollmentAgreementSigningLink,
                                    '_blank',
                                ),
                        },
                        {
                            // FIXME: remove this todo when the IDology work is fully integrated
                            name: 'todo_id',
                            localeKey: 'upload_your_id',
                            isVisible: () => user.recordsIndicateUserShouldUploadIdentificationDocument,
                            isDisabled: () => user.identity_verified,
                            inReview: () => !user.missingIdentificationDocument && !user.identity_verified,
                            onClick: () => scope.loadRoute('/settings/documents'),
                        },
                        {
                            name: 'todo_transcripts',
                            localeKey: 'send_us_your_transcripts',
                            isVisible: () => user.careerProfileIndicatesUserShouldProvideTranscripts,
                            isDisabled: () => user.transcripts_verified,
                            inReview: () =>
                                user.hasUploadedTranscripts && !user.missingTranscripts && !user.transcripts_verified,
                            onClick: () =>
                                scope.loadUrl(
                                    '/help/article/54-am-i-required-to-provide-official-documents-transcript-diploma-etc',
                                    '_blank',
                                ),
                            subTodosConfig: {
                                isVisible: () => user.careerProfileIndicatesUserShouldProvideTranscripts,
                                getSubTodos: () => user.career_profile.educationExperiencesIndicatingTranscriptRequired,
                                isDisabled: educationExperience =>
                                    educationExperience.transcriptApprovedOrWaived || user.transcripts_verified,
                                inReview: educationExperience =>
                                    educationExperience.transcriptInReview && !user.transcripts_verified,
                                getSubTodoText: educationExperience =>
                                    educationExperience.degreeAndOrgNameWithTranscriptRequirementString,
                            },
                        },
                        {
                            name: 'todo_english_language',
                            localeKey: 'upload_english_language_documents',
                            isVisible: () =>
                                user.careerProfileIndicatesUserShouldUploadEnglishLanguageProficiencyDocuments,
                            isDisabled: () => user.english_language_proficiency_documents_approved,
                            inReview: () =>
                                !user.missingEnglishLanguageProficiencyDocuments &&
                                !user.english_language_proficiency_documents_approved,
                            onClick: () => scope.loadRoute('/settings/documents'),
                        },
                        {
                            name: 'todo_id_verify',
                            localeKey: 'verify_your_identity',
                            isVisible: () => user.relevant_cohort.activeIdVerificationPeriod,
                            isDisabled: () => !user.unverifiedForCurrentIdVerificationPeriod,
                            onClick: () => userIdVerificationViewModel.launchVerificationModal(),
                        },
                        {
                            name: 'todo_address',
                            localeKey: 'provide_mailing_address',
                            isVisible: () => true,
                            isDisabled: () => !user.missingAddress,
                            onClick: () => scope.loadRoute('/settings/documents'),
                        },
                        {
                            name: 'todo_complete_student_profile',
                            localeKey: 'complete_student_profile',
                            isVisible: completeStudentProfileTodoIsVisibleHandler,
                            isDisabled: completeStudentProfileTodoIsDisabledHandler,
                            onClick: () => scope.loadRoute(`/settings/my-profile?page=${firstIncompleteStepPage}`),
                        },
                    ];
                }
                scope.enrollmentSidebarTodos = getEnrollmentSidebarTodos(scope.currentUser);

                scope.showHaveQuestions =
                    _.filter(
                        scope.currentUser.career_profile && scope.currentUser.career_profile.education_experiences,
                        e => e.degreeProgram && !e.will_not_complete && e.graduation_year <= new Date().getFullYear(),
                    ).length > 3;

                Object.defineProperty(scope, 'showEnrollment', {
                    get() {
                        const user = scope.currentUser;
                        // user.needsToRegister should be false because we want EMBA registration to take
                        // priority over enrollment. After they've registered, then show the enrollment sidebar
                        return (
                            !offlineModeManager.inOfflineMode &&
                            user.programType &&
                            Cohort.supportsEnrollmentSidebar(user.programType) &&
                            _.contains(CohortApplication.VALID_ENROLLMENT_STATUSES, user.lastCohortApplicationStatus) &&
                            !_.contains(CohortApplication.VALID_GRADUATED_STATUSES, user.graduationStatus) &&
                            !user.needsToRegister &&
                            !!user.relevant_cohort &&
                            ((user.recordsIndicateUserShouldUploadIdentificationDocument && !user.identity_verified) ||
                                (user.careerProfileIndicatesUserShouldProvideTranscripts &&
                                    !user.transcripts_verified) ||
                                (user.careerProfileIndicatesUserShouldUploadEnglishLanguageProficiencyDocuments &&
                                    !user.english_language_proficiency_documents_approved) ||
                                user.missingAddress ||
                                user.lastCohortApplication.hasActiveEnrollmentAgreementSigningLink ||
                                user.career_profile.last_calculated_complete_percentage !== 100)
                        );
                    },
                    configurable: true,
                });

                // These values are collected outside of the `onlyNeedsToCompleteCareerProfileForEnrollment` getter for performance reasons
                const completeCareerProfileEnrollmentTodoIndex = _.findIndex(
                    scope.enrollmentSidebarTodos,
                    todo => todo.name === 'todo_complete_student_profile',
                );

                // See https://trello.com/c/bI5TfZuH
                Object.defineProperty(scope, 'onlyNeedsToCompleteCareerProfileForEnrollment', {
                    get() {
                        if (scope.showEnrollment) {
                            const completeCareerProfileEnrollmentTodo =
                                scope.enrollmentSidebarTodos[completeCareerProfileEnrollmentTodoIndex];
                            if (
                                completeCareerProfileEnrollmentTodo.isVisible(scope.currentUser) &&
                                !completeCareerProfileEnrollmentTodo.isDisabled(scope.currentUser)
                            ) {
                                const otherIncompleteEnrollmentTodo = _.find(
                                    scope.enrollmentSidebarTodos,
                                    todo =>
                                        todo.name !== 'todo_complete_student_profile' &&
                                        todo.isVisible(scope.currentUser) &&
                                        !todo.isDisabled(scope.currentUser),
                                );
                                return !otherIncompleteEnrollmentTodo;
                            }
                        }
                        return false;
                    },
                    configurable: true,
                });

                // If the "complete career profile" enrollment todo is the only incomplete todo item,
                // then we show the enrollment sidebar below the other schedule related sidebar boxes
                // on the student dashboard. See https://trello.com/c/bI5TfZuH
                Object.defineProperty(scope, 'enrollmentSidebarPlacement', {
                    get() {
                        return scope.onlyNeedsToCompleteCareerProfileForEnrollment ? 'below' : 'above';
                    },
                });

                //---------------------------
                // Sharing Helper (not used since tour was taken out, but will likely reappear soon)
                //---------------------------

                scope.share = (provider, label = 'tour-end') => {
                    ShareService.share(label, provider, SiteMetadata.smartlyShareInfo(scope.currentUser));
                };

                //---------------------------
                // Parsing for Display Flat List of Courses View
                //---------------------------

                let myCourses;

                let completedCourses;

                // creates streams, grouped into separate columns
                function createStreamGroups(streams) {
                    if (!streams) {
                        return;
                    }

                    scope.myCoursesStreamGroup = {
                        title: translationHelper.get('my_courses_title'),
                        anchor: 'my-courses',
                        streams: [],
                    };
                    myCourses = scope.myCoursesStreamGroup;

                    scope.completedCoursesStreamGroup = {
                        title: translationHelper.get('completed_courses_title'),
                        anchor: 'completed-courses',
                        streams: [],
                    };
                    completedCourses = scope.completedCoursesStreamGroup;

                    const inProgressCourses = [];
                    const bookmarkedCourses = [];
                    const activePlaylistCourses = [];
                    const orderedStreams = $filter('orderBy')(streams, 'title');
                    orderedStreams.forEach(stream => {
                        let streamGroup;

                        if (stream.progressStatus() === 'completed') {
                            streamGroup = completedCourses;
                        } else {
                            streamGroup = myCourses;
                        }

                        // we only want to feed Keep Learning streams that are in-progress
                        if (stream.progressStatus() === 'in_progress') {
                            inProgressCourses.push(stream);
                        }

                        // Check if it belongs to either the playlist or bookmarked courses, but don't put in both groups
                        if (
                            scope.activePlaylist &&
                            _.contains(scope.activePlaylist.streamLocalePackIds, stream.localePackId)
                        ) {
                            activePlaylistCourses.push(stream);
                        } else if (stream.favorite && stream.progressStatus() !== 'completed') {
                            bookmarkedCourses.push(stream);
                        }

                        // Add the stream to the group
                        streamGroup.streams.push(stream);
                    });

                    const streamGroups = [];
                    [myCourses, completedCourses].forEach(streamGroup => {
                        streamGroups.push(streamGroup);
                    });

                    // Sort completed courses by date completed DESC
                    scope.completedCoursesStreamGroup.streams = _.sortBy(
                        scope.completedCoursesStreamGroup.streams,
                        stream => stream.lastProgressAt,
                    ).reverse();

                    // Set view logic variables
                    scope.topNavInfo = undefined;
                    scope.activePlaylistCourses = activePlaylistCourses;
                    scope.bookmarkedCourses = bookmarkedCourses;
                    scope.streamGroups = streamGroups;
                    scope.keepLearningStream = Stream.keepLearningStream(scope.activePlaylist, myCourses.streams);
                    scope.keepLearningLesson =
                        scope.keepLearningStream && Stream.keepLearningLesson(scope.keepLearningStream);
                    scope.noStreamsAvailable = myCourses.streams.length === 0 && completedCourses.streams.length === 0;
                    scope.allStreamsCompleted = completedCourses.streams.length > 0 && myCourses.streams.length === 0;

                    // This is only relevant to users who do not have any playlists.
                    // The reason for that is that the playlist UI gives an
                    // overall view of how much of the stuff in your curriculum you have completed.
                    // The messages here provide that overall view for people who do not see that
                    // playlists UI.
                    // The vast majority of our users DO have playlists.  The only ones who don't
                    // are demo users and users from institutions with no playlist_pack_ids
                    if (!scope.currentUser.hasPlaylists) {
                        if (scope.noStreamsAvailable) {
                            scope.topNavInfo = {
                                title: translationHelper.get('top_nav_uh_oh'),
                                subTitle: translationHelper.get('top_nav_browse'),
                                classNames: ['prominent'],
                            };
                        } else if (scope.allStreamsCompleted && hasAvailableIncompleteStreams) {
                            scope.topNavInfo = {
                                title: translationHelper.get('top_nav_nice_work'),
                                subTitle: translationHelper.get('top_nav_completed_all'),
                                classNames: ['prominent', 'completed'],
                            };
                        } else if (
                            scope.blueOceanHighSchooler &&
                            scope.allStreamsCompleted &&
                            !scope.hasAvailableIncompleteStreams
                        ) {
                            scope.topNavInfo = {
                                title: translationHelper.get('top_nav_completed'),
                                subTitle: translationHelper.get('top_nav_well_done'),
                                classNames: ['prominent', 'completed'],
                            };
                        } else if (
                            !scope.blueOceanHighSchooler &&
                            scope.allStreamsCompleted &&
                            !hasAvailableIncompleteStreams
                        ) {
                            scope.topNavInfo = {
                                title: translationHelper.get('top_nav_all_complete'),
                                subTitle: '',
                                classNames: ['prominent', 'completed'],
                            };
                        }
                    }

                    // There used to be more complex logic here to decide how to order.  It's possible that
                    // there is still more simplification that can happen here.  (This is used inside of
                    // `student_dashboard_courses_flat.html`)
                    scope.flatStreamOrdering = 'title';
                }

                function updateActivePlaylistForLocalePackId() {
                    // ensure we always handle the single-playlist scenario consistently
                    if (scope.playlists && scope.playlists.length === 1) {
                        // eslint-disable-next-line prefer-destructuring
                        scope.activePlaylist = scope.playlists[0];
                    } else {
                        scope.activePlaylist = _.findWhere(scope.playlists, {
                            localePackId: scope.currentUser.active_playlist_locale_pack_id,
                        });
                    }
                }

                scope.activatePlaylist = playlist => {
                    // skip this junk if it's already the active playlist
                    if (playlist && scope.activePlaylist && playlist.id === scope.activePlaylist.id) {
                        scope.learningBoxMode.mode = 'active_playlist';
                        return;
                    }

                    // save to a local var so that we know not to default to the playlists view after load completes
                    scope.activePlaylist = playlist;
                    scope.currentUser.active_playlist_locale_pack_id = playlist.localePackId;

                    // sometimes we log in as users and change their active playlist
                    // for them, thus the lack of checking for ghostMode
                    scope.currentUser.save();

                    // toggle back to active_playlist mode
                    scope.learningBoxMode.mode = 'active_playlist';
                };

                //---------------------------
                // Data Loading
                //---------------------------

                function loadStudentDashboard() {
                    scope.learningBoxMode = {};
                    const viewAs = $rootScope.viewAs || '';

                    scope.streamGroups = undefined;
                    scope.activePlaylist = undefined;
                    scope.playlists = undefined;
                    scope.relevantCohort = undefined;
                    hasAvailableIncompleteStreams = undefined;

                    if (offlineModeManager.inOfflineMode) {
                        scope.learningBoxMode.mode = 'offline_mode';
                        return;
                    }

                    // load student dashboard data
                    scope.dashboardLoading = true;

                    // rely on learner cache if available
                    LearnerContentCache.ensureStudentDashboard()
                        .then(response => {
                            // If we entered offline mode while this request was
                            // in flight (not sure that's even really possible,
                            // but seems like it could be), then loadStudentDashboard is going to be
                            // called again and we don't want to do any of the rest of this.
                            // (I tried to test this scenario, but since our specs aren't really
                            // asynchronous, I couldn't figure out how to make it fail in specs)
                            if (offlineModeManager.inOfflineMode) {
                                scope.learningBoxMode.mode = 'offline_mode';
                                return;
                            }
                            // Update streams and counts of various statistics
                            const studentDashboard = response.result[0];

                            scope.streamsFromDashboardCall = studentDashboard.lesson_streams;
                            scope.playlists = studentDashboard.available_playlists;

                            updateActivePlaylistForLocalePackId();

                            scope.learningBoxMode.hasSinglePlaylist = scope.playlists && scope.playlists.length === 1;

                            // hasAvailableIncompleteStreams is only defined and relevant for users who have no playlists
                            hasAvailableIncompleteStreams =
                                response.meta && response.meta.has_available_incomplete_streams;

                            // If we viewAs cohort then we need to set the relevantCohort to that cohort rather than the admin user's relevantCohort
                            if (viewAs && viewAs.split(':')[0] === 'cohort') {
                                scope.relevantCohort = _.findWhere($rootScope.availableCohorts, {
                                    id: viewAs.split(':')[1],
                                });
                            } else {
                                scope.relevantCohort = scope.currentUser.relevant_cohort;
                            }

                            // Set the flag if an admin uses viewAs cohort or the user has a relevant_cohort
                            const cohortSupportEnabled = viewAs
                                ? viewAs.split(':')[0] === 'cohort'
                                : !!scope.currentUser.relevant_cohort;

                            // some things only appear if we have a relevant cohort (e.g.: not an external-institution user)
                            if (cohortSupportEnabled) {
                                // determine the first playlist entry in the relevant cohort
                                scope.foundationPlaylist = _.findWhere(scope.playlists, {
                                    localePackId: scope.relevantCohort.foundationsPlaylistLocalePackId,
                                });

                                // detemine the last program's completion status and date
                                scope.programComplete = !!(
                                    scope.currentUser.lastCohortApplication &&
                                    scope.currentUser.lastCohortApplication.completed_at
                                );

                                // program box configuration
                                scope.showProgramBox = true;
                                scope.showSchedule =
                                    Cohort.supportsSchedule(scope.currentUser.programType) &&
                                    scope.currentUser.isAccepted;

                                scope.showCertificateGraduationBox =
                                    Cohort.requiresGraduationForCertificateDownload(scope.currentUser.programType) &&
                                    scope.currentUser.isAccepted &&
                                    scope.programComplete &&
                                    scope.currentUser.lastCohortApplication.graduation_status !== 'failed';

                                scope.numRequiredPlaylists = scope.relevantCohort.getRequiredPlaylists(
                                    scope.playlists,
                                ).length;
                                scope._hideMobileProgramBox =
                                    (scope.currentUser.isRejectedOrExpelledOrDeferredOrFailed &&
                                        !!ClientStorage.getItem('rejected_message_hidden')) ||
                                    (scope.currentUser.isCareerNetworkOnly &&
                                        scope.currentUser.hasPendingOrPreAcceptedCohortApplication) ||
                                    Cohort.hideMobileProgramBox(scope.currentUser.programType);
                            }

                            // decide which mode to show in the learning box
                            if (scope.learningBoxMode.hasSinglePlaylist) {
                                scope.learningBoxMode.mode = 'active_playlist';
                            } else if (scope.showAcceptedMessage) {
                                scope.learningBoxMode.mode = 'playlists';
                            } else if (scope.activePlaylist && !scope.activePlaylist.complete) {
                                // if there's an active, incomplete playlist, show it
                                scope.learningBoxMode.mode = 'active_playlist';
                            } else if (scope.currentUser.hasPlaylists) {
                                // if there are available playlists, show the playlists view
                                scope.learningBoxMode.mode = 'playlists';
                            } else {
                                scope.learningBoxMode.mode = 'course';
                            }

                            // build list of most recent streams with progress
                            scope.recentStreams = _.chain(scope.streamsFromDashboardCall)
                                .filter(stream => stream.started)
                                .sortBy(stream => -stream.lesson_streams_progress.last_progress_at)
                                .first(3)
                                .value();

                            // HasToggleableDisplayMode controls DOM pre-rendering
                            scope.preloadAllDisplayModes();

                            // load any streams not already loaded by the student dashboard call
                            scope.readyToShowPlaylistMap = false;
                            Playlist.loadStreams(scope.playlists).then(() => {
                                // We have to set the recommended playlist inside of this callback to better prevent "No cached stream found
                                // for locale pack" errors (see https://trello.com/c/qi7m0IBX/1547-bug-no-cached-stream-for-locale-pack).
                                if (scope.activePlaylist && scope.activePlaylist.complete) {
                                    const specialization_lpids = scope.relevantCohort
                                        ? scope.relevantCohort.specialization_playlist_pack_ids
                                        : [];

                                    // Don't recommend complete nor specialization playlists (the user chooses those)
                                    let recommendablePlaylists = _.chain(scope.playlists)
                                        .reject('complete')
                                        .reject(playlist => specialization_lpids.includes(playlist.localePackId))
                                        .select(playlist => ContentAccessHelper.canLaunch(playlist))
                                        .value();

                                    if (cohortSupportEnabled) {
                                        // Do not recommend the Business Foundations playlist to
                                        // accepted users in a cohort that should exclude the
                                        // foundations playlist on acceptance.
                                        if (
                                            scope.currentUser.lastCohortApplication &&
                                            scope.currentUser.lastCohortApplication.status === 'accepted' &&
                                            Cohort.excludesFoundationsPlaylistOnAcceptance(
                                                scope.currentUser.programType,
                                            )
                                        ) {
                                            const foundationsPlaylist = _.find(
                                                recommendablePlaylists,
                                                playlist =>
                                                    playlist.localePackId ===
                                                    scope.relevantCohort.foundationsPlaylistLocalePackId,
                                            );
                                            recommendablePlaylists = _.without(
                                                recommendablePlaylists,
                                                foundationsPlaylist,
                                            );
                                        }

                                        // use the playlist order defined by the cohort
                                        scope.recommendedPlaylist = _.chain(recommendablePlaylists)
                                            .sortBy(plist => {
                                                const index = scope.relevantCohort.requiredPlaylistPackIds.indexOf(
                                                    plist.localePackId,
                                                );
                                                return index > -1 ? index : 9999;
                                            })
                                            .first()
                                            .value();
                                    } else {
                                        scope.recommendedPlaylist = _.chain(recommendablePlaylists)
                                            .sortBy('percentComplete')
                                            .last()
                                            .value();
                                    }
                                }

                                scope.readyToShowPlaylistMap = true;
                            });
                        })
                        .catch(err => {
                            // ensureStudentDashboard can throw a DisconnectedError if
                            // we enter offline mode while it is in flight.  If that happens
                            // we can just ignore the error.
                            if (err.constructor !== DisconnectedError) {
                                throw err;
                            }
                        })
                        .finally(() => {
                            scope.dashboardLoading = false;
                        });
                }

                Object.defineProperty(scope, 'showMobileProgramBox', {
                    get() {
                        return (
                            scope.showProgramBox &&
                            !scope._hideMobileProgramBox &&
                            !scope.showSchedule &&
                            !scope.showScheduleInterview &&
                            !offlineModeManager.inOfflineMode
                        );
                    },
                });

                Object.defineProperty(scope, 'showDesktopProgramBox', {
                    get() {
                        return (
                            scope.showProgramBox &&
                            !scope.showSchedule &&
                            !scope.showCertificateGraduationBox &&
                            !scope.showScheduleInterview &&
                            !offlineModeManager.inOfflineMode
                        );
                    },
                });

                scope.hideMobileProgramBox = () => {
                    ClientStorage.setItem('rejected_message_hidden', true);
                    scope._hideMobileProgramBox = true;
                };

                // Watch for the user's active playlist to change in push messages, which
                // can happen if they are accepted into a cohort. See https://trello.com/c/CH2RajXg
                scope.$watch('currentUser.active_playlist_locale_pack_id', newVal => {
                    if (newVal && scope.playlists) {
                        updateActivePlaylistForLocalePackId();
                    }
                });

                scope.$watchGroup(['streamsFromDashboardCall', 'activePlaylist'], () => {
                    createStreamGroups(scope.streamsFromDashboardCall);

                    // helper method added to scope in HasToggleableDisplayMode
                    scope.createTopicGroups(scope.bookmarkedCourses);
                });

                // will also initialize the listing
                scope.$watchGroup([() => $rootScope.viewAs, () => offlineModeManager.inOfflineMode], () => {
                    loadStudentDashboard();
                });

                scope.$on('$destroy', () => {
                    DialogModal.hideAlerts();
                });
            },
        };
    },
]);
