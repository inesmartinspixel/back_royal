import angularModule from 'Lessons/angularModule/scripts/lessons_module';
import template from 'Lessons/angularModule/views/stream/student_dashboard_identity_verification.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('studentDashboardIdentityVerification', [
    '$injector',

    function factory($injector) {
        const UserIdVerificationViewModel = $injector.get('UserIdVerificationViewModel');

        return {
            restrict: 'E',
            templateUrl,
            scope: {},
            link(scope) {
                scope.toggleIdVerification = () => {
                    $('.app-main-container > ng-include > [ng-view] > div').toggleClass('id-verification-open');
                    scope.idVerificationOpen = !scope.idVerificationOpen;
                };

                //------------------------------
                // ID verification
                //------------------------------
                scope.userIdVerificationViewModel = new UserIdVerificationViewModel();
                scope.userIdVerificationViewModel.launchVerificationModalIfLastIdologyVerificationFailed();
            },
        };
    },
]);
