import angularModule from 'Lessons/angularModule/scripts/lessons_module';
import { setupStyleHelpers } from 'AppBrandMixin';
import template from 'Lessons/angularModule/views/stream/student_dashboard_certificate_graduation_box.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('studentDashboardCertificateGraduationBox', [
    '$injector',
    function factory($injector) {
        const CertificateMixin = $injector.get('Stream.CertificateHelperMixin');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                currentUser: '<',
            },
            link(scope) {
                CertificateMixin.onLink(scope);
                setupStyleHelpers($injector, scope);

                scope.proxy = {};

                // controls which UI to show
                Object.defineProperty(scope, 'status', {
                    get() {
                        return scope.currentUser.lastCohortApplication.isGraduated ? 'graduated' : 'not_graduated';
                    },
                    configurable: true,
                });
            },
        };
    },
]);
