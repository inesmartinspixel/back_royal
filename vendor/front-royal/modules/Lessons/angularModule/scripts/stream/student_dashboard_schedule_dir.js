import angularModule from 'Lessons/angularModule/scripts/lessons_module';
import { setupBrandNameProperties, setupBrandEmailProperties, setupStyleHelpers } from 'AppBrandMixin';
import template from 'Lessons/angularModule/views/stream/student_dashboard_schedule.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

import projectResource from 'vectors/project_resource.svg';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('studentDashboardSchedule', [
    '$injector',

    function factory($injector) {
        const $rootScope = $injector.get('$rootScope');
        const isMobileMixin = $injector.get('isMobileMixin');
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
        const TranslationHelper = $injector.get('TranslationHelper');
        const ShareService = $injector.get('Navigation.ShareService');
        const SiteMetadata = $injector.get('SiteMetadata');
        const RouteAnimationHelper = $injector.get('RouteAnimationHelper');
        const EventLogger = $injector.get('EventLogger');
        const dateHelper = $injector.get('dateHelper');
        const FormatsText = $injector.get('FormatsText');
        const ConfigFactory = $injector.get('ConfigFactory');

        return {
            restrict: 'E',
            scope: {
                relevantCohort: '<',
                streams: '<',
            },
            templateUrl,
            link(scope) {
                scope.projectResource = projectResource;

                isMobileMixin.onLink(scope);
                NavigationHelperMixin.onLink(scope);
                setupBrandNameProperties($injector, scope);
                setupBrandEmailProperties($injector, scope, ['support']);
                setupStyleHelpers($injector, scope);
                scope.formatsText = FormatsText;

                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                scope.toggleSchedule = () => {
                    $('.app-main-container > ng-include > [ng-view] > div').toggleClass('schedule-open');
                    scope.proxy.scheduleOpen = !scope.proxy.scheduleOpen;
                };

                // create a lookup object of streams by localePackId
                function indexStreams() {
                    return _.indexBy(scope.streams, 'localePackId');
                }

                let localePackIdHash = indexStreams();

                scope.$watch('streams', () => {
                    localePackIdHash = indexStreams();
                });

                scope.$watch('currentUser.curriculum_status', () => {
                    if (scope.currentUser && scope.relevantCohort) {
                        scope.status = scope.currentUser.curriculum_status;
                        scope.showSchedule =
                            _.contains(
                                ['week_1', 'on_track', 'on_track_finished', 'not_on_track', 'almost_there'],
                                scope.status,
                            ) && !!scope.relevantCohort.currentPeriod;
                        scope.showDiplomaCheck =
                            _.contains(['finished', 'graduated', 'honors'], scope.status) &&
                            new Date() <= scope.relevantCohort.diplomaGeneration;
                    }
                });

                scope.proxy = {};

                scope.proxy.projectEmailAddress = scope.relevantCohort.getFormattedProjectSubmissionEmail();

                function getCoursesFromLocalePackIds(localePackIds, findIncomplete = false) {
                    const courses = [];

                    _.each(_.uniq(localePackIds), id => {
                        const course = localePackIdHash[id];

                        // if there's a course and we're either not looking for incompletes
                        // or we are and we find an incomplete, add course to array
                        if (
                            course &&
                            (findIncomplete === false || (findIncomplete === true && course.complete === false))
                        ) {
                            courses.push(course);
                        }
                    });

                    return courses;
                }

                Object.defineProperty(scope.proxy, 'pastDueCourses', {
                    get() {
                        // this needs to also return past due courses when there is no currentPeriod
                        // such as after all periods have passed, but the user is still not on track
                        const periodIndex = scope.relevantCohort.currentPeriod
                            ? scope.relevantCohort.current_period_index - 1
                            : scope.relevantCohort.periods.length;
                        return getCoursesFromLocalePackIds(
                            scope.relevantCohort.getRequiredStreamPackIdsFromPeriods(periodIndex),
                            true,
                        );
                    },
                });

                Object.defineProperty(scope.proxy, 'requiredCourses', {
                    get() {
                        return (
                            scope.relevantCohort.currentPeriod &&
                            getCoursesFromLocalePackIds(scope.relevantCohort.currentPeriod.requiredStreamLocalePackIds)
                        );
                    },
                });

                Object.defineProperty(scope.proxy, 'optionalCourses', {
                    get() {
                        return (
                            scope.relevantCohort.currentPeriod &&
                            getCoursesFromLocalePackIds(scope.relevantCohort.currentPeriod.optionalStreamLocalePackIds)
                        );
                    },
                });

                Object.defineProperty(scope.proxy, 'periodTitle', {
                    get() {
                        let title = scope.formatsText.stripFormatting(scope.relevantCohort.currentPeriod.periodTitle);

                        // we want "Week #:" to be set apart if it's a specialization period
                        if (
                            scope.relevantCohort.currentPeriod.canHaveSpecializationStyle &&
                            !!scope.relevantCohort.currentPeriod.specialization_style
                        ) {
                            title = title.replace(/^(week)\s[0-9]{1,2}:\s/i, match => `<span>${match.trim()}</span>`);
                        }

                        return title;
                    },
                });

                Object.defineProperty(scope.proxy, 'periodDates', {
                    get() {
                        let startDate = scope.relevantCohort.currentPeriod.startDate;
                        let endDate = scope.relevantCohort.currentPeriod.endDate;

                        // If the currentPeriod is canHaveSpecializationStyle and there is a specialization_style defined
                        if (
                            scope.relevantCohort.currentPeriod.canHaveSpecializationStyle &&
                            !!scope.relevantCohort.currentPeriod.specialization_style
                        ) {
                            // Find all the periods for the current specialization_style
                            const specializationStylePeriods = _.filter(
                                scope.relevantCohort.periods,
                                period =>
                                    period.canHaveSpecializationStyle &&
                                    !!period.specialization_style &&
                                    period.specialization_style ===
                                        scope.relevantCohort.currentPeriod.specialization_style,
                            );

                            // Compute the startDate and endDate of the specialization period that really potentially spans
                            // multiple data model periods
                            startDate = !_.isEmpty(specializationStylePeriods)
                                ? _.first(specializationStylePeriods).startDate
                                : startDate;
                            endDate = !_.isEmpty(specializationStylePeriods)
                                ? _.last(specializationStylePeriods).endDate
                                : endDate;
                        }

                        return scope.relevantCohort.currentPeriod && dateHelper.formattedDateRange(startDate, endDate);
                    },
                });

                scope.proxy.openStreamDashboard = stream => {
                    EventLogger.log('student-dashboard:recent-courses');
                    RouteAnimationHelper.animatePathChange(stream.streamDashboardPath, 'slide-left');
                };

                // social sharing
                const translationHelper = new TranslationHelper('lessons.stream.student_dashboard_schedule');

                const shareTitle = translationHelper.get(`share_title_${scope.currentUser.programType}`);

                const shareDescription = translationHelper.get(`share_description_${scope.currentUser.programType}`, {
                    socialHashtag: ConfigFactory.getSync().social_hashtag,
                });

                const contentItem = {
                    title: shareTitle,
                    description: shareDescription,
                    campaignDescription: shareDescription,
                    entity_metadata: {
                        title: shareTitle,
                        description: shareDescription,
                        tweet_template: shareDescription,
                        canonical_url: '/',
                    },
                };

                // used by processed markdown that contains links
                scope.openExternalLink = link => {
                    scope.loadUrlOrMaybeRoute(link, '_blank');
                };

                scope.share = function share(provider) {
                    contentItem.utmCampaign = provider;
                    ShareService.openShareWindow(
                        provider,
                        SiteMetadata.contentCompletedShareInfo($rootScope.currentUser, contentItem),
                    );
                };
            },
        };
    },
]);
