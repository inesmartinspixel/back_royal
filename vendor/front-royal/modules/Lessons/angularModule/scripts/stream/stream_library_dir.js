import angularModule from 'Lessons/angularModule/scripts/lessons_module';
import 'ExtensionMethods/array';
import template from 'Lessons/angularModule/views/stream/stream_library.html';
import toggleableCourseListButtons from 'Lessons/angularModule/views/stream/toggleable_course_list_buttons.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

import closeIconBeigeDarker from 'vectors/close_icon_beige_darker.svg';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('streamLibrary', [
    '$injector',

    function factory($injector) {
        const $rootScope = $injector.get('$rootScope');
        const $filter = $injector.get('$filter');
        const $location = $injector.get('$location');
        const ClientStorage = $injector.get('ClientStorage');
        const Stream = $injector.get('Lesson.Stream');
        const StreamDashboardDirHelper = $injector.get('Stream.StreamDashboardDirHelper');
        const AppHeaderViewModel = $injector.get('Navigation.AppHeader.AppHeaderViewModel');
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
        const EventLogger = $injector.get('EventLogger');
        const stemmer = $injector.get('stemmer');
        const SearchAttempt = $injector.get('SearchAttempt');
        const SiteMetadata = $injector.get('SiteMetadata');
        const HasToggleableDisplayMode = $injector.get('HasToggleableDisplayMode');
        const TranslationHelper = $injector.get('TranslationHelper');
        const ContentAccessHelper = $injector.get('ContentAccessHelper');
        const Locale = $injector.get('Locale');

        return {
            scope: {},
            restrict: 'E',
            templateUrl,
            controllerAs: 'controller',

            link(scope) {
                scope.closeIconBeigeDarker = closeIconBeigeDarker;
                scope.toggleableCourseListButtons = toggleableCourseListButtons;

                const translationHelper = new TranslationHelper('lessons.stream.stream_library');
                scope.Locale = $injector.get('Locale');

                EventLogger.allowEmptyLabel(['library_search:finish', 'library_search:start', 'library_search:update']);

                //------------------------------
                // Initialization
                //------------------------------

                SearchAttempt.watchLibraryScope(scope);

                scope.searchPlaceholder = translationHelper.get('search_placeholder');

                // Get current user onto the scope
                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                Object.defineProperty(scope, 'blueOceanUser', {
                    get() {
                        return scope.currentUser && scope.currentUser.blueOcean;
                    },
                    configurable: true, // specs
                });

                StreamDashboardDirHelper.onLink(scope);
                NavigationHelperMixin.onLink(scope);
                HasToggleableDisplayMode.onLink(scope, 'toggleableCourseListLibrary', false);

                // Set up header
                AppHeaderViewModel.setBodyBackground('beige');
                AppHeaderViewModel.showAlternateHomeButton = false;
                AppHeaderViewModel.setTitleHTML(translationHelper.get('library_title'));

                // Default title
                SiteMetadata.updateHeaderMetadata();

                scope.topicsToFilterBy = [];
                scope.progressOptionsToFilterBy = [];
                scope.selectFiltersForm = {
                    topicsToFilterBy: [],
                    progressOptionsToFilterBy: [],
                    selectedStatus: 'any_status',
                };
                scope.proxy = {};

                const itemsToFiltersMap = {};

                // available topics will be build once streams are loaded
                scope.availableTopics = [];

                // availableProgressOptions is hard-coded, since we know what options exist
                scope.availableProgressOptions = [
                    {
                        id: 'not_started',
                        name: translationHelper.get('not_started'),
                    },
                    {
                        id: 'in_progress',
                        name: translationHelper.get('in_progress'),
                    },
                    {
                        id: 'completed',
                        name: translationHelper.get('completed'),
                    },
                ];

                // availableStatusOptions is hard-coded, since we know what options exist
                scope.availableStatusOptions = {};
                ['any_status', 'available', 'new_courses', 'updated', 'beta', 'coming_soon', 'elective'].forEach(
                    status => {
                        const statusInfo = {
                            id: status,
                            name: translationHelper.get(status),
                            display: false,
                        };

                        if (status === 'new_courses') {
                            statusInfo.itemProperty = 'just_added';
                        } else if (status === 'updated') {
                            statusInfo.itemProperty = 'just_updated';
                        } else if (status === 'beta') {
                            statusInfo.itemProperty = 'beta';
                        } else if (status === 'coming_soon') {
                            statusInfo.itemProperty = 'coming_soon';
                        } else if (status === 'elective') {
                            statusInfo.itemProperty = 'elective';
                        }

                        scope.availableStatusOptions[status] = statusInfo;
                    },
                );

                //---------------------------
                // Saved Search Params
                //---------------------------

                let savedTopicsToFilter;

                let savedProgressesToFilter;
                let savedSearchText;
                let savedSelectedStatus;
                let savedFiltersLoaded = false;

                // check query params; if any are set, they take precedence
                // this allows us to override whatever is the last saved search when linking to a search
                if (
                    $location.search()['topics[]'] ||
                    $location.search()['progress[]'] ||
                    $location.search().searchText ||
                    $location.search().selectedStatus
                ) {
                    savedTopicsToFilter = $location.search()['topics[]'] || [];
                    // if just one param is specified, it comes back as a string from .search()
                    if (savedTopicsToFilter.constructor !== Array) {
                        savedTopicsToFilter = [savedTopicsToFilter];
                    }

                    savedProgressesToFilter = $location.search()['progress[]'] || [];
                    // if just one param is specified, it comes back as a string from .search()
                    if (savedProgressesToFilter.constructor !== Array) {
                        savedProgressesToFilter = [savedProgressesToFilter];
                    }
                    savedSearchText = $location.search().searchText;
                    savedSelectedStatus = $location.search().selectedStatus;
                } else {
                    // otherwise check ClientStorage
                    try {
                        savedTopicsToFilter = JSON.parse(ClientStorage.getItem('librarySearchTopic'));
                    } catch (e) {}
                    try {
                        savedProgressesToFilter = JSON.parse(ClientStorage.getItem('librarySearchProgress'));
                    } catch (e) {}
                    try {
                        savedSearchText = JSON.parse(ClientStorage.getItem('librarySearchText'));
                    } catch (e) {}
                    try {
                        savedSelectedStatus = JSON.parse(ClientStorage.getItem('librarySelectedStatus'));
                    } catch (e) {}
                }

                function saveSearchParams() {
                    if (!savedFiltersLoaded) {
                        return;
                    }

                    const searchText = scope.searchText.length > 0 ? scope.searchText : undefined;
                    const selectedStatus = scope.selectedStatus;
                    const topics = scope.topicsToFilterBy;
                    const progresses = scope.progressOptionsToFilterBy.map(p => p.name);

                    // update search params and save to localstorage
                    $location.search('topics[]', topics);
                    $location.search('progress[]', progresses);
                    $location.search('searchText', searchText);
                    $location.search('selectedStatus', selectedStatus);

                    ClientStorage.setItem('librarySearchTopic', JSON.stringify(topics));
                    ClientStorage.setItem('librarySearchProgress', JSON.stringify(progresses));
                    ClientStorage.setItem('librarySearchText', JSON.stringify(searchText));
                    ClientStorage.setItem('librarySelectedStatus', JSON.stringify(selectedStatus));
                }

                //---------------------------
                // Navigation
                //---------------------------

                scope.log = message => {
                    EventLogger.log(`stream-library-dashboard:${message}`);
                };

                //---------------------------
                // Data Loading
                //---------------------------

                function loadStreams() {
                    const viewAs = $rootScope.viewAs || '';

                    scope.streamGroups = undefined;
                    const params = {
                        filters: {
                            published: true,
                        },
                        summary: true,
                        include_progress: true,
                    };
                    if (viewAs) {
                        params.filters.view_as = viewAs;
                        params.filters.in_locale_or_en = Locale.activeCode;
                        params.filters.user_can_see = null;
                        params.filters.in_users_locale_or_en = null;
                    }
                    if (!scope.currentUser) {
                        params.filters.in_locale_or_en = Locale.activeCode;
                    }

                    Stream.indexForCurrentUser(params).then(response => {
                        // Update streams and counts of various statistics
                        const streams = response.result;
                        scope.streams = $filter('orderBy')(streams, 'title');
                        scope.addFilters(scope.streams, 'stream');

                        // lookup the saved values
                        const topics = scope.availableTopics.filter(
                            name => savedTopicsToFilter && savedTopicsToFilter.includes(name),
                        );
                        const progresses = scope.availableProgressOptions.filter(
                            p => savedProgressesToFilter && savedProgressesToFilter.includes(p.name),
                        );

                        if (topics) {
                            scope.selectFiltersForm.topicsToFilterBy = topics;
                        }
                        if (progresses) {
                            scope.selectFiltersForm.progressOptionsToFilterBy = progresses;
                        }
                        if (savedSearchText) {
                            scope.selectFiltersForm.searchText = savedSearchText;
                        }
                        if (savedSelectedStatus) {
                            scope.selectFiltersForm.selectedStatus = savedSelectedStatus;
                        }

                        // start allowing things to be saved
                        savedFiltersLoaded = true;

                        // HasToggleableDisplayMode controls DOM pre-rendering
                        scope.preloadAllDisplayModes();
                    });
                }

                //------------------------------
                // Topic / Progress Filtering
                //------------------------------

                /*
                        addFilters runs when streams are loaded.

                        It loops through the loaded records and re-formats the information
                        in a way that will be optimized for filtering later.

                        Each record gets an entry in the itemsToFiltersMap.  The entry
                        stores the topics and the progress status for the record.
                    */
                scope.addFilters = records => {
                    // reset display
                    angular.forEach(scope.availableStatusOptions, (statusOption, key) => {
                        statusOption.display = ['available', 'any_status'].includes(key);
                    });

                    scope.showExtendedStatusOptions = false;

                    records.forEach(record => {
                        const itemsToFiltersEntry = (itemsToFiltersMap[record.id] = itemsToFiltersMap[record.id] || {});
                        itemsToFiltersEntry.topics = {};
                        itemsToFiltersEntry.progressStatus = record.progressStatus();

                        // flatten the record values into a single string so we can conduct substr searches
                        itemsToFiltersEntry.indexedSearchTerms = Object.keys(record.getSearchTermsSet()).join(' ');

                        record.contentTopicNames.forEach(topicName => {
                            if (!scope.availableTopics[topicName]) {
                                scope.availableTopics.push(topicName);

                                // Note: availableTopics is an array, but we add properties to
                                // it to keep track of what we already added
                                scope.availableTopics[topicName] = true;
                            }

                            itemsToFiltersEntry.topics[topicName] = true;
                        });

                        angular.forEach(scope.availableStatusOptions, statusOption => {
                            if (statusOption.itemProperty && record[statusOption.itemProperty]) {
                                statusOption.display = true;
                                scope.showExtendedStatusOptions = true;
                            }
                        });
                    });

                    scope.availableTopics.sort();
                };

                scope.filteredTopics = topics => {
                    if (scope.selectFiltersForm.topicsToFilterBy.length === 0) {
                        return topics;
                    }
                    const filtered = [];
                    topics.forEach(topic => {
                        if (scope.selectFiltersForm.topicsToFilterBy.includes(topic)) {
                            filtered.push(topic);
                        }
                    });
                    return filtered;
                };

                /*
                        shouldFilter out is run on a single stream.

                        It returns true if, given the current filters that the user
                        has selected, the item should be removed from the screen.
                    */
                function shouldFilterOut(item) {
                    const itemsToFiltersEntry = itemsToFiltersMap[item.id];

                    // exclusive
                    if (scope.selectedStatus !== 'any_status') {
                        const selectedOption = scope.availableStatusOptions[scope.selectedStatus];
                        if (scope.selectedStatus === 'available' && !ContentAccessHelper.canLaunch(item)) {
                            return true;
                        }
                        if (scope.selectedStatus !== 'available' && !item[selectedOption.itemProperty]) {
                            return true;
                        }
                    }

                    // inclusive among topic types
                    if (scope.topicsToFilterBy.length > 0) {
                        let hasSomeTopic = false;

                        for (const topic of scope.topicsToFilterBy) {
                            if (itemsToFiltersEntry.topics[topic]) {
                                hasSomeTopic = true;
                                break;
                            }
                        }

                        if (!hasSomeTopic) {
                            return true;
                        }
                    }

                    // inclusive among progress types
                    if (scope.progressOptionsToFilterBy.length > 0) {
                        let hasSomeProgressFilter = false;
                        for (let j = 0; j < scope.progressOptionsToFilterBy.length; j++) {
                            const progressStatus = scope.progressOptionsToFilterBy[j].id;
                            if (itemsToFiltersEntry.progressStatus === progressStatus) {
                                hasSomeProgressFilter = true;
                                break;
                            }
                        }
                        if (!hasSomeProgressFilter) {
                            return true;
                        }
                    }

                    // exclusive
                    if (scope.searchText) {
                        let foundAllSearchTerms = true;
                        const searchTexts = scope.searchText.split(/\s+/);

                        // iterate through each word, stemming and performing substr against indexed terms
                        searchTexts.forEach(searchText => {
                            stemmer.stemmedWords(searchText).forEach(searchTerm => {
                                if (!itemsToFiltersEntry.indexedSearchTerms.includes(searchTerm)) {
                                    foundAllSearchTerms = false;
                                }
                            });
                        });

                        if (!foundAllSearchTerms) {
                            return true;
                        }
                    }

                    // defaults to allowing
                    return false;
                }

                /*
                        filterResults is run when records are loaded or when filters are selected
                        by the user.

                        It loops through the existing streams and selects
                        the ones that should be visible given the current filters that the user
                        has selected.
                    */
                function filterResults() {
                    if (
                        scope.topicsToFilterBy.length === 0 &&
                        scope.progressOptionsToFilterBy.length === 0 &&
                        !scope.searchText &&
                        scope.selectedStatus === 'any_status'
                    ) {
                        scope.filteredStreams = scope.streams;
                    } else if (scope.streams) {
                        scope.filteredStreams = [];
                        scope.streams.forEach(stream => {
                            if (!shouldFilterOut(stream)) {
                                scope.filteredStreams.push(stream);
                            }
                        });
                    }

                    // update the topic groups using helper method added to scope in HasToggleableDisplayMode
                    scope.createTopicGroups(scope.filteredStreams);

                    scope.$emit('filtersUpdated');
                }

                // Topics

                scope.toggleTopicFilter = topic => {
                    if (scope.selectFiltersForm.topicsToFilterBy.includes(topic)) {
                        Array.remove(scope.selectFiltersForm.topicsToFilterBy, topic);
                    } else {
                        scope.selectFiltersForm.topicsToFilterBy.push(topic);
                    }
                };

                // Progress

                scope.toggleProgressFilter = progressOption => {
                    if (scope.selectFiltersForm.progressOptionsToFilterBy.includes(progressOption)) {
                        Array.remove(scope.selectFiltersForm.progressOptionsToFilterBy, progressOption);
                    } else {
                        scope.selectFiltersForm.progressOptionsToFilterBy.push(progressOption);
                    }
                    scope.selectFiltersForm.comingSoonFilter = undefined;
                };

                scope.toggleAnyProgressFilter = () => {
                    scope.selectFiltersForm.progressOptionsToFilterBy = [];
                };

                Object.defineProperty(scope, 'anyProgressToggled', {
                    get() {
                        return scope.selectFiltersForm.progressOptionsToFilterBy.length === 0;
                    },
                });

                // Publishing Status

                scope.toggleStatusFilter = status => {
                    if (scope.selectedStatus === status) {
                        scope.selectFiltersForm.selectedStatus = 'any_status';
                    } else {
                        scope.selectFiltersForm.selectedStatus = status;
                        if (status === 'coming_soon') {
                            scope.selectFiltersForm.progressOptionsToFilterBy = []; // can't have progress
                        }
                    }
                };

                //------------------------------
                // Form Modification
                //------------------------------

                function onFormChange() {
                    if (!scope.proxy.showFilterSelectFormOnMobile) {
                        scope.commitSelectFiltersFormChanges();
                    }
                }

                scope.commitSelectFiltersFormChanges = () => {
                    scope.topicsToFilterBy = scope.selectFiltersForm.topicsToFilterBy.slice(0);
                    scope.progressOptionsToFilterBy = scope.selectFiltersForm.progressOptionsToFilterBy.slice(0);
                    scope.proxy.showFilterSelectFormOnMobile = false;

                    // prevent regexp errors by removing special chars
                    scope.searchText = scope.selectFiltersForm.searchText
                        ? scope.selectFiltersForm.searchText.replace(/[{}\/\[\]\\\(\)$^*+.,:?|]/g, '')
                        : '';
                    scope.selectedStatus = scope.selectFiltersForm.selectedStatus;

                    saveSearchParams();
                };

                scope.resetFilterSelectForm = () => {
                    scope.selectFiltersForm.topicsToFilterBy = scope.topicsToFilterBy.slice(0, 9999);
                    scope.selectFiltersForm.progressOptionsToFilterBy = scope.progressOptionsToFilterBy.slice(0);
                    scope.selectFiltersForm.searchText = scope.searchText;
                    scope.selectFiltersForm.selectedStatus = scope.selectedStatus;
                    scope.proxy.showFilterSelectFormOnMobile = false;

                    saveSearchParams();
                };

                scope.clearFilterAndSearch = () => {
                    scope.searchText = scope.selectFiltersForm.searchText = '';
                    scope.selectedStatus = scope.selectFiltersForm.selectedStatus = 'any_status';
                    scope.selectFiltersForm.progressOptionsToFilterBy = [];
                    scope.selectFiltersForm.topicsToFilterBy = [];

                    saveSearchParams();
                };

                //------------------------------
                // Watches
                //------------------------------

                // watches that actually invoke the filtering
                scope.$watchCollection('topicsToFilterBy', filterResults);
                scope.$watchCollection('progressOptionsToFilterBy', filterResults);
                scope.$watchCollection('searchText', filterResults);
                scope.$watch('selectedStatus', filterResults);
                scope.$watch('streams', filterResults);

                // watches that do some sort of data manipulation prior to filtering
                scope.$watchCollection('selectFiltersForm.topicsToFilterBy', onFormChange);
                scope.$watchCollection('selectFiltersForm.progressOptionsToFilterBy', onFormChange);
                scope.$watch('selectFiltersForm.searchText', onFormChange);
                scope.$watch('selectFiltersForm.selectedStatus', onFormChange);

                scope.$watch('proxy.showFilterSelectFormOnMobile', val => {
                    AppHeaderViewModel.toggleVisibility(!val);
                    $injector.get('scrollHelper').scrollToTop();
                });

                const stopWatchingViewAs = $rootScope.$watch('viewAs', () => {
                    scope.availableTopics = [];
                    loadStreams();
                });

                scope.$on('$destroy', () => {
                    stopWatchingViewAs();
                });
            },
        };
    },
]);
