import angularModule from 'Lessons/angularModule/scripts/lessons_module';
import { setupBrandNameProperties, setupScopeProperties } from 'AppBrandMixin';
import template from 'Lessons/angularModule/views/stream/dashboard_footer.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

import fbIcon from 'vectors/fb-icon.svg';
import fbHoverIcon from 'vectors/fb-hover-icon.svg';
import twitterIcon from 'vectors/twitter-icon.svg';
import twitterHoverIcon from 'vectors/twitter-hover-icon.svg';
import availableAppStore from 'vectors/available-app-store.svg';
import availableGooglePlay from 'vectors/available-google-play.svg';
import logoRed from 'vectors/logo-red.svg';
import logoRedQuantic from 'vectors/logo-red_quantic.svg';
import logoMiyaMiya from 'images/miyamiya/miyamiya-logo.png';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('dashboardFooter', [
    '$injector',

    function factory($injector) {
        const $window = $injector.get('$window');
        const $rootScope = $injector.get('$rootScope');
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
        const ConfigFactory = $injector.get('ConfigFactory');
        const offlineModeManager = $injector.get('offlineModeManager');

        return {
            scope: {},

            restrict: 'E',
            templateUrl,
            controllerAs: 'controller',

            link(scope) {
                scope.fbIcon = fbIcon;
                scope.fbHoverIcon = fbHoverIcon;
                scope.twitterIcon = twitterIcon;
                scope.twitterHoverIcon = twitterHoverIcon;
                scope.availableAppStore = availableAppStore;
                scope.availableGooglePlay = availableGooglePlay;

                NavigationHelperMixin.onLink(scope);

                const config = ConfigFactory.getSync();
                setupBrandNameProperties($injector, scope, config);
                setupScopeProperties(
                    $injector,
                    scope,
                    [
                        {
                            prop: 'logoImgSrc',
                            quantic: logoRedQuantic,
                            fallback: () => (scope.currentUser?.isMiyaMiya ? logoMiyaMiya : logoRed),
                        },
                    ],
                    config,
                );

                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                scope.socialAccountName = scope.currentUser?.isMiyaMiya ? 'MiyaMiyaSchool' : config.social_name;
                scope.showCookiePreferences = config.gdprAppliesToUser();

                const launchCookiePreferences = () => {
                    if ($window.Optanon) {
                        const toggle = $window.Optanon.ToggleInfoDisplay;
                        toggle(); // work around ESLint
                    }
                };

                scope.availableFooterLinksMap = [
                    {
                        onClick: () => scope.loadRoute('/library'),
                        translateOnceKey: 'lessons.stream.dashboard_footer.library',
                        translateOnceTitleKey: 'lessons.stream.dashboard_footer.library',
                    },

                    {
                        onClick: () => {
                            if (scope.currentUser?.isMiyaMiya) {
                                scope.loadUrl('https://miyamiya.org/', '_blank');
                            } else {
                                scope.loadUrl('/about');
                            }
                        },
                        translateOnceKey: 'lessons.stream.dashboard_footer.about',
                        translateOnceTitleKey: 'lessons.stream.dashboard_footer.about',
                    },
                ];

                if (!scope.currentUser?.isMiyaMiya) {
                    scope.availableFooterLinksMap.push(
                        {
                            onClick: () => scope.loadUrl('http://blog.quantic.edu', '_blank'),
                            translateOnceKey: 'lessons.stream.dashboard_footer.blog',
                            translateOnceTitleKey: 'lessons.stream.dashboard_footer.blog',
                        },

                        {
                            onClick: () => scope.loadUrl('/method'),
                            translateOnceKey: 'lessons.stream.dashboard_footer.method',
                            translateOnceTitleKey: 'lessons.stream.dashboard_footer.method',
                        },

                        {
                            onClick: () => scope.loadUrl('/press'),
                            translateOnceKey: 'lessons.stream.dashboard_footer.press',
                            translateOnceTitleKey: 'lessons.stream.dashboard_footer.press',
                        },

                        {
                            onClick: () => scope.loadUrl('/help/', '_blank'),
                            translateOnceKey: 'lessons.stream.dashboard_footer.faq',
                            translateOnceTitleKey: 'lessons.stream.dashboard_footer.faq',
                        },

                        {
                            onClick: () => scope.loadUrl('/help/contact', '_blank'),
                            translateOnceKey: 'lessons.stream.dashboard_footer.contact_us',
                            translateOnceTitleKey: 'lessons.stream.dashboard_footer.contact_us',
                        },
                    );
                }

                if (scope.showCookiePreferences) {
                    // Put this at the beginning (left most in the UI)
                    scope.availableFooterLinksMap.unshift({
                        onClick: () => launchCookiePreferences(),
                        translateOnceKey: 'lessons.stream.dashboard_footer.cookies',
                        translateOnceTitleKey: 'lessons.stream.dashboard_footer.cookies',
                    });
                }

                //---------------------------
                // Initialization
                //---------------------------

                // Hybrid app has no footer.  When offline, none of the
                // links will work, so we hide the whole thing.  Tiny performance
                // bump by only watching when we're not in cordova
                if ($window.CORDOVA) {
                    scope.showFooter = false;
                } else {
                    scope.$watch(() => {
                        scope.showFooter = !offlineModeManager.inOfflineMode;
                    });
                }

                scope.launchAppStoreiOS = () => {
                    let url = '/smartly-ios';
                    if (scope.currentUser?.isMiyaMiya) {
                        url = '/miyamiya-ios';
                    }

                    scope.loadUrl(url);
                };

                scope.launchAppStoreAndroid = () => {
                    let url = '/smartly-android';
                    if (scope.currentUser?.isMiyaMiya) {
                        url = '/miyamiya-android';
                    }
                    scope.loadUrl(url);
                };
            },
        };
    },
]);
