import angularModule from 'Lessons/angularModule/scripts/lessons_module';
import { setupBrandNameProperties, setupBrandEmailProperties } from 'AppBrandMixin';
import template from 'Lessons/angularModule/views/stream/student_dashboard_welcome_box.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

import brainOnboardingMessenger from 'vectors/brain_onboarding_messenger.svg';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('studentDashboardWelcomeBox', [
    '$injector',

    function factory($injector) {
        const TranslationHelper = $injector.get('TranslationHelper');
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
        const dateHelper = $injector.get('dateHelper');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                currentUser: '<',
                foundationPlaylist: '<?',
                streams: '<',
            },
            link(scope) {
                scope.brainOnboardingMessenger = brainOnboardingMessenger;

                //---------------------------
                // Initialization
                //---------------------------

                const translationHeper = new TranslationHelper('lessons.stream.student_dashboard_welcome_box');
                NavigationHelperMixin.onLink(scope);
                setupBrandNameProperties($injector, scope);
                setupBrandEmailProperties($injector, scope, ['admissions']);

                // determine if user in the special blue ocean high school group
                Object.defineProperty(scope, 'blueOceanHighSchooler', {
                    get() {
                        return scope.currentUser && scope.currentUser.sign_up_code === 'BOSHIGH';
                    },
                });

                // title of the foundation playlist for welcome message
                Object.defineProperty(scope, 'foundationTitle', {
                    get() {
                        return scope.foundationPlaylist && scope.foundationPlaylist.title;
                    },
                });

                //---------------------------
                // Determine which message to show
                //---------------------------

                Object.defineProperty(scope, 'message', {
                    get() {
                        // be defensive against no current user
                        if (!scope.currentUser) {
                            return undefined;
                        }
                        if (scope.blueOceanHighSchooler) {
                            return 'BOSHIGH';
                        }
                        if (scope.currentUser.hasExternalInstitution) {
                            return 'EXTERNAL';
                        }
                        return 'SMARTER';
                    },
                });

                // used if message is of the "SMARTER" variety above
                function setSmarterMessageKey() {
                    // be defensive against no current user
                    if (!scope.currentUser) {
                        return;
                    }

                    // We have a standardized structure for the translations to generate the appropriate subtitle.
                    //
                    // Components:
                    // - welcome box key: based on program type, see schedulable.js for definition of studentDashboardWelcomeBoxKey
                    // - status: not_applied, applying, applied, accepted
                    // - foundations_complete: whether the user has finished the foundations playlist; only included for "applied" status
                    //
                    // For EMBA, we also add the following statuses:
                    // - status: pre_accepted
                    //
                    // We don't show the welcome box for rejected users who are potentially considering re-applying.
                    // So, it's safe to assume here that we'll only be dealing with users who have never applied, or, if they
                    // have previously applied, are in the pending, pre-accepted, or accepted states.
                    //
                    // See student_dashboard_dir.js#showWelcomeBox for details of when this directive is visible.

                    const relevantCohort = scope.currentUser.relevant_cohort;

                    const lastApplication = scope.currentUser.lastCohortApplication;
                    const startedApplying = scope.currentUser.program_type_confirmed;
                    const applied = !!lastApplication;
                    const preAccepted = lastApplication && lastApplication.status === 'pre_accepted';
                    const accepted = !!scope.currentUser.acceptedCohortApplication;

                    // compute status
                    let status = 'not_applied';
                    if (startedApplying) {
                        status = 'applying';

                        if (applied) {
                            status = 'applied';

                            // Note: we only really support pre_accepted case for MBA and EMBA and
                            // accepted case for Fundamentals of Business cert and Career Network Only
                            // see student_dashboard_dir.js#showWelcomeBox for details

                            if (preAccepted) {
                                status = 'pre_accepted';
                            } else if (accepted) {
                                status = 'accepted';
                            }
                        }
                    }

                    // construct the locale key
                    let smarterMessageKey = 'not_applied';
                    if (status !== 'not_applied' && relevantCohort) {
                        smarterMessageKey = _.compact([
                            relevantCohort.studentDashboardWelcomeBoxKey,
                            status,
                            status === 'applied' && scope.foundationsComplete ? 'foundations_complete' : undefined,
                        ]).join('_');
                    }

                    // construct the parameters
                    const appliedAt = applied ? lastApplication.applied_at : undefined;
                    const startDate =
                        relevantCohort &&
                        dateHelper.formattedUserFacingMonthDayYearLong(relevantCohort.startDate, false);
                    const applicableAdmissionRound =
                        relevantCohort && relevantCohort.getApplicableAdmissionRound(appliedAt);
                    const applicationDeadline = applicableAdmissionRound
                        ? dateHelper.formattedUserFacingMonthDayYearLong(applicableAdmissionRound.applicationDeadline)
                        : undefined;
                    const decisionDate = applicableAdmissionRound
                        ? dateHelper.formattedUserFacingMonthDayYearLong(applicableAdmissionRound.decisionDate)
                        : undefined;
                    const programGuideUrl = !!lastApplication && lastApplication.cohort_program_guide_url;

                    // get the translated string, with fallback to generic message if key not found
                    scope.smarterMessage = translationHeper.getWithFallbackKey(smarterMessageKey, 'welcome_generic', {
                        name: scope.currentUser.preferredName,
                        foundationTitle: scope.foundationTitle,
                        brandName: scope.brandNameShort,
                        brandEmail: scope.brandAdmissionsEmail,
                        applicationDeadline,
                        decisionDate,
                        startDate,
                        programGuideUrl,
                    });
                }

                scope.$watch('currentUser.hasPendingOrPreAcceptedCohortApplication', setSmarterMessageKey);

                // canCalculateComplete will be false until all of the streams in the foundation playlist are loaded.  It is
                // a little expensive to calculate, and once it's true it should always be true, so stop watching for it once it is true
                scope.foundationsComplete = null;
                const stopWatchingCanCalculateComplete = scope.$watch(
                    'foundationPlaylist.canCalculateComplete',
                    canCalculateComplete => {
                        if (canCalculateComplete) {
                            scope.foundationsComplete = scope.foundationPlaylist.complete;
                            setSmarterMessageKey();
                            stopWatchingCanCalculateComplete();
                        }
                    },
                );
            },
        };
    },
]);
