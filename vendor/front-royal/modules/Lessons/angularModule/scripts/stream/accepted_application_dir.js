import angularModule from 'Lessons/angularModule/scripts/lessons_module';
import { setupBrandNameProperties, setupScopeProperties } from 'AppBrandMixin';
import template from 'Lessons/angularModule/views/stream/accepted_application.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

import fbIconWhite from 'vectors/fb-icon-white.svg';
import twitterIconWhite from 'vectors/twitter-icon-white.svg';
import badge from 'images/badge.png';
import badgeQuantic from 'images/badge_quantic.png';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('applicationAccepted', [
    '$injector',

    function factory($injector) {
        const $rootScope = $injector.get('$rootScope');
        const ShareService = $injector.get('Navigation.ShareService');
        const TranslationHelper = $injector.get('TranslationHelper');
        const DialogModal = $injector.get('DialogModal');
        const dateHelper = $injector.get('dateHelper');
        const ConfigFactory = $injector.get('ConfigFactory');

        return {
            restrict: 'E',
            templateUrl,
            scope: {},
            link(scope) {
                scope.fbIconWhite = fbIconWhite;
                scope.twitterIconWhite = twitterIconWhite;

                setupBrandNameProperties($injector, scope);
                setupScopeProperties($injector, scope, [
                    {
                        prop: 'badgeImgSrc',
                        quantic: badgeQuantic,
                        fallback: badge,
                    },
                ]);

                const translationHelper = new TranslationHelper('lessons.stream.accepted_application');

                // Get current user onto the scope
                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                Object.defineProperty(scope, 'showClass', {
                    get() {
                        return (
                            scope.currentUser &&
                            scope.currentUser.relevant_cohort &&
                            scope.currentUser.relevant_cohort.supportsSchedule
                        );
                    },
                });

                if (scope.showClass && scope.currentUser.relevant_cohort) {
                    scope.startDateString = dateHelper.formattedUserFacingMonthYearLong(
                        scope.currentUser.relevant_cohort.start_date * 1000,
                        false,
                    );
                }

                scope.onGetStarted = () => {
                    DialogModal.hideAlerts();
                };

                scope.shareFacebook = () => {
                    ConfigFactory.getConfig().then(config => {
                        ShareService.share('accepted_application', 'facebook', {
                            url: `https://${config.domain}`,
                            title: shareMessage(),
                        });
                    });
                };

                scope.shareTwitter = () => {
                    ConfigFactory.getConfig().then(config => {
                        ShareService.share('accepted_application', 'twitter', {
                            url: `https://${config.domain}`,
                            title: shareMessage(),
                            description: shareMessage(),
                        });
                    });
                };

                function shareMessage() {
                    return translationHelper.get(scope.currentUser.relevant_cohort.acceptedApplicationShareMessageKey, {
                        brandName: scope.brandNameShort,
                        cohortTitle: scope.currentUser.relevant_cohort.title,
                    });
                }
            },
        };
    },
]);
