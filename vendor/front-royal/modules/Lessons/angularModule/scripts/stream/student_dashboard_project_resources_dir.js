import angularModule from 'Lessons/angularModule/scripts/lessons_module';
import template from 'Lessons/angularModule/views/stream/student_dashboard_project_resources.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

import projectResource from 'vectors/project_resource.svg';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('studentDashboardProjectResources', [
    '$injector',

    function factory() {
        return {
            restrict: 'E',
            templateUrl,
            scope: {
                relevantCohort: '<',
            },
            link(scope) {
                scope.projectResource = projectResource;

                //---------------------------
                // Initialization
                //---------------------------

                scope.formattedProjectSubmissionEmail = scope.relevantCohort.getFormattedProjectSubmissionEmail();
                scope.mailtoLink = `mailto:${scope.formattedProjectSubmissionEmail}`;

                //---------------------------
                // Navigation
                //---------------------------

                //----------------------------
                // Watches
                //----------------------------
            },
        };
    },
]);
