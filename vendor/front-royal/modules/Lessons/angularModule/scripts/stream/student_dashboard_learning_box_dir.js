import angularModule from 'Lessons/angularModule/scripts/lessons_module';
import { setupStyleHelpers, setupBrandNameProperties } from 'AppBrandMixin';
import { getOfflineStreams } from 'StoredContent';

import template from 'Lessons/angularModule/views/stream/student_dashboard_learning_box.html';
import activePlaylistViewTemplate from 'Lessons/angularModule/views/stream/learning_box_active_playlist_view.html';
import coursesViewTemplate from 'Lessons/angularModule/views/stream/learning_box_courses_view.html';
import playlistsViewTemplate from 'Lessons/angularModule/views/stream/learning_box_playlists_view.html';
import offlineViewTemplate from 'Lessons/angularModule/views/stream/learning_box_offline_view.html';

import cacheAngularTemplate from 'cacheAngularTemplate';

import comingSoonIcon from 'images/coming_soon_icon.png';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('studentDashboardLearningBox', [
    '$injector',

    function factory($injector) {
        const Playlist = $injector.get('Playlist');
        const EventLogger = $injector.get('EventLogger');
        const RouteAnimationHelper = $injector.get('RouteAnimationHelper');
        const AppHeaderViewModel = $injector.get('Navigation.AppHeader.AppHeaderViewModel');
        const TranslationHelper = $injector.get('TranslationHelper');
        const Stream = $injector.get('Lesson.Stream');
        const $timeout = $injector.get('$timeout');
        const offlineModeManager = $injector.get('offlineModeManager');
        const safeApply = $injector.get('safeApply');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                hasConcentrations: '<',
                learningBoxMode: '<',
                currentUser: '<',
                playlists: '<',
                streams: '<',
                activePlaylist: '<',
                keepLearningStream: '<',
                keepLearningLesson: '<',
                recommendedPlaylist: '<',
                activatePlaylist: '<',
                relevantCohort: '<',
                readyToShowPlaylistMap: '<',
            },
            link(scope) {
                scope.activePlaylistViewTemplate = activePlaylistViewTemplate;
                scope.coursesViewTemplate = coursesViewTemplate;
                scope.playlistsViewTemplate = playlistsViewTemplate;
                scope.offlineViewTemplate = offlineViewTemplate;
                scope.comingSoonIcon = comingSoonIcon;

                setupBrandNameProperties($injector, scope);
                setupStyleHelpers($injector, scope);
                scope.defaultPlaylistIcon = Playlist.DEFAULT_ICON;

                scope.$watch(
                    () => offlineModeManager.inOfflineMode,
                    inOfflineMode => {
                        if (inOfflineMode) {
                            scope.getOfflineStreams().then(streams => {
                                scope.offlineStreams = streams;
                                safeApply(scope); // getOfflineStreams returns a native promise
                            });
                        } else {
                            scope.offlineStreams = null;
                        }
                    },
                );

                // wrapper than we can mock in specs
                scope.getOfflineStreams = () => getOfflineStreams($injector);

                // We just add this timeout to break the digest change and
                // prevent a 10-digest error
                $timeout().then(() => {
                    scope.waitedABeat = true;
                });

                //---------------------------
                // Initialization
                //---------------------------

                const translationHelper = new TranslationHelper('lessons.stream.student_dashboard_learning_box');

                scope.Playlist = Playlist;

                scope.requiredPlaylists = scope.relevantCohort
                    ? scope.relevantCohort.getRequiredPlaylists(scope.playlists)
                    : scope.playlists;

                Object.defineProperty(scope, 'showCompletePlaylistHeader', {
                    get() {
                        return scope.activePlaylist && scope.activePlaylist.complete;
                    },
                });

                Object.defineProperty(scope, 'showCompletePlaylistsHeader', {
                    get() {
                        return (
                            scope.activePlaylist &&
                            scope.activePlaylist.complete &&
                            scope.recommendedPlaylist &&
                            scope.activePlaylist !== scope.recommendedPlaylist
                        );
                    },
                });

                Object.defineProperty(scope, 'playlistsPercentComplete', {
                    get() {
                        return scope.relevantCohort && scope.relevantCohort.getPercentComplete(scope.playlists);
                    },
                });

                Object.defineProperty(scope, 'justStarting', {
                    get() {
                        return scope.streams && !Stream.keepLearningStreamAndLesson(scope.streams);
                    },
                });

                //---------------------------
                // Navigation
                //---------------------------

                scope.launchKeepLearningStream = () => {
                    EventLogger.log('student-dashboard:keep-learning');
                    RouteAnimationHelper.animatePathChange(scope.keepLearningStream.streamDashboardPath, 'slide-left');
                };

                scope.launchKeepLearningLesson = () => {
                    scope.keepLearningLesson.launch(`keep_learning_${scope.keepLearningLesson.launchText}`);
                };

                scope.switchLearningBoxMode = mode => {
                    scope.learningBoxMode.mode = mode;
                };

                //----------------------------
                // Watches
                //----------------------------

                scope.$watchCollection('learningBoxMode', learningBoxMode => {
                    AppHeaderViewModel.learningBoxMode = learningBoxMode;

                    // set the title based on the learningBoxMode
                    if (learningBoxMode.mode === 'playlists') {
                        if (scope.currentUser.hasExternalInstitution) {
                            AppHeaderViewModel.setTitleHTML(translationHelper.get('select_playlist_title'));
                        } else {
                            AppHeaderViewModel.setTitleHTML(translationHelper.get('select_concentration_title'));
                        }
                    } else {
                        AppHeaderViewModel.setTitleHTML(translationHelper.get('home_title'));
                    }
                });

                scope.$on('$destroy', () => {
                    AppHeaderViewModel.learningBoxMode = null;
                });
            },
        };
    },
]);
