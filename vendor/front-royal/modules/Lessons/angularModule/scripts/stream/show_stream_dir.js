import angularModule from 'Lessons/angularModule/scripts/lessons_module';
import { setupBrandNameProperties } from 'AppBrandMixin';
import template from 'Lessons/angularModule/views/stream/show_stream.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('showStream', [
    '$injector',
    function factory($injector) {
        const Stream = $injector.get('Lesson.Stream');
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
        const ClientConfig = $injector.get('ClientConfig');
        const $rootScope = $injector.get('$rootScope');
        const TranslationHelper = $injector.get('TranslationHelper');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                lessonId: '@',
                streamId: '@',
                chapterIndex: '@', // retaining since we might use this
                mode: '@mode',
            },
            controllerAs: 'controller',
            link(scope) {
                const translationHelper = new TranslationHelper('lessons.stream.show_stream');

                NavigationHelperMixin.onLink(scope);
                setupBrandNameProperties($injector, scope);

                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                Object.defineProperty(scope, 'streamViewModel', {
                    get() {
                        if (!this.stream) {
                            return undefined;
                        }

                        if (!this.lesson.launchableWithCurrentClient) {
                            return undefined;
                        }

                        if (this._streamViewModel) {
                            return this._streamViewModel;
                        }

                        this._streamViewModel = this.stream.createStreamViewModel({
                            logProgress: !!scope.currentUser,
                            scormMode: scope.mode === 'scorm',
                        });

                        if (scope.lessonId) {
                            if (!scope.lesson) {
                                throw new Error(`No lesson found for "${scope.lessonId}"`);
                            }

                            this._streamViewModel.activeLessonId = scope.lessonId;
                        }

                        return this._streamViewModel;
                    },
                });

                scope.$watchGroup(['streamId', 'lessonId'], newValues => {
                    const streamId = newValues[0];
                    const lessonId = newValues[1];

                    // FIXME: should we do something with the stream if it's loaded
                    // but doesn't yet have content?
                    if (streamId && lessonId) {
                        // In the logged out case, we make a special request to attempt to load only SMARTER courses
                        // if this request fails, we want to redirect to the sign-in screen, rather than pop-up a login
                        // window and then retry that special request. This way, when you successfully log in, the student
                        // will be sent back to this route and we'll have a chance to construct a new, logged-in request
                        // for the stream.
                        const options = {
                            'FrontRoyal.ApiErrorHandler': {
                                redirectOnLoggedOut: true,
                            },
                        };

                        Stream.showWithFullContentForLesson(streamId, lessonId, options).then(
                            stream => {
                                scope.stream = stream;
                                scope.lesson = stream.lessonForId(lessonId);

                                if (!scope.lesson.launchableWithCurrentClient) {
                                    if (!scope.lesson.launchableWithLatestAvailableVersionOfCurrentClient) {
                                        scope.buttonText = translationHelper.get('go_back');
                                    }
                                }
                            },
                            () => {
                                // no-op. Since we're using redirectOnLoggedOut, it's possible for this request to error
                                // on a 401.  There may also be the possibility of hitting here after a 404.  It doesn't
                                // seem like that should be possible, but git history suggests that at some point someone
                                // at least believed that it was. (In the 401 case, we'll be redirected away from this directive
                                // anyway)
                            },
                        );
                    }
                });

                scope.onClickUpgradeButton = () => {
                    if (scope.lesson.launchableWithLatestAvailableVersionOfCurrentClient) {
                        ClientConfig.current.upgrade();
                    } else {
                        scope.loadRoute(scope.stream.streamDashboardPath);
                    }
                };

                scope.getUpgradeMessage = () =>
                    translationHelper.get('cannot_be_launched', { brandName: scope.brandNameShort });
            },
        };
    },
]);
