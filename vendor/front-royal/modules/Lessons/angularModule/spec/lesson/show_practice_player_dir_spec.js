import 'AngularSpecHelper';
import 'Lessons/angularModule';

describe('Lesson.ShowPracticePlayerDir', () => {
    let $injector;
    let PracticeFramesPlayerViewModel;
    let SpecHelper;
    let scope;
    let elem;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector => {
                $injector = _$injector;
                SpecHelper = $injector.get('SpecHelper');
                PracticeFramesPlayerViewModel = $injector.get('Lesson.PracticeFramesPlayerViewModel');

                jest.spyOn(PracticeFramesPlayerViewModel.prototype, 'linkToWindow').mockImplementation(() => {});
                jest.spyOn(PracticeFramesPlayerViewModel.prototype, 'loadContent').mockImplementation(() => {});

                SpecHelper.stubDirective('showFramesPlayer');
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    it('should create a playerViewModel and link it to the window', () => {
        render();
        expect(PracticeFramesPlayerViewModel.prototype.linkToWindow).toHaveBeenCalled();
        expect(PracticeFramesPlayerViewModel.prototype.loadContent).toHaveBeenCalledWith('something', 'some-id');
    });

    it('should show a spinner until the player is ready', () => {
        render();
        SpecHelper.expectElement(elem, 'front-royal-spinner');
        SpecHelper.expectNoElement(elem, 'show-frames-player');
        Object.defineProperty(scope.playerViewModel, 'ready', {
            value: true,
        });
        scope.$apply();
        SpecHelper.expectNoElement(elem, 'front-royal-spinner');
        SpecHelper.expectElement(elem, 'show-frames-player');
    });

    function render() {
        const renderer = SpecHelper.renderer();
        renderer.render(
            '<show-practice-player course-or-lesson="something" content-item-id="some-id"></show-practice-player>',
        );
        scope = renderer.childScope;
        elem = renderer.elem;
    }
});
