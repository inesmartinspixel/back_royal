import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/lessons';
import 'Lessons/angularModule/spec/_mock/fixtures/streams';
import setSpecLocales from 'Translation/setSpecLocales';
import lessonFinishScreenLocales from 'Lessons/locales/lessons/lesson/lesson_finish_screen-en.json';
import assessmentLessonEnd from 'Lessons/locales/lessons/lesson/assessment_lesson_end-en.json';
import lessonFinishScreenDemoContent from 'Lessons/locales/lessons/lesson/lesson_finish_screen_demo_content-en.json';

setSpecLocales(lessonFinishScreenLocales);
setSpecLocales(assessmentLessonEnd);
setSpecLocales(lessonFinishScreenDemoContent);

describe('Lesson.LessonFinishScreenDir', () => {
    let $injector;
    let SpecHelper;
    let EventLogger;
    let FormatsText;
    let Lesson;
    let lesson;
    let playerViewModel;
    let scope;
    let elem;
    let renderer;
    let $timeout;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector => {
                $injector = _$injector;
                SpecHelper = $injector.get('SpecHelper');
                $injector.get('LessonFixtures');
                $injector.get('StreamFixtures');
                Lesson = $injector.get('Lesson');
                FormatsText = $injector.get('FormatsText');
                $timeout = $injector.get('$timeout');
                lesson = Lesson.fixtures.getInstance();
                playerViewModel = lesson.createPlayerViewModel();
                renderer = SpecHelper.renderer();
                renderer.scope.playerViewModel = playerViewModel;

                SpecHelper.stubDirective('lessonFinishScreenFooter');
                SpecHelper.stubEventLogging();
                SpecHelper.stubConfig();
                SpecHelper.stubCurrentUser('learner');
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('course summaries', () => {
        let summary1;
        let summary2;

        beforeEach(() => {
            expect(playerViewModel.stream.summaries).not.toBeUndefined(); // sanity check
            playerViewModel.stream.lessons[0].lesson_progress = {
                complete: false,
            };

            summary1 = {
                id: '1',
                title: 'summary1',
                lessons: [playerViewModel.stream.lessons[0].id, playerViewModel.stream.lessons[1].id],
                url: 'path/to/summary1',
            };

            summary2 = {
                id: '2',
                title: 'summary2',
                lessons: [playerViewModel.stream.lessons[3].id],
                url: 'path/to/summary2',
            };

            playerViewModel.stream.summaries = [summary1, summary2];
            lesson.stream.summaries = playerViewModel.stream.summaries;
            playerViewModel._buildNewSummaryUnlocks(); // workaround test setup complexity
        });

        it('should not show course summaries if user failed assessment lesson', () => {
            playerViewModel.lesson.assessment = true;
            Object.defineProperty(playerViewModel, 'lessonFailed', {
                value: true,
            });
            render();
            SpecHelper.expectNoElement(elem, '.unlockable-summaries');
        });

        it('should show course summaries if user succeeded in assessment lesson', () => {
            playerViewModel.lesson.assessment = true;
            Object.defineProperty(playerViewModel, 'lessonFailed', {
                value: false,
            });
            render();
            SpecHelper.expectElement(elem, '.unlockable-summaries');
        });

        it('should display inline within the chapter lessons listing as well', () => {
            render();
            SpecHelper.expectElementText(elem, '.unlockable-summaries', 'Unlocked summary2!');
            jest.spyOn(scope, 'loadUrl').mockImplementation(() => {});
            SpecHelper.click(elem, '.unlockable-summaries .stream-summary');
            expect(scope.loadUrl).toHaveBeenCalledWith(summary2.url, '_blank');
        });
    });

    describe('key terms filtering', () => {
        beforeEach(() => {
            EventLogger = $injector.get('EventLogger');
            jest.spyOn(EventLogger.instance, 'log');
        });
        it('should filter all key terms', () => {
            playerViewModel.lesson.key_terms = [
                'a regular term',
                'a term with {green:color}',
                'a term with [[modals]]',
                'a term with mathjax',
                'a term with %%mathjax%%',
            ];
            jest.spyOn(FormatsText, 'stripFormatting');
            render();
            expect(FormatsText.stripFormatting).toHaveBeenCalledWith('a regular term');
            expect(FormatsText.stripFormatting).toHaveBeenCalledWith('a term with {green:color}');
            expect(FormatsText.stripFormatting).toHaveBeenCalledWith('a term with [[modals]]');
            expect(FormatsText.stripFormatting).toHaveBeenCalledWith('a term with %%mathjax%%');

            // check that we deduped after sanitizing and capitalizing
            expect(scope.keyTerms).toEqual([
                'A regular term',
                'A term with color',
                'A term with modals',
                'A term with mathjax',
            ]);
        });
    });

    describe('assessment', () => {
        it('should show complete header', () => {
            playerViewModel.lesson.assessment = true;
            Object.defineProperty(playerViewModel, 'lessonFailed', {
                value: false,
            });
            render();
            SpecHelper.expectElementText(elem, '.completed-header', 'SMARTCASE COMPLETE');
        });
        it('should show failed', () => {
            playerViewModel.lesson.assessment = true;
            Object.defineProperty(playerViewModel, 'lessonFailed', {
                value: true,
            });
            render();
            SpecHelper.expectElementText(elem, '.completed-header', 'SMARTCASE');
        });
        it('should show failed with a very close score', () => {
            playerViewModel.lesson.assessment = true;
            Object.defineProperty(playerViewModel, 'lessonFailed', {
                value: true,
            });
            Object.defineProperty(playerViewModel, 'lessonScore', {
                value: 39.0 / 49.0,
            });
            render();
            $timeout.flush();
            SpecHelper.expectElement(elem, '.score-gauge[data-gauge=79]'); // Math.floor(39.0/49.0)
            SpecHelper.expectElementText(elem, '.completed-header', 'SMARTCASE');
        });
    });

    describe('test lesson', () => {
        let $rootScope;

        beforeEach(() => {
            $rootScope = $injector.get('$rootScope');
            lesson.test = true;
            $rootScope.currentUser.relevant_cohort = {
                supportsAutograding: false,
            };
        });

        it('should make some display adjustments', () => {
            render();
            SpecHelper.expectElement(elem, '.finish-screen-container.test');
            SpecHelper.expectElementText(elem, '.completed-header', 'TEST COMPLETE');
            SpecHelper.expectNoElement(elem, '.key-terms');
            SpecHelper.expectNoElement(elem, 'assessment-lesson-end');
            SpecHelper.expectElement(elem, '.test-lesson-end > img');
            SpecHelper.expectNoElement(elem, '.test-lesson-end > h2');
        });

        it('should show proper test complete message', () => {
            $rootScope.currentUser.relevant_cohort.supportsAutograding = false;
            render();
            SpecHelper.expectNoElement(elem, '.test-lesson-end > h2');

            $rootScope.currentUser.relevant_cohort.supportsAutograding = true;
            render();
            SpecHelper.expectElementText(
                elem,
                '.test-lesson-end > h2',
                'Your score will be automatically processed. If this is your final exam, you should see your results shortly on the home screen.',
            );

            $rootScope.currentUser = null;
            render();
            SpecHelper.expectNoElement(elem, '.test-lesson-end > h2');
        });
    });

    it('should show lesson-finish-screen-footer', () => {
        render();
        SpecHelper.expectElement(elem, 'lesson-finish-screen-footer');
    });

    it('should handle demo mode', () => {
        playerViewModel.demoMode = true;
        render();
        SpecHelper.expectNoElement(elem, 'lesson-finish-screen-footer');
        SpecHelper.expectNoElement(elem, '.key-terms');
    });

    function render() {
        renderer.render('<lesson-finish-screen player-view-model="playerViewModel"></lesson-finish-screen>');
        scope = renderer.childScope;
        elem = renderer.elem;
    }
});
