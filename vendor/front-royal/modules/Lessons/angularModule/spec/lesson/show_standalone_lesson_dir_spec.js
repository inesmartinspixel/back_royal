import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/lessons';
import setSpecLocales from 'Translation/setSpecLocales';
import showFramesPlayerLocales from 'Lessons/locales/lessons/lesson/show_frames_player-en.json';

setSpecLocales(showFramesPlayerLocales);

describe('Lesson.ShowStandaloneLessonDir', () => {
    let $injector;
    let elem;
    let FrameList;
    let SpecHelper;
    let lesson;
    let $rootScope;
    let playerViewModel;
    let Lesson;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector => {
                $injector = _$injector;
                Lesson = $injector.get('Lesson');
                FrameList = $injector.get('Lesson.FrameList');
                SpecHelper = $injector.get('SpecHelper');
                $injector.get('MockIguana');
                $injector.get('LessonFixtures');
                lesson = FrameList.fixtures.getInstance();
                Lesson.setAdapter('Iguana.Mock.Adapter');
                $rootScope = $injector.get('$rootScope');
                $injector.get('LessonProgress').expect('create');

                // the lesson will not be in a stream
                lesson.$$embeddedIn = undefined;
            },
        ]);
        SpecHelper.stubConfig();
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    it('should display the lesson without a currentUser', () => {
        expect($rootScope.currentUser).toBeUndefined(); // sanity check
        render();
        expect(playerViewModel.logProgress).toBe(false);
    });

    it('should display the lesson with a currentUser', () => {
        SpecHelper.stubCurrentUser();
        render();
        expect(playerViewModel.logProgress).toBe(true);
    });

    it('should set demoMode on the playerViewModel', () => {
        SpecHelper.stubCurrentUser();
        render('demo');
        expect(playerViewModel.demoMode).toBe(true);

        render('');
        expect(playerViewModel.demoMode).toBe(false);

        render('anotherMode');
        expect(playerViewModel.demoMode).toBe(false);
    });

    function render(mode) {
        Lesson.expect('show').returns(lesson);
        const renderer = SpecHelper.renderer();
        renderer.render(`<show-standalone-lesson lesson-id="${lesson.id}" mode="${mode}"></show-standalone-lesson>`);
        Lesson.flush('show');
        elem = renderer.elem;

        const showLessonElem = SpecHelper.expectElement(elem, '.show_lesson');
        playerViewModel = showLessonElem.scope().playerViewModel;
        expect(playerViewModel.lesson.id).toEqual(lesson.id);
    }
});
