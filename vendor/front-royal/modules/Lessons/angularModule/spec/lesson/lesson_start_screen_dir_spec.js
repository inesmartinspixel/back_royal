import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/lessons';
import 'Lessons/angularModule/spec/_mock/fixtures/streams';
import setSpecLocales from 'Translation/setSpecLocales';
import lessonStartScreenLocales from 'Lessons/locales/lessons/lesson/lesson_start_screen-en.json';

setSpecLocales(lessonStartScreenLocales);

describe('Lesson.LessonStartScreenDir', () => {
    let $injector;
    let SpecHelper;
    let Lesson;
    let lesson;
    let playerViewModel;
    let scope;
    let elem;
    let renderer;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector => {
                $injector = _$injector;
                SpecHelper = $injector.get('SpecHelper');
                $injector.get('LessonFixtures');
                $injector.get('StreamFixtures');
                Lesson = $injector.get('Lesson');
                lesson = Lesson.fixtures.getInstance();
                playerViewModel = lesson.createPlayerViewModel();
                renderer = SpecHelper.renderer();
                renderer.scope.playerViewModel = playerViewModel;
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    it('should display the lesson descriptions', () => {
        playerViewModel.lesson.description = [
            'a regular description',
            'a description with {green:color}',
            'a description with [[modals]]',
            'a description with mathjax',
            'a description with %%mathjax%%',
        ];

        render();

        for (let i = 0; i < playerViewModel.lesson.description.length; i++) {
            expect(scope.descriptionBullets[i].text).toEqual(playerViewModel.lesson.description[i]);
        }

        SpecHelper.expectElements(elem, 'ul.drop-2 > li > span', playerViewModel.lesson.description.length);
    });

    describe('assessment lesson', () => {
        it('should display minimum completion score', () => {
            lesson.assessment = true;
            render();
            SpecHelper.expectElementText(elem, 'h2.min-score', 'Score 80% or higher to complete this SMARTCASE');
        });

        it('should not display the "what you will learn" text', () => {
            lesson.assessment = true;
            render();
            SpecHelper.expectNoElement(elem, 'h2.learn');
        });
    });

    describe('test lesson', () => {
        beforeEach(() => {
            lesson.test = true;
            render();
        });

        it('should hide min-score and "why you will learn" sections', () => {
            SpecHelper.expectNoElement(elem, 'h2.min-score');
            SpecHelper.expectNoElement(elem, 'h2.learn');
        });
    });

    function render() {
        renderer.render('<lesson-start-screen player-view-model="playerViewModel"></lesson-start-screen>');
        scope = renderer.childScope;
        elem = renderer.elem;
    }
});
