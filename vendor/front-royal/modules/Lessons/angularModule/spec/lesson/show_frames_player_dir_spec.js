import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Editor/angularModule';
import 'DialogModal/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/lessons';
import setSpecLocales from 'Translation/setSpecLocales';
import showFramesPlayerLocales from 'Lessons/locales/lessons/lesson/show_frames_player-en.json';
import appHeaderViewModelLocales from 'Navigation/locales/navigation/app_header_view_model-en.json';
import previousButtonDesktopLocales from 'Navigation/locales/navigation/previous_button_desktop-en.json';
import lessonStartScreenLocales from 'Lessons/locales/lessons/lesson/lesson_start_screen-en.json';
import lessonFinishScreenFooterLocales from 'Lessons/locales/lessons/lesson/lesson_finish_screen_footer-en.json';
import lessonPlayerViewModelLocales from 'Lessons/locales/lessons/models/lesson/lesson_player_view_model-en.json';
import feedbackSidebarLocales from 'Feedback/locales/feedback/feedback_sidebar-en.json';
import appHeaderLocales from 'Navigation/locales/navigation/app_header-en.json';

setSpecLocales(
    showFramesPlayerLocales,
    previousButtonDesktopLocales,
    lessonStartScreenLocales,
    lessonFinishScreenFooterLocales,
    lessonPlayerViewModelLocales,
    feedbackSidebarLocales,
    appHeaderLocales,
);
setSpecLocales(appHeaderViewModelLocales);

describe('Lessons.ShowFramesPlayerDir', () => {
    let renderer;
    let scope;
    let sampleLesson;
    let elem;
    let FrameList;
    let Frame;
    let playerViewModel;
    let SpecHelper;
    let $timeout;
    let AppHeaderViewModel;
    let $injector;
    let $ocLazyLoad;
    let $q;
    let origManifest;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Editor', 'SpecHelper', 'DialogModal');

        angular.mock.inject([
            '$injector',
            'MockIguana',
            'LessonFixtures',
            'MockIguana',
            _$injector => {
                $injector = _$injector;
                FrameList = $injector.get('Lesson.FrameList');
                Frame = $injector.get('Lesson.FrameList.Frame');
                SpecHelper = $injector.get('SpecHelper');
                AppHeaderViewModel = $injector.get('Navigation.AppHeader.AppHeaderViewModel');
                $timeout = $injector.get('$timeout');
                SpecHelper.stubDirective('lessonFinishScreen');
                $ocLazyLoad = $injector.get('$ocLazyLoad');
                $q = $injector.get('$q');

                SpecHelper.stubConfig();
                SpecHelper.stubEventLogging();

                // stub out front-royal-footer so that the continue button
                // does not get removed from the screen, since there is no
                // app-shell here
                SpecHelper.stubDirective('frontRoyalFooterContent');

                sampleLesson = FrameList.fixtures.getInstance().includeProgress();

                // mock this out
                jest.spyOn(FrameList.PlayerViewModel.prototype, '_logLessonComplete').mockImplementation(() => {});
                jest.spyOn(FrameList.PlayerViewModel.prototype, '_logLessonStart').mockImplementation(() => {});

                // Since we're just using the Frame base class, we need
                // to set directiveName, which would normally be defined
                // in a subclass, to make sure we're not testing subclass-specific functionality
                Frame.include({
                    directiveName: 'frame-directive',
                    buttonDirectiveName: 'frame-directive-button',
                });

                Frame.FrameViewModel.prototype.setBackgroundColor = jest.fn();

                jest.spyOn($ocLazyLoad, 'load').mockImplementation(() => {});

                // Normally, this would be called by RouteAssetLoader.loadGlobalDependencies()
                AppHeaderViewModel.refreshTranslations();

                origManifest = window.webpackManifest;
                window.webpackManifest = window.webpackManifest || {};
                window.webpackManifest['assessment.css'] = '/assets/styles/assessment.css';

                render();
            },
        ]);
    });

    afterEach(() => {
        window.webpackManifest = origManifest;
    });

    describe('continue button', () => {
        function renderStart() {
            render();
            scope.show = 'startScreen';
            scope.$digest();
        }

        // Before filling these in, try to be more certain they
        // don't exist elsewhere
        it('should work in normal case', () => {
            renderStart();
            SpecHelper.expectElementText(elem, 'button.continue.green', 'GET STARTED');
        });
        it('should have special assessment styling', () => {
            sampleLesson.assessment = true;
            renderStart();
            SpecHelper.expectElementText(elem, 'button.continue.turquoise', 'GET STARTED');
        });
        it('should have special exam styling', () => {
            sampleLesson.test = true;
            renderStart();
            SpecHelper.expectElementText(elem, 'button.continue.turquoise', 'BEGIN TEST');
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        if (scope) {
            scope.$destroy();
        }
        renderer = SpecHelper.renderer();
        renderer.scope.playerViewModel = sampleLesson.createPlayerViewModel();
        playerViewModel = renderer.scope.playerViewModel;
        renderer.render('<show-frames-player player-view-model="playerViewModel" />');
        elem = renderer.elem;
        scope = renderer.scope;
    }

    const expectActiveFrame = frame => {
        runFrameTransition();
        const frameContent = SpecHelper.expectElement(elem, '[name="active_frame"]');
        const buttonContent = SpecHelper.expectElement(elem, '.continue-button-animation-wrapper');

        // check that the classname was added
        SpecHelper.expectElementHasClass(frameContent, frame.frame_type);

        // check that the directive was included
        expect(frameContent[0].nodeName.toLowerCase()).toBe(frame.directiveName);
        expect(buttonContent[0].nodeName.toLowerCase()).toBe(frame.buttonDirectiveName);
        expect(scope.playerViewModel.activeFrame).toBe(frame);
        expect(scope.playerViewModel.activeFrameViewModel.setBackgroundColor).toHaveBeenCalled();
    };

    function expectLessonStarted() {
        // Show frame list should always auto-start a lesson
        expect(scope.playerViewModel.started).toBe(true);
    }

    function gotoNextFrame() {
        scope.playerViewModel.gotoNext();
        scope.$digest();
        runFrameTransition();
    }

    function runFrameTransition() {
        $timeout.flush();
        $timeout.flush();
    }

    function gotoPreviousFrame() {
        scope.playerViewModel.gotoPrev();
        scope.$digest();
    }

    it('should pre-load assessment (end screen) styling on an assessment lesson', () => {
        sampleLesson.assessment = true;
        $ocLazyLoad.load.mockClear();
        render();
        expect($ocLazyLoad.load).not.toHaveBeenCalled();
        runFrameTransition();
        expect($ocLazyLoad.load).toHaveBeenCalledWith('assets/styles/assessment.css');
    });

    it('should pre-load assessment (end screen) styling on an assessment lesson', () => {
        sampleLesson.assessment = false;
        $ocLazyLoad.load.mockClear();
        render();
        runFrameTransition();
        expect($ocLazyLoad.load).not.toHaveBeenCalled();
    });

    it('should show lesson start screen', () => {
        // we only show the start screen when there is no
        // lesson progress
        sampleLesson.lesson_progress = undefined;
        render();
        AppHeaderViewModel.playerViewModel = playerViewModel; // attach playerViewModel to AppHeaderViewModel to test instructions
        runFrameTransition();
        SpecHelper.expectElement(elem, 'lesson-start-screen');
        SpecHelper.expectNoElement(elem, '.frame');
        expect(AppHeaderViewModel.frameInstructions).toBe('Begin Lesson');
        expect(AppHeaderViewModel.frameInstructionsForView).toBe('');
        $timeout.flush();
        expect(AppHeaderViewModel.frameInstructionsForView).toBe('Begin Lesson');
        expect(AppHeaderViewModel.showFrameInstructions).toBe(true);
        SpecHelper.click(elem, '.lesson-start-screen-footer button');
        runFrameTransition();
        SpecHelper.expectNoElement(elem, 'lesson-start-screen');
        SpecHelper.expectElement(elem, '.frame');

        // make sure going back to it works
        playerViewModel.showStartScreen = true;
        scope.$digest();
        runFrameTransition();
        SpecHelper.expectElement(elem, 'lesson-start-screen');
        SpecHelper.expectNoElement(elem, '.frame');
    });

    it('should format the start screen for assessment lessons', () => {
        sampleLesson.lesson_progress = undefined;
        sampleLesson.assessment = true;
        render();
        runFrameTransition();
        SpecHelper.expectElement(elem, '.lesson-start-screen.turquoise');
        SpecHelper.expectElement(elem, '.lesson-start-screen-footer button.turquoise');
    });

    it('should show lesson finish screen', () => {
        render();
        AppHeaderViewModel.playerViewModel = playerViewModel; // attach playerViewModel to AppHeaderViewModel to test instructions
        runFrameTransition();
        SpecHelper.expectNoElement(elem, 'lesson-finish-screen');
        SpecHelper.expectNoElement(elem, '.lesson-finish-screen-footer');
        SpecHelper.expectElement(elem, '.frame');

        playerViewModel.showFinishScreen = true;
        scope.$digest();
        runFrameTransition();
        SpecHelper.expectElement(elem, 'lesson-finish-screen');
        SpecHelper.expectElement(elem, '.lesson-finish-screen-footer');
        SpecHelper.expectNoElement(elem, '.frame');
        expect(AppHeaderViewModel.frameInstructions).toBe('Lesson Complete');
        expect(AppHeaderViewModel.frameInstructionsForView).toBe('');
        $timeout.flush();
        expect(AppHeaderViewModel.frameInstructionsForView).toBe('Lesson Complete');
        expect(AppHeaderViewModel.showFrameInstructions).toBe(true);

        // make sure going back to the previous frame works
        playerViewModel.gotoPrev();
        scope.$digest();
        runFrameTransition();
        SpecHelper.expectNoElement(elem, 'lesson-finish-screen');
        SpecHelper.expectNoElement(elem, '.lesson-finish-screen-footer');
        SpecHelper.expectElement(elem, '.frame');

        // should show the
        playerViewModel.showFinishScreen = true;
        scope.$digest();
        runFrameTransition();
    });

    it('should hide lesson finish screen footer in demo mode', () => {
        render();
        AppHeaderViewModel.playerViewModel = playerViewModel; // attach playerViewModel to AppHeaderViewModel to test instructions
        playerViewModel.demoMode = true;
        playerViewModel.showFinishScreen = true;
        scope.$digest();
        runFrameTransition();
        SpecHelper.expectElement(elem, 'lesson-finish-screen');
        SpecHelper.expectNoElement(elem, '.lesson-finish-screen-footer');
    });

    it('should cleanup playerViewModel on destroy', () => {
        jest.spyOn(scope.playerViewModel, 'destroy').mockImplementation(() => {});
        scope.$destroy();
        expect(scope.playerViewModel.destroy).toHaveBeenCalled();
    });

    it('should cleanup playerViewModel on routeChangeStart', () => {
        jest.spyOn(scope.playerViewModel, 'destroy').mockImplementation(() => {});
        $injector.get('$rootScope').$broadcast('$routeChangeStart');
        expect(scope.playerViewModel.destroy).toHaveBeenCalled();
    });

    it('should show the correct activeFrame during lesson progression', () => {
        expectLessonStarted();
        expectActiveFrame(sampleLesson.frames[0]);
        gotoNextFrame();
        expectActiveFrame(sampleLesson.frames[1]);
    });

    // see https://trello.com/c/FvrAjwim/1033-bug-answer-messages-comes-back-after-branching-frame-related-to-practice-refactor#
    it('should successfully dismiss messages if preloading for the next frame completes while last frame is transitioning away', () => {
        expectLessonStarted();
        expectActiveFrame(sampleLesson.frames[0]);
        jest.spyOn(scope.playerViewModel, 'clearMessage').mockImplementation(() => {});

        // The current frame is completed, but preloading has not
        // yet finished for the next frame.  The current frame starts to
        // transition away.
        scope.playerViewModel.activeFrame = null;
        scope.$digest();

        // Before the last frame is done transitioning away,
        // the next frame finished preloading and becomes the
        // active frame
        scope.playerViewModel.activeFrame = sampleLesson.frames[1];
        scope.$digest();

        $timeout.flush();

        expectActiveFrame(sampleLesson.frames[1]);
        expect(scope.playerViewModel.clearMessage).toHaveBeenCalled();
    });

    it('should show a back button on all frames', () => {
        // render the header too, since this is where the back button now lives
        Object.defineProperty(AppHeaderViewModel, 'layout', {
            writable: true,
        });
        AppHeaderViewModel.layout = 'lesson-player';
        const headerRenderer = SpecHelper.renderer();
        headerRenderer.render('<div class="wrap"><app-header></app-header></div>');
        // in the wild, this would be handled by the show-lesson directive
        playerViewModel.linkToWindow($injector);
        const headerElem = headerRenderer.elem;
        const headerScope = headerElem.scope();

        // mock out miniInstructions
        Object.defineProperty(Frame.prototype, 'miniInstructions', {
            value: 'instructionsText',
        });

        expectLessonStarted();
        expectActiveFrame(sampleLesson.frames[0]);
        expect(headerElem.find('.exit-player-container > img').length).toBe(1);
        gotoNextFrame();
        headerScope.$digest();
        expectActiveFrame(sampleLesson.frames[1]);
        expect(headerElem.find('.exit-player-container > img').length).toBe(1);
        gotoPreviousFrame();
        headerScope.$digest();
        gotoNextFrame();
        headerScope.$digest();
        expectActiveFrame(sampleLesson.frames[1]);
        expect(headerElem.find('.exit-player-container > img').length).toBe(1);
    });

    it('should emit an event to scroll to the top of the main container', () => {
        const scrollHelper = $injector.get('scrollHelper');
        jest.spyOn(scrollHelper, 'scrollToTop').mockImplementation(() => {});
        gotoNextFrame();
        expect(scrollHelper.scrollToTop).toHaveBeenCalled();
    });

    describe('messaging support', () => {
        beforeEach(() => {
            // go to the first frame so that message dir will show
            playerViewModel.activeFrameIndex = 0;
            scope.$digest();
            runFrameTransition();
        });

        it('should contain a slide-message directive', () => {
            SpecHelper.expectElement(elem, 'slide-message');
        });

        it('should clear messages after transitioning to next frame', () => {
            jest.spyOn(playerViewModel, 'clearMessage').mockImplementation(() => {});
            gotoNextFrame();
            expect(playerViewModel.clearMessage).toHaveBeenCalled();
        });

        it('should toggle display of messages during transitions', () => {
            expect(AppHeaderViewModel.showMobileMessages).toBe(true);
            scope.playerViewModel.gotoNext();
            scope.$digest();
            $timeout.flush(50);
            expect(AppHeaderViewModel.showMobileMessages).toBe(false);
            $timeout.flush();
            expect(AppHeaderViewModel.showMobileMessages).toBe(true);
        });
    });

    describe('frame preloading', () => {
        beforeEach(() => {
            // setup the lesson
            sampleLesson = FrameList.fixtures.getInstance();
            SpecHelper.expectEqual(true, sampleLesson.frames.length >= 3, 'frames length');
        });

        it('should preload assets for the first frame when on start screen', () => {
            jest.spyOn(FrameList.PlayerViewModel.prototype, 'preloadAssetsForFrame').mockImplementation(() =>
                $q.when(),
            );
            render();
            expect(scope.playerViewModel.activeFrame).toBeUndefined();
            expect(scope.playerViewModel.showStartScreen).toBe(true);
            expect(FrameList.PlayerViewModel.prototype.preloadAssetsForFrame).toHaveBeenCalledWith(
                sampleLesson.frames[0],
            );
        });
        it('should assets images for the next frame', () => {
            jest.spyOn(FrameList.PlayerViewModel.prototype, 'preloadAssetsForFrame').mockImplementation(() =>
                $q.when(),
            );
            sampleLesson.includeProgress(); // include progress so we will start on first frame instead of start screen
            render();
            expect(FrameList.PlayerViewModel.prototype.preloadAssetsForFrame).toHaveBeenCalledWith(
                sampleLesson.frames[1],
            );
        });
        it('should show messages if preloading takes a long time', () => {
            // return a promise that never resolves from preloadImages
            jest.spyOn(Frame.prototype, 'preloadImages').mockReturnValue($q(() => {}));
            sampleLesson.includeProgress(); // include progress so we will start on first frame instead of start screen
            render();
            $timeout.flush(1);
            SpecHelper.expectElement(elem, 'front-royal-spinner');
            SpecHelper.expectDoesNotHaveClass(elem, '[name="slow-loading-message"]', 'visible');
            SpecHelper.expectDoesNotHaveClass(elem, '[name="slow-loading-exit-message"]', 'visible');
            SpecHelper.expectDoesNotHaveClass(elem, '[name="exit-on-slow-loading"]', 'visible');

            $timeout.flush(10 * 1000);
            SpecHelper.expectElement(elem, 'front-royal-spinner');
            SpecHelper.expectHasClass(elem, '[name="slow-loading-message"]', 'visible');
            SpecHelper.expectDoesNotHaveClass(elem, '[name="slow-loading-exit-message"]', 'visible');
            SpecHelper.expectDoesNotHaveClass(elem, '[name="exit-on-slow-loading"]', 'visible');

            $timeout.flush(10 * 1000);
            SpecHelper.expectElement(elem, 'front-royal-spinner');
            SpecHelper.expectDoesNotHaveClass(elem, '[name="slow-loading-message"]', 'visible');
            SpecHelper.expectHasClass(elem, '[name="slow-loading-exit-message"]', 'visible');
            SpecHelper.expectHasClass(elem, '[name="exit-on-slow-loading"]', 'visible');

            jest.spyOn(AppHeaderViewModel, 'exitButtonClick').mockImplementation(() => {});
            SpecHelper.click(elem, '[name="exit-on-slow-loading"]');
            expect(AppHeaderViewModel.exitButtonClick).toHaveBeenCalled();
        });
    });

    describe('hint button', () => {
        beforeEach(() => {
            gotoNextFrame();
        });

        it('should display if frameViewModel supports it', () => {
            SpecHelper.expectNoElement(elem, '.hint-button');
            scope.activeFrameViewModel.showHintButton = true;
            scope.$digest();
            SpecHelper.expectElement(elem, '.hint-button', 1);
        });

        it('should call giveHint in activeFrameViewModel on mousedown', () => {
            scope.activeFrameViewModel.showHintButton = true;
            jest.spyOn(scope.activeFrameViewModel, 'giveHint').mockImplementation(() => {});
            scope.$digest();
            SpecHelper.mousedown(elem, '.hint-button');
            expect(scope.activeFrameViewModel.giveHint).toHaveBeenCalled();
        });

        it('should add with-message class if necessary', () => {
            scope.activeFrameViewModel.showHintButton = true;
            scope.$digest();
            SpecHelper.expectDoesNotHaveClass(elem, '.hint-button', 'with-message');
            jest.spyOn($injector.get('Lesson.MessagingService'), 'hasMessage', 'get').mockReturnValue(true);
            scope.$digest();
            SpecHelper.expectHasClass(elem, '.hint-button', 'with-message');
        });
    });
});
