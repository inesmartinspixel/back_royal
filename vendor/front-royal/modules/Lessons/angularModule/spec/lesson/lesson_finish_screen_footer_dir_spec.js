import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/lessons';
import 'Lessons/angularModule/spec/_mock/fixtures/streams';
import setSpecLocales from 'Translation/setSpecLocales';
import lessonFinishScreenFooterLocales from 'Lessons/locales/lessons/lesson/lesson_finish_screen_footer-en.json';

setSpecLocales(lessonFinishScreenFooterLocales);

describe('Lesson.LessonFinishScreenFooterDir', () => {
    let $injector;
    let SpecHelper;
    let EventLogger;
    let Capabilities;
    let Lesson;
    let $location;
    let lesson;
    let playerViewModel;
    let scope;
    let elem;
    let $interval;
    let $timeout;
    const goodReasons = ['Good1', 'Good2', 'Good3'];
    const badReasons = ['Bad1', 'Bad2', 'Bad3'];

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector => {
                $injector = _$injector;
                SpecHelper = $injector.get('SpecHelper');
                $injector.get('LessonFixtures');
                $injector.get('StreamFixtures');
                Lesson = $injector.get('Lesson');
                $interval = $injector.get('$interval');
                Capabilities = $injector.get('Capabilities');
                $timeout = $injector.get('$timeout');
                lesson = Lesson.fixtures.getInstance();
                playerViewModel = lesson.createPlayerViewModel();
                SpecHelper.stubEventLogging();
                SpecHelper.disableHttpQueue();
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('feedback', () => {
        beforeEach(() => {
            EventLogger = $injector.get('EventLogger');
            jest.spyOn(EventLogger.instance, 'log');
            render();
            elem.find('.lesson-finish-screen-footer').width('470px'); // ensure the 'widthableElement' has a width
            $interval.flush(100);
        });
        it('should send an event with a reason and a comment', () => {
            SpecHelper.click(elem, '.feedback [name="thumbs-up"]');
            SpecHelper.expectHasClass(elem, '.feedback [name="thumbs-up"]', 'selected');
            SpecHelper.click(elem, '[name="reason-0"]');
            SpecHelper.click(elem, '[name="reason-next"]');
            SpecHelper.updateTextInput(elem, '[name="additional"]', 'some thoughts');
            SpecHelper.click(elem, '[name="submit"]');
            expect(EventLogger.instance.log).toHaveBeenCalledWith(
                'lesson:feedback',
                angular.extend(
                    {
                        feedbackIsPositive: true,
                        feedbackReasons: [goodReasons[0]],
                        feedbackAdditionalThoughts: 'some thoughts',
                    },
                    playerViewModel.logInfo(),
                ),
                {},
            );
        });
        it('should send an event with a reason and no comment', () => {
            SpecHelper.click(elem, '.feedback [name="thumbs-down"]');
            SpecHelper.expectHasClass(elem, '.feedback [name="thumbs-down"]', 'selected');
            SpecHelper.click(elem, '[name="reason-1"]');
            SpecHelper.click(elem, '[name="reason-2"]');
            SpecHelper.click(elem, '[name="reason-next"]');
            SpecHelper.click(elem, '[name="submit"]');
            expect(EventLogger.instance.log).toHaveBeenCalledWith(
                'lesson:feedback',
                angular.extend(
                    {
                        feedbackIsPositive: false,
                        feedbackReasons: [badReasons[1], badReasons[2]],
                        feedbackAdditionalThoughts: undefined,
                    },
                    playerViewModel.logInfo(),
                ),
                {},
            );
        });
        it('should skip to submit on mobile', () => {
            Object.defineProperty(Capabilities, 'touchEnabled', {
                value: true,
            });
            SpecHelper.click(elem, '.feedback [name="thumbs-down"]');
            SpecHelper.click(elem, '[name="reason-next"]');
            expect(EventLogger.instance.log).toHaveBeenCalledWith(
                'lesson:feedback',
                angular.extend(
                    {
                        feedbackIsPositive: false,
                        feedbackReasons: [],
                        feedbackAdditionalThoughts: undefined,
                    },
                    playerViewModel.logInfo(),
                ),
                {},
            );
        });
        it('should submit a partially completed feedback on destroy', () => {
            SpecHelper.click(elem, '.feedback [name="thumbs-up"]');
            scope.$destroy();
            expect(EventLogger.instance.log).toHaveBeenCalledWith(
                'lesson:feedback',
                angular.extend(
                    {
                        feedbackIsPositive: true,
                        feedbackReasons: undefined,
                        feedbackAdditionalThoughts: undefined,
                    },
                    playerViewModel.logInfo(),
                ),
                {},
            );
        });
        it('should not submit feedback if not started on destroy', () => {
            EventLogger.instance.log.mockClear();
            scope.$destroy();
            expect(EventLogger.instance.log.mock.calls.length).toBe(0);
        });
        it('should not submit feedback if already submitted on destroy', () => {
            EventLogger.instance.log.mockClear();
            SpecHelper.click(elem, '.feedback [name="thumbs-up"]');
            SpecHelper.click(elem, '[name="reason-0"]');
            SpecHelper.click(elem, '[name="reason-next"]');
            SpecHelper.updateTextInput(elem, '[name="additional"]', 'some thoughts');
            SpecHelper.click(elem, '[name="submit"]');
            expect(EventLogger.instance.log.mock.calls.length).toBe(1);
            scope.$destroy();
            expect(EventLogger.instance.log.mock.calls.length).toBe(1);
        });
    });

    describe('assessment', () => {
        it('should show feedback when complete', () => {
            Object.defineProperty(playerViewModel, 'lessonFailed', {
                value: false,
            });
            playerViewModel.lesson.assessment = true;
            render();
            SpecHelper.expectElementNotHidden(elem, '.feedback');
            SpecHelper.expectNoElement(elem, '.failed-message');
        });
        it('should hide feedback when failed', () => {
            Object.defineProperty(playerViewModel, 'lessonFailed', {
                value: true,
            });
            playerViewModel.lesson.assessment = true;
            render();
            SpecHelper.expectElementHidden(elem, '.feedback');
            SpecHelper.expectElementText(
                elem,
                '.failed-message',
                'Almost there! Score 80% or higher to complete this SMARTCASE.',
            );
        });
    });

    describe('continue button', () => {
        let ContentAccessHelper;
        beforeEach(() => {
            $location = $injector.get('$location');
            jest.spyOn($location, 'url').mockImplementation(() => {});
            ContentAccessHelper = $injector.get('ContentAccessHelper');
        });
        it('should work when next lesson is launchable', () => {
            const nextLesson = Lesson.fixtures.getInstance();
            Object.defineProperty(lesson, 'nextLessonInStream', {
                value: nextLesson,
            });
            jest.spyOn(ContentAccessHelper, 'canLaunch').mockReturnValue(true);
            render();
            SpecHelper.expectElementText(elem, 'button.continue', `NEXT LESSON: ${nextLesson.title.toUpperCase()}`);
            SpecHelper.click(elem, 'button.continue');
            $timeout.flush();
            expect($location.url).toHaveBeenCalledWith(nextLesson.launchUrl);
        });
        it('should work when next lesson is not launchable (or there is none)', () => {
            jest.spyOn(ContentAccessHelper, 'canLaunch').mockReturnValue(false);
            render();
            SpecHelper.expectElementText(elem, 'button.continue', 'Continue');
            SpecHelper.click(elem, 'button.continue');
            expect($location.url).toHaveBeenCalledWith(lesson.stream().streamDashboardPath);
        });
        it('should adjust to a failed assessment lesson', () => {
            jest.spyOn(ContentAccessHelper, 'canLaunch').mockReturnValue(true);
            playerViewModel = lesson.createPlayerViewModel({
                logProgress: true,
            });
            playerViewModel.lesson.assessment = true;
            Object.defineProperty(playerViewModel, 'lessonFailed', {
                value: true,
            });
            render();
            SpecHelper.expectElementText(elem, 'button.continue', 'RETRY SMARTCASE');
            SpecHelper.expectHasClass(elem, 'button.continue', 'retry');
            SpecHelper.click(elem, 'button.continue');
            expect(scope.playerViewModel.replaceMeWithThisPlayerViewModel).not.toBeUndefined();
            expect(scope.playerViewModel.replaceMeWithThisPlayerViewModel.lesson).toBe(scope.playerViewModel.lesson);
            expect(scope.playerViewModel.replaceMeWithThisPlayerViewModel.logProgress).toBe(true);
        });
    });

    function render() {
        const renderer = SpecHelper.renderer();
        renderer.scope.playerViewModel = playerViewModel;
        renderer.render(
            '<lesson-finish-screen-footer player-view-model="playerViewModel" ><lesson-finish-screen-footer>',
        );
        scope = renderer.childScope;
        scope.goodReasons = goodReasons;
        scope.badReasons = badReasons;
        elem = renderer.elem;
    }
});
