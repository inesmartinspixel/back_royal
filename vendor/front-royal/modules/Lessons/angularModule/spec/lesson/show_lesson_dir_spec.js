import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/lessons';
import setSpecLocales from 'Translation/setSpecLocales';
import showLessonLocales from 'Lessons/locales/lessons/lesson/show_lesson-en.json';
import lessonPlayerViewModelLocales from 'Lessons/locales/lessons/models/lesson/lesson_player_view_model-en.json';

setSpecLocales(showLessonLocales);
setSpecLocales(lessonPlayerViewModelLocales);

describe('Lesson.ShowLessonDir', () => {
    let $injector;
    let renderer;
    let Lesson;
    let SpecHelper;
    let expectedLesson;
    let playerViewModel;
    let EntityMetadata;
    let ContentAccessHelper;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector => {
                $injector = _$injector;
                Lesson = $injector.get('Lesson');
                SpecHelper = $injector.get('SpecHelper');
                EntityMetadata = $injector.get('EntityMetadata');
                $injector.get('MockIguana');
                $injector.get('LessonFixtures');
                ContentAccessHelper = $injector.get('ContentAccessHelper');

                expectedLesson = Lesson.fixtures.getInstance({
                    playerViewModel: {
                        lesson: {},
                    },
                });
                playerViewModel = expectedLesson.createPlayerViewModel();

                Lesson.setAdapter('Iguana.Mock.Adapter');
                SpecHelper.stubConfig();
                jest.spyOn(ContentAccessHelper.prototype, 'canLaunch', 'get').mockReturnValue(true);

                render();
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    it('should call showModalIfCannotLaunch', () => {
        const ContentAccessHelper = $injector.get('ContentAccessHelper');
        jest.spyOn(ContentAccessHelper.prototype, 'showModalIfCannotLaunch').mockImplementation(() => {});
        render();
        expect(ContentAccessHelper.prototype.showModalIfCannotLaunch).toHaveBeenCalled();
    });

    it('should display the lesson via a lesson play once it is loaded up', () => {
        const lessonTypeContent = renderer.elem.children();

        expect(lessonTypeContent.length).toBe(1);

        // check the html includes the directive tag 'lesson-type-1'
        expect(lessonTypeContent[0].outerHTML.indexOf(Lesson.prototype.showDirectiveName)).not.toBe(-1);
    });

    it('should link itself to the window', () => {
        jest.spyOn(playerViewModel, 'linkToWindow').mockImplementation(() => {});
        render();
        expect(playerViewModel.linkToWindow).toHaveBeenCalledWith($injector);
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.playerViewModel = playerViewModel;
        renderer.scope.playerViewModel.lesson.entity_metadata = EntityMetadata.new({
            title: 'askjhsdf',
            description: 'sdjfhsdf',
            canonical_url: 'asdasd',
        });
        renderer.scope.playerViewModel.lesson.author = {
            id: 1,
            email: 'blarg@blarg.com',
        };
        renderer.render('<show-lesson player-view-model="playerViewModel" ><show-lesson>');
    }
});
