import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/componentized_fixtures';
import 'Editor/angularModule';

describe('Componentized.Component.Layout.TextImageInteractive', () => {
    let frame;
    let frameViewModel;
    let viewModel;
    let Componentized;
    let SpecHelper;
    let MaxTextLengthConfig;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                Componentized = $injector.get('Lesson.FrameList.Frame.Componentized');
                SpecHelper = $injector.get('SpecHelper');
                $injector.get('ComponentizedFixtures');
                viewModel = getViewModel();
                MaxTextLengthConfig = $injector.get('MaxTextLengthConfig');
            },
        ]);

        SpecHelper.stubConfig();
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('Model', () => {
        describe('mainTextComponent', () => {
            it('should be gettable', () => {
                const model = frame.addTextImageInteractiveLayout();
                model.staticContentForText = frame.addText();
                expect(model.mainTextComponent).toBe(model.staticContentForText);
            });
        });
    });

    describe('ViewModel', () => {
        it('should support static content for each section', () => {
            const model = frame.addTextImageInteractiveLayout();
            const text = frame.addVanillaComponent();
            model.staticContentForText = text;
            const image1 = frame.addVanillaComponent();
            model.staticContentForFirstImage = image1;
            const image2 = frame.addVanillaComponent();
            model.staticContentForSecondImage = image2;
            const interactive = frame.addVanillaComponent();
            model.staticContentForInteractive = interactive;

            viewModel = frameViewModel.viewModelFor(model);

            SpecHelper.expectEqual(text.id, viewModel.contentForTextViewModel.model.id, 'contentForTextViewModel');
            SpecHelper.expectEqual(
                image1.id,
                viewModel.contentForFirstImageViewModel.model.id,
                'contentForFirstImageViewModel',
            );
            SpecHelper.expectEqual(
                image2.id,
                viewModel.contentForSecondImageViewModel.model.id,
                'contentForSecondImageViewModel',
            );
            SpecHelper.expectEqual(
                interactive.id,
                viewModel.contentForInteractiveViewModel.model.id,
                'contentForInteractiveViewModel',
            );
        });

        it('should support dynamic content for each section', () => {
            const model = frame.addTextImageInteractiveLayout();
            const target = frame.addVanillaComponent();
            model.target = target;
            const targetViewModel = frameViewModel.viewModelFor(target);
            const text = 'staticContentForTextViewModel';
            targetViewModel.contentForTextViewModel = text;
            const image1 = 'staticContentForFirstImageViewModel';
            targetViewModel.contentForFirstImageViewModel = image1;
            const image2 = 'staticContentForSecondImageViewModel';
            targetViewModel.contentForSecondImageViewModel = image2;
            const interactive = 'staticContentForInteractiveViewModel';
            targetViewModel.contentForInteractiveViewModel = interactive;

            viewModel = frameViewModel.viewModelFor(model);

            SpecHelper.expectEqual(text, viewModel.contentForTextViewModel, 'contentForTextViewModel');
            SpecHelper.expectEqual(image1, viewModel.contentForFirstImageViewModel, 'contentForFirstImageViewModel');
            SpecHelper.expectEqual(image2, viewModel.contentForSecondImageViewModel, 'contentForSecondImageViewModel');
            SpecHelper.expectEqual(
                interactive,
                viewModel.contentForInteractiveViewModel,
                'contentForInteractiveViewModel',
            );
        });

        it('should support context image sizing', () => {
            viewModel.model.staticContentForFirstImage = frame.addImage();
            const model = viewModel.model;
            SpecHelper.expectEqual(
                'tallContextImage',
                model.imageContext('staticContentForFirstImage'),
                'model.imageContext("staticContentForFirstImage")',
            );
            SpecHelper.expectEqual(
                'tallContextImage',
                viewModel.contextForFirstImage,
                'viewModel.contextForFirstImage',
            );
            viewModel.model.context_image_size = 'short';
            SpecHelper.expectEqual(
                'shortContextImage',
                model.imageContext('staticContentForFirstImage'),
                'model.imageContext("staticContentForFirstImage")',
            );
            SpecHelper.expectEqual(
                'shortContextImage',
                viewModel.contextForFirstImage,
                'viewModel.contextForFirstImage',
            );
            SpecHelper.expectEqual(viewModel.firstContextImageSize, viewModel.model.context_image_size);

            viewModel.model.staticContentForSecondImage = frame.addImage();
            viewModel.model.context_image_2_size = 'long';
            SpecHelper.expectEqual(viewModel.secondContextImageSize, viewModel.model.context_image_2_size);
            SpecHelper.expectEqual(
                'tallContextImage',
                model.imageContext('staticContentForSecondImage'),
                'model.imageContext("staticContentForSecondImage")',
            );
            SpecHelper.expectEqual(
                'tallContextImage',
                viewModel.contextForSecondImage,
                'viewModel.contextForSecondImage',
            );
            viewModel.model.context_image_2_size = 'short';
            SpecHelper.expectEqual(
                'shortContextImage',
                model.imageContext('staticContentForSecondImage'),
                'model.imageContext("staticContentForSecondImage")',
            );
            SpecHelper.expectEqual(
                'shortContextImage',
                viewModel.contextForSecondImage,
                'viewModel.contextForSecondImage',
            );
        });
    });

    describe('cf-text-image-interactive', () => {
        let elem;

        it('should render content in text section', () => {
            const componentForSection = mockContentFor('text');
            render();
            SpecHelper.expectElement(elem, `.text [component-id="${componentForSection.id}"]`);
        });

        it('should render content in first image section', () => {
            const componentForSection = mockContentFor('first_image');
            render();
            SpecHelper.expectElements(elem, `.context-image-first [component-id="${componentForSection.id}"]`, 1);
        });

        it('should render content in second image section', () => {
            const componentForSection = mockContentFor('second_image');
            render();
            SpecHelper.expectElements(elem, `.context-image-second [component-id="${componentForSection.id}"]`, 1);
        });

        it('should render content in interactive section', () => {
            const componentForSection = mockContentFor('interactive');
            render();
            SpecHelper.expectElement(elem, `.interactive [component-id="${componentForSection.id}"]`);
        });

        it('should add text-only class', () => {
            const model = frame.addTextImageInteractiveLayout(); // do not call addDefaultReferences, so we get no content to start
            viewModel = frameViewModel.viewModelFor(model);
            render();
            SpecHelper.expectNoElement(elem, '.text-only');
            mockContentFor('text');
            render();
            SpecHelper.expectElement(elem, '.text-only');
            mockContentFor('first_image');
            render();
            SpecHelper.expectNoElement(elem, '.text-only');
        });

        function mockContentFor(section) {
            const key = `static_content_for_${section}`.camelize();
            viewModel.model[key] = frame.addVanillaComponent();
            return viewModel.model[key];
        }

        function render() {
            const renderer = SpecHelper.renderer();
            renderer.scope.viewModel = viewModel;
            renderer.render('<cf-ui-component view-model="viewModel"></cf-ui-component>');
            elem = renderer.elem;
        }
    });

    describe('EditorViewModel', () => {
        let editorViewModel;

        beforeEach(() => {
            editorViewModel = frame.editorViewModelFor(viewModel.model);
        });

        describe('firstContextImage', () => {
            it('should return an image component if there is one', () => {
                editorViewModel.model.staticContentForFirstImage = frame.addImage();
                const expectedImage = editorViewModel.model.staticContentForFirstImage;
                SpecHelper.expectEqual(expectedImage.id, editorViewModel.firstContextImage.id);
            });
            it('should be settable to undefined', () => {
                editorViewModel.model.staticContentForFirstImage = frame.addImage();
                expect(editorViewModel.firstContextImage).not.toBeUndefined();
                editorViewModel.firstContextImage = undefined;
                expect(editorViewModel.firstContextImage).toBeUndefined();
                expect(editorViewModel.model.staticContentForFirstImage).toBeUndefined();
            });
            it('should be settable to an image', () => {
                const image = frame.addImage();
                editorViewModel.firstContextImage = image;
                expect(editorViewModel.model.staticContentForFirstImage.id).toEqual(image.id);
            });
            it('should throw if set to something other than an image', () => {
                expect(() => {
                    editorViewModel.firstContextImage = 'something';
                }).toThrow(new Error('Cannot set firstContextImage to something that is not an imageModel.'));
            });
        });

        describe('secondContextImage', () => {
            it('should return an image component if there is one', () => {
                editorViewModel.model.staticContentForSecondImage = frame.addImage();
                const expectedImage = editorViewModel.model.staticContentForSecondImage;
                SpecHelper.expectEqual(expectedImage.id, editorViewModel.secondContextImage.id);
            });
            it('should be settable to undefined', () => {
                editorViewModel.model.staticContentForSecondImage = frame.addImage();
                expect(editorViewModel.secondContextImage).not.toBeUndefined();
                editorViewModel.secondContextImage = undefined;
                expect(editorViewModel.secondContextImage).toBeUndefined();
                expect(editorViewModel.model.staticContentForSecondImage).toBeUndefined();
            });
            it('should be settable to an image', () => {
                const image = frame.addImage();
                editorViewModel.secondContextImage = image;
                expect(editorViewModel.model.staticContentForSecondImage.id).toEqual(image.id);
            });
            it('should throw if set to something other than an image', () => {
                expect(() => {
                    editorViewModel.secondContextImage = 'something';
                }).toThrow(new Error('Cannot set secondContextImage to something that is not an imageModel.'));
            });
        });

        describe('mainTextComponent', () => {
            beforeEach(() => {
                editorViewModel.model.staticContentForText = undefined;
            });
            it('should be gettable', () => {
                editorViewModel.model.staticContentForText = frame.addText();
                expect(editorViewModel.mainTextComponent).toBe(editorViewModel.model.staticContentForText);
            });
            it('should be settable', () => {
                editorViewModel.mainTextComponent = frame.addText();
                expect(editorViewModel.mainTextComponent).not.toBeUndefined();
                expect(editorViewModel.mainTextComponent).toBe(editorViewModel.model.staticContentForText);
            });
            it('should throw if there is already a nonText element in contentForText', () => {
                const component = frame.addVanillaComponent();
                editorViewModel.mainTextComponent = component;
                expect(() => {
                    editorViewModel.mainTextComponent = frame.addText();
                }).toThrow(new Error(`Cannot replace existing ${component.type} with setter on mainTextComponent.`));
            });
        });

        describe('maxRecommendedTextLength', () => {
            it('should set config values on staticContentForText', () => {
                editorViewModel.model.staticContentForText = frame.addText();
                editorViewModel.model.staticContentForFirstImage = undefined;
                expect(editorViewModel.model.staticContentForText.editorViewModel.maxRecommendedTextLength()).toBe(
                    MaxTextLengthConfig.TEXT_WITHOUT_IMAGE,
                );
                editorViewModel.model.staticContentForFirstImage = frame.addImage();
                expect(editorViewModel.model.staticContentForText.editorViewModel.maxRecommendedTextLength()).toBe(
                    MaxTextLengthConfig.TEXT_WITH_IMAGE,
                );
                editorViewModel.model.staticContentForFirstImage = undefined;
                editorViewModel.model.staticContentForSecondImage = frame.addImage();
                expect(editorViewModel.model.staticContentForText.editorViewModel.maxRecommendedTextLength()).toBe(
                    MaxTextLengthConfig.TEXT_WITH_IMAGE,
                );
            });
        });
    });

    describe('cf-text-image-interactive-editor', () => {
        let elem;
        let model;

        it('should show a title for each section', () => {
            model = viewModel.model;
            model.staticContentForText = frame.addVanillaComponent();
            model.staticContentForFirstImage = frame.addVanillaComponent();
            model.staticContentForSecondImage = frame.addVanillaComponent();
            model.staticContentForInteractive = frame.addVanillaComponent();
            render();
            expect(SpecHelper.trimText(elem.find('h4').eq(0).text())).toEqual('Options');
            expect(SpecHelper.trimText(elem.find('h4').eq(1).text())).toEqual('Text');
            expect(SpecHelper.trimText(elem.find('h4').eq(2).text())).toEqual('Images');
            expect(SpecHelper.trimText(elem.find('h4').eq(3).text())).toEqual('Context Images');
        });

        describe('options', () => {
            it('should have a next frame selector that sets the next frame on the frameNavigator', () => {
                const frame2 = viewModel.model.frame().lesson().addFrame();
                render();
                expect(model.frame().frameNavigator.next_frame_id).toBeUndefined();
                SpecHelper.selectOptionByLabel(elem, '[name="options"] cf-next-frame-selector select', frame2.label);
                expect(model.frame().frameNavigator.next_frame_id).toBe(frame2.id);
                SpecHelper.selectOptionByLabel(
                    elem,
                    '[name="options"] cf-next-frame-selector select',
                    ' --- Next frame --- ',
                );
                expect(model.frame().frameNavigator.next_frame_id).toBeUndefined();
            });
        });

        function render() {
            SpecHelper.stubDirective('cfImageEditor');
            const renderer = SpecHelper.renderer();
            model = viewModel.model;
            renderer.scope.editorViewModel = frame.editorViewModelFor(model);

            // the frame selector component references text_content, which
            // means this needs to be the mainUiComponent, as it will be in the wild.
            model.frame().mainUiComponent = model;

            renderer.scope.frameViewModel = frameViewModel;
            renderer.render(
                '<cf-component-editor editor-view-model="editorViewModel" frame-view-model="frameViewModel"></cf-component-editor>',
            );
            elem = renderer.elem;
        }
    });

    function getViewModel(options) {
        const model = getModel(options);
        return frameViewModel.viewModelFor(model);
    }

    function getModel() {
        frame = Componentized.fixtures.getInstance();
        frameViewModel = frame.createFrameViewModel();
        return frame.addTextImageInteractiveLayout().addDefaultReferences();
    }
});
