import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/componentized_fixtures';
import 'Editor/angularModule';

describe('ComponentizedFrame', () => {
    let frame;
    let Componentized;
    let SpecHelper;
    let ComponentModel;
    let $injector;
    let ContinueButtonModel;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                Componentized = $injector.get('Lesson.FrameList.Frame.Componentized');
                SpecHelper = $injector.get('SpecHelper');
                ComponentModel = $injector.get('Lesson.FrameList.Frame.Componentized.Component.ComponentModel');
                ContinueButtonModel = $injector.get(
                    'Lesson.FrameList.Frame.Componentized.Component.ContinueButton.ContinueButtonModel',
                );
                $injector.get('ComponentizedFixtures');
                frame = Componentized.new();
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('miniInstructions', () => {
        beforeEach(() => {
            frame.mainUiComponent = frame.components[0];
            frame.mainUiComponent.editor_template = 'KEY';
        });

        it('should return a value from the map', () => {
            frame.$$_miniInstructionsMap = {
                KEY: 'value',
            };
            SpecHelper.expectEqual('value', frame.miniInstructions, 'miniInstructions from map');
        });

        it('should raise if key is not found in map', () => {
            frame.$$_miniInstructionsMap = {};
            expect(() => {
                frame.miniInstructions; // eslint-disable-line
            }).toThrow(new Error('No miniInstructions for "KEY"'));
        });
    });

    describe('continueButton', () => {
        it('should be settable to a continue button', () => {
            const button = frame.addContinueButton();
            frame.continueButton = button;
            expect(frame.continue_button_id).toEqual(button.id);
            expect(frame.continueButton).toEqual(button);
        });
        it('should be settable to undefined', () => {
            const button = frame.addContinueButton();
            frame.continueButton = button;
            jest.spyOn(button, 'remove').mockImplementation(() => {});
            frame.continueButton = undefined;
            expect(frame.continue_button_id).toBeUndefined();
            expect(frame.continueButton).toBeUndefined();
            expect(button.remove).toHaveBeenCalled();
        });
        it('should throw if set to something that is not a continue button', () => {
            expect(() => {
                frame.continueButton = frame.addVanillaComponent();
            }).toThrow(new Error('Cannot set continueButton to something that is not a ContinueButtonModel.'));
        });
        it('should destroy the current continue button when reset', () => {
            const button1 = frame.addContinueButton();
            const button2 = frame.addContinueButton();
            frame.continueButton = button1;
            jest.spyOn(button1, 'remove').mockImplementation(() => {});
            frame.continueButton = button2;
            expect(frame.continue_button_id).toEqual(button2.id);
            expect(frame.continueButton).toEqual(button2);
            expect(button1.remove).toHaveBeenCalled();
        });
        it('should be unset if continueButton.remove() is called', () => {
            const button = frame.addContinueButton();
            frame.continueButton = button;
            button.remove();
            expect(frame.continue_button_id).toBeUndefined();
            expect(frame.continueButton).toBeUndefined();
        });
    });

    describe('dereference', () => {
        it('should return a component when an id is passed in', () => {
            const component = frame.addComponent(ComponentModel.new());
            SpecHelper.expectEqual(component, frame.dereference(component.id), 'frame.dereference(component.id)');
        });

        it('should throw if component not found for id', () => {
            expect(() => {
                frame.dereference('bad id');
            }).toThrow(new Error('No component found for requested id.'));
        });

        it('should cache lookups for performance', () => {
            const component = frame.addComponent(ComponentModel.new());
            jest.spyOn(frame.components, 'forEach');
            frame.dereference(component.id);
            frame.dereference(component.id);
            SpecHelper.expectEqual(1, frame.components.forEach.mock.calls.length, 'calls to forEach');
        });
    });

    describe('addComponent', () => {
        it('should add a component and set up the iguana references', () => {
            const component = frame.addComponent(ComponentModel.new());
            SpecHelper.expectEqual(frame, component.frame(), 'component.frame()');
            // frame.components[0] is now a FrameNavigator
            SpecHelper.expectEqual(component, frame.components[1]);
        });

        it('should do nothing if the component is already present', () => {
            const component = frame.addComponent(ComponentModel.new());
            expect(() => {
                frame.addComponent(component);
            }).toThrow(new Error('Can only add components that are not on any frame'));
            // frame.components[0] is now a FrameNavigator
            SpecHelper.expectEqual(component.id, frame.components[1].id);
        });
    });

    describe('replaceFrameReferences', () => {
        it('should update selectableAnswerNavigator references', () => {
            const oldFrame = Componentized.fixtures.getInstance();
            const newFrame = Componentized.fixtures.getInstance();
            const referencingFrame = Componentized.fixtures.getInstance();
            referencingFrame.addSelectableAnswerNavigator();
            referencingFrame.frameNavigator.selectableAnswerNavigators[0].next_frame_id = oldFrame.id;
            referencingFrame.replaceFrameReferences(oldFrame, newFrame);
            expect(referencingFrame.frameNavigator.selectableAnswerNavigators[0].next_frame_id).toEqual(newFrame.id);
        });

        it('should update frameNavigator references', () => {
            const oldFrame = Componentized.fixtures.getInstance();
            const newFrame = Componentized.fixtures.getInstance();
            const referencingFrame = Componentized.fixtures.getInstance();
            referencingFrame.addSelectableAnswerNavigator();
            referencingFrame.frameNavigator.next_frame_id = oldFrame.id;
            referencingFrame.replaceFrameReferences(oldFrame, newFrame);
            expect(referencingFrame.frameNavigator.next_frame_id).toEqual(newFrame.id);
        });
    });

    describe('importComponent', () => {
        it('should add referenced components', () => {
            const Subclass = ComponentModel.subclass(function () {
                this.references('something').through('something_id');
                this.references('somethings').through('something_ids');
                this.references('unset').through('unset_id');
            });

            const item = frame.addComponent(
                Subclass.new({
                    something_id: '1',
                    something_ids: ['2', '3'],
                }),
            );
            frame.addComponent(
                Subclass.new({
                    id: '1',
                }),
            );
            frame.addComponent(
                Subclass.new({
                    id: '2',
                }),
            );
            frame.addComponent(
                Subclass.new({
                    id: '3',
                }),
            );

            jest.spyOn(Subclass.prototype, 'indirectlyReferencedComponents');
            const options = {
                addReferences: true,
            };

            const anotherFrame = Componentized.new();
            const clonedComponent = anotherFrame.importComponent(item, options);
            expect(clonedComponent.frame()).toBe(anotherFrame);
            expect(clonedComponent.something.frame()).toBe(anotherFrame);
            expect(clonedComponent.somethings[0].frame()).toBe(anotherFrame);
            expect(clonedComponent.indirectlyReferencedComponents).toHaveBeenCalledWith(options);
        });
    });

    describe('swapComponents', () => {
        let frame;
        let thisComponent;
        let thatComponent;

        beforeEach(() => {
            frame = Componentized.fixtures.getInstance();
            thisComponent = frame.addVanillaComponent();
            thatComponent = frame.addVanillaComponent();
        });
        it('should throw if the old component is not on the frame', () => {
            thisComponent.remove();
            expect(() => {
                frame.swapComponents(thisComponent, thatComponent);
            }).toThrow(new Error('oldComponent is not in this frame'));
        });
        it('should throw if the new component is not on the frame', () => {
            thatComponent.remove();
            expect(() => {
                frame.swapComponents(thisComponent, thatComponent);
            }).toThrow(new Error('newComponent is not in this frame'));
        });
        it('should swap the components', () => {
            const Subclass = ComponentModel.subclass(function () {
                this.references('something').through('something_id');
            });
            const referencedComponent = frame.addComponent(
                Subclass.new({
                    id: 'referencedComponent',
                }),
            );
            referencedComponent.constructor.setEditorViewModel(
                'Lesson.FrameList.Frame.Componentized.Component.ComponentEditorViewModel',
            );

            const referencingComponent = frame.addComponent(
                Subclass.new({
                    something_id: 'referencedComponent',
                }),
            );

            const callback = jest.fn();
            referencingComponent.on('set:something', callback);
            frame.swapComponents(referencedComponent, thatComponent);
            expect(callback).toHaveBeenCalledWith(thatComponent);
            expect(referencingComponent.something).toBe(thatComponent);
        });
    });

    describe('copyMainTextAndImagesFrom', () => {
        let frame;
        let otherFrame;
        let otherImages;
        beforeEach(() => {
            frame = Componentized.fixtures.getInstance();
            otherFrame = Componentized.fixtures.getInstance();
            otherImages = [otherFrame.addImage(), otherFrame.addImage()];
            ['main_image_id', 'text_content', 'mainModalTexts'].forEach(prop => {
                Object.defineProperty(otherFrame, prop, {
                    value: `${prop}_value`,
                });
                Object.defineProperty(frame, prop, {
                    writable: true,
                });
            });
        });

        it('should work', () => {
            frame.copyMainTextAndImagesFrom(otherFrame);

            // images should have been copied over
            expect(frame.dereference(otherImages[0].id)).not.toBeUndefined();
            expect(frame.dereference(otherImages[1].id)).not.toBeUndefined();

            // 3 properties should have been copied over
            expect(frame.text_content).toBe('text_content_value');
            expect(frame.main_image_id).toBe('main_image_id_value');
            expect(frame.mainModalTexts).toBe('mainModalTexts_value');
        });

        it('should not override things it should not override', () => {
            frame.text_content = 'already set text_content';
            frame.main_image_id = 'already set main_image_id';
            frame.mainModalTexts = 'already set mainModalTexts';

            frame.copyMainTextAndImagesFrom(otherFrame);
            expect(frame.text_content).toEqual('already set text_content');
            expect(frame.main_image_id).toEqual('already set main_image_id');

            // we do override main modal texts, because the created ones are
            // just defaults.  A bit hacky
            expect(frame.mainModalTexts).toEqual(otherFrame.mainModalTexts);
        });
    });

    describe('editorViewModelFor', () => {
        it('should delegate to editorViewModels', () => {
            const mockComponent = 'mockComponent';
            jest.spyOn(frame, 'editorViewModelsFor').mockReturnValue(['mockEditorViewModel']);
            expect(frame.editorViewModelFor(mockComponent)).toEqual('mockEditorViewModel');
            expect(frame.editorViewModelsFor).toHaveBeenCalledWith([mockComponent]);
        });
    });

    describe('editorViewModelFor', () => {
        it('should return some editorViewModels', () => {
            const components = [frame.addComponent(ComponentModel.new()), frame.addComponent(ComponentModel.new())];
            const editorViewModels = frame.editorViewModelsFor(components);
            editorViewModels.forEach(editorViewModel => {
                SpecHelper.expectEqual(
                    ComponentModel.EditorViewModel,
                    editorViewModel.constructor,
                    'editorViewModel.constructor',
                );
            });
        });

        it('should cache results', () => {
            const component = frame.addComponent(ComponentModel.new());
            const helper1 = frame.editorViewModelsFor([component])[0];
            const helper2 = frame.editorViewModelsFor([component])[0];
            expect(helper1).toBe(helper2);
        });

        it('should call applyCurrentTemplate', () => {
            const component = frame.addComponent(ComponentModel.new());
            jest.spyOn(ComponentModel.EditorViewModel.prototype, 'applyCurrentTemplate').mockImplementation(() => {});
            frame.editorViewModelsFor([component]);
            expect(ComponentModel.EditorViewModel.prototype.applyCurrentTemplate).toHaveBeenCalled();
        });
    });

    describe('editorViewModelsForType', () => {
        it('should return editorViewModels for all components of a specific type', () => {
            const component = frame.addComponent(ComponentModel.new());
            jest.spyOn(frame, 'componentsForType').mockReturnValue([component]);
            SpecHelper.expectEqual(
                [frame.editorViewModelFor(component)],
                frame.editorViewModelsForType('SomeType'),
                'frame.editorViewModelsForType("SomeType")',
            );
            expect(frame.componentsForType).toHaveBeenCalledWith('SomeType');
        });
    });

    describe('componentsForType', () => {
        let MyComponent;
        let MySubComponent;
        let SomeOtherComponent;
        let myComponent; // eslint-disable-line no-unused-vars
        let mySubComponent;

        beforeEach(() => {
            MyComponent = ComponentModel.subclass();
            MySubComponent = MyComponent.subclass();
            SomeOtherComponent = ComponentModel.subclass();

            [MyComponent, MySubComponent, SomeOtherComponent].forEach(Component => {
                Component.setEditorViewModel('Lesson.FrameList.Frame.Componentized.Component.ComponentEditorViewModel');
            });

            myComponent = frame.addComponent(MyComponent.new());
            mySubComponent = frame.addComponent(MySubComponent.new());
            frame.addComponent(SomeOtherComponent.new());
        });

        it('should return all components for a type', () => {
            SpecHelper.expectEqual(
                [mySubComponent],
                frame.componentsForType(MySubComponent),
                'components for MySubComponent',
            );
        });

        it('should return subclasses as well', () => {
            SpecHelper.expectEqual(
                [myComponent, mySubComponent],
                frame.componentsForType(MyComponent),
                'components for MyComponent',
            );
        });

        it('should cache results for easy access, and kill the cache whenever a component is added or removed', () => {
            jest.spyOn(frame.components, 'forEach');
            // call componentsForType
            SpecHelper.expectEqual(
                [myComponent.id, mySubComponent.id],
                frame.componentsForType(MyComponent).map(c => c.id),
                'components for MyComponent',
            );

            // call componentsForType again, and expect the same results
            SpecHelper.expectEqual(
                [myComponent.id, mySubComponent.id],
                frame.componentsForType(MyComponent).map(c => c.id),
                'components for MyComponent',
            );
            // the second time, we should not have needed to loop
            // through all the components
            SpecHelper.expectEqual(1, frame.components.forEach.mock.calls.length, 'frame.components.forEach.calls');

            // add another component
            const newComponent = frame.addComponent(MyComponent.new());
            frame.components.forEach.mockClear();

            // call componentsForType again, and expect the new component to
            // be included
            SpecHelper.expectEqual(
                [myComponent.id, mySubComponent.id, newComponent.id],
                frame.componentsForType(MyComponent).map(c => c.id),
                'components for MyComponent',
            );

            // after adding the new component, we needed to loop through again
            SpecHelper.expectEqual(1, frame.components.forEach.mock.calls.length, 'frame.components.forEach.calls');

            // remove a component
            frame.removeComponent(newComponent);
            frame.components.forEach.mockClear();

            // call componentsForType again, and expect the new component to
            // be removed
            SpecHelper.expectEqual(
                [myComponent.id, mySubComponent.id],
                frame.componentsForType(MyComponent).map(c => c.id),
                'components for MyComponent',
            );

            // after removing the component, we needed to loop through again
            SpecHelper.expectEqual(1, frame.components.forEach.mock.calls.length, 'frame.components.forEach.calls');
        });
    });

    describe('removeComponent', () => {
        it('should remove a component and all references to it', () => {
            const component1 = frame.addComponent(ComponentModel.new());
            const component2 = frame.addComponent(ComponentModel.new());

            jest.spyOn(frame.editorViewModelFor(component1), 'removeReferencesTo').mockImplementation(() => {});
            frame.removeComponent(component2);

            // component2 should have been removed from the components list
            // frame.components[0] is now a FrameNavigator
            SpecHelper.expectEqual(component1, frame.components[1]);
            // and the componentMap
            SpecHelper.expectEqual(false, component2.id in frame.$$_componentMap);
            // and removeReferencesTo should have been called on the remaining frame
            expect(frame.editorViewModelFor(component1).removeReferencesTo).toHaveBeenCalledWith(component2);
        });

        // see https://trello.com/c/7ZoFhlGK/116-bug-error-when-removing-answer-from-answer-list-in-editor
        it('should work even if the removal of this component causes the removal of another', () => {
            const component1 = frame.addComponent(ComponentModel.new());
            const component2 = frame.addComponent(ComponentModel.new());
            const component3 = frame.addComponent(ComponentModel.new());

            // mock out a situation where some event listener means that, since component1 was removed,
            // component2 ends up getting removed as well
            let removed;
            jest.spyOn(frame.editorViewModelFor(component2), 'removeReferencesTo').mockImplementation(() => {
                if (!removed) {
                    removed = true;
                    component2.remove();
                }
            });

            jest.spyOn(frame.editorViewModelFor(component3), 'removeReferencesTo').mockImplementation(() => {});

            component1.remove();

            // component3 still should have had removeReferencesTo called
            expect(frame.editorViewModelFor(component3).removeReferencesTo).toHaveBeenCalledWith(component1);
        });

        it('should unset the mainUiComponent', () => {
            const component = frame.addComponent(ComponentModel.new());
            frame.mainUiComponent = component;
            frame.removeComponent(component);
            expect(frame.main_ui_component_id).toBeUndefined();
        });
    });

    describe('mainUiComponent', () => {
        it('should be settable to a component', () => {
            const component = frame.addComponent(ComponentModel.new());
            frame.mainUiComponent = component;
            expect(frame.main_ui_component_id).toEqual(component.id);
            expect(frame.mainUiComponent).toEqual(component);
        });
        it('should be settable to undefined', () => {
            const component = frame.addComponent(ComponentModel.new());
            frame.mainUiComponent = component;
            frame.mainUiComponent = undefined;
            expect(frame.main_ui_component_id).toBeUndefined();
        });
        it('should throw if set to something that is not a component', () => {
            expect(() => {
                frame.mainUiComponent = 'notAComponent';
            }).toThrow(new Error('Cannot set mainUiComponent to something that is not a ComponentModel.'));
        });
    });

    describe('text_content', () => {
        it('should be gettable', () => {
            const component = frame.addComponent(ComponentModel.new());
            frame.mainUiComponent = component;
            Object.defineProperty(frame.mainUiComponent, 'mainTextComponent', {
                value: frame.addText({
                    text: 'something',
                }),
            });
            expect(frame.text_content).toBe('something');
        });
        it('should be settable', () => {
            frame.embedInLesson();
            const component = frame.addComponent(ComponentModel.new());

            frame.mainUiComponent = component;
            Object.defineProperty(frame.mainUiComponent, 'mainTextComponent', {
                writable: true,
            });
            frame.mainUiComponent.editorViewModel.setMainTextComponent = function (val) {
                this.model.mainTextComponent = val;
            };
            frame.text_content = 'something';
            expect(frame.text_content).toBe('something');
            expect(frame.mainUiComponent.text_content).toBe('something');
        });
    });

    describe('main_image_id', () => {
        let component;
        let image;

        beforeEach(() => {
            component = frame.addComponent(ComponentModel.new());
            image = frame.addImage();
        });

        it('should be gettable', () => {
            frame.mainUiComponent = component;
            Object.defineProperty(frame.mainUiComponentEditorViewModel, 'mainImage', {
                value: image,
            });
            expect(frame.main_image_id).toBe(image.id);
        });
        it('should be settable', () => {
            frame.mainUiComponent = component;
            Object.defineProperty(frame.mainUiComponentEditorViewModel, 'mainImage', {
                writable: true,
            });
            frame.main_image_id = image.id;
            expect(frame.mainUiComponentEditorViewModel.mainImage).toBe(image);
        });
    });

    describe('mainImageSrc', () => {
        it('should return the src of the image image', () => {
            const image = frame.addImage();
            Object.defineProperty(frame, 'main_image_id', {
                value: image.id,
            });
            expect(frame.mainImageSrc).toEqual(image.urlForFormat('original'));
        });
        it('should return undefined if main image undefined', () => {
            Object.defineProperty(frame, 'main_image_id', {
                value: undefined,
            });
            expect(frame.mainImageSrc).toBeUndefined();
        });
    });

    describe('mainModalTexts', () => {
        it('should delegate to mainUiComponentEditorViewModel', () => {
            const component = frame.addComponent(ComponentModel.new());
            frame.mainUiComponent = component;
            Object.defineProperty(frame.mainUiComponentEditorViewModel, 'mainModalTexts', {
                writable: true,
            });
            frame.mainModalTexts = 'something';
            expect(frame.mainModalTexts).toBe('something');
            expect(frame.mainUiComponentEditorViewModel.mainModalTexts).toBe('something');
        });
    });

    describe('removeUnreferencedComponents', () => {
        let Subclass;
        let mainUiComponent;
        let continueButton;
        let referencedComponent;
        let referencedComponentCount;

        beforeEach(() => {
            Subclass = ComponentModel.subclass(function () {
                this.references('something').through('something_id');
            });
            mainUiComponent = Subclass.new({
                id: 'mainUiComponent',
            });
            continueButton = ContinueButtonModel.new({
                id: 'continueButton',
            });
            referencedComponent = frame.addComponent(
                Subclass.new({
                    id: 'referencedComponent',
                }),
            );
            referencedComponent.constructor.setEditorViewModel(
                'Lesson.FrameList.Frame.Componentized.Component.ComponentEditorViewModel',
            );

            mainUiComponent.something = referencedComponent;
            frame.addComponent(mainUiComponent);
            frame.addComponent(continueButton);
            frame.mainUiComponent = mainUiComponent;
            frame.continueButton = continueButton;

            referencedComponentCount = 3;
        });
        it('should remove unreferenced components before saving', () => {
            const component = frame.addVanillaComponent();
            frame.beforeSave();
            expect(frame.components.length).toEqual(referencedComponentCount + 1);
            expect(frame.components.indexOf(component)).toEqual(-1);
        });
        it('should remove unreferenced components that have circular references', () => {
            const component1 = frame.addComponent(Subclass.new());
            const component2 = frame.addComponent(Subclass.new());
            component1.something = component2;
            component2.something = component1;
            frame.beforeSave();
            expect(frame.components.length).toEqual(referencedComponentCount + 1);
            expect(frame.components.indexOf(component1)).toEqual(-1);
            expect(frame.components.indexOf(component2)).toEqual(-1);
        });
        it('should not barf on referenced components that have circular references', () => {
            referencedComponent.something = mainUiComponent;
            frame.runCallbacks('save', () => {});
            expect(frame.components.length).toEqual(referencedComponentCount + 1);
        });
        it('should not remove a component that is allowed to be unreferenced', () => {
            Subclass.prototype.allowUnreferenced = true;
            const component = frame.addComponent(Subclass.new());
            frame.runCallbacks('save', () => {});
            expect(frame.components.indexOf(component)).not.toEqual(-1);
        });
    });

    describe('prevFrame', () => {
        let frames;

        beforeEach(() => {
            frame = Componentized.fixtures.getInstance();
            frame.lesson().addFrame(Componentized.fixtures.getInstance());
            frame.lesson().addFrame(Componentized.fixtures.getInstance());
            frames = frame.lesson().frames;
        });

        it('should return the first frame that mightNavigateTo', () => {
            jest.spyOn(frames[0].frameNavigator, 'mightNavigateTo').mockReturnValue(true);
            expect(frames[2].prevFrame()).toBe(frames[0]);
        });
        it('should not return this frame, even if it navigates to itself', () => {
            jest.spyOn(frames[1].frameNavigator, 'mightNavigateTo').mockReturnValue(true);
            jest.spyOn(frames[0].frameNavigator, 'mightNavigateTo').mockReturnValue(false);
            expect(() => {
                frames[1].prevFrame();
            }).toThrow(new Error(`No frames navigate to ${frames[1].label}`));
        });
        it('should throw if no frames navigate to this one', () => {
            jest.spyOn(frames[0].frameNavigator, 'mightNavigateTo').mockReturnValue(false);
            expect(() => {
                frames[1].prevFrame();
            }).toThrow(new Error(`No frames navigate to ${frames[1].label}`));
        });
        it('should return undefined if this is the first frame', () => {
            jest.spyOn(frames[1].frameNavigator, 'mightNavigateTo').mockImplementation(() => {});
            expect(frames[0].prevFrame()).toBeUndefined();
            expect(frames[1].frameNavigator.mightNavigateTo).not.toHaveBeenCalled();
        });
    });

    describe('preloadImages', () => {
        let frame;
        let images;
        let components;

        beforeEach(() => {
            const Component = ComponentModel.subclass(function () {
                this.alias('PreloadImageTester');
                this.references('something').through('something_id');
                this.references('somethings').through('somethings_id');
            });

            frame = Componentized.fixtures.getInstance();
            images = [frame.addImage(), frame.addImage(), frame.addImage()];
            images.forEach(image => {
                jest.spyOn(image, 'preload').mockImplementation(() => Promise.resolve());
            });
            components = [frame.addComponent(Component.new()), frame.addComponent(Component.new())];
            jest.spyOn(components[0], 'imageContext').mockReturnValue('context0');
            jest.spyOn(components[1], 'imageContext').mockReturnValue('context1');
        });

        it('should call preloadImages on all images', () => {
            components[0].something = images[0];
            components[1].somethings = [components[0], images[1], images[2]];
            frame.mainUiComponent = components[1];
            frame.preloadImages();

            expect(images[0].preload).toHaveBeenCalledWith('context0');
            expect(images[1].preload).toHaveBeenCalledWith('context1');
            expect(images[2].preload).toHaveBeenCalledWith('context1');
        });

        it('should handle circular references', () => {
            components[0].somethings = [components[1], images[0]];
            components[1].somethings = [components[0], images[1]];
            frame.mainUiComponent = components[1];
            frame.preloadImages();

            expect(images[0].preload).toHaveBeenCalledWith('context0');
            expect(images[1].preload).toHaveBeenCalledWith('context1');
        });

        it('should handle two references to the same image', () => {
            components[0].something = images[0];
            components[1].somethings = [components[0], images[0]];
            frame.mainUiComponent = components[1];
            frame.preloadImages();

            expect(images[0].preload).toHaveBeenCalledWith('context0');
            expect(images[0].preload).toHaveBeenCalledWith('context1');
        });
    });

    describe('getReferencedImages', () => {
        it('should only return referencedImages', () => {
            const frame = Componentized.fixtures.getInstance();
            const Component = ComponentModel.subclass(function () {
                this.references('image').through('image_id');
            });
            frame.mainUiComponent = frame.addComponent(Component.new());
            const referencedImage = (frame.mainUiComponent.image = frame.addImage());
            frame.addImage(); // add an unreferenced image
            SpecHelper.expectEqual([referencedImage], frame.getReferencedImages());
        });
    });

    describe('getKeyTerms', () => {
        let keyTermsFrame;

        beforeEach(() => {
            keyTermsFrame = Componentized.fixtures.getInstance();
        });

        it('should return bolded words', () => {
            Object.defineProperty(keyTermsFrame.mainUiComponent, 'mainTextComponent', {
                value: keyTermsFrame.addText({
                    text: 'here is a **bolded** word and **another**',
                }),
            });
            expect(keyTermsFrame.getKeyTerms()).toEqual(['bolded', 'another']);
        });

        it('should work for key terms in text models that are not the main text', () => {
            keyTermsFrame.addText({
                text: 'here is a **bolded** word and **another**',
            });
            expect(keyTermsFrame.getKeyTerms()).toEqual(['bolded', 'another']);
        });

        it('should remove brackets', () => {
            Object.defineProperty(keyTermsFrame.mainUiComponent, 'mainTextComponent', {
                value: keyTermsFrame.addText({
                    text: 'here is a **[[modal]]** word and a **[blank]**',
                }),
            });
            expect(keyTermsFrame.getKeyTerms()).toEqual(['modal', 'blank']);
        });

        it('should remove whitespace', () => {
            Object.defineProperty(keyTermsFrame.mainUiComponent, 'mainTextComponent', {
                value: keyTermsFrame.addText({
                    text: 'a ** whitespace term** and ** whitespace term2 ** and ** \nwhitespace term3 **',
                }),
            });
            expect(keyTermsFrame.getKeyTerms()).toEqual(['whitespace term', 'whitespace term2', 'whitespace term3']);
        });

        it('should ignore things in code blocks', () => {
            Object.defineProperty(keyTermsFrame.mainUiComponent, 'mainTextComponent', {
                value: keyTermsFrame.addText({
                    text: 'python: `3 ** 4 + 4 ** 6`',
                }),
            });
            expect(keyTermsFrame.getKeyTerms()).toEqual([]);
        });

        it('should ignore things that span code blocks', () => {
            Object.defineProperty(keyTermsFrame.mainUiComponent, 'mainTextComponent', {
                value: keyTermsFrame.addText({
                    text: 'python: `3 ** 4` is an expontent and so is `5**6`',
                }),
            });
            expect(keyTermsFrame.getKeyTerms()).toEqual([]);
        });
    });

    describe('FrameViewModel', () => {
        describe('viewModelsFor', () => {
            it('should throw if model is for different frame', () => {
                const frame = Componentized.fixtures.getInstance();
                const frameViewModel = frame.createFrameViewModel();
                const components = [frame.addComponent(ComponentModel.new()), frame.addComponent(ComponentModel.new())];
                let a; // eslint-disable-line no-unused-vars
                components[0].remove();
                expect(() => {
                    a = frameViewModel.viewModelsFor([components[0]]);
                }).toThrowError(
                    `Cannot create viewModel for component that is unrelated to this frame. model.frame() = undefined, model.id = ${components[0].id}`,
                );
            });
        });
    });
});
