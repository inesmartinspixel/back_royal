import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/componentized_fixtures';
import 'Editor/angularModule';

describe('Componentized.Component.ComponentOverlay', () => {
    let frame;
    let frameViewModel;
    let viewModel;
    let model;
    let Componentized;
    let SpecHelper;
    let $timeout;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                Componentized = $injector.get('Lesson.FrameList.Frame.Componentized');
                SpecHelper = $injector.get('SpecHelper');
                $injector.get('ComponentizedFixtures');
                viewModel = getViewModel();
                $timeout = $injector.get('$timeout');
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('Model', () => {
        describe('optionsFor', () => {
            it('should return the options for a component', () => {
                const overlayComponent = frame.addVanillaComponent();
                model.componentOverlays = [overlayComponent];
                model.overlayOptions = {};
                model.overlayOptions[overlayComponent.id] = 'options';
                expect(model.optionsFor(overlayComponent)).toEqual('options');
            });
            it('should throw if the component is not one of the overlay components', () => {
                const unexpectedComponent = frame.addVanillaComponent();
                model.overlayOptions = {};
                expect(() => {
                    model.optionsFor(unexpectedComponent);
                }).toThrow(new Error(`No overlayOptions found for component "${unexpectedComponent.id}"`));
            });
        });
    });

    describe('ViewModel', () => {
        describe('optionsFor', () => {
            it('should return the options for a viewModel', () => {
                const overlayComponent = frame.addVanillaComponent();
                model.overlayComponents = [overlayComponent];
                model.overlayOptions = {};
                model.overlayOptions[overlayComponent.id] = 'options';
                const overlayViewModel = viewModel.overlayComponentsViewModels[0];
                expect(viewModel.optionsFor(overlayViewModel)).toEqual('options');
            });
        });
    });

    describe('cf-component-overlay', () => {
        let elem;
        let scope;

        beforeEach(() => {
            model.image = frame.addVanillaComponent();
            model.overlayComponents.push(frame.addVanillaComponent());
            model.overlayComponents.push(frame.addVanillaComponent());
            model.overlayComponents.forEach(component => {
                model.optionsFor(component).x = Math.random();
                model.optionsFor(component).y = Math.random();
                model.optionsFor(component).units = '%';
            });

            // add a z-index to just one of the components, to check that
            // it works both with and without
            viewModel.overlayComponentsViewModels[0].overlayZIndex = 42;
            render();
        });

        it('should display the main component', () => {
            SpecHelper.expectElement(elem, `[component-id="${model.image.id}"]`);
        });

        it('should rescale whenever the ImageModel image changes', () => {
            jest.spyOn(scope, 'rescale').mockImplementation(() => {});
            model.image = frame.addVanillaComponent();
            model.image.image = {
                foo: true,
            };
            scope.$digest();
            expect(scope.rescale).toHaveBeenCalled();
        });

        it('should fire click_outside_any_overlay', () => {
            const callback = jest.fn();
            scope.$on('component_overlay:click_outside_any_overlay', callback);
            SpecHelper.click(elem, '.content_wrapper');
            expect(callback).toHaveBeenCalled();
            callback.mockClear();
            SpecHelper.click(elem, '[positionable]', 0);
            expect(callback).not.toHaveBeenCalled();
        });

        it('should display the overlay components', () => {
            model.overlayComponents.forEach(component => {
                const el = SpecHelper.expectElement(elem, `[component-id="${component.id}"]`);
                const positionableEl = el.closest('[positionable]');
                const componentViewModel = viewModel.viewModelFor(component);
                SpecHelper.expectEqual(true, !!positionableEl[0], 'positionable parent element');
                SpecHelper.expectEqual(model.optionsFor(component).x, positionableEl.isolateScope().posX, 'posX');
                SpecHelper.expectEqual(model.optionsFor(component).y, positionableEl.isolateScope().posY, 'posY');
                SpecHelper.expectEqual(componentViewModel.overlayZIndex, positionableEl.isolateScope().posZ, 'posZ');
                SpecHelper.expectEqual(
                    model.optionsFor(component).units,
                    positionableEl.isolateScope().posUnits,
                    'posUnits',
                );
                SpecHelper.expectEqual(false, positionableEl.isolateScope().posMovable, 'posMovable');
            });
        });

        it('should make the overlay components movable in editor mode', () => {
            frameViewModel.playerViewModel = {
                editorMode: true,
            };
            scope.$digest();
            model.overlayComponents.forEach(component => {
                const el = SpecHelper.expectElement(elem, `[component-id="${component.id}"]`);
                const positionableEl = el.closest('[positionable]');
                SpecHelper.expectEqual(true, !!positionableEl[0], 'positionable parent element');
                SpecHelper.expectEqual(true, positionableEl.isolateScope().posMovable, 'posMovable');
            });
        });

        it('should add the in-right-half-of-overlay class', () => {
            model.optionsFor(model.overlayComponents[0]).units = '%';
            model.optionsFor(model.overlayComponents[0]).x = 42;
            model.optionsFor(model.overlayComponents[0]).width = 7;

            model.optionsFor(model.overlayComponents[1]).units = '%';
            model.optionsFor(model.overlayComponents[1]).x = 42;
            model.optionsFor(model.overlayComponents[1]).width = 20;
            scope.$digest();

            SpecHelper.expectElementDoesNotHaveClass(
                elem.find('.overlay_component_wrapper').eq(0),
                'in-right-half-of-overlay',
            );
            SpecHelper.expectElementHasClass(elem.find('.overlay_component_wrapper').eq(1), 'in-right-half-of-overlay');
        });

        describe('positioning', () => {
            describe('in editorMode', () => {
                it('should allow for positioning overlay components', () => {
                    frameViewModel.playerViewModel = {
                        editorMode: true,
                    };
                    model.optionsFor(model.overlayComponents[0]).units = 'px';
                    model.optionsFor(model.overlayComponents[0]).x = 10;
                    model.optionsFor(model.overlayComponents[0]).y = 10;
                    scope.$digest();

                    elem.find('[positionable]')
                        .eq(0)
                        .isolateScope()
                        .onPositionableDragStop({
                            dragAndDrop: {
                                movedX: 10,
                                movedY: 10,
                            },
                        });
                    scope.$digest();
                    expect(model.optionsFor(model.overlayComponents[0]).x).toBe(20);
                    expect(model.optionsFor(model.overlayComponents[0]).y).toBe(20);
                });
            });
            describe('not in editorMode', () => {
                it('should not allow for positioning overlay components', () => {
                    expect(elem.find('[positionable]').isolateScope().posMovable).toBe(false);
                });
            });
        });

        describe('positioning', () => {
            describe('in editorMode', () => {
                it('should allow for resizing overlay components', () => {
                    frameViewModel.playerViewModel = {
                        editorMode: true,
                    };
                    const component = frame.addText();
                    model.overlayComponents = [];
                    model.overlayComponents.push(component);
                    model.overlayOptions[component.id] = {
                        units: 'px',
                        width: 10,
                        height: 10,
                    };
                    scope.$digest();
                    $timeout.flush(); // give the resize-handle a chance to get setup

                    elem.find('resize-handle').eq(0).isolateScope().onResizeStart();
                    elem.find('resize-handle')
                        .eq(0)
                        .isolateScope()
                        .onResized({
                            dragAndDrop: {
                                movedX: 10,
                                movedY: 10,
                            },
                        });
                    scope.$digest();
                    expect(model.optionsFor(model.overlayComponents[0]).width).toBe(20);
                    expect(model.optionsFor(model.overlayComponents[0]).height).toBe(20);
                });
            });
            describe('not in editorMode', () => {
                it('should hide resize handle', () => {
                    expect(elem.find('[positionable]').isolateScope().posResizable).toBe(false);
                });
            });
        });

        function render() {
            const renderer = SpecHelper.renderer();
            renderer.scope.viewModel = viewModel;
            renderer.render('<cf-ui-component view-model="viewModel"></cf-ui-component>');
            elem = renderer.elem;
            scope = elem.find('cf-component-overlay').isolateScope();
        }
    });

    describe('EditorViewModel', () => {
        it('should setup overlayOptions when an overlay is added', () => {
            const component = frame.addVanillaComponent();
            model.overlayComponents.push(component);
            expect(model.optionsFor(component)).toEqual({
                x: 0,
                y: 0,
                units: '%',
            });

            // the component needs a reference to the overlayOptions so it can include
            // the editor widget in the right place in the info panel
            expect(component.editorViewModel.overlayOptions).toBe(model.optionsFor(component));
        });

        it('should remove overlayOptions when an overlay is removed', () => {
            const component = frame.addVanillaComponent();
            model.overlayComponents.push(component);
            component.remove();
            expect(model.overlayOptions).toEqual({});
        });
    });

    function getViewModel(options) {
        model = getModel(options);
        return frameViewModel.viewModelFor(model);
    }

    function getModel() {
        frame = Componentized.fixtures.getInstance();
        frameViewModel = frame.createFrameViewModel();
        return frame.addComponentOverlay();
    }
});
