import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/componentized_fixtures';
import 'Editor/angularModule';

describe('Componentized.Component.Challenge', () => {
    let frame;
    let viewModel;
    let model;
    let ChallengeModel;
    let Componentized;
    let ComponentEventListener;
    let SpecHelper;
    let Challenge;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                ChallengeModel = $injector.get(
                    'Lesson.FrameList.Frame.Componentized.Component.Challenge.ChallengeModel',
                );
                Componentized = $injector.get('Lesson.FrameList.Frame.Componentized');
                ComponentEventListener = $injector.get(
                    'Lesson.FrameList.Frame.Componentized.Component.ComponentEventListener',
                );
                Challenge = $injector.get('Lesson.FrameList.Frame.Componentized.Component.Challenge.ChallengeModel');
                SpecHelper = $injector.get('SpecHelper');
                $injector.get('ComponentizedFixtures');
                viewModel = getChallengeViewModel();
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('ViewModel', () => {
        describe('active', () => {
            it('should fire expected events', () => {
                SpecHelper.expectEqual(false, viewModel.active, 'initially active');
                const activatedCallback = jest.fn();
                const deActivatedCallback = jest.fn();
                new ComponentEventListener(viewModel, 'activated', activatedCallback);
                new ComponentEventListener(viewModel, 'deActivated', deActivatedCallback);

                viewModel.active = true;
                SpecHelper.expectEqual(true, viewModel.active, 'active set to true');
                expect(activatedCallback).toHaveBeenCalled();
                expect(deActivatedCallback).not.toHaveBeenCalled();

                viewModel.active = false;
                SpecHelper.expectEqual(false, viewModel.active, 'active set to false');
                expect(deActivatedCallback).toHaveBeenCalled();
            });

            it('should activate and deactivate validator', () => {
                viewModel.active = true;
                SpecHelper.expectEqual(true, viewModel.validatorViewModel.active, 'validator active set to true');
                viewModel.active = false;
                SpecHelper.expectEqual(false, viewModel.validatorViewModel.active, 'validator active set to false');
            });

            it('should deactivate when removed', () => {
                viewModel.active = true;
                viewModel.model.remove();
                expect(viewModel.active).toBe(false);
            });
        });

        describe('showingIncorrectStyling', () => {
            it('should throw', () => {
                expect(() => {
                    /* eslint-disable */
                    viewModel.showingIncorrectStyling;
                    /* eslint-enable */
                }).toThrow(
                    new Error(
                        'Subclasses of ChallengeViewModel should define showingIncorrectStyling. "ChallengeViewModel" does not.',
                    ),
                );
            });
        });

        describe('validate', () => {
            it('should delegate to validator', () => {
                jest.spyOn(viewModel.validatorViewModel, 'validate').mockImplementation(() => {});
                viewModel.validate('info');
                expect(viewModel.validatorViewModel.validate).toHaveBeenCalledWith('info');
            });
        });

        describe('onAfterValidated', () => {
            it('should fire event on correct validation', () => {
                const callback = jest.fn();
                new ComponentEventListener(viewModel, 'validatedCorrect', callback);
                viewModel.validatorViewModel.fire('validated', {
                    result: true,
                });
                expect(callback).toHaveBeenCalled();
            });

            it('should fire event on incorrect validation', () => {
                const callback = jest.fn();
                new ComponentEventListener(viewModel, 'validatedIncorrect', callback);
                viewModel.validatorViewModel.fire('validated', {
                    result: false,
                });
                expect(callback).toHaveBeenCalled();
            });

            it('should raise on null validation', () => {
                expect(() => {
                    viewModel.validatorViewModel.fire('validated', {
                        result: null,
                    });
                }).toThrow(new Error('Do we need to support a result that is neither correct nor incorrect?'));
            });

            it('should call setChallengeScore', () => {
                viewModel.frameViewModel.playerViewModel = {
                    setChallengeScore: jest.fn(),
                };
                viewModel.validatorViewModel.fire('validated', {
                    result: true,
                });
                expect(viewModel.playerViewModel.setChallengeScore).toHaveBeenCalledWith(viewModel);
            });
        });

        describe('complete', () => {
            it('should set complete and fire', () => {
                const callback = jest.fn();
                new ComponentEventListener(viewModel, 'completed', callback);
                SpecHelper.expectEqual(false, viewModel.complete, 'initially not complete');
                viewModel.complete = true;
                SpecHelper.expectEqual(true, viewModel.complete, 'complete set to true');
                expect(callback).toHaveBeenCalled();
            });

            it('should not be settable back to false', () => {
                viewModel.complete = true;
                expect(() => {
                    viewModel.complete = false;
                }).toThrow(new Error('"complete" cannot be set back to false once it is true.'));
            });
        });

        describe('behaviors', () => {
            describe('CompleteOnCorrect', () => {
                it('should set complete to correct after validation and a delay', () => {
                    viewModel = getChallengeViewModel({
                        behaviors: {
                            CompleteOnCorrect: {},
                        },
                    });
                    SpecHelper.expectEqual(false, viewModel.complete, 'complete false before delay');
                    viewModel.fire('validatedCorrect');
                    SpecHelper.expectEqual(true, viewModel.complete, 'complete true after delay');
                });
            });

            describe('ClearMessagesOnDeactivated', () => {
                it('should clear messages when challenge is deactivated', () => {
                    Object.defineProperty(ChallengeModel.ViewModel.prototype, 'playerViewModel', {
                        value: {
                            clearMessage: jest.fn(),
                        },
                    });
                    viewModel = getChallengeViewModel({
                        behaviors: {
                            ClearMessagesOnDeactivated: {},
                        },
                    });
                    viewModel.playerViewModel.clearMessage.mockClear();
                    viewModel.active = true;
                    expect(viewModel.playerViewModel.clearMessage).not.toHaveBeenCalled();
                    viewModel.active = false;
                    expect(viewModel.playerViewModel.clearMessage).toHaveBeenCalled();
                });
            });
        });
    });

    describe('EditorViewModel', () => {
        let helper;
        let model;
        beforeEach(() => {
            helper = Challenge.EditorViewModel.addComponentTo(frame).setup();
            model = helper.model;
        });
        describe('setup', () => {
            describe('setup', () => {
                beforeEach(() => {
                    jest.spyOn(Challenge.EditorViewModel.prototype, 'addValidator').mockImplementation(() => {});
                    Challenge.EditorViewModel.addComponentTo(frame).setup();
                });
                it('should call addValidator', () => {
                    expect(Challenge.EditorViewModel.prototype.addValidator).toHaveBeenCalled();
                });
            });
        });
        describe('addValidator', () => {
            it('should add a validator', () => {
                const validator = model.validator;
                expect(validator).not.toBeUndefined();
                expect(validator.challenge).toBe(model);
            });
        });
        describe('correctAnswerText', () => {
            it('should be undefined', () => {
                expect(helper.correctAnswerText).toBeUndefined();
            });

            it('should throw when set', () => {
                expect(() => {
                    helper.correctAnswerText = 'something';
                }).toThrow(
                    new Error(
                        'Subclasses of ChallengeEditorViewModel can define correctAnswerText=. ChallengeEditorViewModel does not.',
                    ),
                );
            });
        });
        describe('correctAnswerImage', () => {
            it('should be undefined', () => {
                expect(helper.correctAnswerImage).toBeUndefined();
            });

            it('should throw when set', () => {
                expect(() => {
                    helper.correctAnswerImage = 'something';
                }).toThrow(
                    new Error(
                        'Subclasses of ChallengeEditorViewModel can define correctAnswerImage=. ChallengeEditorViewModel does not.',
                    ),
                );
            });
        });

        describe('noIncorrectAnswers', () => {
            it('should be undefined', () => {
                expect(helper.noIncorrectAnswers).toBeUndefined();
            });

            it('should throw when set', () => {
                expect(() => {
                    helper.noIncorrectAnswers = 'something';
                }).toThrow(
                    new Error(
                        'Subclasses of ChallengeEditorViewModel can define noIncorrectAnswers=. ChallengeEditorViewModel does not.',
                    ),
                );
            });
        });
    });

    function getChallengeViewModel(options) {
        model = getChallengeModel(options);
        const frameViewModel = frame.createFrameViewModel();
        return frameViewModel.viewModelFor(model);
    }

    function getChallengeModel(options) {
        let model;
        frame = Componentized.fixtures.getInstance();
        frame.addChallenge(options).addDefaultReferences();
        model = frame.getModelsByType('ChallengeModel')[0];

        expect(model).not.toBeUndefined();
        return model;
    }
});
