import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/componentized_fixtures';
import 'Editor/angularModule';

describe('Componentized.Component.InteractiveCards', () => {
    let frame;
    let frameViewModel;
    let viewModel;
    let Componentized;
    let SpecHelper;
    let $timeout;
    let Capabilities;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                Componentized = $injector.get('Lesson.FrameList.Frame.Componentized');
                SpecHelper = $injector.get('SpecHelper');
                $timeout = $injector.get('$timeout');
                $injector.get('ComponentizedFixtures');
                Capabilities = $injector.get('Capabilities');
                Object.defineProperty(Capabilities, 'cssTransitions', {
                    value: true,
                    configurable: true,
                });
                viewModel = getViewModel();
                SpecHelper.stubEventLogging();
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('Model', () => {});

    describe('ViewModel', () => {
        it('should activate card when an associated challenge is activated', () => {
            expect(viewModel.currentIndex).toBe(0);
            viewModel.overlaysViewModels[1].overlayComponentsViewModels[0].challengeViewModel.active = true;
            expect(viewModel.currentIndex).toBe(1);
            viewModel.overlaysViewModels[0].overlayComponentsViewModels[0].challengeViewModel.active = true;
            expect(viewModel.currentIndex).toBe(0);
        });
    });

    describe('cf-interactive-cards', () => {
        let elem;
        let scope;
        let model;

        it('should render overlay components', () => {
            render();
            SpecHelper.expectElement(elem, `.card [component-id="${model.overlays[0].id}"]`);
            SpecHelper.expectElement(elem, `.card [component-id="${model.overlays[1].id}"]`);
        });

        it('should activate the first incomplete challenge when switching cards', () => {
            render();
            const challengeViewModelsOnCard0 = viewModel.overlaysViewModels[0].overlayComponentsViewModels.map(
                blankViewModel => blankViewModel.challengeViewModel,
            );

            const challengeViewModelsOnCard1 = viewModel.overlaysViewModels[1].overlayComponentsViewModels.map(
                blankViewModel => blankViewModel.challengeViewModel,
            );

            challengeViewModelsOnCard1[0].complete = true;
            challengeViewModelsOnCard0[0].active = true;

            expect(viewModel.currentIndex).toBe(0);
            viewModel.currentIndex = 1;
            scope.$digest();
            elem.find('.carousel').trigger('transitionend');

            expect(challengeViewModelsOnCard1[1].active).toBe(true);
        });

        it('should activate a complete challenge when switching cards if all are complete', () => {
            render();
            const challengeViewModelsOnCard0 = viewModel.overlaysViewModels[0].overlayComponentsViewModels.map(
                blankViewModel => blankViewModel.challengeViewModel,
            );

            const challengeViewModelsOnCard1 = viewModel.overlaysViewModels[1].overlayComponentsViewModels.map(
                blankViewModel => blankViewModel.challengeViewModel,
            );

            challengeViewModelsOnCard1[0].complete = true;
            challengeViewModelsOnCard1[1].complete = true;
            challengeViewModelsOnCard0[0].active = true;

            expect(viewModel.currentIndex).toBe(0);
            viewModel.currentIndex = 1;
            scope.$digest();
            elem.find('.carousel').trigger('transitionend');

            expect(challengeViewModelsOnCard1[0].active).toBe(true);
        });

        describe('on challenge complete', () => {
            it('should go on to next challenge if it is on the current card', () => {
                render();
                expect(viewModel.currentIndex).toBe(0);

                const currentChallenge = model.overlays[0].overlayComponents[0].challenge;
                const nextChallenge = model.overlays[0].overlayComponents[1].challenge;

                Object.defineProperty(viewModel.challengesComponentViewModel, 'nextChallengeViewModel', {
                    value: viewModel.viewModelFor(nextChallenge),
                });

                viewModel.viewModelFor(currentChallenge).fire('completed');
                $timeout.flush();
                expect(viewModel.viewModelFor(nextChallenge).active).toBe(true);
                expect(viewModel.currentIndex).toBe(0);
            });

            it('should go to the next card and then activate the next challenge if challenge is on another card', () => {
                render();
                expect(viewModel.currentIndex).toBe(0);

                const currentChallenge = model.overlays[0].overlayComponents[1].challenge;
                viewModel.viewModelFor(currentChallenge).active = true;
                const nextChallenge = model.overlays[1].overlayComponents[0].challenge;

                Object.defineProperty(viewModel.challengesComponentViewModel, 'nextChallengeViewModel', {
                    value: viewModel.viewModelFor(nextChallenge),
                });

                viewModel.viewModelFor(currentChallenge).fire('completed');
                $timeout.flush();

                expect(viewModel.currentIndex).toBe(1);
                expect(viewModel.viewModelFor(nextChallenge).active).toBe(false);

                // once the new slide has slid into place, the next challenge
                // should be activated
                elem.find('.carousel').trigger('transitionend');
                expect(viewModel.viewModelFor(nextChallenge).active).toBe(true);
                expect(viewModel.currentIndex).toBe(1);
            });
        });

        function render() {
            const renderer = SpecHelper.renderer();
            renderer.scope.viewModel = viewModel;
            model = viewModel.model;
            renderer.render('<cf-ui-component view-model="viewModel"></cf-ui-component>');
            elem = renderer.elem;
            scope = elem.find('.cf-interactive-cards').scope();
        }
    });

    describe('EditorViewModel', () => {
        let editorViewModel;
        let model;

        beforeEach(() => {
            editorViewModel = frame.editorViewModelFor(viewModel.model);
            model = editorViewModel.model;
        });

        describe('initialize', () => {
            it('should listen for overlays to be re-ordered and reorder the challenges', () => {
                const challengesComponent = editorViewModel.model.challengesComponent;
                const initialChallenges = challengesComponent.challenges.clone();
                expect(initialChallenges.length).toBe(4);

                // duplicate what reorderListItemButtonsDirective does
                const firstOverlay = editorViewModel.model.overlays[0];
                editorViewModel.model.overlays.splice(0, 1);
                editorViewModel.model.overlays.splice(1, 1, firstOverlay);

                expect(challengesComponent.challenge_ids).toEqual([
                    initialChallenges[2].id,
                    initialChallenges[3].id,
                    initialChallenges[0].id,
                    initialChallenges[1].id,
                ]);
            });
        });

        describe('addInteractiveCard', () => {
            it('should add a ComponentOverlay to overlays array', () => {
                const initialLength = editorViewModel.model.overlays.length;
                editorViewModel.addInteractiveCard();
                expect(editorViewModel.model.overlays.length).toBe(initialLength + 1);
                const newOverlay = _.last(editorViewModel.model.overlays);
                expect(newOverlay.type).toBe('ComponentOverlayModel');
            });
        });

        describe('removeInteractiveCard', () => {
            it('should remove overlay and challenges', () => {
                const overlayToRemove = editorViewModel.model.overlays[0];
                const initialChallengesLength = editorViewModel.model.challengesComponent.challenges.length;
                editorViewModel.removeInteractiveCard(overlayToRemove.editorViewModel);
                expect(editorViewModel.model.overlays.indexOf(overlayToRemove)).toBe(-1);
                expect(editorViewModel.model.challengesComponent.challenges.length < initialChallengesLength).toBe(
                    true,
                );
            });
        });

        describe('firstImage', () => {
            describe('get', () => {
                it('should return main component from first overlay', () => {
                    expect(editorViewModel.firstImage).toBe(model.overlays[0].image);
                });
                it('should return undefined if no overlays', () => {
                    model.overlays = [];
                    expect(editorViewModel.firstImage).toBeUndefined();
                });
            });

            describe('set', () => {
                it('should create first overlay if it does not exist', () => {
                    model.overlays = [];
                    const image = (editorViewModel.firstImage = frame.addImage());
                    expect(model.overlays.length).toBe(1);
                    expect(model.overlays[0].image).toBe(image);
                });
                it('should set image on existing overlay', () => {
                    const firstOverlay = model.overlays[0];
                    expect(firstOverlay).not.toBeUndefined();
                    const image = (editorViewModel.firstImage = frame.addImage());
                    expect(firstOverlay.image).toBe(image);
                });
            });
        });
    });

    describe('cf-interactive-cards-editor', () => {
        let elem;
        let editorViewModel;
        let model;

        beforeEach(() => {
            editorViewModel = frame.editorViewModelFor(viewModel.model);
            render();
        });

        it('should support removing an overlay', () => {
            const firstOverlay = model.overlays[0];
            SpecHelper.click(elem, '[name="remove_interactive_card"]', 0);
            expect(model.overlays.indexOf(firstOverlay)).toBe(-1);
        });

        it('should support adding an overlay', () => {
            const overlayCount = model.overlays.length;
            SpecHelper.click(elem, '[name="add_interactive_card"]', 0);
            expect(model.overlays.length).toBe(overlayCount + 1);
        });

        it('should support re-ordering overlays', () => {
            const initialOverlays = model.overlays.clone();
            SpecHelper.click(elem, '[name="moveDown"]', 0);
            expect(model.overlays).toEqual([initialOverlays[1], initialOverlays[0]]);
        });

        function render(options) {
            const renderer = SpecHelper.renderer();
            renderer.scope.editorViewModel = editorViewModel;
            renderer.scope.options = options || {};
            renderer.scope.frameViewModel = frameViewModel;
            renderer.render(
                '<cf-component-editor editor-view-model="editorViewModel" options="options" frame-view-model="frameViewModel"></cf-component-editor>',
            );
            elem = renderer.elem;

            model = editorViewModel.model;
        }
    });

    function getViewModel(options) {
        const model = getModel(options);
        return frameViewModel.viewModelFor(model);
    }

    function getModel() {
        frame = Componentized.fixtures.getInstance();
        frameViewModel = frame.createFrameViewModel();
        frameViewModel.playerViewModel = {
            activeFrameViewModel: frameViewModel,
            logInfo() {},
            log() {},
        };
        return frame.addInteractiveCards().addDefaultReferences();
    }
});
