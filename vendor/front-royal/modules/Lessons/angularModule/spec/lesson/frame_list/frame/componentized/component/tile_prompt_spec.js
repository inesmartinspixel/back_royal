import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/componentized_fixtures';
import 'Editor/angularModule';

describe('Componentized.Component.TilePrompt', () => {
    let frame;
    let frameViewModel;
    let viewModel;
    let Componentized;
    let SpecHelper;
    let TilePrompt;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                Componentized = $injector.get('Lesson.FrameList.Frame.Componentized');
                TilePrompt = $injector.get('Lesson.FrameList.Frame.Componentized.Component.TilePrompt.TilePromptModel');
                SpecHelper = $injector.get('SpecHelper');
                $injector.get('ComponentizedFixtures');
                viewModel = getViewModel();
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('Model', () => {});

    describe('ViewModel', () => {});

    describe('cf-tile-prompt', () => {
        let elem;
        let scope;

        it('should render the prompt', () => {
            render();
            const textDiv = SpecHelper.expectElement(elem, '.prompt .text');
            viewModel.model.text.editorViewModel.formatText();
            scope.$digest();
            SpecHelper.expectEqual(viewModel.model.text.formatted_text, textDiv.text());
            SpecHelper.expectElement(elem, '.prompt');
        });

        it('should support text', () => {
            render();
            SpecHelper.expectElement(elem, '.prompt.text');
        });

        it('should support an image', () => {
            viewModel.model.image = frame.addImage();
            render();
            SpecHelper.expectElement(elem, '.prompt.image');
        });

        function render() {
            const renderer = SpecHelper.renderer();
            renderer.scope.viewModel = viewModel;
            renderer.render('<cf-ui-component view-model="viewModel"></cf-ui-component>');
            elem = renderer.elem;
            scope = renderer.scope;
        }
    });

    describe('EditorViewModel', () => {
        describe('setup', () => {
            it('should add text', () => {
                const helper = TilePrompt.EditorViewModel.addComponentTo(frame).setup();
                SpecHelper.expectEqual('TextModel', helper.model.text.type);
            });
        });
    });

    describe('cf-tile-prompt-editor', () => {
        let elem;
        let scope;
        let model;

        beforeEach(() => {
            const renderer = SpecHelper.renderer();
            renderer.scope.editorViewModel = frame.editorViewModelFor(viewModel.model);
            renderer.render('<cf-component-editor editor-view-model="editorViewModel"></cf-component-editor>');
            elem = renderer.elem;
            scope = elem.find('[editor-view-model]').isolateScope();
            model = viewModel.model;
        });

        it('should hide remove button if option not set', () => {
            SpecHelper.expectNoElement(elem, '[name="remove"]');
        });

        it('should have remove button if option set', () => {
            scope.options = {
                allowRemove: true,
            };
            scope.$digest();
            jest.spyOn(model, 'remove').mockImplementation(() => {});
            SpecHelper.click(elem, '[name="remove"]');
            expect(model.remove).toHaveBeenCalled();
        });
    });

    function getViewModel(options) {
        const model = getModel(options);
        return frameViewModel.viewModelFor(model);
    }

    function getModel(options) {
        frame = Componentized.fixtures.getInstance();
        frameViewModel = frame.createFrameViewModel();
        return frame.addTilePrompt(options).addDefaultReferences();
    }
});
