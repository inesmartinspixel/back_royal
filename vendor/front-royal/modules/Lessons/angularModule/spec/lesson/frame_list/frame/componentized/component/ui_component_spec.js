import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/componentized_fixtures';
import 'Editor/angularModule';

describe('Componentized.Component.UiComponent', () => {
    let SpecHelper;
    let UiComponentViewModel;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                SpecHelper = $injector.get('SpecHelper');
                $injector.get('ComponentizedFixtures');
                UiComponentViewModel = $injector.get(
                    'Lesson.FrameList.Frame.Componentized.Component.UiComponent.UiComponentViewModel',
                );
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('Model', () => {});

    describe('ViewModel', () => {
        describe('directiveName', () => {
            it('should throw error', () => {
                const viewModel = new UiComponentViewModel('frameViewModel', 'model');
                expect(() => {
                    viewModel.directiveName;
                }).toThrow(
                    new Error(
                        'Subclasses of UiComponentViewModel should define directiveName. "undefinedViewModel" does not.',
                    ),
                );
            });
        });
    });
});
