import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Editor/angularModule';

describe('Componentized.Component.ChallengeValidator.ValidationError', () => {
    let ValidationError;
    let SpecHelper;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                ValidationError = $injector.get(
                    'Lesson.FrameList.Frame.Componentized.Component.ChallengeValidator.ValidationError',
                );
                SpecHelper = $injector.get('SpecHelper');
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('isRelatedToAnswer', () => {
        it('should return true iff one of the relatedAnswers is the provided answer', () => {
            const mockAnswers = ['mockAnswer0', 'mockAnswer1'];

            // error is related only to 'mockAnswer0'
            const validationError = new ValidationError({
                relatedAnswers: mockAnswers[0],
            });

            // the validation result should be related to 'mockAnswer0' but not 'mockAnswer1'
            SpecHelper.expectEqual(
                true,
                validationError.isRelatedToAnswer(mockAnswers[0]),
                'is related to mockAnswers[0]',
            );
            SpecHelper.expectEqual(
                false,
                validationError.isRelatedToAnswer(mockAnswers[1]),
                'is not related to mockAnswers[1]',
            );
        });

        it('should return false if there are no relatedAnswers', () => {
            const validationError = new ValidationError();
            SpecHelper.expectEqual(false, validationError.isRelatedToAnswer('mockAnswer'), 'is related to mockAnswer');
        });
    });
});
