import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/componentized_fixtures';
import 'Editor/angularModule';

describe('Componentized.Component.Mixins.IsMessageEditorViewModel', () => {
    let frame;
    let viewModel;
    let MyModel;
    let Componentized;
    let SpecHelper;
    let MaxTextLengthConfig;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'FrontRoyal.Editor', 'SpecHelper');

        angular.mock.module($provide => {
            $provide.factory('MyEditorViewModel', [
                'Lesson.FrameList.Frame.Componentized.Component.ComponentEditorViewModel',
                'Lesson.FrameList.Frame.Componentized.Component.Mixins.IsMessageEditorViewModel',

                (ComponentEditorViewModel, IsMessageEditorViewModel) =>
                    ComponentEditorViewModel.subclass(function () {
                        this.include(IsMessageEditorViewModel);

                        return {
                            initialize($super, model) {
                                $super(model);
                                this.onInitializeMessageEditorViewModel();
                            },
                        };
                    }),
            ]);
        });

        angular.mock.inject([
            '$injector',
            $injector => {
                Componentized = $injector.get('Lesson.FrameList.Frame.Componentized');
                SpecHelper = $injector.get('SpecHelper');
                $injector.get('ComponentizedFixtures');
                MaxTextLengthConfig = $injector.get('MaxTextLengthConfig');
                const UiComponentModel = $injector.get(
                    'Lesson.FrameList.Frame.Componentized.Component.UiComponent.UiComponentModel',
                );

                MyModel = UiComponentModel.subclass(function () {
                    this.alias('ComponentizedFrame.MyModel');
                    this.extend({
                        ViewModel: UiComponentModel.ViewModel.subclass(),
                    });
                    this.setEditorViewModel('MyEditorViewModel');
                    this.references('challenge').through('challenge_id');
                    this.references('messageText').through('message_text_id');

                    return {};
                });

                Object.defineProperty(MyModel.EditorViewModel, 'Model', {
                    get() {
                        return MyModel;
                    },
                });

                frame = Componentized.fixtures.getInstance();
                viewModel = getViewModel();
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('setup', () => {
        it('should add a text component', () => {
            const model = MyModel.EditorViewModel.addComponentTo(frame).setup().model;
            expect(model.messageText).not.toBeUndefined();
            expect(model.messageText.type).toBe('TextModel');
        });
    });

    describe('initialize', () => {
        it('should set the maxRecommendedTextLength to  MaxTextLengthConfig.MESSAGE', () => {
            const editorViewModel = MyModel.EditorViewModel.addComponentTo(frame).setup();
            expect(editorViewModel.model.messageText.editorViewModel.config.maxRecommendedTextLength).toBe(
                MaxTextLengthConfig.MESSAGE,
            );

            // just be sure it gets set on a new one too
            editorViewModel.model.messageText = frame.addText();
            expect(editorViewModel.model.messageText.editorViewModel.config.maxRecommendedTextLength).toBe(
                MaxTextLengthConfig.MESSAGE,
            );
        });

        it('should listen for the challenge to be removed, and remove itself', () => {
            const model = getModel();
            model.editorViewModel;
            jest.spyOn(model, 'remove').mockImplementation(() => {});
            model.challenge.remove();
            expect(model.remove).toHaveBeenCalled();
        });
    });

    function getViewModel(options) {
        viewModel = getModel(options).createViewModel(frame.createFrameViewModel());
        return viewModel;
    }

    function getModel() {
        const model = MyModel.EditorViewModel.addComponentTo(frame).setup().model;
        model.text = frame.addText();
        model.challenge = frame.addChallenge().addDefaultReferences();
        return model;
    }
});
