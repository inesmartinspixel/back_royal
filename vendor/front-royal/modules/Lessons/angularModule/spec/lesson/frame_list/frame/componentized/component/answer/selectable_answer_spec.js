import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/componentized_fixtures';
import 'Editor/angularModule';

describe('Componentized.Component.SelectableAnswer', () => {
    let frame;
    let answerViewModel;
    let SelectableAnswerModel;
    let Componentized;
    let SpecHelper;
    let ComponentEventListener;
    let MaxTextLengthConfig;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                SelectableAnswerModel = $injector.get(
                    'Lesson.FrameList.Frame.Componentized.Component.Answer.SelectableAnswer.SelectableAnswerModel',
                );
                Componentized = $injector.get('Lesson.FrameList.Frame.Componentized');
                SpecHelper = $injector.get('SpecHelper');
                ComponentEventListener = $injector.get(
                    'Lesson.FrameList.Frame.Componentized.Component.ComponentEventListener',
                );
                $injector.get('ComponentizedFixtures');
                answerViewModel = getSelectableAnswerViewModel();
                MaxTextLengthConfig = $injector.get('MaxTextLengthConfig');
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('Model', () => {
        describe('imageContext', () => {
            let answerListModel;
            let answerModel;
            let useSingleColumn;
            let editorTemplate;

            beforeEach(() => {
                editorTemplate = 'notMatching';
                frame = Componentized.fixtures.getInstance();
                jest.spyOn(frame, 'editor_template', 'get').mockImplementation(() => editorTemplate);
                answerListModel = frame.addAnswerList().addDefaultReferences();
                jest.spyOn(answerListModel, 'useSingleColumn').mockImplementation(() => useSingleColumn);
                answerModel = answerListModel.answers[0];
            });

            it('should be answerButton on matching frames', () => {
                editorTemplate = 'matching';
                expect(answerModel.imageContext('image')).toEqual('answerButton');
                expect(answerListModel.useSingleColumn).not.toHaveBeenCalled();
            });

            it('should be wideAnswerButton if useSingleColumn', () => {
                useSingleColumn = true;
                expect(answerModel.imageContext('image')).toEqual('wideAnswerButton');
            });

            it('should be answerButton if !useSingleColumn', () => {
                useSingleColumn = false;
                expect(answerModel.imageContext('image')).toEqual('answerButton');
            });
        });
    });

    describe('ViewModel', () => {
        describe('selected', () => {
            it('should be settable and fire appropriate events', () => {
                const callbacks = {};
                ['beforeSelected', 'selected', 'beforeUnselected', 'unselected'].forEach(evt => {
                    const callback = (callbacks[evt] = jest.fn());
                    new ComponentEventListener(answerViewModel, evt, callback);
                });
                SpecHelper.expectEqual(false, answerViewModel.selected, 'initially unselected');

                answerViewModel.selected = true;
                SpecHelper.expectEqual(true, answerViewModel.selected, 'selected set to true');
                expect(callbacks.beforeSelected).toHaveBeenCalled();
                expect(callbacks.selected).toHaveBeenCalled();

                answerViewModel.selected = false;
                SpecHelper.expectEqual(false, answerViewModel.selected, 'selected set to false');
                expect(callbacks.beforeUnselected).toHaveBeenCalled();
                expect(callbacks.unselected).toHaveBeenCalled();
            });
        });

        describe('cssClasses', () => {
            it('should add unselected to defaults', () => {
                assertDefaultClasses();
            });

            it('should add and remove selected classes', () => {
                assertHasClass('unselected');
                assertDoesNotHaveClass('selected');
                answerViewModel.selected = true;
                assertHasClass('selected');
                assertDoesNotHaveClass('unselected');
                answerViewModel.selected = false;
                assertHasClass('unselected');
                assertDoesNotHaveClass('selected');
            });

            it('should support resetting', () => {
                answerViewModel.addIncorrectStyling();
                answerViewModel.selected = true;
                answerViewModel.resetCssClasses();
                assertDefaultClasses();
            });

            function assertDefaultClasses() {
                assertHasClass('unvalidated');
                assertHasClass('unselected');
                assertDoesNotHaveClass('correct');
                assertDoesNotHaveClass('incorrect');
            }

            function assertHasClass(_class) {
                SpecHelper.expectEqual(true, answerViewModel.cssClasses.includes(_class), `hasClass "${_class}"`);
            }

            function assertDoesNotHaveClass(_class) {
                SpecHelper.expectEqual(
                    false,
                    answerViewModel.cssClasses.includes(_class),
                    `doesNotHaveClass "${_class}"`,
                );
            }
        });
    });

    describe('EditorViewModel', () => {
        let editorViewModel;

        beforeEach(() => {
            editorViewModel = SelectableAnswerModel.EditorViewModel.addComponentTo(frame).setup();
        });

        describe('setup', () => {
            it('should add text', () => {
                SpecHelper.expectEqual('TextModel', editorViewModel.model.text.type);
            });
        });

        describe('hasBackground', () => {
            it('should delete the hasBackground key when turned off', () => {
                editorViewModel.setConfig({
                    supportHasBackground: true,
                });
                editorViewModel.model.hasBackground = true;
                editorViewModel.setConfig({
                    supportHasBackground: false,
                });
                expect(editorViewModel.model.hasBackground).toBeUndefined();
            });
        });

        describe('MaximumTextLength', () => {
            it('should default to MaxTextLengthConfig.ANSWER', () => {
                expect(editorViewModel.model.text.editorViewModel.maxRecommendedTextLength()).toBe(
                    MaxTextLengthConfig.ANSWER,
                );

                // check that it gets added to new ones
                editorViewModel.model.text = frame.addText();
                expect(editorViewModel.model.text.editorViewModel.maxRecommendedTextLength()).toBe(
                    MaxTextLengthConfig.ANSWER,
                );
            });
            it('should be overridable', () => {
                editorViewModel.setConfig({
                    maxRecommendedTextLength: 42,
                });

                expect(editorViewModel.model.text.editorViewModel.maxRecommendedTextLength()).toBe(42);

                // check that it gets added to new ones
                editorViewModel.model.text = frame.addText();
                expect(editorViewModel.model.text.editorViewModel.maxRecommendedTextLength()).toBe(42);
            });
        });
    });

    describe('cf-selectable-answer-editor', () => {
        let elem;
        let scope;
        let editorViewModel;

        describe('with inline skin', () => {
            beforeEach(() => {
                render('inline');
            });

            it('should have a remove button', () => {
                jest.spyOn(editorViewModel, 'remove').mockImplementation(() => {});
                SpecHelper.click(elem, '.remove');
                expect(editorViewModel.remove).toHaveBeenCalled();
            });

            it('should pass disallowSwitchContentType to hasTextOrImageEditor', () => {
                scope.options = {
                    disallowSwitchContentType: 'yup!',
                };
                scope.$digest();
                expect(elem.find('cf-has-text-or-image-editor').scope().options.disallowSwitchContentType).toBe('yup!');
            });

            it('should show alignment options', () => {
                const fontStyleEditor = SpecHelper.expectElement(elem, '[name="font-style-editor"]');
                SpecHelper.expectElements(fontStyleEditor, '[name="alignment"]', 3);
            });
        });

        describe('with related blank', () => {
            let challenge;

            beforeEach(() => {
                render('inline');
                editorViewModel.relatedBlankLabel = 'blank label';
                challenge = editorViewModel.relatedBlankChallenge = frame.addChallenge();
                scope.$digest();
            });

            it('should show label for blank', () => {
                SpecHelper.expectElementText(elem, '[name="related-blank-content"]', 'Correct answer for: blank label');
            });
            it('should support locking and unlocking', () => {
                SpecHelper.expectEqual(false, !!challenge.unlink_blank_from_answer, '!!model.unlink_blank_from_answer');
                const textOrImageEditorScope = elem.find('.cf-has-text-or-image-editor').scope();
                SpecHelper.expectEqual(
                    true,
                    textOrImageEditorScope.disabled,
                    'textOrImageEditorScope.disabled before clicking',
                );
                SpecHelper.click(elem, 'button[name="unlink_blank_from_answer"]');
                SpecHelper.expectEqual(true, challenge.unlink_blank_from_answer, 'model.unlink_blank_from_answer');
                SpecHelper.expectEqual(
                    false,
                    textOrImageEditorScope.disabled,
                    'textOrImageEditorScope.disabled after clicking',
                );
            });
        });

        describe('with relatedChallengeOverlayBlank', () => {
            let relatedChallengeOverlayBlank;
            beforeEach(() => {
                render();
                relatedChallengeOverlayBlank = frame.addChallengeOverlayBlank().addDefaultReferences();
                editorViewModel.relatedChallengeOverlayBlank = relatedChallengeOverlayBlank;
                scope.$digest();
            });
        });

        function render(skin) {
            const renderer = SpecHelper.renderer();
            renderer.scope.editorViewModel = frame.editorViewModelFor(answerViewModel.model);
            renderer.scope.frameViewModel = answerViewModel.frameViewModel;
            renderer.render(
                `<cf-component-editor editor-view-model="editorViewModel" frame-view-model="frameViewModel" skin="${skin}"></cf-component-editor>`,
            );
            elem = renderer.elem;
            scope = elem.find('[editor-view-model]').isolateScope();
            editorViewModel = renderer.scope.editorViewModel;
        }
    });

    function getSelectableAnswerViewModel(options) {
        answerViewModel = getSelectableAnswerModel(options).createViewModel(frame.createFrameViewModel());
        return answerViewModel;
    }

    function getSelectableAnswerModel(options) {
        let answerModel;
        frame = Componentized.fixtures.getInstance();
        frame.addSelectableAnswer(options);
        answerModel = frame.getModelsByType('SelectableAnswerModel')[0];

        expect(answerModel).not.toBeUndefined();
        return answerModel;
    }
});
