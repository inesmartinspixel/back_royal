import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/componentized_fixtures';
import 'Lessons/angularModule/spec/_helpers/componentized_spec_helper';
import 'Editor/angularModule';

describe('Componentized.Component.Challenges', () => {
    let frame;
    let frameViewModel;
    let viewModel;
    let Componentized;
    let SpecHelper;
    let ComponentEventListener;
    let ComponentizedSpecHelper;
    let Challenges;
    let ChallengeEditorViewModel;
    let Challenge;
    let AnswerList;
    let MultipleChoiceChallengeModel;
    let $timeout;
    let MaxTextLengthConfig;
    let ErrorLogService;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                Componentized = $injector.get('Lesson.FrameList.Frame.Componentized');
                ComponentEventListener = $injector.get(
                    'Lesson.FrameList.Frame.Componentized.Component.ComponentEventListener',
                );
                Challenges = $injector.get('Lesson.FrameList.Frame.Componentized.Component.Challenges.ChallengesModel');
                MultipleChoiceChallengeModel = $injector.get(
                    'Lesson.FrameList.Frame.Componentized.Component.Challenge.MultipleChoiceChallenge.MultipleChoiceChallengeModel',
                );
                SpecHelper = $injector.get('SpecHelper');
                Challenge = $injector.get('Lesson.FrameList.Frame.Componentized.Component.Challenge.ChallengeModel');
                ChallengeEditorViewModel = Challenge.EditorViewModel;
                AnswerList = $injector.get('Lesson.FrameList.Frame.Componentized.Component.AnswerList.AnswerListModel');
                $injector.get('ComponentizedFixtures');
                $timeout = $injector.get('$timeout');
                ComponentizedSpecHelper = $injector.get('ComponentizedSpecHelper');
                viewModel = getViewModel();
                MaxTextLengthConfig = $injector.get('MaxTextLengthConfig');
                ErrorLogService = $injector.get('ErrorLogService');
                SpecHelper.stubEventLogging();
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('Model', () => {
        describe('mainTextComponent', () => {
            it('should be gettable', () => {
                const model = viewModel.model;
                model.sharedContentForText = frame.addText();
                expect(model.mainTextComponent).toBe(model.sharedContentForText);
            });
        });
    });

    describe('ViewModel', () => {
        describe('orderedChallengeViewModels', () => {
            it('should set indexes on the challenges', () => {
                SpecHelper.expectEqual(0, viewModel.challengesViewModels[0].index, 'index before re-ordering');
                viewModel.orderedChallengeViewModels = [
                    viewModel.challengesViewModels[1],
                    viewModel.challengesViewModels[0],
                    viewModel.challengesViewModels[2],
                ];
                SpecHelper.expectEqual(1, viewModel.challengesViewModels[0].index, 'index after re-ordering');
                viewModel.orderedChallengeViewModels = [
                    viewModel.challengesViewModels[1],
                    viewModel.challengesViewModels[2],
                    viewModel.challengesViewModels[0],
                ];
                SpecHelper.expectEqual(2, viewModel.challengesViewModels[0].index, 'index after re-ordering again');
                viewModel.orderedChallengeViewModels = undefined;
                SpecHelper.expectEqual(
                    0,
                    viewModel.challengesViewModels[0].index,
                    'index after undoing custom ordering',
                );
            });
        });

        describe('currentChallenge', () => {
            it('should initially be set to the first challenge', () => {
                SpecHelper.expectEqual(
                    viewModel.model.challenges[0],
                    viewModel.currentChallenge,
                    'initial value for currentChallenge',
                );
            });

            it('should not error if there are no challenges', () => {
                const model = getModel();
                model.challenges = [];
                viewModel = frameViewModel.viewModelFor(model);
                expect(viewModel.currentChallenge).toBeUndefined();
            });

            it('should set the old currentChallenge to inactive when changed', () => {
                SpecHelper.expectEqual(
                    true,
                    viewModel.challengesViewModels[0].active,
                    'challenge 0 active before switching.',
                );
                viewModel.currentChallenge = viewModel.model.challenges[1];
                SpecHelper.expectEqual(
                    false,
                    viewModel.challengesViewModels[0].active,
                    'challenge 0 active after switching.',
                );
            });

            it('should set the new currentChallenge to active when changed', () => {
                SpecHelper.expectEqual(
                    false,
                    viewModel.challengesViewModels[1].active,
                    'challenge 1 active before switching.',
                );
                viewModel.currentChallenge = viewModel.model.challenges[1];
                SpecHelper.expectEqual(
                    true,
                    viewModel.challengesViewModels[1].active,
                    'challenge 1 active after switching.',
                );
            });

            it('should accept undefined as a viable value', () => {
                expect(viewModel.currentChallenge).not.toBeUndefined(); // sanity check
                viewModel.currentChallenge = undefined;
                expect(viewModel.currentChallenge).toBeUndefined();
            });

            /*
                any challenge might be activated from some action outside of this component,
                like by pressing the blank for that challenge.  Whenever a challenge is set
                to "active", it should become the currentChallenge.
            */
            it('should be set when a challenge is set to active', () => {
                SpecHelper.expectEqual(
                    true,
                    viewModel.challengesViewModels[0].active,
                    'challenge 0 active before switching.',
                );
                viewModel.challengesViewModels[1].active = true;
                SpecHelper.expectEqual(
                    false,
                    viewModel.challengesViewModels[0].active,
                    'challenge 0 active after switching.',
                );
                SpecHelper.expectEqual(
                    true,
                    viewModel.challengesViewModels[1].active,
                    'challenge 1 active after switching.',
                );
            });

            it('log an error if we add the same challenge twice', () => {
                jest.spyOn(ErrorLogService, 'notify').mockImplementation(() => {});
                const dupeChallenge = viewModel.model.challenges[0];
                viewModel.model.challenges.push(dupeChallenge);
                expect(ErrorLogService.notify).toHaveBeenCalledWith(
                    `Challenge (id=${dupeChallenge.id}) has already been added to model with an existing activated listener.`,
                );
            });

            it('should be reset if challenge is removed from the frame', () => {
                let i = 0;
                while (viewModel.model.challenges[0] && i < 10) {
                    i += 1; // I'm always scared of accidental infinite while loops
                    const challenge = viewModel.model.challenges[0];
                    const nextChallenge = viewModel.model.challenges[1];
                    const challengeHelper = frame.editorViewModelFor(challenge);
                    challengeHelper.remove();
                    SpecHelper.expectEqual(nextChallenge, viewModel.currentChallenge, 'currentChallenge');
                }
            });

            it('should change to a new challenge when it is added', () => {
                const challenge = frame.addChallenge().addDefaultReferences();
                viewModel.model.challenges.push(challenge);
                $timeout.flush();
                expect(viewModel.currentChallenge.id).toBe(challenge.id);
            });
        });

        describe('currentChallengeIndex', () => {
            it('should return the index of the currentChallenge', () => {
                SpecHelper.expectEqual(0, viewModel.currentChallengeIndex, 'currentChallengeIndex before switching');
                viewModel.currentChallenge = viewModel.model.challenges[1];
                SpecHelper.expectEqual(1, viewModel.currentChallengeIndex, 'currentChallengeIndex after switching');
            });
        });

        describe('nextChallengeIndex', () => {
            it('should return the index of the challenge following currentChallenge', () => {
                SpecHelper.expectEqual(1, viewModel.nextChallengeIndex, 'nextChallengeIndex before switching');
                viewModel.currentChallenge = viewModel.model.challenges[1];
                SpecHelper.expectEqual(2, viewModel.nextChallengeIndex, 'nextChallengeIndex after switching');
            });
        });

        describe('nextChallengeViewModel', () => {
            it('should return the view model of the challenge following currentChallenge', () => {
                SpecHelper.expectEqual(
                    viewModel.challengesViewModels[1],
                    viewModel.nextChallengeViewModel,
                    'nextChallengeViewModel before switching',
                );
                viewModel.currentChallenge = viewModel.model.challenges[1];
                SpecHelper.expectEqual(
                    viewModel.challengesViewModels[2],
                    viewModel.nextChallengeViewModel,
                    'nextChallengeViewModel after switching',
                );
            });
        });

        describe('allChallengesComplete', () => {
            it('should return true if all challenges are complete', () => {
                viewModel.challengesViewModels.forEach(challengeViewModel => {
                    challengeViewModel.complete = true;
                });
                SpecHelper.expectEqual(true, viewModel.allChallengesComplete, 'complete');
            });

            it('should return false if all challenges are incomplete', () => {
                SpecHelper.expectEqual(false, viewModel.allChallengesComplete, 'complete');
                viewModel.challengesViewModels[0].complete = true; // one challenge is complete, but others are incomplete.
                SpecHelper.expectEqual(false, viewModel.allChallengesComplete, 'complete');
            });
        });

        describe('states', () => {
            it('should be "readyToValidate" if currentChallenge is "readyToValidate"', () => {
                Object.defineProperty(viewModel.currentChallengeViewModel, 'readyToValidate', {
                    value: true,
                });
                assertState('readyToValidate');
            });

            it('should be "complete" if complete', () => {
                viewModel.complete = true;
                assertState('complete');
            });

            it('should be "waitingForAnswer" if not complete or readyToValidate', () => {
                assertState('waitingForAnswer');
            });

            it('should have a true value for "hasInvalidAnswer" if the currentChallenge is showingIncorrectStyling', () => {
                Object.defineProperty(viewModel.currentChallengeViewModel, 'showingIncorrectStyling', {
                    writable: true,
                });
                viewModel.currentChallengeViewModel.showingIncorrectStyling = false;
                SpecHelper.expectEqual(false, viewModel.hasInvalidAnswer);
                viewModel.currentChallengeViewModel.showingIncorrectStyling = true;
                SpecHelper.expectEqual(true, viewModel.hasInvalidAnswer);
            });

            function assertState(state) {
                ['readyToValidate', 'waitingForAnswer', 'complete'].forEach(_state => {
                    SpecHelper.expectEqual(state === _state, viewModel[_state], _state);
                });
            }
        });

        describe('allChallengesComplete', () => {
            it('should listen for challenges to complete and fire when they all are', () => {
                const callback = jest.fn();
                new ComponentEventListener(viewModel, 'allChallengesComplete', callback);
                viewModel.challengesViewModels.forEach(challengeViewModel => {
                    expect(callback).not.toHaveBeenCalled();
                    challengeViewModel.complete = true;
                });
                expect(callback).toHaveBeenCalled();
            });
        });

        describe('gotoNextChallenge', () => {
            let challenges;
            let challengeViewModels;

            beforeEach(() => {
                challenges = viewModel.model.challenges;
                challengeViewModels = viewModel.challengesViewModels;
                SpecHelper.expectEqual(0, viewModel.currentChallengeIndex);
            });

            it('should use orderedChallengeViewModels', () => {
                // sanity check
                expect(viewModel.challengesViewModels.length).toEqual(3);
                viewModel.orderedChallengeViewModels = [
                    viewModel.challengesViewModels[1],
                    viewModel.challengesViewModels[0],
                    viewModel.challengesViewModels[2],
                ];
                // changing the list of orderedChallengeViewModels should send us back to the beginning
                expect(viewModel.currentChallenge.id).toBe(viewModel.challengesViewModels[1].model.id);
                expect(viewModel.currentChallengeIndex).toEqual(0);

                // go to next
                viewModel.gotoNextChallenge();
                expect(viewModel.currentChallenge.id).toBe(viewModel.challengesViewModels[0].model.id);
                expect(viewModel.currentChallengeIndex).toEqual(1);

                // go to next
                viewModel.gotoNextChallenge();
                expect(viewModel.currentChallenge.id).toBe(viewModel.challengesViewModels[2].model.id);
                expect(viewModel.currentChallengeIndex).toEqual(2);

                // go back to beginning
                viewModel.gotoNextChallenge();
                expect(viewModel.currentChallenge.id).toBe(viewModel.challengesViewModels[1].model.id);
                expect(viewModel.currentChallengeIndex).toEqual(0);
            });

            it('should move on to the next challenge if it is not complete', () => {
                viewModel.gotoNextChallenge();
                SpecHelper.expectEqual(1, viewModel.currentChallengeIndex);
            });

            it('should skip a challenge that is already complete', () => {
                challengeViewModels[1].complete = true;
                viewModel.gotoNextChallenge();
                SpecHelper.expectEqual(2, viewModel.currentChallengeIndex);
            });

            it('should loop back around to the beginning if all subsequent challenges are complete', () => {
                viewModel.currentChallenge = challenges[1];
                challengeViewModels[2].complete = true;
                viewModel.gotoNextChallenge();
                SpecHelper.expectEqual(0, viewModel.currentChallengeIndex);
            });

            it('should do nothing if all challenges are already complete', () => {
                challengeViewModels.forEach(challengeViewModel => {
                    challengeViewModel.complete = true;
                });
                viewModel.gotoNextChallenge();
                SpecHelper.expectEqual(0, viewModel.currentChallengeIndex);
            });
        });

        describe('behaviors', () => {
            describe('GotoNextOnChallengeComplete', () => {
                it('should go on the the next challenge without a timeout when delay is 0', () => {
                    viewModel = getViewModel({
                        behaviors: {
                            GotoNextOnChallengeComplete: {
                                delay: 0,
                            },
                        },
                    });
                    viewModel.currentChallengeViewModel.complete = true;
                    SpecHelper.expectEqual(1, viewModel.currentChallengeIndex);
                });
                it('should go on the the next challenge without a timeout when delay > 0', () => {
                    viewModel = getViewModel({
                        behaviors: {
                            GotoNextOnChallengeComplete: {
                                delay: 1500,
                            },
                        },
                    });
                    viewModel.currentChallengeViewModel.complete = true;
                    $timeout.flush();
                    SpecHelper.expectEqual(1, viewModel.currentChallengeIndex);
                });
                it('should only progress if the user has not done so already', () => {
                    viewModel = getViewModel({
                        behaviors: {
                            GotoNextOnChallengeComplete: {
                                delay: 1500,
                            },
                        },
                    });
                    viewModel.currentChallengeViewModel.complete = true;
                    viewModel.currentChallenge = viewModel.model.challenges[1];
                    $timeout.flush();
                    SpecHelper.expectEqual(1, viewModel.currentChallengeIndex);
                });
                it('should respect the delay in the options', () => {
                    // wanted to test this, but it's too hard to check the delay
                    // that went into a timeout
                });
                it('should kill listeners when removed', () => {
                    SpecHelper.verifyNoNewTimeouts(() => {
                        // this method is creating timeouts, so we need to stub it
                        const ChallengeViewModel = viewModel.challengesViewModels[0].constructor;
                        jest.spyOn(ChallengeViewModel.prototype, 'logCompleted').mockImplementation(() => {});
                        viewModel = getViewModel({
                            behaviors: {
                                GotoNextOnChallengeComplete: {},
                            },
                        });
                        viewModel.model.behaviors.GotoNextOnChallengeComplete = undefined;
                        viewModel.currentChallengeViewModel.complete = true;
                    });

                    SpecHelper.expectEqual(0, viewModel.currentChallengeIndex);
                });
            });

            describe('CompleteOnAllChallengesComplete', () => {
                it('should be set to complete when all challenges are completed', () => {
                    viewModel = getViewModel({
                        behaviors: {
                            CompleteOnAllChallengesComplete: {},
                        },
                    });
                    viewModel.challengesViewModels.forEach(challengeViewModel => {
                        challengeViewModel.complete = true;
                    });
                    SpecHelper.expectEqual(true, viewModel.complete);
                });
                it('should kill listeners when removed', () => {
                    viewModel = getViewModel({
                        behaviors: {
                            CompleteOnAllChallengesComplete: {},
                        },
                    });
                    viewModel.model.behaviors.CompleteOnAllChallengesComplete = undefined;
                    viewModel.challengesViewModels.forEach(challengeViewModel => {
                        challengeViewModel.complete = true;
                    });
                    SpecHelper.expectEqual(false, viewModel.complete);
                });
            });

            describe('RandomizeChallengeOrder', () => {
                it('should randomize whenever a challenge is added or removed', () => {
                    let i = 0;
                    jest.spyOn(_, 'shuffle').mockImplementation(() => `shuffled ${i++}`);
                    viewModel = getViewModel({
                        behaviors: {
                            RandomizeChallengeOrder: {},
                        },
                    });
                    // this will have been shuffled once each time a challenge was added
                    expect(viewModel.orderedChallengeViewModels).toEqual('shuffled 2');
                    const newChallenge = frame.addChallenge().addDefaultReferences();
                    viewModel.model.challenges.push(newChallenge);
                    expect(viewModel.orderedChallengeViewModels).toEqual('shuffled 3');
                    viewModel.model.challenges.remove(newChallenge);
                    expect(viewModel.orderedChallengeViewModels).toEqual('shuffled 4');
                });

                it('should reset order when turned off', () => {
                    jest.spyOn(_, 'shuffle').mockImplementation(() => 'shuffled');
                    viewModel = getViewModel({
                        behaviors: {
                            RandomizeChallengeOrder: {},
                        },
                    });
                    // this will have been shuffled once each time a challenge was added
                    expect(viewModel.orderedChallengeViewModels).toEqual('shuffled');
                    viewModel.model.behaviors.RandomizeChallengeOrder = undefined;
                    expect(viewModel.orderedChallengeViewModels).toEqual(viewModel.challengesViewModels);
                });

                // this is a better experience in the editor, because you can see that something has
                // changed
                it('should go to the first challenge when randomize is turned on or off', () => {
                    jest.spyOn(_, 'shuffle').mockImplementation(items => {
                        const cloned = items.slice(0);
                        const theFirstShallBeLast = cloned.shift();
                        cloned.push(theFirstShallBeLast);
                        return cloned;
                    });

                    expect(viewModel.currentChallenge.id).toBe(
                        viewModel.model.challenges[0].id,
                        'currentChallenge before turning on RandomizeChallengeOrder',
                    );

                    viewModel.model.behaviors.RandomizeChallengeOrder = {};
                    expect(viewModel.currentChallenge.id).toBe(
                        viewModel.model.challenges[1].id,
                        'currentChallenge after turning on RandomizeChallengeOrder',
                    );

                    viewModel.model.behaviors.RandomizeChallengeOrder = undefined;
                    expect(viewModel.currentChallenge.id).toBe(
                        viewModel.model.challenges[0].id,
                        'currentChallenge after turning off RandomizeChallengeOrder',
                    );
                });
            });

            describe('GotoNextFrameOnComplete', () => {
                it('should goto the next frame on complete', () => {
                    viewModel = getViewModel({
                        behaviors: {
                            GotoNextFrameOnComplete: {},
                        },
                    });
                    jest.spyOn(viewModel.frameViewModel, 'gotoNextFrame').mockImplementation(() => {});
                    viewModel.complete = true;
                    expect(viewModel.frameViewModel.gotoNextFrame).toHaveBeenCalled();
                });

                it('should be able to be turned off', () => {
                    viewModel = getViewModel({
                        behaviors: {
                            GotoNextFrameOnComplete: {},
                        },
                    });
                    jest.spyOn(viewModel.frameViewModel, 'gotoNextFrame').mockImplementation(() => {});
                    viewModel.model.behaviors.GotoNextFrameOnComplete = undefined;
                    viewModel.complete = true;
                    expect(viewModel.frameViewModel.gotoNextFrame).not.toHaveBeenCalled();
                });
            });
        });

        describe('contentFor...ViewModel', () => {
            it('should return a component that is shared between all challenges', () => {
                const component = (viewModel.model.sharedContentForText = frame.addVanillaComponent());
                expect(viewModel.contentForTextViewModel.model).toBe(component);
            });
            it('should return a component that is specific to a single challenge', () => {
                const component = (viewModel.currentChallenge.contentForText = frame.addVanillaComponent());
                expect(viewModel.contentForTextViewModel.model).toBe(component);
            });
        });

        describe('image sizing', () => {
            it('should support context image sizing', () => {
                viewModel.model.sharedContentForFirstImage = frame.addImage();
                const model = viewModel.model;
                SpecHelper.expectEqual(
                    'tallContextImage',
                    model.imageContext('sharedContentForFirstImage'),
                    'model.imageContext("sharedContentForFirstImage")',
                );
                SpecHelper.expectEqual(
                    'tallContextImage',
                    viewModel.layoutViewModel.contextForFirstImage,
                    'viewModel.contextForFirstImage',
                );
                viewModel.model.context_image_size = 'short';
                SpecHelper.expectEqual(
                    'shortContextImage',
                    model.imageContext('sharedContentForFirstImage'),
                    'model.imageContext("sharedContentForFirstImage")',
                );
                SpecHelper.expectEqual(
                    'shortContextImage',
                    viewModel.layoutViewModel.contextForFirstImage,
                    'viewModel.contextForFirstImage',
                );
                SpecHelper.expectEqual(
                    viewModel.layoutViewModel.firstContextImageSize,
                    viewModel.model.context_image_size,
                );

                viewModel.model.sharedContentForSecondImage = frame.addImage();
                viewModel.model.context_image_2_size = 'long';
                SpecHelper.expectEqual(
                    viewModel.layoutViewModel.secondContextImageSize,
                    viewModel.model.context_image_2_size,
                );
                SpecHelper.expectEqual(
                    'tallContextImage',
                    model.imageContext('sharedContentForSecondImage'),
                    'model.imageContext("sharedContentForSecondImage")',
                );
                SpecHelper.expectEqual(
                    'tallContextImage',
                    viewModel.layoutViewModel.contextForSecondImage,
                    'viewModel.contextForSecondImage',
                );
                viewModel.model.context_image_2_size = 'short';
                SpecHelper.expectEqual(
                    'shortContextImage',
                    model.imageContext('sharedContentForSecondImage'),
                    'model.imageContext("sharedContentForSecondImage")',
                );
                SpecHelper.expectEqual(
                    'shortContextImage',
                    viewModel.layoutViewModel.contextForSecondImage,
                    'viewModel.contextForSecondImage',
                );
            });
        });
    });

    describe('EditorViewModel', () => {
        let editorViewModel;
        let model;

        beforeEach(() => {
            editorViewModel = Challenges.EditorViewModel.addComponentTo(frame).setup();
            model = editorViewModel.model;
        });

        describe('setup', () => {
            beforeEach(() => {
                editorViewModel = Challenges.EditorViewModel.addComponentTo(frame).setup();
            });
            it('should create empty challenges array', () => {
                expect(editorViewModel.model.challenges).toEqual([]);
            });
            it('should create a layout', () => {
                expect(editorViewModel.model.layout).not.toBeUndefined();
                expect(editorViewModel.model.layout.target).toBe(editorViewModel.model);
            });
        });
        describe('addChallenge', () => {
            beforeEach(() => {
                editorViewModel = Challenges.EditorViewModel.addComponentTo(frame).setup();
                jest.spyOn(ChallengeEditorViewModel.prototype, 'setConfig').mockImplementation(() => {});
                editorViewModel.setConfig({
                    newChallengeType: Challenge,
                });
            });
            it('should create and add a challenge', () => {
                SpecHelper.expectEqual(0, editorViewModel.model.challenges.length);
                editorViewModel.addChallenge();
                SpecHelper.expectEqual(1, editorViewModel.model.challenges.length);
                SpecHelper.expectEqual(Challenge, editorViewModel.model.challenges[0].constructor);
            });
            it('should add a challenge at a specific index', () => {
                const challenge0 = editorViewModel.addChallenge().model;
                const challenge2 = editorViewModel.addChallenge().model;
                const challenge1 = editorViewModel.addChallenge(1).model;
                expect(editorViewModel.model.challenges).toEqual([challenge0, challenge1, challenge2]);
            });
        });

        describe('mainTextComponent', () => {
            it('should be gettable', () => {
                editorViewModel.model.sharedContentForText = frame.addText();
                expect(editorViewModel.mainTextComponent).toBe(editorViewModel.model.sharedContentForText);
            });
            it('should be settable', () => {
                editorViewModel.mainTextComponent = frame.addText();
                expect(editorViewModel.mainTextComponent).not.toBeUndefined();
                expect(editorViewModel.mainTextComponent).toBe(editorViewModel.model.sharedContentForText);
            });
            it('should throw if there is already a nonText element in contentForText', () => {
                const component = (editorViewModel.model.sharedContentForText = frame.addVanillaComponent());
                expect(() => {
                    editorViewModel.mainTextComponent = frame.addText();
                }).toThrow(new Error(`Cannot replace existing ${component.type} with setter on mainTextComponent.`));
            });
        });

        describe('mainImage', () => {
            beforeEach(() => {
                editorViewModel.setConfig({
                    allowSetContextImages: true,
                });
                editorViewModel.firstContextImage = frame.addImage();
                editorViewModel.secondContextImage = frame.addImage();
                editorViewModel.model.sharedContentForInteractiveImage = frame.addInteractiveCards();
                editorViewModel.model.sharedContentForInteractiveImage.challengesComponent = editorViewModel.model;
                editorViewModel.interactiveCards.editorViewModel.firstImage = frame.addImage();
            });
            describe('get', () => {
                it('should give first interactive card highest priority', () => {
                    expect(editorViewModel.mainImage).toBe(editorViewModel.interactiveCards.editorViewModel.firstImage);
                });
                it('should give a component overlay in sharedContentForInteractiveImage second priority', () => {
                    const componentOverlay = (editorViewModel.model.sharedContentForInteractiveImage = frame.addComponentOverlay());
                    const image = (componentOverlay.image = frame.addImage());
                    expect(editorViewModel.mainImage).toBe(image);
                });
                it('should give firstContextImage third priority', () => {
                    editorViewModel.model.sharedContentForInteractiveImage = undefined;
                    expect(editorViewModel.mainImage).toBe(editorViewModel.firstContextImage);
                });
                it('should give secondContextImage fourth priority', () => {
                    editorViewModel.model.sharedContentForInteractiveImage = undefined;
                    editorViewModel.firstContextImage = undefined;
                    expect(editorViewModel.mainImage).toBe(editorViewModel.secondContextImage);
                });
            });
            describe('set', () => {
                it('should set first interactive card if there are interactive cards', () => {
                    const image = frame.addImage();
                    editorViewModel.mainImage = image;
                    expect(editorViewModel.interactiveCards.editorViewModel.firstImage).toBe(image);
                });
                it('should set firstContextImage if there are no interactive cards', () => {
                    editorViewModel.model.sharedContentForInteractiveImage = undefined;
                    const image = frame.addImage();
                    editorViewModel.mainImage = image;
                    expect(editorViewModel.firstContextImage).toBe(image);
                });
                it('should not set firstContextImage if allowSetContextImages false', () => {
                    editorViewModel.model.sharedContentForInteractiveImage = undefined;
                    editorViewModel.setConfig({
                        allowSetContextImages: false,
                    });
                    editorViewModel.model.sharedContentForFirstImage = editorViewModel.model.sharedContentForSecondImage = undefined;
                    editorViewModel.mainImage = frame.addImage();
                    expect(editorViewModel.mainImage).toBeUndefined();
                });
            });
        });

        describe('firstContextImage', () => {
            beforeEach(() => {
                editorViewModel.setConfig({
                    allowSetContextImages: true,
                });
            });

            describe('get', () => {
                it('should be undefined if no sharedContentForFirstImage', () => {
                    model.sharedContentForFirstImage = undefined;
                    expect(!!editorViewModel.firstContextImage).toBe(false);
                });
                it('should be undefined if sharedContentForFirstImage is not an Image', () => {
                    model.sharedContentForFirstImage = frame.addVanillaComponent();
                    expect(!!editorViewModel.firstContextImage).toBe(false);
                });
                it('should be defined if sharedContentForFirstImage is an Image', () => {
                    const image = (model.sharedContentForFirstImage = frame.addImage());
                    expect(editorViewModel.firstContextImage.id).toBe(image.id);
                });
            });
            describe('set', () => {
                it('should set the sharedContentForFirstImage if there is an image', () => {
                    const image = (editorViewModel.firstContextImage = frame.addImage());
                    expect(model.sharedContentForFirstImage.id).toBe(image.id);
                });
                it('should do nothing if not config.allowSetContextImages', () => {
                    editorViewModel.setConfig({
                        allowSetContextImages: false,
                    });
                    editorViewModel.firstContextImage = frame.addImage();
                    expect(model.sharedContentForFirstImage).toBeUndefined();
                });
            });
        });

        describe('secondContextImage', () => {
            beforeEach(() => {
                editorViewModel.setConfig({
                    allowSetContextImages: true,
                });
            });

            describe('get', () => {
                it('should be undefined if no sharedContentForSecondImage', () => {
                    model.sharedContentForSecondImage = undefined;
                    expect(!!editorViewModel.secondContextImage).toBe(false);
                });
                it('should be undefined if sharedContentForSecondImage is not an Image', () => {
                    model.sharedContentForSecondImage = frame.addVanillaComponent();
                    expect(!!editorViewModel.secondContextImage).toBe(false);
                });
                it('should be defined if sharedContentForSecondImage is an Image', () => {
                    const image = (model.sharedContentForSecondImage = frame.addImage());
                    expect(editorViewModel.secondContextImage.id).toBe(image.id);
                });
            });
            describe('set', () => {
                it('should set the sharedContentForSecondImage if there is an image', () => {
                    const image = (editorViewModel.secondContextImage = frame.addImage());
                    expect(model.sharedContentForSecondImage.id).toBe(image.id);
                });
                it('should do nothing if not config.allowSetContextImages', () => {
                    editorViewModel.setConfig({
                        allowSetContextImages: false,
                    });
                    editorViewModel.secondContextImage = frame.addImage();
                    expect(!!model.sharedContentForSecondImage).toBe(false);
                });
            });
        });

        describe('sequential/consumable functionality', () => {
            beforeEach(() => {
                model.challenges = [
                    frame.addMultipleChoiceChallenge().addDefaultReferences(),
                    frame.addMultipleChoiceChallenge().addDefaultReferences(),
                ];

                // sharedAnswerList must be set before sequentialAndConsumable
                editorViewModel.setConfig({
                    supportsSharedAnswerList: true,
                });

                // sharedAnswerList must be set before sequentialAndConsumable
                editorViewModel.setConfig({
                    newChallengeType: MultipleChoiceChallengeModel,
                    supportsSequentialAndConsumable: true,
                });
            });

            it('should be considered consumable if no challenges and a sharedAnswerList', () => {
                model.challenges = [];
                editorViewModel.sharedAnswerList = frame.addAnswerList();
                expect(editorViewModel.consumable).toBe(true);
            });

            it('should be considered sequential if no challenges and a sharedAnswerList', () => {
                model.challenges = [];
                editorViewModel.sharedAnswerList = undefined;
                expect(editorViewModel.sequential).toBe(true);
            });

            it('should be considered sequential if all challenges include ResetAnswersOnActivated', () => {
                model.challenges.forEach(challenge => {
                    challenge.behaviors.ResetAnswersOnActivated = {};
                });
                expect(editorViewModel.sequential).toBe(true);
            });

            it('should be considered consumable if no challenges include ResetAnswersOnActivated and there is a shared answer list', () => {
                const sharedAnswerList = (editorViewModel.sharedAnswerList = frame
                    .addAnswerList()
                    .addDefaultReferences());
                model.challenges.forEach(challenge => {
                    challenge.behaviors.ResetAnswersOnActivated = undefined;
                    challenge.answerList = sharedAnswerList;
                });
                expect(editorViewModel.consumable).toBe(true);
            });

            it('should update all challenges when set to sequential from nothing', () => {
                editorViewModel.sequential = true;
                assertSequentialSetup();
            });

            it('should update all challenges when set to consumable from nothing', () => {
                editorViewModel.consumable = true;
                assertConsumableSetup();
            });

            it('should update all challenges when set to sequential from consumable', () => {
                editorViewModel.consumable = true;
                editorViewModel.sequential = true;
                assertSequentialSetup();
            });

            it('should update all challenges when set to consumable from sequential', () => {
                editorViewModel.sequential = true;
                editorViewModel.consumable = true;
                assertConsumableSetup();
            });

            it('should apply the active mode to new challenges', () => {
                editorViewModel.sequential = true;
                editorViewModel.addChallenge();
                assertSequentialSetup();
                editorViewModel.consumable = true;
                assertConsumableSetup();
                editorViewModel.addChallenge();
                assertConsumableSetup();
                editorViewModel.sequential = true;
                assertSequentialSetup();
            });

            it('should copy answers when switching from consumable to sequential', () => {
                // create two challenges
                editorViewModel.consumable = true;
                editorViewModel.model.challenges = [];
                editorViewModel.addChallenge();
                editorViewModel.addChallenge();

                // create correct answers for each one
                const correctAnswer0 = (editorViewModel.model.challenges[0].editorViewModel.correctAnswer = frame.addSelectableAnswer());
                const correctAnswer1 = (editorViewModel.model.challenges[1].editorViewModel.correctAnswer = frame.addSelectableAnswer());

                // create a confuser
                editorViewModel.sharedAnswerList.answers.push(frame.addSelectableAnswer());

                // switch to sequential
                editorViewModel.sequential = true;

                // each challenge should only have the correct answer in it's answer list
                // the confuser should have been thrown away, since we wouldn't know where to put it
                SpecHelper.expectEqual(
                    [correctAnswer0.id],
                    editorViewModel.model.challenges[0].editorViewModel.model.answerList.answer_ids,
                    'challenge 0 answers',
                );
                SpecHelper.expectEqual(
                    correctAnswer0.id,
                    editorViewModel.model.challenges[0].editorViewModel.correctAnswer.id,
                    'challenge 0 correctAnswer',
                );

                SpecHelper.expectEqual(
                    [correctAnswer1.id],
                    editorViewModel.model.challenges[1].answerList.answer_ids,
                    'challenge 1 answers',
                );
                SpecHelper.expectEqual(
                    correctAnswer1.id,
                    editorViewModel.model.challenges[1].editorViewModel.correctAnswer.id,
                    'challenge 1 correctAnswer',
                );
            });

            it('should copy answers when switching from sequential to consumable', () => {
                // create two challenges
                editorViewModel.sequential = true;
                editorViewModel.model.challenges = [];
                editorViewModel.addChallenge();
                editorViewModel.addChallenge();

                // create correct answers for each one
                const correctAnswer0 = (editorViewModel.model.challenges[0].editorViewModel.correctAnswer = frame.addSelectableAnswer());
                const correctAnswer1 = (editorViewModel.model.challenges[1].editorViewModel.correctAnswer = frame.addSelectableAnswer());

                // create a confuser for the first one
                const confuser = frame.addSelectableAnswer();
                editorViewModel.model.challenges[0].editorViewModel.model.answerList.answers.push(confuser);

                // switch to consumable
                editorViewModel.consumable = true;

                // now there is a single shared answerList.  all the correct answers should have been copied over
                SpecHelper.expectEqual(
                    [correctAnswer0.id, correctAnswer1.id, confuser.id],
                    editorViewModel.sharedAnswerList.answer_ids,
                    'sharedAnswerList answers',
                );
                SpecHelper.expectEqual(
                    correctAnswer0.id,
                    editorViewModel.model.challenges[0].editorViewModel.correctAnswer.id,
                    'challenge 0 correctAnswer',
                );
                SpecHelper.expectEqual(
                    correctAnswer1.id,
                    editorViewModel.model.challenges[1].editorViewModel.correctAnswer.id,
                    'challenge 1 correctAnswer',
                );
            });

            describe('config value turned off', () => {
                it('should cancel listeners', () => {
                    editorViewModel.sequential = true;
                    editorViewModel.setConfig({
                        supportsSequentialAndConsumable: false,
                    });

                    const challenge = editorViewModel.addChallenge().model;
                    expect(challenge.behaviors.ResetAnswersOnActivated).toBeUndefined();
                });
            });

            function assertSequentialSetup() {
                SpecHelper.expectEqual(undefined, model.contentForInteractive, 'model.contentForInteractive');
                const answerLists = {};

                // every challenge should have a unique answerList and should
                // have the ResetAnswersOnActivated behavior
                model.challenges.forEach((challenge, i) => {
                    const answerList = challenge.answerList;
                    SpecHelper.expectEqual(true, !!answerList, `answerList defined for challenge ${i}`);
                    SpecHelper.expectEqual('buttons', answerList.skin, `answerList.skin for challenge ${i}`);
                    SpecHelper.expectEqual(
                        answerList.id,
                        challenge.contentForInteractive.id,
                        `contentForInteractive for challenge ${i}`,
                    );
                    SpecHelper.expectEqual(false, !!answerLists[answerList.id], `answerList unique to challenge ${i}`);
                    answerLists[answerList.id] = answerList;
                    SpecHelper.expectEqual(
                        {},
                        challenge.behaviors.ResetAnswersOnActivated,
                        `challenge ${i} ResetAnswersOnActivated behavior`,
                    );
                });
            }

            function assertConsumableSetup() {
                const sharedAnswerList = model.sharedContentForInteractive;
                SpecHelper.expectEqual(
                    true,
                    !!sharedAnswerList,
                    'sharedAnswerList is defined and set to sharedContentForInteractive',
                );
                SpecHelper.expectEqual('buttons', sharedAnswerList.skin, 'sharedAnswerList.skin');

                // every challenge should have the shared answerList and should
                // not have the ResetAnswersOnActivated behavior
                model.challenges.forEach((challenge, i) => {
                    SpecHelper.expectEqual(
                        sharedAnswerList.id,
                        challenge.answerList.id,
                        `answerList defined for challenge ${i}`,
                    );
                    SpecHelper.expectEqual(
                        undefined,
                        challenge.behaviors.ResetAnswersOnActivated,
                        `challenge ${i} ResetAnswersOnActivated behavior`,
                    );
                });
            }
        });

        describe('sharedAnswerList', () => {
            let answerList;
            let anotherAnswerList;

            beforeEach(() => {
                // add some challenges
                editorViewModel.setConfig({
                    newChallengeType: MultipleChoiceChallengeModel,
                    supportsSharedAnswerList: true,
                });
                editorViewModel.addChallenge();
                editorViewModel.addChallenge();
                editorViewModel.addChallenge();

                // ensure that there are always two answers in every answerList
                editorViewModel.model.challenges.on('childAdded', challenge => {
                    challenge.on('set:answerList', answerList => {
                        while (answerList && answerList.answers.length < 2) {
                            answerList.editorViewModel.addAnswer();
                        }
                    });
                });
                answerList = AnswerList.EditorViewModel.addComponentTo(editorViewModel.frame).setup().model;
                anotherAnswerList = AnswerList.EditorViewModel.addComponentTo(editorViewModel.frame).setup().model;
            });

            describe('set to a valid answerList', () => {
                it('should be applied to all challenges', () => {
                    editorViewModel.sharedAnswerList = answerList;
                    assertSharedAnswerList(answerList);
                });

                it('should unset the contentForInteractive on each challenge', () => {
                    model.challenges[0].contentForInteractive = frame.addVanillaComponent();
                    editorViewModel.sharedAnswerList = answerList;
                    assertSharedAnswerList(answerList);
                });

                it('should work if there is already a sharedAnswerList', () => {
                    editorViewModel.sharedAnswerList = answerList;
                    editorViewModel.sharedAnswerList = anotherAnswerList;
                    assertSharedAnswerList(anotherAnswerList);
                });

                it('should work with target=overlayOnImage', () => {
                    const componentOverlay = (editorViewModel.model.sharedContentForInteractiveImage = frame.addComponentOverlay());
                    editorViewModel.setConfig({
                        supportsSharedAnswerList: {
                            target: 'overlayOnImage',
                        },
                    });
                    editorViewModel.sharedAnswerList = answerList;
                    expect(componentOverlay.overlayComponents).toEqual([answerList]);
                });

                it('should work with target=mainImageMatchingBoard', () => {
                    const matchingBoard = (editorViewModel.model.sharedContentForInteractiveImage = frame.addMatchingBoard());
                    editorViewModel.setConfig({
                        supportsSharedAnswerList: {
                            target: 'mainImageMatchingBoard',
                        },
                    });
                    editorViewModel.sharedAnswerList = answerList;
                    expect(matchingBoard.answerList).toBe(answerList);
                });

                it('should work with target=mainImageTilePromptBoard', () => {
                    const tilePromptBoard = (editorViewModel.model.sharedContentForInteractiveImage = frame.addTilePromptBoard());
                    editorViewModel.setConfig({
                        supportsSharedAnswerList: {
                            target: 'mainImageTilePromptBoard',
                        },
                    });
                    editorViewModel.sharedAnswerList = answerList;
                    expect(tilePromptBoard.answerList).toBe(answerList);
                });
            });

            describe('set to undefined', () => {
                it('should clean up', () => {
                    editorViewModel.sharedAnswerList = answerList;
                    editorViewModel.sharedAnswerList = undefined;
                    assertNoSharedAnswerList();
                });
            });

            describe('challenge added with shared answerList set', () => {
                it('should be applied to added challenges', () => {
                    editorViewModel.sharedAnswerList = answerList;
                    editorViewModel.addChallenge();
                    assertSharedAnswerList(answerList);
                });
            });

            describe('challenge added with shared answerList undefined', () => {
                it('should add a distinct answerList to the challenge', () => {
                    editorViewModel.sharedAnswerList = undefined;
                    const challenge = editorViewModel.addChallenge().model;
                    const answerList = challenge.answerList;
                    SpecHelper.expectEqual(true, !!answerList, 'answerList for added challenge is defined');
                    SpecHelper.expectEqual(
                        answerList.id,
                        challenge.contentForInteractive.id,
                        'contentForInteractive for added challenge id',
                    );
                    const otherAnswerLists = editorViewModel.model.challenges.map(ch => {
                        if (ch !== challenge) {
                            return ch.answerList;
                        }
                    });
                    SpecHelper.expectEqual(
                        true,
                        !otherAnswerLists.includes(answerList),
                        'answerList for added challenge is distinct',
                    );
                });
            });

            describe('set to something other than answerList', () => {
                it('should throw', () => {
                    expect(() => {
                        editorViewModel.sharedAnswerList = frame.addVanillaComponent();
                    }).toThrow(new Error('Cannot set sharedAnswerList to something that is not an AnswerListModel'));
                });
            });

            describe('config value turned off', () => {
                it('should cancel listeners', () => {
                    editorViewModel.setConfig({
                        supportsSharedAnswerList: false,
                    });
                    const challenge = editorViewModel.addChallenge().model;
                    expect(challenge.contentForInteractive).toBeUndefined();
                });
            });

            describe('get', () => {
                it('should work', () => {
                    editorViewModel.sharedAnswerList = answerList;
                    expect(editorViewModel.sharedAnswerList.id).toBe(answerList.id);
                    editorViewModel.sharedAnswerList = undefined;
                    expect(editorViewModel.sharedAnswerList).toBeUndefined();
                });
            });

            function assertNoSharedAnswerList() {
                SpecHelper.expectEqual(
                    false,
                    !!editorViewModel.sharedContentForInteractive,
                    'sharedContentForInteractive defined',
                );

                const answerListIds = [];
                model.challenges.forEach((challenge, i) => {
                    const answerList = challenge.answerList;
                    SpecHelper.expectEqual(true, !!answerList, `challenge ${i} answerList is defined`);
                    expect(answerList).not.toBeUndefined();
                    SpecHelper.expectEqual('buttons', answerList.skin, 'answerList.skin');
                    SpecHelper.expectEqual(
                        true,
                        !answerListIds.includes(answerList.id),
                        'answer list is unique to challenge',
                    );
                    answerListIds.push(answerList.id);
                    SpecHelper.expectEqual(
                        answerList.id,
                        challenge.contentForInteractive.id,
                        `challenge ${i} contentForInteractive`,
                    );
                });
            }

            function assertSharedAnswerList(answerList) {
                // sharedContentForInteractive should be the answer list
                SpecHelper.expectEqual(
                    answerList.id,
                    model.sharedContentForInteractive.id,
                    'sharedContentForInteractive id',
                );

                model.challenges.forEach((challenge, i) => {
                    // challenge should have the answerList
                    SpecHelper.expectEqual(answerList.id, challenge.answerList.id, `challenge ${i} answer list id`);
                    // should not have contentForInteractive
                    SpecHelper.expectEqual(
                        false,
                        !!challenge.contentForInteractive,
                        `challenge ${i} contentForInteractive defined`,
                    );
                });
            }
        });

        describe('maxRecommendedTextLength', () => {
            beforeEach(() => {
                editorViewModel.setConfig({
                    allowSetContextImages: true,
                });
            });

            it('should default to values from MaxTextLengthConfig', () => {
                editorViewModel.mainTextComponent = frame.addText();
                const textEditorViewModel = editorViewModel.mainTextComponent.editorViewModel;
                editorViewModel.firstContextImage = undefined;
                expect(textEditorViewModel.maxRecommendedTextLength()).toBe(MaxTextLengthConfig.TEXT_WITHOUT_IMAGE);

                editorViewModel.firstContextImage = frame.addImage();
                expect(textEditorViewModel.maxRecommendedTextLength()).toBe(MaxTextLengthConfig.TEXT_WITH_IMAGE);
            });

            it('should be overridable', () => {
                editorViewModel.setConfig({
                    maxRecommendedTextLengthWithoutImage: 79,
                    maxRecommendedTextLengthWithImage: 42,
                });

                editorViewModel.mainTextComponent = frame.addText();
                const textEditorViewModel = editorViewModel.mainTextComponent.editorViewModel;
                editorViewModel.firstContextImage = undefined;
                expect(textEditorViewModel.maxRecommendedTextLength()).toBe(79);

                editorViewModel.firstContextImage = frame.addImage();
                expect(textEditorViewModel.maxRecommendedTextLength()).toBe(42);
            });
        });

        describe('supportsNoIncorrectAnswers', () => {
            beforeEach(() => {
                editorViewModel.setConfig({
                    newChallengeType: MultipleChoiceChallengeModel,
                });
                editorViewModel.addChallenge();
                editorViewModel.addChallenge();
            });

            it('should set each challenge', () => {
                editorViewModel.supportsNoIncorrectAnswers = true;
                expect(editorViewModel.model.challenges[0].editorViewModel.supportsNoIncorrectAnswers).toBe(true);
                expect(editorViewModel.model.challenges[1].editorViewModel.supportsNoIncorrectAnswers).toBe(true);
                editorViewModel.supportsNoIncorrectAnswers = false;
                expect(editorViewModel.model.challenges[0].editorViewModel.supportsNoIncorrectAnswers).toBe(false);
                expect(editorViewModel.model.challenges[1].editorViewModel.supportsNoIncorrectAnswers).toBe(false);
            });

            it('should set on new challenges', () => {
                let challengeEditorViewModel;

                editorViewModel.supportsNoIncorrectAnswers = true;
                challengeEditorViewModel = editorViewModel.addChallenge();
                expect(challengeEditorViewModel.supportsNoIncorrectAnswers).toBe(true);

                editorViewModel.supportsNoIncorrectAnswers = false;
                challengeEditorViewModel = editorViewModel.addChallenge();
                expect(challengeEditorViewModel.supportsNoIncorrectAnswers).toBe(false);
            });
        });

        describe('noIncorrectAnswers', () => {
            beforeEach(() => {
                editorViewModel.frame.continueButton = frame.addContinueButton();
                editorViewModel.setConfig({
                    newChallengeType: MultipleChoiceChallengeModel,
                });
                editorViewModel.supportsNoIncorrectAnswers = true;
                editorViewModel.addChallenge();
                editorViewModel.addChallenge();
            });

            it('should set each challenge to noIncorrectAnswers', () => {
                editorViewModel.noIncorrectAnswers = true;
                expect(editorViewModel.model.challenges[0].editorViewModel.noIncorrectAnswers).toBe(true);
                expect(editorViewModel.model.challenges[1].editorViewModel.noIncorrectAnswers).toBe(true);
                editorViewModel.noIncorrectAnswers = false;
                expect(editorViewModel.model.challenges[0].editorViewModel.noIncorrectAnswers).toBe(false);
                expect(editorViewModel.model.challenges[1].editorViewModel.noIncorrectAnswers).toBe(false);
            });

            it('should set show_continue_when_ready_to_validate', () => {
                editorViewModel.noIncorrectAnswers = true;
                expect(editorViewModel.frame.continueButton.show_continue_when_ready_to_validate).toBe(true);
                editorViewModel.noIncorrectAnswers = false;
                expect(editorViewModel.frame.continueButton.show_continue_when_ready_to_validate).toBe(false);
            });

            it('should set GotoNextFrameOnComplete', () => {
                editorViewModel.noIncorrectAnswers = true;
                expect(editorViewModel.model.behaviors.GotoNextFrameOnComplete).toEqual({});
                editorViewModel.noIncorrectAnswers = false;
                expect(editorViewModel.model.behaviors.GotoNextFrameOnComplete).toBeUndefined();
            });

            it('should set on new challenges', () => {
                let challengeEditorViewModel;

                editorViewModel.noIncorrectAnswers = true;
                challengeEditorViewModel = editorViewModel.addChallenge();
                expect(challengeEditorViewModel.noIncorrectAnswers).toBe(true);

                editorViewModel.noIncorrectAnswers = false;
                challengeEditorViewModel = editorViewModel.addChallenge();
                expect(challengeEditorViewModel.noIncorrectAnswers).toBe(false);
            });

            it('should initially be set to true based on the challenges', () => {
                Object.defineProperty(
                    editorViewModel.model.challenges[0].editorViewModel.constructor.prototype,
                    'noIncorrectAnswers',
                    {
                        value: true,
                        configurable: true,
                    },
                );

                const newEditorViewModel = new editorViewModel.constructor(editorViewModel.model);
                newEditorViewModel.supportsNoIncorrectAnswers = true;
                expect(newEditorViewModel.noIncorrectAnswers).toBe(true);
            });

            it('should initially be set to false based on the challenges', () => {
                Object.defineProperty(
                    editorViewModel.model.challenges[0].editorViewModel.constructor.prototype,
                    'noIncorrectAnswers',
                    {
                        value: false,
                        configurable: true,
                    },
                );

                const newEditorViewModel = new editorViewModel.constructor(editorViewModel.model);
                expect(newEditorViewModel.noIncorrectAnswers).toBe(false);
            });
        });

        describe('hasBranching', () => {
            beforeEach(() => {
                editorViewModel.supportsHasBranching = true;
            });
            describe('set', () => {
                it('should turn off checkMany when set to true', () => {
                    editorViewModel.setConfig({
                        supportsCheckMany: {
                            answerListSkin: {
                                true: 'checkboxes',
                                false: 'buttons',
                            },
                        },
                        supportsSharedAnswerList: true,
                    });
                    editorViewModel.sharedAnswerList = frame.addAnswerList();
                    editorViewModel.checkMany = true;
                    editorViewModel.hasBranching = true;
                    expect(editorViewModel.checkMany).toBe(false);
                });
                it('should turn on noIncorrectAnswers when set to true', () => {
                    editorViewModel.model.challenges.push(frame.addMultipleChoiceChallenge().addDefaultReferences()); // required for noIncorrectAnswers to work
                    frame.continueButton = frame.addContinueButton();
                    editorViewModel.supportsNoIncorrectAnswers = true;
                    editorViewModel.noIncorrectAnswers = false;
                    editorViewModel.hasBranching = true;
                    expect(editorViewModel.noIncorrectAnswers).toBe(true);
                });
                it('should set challenges on the frameNavigator.editorViewModel when set to true and remove them when set to false', () => {
                    expect(frame.frameNavigator.editorViewModel.challenges).toBeUndefined();
                    editorViewModel.hasBranching = true;
                    expect(frame.frameNavigator.editorViewModel.challenges).toEqual(editorViewModel.model.challenges);
                    // ensure that they are kept in sync when a challenge is added
                    editorViewModel.model.challenges.push(frame.addMultipleChoiceChallenge().addDefaultReferences());
                    expect(frame.frameNavigator.editorViewModel.challenges).toEqual(editorViewModel.model.challenges);
                    editorViewModel.hasBranching = false;
                    expect(frame.frameNavigator.editorViewModel.challenges).toBeUndefined();
                });
                it('should set challenges on the frameNavigator.editorViewModel when supportsHasBranching is first set', () => {
                    editorViewModel.hasBranching = true;
                    const newEditorViewModel = new editorViewModel.constructor(editorViewModel.model);
                    frame.frameNavigator.editorViewModel.challengesComponent = undefined;
                    newEditorViewModel.supportsHasBranching = true;
                    expect(frame.frameNavigator.editorViewModel.challenges).toEqual(editorViewModel.model.challenges);
                });
                it('should delete selectable answer navigators when set to false', () => {
                    editorViewModel.hasBranching = true;
                    const selectableAnswerNavigator = frame.addSelectableAnswerNavigator();
                    frame.frameNavigator.selectableAnswerNavigators.push(selectableAnswerNavigator);
                    editorViewModel.hasBranching = false;
                    expect(selectableAnswerNavigator.frame()).toBeUndefined(); // should have been removed
                    expect(frame.frameNavigator.selectableAnswerNavigators).toEqual([]);
                });
            });
        });
    });

    describe('cf-challenges-editor', () => {
        let elem;
        let scope;
        let model;
        let editorViewModel;

        beforeEach(() => {
            model = viewModel.model;
            const renderer = ComponentizedSpecHelper.renderEditorDir(viewModel, frameViewModel);
            elem = renderer.elem;
            scope = renderer.editorScope;
            editorViewModel = scope.editorViewModel;
        });

        it('should be possible to remove a challenge', () => {
            const length = model.challenges.length;
            SpecHelper.click(elem, '[name="remove_challenge"]', 0);
            expect(model.challenges.length).toBe(length - 1);
        });

        describe('with shared content for text component', () => {
            it('should show the editor', () => {
                const component = (model.sharedContentForText = frame.addVanillaComponent());
                scope.$digest();
                ComponentizedSpecHelper.assertEditorElementPresent(elem, component);
            });
        });

        describe('with shared answer list', () => {
            it('should show the editor', () => {
                const component = frame.addAnswerList().addDefaultReferences();
                Object.defineProperty(editorViewModel, 'sharedAnswerList', {
                    value: component,
                });
                scope.$digest();
                ComponentizedSpecHelper.assertEditorElementPresent(elem, component);
            });
        });

        describe('challenges', () => {
            it('should include an editor for each challenge', () => {
                model.challenges.forEach(challenge => {
                    ComponentizedSpecHelper.assertEditorElementPresent(
                        elem.find('[name="challenges_list"]'),
                        challenge,
                    );
                });
            });
            it('should allow selecting the currentChallenge', () => {
                SpecHelper.click(elem, '[name="challenges_list"] .challenge_row', 1);
                SpecHelper.expectEqual(1, viewModel.currentChallengeIndex);

                SpecHelper.click(elem, '[name="challenges_list"] .challenge_row', 0);
                SpecHelper.expectEqual(0, viewModel.currentChallengeIndex);
            });
            it('should have a working "Add Challenge" button', () => {
                editorViewModel.setConfig({
                    newChallengeType: Challenge,
                });
                const challengeLength = editorViewModel.model.challenges.length;
                SpecHelper.click(elem, '[name="add_challenge"]');
                $timeout.flush();
                SpecHelper.expectEqual(challengeLength + 1, editorViewModel.model.challenges.length, 'challengeLength');
                expect(viewModel.currentChallenge.id).toBe(_.last(editorViewModel.model.challenges).id);
            });
        });

        describe('options', () => {
            it('should show user-defined options', () => {
                editorViewModel.setConfig({
                    userDefinedOptions: [
                        {
                            type: 'toggleBehavior',
                        },
                    ],
                });
                scope.$digest();
                const el = SpecHelper.expectElement(elem, '[name="options"] .cf-user-defined-options');
                expect(el.scope().editorViewModel).toBe(editorViewModel);
                expect(el.scope().options).toBe(editorViewModel.config.userDefinedOptions);
            });

            it('should have a next frame selector that sets the next frame on the frameNavigator', () => {
                const frame2 = model.frame().lesson().addFrame();
                scope.$digest();
                expect(model.frame().frameNavigator.next_frame_id).toBeUndefined();
                SpecHelper.selectOptionByLabel(elem, '[name="options"] cf-next-frame-selector select', frame2.label);
                expect(model.frame().frameNavigator.next_frame_id).toBe(frame2.id);
                SpecHelper.selectOptionByLabel(
                    elem,
                    '[name="options"] cf-next-frame-selector select',
                    ' --- Next frame --- ',
                );
                expect(model.frame().frameNavigator.next_frame_id).toBeUndefined();
            });

            it('should hide next frame selector if hasBranching=true', () => {
                Object.defineProperty(editorViewModel, 'hasBranching', {
                    value: true,
                });
                scope.$digest();
                SpecHelper.expectNoElement(elem, '[name="options"] cf-next-frame-selector');
            });
        });

        describe('disallowAddAndReorderChallenges', () => {
            beforeEach(() => {
                editorViewModel.model.addChallenge();
                editorViewModel.setConfig({
                    disallowAddAndReorderChallenges: true,
                });
                scope.$digest();
            });

            it('should hide the remove challenge button', () => {
                SpecHelper.expectNoElement(elem, '[name="remove_challenge"]');
            });
            it('should hide the addChallenge button', () => {
                SpecHelper.expectNoElement(elem, '[name="add_challenge"]');
            });
            it('should hide the reorder challenge buttons', () => {
                SpecHelper.expectNoElement(elem, '.cf-reorder-list-item-buttons');
            });
        });
    });

    function getViewModel(options) {
        const model = getModel(options);
        return frameViewModel.viewModelFor(model);
    }

    function getModel(options) {
        frame = Componentized.fixtures.getInstance();
        frameViewModel = frame.createFrameViewModel();
        const challenges = frame.addChallenges(options);
        challenges.addChallenge();
        challenges.addChallenge();
        challenges.addChallenge();
        return challenges.addDefaultReferences();
    }
});
