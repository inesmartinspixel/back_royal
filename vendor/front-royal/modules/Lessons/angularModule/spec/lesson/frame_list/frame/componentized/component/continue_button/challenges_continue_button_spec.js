import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/componentized_fixtures';
import 'Editor/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import continueButtonLocales from 'Lessons/locales/lessons/lesson/frame_list/frame/componentized/component/continue_button-en.json';

setSpecLocales(continueButtonLocales);

describe('Componentized.Component.ContinueButton.ChallengesContinueButton', () => {
    let frame;
    let frameViewModel;
    let viewModel;
    let Componentized;
    let SpecHelper;
    let ChallengeViewModel;
    let SoundManager;
    let SoundConfig;
    let scope;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                Componentized = $injector.get('Lesson.FrameList.Frame.Componentized');
                ChallengeViewModel = $injector.get(
                    'Lesson.FrameList.Frame.Componentized.Component.Challenge.ChallengeViewModel',
                );
                SpecHelper = $injector.get('SpecHelper');
                $injector.get('ComponentizedFixtures');
                SoundManager = $injector.get('SoundManager');
                SoundConfig = $injector.get('SoundConfig');
                viewModel = getViewModel();
            },
        ]);
    });
    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('Model', () => {});

    describe('cf-challenges-continue-button', () => {
        let elem;
        let button;

        describe('in waitingForAnswer state', () => {
            beforeEach(() => {
                Object.defineProperty(viewModel, 'miniInstructions', {
                    value: 'mini instructions',
                });
                setState('waitingForAnswer');
                render();
            });

            it('should show skip button if indicated', () => {
                SpecHelper.expectElementText(elem, 'button', '');
                Object.defineProperty(scope.targetComponentViewModel, 'allowSkip', {
                    value: true,
                });
                scope.$digest();
                SpecHelper.expectElementText(elem, 'button', 'Skip');
            });
        });

        describe('in readyToValidate state', () => {
            beforeEach(() => {
                setState('readyToValidate');
                render();
                button = SpecHelper.expectElement(elem, '.cf-continue-button button');
                frameViewModel.playerViewModel = {
                    activeFrame: viewModel.frame,
                    gotoNext: jest.fn(),
                };
            });

            it('should render appropriate button', () => {
                SpecHelper.expectEqual('Check Answer', button.text().trim(), 'button text');
            });

            it('should validate when clicked', () => {
                SpecHelper.expectElementEnabled(elem, '.cf-continue-button button');
                jest.spyOn(viewModel.targetComponentViewModel, 'validate').mockImplementation(() => {});
                button.click();
                expect(viewModel.targetComponentViewModel.validate).toHaveBeenCalled();
            });

            it('should not validate when clicked and activeFrame is different', () => {
                frameViewModel.playerViewModel.activeFrame = {};
                SpecHelper.expectElementEnabled(elem, '.cf-continue-button button');
                button.click();
                expect(frameViewModel.playerViewModel.gotoNext).not.toHaveBeenCalled();
            });

            it('should play a sound when clicked', () => {
                jest.spyOn(viewModel.targetComponentViewModel, 'validate').mockImplementation(() => {});
                jest.spyOn(SoundManager, 'playUrl').mockImplementation(() => {});
                button.click();
                expect(SoundManager.playUrl).toHaveBeenCalledWith(SoundConfig.DEFAULT_CLICK);
            });

            describe('with show_continue_when_ready_to_validate', () => {
                it('should show continue button', () => {
                    viewModel.model.show_continue_when_ready_to_validate = true;
                    scope.$digest();
                    SpecHelper.expectEqual('Continue', button.text().trim(), 'button text');
                });
            });
        });

        describe('in complete state', () => {
            beforeEach(() => {
                setState('complete');
                render();
                button = SpecHelper.expectElement(elem, '.cf-continue-button button');
                frameViewModel.playerViewModel = {
                    activeFrame: viewModel.frame,
                    gotoNext: jest.fn(),
                };
            });

            it('should render appropriate button', () => {
                SpecHelper.expectEqual('Continue', button.text().trim(), 'button text');
            });

            it('should move on when clicked ', () => {
                SpecHelper.expectElementEnabled(elem, '.cf-continue-button button');
                button.click();
                expect(frameViewModel.playerViewModel.gotoNext).toHaveBeenCalled();
            });

            it('should not move on when clicked and activeFrame is different', () => {
                frameViewModel.playerViewModel.activeFrame = {};
                SpecHelper.expectElementEnabled(elem, '.cf-continue-button button');
                button.click();
                expect(frameViewModel.playerViewModel.gotoNext).not.toHaveBeenCalled();
            });

            it('should play a sound when clicked', () => {
                jest.spyOn(SoundManager, 'playUrl').mockImplementation(() => {});
                button.click();
                expect(SoundManager.playUrl).toHaveBeenCalledWith(SoundConfig.DEFAULT_CLICK);
            });

            it('shoud have special buttons in practice mode', () => {
                Object.defineProperty(frameViewModel, 'isPractice', {
                    value: true,
                });
                render();
                SpecHelper.expectElementText(elem, '.practice-mode-continue button:eq(0)', 'Not Helpful');
                SpecHelper.expectElementText(elem, '.practice-mode-continue button:eq(1)', 'Helpful');
                jest.spyOn(viewModel, 'onPracticeModeContinueButtonClick').mockImplementation(() => {});
                SpecHelper.click(elem, 'button:eq(0)');
                expect(viewModel.onPracticeModeContinueButtonClick).toHaveBeenCalledWith(false);
                viewModel.onPracticeModeContinueButtonClick.mockClear();

                SpecHelper.click(elem, 'button:eq(1)');
                expect(viewModel.onPracticeModeContinueButtonClick).toHaveBeenCalledWith(true);
                viewModel.onPracticeModeContinueButtonClick.mockClear();
            });
        });

        it('should set continueButtonVisible', () => {
            render();
            expect(frameViewModel.continueButtonVisible).toBe(false);
            scope.targetComponentViewModel.complete = true;
            scope.$digest();
            expect(frameViewModel.continueButtonVisible).toBe(true);
        });

        function setState(activeState) {
            ['complete', 'waitingForAnswer', 'readyToValidate'].forEach(state => {
                Object.defineProperty(viewModel.targetComponentViewModel, state, {
                    value: state === activeState,
                });
            });
        }

        function render() {
            Object.defineProperty(ChallengeViewModel.prototype, 'showingIncorrectStyling', {
                value: true,
            });
            const renderer = SpecHelper.renderer();
            renderer.scope.viewModel = viewModel;
            renderer.render('<cf-ui-component view-model="viewModel"></cf-ui-component>');
            elem = renderer.elem;
            scope = elem.find('cf-challenges-continue-button').isolateScope();
        }
    });

    function getViewModel(options) {
        const model = getModel(options);
        return frameViewModel.viewModelFor(model);
    }

    function getModel() {
        frame = Componentized.fixtures.getInstance();
        frameViewModel = frame.createFrameViewModel();
        return frame.addChallengesContinueButton().addDefaultReferences();
    }
});
