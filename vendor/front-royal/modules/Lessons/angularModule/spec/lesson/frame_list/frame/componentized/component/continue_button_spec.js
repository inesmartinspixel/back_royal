import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/componentized_fixtures';
import 'Editor/angularModule';

describe('Componentized.Component.ContinueButton', () => {
    let Componentized;
    let SpecHelper;
    let frame;
    let model;
    let viewModel;
    let frameViewModel;
    let EventLogger;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                Componentized = $injector.get('Lesson.FrameList.Frame.Componentized');
                SpecHelper = $injector.get('SpecHelper');
                EventLogger = $injector.get('EventLogger');
                $injector.get('ComponentizedFixtures');
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('Model', () => {});
    describe('ViewModel', () => {
        describe('onPracticeModeContinueButtonClick', () => {
            it('should log and move on', () => {
                getViewModel();
                jest.spyOn(EventLogger.prototype, 'log').mockImplementation(() => {});
                jest.spyOn(frameViewModel, 'gotoNextFrame').mockImplementation(() => {});
                viewModel.onPracticeModeContinueButtonClick(true);
                expect(EventLogger.prototype.log).toHaveBeenCalled();
                expect(EventLogger.prototype.log.mock.calls[0][0]).toEqual('lesson:frame:practice_mode_continue_click');
                expect(EventLogger.prototype.log.mock.calls[0][1].helpful).toEqual(true);
                expect(EventLogger.prototype.log.mock.calls[0][1].frame_id).toEqual(frame.id);
                expect(frameViewModel.gotoNextFrame).toHaveBeenCalled();

                EventLogger.prototype.log.mockClear();
                viewModel.onPracticeModeContinueButtonClick(false);
                expect(EventLogger.prototype.log.mock.calls[0][1].helpful).toEqual(false);
            });
        });
    });

    function getViewModel() {
        getModel();
        viewModel = frameViewModel.viewModelFor(model);
        return viewModel;
    }

    function getModel() {
        frame = Componentized.fixtures.getInstance();
        frameViewModel = frame.createFrameViewModel();
        model = frame.addContinueButton();
        return model;
    }
});
