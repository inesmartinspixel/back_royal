import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/componentized_fixtures';
import 'Editor/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import tilePromptBoardLocales from 'Lessons/locales/lessons/lesson/frame_list/frame/componentized/component/tile_prompt_board-en.json';

setSpecLocales(tilePromptBoardLocales);

describe('Componentized.Component.TilePromptBoard', () => {
    let frame;
    let frameViewModel;
    let viewModel;
    let Componentized;
    let TilePromptBoard;
    let SpecHelper;
    let $injector;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                Componentized = $injector.get('Lesson.FrameList.Frame.Componentized');
                TilePromptBoard = $injector.get(
                    'Lesson.FrameList.Frame.Componentized.Component.TilePromptBoard.TilePromptBoardModel',
                );
                SpecHelper = $injector.get('SpecHelper');
                $injector.get('ComponentizedFixtures');
                viewModel = getViewModel();
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
        $('body').removeClass('modal-open');
    });

    describe('Model', () => {});

    describe('ViewModel', () => {
        describe('initialize', () => {
            it('should set the background color', () => {
                expect(viewModel.frameViewModel.bodyBackgroundColor).toBe('purple');
            });
        });
    });

    describe('cf-tile-prompt-board', () => {
        let elem;
        let scope;
        let model;

        it('should render a tile_prompt challenge button for each challenge', () => {
            render();
            SpecHelper.expectElement(elem, `[component-id="${model.tilePrompts[0].id}"]`);
        });

        it('should render a fake tile prompt', () => {
            render();
            SpecHelper.expectElements(elem, '.fake-tile', 1);
        });

        it('should render an answer list', () => {
            render();
            SpecHelper.expectElement(elem, `[component-id="${model.answerList.id}"]`);
        });

        it('should should style the selected / next / last tiles', () => {
            render();
            const selected = SpecHelper.expectElements(elem, '.tile.selected', 1);
            SpecHelper.expectElementAttr(selected, 'cf-tile-prompt:eq(0)', 'component-id', model.tilePrompts[0].id);

            const next = SpecHelper.expectElements(elem, '.tile.next', 1);
            SpecHelper.expectElementAttr(next, 'cf-tile-prompt:eq(0)', 'component-id', model.tilePrompts[1].id);

            const last = SpecHelper.expectElements(elem, '.tile.last', 1);
            SpecHelper.expectElementAttr(last, 'cf-tile-prompt:eq(0)', 'component-id', model.tilePrompts[2].id);
        });

        it('should should style the tiles left / right + correct / incorrect upon selection', () => {
            render();
            const selected = SpecHelper.expectElements(elem, '.tile.selected', 1);
            SpecHelper.expectElementAttr(selected, 'cf-tile-prompt:eq(0)', 'component-id', model.tilePrompts[0].id);

            validateCorrect();

            SpecHelper.expectElements(elem, '.tile.selected.animate-full.animate-right-full', 1);

            validateIncorrect(true);
            SpecHelper.expectElements(elem, '.tile.selected.animate-right-partial', 1);

            validateIncorrect(false);
            SpecHelper.expectElements(elem, '.tile.selected.animate-right-partial', 0);
        });

        it('should should set completion style when challenges are complete', () => {
            render();
            SpecHelper.expectNoElement(elem, '.tile-prompt-board.completed');
            markComplete();
            SpecHelper.expectElements(elem, '.tile-prompt-board.completed', 1);
        });

        it('should respond to arrow keys', () => {
            render();

            const $document = $injector.get('$document');
            const $window = $injector.get('$window');

            jest.spyOn(scope, 'selectLeft').mockImplementation(() => {});
            let e = $window.jQuery.Event('keydown');
            e.which = 37; // left
            $document.trigger(e);
            scope.$digest();
            expect(scope.selectLeft).toHaveBeenCalled();

            jest.spyOn(scope, 'selectRight').mockImplementation(() => {});
            e = $window.jQuery.Event('keydown');
            e.which = 39; // right
            $document.trigger(e);
            scope.$digest();
            expect(scope.selectRight).toHaveBeenCalled();
        });

        it('should not respond to arrow keys in editorMode', () => {
            viewModel.frameViewModel.playerViewModel = {
                editorMode: true,
            };

            render();

            const $document = $injector.get('$document');
            const $window = $injector.get('$window');

            jest.spyOn(scope, 'selectLeft').mockImplementation(() => {});
            let e = $window.jQuery.Event('keydown');
            e.which = 37; // left
            $document.trigger(e);
            scope.$digest();
            expect(scope.selectLeft).not.toHaveBeenCalled();

            jest.spyOn(scope, 'selectRight').mockImplementation(() => {});
            e = $window.jQuery.Event('keydown');
            e.which = 39; // right
            $document.trigger(e);
            scope.$digest();
            expect(scope.selectRight).not.toHaveBeenCalled();
        });

        it('should not respond to arrow keys when modal is visible', () => {
            render();

            const $document = $injector.get('$document');
            const $window = $injector.get('$window');

            $('body').addClass('modal-open');

            jest.spyOn(scope, 'selectLeft').mockImplementation(() => {});
            let e = $window.jQuery.Event('keydown');
            e.which = 37; // left
            $document.trigger(e);
            scope.$digest();
            expect(scope.selectLeft).not.toHaveBeenCalled();

            jest.spyOn(scope, 'selectRight').mockImplementation(() => {});
            e = $window.jQuery.Event('keydown');
            e.which = 39; // right
            $document.trigger(e);
            scope.$digest();
            expect(scope.selectRight).not.toHaveBeenCalled();
        });

        // TODO: Testing of touch inputs? Have no idea how that is supposed to work

        function render() {
            const renderer = SpecHelper.renderer();
            renderer.scope.viewModel = viewModel;
            model = viewModel.model;
            renderer.render('<cf-ui-component view-model="viewModel"></cf-ui-component>');
            elem = renderer.elem;
            scope = renderer.scope;
            scope = elem.isolateScope().$$childTail.$$childTail.$$childTail; // why is this necessary?
        }

        function validateIncorrect(selected) {
            if (selected === undefined) {
                selected = true;
            }
            viewModel.answerListViewModel.currentChallengeViewModel.fire('validatedIncorrect', {
                result: false,
                info: {
                    event: {
                        type: selected ? 'answerSelected' : 'answerUnselected',
                        target: viewModel.answerListViewModel,
                    },
                },
            });
            scope.$digest();
        }

        function validateCorrect() {
            viewModel.answerListViewModel.currentChallengeViewModel.fire('validatedCorrect', {
                result: true,
                info: {
                    event: {
                        type: 'answerSelected',
                        target: viewModel.answerListViewModel,
                    },
                },
            });
            scope.$digest();
        }

        function markComplete() {
            viewModel.challengesComponentViewModel.fire('completed');
            scope.$digest();
        }
    });

    describe('EditorViewModel', () => {
        let editorViewModel;

        beforeEach(() => {
            editorViewModel = TilePromptBoard.EditorViewModel.addComponentTo(frame).setup();
        });

        describe('setup', () => {
            it('should initialize tilePrompts if not non-existent', () => {
                SpecHelper.expectEqual([], editorViewModel.model.tilePrompts);
            });
        });

        describe('initialize', () => {
            it('should setup challenge listeners if challengesComponent exists', () => {
                jest.spyOn(editorViewModel, 'setupChallengeListeners').mockImplementation(() => {});
                editorViewModel.model.challengesComponent = frame.addChallenges().addDefaultReferences();
                editorViewModel.initialize(editorViewModel.model);
                expect(editorViewModel.setupChallengeListeners).toHaveBeenCalled();
            });

            it('should setup challenge listeners upon addition of challengesComponent if not initially present', () => {
                jest.spyOn(editorViewModel, 'setupChallengeListeners').mockImplementation(() => {});
                editorViewModel.initialize(editorViewModel.model);
                expect(editorViewModel.setupChallengeListeners).not.toHaveBeenCalled();
                editorViewModel.model.challengesComponent = frame.addChallenges().addDefaultReferences();
                expect(editorViewModel.setupChallengeListeners).toHaveBeenCalled();
            });
        });

        describe('challenge listeners', () => {
            let challengesComponent;
            beforeEach(() => {
                challengesComponent = frame.addChallenges().addDefaultReferences();
                editorViewModel.model.challengesComponent = challengesComponent;
                editorViewModel.initialize(editorViewModel.model);
            });

            it('should handle the addition of new challenges', () => {
                jest.spyOn(editorViewModel, 'addTilePromptToChallenge');
                const origChallengesCount = editorViewModel.model.challengesComponent.challenges.length;
                expect(editorViewModel.model.tilePrompts.length).toBe(origChallengesCount);
                editorViewModel.model.challengesComponent.addChallenge();
                expect(editorViewModel.addTilePromptToChallenge).toHaveBeenCalled();
                expect(editorViewModel.model.challengesComponent.challenges.length).toBe(origChallengesCount + 1);
                expect(editorViewModel.model.tilePrompts.length).toBe(origChallengesCount + 1);
            });

            it('should handle the removal of existing challenges', () => {
                jest.spyOn(editorViewModel, 'removeTilePromptForChallenge');
                const origChallengesCount = editorViewModel.model.challengesComponent.challenges.length;
                expect(editorViewModel.model.tilePrompts.length).toBe(origChallengesCount);
                editorViewModel.model.challengesComponent.challenges.remove(
                    editorViewModel.model.challengesComponent.challenges[0],
                );
                expect(editorViewModel.removeTilePromptForChallenge).toHaveBeenCalled();
                expect(editorViewModel.model.challengesComponent.challenges.length).toBe(origChallengesCount - 1);
                expect(editorViewModel.model.tilePrompts.length).toBe(origChallengesCount - 1);
            });

            it('should handle the reordering of existing challenges', () => {
                const origChallengesCount = editorViewModel.model.challengesComponent.challenges.length;
                expect(editorViewModel.model.tilePrompts.length).toBe(origChallengesCount);
                const movedChallenge = editorViewModel.model.challengesComponent.challenges[0];
                editorViewModel.model.challengesComponent.challenges.remove(movedChallenge);
                editorViewModel.model.challengesComponent.challenges.push(movedChallenge);
                expect(editorViewModel.model.challengesComponent.challenges.length).toBe(origChallengesCount);
                expect(editorViewModel.model.tilePrompts.length).toBe(origChallengesCount);
            });
        });
    });

    function getViewModel(options) {
        const model = getModel(options);
        const viewModel = frameViewModel.viewModelFor(model);
        viewModel.answerListViewModel.currentChallengeViewModel = viewModel.tilePromptsViewModels[0].challengeViewModel;
        viewModel.tilePromptsViewModels[0].challengeViewModel.active = true;

        expect(viewModel.tilePromptsViewModels.length).toBe(3); // sanity check
        return viewModel;
    }

    function getModel() {
        frame = Componentized.fixtures.getInstance();
        frameViewModel = frame.createFrameViewModel();
        frameViewModel.playerViewModel = {
            editorMode: false,
        };
        return frame.addTilePromptBoard().addDefaultReferences();
    }
});
