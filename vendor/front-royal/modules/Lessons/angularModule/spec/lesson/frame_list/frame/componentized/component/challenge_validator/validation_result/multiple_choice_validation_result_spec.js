import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/componentized_fixtures';
import 'Editor/angularModule';

describe('Componentized.Component.ChallengeValidator.ValidationResult.MultipleChoiceValidationResult', () => {
    let MultipleChoiceValidationResult;
    let ValidationError;
    let SpecHelper;
    let Componentized;
    let challengeViewModel;
    let frame;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                MultipleChoiceValidationResult = $injector.get(
                    'Lesson.FrameList.Frame.Componentized.Component.ChallengeValidator.ValidationResult.MultipleChoiceValidationResult',
                );
                ValidationError = $injector.get(
                    'Lesson.FrameList.Frame.Componentized.Component.ChallengeValidator.ValidationError',
                );
                SpecHelper = $injector.get('SpecHelper');
                Componentized = $injector.get('Lesson.FrameList.Frame.Componentized');
                $injector.get('ComponentizedFixtures');
                challengeViewModel = getChallengeViewModel();
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('filterAnswersViewModels', () => {
        const index = vm => vm.index;

        it('should filter nothing by default', () => {
            const validationResult = new MultipleChoiceValidationResult(challengeViewModel, []);
            SpecHelper.expectEqual(
                [0, 1, 2, 3],
                validationResult.filterAnswersViewModels({}).map(index),
                'answers filtered by selected',
            );
        });

        it('should filter for selected=true', () => {
            challengeViewModel.answersViewModels[1].selected = true;
            const validationResult = new MultipleChoiceValidationResult(challengeViewModel, []);
            SpecHelper.expectEqual(
                [1],
                validationResult
                    .filterAnswersViewModels({
                        selected: true,
                    })
                    .map(index),
                'answers filtered by selected',
            );
        });

        it('should filter for selected=false', () => {
            challengeViewModel.answersViewModels[0].selected = true;
            const validationResult = new MultipleChoiceValidationResult(challengeViewModel, []);
            SpecHelper.expectEqual(
                [1, 2, 3],
                validationResult
                    .filterAnswersViewModels({
                        selected: false,
                    })
                    .map(index),
                'answers filtered by selected',
            );
        });

        it('should filter for correct=true', () => {
            // mock out an error related to 'answer 0'
            const mockError = new ValidationError();
            jest.spyOn(mockError, 'isRelatedToAnswer').mockImplementation(
                answer => answer === challengeViewModel.answersViewModels[0].model,
            );

            const validationResult = new MultipleChoiceValidationResult(challengeViewModel, [mockError]);
            SpecHelper.expectEqual(
                [1, 2, 3],
                validationResult
                    .filterAnswersViewModels({
                        correct: true,
                    })
                    .map(index),
                'answers filtered by selected',
            );
        });

        it('should filter for correct=false', () => {
            // mock out an error related to 'answer 0'
            const mockError = new ValidationError();
            jest.spyOn(mockError, 'isRelatedToAnswer').mockImplementation(
                answer => answer === challengeViewModel.answersViewModels[0].model,
            );

            const validationResult = new MultipleChoiceValidationResult(challengeViewModel, [mockError]);
            SpecHelper.expectEqual(
                [0],
                validationResult
                    .filterAnswersViewModels({
                        correct: false,
                    })
                    .map(index),
                'answers filtered by selected',
            );
        });
    });

    function getChallengeViewModel(options) {
        const viewModel = getChallengeModel(options).createViewModel(frame.createFrameViewModel());
        // add indexes for easy messaging
        viewModel.answersViewModels.forEach((answerViewModel, i) => {
            answerViewModel.index = i;
        });
        return viewModel;
    }

    function getChallengeModel(options) {
        let model;
        frame = Componentized.fixtures.getInstance();
        frame.addMultipleChoiceChallenge(options).addDefaultReferences();
        model = frame.getModelsByType('MultipleChoiceChallengeModel')[0];
        expect(model).not.toBeUndefined();
        return model;
    }
});
