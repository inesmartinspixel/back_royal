import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/componentized_fixtures';
import 'Editor/angularModule';

describe('Componentized.Component.ComponentEventListener', () => {
    let ComponentEventListener;
    let SpecHelper;
    let viewModel;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                const Componentized = $injector.get('Lesson.FrameList.Frame.Componentized');
                const ComponentModel = $injector.get('Lesson.FrameList.Frame.Componentized.Component.ComponentModel');
                ComponentEventListener = $injector.get(
                    'Lesson.FrameList.Frame.Componentized.Component.ComponentEventListener',
                );
                SpecHelper = $injector.get('SpecHelper');
                $injector.get('ComponentizedFixtures');
                const frame = Componentized.fixtures.getInstance();
                const frameViewModel = frame.createFrameViewModel();
                const model = ComponentModel.new();
                viewModel = model.createViewModel(frameViewModel);
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('trigger', () => {
        it('should fire an event listener that has been created', () => {
            const callback = jest.fn();
            new ComponentEventListener(viewModel, 'event', callback);
            viewModel.fire('event', 'arg1', 'arg2');
            expect(callback).toHaveBeenCalledWith('arg1', 'arg2');
        });

        it('should respect priority', () => {
            const callOrder = [];
            const callbacks = [
                jest.fn().mockImplementation(() => {
                    callOrder.push(0);
                }),
                jest.fn().mockImplementation(() => {
                    callOrder.push(-1);
                }),
                jest.fn().mockImplementation(() => {
                    callOrder.push(1);
                }),
            ];
            new ComponentEventListener(viewModel, 'event', callbacks[0]);
            new ComponentEventListener(viewModel, 'event', callbacks[2], {
                priority: 1,
            });
            new ComponentEventListener(viewModel, 'event', callbacks[1], {
                priority: -1,
            });
            viewModel.fire('event');
            expect(callOrder).toEqual([-1, 0, 1]);
        });
    });

    describe('cancel', () => {
        it('should cancel an event listener', () => {
            const callback = jest.fn();
            const listener = new ComponentEventListener(viewModel, 'event', callback);
            listener.cancel();
            viewModel.fire('event', 'arg1', 'arg2');
            expect(callback).not.toHaveBeenCalled();
        });
    });
});
