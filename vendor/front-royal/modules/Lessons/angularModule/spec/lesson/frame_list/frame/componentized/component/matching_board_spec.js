import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/componentized_fixtures';
import 'Editor/angularModule';

describe('Componentized.Component.MatchingBoard', () => {
    let frame;
    let frameViewModel;
    let viewModel;
    let Componentized;
    let MatchingBoard;
    let SpecHelper;
    let ValidationResult;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                Componentized = $injector.get('Lesson.FrameList.Frame.Componentized');
                MatchingBoard = $injector.get(
                    'Lesson.FrameList.Frame.Componentized.Component.MatchingBoard.MatchingBoardModel',
                );
                SpecHelper = $injector.get('SpecHelper');
                ValidationResult = $injector.get(
                    'Lesson.FrameList.Frame.Componentized.Component.ChallengeValidator.ValidationResult',
                );
                $injector.get('ComponentizedFixtures');
                viewModel = getViewModel();
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('Model', () => {});

    describe('ViewModel', () => {
        beforeEach(() => {
            viewModel.model.addButton();
            viewModel.model.addButton();
            viewModel.model.addButton();
        });

        describe('initialize', () => {
            it('should set the background color', () => {
                expect(viewModel.frameViewModel.bodyBackgroundColor).toBe('turquoise');
            });
            it('should set disallowedAnswerOrder', () => {
                // disallowedAnswerOrder should be set so that the answers never show up
                // in the same order as the challenges
                expect(viewModel.answerListViewModel.disallowedAnswerOrder.map(vm => vm.model.id)).toEqual(
                    viewModel.matchingChallengeButtonsViewModels.map(vm => vm.model.challenge.correctAnswer.id),
                );

                // changing the list should reset disallowedAnswerOrder
                viewModel.model.matchingChallengeButtons = [viewModel.model.matchingChallengeButtons[0]];
                expect(viewModel.answerListViewModel.disallowedAnswerOrder.map(vm => vm.model.id)).toEqual(
                    viewModel.matchingChallengeButtonsViewModels.map(vm => vm.model.challenge.correctAnswer.id),
                );
            });
        });

        describe('orderedMatchingChallengeButtonViewModels', () => {
            it('should order challenges by the order in which they were completed', () => {
                expect(viewModel.matchingChallengeButtonsViewModels.length).toBe(4); // sanity check
                expect(viewModel.orderedMatchingChallengeButtonViewModels).toEqual(
                    viewModel.matchingChallengeButtonsViewModels,
                );

                viewModel.matchingChallengeButtonsViewModels[2].challengeViewModel.complete = true;
                expect(viewModel.orderedMatchingChallengeButtonViewModels).toEqual([
                    viewModel.matchingChallengeButtonsViewModels[2],
                    viewModel.matchingChallengeButtonsViewModels[0],
                    viewModel.matchingChallengeButtonsViewModels[1],
                    viewModel.matchingChallengeButtonsViewModels[3],
                ]);

                viewModel.matchingChallengeButtonsViewModels[0].challengeViewModel.complete = true;
                expect(viewModel.orderedMatchingChallengeButtonViewModels).toEqual([
                    viewModel.matchingChallengeButtonsViewModels[2],
                    viewModel.matchingChallengeButtonsViewModels[0],
                    viewModel.matchingChallengeButtonsViewModels[1],
                    viewModel.matchingChallengeButtonsViewModels[3],
                ]);
            });
        });
        describe('ordering answers', () => {
            it('should order answers by the order in which they were validated correctly', () => {
                expect(viewModel.answerListViewModel.answersViewModels.length).toBe(4);
                const answers = viewModel.answerListViewModel.model.answers;
                const currentChallengeViewModel = viewModel.answerListViewModel.currentChallengeViewModel;
                const correctAnswer = (currentChallengeViewModel.model.editorViewModel.correctAnswer = answers[2]);
                expect(viewModel.answerListViewModel.orderedAnswerViewModels[0].model).toBe(answers[0]);

                // select an incorrect answer, which should do nothing
                currentChallengeViewModel.fire(
                    'validatedIncorrect',
                    new ValidationResult(currentChallengeViewModel, [], {
                        event: {
                            target: viewModel.answerListViewModel.answersViewModels[1],
                        },
                    }),
                );
                expect(viewModel.answerListViewModel.orderedAnswerViewModels[0].model).toBe(answers[0]);

                // select the correct answer, which should cause it to come to the front
                currentChallengeViewModel.fire(
                    'validatedCorrect',
                    new ValidationResult(currentChallengeViewModel, [], {
                        event: {
                            target: viewModel.viewModelFor(correctAnswer),
                        },
                    }),
                );
                expect(viewModel.answerListViewModel.orderedAnswerViewModels[0].model).toBe(correctAnswer);
            });
        });
    });

    describe('cf-matching-board', () => {
        let elem;
        let model;

        it('should render a matching challenge button for each challenge', () => {
            render();
            SpecHelper.expectElement(elem, `[component-id="${model.matchingChallengeButtons[0].id}"]`);
        });

        it('should render an answer list', () => {
            render();
            SpecHelper.expectElement(elem, `[component-id="${model.answerList.id}"]`);
        });

        function render() {
            const renderer = SpecHelper.renderer();
            renderer.scope.viewModel = viewModel;
            model = viewModel.model;
            renderer.render('<cf-ui-component view-model="viewModel"></cf-ui-component>');
            elem = renderer.elem;
        }
    });

    describe('EditorViewModel', () => {
        let editorViewModel;

        beforeEach(() => {
            editorViewModel = MatchingBoard.EditorViewModel.addComponentTo(frame).setup();
        });

        describe('setup', () => {
            it('should initialize matchingChallengeButtons if not non-existent', () => {
                SpecHelper.expectEqual([], editorViewModel.model.matchingChallengeButtons);
            });
        });

        describe('initialize', () => {
            it('should setup challenge listeners if challengesComponent exists', () => {
                jest.spyOn(editorViewModel, 'setupChallengeListeners').mockImplementation(() => {});
                editorViewModel.model.challengesComponent = frame.addChallenges().addDefaultReferences();
                editorViewModel.initialize(editorViewModel.model);
                expect(editorViewModel.setupChallengeListeners).toHaveBeenCalled();
            });

            it('should setup challenge listeners upon addition of challengesComponent if not initially present', () => {
                jest.spyOn(editorViewModel, 'setupChallengeListeners').mockImplementation(() => {});
                editorViewModel.initialize(editorViewModel.model);
                expect(editorViewModel.setupChallengeListeners).not.toHaveBeenCalled();
                editorViewModel.model.challengesComponent = frame.addChallenges().addDefaultReferences();
                expect(editorViewModel.setupChallengeListeners).toHaveBeenCalled();
            });
        });

        describe('challenge listeners', () => {
            let challengesComponent;
            beforeEach(() => {
                challengesComponent = frame.addChallenges().addDefaultReferences();
                editorViewModel.model.challengesComponent = challengesComponent;
                editorViewModel.initialize(editorViewModel.model);

                // make sure we have editor view models
                _.pluck(frame.components, 'editorViewModel');
            });

            it('should handle the addition of new challenges', () => {
                jest.spyOn(editorViewModel, 'addMatchingChallengeButtonToChallenge');
                const origChallengesCount = editorViewModel.model.challengesComponent.challenges.length;
                expect(editorViewModel.model.matchingChallengeButtons.length).toBe(origChallengesCount);
                editorViewModel.model.challengesComponent.addMultipleChoiceChallenge();
                expect(editorViewModel.addMatchingChallengeButtonToChallenge).toHaveBeenCalled();
                expect(editorViewModel.model.challengesComponent.challenges.length).toBe(origChallengesCount + 1);
                expect(editorViewModel.model.matchingChallengeButtons.length).toBe(origChallengesCount + 1);
            });

            it('should handle the removal of existing challenges', () => {
                jest.spyOn(editorViewModel, 'removeMatchingChallengeButtonFromChallenge');
                const origChallengesCount = editorViewModel.model.challengesComponent.challenges.length;
                expect(editorViewModel.model.matchingChallengeButtons.length).toBe(origChallengesCount);
                editorViewModel.model.challengesComponent.challenges.remove(
                    editorViewModel.model.challengesComponent.challenges[0],
                );
                expect(editorViewModel.removeMatchingChallengeButtonFromChallenge).toHaveBeenCalled();
                expect(editorViewModel.model.challengesComponent.challenges.length).toBe(origChallengesCount - 1);
                expect(editorViewModel.model.matchingChallengeButtons.length).toBe(origChallengesCount - 1);
            });

            it('should handle the reordering of existing challenges', () => {
                const origChallengesCount = editorViewModel.model.challengesComponent.challenges.length;
                expect(editorViewModel.model.matchingChallengeButtons.length).toBe(origChallengesCount);
                const movedChallenge = editorViewModel.model.challengesComponent.challenges[0];
                editorViewModel.model.challengesComponent.challenges.remove(movedChallenge);
                editorViewModel.model.challengesComponent.challenges.push(movedChallenge);
                expect(editorViewModel.model.challengesComponent.challenges.length).toBe(origChallengesCount);
                expect(editorViewModel.model.matchingChallengeButtons.length).toBe(origChallengesCount);
            });

            it('should not mess up image answers when running setupChallengeListeners', () => {
                const origChallengesCount = editorViewModel.model.challengesComponent.challenges.length;
                expect(editorViewModel.model.matchingChallengeButtons.length).toBe(origChallengesCount);
                const changedChallenge = editorViewModel.model.challengesComponent.challenges[0];
                // make this challenge an image challenge
                const image = (changedChallenge.editorViewModel.correctAnswerImage = frame.addImage());
                // reload the editorViewModel (to simulate refreshing the editor page after a save)
                editorViewModel.setupChallengeListeners();
                expect(changedChallenge.editorViewModel.correctAnswerText).toBe(undefined);
                expect(changedChallenge.editorViewModel.correctAnswerImage).toBe(image);
            });
        });
    });

    function getViewModel(options) {
        const model = getModel(options);
        const viewModel = frameViewModel.viewModelFor(model);
        viewModel.answerListViewModel.currentChallengeViewModel =
            viewModel.matchingChallengeButtonsViewModels[0].challengeViewModel;
        viewModel.matchingChallengeButtonsViewModels[0].challengeViewModel.active = true;

        expect(viewModel.matchingChallengeButtonsViewModels.length).toBe(1); // sanity check
        return frameViewModel.viewModelFor(model);
    }

    function getModel() {
        frame = Componentized.fixtures.getInstance();
        frameViewModel = frame.createFrameViewModel();
        return frame.addMatchingBoard().addDefaultReferences();
    }
});
