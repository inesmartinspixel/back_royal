import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/componentized_fixtures';
import 'Editor/angularModule';

describe('Componentized.Component.SelectableAnswerNavigator', () => {
    let frame;
    let answerNavigatorViewModel;
    let Componentized;
    let SpecHelper;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                Componentized = $injector.get('Lesson.FrameList.Frame.Componentized');
                SpecHelper = $injector.get('SpecHelper');
                $injector.get('ComponentizedFixtures');
                answerNavigatorViewModel = getSelectableAnswerNavigatorViewModel();
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('ViewModel', () => {
        describe('selected', () => {
            it('should navigate based on selected answer', () => {
                const answer = answerNavigatorViewModel.model.answerMatcher.answer;
                const answerViewModel = answerNavigatorViewModel.viewModelFor(answer);

                // activate the challenge
                answerNavigatorViewModel.challengeViewModel.active = true;

                // select the first answer
                answerViewModel.selected = true;

                // make sure the first answer's next_frame_id has gotten populated into the answerNavigatorViewModel
                expect(answerNavigatorViewModel.frameNavigatorViewModel.nextFrameId).toBe(
                    answerNavigatorViewModel.model.next_frame_id,
                );
            });
        });
    });

    describe('EditorViewModel', () => {
        describe('_onAnswerContentChanged', () => {
            it('should remove the navigator if a change to the answer text causes a conflict', () => {
                const navigator1 = getModel();
                const challenge = navigator1.challenge;

                navigator1.answerMatcher.answer.text.text = 'this';
                const navigator2 = frame
                    .addSelectableAnswerNavigator({
                        challenge_id: challenge.id,
                    })
                    .addDefaultReferences();
                // make sure we have editorViewModels
                _.pluck(frame.components, 'editorViewModel');

                navigator2.attachToAnswer(challenge.answers[1]);
                navigator2.answerMatcher.answer.text.text = 'that';

                expect(navigator2.challenge).toBe(navigator1.challenge);
                expect(navigator2.appliesToAnswer(navigator1.answerMatcher.answer));
                expect(navigator2.answerMatcher.answer).not.toBe(navigator1.answerMatcher.answer);

                // when we change the text on answer 1 to be the same as
                // answer 2, the navigator from answer 1 should be deleted
                jest.spyOn(navigator1, 'remove').mockImplementation(() => {});
                navigator1.answerMatcher.answer.text.text = 'that';
                expect(navigator1.remove).toHaveBeenCalled();
            });

            it('should remove the navigator if a change to the answer image causes a conflict', () => {
                const navigator1 = getModel();
                const challenge = navigator1.challenge;

                navigator1.answerMatcher.answer.text.text = 'this';
                const navigator2 = frame
                    .addSelectableAnswerNavigator({
                        challenge_id: challenge.id,
                    })
                    .addDefaultReferences();
                // make sure we have editorViewModels
                _.pluck(frame.components, 'editorViewModel');
                navigator2.attachToAnswer(challenge.answers[1]);
                const image = (navigator2.answerMatcher.answer.image = frame.addImage());

                expect(navigator2.challenge).toBe(navigator1.challenge);
                expect(navigator2.appliesToAnswer(navigator1.answerMatcher.answer));
                expect(navigator2.answerMatcher.answer).not.toBe(navigator1.answerMatcher.answer);

                // when we change the text on answer 1 to be the same as
                // answer 2, the navigator from answer 1 should be deleted
                jest.spyOn(navigator1, 'remove').mockImplementation(() => {});
                navigator1.answerMatcher.answer.image = image;
                expect(navigator1.remove).toHaveBeenCalled();
            });
        });
    });

    function getSelectableAnswerNavigatorViewModel(options) {
        return getModel(options).createViewModel(frame.createFrameViewModel());
    }

    function getModel(options) {
        frame = Componentized.fixtures.getInstance();
        return frame.addSelectableAnswerNavigator(options).addDefaultReferences();
        // let answerNavigatorModel = frame.getModelsByType('SelectableAnswerNavigatorModel')[0];
        // expect(answerNavigatorModel).not.toBeUndefined();
        // return answerNavigatorModel;
    }
});
