import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/componentized_fixtures';
import 'Editor/angularModule';

describe('Componentized.Component.AnswerMatcher.SimilarToSelectableAnswer', () => {
    let frame;
    let model;
    let Componentized;
    let SpecHelper;
    let ChallengeResponse;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                Componentized = $injector.get('Lesson.FrameList.Frame.Componentized');
                SpecHelper = $injector.get('SpecHelper');
                $injector.get('ComponentizedFixtures');
                ChallengeResponse = $injector.get(
                    'Lesson.FrameList.Frame.Componentized.Component.Challenge.ChallengeResponse',
                );
                model = getModel();
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('Model', () => {
        describe('matches', () => {
            describe('with text on this.answer', () => {
                beforeEach(() => {
                    const answer = frame.addSelectableAnswer().addDefaultReferences();
                    answer.text = frame.addText({
                        text: 'text',
                    });
                    model.answer = answer;
                });

                it('should match an answer with the same text', () => {
                    const answer = frame.addSelectableAnswer().addDefaultReferences();
                    answer.text = frame.addText({
                        text: 'text',
                    });
                    const challengeResponse = ChallengeResponse.fromSelectableAnswer(answer);
                    expect(model.matches(challengeResponse)).toBe(true);
                });

                it('should not match an answer with text that is case-insensitively the same', () => {
                    const answer = frame.addSelectableAnswer().addDefaultReferences();
                    answer.text = frame.addText({
                        text: 'Text',
                    });
                    const challengeResponse = ChallengeResponse.fromSelectableAnswer(answer);
                    expect(model.matches(challengeResponse)).toBe(false);
                });

                it('should not match an answer with different text', () => {
                    const answer = frame.addSelectableAnswer().addDefaultReferences();
                    answer.text = frame.addText({
                        text: 'different',
                    });
                    const challengeResponse = ChallengeResponse.fromSelectableAnswer(answer);
                    expect(model.matches(challengeResponse)).toBe(false);
                });

                it('should not match an answer with no text', () => {
                    const answer = frame.addSelectableAnswer().addDefaultReferences();
                    const challengeResponse = ChallengeResponse.fromSelectableAnswer(answer);
                    expect(model.matches(challengeResponse)).toBe(false);
                });

                it('should work when passed a selectable answer', () => {
                    const answer = frame.addSelectableAnswer().addDefaultReferences();
                    answer.text = frame.addText({
                        text: 'text',
                    });
                    expect(model.matches(answer)).toBe(true);
                });
            });
            describe('with image on this.answer', () => {
                beforeEach(() => {
                    const answer = frame.addSelectableAnswer().addDefaultReferences();
                    answer.text = undefined;
                    answer.image = frame.addImage();
                    model.answer = answer;
                });

                it('should match an answer with the same image', () => {
                    const answer = frame.addSelectableAnswer().addDefaultReferences();
                    answer.text = model.answer.text = undefined;
                    answer.image = model.answer.image;
                    const challengeResponse = ChallengeResponse.fromSelectableAnswer(answer);
                    expect(model.matches(challengeResponse)).toBe(true);
                });

                it('should not match an answer with different image', () => {
                    const answer = frame.addSelectableAnswer().addDefaultReferences();
                    answer.image = frame.addImage();
                    const challengeResponse = ChallengeResponse.fromSelectableAnswer(answer);
                    expect(model.matches(challengeResponse)).toBe(false);
                });

                it('should not match an answer with no image', () => {
                    const answer = frame.addSelectableAnswer().addDefaultReferences();
                    const challengeResponse = ChallengeResponse.fromSelectableAnswer(answer);
                    expect(model.matches(challengeResponse)).toBe(false);
                });
            });
            describe('with position on this.answer', () => {
                let answer;

                beforeEach(() => {
                    answer = frame.addSelectableAnswer().addDefaultReferences();
                    answer.x = 42;
                    answer.y = 42;
                    model.answer = answer;
                });

                it('should match an answer with the same position', () => {
                    const otherAnswer = frame.addSelectableAnswer().addDefaultReferences();
                    otherAnswer.x = 42;
                    otherAnswer.y = 42;
                    const challengeResponse = ChallengeResponse.fromSelectableAnswer(otherAnswer);
                    expect(model.matches(challengeResponse)).toBe(true);
                });

                it('should not match an answer with different x', () => {
                    const otherAnswer = frame.addSelectableAnswer().addDefaultReferences();
                    otherAnswer.x = 21;
                    otherAnswer.y = 42;
                    const challengeResponse = ChallengeResponse.fromSelectableAnswer(otherAnswer);
                    expect(model.matches(challengeResponse)).toBe(false);
                });

                it('should not match an answer with different y', () => {
                    const otherAnswer = frame.addSelectableAnswer().addDefaultReferences();
                    otherAnswer.x = 42;
                    otherAnswer.y = 21;
                    const challengeResponse = ChallengeResponse.fromSelectableAnswer(otherAnswer);
                    expect(model.matches(challengeResponse)).toBe(false);
                });

                it('should match an answer with x and y 0', () => {
                    answer.x = 0;
                    answer.y = 0;
                    const otherAnswer = frame.addSelectableAnswer().addDefaultReferences();
                    otherAnswer.x = 0;
                    otherAnswer.y = 0;
                    const challengeResponse = ChallengeResponse.fromSelectableAnswer(otherAnswer);
                    expect(model.matches(challengeResponse)).toBe(true);
                });
            });
            describe('with neither text nor image on this.answer', () => {
                it('should throw', () => {
                    expect(() => {
                        const answer = frame.addSelectableAnswer().addDefaultReferences();
                        answer.text = answer.image = undefined;
                        model.answer = answer;
                        const challengeResponse = ChallengeResponse.fromSelectableAnswer(answer);
                        model.matches(challengeResponse);
                    }).toThrow(new Error('What should I do here?'));
                });
            });
        });
    });
    describe('ViewModel', () => {});
    describe('behaviors', () => {});
    describe('EditorViewModel', () => {
        it('should delete the model when the answer is deleted', () => {
            const answer = frame.addSelectableAnswer().addDefaultReferences();
            model.answer = answer;
            jest.spyOn(model, 'remove').mockImplementation(() => {});
            model.answer.remove();
            expect(model.remove).toHaveBeenCalled();
        });
    });

    function getModel(options) {
        frame = Componentized.fixtures.getInstance();
        frame.createFrameViewModel();
        return frame.addSimilarToSelectableAnswer(options);
    }
});
