import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/componentized_fixtures';
import 'Editor/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import answerListButtonsLocales from 'Lessons/locales/lessons/lesson/frame_list/frame/componentized/component/answer_list/answer_list_buttons-en.json';
import answerListVennDiagramLocales from 'Lessons/locales/lessons/lesson/frame_list/frame/componentized/component/answer_list/answer_list_venn_diagram-en.json';

setSpecLocales(answerListButtonsLocales);
setSpecLocales(answerListVennDiagramLocales);

describe('Componentized.Component.AnswerList', () => {
    let $injector;
    let frame;
    let answerListViewModel;
    let AnswerListModel;
    let Componentized;
    let SpecHelper;
    let challengeViewModel;
    let frameViewModel;
    let MaxTextLengthConfig;
    let AnswerButtonsDirHelper;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                AnswerListModel = $injector.get(
                    'Lesson.FrameList.Frame.Componentized.Component.AnswerList.AnswerListModel',
                );
                Componentized = $injector.get('Lesson.FrameList.Frame.Componentized');
                SpecHelper = $injector.get('SpecHelper');
                $injector.get('ComponentizedFixtures');
                answerListViewModel = getAnswerListViewModel();
                MaxTextLengthConfig = $injector.get('MaxTextLengthConfig');
                AnswerButtonsDirHelper = $injector.get(
                    'Lesson.FrameList.Frame.Componentized.Component.AnswerList.AnswerButtonsDirHelper',
                );
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('Model', () => {
        describe('useSingleColumn', () => {
            it('should be true if force_single_column', () => {
                const model = answerListViewModel.model;
                model.force_single_column = true;
                expect(model.useSingleColumn()).toBe(true);
            });

            it('should be true if an image has a large aspect ratio', () => {
                const model = answerListViewModel.model;
                model.force_single_column = undefined;
                model.answers[0].image = model.frame().addImage();
                jest.spyOn(model.answers[0].image, 'aspectRatio', 'get').mockReturnValue(2.6);
                expect(model.useSingleColumn()).toBe(true);
            });

            it('should be false even if an image has a large aspect ratio if force_single_column is false', () => {
                const model = answerListViewModel.model;
                model.force_single_column = false;
                model.answers[0].image = model.frame().addImage();
                jest.spyOn(model.answers[0].image, 'aspectRatio', 'get').mockReturnValue(2.6);
                expect(model.useSingleColumn()).toBe(false);
            });

            it('should be false if force_single_column is undefined and no image has a large aspect ratio', () => {
                const model = answerListViewModel.model;
                model.force_single_column = undefined;
                model.answers[0].image = model.frame().addImage();
                jest.spyOn(model.answers[0].image, 'aspectRatio', 'get').mockReturnValue(2.4);
                expect(model.useSingleColumn()).toBe(false);
            });
        });
    });

    describe('ViewModel', () => {
        describe('initialize', () => {
            it('should set the background color', () => {
                const viewModel = getAnswerListViewModel({
                    skin: 'poll',
                });
                expect(viewModel.frameViewModel.bodyBackgroundColor).toBe('blue');
            });
        });

        describe('unselectAnswers', () => {
            it('should unselect answers', () => {
                const answerViewModel = answerListViewModel.answersViewModels[0];
                answerViewModel.selected = true;
                answerListViewModel.unselectAnswers();
                SpecHelper.expectEqual(
                    false,
                    answerViewModel.selected,
                    'answerViewModel unselected after calling unselectAnswers',
                );
            });
        });

        describe('clearStates', () => {
            it('should clear states on answers', () => {
                const answerViewModel = answerListViewModel.answersViewModels[0];
                answerViewModel.selected = true;
                jest.spyOn(answerViewModel, 'resetCssClasses').mockImplementation(() => {});
                answerListViewModel.clearStates();

                SpecHelper.expectEqual(
                    false,
                    answerViewModel.selected,
                    'answerViewModel unselected after calling clearStates',
                );
                expect(answerViewModel.resetCssClasses).toHaveBeenCalled();
            });
        });

        describe('currentChallengeViewModel', () => {
            describe('set', () => {
                it('should work and fire an event', () => {
                    const callback = jest.fn();
                    answerListViewModel.on('set:currentChallengeViewModel', callback);
                    challengeViewModel.active = false;
                    expect(callback).toHaveBeenCalledWith(undefined);
                    expect(answerListViewModel.currentChallengeViewModel).toBeUndefined();
                    challengeViewModel.active = true;
                    expect(callback).toHaveBeenCalledWith(challengeViewModel);
                    expect(answerListViewModel.currentChallengeViewModel).toBe(challengeViewModel);
                });
                it('should not fire if value does not change', () => {
                    const callback = jest.fn();
                    answerListViewModel.on('set:currentChallengeViewModel', callback);
                    challengeViewModel.active = true;
                    expect(callback).not.toHaveBeenCalled();
                });
                it('should set up a validated listener', () => {
                    const callback = jest.fn();
                    answerListViewModel.on('validated', callback);
                    challengeViewModel.fire('validated');
                    expect(callback).toHaveBeenCalled();
                });
                it('should clear old validated listener when changed', () => {
                    const callback = jest.fn();
                    answerListViewModel.on('validated', callback);
                    challengeViewModel.active = false;
                    challengeViewModel.fire('validated');
                    expect(callback).not.toHaveBeenCalled();
                });
            });
        });

        describe('getAnswerViewModelIndex', () => {
            it('should return the index for a given Answer ViewModel', () => {
                const answerViewModel = answerListViewModel.orderedAnswerViewModels[0];
                const answerIndex = answerListViewModel.getAnswerViewModelIndex(answerViewModel);
                SpecHelper.expectEqual(0, answerIndex, 'answerViewModel index via getAnswerViewModelIndex');
            });
        });

        describe('selectAnswerByIndex', () => {
            it('should select the given answer index', () => {
                const answerViewModel = answerListViewModel.orderedAnswerViewModels[0];
                SpecHelper.expectEqual(false, answerViewModel.selected, 'answerViewModel unselected on start'); // sanity check
                answerListViewModel.selectAnswerByIndex(0);
                SpecHelper.expectEqual(
                    true,
                    answerViewModel.selected,
                    'answerViewModel unselected after selectAnswerByIndex',
                );
            });
        });

        describe('shouldShowAnswerMessageIcons', () => {
            beforeEach(() => {
                addAnswerMessage();
                Object.defineProperty(frameViewModel, 'editorMode', {
                    get() {
                        return true;
                    },
                });
            });

            it('should respect editor message hinting preferences', () => {
                const ClientStorage = $injector.get('ClientStorage');
                const answerViewModel = answerListViewModel.orderedAnswerViewModels[0];
                expect(answerListViewModel.shouldShowAnswerMessageIcons(answerViewModel)).toBe(true);
                ClientStorage.setItem('showEditorMessageIcons', true);
                expect(answerListViewModel.shouldShowAnswerMessageIcons(answerViewModel)).toBe(true);
            });
        });
    });

    describe('behaviors', () => {
        describe('RandomizeAnswerOrder', () => {
            it('should randomize whenever an answer is added or removed', () => {
                let i = 0;
                jest.spyOn(_, 'shuffle').mockImplementation(() => `shuffled ${i++}`);
                answerListViewModel = getAnswerListViewModel({
                    behaviors: {
                        RandomizeAnswerOrder: {},
                    },
                });
                // this will have been shuffled once each time a challenge was added
                expect(answerListViewModel.orderedAnswerViewModels).toEqual(
                    'shuffled 3',
                    'Unexpected value for orderedAnswerViewModels',
                );
                const answerListEditorViewModel = AnswerListModel.EditorViewModel.addComponentTo(frame).setup();
                answerListViewModel.model.answers.push(answerListEditorViewModel.model);
                expect(answerListViewModel.orderedAnswerViewModels).toEqual(
                    'shuffled 4',
                    'Unexpected value for orderedAnswerViewModels',
                );
                answerListViewModel.model.answers.remove(answerListEditorViewModel.model);
                expect(answerListViewModel.orderedAnswerViewModels).toEqual(
                    'shuffled 5',
                    'Unexpected value for orderedAnswerViewModels',
                );
            });

            it('should reset answer order when turned off', () => {
                jest.spyOn(_, 'shuffle').mockImplementation(() => 'shuffled');
                answerListViewModel = getAnswerListViewModel({
                    behaviors: {
                        RandomizeAnswerOrder: {},
                    },
                });
                // this will have been shuffled once each time a challenge was added
                expect(answerListViewModel.orderedAnswerViewModels).toEqual(
                    'shuffled',
                    'Unexpected value for orderedAnswerViewModels',
                );
                answerListViewModel.model.behaviors.RandomizeChallengeOrder = undefined;
                expect(answerListViewModel.orderedAnswerViewModels).toEqual(
                    answerListViewModel.orderedAnswerViewModels,
                    'Unexpected value for orderedAnswerViewModels',
                );
            });

            it('should respect ensureLast option', () => {
                /*
                    on the first shuffle, return the answer view models in their
                    regular order.  On the second one, move the first one to the end.
                */
                jest.spyOn(_, 'shuffle').mockImplementation(arr => arr.slice(0));

                // require that the first answer in the list be
                // the last one after shuffling
                answerListViewModel.model.behaviors.RandomizeAnswerOrder = {
                    ensureLast: [answerListViewModel.model.answers[0].id, answerListViewModel.model.answers[1].id],
                };

                const answerVMs = answerListViewModel.answersViewModels;
                // first, shuffle should have been called on the ensureLast array
                expect(_.shuffle).toHaveBeenCalledWith([answerVMs[0].model.id, answerVMs[1].model.id]);
                // then, shuffle should have been called on all the answer view models
                // other than the first one, since that one was chosen to be last
                expect(_.shuffle).toHaveBeenCalledWith(answerVMs.slice(1));

                // and now the result should be the view models all shuffled, with
                // the one that was selected to be last at the end
                expect(answerListViewModel.orderedAnswerViewModels).toEqual(answerVMs.slice(1).concat([answerVMs[0]]));
            });

            it('should respect disallowedAnswerOrder', () => {
                answerListViewModel = getAnswerListViewModel({
                    behaviors: {
                        RandomizeAnswerOrder: {},
                    },
                });

                answerListViewModel.disallowedAnswerOrder = answerListViewModel.answersViewModels.slice(0);

                /*
                    on the first shuffle, return the answer view models in their
                    regular order.  On the second one, move the first one to the end.
                */
                let i = 0;
                let theFirstShallBeLast;
                jest.spyOn(_, 'shuffle').mockImplementation(arr => {
                    if (i === 0) {
                        i += 1;
                        return arr;
                    }
                    // the first shall be last
                    theFirstShallBeLast = arr.slice(1, arr.length).concat([arr[0]]);
                    return theFirstShallBeLast;
                });

                // this is hackish, but it triggers a single re-order, which is hard to do
                // otherwise
                answerListViewModel.model.triggerCallbacks('childAdded_answers');
                expect(_.shuffle.mock.calls.length).toBe(2);
                expect(answerListViewModel.orderedAnswerViewModels.map(vm => vm.model.id)).toEqual(
                    theFirstShallBeLast.map(vm => vm.model.id),
                );
            });
        });
    });

    describe('cf-answer-list directive', () => {
        let elem;
        let scope;

        it('should disable buttons when there is no active challenge', () => {
            render();
            answerListViewModel.currentChallengeViewModel.active = false;
            scope.$digest();
            const buttons = SpecHelper.expectElements(elem, 'button', answerListViewModel.answersViewModels.length);
            angular.forEach(buttons, button => {
                expect($(button).prop('disabled')).toBe(true);
            });
        });

        it('should disable buttons when active challenge is complete', () => {
            render();
            answerListViewModel.currentChallengeViewModel.complete = true;
            scope.$digest();
            const buttons = SpecHelper.expectElements(elem, 'button', answerListViewModel.answersViewModels.length);
            angular.forEach(buttons, button => {
                expect($(button).prop('disabled')).toBe(true);
            });
        });

        describe('buttons renderer', () => {
            it('should add a click handler to each button', () => {
                render({
                    skin: 'buttons',
                });
                const answerViewModel = answerListViewModel.answersViewModels[0];
                jest.spyOn(answerListViewModel.currentChallengeViewModel, 'toggleAnswer').mockImplementation(() => {});
                SpecHelper.click(elem, `button[component-id="${answerViewModel.model.id}"]`);
                expect(answerListViewModel.currentChallengeViewModel.toggleAnswer).toHaveBeenCalledWith(
                    answerViewModel,
                );
            });

            it('should support key clicks', () => {
                render({
                    skin: 'buttons',
                });
                const answerViewModel = answerListViewModel.answersViewModels[0];
                jest.spyOn(answerListViewModel.currentChallengeViewModel, 'toggleAnswer').mockImplementation(() => {});
                SpecHelper.clickKey('1');
                expect(answerListViewModel.currentChallengeViewModel.toggleAnswer).toHaveBeenCalledWith(
                    answerViewModel,
                );
            });

            it('should respect force_single_column', () => {
                render({
                    skin: 'buttons',
                });
                SpecHelper.expectNoElement(elem, '.single_column');
                answerListViewModel.model.force_single_column = true;
                scope.$digest();
                SpecHelper.expectElement(elem, '.single_column');
            });

            it('should load up extra-large images in single column mode', () => {
                const viewModel = getAnswerListViewModel();
                const image = (viewModel.model.answers[0].image = viewModel.frame.addImage());
                jest.spyOn(viewModel.model, 'useSingleColumn').mockReturnValue(true);
                jest.spyOn(AnswerButtonsDirHelper, '_getButtonStyling').mockReturnValue({
                    buttonWrapperClasses: ['small_image_buttons', 'single_column'],
                });
                jest.spyOn(image, 'urlForContext').mockImplementation(() => {});
                render(viewModel);
                expect(image.urlForContext).toHaveBeenCalledWith('wideAnswerButton');
            });

            it('should load up regular-sized images in two column mode', () => {
                const viewModel = getAnswerListViewModel();
                const image = (viewModel.model.answers[0].image = viewModel.frame.addImage());
                jest.spyOn(AnswerButtonsDirHelper, '_getButtonStyling').mockReturnValue({
                    buttonWrapperClasses: ['small_image_buttons'],
                    useSingleColumn: false,
                });
                jest.spyOn(image, 'urlForContext').mockImplementation(() => {});
                render(viewModel);
                expect(image.urlForContext).toHaveBeenCalledWith('answerButton');
            });

            it('should support image buttons', () => {
                const frontRoyalStore = $injector.get('frontRoyalStore');
                const $q = $injector.get('$q');
                const dataUrl = 'some:data:url';
                jest.spyOn(frontRoyalStore, 'getDataUrlIfStored').mockReturnValue($q.resolve(dataUrl));
                render({
                    skin: 'buttons',
                });
                const answerViewModel = answerListViewModel.answersViewModels[0];
                answerViewModel.model.image = frame.addImage();
                const imageEl = SpecHelper.expectElement(
                    elem,
                    `[component-id="${answerViewModel.model.id}"] .button_image`,
                );
                scope.$digest();
                expect(imageEl.css('backgroundImage')).toBe(`url(${dataUrl})`);
                expect(frontRoyalStore.getDataUrlIfStored).toHaveBeenCalledWith(
                    answerViewModel.model.image.urlForContext('answerButton'),
                );
            });
        });

        describe('buttons skin', () => {
            it('should render a button for each answer with the appropriate css class', () => {
                render({
                    skin: 'buttons',
                });
                SpecHelper.expectEqual(
                    answerListViewModel.answersViewModels.length,
                    elem.find('button.button').length,
                    'buttons',
                );
            });
        });

        describe('checkboxes skin', () => {
            it('should render a checkbox for each answer with the appropriate css class', () => {
                render({
                    skin: 'checkboxes',
                });
                SpecHelper.expectEqual(
                    answerListViewModel.answersViewModels.length,
                    elem.find('button.checkbox-button').length,
                    'checkboxes',
                );
            });
        });

        describe('venn_diagram renderer', () => {
            it('should add a click handler to each venn diagram button', () => {
                render({
                    skin: 'venn_diagram',
                });
                const answerViewModel = answerListViewModel.answersViewModels[0];
                jest.spyOn(answerListViewModel.currentChallengeViewModel, 'toggleAnswer').mockImplementation(() => {});
                SpecHelper.click(elem, 'button[component-id="0"]');
                expect(answerListViewModel.currentChallengeViewModel.toggleAnswer).toHaveBeenCalledWith(
                    answerViewModel,
                );
            });

            it('should support key clicks', () => {
                render({
                    skin: 'venn_diagram',
                });
                const answerViewModel = answerListViewModel.answersViewModels[0];
                jest.spyOn(answerListViewModel.currentChallengeViewModel, 'toggleAnswer').mockImplementation(() => {});
                SpecHelper.clickKey('1');
                expect(answerListViewModel.currentChallengeViewModel.toggleAnswer).toHaveBeenCalledWith(
                    answerViewModel,
                );
            });
        });

        describe('image_hotspot renderer', () => {
            it('should add a click handler to each button', () => {
                render({
                    skin: 'image_hotspot',
                });
                const answerViewModel = answerListViewModel.answersViewModels[0];
                jest.spyOn(answerListViewModel.currentChallengeViewModel, 'toggleAnswer').mockImplementation(() => {});
                SpecHelper.click(elem, `button[component-id="${answerViewModel.model.id}"]`);
                expect(answerListViewModel.currentChallengeViewModel.toggleAnswer).toHaveBeenCalledWith(
                    answerViewModel,
                );
            });

            it('should support key clicks', () => {
                render({
                    skin: 'image_hotspot',
                });
                const answerViewModel = answerListViewModel.answersViewModels[0];
                jest.spyOn(answerListViewModel.currentChallengeViewModel, 'toggleAnswer').mockImplementation(() => {});
                SpecHelper.clickKey('1');
                expect(answerListViewModel.currentChallengeViewModel.toggleAnswer).toHaveBeenCalledWith(
                    answerViewModel,
                );
            });

            it('should add no_image class', () => {
                render({
                    skin: 'image_hotspot',
                });
                expect(elem.find('button').get(0).classList.contains('no_image')).toBe(true);
                const answer = answerListViewModel.model.answers[0];
                answer.image = frame.addImage();
                scope.$digest();
                expect(elem.find('button').get(0).classList.contains('no_image')).toBe(false);
            });

            it('should add has_background class', () => {
                render({
                    skin: 'image_hotspot',
                });
                expect(elem.find('button').get(0).classList.contains('has_background')).toBe(false);
                const answer = answerListViewModel.model.answers[0];
                answer.hasBackground = true;
                scope.$digest();
                expect(elem.find('button').get(0).classList.contains('has_background')).toBe(true);
            });

            it('should add click-target class', () => {
                render({
                    skin: 'image_hotspot',
                });
                expect(elem.find('button > div').get(0).classList.contains('click-target')).toBe(true);
            });
        });

        describe('poll skin', () => {
            beforeEach(() => {
                render({
                    skin: 'poll',
                });
                const scope = elem.find('cf-answer-list-poll').scope();
                jest.spyOn(scope, 'getBorderRadius').mockReturnValue('10px');
                jest.spyOn(scope, 'getButtonWidth').mockReturnValue(100);

                const usageReport = {
                    first_answer_counts: {},
                };
                expect(answerListViewModel.model.answers.length).toBe(4); // sanity check
                answerListViewModel.model.answers.forEach((answer, i) => {
                    usageReport.first_answer_counts[answer.id] = i;
                });
                setUsageReport(usageReport);
            });

            it('should fill up the fill bars when challenge is complete', () => {
                expect(elem.find('button .fill-bar').css('width')).toBe('20px');
                completeChallenge();
                scope.$digest();

                // since the first answer was selected, it should get 1 more than what is in the usage report
                expect(sanitizedWidth(elem.find('button .fill-bar:eq(0)').css('width'))).toBe(`${expectedWidth(1)}%`);
                expect(sanitizedWidth(elem.find('button .fill-bar:eq(1)').css('width'))).toBe(`${expectedWidth(1)}%`);
                expect(sanitizedWidth(elem.find('button .fill-bar:eq(2)').css('width'))).toBe(`${expectedWidth(2)}%`);
                expect(sanitizedWidth(elem.find('button .fill-bar:eq(3)').css('width'))).toBe(`${expectedWidth(3)}%`);
            });

            it('should change the index text when challenge is complete', () => {
                SpecHelper.expectElementText(elem, 'button index:eq(0)', 'A');
                completeChallenge();
                scope.$digest();
                // since the first answer was selected, it should get 1 more than what is in the usage report
                SpecHelper.expectElementText(elem, 'button index:eq(0)', `${Math.round(100 * expectedPerc(1))}%`);
            });

            it('should pad the data if there are fewer than 5 answers', () => {
                const answerId = answerListViewModel.model.answers[0].id;
                const usageReport = {
                    first_answer_counts: {},
                };
                usageReport.first_answer_counts[answerId] = 1;
                setUsageReport(usageReport);
                SpecHelper.expectElementText(elem, 'button index:eq(0)', 'A');
                completeChallenge();
                scope.$digest();
                // since the first answer was selected, it should get 1 more than what is in the usage report
                SpecHelper.expectElementText(elem, 'button index:eq(0)', `${Math.round(100 * expectedPerc(2, 2, 3))}%`);
                SpecHelper.expectElementText(elem, 'button index:eq(1)', `${Math.round(100 * expectedPerc(0, 2, 3))}%`);
            });

            it('should prevent a rounded total that is greater than 100 (2 choices)', () => {
                const answerId1 = answerListViewModel.model.answers[0].id;
                const answerId2 = answerListViewModel.model.answers[1].id;
                const usageReport = {
                    first_answer_counts: {},
                };
                usageReport.first_answer_counts[answerId1] = 984; // add 1 to this: 98.5% rounds to --> 99%
                usageReport.first_answer_counts[answerId2] = 15; // 1.5% --> 2%
                setUsageReport(usageReport);
                completeChallenge();
                scope.$digest();

                // ensure that we don't end up with 99% and 2% !
                SpecHelper.expectElementText(elem, 'button index:eq(0)', '99%');
                SpecHelper.expectElementText(elem, 'button index:eq(1)', '1%');
            });

            it('should prevent a rounded total that is greater than 100 (4 choices)', () => {
                const answerId1 = answerListViewModel.model.answers[0].id;
                const answerId2 = answerListViewModel.model.answers[1].id;
                const answerId3 = answerListViewModel.model.answers[2].id;
                const answerId4 = answerListViewModel.model.answers[3].id;
                const usageReport = {
                    first_answer_counts: {},
                };
                usageReport.first_answer_counts[answerId1] = 254; // add 1 to this: 25.5% rounds to --> 26%
                usageReport.first_answer_counts[answerId2] = 255; // 25.5% --> 26%
                usageReport.first_answer_counts[answerId3] = 255; // 25.5% --> 26%
                usageReport.first_answer_counts[answerId4] = 235; // 23.5% --> 24%
                setUsageReport(usageReport);
                completeChallenge();
                scope.$digest();

                // ensure that we don't end up with a total of 102% (simple rounding)
                SpecHelper.expectElementText(elem, 'button index:eq(0)', '26%');
                SpecHelper.expectElementText(elem, 'button index:eq(1)', '25%');
                SpecHelper.expectElementText(elem, 'button index:eq(2)', '26%');
                SpecHelper.expectElementText(elem, 'button index:eq(3)', '23%');
            });

            function completeChallenge() {
                Object.defineProperty(answerListViewModel.currentChallengeViewModel, 'challengeResponses', {
                    value: [
                        {
                            answer: answerListViewModel.model.answers[0],
                        },
                    ],
                });
                answerListViewModel.currentChallengeViewModel.complete = true;
            }

            function setUsageReport(usageReport) {
                answerListViewModel.currentChallengeViewModel.model.usage_report = usageReport;
                const scope = elem.find('cf-answer-list-poll').scope();

                // this is a bit of a hack, but we need to de-activate and re-activate
                // the currentChallenge in order to initialize the fillBarWidths
                const challengeViewModel = scope.viewModel.currentChallengeViewModel;
                challengeViewModel.active = false;
                scope.$digest();
                challengeViewModel.active = true;
                scope.$digest();
            }

            function expectedWidth(count) {
                const minFillWidth = 0.2; // 2*borderRadius / width
                const perc = expectedPerc(count);
                return Number(100 * (minFillWidth + 0.8 * perc)).toFixed(4);
            }

            function sanitizedWidth(cssWidth) {
                return `${Number(cssWidth.replace('%', '')).toFixed(4)}%`;
            }

            function expectedPerc(count, totalCount = 0 + 1 + 2 + 3 + 1, minCount = 0) {
                const perc = (minCount + count) / (4 * minCount + totalCount);
                return perc;
            }
        });

        function render(optionsOrViewModel) {
            if (optionsOrViewModel && optionsOrViewModel.type && optionsOrViewModel.type === 'AnswerListViewModel') {
                answerListViewModel = optionsOrViewModel;
            } else {
                answerListViewModel = getAnswerListViewModel(optionsOrViewModel);
            }
            const renderer = SpecHelper.renderer();
            renderer.scope.viewModel = answerListViewModel;
            renderer.render('<cf-ui-component view-model="viewModel"></cf-ui-component>');
            elem = renderer.elem;
            scope = renderer.scope;
        }
    });

    describe('EditorViewModel', () => {
        let answerListEditorViewModel;

        beforeEach(() => {
            answerListEditorViewModel = AnswerListModel.EditorViewModel.addComponentTo(frame).setup();
        });

        describe('setup', () => {
            it('should initialize answer_id array', () => {
                const editorViewModel = AnswerListModel.EditorViewModel.addComponentTo(frame).setup();
                SpecHelper.expectEqual(0, editorViewModel.model.answer_ids.length);
            });
        });
        describe('addAnswer', () => {
            it('should be able to add answer', () => {
                answerListEditorViewModel.model.answers = [];
                let answerEditorViewModel = answerListEditorViewModel.addAnswer();
                const firstAnswerId = answerEditorViewModel.model.id;
                SpecHelper.expectEqual('AnswerListModel', answerListEditorViewModel.model.type);
                SpecHelper.expectEqual([firstAnswerId], answerListEditorViewModel.model.answer_ids);

                answerEditorViewModel = answerListEditorViewModel.addAnswer();
                const secondAnswerId = answerEditorViewModel.model.id;
                SpecHelper.expectEqual('AnswerListModel', answerListEditorViewModel.model.type);
                SpecHelper.expectEqual([firstAnswerId, secondAnswerId], answerListEditorViewModel.model.answer_ids);
            });
        });
        describe('delete answer', () => {
            it('should be able to delete answer', () => {
                answerListEditorViewModel.model.answers = [];
                const firstAnswerEditorViewModel = answerListEditorViewModel.addAnswer();
                const firstAnswerId = firstAnswerEditorViewModel.model.id;
                SpecHelper.expectEqual('AnswerListModel', answerListEditorViewModel.model.type);
                SpecHelper.expectEqual([firstAnswerId], answerListEditorViewModel.model.answer_ids);

                const secondAnswerEditorViewModel = answerListEditorViewModel.addAnswer();
                const secondAnswerId = secondAnswerEditorViewModel.model.id;
                SpecHelper.expectEqual('AnswerListModel', answerListEditorViewModel.model.type);
                SpecHelper.expectEqual([firstAnswerId, secondAnswerId], answerListEditorViewModel.model.answer_ids);

                secondAnswerEditorViewModel.remove();
                SpecHelper.expectEqual([firstAnswerId], answerListEditorViewModel.model.answer_ids);
                firstAnswerEditorViewModel.remove();
                SpecHelper.expectEqual([], answerListEditorViewModel.model.answer_ids);
            });
        });
        describe('trimAnswers', () => {
            it('should prune the answerlist to the specified length', () => {
                answerListEditorViewModel.model.answers = [];

                const answerViewModels = [
                    answerListEditorViewModel.addAnswer(),
                    answerListEditorViewModel.addAnswer(),
                    answerListEditorViewModel.addAnswer(),
                ];

                SpecHelper.expectEqual('AnswerListModel', answerListEditorViewModel.model.type);
                SpecHelper.expectEqual(
                    [answerViewModels[0].model.id, answerViewModels[1].model.id, answerViewModels[2].model.id],
                    answerListEditorViewModel.model.answer_ids,
                );

                answerListEditorViewModel.trimAnswers(2);

                SpecHelper.expectEqual(
                    [answerViewModels[0].model.id, answerViewModels[1].model.id],
                    answerListEditorViewModel.model.answer_ids,
                );
            });
        });
        describe('MaximumTextLength on vennDiagramHeaders', () => {
            it('should be MaxTextLength.VENN_DIAGRAM_HEADER', () => {
                answerListEditorViewModel.model.vennDiagramHeaders = [];
                const header = frame.addText();
                answerListEditorViewModel.model.vennDiagramHeaders.push(header);
                expect(header.editorViewModel.maxRecommendedTextLength()).toBe(MaxTextLengthConfig.VENN_DIAGRAM_HEADER);
            });
        });
        describe('setUserDefinedOptions', () => {
            // basic functionality is tested in the editor directive test below
            it('should allow for sending override options', () => {
                answerListEditorViewModel.setUserDefinedOptions('RandomizeAnswerOrder', {
                    RandomizeAnswerOrder: {
                        override: true,
                    },
                });
                SpecHelper.expectEqualObjects(true, answerListEditorViewModel.config.userDefinedOptions[0].override);
            });
        });
    });

    describe('cf-answer-list-editor', () => {
        let elem;
        let scope;
        let editorViewModel;
        let answerListViewModel;

        it('should have move buttons', () => {
            render();
            editorViewModel.model.answers = [];
            editorViewModel.addAnswer();
            scope.$digest();

            SpecHelper.expectElement(elem, 'button[name="moveUp"]');
            SpecHelper.expectElement(elem, 'button[name="moveDown"]');
        });

        it('should allow for setting forceSingleColumn if available', () => {
            render();
            editorViewModel.setUserDefinedOptions('forceSingleColumn');
            scope.$digest();
            SpecHelper.toggleRadio(elem, '.cf-user-defined-options input', 0);
            SpecHelper.expectEqual(undefined, editorViewModel.model.force_single_column, 'force_single_column');
            SpecHelper.toggleRadio(elem, '.cf-user-defined-options input', 1);
            SpecHelper.expectEqual(true, editorViewModel.model.force_single_column, 'force_single_column');
            SpecHelper.toggleRadio(elem, '.cf-user-defined-options input', 2);
            SpecHelper.expectEqual(false, editorViewModel.model.force_single_column, 'force_single_column');
        });

        it('should allow for setting RandomizeAnswerOrder if available', () => {
            render();
            editorViewModel.setUserDefinedOptions('RandomizeAnswerOrder');
            scope.$digest();
            SpecHelper.checkCheckbox(elem, '.cf-user-defined-options input');
            SpecHelper.expectEqual(
                true,
                editorViewModel.model.includesBehavior('RandomizeAnswerOrder'),
                "includesBehavior('RandomizeAnswerOrder')",
            );
            SpecHelper.uncheckCheckbox(elem, '.cf-user-defined-options input');
            SpecHelper.expectEqual(
                false,
                editorViewModel.model.includesBehavior('RandomizeAnswerOrder'),
                "includesBehavior('RandomizeAnswerOrder')",
            );
        });

        it('should highlight the correct answer for the current active challenge', () => {
            render();
            challengeViewModel.active = false;
            scope.$digest();
            SpecHelper.expectNoElement(elem, '.correct-answer');
            challengeViewModel.active = true;
            const correctAnswer = challengeViewModel.model.editorViewModel.correctAnswer;
            scope.$digest();
            const highlightedElement = SpecHelper.expectElement(elem, '.correct-answer');
            // check that the correct answer is highlighted
            SpecHelper.expectElement(highlightedElement, `[component-id="${correctAnswer.id}"]`);
        });

        it('should pass disallowSwitchContentTypeOnAnswers down to answer editors', () => {
            render();
            editorViewModel.disallowSwitchContentTypeOnAnswers = 'yup!';
            scope.$digest();
            expect(elem.find('cf-selectable-answer-editor').scope().options.disallowSwitchContentType).toBe('yup!');
        });

        function render(options) {
            const renderer = SpecHelper.renderer();
            answerListViewModel = renderer.scope.viewModel = getAnswerListViewModel(options);
            editorViewModel = renderer.scope.answerListEditorViewModel = answerListViewModel.model.editorViewModel;
            renderer.scope.frameViewModel = frameViewModel;
            renderer.render(
                '<cf-answer-list-editor editor-view-model="answerListEditorViewModel" frame-view-model="frameViewModel"><cf-answer-list-editor>',
            );
            elem = renderer.elem;
            scope = renderer.scope;
        }
    });

    describe('AnswerButtonsDirHelper', () => {
        let imageButton;
        let wideImageButton;
        beforeEach(() => {
            imageButton = {
                contentType: 'image',
                image: {
                    aspectRatio: 1,
                },
            };
            wideImageButton = {
                contentType: 'image',
                image: {
                    aspectRatio: 10,
                },
            };
        });

        describe('buttonWrapperClasses', () => {
            describe('with text buttons', () => {
                it('should use large class when there are 4 or fewer buttons', () => {
                    const classes = AnswerButtonsDirHelper._getButtonStyling([{}, {}, {}, {}], undefined)
                        .buttonWrapperClasses;
                    expect(classes).toEqual(['tall_buttons']);
                });

                it('should use small class when there are 5 or more buttons', () => {
                    const classes = AnswerButtonsDirHelper._getButtonStyling([{}, {}, {}, {}, {}], undefined)
                        .buttonWrapperClasses;
                    expect(classes).toEqual(['short_buttons']);
                });

                describe('with singleColumn=true', () => {
                    it('should use large class when there are 2 or fewer buttons', () => {
                        const classes = AnswerButtonsDirHelper._getButtonStyling([{}, {}], true).buttonWrapperClasses;
                        expect(classes).toEqual(['tall_buttons', 'single_column']);
                    });

                    it('should use small class when there are 3 or more buttons', () => {
                        const classes = AnswerButtonsDirHelper._getButtonStyling([{}, {}, {}], true)
                            .buttonWrapperClasses;
                        expect(classes).toEqual(['short_buttons', 'single_column']);
                    });
                });
            });

            describe('with image buttons', () => {
                it('should use large class when there are 4 or fewer buttons', () => {
                    const classes = AnswerButtonsDirHelper._getButtonStyling([imageButton, {}, {}, {}], undefined)
                        .buttonWrapperClasses;
                    expect(classes).toEqual(['tall_image_buttons']);
                });

                it('should use small class when there are 5 or more buttons', () => {
                    const classes = AnswerButtonsDirHelper._getButtonStyling([imageButton, {}, {}, {}, {}], undefined)
                        .buttonWrapperClasses;
                    expect(classes).toEqual(['short_image_buttons']);
                });

                describe('with singleColumn=true', () => {
                    it('should use large class when there are 2 or fewer buttons', () => {
                        const classes = AnswerButtonsDirHelper._getButtonStyling([imageButton, {}], true)
                            .buttonWrapperClasses;
                        expect(classes).toEqual(['tall_image_buttons', 'single_column']);
                    });

                    it('should use small class when there are 3 or more buttons', () => {
                        const classes = AnswerButtonsDirHelper._getButtonStyling([imageButton, {}, {}], true)
                            .buttonWrapperClasses;
                        expect(classes).toEqual(['short_image_buttons', 'single_column']);
                    });
                });

                describe('with wide images', () => {
                    it('should use large class when there are 2 or fewer buttons in singleColumn mode', () => {
                        const classes = AnswerButtonsDirHelper._getButtonStyling([wideImageButton, {}], true)
                            .buttonWrapperClasses;
                        expect(classes).toEqual(['tall_image_buttons', 'single_column']);
                    });

                    it('should use small class when there are 3 or more buttons in singleColumn mode', () => {
                        const classes = AnswerButtonsDirHelper._getButtonStyling([wideImageButton, {}, {}], true)
                            .buttonWrapperClasses;
                        expect(classes).toEqual(['short_image_buttons', 'single_column']);
                    });

                    it('should use small class when there are 4 or fewer buttons in multiColumn mode', () => {
                        const classes = AnswerButtonsDirHelper._getButtonStyling([wideImageButton, {}, {}, {}], false)
                            .buttonWrapperClasses;
                        expect(classes).toEqual(['tall_image_buttons']);
                    });

                    it('should use small class when there are 5 or more buttons in singleColumn mode', () => {
                        const classes = AnswerButtonsDirHelper._getButtonStyling(
                            [wideImageButton, {}, {}, {}, {}],
                            false,
                        ).buttonWrapperClasses;
                        expect(classes).toEqual(['short_image_buttons']);
                    });
                });
            });
        });
    });

    function getAnswerListViewModel(options) {
        const answerListModel = getAnswerListModel(options);
        frameViewModel = frame.createFrameViewModel();

        // The answer list directive relies on having an active challenge.  So
        // activate the associated challenge
        const challengeModel = frame.getModelsByType('MultipleChoiceChallengeModel')[0];
        challengeViewModel = frameViewModel.viewModelFor(challengeModel);
        challengeViewModel.active = true;
        return frameViewModel.viewModelFor(answerListModel);
    }

    function getAnswerListModel(options) {
        frame = Componentized.fixtures.getInstance();
        const challenge = frame.addMultipleChoiceChallenge().addDefaultReferences();
        angular.extend(challenge.answerList, options);
        const models = frame.getModelsByType('AnswerListModel');
        SpecHelper.expectEqual(1, models.length, 'AnswerListModels count');

        return models[0];
    }

    function addAnswerMessage() {
        const answerViewModel = answerListViewModel.orderedAnswerViewModels[0];
        const challengeEditorViewModel = answerListViewModel.currentChallengeViewModel.model.editorViewModel;
        challengeEditorViewModel.messageEvent = 'validated';
        const message = challengeEditorViewModel.addMessageFor(answerViewModel.model).model;
        message.messageText.text = 'answer message';
        message.messageText.formatted_text = '<p>answer message</p>';
        expect(challengeEditorViewModel.messageComponentFor(answerViewModel.model, 'validated')).toBe(message);
        return message;
    }
});
