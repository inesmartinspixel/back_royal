import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/componentized_fixtures';
import 'Editor/angularModule';

describe('Componentized.Component.MatchingChallengeButton', () => {
    let $injector;
    let frame;
    let frameViewModel;
    let viewModel;
    let Componentized;
    let SpecHelper;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector => {
                $injector = _$injector;
                Componentized = $injector.get('Lesson.FrameList.Frame.Componentized');
                SpecHelper = $injector.get('SpecHelper');
                $injector.get('ComponentizedFixtures');
                viewModel = getViewModel();
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    // describe('Model', () => {});

    // describe('ViewModel', () => {});

    describe('cf-matching-challenge-button', () => {
        let elem;
        let scope;

        it('should support text', () => {
            render();
            viewModel.model.text.editorViewModel.formatText();
            scope.$digest();
            SpecHelper.expectElementText(elem, '.button_label', viewModel.model.text.formatted_text);
        });

        it('should support an image', () => {
            const frontRoyalStore = $injector.get('frontRoyalStore');
            const $q = $injector.get('$q');
            const dataUrl = 'some:data:url';
            jest.spyOn(frontRoyalStore, 'getDataUrlIfStored').mockReturnValue($q.resolve(dataUrl));
            viewModel.model.image = frame.addImage();
            render();
            const button = SpecHelper.expectElement(elem, 'button');
            expect(button.find('.button_image').css('backgroundImage')).toBe(`url(${dataUrl})`);
            expect(frontRoyalStore.getDataUrlIfStored).toHaveBeenCalledWith(
                viewModel.model.image.urlForContext('answerButton'),
            );
        });

        it('should activate the challenge on click', () => {
            render();
            viewModel.challengeViewModel.active = false;
            SpecHelper.click(elem, 'button');
            expect(viewModel.challengeViewModel.active).toBe(true);
        });

        function render() {
            const renderer = SpecHelper.renderer();
            renderer.scope.viewModel = viewModel;
            renderer.render('<cf-ui-component view-model="viewModel"></cf-ui-component>');
            elem = renderer.elem;
            scope = renderer.scope;
        }
    });

    describe('cf-matching-challenge-button', () => {
        let elem;

        beforeEach(() => {
            const renderer = SpecHelper.renderer();
            renderer.scope.editorViewModel = frame.editorViewModelFor(viewModel.model);
            renderer.render('<cf-component-editor editor-view-model="editorViewModel"></cf-component-editor>');
            elem = renderer.elem;
        });

        it('should show text or image editor', () => {
            SpecHelper.expectElement(elem, '.cf-has-text-or-image-editor');
        });
    });

    // describe('EditorViewModel', () => {});

    function getViewModel(options) {
        const model = getModel(options);
        return frameViewModel.viewModelFor(model);
    }

    function getModel() {
        frame = Componentized.fixtures.getInstance();
        frameViewModel = frame.createFrameViewModel();
        return frame.addMatchingChallengeButton().addDefaultReferences();
    }
});
