import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/componentized_fixtures';
import 'Editor/angularModule';

describe('Componentized.Component.Challenge.UserInputChallenge', () => {
    let frame;
    let viewModel;
    let model;
    let Componentized;
    let SpecHelper;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'FrontRoyal.Editor', 'SpecHelper');
        angular.mock.inject([
            '$injector',
            $injector => {
                Componentized = $injector.get('Lesson.FrameList.Frame.Componentized');
                SpecHelper = $injector.get('SpecHelper');
                $injector.get('ComponentizedFixtures');
                viewModel = getViewModel();
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('Model', () => {
        describe('correctAnswerText', () => {
            it('should return the text from the answerMatcher', () => {
                model.validator.addMatchesExpectedText({
                    expectedText: 'some text',
                });
                expect(model.correctAnswerText).toBe('some text');
            });
        });

        describe('caseSensitive', () => {
            it('should delegate to the answerMatcher', () => {
                expect(model.caseSensitive).toBe(false);

                model.caseSensitive = true;
                expect(model.caseSensitive).toBe(true);
                expect(model.validator.expectedAnswerMatchers[0].caseSensitive).toBe(true);

                model.caseSensitive = false;
                expect(model.caseSensitive).toBe(false);
                expect(model.validator.expectedAnswerMatchers[0].caseSensitive).toBe(false);
            });
        });

        describe('correctThreshold', () => {
            it('should delegate to the answerMatcher', () => {
                expect(model.correctThreshold).toBe(100);

                model.correctThreshold = 50;
                expect(model.correctThreshold).toBe(50);
                expect(model.validator.expectedAnswerMatchers[0].correctThreshold).toBe(50);
            });
        });
    });

    describe('ViewModel', () => {
        beforeEach(() => {
            SpecHelper.stubCurrentUser('learner');
        });

        describe('challengeResponses', () => {
            it('should create a ChallengeResponse with the text entered by the user', () => {
                viewModel.userAnswer = 'some text';
                const challengeResponses = viewModel.challengeResponses;
                expect(challengeResponses.length).toBe(1);
                const challengeResponse = challengeResponses[0];
                expect(challengeResponse.text).toBe('some text');
            });
        });

        it('should display incomplete when incomplete answer is entered', () => {
            viewModel.fire('validatedIncorrect', {
                partiallyCorrect: true,
                answerMatchingDetails: {
                    nextCharacterExpected: undefined,
                },
            });
            expect(viewModel.showingIncorrectStyling).toBe(false);
            expect(viewModel.showingIncompleteStyling).toBe(true);
        });

        it('should not display styling after answer is removed', () => {
            // show incorrect styling
            viewModel.fire('validatedIncorrect', {
                userInputAnswerText: 'a',
                partiallyCorrect: true,
                answerMatchingDetails: {
                    nextCharacterExpected: undefined,
                },
            });
            expect(viewModel.showingIncorrectStyling).toBe(false);
            expect(viewModel.showingIncompleteStyling).toBe(true);

            // incorrect styling should be removed when answer is removed
            viewModel.fire('validatedIncorrect', {
                userInputAnswerText: '',
                partiallyCorrect: false,
                answerMatchingDetails: {
                    nextCharacterExpected: undefined,
                },
            });
            expect(viewModel.showingIncorrectStyling).toBe(false);
            expect(viewModel.showingIncompleteStyling).toBe(false);

            // show incomplete styling
            viewModel.fire('validatedIncorrect', {
                userInputAnswerText: 'a',
                partiallyCorrect: false,
                answerMatchingDetails: {
                    nextCharacterExpected: undefined,
                },
            });
            expect(viewModel.showingIncorrectStyling).toBe(true);
            expect(viewModel.showingIncompleteStyling).toBe(false);

            // incomplete styling should be removed when answer is removed
            viewModel.fire('validatedIncorrect', {
                userInputAnswerText: '',
                partiallyCorrect: false,
                answerMatchingDetails: {
                    nextCharacterExpected: undefined,
                },
            });
            expect(viewModel.showingIncorrectStyling).toBe(false);
            expect(viewModel.showingIncompleteStyling).toBe(false);
        });

        it('should replace answer with userAnswerToDisplay from validator', () => {
            viewModel.fire('validatedIncorrect', {
                userAnswerToDisplay: 'oh yeah!',
                answerMatchingDetails: {
                    nextCharacterExpected: undefined,
                },
            });
            expect(viewModel.userAnswer).toBe('oh yeah!');
        });

        it('should hide messages on correct validation if this challenge has no message', () => {
            jest.spyOn(viewModel, 'playerViewModel', 'get').mockReturnValue({
                clearMessage: jest.fn(),
            });
            jest.spyOn(viewModel.model, 'hasMessage').mockReturnValue(false);
            viewModel.fire('validatedCorrect', {});
            expect(viewModel.playerViewModel.clearMessage).toHaveBeenCalled();
        });

        it('shouldnot  hide messages on correct validation if this challenge has a message', () => {
            jest.spyOn(viewModel, 'playerViewModel', 'get').mockReturnValue({
                clearMessage: jest.fn(),
            });
            jest.spyOn(viewModel.model, 'hasMessage').mockReturnValue(true);
            viewModel.fire('validatedCorrect', {});
            expect(viewModel.playerViewModel.clearMessage).not.toHaveBeenCalled();
        });

        describe('incremental hinting', () => {
            beforeEach(stubPlayerViewModelLocalStorage);

            it('should trim an invalid character from the end of the user answer', () => {
                viewModel.fire('validatedIncorrect', {
                    answerMatchingDetails: {
                        nextCharacterExpected: undefined,
                    },
                });

                const originalAnswer = (viewModel.userAnswer = 'some XXX');
                viewModel.incrementHint();
                expect(viewModel.userAnswer).toEqual(originalAnswer.slice(0, -1));
            });

            it('should append the next expected character to the end of a partially correct answer', () => {
                viewModel.fire('validatedIncorrect', {
                    partiallyCorrect: true,
                    answerMatchingDetails: {
                        nextCharacterExpected: 't',
                    },
                });

                viewModel.userAnswer = 'some ';
                viewModel.incrementHint();
                expect(viewModel.userAnswer).toEqual('some t');
            });

            it('should append the next expected character to the end of an empty, unvalidated answer', () => {
                viewModel.userAnswer = '';
                viewModel.incrementHint();
                expect(viewModel.userAnswer).toEqual('s');
            });

            function stubPlayerViewModelLocalStorage() {
                jest.spyOn(viewModel, 'playerViewModel', 'get').mockReturnValue({
                    getLessonClientStorage(keyParts) {
                        return this[JSON.stringify(keyParts)];
                    },
                    setLessonClientStorage(keyParts, val) {
                        this[JSON.stringify(keyParts)] = val;
                    },
                });
            }
        });

        describe('getScore', () => {
            beforeEach(stubReceivedHintStoredInClientStorage);

            it('should null if no correct or incorrect validation result', () => {
                expect(viewModel.getScore()).toBe(null);
            });

            it('should be 0 if a hint was requested', () => {
                viewModel.incrementHint();
                enterCorrectAnswer();
                expect(viewModel.getScore()).toEqual(0);
            });

            it('should be 0 if receivedHintStoredInClientStorage and testOrAssessment', () => {
                viewModel.receivedHintStoredInClientStorage = true;
                viewModel.playerViewModel.lesson.testOrAssessment = true;
                enterCorrectAnswer();
                expect(viewModel.getScore()).toEqual(0);
            });

            it('should be 1 if receivedHintStoredInClientStorage and !testOrAssessment', () => {
                viewModel.receivedHintStoredInClientStorage = true;
                viewModel.playerViewModel.lesson.testOrAssessment = false;
                enterCorrectAnswer();
                expect(viewModel.getScore()).toEqual(1);
            });

            it('should be 1 with correct validation result and no hint', () => {
                enterCorrectAnswer();
                expect(viewModel.getScore()).toEqual(1);
            });

            function stubReceivedHintStoredInClientStorage() {
                Object.defineProperty(viewModel, 'playerViewModel', {
                    value: {
                        lesson: {},
                        clearMessage: jest.fn(),
                        setChallengeScore: jest.fn(),
                    },
                });
                let receivedHintStoredInClientStorage;
                Object.defineProperty(viewModel, 'receivedHintStoredInClientStorage', {
                    get() {
                        return receivedHintStoredInClientStorage;
                    },
                    set(val) {
                        receivedHintStoredInClientStorage = val;
                    },
                });
            }

            function enterCorrectAnswer() {
                viewModel.userAnswer = viewModel.model.correctAnswerText;
                const validationResult = viewModel.validate();
                expect(validationResult.result).toBe(true); // sanity check
            }
        });

        describe('showsCorrectnessDuringInput', () => {
            it('should be false for a locale that supports an IME', () => {
                viewModel.model.lesson.locale = 'zh';
                expect(viewModel.showsCorrectnessDuringInput).toBe(false);
            });
            it('should be true for a locale that does not support an IME', () => {
                viewModel.model.lesson.locale = 'en';
                expect(viewModel.showsCorrectnessDuringInput).toBe(true);
            });
        });

        describe('userAnswer', () => {
            const hindiNumerals = ['٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩'];
            let editorViewModel;
            beforeEach(() => {
                editorViewModel = model.editorViewModel;
                editorViewModel.correctAnswerText = '٠١٢٣٤٥٦٧٨٩';
            });

            it('should convert arabic numerals to hindi on arabic lessons', () => {
                viewModel.model.lesson.locale = 'ar';

                // put each numeral twice to test the global replace
                viewModel.userAnswer = '01234567890123456789abc';
                expect(viewModel.userAnswer).toEqual(`${hindiNumerals.join('') + hindiNumerals.join('')}abc`);
            });

            it('should not convert arabic numerals to hindi on non-arabic lessons', () => {
                viewModel.model.lesson.locale = 'not_ar';
                viewModel.userAnswer = '0123456789abc';
                expect(viewModel.userAnswer).toEqual('0123456789abc');
            });

            it('should not convert arabic numerals to hindi when correct answer text does not contain Hindi numerals', () => {
                viewModel.model.lesson.locale = 'ar';
                editorViewModel.correctAnswerText = '01234';

                viewModel.userAnswer = '0123456789abc';
                expect(viewModel.userAnswer).toEqual('0123456789abc');
            });
        });
    });

    describe('EditorViewModel', () => {
        let editorViewModel;

        beforeEach(() => {
            editorViewModel = model.editorViewModel;
        });

        describe('correctAnswerText', () => {
            describe('get', () => {
                it('should return undefined with no answer matcher', () => {
                    model.validator.expectedAnswerMatchers = [];
                    expect(editorViewModel.correctAnswerText).toBeUndefined();
                });
                it('should return the text from the answerMatcher', () => {
                    model.validator.addMatchesExpectedText({
                        expectedText: 'some text',
                    });
                    expect(model.correctAnswerText).toBe('some text');
                });
            });

            describe('set', () => {
                it('should set the text on the answerMatcher', () => {
                    editorViewModel.correctAnswerText = 'something';
                    expect(model.validator.expectedAnswerMatchers.length).toBe(1);
                    const matcher = model.validator.expectedAnswerMatchers[0];
                    expect(matcher.type).toBe('MatchesExpectedTextModel');
                    expect(matcher.expectedText).toBe('something');
                });
            });
        });

        describe('addMessage', () => {
            it('should work', () => {
                const message = model.editorViewModel.addMessage().model;
                expect(message.challenge).toBe(model);
                expect(message.show_on_correct_answer).toBe(true);
                expect(model.messages).toEqual([message]);
            });
        });

        describe('removeTransN', () => {
            it('should work when nothing changes', () => {
                editorViewModel.correctAnswerText = '1234';
                editorViewModel.removeTransN();
                expect(model.unlink_blank_from_answer).not.toBe(true);
                expect(editorViewModel.correctAnswerText).toBe('1234');
            });

            it('should work when transn is removed', () => {
                editorViewModel.correctAnswerText = '\\transn{1.234}';
                editorViewModel.removeTransN();
                expect(model.unlink_blank_from_answer).toBe(true);
                expect(editorViewModel.correctAnswerText).toBe('1.234');
            });
        });
    });

    describe('cf-user-input-challenge-editor', () => {
        let elem;
        let model;

        function render() {
            const renderer = SpecHelper.renderer();
            renderer.scope.editorViewModel = frame.editorViewModelFor(viewModel.model);
            renderer.render('<cf-component-editor editor-view-model="editorViewModel" ></cf-component-editor>');
            elem = renderer.elem;
            model = viewModel.model;
        }

        it('should allow for locking and unlocking', () => {
            render();
            SpecHelper.expectElementDisabled(elem, '[name="correct_answer_text"]');
            SpecHelper.click(elem, '[name="unlink_blank_from_answer"]');
            expect(model.unlink_blank_from_answer).toBe(true);
            SpecHelper.expectElementEnabled(elem, '[name="correct_answer_text"]');
            SpecHelper.updateTextInput(elem, '[name="correct_answer_text"]', 'some other text');
            expect(model.correctAnswerText).toBe('some other text');
        });

        it('should support adding a message', () => {
            render();
            SpecHelper.click(elem, '[name="add_message"]', 0);
            SpecHelper.updateTextInput(elem, '.messages .input textarea', 'message');
            expect(viewModel.model.messages.length).toBe(1);
            viewModel.model.messages[0];
        });

        it('should support deleting a message', () => {
            render();
            SpecHelper.click(elem, '[name="add_message"]', 0);
            expect(viewModel.model.messages.length).toBe(1);
            SpecHelper.click(elem, '[name="remove_message"]', 0);
            expect(viewModel.model.messages.length).toBe(0);
        });
    });

    function getViewModel(options) {
        // For some reason here, it is iomportant to
        // create the frameViewModel first and then call viewModelFor,
        // rather than using model.createViewModel like we do in some other tests.
        const model = getModel(options);
        const frameViewModel = model.frame().createFrameViewModel();
        viewModel = frameViewModel.viewModelFor(model);
        return viewModel;
    }

    function getModel(options) {
        frame = Componentized.fixtures.getInstance();
        frame.addUserInputChallenge(options).addDefaultReferences();
        model = frame.getModelsByType('UserInputChallengeModel')[0];
        expect(model).not.toBeUndefined();
        return model;
    }
});
