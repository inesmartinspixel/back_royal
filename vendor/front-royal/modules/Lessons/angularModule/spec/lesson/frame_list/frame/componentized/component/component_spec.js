import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/componentized_fixtures';
import 'Editor/angularModule';

describe('Componentized.Component.Component', () => {
    let Componentized;
    let Component;
    let SpecHelper;
    let $injector;
    let frame;
    let frameViewModel;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                Componentized = $injector.get('Lesson.FrameList.Frame.Componentized');
                Component = $injector.get('Lesson.FrameList.Frame.Componentized.Component.ComponentModel');
                SpecHelper = $injector.get('SpecHelper');
                $injector.get('ComponentizedFixtures');
                frame = Componentized.fixtures.getInstance();
                frameViewModel = frame.createFrameViewModel();
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('Model', () => {
        describe('callbacks', () => {
            let model;
            beforeEach(() => {
                model = Component.new();
            });
            it('should be able to be turned on', () => {
                const callback = jest.fn();
                model.on('event', callback);
                expect(callback).not.toHaveBeenCalled();
                model.triggerCallbacks('event', 1, 2);
                expect(callback).toHaveBeenCalledWith(1, 2);
            });

            it('should be able to be turned off', () => {
                const callback = jest.fn();
                model.on('event', callback);
                model.off('event', callback);
                model.triggerCallbacks('event');
                expect(callback).not.toHaveBeenCalled();
            });

            it('should fire in the context of the instance', () => {
                const callback = jest.fn().mockImplementation(function () {
                    expect(this).toBe(model);
                });
                model.on('event', callback);
                model.triggerCallbacks('event');
                expect(callback).toHaveBeenCalled();
            });

            it('should return an object with a cancel method', () => {
                const callback = jest.fn();
                const listener = model.on('event', callback);
                listener.cancel();
                model.triggerCallbacks('event');
                expect(callback).not.toHaveBeenCalled();
            });

            it('should not mess things up if a callback removes itself', () => {
                const listeners = [];
                const callbacks = [
                    jest.fn(),
                    jest.fn().mockImplementation(() => {
                        listeners[1].cancel();
                    }),
                    jest.fn(),
                    jest.fn(),
                ];
                callbacks.forEach(callback => {
                    listeners.push(model.on('event', callback));
                });
                model.triggerCallbacks('event');
                model.triggerCallbacks('event');
                expect(callbacks[0].mock.calls.length).toBe(2);
                expect(callbacks[1].mock.calls.length).toBe(1);
                expect(callbacks[2].mock.calls.length).toBe(2);
                expect(callbacks[3].mock.calls.length).toBe(2);
            });

            it('should respect priority', () => {
                const callOrder = [];
                const callbacks = [
                    jest.fn().mockImplementation(() => {
                        callOrder.push(0);
                    }),
                    jest.fn().mockImplementation(() => {
                        callOrder.push(-1);
                    }),
                    jest.fn().mockImplementation(() => {
                        callOrder.push(1);
                    }),
                ];
                model.on('event', callbacks[0]);
                model.on('event', callbacks[2], {
                    priority: 1,
                });
                model.on('event', callbacks[1], {
                    priority: -1,
                });
                model.triggerCallbacks('event');
                expect(callOrder).toEqual([-1, 0, 1]);
            });

            /*
                This test is actually not sufficient.  There seems
                to be some randomness to the ordering when
                priorities are equal if we don't explicitly force
                the original order.  So this test would pass, at least
                sometimes, even if the code did not always work
                as expected.  In any case, we handle it explicitly now,
                and I guess it's better to have a test than to not
                have one?
            */
            it('should not change order if priority not set', () => {
                const callOrder = [];
                const callbacks = [
                    jest.fn().mockImplementation(() => {
                        callOrder.push(0);
                    }),
                    jest.fn().mockImplementation(() => {
                        callOrder.push(1);
                    }),
                    jest.fn().mockImplementation(() => {
                        callOrder.push(2);
                    }),
                ];
                model.on('event', callbacks[0]);
                model.on('event', callbacks[1]);
                model.on('event', callbacks[2]);
                model.triggerCallbacks('event');
                expect(callOrder).toEqual([0, 1, 2]);
            });
        });

        describe('remove', () => {
            it('should remove a component from the frame', () => {
                const component = frame.addComponent(Component.new());

                expect(frame.components.indexOf(component)).not.toEqual(-1); // sanity check
                expect(component.frame()).not.toBeUndefined(); // sanity check
                const callback = jest.fn();
                component.on('remove', callback);
                jest.spyOn(component, 'off').mockImplementation(() => {});
                component.remove();
                expect(frame.components.indexOf(component)).toEqual(-1);
                expect(component.frame()).toBeUndefined();
                expect(callback).toHaveBeenCalled();
                expect(component.off).toHaveBeenCalled();
            });
        });

        describe('key', () => {
            it('should fire callbacks when set', () => {
                const Subclass = Component.subclass(function () {
                    this.key('something');
                });

                const component = Subclass.new();
                const callback = jest.fn();
                component.on('set:something', callback);
                component.something = 1;
                expect(callback).toHaveBeenCalledWith(1, undefined);
                callback.mockClear();
                component.something = 2;
                expect(callback).toHaveBeenCalledWith(2, 1);
            });

            it('should fire callbacks immediately if runNow is true and property is set', () => {
                const Subclass = Component.subclass(function () {
                    this.key('something');
                });

                const component = Subclass.new();
                const callback = jest.fn();
                component.something = 1;
                component.on('set:something', callback, true);
                expect(callback).toHaveBeenCalledWith(1);
            });

            it('should fire callbacks immediately if runNow is true and property is not set', () => {
                const Subclass = Component.subclass(function () {
                    this.key('something');
                });

                const component = Subclass.new();
                const callback = jest.fn();
                component.on('set:something', callback, true);
                expect(callback).toHaveBeenCalledWith(undefined);
            });

            it('should not fire callbacks when set to the current value', () => {
                const Subclass = Component.subclass(function () {
                    this.key('something');
                });

                const component = Subclass.new();
                const callback = jest.fn();
                component.something = 1;
                component.on('set:something', callback);
                component.something = 1;
                expect(callback).not.toHaveBeenCalled();
            });

            it('should work if set in initialize', () => {
                const Subclass = Component.subclass(function () {
                    this.key('something');
                });

                const component = Subclass.new({
                    something: 1,
                });
                expect(component.something).toEqual(1);
            });

            it('should be in the json', () => {
                const Subclass = Component.subclass(function () {
                    this.key('something');
                });

                const component = Subclass.new();
                component.something = 1;
                expect(component.asJson().something).toEqual(1);
            });
        });

        describe('references', () => {
            it('should create a reference to another model', () => {
                const Subclass = Component.subclass(function () {
                    this.references('something').through('something_id');
                });
                frame.components = [];
                frame.addComponent(
                    Subclass.new({
                        id: 1,
                        something_id: 2,
                    }),
                );
                frame.addComponent(
                    Subclass.new({
                        id: 2,
                    }),
                );
                SpecHelper.expectEqual(frame.components[1], frame.components[0].something);
            });

            it('should create a reference to a list of models', () => {
                const Subclass = Component.subclass(function () {
                    this.references('somethings').through('something_ids');
                });
                frame.components = [];
                frame.addComponent(
                    Subclass.new({
                        id: 1,
                        something_ids: [2, 3],
                    }),
                );
                frame.addComponent(
                    Subclass.new({
                        id: 2,
                    }),
                );
                frame.addComponent(
                    Subclass.new({
                        id: 3,
                    }),
                );
                SpecHelper.expectEqual([frame.components[1], frame.components[2]], frame.components[0].somethings);
            });

            it('should create a setter that supports a single model', () => {
                const MyComponent = Component.subclass(function () {
                    this.references('something').through('something_id');
                });

                const component = frame.addComponent(MyComponent.new());
                const referencedComponent1 = frame.addComponent(MyComponent.new());
                const referencedComponent2 = frame.addComponent(MyComponent.new());

                component.something = referencedComponent1;
                SpecHelper.expectEqual(
                    referencedComponent1.id,
                    component.something_id,
                    'something_id after setting to referencedComponent1',
                );
                SpecHelper.expectEqual(
                    referencedComponent1,
                    component.something,
                    'something after setting to referencedComponent1',
                );

                component.something = referencedComponent2;
                SpecHelper.expectEqual(
                    referencedComponent2.id,
                    component.something_id,
                    'something_id after setting to referencedComponent2',
                );
                SpecHelper.expectEqual(
                    referencedComponent2,
                    component.something,
                    'something after setting to referencedComponent2',
                );
            });

            it('should create a setter that supports undefined', () => {
                const MyComponent = Component.subclass(function () {
                    this.references('something').through('something_id');
                });

                const component = frame.addComponent(MyComponent.new());
                const referencedComponent1 = frame.addComponent(MyComponent.new());

                component.something = referencedComponent1;
                SpecHelper.expectEqual(
                    referencedComponent1.id,
                    component.something_id,
                    'something_id after setting to referencedComponent1',
                );
                SpecHelper.expectEqual(
                    referencedComponent1,
                    component.something,
                    'something after setting to referencedComponent1',
                );

                component.something = undefined;
                SpecHelper.expectEqual(
                    false,
                    component.hasOwnProperty('something_id'),
                    'something_id property exists after setting to undefined',
                );
                SpecHelper.expectEqual(undefined, component.something, 'something after setting to undefined');
            });

            it('should create a setter that supports a list of models', () => {
                const MyComponent = Component.subclass(function () {
                    this.references('somethings').through('something_ids');
                });

                const component = frame.addComponent(MyComponent.new());
                const referencedComponent1 = frame.addComponent(MyComponent.new());
                const referencedComponent2 = frame.addComponent(MyComponent.new());

                component.somethings = [referencedComponent1];
                SpecHelper.expectEqual(
                    [referencedComponent1.id],
                    component.something_ids,
                    'something_ids after setting to referencedComponent1',
                );
                SpecHelper.expectEqual(
                    [referencedComponent1],
                    component.somethings,
                    'somethings after setting to referencedComponent1',
                );

                const childAddedCallback = jest.fn();
                const childRemovedCallback = jest.fn();
                component.somethings.on('childAdded', childAddedCallback);
                component.somethings.on('childRemoved', childRemovedCallback);
                childAddedCallback.mockClear();
                childRemovedCallback.mockClear();

                component.somethings = [referencedComponent2, referencedComponent1];
                SpecHelper.expectEqual(
                    [referencedComponent2.id, referencedComponent1.id],
                    component.something_ids,
                    'something_ids after setting to list',
                );
                SpecHelper.expectEqual(
                    [referencedComponent2, referencedComponent1],
                    component.somethings,
                    'somethings after setting to list',
                );

                expect(childAddedCallback.mock.calls.length).toBe(1);
                expect(childAddedCallback).toHaveBeenCalledWith(referencedComponent2);

                component.somethings = [referencedComponent2];
                expect(childRemovedCallback.mock.calls.length).toBe(1);
                expect(childRemovedCallback).toHaveBeenCalledWith(referencedComponent1);
            });

            it('should fire callbacks when set', () => {
                const Subclass = Component.subclass(function () {
                    this.references('something').through('something_id');
                });

                const component = Subclass.new();
                const callback = jest.fn();
                component.on('set:something', callback);
                component.something = component;
                expect(callback).toHaveBeenCalledWith(component);
            });

            it('should fire callbacks immediately if property is set and runNow=true', () => {
                const Subclass = Component.subclass(function () {
                    this.references('something').through('something_id');
                });

                const component = Subclass.new();
                frame.addComponent(component);
                const callback = jest.fn();
                component.something = component;
                component.on('set:something', callback, true);
                expect(callback).toHaveBeenCalledWith(component);
            });

            it('should fire callbacks immediately if property is not set and runNow=false', () => {
                const Subclass = Component.subclass(function () {
                    this.references('something').through('something_id');
                });

                const component = Subclass.new();
                const callback = jest.fn();
                component.on('set:something', callback, true);
                expect(callback).toHaveBeenCalledWith(undefined);
            });

            it('should throw if assigning to something other than a component model', () => {
                const Subclass = Component.subclass(function () {
                    this.alias('MyComponent');
                    this.references('something').through('something_id');
                });
                const component = Subclass.new();
                expect(() => {
                    component.something = {};
                }).toThrow(
                    new Error('Cannot set MyComponentModel.something to something that is not a ComponentModel'),
                );
            });

            it('should throw if assigning to an array with something in it that is not a component model', () => {
                const Subclass = Component.subclass(function () {
                    this.alias('MyComponent');
                    this.references('somethings').through('something_ids');
                });
                const component = Subclass.new();
                expect(() => {
                    component.somethings = [{}];
                }).toThrow(
                    new Error('Cannot add something that is not a ComponentModel to MyComponentModel.somethings'),
                );
            });

            describe('ProxyList', () => {
                describe('push', () => {
                    it('should push a reference', () => {
                        const MyComponent = Component.subclass(function () {
                            this.references('somethings').through('something_ids');
                        });

                        const component = frame.addComponent(MyComponent.new());
                        const referencedComponent1 = frame.addComponent(MyComponent.new());
                        const referencedComponent2 = frame.addComponent(MyComponent.new());
                        component.somethings = [];

                        component.somethings.push(referencedComponent1);
                        SpecHelper.expectEqual(
                            [referencedComponent1.id],
                            component.something_ids,
                            'something_ids after pushing referencedComponent1',
                        );
                        SpecHelper.expectEqual(
                            [referencedComponent1],
                            component.somethings,
                            'somethings after pushing referencedComponent1',
                        );

                        component.somethings.push(referencedComponent2);
                        SpecHelper.expectEqual(
                            [referencedComponent1.id, referencedComponent2.id],
                            component.something_ids,
                            'something_ids after pushing referencedComponent2',
                        );
                        SpecHelper.expectEqual(
                            [referencedComponent1, referencedComponent2],
                            component.somethings,
                            'somethings after pushing referencedComponent2',
                        );
                    });

                    it('should throw if passed something other than a component model', () => {
                        const MyComponent = Component.subclass(function () {
                            this.alias('MyComponent');
                            this.references('somethings').through('something_ids');
                        });

                        const component = frame.addComponent(MyComponent.new());
                        component.somethings = [];
                        expect(() => {
                            component.somethings.push({});
                        }).toThrow(
                            new Error(
                                'Cannot add something that is not a ComponentModel to MyComponentModel.somethings',
                            ),
                        );
                    });
                });

                describe('remove', () => {
                    it('should remove a reference', () => {
                        const MyComponent = Component.subclass(function () {
                            this.references('somethings').through('something_ids');
                        });

                        const component = frame.addComponent(MyComponent.new());
                        const referencedComponents = [
                            frame.addComponent(MyComponent.new()),
                            frame.addComponent(MyComponent.new()),
                        ];
                        component.somethings = referencedComponents;

                        component.somethings.remove(referencedComponents[0]);
                        SpecHelper.expectEqual([referencedComponents[1].id], component.something_ids);
                        SpecHelper.expectEqual([referencedComponents[1]], component.somethings);
                    });
                });

                describe('splice', () => {
                    let addedCallback;
                    let removedCallback;
                    let component;
                    let referencedComponents;
                    let MyComponent;

                    beforeEach(() => {
                        addedCallback = jest.fn();
                        removedCallback = jest.fn();

                        MyComponent = Component.subclass(function () {
                            this.references('somethings').through('something_ids');
                        });

                        component = frame.addComponent(MyComponent.new());
                        referencedComponents = [
                            frame.addComponent(MyComponent.new()),
                            frame.addComponent(MyComponent.new()),
                            frame.addComponent(MyComponent.new()),
                        ];
                        component.somethings = referencedComponents;

                        component.somethings.on('childAdded', addedCallback);
                        component.somethings.on('childRemoved', removedCallback);
                        addedCallback.mockClear(); // ignore the calls to childAdded that happen when the listener is setup
                    });

                    it('should handle start index only (one parameter) calls', () => {
                        component.somethings.splice(1);

                        SpecHelper.expectEqual([referencedComponents[0].id], component.something_ids);
                        SpecHelper.expectEqual([referencedComponents[0]], component.somethings);

                        // elements 1 and 2 should have been removed
                        expect(removedCallback).toHaveBeenCalledWith(referencedComponents[1]);
                        expect(removedCallback).toHaveBeenCalledWith(referencedComponents[2]);
                        SpecHelper.expectEqual(
                            2,
                            removedCallback.mock.calls.length,
                            'removedCallback.mock.calls.length',
                        );
                        expect(addedCallback).not.toHaveBeenCalled();
                    });

                    it('should handle index and offset (two parameter) calls', () => {
                        component.somethings.splice(0, 1);
                        SpecHelper.expectEqual(
                            [referencedComponents[1].id, referencedComponents[2].id],
                            component.something_ids,
                        );
                        SpecHelper.expectEqual(
                            [referencedComponents[1], referencedComponents[2]],
                            component.somethings,
                        );

                        // element 0 should have been removed
                        expect(removedCallback).toHaveBeenCalledWith(referencedComponents[0]);
                        SpecHelper.expectEqual(
                            1,
                            removedCallback.mock.calls.length,
                            'removedCallback.mock.calls.length',
                        );
                        expect(addedCallback).not.toHaveBeenCalled();
                    });

                    it('should handle index, offset, and elements (three+ parameter) calls', () => {
                        const additionalComponent = frame.addComponent(MyComponent.new());

                        component.somethings.splice(0, 1, additionalComponent);
                        SpecHelper.expectEqual(
                            [additionalComponent.id, referencedComponents[1].id, referencedComponents[2].id],
                            component.something_ids,
                        );
                        SpecHelper.expectEqual(
                            [additionalComponent, referencedComponents[1], referencedComponents[2]],
                            component.somethings,
                        );

                        // element 0 should have been removed and additionalComponent added
                        expect(removedCallback).toHaveBeenCalledWith(referencedComponents[0]);
                        SpecHelper.expectEqual(
                            1,
                            removedCallback.mock.calls.length,
                            'removedCallback.mock.calls.length',
                        );
                        expect(addedCallback).toHaveBeenCalledWith(additionalComponent);
                        SpecHelper.expectEqual(1, addedCallback.mock.calls.length, 'addedCallback.mock.calls.length');
                    });
                });

                describe('on', () => {
                    it('should support childAdded', () => {
                        const MyComponent = Component.subclass(function () {
                            this.references('somethings').through('something_ids');
                        });

                        const callback = jest.fn();

                        const component = frame.addComponent(MyComponent.new());
                        const referencedComponent1 = frame.addComponent(
                            MyComponent.new({
                                id: 'referencedComponent1',
                            }),
                        );
                        const referencedComponent2 = frame.addComponent(
                            MyComponent.new({
                                id: 'referencedComponent2',
                            }),
                        );
                        component.somethings = [referencedComponent1];
                        component.somethings.on('childAdded', callback);

                        expect(callback.mock.calls.length).toBe(1);
                        expect(callback.mock.calls[0][0].id).toBe(referencedComponent1.id);

                        component.somethings.push(referencedComponent2);
                        expect(callback.mock.calls.length).toBe(2);
                        expect(callback.mock.calls[1][0].id).toBe(referencedComponent2.id);
                    });

                    it('should support childRemoved', () => {
                        const MyComponent = Component.subclass(function () {
                            this.references('somethings').through('something_ids');
                        });

                        const callback = jest.fn();

                        const component = frame.addComponent(MyComponent.new());
                        const referencedComponent = frame.addComponent(
                            MyComponent.new({
                                id: 'referencedComponent',
                            }),
                        );
                        component.somethings = [referencedComponent];

                        component.somethings.on('childRemoved', callback);
                        expect(callback).not.toHaveBeenCalled();
                        component.somethings.remove(referencedComponent);
                        expect(callback).toHaveBeenCalledWith(referencedComponent);
                    });

                    it('should only fire on the relevant model', () => {
                        const MyComponent = Component.subclass(function () {
                            this.references('somethings').through('something_ids');
                        });

                        const callback = jest.fn();

                        const component1 = frame.addComponent(MyComponent.new());
                        const component2 = frame.addComponent(MyComponent.new());
                        const referencedComponent = frame.addComponent(MyComponent.new());
                        component1.somethings = [];
                        component2.somethings = [];

                        component1.somethings.on('childAdded', callback);

                        component2.somethings.push(referencedComponent);
                        expect(callback).not.toHaveBeenCalled();
                    });

                    it('should return a cancelable listener like model.on does', () => {
                        const MyComponent = Component.subclass(function () {
                            this.references('somethings').through('something_ids');
                        });

                        const callback = jest.fn();

                        const component = frame.addComponent(MyComponent.new());
                        const referencedComponent1 = frame.addComponent(
                            MyComponent.new({
                                id: 'referencedComponent1',
                            }),
                        );
                        const referencedComponent2 = frame.addComponent(
                            MyComponent.new({
                                id: 'referencedComponent2',
                            }),
                        );
                        component.somethings = [referencedComponent1];
                        const listener = component.somethings.on('childAdded', callback);
                        callback.mockClear();
                        listener.cancel();
                        component.somethings.push(referencedComponent2);
                        expect(callback).not.toHaveBeenCalled();
                    });
                });
            });
        });

        describe('listeners on referenced components', () => {
            let MyComponent;
            let component;

            beforeEach(() => {
                MyComponent = Component.subclass(function () {
                    this.references('something').through('something_id');
                    this.references('somethings').through('something_ids');
                    this.key('scalar');
                });
                component = createComponent();
            });

            describe('with referenced component', () => {
                it('should allow for setting a callback on a referenced component before it is set', () => {
                    const callback = jest.fn();
                    component.on('.something:event', callback);
                    component.something = createComponent();
                    component.something.triggerCallbacks('event');
                    expect(callback).toHaveBeenCalled();
                });

                it('should allow for setting a callback on a referenced component after it is set', () => {
                    const callback = jest.fn();
                    component.something = createComponent();
                    component.on('.something:event', callback);
                    component.something.triggerCallbacks('event');
                    expect(callback).toHaveBeenCalled();
                });

                it('should retain callbacks if a referenced component is replaced', () => {
                    const callback = jest.fn();
                    const oldSomething = (component.something = createComponent());
                    component.on('.something:event', callback);
                    component.something = createComponent();
                    oldSomething.triggerCallbacks('event');
                    expect(callback).not.toHaveBeenCalled();
                    component.something.triggerCallbacks('event');
                    expect(callback).toHaveBeenCalled();
                });

                it('should cancel listeners reference is unset', () => {
                    const callback = jest.fn();
                    const oldSomething = (component.something = createComponent());
                    component.on('.something:event', callback);
                    component.something = undefined;
                    oldSomething.triggerCallbacks('event');
                    expect(callback).not.toHaveBeenCalled();
                });

                it('should be cancelable', () => {
                    const callback = jest.fn();
                    const listener = component.on('.something:event', callback);
                    component.something = createComponent();
                    listener.cancel();
                    component.something.triggerCallbacks('event');
                    expect(callback).not.toHaveBeenCalled();
                });

                it('should fire immediately if event is set and runNow=true', () => {
                    const callback = jest.fn();
                    component.something = createComponent();
                    component.something.something = createComponent();
                    component.on('.something:set:something', callback, true);
                    expect(callback).toHaveBeenCalledWith(component.something.something);
                });
            });

            describe('with referenced list', () => {
                it('should allow for setting a callback on a referenced component before it is set', () => {
                    const callback = jest.fn();
                    component.on('.somethings[]:event', callback);
                    component.somethings = [createComponent()];
                    component.somethings[0].triggerCallbacks('event');
                    expect(callback).toHaveBeenCalled();
                });

                it('should allow for setting a callback on a referenced component after it is set', () => {
                    const callback = jest.fn();
                    component.somethings = [createComponent()];
                    component.on('.somethings[]:event', callback);
                    component.somethings[0].triggerCallbacks('event');
                    expect(callback).toHaveBeenCalled();
                });

                it('should retain callbacks if a referenced component is replaced', () => {
                    const callback = jest.fn();
                    component.somethings = [createComponent()];
                    const oldSomething = component.somethings[0];
                    component.on('.somethings[]:event', callback);
                    component.somethings = [createComponent()];
                    oldSomething.triggerCallbacks('event');
                    expect(callback).not.toHaveBeenCalled();
                    component.somethings[0].triggerCallbacks('event');
                    expect(callback).toHaveBeenCalled();
                });

                it('should cancel listeners reference is unset', () => {
                    const callback = jest.fn();
                    component.somethings = [createComponent()];
                    const oldSomething = component.somethings[0];
                    component.on('.somethings[]:event', callback);
                    component.somethings = undefined;
                    oldSomething.triggerCallbacks('event');
                    expect(callback).not.toHaveBeenCalled();
                });

                it('should stop listening to a component that is removed from the referenced list', () => {
                    const callback = jest.fn();
                    component.somethings = [createComponent()];
                    const oldSomething = component.somethings[0];
                    component.on('.somethings[]:event', callback);
                    component.somethings.remove(oldSomething);
                    oldSomething.triggerCallbacks('event');
                    expect(callback).not.toHaveBeenCalled();
                });

                it('should be cancelable', () => {
                    const callback = jest.fn();
                    const listener = component.on('.somethings[]:event', callback);
                    component.somethings = [createComponent()];
                    listener.cancel();
                    component.somethings[0].triggerCallbacks('event');
                    expect(callback).not.toHaveBeenCalled();
                });

                it('should fire immediately if event is set and runNow=true', () => {
                    const callback = jest.fn();
                    component.somethings = [createComponent()];
                    component.somethings[0].something = createComponent();
                    component.on('.somethings[]:set:something', callback, true);
                    expect(callback).toHaveBeenCalledWith(component.somethings[0].something);
                });
            });

            describe('with ridiculous nesting', () => {
                it('should work with ridiculous nesting', () => {
                    const callback = jest.fn();
                    component.on('.somethings[].something.something:event', callback);
                    component.somethings = [createComponent()];
                    component.somethings[0].something = createComponent();
                    component.somethings[0].something.something = createComponent();
                    component.somethings[0].something.something.triggerCallbacks('event');
                    expect(callback).toHaveBeenCalled();
                });

                it('should be cancelable', () => {
                    const callback = jest.fn();
                    const listener = component.on('.somethings[].something.something:event', callback);
                    component.somethings = [createComponent()];
                    component.somethings[0].something = createComponent();
                    component.somethings[0].something.something = createComponent();
                    listener.cancel();
                    component.somethings[0].something.something.triggerCallbacks('event');
                    expect(callback).not.toHaveBeenCalled();
                });

                it('should be canceled when off is called', () => {
                    const callback = jest.fn();
                    component.on('.somethings[].something.something:event', callback);
                    component.somethings = [createComponent()];
                    component.somethings[0].something = createComponent();
                    component.somethings[0].something.something = createComponent();
                    component.off();
                    component.somethings[0].something.something.triggerCallbacks('event');
                    expect(callback).not.toHaveBeenCalled();
                });

                it('should work with priority', () => {
                    const callOrder = [];
                    const callbacks = [
                        jest.fn().mockImplementation(() => {
                            callOrder.push(0);
                        }),
                        jest.fn().mockImplementation(() => {
                            callOrder.push(-1);
                        }),
                        jest.fn().mockImplementation(() => {
                            callOrder.push(1);
                        }),
                    ];
                    component.on('.somethings[].something.something:event', callbacks[0]);
                    component.on('.somethings[].something.something:event', callbacks[2], {
                        priority: 1,
                    });
                    component.on('.somethings[].something.something:event', callbacks[1], {
                        priority: -1,
                    });

                    component.somethings = [createComponent()];
                    component.somethings[0].something = createComponent();
                    component.somethings[0].something.something = createComponent();
                    component.somethings[0].something.something.triggerCallbacks('event');
                    expect(callOrder).toEqual([-1, 0, 1]);
                });

                it('should work with set on a scalar value', () => {
                    const callback = jest.fn();
                    component.on('.something:set:scalar', callback);
                    component.something = createComponent();
                    component.something.scalar = 'scalar';
                    expect(callback).toHaveBeenCalledWith('scalar', undefined);
                });
            });

            function createComponent() {
                const component = MyComponent.new();
                return frame.addComponent(component);
            }
        });

        describe('componentName', () => {
            it('should work', () => {
                const component = Component.new();
                component.component_type = 'ComponentizedFrame.MyComponent';
                SpecHelper.expectEqual('MyComponent', component.componentName);
            });
        });

        describe('type', () => {
            it('should work', () => {
                const component = Component.new();
                component.component_type = 'ComponentizedFrame.MyComponent';
                SpecHelper.expectEqual('MyComponentModel', component.type);
            });
        });

        describe('mainTextComponent.', () => {
            it('should throw on get', () => {
                const model = frame.addVanillaComponent();
                expect(() => {
                    model.mainTextComponent;
                }).toThrow(
                    new Error(
                        'Components that are used as mainUiComponents should define a getter for mainTextComponent. ComponentModelModel does not',
                    ),
                );
            });
        });

        describe('isReference', () => {
            it('should set up reference getter', () => {
                const Subclass = Component.subclass(function () {
                    this.references('text').through('text_id');
                });

                const component = Subclass.new();
                component.component_type = 'ComponentizedFrame.MyComponent';

                SpecHelper.expectEqual(true, component.isReference('text'));
                SpecHelper.expectEqual(false, component.isReference('somethingElse'));
                SpecHelper.expectEqual(false, component.isReference('text_id'));
            });
        });

        describe('hasReferenceTo', () => {
            it('should be able to access referenced component', () => {
                const Subclass = Component.subclass(function () {
                    this.references('text').through('text_id');
                });

                const component = Subclass.new();
                component.component_type = 'ComponentizedFrame.MyComponent';

                SpecHelper.expectEqual(true, component.hasReferenceThrough('text_id'));
                SpecHelper.expectEqual(false, component.hasReferenceThrough('text'));
                SpecHelper.expectEqual(false, component.hasReferenceThrough('something_else_id'));
            });
        });

        describe('behaviors', () => {
            let model;
            let callback;
            beforeEach(() => {
                const Subclass = Component.subclass(function () {
                    this.supportBehavior('MyBehavior');
                    this.supportBehavior('AnotherBehavior');
                });
                model = Subclass.new();
                callback = jest.fn().mockImplementation(function () {
                    expect(this).toBe(model);
                });
            });
            it('should trigger callback when behavior is added', () => {
                model.on('behavior_added:MyBehavior', callback);
                model.behaviors.MyBehavior = {
                    a: 'b',
                };
                expect(callback).toHaveBeenCalledWith({
                    a: 'b',
                });
            });
            it('should trigger callback when behavior is removed', () => {
                model.on('behavior_removed:MyBehavior', callback);
                model.behaviors.MyBehavior = {
                    a: 'b',
                };
                model.behaviors.MyBehavior = undefined;
                expect(callback).toHaveBeenCalled();
            });
            it('should fire two callbacks when behavior options are changed', () => {
                model.behaviors = {
                    MyBehavior: {
                        a: 'b',
                    },
                };
                const addedCallback = jest.fn();
                const removedCallback = jest.fn();
                model.on('behavior_removed:MyBehavior', removedCallback);
                model.on('behavior_added:MyBehavior', addedCallback);
                model.behaviors.MyBehavior = {
                    a: 'c',
                };
                expect(addedCallback).toHaveBeenCalled();
                expect(removedCallback).toHaveBeenCalled();
            });
            it('should not fire when setting to current value', () => {
                model.behaviors = {
                    MyBehavior: {
                        a: 'b',
                    },
                };
                const addedCallback = jest.fn();
                const removedCallback = jest.fn();
                model.on('behavior_removed:MyBehavior', removedCallback);
                model.on('behavior_added:AnotherBehavior', addedCallback);
                model.behaviors.MyBehavior = {
                    a: 'b',
                };
                expect(addedCallback).not.toHaveBeenCalled();
                expect(removedCallback).not.toHaveBeenCalled();
            });
            it('should work when replacing the whole object', () => {
                model.behaviors = {
                    MyBehavior: {},
                };
                const addedCallback = jest.fn();
                const removedCallback = jest.fn();
                model.on('behavior_removed:MyBehavior', removedCallback);
                model.on('behavior_added:AnotherBehavior', addedCallback);
                SpecHelper.expectEqual(0, addedCallback.mock.calls.length, 'calls to addedCallback before swap');
                SpecHelper.expectEqual(0, removedCallback.mock.calls.length, 'calls to removedCallback before swap');
                model.behaviors = {
                    AnotherBehavior: {},
                };
                SpecHelper.expectEqual(1, addedCallback.mock.calls.length, 'calls to addedCallback after swap');
                SpecHelper.expectEqual(1, removedCallback.mock.calls.length, 'calls to removedCallback after swap');
            });
            it('should have a healthy toJson', () => {
                model.behaviors.MyBehavior = {
                    a: 'b',
                };
                expect(model.asJson().behaviors.MyBehavior).toEqual({
                    a: 'b',
                });
            });
            it('should trigger when the callback is setup if the behavior is already there', () => {
                model.behaviors = {
                    MyBehavior: {},
                };
                model.on('behavior_added:MyBehavior', callback);
                expect(callback).toHaveBeenCalled();
            });

            it('should not trigger when behavior value is undefined', () => {
                model.behaviors = {
                    MyBehavior: undefined,
                };
                model.on('behavior_added:MyBehavior', callback);
                expect(callback).not.toHaveBeenCalled();
            });
        });

        describe('referencedComponents', () => {
            it('should work', () => {
                const Subclass = Component.subclass(function () {
                    this.references('something').through('something_id');
                    this.references('somethings').through('something_ids');
                    this.references('unset').through('unset_id');
                });

                const item = frame.addComponent(
                    Subclass.new({
                        something_id: '1',
                        something_ids: ['2', '3'],
                    }),
                );
                frame.addComponent(
                    Subclass.new({
                        id: '1',
                    }),
                );
                frame.addComponent(
                    Subclass.new({
                        id: '2',
                    }),
                );
                frame.addComponent(
                    Subclass.new({
                        id: '3',
                    }),
                );

                expect(item.referencedComponents().map(c => c.id)).toEqual(['1', '2', '3']);
            });

            it('should filter out references based on options', () => {
                const Subclass = Component.subclass(function () {
                    this.references('something').through('something_id');
                    this.references('somethings').through('something_ids');
                    this.references('unset').through('unset_id');
                });

                const item = frame.addComponent(
                    Subclass.new({
                        something_id: '1',
                        something_ids: ['2', '3'],
                    }),
                );
                frame.addComponent(
                    Subclass.new({
                        id: '1',
                    }),
                );
                frame.addComponent(
                    Subclass.new({
                        id: '2',
                    }),
                );
                frame.addComponent(
                    Subclass.new({
                        id: '3',
                    }),
                );

                expect(
                    item
                        .referencedComponents({
                            skipReferences: ['something'],
                        })
                        .map(c => c.id),
                ).toEqual(['2', '3']);

                expect(
                    item
                        .referencedComponents({
                            skipReferences: ['somethings'],
                        })
                        .map(c => c.id),
                ).toEqual(['1']);
            });
        });

        describe('imageContext', () => {
            it('should return a defined context', () => {
                Component.references('something').through('something_id');
                Component.setImageContext('something', 'context');
                expect(Component.new().imageContext('something')).toBe('context');
            });

            it('should use a provided function to determine the defined context', () => {
                Component.references('something').through('something_id');
                Component.setImageContext('something', key => `context_for_${key}`);
                expect(Component.new().imageContext('something')).toBe('context_for_something');
            });

            it('should notify sentry if no context defined', () => {
                Component.references('something').through('something_id');
                const ErrorLogService = $injector.get('ErrorLogService');
                jest.spyOn(ErrorLogService, 'notify').mockImplementation(() => {});
                Component.new().imageContext('key');
                expect(ErrorLogService.notify).toHaveBeenCalled();
            });
        });

        describe('clone', () => {
            it('should clone with newId=false', () => {
                const model = Component.new({
                    prop: 'wow',
                });
                const clone = model.clone();
                expect(model).not.toBe(clone);
                expect(model.asJson()).toEqual(clone.asJson());
            });

            it('should clone and create a newId with newId=true', () => {
                const model = Component.new({
                    prop: 'wow',
                });
                const clone = model.clone(true);
                expect(model).not.toBe(clone);
                expect(model.id).not.toEqual(clone.id);
                const modelJson = model.asJson();
                delete modelJson.id;
                const cloneJson = clone.asJson();
                delete cloneJson.id;
                expect(modelJson).toEqual(cloneJson);
            });
        });

        describe('localeObject', () => {
            let Locale;
            let Componentized;

            beforeEach(() => {
                Locale = $injector.get('Locale');
                $injector.get('ComponentizedFixtures');
                Componentized = $injector.get('Lesson.FrameList.Frame.Componentized');
            });

            it('should delegate to lesson', () => {
                const frame = Componentized.fixtures.getInstance().embedInLesson();
                frame.lesson().locale = 'zh';
                expect(frame.components[0].localeObject).toBe(Locale.chinese);
                frame.lesson().locale = 'en';
                expect(frame.components[0].localeObject).toBe(Locale.english);
            });

            it('should fallback to English', () => {
                const component = Component.new();
                expect(component.lesson).toBeUndefined(); // sanity check
                expect(component.localeObject).toBe(Locale.english);
            });
        });
    });

    describe('ViewModel', () => {
        describe('..ViewModel', () => {
            it('should be created when reference is called', () => {
                const Subclass = Component.subclass(function () {
                    this.references('something').through('something_id');
                });
                frame.components = [];
                const component1 = frame.addComponent(
                    Subclass.new({
                        id: 1,
                        something_id: 2,
                    }),
                );
                const component2 = frame.addComponent(
                    Subclass.new({
                        id: 2,
                    }),
                );
                const viewModel = component1.createViewModel(frameViewModel);
                SpecHelper.expectEqual(component2.id, viewModel.somethingViewModel.model.id);
            });
        });

        describe('..ViewModels', () => {
            it('should be created when reference is called', () => {
                const Subclass = Component.subclass(function () {
                    this.references('somethings').through('something_ids');
                });
                frame.components = [];
                frame.addComponent(
                    Subclass.new({
                        id: 1,
                        something_ids: [2, 3],
                    }),
                );
                frame.addComponent(
                    Subclass.new({
                        id: 2,
                    }),
                );
                frame.addComponent(
                    Subclass.new({
                        id: 3,
                    }),
                );

                const viewModel = frame.components[0].createViewModel(frameViewModel);
                SpecHelper.expectEqual(
                    [frame.components[1].id, frame.components[2].id],
                    viewModel.somethingsViewModels.map(vm => vm.model.id),
                );
            });
            it('should return an empty array if none available', () => {
                const Subclass = Component.subclass(function () {
                    this.references('somethings').through('something_ids');
                });
                const component = frame.addComponent(Subclass.new());
                const viewModel = component.createViewModel(frameViewModel);
                SpecHelper.expectEqual([], viewModel.somethingsViewModels);
            });
        });

        describe('includesBehavior', () => {
            it('should return true if the behavior is included', () => {
                const component = Component.new({
                    behaviors: {
                        MyBehavior: {},
                    },
                });
                const viewModel = component.createViewModel(frameViewModel);
                SpecHelper.expectEqual(true, viewModel.includesBehavior('MyBehavior'));
            });

            it('should return false if the behavior is not included', () => {
                const component = Component.new({
                    behaviors: {
                        MyBehavior: {},
                    },
                });
                const viewModel = component.createViewModel(frameViewModel);
                SpecHelper.expectEqual(false, viewModel.includesBehavior('SomeOtherBehavior'));
            });
        });

        describe('optionsForBehavior', () => {
            it('should return the options for a behavior', () => {
                const component = Component.new({
                    behaviors: {
                        MyBehavior: {
                            some: 'options',
                        },
                    },
                });
                const viewModel = component.createViewModel(frameViewModel);
                SpecHelper.expectEqual(
                    {
                        some: 'options',
                    },
                    viewModel.optionsForBehavior('MyBehavior'),
                );
            });

            it('should return an empty hash if the behavior is not included', () => {
                const component = Component.new({
                    behaviors: {
                        MyBehavior: {
                            some: 'options',
                        },
                    },
                });
                const viewModel = component.createViewModel(frameViewModel);
                SpecHelper.expectEqual({}, viewModel.optionsForBehavior('SomeOtherBehavior'));
            });
        });

        describe('viewModelFor', () => {
            it('should delegate to frameViewModel if a componentModel is passed in', () => {
                jest.spyOn(frameViewModel, 'viewModelFor').mockReturnValue('viewModel');
                const component = Component.new();
                const viewModel = component.createViewModel(frameViewModel);
                SpecHelper.expectEqual('viewModel', viewModel.viewModelFor('component'));
                expect(frameViewModel.viewModelFor).toHaveBeenCalledWith('component');
            });

            it('should be undefined if undefined is passed in', () => {
                jest.spyOn(frameViewModel, 'viewModelFor').mockImplementation(() => {});
                const component = Component.new();
                const viewModel = component.createViewModel(frameViewModel);
                SpecHelper.expectEqual(undefined, viewModel.viewModelFor(undefined));
                expect(frameViewModel.viewModelFor).not.toHaveBeenCalled();
            });
        });

        describe('viewModelsFor', () => {
            it('should delegate to frameViewModel if a componentModel is passed in', () => {
                jest.spyOn(frameViewModel, 'viewModelsFor').mockReturnValue('viewModels');
                const component = Component.new();
                const viewModel = component.createViewModel(frameViewModel);
                SpecHelper.expectEqual('viewModels', viewModel.viewModelsFor(['component']));
                expect(frameViewModel.viewModelsFor).toHaveBeenCalledWith(['component']);
            });

            it('should be undefined if undefined is passed in', () => {
                jest.spyOn(frameViewModel, 'viewModelsFor').mockImplementation(() => {});
                const component = Component.new();
                const viewModel = component.createViewModel(frameViewModel);
                SpecHelper.expectEqual(undefined, viewModel.viewModelsFor(undefined));
                expect(frameViewModel.viewModelsFor).not.toHaveBeenCalled();
            });
        });

        describe('type', () => {
            it('should work', () => {
                const component = Component.new();
                component.component_type = 'ComponentizedFrame.MyComponent';
                const viewModel = component.createViewModel(frameViewModel);
                SpecHelper.expectEqual('MyComponentViewModel', viewModel.type);
            });
        });
    });

    describe('EditorViewModel', () => {
        let EditorViewModel;
        let editorViewModel;
        let model;

        beforeEach(() => {
            EditorViewModel = $injector.get('Lesson.FrameList.Frame.Componentized.Component.ComponentEditorViewModel');
            editorViewModel = EditorViewModel.addComponentTo(frame).setup();
            model = editorViewModel.model;
        });

        describe('getter', () => {
            it('should give a nice error with a circular reference issue', () => {
                const MyModel = Component.subclass();
                MyModel.EditorViewModel = EditorViewModel.subclass(() => ({
                    initialize($super, model) {
                        $super(model);

                        return model.editorViewModel;
                    },
                }));

                const model = MyModel.new();
                frame.addComponent(model);

                expect(() => {
                    model.editorViewModel;
                }).toThrowError(
                    'The EditorViewModel initializer for UnidentifiedComponentModel raised a Maximum call stack error.  Most likely it referenced model.editorViewModel.  Do not do that.',
                );
            });
        });

        describe('componentName', () => {
            it('should work', () => {
                editorViewModel.model.component_type = 'ComponentizedFrame.MyComponent';
                SpecHelper.expectEqual('MyComponent', editorViewModel.componentName);
            });
        });

        describe('type', () => {
            it('should work', () => {
                editorViewModel.model.component_type = 'ComponentizedFrame.MyComponent';
                SpecHelper.expectEqual('MyComponentEditorViewModel', editorViewModel.type);
            });
        });

        describe('addTemplate', () => {
            it('should add a template', () => {
                const factory = () => {};
                EditorViewModel.addTemplate('myTemplate', 'My Template', factory);
                SpecHelper.expectEqualObjects(
                    {
                        myTemplate: {
                            EditorViewModel,
                            identifier: 'myTemplate',
                            title: 'My Template',
                            factory,
                        },
                    },
                    EditorViewModel.templates,
                );
            });
        });

        describe('activeTemplate', () => {
            it('should return the activeTemplate if there is one', () => {
                const template = EditorViewModel.addTemplate('myTemplate', 'My Template', () => {});
                editorViewModel.model.editor_template = 'myTemplate';
                expect(editorViewModel.activeTemplate).toBe(template);
            });
            it('should return undefined if there is no active template', () => {
                expect(editorViewModel.activeTemplate).toBeUndefined();
            });
            it('should throw if template not found', () => {
                editorViewModel.model.editor_template = 'myTemplate';
                expect(() => {
                    editorViewModel.activeTemplate;
                }).toThrow(new Error('No template found for "myTemplate"'));
            });
        });

        describe('applyTemplate', () => {
            let myTemplate;
            let templateFactory;

            beforeEach(() => {
                editorViewModel.constructor.supportConfigOption('a');
                templateFactory = jest.fn().mockImplementation(function () {
                    expect(this).toBe(editorViewModel);
                });
                myTemplate = editorViewModel.constructor.addTemplate('myTemplate', 'My Template', templateFactory);
            });

            it('should set the editor_template and run the function', () => {
                editorViewModel.applyTemplate(myTemplate);
                expect(model.editor_template).toEqual('myTemplate');
                expect(templateFactory).toHaveBeenCalled();
            });

            it('should support the identifier for the template as the argument', () => {
                editorViewModel.applyTemplate('myTemplate');
                expect(model.editor_template).toEqual('myTemplate');
            });

            it('should throw if an idenfier is passed in that cannot be found in the templates hash', () => {
                expect(() => {
                    editorViewModel.applyTemplate('unknownTemplate');
                }).toThrow(new Error('No template found for "unknownTemplate" on ComponentModelEditorViewModel.'));
            });

            it('should throw if EditorViewModel does not match', () => {
                myTemplate.EditorViewModel = 'something else';
                expect(() => {
                    editorViewModel.applyTemplate(myTemplate);
                }).toThrow(
                    new Error(`Cannot apply template from a different EditorViewModel to ${editorViewModel.type}`),
                );
            });

            it('should be settable to undefined', () => {
                editorViewModel.applyTemplate(myTemplate);
                editorViewModel.applyTemplate(undefined);
                expect(editorViewModel.model.editor_template).toBeUndefined();
            });

            it('should not run if the template is already set to the one being called', () => {
                editorViewModel.applyTemplate(myTemplate);
                templateFactory.mockClear();
                editorViewModel.applyTemplate(myTemplate);
                expect(templateFactory).not.toHaveBeenCalled();
            });

            it('should run if the template is already set to the one being called and force is true', () => {
                editorViewModel.applyTemplate(myTemplate);
                templateFactory.mockClear();
                editorViewModel.applyTemplate(myTemplate, true);
                expect(templateFactory).toHaveBeenCalled();
            });

            it('should pass through convertedFromFrame', () => {
                const convertedFromFrame = {};
                editorViewModel.applyTemplate(myTemplate, false, convertedFromFrame);
                expect(model.editor_template).toEqual('myTemplate');
                expect(templateFactory).toHaveBeenCalledWith(convertedFromFrame);
            });
        });

        describe('applyCurrentTemplate', () => {
            let myTemplate;
            let templateFactory;

            beforeEach(() => {
                editorViewModel.constructor.supportConfigOption('a');
                templateFactory = jest.fn().mockImplementation(function () {
                    expect(this).toBe(editorViewModel);
                });
                myTemplate = editorViewModel.constructor.addTemplate('myTemplate', 'My Template', templateFactory);
            });

            it('should apply a template if there is an editor_template', () => {
                editorViewModel.model.editor_template = 'myTemplate';
                jest.spyOn(editorViewModel.constructor.prototype, 'applyTemplate').mockImplementation(() => {});
                new editorViewModel.constructor(model).applyCurrentTemplate();
                expect(editorViewModel.constructor.prototype.applyTemplate).toHaveBeenCalledWith(myTemplate, true);
            });

            it('should not do anything if there is not an editor_template', () => {
                jest.spyOn(editorViewModel.constructor.prototype, 'applyTemplate').mockImplementation(() => {});
                new editorViewModel.constructor(model).applyCurrentTemplate();
                expect(editorViewModel.constructor.prototype.applyTemplate).not.toHaveBeenCalled();
            });

            it('should raise if template is not found', () => {
                editorViewModel.model.editor_template = 'unkownTemplate';
                expect(() => {
                    new editorViewModel.constructor(model).applyCurrentTemplate();
                }).toThrow(new Error(`Template "unknownTemplate" is not defined on ${editorViewModel.type}`));
            });
        });

        describe('..EditorViewModel', () => {
            it('should be created when reference is called', () => {
                const Subclass = Component.subclass(function () {
                    this.references('something').through('something_id');
                });
                Subclass.setEditorViewModel('Lesson.FrameList.Frame.Componentized.Component.ComponentEditorViewModel');
                frame.components = [];
                const component = frame.addComponent(
                    Subclass.new({
                        id: 1,
                        something_id: 2,
                    }),
                );
                const referencedComponent = frame.addComponent(
                    Subclass.new({
                        id: 2,
                    }),
                );
                const editorViewModel = frame.editorViewModelFor(component);
                SpecHelper.expectEqual(referencedComponent.id, editorViewModel.model.something.id);
            });
        });

        describe('editorViewModelFor', () => {
            it('should delegate to the frame if a model is passed in', () => {
                jest.spyOn(frame, 'editorViewModelFor').mockReturnValue('mockEditorViewModel');
                expect(editorViewModel.editorViewModelFor('model')).toEqual('mockEditorViewModel');
                expect(frame.editorViewModelFor).toHaveBeenCalledWith('model');
            });
            it('should return undefined if undefined is passed in', () => {
                jest.spyOn(frame, 'editorViewModelFor').mockImplementation(() => {});
                expect(editorViewModel.editorViewModelFor(undefined)).toEqual(undefined);
                expect(frame.editorViewModelFor).not.toHaveBeenCalled();
            });
        });

        describe('editorViewModelsFor', () => {
            it('should delegate to the frame if a model is passed in', () => {
                jest.spyOn(frame, 'editorViewModelsFor').mockReturnValue('mockEditorViewModels');
                expect(editorViewModel.editorViewModelsFor('models')).toEqual('mockEditorViewModels');
                expect(frame.editorViewModelsFor).toHaveBeenCalledWith('models');
            });
            it('should return an empty array if undefined is passed in', () => {
                jest.spyOn(frame, 'editorViewModelsFor').mockImplementation(() => {});
                expect(editorViewModel.editorViewModelsFor(undefined)).toEqual([]);
                expect(frame.editorViewModelsFor).not.toHaveBeenCalled();
            });
        });

        describe('swapReferences', () => {
            it('should replace references to one component with references to another', () => {
                const MyComponent = Component.subclass(function () {
                    this.references('something').through('something_id');
                });

                const referencedComponent = frame.addComponent(MyComponent.new());
                const component = frame.addComponent(
                    MyComponent.new({
                        something_id: referencedComponent.id,
                    }),
                );

                const editorViewModel = new EditorViewModel(component);
                const callback = jest.fn();
                const newComponent = frame.addVanillaComponent();

                editorViewModel.model.on('set:something', callback);
                editorViewModel.swapReferences(referencedComponent, newComponent);
                SpecHelper.expectEqual(newComponent.id, component.something_id, 'something_id');
                SpecHelper.expectEqual(newComponent, component.something, 'something');
                expect(callback).toHaveBeenCalledWith(newComponent);
            });
        });

        describe('removeReferencesTo', () => {
            it('should remove a reference to a component', () => {
                const MyComponent = Component.subclass(function () {
                    this.references('something').through('something_id');
                });

                const referencedComponent = frame.addComponent(MyComponent.new());
                const component = frame.addComponent(
                    MyComponent.new({
                        something_id: referencedComponent.id,
                    }),
                );

                const editorViewModel = new EditorViewModel(component);
                const callback = jest.fn();

                editorViewModel.model.on('set:something', callback);
                editorViewModel.removeReferencesTo(referencedComponent);
                SpecHelper.expectEqual(undefined, component.something_id, 'something_id');
                SpecHelper.expectEqual(undefined, component.something, 'something');
                expect(callback).toHaveBeenCalledWith(undefined);
            });

            it('should remove a reference to a component in a list', () => {
                const MyComponent = Component.subclass(function () {
                    this.references('somethings').through('something_ids');
                });

                const referencedComponent = frame.addComponent(MyComponent.new());
                const component = frame.addComponent(
                    MyComponent.new({
                        something_ids: [referencedComponent.id],
                    }),
                );
                const editorViewModel = new EditorViewModel(component);

                const callback = jest.fn();
                editorViewModel.model.somethings.on('childRemoved', callback);
                editorViewModel.removeReferencesTo(referencedComponent);
                SpecHelper.expectEqual([], component.something_ids, 'something_ids');
                SpecHelper.expectEqual([], component.somethings, 'somethings');
                expect(callback).toHaveBeenCalledWith(referencedComponent);
            });
        });

        describe('config', () => {
            it('should pass config down to referenced component', () => {
                const MyComponent = Component.subclass(function () {
                    this.references('text_area').through('text_area_id');
                });
                MyComponent.setEditorViewModel(
                    'Lesson.FrameList.Frame.Componentized.Component.ComponentEditorViewModel',
                );

                const textAreaComponent = frame.addComponent(MyComponent.new());

                const blogComponent = frame.addComponent(MyComponent.new());
                blogComponent.text_area = textAreaComponent;

                const blogComponentHelper = frame.editorViewModelFor(blogComponent);
                const textAreaComponentHelper = frame.editorViewModelFor(textAreaComponent);

                const config = {
                    text_area: {
                        rainbow_text: true,
                    },
                };

                jest.spyOn(textAreaComponentHelper, 'setConfig').mockImplementation(() => {});
                blogComponentHelper.setConfig(config);
                expect(textAreaComponentHelper.setConfig).toHaveBeenCalledWith({
                    rainbow_text: true,
                });
            });

            it('should have delegate config down to new components that are created', () => {
                // haven't written this functionality yet. we're not using it
                // yet, so can put it off
                // expect(false).toBe(true);
            });
        });

        describe('mainTextComponent.', () => {
            it('should throw on set', () => {
                const editorViewModel = new EditorViewModel(frame.addVanillaComponent());
                expect(() => {
                    editorViewModel.mainTextComponent = 'something';
                }).toThrow(
                    new Error(
                        'Components that are used as mainUiComponents should define mainTextComponent. ComponentModelEditorViewModel does not',
                    ),
                );
            });
        });

        describe('mainImage', () => {
            it('should throw on get', () => {
                const editorViewModel = new EditorViewModel(frame.addVanillaComponent());
                expect(() => {
                    editorViewModel.mainImage;
                }).toThrow(
                    new Error(
                        'Components that are used as mainUiComponents should define mainImage. ComponentModelEditorViewModel does not',
                    ),
                );
            });
            it('should throw on set', () => {
                const editorViewModel = new EditorViewModel(frame.addVanillaComponent());
                expect(() => {
                    editorViewModel.mainImage = 'something';
                }).toThrow(
                    new Error(
                        'Components that are used as mainUiComponents should define mainImage. ComponentModelEditorViewModel does not',
                    ),
                );
            });
        });

        describe('mainModalTexts', () => {
            beforeEach(() => {
                Object.defineProperty(editorViewModel, 'mainTextComponent', {
                    value: frame.addText(),
                });
            });
            it('should get the text from the modals on the mainTextComponent', () => {
                editorViewModel.mainTextComponent.modals = [
                    frame.addText({
                        text: 'modal content 1',
                    }),
                    frame.addText({
                        text: 'modal content 2',
                    }),
                ];
                expect(editorViewModel.mainModalTexts).toEqual(['modal content 1', 'modal content 2']);
            });
            it('should set the text from the modals on the mainTextComponent', () => {
                editorViewModel.mainModalTexts = ['modal content 1', 'modal content 2'];
                expect(editorViewModel.mainTextComponent.modals.map(m => m.text)).toEqual([
                    'modal content 1',
                    'modal content 2',
                ]);
            });
        });
    });
});
