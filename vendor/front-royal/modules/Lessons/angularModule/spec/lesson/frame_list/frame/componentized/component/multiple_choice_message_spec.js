import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/componentized_fixtures';
import 'Editor/angularModule';

describe('Componentized.Component.MultipleChoiceMessage', () => {
    let frame;
    let frameViewModel;
    let model;
    let viewModel;
    let Componentized;
    let SpecHelper;
    let ValidationResult;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                Componentized = $injector.get('Lesson.FrameList.Frame.Componentized');
                ValidationResult = $injector.get(
                    'Lesson.FrameList.Frame.Componentized.Component.ChallengeValidator.ValidationResult.MultipleChoiceValidationResult',
                );
                SpecHelper = $injector.get('SpecHelper');
                $injector.get('ComponentizedFixtures');
                viewModel = getViewModel();
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('Model', () => {
        describe('appliesToAnswer', () => {
            it('should work', () => {
                jest.spyOn(model.answerMatcher, 'matches').mockReturnValue('RETURN_VALUE');
                expect(model.appliesToAnswer('ANSWER')).toEqual('RETURN_VALUE');
                expect(model.answerMatcher.matches).toHaveBeenCalledWith('ANSWER');
            });
        });
    });

    describe('ViewModel', () => {
        describe('on selected', () => {
            it('should show a message when an answer is selected', () => {
                viewModel = getViewModel({
                    event: 'selected',
                });
                viewModel.challengeViewModel.active = true;
                const answerViewModel = viewModel.viewModelFor(model.challenge.answers[0]);

                answerViewModel.selected = true;
                expectShowMessageCalled(model.messageText, false);
            });

            it('should not show a message on selected if challenge is not active', () => {
                viewModel = getViewModel({
                    event: 'selected',
                });
                viewModel.challengeViewModel.active = false;
                const answerViewModel = viewModel.viewModelFor(model.challenge.answers[0]);

                answerViewModel.selected = true;
                expect(frameViewModel.playerViewModel.showMessage).not.toHaveBeenCalled();
            });
        });

        describe('on validated', () => {
            describe('with answerSelected event in the validator info', () => {
                it('should show a message on validated when a correct answer is selected', () => {
                    viewModel = getViewModel({
                        event: 'validated',
                    });

                    const answerViewModel = viewModel.challengeViewModel.answersViewModels[0];

                    // make the answer correct
                    jest.spyOn(ValidationResult.prototype, 'hasErrorRelatedToAnswer').mockReturnValue(false);

                    answerViewModel.selected = true;
                    const validationResult = viewModel.challengeViewModel.validate({
                        event: {
                            type: 'answerSelected',
                            target: answerViewModel,
                        },
                    });
                    expect(ValidationResult.prototype.hasErrorRelatedToAnswer).toHaveBeenCalledWith(
                        answerViewModel.model,
                    );
                    expectShowMessageCalled(model.messageText, true, true);
                    expect(validationResult.messages).toEqual([model.messageText]);
                });

                it('should show a message on validated when an incorrect answer is selected', () => {
                    const answerViewModel = viewModel.challengeViewModel.answersViewModels[0];

                    // make the answer incorrect
                    jest.spyOn(ValidationResult.prototype, 'hasErrorRelatedToAnswer').mockReturnValue(true);

                    answerViewModel.selected = true;
                    const validationResult = viewModel.challengeViewModel.validate({
                        event: {
                            type: 'answerSelected',
                            target: answerViewModel,
                        },
                    });
                    expect(ValidationResult.prototype.hasErrorRelatedToAnswer).toHaveBeenCalledWith(
                        answerViewModel.model,
                    );
                    expectShowMessageCalled(model.messageText, true, false);
                    expect(validationResult.messages).toEqual([model.messageText]);
                });
            });

            describe('with answerUnselected event in the validator info', () => {
                it('should do nothing', () => {
                    viewModel = getViewModel({
                        event: 'validated',
                    });

                    const answerViewModel = viewModel.challengeViewModel.answersViewModels[0];
                    answerViewModel.selected = true;
                    const validationResult = viewModel.challengeViewModel.validate({
                        event: {
                            type: 'answerUnselected',
                            target: answerViewModel,
                        },
                    });
                    expect(frameViewModel.playerViewModel.showMessage).not.toHaveBeenCalled();
                    expect(validationResult.messages).toEqual([]);
                });
            });

            describe('with no event in the validator info', () => {
                it('should show a message on validated when a single correct answer is selected', () => {
                    viewModel = getViewModel({
                        event: 'validated',
                    });

                    const answerViewModel = viewModel.challengeViewModel.answersViewModels[0];

                    // make the answer correct
                    jest.spyOn(ValidationResult.prototype, 'hasErrorRelatedToAnswer').mockReturnValue(false);

                    answerViewModel.selected = true;
                    viewModel.challengeViewModel.validate({});
                    expect(ValidationResult.prototype.hasErrorRelatedToAnswer).toHaveBeenCalledWith(
                        answerViewModel.model,
                    );
                    expectShowMessageCalled(model.messageText, true, true);
                });

                it('should show a message on validated when a single incorrect answer is selected', () => {
                    const answerViewModel = viewModel.challengeViewModel.answersViewModels[0];

                    // make the answer incorrect
                    jest.spyOn(ValidationResult.prototype, 'hasErrorRelatedToAnswer').mockReturnValue(true);

                    answerViewModel.selected = true;
                    viewModel.challengeViewModel.validate({});
                    expect(ValidationResult.prototype.hasErrorRelatedToAnswer).toHaveBeenCalledWith(
                        answerViewModel.model,
                    );
                    expectShowMessageCalled(model.messageText, true, false);
                });

                it('should throw an error on validated with multiple answers selected', () => {
                    viewModel.challengeViewModel.answersViewModels[0].selected = true;
                    viewModel.challengeViewModel.answersViewModels[1].selected = true;

                    expect(() => {
                        viewModel.challengeViewModel.validate({});
                    }).toThrow(new Error('Cannot show messages for multiple selected answers.'));
                });
            });
        });

        function expectShowMessageCalled(messageTextModel, correctExists, correct) {
            expect(frameViewModel.playerViewModel.showMessage).toHaveBeenCalled();
            const arg = frameViewModel.playerViewModel.showMessage.mock.calls[0][0];
            expect(arg.scope.textViewModel.model).toBe(messageTextModel);
            expect(arg.content).toBe('<cf-text view-model="textViewModel"></cf-text>');
            expect(arg.correctExists).toBe(correctExists);
            expect(arg.correct).toBe(correct);
        }
    });

    describe('EditorViewModel', () => {
        describe('initialize', () => {
            it('should listen for the answerMatcher to be removed, and remove itself', () => {
                const model = getModel();
                model.editorViewModel;
                jest.spyOn(model, 'remove').mockImplementation(() => {});
                model.answerMatcher.remove();
                expect(model.remove).toHaveBeenCalled();
            });
        });

        describe('_onAnswerContentChanged', () => {
            it('should remove the message if a change to the answer text causes a conflict', () => {
                const message1 = getModel();
                const challenge = message1.challenge;

                message1.answerMatcher.answer.text.text = 'this';
                const message2 = frame
                    .addMultipleChoiceMessage({
                        challenge_id: challenge.id,
                    })
                    .addDefaultReferences();
                message2.attachToAnswer(challenge.answers[1]);
                message2.answerMatcher.answer.text.text = 'that';

                expect(message2.challenge).toBe(message1.challenge);
                expect(message2.appliesToAnswer(message1.answerMatcher.answer));
                expect(message2.answerMatcher.answer).not.toBe(message1.answerMatcher.answer);

                // make sure we have editorViewModels
                _.chain(frame.components).pluck('editorViewModel');

                // when we change the text on answer 1 to be the same as
                // answer 2, the message from answer 1 should be deleted
                jest.spyOn(message1, 'remove').mockImplementation(() => {});
                message1.answerMatcher.answer.text.text = 'that';
                expect(message1.remove).toHaveBeenCalled();
            });

            it('should remove the message if a change to the answer image causes a conflict', () => {
                const message1 = getModel();
                const challenge = message1.challenge;

                message1.answerMatcher.answer.text.text = 'this';
                const message2 = frame
                    .addMultipleChoiceMessage({
                        challenge_id: challenge.id,
                    })
                    .addDefaultReferences();

                // make sure we have editorViewModels
                _.chain(frame.components).pluck('editorViewModel');

                message2.attachToAnswer(challenge.answers[1]);
                const image = (message2.answerMatcher.answer.image = frame.addImage());

                expect(message2.challenge).toBe(message1.challenge);
                expect(message2.appliesToAnswer(message1.answerMatcher.answer));
                expect(message2.answerMatcher.answer).not.toBe(message1.answerMatcher.answer);

                // when we change the text on answer 1 to be the same as
                // answer 2, the message from answer 1 should be deleted
                jest.spyOn(message1, 'remove').mockImplementation(() => {});
                message1.answerMatcher.answer.image = image;
                expect(message1.remove).toHaveBeenCalled();
            });
        });
    });

    function getViewModel(options) {
        model = getModel(options);
        return frameViewModel.viewModelFor(model);
    }

    function getModel(options) {
        frame = Componentized.fixtures.getInstance();
        frameViewModel = frame.createFrameViewModel();
        frameViewModel.playerViewModel = {
            showMessage: jest.fn(),
            logInfo: jest.fn(),
            log: jest.fn(),
            setChallengeScore: jest.fn(),
        };
        return frame.addMultipleChoiceMessage(options).addDefaultReferences();
    }
});
