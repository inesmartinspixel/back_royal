import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/componentized_fixtures';
import 'Editor/angularModule';

describe('Componentized.Component.AnswerMatcher', () => {
    let frame;
    let model;
    let Componentized;
    let SpecHelper;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                Componentized = $injector.get('Lesson.FrameList.Frame.Componentized');
                SpecHelper = $injector.get('SpecHelper');
                $injector.get('ComponentizedFixtures');
                model = getAnswerMatcherModel();
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('Model', () => {
        describe('matches', () => {
            it('should throw', () => {
                expect(() => {
                    model.matches({});
                }).toThrow(
                    new Error('subclasses of AnswerMatcherModel should define matches.  AnswerMatcherModel does not.'),
                );
            });
        });

        describe('matchesAny', () => {
            it('should return true if any of the answers match', () => {
                jest.spyOn(model, 'matches').mockImplementation(answer => answer === 2);
                SpecHelper.expectEqual(true, model.matchesAny([1, 2, 3]));
            });

            it('should return false if none of the answers match', () => {
                jest.spyOn(model, 'matches').mockReturnValue(false);
                SpecHelper.expectEqual(false, model.matchesAny([1, 2, 3]));
            });
        });
    });

    function getAnswerMatcherModel() {
        frame = Componentized.fixtures.getInstance();
        frame.addAnswerMatcher();
        const model = frame.getModelsByType('AnswerMatcherModel')[0];

        expect(model).not.toBeUndefined();
        return model;
    }
});
