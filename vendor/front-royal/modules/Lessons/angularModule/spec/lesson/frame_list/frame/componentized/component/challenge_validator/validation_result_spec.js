import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/componentized_fixtures';
import 'Editor/angularModule';

describe('Componentized.Component.ChallengeValidator.ValidationResult', () => {
    let ValidationResult;
    let ValidationError;
    let SpecHelper;
    let HasUnexpectedAnswer;
    let MissingExpectedAnswer;
    let Componentized;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                ValidationResult = $injector.get(
                    'Lesson.FrameList.Frame.Componentized.Component.ChallengeValidator.ValidationResult',
                );
                ValidationError = $injector.get(
                    'Lesson.FrameList.Frame.Componentized.Component.ChallengeValidator.ValidationError',
                );
                SpecHelper = $injector.get('SpecHelper');
                HasUnexpectedAnswer = $injector.get(
                    'Lesson.FrameList.Frame.Componentized.Component.ChallengeValidator.ValidationError.HasUnexpectedAnswer',
                );
                MissingExpectedAnswer = $injector.get(
                    'Lesson.FrameList.Frame.Componentized.Component.ChallengeValidator.ValidationError.MissingExpectedAnswer',
                );
                Componentized = $injector.get('Lesson.FrameList.Frame.Componentized');
                $injector.get('ComponentizedFixtures');
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('initialize', () => {
        it('should store info', () => {
            const validationResult = new ValidationResult('mockChallengeViewModel', [], 'info');
            expect(validationResult.info).toEqual('info');
        });
    });

    describe('hasErrorRelatedToAnswer', () => {
        it('should return true iff one of the errors is related to the provided answer', () => {
            const mockAnswers = ['mockAnswer0', 'mockAnswer1'];

            // mock out an error related to 'mockAnswer0'
            const mockError = new ValidationError();
            jest.spyOn(mockError, 'isRelatedToAnswer').mockImplementation(answer => answer === mockAnswers[0]);

            // create a validation result with the mockError
            const validationResult = new ValidationResult('mockChallengeViewModel', [mockError]);

            // the validation result should be related to 'mockAnswer0' but not 'mockAnswer1'
            SpecHelper.expectEqual(
                true,
                validationResult.hasErrorRelatedToAnswer(mockAnswers[0]),
                'there is an error related to mockAnswers[0]',
            );
            SpecHelper.expectEqual(
                false,
                validationResult.hasErrorRelatedToAnswer(mockAnswers[1]),
                'there is an error related to mockAnswers[1]',
            );
        });
    });

    describe('partiallyCorrect', () => {
        describe('with multiple expectedAnswerMatchers', () => {
            let mockChallengeViewModel;

            beforeEach(() => {
                mockChallengeViewModel = {
                    challengeResponses: ['mockChallengeResponse1'],
                    validator: {
                        expectedAnswerMatchers: ['mockAnswerMatcher1', 'mockAnswerMatcher2'],
                    },
                };
            });
            it('should be true if the only thing wrong is an expected response is missing', () => {
                // create a validation result with the mockError
                const validationResult = new ValidationResult(mockChallengeViewModel, [
                    new MissingExpectedAnswer(),
                    new MissingExpectedAnswer(),
                ]);

                expect(validationResult.partiallyCorrect).toBe(true);
            });

            it('should be false if there is an unexpected response', () => {
                // create a validation result with the mockError
                const validationResult = new ValidationResult(mockChallengeViewModel, [
                    new MissingExpectedAnswer(),
                    new HasUnexpectedAnswer(),
                ]);

                expect(validationResult.partiallyCorrect).toBe(false);
            });
        });

        describe('with one expectedAnswerMatcher', () => {
            it('should be true if response is a partial match', () => {
                const validationResult = mockValidationResultWithMatchingDetails({
                    matches: false,
                    partialMatch: true,
                });

                expect(validationResult.partiallyCorrect).toBe(true);
            });

            it('should be false if response is not partial match', () => {
                const validationResult = mockValidationResultWithMatchingDetails({
                    matches: false,
                    partialMatch: false,
                });

                expect(validationResult.partiallyCorrect).toBe(false);
            });

            it('should be false if there is no matchesWithDetails', () => {
                const validationResult = mockValidationResultWithMatchingDetails({});
                validationResult._validator.expectedAnswerMatchers[0].matchesWithDetails = undefined;

                expect(validationResult.partiallyCorrect).toBe(false);
            });

            it('should be false if there are multiple responses', () => {
                const validationResult = mockValidationResultWithMatchingDetails({}, [
                    'mockChallengeResponse1',
                    'mockChallengeResponse2',
                ]);

                expect(validationResult.partiallyCorrect).toBe(false);
                expect(validationResult._validator.expectedAnswerMatchers[0].matchesWithDetails).not.toHaveBeenCalled();
            });
        });
    });

    describe('userAnswerToDisplay', () => {
        it('should have tests', () => {
            const validationResult = mockValidationResultWithMatchingDetails({
                userAnswerToDisplay: 'new answer',
            });

            expect(validationResult.userAnswerToDisplay).toBe('new answer');
        });
    });

    describe('asJson', () => {
        it('should include expected values for all challenges', () => {
            const validationResult = validationResultWithMultipleChoiceChallenge();

            // mock out errors with asJson methods
            const mockError = {
                asJson: jest.fn(),
            };
            mockError.asJson.mockReturnValue('errorJson');
            validationResult.errors = [mockError];

            // mock out challenge responses with asJson methods
            const mockChallengeResponse = {
                asJson: jest.fn(),
            };
            mockChallengeResponse.asJson.mockReturnValue('challengeResponseJson');
            Object.defineProperty(validationResult, '_challengeResponses', {
                value: [mockChallengeResponse],
            });

            // mock out partiallyCorrect
            Object.defineProperty(validationResult, 'partiallyCorrect', {
                value: true,
            });

            const json = validationResult.asJson();

            // sanity checks
            expect(validationResult.challenge.id).not.toBeUndefined();
            expect(validationResult.result).not.toBeUndefined();
            expect(validationResult.partiallyCorrect).not.toBeUndefined();
            expect(validationResult.info).not.toBeUndefined();

            expect(json.challenge_id).toBe(validationResult.challenge.id);
            expect(json.result).toBe(validationResult.result);
            expect(json.partiallyCorrect).toBe(validationResult.partiallyCorrect);
            expect(json.errors).toEqual(['errorJson']);
            expect(json.challengeResponses).toEqual(['challengeResponseJson']);
            expect(json.caused_by_event).toEqual(validationResult.causedByEvent);
        });
        it('should include expected values for multiple choice challenge', () => {
            const validationResult = validationResultWithMultipleChoiceChallenge();

            // sanity check, check that these properties actually exist on challengeViewModel
            // before we mock them out
            expect(validationResult.challengeViewModel.availableAnswersViewModels).not.toBeUndefined();

            Object.defineProperties(validationResult.challengeViewModel, {
                availableAnswersViewModels: {
                    value: [
                        {
                            model: {
                                id: 1,
                            },
                        },
                    ],
                },
            });

            const json = validationResult.asJson();

            expect(json.availableAnswerIds).toEqual([1]);
        });
        it('should include expected values for user input challenge', () => {
            const validationResult = validationResultWithUserInputChallenge();

            Object.defineProperty(validationResult, 'userAnswerToDisplay', {
                value: 'abcs',
            });

            const json = validationResult.asJson();

            expect(json.userAnswerToDisplay).toEqual(validationResult.userAnswerToDisplay);
        });
    });

    describe('shouldLogEvent', () => {
        describe('for multiple choice challenge', () => {
            it('should be true', () => {
                expect(validationResultWithMultipleChoiceChallenge().shouldLogEvent).toBe(true);
            });
        });
        describe('for user input challenge', () => {
            it('should be true if correct', () => {
                expect(validationResultWithUserInputChallenge().shouldLogEvent).toBe(true);
            });
            it('should be true if incorrect and not incomplete', () => {
                expect(validationResultWithUserInputChallenge().shouldLogEvent).toBe(true);
            });
            it('should be false if partiallyCorrect', () => {
                const result = validationResultWithUserInputChallenge();
                Object.defineProperty(result, 'partiallyCorrect', {
                    value: true,
                });
                expect(result.shouldLogEvent).toBe(false);
            });
        });
    });

    function mockValidationResultWithMatchingDetails(details, challengeResponses = ['mockChallengeResponse1']) {
        const mockAnswerMatcher = {
            matchesWithDetails: jest.fn().mockReturnValue(details),
        };

        const mockChallengeViewModel = {
            challengeResponses,
            validator: {
                expectedAnswerMatchers: [mockAnswerMatcher],
            },
        };

        // create a validation result with the mockError
        const validationResult = new ValidationResult(mockChallengeViewModel, [
            {
                isA() {
                    return false;
                },
            },
        ]);
        return validationResult;
    }

    function validationResultWithMultipleChoiceChallenge() {
        const frame = Componentized.fixtures.getInstance();
        const challenge = frame.addMultipleChoiceChallenge().addDefaultReferences();
        const challengeViewModel = frame.createFrameViewModel().viewModelFor(challenge);
        return new ValidationResult(challengeViewModel, [], {
            event: {
                type: 'eventType',
            },
        });
    }

    function validationResultWithUserInputChallenge() {
        const frame = Componentized.fixtures.getInstance();
        const challenge = frame.addUserInputChallenge().addDefaultReferences();
        const challengeViewModel = frame.createFrameViewModel().viewModelFor(challenge);
        return new ValidationResult(challengeViewModel, [], {
            event: {
                type: 'eventType',
            },
        });
    }
});
