import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/componentized_fixtures';
import 'Lessons/angularModule/spec/_mock/fixtures/lessons';
import 'Editor/angularModule';

describe('Lesson::FrameListGrader', () => {
    let SpecHelper;
    let lesson;
    let Componentized;
    let MultipleChoiceMessageModel;
    let $timeout;
    const CRITICAL_ISSUE = 'CRITICAL_ISSUE';
    const POTENTIAL_ISSUE = 'POTENTIAL_ISSUE';
    const NOTE = 'NOTE';

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            $injector.get('LessonFixtures');
            $injector.get('ComponentizedFixtures');
            $timeout = $injector.get('$timeout');
            const FrameList = $injector.get('Lesson.FrameList');
            Componentized = $injector.get('Lesson.FrameList.Frame.Componentized');
            MultipleChoiceMessageModel = $injector.get(
                'Lesson.FrameList.Frame.Componentized.Component.MultipleChoiceMessage.MultipleChoiceMessageModel',
            );
            lesson = FrameList.fixtures.getInstance();
            lesson.frames.forEach(frame => {
                frame.reify();
            });
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    it('should penalize not having a tag', () => {
        lesson.tag = 'tag';
        assertIssue(CRITICAL_ISSUE, 'Lesson must have a tag.', () => {
            lesson.tag = undefined;
        });
    });

    describe('descriptions', () => {
        it('should penalize not having a description', () => {
            lesson.description = 'description';
            assertIssue(CRITICAL_ISSUE, 'Lesson must have a description.', () => {
                lesson.description = [];
            });
        });

        it('should penalize a description without a capital letter', () => {
            assertIssue(
                POTENTIAL_ISSUE,
                'Ensure that descriptions begin with a capital letter and end with a punctuation mark',
                () => {
                    Object.defineProperty(lesson, 'description', {
                        value: ['a description without a capital letter.'],
                    });
                },
            );
        });

        it('should penalize a description without ending punctuation', () => {
            assertIssue(
                POTENTIAL_ISSUE,
                'Ensure that descriptions begin with a capital letter and end with a punctuation mark',
                () => {
                    Object.defineProperty(lesson, 'description', {
                        value: ['A description without ending punctuation'],
                    });
                },
            );
        });

        it('should not penalize a proper description', () => {
            assertNoIssue(() => {
                Object.defineProperty(lesson, 'description', {
                    value: [
                        'A proper lesson description!',
                        'This is also a proper description.',
                        'And this one is too?',
                    ],
                });
            });
        });
    });

    describe('shortestPathLength', () => {
        it('should be required to be at least 8', () => {
            assertIssue(CRITICAL_ISSUE, 'Lesson length is non-optimal; must be from 8 to 22 screens long (7)', () => {
                Object.defineProperty(lesson, 'shortestPathLength', {
                    value: 7,
                });
            });
        });

        it('should be required to be no more than 22', () => {
            assertIssue(CRITICAL_ISSUE, 'Lesson length is non-optimal; must be from 8 to 22 screens long (23)', () => {
                Object.defineProperty(lesson, 'shortestPathLength', {
                    value: 23,
                });
            });
        });

        it('should penalize if lesson is one slide from maximum length', () => {
            assertIssue(POTENTIAL_ISSUE, 'Lesson length is close to the maximum allowed', () => {
                Object.defineProperty(lesson, 'shortestPathLength', {
                    value: 22,
                });
            });
        });

        it('should penalize if lesson is two slides from maximum length', () => {
            assertIssue(POTENTIAL_ISSUE, 'Lesson length is close to the maximum allowed', () => {
                Object.defineProperty(lesson, 'shortestPathLength', {
                    value: 21,
                });
            });
        });
    });

    describe('multiple choice or fill in the blanks', () => {
        it('should be okay with one multiple choice frame', () => {
            setEditorTemplate(lesson.frames[0], 'basic_multiple_choice');

            assertIssue(POTENTIAL_ISSUE, 'No multiple choice or fill in the blanks in this lesson.', () => {
                setEditorTemplate(lesson.frames[0], 'not_basic_multiple_choice');
            });
        });

        it('should be okay with one fill in the blanks frame', () => {
            setEditorTemplate(lesson.frames[0], 'fill_in_the_blanks');

            assertIssue(POTENTIAL_ISSUE, 'No multiple choice or fill in the blanks in this lesson.', () => {
                setEditorTemplate(lesson.frames[0], 'not_fill_in_the_blanks');
            });
        });
    });

    describe('poll, matching, this_or_that limit', () => {
        it('should have an issue with more than two this_or_that frames', () => {
            assertIssue(POTENTIAL_ISSUE, "More than two 'This or That?' screens (3)", () => {
                setEditorTemplate(lesson.frames[0], 'this_or_that');
                setEditorTemplate(lesson.frames[1], 'this_or_that');
                setEditorTemplate(lesson.frames[2], 'this_or_that');
            });
        });

        it('should have an issue with more than two matching frames', () => {
            assertIssue(POTENTIAL_ISSUE, "More than two 'Matching' screens (3)", () => {
                setEditorTemplate(lesson.frames[0], 'matching');
                setEditorTemplate(lesson.frames[1], 'matching');
                setEditorTemplate(lesson.frames[2], 'matching');
            });
        });

        it('should have an issue with more than two multiple_choice_poll frames', () => {
            assertIssue(POTENTIAL_ISSUE, "More than two 'Poll' screens (3)", () => {
                setEditorTemplate(lesson.frames[0], 'multiple_choice_poll');
                setEditorTemplate(lesson.frames[1], 'multiple_choice_poll');
                setEditorTemplate(lesson.frames[2], 'multiple_choice_poll');
            });
        });
    });

    describe('no_interaction limits', () => {
        beforeEach(() => {
            setEditorTemplate(lesson.frames[0], 'not_no_interaction');
            setEditorTemplate(lesson.frames[1], 'not_no_interaction');
            setEditorTemplate(lesson.frames[2], 'not_no_interaction');
        });

        it('should have an issue with more than 50% or 80% no_interaction frames', () => {
            assertIssue(POTENTIAL_ISSUE, 'More than 50% no interaction screens.', () => {
                setEditorTemplate(lesson.frames[0], 'no_interaction');
                setEditorTemplate(lesson.frames[1], 'no_interaction');
            });
        });

        it('should have an issue with more than 80% no_interaction frames', () => {
            assertIssue(POTENTIAL_ISSUE, 'More than 80% no interaction screens.', () => {
                setEditorTemplate(lesson.frames[0], 'no_interaction');
                setEditorTemplate(lesson.frames[1], 'no_interaction');
                setEditorTemplate(lesson.frames[2], 'no_interaction');
            });
        });
    });

    describe('basic_multiple_choice or fill_in_the_blanks limits', () => {
        it('should have an issue with 1 of 1 interactive screens as basic_multiple_choice', () => {
            assertIssue(
                POTENTIAL_ISSUE,
                'More than 80% of interactive screens are multiple choice or fill in the blanks.',
                () => {
                    setEditorTemplate(lesson.frames[0], 'basic_multiple_choice');
                },
            );
        });

        it('should have an issue with 1 of 1 interactive screens as fill_in_the_blanks', () => {
            assertIssue(
                POTENTIAL_ISSUE,
                'More than 80% of interactive screens are multiple choice or fill in the blanks.',
                () => {
                    setEditorTemplate(lesson.frames[0], 'fill_in_the_blanks');
                },
            );
        });

        it('should have an issue with 5 of 6 interactive screens as fill_in_the_blanks or basic_multiple_choice', () => {
            setEditorTemplate(lesson.frames[0], 'basic_multiple_choice');
            setEditorTemplate(lesson.frames[1], 'basic_multiple_choice');
            setEditorTemplate(lesson.frames[2], 'basic_multiple_choice');
            setEditorTemplate(lesson.addFrame(), 'fill_in_the_blanks');
            setEditorTemplate(lesson.addFrame(), 'other_interactive');
            setEditorTemplate(lesson.addFrame(), 'other_interactive');

            assertIssue(
                POTENTIAL_ISSUE,
                'More than 80% of interactive screens are multiple choice or fill in the blanks.',
                () => {
                    // since setEditorTemplate made this writable, we can just set it now
                    lesson.frames[4].editor_template = 'fill_in_the_blanks';
                },
            );
        });
    });

    it('should have an issue if same text bolded twice', () => {
        function defineText(frame, text) {
            Object.defineProperty(frame, 'text_content', {
                value: text,
            });
        }

        assertIssue(POTENTIAL_ISSUE, "'bolded' is bolded more than once in the lesson: frames 1, 2", () => {
            defineText(lesson.frames[0], 'a **bolded**');
            defineText(lesson.frames[1], 'a **bolded**');
        });
    });

    describe('same frame type twice in a row', () => {
        it('should have an issue if the same frame type is used more than twice in a row', () => {
            setEditorTemplate(lesson.frames[0], 'no_interaction');
            setEditorTemplate(lesson.frames[1], 'basic_multiple_choice');
            setEditorTemplate(lesson.frames[2], 'basic_multiple_choice');

            assertIssue(
                POTENTIAL_ISSUE,
                "'Multiple Choice' used 3 times in a row (2, 3, 4); avoid using a frametype more than twice in a row.",
                () => {
                    setEditorTemplate(lesson.addFrame(), 'basic_multiple_choice');
                },
            );
        });

        it('should have an issue if it happens in branching', () => {
            lesson.addFrame();
            setEditorTemplate(lesson.frames[0], 'no_interaction');
            setEditorTemplate(lesson.frames[1], 'not_no_interaction');
            setEditorTemplate(lesson.frames[2], 'no_interaction');
            setEditorTemplate(lesson.frames[3], 'no_interaction');

            assertIssue(
                POTENTIAL_ISSUE,
                "'No Interaction' used 3 times in a row (1, 3, 4); avoid using a frametype more than twice in a row.",
                () => {
                    Object.defineProperty(lesson, 'possiblePaths', {
                        value: [
                            {
                                frames: [lesson.frames[0], lesson.frames[2], lesson.frames[3]],
                                loops: false,
                            },
                        ],
                    });
                },
            );
        });

        it('should only have one issue even if there are many in a row', () => {
            lesson.addFrame();
            lesson.addFrame();
            lesson.addFrame();
            lesson.frames.forEach(frame => {
                setEditorTemplate(frame, 'no_interaction');
            });

            let count = 0;
            lesson.grade().then(grader => {
                grader.issues.forEach(issue => {
                    if (issue.identifier === 'same_frame_type_more_than_twice') {
                        count += 1;
                    }
                });
            });
            // flush the promise
            $timeout.flush();

            expect(count).toBe(1);
        });

        it('should only have one issue even if it happens in multiple branches', () => {
            lesson.addFrame();
            lesson.addFrame();
            lesson.addFrame();
            setEditorTemplate(lesson.frames[0], 'no_interaction');
            setEditorTemplate(lesson.frames[1], 'not_no_interaction');
            setEditorTemplate(lesson.frames[2], 'no_interaction');
            setEditorTemplate(lesson.frames[3], 'not_no_interaction');
            setEditorTemplate(lesson.frames[4], 'no_interaction');
            setEditorTemplate(lesson.frames[5], 'no_interaction');

            Object.defineProperty(lesson, 'possiblePaths', {
                value: [
                    {
                        frames: [lesson.frames[0], lesson.frames[2], lesson.frames[4]],
                        loops: false,
                    },
                    {
                        frames: [lesson.frames[0], lesson.frames[2], lesson.frames[5]],
                        loops: false,
                    },
                ],
            });

            let count = 0;
            lesson.grade().then(grader => {
                grader.issues.forEach(issue => {
                    if (issue.identifier === 'same_frame_type_more_than_twice') {
                        count += 1;
                    }
                });
            });
            // flush the promise
            $timeout.flush();

            expect(count).toBe(1);
        });
    });

    it('should have an issue if duplicate error messages', () => {
        lesson.frames = [];
        const frame = lesson.addFrame(Componentized.fixtures.getInstance());
        setEditorTemplate(frame, 'editor_template');
        frame.addMultipleChoiceMessage().addMessageText('a message');

        assertIssue(POTENTIAL_ISSUE, "Duplicate answer messages for frame 1. 'a message'", frame, () => {
            frame.addMultipleChoiceMessage().addMessageText('a message');
        });
    });

    describe('minimum message threshold', () => {
        beforeEach(() => {
            lesson.frames = [];
            for (let i = 1; i <= 10; i++) {
                const frame = lesson.addFrame(Componentized.fixtures.getInstance());
                setEditorTemplate(frame, 'editor_template');
                frame.addMultipleChoiceChallenge();
                frame.addMultipleChoiceMessage().addMessageText();
            }
        });

        it('should have an issue if more than 70% of messagable frames have no messages', () => {
            assertIssue(
                POTENTIAL_ISSUE,
                'Greater than 70% of interactive frames that could have messages do not (Frames 1, 2, 3, 4, 5, 6, 7, 8)',
                () => {
                    for (let i = 1; i <= 8; i++) {
                        const frame = lesson.frames[i - 1];
                        frame.componentsForType(MultipleChoiceMessageModel).forEach(c => {
                            c.remove();
                        });
                    }
                },
            );
        });

        it('should not treat multiple choice poll as messagable', () => {
            const frame = lesson.addFrame(Componentized.fixtures.getInstance());
            setEditorTemplate(frame, 'multiple_choice_poll');
            frame.addMultipleChoiceChallenge();
            frame.addMultipleChoiceMessage().addMessageText();

            // last frame is not included in the list of those without messages
            assertIssue(
                POTENTIAL_ISSUE,
                'Greater than 70% of interactive frames that could have messages do not (Frames 1, 2, 3, 4, 5, 6, 7, 8)',
                () => {
                    for (let i = 1; i <= 8; i++) {
                        const thisFrame = lesson.frames[i - 1];
                        thisFrame.componentsForType(MultipleChoiceMessageModel).forEach(c => {
                            c.remove();
                        });
                    }
                },
            );
        });
    });

    describe('punctuation within bold and italic validations', () => {
        it('should have an issue with punctuation inside of bold or italic', () => {
            assertIssue(
                POTENTIAL_ISSUE,
                "Frame 1 has punctuation inside of markdown: '**text.**', '*some more.*'",
                lesson.frames[0],
                () => {
                    Object.defineProperty(lesson.frames[0], 'text_content', {
                        value: 'some **text.** and *some more.*',
                    });
                },
            );
        });

        it('should not have an issue with a whole sentence inside of bold', () => {
            assertNoIssue(() => {
                Object.defineProperty(lesson.frames[0], 'text_content', {
                    value: 'A sentence. **And a whole sentence inside of bold.**',
                });
            });
        });

        it('should have an issue if preceded by a modal', () => {
            assertIssue(
                POTENTIAL_ISSUE,
                "Frame 1 has punctuation inside of markdown: '*and an italic.*'",
                lesson.frames[0],
                () => {
                    Object.defineProperty(lesson.frames[0], 'text_content', {
                        value: '[[A modal]] *and an italic.*',
                    });
                },
            );
        });

        it('should not have an issue with a whole sentence inside of italic', () => {
            assertNoIssue(() => {
                Object.defineProperty(lesson.frames[0], 'text_content', {
                    value: 'A sentence. *And a whole sentence inside of italic.*',
                });
            });
        });

        it('should not have an issue if the preceding sentence ends with an asterisk', () => {
            assertNoIssue(() => {
                Object.defineProperty(lesson.frames[0], 'text_content', {
                    value: '*A sentence.* *And a whole sentence inside of italic.*',
                });
            });
        });

        it('should not have an issue if the preceding sentence ends with a bracket', () => {
            assertNoIssue(() => {
                Object.defineProperty(lesson.frames[0], 'text_content', {
                    value: '[[A sentence.]] *And a whole sentence inside of italic.*',
                });
            });
        });
    });

    describe('colored text limits', () => {
        it('should have an issue if more than 30% of slides have colored text', () => {
            assertIssue(POTENTIAL_ISSUE, 'Overuse of colored text in this lesson (greater than 30% of frames)', () => {
                lesson.frames.forEach(frame => {
                    Object.defineProperty(frame, 'text_content', {
                        value: '{plum:asdasd}',
                    });
                });
            });
        });

        it('should have an issue with colored text in mathjax', () => {
            assertIssue(POTENTIAL_ISSUE, 'Overuse of colored text in this lesson (greater than 30% of frames)', () => {
                lesson.frames.forEach(frame => {
                    Object.defineProperty(frame, 'text_content', {
                        value: '%%textcolor{coral}{colorful!}%%',
                    });
                });
            });
        });
    });

    describe('consecutive question marks and exclamation points', () => {
        it('should have an issue with two question marks', () => {
            assertIssue(
                POTENTIAL_ISSUE,
                "Frame 1 has too many question marks and/or exclamation points in a row: '...is awesome??'",
                lesson.frames[0],
                () => {
                    Object.defineProperty(lesson.frames[0], 'text_content', {
                        value: 'Is this awesome??',
                    });
                },
            );
        });

        it('should have an issue with two exclamation points', () => {
            assertIssue(
                POTENTIAL_ISSUE,
                "Frame 1 has too many question marks and/or exclamation points in a row: '...is awesome!!'",
                lesson.frames[0],
                () => {
                    Object.defineProperty(lesson.frames[0], 'text_content', {
                        value: 'This is awesome!!',
                    });
                },
            );
        });

        it('should have an issue with of of each', () => {
            assertIssue(
                POTENTIAL_ISSUE,
                "Frame 1 has too many question marks and/or exclamation points in a row: '...ly awesome!?'",
                lesson.frames[0],
                () => {
                    Object.defineProperty(lesson.frames[0], 'text_content', {
                        value: 'Is this really awesome!?',
                    });
                },
            );
        });
    });

    describe('hyphen', () => {
        it('should have an issue if using a dash instead of emdash', () => {
            assertIssue(
                POTENTIAL_ISSUE,
                "Frame 1 uses a hyphen (-) instead of an emdash (—): '...this - looks...'",
                lesson.frames[0],
                () => {
                    Object.defineProperty(lesson.frames[0], 'text_content', {
                        value: 'this - looks awful',
                    });
                },
            );
        });

        it('should not have an issue if using a dash instead of emdash within MahtJax', () => {
            assertNoIssue(() => {
                Object.defineProperty(lesson.frames[0], 'text_content', {
                    value: '%%this - looks awful%%',
                });
            });
            assertNoIssue(() => {
                Object.defineProperty(lesson.frames[0], 'text_content', {
                    value: '$$this - looks awful$$',
                });
            });
        });

        it('should not have an issue if creating a markdown list', () => {
            assertNoIssue(() => {
                Object.defineProperty(lesson.frames[0], 'text_content', {
                    value: '- item 1',
                });
            });

            assertNoIssue(() => {
                Object.defineProperty(lesson.frames[0], 'text_content', {
                    value: ' - item 1',
                });
            });

            assertNoIssue(() => {
                Object.defineProperty(lesson.frames[0], 'text_content', {
                    value: '\n\n - item 1',
                });
            });
        });

        it('should have an issue if a markdown list item uses a hyphen', () => {
            assertIssue(
                POTENTIAL_ISSUE,
                "Frame 1 uses a hyphen (-) instead of an emdash (—): '...list - also...'",
                lesson.frames[0],
                () => {
                    Object.defineProperty(lesson.frames[0], 'text_content', {
                        value: '- a list - also with a hyphen',
                    });
                },
            );
        });
    });

    describe('image quota', () => {
        beforeEach(() => {
            lesson.frames = [];
            for (let i = 0; i < 2; i++) {
                const frame = lesson.addFrame(Componentized.fixtures.getInstance());
                setEditorTemplate(frame, 'no_interaction');
                frame.mainUiComponent = frame.addTextImageInteractiveLayout();
                frame.mainUiComponent.staticContentForFirstImage = frame.addImage();
                frame.mainUiComponent.staticContentForSecondImage = frame.addImage();
            }
        });

        it('should have an issue if not enough frames have used images', () => {
            assertIssue(POTENTIAL_ISSUE, 'Too few slides with images in this lesson (less than 2)', () => {
                lesson.frames[0].mainUiComponent.staticContentForFirstImage.remove();
                lesson.frames[0].mainUiComponent.staticContentForSecondImage.remove();
            });
        });

        it('should not count unused images', () => {
            assertIssue(POTENTIAL_ISSUE, 'Too few slides with images in this lesson (less than 2)', () => {
                lesson.frames[0].mainUiComponent.staticContentForSecondImage = undefined;
                lesson.frames[0].mainUiComponent.staticContentForFirstImage = undefined;
            });
        });

        it('should not count inline images', () => {
            assertIssue(POTENTIAL_ISSUE, 'Too few slides with images in this lesson (less than 2)', () => {
                const image = lesson.frames[0].mainUiComponent.staticContentForFirstImage;
                lesson.frames[0].mainUiComponent.staticContentForSecondImage = undefined;
                lesson.frames[0].mainUiComponent.staticContentForFirstImage = undefined;
                lesson.frames[0].text_content = `an inline image: ![${image.label}]`;
            });
        });
    });

    describe('compose limits', () => {
        it('should have an issue with more than 40% compose frames', () => {
            lesson.frames = [];
            setEditorTemplate(lesson.addFrame(), 'no_interaction');
            setEditorTemplate(lesson.addFrame(), 'no_interaction');
            setEditorTemplate(lesson.addFrame(), 'compose_blanks');
            setEditorTemplate(lesson.addFrame(), 'compose_blanks');
            setEditorTemplate(lesson.addFrame(), 'basic_multiple_choice');
            setEditorTemplate(lesson.addFrame(), 'basic_multiple_choice');
            setEditorTemplate(lesson.addFrame(), 'basic_multiple_choice');

            assertIssue(
                POTENTIAL_ISSUE,
                'Too many typing screens for this lesson (greater than 40% of interactive slides)',
                () => {
                    setEditorTemplate(lesson.addFrame(), 'compose_blanks');
                },
            );
        });
    });

    describe('UserInputChallenge expectedText limits', () => {
        it('should have an issue with user input challenge expected text more the 20 characters long', () => {
            lesson.frames = [];
            const frame = lesson.addFrame(Componentized.fixtures.getInstance());
            setEditorTemplate(frame, 'editor_template');
            const challenge = frame.addUserInputChallenge().addDefaultReferences();
            challenge.editorViewModel.correctAnswerText = 'nice and short';

            const text = 'abcdefghigjklmnopqrstuvwxyz';
            assertIssue(
                POTENTIAL_ISSUE,
                `Frame 1 requires text to type that is longer than 20 characters: '${text}'`,
                frame,
                () => {
                    challenge.editorViewModel.correctAnswerText = text;
                },
            );
        });
    });

    it('should have an issue if lesson does not start with a no_interaction screen', () => {
        setEditorTemplate(lesson.frames[0], 'no_interaction');
        assertIssue(NOTE, "Does not begin with 'No Interaction' (begins with 'something_else')", () => {
            setEditorTemplate(lesson.frames[0], 'something_else');
        });
    });

    it('should have an issue if max text length warning', () => {
        lesson.frames = [];
        const frame = lesson.addFrame(Componentized.fixtures.getInstance());
        setEditorTemplate(frame, 'editor_template');

        const textModel = frame.addSelectableAnswer({
            text: 'abc',
        }).text;

        jest.spyOn(textModel.editorViewModel, 'maxRecommendedTextLength').mockReturnValue(12);

        assertIssue(
            NOTE,
            "Frame 1 has text that exceeds recommended character limits: '12345678...'",
            lesson.frames[0],
            () => {
                textModel.formatted_text = '<p>1234567890123</p>';
                textModel.text = '1234567890123';
            },
        );
    });

    describe('bolded text in disallowed places', () => {
        let message;
        let modal;

        beforeEach(() => {
            lesson.frames = [];
            const frame = lesson.addFrame(Componentized.fixtures.getInstance());
            setEditorTemplate(frame, 'editor_template');
            const textModel = frame.addText();
            modal = frame.addText();
            textModel.modals = [modal];
            message = frame.addMultipleChoiceMessage().addDefaultReferences();
        });

        it('should have an issue if bolded text in a modal', () => {
            assertIssue(
                POTENTIAL_ISSUE,
                'Do not introduce key terms in message or modals (frame 1)',
                lesson.frames[0],
                () => {
                    modal.formatted_text = '<p>this is <strong>bolded</strong></p>';
                    modal.text = 'this is **bolded**';
                },
            );
        });

        it('should have an issue if bolded text in a message', () => {
            assertIssue(
                POTENTIAL_ISSUE,
                'Do not introduce key terms in message or modals (frame 1)',
                lesson.frames[0],
                () => {
                    message.messageText.text = 'this is **bolded**';
                },
            );
        });
    });

    describe('duplicated words', () => {
        let modal;

        beforeEach(() => {
            lesson.frames = [];
            const frame = lesson.addFrame(Componentized.fixtures.getInstance());
            setEditorTemplate(frame, 'editor_template');
            const textModel = frame.addText();
            modal = frame.addText();
            textModel.modals = [modal];
            frame.addMultipleChoiceMessage().addDefaultReferences();
        });

        it('should have an issue when duplicating a word', () => {
            assertIssue(
                CRITICAL_ISSUE,
                'Frame 1 has duplicated words: ...this this, ...is is, ...the the',
                lesson.frames[0],
                () => {
                    modal.formatted_text = 'this this is is the the worst.';
                    modal.text = 'this this is is the the worst.';
                },
            );
        });
    });

    function assertIssue(expectedType, expectedMessage, expectedFrame, fn) {
        if (arguments.length === 3) {
            fn = expectedFrame;
            expectedFrame = undefined;
        }
        let result = checkForIssue(expectedType, expectedMessage, expectedFrame);
        if (result.hasMatchingIssue) {
            throw new Error('Expected issue already exists before running the given code');
        }
        fn();
        result = checkForIssue(expectedType, expectedMessage, expectedFrame);
        if (!result.hasMatchingIssue) {
            const messages = result.grader.issues.map(issue => `${issue.type}: ${issue.message}`);
            throw new Error(`Expected issue not found: ${expectedMessage}. ISSUES: ${messages.join(' ---- ')}`);
        }
    }

    function assertNoIssue(fn) {
        const issues = {};
        lesson.grade().then(grader => {
            grader.issues.forEach(issue => {
                const key = `${issue.type}: ${issue.message}`;
                issues[key] = true;
            });
        });
        // flush the promise
        $timeout.flush();

        fn();
        const newIssues = [];

        lesson.grade().then(grader => {
            grader.issues.forEach(issue => {
                const key = `${issue.type}: ${issue.message}`;
                if (!issues[key]) {
                    newIssues.push(key);
                }
            });
        });
        // flush the promise
        $timeout.flush();

        if (newIssues.length > 0) {
            throw new Error(`New issues found: ${newIssues.join(' ---- ')}`);
        }
    }

    function checkForIssue(expectedType, expectedMessage, expectedFrame) {
        let found = false;
        let _grader;
        lesson.grade().then(grader => {
            _grader = grader;
            grader.issues.forEach(issue => {
                if (issue.type === expectedType && issue.message === expectedMessage) {
                    found = true;
                    SpecHelper.expectEqual(expectedFrame, issue.frame, `frame for '${expectedMessage}'`);
                }
            });
        });
        // flush the promise
        $timeout.flush();
        return {
            hasMatchingIssue: found,
            grader: _grader,
        };
    }

    function setEditorTemplate(frame, editorTemplate) {
        // using writable instead of just value because I was
        // having trouble changing the value
        Object.defineProperty(frame, 'editor_template', {
            writable: true,
        });
        frame.editor_template = editorTemplate;
    }
});
