import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/componentized_fixtures';
import 'Lessons/angularModule/spec/_helpers/componentized_spec_helper';
import 'Editor/angularModule';

describe('Componentized.Component.Mixins.HasTextOrImage', () => {
    let frame;
    let viewModel;
    let MyModel;
    let Componentized;
    let SpecHelper;
    let ComponentizedSpecHelper;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                Componentized = $injector.get('Lesson.FrameList.Frame.Componentized');
                SpecHelper = $injector.get('SpecHelper');
                $injector.get('ComponentizedFixtures');
                ComponentizedSpecHelper = $injector.get('ComponentizedSpecHelper');
                const UiComponentModel = $injector.get(
                    'Lesson.FrameList.Frame.Componentized.Component.UiComponent.UiComponentModel',
                );
                const HasTextOrImage = $injector.get(
                    'Lesson.FrameList.Frame.Componentized.Component.Mixins.HasTextOrImage',
                );
                const HasTextOrImageEditorViewModelMixin = $injector.get(
                    'Lesson.FrameList.Frame.Componentized.Component.Mixins.HasTextOrImageEditorViewModelMixin',
                );

                MyModel = UiComponentModel.subclass(function () {
                    this.alias('ComponentizedFrame.MyModel');
                    this.extend({
                        ViewModel: UiComponentModel.ViewModel.subclass(),
                    });
                    this.setEditorViewModel('Lesson.FrameList.Frame.Componentized.Component.ComponentEditorViewModel');

                    this.include(HasTextOrImage);

                    return {};
                });

                Object.defineProperty(MyModel.EditorViewModel, 'Model', {
                    get() {
                        return MyModel;
                    },
                });
                MyModel.EditorViewModel.include(HasTextOrImageEditorViewModelMixin);

                frame = Componentized.fixtures.getInstance();
                viewModel = getViewModel();
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('Model', () => {
        describe('contentType', () => {
            it('should return image if there is one', () => {
                viewModel.model.image = frame.addImage();
                expect(viewModel.model.contentType).toEqual('image');
            });
            it('should return text if no image', () => {
                expect(viewModel.model.contentType).toEqual('text');
            });
        });
        describe('label', () => {
            it('should return image label', () => {
                viewModel.model.image = frame.addImage();
                expect(viewModel.model.label).toEqual(viewModel.model.image.label);
            });
            it('should return text', () => {
                expect(viewModel.model.label).toEqual(viewModel.model.text.text);
            });
        });
        describe('restrictAspectRatio', () => {
            it('should be true when there is an image', () => {
                viewModel.model.image = frame.addImage();
                expect(viewModel.model.restrictAspectRatio).toBe(true);
            });
            it('should be false when there is text', () => {
                expect(viewModel.model.contentType).toEqual('text');
                expect(viewModel.model.restrictAspectRatio).toBe(false);
            });
        });
        describe('contentEqualTo', () => {
            let thisOne;
            let thatOne;
            beforeEach(() => {
                thisOne = getViewModel().model;
                thatOne = getViewModel().model;
            });
            it('should be true when text is the same', () => {
                thisOne.text.text = thatOne.text.text = 'ok';
                expect(thisOne.contentEqualTo(thatOne)).toBe(true);
            });
            it('should be false when text is different', () => {
                thisOne.text.text = 'nope';
                thatOne.text.text = 'ok';
                expect(thisOne.contentEqualTo(thatOne)).toBe(false);
            });
            it('should be true when image is the same', () => {
                const image = frame.addImage();
                thisOne.image = image;
                thatOne.image = image;
                expect(thisOne.contentEqualTo(thatOne)).toBe(true);
            });
            it('should be false when image is different', () => {
                thisOne.image = frame.addImage();
                thatOne.image = frame.addImage();
                expect(thisOne.contentEqualTo(thatOne)).toBe(false);
            });
            it('should be false when this is image and that is text', () => {
                thisOne.image = frame.addImage();
                thatOne.text = frame.addText();
                expect(thisOne.contentEqualTo(thatOne)).toBe(false);
            });
            it('should be false when this is text and that is image', () => {
                thisOne.text = frame.addText();
                thatOne.image = frame.addImage();
                expect(thisOne.contentEqualTo(thatOne)).toBe(false);
            });
        });
        describe('remove', () => {
            it('should remove a blank image', () => {
                const image = (viewModel.model.image = frame.addImage());
                viewModel.model.image.image = undefined;
                jest.spyOn(image, 'remove').mockImplementation(() => {});
                viewModel.model.remove();
                expect(image.remove).toHaveBeenCalled();
            });
            it('should not remove a non-blank image', () => {
                const image = (viewModel.model.image = frame.addImage());
                expect(image.image).not.toBeUndefined(); // sanity check
                jest.spyOn(image, 'remove').mockImplementation(() => {});
                viewModel.model.remove();
                expect(image.remove).not.toHaveBeenCalled();
            });
        });
        it('should remove text when image is set', () => {
            expect(viewModel.model.text).not.toBeUndefined();
            viewModel.model.image = frame.addImage();
            expect(viewModel.model.text).toBeUndefined();
        });
        it('should remove image when text is set', () => {
            viewModel.model.image = frame.addImage();
            viewModel.model.text = frame.addText();
            expect(viewModel.model.image).toBeUndefined();
        });
        it('should add text when image is removed', () => {
            viewModel.model.image = frame.addImage();
            viewModel.model.image.remove();
            expect(viewModel.model.text).not.toBeUndefined();
        });
    });

    describe('ViewModel', () => {});

    describe('EditorViewModel', () => {
        describe('toggleContentType', () => {
            it('should remove text and add image when set to image', () => {
                const helper = MyModel.EditorViewModel.addComponentTo(frame).setup();
                helper.model.text = frame.addText();

                SpecHelper.expectEqual('text', helper.model.contentType, 'contentType');
                SpecHelper.expectEqual(true, !!helper.model.text, 'text defined');
                SpecHelper.expectEqual(false, !!helper.model.image, 'image defined');

                helper.toggleContentType();
                SpecHelper.expectEqual('image', helper.model.contentType, 'contentType');
                SpecHelper.expectEqual(false, !!helper.model.text, 'text defined');
                SpecHelper.expectEqual(true, !!helper.model.image, 'image defined');
            });

            it('should remove image and add text when set to text', () => {
                const helper = MyModel.EditorViewModel.addComponentTo(frame).setup();
                helper.toggleContentType();
                SpecHelper.expectEqual('image', helper.model.contentType, 'contentType');
                SpecHelper.expectEqual(false, !!helper.model.text, 'text defined');
                SpecHelper.expectEqual(true, !!helper.model.image, 'image defined');

                helper.toggleContentType();
                SpecHelper.expectEqual('text', helper.model.contentType, 'contentType');
                SpecHelper.expectEqual(true, !!helper.model.text, 'text defined');
                SpecHelper.expectEqual(false, !!helper.model.image, 'image defined');
            });

            it('should delete blank image when switching to text', () => {
                const helper = MyModel.EditorViewModel.addComponentTo(frame).setup();
                helper.toggleContentType();
                SpecHelper.expectEqual('image', helper.model.contentType, 'contentType');
                const image = helper.model.image;
                expect(image.image).toBeUndefined(); // sanity check
                jest.spyOn(image, 'remove').mockImplementation(() => {});

                helper.toggleContentType();
                expect(image.remove).toHaveBeenCalled();
            });

            it('should not delete image that is not blank when switching to text', () => {
                const helper = MyModel.EditorViewModel.addComponentTo(frame).setup();
                helper.toggleContentType();
                SpecHelper.expectEqual('image', helper.model.contentType, 'contentType');
                const image = helper.model.image;
                image.image = {};
                jest.spyOn(image, 'remove').mockImplementation(() => {});

                helper.toggleContentType();
                expect(image.remove).not.toHaveBeenCalled();
            });
        });

        describe('setContentType', () => {
            it('should do nothing if changing from text to text', () => {
                const helper = MyModel.EditorViewModel.addComponentTo(frame).setup();
                const text = (helper.model.text = frame.addText());
                expect(text).not.toBeUndefined();
                helper.setContentType('text');
                expect(text).toBe(helper.model.text);
            });
            it('should do nothing if changing from image to image', () => {
                const helper = MyModel.EditorViewModel.addComponentTo(frame).setup();
                const image = (helper.model.image = frame.addImage());
                expect(image).not.toBeUndefined();
                helper.setContentType('image');
                expect(image).toBe(helper.model.image);
            });
            it('should change from image to text', () => {
                const helper = MyModel.EditorViewModel.addComponentTo(frame).setup();
                helper.model.image = frame.addImage();
                helper.setContentType('text');
                expect(helper.model.contentType).toBe('text');
            });
            it('should change from text to image', () => {
                const helper = MyModel.EditorViewModel.addComponentTo(frame).setup();
                helper.model.text = frame.addText();
                helper.setContentType('image');
                expect(helper.model.contentType).toBe('image');
            });
        });
    });

    describe('cf-has-text-or-image-editor', () => {
        let elem;
        let scope;
        let model;
        let editorViewModel;

        beforeEach(() => {
            render();
        });

        describe('contentType', () => {
            it('should be settable to image', () => {
                SpecHelper.expectEqual('text', model.contentType);
                assertToggleContentTypeFrom('text', () => {
                    SpecHelper.click(elem, '.text[name="toggle_content_type"]');
                });
            });
            it('should be settable to text', () => {
                model.image = frame.addImage();
                scope.$digest();
                assertToggleContentTypeFrom('image', () => {
                    SpecHelper.click(elem, '.image[name="toggle_content_type"]');
                    model.text = frame.addText();
                    scope.$digest();
                    ComponentizedSpecHelper.assertEditorElementPresent(elem, model.text);
                });
            });

            it('should not be settable if disallowSwitchContentType is set', () => {
                render({
                    disallowSwitchContentType: true,
                });
                scope.$digest();
                SpecHelper.expectElementDisabled(elem, 'button[name="content_type"]');
            });
        });
        it('should show text editor', () => {
            ComponentizedSpecHelper.assertEditorElementPresent(elem, model.text);
        });

        it('should show image editor', () => {
            model.image = frame.addImage();
            scope.$digest();
            ComponentizedSpecHelper.assertEditorElementPresent(elem, model.image);
        });
        it('should be possible to disable image editor', () => {
            model.image = frame.addImage();
            scope.$digest();
            SpecHelper.expectElementEnabled(elem, 'button[name="content_type"]');
            SpecHelper.expectElementEnabled(elem, 'select[name="current_image"]');
            SpecHelper.expectEqual(undefined, elem.find('.cf-image-editor').scope().disabled);
            scope.disabled = true;
            scope.$digest();
            SpecHelper.expectElementDisabled(elem, 'button[name="content_type"]');
            SpecHelper.expectElementDisabled(elem, 'select[name="current_image"]');
            SpecHelper.expectEqual(true, elem.find('.cf-image-editor').scope().disabled);
        });
        it('should be possible to disable text editor', () => {
            SpecHelper.expectElementEnabled(elem, 'button[name="content_type"]');
            SpecHelper.expectEqual(undefined, elem.find('.cf-text-editor').scope().disabled);
            scope.disabled = true;
            scope.$digest();
            SpecHelper.expectElementDisabled(elem, 'button[name="content_type"]');
            SpecHelper.expectEqual(true, elem.find('.cf-text-editor').scope().disabled);
        });

        describe('image swap', () => {
            let existingImage;

            beforeEach(() => {
                existingImage = frame.addImage();
                existingImage.label = 'existing image';
            });

            it('should allow for selecting an image', () => {
                SpecHelper.click(elem, '.text[name="toggle_content_type"]');
                expect(model.image).not.toBe(existingImage);
                expect(model.image).not.toBeUndefined();
                SpecHelper.selectOptionByLabel(elem, 'select', '(1) existing image');
                expect(model.image).toBe(existingImage);
            });
        });

        function render(options) {
            const renderer = SpecHelper.renderer();
            renderer.scope.editorViewModel = frame.editorViewModelFor(viewModel.model);
            renderer.scope.frameViewModel = frame.createFrameViewModel();
            renderer.scope.options = options;
            renderer.render(
                '<cf-has-text-or-image-editor frame-view-model="frameViewModel" editor-view-model="editorViewModel" options="options"></cf-has-text-or-image-editor>',
            );
            elem = renderer.elem;
            scope = renderer.childScope;
            model = viewModel.model;
            editorViewModel = renderer.scope.editorViewModel;
        }

        function assertToggleContentTypeFrom(initialType, block) {
            SpecHelper.expectEqual(initialType, model.contentType);
            jest.spyOn(editorViewModel, 'toggleContentType').mockImplementation(() => {});
            block();
            expect(editorViewModel.toggleContentType).toHaveBeenCalled();
        }
    });

    function getViewModel(options) {
        viewModel = getModel(options).createViewModel(frame.createFrameViewModel());
        return viewModel;
    }

    function getModel() {
        const model = MyModel.EditorViewModel.addComponentTo(frame).setup().model;
        model.text = frame.addText();
        return model;
    }
});
