import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/componentized_fixtures';
import 'Editor/angularModule';

describe('Componentized.Component.FrameNavigator', () => {
    let frame;
    let frameViewModel;
    let viewModel;
    let model;
    let Componentized;
    let SpecHelper;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                Componentized = $injector.get('Lesson.FrameList.Frame.Componentized');
                SpecHelper = $injector.get('SpecHelper');
                $injector.get('ComponentizedFixtures');
                viewModel = getViewModel();
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('Model', () => {
        describe('mightNavigateTo', () => {
            let frames;

            beforeEach(() => {
                frame.lesson().addFrame(Componentized.fixtures.getInstance());
                frame.lesson().addFrame(Componentized.fixtures.getInstance());
                frames = frame.lesson().frames;

                frame.addSelectableAnswerNavigator();
                expect(model.selectableAnswerNavigators.length).toBe(1);

                // default this to undefined
                model.next_frame_id = undefined;
            });

            it('should be true if a selectable answer navigates to provided frame', () => {
                model.selectableAnswerNavigators[0].next_frame_id = frames[2].id;
                expect(model.mightNavigateTo(frames[1])).toBe(false);
                expect(model.mightNavigateTo(frames[2])).toBe(true);
            });

            it('should be false for the next frame if a selectable answer navigates to a different one', () => {
                model.selectableAnswerNavigators[0].next_frame_id = frames[2].id;
                expect(model.mightNavigateTo(frames[1])).toBe(false);
            });

            it('should be false if a selectable answer navigator navigates elsewhere', () => {
                model.selectableAnswerNavigators[0].next_frame_id = frames[1].id;
                expect(model.mightNavigateTo(frames[2])).toBe(false);
            });

            it('should default to next frame if a selectable answer uses the default', () => {
                model.selectableAnswerNavigators[0].next_frame_id = undefined;
                expect(model.mightNavigateTo(frames[1])).toBe(true);
                expect(model.mightNavigateTo(frames[2])).toBe(false);
            });

            it('should be true for the frame that matches next_frame_id', () => {
                model.selectableAnswerNavigators = [];
                model.next_frame_id = frames[2].id;
                expect(model.mightNavigateTo(frames[1])).toBe(false);
                expect(model.mightNavigateTo(frames[2])).toBe(true);
            });

            it('should be true for the next frame if no overrides are set', () => {
                model.selectableAnswerNavigators = [];
                model.next_frame_id = undefined;
                expect(model.mightNavigateTo(frames[1])).toBe(true);
                expect(model.mightNavigateTo(frames[2])).toBe(false);
            });
        });
    });

    describe('ViewModel', () => {
        describe('nextFrameId', () => {
            it('should default to models next_frame_id', () => {
                viewModel.model.next_frame_id = 'next_frame_id';
                expect(viewModel.nextFrameId).toBe(viewModel.model.next_frame_id);
            });

            it('can be set to something else', () => {
                viewModel.model.next_frame_id = 'next_frame_id';
                viewModel.nextFrameIdOverride = 'other_frame_id';
                expect(viewModel.nextFrameId).toBe('other_frame_id');
            });

            it('can default to next frame', () => {
                viewModel.model.next_frame_id = undefined;
                viewModel.nextFrameIdOverride = 'other_frame_id';
                expect(viewModel.nextFrameId).toBe('other_frame_id');
            });
        });
    });

    describe('EditorViewModel', () => {
        let editorViewModel;

        beforeEach(() => {
            editorViewModel = frame.editorViewModelFor(viewModel.model);
            Object.defineProperty(editorViewModel, 'challenges', {
                value: [frame.addMultipleChoiceChallenge().addDefaultReferences()],
            });
        });

        it('ensureSelectableAnswerNavigatorFor should create a navigator if one doesnt already exist', () => {
            expect(viewModel.model.selectableAnswerNavigators.length).toBe(0);
            editorViewModel.ensureSelectableAnswerNavigatorFor(
                editorViewModel.challenges[0],
                editorViewModel.challenges[0].answers[0],
                'gotoLastSelectedWhenMovingOnFromFrame',
            );
            expect(viewModel.model.selectableAnswerNavigators.length).toBe(1);
            editorViewModel.ensureSelectableAnswerNavigatorFor(
                editorViewModel.challenges[0],
                editorViewModel.challenges[0].answers[0],
                'gotoLastSelectedWhenMovingOnFromFrame',
            );
            expect(viewModel.model.selectableAnswerNavigators.length).toBe(1);
        });

        it('can addSelectableAnswerNavigatorFor', () => {
            expect(viewModel.model.selectableAnswerNavigators.length).toBe(0);
            editorViewModel.addSelectableAnswerNavigatorFor(
                editorViewModel.challenges[0],
                editorViewModel.challenges[0].answers[0],
                'gotoLastSelectedWhenMovingOnFromFrame',
            );
            expect(viewModel.model.selectableAnswerNavigators.length).toBe(1);
            expect(
                editorViewModel.hasSelectableAnswerNavigatorFor(
                    editorViewModel.challenges[0],
                    editorViewModel.challenges[0].answers[0],
                    'gotoLastSelectedWhenMovingOnFromFrame',
                ),
            ).toBe(true);
            expect(viewModel.model.selectableAnswerNavigators[0].next_frame_id).toBeUndefined();
        });
    });

    describe('cf-frame-navigator-editor', () => {
        let elem;
        let scope;
        let editorViewModel;

        beforeEach(() => {
            editorViewModel = frame.editorViewModelFor(viewModel.model);
            Object.defineProperty(editorViewModel, 'challenges', {
                value: [frame.addMultipleChoiceChallenge().addDefaultReferences()],
            });
            render();
        });

        it('should be able to set next_frame_id on a selectable answer navigator', () => {
            const frame2 = Componentized.fixtures.getInstance();
            frame.lesson().addFrame(frame2);
            scope.$digest();
            SpecHelper.selectOptionByLabel(elem.find('cf-next-frame-selector').eq(0), 'select', frame2.label);
            expect(viewModel.model.selectableAnswerNavigators[0].next_frame_id).toBe(frame2.id);
        });

        it('should set the default frame in the selector', () => {
            const frame2 = Componentized.fixtures.getInstance();
            frame.lesson().addFrame(frame2);
            scope.$digest();
            expect(elem.find('cf-next-frame-selector').isolateScope().defaultFrame).toBeUndefined();
            editorViewModel.model.next_frame_id = frame2.id;
            scope.$digest();
            expect(elem.find('cf-next-frame-selector').isolateScope().defaultFrame).toBe(frame2);
        });

        function render(options) {
            const renderer = SpecHelper.renderer();
            renderer.scope.editorViewModel = editorViewModel;
            renderer.scope.options = options || {};
            renderer.render(
                '<cf-component-editor editor-view-model="editorViewModel" options="options"></cf-component-editor>',
            );
            elem = renderer.elem;
            scope = elem.find('[editor-view-model]').isolateScope();
        }
    });

    function getViewModel(options) {
        model = getModel(options);
        return frameViewModel.viewModelFor(model);
    }

    function getModel() {
        frame = Componentized.fixtures.getInstance();
        frameViewModel = frame.createFrameViewModel();
        return frame.frameNavigator;
    }
});
