import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/componentized_fixtures';
import 'Editor/angularModule';

import { markdown } from 'Markdown';

describe('Componentized.Component.Text', () => {
    let frame;
    let frameViewModel;
    let viewModel;
    let model;
    let Componentized;
    let SpecHelper;
    let $window;
    let ChallengeModel;
    let MaxTextLengthConfig;
    let UserInputChallengeModel;
    let MultipleChoiceChallengeModel;
    let $timeout;
    let ErrorLogService;
    let $q;
    let $injector;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                Componentized = $injector.get('Lesson.FrameList.Frame.Componentized');
                SpecHelper = $injector.get('SpecHelper');
                ChallengeModel = $injector.get(
                    'Lesson.FrameList.Frame.Componentized.Component.Challenge.ChallengeModel',
                );
                UserInputChallengeModel = $injector.get(
                    'Lesson.FrameList.Frame.Componentized.Component.Challenge.UserInputChallenge.UserInputChallengeModel',
                );
                MultipleChoiceChallengeModel = $injector.get(
                    'Lesson.FrameList.Frame.Componentized.Component.Challenge.MultipleChoiceChallenge.MultipleChoiceChallengeModel',
                );
                $injector.get('ComponentizedFixtures');
                $window = $injector.get('$window');
                $timeout = $injector.get('$timeout');
                $q = $injector.get('$q');
                viewModel = getViewModel();
                MaxTextLengthConfig = $injector.get('MaxTextLengthConfig');
                ErrorLogService = $injector.get('ErrorLogService');
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('Model', () => {
        describe('formatText', () => {
            let editorViewModel;
            let resolveFormatting;
            let resolvedCount;

            beforeEach(() => {
                model = viewModel.model;
                editorViewModel = viewModel.model.editorViewModel;
                $timeout.flush();
                resolvedCount = 0;
                jest.spyOn(editorViewModel.formatters, 'process').mockImplementation(() =>
                    $q(resolve => {
                        resolveFormatting = resolve;
                    }),
                );
            });

            it('should format text once if formatText called again without changing text', () => {
                model.text = 'original text';

                // call formatText once
                editorViewModel.formatText().then(() => {
                    expect(model.formatted_text).toEqual('formattedText');
                    resolvedCount += 1;
                });

                // resolve the promise and assert that the callback was called
                resolveFormatting('formattedText');
                $timeout.flush(); // flush promise resolution
                expect(resolvedCount).toBe(1); // callback was called
                expect(editorViewModel.formatters.process.mock.calls.length).toBe(1); // formatters.process was called

                // call formatText again.  Assert that the callback was called but
                // that formatters.process was not called again.  Also, we should not
                // need to call resolveFormatting again, since the promise is already resolved.
                editorViewModel.formatText().then(() => {
                    expect(model.formatted_text).toEqual('formattedText');
                    resolvedCount += 1;
                });
                $timeout.flush(); // flush promise resolution
                expect(resolvedCount).toBe(2); // callback was called
                expect(editorViewModel.formatters.process.mock.calls.length).toBe(1); // formatters.process was not called again
            });

            it('should re-format when text changes', () => {
                // call formatText once
                model.text = 'original text';
                editorViewModel.formatText().then(() => {
                    expect(model.formatted_text).toEqual('formattedText1');
                    resolvedCount += 1;
                });

                // resolve the promise and assert that the callback was called
                resolveFormatting('formattedText1');
                $timeout.flush(); // flush promise resolution
                expect(resolvedCount).toBe(1); // callback was called
                expect(editorViewModel.formatters.process.mock.calls.length).toBe(1); // formatters.process was called

                // Change the text and call formatText again.  We should re-process
                // and call the callback
                model.text = 'new text';
                editorViewModel.formatText().then(() => {
                    expect(model.formatted_text).toEqual('formattedText2');
                    resolvedCount += 1;
                });
                resolveFormatting('formattedText2');
                $timeout.flush(); // flush promise resolution
                expect(resolvedCount).toBe(2); // callback was called
                expect(editorViewModel.formatters.process.mock.calls.length).toBe(2); // formatters.process was not called again
            });

            it('should format text once if formatText called again while formatting is in process', () => {
                // call formatText once
                model.text = 'original text';
                editorViewModel.formatText().then(() => {
                    expect(model.formatted_text).toEqual('formattedText');
                    resolvedCount += 1;
                });

                // before the promise is resolved, call
                // formatText again
                editorViewModel.formatText().then(() => {
                    expect(model.formatted_text).toEqual('formattedText');
                    resolvedCount += 1;
                });

                // resolve the promise and assert that both callbacks were called
                resolveFormatting('formattedText');
                $timeout.flush(); // flush promise resolution
                expect(resolvedCount).toBe(2); // both callbacks were called
                expect(editorViewModel.formatters.process.mock.calls.length).toBe(1); // formatters.process was called only once
            });
        });
    });
    describe('ViewModel', () => {});

    describe('behaviors', () => {
        function formatText() {
            let formatted;
            let formattedText;
            viewModel.model.editorViewModel.formatText().then(() => {
                formatted = true;
                formattedText = viewModel.model.formatted_text;
            });
            $timeout.flush();
            expect(formatted).toBe(true);
            return formattedText;
        }

        describe('ProcessesMarkdown', () => {
            describe('Editor', () => {
                it('should do markdown processing', () => {
                    viewModel = getViewModel({
                        behaviors: {
                            ProcessesMarkdown: {},
                        },
                        text: 'this is some _markdown_ formatted text without mathjax',
                    });
                    jest.spyOn(markdown, 'toHTML').mockReturnValue('markdown formatted');
                    const text = formatText();
                    expect(text).toEqual('markdown formatted');
                    expect($window.markdown.toHTML).toHaveBeenCalledWith(
                        `IGNORETHISLINE\n\n${viewModel.model.text}`,
                        'Maruku',
                    );
                });

                it('should not treat a parenthesis after a modal as a link', () => {
                    viewModel = getViewModel({
                        behaviors: {
                            ProcessesMarkdown: {},
                            ProcessesModals: {},
                        },
                        text: 'this is a [[modal]] (not a link) and [[this is also not a link]] (for real).',
                    });
                    const text = formatText();
                    expect($(text).find('a').length).toBe(0);
                });
            });

            it('should handle standard color commands', () => {
                viewModel = getViewModel({
                    behaviors: {
                        ProcessesMarkdown: {},
                    },
                    text: 'some {coral:text that is kind of long...!}',
                });
                expect(formatText()).toEqual('<p>some <span class="coral">text that is kind of long...!</span></p>');
            });

            it('should handle curly braces inside of color commands', () => {
                viewModel = getViewModel({
                    behaviors: {
                        ProcessesMarkdown: {},
                    },
                    text: 'some {purple:{text\\\\}}',
                });
                expect(formatText()).toEqual('<p>some <span class="purple">{text}</span></p>');
            });

            it('should insert start attr on a list', () => {
                viewModel = getViewModel({
                    behaviors: {
                        ProcessesMarkdown: {},
                    },
                    text: 'some text\n\n2. item\n3. item',
                });
                expect(formatText()).toEqual('<p>some text</p>\n\n<ol start="2"><li>item</li><li>item</li></ol>');
            });

            it('should insert start attr on a list at the beginning', () => {
                viewModel = getViewModel({
                    behaviors: {
                        ProcessesMarkdown: {},
                    },
                    text: '2. item\n3. item\n\nsome text',
                });
                expect(formatText()).toEqual('<ol start="2"><li>item</li><li>item</li></ol>\n\n<p>some text</p>');
            });

            it('should insert start attr on multiple lists that do not start at one', () => {
                viewModel = getViewModel({
                    behaviors: {
                        ProcessesMarkdown: {},
                    },
                    text: '2. item\n3. item\n\nsome text\n\n5. item\n6. item',
                });
                expect(formatText()).toEqual(
                    '<ol start="2"><li>item</li><li>item</li></ol>\n\n<p>some text</p>\n\n<ol start="5"><li>item</li><li>item</li></ol>',
                );
            });

            it('should insert start attr on list that starts with a double digit', () => {
                viewModel = getViewModel({
                    behaviors: {
                        ProcessesMarkdown: {},
                    },
                    text: '10. item\n11. item\n\nsome text\n\n5. item\n6. item',
                });
                expect(formatText()).toEqual(
                    '<ol start="10"><li>item</li><li>item</li></ol>\n\n<p>some text</p>\n\n<ol start="5"><li>item</li><li>item</li></ol>',
                );
            });

            it('should insert start attr on list that has whitespace before the number', () => {
                viewModel = getViewModel({
                    behaviors: {
                        ProcessesMarkdown: {},
                    },
                    text: '  10. item\n11. item\n\nsome text\n\n  5. item\n6. item',
                });
                expect(formatText()).toEqual(
                    '<ol start="10"><li>item</li><li>item</li></ol>\n\n<p>some text</p>\n\n<ol start="5"><li>item</li><li>item</li></ol>',
                );
            });
        });

        describe('ProcessesMathjax', () => {
            describe('Editor', () => {
                // we mock out MathJax due to a variety of reasons, particularly when it comes to
                // asynchronous initialization. but for coverage sake, just verify that it's being
                // called as we'd expect

                it('should process mathjax', () => {
                    viewModel = getViewModel({
                        behaviors: {
                            ProcessesMathjax: {},
                        },
                    });

                    const editorViewModel = viewModel.frame.editorViewModelFor(viewModel.model);
                    jest.spyOn(editorViewModel, 'replaceMathJaxElement');
                    jest.spyOn($window.MathJax.Hub, 'Queue').mockImplementation(args => {
                        args[3]();
                    });

                    viewModel.model.text = 'MathJax: %%x^2%%';
                    expect(formatText()).toEqual(`<div>${viewModel.model.text}</div>`);
                    expect($window.MathJax.Hub.Queue).toHaveBeenCalled();
                    expect(editorViewModel.replaceMathJaxElement).toHaveBeenCalled();
                });

                it('should enforce replacement of escaped delimeters', () => {
                    const text = 'Not MathJax: \\%\\%x^2\\%\\%';
                    const expectedText = 'Not MathJax: %%x^2%%';
                    const elem = $('<div>').html(text);
                    const htmlElem = elem[0];
                    const resolve = jest.fn();

                    const editorViewModel = viewModel.frame.editorViewModelFor(viewModel.model);
                    editorViewModel.replaceMathJaxElement(elem, htmlElem, resolve);
                    expect(resolve).toHaveBeenCalledWith(`<div>${expectedText}</div>`);
                });
            });
        });

        describe('ProcessesInlineImages', () => {
            describe('Editor', () => {
                it('should process inline images', () => {
                    viewModel = getViewModel({
                        behaviors: {
                            ProcessesMarkdown: {},
                            ProcessesInlineImages: {},
                        },
                    });
                    const image = frame.addImage({
                        label: 'inline_image',
                    });
                    viewModel.model.text = 'Here is an ![inline_image]';
                    expect(formatText()).toEqual(
                        `<p>Here is an <img alt="" src="${image.urlForContext('inline')}"></p>`,
                    );
                });
                it('should throw if ProcessMarkdown not included', () => {
                    expect(() => {
                        viewModel = getViewModel({
                            behaviors: {
                                ProcessesInlineImages: {},
                            },
                        });
                        viewModel.model.text = 'Here is an ![inline_image]';
                        formatText();
                    }).toThrow(new Error('ProcessesInlineImages requires ProcessesMarkdown'));
                });
                it('should skip over an image that cannot be found', () => {
                    viewModel = getViewModel({
                        behaviors: {
                            ProcessesMarkdown: {},
                            ProcessesInlineImages: {},
                        },
                    });
                    viewModel.model.text = 'Here is an ![inline_image]';
                    expect(formatText()).toEqual('<p>Here is an </p>');
                });
                it('should preload inline images', () => {
                    viewModel = getViewModel({
                        behaviors: {
                            ProcessesMarkdown: {},
                            ProcessesInlineImages: {},
                        },
                    });
                    const image = frame.addImage({
                        label: 'inline_image',
                    });
                    viewModel.model.text = 'Here is an ![inline_image]';
                    jest.spyOn(image, 'preload').mockImplementation(() => {});
                    viewModel.model.recursivelyPreloadImages();
                    expect(image.preload).toHaveBeenCalled();
                });
                it('should ignore special blocks', () => {
                    viewModel = getViewModel({
                        behaviors: {
                            ProcessesMarkdown: {},
                            ProcessesInlineImages: {},
                        },
                    });
                    viewModel.model.text = '`print("Todays high: " + temp)`';
                    expect(formatText()).toEqual('<p><code>print("Todays high: &#34; + temp)</code></p>');
                });
            });
        });

        describe('ProcessesStorableImages', () => {
            describe('Editor', () => {
                it('should work', () => {
                    viewModel = getViewModel({
                        behaviors: {
                            ProcessesMarkdown: {},
                            ProcessesInlineImages: {},
                            ProcessesStorableImages: {},
                        },
                    });
                    const image1 = frame.addImage({
                        label: 'inline_image',
                    });
                    const image2 = frame.addImage({
                        label: 'another',
                    });
                    viewModel.model.text = 'Here is an ![inline_image] and ![another]';
                    expect(formatText()).toEqual(
                        `<p>Here is an <img alt="" storable-image="${image1.urlForContext(
                            'inline',
                        )}"> and <img alt="" storable-image="${image2.urlForContext('inline')}"></p>`,
                    );
                });
            });
        });

        describe('ProcessesModals', () => {
            describe('Editor', () => {
                it('should process modals', () => {
                    viewModel = getViewModel({
                        text: 'here is an [[awesome modal]]',
                        behaviors: {
                            ProcessesModals: {},
                        },
                    });
                    model.modals = [frame.addText()];
                    const el = $(document.createElement('div'));
                    el.append(formatText());
                    const modal = SpecHelper.expectElement(el, 'modal-popup');
                    expect(modal.attr('event-type')).toEqual('lesson:modal-click');
                    SpecHelper.expectElementText(modal, '[label]', 'awesome modal');
                    SpecHelper.expectElement(modal, '[content] cf-ui-component');
                });

                describe('updateModalKeys', () => {
                    beforeEach(() => {
                        viewModel = getViewModel({
                            behaviors: {
                                ProcessesModals: {},
                            },
                        });
                        formatText();
                    });

                    it('should add one modal at the end', () => {
                        expect(model.modals).toBeUndefined();
                        viewModel.model.text = '[[another]]';

                        expect(model.modals.length).toBe(1);
                        const newModal = model.modals[0];
                        expect(newModal.text).toEqual('another');
                    });

                    it('should add one modal in the middle', () => {
                        model.text = '[[modal_1]] and [[modal_2]]';
                        expect(modalTexts()).toEqual(['modal_1', 'modal_2']);
                        model.text = '[[modal_1]] and [[modal_1.5]] and [[modal_2]]';
                        expect(modalTexts()).toEqual(['modal_1', 'modal_1.5', 'modal_2']);
                    });

                    it('should update a modal when the modalKey changes', () => {
                        model.text = '[[modal_1]] and [[modal_2]]';
                        expect(modalTexts()).toEqual(['modal_1', 'modal_2']);
                        model.text = '[[same_modal]] and [[modal_2]]';

                        // it's still the same modal, even though the key changed
                        expect(modalTexts()).toEqual(['modal_1', 'modal_2']);
                    });

                    it('should support editing multiple modalKeys at once', () => {
                        model.text = '[[modal_1]] and [[modal_2]]';
                        expect(modalTexts()).toEqual(['modal_1', 'modal_2']);

                        // if two things change, we lose track and have to re-build from scratch
                        model.text = '[[modal_1.1]] and [[modal_2.1]]';
                        expect(modalTexts()).toEqual(['modal_1.1', 'modal_2.1']);
                    });

                    it('should remove a modal', () => {
                        model.text = '[[modal_1]] and [[modal_2]] and [[modal_3]]';
                        model.text = '[[modal_1]] and [[modal_3]]';

                        // the middle modal shoudl have been removed
                        expect(modalTexts()).toEqual(['modal_1', 'modal_3']);
                    });

                    it('should bring back the a modal from the cache', () => {
                        model.text = '[[modal_1]]';
                        model.modals[0].text = 'I changed it';
                        model.text = '';
                        model.text = '[[modal_1]]';
                        expect(modalTexts()).toEqual(['I changed it']);
                    });

                    function modalTexts() {
                        return model.modals.map(modal => modal.text);
                    }
                });
            });
        });

        describe('Preventing Line Breaks', () => {
            function renderViewModel() {
                const el = $(document.createElement('div'));
                el.append(formatText());
                const renderer = SpecHelper.renderer();
                renderer.scope.viewModel = viewModel;
                renderer.render(el);
                return el;
            }

            let challengesComponent;
            let challengesEditorViewModel;

            beforeEach(() => {
                viewModel = getViewModel({
                    text: '',
                });

                Object.defineProperty(ChallengeModel.EditorViewModel.prototype, 'correctAnswerText', {
                    writable: true,
                });

                // we need to make sure the editorViewModel gets instantiated
                viewModel.frame.editorViewModelFor(viewModel.model);

                // setup challenges component
                challengesComponent = viewModel.frame.addChallenges();
                challengesEditorViewModel = viewModel.frame.editorViewModelFor(challengesComponent);
                challengesEditorViewModel.setConfig({
                    newChallengeType: UserInputChallengeModel,
                });

                challengesEditorViewModel = viewModel.frame.editorViewModelFor(challengesComponent);
                expect(challengesComponent.challenges.length).toEqual(0); // sanity check
                viewModel.model.challengesComponent = challengesComponent;

                viewModel.model.behaviors = {
                    ProcessesChallengeBlanks: {},
                };
            });

            it('should correctly wrap each blank that ends with punctuation', () => {
                viewModel.model.text =
                    'here is a [blank]. and here is [another]! and [another]? but [wait]: [yet]; [more], [and]- [aaand]\u2013 [aaaaand]\u2014';
                const el = renderViewModel();
                expect(el.find('> span').length).toBe(9);
                expect(el.find('> span > cf-challenge-blank').length).toBe(9);
            });

            it('should correctly wrap blanks that begin or end with adjacent characters', () => {
                viewModel.model.text = 'a1-[leading] xxx a2-[wrapped]b2! ([wrapped2]) xxx [trailing]b3!';
                const el = renderViewModel();
                expect(el.find('> span').length).toBe(4);
                SpecHelper.expectElements(el, '> span > cf-challenge-blank', 4);
            });

            it('should correctly wrap blanks that begin or end with html character codes', () => {
                viewModel.model.text = 'here is a [blank]¢ and here is another £[blank] and yet another ¤[one]¥';
                const el = renderViewModel();
                expect(el.find('> span').length).toBe(3);
                SpecHelper.expectElements(el, '> span > cf-challenge-blank', 3);
            });

            it('should correctly wrap blanks that begin or end with adjacent styling spans', () => {
                viewModel.model.text =
                    '<span class="red">a1-</span>[leading] xxx <span class="red">a1-</span>[wrapped]<span class="green">b2?</span> xxx [trailing]<span class="green">b2?</span>';
                const el = renderViewModel();
                expect(el.find('> span').length).toBe(3);
                SpecHelper.expectElements(el, '> span > cf-challenge-blank', 3);
            });

            it('should correctly wrap mathjax adjacent to punctuation', () => {
                viewModel.model.text =
                    'here is some %% mathjax %%. and here is %%more%%! and %%more%%? but %%wait%%: %%yet%%; %%more%%, %%and%%- %%aaand%%\u2013 %%aaaaand%%\u2014';
                const el = renderViewModel();
                expect(el.find('> span').length).toBe(9);
            });

            it('should correctly wrap mathjax that begin or end with adjacent characters', () => {
                viewModel.model.text = 'a1-%%leading%% xxx a2-%%wrapped%%b2! (%%wrapped2%%) xxx %%trailing%%b3!';
                const el = renderViewModel();
                expect(el.find('> span').length).toBe(4);
            });
        });

        describe('ProcessesChallengeBlanks', () => {
            describe('Editor', () => {
                function renderViewModel() {
                    const el = $(document.createElement('div'));
                    el.append(formatText());
                    const renderer = SpecHelper.renderer();
                    renderer.scope.viewModel = viewModel;
                    renderer.render(el);
                    return el;
                }

                it('should process blanks', () => {
                    viewModel = getViewModel({
                        text: 'here is a [blank]',
                    });
                    const challengesComponent = viewModel.frame.addChallenges();
                    challengesComponent.challenges = [];
                    challengesComponent.editorViewModel.setConfig({
                        newChallengeType: MultipleChoiceChallengeModel,
                    });

                    viewModel.model.challengesComponent = challengesComponent;
                    viewModel.model.behaviors = {
                        ProcessesChallengeBlanks: {},
                    };

                    const el = renderViewModel();

                    const blank = SpecHelper.expectElement(el, '.cf-challenge-blank');
                    expect(blank.scope().viewModel.model).toBe(challengesComponent.challenges[0]);
                });

                // Would like to test mathjax, but every time I create a string
                // with two percent signs, one of them gets removed

                it('should add z-indexes to blanks', () => {
                    viewModel = getViewModel({
                        text: 'here is a [blank] and [another]',
                    });
                    const challengesComponent = viewModel.frame.addChallenges();
                    challengesComponent.challenges = [];
                    challengesComponent.editorViewModel.setConfig({
                        newChallengeType: MultipleChoiceChallengeModel,
                    });
                    viewModel.model.challengesComponent = challengesComponent;
                    viewModel.model.behaviors = {
                        ProcessesChallengeBlanks: {},
                    };
                    const el = renderViewModel();

                    const blanks = SpecHelper.expectElements(el, 'cf-challenge-blank', 2);
                    SpecHelper.expectEqual('100', blanks.eq(0).css('zIndex'), 'blank 0 z-index');
                    SpecHelper.expectEqual('99', blanks.eq(1).css('zIndex'), 'blank 1 z-index');
                });

                it('should set related blank information on correct answer', () => {
                    viewModel = getViewModel({
                        text: 'here is a [blank]',
                    });
                    const challengesComponent = viewModel.frame.addChallenges();
                    challengesComponent.challenges = [];
                    viewModel.model.challengesComponent = challengesComponent;
                    viewModel.model.behaviors = {
                        ProcessesChallengeBlanks: {},
                    };
                    challengesComponent.editorViewModel.setConfig({
                        newChallengeType: MultipleChoiceChallengeModel,
                    });

                    // kick off the processing
                    viewModel.model.editorViewModel; // eslint-disable-line

                    const challenge = challengesComponent.challenges[0];
                    const correctAnswer = challenge.editorViewModel.correctAnswer;
                    expect(correctAnswer.editorViewModel.relatedBlankLabel).toBe('blank');
                    expect(correctAnswer.editorViewModel.relatedBlankChallenge).toBe(challenge);
                });

                describe('updateBlanks', () => {
                    let challengesComponent;
                    let challengesEditorViewModel;

                    beforeEach(() => {
                        viewModel = getViewModel({
                            text: '',
                        });

                        Object.defineProperty(ChallengeModel.EditorViewModel.prototype, 'correctAnswerText', {
                            writable: true,
                        });

                        // we need to make sure the editorViewModel gets instantiated
                        viewModel.frame.editorViewModelFor(viewModel.model);

                        // setup challenges component
                        challengesComponent = viewModel.frame.addChallenges();
                        challengesEditorViewModel = viewModel.frame.editorViewModelFor(challengesComponent);
                        challengesEditorViewModel.setConfig({
                            newChallengeType: UserInputChallengeModel,
                        });

                        challengesEditorViewModel = viewModel.frame.editorViewModelFor(challengesComponent);
                        expect(challengesComponent.challenges.length).toEqual(0); // sanity check
                        viewModel.model.challengesComponent = challengesComponent;

                        viewModel.model.behaviors = {
                            ProcessesChallengeBlanks: {},
                            ProcessesMarkdown: {},
                            ProcessesMathjax: {},
                        };
                    });

                    it('should add one challenge at the end', () => {
                        viewModel.model.text = 'and a [blank]';
                        expect(challengeTexts()).toEqual(['blank']);
                    });

                    it('should add one challenge in the middle', () => {
                        viewModel.model.text = 'and a [blank1] and a [blank2]';
                        viewModel.model.text = 'and a [blank1] and a [blank1.5] and a [blank2]';
                        expect(challengeTexts()).toEqual(['blank1', 'blank1.5', 'blank2']);
                    });

                    it('should update a challenge when the blank changes', () => {
                        viewModel.model.text = 'and a [blank]';
                        viewModel.model.text = 'and a [blank that was edited]';
                        expect(challengeTexts()).toEqual(['blank that was edited']);
                    });

                    it('should remove a challenge', () => {
                        viewModel.model.text = 'and a [blank]';
                        viewModel.model.text = 'and a';
                        expect(challengeTexts()).toEqual([]);
                    });

                    it('should support editing multiple blanks at once', () => {
                        viewModel.model.text = 'and a [blank1] and a [blank2]';
                        viewModel.model.text = 'and a [blank1.1] and a [blank2.1]';
                        expect(challengeTexts()).toEqual(['blank1.1', 'blank2.1']);
                    });

                    it('should use the same challenge when changing a blank', () => {
                        viewModel.model.text = 'and a [blank]';
                        const challenge = challengesComponent.challenges[0];
                        viewModel.model.text = 'and a [blank that was edited]';
                        expect(challengesComponent.challenges[0]).toBe(challenge);
                    });

                    it('should correctly wrap blanks that begin or end with adjacent characters', () => {
                        viewModel.model.text = 'a1-[leading] xxx a2-[wrapped]b2! ([wrapped2]) xxx [trailing]b3!';
                        const el = renderViewModel();
                        expect(el.find('p > span').length).toBe(4);
                        SpecHelper.expectElements(el, 'p > span > cf-challenge-blank', 4);
                    });

                    it('should not update answer text if unlink_blank_from_answer not true', () => {
                        viewModel.model.text = 'and a [blank]';
                        const challenge = challengesComponent.challenges[0];
                        challenge.unlink_blank_from_answer = true;
                        viewModel.model.text = 'and a [blank that was edited]';
                        expect(challengeTexts()).toEqual(['blank']);
                    });

                    it('should re-link the answer and the blank when unlink_blank_from_answer set to false', () => {
                        viewModel.model.text = 'and a [blank]';
                        const challenge = challengesComponent.challenges[0];
                        challenge.unlink_blank_from_answer = true;
                        viewModel.model.text = 'and a [blank that was edited]';
                        expect(challengeTexts()).toEqual(['blank']);
                        challenge.unlink_blank_from_answer = false;
                        expect(challengeTexts()).toEqual(['blank that was edited']);
                    });

                    it('should update related blank information on correct answer', () => {
                        challengesEditorViewModel.setConfig({
                            newChallengeType: MultipleChoiceChallengeModel,
                        });
                        viewModel.model.text = 'and a [blank]';
                        viewModel.model.text = 'and a [blank that was edited]';

                        const challenge = challengesComponent.challenges[0];
                        const correctAnswer = challenge.editorViewModel.correctAnswer;
                        expect(correctAnswer.editorViewModel.relatedBlankLabel).toBe('blank that was edited');
                        expect(correctAnswer.editorViewModel.relatedBlankChallenge).toBe(challenge);
                    });

                    // BRACKET_SUPPORT
                    it('should work with blank directly next to a blank if escaped', () => {
                        // markdown will not treat this as a link
                        viewModel.model.text = '[blank1]\\\\[blank2]';
                        expect(challengeTexts()).toEqual(['blank1', 'blank2']);
                        $timeout.flush();
                        expect(
                            $('<div>').html(viewModel.model.formatted_text).find('cf-challenge-blank').length,
                        ).toEqual(2);
                    });

                    it('should handle a formatted blank', () => {
                        // markdown will not treat this as a link
                        viewModel.model.text = '[**blank**]';
                        expect(challengeTexts()).toEqual(['**blank**']);
                    });

                    it('should not destroy an empty blank', () => {
                        challengesEditorViewModel.setConfig({
                            newChallengeType: MultipleChoiceChallengeModel,
                        });
                        viewModel.model.text = 'and a [blank]';
                        const challenge = challengesComponent.challenges[0];
                        viewModel.model.text = 'and a []';
                        viewModel.model.text = 'and a [changed one]';
                        expect(challengesComponent.challenges[0]).toBe(challenge);
                    });

                    it('should ignore modals', () => {
                        viewModel.model.text = 'and a [blank1] and a [[modal]] and a [blank2] and a [[modal]]';
                        expect(challengeTexts()).toEqual(['blank1', 'blank2']);
                    });

                    it('should ignore inline images', () => {
                        viewModel.model.text = 'and a [blank1] and a ![image] and a [blank2] and a ![image]';
                        expect(challengeTexts()).toEqual(['blank1', 'blank2']);
                    });

                    it('should ignore links with markdown', () => {
                        viewModel.model.text = 'and a [blank1] and a [link](ht';
                        // at this point, the link is not complete, so markdown will not
                        // treat this as a link.  We need to make a blank in order to keep
                        // processed text consistent with challenge list
                        expect(challengeTexts()).toEqual(['blank1', 'link']);
                        $timeout.flush();
                        expect(
                            $('<div>').html(viewModel.model.formatted_text).find('cf-challenge-blank').length,
                        ).toEqual(2);

                        // once we're all done, the links should be ignored
                        viewModel.model.text =
                            'and a [blank1] and a [link](http://google.com) and a [blank2] and a [link](http://google.com)';
                        expect(challengeTexts()).toEqual(['blank1', 'blank2']);
                        $timeout.flush();
                        expect(
                            $('<div>').html(viewModel.model.formatted_text).find('cf-challenge-blank').length,
                        ).toEqual(2);
                    });

                    it('should work with blank then blank then link', () => {
                        // markdown will not treat this as a link
                        viewModel.model.text = '[blank]  [link](asdasd)';
                        expect(challengeTexts()).toEqual(['blank', 'link']);
                        $timeout.flush();
                        expect(
                            $('<div>').html(viewModel.model.formatted_text).find('cf-challenge-blank').length,
                        ).toEqual(2);
                    });

                    it('should handle a formatted blank', () => {
                        // markdown will not treat this as a link
                        viewModel.model.text = '[**blank**]';
                        expect(challengeTexts()).toEqual(['**blank**']);
                    });

                    it('should handle special chars in a blank', () => {
                        // markdown will not treat this as a link
                        viewModel.model.text = '[peanut butter & jelly]';
                        expect(challengeTexts()).toEqual(['peanut butter & jelly']);
                    });

                    it('should not throw an error when it cannot handle things', () => {
                        const $route = $injector.get('$route');
                        $route.current = {};
                        $route.current.directive = 'edit-lesson';

                        expect(() => {
                            viewModel.model.text = ' a [[[link](http://google.com) and a [blank]';
                            expect(
                                $('div').html(viewModel.model.formatted_text).find('cf-challenge-blank').length,
                            ).toEqual(0);
                        }).not.toThrow();

                        expect(challengeTexts()).toEqual([]);
                    });

                    // https://trello.com/c/C2mRH6zb
                    it('should work with a colon in the first line', () => {
                        viewModel.model.text = 'Remember: this is [a blank]';
                        expect(challengeTexts()).toEqual(['a blank']);
                        $timeout.flush();
                        expect(
                            $('<div>').html(viewModel.model.formatted_text).find('cf-challenge-blank').length,
                        ).toEqual(1);
                    });

                    it('should not update blanks when $$suppressBlanksUpdate is set on the TextEditorViewModel', () => {
                        viewModel.model.text = '[blank]';
                        expect(challengeTexts()).toEqual(['blank']);
                        viewModel.model.editorViewModel.$$suppressBlanksUpdate = true;
                        viewModel.model.text = '[blank] and [blank2]';
                        expect(challengeTexts()).toEqual(['blank']);
                    });

                    describe('code blocks', () => {
                        it('should handle a blank', () => {
                            viewModel.model.text = 'and a `[not blank] and a Blank[blank]`';
                            expect(challengeTexts()).toEqual(['blank']);
                            $timeout.flush();
                            expect(
                                $('<div>').html(viewModel.model.formatted_text).find('cf-challenge-blank').length,
                            ).toEqual(1);
                        });

                        it('should handle multiple blanks', () => {
                            viewModel.model.text = '`Blank[blank1]\nBlank[blank2]`';
                            expect(challengeTexts()).toEqual(['blank1', 'blank2']);
                            $timeout.flush();
                            expect(
                                $('<div>').html(viewModel.model.formatted_text).find('cf-challenge-blank').length,
                            ).toEqual(2);
                        });

                        it('should handle a blank nested in brackets when in a code block', () => {
                            viewModel.model.text = '`this is a blank nested in brackets array[Blank[0]]`';
                            expect(challengeTexts()).toEqual(['0']);
                            $timeout.flush();
                            expect(
                                $('<div>').html(viewModel.model.formatted_text).find('cf-challenge-blank').length,
                            ).toEqual(1);
                        });

                        it('should handle a code block inside of a blank', () => {
                            viewModel.model.text = 'this is a blank with [`code`]';
                            expect(challengeTexts()).toEqual(['`code`']);
                            $timeout.flush();
                            expect(
                                $('<div>').html(viewModel.model.formatted_text).find('cf-challenge-blank').length,
                            ).toEqual(1);
                        });

                        // see 4th checkbox on https://trello.com/c/1vawP4jj/3150-bug-fix-some-editor-regex-logic-that-was-recently-changed-to-support-our-python-lessons
                        // We are mainly checking that we do not get an 'unexpected match' error here
                        it('should handle a closing code tag when not in a special block', () => {
                            viewModel.model.text = 'Test[ `goal[-4:-2]` gives [`en`]';
                            expect(challengeTexts()).toEqual(['-4:-2']);
                            $timeout.flush();
                            expect(
                                $('<div>').html(viewModel.model.formatted_text).find('cf-challenge-blank').length,
                            ).toEqual(1);
                        });

                        // BRACKET_SUPPORT
                        it('should allow for brackets in blanks that are within code blocks', () => {
                            viewModel.model.text = 'find the first element with: `not blank and a Blank[array\\[0\\]]`';
                            expect(challengeTexts()).toEqual(['array[0]']);
                            $timeout.flush();
                            expect(
                                $('<div>').html(viewModel.model.formatted_text).find('cf-challenge-blank').length,
                            ).toEqual(1);
                        });

                        // BRACKET_SUPPORT
                        it('should allow for multiple brackets in blanks that are within code blocks', () => {
                            viewModel.model.text =
                                'multiple brackets in a blank: `not blank and a Blank[array\\[0\\] and array\\[1\\]]`';
                            expect(challengeTexts()).toEqual(['array[0] and array[1]']);
                            $timeout.flush();
                            expect(
                                $('<div>').html(viewModel.model.formatted_text).find('cf-challenge-blank').length,
                            ).toEqual(1);
                        });
                    });

                    describe('multi-line code blocks', () => {
                        it('should handle a blank', () => {
                            viewModel.model.text = 'Testing:\n\n    [not blank] and a Blank[blank]';
                            expect(challengeTexts()).toEqual(['blank']);
                            $timeout.flush();
                            expect(
                                $('<div>').html(viewModel.model.formatted_text).find('cf-challenge-blank').length,
                            ).toEqual(1);
                        });

                        it('should handle multiple blanks', () => {
                            viewModel.model.text = 'Testing:\n\n    Blank[blank1]\n\n    Blank[blank2]';
                            expect(challengeTexts()).toEqual(['blank1', 'blank2']);
                            $timeout.flush();
                            expect(
                                $('<div>').html(viewModel.model.formatted_text).find('cf-challenge-blank').length,
                            ).toEqual(2);
                        });

                        it('should not get confused by multiple spaces', () => {
                            viewModel.model.text =
                                'Testing:\n\n    [not blank] and four spaces:    and then [not blank] and real Blank[blank]';
                            expect(challengeTexts()).toEqual(['blank']);
                            $timeout.flush();
                            expect(
                                $('<div>').html(viewModel.model.formatted_text).find('cf-challenge-blank').length,
                            ).toEqual(1);
                        });

                        it('should handle a blank nested in brackets when in a code block', () => {
                            viewModel.model.text = 'Testing:\n\n    this is a blank nested in brackets array[Blank[0]]';
                            expect(challengeTexts()).toEqual(['0']);
                            $timeout.flush();
                            expect(
                                $('<div>').html(viewModel.model.formatted_text).find('cf-challenge-blank').length,
                            ).toEqual(1);
                        });

                        // BRACKET_SUPPORT
                        it('should allow for brackets in blanks that are within code blocks', () => {
                            viewModel.model.text =
                                'find the first element with:\n\n    not blank and a Blank[array\\[0\\]]';
                            expect(challengeTexts()).toEqual(['array[0]']);
                            $timeout.flush();
                            expect(
                                $('<div>').html(viewModel.model.formatted_text).find('cf-challenge-blank').length,
                            ).toEqual(1);
                        });
                    });

                    describe('replacing multiple blanks', () => {
                        beforeEach(() => {
                            challengesEditorViewModel.setConfig({
                                newChallengeType: MultipleChoiceChallengeModel,
                            });

                            viewModel.model.text = 'This [screen] is a [test].';
                            $timeout.flush();
                        });

                        it('should remove all challenges if all blanks are deleted', () => {
                            // clone challenges array
                            const challenges = challengesEditorViewModel.model.challenges.clone();
                            jest.spyOn(challenges[0], 'remove');
                            jest.spyOn(challenges[1], 'remove');
                            viewModel.model.text = '';
                            expect(challenges[0].remove).toHaveBeenCalled();
                            expect(challenges[1].remove).toHaveBeenCalled();
                        });

                        /*
                            When I tried to do this in the actual editor, it did not work.  Not all the blanks were
                            added to the text.  I fixed this with a timeout in challenges_view_model.  I tried and tried
                            but could not write a test to repro it.  See childAdded listener in challenges_view_model.js
                            to see where I fixed this
                        */
                        it('should add a bunch of new blanks in different places', () => {
                            const challenges = challengesEditorViewModel.model.challenges.clone();

                            // HANGS HERE
                            viewModel.model.text =
                                'This [new blank 1] [screen] [new blank 2] is a [test]. [new blank 3] [new blank 4]';

                            // make sure the existing challenges were not removed, and they are in the right positions
                            expect(challengesEditorViewModel.model.challenges.length).toBe(6);
                            expect(challengesEditorViewModel.model.challenges.indexOf(challenges[0])).toBe(1);
                            expect(challengesEditorViewModel.model.challenges.indexOf(challenges[1])).toBe(3);

                            const el = renderViewModel();
                            expect(el.find('cf-challenge-blank').length).toBe(6);
                        });

                        it('should work when two blanks have the same text', () => {
                            const challenges = challengesEditorViewModel.model.challenges.clone();
                            viewModel.model.text = 'This [screen] [screen] [new blank 2] is a [test].';
                            // make sure the existing challenges were not removed, they are in the right positions
                            expect(challengesEditorViewModel.model.challenges.length).toBe(4);
                            expect(challengesEditorViewModel.model.challenges.indexOf(challenges[0])).toBe(0);

                            // even though the second blank has the same text as the first, they should
                            // not have same challenge
                            expect(challengesEditorViewModel.model.challenges[1]).not.toBe(challenges[0]);
                            expect(challengesEditorViewModel.model.challenges.indexOf(challenges[1])).toBe(3);

                            const el = renderViewModel();
                            expect(el.find('cf-challenge-blank').length).toBe(4);
                        });
                    });

                    // HANGS INDEFINITELY
                    function challengeTexts() {
                        return challengesEditorViewModel.model.challenges.map(
                            challengeModel => challengeModel.editorViewModel.correctAnswerText,
                        );
                    }
                });
            });
        });
    });

    describe('cf-text', () => {
        let elem;
        let scope;

        it('should render text', () => {
            render();
            viewModel.model.editorViewModel.formatText();
            scope.$digest();
            SpecHelper.expectEqual(viewModel.model.formatted_text, elem.text());
        });

        it('should set fontSize', () => {
            render();
            viewModel.model.fontSize = 10;
            scope.$digest();
            SpecHelper.expectEqual('10px', elem.find('cf-text').css('fontSize'));
        });

        describe('$compile error handling', () => {
            beforeEach(() => {
                render();
                viewModel.model.formatted_text = 'new value';
                jest.spyOn(scope, 'compile').mockImplementation(() => {
                    throw new Error('compile failure');
                });
            });

            it('should catch compile errors in editorMode', () => {
                Object.defineProperty(viewModel.frameViewModel, 'editorMode', {
                    value: true,
                });
                scope.$digest();
                SpecHelper.expectElementText(elem, 'cf-text', 'Error processing text.');
            });

            it('should catch compile errors in previewMode', () => {
                Object.defineProperty(viewModel.frameViewModel, 'previewMode', {
                    value: true,
                });
                scope.$digest();
                SpecHelper.expectElementText(elem, 'cf-text', 'Error processing text.');
            });

            it('should notify ErrorLogService in the player', () => {
                jest.spyOn(ErrorLogService, 'notify').mockImplementation(() => {});
                scope.$digest();
                const id = [scope.frameViewModel.frame.lesson().id, scope.frameViewModel.frame.id, scope.model.id].join(
                    '/',
                );
                expect(ErrorLogService.notify).toHaveBeenCalledWith(
                    `Error processing text for (lesson_id/frame_id/component_id=${id}): compile failure`,
                );
            });
        });

        function render() {
            const renderer = SpecHelper.renderer();
            renderer.scope.viewModel = viewModel;
            renderer.render('<cf-ui-component view-model="viewModel"></cf-ui-component>');
            elem = renderer.elem;
            scope = elem.find('cf-text').isolateScope();
        }
    });

    describe('cf-text-editor', () => {
        let elem;
        let scope;
        let editorViewModel;

        it('should have a textarea that updates text property', () => {
            render();
            SpecHelper.updateTextArea(elem, 'textarea', 'Some content');
            expect(viewModel.model.text).toEqual('Some content');
        });

        it('should add text-length-monitor', () => {
            render();
            editorViewModel.model.formatted_text = '<p>asdsa</p>';
            editorViewModel.setConfig({
                maxRecommendedTextLength: 23,
            });
            scope.$digest();
            SpecHelper.expectElement(elem, 'textarea');
            expect(!!elem.find('.tlm-counter').text().match('23')).toBe(true);
        });

        describe('text length monitoring', () => {
            beforeEach(() => {
                render();
                model.text = '';
                scope.$digest();
            });

            function mockFormattedTextLength(length) {
                for (let i = 0; i < length; i++) {
                    model.formatted_text += 'x';
                }
            }

            it('should monitor the length', () => {
                mockFormattedTextLength(6);
                $timeout.flush();
                scope.$digest();
                SpecHelper.expectElementText(elem, '.tlm-counter', '294 / 300 remaining');
            });

            it('should show "Calculating" when there is no formatted_text but actual text', () => {
                model.text = 'not_empty';
                model.formatted_text = undefined;
                $timeout.flush();
                scope.$digest();
                SpecHelper.expectElementText(elem, '.tlm-counter', 'Calculating ...');
            });

            it('should not show "calculating" when the text is completely blank', () => {
                model.text = undefined;
                model.formatted_text = undefined;
                $timeout.flush();
                scope.$digest();
                SpecHelper.expectElementText(elem, '.tlm-counter', '300 / 300 remaining');
            });

            it('should add warning class', () => {
                mockFormattedTextLength(290);
                $timeout.flush();
                scope.$digest();
                SpecHelper.expectElement(elem, '.component_editor.tlm-warning');
                SpecHelper.expectElementText(elem, '.tlm-counter.tlm-warning', '10 / 300 remaining');
            });

            it('should add error class', () => {
                mockFormattedTextLength(304);
                $timeout.flush();
                scope.$digest();
                SpecHelper.expectElement(elem, '.component_editor.tlm-over');
                SpecHelper.expectElementText(elem, '.tlm-counter.tlm-over', '-4 / 300 remaining');
            });

            it('should not count HTML elements as text', () => {
                model.formatted_text += '<span>text</span>';
                $timeout.flush();
                scope.$digest();
                SpecHelper.expectElementText(elem, '.tlm-counter', '296 / 300 remaining');
            });
        });

        describe('with ProcessesModals', () => {
            beforeEach(() => {
                render();
                model.behaviors.ProcessesModals = {};
            });

            it('should allow for editing a modal', () => {
                SpecHelper.expectNoElement(elem, '.modals');
                SpecHelper.updateTextArea(elem, 'textarea', 'A [[modal]]');
                SpecHelper.expectElement(elem, '.modals');
                SpecHelper.updateTextArea(elem, '.modals textarea', 'Modal content');
                SpecHelper.expectEqual('Modal content', model.modals[0].text);
            });

            it('should allow for selecting between modals', () => {
                SpecHelper.updateTextArea(elem, 'textarea', 'A [[modal]] and [[another]]');
                SpecHelper.expectEqual('modal', elem.find('.modals select option:selected').text());
                SpecHelper.updateSelect(elem, '.modals select', 'another');
                SpecHelper.updateTextArea(elem, '.modals textarea', 'Modal content');
                SpecHelper.expectEqual('Modal content', model.modals[1].text);
            });
        });

        describe('with default skin', () => {
            beforeEach(() => {
                render();
            });

            it('should have a big textarea', () => {
                const textArea = SpecHelper.updateTextArea(elem, 'textarea', 'Some content');
                SpecHelper.expectEqual('4', textArea.attr('rows'), 'textArea rows');
            });
        });

        describe('with inline skin', () => {
            beforeEach(() => {
                render('inline');
            });

            it('should have a small textarea', () => {
                const textArea = SpecHelper.updateTextArea(elem, 'textarea', 'Some content');
                SpecHelper.expectEqual('1', textArea.attr('rows'), 'textArea rows');
            });

            it('should hide formatting hints', () => {
                SpecHelper.expectNoElement(elem, '.help-link');
            });
        });

        describe('fontStyleEditor elements', () => {
            beforeEach(() => {
                render('inline');
            });

            it('should show fontSize when config.showFontStyleEditor === true', () => {
                editorViewModel.setConfig({
                    showFontStyleEditor: true,
                });
                scope.$digest();
                SpecHelper.updateTextInput(elem, '[name="fontSize"]', 10);
                SpecHelper.expectEqual(10, model.fontSize);
            });

            it('should show alignment buttons when config.showFontStyleEditor === true', () => {
                editorViewModel.setConfig({
                    showFontStyleEditor: true,
                });
                scope.$digest();
                const buttons = SpecHelper.expectElements(elem, '[name="font-style-editor"] button.inline', 3);
                SpecHelper.expectElement(buttons.eq(0), '.icon.align-left');
                SpecHelper.expectElement(buttons.eq(1), '.icon.align-center');
                SpecHelper.expectElement(buttons.eq(2), '.icon.align-right');
            });

            it('should not show fontSize when config.showFontStyleEditor !== true', () => {
                SpecHelper.expectNoElement(elem, '[name="fontSize"]');
            });

            it('should not show alignment when config.showFontStyleEditor !== true', () => {
                SpecHelper.expectNoElement(elem, '.icon.align-left');
                SpecHelper.expectNoElement(elem, '.icon.align-center');
                SpecHelper.expectNoElement(elem, '.icon.align-right');
            });

            it('should hide the fontSize input when config.hideFontSizeEditor === true', () => {
                editorViewModel.setConfig({
                    showFontStyleEditor: true,
                    hideFontSizeEditor: true,
                });
                scope.$digest();
                SpecHelper.expectNoElement(elem, '[name="fontSize"]');

                const buttons = SpecHelper.expectElements(elem, '[name="font-style-editor"] button.inline', 3);
                SpecHelper.expectElement(buttons.eq(0), '.icon.align-left');
                SpecHelper.expectElement(buttons.eq(1), '.icon.align-center');
                SpecHelper.expectElement(buttons.eq(2), '.icon.align-right');
            });
        });

        function render(skin) {
            SpecHelper.stubDirective('cfImageEditor');
            const renderer = SpecHelper.renderer();
            renderer.scope.editorViewModel = frame.editorViewModelFor(viewModel.model);
            renderer.render(
                `<cf-component-editor editor-view-model="editorViewModel" skin="${skin}"></cf-component-editor>`,
            );
            elem = renderer.elem;
            scope = elem.find('[editor-view-model]').isolateScope();
            model = viewModel.model;
            editorViewModel = renderer.scope.editorViewModel;
        }
    });

    describe('EditorViewModel', () => {
        let editorViewModel;

        beforeEach(() => {
            editorViewModel = frame.editorViewModelFor(viewModel.model);
        });

        it('should set the text to an empty string', () => {
            delete editorViewModel.model.text;
            editorViewModel.setup();
            expect(editorViewModel.model.text).toEqual('');
        });

        describe('initialize', () => {
            it('should set a default maxRecommendedTextLength', () => {
                expect(editorViewModel.config.maxRecommendedTextLength).toBe(MaxTextLengthConfig.DEFAULT);
            });

            it('should set maxRecommendedTextLength on modals', () => {
                editorViewModel.model.modals = [];
                const modal = frame.addText();
                editorViewModel.model.modals.push(modal);
                expect(modal.editorViewModel.config.maxRecommendedTextLength).toBe(MaxTextLengthConfig.MODAL);
            });

            it('should set normalize quotes', () => {
                viewModel.model.text = 'left-single‘ left-double“ right-single’ right-double”';
                editorViewModel = frame.editorViewModelFor(viewModel.model);
                expect(editorViewModel.model.text).toEqual('left-single\' left-double" right-single\' right-double"');
            });

            it('should add normalization to updates', () => {
                editorViewModel.model.text = 'left-single‘ left-double“ right-single’ right-double”';
                expect(editorViewModel.model.text).toEqual('left-single\' left-double" right-single\' right-double"');
            });
        });

        describe('maxRecommendedTextLength', () => {
            it('should return undefined if config value is undefined', () => {
                editorViewModel.setConfig({
                    maxRecommendedTextLength: undefined,
                });
                expect(editorViewModel.maxRecommendedTextLength()).toBeUndefined();
            });

            it('should return a number config value', () => {
                editorViewModel.setConfig({
                    maxRecommendedTextLength: 42,
                });
                expect(editorViewModel.maxRecommendedTextLength()).toBe(42);
            });

            it('should return run a function config value', () => {
                editorViewModel.setConfig({
                    // eslint-disable-next-line lodash-fp/prefer-constant
                    maxRecommendedTextLength() {
                        return 42;
                    },
                });
                expect(editorViewModel.maxRecommendedTextLength()).toBe(42);
            });
        });

        describe('transformMixedFractions', () => {
            function assertMixedFraction(beforeText, afterText) {
                afterText = afterText || beforeText;
                editorViewModel.model.text = beforeText;
                editorViewModel.transformMixedFractions();
                expect(editorViewModel.model.text).toEqual(afterText);
            }

            it('should replace standard fractions', () => {
                assertMixedFraction(
                    'Mathjax with %% 123\\frac{456}{789} %% in the middle',
                    'Mathjax with %% \\tmfrac{123}{456}{789} %% in the middle',
                );
                assertMixedFraction(
                    'Mathjax with %% 123\\dfrac{456}{789} %% in the middle',
                    'Mathjax with %% \\tmdfrac{123}{456}{789} %% in the middle',
                );
                assertMixedFraction(
                    'Mathjax with %% 123 \\frac{456}{789} %% in the middle',
                    'Mathjax with %% \\tmfrac{123}{456}{789} %% in the middle',
                );
                assertMixedFraction(
                    'Mathjax with %% 123 \\dfrac{456}{789} %% in the middle',
                    'Mathjax with %% \\tmdfrac{123}{456}{789} %% in the middle',
                );
                assertMixedFraction(
                    'Mathjax with %% -123\\dfrac{456}{789} %% in the middle',
                    'Mathjax with %% -\\tmdfrac{123}{456}{789} %% in the middle',
                );
            });

            it('should replace standard fractions with commas', () => {
                assertMixedFraction(
                    'Mathjax with %% 1,234\\frac{4,567}{7,890} %% in the middle',
                    'Mathjax with %% \\tmfrac{1,234}{4,567}{7,890} %% in the middle',
                );
                assertMixedFraction(
                    'Mathjax with %% 1,234 \\frac{4,567}{7,890} %% in the middle',
                    'Mathjax with %% \\tmfrac{1,234}{4,567}{7,890} %% in the middle',
                );
                assertMixedFraction(
                    'Mathjax with %% -1,234\\frac{4,567}{7,890} %% in the middle',
                    'Mathjax with %% -\\tmfrac{1,234}{4,567}{7,890} %% in the middle',
                );
            });

            it('should replace bracketless fractions', () => {
                assertMixedFraction(
                    'Mathjax with %% 123\\frac45 %% in the middle',
                    'Mathjax with %% \\tmfrac{123}{4}{5} %% in the middle',
                );
                assertMixedFraction(
                    'Mathjax with %% 123\\dfrac45 %% in the middle',
                    'Mathjax with %% \\tmdfrac{123}{4}{5} %% in the middle',
                );
                assertMixedFraction(
                    'Mathjax with %% 123 \\frac45 %% in the middle',
                    'Mathjax with %% \\tmfrac{123}{4}{5} %% in the middle',
                );
                assertMixedFraction(
                    'Mathjax with %% 123 \\dfrac45 %% in the middle',
                    'Mathjax with %% \\tmdfrac{123}{4}{5} %% in the middle',
                );
                assertMixedFraction(
                    'Mathjax with %% -123\\dfrac45 %% in the middle',
                    'Mathjax with %% -\\tmdfrac{123}{4}{5} %% in the middle',
                );
            });

            // situations where we should not replace

            it('should not replace fractions without a number in front', () => {
                assertMixedFraction('Mathjax with %% \\frac{456}{789} %% in the middle');
                assertMixedFraction('Mathjax with %% xyz\\frac{456}{789} %% in the middle');
                assertMixedFraction('Mathjax with %% 3\\times\\frac{456}{789} %% in the middle');
                assertMixedFraction('Mathjax with %% 3\\times\\frac45 %% in the middle');
                assertMixedFraction('Mathjax with %% 3\\times\\dfrac45 %% in the middle');
            });
        });

        describe('wrapTransN', () => {
            function assertWrapTransN(beforeText, afterText) {
                afterText = afterText || beforeText;
                editorViewModel.model.text = beforeText;
                editorViewModel.wrapTransN();
                expect(editorViewModel.model.text).toEqual(afterText);

                // also check that running it again on the text doesn't change it anymore
                editorViewModel.wrapTransN();
                expect(editorViewModel.model.text).toEqual(afterText);
            }

            beforeEach(() => {
                frame.mainUiComponent.editor_template = 'fill_in_the_blanks';
            });

            it('should work', () => {
                assertWrapTransN('%% \\transn{1.234} %%');
                assertWrapTransN('$$ \\transn{1.234} $$');

                assertWrapTransN('\\transn{1.234}', '%% \\transn{1.234} %%');
                assertWrapTransN('\\transn{1,234,000}', '%% \\transn{1,234,000} %%');
            });
        });

        describe('addTransN', () => {
            function assertTransN(convertNonMathjax, beforeText, afterText) {
                afterText = afterText || beforeText;
                editorViewModel.model.text = beforeText;
                editorViewModel.addTransN(convertNonMathjax);
                expect(editorViewModel.model.text).toEqual(afterText);

                // also check that running it again on the text doesn't change it anymore
                editorViewModel.addTransN(convertNonMathjax);
                expect(editorViewModel.model.text).toEqual(afterText);
            }

            beforeEach(() => {
                frame.mainUiComponent.editor_template = 'compose_blanks';
            });

            describe('when already in MathJax', () => {
                it('should replace basic decimals', () => {
                    assertTransN(
                        false,
                        'This is some text with %% 5.55 %% in the middle',
                        'This is some text with %% \\transn{5.55} %% in the middle',
                    );
                    assertTransN(
                        false,
                        'This is some text with %% .55 %% in the middle',
                        'This is some text with %% \\transn{.55} %% in the middle',
                    );
                    assertTransN(
                        false,
                        'This is some text with %% -.55 %% in the middle',
                        'This is some text with %% -\\transn{.55} %% in the middle',
                    );
                    assertTransN(
                        false,
                        'This is some text with %% -5.55 %% in the middle',
                        'This is some text with %% -\\transn{5.55} %% in the middle',
                    );
                });

                it('should replace comma-delimited numbers', () => {
                    assertTransN(
                        false,
                        'This is some text with %% 3,000 %% in the middle',
                        'This is some text with %% \\transn{3,000} %% in the middle',
                    );
                    assertTransN(
                        false,
                        'This is some text with %% 3,000,000 %% in the middle',
                        'This is some text with %% \\transn{3,000,000} %% in the middle',
                    );
                    assertTransN(
                        false,
                        'This is some text with %% -3,000 %% in the middle',
                        'This is some text with %% -\\transn{3,000} %% in the middle',
                    );
                    assertTransN(
                        false,
                        'This is some text with %% -3,000,000 %% in the middle',
                        'This is some text with %% -\\transn{3,000,000} %% in the middle',
                    );
                });

                it('should replace decimals in a comma separated dataset', () => {
                    assertTransN(
                        false,
                        '{%%20.5, 30, 40, 50, -.55, 2, 3%%}',
                        '{%%\\transn{20.5}, 30, 40, 50, -\\transn{.55}, 2, 3%%}',
                    );
                });

                it('should replace basic decimals in blanks', () => {
                    assertTransN(
                        false,
                        'This is some text with %% Blank[.55] %% in the middle',
                        'This is some text with %% Blank[\\transn{.55}] %% in the middle',
                    );
                    assertTransN(
                        false,
                        'This is some text with %% Blank[5.55] %% in the middle',
                        'This is some text with %% Blank[\\transn{5.55}] %% in the middle',
                    );
                    assertTransN(
                        false,
                        'This is some text with %% Blank[-.55] %% in the middle',
                        'This is some text with %% Blank[-\\transn{.55}] %% in the middle',
                    );
                    assertTransN(
                        false,
                        'This is some text with %% Blank[-5.55] %% in the middle',
                        'This is some text with %% Blank[-\\transn{5.55}] %% in the middle',
                    );
                });

                it('should replace comma-delimited numbers in blanks', () => {
                    assertTransN(
                        false,
                        'This is some text with %% Blank[3,000] %% in the middle',
                        'This is some text with %% Blank[\\transn{3,000}] %% in the middle',
                    );
                    assertTransN(
                        false,
                        'This is some text with %% Blank[3,000,000] %% in the middle',
                        'This is some text with %% Blank[\\transn{3,000,000}] %% in the middle',
                    );
                    assertTransN(
                        false,
                        'This is some text with %% Blank[-3,000] %% in the middle',
                        'This is some text with %% Blank[-\\transn{3,000}] %% in the middle',
                    );
                    assertTransN(
                        false,
                        'This is some text with %% Blank[-3,000,000] %% in the middle',
                        'This is some text with %% Blank[-\\transn{3,000,000}] %% in the middle',
                    );
                });

                // cases when we shouldn't replace

                it('should not replace decimals inside of \\hspace{...}', () => {
                    assertTransN(false, 'This is some text with %% \\hspace{.2in} %% in the middle');
                    assertTransN(false, 'This is some text with %% \\hspace{2.25in} %% in the middle');
                });

                it('should not replace decimals inside of \\vspace{...}', () => {
                    assertTransN(false, 'This is some text with %% \\vspace{.2in} %% in the middle');
                    assertTransN(false, 'This is some text with %% \\vspace{2.25in} %% in the middle');
                });

                it('should not replace decimals/commas inside of \\text{...}', () => {
                    assertTransN(false, 'This is some text with %% \\text{.2} %% in the middle');
                    assertTransN(false, 'This is some text with %% \\text{2.25} %% in the middle');
                    assertTransN(false, 'This is some text with %% \\text{1,200} %% in the middle');
                    assertTransN(false, 'This is some text with %% \\text{1,200,000} %% in the middle');
                    assertTransN(false, 'This is some text with %% \\text{-.2} %% in the middle');
                    assertTransN(false, 'This is some text with %% \\text{-2.25} %% in the middle');
                    assertTransN(false, 'This is some text with %% \\text{-1,200} %% in the middle');
                    assertTransN(false, 'This is some text with %% \\text{-1,200,000} %% in the middle');
                });

                it('should not replace decimals immediately after \\kern-...', () => {
                    assertTransN(false, 'This is some text with %% \\kern-.2ex %% in the middle');
                    assertTransN(false, 'This is some text with %% \\kern-0.2ex %% in the middle');
                });

                it('should not replace bare numbers in a comma separated dataset', () => {
                    assertTransN(false, '{%%20, 30, 40, 50, 1, 2, 3%%}');
                    assertTransN(false, '{%%20, -30, 40, 50, 1, -2, 3%%}');
                });
            });

            describe('when outside of MathJax', () => {
                it('should replace basic decimals', () => {
                    assertTransN(
                        true,
                        'this is a bare number outside of mathjax: 3.55',
                        'this is a bare number outside of mathjax: %% \\transn{3.55} %%',
                    );
                    assertTransN(
                        true,
                        'this is a bare number outside of mathjax: .55',
                        'this is a bare number outside of mathjax: %% \\transn{.55} %%',
                    );
                    assertTransN(
                        true,
                        'this is a bare number outside of mathjax: -3.55',
                        'this is a bare number outside of mathjax: %% -\\transn{3.55} %%',
                    );
                    assertTransN(
                        true,
                        'this is a bare number outside of mathjax: -.55',
                        'this is a bare number outside of mathjax: %% -\\transn{.55} %%',
                    );
                });

                it('should replace comma-delimited numbers', () => {
                    assertTransN(
                        true,
                        'this is a bare number outside of mathjax: 3,000',
                        'this is a bare number outside of mathjax: %% \\transn{3,000} %%',
                    );
                    assertTransN(
                        true,
                        'this is a bare number outside of mathjax: 3,000,000',
                        'this is a bare number outside of mathjax: %% \\transn{3,000,000} %%',
                    );
                    assertTransN(
                        true,
                        'this is a bare number outside of mathjax: -3,000',
                        'this is a bare number outside of mathjax: %% -\\transn{3,000} %%',
                    );
                    assertTransN(
                        true,
                        'this is a bare number outside of mathjax: -3,000,000',
                        'this is a bare number outside of mathjax: %% -\\transn{3,000,000} %%',
                    );
                });

                it('should work with modals', () => {
                    assertTransN(
                        true,
                        'this is a bare number outside of mathjax: [[.55]]',
                        'this is a bare number outside of mathjax: [[%% \\transn{.55} %%]]',
                    );
                    assertTransN(
                        true,
                        'this is a bare number outside of mathjax: [[3,000,000]]',
                        'this is a bare number outside of mathjax: [[%% \\transn{3,000,000} %%]]',
                    );
                    assertTransN(
                        true,
                        'this is a bare number outside of mathjax: [[-.55]]',
                        'this is a bare number outside of mathjax: [[%% -\\transn{.55} %%]]',
                    );
                    assertTransN(
                        true,
                        'this is a bare number outside of mathjax: [[-3,000,000]]',
                        'this is a bare number outside of mathjax: [[%% -\\transn{3,000,000} %%]]',
                    );
                });

                it('should replace decimals with commas, too', () => {
                    assertTransN(
                        true,
                        'this is a bare number outside of mathjax: 3,000.55',
                        'this is a bare number outside of mathjax: %% \\transn{3,000.55} %%',
                    );
                    assertTransN(
                        true,
                        'this is a bare number outside of mathjax: [[3,000,000.55]]',
                        'this is a bare number outside of mathjax: [[%% \\transn{3,000,000.55} %%]]',
                    );
                    assertTransN(
                        true,
                        'this is a bare number outside of mathjax: -3,000.55',
                        'this is a bare number outside of mathjax: %% -\\transn{3,000.55} %%',
                    );
                    assertTransN(
                        true,
                        'this is a bare number outside of mathjax: [[-3,000,000.55]]',
                        'this is a bare number outside of mathjax: [[%% -\\transn{3,000,000.55} %%]]',
                    );
                });

                // cases when we shouldn't replace

                it('should not replace basic decimals in blanks', () => {
                    assertTransN(true, 'this is a blank outside of mathjax: [.55]');
                    assertTransN(true, 'this is a blank outside of mathjax: [5.55]');
                    assertTransN(true, 'this is a blank outside of mathjax: [-.55]');
                    assertTransN(true, 'this is a blank outside of mathjax: [-5.55]');
                });

                it('should not replace comma-delimited numbers in blanks', () => {
                    assertTransN(true, 'this is a blank outside of mathjax: [3,000,000]');
                    assertTransN(true, 'this is a blank outside of mathjax: [3,000]');
                    assertTransN(true, 'this is a blank outside of mathjax: [-3,000,000]');
                    assertTransN(true, 'this is a blank outside of mathjax: [-3,000]');
                });

                it('should not replace in blanks that are side-by-side', () => {
                    assertTransN(true, '[3,000,000]\\[5.55]');
                    assertTransN(true, '[-3,000,000]\\[-5.55]');
                });

                it('should not replace inline images', () => {
                    assertTransN(true, 'this an inlime image ![1.5testing.png]');
                    assertTransN(true, 'this an inlime image ![.5testing.png]');
                    assertTransN(true, 'this an inlime image ![1,555testing.png]');
                    assertTransN(true, 'this an inlime image ![-1.5testing.png]');
                    assertTransN(true, 'this an inlime image ![-.5testing.png]');
                    assertTransN(true, 'this an inlime image ![-1,555testing.png]');
                });

                it('should not replace numbers in a comma separated dataset', () => {
                    assertTransN(true, '{20, -30, -40, 50, 1, -2, 3}');
                    assertTransN(true, '{[20], [-30], [-40], [50], [1], [-2], [3]}');
                });
            });

            describe('in non-compose modes', () => {
                beforeEach(() => {
                    frame.mainUiComponent.editor_template = 'fill_in_the_blanks';
                });

                it('should replace basic decimals', () => {
                    assertTransN(
                        true,
                        'this is a bare number outside of mathjax: 3.55',
                        'this is a bare number outside of mathjax: %% \\transn{3.55} %%',
                    );
                    assertTransN(
                        true,
                        'this is a bare number outside of mathjax: .55',
                        'this is a bare number outside of mathjax: %% \\transn{.55} %%',
                    );
                    assertTransN(
                        true,
                        'this is a bare number outside of mathjax: -3.55',
                        'this is a bare number outside of mathjax: %% -\\transn{3.55} %%',
                    );
                    assertTransN(
                        true,
                        'this is a bare number outside of mathjax: -.55',
                        'this is a bare number outside of mathjax: %% -\\transn{.55} %%',
                    );
                });

                it('should replace comma-delimited numbers', () => {
                    assertTransN(
                        true,
                        'this is a bare number outside of mathjax: 3,000',
                        'this is a bare number outside of mathjax: %% \\transn{3,000} %%',
                    );
                    assertTransN(
                        true,
                        'this is a bare number outside of mathjax: 3,000,000',
                        'this is a bare number outside of mathjax: %% \\transn{3,000,000} %%',
                    );
                    assertTransN(
                        true,
                        'this is a bare number outside of mathjax: -3,000',
                        'this is a bare number outside of mathjax: %% -\\transn{3,000} %%',
                    );
                    assertTransN(
                        true,
                        'this is a bare number outside of mathjax: -3,000,000',
                        'this is a bare number outside of mathjax: %% -\\transn{3,000,000} %%',
                    );
                });

                it('should work with modals', () => {
                    assertTransN(
                        true,
                        'this is a bare number outside of mathjax: [[.55]]',
                        'this is a bare number outside of mathjax: [[%% \\transn{.55} %%]]',
                    );
                    assertTransN(
                        true,
                        'this is a bare number outside of mathjax: [[3,000,000]]',
                        'this is a bare number outside of mathjax: [[%% \\transn{3,000,000} %%]]',
                    );
                    assertTransN(
                        true,
                        'this is a bare number outside of mathjax: [[-.55]]',
                        'this is a bare number outside of mathjax: [[%% -\\transn{.55} %%]]',
                    );
                    assertTransN(
                        true,
                        'this is a bare number outside of mathjax: [[-3,000,000]]',
                        'this is a bare number outside of mathjax: [[%% -\\transn{3,000,000} %%]]',
                    );
                });

                it('should replace decimals with commas, too', () => {
                    assertTransN(
                        true,
                        'this is a bare number outside of mathjax: 3,000.55',
                        'this is a bare number outside of mathjax: %% \\transn{3,000.55} %%',
                    );
                    assertTransN(
                        true,
                        'this is a bare number outside of mathjax: [[3,000,000.55]]',
                        'this is a bare number outside of mathjax: [[%% \\transn{3,000,000.55} %%]]',
                    );
                    assertTransN(
                        true,
                        'this is a bare number outside of mathjax: -3,000.55',
                        'this is a bare number outside of mathjax: %% -\\transn{3,000.55} %%',
                    );
                    assertTransN(
                        true,
                        'this is a bare number outside of mathjax: [[-3,000,000.55]]',
                        'this is a bare number outside of mathjax: [[%% -\\transn{3,000,000.55} %%]]',
                    );
                });

                it('should replace basic decimals in blanks', () => {
                    assertTransN(
                        true,
                        'this is a blank outside of mathjax: [.55]',
                        'this is a blank outside of mathjax: [%% \\transn{.55} %%]',
                    );
                    assertTransN(
                        true,
                        'this is a blank outside of mathjax: [5.55]',
                        'this is a blank outside of mathjax: [%% \\transn{5.55} %%]',
                    );
                    assertTransN(
                        true,
                        'this is a blank outside of mathjax: [-.55]',
                        'this is a blank outside of mathjax: [%% -\\transn{.55} %%]',
                    );
                    assertTransN(
                        true,
                        'this is a blank outside of mathjax: [-5.55]',
                        'this is a blank outside of mathjax: [%% -\\transn{5.55} %%]',
                    );
                });

                it('should replace comma-delimited numbers in blanks', () => {
                    assertTransN(
                        true,
                        'this is a blank outside of mathjax: [3,000,000]',
                        'this is a blank outside of mathjax: [%% \\transn{3,000,000} %%]',
                    );
                    assertTransN(
                        true,
                        'this is a blank outside of mathjax: [3,000]',
                        'this is a blank outside of mathjax: [%% \\transn{3,000} %%]',
                    );
                    assertTransN(
                        true,
                        'this is a blank outside of mathjax: [-3,000,000]',
                        'this is a blank outside of mathjax: [%% -\\transn{3,000,000} %%]',
                    );
                    assertTransN(
                        true,
                        'this is a blank outside of mathjax: [-3,000]',
                        'this is a blank outside of mathjax: [%% -\\transn{3,000} %%]',
                    );
                });

                it('should replace in blanks that are side-by-side', () => {
                    assertTransN(true, '[3,000,000]\\[5.55]', '[%% \\transn{3,000,000} %%]\\[%% \\transn{5.55} %%]');
                    assertTransN(
                        true,
                        '[-3,000,000]\\[-5.55]',
                        '[%% -\\transn{3,000,000} %%]\\[%% -\\transn{5.55} %%]',
                    );
                });

                // cases where we shouldn't replace

                it('should not replace inline images', () => {
                    assertTransN(true, 'this an inlime image ![1.5testing.png]');
                    assertTransN(true, 'this an inlime image ![.5testing.png]');
                    assertTransN(true, 'this an inlime image ![1,555testing.png]');
                    assertTransN(true, 'this an inlime image ![-1.5testing.png]');
                    assertTransN(true, 'this an inlime image ![-.5testing.png]');
                    assertTransN(true, 'this an inlime image ![-1,555testing.png]');
                });

                it('should not replace numbers in a comma separated dataset', () => {
                    assertTransN(true, '{20, -30, -40, 50, 1, -2, 3}');
                    assertTransN(true, '{[20], [-30], [-40], [50], [1], [-2], [3]}');
                });
            });

            describe('multi-line replacements', () => {
                it('should replace multiple occurences of MathJax across multiple lines', () => {
                    assertTransN(
                        false,
                        '3.55\n\n.55\n\n[3.55]\n\n%% 3,000,000 %%\n\n%% Blank[3,000,000] %%',
                        '3.55\n\n.55\n\n[3.55]\n\n%% \\transn{3,000,000} %%\n\n%% Blank[\\transn{3,000,000}] %%',
                    );
                    assertTransN(
                        false,
                        '-3.55\n\n-.55\n\n[-3.55]\n\n%% -3,000,000 %%\n\n%% Blank[-3,000,000] %%',
                        '-3.55\n\n-.55\n\n[-3.55]\n\n%% -\\transn{3,000,000} %%\n\n%% Blank[-\\transn{3,000,000}] %%',
                    );
                });

                it('should replace multiple occurences within MathJax across multiple lines', () => {
                    assertTransN(
                        false,
                        '%% 3.55\n\n3,000\n\nBlank[3,000,000]\n%%',
                        '%% \\transn{3.55}\n\n\\transn{3,000}\n\nBlank[\\transn{3,000,000}]\n%%',
                    );
                    assertTransN(
                        false,
                        '%% -3.55\n\n-3,000\n\nBlank[-3,000,000]\n%%',
                        '%% -\\transn{3.55}\n\n-\\transn{3,000}\n\nBlank[-\\transn{3,000,000}]\n%%',
                    );
                });

                it('should replace multiple occurences outside of MathJax across multiple lines', () => {
                    assertTransN(
                        true,
                        '3.55\n\n.55\n\n[3.55]\n\n%% 3,000,000 %%\n\n%% Blank[3,000,000] %%',
                        '%% \\transn{3.55} %%\n\n%% \\transn{.55} %%\n\n[3.55]\n\n%% 3,000,000 %%\n\n%% Blank[3,000,000] %%',
                    );
                    assertTransN(
                        true,
                        '-3.55\n\n-.55\n\n[-3.55]\n\n%% -3,000,000 %%\n\n%% Blank[-3,000,000] %%',
                        '%% -\\transn{3.55} %%\n\n%% -\\transn{.55} %%\n\n[-3.55]\n\n%% -3,000,000 %%\n\n%% Blank[-3,000,000] %%',
                    );
                });
            });
        });
    });

    function getViewModel(options) {
        model = getModel(options);
        return frameViewModel.viewModelFor(model);
    }

    function getModel(options) {
        frame = Componentized.fixtures.getInstance();
        frameViewModel = frame.createFrameViewModel();
        return frame.addText(options);
    }
});
