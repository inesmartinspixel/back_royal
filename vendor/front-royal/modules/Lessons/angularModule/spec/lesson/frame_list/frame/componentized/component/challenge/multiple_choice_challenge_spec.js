import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/componentized_fixtures';
import 'Lessons/angularModule/spec/_helpers/componentized_spec_helper';
import 'Editor/angularModule';

describe('Componentized.Component.Challenge.MultipleChoiceChallenge', () => {
    let frame;
    let viewModel;
    let MultipleChoiceChallengeModel;
    let MultipleChoiceValidationResult;
    let Componentized;
    let ComponentEventListener;
    let SpecHelper;
    let $timeout;
    let $injector;
    let ComponentizedSpecHelper;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'FrontRoyal.Editor', 'SpecHelper');
        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                MultipleChoiceChallengeModel = $injector.get(
                    'Lesson.FrameList.Frame.Componentized.Component.Challenge.MultipleChoiceChallenge.MultipleChoiceChallengeModel',
                );
                Componentized = $injector.get('Lesson.FrameList.Frame.Componentized');
                ComponentEventListener = $injector.get(
                    'Lesson.FrameList.Frame.Componentized.Component.ComponentEventListener',
                );
                ComponentizedSpecHelper = $injector.get('ComponentizedSpecHelper');
                MultipleChoiceValidationResult = $injector.get(
                    'Lesson.FrameList.Frame.Componentized.Component.ChallengeValidator.ValidationResult.MultipleChoiceValidationResult',
                );
                SpecHelper = $injector.get('SpecHelper');
                $timeout = $injector.get('$timeout');
                $injector.get('ComponentizedFixtures');
                viewModel = getViewModel();
                SpecHelper.stubEventLogging();
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('Model', () => {
        describe('isExpectedAnswer', () => {
            it('should delegate to validator', () => {
                const model = viewModel.model;
                jest.spyOn(model.validator, 'isExpectedAnswer').mockImplementation(() => {});
                const answerViewModel = viewModel.answersViewModels[0];
                model.isExpectedAnswer(answerViewModel.model);
                expect(model.validator.isExpectedAnswer).toHaveBeenCalledWith(answerViewModel.model);
            });
        });
    });

    describe('ViewModel', () => {
        describe('showingIncorrectStyling', () => {
            it('should be true if any of the answers are showing incorrect styling', () => {
                SpecHelper.expectEqual(
                    false,
                    viewModel.showingIncorrectStyling,
                    'showingIncorrectStyling initially false',
                );

                viewModel.answersViewModels[0].addIncorrectStyling();

                SpecHelper.expectEqual(
                    false,
                    viewModel.showingIncorrectStyling,
                    'showingIncorrectStyling false when answer is incorrect and inactive',
                );

                viewModel.active = true;

                SpecHelper.expectEqual(
                    true,
                    viewModel.showingIncorrectStyling,
                    'showingIncorrectStyling true when answer is incorrect and active',
                );

                viewModel.answersViewModels[0].removeIncorrectStyling();

                SpecHelper.expectEqual(
                    false,
                    viewModel.showingIncorrectStyling,
                    'showingIncorrectStyling false when answer has incorrect styling removed',
                );
            });
        });

        describe('challengeResponses', () => {
            it('should return a challenge response for each selected answer', () => {
                SpecHelper.expectEqual([], viewModel.challengeResponses, 'initially empty');
                viewModel.toggleAnswer(viewModel.answersViewModels[0]);
                SpecHelper.expectEqual(
                    [viewModel.answersViewModels[0].model],
                    viewModel.userAnswers,
                    '1st answer selected',
                );

                viewModel.toggleAnswer(viewModel.answersViewModels[1]);
                SpecHelper.expectEqual(
                    [viewModel.answersViewModels[0].model, viewModel.answersViewModels[1].model],
                    viewModel.userAnswers,
                    '2nd answer selected',
                );

                viewModel.toggleAnswer(viewModel.answersViewModels[0]);
                SpecHelper.expectEqual(
                    [viewModel.answersViewModels[1].model],
                    viewModel.userAnswers,
                    '1st answer unselected',
                );
            });
        });

        describe('onActivated', () => {
            it('should set the currentChallenge on the answerList', () => {
                const answerListViewModel = viewModel.answerListViewModel;
                SpecHelper.expectEqual(
                    false,
                    !!answerListViewModel.currentChallengeViewModel,
                    'answerListViewModel.currentChallengeViewModel initially not set',
                );
                viewModel.active = true;
                SpecHelper.expectEqual(
                    viewModel.model.id,
                    answerListViewModel.currentChallengeViewModel.model.id,
                    'answerListViewModel.currentChallengeViewModel set to this challenge when activated',
                );
            });
        });

        describe('onDectivated', () => {
            it('should unset the currentChallenge on the answerList', () => {
                const answerListViewModel = viewModel.answerListViewModel;
                viewModel.active = true;
                viewModel.active = false;
                SpecHelper.expectEqual(
                    undefined,
                    answerListViewModel.currentChallengeViewModel,
                    'answerListViewModel.currentChallengeViewModel after deActivated',
                );
            });
        });

        describe('when created initially with no answerList', () => {
            beforeEach(() => {
                const answerList = viewModel.model.answerList;
                viewModel.model.answerList = undefined;
                // recreate the viewModel, now that the model has no answerList
                viewModel = viewModel.model.createViewModel(frame.createFrameViewModel());
                // add an answerList after the view model was created
                viewModel.model.answerList = answerList;
            });

            describe('onActivated', () => {
                it('should set the currentChallenge on the answerList', () => {
                    const answerListViewModel = viewModel.answerListViewModel;
                    SpecHelper.expectEqual(
                        false,
                        !!answerListViewModel.currentChallengeViewModel,
                        'answerListViewModel.currentChallengeViewModel initially not set',
                    );
                    viewModel.active = true;
                    SpecHelper.expectEqual(
                        viewModel.model.id,
                        answerListViewModel.currentChallengeViewModel.model.id,
                        'answerListViewModel.currentChallengeViewModel set to this challenge when activated',
                    );
                });
            });

            describe('onDeActivated', () => {
                it('should unset the currentChallenge on the answerList', () => {
                    const answerListViewModel = viewModel.answerListViewModel;
                    viewModel.active = true;
                    viewModel.active = false;
                    SpecHelper.expectEqual(
                        undefined,
                        answerListViewModel.currentChallengeViewModel,
                        'answerListViewModel.currentChallengeViewModel after deActivated',
                    );
                });
            });
        });

        describe('when answerList is changed', () => {
            beforeEach(() => {
                const answerList = frame.addAnswerList().addDefaultReferences();
                viewModel.model.answerList = answerList;
            });

            describe('onActivated', () => {
                it('should set the currentChallenge on the answerList', () => {
                    const answerListViewModel = viewModel.answerListViewModel;
                    SpecHelper.expectEqual(
                        false,
                        !!answerListViewModel.currentChallengeViewModel,
                        'answerListViewModel.currentChallengeViewModel initially not set',
                    );
                    viewModel.active = true;
                    SpecHelper.expectEqual(
                        viewModel.model.id,
                        answerListViewModel.currentChallengeViewModel.model.id,
                        'answerListViewModel.currentChallengeViewModel set to this challenge when activated',
                    );
                });
            });

            describe('onDeActivated', () => {
                it('should unset the currentChallenge on the answerList', () => {
                    const answerListViewModel = viewModel.answerListViewModel;
                    viewModel.active = true;
                    viewModel.active = false;
                    SpecHelper.expectEqual(
                        undefined,
                        answerListViewModel.currentChallengeViewModel,
                        'answerListViewModel.currentChallengeViewModel after deActivated',
                    );
                });
            });
        });

        describe('toggleAnswer', () => {
            it('should toggle the selected property on an answer', () => {
                const answerViewModel = viewModel.answersViewModels[0];
                SpecHelper.expectEqual(false, answerViewModel.selected, 'selected initially false');

                viewModel.toggleAnswer(answerViewModel);
                SpecHelper.expectEqual(true, answerViewModel.selected, 'selected toggled to true');

                viewModel.toggleAnswer(answerViewModel);
                SpecHelper.expectEqual(false, answerViewModel.selected, 'selected toggled to false');
            });
        });

        describe('MultipleChoiceValidationResult', () => {
            it('should fire validated with an instance of MultipleChoiceValidationResult', () => {
                stubAnsweredIncorrectlyStoredInClientStorage();
                let called = false;
                new ComponentEventListener(viewModel, 'validated', validationResult => {
                    called = true;
                    SpecHelper.expectEqual(
                        MultipleChoiceValidationResult,
                        validationResult.constructor,
                        'validationResult constructor',
                    );
                });
                viewModel.validate();
                SpecHelper.expectEqual(true, called, 'callback called');
            });
        });

        describe('getScore', () => {
            beforeEach(stubAnsweredIncorrectlyStoredInClientStorage);

            it('should be undefined if no_incorrect_answers', () => {
                viewModel.model.no_incorrect_answers = true;
                expect(viewModel.getScore()).toBeUndefined();
            });

            it('should be null if no incorrect validationResults and no correct validationResults', () => {
                viewModel.validationResults = [
                    {
                        totallyIncorrect: false,
                    },
                    {
                        totallyIncorrect: true,
                        causedByEvent: 'something else',
                    },
                ];
                expect(viewModel.getScore()).toBe(null);
            });

            it('should be 1 if no incorrect validationResults and a correct validationResults', () => {
                viewModel.validationResults = [
                    {
                        totallyIncorrect: false,
                    },
                    {
                        totallyIncorrect: true,
                        causedByEvent: 'something else',
                    },
                    {
                        result: true,
                    },
                ];
                expect(viewModel.getScore()).toBe(1);
            });

            it('should be 0 if there is an incorrect validationResult', () => {
                viewModel.validationResults = [
                    {
                        totallyIncorrect: false,
                    },
                    {
                        totallyIncorrect: true,
                        causedByEvent: 'answerSelected',
                    },
                ];
                expect(viewModel.getScore()).toBe(0);
            });

            it('should be 0 if testOrAssessment and answeredIncorrectlyStoredInClientStorage', () => {
                viewModel.playerViewModel.lesson.testOrAssessment = true;
                viewModel.answeredIncorrectlyStoredInClientStorage = true;
                viewModel.validationResults = [
                    {
                        result: true,
                    },
                ];
                expect(viewModel.getScore()).toBe(0);
            });

            it('should be 1 if !testOrAssessment and answeredIncorrectlyStoredInClientStorage', () => {
                viewModel.playerViewModel.lesson.testOrAssessment = false;
                viewModel.answeredIncorrectlyStoredInClientStorage = true;
                viewModel.validationResults = [
                    {
                        result: true,
                    },
                ];
                expect(viewModel.getScore()).toBe(1);
            });

            it('should be 1 if testOrAssessment and !answeredIncorrectlyStoredInClientStorage', () => {
                viewModel.playerViewModel.lesson.testOrAssessment = true;
                viewModel.answeredIncorrectlyStoredInClientStorage = false;
                viewModel.validationResults = [
                    {
                        result: true,
                    },
                ];
                expect(viewModel.getScore()).toBe(1);
            });
        });

        describe('availableAnswersViewModels', () => {
            it('should return answers that are not disabled', () => {
                const answerViewModel = viewModel.answersViewModels[0];
                expect(viewModel.availableAnswersViewModels.includes(answerViewModel)).toBe(true);
                Object.defineProperty(answerViewModel, 'disabled', {
                    value: true,
                });
                expect(viewModel.availableAnswersViewModels.includes(answerViewModel)).toBe(false);
            });
        });

        describe('behaviors', () => {
            describe('DisallowMultipleSelect', () => {
                it('should allow only one selected answer', () => {
                    viewModel = getViewModel({
                        behaviors: {
                            DisallowMultipleSelect: {},
                        },
                    });
                    viewModel.active = true;
                    viewModel.answersViewModels[0].selected = true;
                    viewModel.answersViewModels[1].selected = true;
                    SpecHelper.expectEqual(false, viewModel.answersViewModels[0].selected, 'answer 0 was deselected');
                    SpecHelper.expectEqual(true, viewModel.answersViewModels[1].selected, 'answer 1 is still selected');
                });

                it('should do nothing when not active', () => {
                    viewModel = getViewModel({
                        behaviors: {
                            DisallowMultipleSelect: {},
                        },
                    });
                    viewModel.active = false;
                    viewModel.answersViewModels[0].selected = true;
                    viewModel.answersViewModels[1].selected = true;
                    SpecHelper.expectEqual(true, viewModel.answersViewModels[0].selected, 'answer 0 is still selected');
                    SpecHelper.expectEqual(true, viewModel.answersViewModels[1].selected, 'answer 1 is still selected');
                });
            });

            describe('ImmediateValidation', () => {
                it('should validate when an answer is selected', () => {
                    viewModel = getViewModel({
                        behaviors: {
                            ImmediateValidation: {},
                        },
                    });
                    viewModel.active = true;
                    jest.spyOn(viewModel.validatorViewModel, 'validate').mockImplementation(() => {});

                    viewModel.answersViewModels[0].selected = true;
                    expect(viewModel.validatorViewModel.validate).toHaveBeenCalledWith({
                        event: {
                            target: viewModel.answersViewModels[0],
                            type: 'answerSelected',
                        },
                    });
                });

                it('should validate when an answer is unselected', () => {
                    viewModel = getViewModel({
                        behaviors: {
                            ImmediateValidation: {},
                        },
                    });
                    viewModel.answersViewModels[0].selected = true;
                    viewModel.active = true;
                    jest.spyOn(viewModel.validatorViewModel, 'validate').mockImplementation(() => {});

                    viewModel.answersViewModels[0].selected = false;
                    expect(viewModel.validatorViewModel.validate).toHaveBeenCalledWith({
                        event: {
                            target: viewModel.answersViewModels[0],
                            type: 'answerUnselected',
                        },
                    });
                });

                it('should do nothing when not active', () => {
                    viewModel = getViewModel({
                        behaviors: {
                            ImmediateValidation: {},
                        },
                    });
                    viewModel.active = false;
                    jest.spyOn(viewModel.validatorViewModel, 'validate').mockImplementation(() => {});

                    viewModel.answersViewModels[0].selected = true;
                    expect(viewModel.validatorViewModel.validate).not.toHaveBeenCalled();
                });

                it('should cleanup when the answer list is changed more than once', () => {
                    viewModel = getViewModel({
                        behaviors: {
                            ImmediateValidation: {},
                        },
                    });
                    const answerList = viewModel.model.answerList;
                    viewModel.model.answerList = undefined;
                    viewModel.model.answerList = answerList;

                    viewModel.active = true;
                    jest.spyOn(viewModel.validatorViewModel, 'validate').mockImplementation(() => {});

                    viewModel.answersViewModels[0].selected = true;
                    expect(viewModel.validatorViewModel.validate.mock.calls.length).toBe(1);
                });

                it('should be possible to turn it off', () => {
                    viewModel = getViewModel({
                        behaviors: {
                            ImmediateValidation: {},
                        },
                    });
                    viewModel.active = true;
                    jest.spyOn(viewModel.validatorViewModel, 'validate').mockImplementation(() => {});

                    viewModel.model.behaviors.ImmediateValidation = undefined;
                    viewModel.answersViewModels[0].selected = true;
                    expect(viewModel.validatorViewModel.validate).not.toHaveBeenCalled();
                });
            });

            describe('ReadyToValidateWhenAnswerIsSelected', () => {
                it('should be ready to validate so long as an unvalidated answer is selected', () => {
                    viewModel = getViewModel({
                        behaviors: {
                            ReadyToValidateWhenAnswerIsSelected: {},
                        },
                    });
                    SpecHelper.expectEqual(false, viewModel.readyToValidate, 'readyToValidate before selection');
                    viewModel.answersViewModels[0].selected = true;
                    SpecHelper.expectEqual(true, viewModel.readyToValidate, 'readyToValidate after selection');
                    viewModel.answersViewModels[0].addCorrectStyling();
                    SpecHelper.expectEqual(
                        false,
                        viewModel.readyToValidate,
                        'readyToValidate after showing validation',
                    );
                });
            });

            describe('ResetAnswersOnActivated', () => {
                it('should reset the answer list when activated', () => {
                    viewModel = getViewModel({
                        behaviors: {
                            ResetAnswersOnActivated: {},
                        },
                    });
                    jest.spyOn(viewModel.answerListViewModel, 'clearStates').mockImplementation(() => {});
                    viewModel.active = true;
                    expect(viewModel.answerListViewModel.clearStates).toHaveBeenCalled();
                });
            });

            describe('ShowCorrectStyling', () => {
                it('should add correct styling on validation', () => {
                    viewModel = getViewModel({
                        behaviors: {
                            ShowCorrectStyling: {},
                        },
                    });
                    assertAddedOnCorrectValidation('showingCorrectStyling');
                });
            });

            describe('PlayScalingSoundOnSelected', () => {
                it('should play a sound when an answer is selected', () => {
                    viewModel = getViewModel({
                        behaviors: {
                            PlayScalingSoundOnSelected: {},
                        },
                    });
                    const SoundManager = $injector.get('SoundManager');
                    const SoundConfig = $injector.get('SoundConfig');

                    jest.spyOn(SoundManager, 'playUrl').mockImplementation(() => {});

                    viewModel.answersViewModels[0].selected = true;
                    expect(SoundManager.playUrl).toHaveBeenCalledWith(SoundConfig.SCALING_CLICKS[0]);

                    viewModel.answersViewModels[1].selected = true;
                    expect(SoundManager.playUrl).toHaveBeenCalledWith(SoundConfig.SCALING_CLICKS[1]);
                });
            });

            describe('ClearMessagesOnAnswerSelect', () => {
                it('should clear messages when an answer is selected', () => {
                    viewModel = getViewModel({
                        behaviors: {
                            ClearMessagesOnAnswerSelect: {},
                        },
                    });
                    viewModel.frameViewModel.playerViewModel = {
                        clearMessage: jest.fn(),
                    };
                    viewModel.answersViewModels[0].selected = true;
                    expect(viewModel.playerViewModel.clearMessage).toHaveBeenCalled();
                });
            });

            describe('FlashIncorrectStyling', () => {
                it('should flash incorrect styling on incorrectly selected answers', () => {
                    viewModel = getViewModel({
                        behaviors: {
                            FlashIncorrectStyling: {},
                        },
                    });
                    validateOneCorrectAndOneIncorrectAnswer();

                    SpecHelper.expectEqual(
                        true,
                        viewModel.answersViewModels[0].showingIncorrectStyling,
                        'showing incorrect styling on incorrectly selected answer',
                    );

                    SpecHelper.expectEqual(
                        false,
                        viewModel.answersViewModels[1].showingIncorrectStyling,
                        'no incorrect styling on correctly selected answer',
                    );

                    SpecHelper.expectEqual(
                        false,
                        viewModel.answersViewModels[2].showingIncorrectStyling,
                        'no incorrect styling on unselected answer',
                    );

                    $timeout.flush();

                    SpecHelper.expectEqual(
                        false,
                        viewModel.answersViewModels[0].showingIncorrectStyling,
                        'incorrect styling removed from incorrectly selected answer',
                    );

                    SpecHelper.expectEqual(
                        false,
                        viewModel.answersViewModels[0].selected,
                        'incorrectly selected answer unselected',
                    );
                });

                it('should remove incorrect styling if an incorrect answer is deselected', () => {
                    assertRemovesIncorrectStyling(viewModel => {
                        viewModel.answersViewModels[0].selected = false;
                    });
                });

                it('should remove incorrect styling if challenge is deactivated', () => {
                    assertRemovesIncorrectStyling(viewModel => {
                        viewModel.active = false;
                    });
                });

                it('should not add incorrect styling twice if validations happen quickly', () => {
                    viewModel = getViewModel({
                        behaviors: {
                            FlashIncorrectStyling: {},
                        },
                    });
                    const incorrectAnswerViewModel = viewModel.answerListViewModel.answersViewModels[0];
                    jest.spyOn(incorrectAnswerViewModel, 'addIncorrectStyling');
                    validateOneCorrectAndOneIncorrectAnswer();
                    fireValidated();
                    expect(incorrectAnswerViewModel.addIncorrectStyling.mock.calls.length).toBe(1);
                });

                function assertRemovesIncorrectStyling(func) {
                    // the timeout should be cancelled

                    SpecHelper.verifyNoNewTimeouts(() => {
                        viewModel = getViewModel({
                            behaviors: {
                                FlashIncorrectStyling: {},
                            },
                        });
                        validateOneCorrectAndOneIncorrectAnswer();

                        SpecHelper.expectEqual(
                            true,
                            viewModel.answersViewModels[0].showingIncorrectStyling,
                            'showing incorrect styling on incorrectly selected answer',
                        );

                        func(viewModel);

                        // styling should have been removed
                        SpecHelper.expectEqual(
                            false,
                            viewModel.answersViewModels[0].showingIncorrectStyling,
                            'incorrect styling remains on incorrectly selected answer',
                        );
                    });
                }
            });

            function validateOneCorrectAndOneIncorrectAnswer() {
                viewModel.active = true;

                // select answers 0 and 1
                viewModel.toggleAnswer(viewModel.answersViewModels[0]);
                viewModel.toggleAnswer(viewModel.answersViewModels[1]);

                // validate. answer 0 is incorrect; answer 1 is correct
                fireValidated();
            }

            function fireValidated() {
                const validationResult = new MultipleChoiceValidationResult(viewModel, []);
                jest.spyOn(validationResult, 'hasErrorRelatedToAnswer').mockImplementation(
                    answer => answer === viewModel.answersViewModels[0].model,
                );
                viewModel.validatorViewModel.fire('validated', validationResult);
            }

            function assertAddedOnCorrectValidation(prop) {
                validateOneCorrectAndOneIncorrectAnswer();

                SpecHelper.expectEqual(
                    false,
                    viewModel.answersViewModels[0][prop],
                    `no ${prop} on incorrectly selected answer`,
                );

                SpecHelper.expectEqual(
                    true,
                    viewModel.answersViewModels[1][prop],
                    `${prop} on correctly selected answer`,
                );

                SpecHelper.expectEqual(
                    false,
                    viewModel.answersViewModels[2][prop],
                    `${prop} styling on unselected answer`,
                );
            }
        });

        describe('validatedIncorrect handling', () => {
            it('should set answeredIncorrectlyStoredInClientStorage if totallyIncorrect and caused by an answerSelected event', () => {
                stubPlayerViewModelLocalStorage();
                viewModel.fire('validatedIncorrect', {
                    result: false,
                    totallyIncorrect: true,
                    causedByEvent: 'answerSelected',
                });
                expect(viewModel.answeredIncorrectlyStoredInClientStorage).toBe(true);
            });

            it('should not set answeredIncorrectlyStoredInClientStorage if partiallyIncorrect', () => {
                stubPlayerViewModelLocalStorage();
                viewModel.fire('validatedIncorrect', {
                    result: false,
                    totallyIncorrect: false,
                    causedByEvent: 'answerSelected',
                });
                expect(viewModel.answeredIncorrectlyStoredInClientStorage).not.toBe(true);
            });

            it('should not set answeredIncorrectlyStoredInClientStorage if not caused by an answerSelected event', () => {
                stubPlayerViewModelLocalStorage();
                viewModel.fire('validatedIncorrect', {
                    result: false,
                    totallyIncorrect: false,
                    causedByEvent: 'somethingElse',
                });
                expect(viewModel.answeredIncorrectlyStoredInClientStorage).not.toBe(true);
            });
        });

        function stubPlayerViewModelLocalStorage() {
            jest.spyOn(viewModel, 'playerViewModel', 'get').mockReturnValue({
                getLessonClientStorage(keyParts) {
                    return this[JSON.stringify(keyParts)];
                },
                setLessonClientStorage(keyParts, val) {
                    this[JSON.stringify(keyParts)] = val;
                },
            });
        }

        function stubAnsweredIncorrectlyStoredInClientStorage() {
            Object.defineProperty(viewModel, 'playerViewModel', {
                value: {
                    lesson: {},
                    setChallengeScore: jest.fn(),
                },
            });
            let answeredIncorrectlyStoredInClientStorage;
            Object.defineProperty(viewModel, 'answeredIncorrectlyStoredInClientStorage', {
                get() {
                    return answeredIncorrectlyStoredInClientStorage;
                },
                set(val) {
                    answeredIncorrectlyStoredInClientStorage = val;
                },
            });
        }
    });

    describe('EditorViewModel', () => {
        let editorViewModel;
        let model;
        beforeEach(() => {
            editorViewModel = MultipleChoiceChallengeModel.EditorViewModel.addComponentTo(frame).setup();
            editorViewModel.addAnswerList();
            editorViewModel.model.answerList.editorViewModel.addAnswer();
            model = editorViewModel.model;
        });
        describe('setup', () => {
            it('sets up an answer list', () => {
                SpecHelper.expectEqual('AnswerListModel', editorViewModel.model.answerList.type);
            });
        });

        describe('correctAnswerText', () => {
            beforeEach(() => {
                editorViewModel.correctAnswer = frame.addSelectableAnswer().addDefaultReferences();
            });

            describe('get', () => {
                it('should get the text from the correctAnswer if there is one', () => {
                    expect(editorViewModel.correctAnswerText).toBe(editorViewModel.correctAnswer.text.text);
                });

                it('should be undefined if no correctAnswer', () => {
                    editorViewModel.correctAnswer = undefined;
                    expect(editorViewModel.correctAnswerText).toBeUndefined();
                });

                it('should be undefined if correct answer has no text', () => {
                    editorViewModel.correctAnswer.text = undefined;
                    expect(editorViewModel.correctAnswerText).toBeUndefined();
                });
            });

            describe('set', () => {
                it('should set the text on the correctAnswer if there is one', () => {
                    expect(editorViewModel.correctAnswer).not.toBeUndefined(); // sanity check
                    editorViewModel.correctAnswerText = 'Some text';
                    expect(editorViewModel.correctAnswer.text.text).toBe('Some text');
                });

                it('should create a correctAnswer if there is not one', () => {
                    editorViewModel.correctAnswer = undefined;
                    editorViewModel.correctAnswerText = 'Some text';
                    expect(editorViewModel.correctAnswer).not.toBeUndefined();
                    expect(editorViewModel.correctAnswer.text.text).toBe('Some text');
                });

                it('should change content type of answer if not already text', () => {
                    editorViewModel.correctAnswer.image = frame.addImage();
                    editorViewModel.correctAnswerText = 'Some text';
                    expect(editorViewModel.correctAnswerText).toBe('Some text');
                });
            });
        });

        describe('correctAnswerImage', () => {
            beforeEach(() => {
                editorViewModel.correctAnswer = frame.addSelectableAnswer().addDefaultReferences();
                editorViewModel.correctAnswer.image = frame.addImage();
            });

            describe('get', () => {
                it('should get the image from the correctAnswer if there is one', () => {
                    expect(editorViewModel.correctAnswerImage).toBe(editorViewModel.correctAnswer.image);
                });

                it('should be undefined if no correctAnswer', () => {
                    editorViewModel.correctAnswer = undefined;
                    expect(editorViewModel.correctAnswerImage).toBeUndefined();
                });

                it('should be undefined if correct answer has no image', () => {
                    editorViewModel.correctAnswer.image = undefined;
                    expect(editorViewModel.correctAnswerImage).toBeUndefined();
                });
            });

            describe('set', () => {
                it('should set the image on the correctAnswer if there is one', () => {
                    expect(editorViewModel.correctAnswer).not.toBeUndefined(); // sanity check
                    const image = (editorViewModel.correctAnswerImage = frame.addImage());
                    expect(editorViewModel.correctAnswer.image).toBe(image);
                });

                it('should create a correctAnswer if there is not one', () => {
                    editorViewModel.correctAnswer = undefined;
                    const image = (editorViewModel.correctAnswerImage = frame.addImage());
                    expect(editorViewModel.correctAnswer).not.toBeUndefined();
                    expect(editorViewModel.correctAnswer.image).toBe(image);
                });
            });
        });

        describe('correctAnswer', () => {
            describe('get', () => {
                it('should throw if multiple correct answers', () => {
                    model.validator.expectedAnswerMatchers = [
                        frame.addSimilarToSelectableAnswer().addDefaultReferences(),
                        frame.addSimilarToSelectableAnswer().addDefaultReferences(),
                    ];
                    expect(() => {
                        editorViewModel.correctAnswer;
                    }).toThrow(new Error('There is more than one correct answer'));
                });
                it('should return undefined of no matchers', () => {
                    model.validator.expectedAnswerMatchers = [];
                    expect(editorViewModel.correctAnswer).toBeUndefined();
                });
                it('should return the correct answer', () => {
                    model.validator.expectedAnswerMatchers = [
                        frame.addSimilarToSelectableAnswer().addDefaultReferences(),
                    ];
                    expect(editorViewModel.correctAnswer).toBe(model.validator.expectedAnswerMatchers[0].answer);
                });
            });
            describe('set', () => {
                it('should throw if not a SelectableAnswerModel', () => {
                    expect(() => {
                        editorViewModel.correctAnswer = frame.addAnswer();
                    }).toThrow(new Error('unexpected answer type AnswerModel'));
                });
                it('should remove current answers', () => {
                    const matchers = [
                        frame.addSimilarToSelectableAnswer().addDefaultReferences(),
                        frame.addSimilarToSelectableAnswer().addDefaultReferences(),
                    ];
                    jest.spyOn(matchers[0], 'remove').mockImplementation(() => {});
                    jest.spyOn(matchers[1], 'remove').mockImplementation(() => {});
                    model.validator.expectedAnswerMatchers = matchers;
                    editorViewModel.correctAnswer = frame.addSelectableAnswer();
                    expect(matchers[0].remove).toHaveBeenCalled();
                    expect(matchers[1].remove).toHaveBeenCalled();
                });
                it('should create an answer matcher', () => {
                    const answer = frame.addSelectableAnswer();
                    editorViewModel.correctAnswer = answer;
                    const matchers = model.validator.expectedAnswerMatchers;
                    expect(matchers.length).toEqual(1);
                    const matcher = matchers[0];
                    expect(matcher.answer).toBe(answer);
                });
                it('should be settable to undefined', () => {
                    const answer = frame.addSelectableAnswer();
                    editorViewModel.correctAnswer = answer;
                    editorViewModel.correctAnswer = undefined;
                    const matchers = model.validator.expectedAnswerMatchers;
                    expect(matchers.length).toEqual(0);
                });
                it('should add the answer to the answerList if it is not already there', () => {
                    const answer = frame.addSelectableAnswer();
                    SpecHelper.expectEqual(
                        false,
                        editorViewModel.model.answerList.answers.includes(answer),
                        'answer in answerList',
                    );
                    editorViewModel.correctAnswer = answer;
                    SpecHelper.expectEqual(
                        true,
                        editorViewModel.model.answerList.answers.includes(answer),
                        'answer in answerList',
                    );
                });
            });
        });

        describe('wrapTransN', () => {
            it('should work with text to be corrected', () => {
                model.validator.expectedAnswerMatchers = [frame.addSimilarToSelectableAnswer().addDefaultReferences()];
                editorViewModel.correctAnswers = [frame.addSelectableAnswer()];
                editorViewModel.correctAnswers[0].text.text = '\\transn{1.234}';
                model.editorViewModel.wrapTransN();
                expect(editorViewModel.correctAnswerText).toBe('%% \\transn{1.234} %%');
                expect(model.unlink_blank_from_answer).toBe(true);
            });

            it('should work with text that is already correct', () => {
                model.validator.expectedAnswerMatchers = [frame.addSimilarToSelectableAnswer().addDefaultReferences()];
                editorViewModel.correctAnswers = [frame.addSelectableAnswer()];
                editorViewModel.correctAnswers[0].text.text = '%% \\transn{1.234} %%';
                model.editorViewModel.wrapTransN();
                expect(editorViewModel.correctAnswerText).toBe('%% \\transn{1.234} %%');
                expect(model.unlink_blank_from_answer).not.toBe(true);
            });

            it('should work with no correct answers', () => {
                model.validator.expectedAnswerMatchers = [];
                model.editorViewModel.wrapTransN();
            });

            it('should not throw errors with multiple correct answers', () => {
                model.validator.expectedAnswerMatchers = [
                    frame.addSimilarToSelectableAnswer().addDefaultReferences(),
                    frame.addSimilarToSelectableAnswer().addDefaultReferences(),
                ];
                editorViewModel.correctAnswers = [frame.addSelectableAnswer(), frame.addSelectableAnswer()];
                model.editorViewModel.wrapTransN();
            });
        });

        describe('correctAnswers', () => {
            describe('get', () => {
                it('should return undefined of no matchers', () => {
                    model.validator.expectedAnswerMatchers = [];
                    expect(editorViewModel.correctAnswer).toBeUndefined();
                });
                it('should return the correct answer', () => {
                    model.validator.expectedAnswerMatchers = [
                        frame.addSimilarToSelectableAnswer().addDefaultReferences(),
                        frame.addSimilarToSelectableAnswer().addDefaultReferences(),
                    ];
                    expect(editorViewModel.correctAnswers[0]).toBe(model.validator.expectedAnswerMatchers[0].answer);
                    expect(editorViewModel.correctAnswers[1]).toBe(model.validator.expectedAnswerMatchers[1].answer);
                });
            });
            describe('set', () => {
                it('should throw if not a SelectableAnswerModel', () => {
                    expect(() => {
                        editorViewModel.correctAnswers = [frame.addAnswer()];
                    }).toThrow(new Error('unexpected answer type AnswerModel'));
                });
                it('should remove current answers', () => {
                    const matchers = [
                        frame.addSimilarToSelectableAnswer().addDefaultReferences(),
                        frame.addSimilarToSelectableAnswer().addDefaultReferences(),
                    ];
                    jest.spyOn(matchers[0], 'remove').mockImplementation(() => {});
                    jest.spyOn(matchers[1], 'remove').mockImplementation(() => {});
                    model.validator.expectedAnswerMatchers = matchers;
                    editorViewModel.correctAnswers = [frame.addSelectableAnswer()];
                    expect(matchers[0].remove).toHaveBeenCalled();
                    expect(matchers[1].remove).toHaveBeenCalled();
                });
                it('should create an answer matcher', () => {
                    const answer1 = frame.addSelectableAnswer();
                    const answer2 = frame.addSelectableAnswer();
                    editorViewModel.correctAnswers = [answer1, answer2];
                    const matchers = model.validator.expectedAnswerMatchers;
                    expect(matchers.length).toEqual(2);
                    expect(matchers[0].answer).toBe(answer1);
                    expect(matchers[1].answer).toBe(answer2);
                });
                it('should be settable to undefined', () => {
                    const answer = frame.addSelectableAnswer();
                    editorViewModel.correctAnswer = answer;
                    editorViewModel.correctAnswer = undefined;
                    const matchers = model.validator.expectedAnswerMatchers;
                    expect(matchers.length).toEqual(0);
                });
            });
        });

        describe('correctAnswerIds', () => {
            it('should return the ids for the correct answers', () => {
                const answer = frame.addSelectableAnswer();
                editorViewModel.correctAnswers = [answer];
                expect(editorViewModel.correctAnswerIds).toEqual([answer.id]);
            });
        });

        describe('hasMessageFor', () => {
            it('should return false if there is no message', () => {
                jest.spyOn(editorViewModel, 'messageComponentFor').mockReturnValue(undefined);
                expect(editorViewModel.hasMessageFor('ANSWER', 'EVENT')).toBe(false);
                expect(editorViewModel.messageComponentFor).toHaveBeenCalledWith('ANSWER', 'EVENT');
            });
            it('should return true if there is a message', () => {
                jest.spyOn(editorViewModel, 'messageComponentFor').mockReturnValue({});
                expect(editorViewModel.hasMessageFor('ANSWER', 'EVENT')).toBe(true);
                expect(editorViewModel.messageComponentFor).toHaveBeenCalledWith('ANSWER', 'EVENT');
            });
        });

        describe('messageComponentFor', () => {
            it('should return a message that applies to the answer', () => {
                editorViewModel.messageEvent = 'EVENT';
                const message = editorViewModel.addMessageFor(model.answers[0]).model;
                expect(editorViewModel.messageComponentFor(model.answers[0], 'EVENT')).toBe(message);
            });
            it('should return undefined if nothing found', () => {
                expect(editorViewModel.messageComponentFor(model.answers[0], 'EVENT')).toBeUndefined();
            });
            it('should pay attention to the event', () => {
                editorViewModel.addMessageFor(model.answers[0], 'EVENT');
                expect(editorViewModel.messageComponentFor(model.answers[0], 'DIFFERENT_EVENT')).toBeUndefined();
            });
        });

        describe('messageEditorViewModelFor', () => {
            it('should return a message that applies to the answer', () => {
                editorViewModel.messageEvent = 'EVENT';
                const message = editorViewModel.addMessageFor(model.answers[0]).model;
                expect(editorViewModel.messageEditorViewModelFor(model.answers[0], 'EVENT').model).toBe(message);
            });
            it('should return undefined if nothing found', () => {
                expect(editorViewModel.messageEditorViewModelFor(model.answers[0], 'EVENT')).toBeUndefined();
            });
            it('should use the current messageEvent', () => {
                editorViewModel.messageEvent = 'EVENT';
                editorViewModel.addMessageFor(model.answers[0]);
                expect(editorViewModel.messageEditorViewModelFor(model.answers[0], 'DIFFERENT_EVENT')).toBeUndefined();
            });
        });

        describe('addMessageFor', () => {
            it('should add a message', () => {
                const message = editorViewModel.addMessageFor(model.answers[0]).model;
                expect(message.event).toBe('validated');
                expect(message.answerMatcher.answer).toBe(model.answers[0]);
            });
        });

        describe('noIncorrectAnswers', () => {
            beforeEach(() => {
                editorViewModel.supportsNoIncorrectAnswers = {
                    ImmediateValidation: true,
                };
            });

            describe('set', () => {
                describe('with ImmediateValidation=false', () => {
                    beforeEach(() => {
                        editorViewModel.supportsNoIncorrectAnswers = {
                            ImmediateValidation: false,
                        };
                    });

                    it('should turn off ImmediateValidation when on and back on when off', () => {
                        editorViewModel.model.behaviors.ImmediateValidation = {};
                        editorViewModel.noIncorrectAnswers = true;
                        expect(editorViewModel.model.behaviors.ImmediateValidation).toBeUndefined();
                        editorViewModel.noIncorrectAnswers = false;
                        expect(editorViewModel.model.behaviors.ImmediateValidation).toEqual({});
                    });

                    it('should convert all message actions to selected when on and revert when off', () => {
                        const message = editorViewModel.addMessageFor(
                            editorViewModel.model.answerList.answers[0],
                            'validated',
                        ).model;
                        editorViewModel.noIncorrectAnswers = true;
                        expect(message.event).toBe('selected');
                        editorViewModel.noIncorrectAnswers = false;
                        expect(message.event).toBe('validated');
                    });

                    it('should add ReadyToValidateWhenAnswerIsSelected behavior when on and revert when off', () => {
                        editorViewModel.noIncorrectAnswers = true;
                        expect(editorViewModel.model.behaviors.ReadyToValidateWhenAnswerIsSelected).toEqual({});
                        editorViewModel.noIncorrectAnswers = false;
                        expect(editorViewModel.model.behaviors.ReadyToValidateWhenAnswerIsSelected).toBeUndefined();
                    });
                });

                describe('with ImmediateValidation=true', () => {
                    beforeEach(() => {
                        editorViewModel.supportsNoIncorrectAnswers = {
                            ImmediateValidation: true,
                        };
                    });

                    it('should not turn off ImmediateValidation', () => {
                        editorViewModel.model.behaviors.ImmediateValidation = {};
                        editorViewModel.noIncorrectAnswers = true;
                        expect(editorViewModel.model.behaviors.ImmediateValidation).toEqual({});
                        editorViewModel.noIncorrectAnswers = false;
                        expect(editorViewModel.model.behaviors.ImmediateValidation).toEqual({});
                    });

                    it('should not convert all message actions to selected', () => {
                        const message = editorViewModel.addMessageFor(
                            editorViewModel.model.answerList.answers[0],
                            'validated',
                        ).model;
                        editorViewModel.noIncorrectAnswers = true;
                        expect(message.event).toBe('validated');
                        editorViewModel.noIncorrectAnswers = false;
                        expect(message.event).toBe('validated');
                    });

                    it('should not add ReadyToValidateWhenAnswerIsSelected behavior ', () => {
                        editorViewModel.noIncorrectAnswers = true;
                        expect(editorViewModel.model.behaviors.ReadyToValidateWhenAnswerIsSelected).toBeUndefined();
                        editorViewModel.noIncorrectAnswers = false;
                        expect(editorViewModel.model.behaviors.ReadyToValidateWhenAnswerIsSelected).toBeUndefined();
                    });
                });

                it('should remove ShowCorrectStyling behavior when on and revert when off', () => {
                    editorViewModel.model.behaviors.ShowCorrectStyling = {};
                    editorViewModel.noIncorrectAnswers = true;
                    expect(editorViewModel.model.behaviors.ShowCorrectStyling).toBeUndefined();
                    editorViewModel.noIncorrectAnswers = false;
                    expect(editorViewModel.model.behaviors.ShowCorrectStyling).toEqual({});
                });

                it('should remove all validations when on and revert when off', () => {
                    editorViewModel.noIncorrectAnswers = true;
                    expect(editorViewModel.model.validator.behaviors.HasAllExpectedAnswers).toBeUndefined();
                    expect(editorViewModel.model.validator.behaviors.HasNoUnexpectedAnswers).toBeUndefined();
                    editorViewModel.noIncorrectAnswers = false;
                    expect(editorViewModel.model.validator.behaviors.HasAllExpectedAnswers).toEqual({});
                    expect(editorViewModel.model.validator.behaviors.HasNoUnexpectedAnswers).toEqual({});
                });

                it('should remove all expectedAnswerMatchers from validator', () => {
                    editorViewModel.correctAnswer = editorViewModel.model.answerList.answers[0];
                    expect(editorViewModel.model.validator.expectedAnswerMatchers.length).not.toBe(0);
                    editorViewModel.noIncorrectAnswers = true;
                    expect(editorViewModel.model.validator.expectedAnswerMatchers.length).toBe(0);
                });

                it('should turn on disableCorrectAnswerSelect when on and revert when off', () => {
                    editorViewModel.noIncorrectAnswers = true;
                    expect(editorViewModel.config.disableCorrectAnswerSelect).toBe(true);
                    editorViewModel.noIncorrectAnswers = false;
                    expect(editorViewModel.config.disableCorrectAnswerSelect).toBe(false);

                    // ensure that the initial value is correct
                    editorViewModel.noIncorrectAnswers = true;
                    const newEditorViewModel = new editorViewModel.constructor(editorViewModel.model);
                    newEditorViewModel.supportsNoIncorrectAnswers = true;
                    expect(newEditorViewModel.config.disableCorrectAnswerSelect).toBe(true);
                });
            });

            it('should do initial setup when supportsNoIncorrectAnswers is set', () => {
                editorViewModel.noIncorrectAnswers = true;
                const newEditorViewModel = new editorViewModel.constructor(editorViewModel.model);
                newEditorViewModel.supportsNoIncorrectAnswers = true;
                expect(newEditorViewModel.config.disableCorrectAnswerSelect).toBe(true);
                expect(newEditorViewModel.messageEvent).toBe('selected');
            });

            describe('get', () => {
                beforeEach(() => {});

                it('should defer to model', () => {
                    editorViewModel.model.no_incorrect_answers = true;
                    expect(editorViewModel.noIncorrectAnswers).toBe(true);
                    editorViewModel.model.no_incorrect_answers = false;
                    expect(editorViewModel.noIncorrectAnswers).toBe(false);
                });
            });
        });
    });

    describe('cf-multiple-choice-challenge-editor', () => {
        let elem;
        let scope;
        let model;
        let editorViewModel;

        describe('with DisallowMultipleSelect', () => {
            beforeEach(() => {
                render();
                model.behaviors.DisallowMultipleSelect = {};
                scope.$digest();
            });

            it('should allow for selecting a correct answer', () => {
                expect(editorViewModel.correctAnswer).toBe(model.answers[0]);
                SpecHelper.updateSelect(elem, '[name="correct_answer"]', model.answers[1]);
                expect(editorViewModel.correctAnswer).toBe(model.answers[1]);
            });

            it('should show labels in dropdown for text answers', () => {
                const answer = frame.addSelectableAnswer();
                answer.text = frame.addText({
                    text: 'Answer text',
                });
                viewModel.model.answerList.answers = [answer];
                // set the correct answer so there will only be one option in dropdown
                editorViewModel.correctAnswer = answer;
                scope.$digest();
                SpecHelper.expectElementText(elem, '[name="correct_answer"] option', '1: Answer text');
            });

            it('should show labels in dropdown for image answers', () => {
                const answer = frame.addSelectableAnswer();
                answer.image = frame.addImage({
                    label: 'Answer label',
                });
                answer.text = undefined;
                viewModel.model.answerList.answers = [answer];
                // set the correct answer so there will only be one option in dropdown
                editorViewModel.correctAnswer = answer;
                scope.$digest();
                SpecHelper.expectElementText(elem, '[name="correct_answer"] option', '1: Answer label'); // account for indexing
            });
        });

        describe('with disableCorrectAnswerSelect', () => {
            it('should disable select', () => {
                render();
                editorViewModel.setConfig({
                    disableCorrectAnswerSelect: true,
                });
                scope.$digest();
                SpecHelper.expectElementDisabled(elem, '[name="correct_answers"] input');
            });
        });

        it('should show the editor for contentForImage', () => {
            render();
            model.contentForImage = frame.addVanillaComponent();
            scope.$digest();
            ComponentizedSpecHelper.assertEditorElementPresent(elem, model.contentForImage);
        });

        it('should support adding a message', () => {
            render();
            SpecHelper.click(elem, '[name="add_message"]', 0);
            SpecHelper.updateTextInput(elem, '.messages .input textarea', 'message');
            expect(viewModel.model.messages.length).toBe(1);
            const message = viewModel.model.messages[0];
            expect(message.messageText.text).toEqual('message');
            expect(message.event).toEqual('validated');
            expect(message.challenge).toEqual(viewModel.model);
        });

        it('should support deleting a message', () => {
            render();
            SpecHelper.click(elem, '[name="add_message"]', 0);
            expect(viewModel.model.messages.length).toBe(1);
            SpecHelper.click(elem, '[name="remove_message"]', 0);
            expect(viewModel.model.messages.length).toBe(0);
        });

        describe('with noIncorrectAnswers', () => {
            beforeEach(() => {
                render();
                editorViewModel.supportsNoIncorrectAnswers = true;
            });

            it('should show existing messages after switching', () => {
                render();
                SpecHelper.click(elem, '[name="add_message"]', 0);
                editorViewModel.noIncorrectAnswers = true;
                scope.$digest();
                SpecHelper.expectElement(elem, '.messages .input textarea');
            });

            it('should support adding a message', () => {
                editorViewModel.noIncorrectAnswers = true;
                render();
                SpecHelper.click(elem, '[name="add_message"]', 0);
                SpecHelper.updateTextInput(elem, '.messages .input textarea', 'message');
                expect(viewModel.model.messages.length).toBe(1);
                const message = viewModel.model.messages[0];
                expect(message.messageText.text).toEqual('message');
                expect(message.event).toEqual('selected');
                expect(message.challenge).toEqual(viewModel.model);
            });

            it('should support deleting a message', () => {
                editorViewModel.noIncorrectAnswers = true;
                render();
                SpecHelper.click(elem, '[name="add_message"]', 0);
                expect(viewModel.model.messages.length).toBe(1);
                SpecHelper.click(elem, '[name="remove_message"]', 0);
                expect(viewModel.model.messages.length).toBe(0);
            });
        });

        function render() {
            const renderer = SpecHelper.renderer();
            editorViewModel = renderer.scope.editorViewModel = frame.editorViewModelFor(viewModel.model);
            renderer.scope.frameViewModel = viewModel.frameViewModel;
            renderer.render(
                '<cf-component-editor editor-view-model="editorViewModel" frame-view-model="frameViewModel"></cf-component-editor>',
            );
            elem = renderer.elem;
            scope = elem.find('[editor-view-model]').isolateScope();
            model = viewModel.model;
        }
    });

    function getViewModel(options) {
        // In most of our tests, we do
        // getModel(options).createViewModel(frame.createFrameViewModel());
        // This is actually dangerous because you can end up creating
        // two viewModels for the same model, and they can confuse each
        // other in certain tests
        const model = getModel(options);
        const frameViewModel = model.frame().createFrameViewModel();
        viewModel = frameViewModel.viewModelFor(model);
        return viewModel;
    }

    function getModel(options) {
        let model;
        frame = Componentized.fixtures.getInstance();
        frame.addMultipleChoiceChallenge(options).addDefaultReferences();
        model = frame.getModelsByType('MultipleChoiceChallengeModel')[0];
        expect(model).not.toBeUndefined();
        return model;
    }
});
