import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/componentized_fixtures';
import 'Editor/angularModule';

describe('Componentized.Component.Challenge.ChallengeResponse', () => {
    let ChallengeResponse;
    let SpecHelper;
    let challengeResponse;
    let image;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                ChallengeResponse = $injector.get(
                    'Lesson.FrameList.Frame.Componentized.Component.Challenge.ChallengeResponse',
                );
                SpecHelper = $injector.get('SpecHelper');
                challengeResponse = new ChallengeResponse();

                const Componentized = $injector.get('Lesson.FrameList.Frame.Componentized');
                $injector.get('ComponentizedFixtures');
                const frame = Componentized.fixtures.getInstance();
                image = frame.addImage();
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('initialize', () => {
        it('should set text', () => {
            const challengeResponse = new ChallengeResponse({
                text: 'something',
            });
            expect(challengeResponse.text).toBe('something');
        });
        it('should set image', () => {
            const challengeResponse = new ChallengeResponse({
                image,
            });
            expect(challengeResponse.image).toBe(image);
        });
        it('should set x', () => {
            const challengeResponse = new ChallengeResponse({
                x: 42,
            });
            expect(challengeResponse.x).toBe(42);
        });
        it('should set y', () => {
            const challengeResponse = new ChallengeResponse({
                y: 42,
            });
            expect(challengeResponse.y).toBe(42);
        });
        it('should set answer', () => {
            const answer = {};
            const challengeResponse = new ChallengeResponse({
                answer,
            });
            expect(challengeResponse.answer).toBe(answer);
        });
    });

    describe('asJson', () => {
        it('should include text', () => {
            const challengeResponse = new ChallengeResponse({
                text: 'something',
            });
            expect(challengeResponse.asJson().text).toBe('something');
        });
        it('should include image', () => {
            const challengeResponse = new ChallengeResponse({
                image,
            });
            expect(challengeResponse.asJson().image_id).toBe(image.id);
        });
        it('should include x', () => {
            const challengeResponse = new ChallengeResponse({
                x: 42,
            });
            expect(challengeResponse.asJson().x).toBe(42);
        });
        it('should include y', () => {
            const challengeResponse = new ChallengeResponse({
                y: 42,
            });
            expect(challengeResponse.asJson().y).toBe(42);
        });
        it('should include answer', () => {
            const answer = {};
            const challengeResponse = new ChallengeResponse({
                answer,
            });
            expect(challengeResponse.asJson().answer_id).toBe(answer.id);
        });
    });

    describe('matchesAny', () => {
        it('should return true if any of the matchers match', () => {
            const matchers = [
                {
                    matches: jest.fn().mockReturnValue(false),
                },
                {
                    matches: jest.fn().mockReturnValue(true),
                },
                {
                    matches: jest.fn().mockReturnValue(false),
                },
            ];
            expect(challengeResponse.matchesAny(matchers)).toBe(true);
        });
        it('should return false if none of the matchers match', () => {
            const matchers = [
                {
                    matches: jest.fn().mockReturnValue(false),
                },
                {
                    matches: jest.fn().mockReturnValue(false),
                },
                {
                    matches: jest.fn().mockReturnValue(false),
                },
            ];
            expect(challengeResponse.matchesAny(matchers)).toBe(false);
        });
    });

    describe('text', () => {
        describe('set', () => {
            it('should work with a string', () => {
                challengeResponse.text = 'something';
                expect(challengeResponse.text).toBe('something');
            });
            it('should work with undefined', () => {
                challengeResponse.text = 'something';
                challengeResponse.text = undefined;
                expect(challengeResponse.text).toBeUndefined();
            });
            it('should throw with a non-string', () => {
                expect(() => {
                    challengeResponse.text = 1;
                }).toThrow(new Error('"text" must be a string.'));
            });
        });
    });

    describe('image', () => {
        describe('set', () => {
            it('should work with an image', () => {
                challengeResponse.image = image;
                expect(challengeResponse.image).toBe(image);
            });
            it('should work with undefined', () => {
                challengeResponse.image = image;
                challengeResponse.image = undefined;
                expect(challengeResponse.image).toBeUndefined();
            });
            it('should throw with a non-image', () => {
                expect(() => {
                    challengeResponse.image = 1;
                }).toThrow(new Error('"image" must be an instance of ImageModel.'));
            });
        });
    });
});
