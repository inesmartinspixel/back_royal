import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/componentized_fixtures';
import 'Editor/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import continueButtonLocales from 'Lessons/locales/lessons/lesson/frame_list/frame/componentized/component/continue_button-en.json';

setSpecLocales(continueButtonLocales);

describe('Componentized.Component.ContinueButton.AlwaysReadyContinueButton', () => {
    let frame;
    let frameViewModel;
    let viewModel;
    let Componentized;
    let SpecHelper;
    let SoundManager;
    let SoundConfig;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'FrontRoyal.Editor', 'SpecHelper', 'SoundManager');

        angular.mock.inject([
            '$injector',
            $injector => {
                Componentized = $injector.get('Lesson.FrameList.Frame.Componentized');
                SpecHelper = $injector.get('SpecHelper');
                $injector.get('ComponentizedFixtures');
                SoundManager = $injector.get('SoundManager');
                SoundConfig = $injector.get('SoundConfig');
                viewModel = getViewModel();
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('Model', () => {});

    describe('ViewModel', () => {});

    describe('cf-always-ready-button', () => {
        let elem;
        let button;

        it('should set continueButtonVisible to true', () => {
            expect(frameViewModel.continueButtonVisible).toBe(false);
            render();
            expect(frameViewModel.continueButtonVisible).toBe(true);
        });

        describe('in ready state', () => {
            beforeEach(() => {
                render();
                button = SpecHelper.expectElement(elem, '.cf-continue-button button');
                frameViewModel.playerViewModel = {
                    activeFrame: viewModel.frame,
                    gotoNext: jest.fn(),
                };
            });

            it('should render button', () => {
                SpecHelper.expectEqual('Continue', button.text().trim(), 'button text');
            });

            it('should move on when clicked', () => {
                jest.spyOn(frameViewModel, 'gotoNextFrame').mockImplementation(() => {});
                SpecHelper.expectElementEnabled(elem, '.cf-continue-button button');
                button.click();
                expect(frameViewModel.gotoNextFrame).toHaveBeenCalled();
            });

            it('should not move on when clicked and activeFrame is different', () => {
                frameViewModel.playerViewModel.activeFrame = {};
                SpecHelper.expectElementEnabled(elem, '.cf-continue-button button');
                button.click();
                expect(frameViewModel.playerViewModel.gotoNext).not.toHaveBeenCalled();
            });

            it('should play a sound when clicked', () => {
                jest.spyOn(SoundManager, 'playUrl').mockImplementation(() => {});
                button.click();
                expect(SoundManager.playUrl).toHaveBeenCalledWith(SoundConfig.DEFAULT_CLICK);
            });

            it('shoud have special buttons in practice mode', () => {
                Object.defineProperty(frameViewModel, 'isPractice', {
                    value: true,
                });
                render();
                SpecHelper.expectElementText(elem, '.practice-mode-continue button:eq(0)', 'Not Helpful');
                SpecHelper.expectElementText(elem, '.practice-mode-continue button:eq(1)', 'Helpful');
                jest.spyOn(viewModel, 'onPracticeModeContinueButtonClick').mockImplementation(() => {});
                SpecHelper.click(elem, 'button:eq(0)');
                expect(viewModel.onPracticeModeContinueButtonClick).toHaveBeenCalledWith(false);
                viewModel.onPracticeModeContinueButtonClick.mockClear();

                SpecHelper.click(elem, 'button:eq(1)');
                expect(viewModel.onPracticeModeContinueButtonClick).toHaveBeenCalledWith(true);
                viewModel.onPracticeModeContinueButtonClick.mockClear();
            });
        });

        function render() {
            const renderer = SpecHelper.renderer();
            renderer.scope.viewModel = viewModel;
            renderer.render('<cf-ui-component view-model="viewModel"></cf-ui-component>');
            elem = renderer.elem;
        }
    });

    function getViewModel(options) {
        const model = getModel(options);
        return frameViewModel.viewModelFor(model);
    }

    function getModel() {
        frame = Componentized.fixtures.getInstance();
        frameViewModel = frame.createFrameViewModel();
        return frame.addAlwaysReadyContinueButton().addDefaultReferences();
    }
});
