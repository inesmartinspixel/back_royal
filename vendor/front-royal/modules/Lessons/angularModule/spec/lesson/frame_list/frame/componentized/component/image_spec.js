import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/componentized_fixtures';
import 'Editor/angularModule';

describe('Componentized.Component.Image', () => {
    let frame;
    let frameViewModel;
    let viewModel;
    let Componentized;
    let SpecHelper;
    let guid;
    let $timeout;
    let EventLogger;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                Componentized = $injector.get('Lesson.FrameList.Frame.Componentized');
                SpecHelper = $injector.get('SpecHelper');
                $injector.get('ComponentizedFixtures');
                viewModel = getViewModel();
                guid = $injector.get('guid');
                $timeout = $injector.get('$timeout');
                EventLogger = $injector.get('EventLogger');
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('Model', () => {
        describe('urlForFormat', () => {
            it('should work', () => {
                const model = viewModel.model;
                model.image = {
                    formats: {
                        formatName: {
                            url: 'url',
                        },
                    },
                };
                expect(model.urlForFormat('formatName')).toEqual('url');
            });

            it('should use altered urls', () => {
                const model = viewModel.model;
                model.frame().useAlteredUrl('alteredUrl', 'alteredUrl?guid');

                model.image = {
                    formats: {
                        formatName: {
                            url: 'alteredUrl',
                        },
                    },
                };
                expect(model.urlForFormat('formatName')).toEqual('alteredUrl?guid');
            });
        });

        describe('urlForContext', () => {
            it('should return original format if format is gif', () => {
                const model = viewModel.model;
                const originalUrl = 'http://example.com/original.gif';

                model.image = {
                    formats: {
                        original: {
                            url: originalUrl,
                        },
                        tallContextImage: {
                            url: 'anotherURL',
                        },
                        wideAnswerButton: {
                            url: 'anotherURL',
                        },
                    },
                };
                expect(model.urlForContext('original')).toBe(originalUrl);
                expect(model.urlForContext('tallContextImage')).toBe(originalUrl);
                expect(model.urlForContext('wideAnswerButton')).toBe(originalUrl);
            });

            it('should return an existing known format if it exists', () => {
                const model = viewModel.model;
                jest.spyOn(model, 'urlForFormat').mockReturnValue('some url');
                expect(model.urlForContext('tallContextImage')).toBe('some url');
            });

            it('should throw if given an unkown context', () => {
                const model = viewModel.model;
                expect(() => {
                    model.urlForContext('unknown');
                }).toThrow(new Error('Unable to find format for unknown context "unknown"'));
            });

            it('should default to original if the format is known but is not populated', () => {
                const model = viewModel.model;
                model.image = {
                    formats: {
                        original: {
                            url: 'originalUrl',
                        },
                    },
                };
                expect(model.urlForContext('tallContextImage')).toBe('originalUrl');
            });

            describe('magnifications', () => {
                let model;

                beforeEach(() => {
                    model = viewModel.model;
                    model.image = {
                        formats: {
                            original: {
                                url: 'originalUrl',
                            },
                            '700x275': {
                                url: '1x.png',
                            },
                            '1400x550': {
                                url: '2x.png',
                            },
                            '2100x825': {
                                url: '3x.png',
                            },
                            '1020x400': {
                                url: 'mobile2x.png',
                            },
                        },
                    };
                });

                it('should use a 1x format if necessary', () => {
                    setScreen(1, false);
                    expect(model.urlForContext('tallContextImage')).toBe('1x.png');
                });

                it('should use a 2x format if necessary', () => {
                    setScreen(2, false);
                    expect(model.urlForContext('tallContextImage')).toBe('2x.png');
                });

                it('should use a 3x format if necessary', () => {
                    setScreen(3, false);
                    expect(model.urlForContext('tallContextImage')).toBe('3x.png');
                });

                it('should use a mobile format if necessary', () => {
                    setScreen(2, true);
                    expect(model.urlForContext('tallContextImage')).toBe('mobile2x.png');
                });

                function setScreen(pixelRatio, isMobile) {
                    jest.spyOn(model, '_devicePixelRatio').mockReturnValue(pixelRatio);
                    jest.spyOn(model, '_isMobile').mockReturnValue(isMobile);
                }
            });
        });

        describe('preload', () => {
            let success;
            let failure;
            beforeEach(() => {
                success = jest.fn();
                failure = jest.fn();
            });

            it('should preload images for a frame', () => {
                SpecHelper.stubWindowImage(createdImages => {
                    const model = viewModel.model;
                    jest.spyOn(model, 'urlForContext').mockReturnValue('url');

                    model.preload('some_context').then(success, failure);
                    expect(model.urlForContext).toHaveBeenCalledWith('some_context');
                    expect(createdImages.length).toBe(1);
                    expect(createdImages[0].src).toBe('url');

                    expect(success).not.toHaveBeenCalled();
                    expect(failure).not.toHaveBeenCalled();
                    createdImages[0].onload();
                    $timeout.flush();
                    expect(success).toHaveBeenCalled();
                    expect(failure).not.toHaveBeenCalled();
                });
            });

            it('should retry failed images and handle a subsequent success', () => {
                const image = failToLoadImageFirstTime();
                image.onload();
                $timeout.flush();
                expect(success).toHaveBeenCalled();
                expect(failure).not.toHaveBeenCalled();
                expect(image.onload).toBe(null);
                expect(image.onerror).toBe(null);
                expect(image.onabort).toBe(null);
            });

            it('should retry failed images forever with exponential backoff, logging an event after 3 fails', () => {
                jest.spyOn(EventLogger.prototype, 'log').mockImplementation(() => {});

                const image = failToLoadImageFirstTime();

                // calculate exponential delay
                const baseDelay = 500;
                const maxDelay = 5000;
                let delay = baseDelay * 2 ** 2;

                jest.spyOn(guid, 'generate').mockReturnValue('guid2');
                image.onerror();
                $timeout.flush(delay - 1);
                expect(image.src).toBe('url?g=guid');
                $timeout.flush(2);
                expect(image.src).toBe('url?g=guid2');
                expect(success).not.toHaveBeenCalled();
                expect(failure).not.toHaveBeenCalled(); // we never fail!
                expect(image.onload).not.toBe(null);
                expect(image.onerror).not.toBe(null);
                expect(image.onabort).not.toBe(null);
                expect(EventLogger.prototype.log).not.toHaveBeenCalled();

                delay = Math.min(maxDelay, baseDelay * 3 ** 2);

                jest.spyOn(guid, 'generate').mockReturnValue('guid3');
                image.onerror();
                $timeout.flush(delay - 1);
                expect(image.src).toBe('url?g=guid2');
                $timeout.flush(2);
                expect(image.src).toBe('url?g=guid3');
                expect(success).not.toHaveBeenCalled();
                expect(failure).not.toHaveBeenCalled(); // we never fail!
                expect(image.onload).not.toBe(null);
                expect(image.onerror).not.toBe(null);
                expect(image.onabort).not.toBe(null);
                expect(EventLogger.prototype.log).not.toHaveBeenCalled();

                delay = Math.min(maxDelay, baseDelay * 4 ** 2);

                jest.spyOn(guid, 'generate').mockReturnValue('guid4');
                image.onerror();
                $timeout.flush(delay - 1);
                expect(image.src).toBe('url?g=guid3');

                expect(EventLogger.prototype.log).toHaveBeenCalledWith(
                    'image:load_failure',
                    angular.extend(viewModel.model.frame().logInfo(), {
                        src: image.src,
                        label: viewModel.model.id,
                        image_component_id: viewModel.model.id,
                    }),
                );
                EventLogger.prototype.log.mockClear();

                $timeout.flush(2);
                expect(image.src).toBe('url?g=guid4');
                expect(success).not.toHaveBeenCalled();
                expect(failure).not.toHaveBeenCalled(); // we never fail!
                expect(image.onload).not.toBe(null);
                expect(image.onerror).not.toBe(null);
                expect(image.onabort).not.toBe(null);

                delay = Math.min(maxDelay, baseDelay * 5 ** 2);

                jest.spyOn(guid, 'generate').mockReturnValue('guid5');
                image.onerror();
                $timeout.flush(delay - 1);
                expect(image.src).toBe('url?g=guid4');
                $timeout.flush(2);
                expect(image.src).toBe('url?g=guid5');
                expect(success).not.toHaveBeenCalled();
                expect(failure).not.toHaveBeenCalled(); // we never fail!
                expect(image.onload).not.toBe(null);
                expect(image.onerror).not.toBe(null);
                expect(image.onabort).not.toBe(null);
                expect(EventLogger.prototype.log).not.toHaveBeenCalled();
            });

            function failToLoadImageFirstTime() {
                let image;
                SpecHelper.stubWindowImage(createdImages => {
                    const model = viewModel.model;
                    jest.spyOn(model.frame(), 'useAlteredUrl').mockImplementation(() => {});
                    jest.spyOn(model, 'urlForContext').mockReturnValue('url');
                    jest.spyOn(guid, 'generate').mockReturnValue('guid');

                    model.preload('some_context').then(success, failure);
                    createdImages[0].onerror();
                    $timeout.flush();
                    expect(success).not.toHaveBeenCalled();
                    expect(failure).not.toHaveBeenCalled();

                    expect(frame.useAlteredUrl).toHaveBeenCalledWith('url', 'url?g=guid');
                    expect(createdImages[0].src).toBe('url?g=guid');
                    image = createdImages[0];
                });

                return image;
            }
        });

        describe('hasImage', () => {
            it('should work', () => {
                const model = viewModel.model;
                expect(model.hasImage).toBe(true);
                delete model.image;
                expect(model.hasImage).toBe(false);
            });
        });

        describe('aspectRatio', () => {
            it('should be undefined with no dimensions', () => {
                const model = viewModel.model;
                model.image.dimensions = undefined;
                expect(model.aspectRatio).toBeUndefined();
            });
            it('should be defined with dimensions', () => {
                const model = viewModel.model;
                model.image.dimensions = {
                    width: 2,
                    height: 1,
                };
                expect(model.aspectRatio).toBe(2);
            });
        });

        describe('clone', () => {
            it('should copy $$dataUrl', () => {
                const model = viewModel.model;
                model.$$dataUrl = 'asdasdasd';
                const clone = model.clone();
                expect(clone.asJson()).toEqual(model.asJson());
                expect(clone.$$dataUrl).toEqual(model.$$dataUrl);
            });
        });

        describe('replaceDataUrlWithImage', () => {
            it('should work', () => {
                const model = viewModel.model;
                const newFrame = Componentized.fixtures.getInstance();
                model.frame().lesson().addFrame(newFrame);
                const otherImage = newFrame.addImage();
                model.$$dataUrl = 'asdasda';
                otherImage.$$dataUrl = 'asdasda';
                model.image = undefined;
                otherImage.image = undefined;

                model.replaceDataUrlWithImage({
                    a: 'b',
                });
                expect(model.$$dataUrl).toBeUndefined();
                expect(otherImage.$$dataUrl).toBeUndefined();

                expect(model.image).toEqual(otherImage.image);
                expect(model.image).not.toBe(otherImage.image);
            });
        });

        describe('allowUnreferenced', () => {
            it('should be true if and only if an image has been uploaded', () => {
                const model = viewModel.model;
                expect(model.image).not.toBeUndefined(); // sanity check
                expect(model.allowUnreferenced).toBe(true);
                model.image = undefined;
                expect(model.allowUnreferenced).toBe(false);
            });
        });
    });

    describe('ViewModel', () => {});

    describe('cf-image', () => {
        let elem;

        it('should render image', () => {
            render();
            const img = SpecHelper.expectElement(elem, 'img');
            SpecHelper.expectEqual(viewModel.model.image.formats.original.url, img.prop('src'));
        });

        it('should render nothing if there is no image', () => {
            delete viewModel.model.image;
            render();
            SpecHelper.expectNoElement(elem, 'img');
        });

        function render() {
            const renderer = SpecHelper.renderer();
            renderer.scope.viewModel = viewModel;
            renderer.render('<cf-ui-component view-model="viewModel"></cf-ui-component>');
            elem = renderer.elem;
        }
    });

    describe('cf-image-editor', () => {
        let elem;
        let scope;
        let helper;

        beforeEach(() => {
            helper = frame.editorViewModelFor(viewModel.model);
            render();
        });

        it('should have an editable label', () => {
            SpecHelper.updateTextInput(elem, '[name="label"]', 'new label');
            SpecHelper.expectEqual('new label', helper.model.label, 'label');
        });

        it('should hide label if options.hideLabelInput', () => {
            render({
                hideLabelInput: true,
            });
            SpecHelper.expectNoElement(elem, '[name="label"]');
        });

        it('should support uploading an image', () => {
            const uploader = SpecHelper.startUploadWithContentItemImageUpload(elem, '.content-item-image-upload');
            expect(viewModel.model.lesson.blockSaving).toBe(true);
            expect(viewModel.model.$$dataUrl).toBe('dataUrl');

            const s3Asset = uploader.getResponseFromServer();
            expect(viewModel.model.image).toEqual(s3Asset);
            expect(viewModel.model.lesson.blockSaving).toBe(false);
        });

        it('should reset image if upload fails', () => {
            const origImage = viewModel.model.image;
            expect(origImage).not.toBeUndefined();

            const uploader = SpecHelper.startUploadWithContentItemImageUpload(elem, '.content-item-image-upload');
            expect(viewModel.model.lesson.blockSaving).toBe(true);
            expect(viewModel.model.$$dataUrl).toBe('dataUrl');

            uploader.getErrorFromServer();
            expect(viewModel.model.image).toBe(origImage);
            expect(viewModel.model.lesson.blockSaving).toBe(false);
        });

        it('should show a thumbnail', () => {
            SpecHelper.expectElement(elem, 'img.cf-image');
        });

        it('should be possible to disable', () => {
            SpecHelper.expectElementEnabled(elem, 'input');
            SpecHelper.expectElement(elem, '[name="hide-if-disabled"]');
            scope.disabled = true;
            scope.$digest();
            SpecHelper.expectElementDisabled(elem, 'input');
            SpecHelper.expectNoElement(elem, '[name="hide-if-disabled"]');
        });

        it('should replace filename if upload succeeds', () => {
            SpecHelper.updateTextInput(elem, '[name="label"]', 'origImage');

            const uploader = SpecHelper.startUploadWithContentItemImageUpload(elem, '.content-item-image-upload');
            uploader.getResponseFromServer('newImage');

            SpecHelper.expectEqual('newImage', helper.model.label, 'label');
        });

        function render(options) {
            const renderer = SpecHelper.renderer();
            renderer.scope.editorViewModel = helper;
            renderer.scope.options = options || {};
            renderer.render(
                '<cf-component-editor editor-view-model="editorViewModel" options="options"></cf-component-editor>',
            );
            elem = renderer.elem;
            scope = elem.find('[editor-view-model]').isolateScope();
        }
    });

    function getViewModel(options) {
        const model = getModel(options);
        return frameViewModel.viewModelFor(model);
    }

    function getModel() {
        frame = Componentized.fixtures.getInstance();
        frameViewModel = frame.createFrameViewModel();
        return frame.addImage();
    }
});
