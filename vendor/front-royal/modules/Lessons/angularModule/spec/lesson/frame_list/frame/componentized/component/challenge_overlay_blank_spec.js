import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/componentized_fixtures';
import 'Editor/angularModule';

describe('Componentized.Component.ChallengeOverlayBlank', () => {
    let frame;
    let frameViewModel;
    let model;
    let viewModel;
    let Componentized;
    let SpecHelper;
    let MaxTextLengthConfig;
    let $rootScope;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                Componentized = $injector.get('Lesson.FrameList.Frame.Componentized');
                SpecHelper = $injector.get('SpecHelper');
                $injector.get('ComponentizedFixtures');
                viewModel = getViewModel();
                MaxTextLengthConfig = $injector.get('MaxTextLengthConfig');
                $rootScope = $injector.get('$rootScope');
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('Model', () => {});
    describe('ViewModel', () => {
        it('should set the overlayZIndex', () => {
            viewModel.challengeViewModel.index = 42;
            expect(viewModel.overlayZIndex).toBe(-42);
        });
    });

    describe('cf-challenge-overlay-blank', () => {
        let elem;
        let scope;

        beforeEach(() => {
            render();
        });

        it('should not render text if incomplete', () => {
            model.text = frame.addText();
            scope.$digest();
            SpecHelper.expectNoElement(elem, `.text [component-id="${model.text.id}"]`);
        });

        it('should render text if complete', () => {
            model.text = frame.addText();
            viewModel.challengeViewModel.complete = true;
            scope.$digest();
            SpecHelper.expectElement(elem, `.text [component-id="${model.text.id}"]`);
        });

        it('should render text if editorMode', () => {
            model.text = frame.addText();
            frameViewModel.playerViewModel = {
                editorMode: true,
            };
            scope.$digest();
            SpecHelper.expectElement(elem, `.text [component-id="${model.text.id}"]`);
        });

        it('should render image', () => {
            model.image = frame.addImage();
            scope.$digest();
            SpecHelper.expectElement(elem, `.image_blank [component-id="${model.image.id}"]`);
        });

        it('should add editable class in editorMode', () => {
            SpecHelper.expectNoElement(elem, '.editable');
            frameViewModel.playerViewModel = {
                editorMode: true,
            };
            scope.$digest();
            SpecHelper.expectElement(elem, '.editable');
        });

        it('should set the activatable class', () => {
            const blank = elem.find('.cf-challenge-overlay-blank');
            viewModel.challengeViewModel.active = false;
            SpecHelper.expectElementHasClass(blank, 'activatable');

            // if it's active, the class should be removed
            viewModel.challengeViewModel.active = true;
            scope.$digest();
            SpecHelper.expectElementDoesNotHaveClass(blank, 'activatable');

            // if it's complete, the class should be removed
            viewModel.challengeViewModel.active = false;
            viewModel.challengeViewModel.complete = true;
            scope.$digest();
            SpecHelper.expectElementDoesNotHaveClass(blank, 'activatable');
        });

        it('should add a click-target to multiple choice challenges', () => {
            Object.defineProperty(viewModel.challengeViewModel, 'type', {
                value: 'MultipleChoiceChallengeViewModel',
            });
            scope.$digest();
            SpecHelper.expectElement(elem, '.click-target');
        });

        it('should not add a click-target to non-multiple choice challenges', () => {
            Object.defineProperty(viewModel.challengeViewModel, 'type', {
                value: 'NotAMultipleChoiceChallengeViewModel',
            });
            scope.$digest();
            SpecHelper.expectNoElement(elem, '.click-target');
        });

        describe('with UserInputChallenge', () => {
            beforeEach(() => {
                model.addUserInputChallenge();
                render();
            });
        });

        function render() {
            const renderer = SpecHelper.renderer();
            renderer.scope.viewModel = viewModel;
            renderer.render('<cf-ui-component view-model="viewModel"></cf-ui-component>');
            elem = renderer.elem;
            scope = renderer.scope;
        }
    });

    describe('EditorViewModel', () => {
        beforeEach(() => {
            model.editorViewModel;
            $rootScope.$digest();
        });

        it('should remove blank when challenge is removed', () => {
            jest.spyOn(model, 'remove').mockImplementation(() => {});
            model.challenge.remove();
            expect(model.remove).toHaveBeenCalled();
        });

        it('should set showFontStyleEditor to true', () => {
            expect(model.text.editorViewModel.config.showFontStyleEditor).toBe(true);
        });

        it('should set max text length', () => {
            expect(model.text.editorViewModel.config.maxRecommendedTextLength).toBe(MaxTextLengthConfig.BLANK);
        });

        describe('with MultipleChoiceChallenge', () => {
            beforeEach(() => {
                // recreate the model so that editorViewModel will not be initialized yet
                model = getModel();
                model.challenge = frame.addMultipleChoiceChallenge().addDefaultReferences();
                model.challenge.editorViewModel.correctAnswer = model.challenge.answers[0];
                model.editorViewModel;
                $rootScope.$digest();
            });

            it('should set the relatedChallengeOverlayBlank on the correctAnswer', () => {
                const origCorrectAnswer = model.challenge.editorViewModel.correctAnswer;
                expect(origCorrectAnswer.editorViewModel.relatedBlankChallenge).toBe(model.challenge);
                expect(origCorrectAnswer.editorViewModel.relatedBlankLabel).toBe(model.text.text);

                model.text.text = 'changed';
                expect(origCorrectAnswer.editorViewModel.relatedBlankChallenge).toBe(model.challenge);
                expect(origCorrectAnswer.editorViewModel.relatedBlankLabel).toBe(model.text.text);
            });

            it('should update the content on the correct answer when it is changed on the blank', () => {
                const correctAnswer = model.challenge.editorViewModel.correctAnswer;
                expect(correctAnswer.text.text).toEqual(model.text.text);
                model.text.text = 'ok';
                expect(correctAnswer.text.text).toEqual(model.text.text);
                model.image = frame.addImage();
                expect(correctAnswer.text).toBeUndefined();
                expect(correctAnswer.image).toBe(model.image);
            });

            it('should not update the content on the correct answer if unlink_blank_from_answer=true', () => {
                model.challenge.unlink_blank_from_answer = true;
                const correctAnswer = model.challenge.editorViewModel.correctAnswer;
                const originalValue = correctAnswer.text.text;
                model.text.text = 'ok';
                expect(correctAnswer.text.text).not.toEqual(model.text.text);
                expect(correctAnswer.text.text).toEqual(originalValue);
            });
        });

        describe('with UserInputChallenge', () => {
            beforeEach(() => {
                // recreate the model so that editorViewModel will not be initialized yet
                model = getModel();
                model.challenge = frame.addUserInputChallenge().addDefaultReferences();
                model.editorViewModel;
                $rootScope.$digest();
            });

            it('should update the content on the correct answer when it is changed on the blank', () => {
                expect(model.challenge.editorViewModel.correctAnswerText).toEqual(model.text.text);
                model.text.text = 'ok';
                expect(model.challenge.editorViewModel.correctAnswerText).toEqual(model.text.text);
            });

            it('should not update the content on the correct answer if unlink_blank_from_answer=true', () => {
                model.challenge.unlink_blank_from_answer = true;
                const originalValue = model.challenge.editorViewModel.correctAnswerText;
                model.text.text = 'ok';
                expect(model.challenge.editorViewModel.correctAnswerText).not.toEqual(model.text.text);
                expect(model.challenge.editorViewModel.correctAnswerText).toEqual(originalValue);
            });
        });
    });
    describe('cf-image-editor', () => {});

    function getViewModel(options) {
        model = getModel(options);
        return frameViewModel.viewModelFor(model);
    }

    function getModel(options) {
        frame = Componentized.fixtures.getInstance();
        frameViewModel = frame.createFrameViewModel();
        return frame.addChallengeOverlayBlank(options).addDefaultReferences();
    }
});
