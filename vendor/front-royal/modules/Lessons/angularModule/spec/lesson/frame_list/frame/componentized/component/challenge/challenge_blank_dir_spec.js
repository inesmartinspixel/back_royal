import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/componentized_fixtures';
import 'Editor/angularModule';

describe('cf-challenge-blank', () => {
    let frame;
    let frameViewModel;
    let viewModel;
    let model;
    let elem;
    let mathjaxWrapper;
    let scope;
    let $timeout;
    let $interval;
    let Componentized;
    let SpecHelper;
    let PlayerViewModel;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                Componentized = $injector.get('Lesson.FrameList.Frame.Componentized');
                SpecHelper = $injector.get('SpecHelper');
                $injector.get('ComponentizedFixtures');
                $timeout = $injector.get('$timeout');
                $interval = $injector.get('$interval');
                PlayerViewModel = $injector.get('Lesson.FrameList.FrameListPlayerViewModel');
                frame = Componentized.fixtures.getInstance();
                getMultipleChoiceChallengeViewModel();
                render();
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    it('should add completed class when challenge is completed', () => {
        let blank = SpecHelper.expectElement(elem, '.blank');
        expect(blank.get(0).classList.contains('completed')).toBe(false);
        viewModel.complete = true;
        scope.$digest();
        blank = SpecHelper.expectElement(elem, '.blank');
        expect(blank.get(0).classList.contains('completed')).toBe(true);
    });

    it('should add pending class when challenge is not completed', () => {
        let blank = SpecHelper.expectElement(elem, '.blank');
        expect(blank.get(0).classList.contains('pending')).toBe(true);
        viewModel.complete = true;
        scope.$digest();
        blank = SpecHelper.expectElement(elem, '.blank');
        expect(blank.get(0).classList.contains('pending')).toBe(false);
    });

    it('should add selected class when challenge is active', () => {
        let blank = SpecHelper.expectElement(elem, '.blank');
        expect(blank.get(0).classList.contains('selected')).toBe(false);
        viewModel.active = true;
        scope.$digest();
        blank = SpecHelper.expectElement(elem, '.blank');
        expect(blank.get(0).classList.contains('selected')).toBe(true);
    });

    it('should disable when challenge is complete', () => {
        SpecHelper.expectElementEnabled(elem, '.blank');
        viewModel.complete = true;
        scope.$digest();
        SpecHelper.expectElementDisabled(elem, '.blank');
    });

    describe('inline', () => {
        describe('on', () => {
            beforeEach(() => {
                getUserInputChallengeViewModel();
                renderInline();
            });

            it('should use an input tag', () => {
                SpecHelper.expectElement(elem, 'input');
                SpecHelper.expectNoElement(elem, 'textarea');
            });
            it('should add inline class', () => {
                SpecHelper.expectHasClass(elem, '.cf-challenge-blank', 'inline');
            });
        });

        describe('off', () => {
            beforeEach(() => {
                getUserInputChallengeViewModel();
                render();
            });
            it('should use a textarea tag', () => {
                SpecHelper.expectElement(elem, 'textarea');
                SpecHelper.expectNoElement(elem, 'input');
            });
            it('should not add inline class', () => {
                SpecHelper.expectElementDoesNotHaveClass(elem, 'inline');
            });
        });
    });

    describe('clickTarget', () => {
        it('should activate a challenge on click', () => {
            viewModel.active = false;
            SpecHelper.click(elem, '.click-target');
            expect(viewModel.active).toBe(true);
        });
        it('should not activate a challenge on click if completed', () => {
            viewModel.active = false;
            viewModel.complete = true;
            SpecHelper.click(elem, '.click-target');
            expect(viewModel.active).toBe(false);
        });
        it('should disappear when challenge is finished', () => {
            viewModel.complete = false;
            scope.$digest();
            SpecHelper.expectDoesNotHaveClass(elem, '.click-target', 'ng-hide');
            viewModel.complete = true;
            scope.$digest();
            SpecHelper.expectHasClass(elem, '.click-target', 'ng-hide');
        });
    });

    describe('with MultipleChoiceChallenge', () => {
        it('should activate challenge when clicked', () => {
            const blank = SpecHelper.expectElement(elem, '.blank');
            expect(viewModel.active).toBe(false);
            blank.click();
            expect(viewModel.active).toBe(true);
        });

        it('should render', () => {
            getMultipleChoiceChallengeViewModel();
            render();
            SpecHelper.expectElementText(elem, '.blank', ''); // ensure cheaters can't see the answer by searching the page or using screen readers
        });
    });

    describe('with UserInputChallenge', () => {
        let input;

        beforeEach(() => {
            getUserInputChallengeViewModel();
            render();
            $timeout.flush();
            input = SpecHelper.expectElement(elem, 'textarea');
        });

        it('should render contents on complete', () => {
            viewModel.complete = true;
            render();
            SpecHelper.expectElementText(elem, '.blank [name="content"]', 'contents');
        });

        it('should focus if the challenge is active on component_overlay:click_outside_any_overlay', () => {
            jest.spyOn(scope, 'setFocus').mockImplementation(() => {});
            viewModel.active = true;
            scope.$broadcast('component_overlay:click_outside_any_overlay');
            expect(scope.setFocus).toHaveBeenCalledWith(true);
            viewModel.active = false;
            scope.setFocus.mockClear();
            expect(scope.setFocus).not.toHaveBeenCalled();
            scope.$broadcast('component_overlay:click_outside_any_overlay');
            expect(scope.setFocus).not.toHaveBeenCalled();
        });

        it('should validate whenever the answer changes, but only digest when necessary', () => {
            expect(viewModel.model.correctAnswerText).toBe('some text'); // sanity check
            jest.spyOn(viewModel, 'validate');
            jest.spyOn(scope, '$digest');

            // changing from nothing to correct. must digest
            updateTextInputAndAssertDigest('s', true);

            // no change. still correct. no digest
            updateTextInputAndAssertDigest('so', false);

            // changing from correct to incorrect. must digest
            updateTextInputAndAssertDigest('sox', true);

            // no change. still incorrect. no digest
            updateTextInputAndAssertDigest('soxx', false);

            // correct again. digest
            updateTextInputAndAssertDigest('so', true);

            // complete. digest
            updateTextInputAndAssertDigest('some text', true);
        });

        it('should update the text input if validation changes the userAnswer', () => {
            const origValidate = viewModel.validate;
            viewModel.validate = function () {
                const val = origValidate.apply(this);
                viewModel.userAnswer = input.val().toUpperCase();
                return val;
            };
            jest.spyOn(viewModel, 'validate');
            jest.spyOn(scope, '$digest');

            // should work if there is a digest
            updateTextInputAndAssertDigest('x', true);
            expect(input.val()).toBe('X');

            // abd if there is no digest
            updateTextInputAndAssertDigest('Xx', false);
            expect(input.val()).toBe('XX');
        });

        // Couldn't find a good way to test that the cancelNewLine function is being called on a keydown
        // event on the element with specific key and keyCode properties, so two tests were made.
        // The first test is making sure that it's being called on a keydown event. The second test
        // makes the assumption that the function was called successfully on a keydown event
        // and has the event object mocked out with the appropriate key and keyCode properties.
        it('should call cancelNewLine on keydown event', () => {
            jest.spyOn(scope, 'cancelNewLine');
            input.keydown();
            expect(scope.cancelNewLine).toHaveBeenCalled();
        });

        it('should not cancel new line when Shift and Enter are pressed', () => {
            // using generalized Event constuctor since the KeyboardEvent constructor isn't supported
            const e = new Event('keydown');
            e.shiftKey = true;
            e.which = 13;
            expect(scope.cancelNewLine(e)).toBe(false);
        });

        it("should cancel new line only when Enter key is pressed and the key property is equal to 'Enter'", () => {
            const e = new Event('keydown');
            e.shiftKey = false;
            e.which = 13;
            expect(scope.cancelNewLine(e)).toBe(true);
        });

        // SpecHelper.updateTextInput does not work here because it assumes scope applies
        // that are not happening
        function updateTextInput(val) {
            input.val(val);
            input.keyup();
        }

        function updateTextInputAndAssertDigest(val, digested) {
            const validateCallCount = viewModel.validate.mock.calls.length;
            const digestCallCount = scope.$digest.mock.calls.length;
            updateTextInput(val);
            SpecHelper.expectEqual(
                validateCallCount + 1,
                viewModel.validate.mock.calls.length,
                'viewModel.validate.mock.calls.length',
            );
            SpecHelper.expectEqual(
                digestCallCount + (digested ? 1 : 0),
                scope.$digest.mock.calls.length,
                'scope.$digest.mock.calls.length',
            );
        }

        // // this works in a browser.  Don't know why the test fails, but it
        // // fails in Phantom and Chrome
        // it('should activate challenge when focused', function() {
        //     var input = SpecHelper.expectElement(elem, '.blank input');
        //     expect(viewModel.active).toBe(false);
        //     input.focus();
        //     expect(viewModel.active).toBe(true);
        // });

        // For some reason, checking focus directly on the input elements
        // did not seem to work here. I guess because we're not in a browser?

        describe('input focusing', () => {
            [render, renderInline].forEach(renderFunction => {
                const inputType = renderFunction === render ? 'textarea' : 'input';

                it('should set initial focus if challenge is active', () => {
                    viewModel.active = false;

                    renderFunction();
                    $('body').append(elem); // spechelper will cleanup

                    SpecHelper.expectElement(elem, inputType);

                    jest.spyOn(scope, 'setFocus');
                    $timeout.flush();
                    expect(scope.setFocus).not.toHaveBeenCalled();

                    viewModel.active = true;
                    // do not add a $timeout.flush() here.  If you need one, then
                    // you broke mobile safari and the keyboard will disappear when
                    // switching challenges
                    expect(scope.setFocus).toHaveBeenCalledWith(true);

                    // this assertion started failing randomly at some point in
                    // very odd ways.  Seems like the assertiong on scope.setFocus
                    // is good enough, so commenting it out.
                    // expect(document.activeElement).toEqual(input.get(0));
                });

                it('should set focus on activated', () => {
                    viewModel.active = true;

                    renderFunction();
                    $('body').append(elem); // spechelper will cleanup

                    jest.spyOn(scope, 'setFocus');
                    const input = SpecHelper.expectElement(elem, inputType);

                    $timeout.flush();
                    expect(scope.setFocus).not.toHaveBeenCalled();
                    scope.$broadcast('frame_visible');
                    expect(scope.setFocus).toHaveBeenCalledWith(true);
                    expect(document.activeElement).toEqual(input.get(0));
                    scope.setFocus.mockClear();

                    viewModel.active = false;
                    $(document.activeElement).blur();
                    expect(document.activeElement).not.toEqual(input.get(0));
                    viewModel.active = true;
                    // do not add a $timeout.flush() here.  If you need one, then
                    // you broke mobile safari and the keyboard will disappear when
                    // switching challenges
                    expect(scope.setFocus).toHaveBeenCalledWith(true);

                    // this assertion started failing randomly at some point in
                    // very odd ways.  Seems like the assertiong on scope.setFocus
                    // is good enough, so commenting it out.
                    // expect(document.activeElement).toEqual(input.get(0));
                });
            });
        });

        describe('with withinMathjax attr', () => {
            it('should set the width of the input field', () => {
                viewModel.model.lesson.locale = 'en';
                viewModel.model.editorViewModel.correctAnswerText = '12.2';
                renderWithinMathjax();
                const width = 0.59 * 3 + 0.24 * 1;
                expect(elem.find('input').css('width')).toBe(`${width}em`);
            });

            it('should set the width of the input field for a language that supports IME', () => {
                viewModel.model.lesson.locale = 'zh';
                viewModel.model.editorViewModel.correctAnswerText = '12.2中文';
                renderWithinMathjax();
                const width = Number(0.59 * 3 + 0.24 * 1 + 4 * 0.59 * 2).toFixed(16);
                expect(elem.find('input').css('width')).toBe(`${width}em`);
            });
        });

        describe('without withinMathjax attr', () => {
            it('should set the size of the input field', () => {
                viewModel.model.lesson.locale = 'en';
                renderInline();
                scope.$digest();
                expect(elem.find('input').attr('size')).toBe(String(viewModel.correctAnswerText.length));
            });

            it('should set the size of the input field for a language that supports IME', () => {
                viewModel.model.lesson.locale = 'zh';
                viewModel.model.editorViewModel.correctAnswerText = '对了abc';
                renderInline();
                scope.$digest();
                // 4 spaces for each fo the two chinese characters and one for each English character
                expect(elem.find('input').attr('size')).toBe(String(4 * 2 + 3));
            });
        });

        describe('hinting support', () => {
            const stubEvent = {
                preventDefault() {},
                stopImmediatePropagation() {},
                view: window,
            };

            it('should have the viewModel increment hints once when missing correct characters', () => {
                viewModel.active = true;
                expect(viewModel.userAnswer).toBeUndefined(); // sanity check
                jest.spyOn(viewModel, 'incrementHint').mockImplementation(() => {});
                viewModel.frameViewModel.giveHint(stubEvent);
                expect(viewModel.incrementHint).toHaveBeenCalled();
                expect(viewModel.incrementHint.mock.calls.length).toEqual(1);
            });

            it('should only digest when necessary', () => {
                viewModel.active = true;
                expect(viewModel.userAnswer).toBeUndefined(); // sanity check
                jest.spyOn(scope, '$digest');

                // first click should digest, since we're switching
                // from no answer to correct
                viewModel.frameViewModel.giveHint(stubEvent);
                expect(scope.$digest.mock.calls.length).toBe(1);

                // second click does not need a digest, since it's
                // still correct
                viewModel.frameViewModel.giveHint(stubEvent);
                expect(scope.$digest.mock.calls.length).toBe(1);
            });

            it('should have the viewModel increment hints X+1 times for X number of wrong trailing characters', () => {
                SpecHelper.stubEventLogging();
                viewModel.active = true;
                viewModel.userAnswer = 'incorrect';
                viewModel.showingIncorrectStyling = true;
                const expectedCallCount = viewModel.userAnswer.length + 1;
                jest.spyOn(viewModel, 'incrementHint').mockImplementation(() => {});
                viewModel.frameViewModel.giveHint(stubEvent);
                $interval.flush(125 * expectedCallCount); // TODO: better way to test intervals???
                expect(viewModel.incrementHint).toHaveBeenCalled();
                expect(viewModel.incrementHint.mock.calls.length).toEqual(expectedCallCount);
            });
        });
    });

    describe('with withinMathjax attr', () => {
        it('should remove the clip class from a parent', () => {
            renderWithinMathjax();
            $timeout.flush();
            const clippedContainer = SpecHelper.expectElement(mathjaxWrapper, '[clipped-container]');
            expect(clippedContainer.css('clip')).toBe('');
        });
    });

    function getUserInputChallengeViewModel(options) {
        getUserInputChallengeModel(options);
        viewModel = frameViewModel.viewModelFor(model);
        frameViewModel.mainUiComponentViewModel.currentChallengeViewModel = viewModel;
        return viewModel;
    }

    function getUserInputChallengeModel(options) {
        frameViewModel = frame.createFrameViewModel();
        frameViewModel.playerViewModel = new PlayerViewModel(frame.lesson());
        model = frame.addUserInputChallenge(options).addDefaultReferences();
        return model;
    }

    function getMultipleChoiceChallengeViewModel(options) {
        getMultipleChoiceChallengeModel(options);
        viewModel = frameViewModel.viewModelFor(model);
        return viewModel;
    }

    function getMultipleChoiceChallengeModel(options) {
        frameViewModel = frame.createFrameViewModel();
        model = frame.addMultipleChoiceChallenge(options).addDefaultReferences();
        return model;
    }

    function render() {
        const renderer = SpecHelper.renderer();
        renderer.scope.viewModel = viewModel;
        renderer.render('<cf-challenge-blank view-model="viewModel">contents</cf-component-editor>');
        elem = renderer.elem;
        scope = renderer.childScope;
    }

    function renderInline() {
        const renderer = SpecHelper.renderer();
        renderer.scope.viewModel = viewModel;
        renderer.render('<cf-challenge-blank inline="true" view-model="viewModel">contents</cf-component-editor>');
        elem = renderer.elem;
        scope = renderer.childScope;
    }

    function renderWithinMathjax() {
        // copied some mathjax generated html from a browser, added blank-container attribute
        // to the element that contains a blank
        /* eslint-disable */
        const mathjaxHtml =
            '\
            <span class="MathJax" id="MathJax-Element-11-Frame" role="textbox" aria-readonly="true">\
               <nobr>\
                  <span class="math" id="MathJax-Span-74" style="width: 6.381em; display: inline-block;">\
                     <span style="display: inline-block; position: relative; width: 5.679em; height: 0px; font-size: 112%;">\
                        <span clipped-container style="position: absolute; clip: rect(1.661em 1000.003em 2.937em -0.507em); top: -2.548em; left: 0.003em;">\
                           <span blank-container class="mrow" id="MathJax-Span-75">\
                           </span>\
                           <span style="display: inline-block; width: 0px; height: 2.554em;"></span>\
                        </span>\
                     </span>\
                     <span style="border-left-width: 0.004em; border-left-style: solid; display: inline-block; overflow: hidden; width: 0px; height: 1.146em; vertical-align: -0.282em;"></span>\
                  </span>\
               </nobr>\
            </span>\
        ';
        mathjaxWrapper = $(mathjaxHtml);
        /* eslint-enable */
        const blankContainer = mathjaxWrapper.find('[blank-container]');
        const renderer = SpecHelper.renderer();
        renderer.scope.viewModel = viewModel;
        renderer.render(
            '<cf-challenge-blank within-mathjax view-model="viewModel">contents</cf-component-editor>',
            blankContainer,
        );
        elem = renderer.elem;
        scope = renderer.childScope;
    }
});
