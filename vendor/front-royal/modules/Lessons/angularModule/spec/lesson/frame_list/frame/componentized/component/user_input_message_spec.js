import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/componentized_fixtures';
import 'Editor/angularModule';

describe('Componentized.Component.UserInputMessage', () => {
    let frame;
    let frameViewModel;
    let model;
    let viewModel;
    let Componentized;
    let SpecHelper;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                Componentized = $injector.get('Lesson.FrameList.Frame.Componentized');
                SpecHelper = $injector.get('SpecHelper');
                $injector.get('ComponentizedFixtures');
                viewModel = getViewModel();
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('Model', () => {});

    describe('ViewModel', () => {
        it('should show a message on validated correct', () => {
            viewModel = getViewModel({});
            const addMessage = jest.fn();
            viewModel.challengeViewModel.fire('validatedCorrect', {
                addMessage,
            });
            expectShowMessageCalled(model.messageText);
            expect(addMessage).toHaveBeenCalledWith(model.messageText);
        });

        function expectShowMessageCalled(messageTextModel) {
            expect(frameViewModel.playerViewModel.showMessage).toHaveBeenCalled();
            const arg = frameViewModel.playerViewModel.showMessage.mock.calls[0][0];
            expect(arg.scope.textViewModel.model).toBe(messageTextModel);
            expect(arg.content).toBe('<cf-text view-model="textViewModel"></cf-text>');
            expect(arg.correctExists).toBe(true);
            expect(arg.correctExists).toBe(true);
        }
    });

    function getViewModel(options) {
        model = getModel(options);
        return frameViewModel.viewModelFor(model);
    }

    function getModel(options) {
        frame = Componentized.fixtures.getInstance();
        frameViewModel = frame.createFrameViewModel();
        frameViewModel.playerViewModel = {
            showMessage: jest.fn(),
            clearMessage: jest.fn(),
            logInfo: jest.fn(),
            log: jest.fn(),
        };
        return frame.addUserInputMessage(options).addDefaultReferences();
    }
});
