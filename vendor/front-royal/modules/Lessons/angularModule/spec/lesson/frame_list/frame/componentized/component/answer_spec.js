import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/componentized_fixtures';
import 'Editor/angularModule';

describe('Componentized.Component.Answer', () => {
    let frame;
    let answerViewModel;
    let Componentized;
    let SpecHelper;
    let SoundManager;
    let SoundConfig;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                Componentized = $injector.get('Lesson.FrameList.Frame.Componentized');
                SpecHelper = $injector.get('SpecHelper');
                $injector.get('ComponentizedFixtures');
                SoundManager = $injector.get('SoundManager');
                SoundConfig = $injector.get('SoundConfig');
                answerViewModel = getAnswerViewModel();
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('ViewModel', () => {
        describe('selected', () => {
            it('should throw an error on get', () => {
                expect(() => {
                    answerViewModel.selected;
                }).toThrow(new Error('AnswerViewModel does not support the "selected" property.'));
            });

            it('should throw an error on set', () => {
                expect(() => {
                    answerViewModel.selected = true;
                }).toThrow(new Error('AnswerViewModel does not support the "selected" property.'));
            });
        });

        describe('showingIncorrectStyling', () => {
            it('should be true iff answerViewModel has the incorrect class', () => {
                SpecHelper.expectEqual(
                    false,
                    answerViewModel.showingIncorrectStyling,
                    'showingIncorrectStyling without class',
                );
                answerViewModel.cssClasses.push('incorrect');
                SpecHelper.expectEqual(
                    true,
                    answerViewModel.showingIncorrectStyling,
                    'showingIncorrectStyling with class',
                );
            });
        });

        describe('cssClasses', () => {
            it('should have some defaults', () => {
                assertDefaultClasses();
            });

            it('should support adding and removing correct', () => {
                assertDoesNotHaveClass('correct');
                answerViewModel.addCorrectStyling();
                assertHasClass('correct');
                assertDoesNotHaveClass('unvalidated');
                answerViewModel.removeCorrectStyling();
                assertDoesNotHaveClass('correct');
                assertHasClass('unvalidated');
            });

            it('should support playing sounds during correct styling', () => {
                jest.spyOn(SoundManager, 'playUrl').mockImplementation(() => {});
                answerViewModel.addCorrectStyling();
                expect(SoundManager.playUrl).toHaveBeenCalledWith(SoundConfig.VALIDATE_CORRECT);
            });

            it('should support adding and removing correct', () => {
                assertDoesNotHaveClass('incorrect');
                answerViewModel.addIncorrectStyling();
                assertHasClass('incorrect');
                assertDoesNotHaveClass('unvalidated');
                answerViewModel.removeIncorrectStyling();
                assertDoesNotHaveClass('incorrect');
                assertHasClass('unvalidated');
            });

            it('should support playing sounds during incorrect styling', () => {
                jest.spyOn(SoundManager, 'playUrl').mockImplementation(() => {});
                answerViewModel.addIncorrectStyling();
                expect(SoundManager.playUrl).toHaveBeenCalledWith(SoundConfig.VALIDATE_INCORRECT);
            });

            it('should support resetting', () => {
                answerViewModel.addIncorrectStyling();
                answerViewModel.resetCssClasses();
                assertDefaultClasses();
            });

            function assertDefaultClasses() {
                assertHasClass('unvalidated');
                assertDoesNotHaveClass('selected');
                assertDoesNotHaveClass('correct');
                assertDoesNotHaveClass('incorrect');
            }

            function assertHasClass(_class) {
                SpecHelper.expectEqual(true, answerViewModel.cssClasses.includes(_class), `hasClass "${_class}"`);
            }

            function assertDoesNotHaveClass(_class) {
                SpecHelper.expectEqual(
                    false,
                    answerViewModel.cssClasses.includes(_class),
                    `doesNotHaveClass "${_class}"`,
                );
            }
        });
    });

    describe('EditorViewModel', () => {});

    function getAnswerViewModel() {
        answerViewModel = getAnswerModel().createViewModel(frame.createFrameViewModel());
        return answerViewModel;
    }

    function getAnswerModel() {
        let answerModel;
        frame = Componentized.fixtures.getInstance();
        frame.addAnswer();
        answerModel = frame.getModelsByType('AnswerModel')[0];

        expect(answerModel).not.toBeUndefined();
        return answerModel;
    }
});
