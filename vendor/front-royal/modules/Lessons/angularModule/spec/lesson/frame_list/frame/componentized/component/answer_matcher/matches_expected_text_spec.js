import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/componentized_fixtures';
import 'Editor/angularModule';

import * as userAgentHelper from 'userAgentHelper';

describe('Componentized.Component.AnswerMatcher.MatchesExpectedText', () => {
    let $injector;
    let frame;
    let model;
    let Componentized;
    let SpecHelper;
    let ChallengeResponse;
    let MatchesExpectedText;
    let currentUser;
    let Locale;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                Componentized = $injector.get('Lesson.FrameList.Frame.Componentized');
                SpecHelper = $injector.get('SpecHelper');
                $injector.get('ComponentizedFixtures');
                ChallengeResponse = $injector.get(
                    'Lesson.FrameList.Frame.Componentized.Component.Challenge.ChallengeResponse',
                );
                MatchesExpectedText = $injector.get(
                    'Lesson.FrameList.Frame.Componentized.Component.AnswerMatcher.MatchesExpectedText.MatchesExpectedTextModel',
                );
                Locale = $injector.get('Locale');
                model = getModel();

                currentUser = SpecHelper.stubCurrentUser('learner');
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('Model', () => {
        describe('matches', () => {
            describe('baseline test', () => {
                it('should be false if no text in response', () => {
                    const challengeResponse = new ChallengeResponse({});
                    expect(model.matches(challengeResponse)).toBe(false);
                });
            });
            describe('with case sensitivity', () => {
                it('should be true if exactly the same', () => {
                    const testText = 'some CAPITALIZED text';
                    model = getModel({
                        caseSensitive: true,
                        expectedText: testText,
                    });
                    const challengeResponse = new ChallengeResponse({
                        text: testText,
                    });
                    expect(model.matches(challengeResponse)).toBe(true);
                });
                it('should be false if completely different', () => {
                    const testText = 'some CAPITALIZED text';
                    model = getModel({
                        caseSensitive: true,
                        expectedText: 'SOMETHING completely DIFFERENT',
                    });
                    const challengeResponse = new ChallengeResponse({
                        text: testText,
                    });
                    expect(model.matches(challengeResponse)).toBe(false);
                });
                it('should be false if case different', () => {
                    const testText = 'some CAPITALIZED text';
                    model = getModel({
                        caseSensitive: true,
                        expectedText: testText.toLowerCase(),
                    });
                    const challengeResponse = new ChallengeResponse({
                        text: testText,
                    });
                    expect(model.matches(challengeResponse)).toBe(false);
                });
            });
            describe('without case sensitivity', () => {
                it('should be true if exactly the same', () => {
                    const testText = 'some CAPITALIZED text';
                    model = getModel({
                        caseSensitive: false,
                        expectedText: testText,
                    });
                    const challengeResponse = new ChallengeResponse({
                        text: testText,
                    });
                    expect(model.matches(challengeResponse)).toBe(true);
                });
                it('should be false if completely different', () => {
                    const testText = 'some CAPITALIZED text';
                    model = getModel({
                        caseSensitive: false,
                        expectedText: 'SOMETHING completely DIFFERENT',
                    });
                    const challengeResponse = new ChallengeResponse({
                        text: testText,
                    });
                    expect(model.matches(challengeResponse)).toBe(false);
                });
                it('should be true if case different', () => {
                    const testText = 'some CAPITALIZED text';
                    model = getModel({
                        caseSensitive: false,
                        expectedText: testText.toLowerCase(),
                    });
                    const challengeResponse = new ChallengeResponse({
                        text: testText,
                    });
                    expect(model.matches(challengeResponse)).toBe(true);
                });
            });

            describe('with smart punctuation', () => {
                it('should correctly accept curly single and double quotes if the answer has straight', () => {
                    model = getModel({
                        caseSensitive: true,
                        expectedText: '\'test\' "test"',
                    });

                    expect(
                        model.matches(
                            new ChallengeResponse({
                                text: '\u2018test\u2019 \u201Ctest\u201D',
                            }),
                        ),
                    ).toBe(true);
                });

                it('should correctly accept straight single and double quotes if the answer has curly', () => {
                    model = getModel({
                        caseSensitive: true,
                        expectedText: '\u2018test\u2019 \u201Ctest\u201D',
                    });

                    expect(
                        model.matches(
                            new ChallengeResponse({
                                text: '\'test\' "test"',
                            }),
                        ),
                    ).toBe(true);
                });
            });

            describe('with special correctThreshold', () => {
                it('should be true if exactly the same', () => {
                    const testText = 'some CAPITALIZED text';
                    model = getModel({
                        caseSensitive: false,
                        expectedText: testText,
                        correctThreshold: 50,
                    });
                    const challengeResponse = new ChallengeResponse({
                        text: testText,
                    });
                    expect(model.matches(challengeResponse)).toBe(true);
                });
                it('should be false if completely different', () => {
                    const testText = 'some CAPITALIZED text';
                    model = getModel({
                        caseSensitive: false,
                        expectedText: 'SOMETHING completely DIFFERENT',
                        correctThreshold: 50,
                    });
                    const challengeResponse = new ChallengeResponse({
                        text: testText,
                    });
                    expect(model.matches(challengeResponse)).toBe(false);
                });
                it('should be true if partially complete up to threshold', () => {
                    const testText = 'some CAPITALIZED text';
                    model = getModel({
                        caseSensitive: false,
                        expectedText: testText,
                        correctThreshold: 50,
                    });
                    let challengeResponse = new ChallengeResponse({
                        text: testText.substr(0, testText.length / 2 + 1),
                    });
                    expect(model.matches(challengeResponse)).toBe(true);

                    challengeResponse = new ChallengeResponse({
                        text: testText.substr(0, testText.length / 2 - 1),
                    });
                    expect(model.matches(challengeResponse)).toBe(false);
                });
            });

            describe('with mode: exact', () => {
                it('should require all characters', () => {
                    const currencies = MatchesExpectedText.prototype._currencies;
                    const testText = `abcdefghijklmnopqrtsuvwxyz1234567890 .,;!@#%^&*()[]${currencies}`;
                    model = getModel({
                        caseSensitive: false,
                        expectedText: testText,
                        correctThreshold: 100,
                        mode: 'exact',
                    });
                    let challengeResponse = new ChallengeResponse({
                        text: testText,
                    });
                    expect(model.matches(challengeResponse)).toBe(true);

                    challengeResponse = new ChallengeResponse({
                        text: testText.replace(/[,]/g, ''),
                    });
                    expect(model.matches(challengeResponse)).toBe(false);

                    challengeResponse = new ChallengeResponse({
                        text: testText.replace(new RegExp(`[,${currencies}]`, 'g'), ''),
                    });
                    expect(model.matches(challengeResponse)).toBe(false);
                });
            });

            describe('with mode: number', () => {
                it('should respect period decimal delimeter preferences', () => {
                    currentUser.pref_decimal_delim = '.';

                    model = getModel({
                        caseSensitive: false,
                        expectedText: '1,000,000.50',
                        correctThreshold: 100,
                        mode: 'number',
                    });

                    expect(
                        model.matches(
                            new ChallengeResponse({
                                text: '1000000.50',
                            }),
                        ),
                    ).toBe(true);

                    expect(
                        model.matches(
                            new ChallengeResponse({
                                text: '1,000,000.50',
                            }),
                        ),
                    ).toBe(true);

                    expect(
                        model.matches(
                            new ChallengeResponse({
                                text: '1,000,000,50',
                            }),
                        ),
                    ).toBe(false);

                    expect(
                        model.matches(
                            new ChallengeResponse({
                                text: '1000000,50',
                            }),
                        ),
                    ).toBe(false);

                    expect(
                        model.matches(
                            new ChallengeResponse({
                                text: '1.000.000,50',
                            }),
                        ),
                    ).toBe(false);
                });

                it('should respect comma decimal delimeter preferences', () => {
                    currentUser.pref_decimal_delim = ',';

                    model = getModel({
                        caseSensitive: false,
                        expectedText: '1,000,000.50',
                        correctThreshold: 100,
                        mode: 'number',
                    });

                    expect(
                        model.matches(
                            new ChallengeResponse({
                                text: '1000000.50',
                            }),
                        ),
                    ).toBe(false);

                    expect(
                        model.matches(
                            new ChallengeResponse({
                                text: '1,000,000.50',
                            }),
                        ),
                    ).toBe(false);

                    expect(
                        model.matches(
                            new ChallengeResponse({
                                text: '1,000,000,50',
                            }),
                        ),
                    ).toBe(false);

                    expect(
                        model.matches(
                            new ChallengeResponse({
                                text: '1000000,50',
                            }),
                        ),
                    ).toBe(true);

                    expect(
                        model.matches(
                            new ChallengeResponse({
                                text: '1.000.000,50',
                            }),
                        ),
                    ).toBe(true);
                });

                it('should remove spaces', () => {
                    model = getModel({
                        caseSensitive: false,
                        expectedText: '0.444',
                        correctThreshold: 100,
                        mode: 'number',
                    });

                    expect(
                        model.matches(
                            new ChallengeResponse({
                                text: '0.44 4',
                            }),
                        ),
                    ).toBe(true);
                });
            });

            describe('with mode: decimal', () => {
                it('should respect period decimal delimeter preferences', () => {
                    currentUser.pref_decimal_delim = '.';

                    model = getModel({
                        caseSensitive: false,
                        expectedText: '1,000,000.50',
                        correctThreshold: 100,
                        mode: 'decimal',
                    });

                    expect(
                        model.matches(
                            new ChallengeResponse({
                                text: '1000000.50',
                            }),
                        ),
                    ).toBe(true);

                    expect(
                        model.matches(
                            new ChallengeResponse({
                                text: '1,000,000.50',
                            }),
                        ),
                    ).toBe(true);

                    expect(
                        model.matches(
                            new ChallengeResponse({
                                text: '1,000,000,50',
                            }),
                        ),
                    ).toBe(false);

                    expect(
                        model.matches(
                            new ChallengeResponse({
                                text: '1000000,50',
                            }),
                        ),
                    ).toBe(false);

                    expect(
                        model.matches(
                            new ChallengeResponse({
                                text: '1.000.000,50',
                            }),
                        ),
                    ).toBe(false);
                });

                it('should respect comma decimal delimeter preferences', () => {
                    currentUser.pref_decimal_delim = ',';

                    model = getModel({
                        caseSensitive: false,
                        expectedText: '1,000,000.50',
                        correctThreshold: 100,
                        mode: 'decimal',
                    });

                    expect(
                        model.matches(
                            new ChallengeResponse({
                                text: '1000000.50',
                            }),
                        ),
                    ).toBe(false);

                    expect(
                        model.matches(
                            new ChallengeResponse({
                                text: '1,000,000.50',
                            }),
                        ),
                    ).toBe(false);

                    expect(
                        model.matches(
                            new ChallengeResponse({
                                text: '1,000,000,50',
                            }),
                        ),
                    ).toBe(false);

                    expect(
                        model.matches(
                            new ChallengeResponse({
                                text: '1000000,50',
                            }),
                        ),
                    ).toBe(true);

                    expect(
                        model.matches(
                            new ChallengeResponse({
                                text: '1.000.000,50',
                            }),
                        ),
                    ).toBe(true);
                });

                it('should remove spaces', () => {
                    model = getModel({
                        caseSensitive: false,
                        expectedText: '0.444',
                        correctThreshold: 100,
                        mode: 'decimal',
                    });

                    expect(
                        model.matches(
                            new ChallengeResponse({
                                text: '0.44 4',
                            }),
                        ),
                    ).toBe(true);
                });

                it('should ignore missing leading zero', () => {
                    model = getModel({
                        caseSensitive: false,
                        expectedText: '0.444',
                        correctThreshold: 100,
                        mode: 'decimal',
                    });

                    expect(
                        model.matches(
                            new ChallengeResponse({
                                text: '.44 4',
                            }),
                        ),
                    ).toBe(true);
                });

                it('should ignore extra leading zero', () => {
                    model = getModel({
                        caseSensitive: false,
                        expectedText: '.444',
                        correctThreshold: 100,
                        mode: 'decimal',
                    });

                    expect(
                        model.matches(
                            new ChallengeResponse({
                                text: '0.44 4',
                            }),
                        ),
                    ).toBe(true);
                });

                it('should ignore missing leading zero with alternative delimeter', () => {
                    currentUser.pref_decimal_delim = ',';

                    model = getModel({
                        caseSensitive: false,
                        expectedText: '0.444',
                        correctThreshold: 100,
                        mode: 'decimal',
                    });

                    expect(
                        model.matches(
                            new ChallengeResponse({
                                text: ',44 4',
                            }),
                        ),
                    ).toBe(true);
                });

                it('should ignore extra leading zero with alternative delimeter', () => {
                    currentUser.pref_decimal_delim = ',';

                    model = getModel({
                        caseSensitive: false,
                        expectedText: '.444',
                        correctThreshold: 100,
                        mode: 'decimal',
                    });

                    expect(
                        model.matches(
                            new ChallengeResponse({
                                text: '0,44 4',
                            }),
                        ),
                    ).toBe(true);
                });
            });

            describe('with mode: currency', () => {
                it('should not care about currency characters', () => {
                    const currencies = MatchesExpectedText.prototype._currencies;
                    const testText = `abcdefghijklmnopqrtsuvwxyz1234567890 .,;!@#%^&*()[]${currencies}`;
                    model = getModel({
                        caseSensitive: false,
                        expectedText: testText,
                        correctThreshold: 100,
                        mode: 'currency',
                    });
                    let challengeResponse = new ChallengeResponse({
                        text: testText,
                    });
                    expect(model.matches(challengeResponse)).toBe(true);

                    challengeResponse = new ChallengeResponse({
                        text: testText.replace(new RegExp(`[,${currencies}]`, 'g'), ''),
                    });
                    expect(model.matches(challengeResponse)).toBe(true);
                });

                it('should respect period decimal delimeter preferences', () => {
                    currentUser.pref_decimal_delim = '.';

                    model = getModel({
                        caseSensitive: false,
                        expectedText: '1,000,000.50',
                        correctThreshold: 100,
                        mode: 'currency',
                    });

                    expect(
                        model.matches(
                            new ChallengeResponse({
                                text: '1000000.50',
                            }),
                        ),
                    ).toBe(true);

                    expect(
                        model.matches(
                            new ChallengeResponse({
                                text: '1,000,000.50',
                            }),
                        ),
                    ).toBe(true);

                    expect(
                        model.matches(
                            new ChallengeResponse({
                                text: '1,000,000,50',
                            }),
                        ),
                    ).toBe(false);

                    expect(
                        model.matches(
                            new ChallengeResponse({
                                text: '1000000,50',
                            }),
                        ),
                    ).toBe(false);

                    expect(
                        model.matches(
                            new ChallengeResponse({
                                text: '1.000.000,50',
                            }),
                        ),
                    ).toBe(false);
                });

                it('should respect comma decimal delimeter preferences', () => {
                    currentUser.pref_decimal_delim = ',';

                    model = getModel({
                        caseSensitive: false,
                        expectedText: '1,000,000.50',
                        correctThreshold: 100,
                        mode: 'currency',
                    });

                    expect(
                        model.matches(
                            new ChallengeResponse({
                                text: '1000000.50',
                            }),
                        ),
                    ).toBe(false);

                    expect(
                        model.matches(
                            new ChallengeResponse({
                                text: '1,000,000.50',
                            }),
                        ),
                    ).toBe(false);

                    expect(
                        model.matches(
                            new ChallengeResponse({
                                text: '1,000,000,50',
                            }),
                        ),
                    ).toBe(false);

                    expect(
                        model.matches(
                            new ChallengeResponse({
                                text: '1000000,50',
                            }),
                        ),
                    ).toBe(true);

                    expect(
                        model.matches(
                            new ChallengeResponse({
                                text: '1.000.000,50',
                            }),
                        ),
                    ).toBe(true);
                });

                it('should remove spaces', () => {
                    model = getModel({
                        caseSensitive: false,
                        expectedText: '$0.444',
                        correctThreshold: 100,
                        mode: 'currency',
                    });

                    expect(
                        model.matches(
                            new ChallengeResponse({
                                text: '$0.44 4',
                            }),
                        ),
                    ).toBe(true);
                });

                // the following are based on the decimal mode testing
                it('should ignore missing leading zero', () => {
                    model = getModel({
                        caseSensitive: false,
                        expectedText: '$0.444',
                        correctThreshold: 100,
                        mode: 'currency',
                    });

                    expect(
                        model.matches(
                            new ChallengeResponse({
                                text: '.44 4',
                            }),
                        ),
                    ).toBe(true);

                    expect(
                        model.matches(
                            new ChallengeResponse({
                                text: '$.44 4',
                            }),
                        ),
                    ).toBe(true);
                });

                it('should ignore extra leading zero', () => {
                    model = getModel({
                        caseSensitive: false,
                        expectedText: '$.444',
                        correctThreshold: 100,
                        mode: 'currency',
                    });

                    expect(
                        model.matches(
                            new ChallengeResponse({
                                text: '0.44 4',
                            }),
                        ),
                    ).toBe(true);

                    expect(
                        model.matches(
                            new ChallengeResponse({
                                text: '$0.44 4',
                            }),
                        ),
                    ).toBe(true);
                });

                it('should ignore missing leading zero with alternative delimeter', () => {
                    currentUser.pref_decimal_delim = ',';

                    model = getModel({
                        caseSensitive: false,
                        expectedText: '$0.444',
                        correctThreshold: 100,
                        mode: 'currency',
                    });

                    expect(
                        model.matches(
                            new ChallengeResponse({
                                text: ',44 4',
                            }),
                        ),
                    ).toBe(true);

                    expect(
                        model.matches(
                            new ChallengeResponse({
                                text: '$,44 4',
                            }),
                        ),
                    ).toBe(true);
                });

                it('should ignore extra leading zero with alternative delimeter', () => {
                    currentUser.pref_decimal_delim = ',';

                    model = getModel({
                        caseSensitive: false,
                        expectedText: '$.444',
                        correctThreshold: 100,
                        mode: 'currency',
                    });

                    expect(
                        model.matches(
                            new ChallengeResponse({
                                text: '0,44 4',
                            }),
                        ),
                    ).toBe(true);

                    expect(
                        model.matches(
                            new ChallengeResponse({
                                text: '$0,44 4',
                            }),
                        ),
                    ).toBe(true);
                });

                // also handle the odd case where we have a currency mode but no currency symbol
                it('this', () => {
                    model = getModel({
                        caseSensitive: false,
                        expectedText: '.50',
                        correctThreshold: 100,
                        mode: 'currency',
                    });

                    expect(
                        model.matches(
                            new ChallengeResponse({
                                text: '$0.50',
                            }),
                        ),
                    ).toBe(true);
                });

                it('this', () => {
                    model = getModel({
                        caseSensitive: false,
                        expectedText: '0.50',
                        correctThreshold: 100,
                        mode: 'currency',
                    });

                    expect(
                        model.matches(
                            new ChallengeResponse({
                                text: '$.50',
                            }),
                        ),
                    ).toBe(true);
                });
            });

            describe('with Arabic locale', () => {
                describe('with mode: number', () => {
                    it('should always use comma decimal delimeter and not allow unnecessary thousands separators', () => {
                        // set them to the "period" decimal preference to verify we properly ignore it
                        currentUser.pref_decimal_delim = '.';

                        // Note: the expected text here is the Arabic equivalent of 1,000,000.50 in English
                        model = getModel({
                            caseSensitive: false,
                            expectedText: '١٠٠٠٠٠٠,٥٠',
                            correctThreshold: 100,
                            mode: 'number',
                        });
                        jest.spyOn(model, 'localeObject', 'get').mockReturnValue(Locale.arabic);

                        // wrong decimal symbol (.)
                        expect(
                            model.matches(
                                new ChallengeResponse({
                                    text: '١٠٠٠٠٠٠.٥٠',
                                }),
                            ),
                        ).toBe(false);

                        // wrong thousands separators
                        expect(
                            model.matches(
                                new ChallengeResponse({
                                    text: '١,٠٠٠,٠٠٠.٥٠',
                                }),
                            ),
                        ).toBe(false);

                        // just wrong; incorrect thousands separators, wrong decimal point
                        expect(
                            model.matches(
                                new ChallengeResponse({
                                    text: '١,٠٠٠,٠٠٠,٥٠',
                                }),
                            ),
                        ).toBe(false);

                        // unnecessary thousands separator
                        expect(
                            model.matches(
                                new ChallengeResponse({
                                    text: '١.٠٠٠.٠٠٠,٥٠',
                                }),
                            ),
                        ).toBe(false);

                        // correct, exact match
                        expect(
                            model.matches(
                                new ChallengeResponse({
                                    text: '١٠٠٠٠٠٠,٥٠',
                                }),
                            ),
                        ).toBe(true);
                    });

                    it('should always use comma decimal delimeter allow alternative thousands separator', () => {
                        // set them to the "period" decimal preference to verify we properly ignore it
                        currentUser.pref_decimal_delim = '.';

                        // Note: the expected text here is the Arabic equivalent of 1,000,000.50 in English
                        model = getModel({
                            caseSensitive: false,
                            expectedText: '١.٠٠٠.٠٠٠,٥٠',
                            correctThreshold: 100,
                            mode: 'number',
                        });
                        jest.spyOn(model, 'localeObject', 'get').mockReturnValue(Locale.arabic);

                        expect(
                            model.matches(
                                new ChallengeResponse({
                                    text: '١٠٠٠٠٠٠,٥٠',
                                }),
                            ),
                        ).toBe(true);

                        expect(
                            model.matches(
                                new ChallengeResponse({
                                    text: '١.٠٠٠.٠٠٠,٥٠',
                                }),
                            ),
                        ).toBe(true);
                    });
                });

                describe('with mode: decimal', () => {
                    it('should always use comma decimal delimeter and not allow unnecessary thousands separators', () => {
                        // set them to the "period" decimal preference to verify we properly ignore it
                        currentUser.pref_decimal_delim = '.';

                        // Note: the expected text here is the Arabic equivalent of 1,000,000.50 in English
                        model = getModel({
                            caseSensitive: false,
                            expectedText: '١٠٠٠٠٠٠,٥٠',
                            correctThreshold: 100,
                            mode: 'number',
                        });
                        jest.spyOn(model, 'localeObject', 'get').mockReturnValue(Locale.arabic);

                        // wrong decimal symbol (.)
                        expect(
                            model.matches(
                                new ChallengeResponse({
                                    text: '١٠٠٠٠٠٠.٥٠',
                                }),
                            ),
                        ).toBe(false);

                        // wrong thousands separators
                        expect(
                            model.matches(
                                new ChallengeResponse({
                                    text: '١,٠٠٠,٠٠٠.٥٠',
                                }),
                            ),
                        ).toBe(false);

                        // just wrong; incorrect thousands separators, wrong decimal point
                        expect(
                            model.matches(
                                new ChallengeResponse({
                                    text: '١,٠٠٠,٠٠٠,٥٠',
                                }),
                            ),
                        ).toBe(false);

                        // unnecessary thousands separator
                        expect(
                            model.matches(
                                new ChallengeResponse({
                                    text: '١.٠٠٠.٠٠٠,٥٠',
                                }),
                            ),
                        ).toBe(false);

                        // correct, exact match
                        expect(
                            model.matches(
                                new ChallengeResponse({
                                    text: '١٠٠٠٠٠٠,٥٠',
                                }),
                            ),
                        ).toBe(true);
                    });

                    it('should always use comma decimal delimeter allow alternative thousands separator', () => {
                        // set them to the "period" decimal preference to verify we properly ignore it
                        currentUser.pref_decimal_delim = '.';

                        // Note: the expected text here is the Arabic equivalent of 1,000,000.50 in English
                        model = getModel({
                            caseSensitive: false,
                            expectedText: '١.٠٠٠.٠٠٠,٥٠',
                            correctThreshold: 100,
                            mode: 'number',
                        });
                        jest.spyOn(model, 'localeObject', 'get').mockReturnValue(Locale.arabic);

                        expect(
                            model.matches(
                                new ChallengeResponse({
                                    text: '١٠٠٠٠٠٠,٥٠',
                                }),
                            ),
                        ).toBe(true);

                        expect(
                            model.matches(
                                new ChallengeResponse({
                                    text: '١.٠٠٠.٠٠٠,٥٠',
                                }),
                            ),
                        ).toBe(true);
                    });

                    it('should ignore missing leading zero and remove spaces', () => {
                        model = getModel({
                            caseSensitive: false,
                            expectedText: '٠,٤٤٤',
                            correctThreshold: 100,
                            mode: 'decimal',
                        });
                        jest.spyOn(model, 'localeObject', 'get').mockReturnValue(Locale.arabic);

                        expect(
                            model.matches(
                                new ChallengeResponse({
                                    text: ',٤٤ ٤',
                                }),
                            ),
                        ).toBe(true);
                    });

                    it('should ignore extra leading zero and remove spaces', () => {
                        model = getModel({
                            caseSensitive: false,
                            expectedText: ',٤٤٤',
                            correctThreshold: 100,
                            mode: 'decimal',
                        });
                        jest.spyOn(model, 'localeObject', 'get').mockReturnValue(Locale.arabic);

                        expect(
                            model.matches(
                                new ChallengeResponse({
                                    text: '٠,٤٤ ٤',
                                }),
                            ),
                        ).toBe(true);
                    });
                });

                describe('with mode: currency', () => {
                    it('should always use comma decimal delimeter and not allow unnecessary thousands separators', () => {
                        // set them to the "period" decimal preference to verify we properly ignore it
                        currentUser.pref_decimal_delim = '.';

                        // Note: the expected text here is the Arabic equivalent of 1,000,000.50 in English
                        model = getModel({
                            caseSensitive: false,
                            expectedText: '١٠٠٠٠٠٠,٥٠',
                            correctThreshold: 100,
                            mode: 'number',
                        });
                        jest.spyOn(model, 'localeObject', 'get').mockReturnValue(Locale.arabic);

                        // wrong decimal symbol (.)
                        expect(
                            model.matches(
                                new ChallengeResponse({
                                    text: '١٠٠٠٠٠٠.٥٠',
                                }),
                            ),
                        ).toBe(false);

                        // wrong thousands separators
                        expect(
                            model.matches(
                                new ChallengeResponse({
                                    text: '١,٠٠٠,٠٠٠.٥٠',
                                }),
                            ),
                        ).toBe(false);

                        // just wrong; incorrect thousands separators, wrong decimal point
                        expect(
                            model.matches(
                                new ChallengeResponse({
                                    text: '١,٠٠٠,٠٠٠,٥٠',
                                }),
                            ),
                        ).toBe(false);

                        // unnecessary thousands separator
                        expect(
                            model.matches(
                                new ChallengeResponse({
                                    text: '١.٠٠٠.٠٠٠,٥٠',
                                }),
                            ),
                        ).toBe(false);

                        // correct, exact match
                        expect(
                            model.matches(
                                new ChallengeResponse({
                                    text: '١٠٠٠٠٠٠,٥٠',
                                }),
                            ),
                        ).toBe(true);
                    });

                    it('should ignore trailing dinar (د.أ ) after number', () => {
                        model = getModel({
                            caseSensitive: false,
                            expectedText: '١٠٠ د.أ',
                            correctThreshold: 100,
                            mode: 'currency',
                        });
                        jest.spyOn(model, 'localeObject', 'get').mockReturnValue(Locale.arabic);

                        expect(
                            model.matches(
                                new ChallengeResponse({
                                    text: '١٠٠ د.أ',
                                }),
                            ),
                        ).toBe(true);

                        // missing dinar
                        expect(
                            model.matches(
                                new ChallengeResponse({
                                    text: '١٠٠',
                                }),
                            ),
                        ).toBe(true);

                        // missing space
                        expect(
                            model.matches(
                                new ChallengeResponse({
                                    text: '١٠٠د.أ',
                                }),
                            ),
                        ).toBe(true);
                    });
                });
            });
        });

        describe('matchesWithDetails', () => {
            it('handle correct response details properly', () => {
                const testText = 'some valid text';
                model = getModel({
                    caseSensitive: true,
                    expectedText: testText,
                });
                const challengeResponse = new ChallengeResponse({
                    text: testText,
                });
                const expectedDetails = {
                    matches: true,
                    partialMatch: false,
                    userAnswerToDisplay: 'some valid text',
                    nextCharacterExpected: undefined,
                };
                expect(model.matchesWithDetails(challengeResponse)).toEqual(expectedDetails);
            });

            it('handle incorrect response details properly', () => {
                const testText = 'some valid text';
                model = getModel({
                    caseSensitive: true,
                    expectedText: testText,
                });
                const challengeResponse = new ChallengeResponse({
                    text: 'some invalid text',
                });
                const expectedDetails = {
                    matches: false,
                    partialMatch: false,
                    userAnswerToDisplay: 'some invalid text',
                    nextCharacterExpected: 'v',
                };
                expect(model.matchesWithDetails(challengeResponse)).toEqual(expectedDetails);
            });

            describe('with mode: exact', () => {
                let $window;

                beforeEach(() => {
                    $window = $injector.get('$window');
                });

                afterEach(() => {
                    $window.CORDOVA = false;
                });

                it('should format userAnswerToDisplay properly', () => {
                    const testText = 'Text123';
                    model = getModel({
                        caseSensitive: false,
                        expectedText: testText,
                        mode: 'exact',
                    });
                    const challengeResponse = new ChallengeResponse({
                        text: 'text',
                    });
                    const expectedDetails = {
                        matches: false,
                        partialMatch: true,
                        userAnswerToDisplay: 'Text',
                        nextCharacterExpected: '1',
                    };
                    expect(model.matchesWithDetails(challengeResponse)).toEqual(expectedDetails);
                });

                it('should not userAnswerToDisplay if Android Cordova', () => {
                    $window.CORDOVA = true;
                    jest.spyOn(userAgentHelper, 'isAndroidDevice').mockReturnValue(true);

                    const testText = 'Text123';
                    model = getModel({
                        caseSensitive: false,
                        expectedText: testText,
                        mode: 'exact',
                    });
                    const challengeResponse = new ChallengeResponse({
                        text: 'text',
                    });
                    const expectedDetails = {
                        matches: false,
                        partialMatch: true,
                        userAnswerToDisplay: 'text',
                        nextCharacterExpected: '1',
                    };
                    expect(model.matchesWithDetails(challengeResponse)).toEqual(expectedDetails);
                });
            });
        });
    });

    function getModel(options) {
        frame = Componentized.fixtures.getInstance();
        return frame.addMatchesExpectedText(options);
    }
});
