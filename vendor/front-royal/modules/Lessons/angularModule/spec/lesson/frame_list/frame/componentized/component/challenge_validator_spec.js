import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/componentized_fixtures';
import 'Editor/angularModule';

describe('Componentized.Component.ChallengeValidator', () => {
    let frame;
    let frameViewModel;
    let viewModel;
    let ChallengeValidatorModel;
    let Componentized;
    let ComponentEventListener;
    let ValidationResult;
    let MissingExpectedAnswer;
    let HasUnexpectedAnswer;
    let SpecHelper;
    let EventLogger;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                ChallengeValidatorModel = $injector.get(
                    'Lesson.FrameList.Frame.Componentized.Component.ChallengeValidator.ChallengeValidatorModel',
                );
                Componentized = $injector.get('Lesson.FrameList.Frame.Componentized');
                ComponentEventListener = $injector.get(
                    'Lesson.FrameList.Frame.Componentized.Component.ComponentEventListener',
                );
                SpecHelper = $injector.get('SpecHelper');
                $injector.get('ComponentizedFixtures');
                ValidationResult = $injector.get(
                    'Lesson.FrameList.Frame.Componentized.Component.ChallengeValidator.ValidationResult',
                );
                MissingExpectedAnswer = $injector.get(
                    'Lesson.FrameList.Frame.Componentized.Component.ChallengeValidator.ValidationError.MissingExpectedAnswer',
                );
                HasUnexpectedAnswer = $injector.get(
                    'Lesson.FrameList.Frame.Componentized.Component.ChallengeValidator.ValidationError.HasUnexpectedAnswer',
                );
                EventLogger = $injector.get('EventLogger');
                viewModel = getViewModel();

                // mock out asJson so we don't have to use fully operational challenges
                jest.spyOn(ValidationResult.prototype, 'asJson').mockReturnValue({
                    validation_result: 'json',
                });
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('Model', () => {
        describe('challenge', () => {
            it('should have a reference to the model for the associated challenge', () => {
                const challengeModel = viewModel.model.challenge;
                expect(challengeModel).not.toBeUndefined();
                expect(challengeModel.validator).toBe(viewModel.model);
            });
        });

        describe('isExpectedAnswer', () => {
            let mockAnswer;
            let model;
            let mockAnswerMatcher;

            beforeEach(() => {
                model = viewModel.model;
                mockAnswerMatcher = frame.addVanillaComponent();
                model.expectedAnswerMatchers = [mockAnswerMatcher];
                mockAnswer = {};
            });

            it('should return true if it has an answerMatcher that matches', () => {
                mockAnswerMatcher.matches = jest.fn().mockReturnValue(true);
                expect(model.isExpectedAnswer(mockAnswer)).toBe(true);
                expect(mockAnswerMatcher.matches).toHaveBeenCalledWith(mockAnswer);
            });

            it('should return false if it does not have an answerMatcher that matches', () => {
                mockAnswerMatcher.matches = jest.fn().mockReturnValue(false);
                expect(model.isExpectedAnswer(mockAnswer)).toBe(false);
                expect(mockAnswerMatcher.matches).toHaveBeenCalledWith(mockAnswer);
            });
        });
    });

    describe('ViewModel', () => {
        describe('active', () => {
            it('should be settable and fire events', () => {
                const activatedCallback = jest.fn();
                const deActivatedCallback = jest.fn();
                new ComponentEventListener(viewModel, 'activated', activatedCallback);
                new ComponentEventListener(viewModel, 'activated', deActivatedCallback);
                viewModel.active = true;
                expect(viewModel.active).toBe(true);
                expect(activatedCallback).toHaveBeenCalled();
                viewModel.active = false;
                expect(viewModel.active).toBe(false);
                expect(deActivatedCallback).toHaveBeenCalled();
            });
        });

        describe('challengeViewModel', () => {
            it('should have a reference the viewModel for the associated challenge', () => {
                const challengeViewModel = viewModel.challengeViewModel;
                expect(challengeViewModel).not.toBeUndefined();
                expect(challengeViewModel.validatorViewModel).toBe(viewModel);
            });
        });

        describe('validate', () => {
            it('should run validations and then fire with errors', () => {
                const callback = jest.fn();

                viewModel.validations = [
                    errors => {
                        errors.push('An error');
                    },
                ];

                new ComponentEventListener(viewModel, 'validated', callback);
                viewModel.validate('info');
                expect(callback).toHaveBeenCalled();
                expect(callback.mock.calls[0][0].constructor).toBe(ValidationResult);
                expect(callback.mock.calls[0][0].errors).toEqual(['An error']);
                expect(callback.mock.calls[0][0].info).toEqual('info');
            });

            it('should run validations and then fire with no errors', () => {
                const callback = jest.fn();

                new ComponentEventListener(viewModel, 'validated', callback);
                viewModel.validate('info');
                expect(callback).toHaveBeenCalled();
                expect(callback.mock.calls[0][0].constructor).toBe(ValidationResult);
                expect(callback.mock.calls[0][0].errors).toEqual([]);
                expect(callback.mock.calls[0][0].info).toEqual('info');
            });

            it('should fire with a custom ValidationResult class', () => {
                const customValidationResult = function () {
                    this.result = true;
                    this.asJson = () => {};
                };
                viewModel.challengeViewModel.constructor.ValidationResult = customValidationResult;
                const callback = jest.fn();
                new ComponentEventListener(viewModel, 'validated', callback);
                viewModel.validate();
                expect(callback.mock.calls[0][0].constructor).toBe(customValidationResult);
            });

            it('should return validation result', () => {
                const result = viewModel.validate();
                expect(result.isA(ValidationResult)).toBe(true);
            });

            it('should log to EventLogger if shouldLogEvent is true', () => {
                Object.defineProperty(ValidationResult.prototype, 'shouldLogEvent', {
                    value: true,
                });
                jest.spyOn(EventLogger.prototype, 'log').mockImplementation(() => {});
                jest.spyOn(frameViewModel, 'logInfo').mockReturnValue({
                    logInfo: 'json',
                });
                viewModel.validate();
                expect(EventLogger.prototype.log).toHaveBeenCalledWith(
                    'lesson:challenge_validation',
                    {
                        validation_result: 'json',
                        logInfo: 'json',
                    },
                    {
                        segmentio: false,
                    },
                );
            });

            it('should not log to EventLogger if shouldLogEvent is false', () => {
                Object.defineProperty(ValidationResult.prototype, 'shouldLogEvent', {
                    value: false,
                });
                jest.spyOn(EventLogger.prototype, 'log').mockImplementation(() => {});
                viewModel.validate();
                expect(EventLogger.prototype.log).not.toHaveBeenCalled();
            });

            it('should save the validation result in validationResults', () => {
                // do the assertion in an event listener to make sure that the value was set before
                // the event fired
                expect(viewModel.challengeViewModel.validationResults.length).toEqual(0);
                new ComponentEventListener(viewModel, 'validated', () => {
                    expect(viewModel.challengeViewModel.validationResults.length).toEqual(1);
                });
                viewModel.validate('info');
            });
        });

        describe('behaviors', () => {
            let model;
            let challengeModel;
            let challengeViewModel;
            let validator;

            describe('HasAllExpectedAnswers', () => {
                itShouldRaiseIfNoExpectedAnswerMatchers('HasAllExpectedAnswers');

                it('should add no errors if all expected answers found', () => {
                    setupChallengeWithExpectedAnswer('HasAllExpectedAnswers');
                    challengeViewModel.challengeResponses = 'challengeResponses';

                    // When validate() is called, the one expectedAnswerMatcher should check
                    // the challengeResponses to see if there are any matches
                    jest.spyOn(validator.expectedAnswerMatchers[0], 'matchesAny').mockReturnValue(true);
                    const errors = viewModel.validate().errors;
                    expect(validator.expectedAnswerMatchers[0].matchesAny).toHaveBeenCalledWith('challengeResponses');

                    // since matchesAny returned true, there are no errors
                    SpecHelper.expectEqual(0, errors.length, 'No errors');
                });

                it('should add an error if an expected answer not found', () => {
                    setupChallengeWithExpectedAnswer('HasAllExpectedAnswers');
                    challengeViewModel.challengeResponses = [
                        {
                            answer: frame.addAnswer(),
                        },
                    ];

                    // When validate() is called, the one expectedAnswerMatcher should check
                    // the challengeResponses to see if there are any matches
                    jest.spyOn(validator.expectedAnswerMatchers[0], 'matchesAny').mockReturnValue(false);
                    const errors = viewModel.validate().errors;
                    expect(validator.expectedAnswerMatchers[0].matchesAny).toHaveBeenCalledWith(
                        challengeViewModel.challengeResponses,
                    );

                    // since matchesAny returned false, there is an error
                    SpecHelper.expectEqual(1, errors.length, 'One error');
                    SpecHelper.expectEqual(
                        MissingExpectedAnswer,
                        errors[0].constructor,
                        'error is instance of MissingExpectedAnswer',
                    );
                    SpecHelper.expectEqual(
                        validator.expectedAnswerMatchers[0],
                        errors[0].expectedAnswerMatcher,
                        'answer matcher',
                    );
                });
            });

            describe('HasNoUnexpectedAnswers', () => {
                itShouldRaiseIfNoExpectedAnswerMatchers('HasNoUnexpectedAnswers');

                it('should add no errors if all expected answers found', () => {
                    setupChallengeWithExpectedAnswer('HasNoUnexpectedAnswers');
                    challengeViewModel.challengeResponses = 'challengeResponses';

                    // When validate() is called, the one expectedAnswerMatcher should check
                    // the challengeResponses to see if there are any matches
                    const answer = {
                        matchesAny: jest.fn().mockReturnValue(true),
                    };
                    challengeViewModel.challengeResponses = [answer];
                    const errors = viewModel.validate().errors;
                    expect(answer.matchesAny).toHaveBeenCalledWith(validator.expectedAnswerMatchers);

                    // since matchesAny returned true, there are no errors
                    SpecHelper.expectEqual(0, errors.length, 'No errors');
                });

                it('should add an error if an expected answer not found', () => {
                    setupChallengeWithExpectedAnswer('HasNoUnexpectedAnswers');

                    // When validate() is called, the one expectedAnswerMatcher should check
                    // the challengeResponses to see if there are any matches
                    const challengeResponse = {
                        matchesAny: jest.fn().mockReturnValue(false),
                        answer: frame.addAnswer(),
                    };
                    challengeViewModel.challengeResponses = [challengeResponse];
                    const errors = viewModel.validate().errors;
                    expect(challengeResponse.matchesAny).toHaveBeenCalledWith(validator.expectedAnswerMatchers);

                    // since matchesAny returned false, there is an error
                    SpecHelper.expectEqual(1, errors.length, 'One error');
                    SpecHelper.expectEqual(
                        HasUnexpectedAnswer,
                        errors[0].constructor,
                        'error is instance of HasUnexpectedAnswer',
                    );
                    SpecHelper.expectEqual(challengeResponse.answer, errors[0].unexpectedAnswer, 'unexpectedAnswer');
                    SpecHelper.expectEqual([challengeResponse.answer], errors[0].relatedAnswers, 'relatedAnswers');
                });
            });

            function setupChallengeWithExpectedAnswer(behavior) {
                // set up a model with the HasAllExpectedAnswers behavior and
                // one expected answer
                const behaviors = {};
                behaviors[behavior] = {};
                model = getModel({
                    behaviors,
                });
                challengeModel = frame.getModelsByType('ChallengeModel')[0];
                challengeModel.validator.expectedAnswerMatchers = [
                    frame.addAnswerMatcher({
                        text: 'answer text',
                    }),
                ];

                viewModel = frameViewModel.viewModelFor(model);
                challengeViewModel = frameViewModel.viewModelFor(challengeModel);
                validator = challengeViewModel.model.validator;
            }

            function itShouldRaiseIfNoExpectedAnswerMatchers(behavior) {
                it('should raise if no expected answer matchers', () => {
                    expect(() => {
                        const behaviors = {};
                        behaviors[behavior] = {};
                        viewModel = getViewModel({
                            behaviors,
                        });
                    }).toThrow(new Error(`${behavior} requires an "expectedAnswerMatchers" list.`));
                });
            }
        });
    });

    describe('EditorViewModel', () => {
        let helper;

        beforeEach(() => {
            helper = ChallengeValidatorModel.EditorViewModel.addComponentTo(frame).setup();
        });
        describe('setup', () => {
            it('should create an empty expectedAnswerMatchers array', () => {
                expect(helper.model.expectedAnswerMatchers).toEqual([]);
            });
        });
        describe('removeExpectedAnswersFor', () => {
            it('should remove matchers that match an answer', () => {
                const mockAnswerMatchers = [frame.addVanillaComponent(), frame.addVanillaComponent()];
                const mockAnswerMatcherHelpers = frame.editorViewModelsFor(mockAnswerMatchers);
                helper.model.expectedAnswerMatchers = mockAnswerMatchers;

                const mockAnswer = {};
                mockAnswerMatcherHelpers.forEach((helper, i) => {
                    // the first one returns true, the second returns false
                    helper.model.matches = jest.fn();
                    helper.model.matches.mockReturnValue(i === 0);
                    jest.spyOn(helper, 'remove').mockImplementation(() => {});
                });

                helper.removeExpectedAnswersFor(mockAnswer);
                expect(mockAnswerMatchers[0].matches).toHaveBeenCalledWith(mockAnswer);
                expect(mockAnswerMatchers[1].matches).toHaveBeenCalledWith(mockAnswer);
                expect(mockAnswerMatcherHelpers[0].remove).toHaveBeenCalled();
                expect(mockAnswerMatcherHelpers[1].remove).not.toHaveBeenCalled();
            });
        });
    });

    function getViewModel(options) {
        const model = getModel(options);
        const viewModel = frameViewModel.viewModelFor(model);
        viewModel.challengeViewModel.challengeResponses = [];
        return viewModel;
    }

    function getModel(options) {
        let model;
        frame = Componentized.fixtures.getInstance();
        frameViewModel = frame.createFrameViewModel();
        const challenge = frame.addChallenge();
        challenge.addValidator(options);
        challenge.addDefaultReferences();
        model = frame.getModelsByType('ChallengeValidatorModel')[0];

        expect(model).not.toBeUndefined();
        return model;
    }
});
