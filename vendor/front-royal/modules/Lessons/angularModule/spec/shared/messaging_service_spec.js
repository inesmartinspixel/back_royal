import 'AngularSpecHelper';
import 'Lessons/angularModule';

describe('Lesson.MessagingService', () => {
    let MessagingService;
    let SpecHelper;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                MessagingService = $injector.get('Lesson.MessagingService');
                SpecHelper = $injector.get('SpecHelper');
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('showMessage', () => {
        it('should compile and store the element', () => {
            MessagingService.showMessage({
                content: '<div>ok!</div>',
                scope: {
                    foo: 'bar',
                },
            });
            const element1 = MessagingService.getElement();
            expect(element1).not.toBeUndefined();
            expect(element1.text()).toBe('ok!');
            expect(element1.scope().foo).toBe('bar');

            const element2 = MessagingService.getElement();
            // element1 should be different from element2,
            // so check that changing the one does not affect the
            // other
            element1.empty();
            element1.scope().foo = undefined;

            expect(element2).not.toBeUndefined();
            expect(element1.text()).not.toBe('ok!');
            expect(element2.text()).toBe('ok!');
            expect(element1.scope().foo).not.toBe('bar');
            expect(element2.scope().foo).toBe('bar');
        });
        it('should remove the last element and destroy the scope when called with undefined', () => {
            MessagingService.showMessage({
                content: '<div>ok!</div>',
            });
            const element = MessagingService.getElement();
            const scope = element.scope();

            jest.spyOn(scope, '$destroy');
            // var $destroy = scope.$destroy; //have to save to local variable because $destroy replaces itself

            jest.spyOn(element, 'remove');
            MessagingService.showMessage();
            expect(element.remove).toHaveBeenCalled();
            expect(MessagingService.element).toBeUndefined();
            expect(MessagingService.scope).toBeUndefined();
        });
        it('should add correct info', () => {
            MessagingService.showMessage({
                content: '<div>ok!</div>',
                correctExists: true,
                correct: false,
            });
            expect(MessagingService.correctExists).toBe(true);
            expect(MessagingService.correct).toBe(false);
        });
        it('should update the messageId', () => {
            expect(MessagingService.messageId).toBeUndefined();
            MessagingService.showMessage({
                content: '<div>ok!</div>',
            });
            const id1 = MessagingService.messageId;
            expect(id1).not.toBeUndefined();

            MessagingService.showMessage({
                content: '<div>ok!</div>',
            });
            const id2 = MessagingService.messageId;
            expect(id2).not.toBeUndefined();
            expect(id1).not.toBe(id2);

            MessagingService.clearMessage();
            expect(MessagingService.messageId).toBeUndefined();
        });
    });

    describe('hasMessage', () => {
        it('should work', () => {
            MessagingService.showMessage({
                content: '<div>ok!</div>',
            });
            expect(MessagingService.hasMessage).toBe(true);
            MessagingService.showMessage();
            expect(MessagingService.hasMessage).toBe(false);
        });
    });

    describe('clearMessage', () => {
        it('should remove the messageInfo', () => {
            MessagingService.showMessage('message');
            MessagingService.clearMessage();
            expect(MessagingService.hasMessage).toBe(false);
        });
    });
});
