import 'AngularSpecHelper';
import 'Lessons/angularModule';

describe('Lessons.SlideMessageDirSpec', () => {
    let SpecHelper;
    let MessagingService;
    let elem;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                SpecHelper = $injector.get('SpecHelper');
                MessagingService = $injector.get('Lesson.MessagingService');
            },
        ]);

        render();
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    it('should add the appropriate correct class', () => {
        MessagingService.showMessage({
            correctExists: false,
            content: '<div></div>',
        });
        const el = elem.find('.slide-message-display');
        SpecHelper.expectElementHasClass(el, 'neutral');
        SpecHelper.expectElementDoesNotHaveClass(el, 'correct');
        SpecHelper.expectElementDoesNotHaveClass(el, 'incorrect');

        MessagingService.showMessage({
            correctExists: true,
            correct: true,
            content: '<div></div>',
        });
        SpecHelper.expectElementDoesNotHaveClass(el, 'neutral');
        SpecHelper.expectElementHasClass(el, 'correct');
        SpecHelper.expectElementDoesNotHaveClass(el, 'incorrect');

        MessagingService.showMessage({
            correctExists: true,
            correct: false,
            content: '<div></div>',
        });
        SpecHelper.expectElementDoesNotHaveClass(el, 'neutral');
        SpecHelper.expectElementDoesNotHaveClass(el, 'correct');
        SpecHelper.expectElementHasClass(el, 'incorrect');
    });

    it('should add the message', () => {
        MessagingService.showMessage({
            content: '<div id="here-i-am"></div>',
        });
        SpecHelper.expectElement(elem, '#here-i-am');

        SpecHelper.expectEqual(1, elem.children().length);
        MessagingService.clearMessage();
        SpecHelper.expectEqual(0, elem.children().length);
    });

    function render() {
        const renderer = SpecHelper.render('<slide-message></slide-message>');
        elem = renderer.elem;
    }
});
