import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';
import 'Lessons/angularModule/spec/_mock/fixtures/lessons';
import 'Playlists/angularModule/spec/_mock/fixtures/playlists';
import 'Users/angularModule/spec/_mock/fixtures/users';
import 'Lessons/angularModule/spec/_mock/fixtures/streams';

describe('Lessons.ContentAccessHelper', () => {
    let SpecHelper;
    let ContentAccessHelper;
    let contentItem;
    let contentAccessHelper;
    let user;
    let Playlist;
    let Stream;
    let Lesson;
    let $injector;
    let Cohort;
    let Period;
    let guid;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                ContentAccessHelper = $injector.get('ContentAccessHelper');
                Cohort = $injector.get('Cohort');
                guid = $injector.get('guid');

                $injector.get('CohortFixtures');
            },
        ]);

        SpecHelper.stubConfig();
        SpecHelper.stubEventLogging();

        user = SpecHelper.stubCurrentUser();
        contentItem = {
            isA: jest.fn(),
        };
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('playlistsLockedReasonKey', () => {
        it('should work if academic_hold', () => {
            user.academic_hold = true;
            expect(ContentAccessHelper.playlistsLockedReasonKey(user)).toEqual(
                'reason_message_playlist_placed_on_academic_hold',
            );
        });

        it('should work if pastDueForIdVerification', () => {
            jest.spyOn(user, 'pastDueForIdVerification', 'get').mockReturnValue(true);
            expect(ContentAccessHelper.playlistsLockedReasonKey(user)).toEqual(
                'reason_message_playlist_requires_id_verification',
            );
        });

        it('should work if there is a relevant cohort', () => {
            _.each(
                {
                    mba: 'locked_mba_message',
                    emba: 'locked_emba_message',
                    the_business_certificate: 'locked_certificates_message',
                },
                (key, programType) => {
                    user.relevant_cohort = Cohort.fixtures.getInstance({
                        program_type: programType,
                    });
                    expect(ContentAccessHelper.playlistsLockedReasonKey(user)).toEqual(key);
                },
            );
        });

        it('should work if there is no relevant cohort', () => {
            user.relevant_cohort = null;
            expect(ContentAccessHelper.playlistsLockedReasonKey(user)).toEqual('locked_mba_message');
        });

        it('should work if isRejectedOrExpelledOrDeferredOrReApplied', () => {
            jest.spyOn(user, 'isRejectedOrExpelledOrDeferredOrReApplied', 'get').mockReturnValue(true);
            _.each(
                {
                    mba: 'locked_mba_message_rejected',
                    emba: 'locked_emba_message_rejected',
                    the_business_certificate: 'locked_certificates_message_rejected',
                },
                (key, programType) => {
                    user.relevant_cohort = Cohort.fixtures.getInstance({
                        program_type: programType,
                    });
                    expect(ContentAccessHelper.playlistsLockedReasonKey(user)).toEqual(key);
                },
            );
        });
    });

    describe('canLaunch', () => {
        describe('with Playlist', () => {
            beforeEach(() => {
                Playlist = $injector.get('Playlist');
                $injector.get('PlaylistFixtures');
                contentItem = Playlist.fixtures.getInstance();
                contentAccessHelper = new ContentAccessHelper(contentItem);
            });

            it('should be false without a user', () => {
                contentAccessHelper.user = null;
                expect(contentAccessHelper.canLaunch).toBe(false);
                // messages not set right now, because not used
            });

            it('should be true for superviewers', () => {
                jest.spyOn(user, 'inGroup').mockReturnValue(true);
                expect(contentAccessHelper.canLaunch).toBe(true);
                expect(user.inGroup).toHaveBeenCalledWith('SUPERVIEWER');
            });

            it('should be false if playlist is locked', () => {
                jest.spyOn(contentAccessHelper, '_isPlaylistLocked').mockReturnValue(true);
                expect(contentAccessHelper.canLaunch).toBe(false);
                expect(contentAccessHelper._isPlaylistLocked).toHaveBeenCalled();
                // messages not set right now, because not used
            });

            it('should be false if pastDueForIdVerification', () => {
                jest.spyOn(contentAccessHelper, '_isPlaylistLocked').mockReturnValue(false);
                const spy = jest.spyOn(user, 'pastDueForIdVerification', 'get').mockReturnValue(true);
                expect(contentAccessHelper.canLaunch).toBe(false);
                expect(spy).toHaveBeenCalled();
                // messages not set right now, because not used
            });

            it('should be false if academic_hold', () => {
                jest.spyOn(contentAccessHelper, '_isPlaylistLocked').mockReturnValue(false);
                jest.spyOn(user, 'pastDueForIdVerification', 'get').mockReturnValue(false);
                user.academic_hold = true;
                expect(contentAccessHelper.canLaunch).toBe(false);
            });

            describe('_isPlaylistLocked', () => {
                beforeEach(() => {
                    user.relevant_cohort = Cohort.fixtures.getInstance();
                });

                it('should be true if no user', () => {
                    contentAccessHelper.user = null;
                    expect(contentAccessHelper._isPlaylistLocked()).toBe(true);
                });

                it("should be false if playlist is the first playlist in user's relevant_cohort", () => {
                    const playlistLocalePackId = contentItem.addLocalePackFixture().localePackId;
                    Object.defineProperty(user.relevant_cohort, 'playlistPackIds', {
                        value: [playlistLocalePackId, 'foo', 'bar', 'baz'],
                    });
                    expect(contentAccessHelper._isPlaylistLocked()).toBe(false);
                });

                it('should be false if mba content is not lockable', () => {
                    jest.spyOn(contentAccessHelper, '_mbaContentLocked').mockReturnValue(false);
                    expect(contentAccessHelper._isPlaylistLocked()).toBe(false);
                    expect(contentAccessHelper._mbaContentLocked).toHaveBeenCalled();
                });
            });
        });

        describe('with Stream', () => {
            beforeEach(() => {
                Stream = $injector.get('Lesson.Stream');
                $injector.get('StreamFixtures');
                contentItem = Stream.fixtures.getInstance();
                contentAccessHelper = new ContentAccessHelper(contentItem);
            });

            it('should be false without a user', () => {
                contentAccessHelper.user = null;
                expect(contentAccessHelper.canLaunch).toEqual(false);
                // messages not set right now, because not used
            });

            it('should be true for superviewer coming_soon', () => {
                jest.spyOn(user, 'inGroup').mockReturnValue(true);
                expect(contentAccessHelper.canLaunch).toBe(true);
                expect(user.inGroup).toHaveBeenCalledWith('SUPERVIEWER');
            });

            it('should be false if stream is locked', () => {
                jest.spyOn(contentAccessHelper, '_isStreamLocked').mockReturnValue(true);
                expect(contentAccessHelper.canLaunch).toBe(false);
                expect(contentAccessHelper._isStreamLocked).toHaveBeenCalled();
                expect(contentAccessHelper.reasonTitle).toEqual('Locked Course');
                expect(contentAccessHelper.reasonMessage).toEqual(
                    'This course is available as part of our MBA program.',
                );
            });

            it('should be false if coming_soon', () => {
                jest.spyOn(contentAccessHelper, '_isStreamLocked').mockReturnValue(false);
                contentItem.coming_soon = true;
                expect(contentAccessHelper.canLaunch).toEqual(false);
                // messages not set right now, because not used
            });

            it('should be false when !_hasCompletedPrerequisitesWhenSchedule', () => {
                jest.spyOn(contentAccessHelper, '_isStreamLocked').mockReturnValue(false);
                jest.spyOn(contentAccessHelper, '_hasCompletedPrerequisitesWhenSchedule').mockReturnValue(false);
                expect(contentAccessHelper.canLaunch).toBe(false);
                expect(contentAccessHelper._hasCompletedPrerequisitesWhenSchedule).toHaveBeenCalled();
                expect(contentAccessHelper.reasonTitle).toEqual('Exam Locked');
                expect(contentAccessHelper.reasonMessage).toEqual(
                    'To unlock this exam, complete all prior required courses in your class schedule.',
                );
            });

            it('should be true when !_hasCompletedPrerequisitesWhenSchedule and stream is complete', () => {
                jest.spyOn(contentAccessHelper, '_isStreamLocked').mockReturnValue(false);
                jest.spyOn(contentAccessHelper, '_hasCompletedPrerequisitesWhenSchedule').mockReturnValue(false);
                jest.spyOn(contentAccessHelper.contentItem, 'complete', 'get').mockReturnValue(true);
                expect(contentAccessHelper.canLaunch).toBe(true);
            });

            it('should be false when !_hasCompletedPrerequisitesWhenPlaylist', () => {
                jest.spyOn(contentAccessHelper, '_isStreamLocked').mockReturnValue(false);
                jest.spyOn(contentAccessHelper, '_hasCompletedPrerequisitesWhenPlaylist').mockReturnValue(false);
                expect(contentAccessHelper.canLaunch).toBe(false);
                expect(contentAccessHelper._hasCompletedPrerequisitesWhenPlaylist).toHaveBeenCalled();
                expect(contentAccessHelper.reasonTitle).toEqual('Exam Locked');
                expect(contentAccessHelper.reasonMessage).toEqual(
                    'To unlock this exam, complete all prior courses in the appropriate concentration.',
                );
            });

            it('should be true when !_hasCompletedPrerequisitesWhenPlaylist and stream is complete', () => {
                jest.spyOn(contentAccessHelper, '_isStreamLocked').mockReturnValue(false);
                jest.spyOn(contentAccessHelper, '_hasCompletedPrerequisitesWhenPlaylist').mockReturnValue(false);
                jest.spyOn(contentAccessHelper.contentItem, 'complete', 'get').mockReturnValue(true);
                expect(contentAccessHelper.canLaunch).toBe(true);
            });

            it('should be false beforeLaunchWindow', () => {
                jest.spyOn(contentAccessHelper, '_isStreamLocked').mockReturnValue(false);
                jest.spyOn(contentAccessHelper, '_hasCompletedPrerequisites').mockReturnValue(true);
                const spy = jest.spyOn(contentAccessHelper.contentItem, 'beforeLaunchWindow').mockReturnValue(true);
                jest.spyOn(contentAccessHelper.contentItem, 'examOpenTime').mockReturnValue(
                    new Date('2016/01/01 12:00'),
                );
                expect(contentAccessHelper.canLaunch).toBe(false);
                expect(spy).toHaveBeenCalled();
                expect(contentAccessHelper.reasonTitle).toEqual('Exam Locked');
                expect(contentAccessHelper.reasonMessage).toEqual(
                    'This exam will become available on January 1, 2016.',
                );
            });

            it('should be true when beforeLaunchWindow stream is complete', () => {
                jest.spyOn(contentAccessHelper, '_isStreamLocked').mockReturnValue(false);
                jest.spyOn(contentAccessHelper, '_hasCompletedPrerequisites').mockReturnValue(true);
                jest.spyOn(contentAccessHelper.contentItem, 'beforeLaunchWindow').mockReturnValue(true);
                jest.spyOn(contentAccessHelper.contentItem, 'complete', 'get').mockReturnValue(true);
                expect(contentAccessHelper.canLaunch).toBe(true);
            });

            it('should be true even if exam time limit is up', () => {
                // You can open a stream dashboard when the exam time limit is up, but
                // cannot open the lessons within it (see in canLaunch with Lesson)
                jest.spyOn(contentItem, 'msLeftInTimeLimit', 'get').mockReturnValue(0);
                expect(contentAccessHelper.canLaunch).toBe(true);
            });

            it('should be false if pastDueForIdVerification', () => {
                jest.spyOn(contentAccessHelper, '_isStreamLocked').mockReturnValue(false);
                jest.spyOn(contentAccessHelper, '_hasCompletedPrerequisites').mockReturnValue(true);
                const spy = jest.spyOn(user, 'pastDueForIdVerification', 'get').mockReturnValue(true);
                expect(contentAccessHelper.canLaunch).toBe(false);
                expect(spy).toHaveBeenCalled();
                expect(contentAccessHelper.reasonTitle).toEqual('Course Locked');
                expect(contentAccessHelper.reasonMessage).toEqual('To unlock, complete your identity verification.');
            });

            it('should be false if academic_hold', () => {
                jest.spyOn(contentAccessHelper, '_isStreamLocked').mockReturnValue(false);
                jest.spyOn(contentAccessHelper, '_hasCompletedPrerequisites').mockReturnValue(true);
                user.academic_hold = true;
                expect(contentAccessHelper.canLaunch).toBe(false);
                expect(contentAccessHelper.reasonTitle).toEqual('Course Locked');
                expect(contentAccessHelper.reasonMessage).toEqual(
                    'You have been placed on an Academic Hold. Please contact <a href="mailto:support@quantic.edu">support@quantic.edu</a> for more information.',
                );
            });

            it('should be true if there is no reason to be false', () => {
                jest.spyOn(contentAccessHelper, '_isStreamLocked').mockReturnValue(false);
                jest.spyOn(contentAccessHelper, '_hasCompletedPrerequisites').mockReturnValue(true);
                expect(contentAccessHelper.canLaunch).toBe(true);
            });

            describe('reason title and message when beforeLaunchWindow', () => {
                let spy;
                beforeEach(() => {
                    spy = jest.spyOn(contentAccessHelper.contentItem, 'beforeLaunchWindow').mockReturnValue(true);
                });

                describe('and _hasCompletedPrerequisitesWhenSchedule returns false', () => {
                    it('should be as expected', () => {
                        jest.spyOn(contentAccessHelper, '_isStreamLocked').mockReturnValue(false);
                        jest.spyOn(contentAccessHelper.contentItem, 'examOpenTime').mockReturnValue(
                            new Date('2016/01/01 12:00'),
                        );
                        jest.spyOn(contentAccessHelper, '_hasCompletedPrerequisitesWhenSchedule').mockReturnValue(
                            false,
                        );
                        expect(contentAccessHelper.canLaunch).toBe(false);
                        expect(spy).toHaveBeenCalled();
                        expect(contentAccessHelper.reasonTitle).toEqual('Exam Locked');
                        expect(contentAccessHelper.reasonMessage).toEqual(
                            'This exam will become available on January 1, 2016 once all prior required courses in your class schedule are completed.',
                        );
                    });
                });

                describe('and _hasCompletedPrerequisitesWhenPlaylist returns false', () => {
                    it('should be as expected', () => {
                        jest.spyOn(contentAccessHelper, '_isStreamLocked').mockReturnValue(false);
                        jest.spyOn(contentAccessHelper.contentItem, 'examOpenTime').mockReturnValue(
                            new Date('2016/01/01 12:00'),
                        );
                        jest.spyOn(contentAccessHelper, '_hasCompletedPrerequisitesWhenSchedule').mockReturnValue(true);
                        jest.spyOn(contentAccessHelper, '_hasCompletedPrerequisitesWhenPlaylist').mockReturnValue(
                            false,
                        );
                        expect(contentAccessHelper.canLaunch).toBe(false);
                        expect(spy).toHaveBeenCalled();
                        expect(contentAccessHelper.reasonTitle).toEqual('Exam Locked');
                        expect(contentAccessHelper.reasonMessage).toEqual(
                            'This exam will become available on January 1, 2016 once all prior courses in the appropriate concentration are completed.',
                        );
                    });
                });
            });

            describe('_isStreamLocked', () => {
                it('should be true if no user', () => {
                    contentAccessHelper.user = null;
                    expect(contentAccessHelper._isStreamLocked()).toBe(true);
                });

                it('should be false if unlocked_for_mba_users', () => {
                    jest.spyOn(contentAccessHelper, '_mbaContentLocked').mockReturnValue(true);
                    contentItem.unlocked_for_mba_users = true;
                    expect(contentAccessHelper._isStreamLocked()).toBe(false);
                });

                it("should be false if stream locale pack id is associated with one of the user's access groups", () => {
                    jest.spyOn(contentAccessHelper, '_mbaContentLocked').mockReturnValue(true);
                    contentItem.unlocked_for_mba_users = false;
                    jest.spyOn(user, 'streamIsAssociatedWithAccessGroup').mockReturnValue(true);
                    expect(contentAccessHelper._isStreamLocked()).toBe(false);
                });

                it('should be false if mba content is not lockable', () => {
                    jest.spyOn(contentAccessHelper, '_mbaContentLocked').mockReturnValue(false);
                    expect(contentAccessHelper._isStreamLocked()).toBe(false);
                    expect(contentAccessHelper._mbaContentLocked).toHaveBeenCalled();
                });
            });
        });

        describe('with Lesson', () => {
            beforeEach(() => {
                Lesson = $injector.get('Lesson');
                $injector.get('LessonFixtures');
                contentItem = Lesson.fixtures.getInstance();
                contentAccessHelper = new ContentAccessHelper(contentItem);
            });

            it('should delegate to stream if that cannot be launched', () => {
                const User = $injector.get('User');
                $injector.get('UserFixtures');
                // set to something other than currentUser to ensure it is
                // passed through.
                contentAccessHelper.user = User.fixtures.getInstance();
                Stream = $injector.get('Lesson.Stream');
                const stream = Stream.fixtures.getInstance({
                    // make it unlaunchable.  Would be nice to
                    // find a way to mock canLaunch directly, but
                    // it seems like a lot of trouble
                    coming_soon: true,
                });

                jest.spyOn(contentItem, 'stream').mockReturnValue(stream);

                expect(contentAccessHelper.canLaunch).toBe(false);
                expect(contentAccessHelper._delegateReasonTo).not.toBeUndefined();
                expect(contentAccessHelper._delegateReasonTo.contentItem).toEqual(stream);
                expect(contentAccessHelper._delegateReasonTo.user).toEqual(contentAccessHelper.user);
            });

            it('should not delegate to stream if that can be launched', () => {
                const User = $injector.get('User');
                $injector.get('UserFixtures');
                // set to something other than currentUser to ensure it is
                // passed through.
                contentAccessHelper.user = User.fixtures.getInstance();
                Stream = $injector.get('Lesson.Stream');

                // this stream should (hopefully) be launchable,
                // but it's hard to mock that directly
                const stream = Stream.fixtures.getInstance({});

                jest.spyOn(contentItem, 'stream').mockReturnValue(stream);

                expect(contentAccessHelper.canLaunch).not.toBeUndefined();
                expect(contentAccessHelper._delegateReasonTo).toBeUndefined();
            });

            it('should be true for superviewers', () => {
                jest.spyOn(user, 'inGroup').mockReturnValue(true);
                expect(contentAccessHelper.canLaunch).toBe(true);
                expect(user.inGroup).toHaveBeenCalledWith('SUPERVIEWER');
            });

            it('should be false if coming soon', () => {
                const spy = jest.spyOn(contentItem, 'comingSoon', 'get').mockReturnValue(true);
                expect(contentAccessHelper.canLaunch).toBe(false);
                // messages not set right now, because not used
                expect(spy).toHaveBeenCalled();
            });

            it('should be true if unrestricted', () => {
                contentItem.unrestricted = true;
                expect(contentAccessHelper.canLaunch).toBe(true);
            });

            it('should be false if no user', () => {
                contentAccessHelper.user = null;
                expect(contentAccessHelper.canLaunch).toBe(false);
                expect(contentAccessHelper.reasonTitle).toEqual('Lesson Locked');
                expect(contentAccessHelper.reasonMessage).toEqual(
                    'This lesson is available as part of our MBA program.',
                );
            });

            it('should be false if test lesson completed', () => {
                const spy = jest.spyOn(contentItem, 'completedTest', 'get').mockReturnValue(true);
                expect(contentAccessHelper.canLaunch).toBe(false);
                expect(contentAccessHelper.reasonTitle).toEqual('Exam Locked');
                expect(contentAccessHelper.reasonMessage).toEqual('This exam is now closed.');
                expect(spy).toHaveBeenCalled();
            });

            it('should be false if exam time limit is up', () => {
                const spy = jest.spyOn(contentItem.stream(), 'msLeftInTimeLimit', 'get').mockReturnValue(0);
                expect(contentAccessHelper.canLaunch).toBe(false);
                expect(contentAccessHelper.reasonTitle).toEqual('Exam Locked');
                expect(contentAccessHelper.reasonMessage).toEqual('This exam is now closed.');
                expect(spy).toHaveBeenCalled();
            });

            it('should be true if no reason to be false', () => {
                expect(contentAccessHelper.canLaunch).toBe(true);
            });
        });
    });

    describe('launchText', () => {
        beforeEach(() => {
            Stream = $injector.get('Lesson.Stream');
            $injector.get('StreamFixtures');
            contentItem = Stream.fixtures.getInstance();
            contentAccessHelper = new ContentAccessHelper(contentItem);
        });

        it('should delegate to contentItem if canLaunch and !completedExam', () => {
            jest.spyOn(contentItem, 'launchText', 'get').mockReturnValue('launchText');
            jest.spyOn(contentItem, 'exam', 'get').mockReturnValue(true);
            jest.spyOn(contentItem, 'complete', 'get').mockReturnValue(false);
            jest.spyOn(contentAccessHelper, 'canLaunch', 'get').mockReturnValue(true);
            expect(contentAccessHelper.launchText).toEqual('launchText');
        });

        it('should say completed if completedExam', () => {
            jest.spyOn(contentItem, 'launchText', 'get').mockReturnValue('launchText');
            jest.spyOn(contentItem, 'exam', 'get').mockReturnValue(true);
            jest.spyOn(contentItem, 'complete', 'get').mockReturnValue(true);
            jest.spyOn(contentAccessHelper, 'canLaunch', 'get').mockReturnValue(true);
            expect(contentAccessHelper.launchText).toEqual('COMPLETED');
        });

        it('should say locked if cannot launch', () => {
            jest.spyOn(contentAccessHelper, 'canLaunch', 'get').mockReturnValue(false);
            expect(contentAccessHelper.launchText).toEqual('LOCKED');
        });
    });

    describe('reasonMessage', () => {
        beforeEach(() => {
            contentAccessHelper = new ContentAccessHelper(contentItem);
            jest.spyOn(contentAccessHelper, 'canLaunch', 'get').mockReturnValue(false);
        });

        it('should work with _delegateReasonTo', () => {
            contentAccessHelper._delegateReasonTo = new ContentAccessHelper({
                isA: jest.fn(),
            });
            jest.spyOn(contentAccessHelper._delegateReasonTo, 'reasonMessage', 'get').mockReturnValue('delegated');
            expect(contentAccessHelper.reasonMessage).toEqual('delegated');
        });

        it('should call _getReasonText', () => {
            const spy = jest.spyOn(contentAccessHelper, '_getReasonText').mockReturnValue('text');
            expect(contentAccessHelper.reasonMessage).toEqual('text');
            expect(spy).toHaveBeenCalledWith('message');
        });
    });

    describe('reasonTitle', () => {
        beforeEach(() => {
            contentAccessHelper = new ContentAccessHelper(contentItem);
            jest.spyOn(contentAccessHelper, 'canLaunch', 'get').mockReturnValue(false);
        });

        it('should work with _delegateReasonTo', () => {
            contentAccessHelper._delegateReasonTo = new ContentAccessHelper({
                isA: jest.fn(),
            });
            jest.spyOn(contentAccessHelper._delegateReasonTo, 'reasonTitle', 'get').mockReturnValue('delegated');
            expect(contentAccessHelper.reasonTitle).toEqual('delegated');
        });

        it('should call _getReasonText', () => {
            const spy = jest.spyOn(contentAccessHelper, '_getReasonText').mockReturnValue('text');
            expect(contentAccessHelper.reasonTitle).toEqual('text');
            expect(spy).toHaveBeenCalledWith('title');
        });
    });

    describe('reason', () => {
        beforeEach(() => {
            contentAccessHelper = new ContentAccessHelper(contentItem);
            jest.spyOn(contentAccessHelper, 'canLaunch', 'get').mockReturnValue(false);
        });

        it('should work with _delegateReasonTo', () => {
            contentAccessHelper._delegateReasonTo = new ContentAccessHelper({
                isA: jest.fn(),
            });
            jest.spyOn(contentAccessHelper._delegateReasonTo, 'reason', 'get').mockReturnValue('delegated');
            expect(contentAccessHelper.reason).toEqual('delegated');
        });
    });

    describe('_getReasonText', () => {
        beforeEach(() => {
            contentAccessHelper = new ContentAccessHelper(contentItem);
            contentAccessHelper.contentType = 'content';
            jest.spyOn(contentAccessHelper.translationHelper, 'getWithFallbackKey').mockReturnValue('translated');
        });

        it('should notify and fallback to the default if there is no reason defined', () => {
            contentAccessHelper._reason = null;
            expect(contentAccessHelper._getReasonText('type')).toEqual('translated');
            const calledWithKeys = contentAccessHelper.translationHelper.getWithFallbackKey.mock.calls[0].slice(0, 2);
            expect(calledWithKeys).toEqual(['reason_type_content_default', 'reason_type_content_default']);
        });

        it('should work if there is a reason defined', () => {
            contentAccessHelper._reason = 'reason';
            expect(contentAccessHelper._getReasonText('type')).toEqual('translated');
            const calledWithKeys = contentAccessHelper.translationHelper.getWithFallbackKey.mock.calls[0].slice(0, 2);
            expect(calledWithKeys).toEqual(['reason_type_content_reason', 'reason_type_content_default']);
        });

        it('should add openTimeStr', () => {
            const $filter = $injector.get('$filter');
            const openTime = new Date();
            contentAccessHelper.contentItem.examOpenTime = jest.fn().mockReturnValue(openTime);
            expect(contentAccessHelper._getReasonText('type')).toEqual('translated');
            const interpolationParams = contentAccessHelper.translationHelper.getWithFallbackKey.mock.calls[0][2];
            expect(interpolationParams.openTimeStr).toEqual($filter('amDateFormat')(openTime, 'MMMM D, YYYY'));
        });
    });

    describe('showModalIfCannotLaunch', () => {
        it('should show modal if cannot launch', () => {
            contentAccessHelper = new ContentAccessHelper(contentItem);
            jest.spyOn(contentAccessHelper, 'canLaunch', 'get').mockReturnValue(false);
            jest.spyOn(contentAccessHelper, 'reasonMessage', 'get').mockReturnValue('reason');
            jest.spyOn(contentAccessHelper, 'reasonTitle', 'get').mockReturnValue('title');
            contentAccessHelper.showModalIfCannotLaunch();
            $injector.get('$timeout').flush(); // show the modal
            SpecHelper.expectElementText($('html'), '.modal .message', 'reason');
            SpecHelper.expectElementText($('html'), '.modal .modal-action-button', 'title');
            SpecHelper.click($('html'), '.modal .modal-action-button');
            SpecHelper.expectNoElement($('html'), '.modal');
        });

        it('should do nothing if canLaunch', () => {
            const DialogModal = $injector.get('DialogModal');
            jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
            contentAccessHelper = new ContentAccessHelper(contentItem);
            jest.spyOn(contentAccessHelper, 'canLaunch', 'get').mockReturnValue(true);
            contentAccessHelper.showModalIfCannotLaunch();
            expect(DialogModal.alert).not.toHaveBeenCalled();
        });
    });

    describe('showModalIfPrereqsNotMet', () => {
        beforeEach(() => {
            contentAccessHelper = new ContentAccessHelper(contentItem);
            jest.spyOn(contentAccessHelper, 'showModalIfCannotLaunch').mockImplementation(() => {});
        });

        it('should show modal if prereqs not met', () => {
            jest.spyOn(contentAccessHelper, '_hasCompletedPrerequisites').mockReturnValue(false);
            contentAccessHelper.showModalIfPrereqsNotMet();
            expect(contentAccessHelper.showModalIfCannotLaunch).toHaveBeenCalled();
        });

        it('should do nothing if prereqs not met', () => {
            jest.spyOn(contentAccessHelper, '_hasCompletedPrerequisites').mockReturnValue(true);
            contentAccessHelper.showModalIfPrereqsNotMet();
            expect(contentAccessHelper.showModalIfCannotLaunch).not.toHaveBeenCalled();
        });
    });

    describe('_mbaContentLocked', () => {
        beforeEach(() => {
            contentAccessHelper = new ContentAccessHelper(contentItem);
        });

        it('should be true if no user', () => {
            contentAccessHelper.user = null;
            expect(contentAccessHelper._mbaContentLocked()).toBe(true);
        });

        it('should be true if user is lockedDueToPastDuePayment', () => {
            jest.spyOn(user, 'lockedDueToPastDuePayment', 'get').mockReturnValue(true);
            expect(contentAccessHelper._mbaContentLocked()).toBe(true);
        });

        it('should be false if not mba_content_lockable for MBA cohort', () => {
            Object.defineProperty(contentAccessHelper.user, 'programType', {
                value: 'mba',
                configurable: true,
            });
            contentAccessHelper.user.relevant_cohort = {};
            contentAccessHelper.user.mba_content_lockable = false;
            expect(contentAccessHelper._mbaContentLocked()).toBe(false);
        });

        it('should be false if not mba_content_lockable for no relevant cohort', () => {
            Object.defineProperty(contentAccessHelper.user, 'programType', {
                value: 'mba',
                configurable: true,
            });
            contentAccessHelper.user.relevant_cohort = undefined;
            contentAccessHelper.user.mba_content_lockable = false;
            expect(contentAccessHelper._mbaContentLocked()).toBe(false);
        });

        it('should be false if not mba_content_lockable for demo user', () => {
            Object.defineProperty(contentAccessHelper.user, 'programType', {
                value: 'demo',
                configurable: true,
            });
            contentAccessHelper.user.relevant_cohort = {};
            contentAccessHelper.user.mba_content_lockable = false;
            expect(contentAccessHelper._mbaContentLocked()).toBe(false);
        });

        it('should be true if not mba_content_lockable for certificate cohorts', () => {
            _.each(Cohort.CERTIFICATE_PROGRAM_TYPES, programType => {
                Object.defineProperty(contentAccessHelper.user, 'programType', {
                    value: programType,
                    configurable: true,
                });
                contentAccessHelper.user.relevant_cohort = {};
                contentAccessHelper.user.mba_content_lockable = false;
                expect(contentAccessHelper._mbaContentLocked()).toBe(true);
            });
        });

        it('should be false if acceptedCohortApplication', () => {
            contentAccessHelper.user.mba_content_lockable = true;
            const spy = jest.spyOn(contentAccessHelper.user, 'acceptedCohortApplication', 'get').mockReturnValue({});
            expect(contentAccessHelper._mbaContentLocked()).toBe(false);
            expect(spy).toHaveBeenCalled();
        });

        it('should be true if no reason to be false', () => {
            contentAccessHelper.user.mba_content_lockable = true;
            jest.spyOn(contentAccessHelper.user, 'acceptedCohortApplication', 'get').mockReturnValue(null);
            expect(contentAccessHelper._mbaContentLocked()).toBe(true);
        });
    });

    describe('prerequisites', () => {
        beforeEach(() => {
            Playlist = $injector.get('Playlist');
            Stream = $injector.get('Lesson.Stream');
            Period = $injector.get('Period');

            $injector.get('PlaylistFixtures');
            $injector.get('StreamFixtures');

            contentItem = Stream.fixtures.getInstance();
            contentAccessHelper = new ContentAccessHelper(contentItem);
        });

        describe('_hasCompletedPrerequisitesWhenPlaylist', () => {
            it('should be true if playlist.getCachedForLocalePackId returns null for a playlist', () => {
                contentItem.exam = true;
                jest.spyOn(contentAccessHelper, '_shouldCheckPrerequisites').mockReturnValue(true);

                const requiredStream = Stream.fixtures.getInstance();
                jest.spyOn(requiredStream, 'complete', 'get').mockReturnValue(true);

                const requiredPlaylist = Playlist.fixtures.getInstance({
                    stream_entries: [
                        {
                            locale_pack_id: requiredStream.localePackId,
                        },
                        {
                            locale_pack_id: contentItem.localePackId,
                        },
                    ],
                });
                requiredPlaylist.ensureLocalePack();

                user.relevant_cohort = Cohort.new({
                    playlists: [requiredPlaylist, Playlist.fixtures.getInstance()],
                });

                expect(contentAccessHelper._hasCompletedPrerequisitesWhenPlaylist('concentration')).toBe(true);

                // Reset cache so playlist.getCachedForLocalePackId returns null
                // Expect this does not impact return value
                // Expect this does not throw
                Playlist.resetCache();
                expect(contentAccessHelper._hasCompletedPrerequisitesWhenPlaylist('concentration')).toBe(true);
                expect(() => {
                    contentAccessHelper._hasCompletedPrerequisitesWhenPlaylist('concentration');
                }).not.toThrowError();
            });

            it('should be true if all required streams are complete', () => {
                contentItem.exam = true;
                jest.spyOn(contentAccessHelper, '_shouldCheckPrerequisites').mockReturnValue(true);

                const requiredStream = Stream.fixtures.getInstance();
                jest.spyOn(requiredStream, 'complete', 'get').mockReturnValue(true);

                const requiredPlaylist = Playlist.fixtures.getInstance({
                    stream_entries: [
                        {
                            locale_pack_id: requiredStream.localePackId,
                        },
                        {
                            locale_pack_id: contentItem.localePackId,
                        },
                    ],
                });

                user.relevant_cohort = Cohort.fixtures.getInstance({
                    playlists: [requiredPlaylist, Playlist.fixtures.getInstance()],
                });
                jest.spyOn(contentAccessHelper, '_hasCompletedAllStreamsAndStartedExams').mockReturnValue(true);

                expect(contentAccessHelper._hasCompletedPrerequisitesWhenPlaylist('concentration')).toBe(true);
                expect(contentAccessHelper._hasCompletedAllStreamsAndStartedExams).toHaveBeenCalledWith([
                    requiredStream.localePackId,
                ]);
            });

            it('should be true if all specialization streams are complete', () => {
                contentItem.exam = true;
                jest.spyOn(contentAccessHelper, '_shouldCheckPrerequisites').mockReturnValue(true);

                const requiredStream = Stream.fixtures.getInstance();
                jest.spyOn(requiredStream, 'complete', 'get').mockReturnValue(true);

                const specializationPlaylist = Playlist.fixtures.getInstance({
                    stream_entries: [
                        {
                            locale_pack_id: requiredStream.localePackId,
                        },
                        {
                            locale_pack_id: contentItem.localePackId,
                        },
                    ],
                });

                user.relevant_cohort = Cohort.new({
                    specialization_playlist_pack_ids: [
                        specializationPlaylist.localePackId,
                        Playlist.fixtures.getInstance().localePackId,
                    ],
                });

                jest.spyOn(contentAccessHelper, '_hasCompletedAllStreamsAndStartedExams').mockReturnValue(true);

                expect(contentAccessHelper._hasCompletedPrerequisitesWhenPlaylist('specialization')).toBe(true);
                expect(contentAccessHelper._hasCompletedAllStreamsAndStartedExams).toHaveBeenCalledWith([
                    requiredStream.localePackId,
                ]);
            });

            it('should be false if some required streams are not complete', () => {
                contentItem.exam = true;
                jest.spyOn(contentAccessHelper, '_shouldCheckPrerequisites').mockReturnValue(true);

                const requiredStreamLocalePackId = guid.generate();
                const specializationPlaylist = Playlist.fixtures.getInstance({
                    stream_entries: [
                        {
                            locale_pack_id: requiredStreamLocalePackId,
                        },
                        {
                            locale_pack_id: contentItem.localePackId,
                        },
                    ],
                });

                user.relevant_cohort = Cohort.new({
                    specialization_playlist_pack_ids: [
                        specializationPlaylist.localePackId,
                        Playlist.fixtures.getInstance().localePackId,
                    ],
                });

                jest.spyOn(contentAccessHelper, '_hasCompletedAllStreamsAndStartedExams').mockReturnValue(false);

                expect(contentAccessHelper._hasCompletedPrerequisitesWhenPlaylist()).toBe(false);
                expect(contentAccessHelper._hasCompletedAllStreamsAndStartedExams).toHaveBeenCalledWith([
                    requiredStreamLocalePackId,
                ]);
            });
        });

        describe('_hasCompletedPrerequisitesWhenSchedule', () => {
            it('should be true if all required streams are complete', () => {
                contentItem.exam = true;
                jest.spyOn(contentAccessHelper, '_shouldCheckPrerequisites').mockReturnValue(true);

                const requiredStream = Stream.fixtures.getInstance();
                const anotherStream = Stream.fixtures.getInstance();
                const requiredPeriod = Period.new({
                    stream_entries: [
                        {
                            required: true,
                            locale_pack_id: requiredStream.localePackId,
                        },
                    ],
                });

                const examPeriod = Period.new({
                    stream_entries: [
                        {
                            required: true,
                            locale_pack_id: contentItem.localePackId,
                        },
                    ],
                });

                const anotherPeriod = Period.new({
                    stream_entries: [
                        {
                            required: true,
                            locale_pack_id: anotherStream.localePackId,
                        },
                    ],
                });

                user.relevant_cohort = Cohort.new({
                    periods: [requiredPeriod, examPeriod, anotherPeriod], // examPeriod at index 1
                });
                jest.spyOn(user.relevant_cohort, 'supportsSchedule', 'get').mockReturnValue(true);
                jest.spyOn(user.relevant_cohort, 'getRequiredStreamPackIdsFromPeriods').mockReturnValue([
                    requiredStream.localePackId,
                ]);
                jest.spyOn(contentAccessHelper, '_hasCompletedAllStreamsAndStartedExams').mockReturnValue(true);

                expect(contentAccessHelper._hasCompletedPrerequisitesWhenSchedule()).toBe(true);
                // examPeriod is at index 1 in the periods array and getRequiredStreamPackIdsFromPeriods accepts an array index
                // as an argument, but we don't want to include the examPeriod in the calculation, so we subtract 1 from it.
                expect(user.relevant_cohort.getRequiredStreamPackIdsFromPeriods).toHaveBeenCalledWith(0);
                expect(contentAccessHelper._hasCompletedAllStreamsAndStartedExams).toHaveBeenCalledWith([
                    requiredStream.localePackId,
                ]);
            });

            it('should be false if some required streams are not complete', () => {
                contentItem.exam = true;
                jest.spyOn(contentAccessHelper, '_shouldCheckPrerequisites').mockReturnValue(true);

                const requiredStream = Stream.fixtures.getInstance();
                const requiredPeriod = Period.new({
                    stream_entries: [
                        {
                            required: true,
                            locale_pack_id: requiredStream.localePackId,
                        },
                    ],
                });

                const examPeriod = Period.new({
                    stream_entries: [
                        {
                            required: true,
                            locale_pack_id: contentItem.localePackId,
                        },
                    ],
                });

                user.relevant_cohort = Cohort.new({
                    periods: [requiredPeriod, examPeriod], // examPeriod at index 1
                });
                jest.spyOn(user.relevant_cohort, 'supportsSchedule', 'get').mockReturnValue(true);
                jest.spyOn(user.relevant_cohort, 'getRequiredStreamPackIdsFromPeriods').mockReturnValue([
                    requiredStream.localePackId,
                ]);
                jest.spyOn(contentAccessHelper, '_hasCompletedAllStreamsAndStartedExams').mockReturnValue(false);

                expect(contentAccessHelper._hasCompletedPrerequisites()).toBe(false);
                // examPeriod is at index 1 in the periods array and getRequiredStreamPackIdsFromPeriods accepts an array index
                // as an argument, but we don't want to include the examPeriod in the calculation, so we subtract 1 from it.
                expect(user.relevant_cohort.getRequiredStreamPackIdsFromPeriods).toHaveBeenCalledWith(0);
                expect(contentAccessHelper._hasCompletedAllStreamsAndStartedExams).toHaveBeenCalledWith([
                    requiredStream.localePackId,
                ]);
            });
        });

        describe('_shouldCheckPrerequisites', () => {
            it('should be true if no user', () => {
                contentAccessHelper.user = null;
                expect(contentAccessHelper._hasCompletedPrerequisites()).toBe(true);
            });

            it('should be true if no relevant cohort', () => {
                user.relevant_cohort = null;
                expect(contentAccessHelper._hasCompletedPrerequisites()).toBe(true);
            });

            it('should be true if not an exam', () => {
                user.relevant_cohort = {};
                contentItem.exam = false;
                expect(contentAccessHelper._hasCompletedPrerequisites()).toBe(true);
            });

            it('should be true if disable_exam_locking', () => {
                user.relevant_cohort = {};
                contentItem.exam = true;
                const spy = jest.spyOn(user, 'acceptedCohortApplication', 'get').mockReturnValue({
                    disable_exam_locking: true,
                });
                expect(contentAccessHelper._hasCompletedPrerequisites()).toBe(true);
                expect(spy).toHaveBeenCalled();
            });
        });
    });

    describe('_hasCompletedAllStreamsAndStartedExams', () => {
        let streams;

        beforeEach(() => {
            Stream = $injector.get('Lesson.Stream');
            $injector.get('StreamFixtures');
            contentAccessHelper = new ContentAccessHelper();
            streams = [Stream.fixtures.getInstance(), Stream.fixtures.getInstance(), Stream.fixtures.getInstance()];
        });

        it('should return false if not all streams have been completed', () => {
            jest.spyOn(streams[0], 'complete', 'get').mockReturnValue(true);
            jest.spyOn(streams[1], 'complete', 'get').mockReturnValue(true);
            jest.spyOn(streams[2], 'complete', 'get').mockReturnValue(false); // incomplete stream
            jest.spyOn(Stream, 'getCachedForLocalePackId')
                .mockReturnValueOnce(streams[0])
                .mockReturnValueOnce(streams[1])
                .mockReturnValueOnce(streams[2]);
            expect(contentAccessHelper._hasCompletedAllStreamsAndStartedExams(_.pluck(streams, 'localePackId'))).toBe(
                false,
            );
        });

        it('should return true if all streams have been completed', () => {
            jest.spyOn(streams[0], 'complete', 'get').mockReturnValue(true);
            jest.spyOn(streams[1], 'complete', 'get').mockReturnValue(true);
            jest.spyOn(streams[2], 'complete', 'get').mockReturnValue(true);
            jest.spyOn(Stream, 'getCachedForLocalePackId')
                .mockReturnValueOnce(streams[0])
                .mockReturnValueOnce(streams[1])
                .mockReturnValueOnce(streams[2]);
            expect(contentAccessHelper._hasCompletedAllStreamsAndStartedExams(_.pluck(streams, 'localePackId'))).toBe(
                true,
            );
        });

        it('should return false if not all exam streams have been started', () => {
            jest.spyOn(streams[0], 'exam', 'get').mockReturnValue(true);
            jest.spyOn(streams[0], 'notStarted', 'get').mockReturnValue(false);
            jest.spyOn(streams[1], 'exam', 'get').mockReturnValue(true);
            jest.spyOn(streams[1], 'notStarted', 'get').mockReturnValue(false);
            jest.spyOn(streams[2], 'exam', 'get').mockReturnValue(true);
            jest.spyOn(streams[2], 'notStarted', 'get').mockReturnValue(true); // not started exam stream
            jest.spyOn(Stream, 'getCachedForLocalePackId')
                .mockReturnValueOnce(streams[0])
                .mockReturnValueOnce(streams[1])
                .mockReturnValueOnce(streams[2]);
            expect(contentAccessHelper._hasCompletedAllStreamsAndStartedExams(_.pluck(streams, 'localePackId'))).toBe(
                false,
            );
        });

        it('should return true if all exam streams have been started', () => {
            jest.spyOn(streams[0], 'exam', 'get').mockReturnValue(true);
            jest.spyOn(streams[0], 'notStarted', 'get').mockReturnValue(false);
            jest.spyOn(streams[1], 'exam', 'get').mockReturnValue(true);
            jest.spyOn(streams[1], 'notStarted', 'get').mockReturnValue(false);
            jest.spyOn(streams[2], 'exam', 'get').mockReturnValue(true);
            jest.spyOn(streams[2], 'notStarted', 'get').mockReturnValue(false);
            jest.spyOn(Stream, 'getCachedForLocalePackId')
                .mockReturnValueOnce(streams[0])
                .mockReturnValueOnce(streams[1])
                .mockReturnValueOnce(streams[2]);
            expect(contentAccessHelper._hasCompletedAllStreamsAndStartedExams(_.pluck(streams, 'localePackId'))).toBe(
                true,
            );
        });
    });
});
