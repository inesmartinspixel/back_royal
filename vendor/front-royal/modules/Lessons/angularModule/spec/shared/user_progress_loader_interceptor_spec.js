import 'AngularSpecHelper';
import 'Lessons/angularModule';

describe('UserProgressLoaderInterceptor', () => {
    let SpecHelper;
    let UserProgressLoaderInterceptor;
    let currentUser;

    beforeEach(() => {
        angular.mock.module('SpecHelper', 'FrontRoyal.Users');

        angular.mock.inject([
            '$injector',
            $injector => {
                SpecHelper = $injector.get('SpecHelper');
                UserProgressLoaderInterceptor = $injector.get('UserProgressLoaderInterceptor');
                currentUser = SpecHelper.stubCurrentUser();
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('response', () => {
        it('should respond if the progress_max_updated_at that comes down is greater than before', async () => {
            currentUser.progress.progressMaxUpdatedAt = 1;
            jest.spyOn(currentUser.progress, 'clear').mockImplementation(() => {});

            await UserProgressLoaderInterceptor.response({
                data: {
                    meta: {
                        push_messages: {
                            progress_max_updated_at: 12345,
                        },
                    },
                },
                config: {
                    method: 'PUT',
                    url: '/lesson_progress.json',
                },
            });

            expect(currentUser.progress.clear).toHaveBeenCalled();
            expect(currentUser.progress.progressMaxUpdatedAt).toBeUndefined();
        });

        it('should respond if we save a progress record and there was already a change in the db that we did not know about', async () => {
            currentUser.progress.progressMaxUpdatedAt = 1;
            jest.spyOn(currentUser.progress, 'clear').mockImplementation(() => {});

            await UserProgressLoaderInterceptor.response({
                data: {
                    meta: {
                        push_messages: {
                            // this is the server telling us, after an update to lesson_progress, that
                            // the max value in the db BEFORE our change was 42.  Since the max change we
                            // know about is 1, we need to reload
                            progress_max_updated_at_before_changes: 42,
                            progress_max_updated_at: 12345,
                        },
                    },
                },
                config: {
                    method: 'PUT',
                    url: '/lesson_progress.json',
                },
            });

            expect(currentUser.progress.clear).toHaveBeenCalled();
            expect(currentUser.progress.progressMaxUpdatedAt).toBeUndefined();
        });

        it('should NOT respond if we save a progress record and the only change is the one we just saved', async () => {
            currentUser.progress.progressMaxUpdatedAt = 1;
            jest.spyOn(currentUser.progress, 'clear').mockImplementation(() => {});

            await UserProgressLoaderInterceptor.response({
                data: {
                    meta: {
                        push_messages: {
                            // this is the server telling us, after an update to lesson_progress, that
                            // the max value in the db BEFORE our change was still 1.  Since we already know
                            // about all changes up to 1, we do not need to reload
                            progress_max_updated_at_before_changes: 1,
                            progress_max_updated_at: 12345,
                        },
                    },
                },
                config: {
                    method: 'PUT',
                    url: '/lesson_progress.json',
                },
            });

            expect(currentUser.progress.clear).not.toHaveBeenCalled();
            expect(currentUser.progress.progressMaxUpdatedAt).toEqual(12345);
        });
    });
});
