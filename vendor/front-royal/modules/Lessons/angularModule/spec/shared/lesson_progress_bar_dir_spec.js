import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/lessons';

describe('Lessons.LessonProgressBarDirSpec', () => {
    let SpecHelper;
    let elem;
    let scope;
    let lesson;
    let playerViewModel;
    let $timeout;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                SpecHelper = $injector.get('SpecHelper');
                const FrameList = $injector.get('Lesson.FrameList');
                $injector.get('LessonFixtures');
                $timeout = $injector.get('$timeout');
                SpecHelper.stubEventLogging();

                lesson = FrameList.fixtures.getInstance();
                expect(lesson.frames.length).toBe(3);
                playerViewModel = lesson.createPlayerViewModel();
                playerViewModel.activeFrameIndex = 0;
            },
        ]);

        render();
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('rows', () => {
        it('should respect row-threshold when rendering indicators', () => {
            SpecHelper.expectElements(elem, '.indicator-row', 2);
            SpecHelper.expectElements(elem, '.indicator-row:eq(0) .indicator', 2);
            SpecHelper.expectElements(elem, '.indicator-row:eq(1) .indicator', 1);
        });
    });

    describe('indicators', () => {
        it('should fill completed frames and active frame', () => {
            // no completed frames, but on first frame so it is filled
            SpecHelper.expectHasClass(elem, '.indicator:eq(0)', 'filled');
            SpecHelper.expectDoesNotHaveClass(elem, '.indicator:eq(1)', 'filled');
            SpecHelper.expectDoesNotHaveClass(elem, '.indicator:eq(2)', 'filled');

            // 1st frame complete, 2nd frame active
            completeActiveFrame();
            SpecHelper.expectHasClass(elem, '.indicator:eq(0)', 'filled');
            SpecHelper.expectHasClass(elem, '.indicator:eq(1)', 'filled');
            SpecHelper.expectDoesNotHaveClass(elem, '.indicator:eq(2)', 'filled');

            // 1st and 2nd frame complete. 3rd frame active
            completeActiveFrame();
            SpecHelper.expectHasClass(elem, '.indicator:eq(0)', 'filled');
            SpecHelper.expectHasClass(elem, '.indicator:eq(1)', 'filled');
            SpecHelper.expectHasClass(elem, '.indicator:eq(2)', 'filled');
        });

        it('should leave a previously active frame filled when navigating backward', () => {
            completeActiveFrame(); // goto frame 1
            completeActiveFrame(); // goto frame 2
            setActiveFrameIndex(0);
            SpecHelper.expectHasClass(elem, '.indicator:eq(2)', 'filled');
            SpecHelper.expectElementEnabled(elem, '.indicator:eq(2)');
        });

        it('should partially fill incomplete frames that are before the active one', () => {
            setActiveFrameIndex(2);
            SpecHelper.expectHasClass(elem, '.indicator:eq(1)', 'partially-filled');
        });

        it('should add the active-highlighting to the current frame only if it is not the latest one', () => {
            completeActiveFrame();
            completeActiveFrame();
            SpecHelper.expectDoesNotHaveClass(elem, '.indicator:eq(2)', 'active-highlight');
            setActiveFrameIndex(0);
            SpecHelper.expectHasClass(elem, '.indicator:eq(0)', 'active-highlight');
            setActiveFrameIndex(1);
            SpecHelper.expectDoesNotHaveClass(elem, '.indicator:eq(0)', 'active-highlight');
        });

        it('it should add the active-higlighting when going to the second-to-last-frame', () => {
            completeActiveFrame();
            completeActiveFrame();
            setActiveFrameIndex(1);
            SpecHelper.expectHasClass(elem, '.indicator:eq(1)', 'active-highlight');
        });

        it('should disable navigation to all frames if !canNavigateBackToCompletedFrames', () => {
            jest.spyOn(playerViewModel, 'canNavigateBackToCompletedFrames', 'get').mockReturnValue(false);
            jest.spyOn(playerViewModel, 'canNavigateFreely', 'get').mockReturnValue(false);
            completeActiveFrame();
            SpecHelper.expectElementDisabled(elem, '.indicator:eq(0)');
        });

        it('should disable navigation to later frames if canNavigateBackToCompletedFrames', () => {
            jest.spyOn(playerViewModel, 'canNavigateBackToCompletedFrames', 'get').mockReturnValue(true);
            jest.spyOn(playerViewModel, 'canNavigateFreely', 'get').mockReturnValue(false);
            completeActiveFrame();
            SpecHelper.expectElementEnabled(elem, '.indicator:eq(0)');
            SpecHelper.expectElementDisabled(elem, '.indicator:eq(2)');
        });

        it('should enable all but the active frame if canNavigateFreely', () => {
            jest.spyOn(playerViewModel, 'canNavigateFreely', 'get').mockReturnValue(true);
            completeActiveFrame();
            SpecHelper.expectElementEnabled(elem, '.indicator:eq(0)');
            SpecHelper.expectElementDisabled(elem, '.indicator:eq(1)');
            SpecHelper.expectElementEnabled(elem, '.indicator:eq(2)');
        });

        it('should disable the active frame even if it is completed', () => {
            completeActiveFrame();
            setActiveFrameIndex(0);
            SpecHelper.expectElementDisabled(elem, '.indicator:eq(0)');
        });

        it('should navigate to a particular frame when clicked', () => {
            completeActiveFrame();
            completeActiveFrame();
            expect(playerViewModel.frameHistory).not.toEqual([]);
            SpecHelper.click(elem, '.indicator:eq(1)');
            expect(playerViewModel.frameHistory).toEqual([]);
            expect(playerViewModel.activeFrameIndex).toEqual(1);
        });
    });

    function render() {
        if (scope) {
            scope.$destroy();
        }
        const renderer = SpecHelper.renderer();
        renderer.scope.playerViewModel = playerViewModel;
        renderer.render('<lesson-progress-bar player-view-model="playerViewModel" row-threshold="2" />');
        elem = renderer.elem;
        scope = renderer.scope;
    }

    function setActiveFrameIndex(activeFrameIndex) {
        playerViewModel.activeFrameIndex = activeFrameIndex;
        scope.$digest();
        $timeout.flush();
    }

    function completeActiveFrame() {
        playerViewModel.completedFrames[playerViewModel.activeFrame.id] = true;
        playerViewModel.gotoNext();
        scope.$digest();
        $timeout.flush();
    }
});
