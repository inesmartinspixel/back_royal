import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/streams';

describe('Lessons.UserProgressLoaderSpec', () => {
    let SpecHelper;
    let StreamProgress;
    let LessonProgress;
    let userProgressLoader;
    let $timeout;
    let $rootScope;
    let currentUser;
    let frontRoyalStore;
    let $injector;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'SpecHelper');

        angular.mock.module($provide => {
            frontRoyalStore = {
                enabled: false,
            };
            $provide.value('frontRoyalStore', frontRoyalStore);
        });

        angular.mock.inject([
            '$injector',
            _$injector => {
                $injector = _$injector;
                SpecHelper = $injector.get('SpecHelper');
                StreamProgress = $injector.get('Lesson.StreamProgress');
                $injector.get('StreamProgressFixtures');
                LessonProgress = $injector.get('LessonProgress');
                $injector.get('LessonProgressFixtures');
                $timeout = $injector.get('$timeout');
                $rootScope = $injector.get('$rootScope');

                currentUser = SpecHelper.stubCurrentUser();
                $rootScope.$digest();
                userProgressLoader = currentUser.progress;
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('auto-destroy', () => {
        it('should be destroyed when the currentUser is changed', () => {
            jest.spyOn(userProgressLoader, 'destroy').mockImplementation(() => {});
            $rootScope.$digest();
            expect(userProgressLoader.destroy).not.toHaveBeenCalled();
            $rootScope.currentUser = {
                id: 'changed',
            };
            $rootScope.$digest();
            expect(userProgressLoader.destroy).toHaveBeenCalled();
        });

        it('should be destroyed when frontRoyalStore.enabled is toggled', () => {
            jest.spyOn(userProgressLoader, 'destroy').mockImplementation(() => {});
            $rootScope.$digest();
            expect(userProgressLoader.destroy).not.toHaveBeenCalled();
            frontRoyalStore.enabled = !frontRoyalStore.enabled;
            $rootScope.$digest();
            expect(userProgressLoader.destroy).toHaveBeenCalled();
        });
    });

    describe('UserProgressLoader', () => {
        beforeEach(() => {
            frontRoyalStore.enabled = true;
            $rootScope.$digest();
            userProgressLoader = currentUser.progress;
        });

        describe('destroy', () => {
            it('should set the destroyed flag', () => {
                userProgressLoader.destroy();
                expect(userProgressLoader.destroyed).toBe(true);
            });
        });
        describe('replaceProgress', () => {
            it('should pull progress from the store', () => {
                const $q = $injector.get('$q');
                const Stream = $injector.get('Lesson.Stream');
                $injector.get('StreamFixtures');
                const stream = Stream.fixtures.getInstance().addLocalePackFixture();
                const lesson = stream.lessons[0].addLocalePackFixture();
                const streamProgress = StreamProgress.new({ locale_pack_id: stream.localePackId });
                const lessonProgress = LessonProgress.new({ localePackId: lesson.localePackId });
                const getProgressReturnValues = {};
                getProgressReturnValues[stream.localePackId] = streamProgress;
                getProgressReturnValues[lesson.localePackId] = lessonProgress;
                jest.spyOn(userProgressLoader, '_getProgressForReplace').mockReturnValue(
                    $q.when(getProgressReturnValues),
                );

                let returnedStream;
                userProgressLoader.replaceProgress(stream).then(s => {
                    returnedStream = s;
                });
                $timeout.flush();

                // check that progress from the store has been pushed onto the stream
                expect(returnedStream.lesson_streams_progress).toBe(streamProgress);
                expect(returnedStream.lesson_streams_progress.stream()).toBe(returnedStream);
                const clonedLesson = returnedStream.lessons[0];
                expect(clonedLesson.lesson_progress).toBe(lessonProgress);
                expect(clonedLesson.lesson_progress.lesson()).toBe(clonedLesson);

                // Check that the original stream was not changed
                expect(stream.lesson_streams_progress).not.toBe(streamProgress);
                expect(stream.lessons[0].lesson_progress).not.toBe(lessonProgress);
            });
        });
    });

    describe('LegacyUserProgressLoader', () => {
        beforeEach(() => {
            frontRoyalStore.enabled = false;
            $rootScope.$digest();
            userProgressLoader = currentUser.progress;
        });
        describe('getAllProgress', () => {
            it('should load up progress and build a convenient result object', () => {
                const streamProgresses = [StreamProgress.fixtures.getInstance()];
                const lessonProgresses = [LessonProgress.fixtures.getInstance];
                const favoriteStreamsSet = 'favoriteStreamsSet';

                StreamProgress.expect('index')
                    .toBeCalledWith({
                        filters: {
                            user_id: userProgressLoader.user.id,
                        },
                        include_lesson_progress: true,
                        include_favorite_streams: true,
                    })
                    .returns({
                        result: streamProgresses,
                        meta: {
                            lesson_progress: lessonProgresses,
                            favorite_streams_set: favoriteStreamsSet,
                        },
                    });

                assertGetAllProgress(streamProgresses, lessonProgresses, favoriteStreamsSet);

                // check that the result is now cached
                jest.spyOn(StreamProgress, 'index').mockImplementation(() => {});
                assertGetAllProgress(streamProgresses, lessonProgresses, favoriteStreamsSet);
                expect(StreamProgress.index).not.toHaveBeenCalled();
            });

            function assertGetAllProgress(streamProgresses, lessonProgresses, favoriteStreamsSet) {
                let result;
                userProgressLoader.getAllProgress().then(_result => {
                    result = _result;
                });
                try {
                    StreamProgress.flush('index');
                } catch (err) {
                    // if the result is cached, we will not make a request here
                    $timeout.flush();
                }

                expect(_.pluck(result.streamProgress, 'locale_pack_id')).toEqual(
                    _.pluck(streamProgresses, 'locale_pack_id'),
                );
                expect(_.pluck(result.lessonProgress, 'locale_pack_id')).toEqual(
                    _.pluck(lessonProgresses, 'locale_pack_id'),
                );
                expect(result.favoriteStreamsSet).toEqual(favoriteStreamsSet);
            }
        });

        describe('favorite lesson streams watch', () => {
            it('should clear if the list of favorite lesson streams is reset', () => {
                jest.spyOn(userProgressLoader, 'clear');
                $rootScope.$digest();
                expect(userProgressLoader.clear).not.toHaveBeenCalled();

                // changing the list should trigger a clear
                userProgressLoader.user.favorite_lesson_stream_locale_packs = [
                    {
                        id: 1,
                    },
                ];
                $rootScope.$digest();
                expect(userProgressLoader.clear).toHaveBeenCalled();
                userProgressLoader.clear.mockClear();

                // having the list replaced with a clone should not trigger
                userProgressLoader.user.favorite_lesson_stream_locale_packs = [
                    {
                        id: 1,
                    },
                ];
                $rootScope.$digest();
                expect(userProgressLoader.clear).not.toHaveBeenCalled();

                // destroying the loader should turn off the watch
                userProgressLoader.destroy();
                userProgressLoader.user.favorite_lesson_stream_locale_packs = [
                    {
                        id: 2,
                    },
                ];
                $rootScope.$digest();
                expect(userProgressLoader.clear).not.toHaveBeenCalled();
            });
        });

        describe('replaceProgress', () => {
            it('should return the passed in stream', () => {
                const stream = {};
                let returnedStream;
                userProgressLoader.replaceProgress(stream).then(s => {
                    returnedStream = s;
                });
                $timeout.flush();
                expect(returnedStream).toBe(stream);
            });
        });
    });
});
