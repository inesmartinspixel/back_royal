import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/streams';

angular.module('FrontRoyal.Lessons').factory('StreamProgressFixtures', [
    'Lesson.StreamProgress',
    '$injector',
    (StreamProgress, $injector) => {
        StreamProgress.fixtures = {
            sampleAttrs(overrides = {}) {
                return angular.extend(
                    {
                        locale_pack_id: '4',
                        lesson_bookmark_id: 'ddba3178-d76d-4eaa-96ba-2cd47e3378a8',
                        user_id: 1,
                        started_at: '1395345127',
                        complete: false,
                        time_runs_out_at: null,
                    },
                    overrides,
                );
            },

            getInstance(overrides) {
                return StreamProgress.new(this.sampleAttrs(overrides));
            },
        };

        StreamProgress.prototype.embedInStream = function (stream) {
            if (!stream) {
                $injector.get('StreamFixtures');
                stream = $injector.get('Lesson.Stream').fixtures.getInstance();
            }

            stream.lesson_streams_progress = this;
            this.$$embeddedIn = stream;
            this.locale_pack_id = stream.id;
            this.lesson_bookmark_id = stream.lessons[0].id;
            return this;
        };

        return {};
    },
]);
