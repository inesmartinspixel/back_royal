import 'Lessons/angularModule';
import './lesson_progress';
import './streams';
import 'Lessons/angularModule/spec/_mock/fixtures/componentized_fixtures';
import 'Lessons/angularModule/spec/_mock/fixtures/streams';

angular.module('FrontRoyal.Lessons').factory('LessonFixtures', [
    '$injector',
    $injector => {
        const Lesson = $injector.get('Lesson');
        const FrameList = $injector.get('Lesson.FrameList');
        const Frame = $injector.get('Lesson.FrameList.Frame');
        const Componentized = $injector.get('Lesson.FrameList.Frame.Componentized');
        const LessonProgress = $injector.get('LessonProgress');
        $injector.get('LessonProgressFixtures');
        $injector.get('Lesson.LessonVersion');
        $injector.get('EntityMetadata');
        const guid = $injector.get('guid');

        const PlayerViewModel = $injector.get('Lesson.PlayerViewModel');
        const PlayerViewModelWithLesson = $injector.get('PlayerViewModelWithLesson');

        const MockLessonPlayerViewModel = PlayerViewModel.subclass(function () {
            this.include(PlayerViewModelWithLesson);
        });

        // Since we're just using the Lesson base class, we need
        // to set showDirectiveName, which would normally be defined
        // in a subclass, to make sure we're not testing subclass-specific functionality
        Lesson.include({
            showDirectiveName: 'lesson-directive',
        });

        function embedInStream(lesson, comingSoon) {
            comingSoon = comingSoon === true;
            $injector.get('StreamFixtures');
            const stream = $injector.get('Lesson.Stream').fixtures.getInstance();
            lesson.$$embeddedIn = stream;
            stream.lessons.push(lesson);
            stream.chapters[0].lesson_hashes.push({
                lesson_id: lesson.id,
                coming_soon: comingSoon,
            });
            return lesson;
        }

        Lesson.prototype.includeProgress = function () {
            this.lesson_progress = LessonProgress.fixtures.getInstance({
                lesson_id: this.id,
            });
            return this;
        };

        Lesson.prototype.addLocalePackFixture = function () {
            if (this.locale !== 'en') {
                throw new Error('We are assuming an en locale');
            }
            this.locale_pack = $injector.get('LocalePack').new({
                id: guid.generate(),
                content_items: [
                    {
                        id: this.id,
                        title: this.title,
                        locale: 'en',
                    },
                    {
                        id: guid.generate(),
                        title: 'Spanish',
                        locale: 'es',
                    },
                    {
                        id: guid.generate(),
                        title: 'Chinese',
                        locale: 'zh',
                    },
                ],
            });
            return this;
        };

        FrameList.prototype.includeProgress = function () {
            this.lesson_progress = LessonProgress.fixtures.getInstance({
                lesson_id: this.id,
                frame_bookmark_id: this.frames[0].id,
            });
            return this;
        };

        Lesson.fixtures = {
            sampleAttrs(overrides = {}) {
                overrides.id = overrides.id || String(Math.random());
                return angular.extend(
                    {
                        title: `My Lesson ${overrides.id}`,
                        locale: 'en',
                        archived: false,
                        stream_titles: [],
                        client_requirements: {
                            min_allowed_version: 0,
                            supported_in_latest_available_version: 0,
                        },
                        entity_metadata: {
                            title: 'askjhsdf',
                            description: 'sdjfhsdf',
                            canonical_url: 'askdjhs',
                        },
                        key_terms: [],
                    },
                    overrides,
                );
            },

            getInstance(overrides, withoutStream) {
                const lesson = Lesson.new(this.sampleAttrs(overrides));
                lesson.createPlayerViewModel = function (options) {
                    return new MockLessonPlayerViewModel(this, options);
                };
                return withoutStream ? lesson : embedInStream(lesson);
            },
        };

        function embedFrameInLesson(frame) {
            const lesson = FrameList.fixtures.getInstance();
            lesson.frames = [frame];
            frame.$$embeddedIn = lesson;
            return frame;
        }

        FrameList.fixtures = {
            sampleAttrs(overrides, makeComponentized) {
                $injector.get('ComponentizedFixtures');
                overrides = overrides || {};
                overrides.id = overrides.id || String(Math.random());

                const frameKlass = makeComponentized ? Componentized : Frame;
                return angular.extend(
                    {
                        title: `My FrameList Lesson ${String(Math.random())}`,
                        archived: false,
                        stream_titles: [],
                        key_terms: [],
                        locale: 'en',
                        frame_count: 3,
                        frames: [
                            frameKlass.fixtures.sampleAttrs({
                                text_content: 'Here is some text for frame 1.',
                            }),
                            frameKlass.fixtures.sampleAttrs({
                                text_content: 'Here is some text for frame 2.',
                            }),
                            frameKlass.fixtures.sampleAttrs({
                                text_content: 'Here is some text for frame 3.',
                            }),
                        ],
                        entity_metadata: {
                            title: 'askjhsdf',
                            description: 'sdjfhsdf',
                            canonical_url: 'askdjhs',
                        },
                        version_id: 'lesson_version_id',
                        client_requirements: {
                            min_allowed_version: 0,
                            supported_in_latest_available_version: 0,
                        },
                    },
                    overrides,
                );
            },

            embedFrameInLesson(frame) {
                return embedFrameInLesson(frame);
            },

            getInstance(overrides, withoutStream, makeComponentized) {
                const lesson = FrameList.new(this.sampleAttrs(overrides, makeComponentized));
                return withoutStream ? lesson : embedInStream(lesson);
            },
        };

        // Subclasses are supposed to set this, but in tests
        // we sometimes instantiate Frame directly, so we want
        // it set in tests
        Frame.include({
            directiveName: 'fake-dynamic-node',
            infoPanelDirectiveName: 'fake-info-panel-name',
        });

        // Hack up Frame.subclasses so that we don't
        // rely on specific subclasses of frames in tests
        // that test at a higher level.
        Frame.mockOutSubclasses = () => {
            Frame.subclasses = [
                Frame.subclass(function () {
                    this.alias('FakeFrame1');
                }),
                Frame.subclass(function () {
                    this.alias('FakeFrame2');
                }),
            ];
        };

        const frameFixtures = {
            frame: {
                sampleAttrs(overrides = {}) {
                    return angular.extend(
                        {
                            id: String(Math.random()),
                            editor_template: 'no_interaction',
                        },
                        overrides,
                    );
                },

                getInstance(overrides) {
                    return embedFrameInLesson(this.klass.new(this.sampleAttrs(overrides)));
                },
            },
        };

        const origSubclass = Frame.subclass;

        function assignFixtures(klass, obj) {
            if (obj) {
                klass.fixtures = obj;
                obj.klass = klass;
            }

            klass.subclasses.forEach(subclass => {
                if (subclass.isQuestionsFrame) {
                    assignFixtures(subclass, frameFixtures.questions_frame);
                } else {
                    assignFixtures(subclass, frameFixtures[subclass.alias()]);
                }
            });
        }

        // this craziness is necessary because we originally relied on all
        // classes being present all the time, and later switched to lazy
        // loading, but didn't want to go change things everwhere, especially
        // since all this stuff is planned to be removed.
        Frame.extend({
            subclass(options) {
                const subclass = origSubclass.apply(this, [options]);
                assignFixtures(subclass, frameFixtures[subclass.alias()]);
                return subclass;
            },
        });

        assignFixtures(Frame, frameFixtures.frame);

        return {};
    },
]);
