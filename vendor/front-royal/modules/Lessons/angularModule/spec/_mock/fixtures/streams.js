import 'Lessons/angularModule';
import './stream_progress';
import './lessons';
import 'Lessons/angularModule/spec/_mock/fixtures/lessons';

angular.module('FrontRoyal.Lessons').factory('StreamFixtures', [
    '$injector',
    $injector => {
        const Stream = $injector.get('Lesson.Stream');
        const StreamProgress = $injector.get('Lesson.StreamProgress');
        const FrameList = $injector.get('Lesson.FrameList');
        let counter = 0;
        $injector.get('StreamProgressFixtures');
        $injector.get('LessonFixtures');
        const guid = $injector.get('guid');

        // FIXME: we could potentially DRY this up with all content items
        Stream.prototype.addLocalePackFixture = function () {
            if (this.locale !== 'en') {
                throw new Error('We are assuming an en locale');
            }
            this.locale_pack = $injector.get('LocalePack').new({
                id: guid.generate(),
                content_items: [
                    {
                        id: this.id,
                        title: this.title,
                        locale: 'en',
                    },
                    {
                        id: guid.generate(),
                        title: 'Spanish',
                        locale: 'es',
                    },
                    {
                        id: guid.generate(),
                        title: 'Chinese',
                        locale: 'zh',
                    },
                ],
            });
            return this;
        };

        Stream.fixtures = {
            sampleAttrs(overrides = {}) {
                const lesson1 = FrameList.fixtures.getInstance({}, true).asJson();
                const lesson2 = FrameList.fixtures.getInstance({}, true).asJson();
                const lesson3 = FrameList.fixtures.getInstance({}, true).asJson();

                const id = String(Math.random());

                return angular.extend(
                    {
                        id,
                        title: `My Stream ${(counter += 1)}`,
                        description: 'This is a really good one',
                        locale: 'en',
                        what_you_will_learn: [
                            'The first **thing** to learn',
                            'The second thing to learn',
                            'The third thing to learn',
                        ],
                        resource_downloads: [
                            {
                                url:
                                    'https://uploads.smart.ly/images/820bf3aa1fbc1fdb3167c63f2cfcf41f/1400x550/820bf3aa1fbc1fdb3167c63f2cfcf41f.jpg',
                                title: 'A picture of a dog collar',
                                type: 'jpg',
                            },
                            {
                                url:
                                    'https://uploads.smart.ly/images/820bf3aa1fbc1fdb3167c63f2cfcf41f/1400x550/820bf3aa1fbc1fdb3167c63f2cfcf41f.jpg',
                                title: 'Another picture of a dog collar',
                                type: 'jpg',
                            },
                        ],
                        resource_links: [
                            {
                                url:
                                    'https://trello.com/c/1bS05mh0/194-feat-ui-ux-new-sidebar-sections-for-course-page',
                                title: 'FEAT: UI/UX New Sidebar sections for Course Page',
                            },
                            {
                                url:
                                    'https://trello.com/c/1bS05mh0/194-feat-ui-ux-new-sidebar-sections-for-course-page',
                                title: 'FEAT: UI/UX New Sidebar sections for Course Page',
                            },
                        ],
                        credits:
                            '**BRENT FORD**  \nContent Master\n\n**DANIEL MINTZ**  \nEditor\n\n**ALEXIE HARPER**  \nManaging Editor',
                        image: {
                            formats: {
                                '200x150': {
                                    url: 'NA',
                                },
                                original: {
                                    url: 'NA',
                                },
                            },
                        },
                        author: {
                            name: 'Cool Brent',
                        },
                        topics: [],
                        lessons: [lesson1, lesson2, lesson3],
                        entity_metadata: {
                            title: 'askjhsdf',
                            description: 'sdjfhsdf',
                            canonical_url: 'askdjhs',
                        },
                        exam: false,
                        time_limit_hours: null,
                        exam_open_time: null,
                        exam_close_time: null,

                        chapters: [
                            {
                                title: 'Chapter 1',
                                lesson_ids: [lesson1.id, lesson3.id],
                                lesson_hashes: [
                                    {
                                        lesson_id: lesson1.id,
                                        coming_soon: false,
                                    },
                                    {
                                        lesson_id: lesson3.id,
                                        coming_soon: false,
                                    },
                                ],
                                pending: false,
                            },
                            {
                                title: 'Chapter 2',
                                lesson_ids: [lesson2.id],
                                lesson_hashes: [
                                    {
                                        lesson_id: lesson2.id,
                                        coming_soon: false,
                                    },
                                ],
                                pending: false,
                            },
                        ],

                        lesson_streams_progress: StreamProgress.fixtures.getInstance({
                            lesson_stream_id: id,
                        }),

                        summaries: [
                            {
                                id: String(Math.random()),
                                url:
                                    'https://uploads.smart.ly/images/820bf3aa1fbc1fdb3167c63f2cfcf41f/1400x550/820bf3aa1fbc1fdb3167c63f2cfcf41f.jpg',
                                title: 'Summary1',
                                lessons: [lesson1.id, lesson2.id],
                            },
                            {
                                id: String(Math.random()),
                                url:
                                    'https://uploads.smart.ly/images/820bf3aa1fbc1fdb3167c63f2cfcf41f/1400x550/820bf3aa1fbc1fdb3167c63f2cfcf41f.jpg',
                                title: 'Summary2',
                                lessons: [lesson3.id],
                            },
                        ],

                        version_id: 'version_id',
                        favorite: false,

                        created_at: 0,
                        updated_at: 0,
                    },
                    overrides,
                );
            },

            getInstance(overrides) {
                const stream = Stream.new(this.sampleAttrs(overrides));
                stream.ensureLocalePack();
                // a promise is created when caching this stream.  flush it
                // now so it's not interfering with other attempts to deal
                // with timeouts in tests
                $injector.get('$timeout').flush();
                return stream;
            },
        };

        return {};
    },
]);
