import 'Lessons/angularModule';
import './lessons';
import 'Lessons/angularModule/spec/_mock/fixtures/lessons';

angular.module('FrontRoyal.Lessons').factory('ComponentizedFixtures', [
    '$injector',
    $injector => {
        const FrameList = $injector.get('Lesson.FrameList');
        const Componentized = $injector.get('Lesson.FrameList.Frame.Componentized');
        $injector.get('LessonFixtures');

        const getComponentModel = rel => {
            const name = _.last(rel.split('.'));
            return $injector.get(`Lesson.FrameList.Frame.Componentized.Component.${rel}.${name}Model`);
        };

        function getBaseComponentModel() {
            const ComponentModel = $injector.get('Lesson.FrameList.Frame.Componentized.Component.ComponentModel');
            ComponentModel.ViewModel.prototype.directiveName = 'cf-vanilla-component';
            return ComponentModel;
        }

        function getAnswerModel() {
            return getComponentModel('Answer');
        }

        function getSelectableAnswerModel() {
            return getComponentModel('Answer.SelectableAnswer');
        }

        function getSelectableAnswerNavigatorModel() {
            return $injector.get(
                'Lesson.FrameList.Frame.Componentized.Component.Answer.SelectableAnswer.SelectableAnswerNavigatorModel',
            );
        }

        function getAnswerListModel() {
            return getComponentModel('AnswerList');
        }

        function getAnswerMatcherModel() {
            return getComponentModel('AnswerMatcher');
        }

        function getSimilarTogetSelectableAnswerModel() {
            return getComponentModel('AnswerMatcher.SimilarToSelectableAnswer');
        }

        function getMatchesExpectedText() {
            return getComponentModel('AnswerMatcher.MatchesExpectedText');
        }

        function getChallengeModel() {
            return getComponentModel('Challenge');
        }

        function getChallengeOverlayBlankModel() {
            return getComponentModel('ChallengeOverlayBlank');
        }

        function getMultipleChoiceChallengeModel() {
            return getComponentModel('Challenge.MultipleChoiceChallenge');
        }

        function getUserInputChallengeModel() {
            return getComponentModel('Challenge.UserInputChallenge');
        }

        function getChallengeValidatorModel() {
            return getComponentModel('ChallengeValidator');
        }

        function getComponentOverlayModel() {
            return getComponentModel('ComponentOverlay');
        }

        function getChallengesModel() {
            return getComponentModel('Challenges');
        }

        function getContinueButtonModel() {
            return getComponentModel('ContinueButton');
        }

        function getChallengesContinueButtonModel() {
            return getComponentModel('ContinueButton.ChallengesContinueButton');
        }

        function getAlwaysReadyContinueButtonModel() {
            return getComponentModel('ContinueButton.AlwaysReadyContinueButton');
        }

        function getImageModel() {
            return getComponentModel('Image');
        }

        function getInteractiveCardsModel() {
            return getComponentModel('InteractiveCards');
        }

        function getLayoutModel() {
            return getComponentModel('Layout');
        }

        function getMatchingBoardModel() {
            return getComponentModel('MatchingBoard');
        }

        function getMatchingChallengeButtonModel() {
            return getComponentModel('MatchingChallengeButton');
        }

        function getMultipleChoiceMessageModel() {
            return getComponentModel('MultipleChoiceMessage');
        }

        function getUserInputMessageModel() {
            return getComponentModel('UserInputMessage');
        }

        function getTilePromptModel() {
            return getComponentModel('TilePrompt');
        }

        function getTilePromptBoardModel() {
            return getComponentModel('TilePromptBoard');
        }

        function getTextImageInteractiveModel() {
            return getComponentModel('Layout.TextImageInteractive');
        }

        function getTextModel() {
            return getComponentModel('Text');
        }
        const guid = $injector.get('guid');

        const embedFrameInLesson = FrameList.fixtures.embedFrameInLesson;

        Componentized.include({
            addVanillaComponent(options = {}) {
                const editorViewModel = getBaseComponentModel().EditorViewModel.addComponentTo(this, options);
                const component = editorViewModel.model;

                component.addDefaultReferences = function () {
                    editorViewModel.setup();
                    return this;
                };

                return component;
            },

            addAnswer(options = {}) {
                const component = getAnswerModel().new(options);
                return this.addComponent(component);
            },

            addSelectableAnswer(options = {}) {
                options.text = options.hasOwnProperty('text') ? options.text : `Answer ${guid.generate()}`;
                const attrs = {
                    id: guid.generate(),
                    $$embeddedIn: this,
                };
                if (options.text) {
                    attrs.text_id = this.addText({
                        text: options.text,
                    }).id;
                }
                const component = getSelectableAnswerModel().new(attrs);

                component.addDefaultReferences = function () {
                    return this;
                };

                this.components.push(component);
                return component;
            },

            addSelectableAnswerNavigator(options) {
                options = angular.extend(
                    {
                        event: 'gotoLastSelectedWhenMovingOnFromFrame',
                        next_frame_id: 'next_frame_id',
                    },
                    options,
                );

                const component = getSelectableAnswerNavigatorModel().new(options);
                if (!this.frameNavigator) {
                    throw new Error('Expecting a frame navigator');
                }
                component.frameNavigator = this.frameNavigator;

                component.addChallenge = function (options) {
                    const challenge = this.frame().addMultipleChoiceChallenge(options).addDefaultReferences();
                    this.challenge = challenge;
                    return this.challenge;
                };

                component.attachToAnswer = function (answer = this.challenge.answers[0]) {
                    const answerMatcher = this.frame().addSimilarToSelectableAnswer();
                    answerMatcher.answer = answer;
                    this.answerMatcher = answerMatcher;
                    return answer;
                };

                component.addDefaultReferences = function () {
                    if (!this.challenge) {
                        this.addChallenge();
                    }
                    this.attachToAnswer();
                    return this;
                };

                this.addComponent(component);
                component.frameNavigator.selectableAnswerNavigators.push(component);
                return component;
            },

            addAnswerList(options) {
                options = angular.extend(
                    {
                        skin: 'buttons',
                        randomize: false,
                    },
                    options || {},
                );

                const component = getAnswerListModel().new(options);

                component.addDefaultReferences = function () {
                    this.answers = [
                        this.frame().addSelectableAnswer(),
                        this.frame().addSelectableAnswer(),
                        this.frame().addSelectableAnswer(),
                        this.frame().addSelectableAnswer(),
                    ];

                    return this;
                };

                return this.addComponent(component);
            },

            addAnswerMatcher(options = {}) {
                const component = getAnswerMatcherModel().new(options);

                component.addHasText = function (options) {
                    const text = this.frame().addText(options);
                    this.hasText = text;
                    return text;
                };

                return this.addComponent(component);
            },

            addSimilarToSelectableAnswer(options = {}) {
                const component = getSimilarTogetSelectableAnswerModel().new(options);

                component.addDefaultReferences = function () {
                    return this;
                };

                return this.addComponent(component);
            },

            addMatchesExpectedText(options = {}) {
                const component = getMatchesExpectedText().new(options);

                component.addDefaultReferences = function () {
                    return this;
                };

                return this.addComponent(component);
            },

            addChallenge(options = {}) {
                const component = getChallengeModel().new(options);

                component.addValidator = function (options) {
                    const validator = this.frame().addChallengeValidator(options);
                    this.validator = validator;
                    validator.challenge = this;
                };

                component.addLayout = function (options) {
                    this.layout = this.frame().addTextImageInteractiveLayout(options).addDefaultReferences();
                    return this.layout;
                };

                component.addDefaultReferences = function () {
                    if (!this.hasOwnProperty('validator_id')) {
                        this.addValidator();
                    }
                    this.addLayout();

                    return this;
                };

                return this.addComponent(component);
            },

            addChallengeOverlayBlank() {
                const editorViewModel = getChallengeOverlayBlankModel().EditorViewModel.addComponentTo(this);
                const component = editorViewModel.model;

                component.addDefaultReferences = function () {
                    editorViewModel.setup();
                    // Note: adding a MultipleChoiceChallenge here so the overlay won't be empty
                    // In the future, might need to enhance this helper so we can mock UserInputChallenge overlays?
                    editorViewModel.model.challenge = this.frame().addMultipleChoiceChallenge().addDefaultReferences();
                    return this;
                };

                component.addUserInputChallenge = function () {
                    editorViewModel.model.challenge = this.frame().addUserInputChallenge().addDefaultReferences();
                };

                return component;
            },

            addChallengeValidator(options = {}) {
                const component = getChallengeValidatorModel().new(options);

                component.addDefaultBehaviors = function () {
                    this.behaviors = {
                        HasAllExpectedAnswers: {},
                        HasNoUnexpectedAnswers: {},
                    };
                };

                component.addExpectedAnswerMatcher = function (answerList) {
                    if (!answerList) {
                        throw new Error('No answer list provided.');
                    }
                    this.addDefaultBehaviors();
                    const answerMatcher = this.frame().addAnswerMatcher();
                    answerMatcher.addHasText({
                        text: answerList.answers[0].text.text,
                    });
                    this.expectedAnswerMatchers = [answerMatcher];
                    return this.expectedAnswerMatchers;
                };

                component.addMatchesExpectedText = function (options) {
                    this.addDefaultBehaviors();
                    const answerMatcher = this.frame().addMatchesExpectedText(options);
                    this.expectedAnswerMatchers = [answerMatcher];
                    return this.expectedAnswerMatchers;
                };

                component.addSimilarToSelectableAnswer = function (answerList) {
                    if (!answerList) {
                        throw new Error('No answer list provided.');
                    }
                    this.addDefaultBehaviors();
                    const answerMatcher = this.frame().addSimilarToSelectableAnswer();
                    answerMatcher.answer = answerList.answers[0];
                    this.expectedAnswerMatchers = [answerMatcher];
                    return this.expectedAnswerMatchers;
                };

                return this.addComponent(component);
            },

            addMultipleChoiceChallenge(options = {}) {
                const editorViewModel = getMultipleChoiceChallengeModel().EditorViewModel.addComponentTo(this, options);
                const component = editorViewModel.model;

                component.addAnswerList = function (options) {
                    const answerList = this.frame().addAnswerList(options);
                    answerList.addDefaultReferences();
                    this.answerList = answerList;
                };

                component.addContentForInteractive = function () {
                    if (!this.answerList) {
                        throw new Error('No answer list found on MultipleChoiceChallengeModel component.');
                    }
                    this.contentForInteractive = this.answerList;
                };

                component.addValidator = function (options) {
                    if (!this.answerList) {
                        throw new Error('No answer list found on MultipleChoiceChallengeModel component.');
                    }
                    const validator = this.frame().addChallengeValidator(options);
                    validator.answerList = this.answerList;
                    validator.challenge = this;

                    // addSimilarToSelectableAnswer will make the first answer the expected one by default
                    validator.addSimilarToSelectableAnswer(this.answerList);
                    this.validator = validator;
                };

                component.addDefaultReferences = function () {
                    editorViewModel.setup();

                    if (!this.answerList) {
                        this.addAnswerList();
                    }

                    // this is ugly.  When we used addAnswerList for this, it just worked, but
                    // not the mix of editorViewModel and fixtures is causing ugliness :(
                    this.answerList.skin = 'buttons';
                    this.answerList.randomize = false;
                    this.answerList.answers = [
                        this.frame().addSelectableAnswer(),
                        this.frame().addSelectableAnswer(),
                        this.frame().addSelectableAnswer(),
                        this.frame().addSelectableAnswer(),
                    ];
                    this.addContentForInteractive();
                    this.addValidator();
                    return this;
                };

                return component;
            },

            addUserInputChallenge(options = {}) {
                const editorViewModel = getUserInputChallengeModel().EditorViewModel.addComponentTo(this, options);
                const component = editorViewModel.model;

                component.addValidator = function (options) {
                    const validator = this.frame().addChallengeValidator(options);
                    validator.challenge = this;

                    // addSimilarToSelectableAnswer will make the first answer the expected one by default
                    validator.addMatchesExpectedText({
                        expectedText: 'some text',
                    });
                    this.validator = validator;
                };

                component.addDefaultReferences = function () {
                    editorViewModel.setup();
                    this.addValidator();
                    return this;
                };

                return component;
            },

            addChallenges(options = {}) {
                const component = getChallengesModel().EditorViewModel.addComponentTo(this, options).setup().model;

                component.addChallenges = function (options) {
                    this.addMultipleChoiceChallenge(options);
                    return this.challenges;
                };

                component.addChallenge = function (options) {
                    const challenge = this.frame().addChallenge(options).addDefaultReferences();
                    let challenges = this.challenges || [];
                    challenges = challenges.concat([challenge]);
                    this.challenges = challenges;
                    return challenges;
                };

                component.addMultipleChoiceChallenge = function (options) {
                    const challenge = this.frame().addMultipleChoiceChallenge(options).addDefaultReferences();
                    let challenges = this.challenges || [];
                    challenges = challenges.concat([challenge]);
                    this.challenges = challenges;
                    return challenges;
                };

                component.addDefaultReferences = function () {
                    if (!this.challenges || this.challenges.length === 0) {
                        this.addChallenges();
                    }
                    return this;
                };

                return component;
            },

            addContinueButton(options = {}) {
                const component = getContinueButtonModel().new(options);

                component.addDefaultReferences = function () {
                    return this;
                };

                return this.addComponent(component);
            },

            addAlwaysReadyContinueButton(options = {}) {
                const component = getAlwaysReadyContinueButtonModel().new(options);

                component.addDefaultReferences = function () {
                    return this;
                };

                return this.addComponent(component);
            },

            addChallengesContinueButton(options = {}) {
                const component = getChallengesContinueButtonModel().new(options);

                component.addTargetComponent = function (options) {
                    const targetComponent = this.frame().addChallenges(options).addDefaultReferences();
                    this.targetComponent = targetComponent;
                    return targetComponent;
                };

                component.addDefaultReferences = function () {
                    if (!this.targetComponent) {
                        this.addTargetComponent();
                    }
                    return this;
                };

                return this.addComponent(component);
            },

            addImage(options) {
                options = angular.extend(
                    {
                        image: {
                            id: 1,
                            file_file_name: `three_cluster_2_3_6.${guid.generate()}.png`,
                            formats: {
                                '150x150': {
                                    url: `https://path/to/image.${guid.generate()}.150.png`,
                                    width: 150,
                                    height: 150,
                                },
                                original: {
                                    url: `https://path/to/image.${guid.generate()}.orig.png`,
                                },
                            },
                            dimensions: {
                                width: 42,
                                height: 42,
                            },
                        },
                        label: `image ${guid.generate()}`,
                    },
                    options || {},
                );

                const component = getImageModel().new(options);
                component.addDefaultReferences = function () {
                    return this;
                };
                return this.addComponent(component);
            },

            addInteractiveCards(options = {}) {
                const component = getInteractiveCardsModel().new(options);

                component.addDefaultReferences = function () {
                    if (!this.overlays) {
                        this.overlays = [this.frame().addComponentOverlay(), this.frame().addComponentOverlay()];
                    }

                    this.overlays.forEach(overlay => {
                        overlay.image = this.frame().addImage();
                    });

                    if (!this.challengesComponent) {
                        this.challengesComponent = this.frame().addChallenges().addDefaultReferences();
                    }

                    while (this.challengesComponent.challenges.length < 4) {
                        this.challengesComponent.addMultipleChoiceChallenge();
                    }

                    for (let i = 0; i < this.challengesComponent.challenges.length; i++) {
                        // add the first two challenge blanks to the first
                        // overlay, and all the rest to the second.
                        const overlayIndex = i < 2 ? 0 : 1;
                        const componentOverlay = this.overlays[overlayIndex];
                        const challenge = this.challengesComponent.challenges[i];

                        const challengeOverlayBlank = this.frame().addChallengeOverlayBlank();
                        challengeOverlayBlank.challenge = challenge;
                        componentOverlay.overlayComponents.push(challengeOverlayBlank);
                    }

                    return this;
                };

                return this.addComponent(component);
            },

            addLayout(options = {}) {
                const component = getLayoutModel().new(options);

                component.addDefaultReferences = function () {
                    return this;
                };

                return this.addComponent(component);
            },

            addMatchingBoard(options = {}) {
                const component = getMatchingBoardModel().new(options);

                component.addButton = function () {
                    const button = this.frame().addMatchingChallengeButton().addDefaultReferences();
                    this.matchingChallengeButtons = this.matchingChallengeButtons || [];
                    this.matchingChallengeButtons.push(button);
                    return button;
                };

                component.addDefaultReferences = function () {
                    if (!this.answerList) {
                        this.answerList = this.frame().addAnswerList().addDefaultReferences();
                    }
                    if (!this.matchingChallengeButtons) {
                        this.addButton();
                    }
                    return this;
                };

                return this.addComponent(component);
            },

            addTilePromptBoard(options = {}) {
                const component = getTilePromptBoardModel().new(options);

                component.addDefaultReferences = function () {
                    if (!this.answerList) {
                        this.answerList = this.frame().addAnswerList().addDefaultReferences();
                    }

                    if (!this.challengesComponent) {
                        this.challengesComponent = this.frame().addChallenges().addDefaultReferences();
                        this.challengesComponent.addChallenge();
                        this.challengesComponent.addChallenge();
                    }

                    if (!this.tilePrompts) {
                        this.tilePrompts = [
                            this.frame().addTilePrompt().addDefaultReferences(),
                            this.frame().addTilePrompt().addDefaultReferences(),
                            this.frame().addTilePrompt().addDefaultReferences(),
                        ];

                        this.challengesComponent.challenges = [
                            this.tilePrompts[0].challenge,
                            this.tilePrompts[1].challenge,
                            this.tilePrompts[2].challenge,
                        ]; // replace list based on tileprompt generated
                    }
                    return this;
                };

                return this.addComponent(component);
            },

            addMatchingChallengeButton(options = {}) {
                const component = getMatchingChallengeButtonModel().new(options);

                component.addText = function (options) {
                    const text = this.frame().addText(options);
                    this.text = text;
                    return text;
                };

                component.addDefaultReferences = function () {
                    this.addText();
                    if (!this.challenge) {
                        this.challenge = this.frame().addMultipleChoiceChallenge(options).addDefaultReferences();
                    }
                    return this;
                };

                return this.addComponent(component);
            },

            addComponentOverlay() {
                const editorViewModel = getComponentOverlayModel().EditorViewModel.addComponentTo(this);
                const component = editorViewModel.model;

                component.addDefaultReferences = function () {
                    editorViewModel.setup();
                    return this;
                };

                return component;
            },

            addMultipleChoiceMessage(options) {
                options = angular.extend(
                    {
                        event: 'validated',
                    },
                    options,
                );

                const component = getMultipleChoiceMessageModel().new(options);

                component.addChallenge = function (options) {
                    const challenge = this.frame().addMultipleChoiceChallenge(options).addDefaultReferences();
                    this.challenge = challenge;
                    return this.challenge;
                };

                component.attachToAnswer = function (answer = this.challenge.answers[0]) {
                    const answerMatcher = this.frame().addSimilarToSelectableAnswer();
                    answerMatcher.answer = answer;
                    this.answerMatcher = answerMatcher;
                    return answer;
                };

                component.addMessageText = function (options) {
                    if (typeof options === 'string') {
                        options = {
                            text: options,
                        };
                    }
                    options = angular.extend(
                        {
                            text: `Message ${guid.generate()}`,
                        },
                        options || {},
                    );
                    const text = this.frame().addText(options);
                    this.messageText = text;
                    return text;
                };

                component.addDefaultReferences = function () {
                    if (!this.challenge) {
                        this.addChallenge();
                    }
                    this.attachToAnswer();
                    this.addMessageText();
                    return this;
                };

                return this.addComponent(component);
            },

            addUserInputMessage(options) {
                options = angular.extend({}, options);

                const component = getUserInputMessageModel().new(options);

                component.addChallenge = function (options) {
                    const challenge = this.frame().addUserInputChallenge(options).addDefaultReferences();
                    this.challenge = challenge;
                    return this.challenge;
                };

                component.addMessageText = function (options) {
                    if (typeof options === 'string') {
                        options = {
                            text: options,
                        };
                    }
                    options = angular.extend(
                        {
                            text: `Message ${guid.generate()}`,
                        },
                        options || {},
                    );
                    const text = this.frame().addText(options);
                    this.messageText = text;
                    return text;
                };

                component.addDefaultReferences = function () {
                    if (!this.challenge) {
                        this.addChallenge();
                    }
                    this.addMessageText();
                    return this;
                };

                return this.addComponent(component);
            },

            addTilePrompt(options = {}) {
                const component = getTilePromptModel().new(options);

                component.addText = function (options) {
                    const text = this.frame().addText(options);
                    this.text = text;
                    return text;
                };

                component.addDefaultReferences = function () {
                    this.addText();
                    if (!this.challenge) {
                        this.challenge = this.frame().addMultipleChoiceChallenge(options).addDefaultReferences();
                    }
                    return this;
                };

                return this.addComponent(component);
            },

            addTextImageInteractiveLayout(options = {}) {
                const component = getTextImageInteractiveModel().new(options);

                component.addDefaultReferences = function () {
                    const frame = this.frame();

                    this.staticContentForText = frame.addText();
                    this.staticContentForFirstImage = frame.addText();
                    this.staticContentForSecondImage = frame.addText();
                    this.staticContentForInteractive = frame.addText();

                    // this.staticContentForText.components = [frame.addText()];
                    // this.staticContentForFirstImage.components = [frame.addText()];
                    // this.staticContentForSecondImage.components = [frame.addText()];
                    // this.staticContentForInteractive.components = [frame.addText()];
                    return this;
                };

                return this.addComponent(component);
            },

            addText(options = {}) {
                options.text = options.text || `Text with block-level $$ math $$${guid.generate()}`;
                const component = getTextModel().new(options);
                return this.addComponent(component);
            },

            getModelsByType(type) {
                const components = [];
                this.components.forEach(component => {
                    if (component.type === type) {
                        components.push(component);
                    }
                });
                return components;
            },

            getViewModelsByType(type) {
                const components = [];
                this.components.forEach(component => {
                    if (this.viewModelFor(component).type === type) {
                        components.push(this.viewModelFor(component));
                    }
                });
                return components;
            },

            embedInLesson() {
                return embedFrameInLesson(this);
            },
        });

        Componentized.fixtures = {
            getInstance(attrs = {}) {
                const _attrs = this.sampleAttrs(attrs);
                const frame = embedFrameInLesson(Componentized.new(_attrs));

                frame.mainUiComponent = frame.addVanillaComponent().addDefaultReferences();
                Object.defineProperty(frame.mainUiComponent, 'text_content', {
                    value: 'text_content',
                });

                angular.extend(frame, this.mixin);

                return frame;
            },

            sampleAttrs(attrs) {
                return angular.extend(
                    {
                        frame_type: 'componentized',
                        id: String(Math.random()),
                        components: [],
                    },
                    attrs,
                );
            },

            getNoInteractionInstance() {
                const attrs = {
                    components: [
                        {
                            id: '93dd69a0-8eda-b40d-fbc1-1d54af060373',
                            behaviors: {},
                            component_type: 'ComponentizedFrame.TextImageInteractive',
                            editor_template: 'no_interaction',
                            content_for_text_id: '6460a956-1f92-a389-feb6-2a8ec8d3a8d0',
                            context_image_size: 'tall',
                            context_image_2_size: 'tall',
                        },
                        {
                            id: '6460a956-1f92-a389-feb6-2a8ec8d3a8d0',
                            behaviors: {
                                ProcessesMarkdown: {},
                                ProcessesModals: {},
                                ProcessesInlineImages: {},
                                ProcessesMathjax: {},
                            },
                            component_type: 'ComponentizedFrame.Text',
                            text: 'text',
                        },
                        {
                            id: 'decebd1f-7fde-39d9-3de0-a201baa18b2c',
                            behaviors: {},
                            component_type: 'ComponentizedFrame.AlwaysReadyContinueButton',
                        },
                    ],
                    frame_type: 'componentized',
                    id: 'b792131b-ed6a-607d-7c8f-f6167b1055ad',
                    main_ui_component_id: '93dd69a0-8eda-b40d-fbc1-1d54af060373',
                    continue_button_id: 'decebd1f-7fde-39d9-3de0-a201baa18b2c',
                };
                return Componentized.new(attrs);
            },

            getMultipleChoiceInstance() {
                const attrs = {
                    components: [
                        {
                            id: '9f6ae27d-a8cf-9e95-9886-20fdd564a016',
                            behaviors: {
                                GotoNextOnChallengeComplete: {},
                                CompleteOnAllChallengesComplete: {},
                            },
                            challenge_ids: ['79a0a5c6-dbef-ccc7-05b9-b332c58a5140'],
                            component_type: 'ComponentizedFrame.Challenges',
                            context_image_2_size: 'tall',
                            context_image_size: 'tall',
                            editor_template: 'basic_multiple_choice',
                            layout_id: 'fdd56d57-d5bd-8619-fa11-9cd47e0d89a7',
                            shared_content_for_interactive_id: 'd859e829-92db-a2b6-49e8-86465f2157f1',
                            shared_content_for_text_id: 'b2a3f6e6-88f4-d54b-c49a-6fd2d363acb0',
                        },
                        {
                            id: 'fdd56d57-d5bd-8619-fa11-9cd47e0d89a7',
                            behaviors: {},
                            component_type: 'ComponentizedFrame.TextImageInteractive',
                            target_id: '9f6ae27d-a8cf-9e95-9886-20fdd564a016',
                        },
                        {
                            id: '9c6c5f4d-a2ca-cf1c-6aff-7d5eb073e387',
                            behaviors: {},
                            component_type: 'ComponentizedFrame.ChallengesContinueButton',
                            target_component_id: '9f6ae27d-a8cf-9e95-9886-20fdd564a016',
                        },
                        {
                            id: 'd859e829-92db-a2b6-49e8-86465f2157f1',
                            answer_ids: [
                                '43d76dfc-a366-5250-8ddf-b17201c01b3a',
                                '827fdac3-1d45-7724-28cd-71e5b62566c6',
                            ],
                            behaviors: {},
                            component_type: 'ComponentizedFrame.AnswerList',
                            skin: 'buttons',
                        },
                        {
                            id: '43d76dfc-a366-5250-8ddf-b17201c01b3a',
                            behaviors: {},
                            component_type: 'ComponentizedFrame.SelectableAnswer',
                            text_id: '67773cae-5cc7-6952-bff0-674db4b1aade',
                        },
                        {
                            id: '67773cae-5cc7-6952-bff0-674db4b1aade',
                            behaviors: {
                                ProcessesMarkdown: {},
                                ProcessesModals: {},
                                ProcessesInlineImages: {},
                                ProcessesMathjax: {},
                            },
                            component_type: 'ComponentizedFrame.Text',
                            text: 'a',
                        },
                        {
                            id: '827fdac3-1d45-7724-28cd-71e5b62566c6',
                            behaviors: {},
                            component_type: 'ComponentizedFrame.SelectableAnswer',
                            text_id: '0dcfc117-8e90-2a66-8266-be7897d3410e',
                        },
                        {
                            id: '0dcfc117-8e90-2a66-8266-be7897d3410e',
                            behaviors: {
                                ProcessesMarkdown: {},
                                ProcessesModals: {},
                                ProcessesInlineImages: {},
                                ProcessesMathjax: {},
                            },
                            component_type: 'ComponentizedFrame.Text',
                            text: 'b',
                        },
                        {
                            id: 'b2a3f6e6-88f4-d54b-c49a-6fd2d363acb0',
                            behaviors: {
                                ProcessesMarkdown: {},
                                ProcessesModals: {},
                                ProcessesInlineImages: {},
                                ProcessesMathjax: {},
                            },
                            component_type: 'ComponentizedFrame.Text',
                            modal_ids: [],
                            text: 'text',
                        },
                        {
                            id: '79a0a5c6-dbef-ccc7-05b9-b332c58a5140',
                            answer_list_id: 'd859e829-92db-a2b6-49e8-86465f2157f1',
                            behaviors: {
                                CompleteOnCorrect: {},
                                ClearMessagesOnAnswerSelect: {},
                                DisallowMultipleSelect: {},
                                FlashIncorrectStyling: {},
                                ImmediateValidation: {},
                                ResetAnswersOnActivated: {},
                                ShowCorrectStyling: {},
                            },
                            component_type: 'ComponentizedFrame.MultipleChoiceChallenge',
                            editor_template: 'basic_multiple_choice',
                            validator_id: 'c054427b-833b-28e4-defb-25813b0d0562',
                        },
                        {
                            id: 'c054427b-833b-28e4-defb-25813b0d0562',
                            behaviors: {
                                HasAllExpectedAnswers: {},
                                HasNoUnexpectedAnswers: {},
                            },
                            challenge_id: '79a0a5c6-dbef-ccc7-05b9-b332c58a5140',
                            component_type: 'ComponentizedFrame.ChallengeValidator',
                            expected_answer_matcher_ids: ['fd9e05a0-c857-0b88-4e64-731b3016726e'],
                        },
                        {
                            id: 'fd9e05a0-c857-0b88-4e64-731b3016726e',
                            answer_id: '43d76dfc-a366-5250-8ddf-b17201c01b3a',
                            behaviors: {},
                            component_type: 'ComponentizedFrame.SimilarToSelectableAnswer',
                        },
                    ],
                    continue_button_id: '9c6c5f4d-a2ca-cf1c-6aff-7d5eb073e387',
                    id: 'bf1bfcee-0d76-1a3c-987d-79ad40111f5e',
                    frame_type: 'componentized',
                    main_ui_component_id: '9f6ae27d-a8cf-9e95-9886-20fdd564a016',
                };
                return Componentized.new(attrs);
            },

            getNoIncorrectAnswersMultipleChoiceInstance() {
                const attrs = {
                    components: [
                        {
                            id: 'dd22ee4b-5ddb-4c03-d2fa-0a4dbdd6a1bf',
                            behaviors: {},
                            component_type: 'ComponentizedFrame.FrameNavigator',
                            selectable_answer_navigator_ids: [],
                        },
                        {
                            id: '81b5bcfd-9d55-48ad-ef05-0058718aaec0',
                            behaviors: {
                                GotoNextOnChallengeComplete: {},
                                CompleteOnAllChallengesComplete: {},
                                GotoNextFrameOnComplete: {},
                            },
                            challenge_ids: ['f137cf60-e040-4eea-d485-a5ea844b907c'],
                            component_type: 'ComponentizedFrame.Challenges',
                            editor_template: 'basic_multiple_choice',
                            context_image_size: 'tall',
                            context_image_2_size: 'tall',
                            layout_id: '9b2be7be-f6ec-4c53-e893-ba03cecc97c4',
                            shared_content_for_interactive_id: '7f34462c-14fd-4cc8-b943-fdf89266a091',
                            shared_content_for_text_id: '9d8047ce-c1fc-4e31-bd2c-e477a311ffc8',
                        },
                        {
                            id: '9b2be7be-f6ec-4c53-e893-ba03cecc97c4',
                            behaviors: {},
                            component_type: 'ComponentizedFrame.TextImageInteractive',
                            target_id: '81b5bcfd-9d55-48ad-ef05-0058718aaec0',
                        },
                        {
                            id: '86afed43-07eb-4ba4-c496-ccc66a704b3c',
                            behaviors: {},
                            component_type: 'ComponentizedFrame.ChallengesContinueButton',
                            show_continue_when_ready_to_validate: true,
                            target_component_id: '81b5bcfd-9d55-48ad-ef05-0058718aaec0',
                        },
                        {
                            id: '7f34462c-14fd-4cc8-b943-fdf89266a091',
                            behaviors: {},
                            component_type: 'ComponentizedFrame.AnswerList',
                            skin: 'buttons',
                            answer_ids: [
                                'aff0dcbc-84a1-4c71-9930-ce5c02af605f',
                                'e5820fec-273b-4516-8226-297db9f0388e',
                            ],
                        },
                        {
                            id: 'aff0dcbc-84a1-4c71-9930-ce5c02af605f',
                            behaviors: {},
                            component_type: 'ComponentizedFrame.SelectableAnswer',
                            text_id: '3af92e84-2e2c-4fb2-8316-52e91d58226c',
                        },
                        {
                            id: '3af92e84-2e2c-4fb2-8316-52e91d58226c',
                            behaviors: {
                                ProcessesMarkdown: {},
                                ProcessesModals: {},
                                ProcessesInlineImages: {},
                                ProcessesMathjax: {},
                            },
                            component_type: 'ComponentizedFrame.Text',
                            text: 'a',
                        },
                        {
                            id: 'e5820fec-273b-4516-8226-297db9f0388e',
                            behaviors: {},
                            component_type: 'ComponentizedFrame.SelectableAnswer',
                            text_id: '0183a20d-0cbc-4d0c-cba5-4b407e510c27',
                        },
                        {
                            id: '0183a20d-0cbc-4d0c-cba5-4b407e510c27',
                            behaviors: {
                                ProcessesMarkdown: {},
                                ProcessesModals: {},
                                ProcessesInlineImages: {},
                                ProcessesMathjax: {},
                            },
                            component_type: 'ComponentizedFrame.Text',
                            text: 'b',
                        },
                        {
                            id: '9d8047ce-c1fc-4e31-bd2c-e477a311ffc8',
                            behaviors: {
                                ProcessesMarkdown: {},
                                ProcessesModals: {},
                                ProcessesInlineImages: {},
                                ProcessesMathjax: {},
                            },
                            component_type: 'ComponentizedFrame.Text',
                            text: 'No Incorrect Answers',
                            modal_ids: [],
                        },
                        {
                            id: 'f137cf60-e040-4eea-d485-a5ea844b907c',
                            behaviors: {
                                CompleteOnCorrect: {},
                                ClearMessagesOnAnswerSelect: {},
                                DisallowMultipleSelect: {},
                                FlashIncorrectStyling: {},
                                ReadyToValidateWhenAnswerIsSelected: {},
                                ResetAnswersOnActivated: {},
                                PlayScalingSoundOnSelected: {},
                            },
                            component_type: 'ComponentizedFrame.MultipleChoiceChallenge',
                            editor_template: 'basic_multiple_choice',
                            no_incorrect_answers: true,
                            message_ids: [],
                            validator_id: 'c04c3923-aaf6-4fba-e703-e7742c680459',
                            answer_list_id: '7f34462c-14fd-4cc8-b943-fdf89266a091',
                        },
                        {
                            id: 'c04c3923-aaf6-4fba-e703-e7742c680459',
                            behaviors: {},
                            component_type: 'ComponentizedFrame.ChallengeValidator',
                            expected_answer_matcher_ids: [],
                            challenge_id: 'f137cf60-e040-4eea-d485-a5ea844b907c',
                        },
                        {
                            id: 'f589499c-0c8a-4012-a722-6bb40d9abfdc',
                            behaviors: {},
                            component_type: 'ComponentizedFrame.AnswerList',
                            skin: 'buttons',
                            answer_ids: [],
                        },
                    ],
                    frame_type: 'componentized',
                    id: '2d7ab7d5-a4dd-420b-90fc-0765140d0b48',
                    frame_navigator_id: 'dd22ee4b-5ddb-4c03-d2fa-0a4dbdd6a1bf',
                    main_ui_component_id: '81b5bcfd-9d55-48ad-ef05-0058718aaec0',
                    continue_button_id: '86afed43-07eb-4ba4-c496-ccc66a704b3c',
                };
                return Componentized.new(attrs);
            },
        };

        return {};
    },
]);
