import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/lessons';

angular.module('FrontRoyal.Lessons').factory('LessonProgressFixtures', [
    '$injector',
    $injector => {
        const LessonProgress = $injector.get('LessonProgress');

        LessonProgress.fixtures = {
            sampleAttrs(overrides = {}) {
                return angular.extend(
                    {
                        lesson_id: '700f0a6b-d733-4de3-8ff4-ad1cda796ce4',
                        frame_bookmark_id: 'ddba3178-d76d-4eaa-96ba-2cd47e3378a8',
                        user_id: 1,
                        started_at: '1395345127',
                        complete: false,
                        frame_history: [],
                        completed_frames: {},
                        challenge_scores: {},
                        for_assessment_lesson: false,
                        for_test_lesson: false,
                    },
                    overrides,
                );
            },

            getInstance(overrides) {
                const lessonProgress = LessonProgress.new(this.sampleAttrs(overrides));
                if (!lessonProgress.lesson()) {
                    $injector.get('LessonFixtures');
                    lessonProgress.$$embeddedIn = $injector.get('Lesson').fixtures.getInstance({
                        lesson_id: lessonProgress.lesson_id,
                    });
                }
                return lessonProgress;
            },
        };

        return {};
    },
]);
