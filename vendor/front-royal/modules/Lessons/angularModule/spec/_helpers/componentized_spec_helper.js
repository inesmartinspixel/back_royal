angular.module('FrontRoyal.Lessons').factory('ComponentizedSpecHelper', [
    'SpecHelper',
    'LessonFixtures',
    SpecHelper => ({
        renderEditorDir(viewModel, frameViewModel) {
            const renderer = SpecHelper.renderer();
            renderer.scope.editorViewModel = frameViewModel.frame.editorViewModelFor(viewModel.model);
            renderer.scope.frameViewModel = frameViewModel;
            renderer.render(
                '<cf-component-editor editor-view-model="editorViewModel" frame-view-model="frameViewModel"></cf-component-editor>',
            );
            renderer.editorScope = renderer.elem.find('[editor-view-model]').isolateScope();
            return renderer;
        },

        assertEditorElementPresent(elem, model) {
            const editorEl = this.editorElement(elem, model);
            SpecHelper.expectEqual(true, !!editorEl, `found cf-component-editor for ${model.type}:${model.id}`);
            return editorEl;
        },

        assertEditorElementNotPresent(elem, model) {
            const editorEl = this.editorElement(elem, model);
            SpecHelper.expectEqual(false, !!editorEl, `found cf-component-editor for ${model.type}:${model.id}`);
            return editorEl;
        },

        editorElement(elem, model) {
            let editorEl;
            angular.forEach(elem.find('cf-component-editor'), el => {
                const scope = $(el).isolateScope();
                const modelToEdit = scope.editorViewModel.model;
                if (model === modelToEdit) {
                    if (editorEl) {
                        throw new Error(`More than one editor found for ${model.type}:${model.id}`);
                    } else {
                        editorEl = $(el);
                    }
                }
            });
            return editorEl;
        },
    }),
]);
