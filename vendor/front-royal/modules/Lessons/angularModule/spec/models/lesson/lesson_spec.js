import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/lessons';
import 'Lessons/angularModule/spec/_mock/fixtures/streams';

describe('Lessons::Lesson', () => {
    let $injector;
    let Lesson;
    let LessonProgress;
    let SomeLessonType;
    let SpecHelper;
    let ClientConfig;
    let $q;
    let $timeout;
    let $location;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'SpecHelper');

        angular.mock.inject([
            '$injector',

            _$injector => {
                $injector = _$injector;
                SpecHelper = $injector.get('SpecHelper');
                Lesson = $injector.get('Lesson');
                $injector.get('MockIguana');
                $injector.get('LessonFixtures');
                SomeLessonType = Lesson.subclass();
                LessonProgress = $injector.get('LessonProgress');
                Lesson.setAdapter('Iguana.Mock.Adapter');
                ClientConfig = $injector.get('ClientConfig');
                $q = $injector.get('$q');
                $timeout = $injector.get('$timeout');
                $location = $injector.get('$location');
            },
        ]);
    });

    it('should throw an error if importFromCsv is not implemented', () => {
        const lesson = SomeLessonType.new({});
        expect(lesson.importFromCsv).toThrow(new Error('importFromCsv should be implemented by subclasses of Lesson.'));
    });

    describe('allContentLoaded', () => {
        it('should throw', () => {
            const lesson = SomeLessonType.new({});
            expect(() => {
                lesson.allContentLoaded;
            }).toThrow(new Error('Subclasses of Lesson should define allContentLoaded. "undefined" does not.'));
        });
    });

    describe('chapter', () => {
        it('should return the chapter', () => {
            const lesson = Lesson.fixtures.getInstance();
            const expectedChapter = lesson.stream().chapters[0];
            expect(expectedChapter).not.toBeUndefined();
            expect(expectedChapter.lessonIds.indexOf(lesson.id)).not.toBe(-1);
            expect(lesson.chapter()).toBe(expectedChapter);
        });
    });

    describe('chapterIndex', () => {
        it('should return the chapterIndex', () => {
            const lesson = Lesson.fixtures.getInstance();
            const expectedChapter = lesson.stream().chapters[0];
            expect(expectedChapter).not.toBeUndefined();
            expect(expectedChapter.lessonIds.indexOf(lesson.id)).not.toBe(-1);
            expect(lesson.chapterIndex()).toBe(0);
        });
    });

    describe('complete', () => {
        it('should delegate to lesson progress', () => {
            const lesson = Lesson.fixtures.getInstance();
            lesson.includeProgress();

            // using defineProperty to avoid mocking out a chapter and stuff
            Object.defineProperty(lesson.lesson_progress, 'complete', {
                value: true,
            });
            // expect(lesson.complete).toBe(true);
            // lesson.lesson_progress.complete = false;
            // expect(lesson.complete).toBe(false);
        });
        it('should be false with no lesson progress', () => {
            const lesson = Lesson.fixtures.getInstance();
            lesson.lesson_progress = undefined;
            expect(lesson.complete).toBe(false);
        });
    });

    describe('started', () => {
        it('should be true with lesson progress', () => {
            const lesson = Lesson.fixtures.getInstance();
            lesson.includeProgress();
            expect(lesson.lesson_progress).not.toBeUndefined();
            expect(lesson.started).toBe(true);
        });
        it('should be false with no lesson progress', () => {
            const lesson = Lesson.fixtures.getInstance();
            lesson.lesson_progress = undefined;
            expect(lesson.started).toBe(false);
        });
    });

    describe('launchUrl', () => {
        it('should return a url', () => {
            const lesson = Lesson.fixtures.getInstance();
            expect(lesson.launchUrl).toBe(
                `/course/${lesson.stream().id}/chapter/${lesson.chapterIndex()}/lesson/${lesson.id}/show`,
            );
        });
    });

    describe('standaloneRoute', () => {
        it('should return a url when there is no canonical_url', () => {
            const lesson = Lesson.fixtures.getInstance();
            lesson.entity_metadata = {
                canonical_url: 'path/to/lesson',
            };
            expect(lesson.standaloneRoute).toBe(lesson.entity_metadata.canonical_url);
        });
        it('should return a url when there is a canonical_url', () => {
            const lesson = Lesson.fixtures.getInstance({
                title: 'My Lesson',
                id: 'id',
                entity_metadata: undefined,
            });
            expect(lesson.standaloneRoute).toBe('/lesson/my-lesson/show/id');
        });
    });

    describe('logInfo', () => {
        it('should work with a stream', () => {
            const lesson = Lesson.fixtures.getInstance();
            expect(lesson.stream()).not.toBeUndefined();
            jest.spyOn(lesson.stream(), 'logInfo').mockReturnValue({
                some: 'log info for stream',
            });
            assertLessonLogStuff(lesson, {
                some: 'log info for stream',
            });
        });
        it('should work without a stream', () => {
            const lesson = Lesson.fixtures.getInstance();
            lesson.stream = () => undefined;
            assertLessonLogStuff(lesson);
        });

        function assertLessonLogStuff(lesson, extra) {
            ['lesson_id', 'lesson_version_id', 'title'].forEach(key => {
                SpecHelper.expectEqual(true, !!key, `"${key}" is not undefined.`);
            });

            SpecHelper.expectEqualObjects(
                angular.extend(
                    {
                        lesson_id: lesson.id,
                        lesson_title: lesson.title,
                        lesson_version_id: lesson.version_id,
                        lesson_complete: lesson.complete,
                    },
                    extra,
                ),
                lesson.logInfo(),
            );
        }
    });

    describe('ensureLessonProgress', () => {
        let lesson;

        beforeEach(() => {
            lesson = Lesson.fixtures.getInstance();
            jest.spyOn(LessonProgress, 'startLesson').mockReturnValue('mockLessonProgress');
        });
        it('should return an existing lesson progress', () => {
            lesson.lesson_progress = 'mockLessonProgress';
            expect(lesson.ensureLessonProgress()).toBe(lesson.lesson_progress);
            expect(LessonProgress.startLesson).not.toHaveBeenCalled();
        });
        it('should create a new lesson progress', () => {
            lesson.lesson_progress = undefined;
            const lessonProgress = lesson.ensureLessonProgress();
            expect(lessonProgress).toBe('mockLessonProgress');
            expect(LessonProgress.startLesson).toHaveBeenCalledWith(lesson);
        });
    });
    describe('titleWithTag', () => {
        it('should return a title with a lesson tag prefixed if available', () => {
            const lesson = Lesson.fixtures.getInstance();
            lesson.tag = 'TAG';
            expect(lesson.titleWithTag).toBe(`[TAG] ${lesson.title}`);
        });
        it('should return a title only if no tag is available', () => {
            const lesson = Lesson.fixtures.getInstance();
            expect(lesson.titleWithTag).toBe(lesson.title);
        });
    });
    describe('lastProgressAt', () => {
        it('should be undefined with no lesson_progress', () => {
            const lesson = Lesson.fixtures.getInstance();
            lesson.lesson_progress = undefined;
            expect(lesson.lastProgressAt).toBeUndefined();
        });
        it('should be undefined with no lesson_progress.last_progress_at', () => {
            const lesson = Lesson.fixtures.getInstance().includeProgress();
            lesson.lesson_progress.last_progress_at = undefined;
            expect(lesson.lastProgressAt).toBeUndefined();
        });
        it('should be a Date with lesson_progress.last_progress_at', () => {
            const lesson = Lesson.fixtures.getInstance().includeProgress();
            lesson.lesson_progress.last_progress_at = 12345;
            expect(lesson.lastProgressAt.getTime()).toBe(12345000);
        });
    });

    describe('launchableWithCurrentClient', () => {
        it('should work', () => {
            const lesson = Lesson.fixtures.getInstance();
            lesson.client_requirements.min_allowed_version = 43;
            ClientConfig.current.versionNumber = 42;
            expect(lesson.launchableWithCurrentClient).toBe(false);
            lesson.client_requirements.min_allowed_version = 42;
            expect(lesson.launchableWithCurrentClient).toBe(true);
        });
    });
    describe('launchableWithLatestAvailableVersionOfCurrentClient', () => {
        it('should work', () => {
            const lesson = Lesson.fixtures.getInstance();
            lesson.client_requirements.supported_in_latest_available_version = false;
            expect(lesson.launchableWithLatestAvailableVersionOfCurrentClient).toBe(false);
            lesson.client_requirements.supported_in_latest_available_version = true;
            expect(lesson.launchableWithLatestAvailableVersionOfCurrentClient).toBe(true);
        });
    });
    describe('nextLessonInStream', () => {
        it('should return next lesson in stream', () => {
            const Stream = $injector.get('Lesson.Stream');
            $injector.get('StreamFixtures');
            const stream = Stream.fixtures.getInstance();

            // sanity checks
            expect(stream.chapters[0].lessonIds).toEqual([stream.lessons[0].id, stream.lessons[2].id]);
            expect(stream.chapters[1].lessonIds).toEqual([stream.lessons[1].id]);

            expect(stream.lessons[0].nextLessonInStream).toBe(stream.lessons[2]);
            expect(stream.lessons[2].nextLessonInStream).toBe(stream.lessons[1]);
            expect(stream.lessons[1].nextLessonInStream).toBeUndefined();
        });
        it('should return undefined if no stream', () => {
            const lesson = Lesson.fixtures.getInstance({}, true);
            expect(lesson.stream()).toBeUndefined();
            expect(lesson.nextLessonInStream).toBeUndefined();
        });
    });

    describe('archived', () => {
        it('should remove lesson from locale pack', () => {
            const lesson = Lesson.fixtures.getInstance().addLocalePackFixture();
            lesson.archived = true;
            expect(lesson.locale_pack).toBe(null);
        });
    });

    describe('openPracticeEditor', () => {
        it('should call ensurePracticeLesson and then open in a new window', () => {
            const lesson = Lesson.fixtures.getInstance();
            const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
            jest.spyOn(NavigationHelperMixin, 'loadUrl').mockImplementation(() => {});
            jest.spyOn(lesson, 'ensurePracticeLesson').mockReturnValue($q.when('lessonId'));
            lesson.openPracticeEditor(true);
            $timeout.flush();
            expect(NavigationHelperMixin.loadUrl).toHaveBeenCalledWith(Lesson.editorUrl('lessonId'), '_blank');
        });

        it('should call ensurePracticeLesson and then open in this window', () => {
            const lesson = Lesson.fixtures.getInstance();
            jest.spyOn($location, 'url').mockImplementation(() => {});
            jest.spyOn(lesson, 'ensurePracticeLesson').mockReturnValue($q.when('lessonId'));
            lesson.openPracticeEditor(false);
            $timeout.flush();
            expect($location.url).toHaveBeenCalledWith(Lesson.editorUrl('lessonId'));
        });
    });

    describe('ensurePracticeLesson', () => {
        it('should return the id for an existing practice lesson', () => {
            const lesson = Lesson.fixtures.getInstance();
            lesson.ensureLocalePack();
            lesson.locale_pack.practice_content_items = [
                {
                    locale: lesson.locale,
                    id: 'practice_lesson_id',
                },
            ];
            let returnedPracticeLessonId;
            lesson.ensurePracticeLesson().then(practiceLessonId => {
                returnedPracticeLessonId = practiceLessonId;
            });
            $timeout.flush();
            expect(returnedPracticeLessonId).toEqual('practice_lesson_id');
        });

        it('should create a new practice lesson and return the id', () => {
            const lesson = Lesson.fixtures.getInstance();
            lesson.ensureLocalePack();
            expect(lesson.locale_pack.practice_content_items).toBeUndefined();

            const practiceLessonAttrs = {
                title: `Practice for "${lesson.title}"`,
                locale: lesson.locale,
                lesson_type: 'Lesson',
                id: 'practice_lesson_id',
                locale_pack: {
                    id: 'practice_locale_pack_id',
                },
            };
            Lesson.expect('create')
                .toBeCalledWith(_.pick(practiceLessonAttrs, 'title', 'locale', 'lesson_type'), {
                    is_practice_for_locale_pack_id: lesson.localePackId,
                })
                .returns(practiceLessonAttrs);

            let returnedPracticeLessonId;
            lesson.ensurePracticeLesson().then(practiceLessonId => {
                returnedPracticeLessonId = practiceLessonId;
            });
            Lesson.flush('create');
            expect(returnedPracticeLessonId).toEqual('practice_lesson_id');

            expect(lesson.locale_pack.practice_locale_pack_id).toEqual(practiceLessonAttrs.locale_pack.id);
            expect(lesson.locale_pack.practice_content_items[0]).toEqual({
                id: practiceLessonAttrs.id,
                title: practiceLessonAttrs.title,
                locale: practiceLessonAttrs.locale,
            });
        });
    });
});
