import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Editor/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/componentized_fixtures';
import 'Lessons/angularModule/spec/_mock/fixtures/lessons';

describe('Lessons::FrameList', () => {
    let FrameList;
    let Frame;
    let Componentized;
    let SpecHelper;
    let $injector;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector => {
                $injector = _$injector;
                FrameList = $injector.get('Lesson.FrameList');
                Frame = $injector.get('Lesson.FrameList.Frame');
                Componentized = $injector.get('Lesson.FrameList.Frame.Componentized');
                SpecHelper = $injector.get('SpecHelper');
                $injector.get('LessonFixtures');
                $injector.get('ComponentizedFixtures');
            },
        ]);
    });

    describe('lazy frame loading', () => {
        it('should only instantiate frames when reify is called on them', () => {
            const lesson = FrameList.new({
                frames: [
                    {
                        frame_type: 'Lesson.FrameList.Frame',
                        id: 'ec98ca31-d499-4db9-a3f7-d35856d16f6e',
                    },
                ],
            });

            const frameObj = lesson.frames[0];
            expect(frameObj.constructor).toBe(Object);
            frameObj.reify();
            const frame = lesson.frames[0];
            expect(frame.constructor).toBe(Frame);
            expect(frame.asJson()).toEqual(frameObj);
        });
        it('should also work for practice frames', () => {
            const lesson = FrameList.new({
                practice_frames: [
                    {
                        frame_type: 'Lesson.FrameList.Frame',
                        id: 'ec98ca31-d499-4db9-a3f7-d35856d16f6e',
                    },
                ],
            });

            const frameObj = lesson.practice_frames[0];
            expect(frameObj.constructor).toBe(Object);
            frameObj.reify();
            const frame = lesson.practice_frames[0];
            expect(frame.constructor).toBe(Frame);
            expect(frame.asJson()).toEqual(frameObj);
        });
        it('should not cause any problems if reify is called on a frame instance', () => {
            const lesson = FrameList.new({
                frames: [{}],
            });

            const frame = lesson.frames[0].reify();
            expect(frame.reify()).toBe(frame);
        });
    });

    describe('removeFrames', () => {
        it('should call into removeFrame for each frame provided', () => {
            const lesson = FrameList.fixtures.getInstance();
            expect(lesson.frames.length).toBeGreaterThan(1); // sanity check
            const frames = [lesson.frames[0], lesson.frames[1]];
            jest.spyOn(lesson, 'removeFrame').mockImplementation(() => {});
            lesson.removeFrames(frames);
            expect(lesson.removeFrame).toHaveBeenCalled();
            expect(lesson.removeFrame.mock.calls.length).toEqual(2);
        });
    });

    describe('addFrame', () => {
        it('should add a frame that is passed in', () => {
            const lesson = FrameList.fixtures.getInstance();
            const frame = Frame.new();
            lesson.addFrame(frame);
            expect(lesson.frames[lesson.frames.length - 1]).toBe(frame);
            expect(frame.lesson()).toBe(lesson);
        });

        it('should add a default frame', () => {
            const lesson = FrameList.fixtures.getInstance();
            jest.spyOn(Componentized.prototype, 'applyDefaultEditorTemplate').mockImplementation(() => {});
            const frame = lesson.addFrame();
            expect(lesson.frames[lesson.frames.length - 1]).toBe(frame);
            expect(frame.frame_type).toBe('componentized');
            expect(frame.lesson()).toBe(lesson);
            expect(Componentized.prototype.applyDefaultEditorTemplate).toHaveBeenCalled();
        });

        it('should not apply default editor template when told not to', () => {
            const lesson = FrameList.fixtures.getInstance();
            jest.spyOn(Componentized.prototype, 'applyDefaultEditorTemplate').mockImplementation(() => {});
            const frame = lesson.addFrame(undefined, undefined, false);
            expect(lesson.frames[lesson.frames.length - 1]).toBe(frame);
            expect(frame.frame_type).toBe('componentized');
            expect(frame.lesson()).toBe(lesson);
            expect(Componentized.prototype.applyDefaultEditorTemplate).not.toHaveBeenCalled();
        });
    });

    describe('replaceFrameWithEmptyOne', () => {
        it('should replace a frame with a new, empty one', () => {
            const lesson = FrameList.fixtures.getInstance().reifyAllFrames();
            const oldFrame = lesson.frames[1];
            Frame.prototype.replaceFrameReferences = jest.fn().mockReturnValue(oldFrame);
            lesson.replaceFrameWithEmptyOne(oldFrame);
            expect(lesson.frames[1]).not.toBe(oldFrame);
            // by default we'll get a FrameNavigator after init
            expect(lesson.frames[1].components.length).toEqual(1);
        });

        it('should copy over author comments', () => {
            const author_comments = [
                {
                    text:
                        'OPR: this should be consumable, but I made it sequential since we can nott reorder in consumable.',
                    timestamp: 1390936296612,
                    author_id: 4,
                },
            ];
            const lesson = FrameList.fixtures.getInstance().reifyAllFrames();
            const oldFrame = lesson.frames[1];
            oldFrame.author_comments = author_comments;
            Frame.prototype.replaceFrameReferences = jest.fn().mockReturnValue(oldFrame);
            const newFrame = lesson.replaceFrameWithEmptyOne(oldFrame);
            expect(newFrame.author_comments).toEqual(author_comments);
        });
    });

    describe('allContentLoaded', () => {
        it('should be false if frames is undefined', () => {
            const lesson = FrameList.fixtures.getInstance();
            lesson.frames = undefined;
            expect(lesson.allContentLoaded).toBe(false);
        });

        it('should be false if there are no frames', () => {
            const lesson = FrameList.fixtures.getInstance();
            lesson.frames = [];
            expect(lesson.allContentLoaded).toBe(false);
        });

        it('should be true if there are some frames', () => {
            const lesson = FrameList.fixtures.getInstance();
            expect(lesson.allContentLoaded).toBe(true);
        });
    });

    describe('importFrames', () => {
        it('should call into addFrame for each frame provided', () => {
            const lesson = FrameList.fixtures.getInstance();
            const frames = [Frame.new(), Frame.new()];
            jest.spyOn(lesson, 'addFrame').mockImplementation(() => {});
            lesson.importFrames(frames);
            expect(lesson.addFrame).toHaveBeenCalled();
            expect(lesson.addFrame.mock.calls.length).toEqual(2);
        });

        it('should rebuild guilds for any frames being passed', () => {
            const lesson = FrameList.fixtures.getInstance();
            const frames = [Frame.new(), Frame.new()];
            const originalIds = [frames[0].id, frames[1].id];
            lesson.importFrames(frames);
            const updatedIds = [lesson.frames[0].id, lesson.frames[1].id];
            expect(originalIds[0]).not.toEqual(updatedIds[0]);
            expect(originalIds[1]).not.toEqual(updatedIds[1]);
        });
    });

    describe('duplicate', () => {
        it('should reset frame guids on each frame', () => {
            const lesson = FrameList.fixtures.getInstance();
            jest.spyOn(FrameList, 'create').mockImplementation(params => {
                // should repopulate frame_ids
                expect(params.frames[0].id).not.toBe(lesson.frames[0].id);
                expect(params.frames[1].id).not.toBe(lesson.frames[1].id);
            });
            const toFrame = Componentized.fixtures.getInstance();
            const fromFrame = Componentized.fixtures.getInstance();
            lesson.frames = [fromFrame, toFrame];
            lesson.duplicate({
                title: 'new item',
            });
        });
        it('should update branching references', () => {
            const lesson = FrameList.fixtures.getInstance();
            jest.spyOn(FrameList, 'create').mockImplementation(params => {
                const proxy = FrameList.new(params).reifyAllFrames();
                // should maintain to/from branching references after updating frame_ids
                expect(proxy.frames[0].frameNavigator.selectableAnswerNavigators[0].next_frame_id).toEqual(
                    proxy.frames[1].id,
                );
            });
            const toFrame = Componentized.fixtures.getInstance();
            const fromFrame = Componentized.fixtures.getInstance();
            lesson.frames = [fromFrame, toFrame];

            fromFrame.addSelectableAnswerNavigator();
            fromFrame.frameNavigator.selectableAnswerNavigators[0].next_frame_id = toFrame.id;

            lesson.duplicate({
                title: 'new item',
            });
        });
    });

    describe('duplicateFrame', () => {
        it('should duplicate a frame that is passed in', () => {
            const lesson = FrameList.fixtures.getInstance().reifyAllFrames();
            expect(lesson.frames.length).toBe(3); // sanity check
            // var frame = lesson.$$activeFrame = lesson.frames[1];
            const frame = lesson.frames[1];
            lesson.duplicateFrame(frame);
            expect(lesson.frames.length).toBe(4);
            expect(lesson.frames[2].text_content).toEqual(frame.text_content);
            expect(lesson.frames[2].constructor).toBe(frame.constructor);
            // should regenerate frame guid on duplication
            expect(lesson.frames[2].id).not.toBe(frame.id);
        });
    });

    describe('before save callback', () => {
        it('should call beforeSave on a frame if available ', () => {
            const lesson = FrameList.fixtures.getInstance();
            jest.spyOn(lesson, 'getKeyTerms').mockImplementation(() => {});
            const supportedFrame = lesson.frames[0];
            supportedFrame.beforeSave = jest.fn();
            lesson.runCallbacks('save', () => {});
            expect(supportedFrame.beforeSave).toHaveBeenCalled();
        });

        it('should set key terms', () => {
            const lesson = FrameList.fixtures.getInstance();
            jest.spyOn(lesson, 'getKeyTerms').mockReturnValue(['a', 'b']);
            lesson.key_terms = undefined;
            lesson.runCallbacks('save', () => {});
            expect(lesson.key_terms).toEqual(['a', 'b']);
        });
    });

    describe('frameForId', () => {
        it('should return the frame with the guid', () => {
            const lesson = FrameList.fixtures.getInstance();
            const frame = lesson.frames[1];
            expect(lesson.frameForId(frame.id)).toBe(frame);
        });
        it('should throw if frame not found', () => {
            const lesson = FrameList.fixtures.getInstance();
            expect(() => {
                lesson.frameForId('not found');
            }).toThrow(new Error('No frame found for guid="not found"'));
        });
    });

    describe('createNavigationPointers', () => {
        let lesson;
        beforeEach(() => {
            lesson = FrameList.fixtures.getInstance();
        });

        it('should default to no pointers if no branching', () => {
            const navPointers = lesson.createNavigationPointers();
            expect(navPointers[lesson.frames[0].id].to.length).toBe(0);
            expect(navPointers[lesson.frames[0].id].from.length).toBe(0);
        });
        it('should add pointers for direct links', () => {
            const newComponentizedFrame = Componentized.fixtures.getInstance();
            const frame2 = Componentized.fixtures.getInstance();
            const newPointedToComponentizedFrame = Componentized.fixtures.getInstance();
            newComponentizedFrame.frameNavigator.next_frame_id = newPointedToComponentizedFrame.id;

            lesson.addFrame(newComponentizedFrame);
            lesson.addFrame(newPointedToComponentizedFrame);
            lesson.addFrame(frame2);

            let navPointers = lesson.createNavigationPointers();
            expect(navPointers[newComponentizedFrame.id].to.length).toBe(1);
            expect(navPointers[newComponentizedFrame.id].to[0]).toBe(newPointedToComponentizedFrame.id);
            expect(navPointers[newPointedToComponentizedFrame.id].from.length).toBe(1);
            expect(navPointers[newPointedToComponentizedFrame.id].from[0]).toBe(newComponentizedFrame.id);

            // if the frame guid on a direct link is invalid, there should not be a pointer
            newComponentizedFrame.frameNavigator.next_frame_id = 'cannot_find_this_one';
            navPointers = lesson.createNavigationPointers();
            expect(navPointers[newComponentizedFrame.id].to.length).toBe(0);
            expect(navPointers[frame2.id].from.length).toBe(0);
        });

        it('should add pointers for answer specific links', () => {
            const newComponentizedFrame = Componentized.fixtures.getInstance();
            const frame2 = Componentized.fixtures.getInstance();
            const newPointedToComponentizedFrame = Componentized.fixtures.getInstance();
            const selectableAnswerNavigator = newComponentizedFrame.addSelectableAnswerNavigator();
            selectableAnswerNavigator.next_frame_id = newPointedToComponentizedFrame.id;

            lesson.addFrame(newComponentizedFrame);
            lesson.addFrame(frame2);
            lesson.addFrame(newPointedToComponentizedFrame);

            let navPointers = lesson.createNavigationPointers();
            expect(navPointers[newComponentizedFrame.id].to.length).toBe(1);
            expect(navPointers[newComponentizedFrame.id].to[0]).toBe(newPointedToComponentizedFrame.id);
            expect(navPointers[newPointedToComponentizedFrame.id].from.length).toBe(1);
            expect(navPointers[newPointedToComponentizedFrame.id].from[0]).toBe(newComponentizedFrame.id);

            // if the frame guid on an answer-specfic link is invalid, the pointer should default
            // to the next frame
            selectableAnswerNavigator.next_frame_id = 'cannot_find_this_one';
            navPointers = lesson.createNavigationPointers();
            expect(navPointers[newComponentizedFrame.id].to.length).toBe(1);
            expect(navPointers[newComponentizedFrame.id].to[0]).toBe(frame2.id);
            expect(navPointers[frame2.id].from.length).toBe(1);
            expect(navPointers[frame2.id].from[0]).toBe(newComponentizedFrame.id);
        });

        it('should add pointers for Next Frame links', () => {
            const newComponentizedFrame = Componentized.fixtures.getInstance();
            const newPointedToComponentizedFrame = Componentized.fixtures.getInstance();
            const selectableAnswerNavigator = newComponentizedFrame.addSelectableAnswerNavigator();
            selectableAnswerNavigator.next_frame_id = undefined;

            lesson.addFrame(newComponentizedFrame);
            lesson.addFrame(newPointedToComponentizedFrame);

            const navPointers = lesson.createNavigationPointers();
            expect(navPointers[newComponentizedFrame.id].to.length).toBe(1);
            expect(navPointers[newComponentizedFrame.id].to[0]).toBe(newPointedToComponentizedFrame.id);
            expect(navPointers[newPointedToComponentizedFrame.id].from.length).toBe(1);
            expect(navPointers[newPointedToComponentizedFrame.id].from[0]).toBe(newComponentizedFrame.id);
        });
    });

    describe('approxLessonMinutes', () => {
        let lesson;
        beforeEach(() => {
            lesson = FrameList.fixtures.getInstance();
            lesson.frames = [];
            // 3 frames
            lesson.addFrame();
            lesson.addFrame();
            lesson.addFrame();
            lesson.frame_count = 3;
        });

        it('should estimate time for a lesson', () => {
            expect(lesson.approxLessonMinutes).toBeCloseTo((3 * 21) / 60.0, 3);
        });

        it('should estimate zero time for an empty lesson', () => {
            lesson.frames = [];
            lesson.frame_count = 0;
            expect(lesson.approxLessonMinutes).toEqual(0);
        });
    });

    describe('possiblePaths', () => {
        let lesson;
        beforeEach(() => {
            lesson = FrameList.fixtures.getInstance();
            lesson.frames = [];
        });

        it('should work without branching', () => {
            lesson.addFrame();
            lesson.addFrame();
            SpecHelper.expectEqual(
                [
                    {
                        loops: false,
                        frames: lesson.frames,
                    },
                ],
                lesson.possiblePaths,
            );
        });

        it('should handle a loop', () => {
            const newComponentizedFrame = Componentized.fixtures.getInstance();
            const newPointedToComponentizedFrame = Componentized.fixtures.getInstance();
            newComponentizedFrame.frameNavigator.next_frame_id = newPointedToComponentizedFrame.id;

            lesson.addFrame(newPointedToComponentizedFrame);
            lesson.addFrame(newComponentizedFrame);

            SpecHelper.expectEqual(
                [
                    {
                        loops: true,
                        frames: [newPointedToComponentizedFrame, newComponentizedFrame, newPointedToComponentizedFrame],
                    },
                ],
                lesson.possiblePaths,
            );
        });

        it('should handle branching', () => {
            lesson.addFrame();
            lesson.addFrame();
            lesson.addFrame();

            const pointers = {};
            pointers[lesson.frames[0].id] = {
                to: [lesson.frames[1].id, lesson.frames[2].id],
            };
            pointers[lesson.frames[1].id] = {
                to: [],
                from: [lesson.frames[0].id],
            };
            pointers[lesson.frames[2].id] = {
                to: [],
                from: [lesson.frames[0].id],
            };
            jest.spyOn(lesson, 'createNavigationPointers').mockReturnValue(pointers);

            SpecHelper.expectEqual(
                [
                    {
                        loops: false,
                        frames: [lesson.frames[0], lesson.frames[2]],
                    },
                    {
                        loops: false,
                        frames: [lesson.frames[0], lesson.frames[1], lesson.frames[2]],
                    },
                ],
                lesson.possiblePaths,
            );
        });
    });

    describe('shortestPathLength', () => {
        it('give shortest path length', () => {
            const lesson = FrameList.fixtures.getInstance();
            Object.defineProperty(lesson, 'possiblePaths', {
                value: [
                    {
                        frames: [{}, {}, {}],
                    },
                    {
                        frames: [{}, {}],
                    },
                    {
                        frames: [{}, {}, {}, {}],
                    },
                ],
            });

            expect(lesson.shortestPathLength).toBe(2);
        });
    });

    describe('getKeyTerms', () => {
        it('should return keyTerms', () => {
            const lesson = FrameList.fixtures.getInstance();
            lesson.frames = lesson.frames.slice(0, 2);
            jest.spyOn(Frame.prototype, 'getKeyTerms').mockImplementation(function () {
                return [`term-${this.index()}`];
            });
            expect(lesson.getKeyTerms()).toEqual(['term-0', 'term-1']);
        });
    });
});
