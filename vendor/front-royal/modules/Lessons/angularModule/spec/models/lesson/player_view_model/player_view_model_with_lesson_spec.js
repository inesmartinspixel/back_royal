import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/lessons';
import setSpecLocales from 'Translation/setSpecLocales';
import lessonPlayerViewModelLocales from 'Lessons/locales/lessons/models/lesson/lesson_player_view_model-en.json';

setSpecLocales(lessonPlayerViewModelLocales);

describe('Lessons::Lesson::PlayerViewModelWithLesson', () => {
    let LessonProgress;
    let Lesson;
    let EventLogger;
    let lesson;
    let playerViewModel;
    let AppHeaderViewModel;
    let streamViewModel;
    let SpecHelper;
    let $injector;
    let $rootScope;
    let $q;
    let SessionTracker;
    let Stream;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector => {
                $injector = _$injector;
                LessonProgress = $injector.get('LessonProgress');
                Lesson = $injector.get('Lesson');
                EventLogger = $injector.get('EventLogger');
                Lesson.setAdapter('Iguana.Mock.Adapter');
                AppHeaderViewModel = $injector.get('Navigation.AppHeader.AppHeaderViewModel');
                SpecHelper = $injector.get('SpecHelper');
                $q = $injector.get('$q');
                SessionTracker = $injector.get('SessionTracker');
                Stream = $injector.get('Lesson.Stream');

                $rootScope = $injector.get('$rootScope');
                $rootScope.currentUser = SpecHelper.stubCurrentUser('learner');

                $injector.get('MockIguana');
                $injector.get('LessonFixtures');

                // lesson fixtures mocks out a stub PlayerViewModel subclass
                // that includes PlayerViewModelWithLesson and nothing else
                lesson = Lesson.fixtures.getInstance();
                lesson.includeProgress();
                streamViewModel = lesson.stream().createStreamViewModel();
                streamViewModel.activeLesson = lesson;
                playerViewModel = streamViewModel.activePlayerViewModel;
            },
        ]);
    });

    describe('callbacks', () => {
        it('should set the stream cache on destroy', () => {
            jest.spyOn(Stream, 'setCache').mockImplementation(() => {});
            const stream = playerViewModel.stream;
            playerViewModel.destroy();
            expect(Stream.setCache).toHaveBeenCalledWith(stream);
        });
    });

    describe('newSummaryUnlocks', () => {
        let summary1;
        let summary2;

        describe('get', () => {
            beforeEach(() => {
                summary1 = {
                    id: '1',
                    title: 'summary1',
                    lessons: [playerViewModel.stream.lessons[0].id, playerViewModel.stream.lessons[1].id],
                };

                summary2 = {
                    id: '2',
                    title: 'summary2',
                    lessons: [playerViewModel.stream.lessons[3].id],
                };

                playerViewModel.stream.summaries = [summary1, summary2];
                lesson.stream.summaries = playerViewModel.stream.summaries;
                streamViewModel.stream.summaries = playerViewModel.stream.summaries;

                playerViewModel._buildNewSummaryUnlocks(); // workaround test setup complexity

                jest.spyOn(EventLogger.prototype, 'log').mockImplementation(() => {});
                LessonProgress.expect('save');
                jest.spyOn(LessonProgress.prototype, 'save');
            });

            it('should only display summaries that will be completed by the current lesson', () => {
                expect(playerViewModel.newSummaryUnlocks).toEqual([summary2]);
            });

            it('should not contain any previously completed summaries', () => {
                playerViewModel.started = true;
                playerViewModel.completed = true;
                playerViewModel._buildNewSummaryUnlocks(); // workaround test setup complexity
                expect(playerViewModel.newSummaryUnlocks).toEqual([]);
            });
        });
    });

    describe('completed', () => {
        describe('set', () => {
            beforeEach(() => {
                LessonProgress.expect('create');
                jest.spyOn(lesson.lesson_progress, 'save');

                // FIXME: this can be removed when we stop calling getScore in _logLessonComplete
                jest.spyOn(lesson, 'getScore').mockReturnValue(1);
                playerViewModel._started = true;
            });

            it('should set the value of completed', () => {
                jest.spyOn(playerViewModel, '_preloadCertificateImage').mockImplementation(() => {});
                playerViewModel.completed = true;
                expect(playerViewModel.completed).toBe(true);
                expect(playerViewModel._preloadCertificateImage).toHaveBeenCalled();
            });
            it('should update and save the lesson progress when switching from false to true', () => {
                jest.spyOn(lesson.stream(), 'setCompleteIfAllLessonsComplete').mockImplementation(() => {});
                playerViewModel.completed = true;
                expect(lesson.lesson_progress.complete).toBe(true);
                expect(lesson.stream().setCompleteIfAllLessonsComplete).toHaveBeenCalled();
                expect(lesson.lesson_progress.save).toHaveBeenCalledWith(lesson.stream().lesson_streams_progress);
            });
            it('should not update any lesson_progress when value is not changing', () => {
                playerViewModel._completed = false;
                playerViewModel.completed = false;
                expect(lesson.lesson_progress.save).not.toHaveBeenCalled();
            });
            it('should log finish but not complete on a failed smartcase', () => {
                playerViewModel.lesson.assessment = true;
                const score = 0.42;
                jest.spyOn(playerViewModel.lesson.ensureLessonProgress(), 'restart').mockImplementation(() => {});
                jest.spyOn(playerViewModel.lesson, 'getScore').mockReturnValue(score);
                Object.defineProperty(playerViewModel, 'lessonScore', {
                    value: score,
                });
                Object.defineProperty(playerViewModel, 'scoreMetadata', {
                    value: {
                        a: 1,
                    },
                });
                expect(playerViewModel.$$showFinishScreen).toBe(false);

                jest.spyOn(EventLogger.prototype, 'log').mockImplementation(() => {});
                playerViewModel.completed = true;

                const loggedEventTypes = _.pluck(EventLogger.prototype.log.mock.calls, 0);
                expect(loggedEventTypes).toEqual(['lesson:finish']);
                expect(EventLogger.prototype.log.mock.calls[0][1].score).toBe(score);
                expect(EventLogger.prototype.log.mock.calls[0][1].lesson_passed).toBe(false);
                expect(EventLogger.prototype.log.mock.calls[0][1].score_metadata).toEqual({
                    a: 1,
                });
                expect(playerViewModel.lesson.lesson_progress.restart).toHaveBeenCalled();
            });
            it('should log finish and complete on a passed smartcase', () => {
                playerViewModel.lesson.assessment = true;
                jest.spyOn(playerViewModel.lesson.ensureLessonProgress(), 'restart').mockImplementation(() => {});
                Object.defineProperty(playerViewModel, 'lessonScore', {
                    value: 1,
                });
                expect(playerViewModel.$$showFinishScreen).toBe(false);

                jest.spyOn(EventLogger.prototype, 'log').mockImplementation(() => {});
                playerViewModel.completed = true;

                const loggedEventTypes = _.pluck(EventLogger.prototype.log.mock.calls, 0);
                expect(loggedEventTypes).toEqual(['lesson:finish', 'lesson:complete']);
                expect(EventLogger.prototype.log.mock.calls[0][1].score).toBe(1);
                expect(EventLogger.prototype.log.mock.calls[0][1].lesson_passed).toBe(true);
                expect(playerViewModel.lesson.lesson_progress.restart).toHaveBeenCalled();
            });
            it('should log finish and complete on a normal lesson', () => {
                playerViewModel.lesson.assessment = false;
                jest.spyOn(playerViewModel.lesson.ensureLessonProgress(), 'restart').mockImplementation(() => {});
                Object.defineProperty(playerViewModel, 'lessonScore', {
                    value: 1,
                });
                expect(playerViewModel.$$showFinishScreen).toBe(false);

                jest.spyOn(EventLogger.prototype, 'log').mockImplementation(() => {});
                playerViewModel.completed = true;

                const loggedEventTypes = _.pluck(EventLogger.prototype.log.mock.calls, 0);
                expect(loggedEventTypes).toEqual(['lesson:finish', 'lesson:complete']);
                expect(EventLogger.prototype.log.mock.calls[0][1].score).toBe(1);
                expect(playerViewModel.lesson.lesson_progress.restart).not.toHaveBeenCalled();
            });
            it('should update best score if there is none', () => {
                const lessonProgress = playerViewModel.lesson.ensureLessonProgress();
                delete lessonProgress.best_score;
                Object.defineProperty(playerViewModel, 'lessonScore', {
                    value: 0.42,
                });
                jest.spyOn(EventLogger.prototype, 'log').mockImplementation(() => {});
                playerViewModel.completed = true;
                expect(lessonProgress.best_score).toBe(0.42);
            });
            it('should update best score if the new one is better', () => {
                const lessonProgress = playerViewModel.lesson.ensureLessonProgress();
                lessonProgress.best_score = 0.42;
                Object.defineProperty(playerViewModel, 'lessonScore', {
                    value: 0.84,
                });
                jest.spyOn(EventLogger.prototype, 'log').mockImplementation(() => {});
                playerViewModel.completed = true;
                expect(lessonProgress.best_score).toBe(0.84);
            });
            it('should not update best score if the new one is worse', () => {
                const lessonProgress = playerViewModel.lesson.ensureLessonProgress();
                lessonProgress.best_score = 0.42;
                Object.defineProperty(playerViewModel, 'lessonScore', {
                    value: 0.21,
                });
                jest.spyOn(EventLogger.prototype, 'log').mockImplementation(() => {});
                playerViewModel.completed = true;
                expect(lessonProgress.best_score).toBe(0.42);
            });
            it('should not update best score if there is no score', () => {
                const lessonProgress = playerViewModel.lesson.ensureLessonProgress();
                lessonProgress.best_score = 0.42;
                Object.defineProperty(playerViewModel, 'lessonScore', {
                    value: null,
                });
                jest.spyOn(EventLogger.prototype, 'log').mockImplementation(() => {});
                playerViewModel.completed = true;
                expect(lessonProgress.best_score).toBe(0.42);
            });
        });
    });

    describe('started', () => {
        describe('set', () => {
            const response = {};

            beforeEach(() => {
                jest.spyOn(EventLogger.prototype, 'log').mockImplementation(() => {});
                jest.spyOn(LessonProgress.prototype, 'save').mockReturnValue($q.when(response));
            });

            it('should set started and save lesson progress', () => {
                expect(playerViewModel.started).toBe(false);
                playerViewModel.started = true;
                expect(playerViewModel.started).toBe(true);
                expect(playerViewModel.lessonProgress.save).toHaveBeenCalled();
            });
            it('should set streamProgress.lesson_bookmark_id ', () => {
                const streamProgress = playerViewModel.stream.lesson_streams_progress;
                expect(streamProgress.lesson_bookmark_id).not.toBe(playerViewModel.lesson.id);

                playerViewModel.started = true;

                expect(streamProgress.lesson_bookmark_id).toBe(playerViewModel.lesson.id);
            });
            it('should set retaking to true if lesson is already completed', () => {
                jest.spyOn(playerViewModel.lesson, 'progressStatus').mockReturnValue('completed');
                playerViewModel.started = true;
                expect(playerViewModel.retaking).toBe(true);
            });
            it('should leave retaking to false if lesson is not completed', () => {
                jest.spyOn(playerViewModel.lesson, 'progressStatus').mockReturnValue('in_progress');
                playerViewModel.started = true;
                expect(playerViewModel.retaking).toBe(false);
            });
            it('should update bookmarks if a new stream is started and not found in bookmarks already', () => {
                const existingBookmarks = [];
                const newBookmarks = [
                    {
                        id: playerViewModel.stream.localePackId,
                    },
                ];

                $rootScope.currentUser.favorite_lesson_stream_locale_packs = existingBookmarks;
                response.meta = {
                    favorite_lesson_stream_locale_packs: newBookmarks,
                };
                jest.spyOn(playerViewModel.lesson, 'progressStatus').mockReturnValue(undefined);
                playerViewModel.started = true;
                $rootScope.$digest();
                expect($rootScope.currentUser.favorite_lesson_stream_locale_packs).toBe(newBookmarks); // equality to check re-assignment
                const LearnerContentCache = $injector.get('LearnerContentCache');
                jest.spyOn(LearnerContentCache, 'preloadStudentDashboard').mockImplementation(() => {});
                $injector.get('$timeout').flush();
                expect(LearnerContentCache.preloadStudentDashboard).toHaveBeenCalled();
            });
            it('should not update bookmarks if a new stream is started but prexists as a bookmark already', () => {
                const existingBookmarks = [
                    {
                        id: playerViewModel.stream.localePackId,
                    },
                ];

                $rootScope.currentUser.favorite_lesson_stream_locale_packs = existingBookmarks;
                response.meta = {
                    favorite_lesson_stream_locale_packs: [
                        {
                            id: playerViewModel.stream.localePackId,
                        },
                    ],
                };
                jest.spyOn(playerViewModel.lesson, 'progressStatus').mockReturnValue(undefined);
                playerViewModel.started = true;
                $rootScope.$digest();
                expect($rootScope.currentUser.favorite_lesson_stream_locale_packs).toBe(existingBookmarks); // equality to check re-assignment
                const LearnerContentCache = $injector.get('LearnerContentCache');
                jest.spyOn(LearnerContentCache, 'preloadStudentDashboard').mockImplementation(() => {});
                $injector.get('$timeout').flush();
                expect(LearnerContentCache.preloadStudentDashboard).not.toHaveBeenCalled();
            });
            it('should update bookmarks when append_to_favorite_lesson_stream_locale_packs is set', () => {
                const existingBookmarks = [
                    {
                        id: 'existing_bookmark',
                    },
                ];
                const newBookmarks = [
                    {
                        id: playerViewModel.stream.localePackId,
                    },
                ];

                $rootScope.currentUser.favorite_lesson_stream_locale_packs = existingBookmarks;
                response.meta = {
                    append_to_favorite_lesson_stream_locale_packs: newBookmarks,
                };
                jest.spyOn(playerViewModel.lesson, 'progressStatus').mockReturnValue(undefined);
                playerViewModel.started = true;
                $rootScope.$digest();
                expect($rootScope.currentUser.favorite_lesson_stream_locale_packs).toEqual(
                    existingBookmarks.concat(newBookmarks),
                );
                const LearnerContentCache = $injector.get('LearnerContentCache');
                jest.spyOn(LearnerContentCache, 'preloadStudentDashboard').mockImplementation(() => {});
                $injector.get('$timeout').flush();
                expect(LearnerContentCache.preloadStudentDashboard).toHaveBeenCalled();
            });
            it('should not update bookmarks if append_to_favorite_lesson_stream_locale_packs includes an entry that prexists as a bookmark already', () => {
                const existingBookmarks = [
                    {
                        id: playerViewModel.stream.localePackId,
                    },
                ];

                $rootScope.currentUser.favorite_lesson_stream_locale_packs = existingBookmarks;
                response.meta = {
                    append_to_favorite_lesson_stream_locale_packs: [
                        {
                            id: playerViewModel.stream.localePackId,
                        },
                    ],
                };
                jest.spyOn(playerViewModel.lesson, 'progressStatus').mockReturnValue(undefined);
                playerViewModel.started = true;
                $rootScope.$digest();
                expect($rootScope.currentUser.favorite_lesson_stream_locale_packs).toEqual([
                    {
                        id: playerViewModel.stream.localePackId,
                    },
                ]);
                const LearnerContentCache = $injector.get('LearnerContentCache');
                jest.spyOn(LearnerContentCache, 'preloadStudentDashboard').mockImplementation(() => {});
                $injector.get('$timeout').flush();
                expect(LearnerContentCache.preloadStudentDashboard).not.toHaveBeenCalled();
            });
            it('should not attempt to update bookmarks if no currentUser', () => {
                $rootScope.currentUser = null;

                const LearnerContentCache = $injector.get('LearnerContentCache');
                jest.spyOn(LearnerContentCache, 'preloadStudentDashboard').mockImplementation(() => {});
                $injector.get('$timeout').flush();
                expect(LearnerContentCache.preloadStudentDashboard).not.toHaveBeenCalled();
            });
            describe('with logProgress=false', () => {
                it('should log lesson:play but nothing else', () => {
                    playerViewModel.logProgress = false;
                    playerViewModel.started = true;
                    expect(EventLogger.prototype.log).toHaveBeenCalledWith(
                        'lesson:play',
                        playerViewModel.logInfo(),
                        {},
                    );
                });
            });
            describe('with a stream', () => {
                it('should set lesson_bookmark_id', () => {
                    expect(playerViewModel.stream).not.toBeUndefined(); // sanity check
                    const bookmark =
                        playerViewModel.stream.streamProgress &&
                        playerViewModel.stream.streamProgress.lesson_bookmark_id;
                    expect(bookmark).not.toEqual(playerViewModel.lesson.id);
                    playerViewModel.started = true;
                    expect(playerViewModel.stream.lesson_streams_progress.lesson_bookmark_id).toEqual(
                        playerViewModel.lesson.id,
                    );
                });
            });
            describe('without a stream', () => {
                it('should just log to EventLogger', () => {
                    playerViewModel.lesson.$$embeddedIn = undefined;
                    expect(playerViewModel.stream).toBeUndefined(); // sanity check
                    playerViewModel.started = true;
                    expect(EventLogger.prototype.log).toHaveBeenCalledWith(
                        'lesson:play',
                        playerViewModel.logInfo(),
                        {},
                    );
                });
            });
        });
    });

    describe('logInfo', () => {
        it('should work with a streamViewModel', () => {
            expect(playerViewModel.id).not.toBeUndefined();
            jest.spyOn(playerViewModel.lesson, 'logInfo').mockReturnValue({
                some: 'log info for lesson',
            });
            expect(playerViewModel.logInfo()).toEqual({
                some: 'log info for lesson',
                lesson_play_id: playerViewModel.id,
                editorMode: false,
                previewMode: false,
                demoMode: false,
                lesson_player_session_id: playerViewModel.sessionId,
            });
        });

        it('should work without a streamViewModel', () => {
            Object.defineProperty(playerViewModel, 'streamViewModel', {
                value: undefined,
            });
            expect(playerViewModel.id).not.toBeUndefined();
            jest.spyOn(playerViewModel.lesson, 'logInfo').mockReturnValue({
                some: 'log info for lesson',
            });
            expect(playerViewModel.logInfo()).toEqual({
                some: 'log info for lesson',
                lesson_play_id: playerViewModel.id,
                editorMode: false,
                previewMode: false,
                demoMode: false,
                lesson_player_session_id: playerViewModel.sessionId,
            });
        });
    });

    describe('linked_to_app_header_view_model callback', () => {
        it('should set some values', () => {
            const chapter = playerViewModel.lesson.chapter();
            const index = chapter.lessonIds.indexOf(playerViewModel.lesson.id);
            const count = chapter.lessonIds.length;

            playerViewModel.linkToWindow($injector);
            expect(AppHeaderViewModel.playerViewModel).toBe(playerViewModel);
            expect(AppHeaderViewModel.lastPlayedStream).toBe(playerViewModel.stream);
            expect(AppHeaderViewModel.textRows).toEqual([
                // chapter.title,
                `Lesson ${index + 1} of ${count}`,
            ]);

            // should also delink
            playerViewModel.delinkFromWindows();
            expect(AppHeaderViewModel.playerViewModel).toBeUndefined();
            expect(AppHeaderViewModel.textRows).toBeUndefined();
        });
    });

    describe('moveOnAfterComplete', () => {
        let $location;
        let ContentAccessHelper;
        beforeEach(() => {
            $location = $injector.get('$location');
            jest.spyOn($location, 'url').mockImplementation(() => {});
            ContentAccessHelper = $injector.get('ContentAccessHelper');
        });

        it('should prevent double-clicks', () => {
            const spy = jest.fn().mockReturnValue(true);
            Object.defineProperty(playerViewModel, 'lessonFailed', {
                get: spy,
            });
            playerViewModel.moveOnAfterComplete();
            playerViewModel.moveOnAfterComplete();
            expect(spy.mock.calls.length).toEqual(1);
        });

        it('should navigate to next lesson in stream if available', () => {
            const $timeout = $injector.get('$timeout');
            playerViewModel.showFinishScreen = true;
            jest.spyOn(ContentAccessHelper, 'canLaunch').mockReturnValue(true);
            playerViewModel.moveOnAfterComplete();
            expect(playerViewModel.showFinishScreen).toBe(false);
            $timeout.flush();
            expect($location.url).toHaveBeenCalledWith(lesson.nextLessonInStream.launchUrl);
        });

        it('should navigate to completed if stream completed', () => {
            LessonProgress.expect('create');
            lesson.stream().ensureStreamProgress();
            lesson.stream().lesson_streams_progress.complete = false;
            playerViewModel = lesson.createPlayerViewModel({
                logProgress: true,
            }); // recreate a new playerViewModel now that stream status is set up

            jest.spyOn(playerViewModel.stream, 'setCompleteIfAllLessonsComplete').mockImplementation(() => {
                playerViewModel.stream.lesson_streams_progress.complete = true;
            });

            playerViewModel._logLessonComplete();
            playerViewModel.moveOnAfterComplete();
            expect($location.url).toHaveBeenCalledWith(`/course/${playerViewModel.stream.id}/completed`);
        });

        // https://trello.com/c/GaGSBVr5/479-bug-adding-a-new-lesson-to-an-existing-stream-causes-learners-who-previously-completed-it-to-see-the-course-complete-screen-agai
        it('should not navigate to stream completed if stream was already complete', () => {
            LessonProgress.expect('create');
            lesson.stream().ensureStreamProgress();
            lesson.stream().lesson_streams_progress.complete = true;
            playerViewModel = lesson.createPlayerViewModel({
                logProgress: true,
            }); // recreate a new playerViewModel now that stream status is set up

            // since stream progress was true before doing this lesson, we
            // should not log stream success
            playerViewModel._logLessonComplete();
            playerViewModel.moveOnAfterComplete();
            expect($location.url).not.toHaveBeenCalledWith(`/course/${playerViewModel.stream.id}/completed`);
        });

        it('should navigate to completed if screen completed even if there is a next lesson', () => {
            jest.spyOn(lesson, 'stream').mockReturnValue({
                id: 123,
                orderedLessons: [lesson, {}],
                progressStatus() {
                    return 'completed';
                },
            });

            playerViewModel.moveOnAfterComplete();
            expect($location.url).toHaveBeenCalledWith('/course/123/completed');
        });

        it('should navigate to dashboard if within a stream but not complete and without a next lesson', () => {
            jest.spyOn(lesson, 'stream').mockReturnValue({
                orderedLessons: [],
                progressStatus() {
                    return 'in_progress';
                },
                streamDashboardPath: '/dashboard',
            });

            playerViewModel.moveOnAfterComplete();
            expect($location.url).toHaveBeenCalledWith('/dashboard');
        });

        it('should navigate to stream dashboard if within a stream but next lesson not launchable', () => {
            jest.spyOn(ContentAccessHelper, 'canLaunch').mockReturnValue(false);
            playerViewModel.moveOnAfterComplete();
            expect($location.url).toHaveBeenCalledWith(playerViewModel.lesson.stream().streamDashboardPath);
        });

        it('should navigate to library view if within a daily lesson', () => {
            jest.spyOn(lesson, 'stream').mockReturnValue(undefined);
            playerViewModel.moveOnAfterComplete();
            expect($location.url).toHaveBeenCalledWith('/library');
        });
    });

    describe('assessment', () => {
        it('should delegate to lesson', () => {
            lesson.assessment = true;
            expect(playerViewModel.assessment).toBe(true);
            lesson.assessment = false;
            expect(playerViewModel.assessment).toBe(false);
        });
    });

    describe('hasLesson', () => {
        it('should be true', () => {
            expect(playerViewModel.hasLesson).toBe(true);
        });
    });

    describe('sessionId', () => {
        it('should work when not in editor mode', () => {
            jest.spyOn(SessionTracker, 'pingCurrentSession');
            expect(playerViewModel.sessionId).not.toBeUndefined();
            expect(SessionTracker.pingCurrentSession).toHaveBeenCalledWith(
                `lessonPlayer:${playerViewModel.lesson.id}:${playerViewModel.lesson.stream().id}`,
                $injector.get('PLAYER_SESSION_EXPIRY_MINUTES') * 60 * 1000,
            );
        });

        it('should work in editor mode', () => {
            jest.spyOn(SessionTracker, 'pingCurrentSession');
            playerViewModel.editorMode = true;
            // check that it works without a stream
            playerViewModel.lesson.$$embeddedIn = null;
            expect(playerViewModel.lesson.stream()).toBe(null);
            expect(playerViewModel.sessionId).not.toBeUndefined();
            expect(SessionTracker.pingCurrentSession).toHaveBeenCalledWith(
                `lessonEditor:${playerViewModel.lesson.id}:`,
                5 * 60 * 1000,
            );
        });
    });

    describe('with unlaunchable lesson', () => {
        it('should turn off logProgress if !canLaunch', () => {
            const ContentAccessHelper = $injector.get('ContentAccessHelper');
            jest.spyOn(ContentAccessHelper, 'canLaunch').mockReturnValue(false);
            playerViewModel = lesson.createPlayerViewModel({
                logProgress: true,
            });
            expect(playerViewModel.logProgress).toBe(false);
        });
    });

    describe('clientStorage', () => {
        it('should work', () => {
            playerViewModel = lesson.createPlayerViewModel();
            playerViewModel.setLessonClientStorage(['some', 'thing'], 'is awesome');
            expect(playerViewModel.getLessonClientStorage(['some', 'thing'])).toEqual('is awesome');

            // It should be accessible by another playerViewModel for the same lesson
            playerViewModel = lesson.createPlayerViewModel();
            expect(playerViewModel.getLessonClientStorage(['some', 'thing'])).toEqual('is awesome');

            // It should not be accessible by a playerViewModel for another lesson
            const anotherLesson = Lesson.fixtures.getInstance();
            const anotherLessonPlayerViewModel = anotherLesson.createPlayerViewModel();
            expect(anotherLessonPlayerViewModel.getLessonClientStorage(['some', 'thing'])).toBeUndefined();

            // It should be cleared out when the lesson is finished
            playerViewModel._logLessonComplete();
            expect(playerViewModel.getLessonClientStorage(['some', 'thing'])).toBeUndefined();
            playerViewModel = lesson.createPlayerViewModel();
            expect(playerViewModel.getLessonClientStorage(['some', 'thing'])).toBeUndefined();
        });

        it('should successfully parse an object', () => {
            const ClientStorage = $injector.get('ClientStorage');
            jest.spyOn(ClientStorage, 'getItem').mockReturnValue({
                foo: 'bar',
            });
            expect(playerViewModel.getLessonClientStorage(['foo'])).toEqual('bar');
        });

        it('should notify Sentry if encountering a parse error', () => {
            playerViewModel = lesson.createPlayerViewModel();
            const ClientStorage = $injector.get('ClientStorage');
            ClientStorage.setItem(playerViewModel.lessonClientStorageKey, 'object');
            const ErrorLogService = $injector.get('ErrorLogService');
            jest.spyOn(ErrorLogService, 'notify').mockImplementation(() => {});
            expect(playerViewModel.getLessonClientStorage(['some', 'thing'])).toBeUndefined();
            expect(ErrorLogService.notify).toHaveBeenCalledWith(
                'unable to parse invalid lesson progress store',
                undefined,
                {
                    error: 'Unexpected token o in JSON at position 0',
                    lesson_client_storage_key: playerViewModel.lessonClientStorageKey,
                    json_string: 'object',
                },
            );
        });
    });

    describe('finishSavingProgress', () => {
        it('should resolve once all outstanding progress saves are complete', () => {
            const $timeout = $injector.get('$timeout');
            playerViewModel = lesson.createPlayerViewModel({
                logProgress: true,
            });

            // create some promises, grab the resolve functions,
            // and mock lessonProgress.save() to return the promises
            const resolvers = [];
            const promises = [$q(resolve => resolvers.push(resolve)), $q(resolve => resolvers.push(resolve))];
            let i = 0;
            jest.spyOn(playerViewModel.lesson.ensureLessonProgress(), 'save').mockImplementation(() => {
                i += 1;
                return promises[i - 1].then(() => ({}));
            });

            // call save twice
            playerViewModel._saveLessonAndStreamProgress();
            playerViewModel._saveLessonAndStreamProgress();

            // call finishSavingProgress and monitor it's resolution
            const onComplete = jest.fn();
            playerViewModel.finishSavingProgress().then(onComplete);

            // resolve the first save and the callback is not called
            resolvers[0]();
            $timeout.flush();
            expect(onComplete).not.toHaveBeenCalled();

            // resolve the second save and the callback is called
            resolvers[1]();
            $timeout.flush();
            expect(onComplete).toHaveBeenCalled();

            // Now that there are no pending saves,
            // a subsequent call should resolve immediately
            onComplete.mockClear();
            playerViewModel.finishSavingProgress().then(onComplete);
            $timeout.flush();
            expect(onComplete).toHaveBeenCalled();
        });
    });
});
