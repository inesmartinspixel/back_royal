import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/lessons';

describe('Frame::FrameViewModel', () => {
    let frameViewModel;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'SpecHelper');
        angular.mock.inject([
            '$injector',
            _$injector_ => {
                const $injector = _$injector_;
                const Frame = $injector.get('Lesson.FrameList.Frame');
                $injector.get('LessonFixtures');

                const frame = Frame.new();
                frameViewModel = frame.createFrameViewModel({});
            },
        ]);
    });

    describe('logInfo', () => {
        it('should include frame info', () => {
            jest.spyOn(frameViewModel.frame, 'logInfo').mockReturnValue({
                a: 'b',
            });
            const info = frameViewModel.logInfo();
            expect(info.a).toBe('b');
            expect(info.frame_play_id).toBe(frameViewModel.id);
        });
    });

    describe('index', () => {
        it('should work', () => {
            const mockPlayerViewModel = {
                frames: [null, frameViewModel.frame, null],
            };
            frameViewModel.playerViewModel = mockPlayerViewModel;
            expect(frameViewModel.index).toEqual(1);
        });
    });
});
