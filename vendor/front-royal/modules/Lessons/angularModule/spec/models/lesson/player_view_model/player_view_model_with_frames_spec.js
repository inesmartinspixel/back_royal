import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/lessons';

describe('Lessons::Lesson::PlayerViewModelWithFrames', () => {
    let FramePlayerViewModel;
    let playerViewModel;
    let $injector;
    let EventLogger;
    let frames;
    let Event;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector => {
                $injector = _$injector;
                $injector.get('LessonFixtures');
                EventLogger = $injector.get('EventLogger');
                Event = $injector.get('EventLogger.Event');
                const Frame = $injector.get('Lesson.FrameList.Frame');

                jest.spyOn(Event.prototype, 'addDurationInfo').mockImplementation(() => {});

                frames = [Frame.fixtures.getInstance(), Frame.fixtures.getInstance(), Frame.fixtures.getInstance()];

                FramePlayerViewModel = $injector.get('Lesson.PlayerViewModel').subclass(function () {
                    this.include($injector.get('PlayerViewModelWithFrames'));

                    Object.defineProperty(this.prototype, 'frames', {
                        get() {
                            return frames;
                        },
                    });
                });

                Object.defineProperty(FramePlayerViewModel.prototype, 'sessionId', {
                    value: 'sessionId',
                });
                playerViewModel = new FramePlayerViewModel(null, {});
            },
        ]);
    });

    describe('logging', () => {
        it('should only log activate events in editor mode', () => {
            const oldFrame = frames[0];
            const newFrame = frames[1];
            playerViewModel.editorMode = true;
            playerViewModel.activeFrame = oldFrame;
            expect(playerViewModel.activeFrame).toBe(oldFrame);
            jest.spyOn(EventLogger.prototype, 'log').mockImplementation(() => {});
            Event.prototype.addDurationInfo.mockClear();
            playerViewModel.gotoFrame(newFrame);
            $injector.get('$timeout').flush();

            expect(EventLogger.prototype.log.mock.calls.length).toBe(1);
            expect(EventLogger.prototype.log.mock.calls[0][0]).toEqual('lesson:frame:activate:editor');
        });

        it('should only log activate events in preview mode', () => {
            const oldFrame = frames[0];
            const newFrame = frames[1];
            playerViewModel.previewMode = true;
            playerViewModel.activeFrame = oldFrame;
            expect(playerViewModel.activeFrame).toBe(oldFrame);
            jest.spyOn(EventLogger.prototype, 'log').mockImplementation(() => {});
            Event.prototype.addDurationInfo.mockClear();
            playerViewModel.gotoFrame(newFrame);
            $injector.get('$timeout').flush();

            expect(EventLogger.prototype.log.mock.calls.length).toBe(1);
            expect(EventLogger.prototype.log.mock.calls[0][0]).toEqual('lesson:frame:activate:preview');
        });
    });

    describe('activeFrame', () => {
        describe('set', () => {
            it('should set the activeFrame', () => {
                const newFrame = frames[1];
                expect(playerViewModel.activeFrame).not.toBe(newFrame);
                playerViewModel.activeFrame = newFrame;
                expect(playerViewModel.activeFrame).toBe(newFrame);
            });
            it('should set the activeFrameViewModel', () => {
                const newFrame = frames[1];
                expect(playerViewModel.activeFrame).not.toBe(newFrame);
                playerViewModel.activeFrame = newFrame;
                expect(playerViewModel.activeFrameViewModel.frame).toBe(newFrame);
            });
            it('should log to EventLogger', () => {
                const oldFrame = frames[0];
                const newFrame = frames[1];
                playerViewModel.activeFrame = oldFrame;
                expect(playerViewModel.activeFrame).toBe(oldFrame);
                jest.spyOn(EventLogger.prototype, 'log');
                Event.prototype.addDurationInfo.mockClear();
                playerViewModel.activeFrame = newFrame;

                expect(EventLogger.prototype.log.mock.calls.length).toBe(1);
                expect(EventLogger.prototype.log.mock.calls[0][0]).toBe('lesson:frame:unload');
                expect(EventLogger.prototype.log.mock.calls[0][1].frame_id).toBe(oldFrame.id);

                // // addDurationInfo should have been called once with the activate
                // // event as an argument and once with the frame play event
                // // as an argument with the play event as an argument
                // expect(Event.prototype.addDurationInfo).toHaveBeenCalled();
                // expect(Event.prototype.addDurationInfo.mock.calls[0][0].event_type).toBe('lesson:frame:play');

                // expect(EventLogger.prototype.log.mock.calls[1][0]).toBe('lesson:frame:play');
                // expect(EventLogger.prototype.log.mock.calls[1][1].frame_id).toBe(newFrame.id);

                // setting the activeFrame to the same value should
                // not log again
                EventLogger.prototype.log.mockClear();
                playerViewModel.activeFrame = newFrame;
                expect(EventLogger.prototype.log).not.toHaveBeenCalled();
            });
            it('should be settable to undefined', () => {
                playerViewModel.started = true;
                playerViewModel.activeFrame = frames[0];
                expect(playerViewModel.activeFrame).not.toBeUndefined();
                playerViewModel.activeFrame = undefined;
                expect(playerViewModel.activeFrame).toBeUndefined();
            });
            it('should destroy the activeFrame', () => {
                playerViewModel.started = true;
                playerViewModel.activeFrame = frames[0];
                const activeFrameViewModel = playerViewModel.activeFrameViewModel;
                expect(activeFrameViewModel).not.toBeUndefined();
                jest.spyOn(activeFrameViewModel, 'destroy').mockImplementation(() => {});
                playerViewModel.activeFrame = undefined;
                expect(activeFrameViewModel.destroy).toHaveBeenCalled();
            });
        });
    });

    describe('destroy callback', () => {
        it('should unset the activeFrame', () => {
            playerViewModel.activeFrame = frames[0];
            playerViewModel.destroy();
            expect(playerViewModel.activeFrame).toBe(null);
        });
        it('should set frameThatWasActiveWhenDestroyed', () => {
            playerViewModel.activeFrame = frames[0];
            playerViewModel.destroy();
            expect(playerViewModel.frameThatWasActiveWhenDestroyed).toBe(frames[0]);
        });
    });

    describe('imagePreloading', () => {
        describe('preloadAllImages', () => {
            it('should call preloadImagesForFrame once for each componentized frame', () => {
                jest.spyOn(playerViewModel.frames[0], 'preloadImages').mockImplementation(() => {});
                playerViewModel.preloadAllImages();
                expect(playerViewModel.frames[0].preloadImages).toHaveBeenCalled();
            });
        });

        describe('preloadImagesForNextFrame', () => {
            it('should preload the first frame', () => {
                playerViewModel.activeFrame = undefined;
                jest.spyOn(playerViewModel, 'preloadAssetsForFrame').mockImplementation(() => {});
                playerViewModel.preloadAssetsForNextFrame();
                expect(playerViewModel.preloadAssetsForFrame).toHaveBeenCalled();
                expect(playerViewModel.preloadAssetsForFrame.mock.calls.length).toBe(1);
                expect(playerViewModel.preloadAssetsForFrame.mock.calls[0]).toEqual([frames[0]]);
            });

            it('should preload the next frame', () => {
                playerViewModel.activeFrame = frames[0];
                jest.spyOn(playerViewModel, 'preloadAssetsForFrame').mockImplementation(() => {});
                playerViewModel.preloadAssetsForNextFrame();
                expect(playerViewModel.preloadAssetsForFrame).toHaveBeenCalled();
                expect(playerViewModel.preloadAssetsForFrame.mock.calls.length).toBe(1);
                expect(playerViewModel.preloadAssetsForFrame.mock.calls[0]).toEqual([frames[1]]);
            });
        });

        describe('preloadAssetsForFrame', () => {
            let frame;

            beforeEach(() => {
                frame = playerViewModel.frames[0];
                jest.spyOn(frame, 'preloadAssets').mockReturnValue(Promise.resolve());
            });

            it('should not preload if the lesson is available offline', () => {
                jest.spyOn(playerViewModel, 'allContentStored', 'get').mockReturnValue(true);
                playerViewModel.preloadAssetsForFrame(frame);
                expect(frame.preloadAssets).not.toHaveBeenCalled();
            });

            it('should preload if the lesson is not available offline', () => {
                jest.spyOn(playerViewModel, 'allContentStored', 'get').mockReturnValue(false);
                playerViewModel.preloadAssetsForFrame(frame);
                expect(frame.preloadAssets).toHaveBeenCalled();
            });
        });
    });

    describe('activeFrameId', () => {
        it('should be gettable', () => {
            playerViewModel.activeFrame = undefined;
            expect(playerViewModel.activeFrameId).toBeUndefined();
            playerViewModel.activeFrame = frames[0];
            expect(playerViewModel.activeFrameId).toBe(playerViewModel.activeFrame.id);
        });
        // setter is defined only in FrameListPlayerViewModel
    });

    describe('mainContentStyles', () => {
        it('should include frame_type when there is a frame', () => {
            playerViewModel.activeFrame = frames[0];
            playerViewModel.activeFrame.frame_type = 'something';
            expect(playerViewModel.mainContentStyles(playerViewModel.activeFrameViewModel)).toEqual([
                'animated',
                'frame',
                'something',
            ]);
        });
        it('should work with no activeFrame', () => {
            playerViewModel.activeFrame = undefined;
            expect(playerViewModel.mainContentStyles()).toEqual(['animated', 'frame']);
        });
    });

    describe('skipFrame', () => {
        it('should log and goto next frame', () => {
            playerViewModel.activeFrame = frames[0];
            jest.spyOn(playerViewModel, 'gotoNext').mockImplementation(() => {});
            jest.spyOn(EventLogger.prototype, 'log').mockImplementation(() => {});
            playerViewModel.skipFrame();
            expect(EventLogger.prototype.log).toHaveBeenCalled();
            expect(EventLogger.prototype.log.mock.calls[0][0]).toEqual('lesson:frame:skip');
            expect(playerViewModel.gotoNext).toHaveBeenCalled();
        });
    });
});
