import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/lessons';

describe('Frame', () => {
    let Frame;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'SpecHelper');
        angular.mock.inject([
            'Lesson.FrameList.Frame',
            'LessonFixtures',
            _Frame_ => {
                Frame = _Frame_;
            },
        ]);
    });

    describe('initialize', () => {
        it('should add a default guids to a newly created frame', () => {
            const frame = Frame.new();
            expect(frame.$$objectId).not.toBe(null);
            expect(frame.$$previewId).not.toBe(null);
            expect(frame.id).not.toBe(null);
        });

        it('shouldnt override existing id', () => {
            Frame.defaultAttrs().set('id', '');
            const newlyCreatedframe = Frame.new();
            const frameBeingEdited = Frame.new(newlyCreatedframe);

            expect(frameBeingEdited.id).toBe(newlyCreatedframe.id);
        });
    });

    describe('defaultAttrs', () => {
        it('should apply defaultAttrs', () => {
            Frame.defaultAttrs().set('a', 'b');
            const frame = Frame.new();
            expect(frame.a).toBe('b');
        });

        it('should apply default attrs from all classes along the inheritance hierarchy', () => {
            Frame.defaultAttrs().set('a', 'b');
            const Subframe = Frame.subclass(function () {
                this.defaultAttrs().set('c', 'd');
            });
            const frame = Subframe.new();
            expect(frame.a).toBe('b');
            expect(frame.c).toBe('d');
        });

        it('should ensure defaultAttrs are deep cloned', () => {
            Frame.defaultAttrs().set('obj', {
                key: {},
            });
            expect(Frame.new().obj).not.toBe(Frame.new().obj);
            expect(Frame.new().obj.key).not.toBe(Frame.new().obj.key);
        });

        it('should deep merge the defaultAttrs', () => {
            Frame.defaultAttrs().set('key', {
                prop1: 'default1',
                prop2: 'default2',
            });
            const frame = Frame.new({
                key: {
                    prop2: 'overridden',
                },
            });
            expect(frame.key.prop1).toBe('default1');
            expect(frame.key.prop2).toBe('overridden');
        });
    });

    describe('hasSection', () => {
        it('should return a section if it is present', () => {
            jest.spyOn(Frame.prototype, 'layout').mockReturnValue(['main-image', 'text']);
            const frame = Frame.fixtures.getInstance();
            expect(frame.hasSection('main-image')).toBe(true);
        });

        it('should return no section if it is not present', () => {
            jest.spyOn(Frame.prototype, 'layout').mockReturnValue(['text']);
            const frame = Frame.fixtures.getInstance();
            expect(frame.hasSection('main-image')).toBe(false);
        });
    });

    describe('image accessors', () => {
        let frame;

        beforeEach(() => {
            frame = Frame.fixtures.getInstance({
                images: [
                    {
                        id: '1',
                        label: 'image1',
                        src: 'image',
                    },
                    {
                        id: '2',
                        label: 'image2',
                        src: 'image',
                    },
                    {
                        id: '3',
                        label: 'image3',
                        src: 'image',
                    },
                ],
            });
        });

        describe('image()', () => {
            it('should return an image for an id', () => {
                expect(frame.image('2')).toBe(frame.images[1]);
            });

            it('should return undefined if image not found', () => {
                expect(frame.image('nope')).toBeUndefined();
            });

            it('should return undefined if no images array', () => {
                delete frame.images;
                expect(frame.image('nope')).toBeUndefined();
            });
        });

        describe('label', () => {
            it('should return an label for an frame', () => {
                expect(frame.label).toBe('Frame 1');
            });
        });

        describe('imageForLabel', () => {
            it('should return an image for a label', () => {
                expect(frame.imageForLabel('image2')).toBe(frame.images[1]);
            });

            it('should return undefined if image not found', () => {
                expect(frame.imageForLabel('nope')).toBeUndefined();
            });

            it('should return undefined if no images array', () => {
                delete frame.images;
                expect(frame.imageForLabel('nope')).toBeUndefined();
            });
        });
    });

    describe('imageForLabel', () => {});

    describe('mainImage', () => {
        it('should return an image if there is an main-image section and a main_image', () => {
            jest.spyOn(Frame.prototype, 'layout').mockReturnValue(['main-image', 'text']);
            const image = {
                id: 'imageId',
                label: 'imageName',
                src: 'image',
            };
            const frame = Frame.fixtures.getInstance({
                images: [image],
                main_image_id: 'imageId',
            });

            expect(frame.mainImage()).toEqual(image);
        });

        it('should return no image if there is no image section', () => {
            jest.spyOn(Frame.prototype, 'layout').mockReturnValue(['text']);
            const frame = Frame.fixtures.getInstance({
                images: [
                    {
                        id: 'imageId',
                        label: 'imageName',
                        src: 'image',
                    },
                ],
                main_image_id: 'imageId',
            });
            expect(frame.mainImage()).toBeUndefined();
        });
    });

    describe('subTypes', () => {
        it('should return a list of aliased subclasses', () => {
            const SubFrame = Frame.subclass();
            // this is a real frame type, so it has an alias
            const FrameType1 = SubFrame.subclass(function () {
                this.alias('frame_type_1');
            });
            // this is not a frame type, it is a base class for multiple frame types
            const SubSuper = SubFrame.subclass();
            // these two are real frame types that inherit from SubSuper
            const FrameType2 = SubSuper.subclass(function () {
                this.alias('frame_type_2');
            });
            const FrameType3 = SubSuper.subclass(function () {
                this.alias('frame_type_3');
            });
            expect(SubFrame.subTypes()).toEqual([FrameType1, FrameType2, FrameType3]);
        });
    });

    describe('altered urls', () => {
        let frame;
        beforeEach(() => {
            frame = Frame.fixtures.getInstance();
        });

        it('should work with an unaltered url', () => {
            expect(frame.getAlteredUrl('unalteredUrl')).toBe('unalteredUrl');
        });

        it('should work with an altered url', () => {
            frame.useAlteredUrl('alteredUrl', 'alteredUrl?guid');
            expect(frame.getAlteredUrl('alteredUrl')).toBe('alteredUrl?guid');
        });
    });

    describe('isPractice', () => {
        it('should be true if in practice lesson', () => {
            const frame = Frame.fixtures.getInstance();
            Object.defineProperty(frame.lesson(), 'isPractice', {
                value: true,
            });
            expect(frame.isPractice).toBe(true);
            frame.isPractice = false; // this does nothing if you are in a practice lesson
            expect(frame.isPractice).toBe(true);
        });

        it('should be settable if not in practice lesson', () => {
            const frame = Frame.fixtures.getInstance();
            Object.defineProperty(frame.lesson(), 'isPractice', {
                value: false,
            });
            expect(frame.isPractice).toBe(false);
            frame.isPractice = true;
            expect(frame.isPractice).toBe(true);
        });
    });
});
