import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/stream_progress';

describe('Lesson.StreamProgress', () => {
    let StreamProgress;
    let EventLogger;
    let SpecHelper;
    let $injector;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector => {
                $injector = _$injector;

                StreamProgress = $injector.get('Lesson.StreamProgress');

                SpecHelper = $injector.get('SpecHelper');
                EventLogger = $injector.get('EventLogger');

                $injector.get('MockIguana');
                $injector.get('StreamProgressFixtures');

                StreamProgress.setAdapter('Iguana.Mock.Adapter');
            },
        ]);
    });

    describe('stream status', () => {
        it('should determine inProgress', () => {
            SpecHelper.stubEventLogging();
            const streamProgress = StreamProgress.fixtures.getInstance().embedInStream();

            streamProgress.complete = true;

            expect(streamProgress.inProgress).toBe(false);

            Object.defineProperty(streamProgress, 'complete', {
                value: false,
            });
            expect(streamProgress.inProgress).toBe(true);
        });
    });

    describe('complete', () => {
        it('should log when set to true', () => {
            const streamProgress = StreamProgress.fixtures.getInstance().embedInStream();
            expect(streamProgress.complete).toBe(false);
            jest.spyOn(EventLogger.prototype, 'log').mockImplementation(() => {});
            streamProgress.complete = true;
            expect(streamProgress.complete).toBe(true);
            expect(EventLogger.prototype.log).toHaveBeenCalledWith(
                'lesson:stream:complete',
                streamProgress.stream().logInfo(),
            );
        });
        it('should not log when set from true to true', () => {
            const streamProgress = StreamProgress.fixtures.getInstance().embedInStream();
            streamProgress.complete = true;
            jest.spyOn(EventLogger.prototype, 'log').mockImplementation(() => {});
            streamProgress.complete = true;
            expect(streamProgress.complete).toBe(true);
            expect(EventLogger.prototype.log).not.toHaveBeenCalled();
        });
        it('should do nothing  when set back to false', () => {
            const streamProgress = StreamProgress.fixtures.getInstance().embedInStream();
            streamProgress.complete = true;
            // expect(function() {
            //     streamProgress.complete = false;
            // }).toThrow(new Error('Cannot set stream_progress.complete back to false'));
            streamProgress.complete = false;
            expect(streamProgress.complete).toBe(true);
        });
    });

    describe('startStream', () => {
        it('should work', () => {
            SpecHelper.stubCurrentUser();
            const $rootScope = $injector.get('$rootScope');

            const stream = StreamProgress.fixtures.getInstance().embedInStream().stream();
            stream.ensureLocalePack();
            stream.lesson_streams_progress = undefined;
            jest.spyOn(EventLogger.prototype, 'log').mockImplementation(() => {});
            const logInfo = {
                _startStreamLogInfo: true,
            };
            jest.spyOn(StreamProgress, '_startStreamLogInfo').mockReturnValue(logInfo);

            const streamProgress = StreamProgress.startStream(stream);
            expect(streamProgress.stream()).toBe(stream);
            expect(streamProgress.user_id).toBe($rootScope.currentUser.id);
            expect(streamProgress.locale_pack_id).toBe(stream.localePackId);
            expect(streamProgress.complete).toBe(false);
            expect(stream.lesson_streams_progress).toBe(streamProgress);

            expect(StreamProgress._startStreamLogInfo).toHaveBeenCalledWith(stream);
            expect(EventLogger.prototype.log).toHaveBeenCalledWith('lesson:stream:start', logInfo);
        });
    });

    describe('_startStreamLogInfo', () => {
        let stream;
        let user;
        let period;

        beforeEach(() => {
            stream = StreamProgress.fixtures.getInstance().embedInStream().stream();
            jest.spyOn(stream, 'logInfo').mockReturnValue({
                stream: 'info',
            });
            user = SpecHelper.stubCurrentUser();
            period = {
                logInfo: jest.fn().mockReturnValue({
                    period: 'info',
                }),
            };
        });

        it('should add just stream info when there is no relevant cohort', () => {
            user.relevant_cohort = null;
            expect(StreamProgress._startStreamLogInfo(stream)).toEqual(stream.logInfo());
        });

        it('should add just stream info when there is no relevant period', () => {
            user.relevant_cohort = {
                periodForRequiredStream: jest.fn().mockReturnValue(null),
            };
            expect(StreamProgress._startStreamLogInfo(stream)).toEqual(stream.logInfo());
        });

        it('should add period info', () => {
            expectPeriod();
            expectLogInfoWithPeriod();
        });

        it('should add exam info when not starting final', () => {
            stream.exam = true;
            period.exam_style = 'intermediate';
            expectPeriod();
            expectLogInfoWithPeriod({
                starting_final_exam: false,
            });
        });

        it('should add exam info when starting final', () => {
            stream.exam = true;
            period.exam_style = 'final';
            expectPeriod();
            expectLogInfoWithPeriod({
                starting_final_exam: true,
            });
        });

        function expectPeriod() {
            user.relevant_cohort = {
                periodForRequiredStream: jest.fn().mockReturnValue(period),
            };
        }

        function expectLogInfoWithPeriod(extra = {}) {
            const expected = _.extend(
                {
                    stream: 'info',
                    period: {
                        period: 'info',
                    },
                },
                extra,
            );
            expect(StreamProgress._startStreamLogInfo(stream)).toEqual(expected);
        }
    });

    describe('destroy', () => {
        it('should be tested', () => {
            const stream = StreamProgress.fixtures.getInstance().embedInStream().stream();
            stream.lessons[0].ensureLessonProgress();

            StreamProgress.expect('destroy');
            stream.lesson_streams_progress.destroy();
            StreamProgress.flush('destroy');

            expect(stream.lesson_streams_progress).toBeUndefined();
            expect(stream.lessons[0].lesson_progress).toBeUndefined();
        });
    });
});
