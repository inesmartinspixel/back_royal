import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/lessons';

describe('Lessons::PracticeFramesPlayerViewModel', () => {
    let $injector;
    let playerViewModel;
    let Frame;
    let Lesson;
    let Stream;
    let SessionTracker;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector => {
                $injector = _$injector;
                const PracticeFramesPlayerViewModel = $injector.get('Lesson.PracticeFramesPlayerViewModel');
                $injector.get('LessonFixtures');
                $injector.get('MockIguana');
                Frame = $injector.get('Lesson.FrameList.Frame');
                Lesson = $injector.get('Lesson');
                Stream = $injector.get('Lesson.Stream');
                SessionTracker = $injector.get('SessionTracker');
                playerViewModel = new PracticeFramesPlayerViewModel();
            },
        ]);
    });

    describe('ready', () => {
        it('should be true once there are some frames', () => {
            expect(playerViewModel.ready).toBe(false);
            playerViewModel.frames = [Frame.new()];
            expect(playerViewModel.ready).toBe(true);
        });
    });

    describe('shouldShowPreviousButton', () => {
        beforeEach(() => {
            playerViewModel.frames = [Frame.fixtures.getInstance(), Frame.fixtures.getInstance()];
            playerViewModel.activeFrame = null;
        });

        it('should be false if there is no active frame', () => {
            expect(playerViewModel.shouldShowPreviousButton).toBe(false);
        });

        it('should be false if first frame is active', () => {
            playerViewModel.activeFrameIndex = 0;
            expect(playerViewModel.shouldShowPreviousButton).toBe(false);
        });

        it('should be true if a frame other than the first is active', () => {
            playerViewModel.activeFrameIndex = 1;
            expect(playerViewModel.shouldShowPreviousButton).toBe(true);
        });
    });

    describe('loadContent', () => {
        describe('with lesson', () => {
            it('should load the lesson and add frames to the list', () => {
                const frames = [Frame.fixtures.getInstance(), Frame.fixtures.getInstance()];
                Lesson.expect('show')
                    .toBeCalledWith('someId', {
                        filters: {
                            published: true,
                        },
                        'fields[]': ['practice_frames', 'lesson_type'],
                    })
                    .returns({
                        lesson_type: 'frame_list',
                        practice_frames: frames,
                    });
                playerViewModel.loadContent('lesson', 'someId');
                Lesson.flush('show');
                expect(_.pluck(playerViewModel.frames, 'id').sort()).toEqual(_.pluck(frames, 'id').sort());
            });
        });

        describe('with course', () => {
            it('should load the course and add frames to the list', () => {
                const frames = [
                    Frame.fixtures.getInstance(),
                    Frame.fixtures.getInstance(),
                    Frame.fixtures.getInstance(),
                    Frame.fixtures.getInstance(),
                ];
                Stream.expect('show')
                    .toBeCalledWith('someId', {
                        filters: {
                            published: true,
                        },
                        'fields[]': ['lessons'],
                        'lesson_fields[]': ['id', 'lesson_type', 'practice_frames', 'lesson_progress'],
                    })
                    .returns({
                        lessons: [
                            {
                                lesson_type: 'frame_list',
                                practice_frames: [frames[0], frames[1]],
                                lesson_progress: {
                                    complete: true,
                                },
                            },
                            {
                                lesson_type: 'frame_list',
                                practice_frames: [],
                                lesson_progress: {
                                    complete: true,
                                },
                            },
                            {
                                lesson_type: 'frame_list',
                                practice_frames: [frames[2], frames[3]],
                                lesson_progress: {
                                    complete: true,
                                },
                            },
                        ],
                    });
                playerViewModel.loadContent('course', 'someId');
                Stream.flush('show');
                expect(_.pluck(playerViewModel.frames, 'id').sort()).toEqual(_.pluck(frames, 'id').sort());
            });

            it('should filter out incomplete lessons', () => {
                const frames = [
                    Frame.fixtures.getInstance(),
                    Frame.fixtures.getInstance(),
                    Frame.fixtures.getInstance(),
                    Frame.fixtures.getInstance(),
                ];
                Stream.expect('show')
                    .toBeCalledWith('someId', {
                        filters: {
                            published: true,
                        },
                        'fields[]': ['lessons'],
                        'lesson_fields[]': ['id', 'lesson_type', 'practice_frames', 'lesson_progress'],
                    })
                    .returns({
                        lessons: [
                            {
                                lesson_type: 'frame_list',
                                practice_frames: [frames[0], frames[1]],
                                lesson_progress: {
                                    complete: true,
                                },
                            },
                            {
                                lesson_type: 'frame_list',
                                practice_frames: [frames[2], frames[3]],
                                lesson_progress: {
                                    complete: false,
                                },
                            },
                        ],
                    });
                playerViewModel.loadContent('course', 'someId');
                Stream.flush('show');
                expect(_.pluck(playerViewModel.frames, 'id').sort()).toEqual(
                    _.pluck([frames[0], frames[1]], 'id').sort(),
                );
            });
        });
    });

    describe('gotoNext', () => {
        beforeEach(() => {
            playerViewModel.frames = [
                Frame.fixtures.getInstance(),
                Frame.fixtures.getInstance(),
                Frame.fixtures.getInstance(),
            ];
            jest.spyOn(playerViewModel, 'gotoFrameIndex').mockImplementation(() => {});
        });

        it('should activate the next frame', () => {
            playerViewModel.activeFrameIndex = 0;
            playerViewModel.gotoNext();
            expect(playerViewModel.gotoFrameIndex).toHaveBeenCalledWith(1);
        });

        it('should activate the first frame if at the end', () => {
            playerViewModel.activeFrameIndex = 2;
            playerViewModel.gotoNext();
            expect(playerViewModel.gotoFrameIndex).toHaveBeenCalledWith(0);
        });

        it('should do nothing if no active frame', () => {
            playerViewModel.activeFrame = null;
            playerViewModel.gotoNext();
            expect(playerViewModel.gotoFrameIndex).not.toHaveBeenCalled();
        });
    });

    describe('gotoPrev', () => {
        beforeEach(() => {
            playerViewModel.frames = [
                Frame.fixtures.getInstance(),
                Frame.fixtures.getInstance(),
                Frame.fixtures.getInstance(),
            ];
            jest.spyOn(playerViewModel, 'gotoFrameIndex').mockImplementation(() => {});
        });

        it('should activate the previous frame', () => {
            playerViewModel.activeFrameIndex = 2;
            playerViewModel.gotoPrev();
            expect(playerViewModel.gotoFrameIndex).toHaveBeenCalledWith(1);
        });

        it('should do nothing if already on first frame', () => {
            playerViewModel.activeFrameIndex = 0;
            playerViewModel.gotoPrev();
            expect(playerViewModel.gotoFrameIndex).not.toHaveBeenCalled();
        });

        it('should do nothing if no active frame', () => {
            playerViewModel.activeFrame = null;
            playerViewModel.gotoPrev();
            expect(playerViewModel.gotoFrameIndex).not.toHaveBeenCalled();
        });
    });

    describe('sessionId', () => {
        it('should work', () => {
            jest.spyOn(SessionTracker, 'pingCurrentSession');
            expect(playerViewModel.sessionId).not.toBeUndefined();
            expect(SessionTracker.pingCurrentSession).toHaveBeenCalledWith(
                'lessonPracticePlayer',
                $injector.get('PLAYER_SESSION_EXPIRY_MINUTES') * 60 * 1000,
            );
        });
    });
});
