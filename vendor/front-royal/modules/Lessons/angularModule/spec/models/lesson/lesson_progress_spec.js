import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/lesson_progress';
import 'Lessons/angularModule/spec/_mock/fixtures/stream_progress';

/* eslint-disable no-shadow */
describe('Lessons::LessonProgress', () => {
    let LessonProgress;
    let Iguana;
    let lessonProgress;
    let EventLogger;
    let $q;
    let $timeout;
    let $injector;
    let SpecHelper;
    let StreamProgress;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            'MockIguana',
            'LessonProgressFixtures',
            _$injector => {
                $injector = _$injector;
                LessonProgress = $injector.get('LessonProgress');
                Iguana = $injector.get('Iguana');
                LessonProgress.setAdapter('Iguana.Mock.Adapter');
                EventLogger = $injector.get('EventLogger');
                $q = $injector.get('$q');
                $timeout = $injector.get('$timeout');
                SpecHelper = $injector.get('SpecHelper');
                StreamProgress = $injector.get('Lesson.StreamProgress');
                $injector.get('StreamProgressFixtures');

                lessonProgress = LessonProgress.fixtures.getInstance();
            },
        ]);
    });

    describe('inProgress', () => {
        it('should be inverse of complete', () => {
            lessonProgress.complete = true;
            expect(lessonProgress.inProgress).toBe(false);

            lessonProgress.writeKey('complete', false);
            expect(lessonProgress.inProgress).toBe(true);
        });
    });

    describe('save', () => {
        it('should work if no streamProgress passed in', () => {
            LessonProgress.expect('save').returnsMeta({});
            const success = jest.fn();
            lessonProgress.save().then(success);
            LessonProgress.flush('save');
            expect(success).toHaveBeenCalled();
        });

        it('should send the stream progress along with the metadata', () => {
            const mockPromise = {
                then: jest.fn(),
            };

            jest.spyOn(Iguana.prototype, 'save').mockReturnValue(mockPromise);
            const returnValue = lessonProgress.save({
                isA() {
                    return true;
                },
                asJson() {
                    return 'mockStreamProgressJSON';
                },
            });

            expect(returnValue).toBe(mockPromise.then());
            expect(Iguana.prototype.save).toHaveBeenCalledWith({
                stream_progress_records: ['mockStreamProgressJSON'],
            });
        });

        it('should copy response back to the stream progress record', () => {
            const streamProgress = StreamProgress.fixtures.getInstance();
            const returnValue = $q.resolve({
                meta: {
                    stream_progress_records: [
                        { locale_pack_id: 'some_other_one', prop: 'wrongAnswer' },
                        { locale_pack_id: streamProgress.locale_pack_id, prop: 'fromServer' },
                    ],
                },
            });

            jest.spyOn(Iguana.prototype, 'save').mockReturnValue(returnValue);
            lessonProgress.save(streamProgress);
            $timeout.flush();
            expect(streamProgress.prop).toEqual('fromServer');
        });
    });

    describe('complete', () => {
        it('should log when set to true', () => {
            const lessonProgress = LessonProgress.fixtures.getInstance();
            expect(lessonProgress.complete).toBe(false);
            jest.spyOn(EventLogger.prototype, 'log').mockImplementation(() => {});
            lessonProgress.complete = true;
            expect(lessonProgress.complete).toBe(true);
            expect(EventLogger.prototype.log).toHaveBeenCalledWith(
                'lesson:complete',
                lessonProgress.lesson().logInfo(),
            );
        });
        it('should not log when set from true to true', () => {
            const lessonProgress = LessonProgress.fixtures.getInstance();
            lessonProgress.complete = true;
            jest.spyOn(EventLogger.prototype, 'log').mockImplementation(() => {});
            lessonProgress.complete = true;
            expect(lessonProgress.complete).toBe(true);
            expect(EventLogger.prototype.log).not.toHaveBeenCalled();
        });
        it('should do nothing when set back to false', () => {
            const lessonProgress = LessonProgress.fixtures.getInstance();
            lessonProgress.complete = true;
            // expect(function() {
            //     lessonProgress.complete = false;
            // }).toThrow(new Error('LessonProgress#complete cannot be set back to false from true.'));
            lessonProgress.complete = false;
            expect(lessonProgress.complete).toBe(true);
        });
    });

    describe('startLesson', () => {
        it('should work', () => {
            SpecHelper.stubCurrentUser();
            const $rootScope = $injector.get('$rootScope');
            const lesson = LessonProgress.fixtures.getInstance().lesson();
            lesson.ensureLocalePack();
            lesson.lesson_progress = undefined;
            jest.spyOn(EventLogger.prototype, 'log').mockImplementation(() => {});
            const lessonProgress = LessonProgress.startLesson(lesson);
            expect(lessonProgress.lesson()).toBe(lesson);
            expect(lessonProgress.user_id).toBe($rootScope.currentUser.id);
            expect(lessonProgress.locale_pack_id).toBe(lesson.localePackId);
            expect(lessonProgress.complete).toBe(false);
            expect(lesson.lesson_progress).toBe(lessonProgress);
            expect(EventLogger.prototype.log).toHaveBeenCalledWith('lesson:start', lessonProgress.lesson().logInfo());
        });
    });

    describe('_save', () => {
        let instance;
        beforeEach(() => {
            instance = LessonProgress.fixtures.getInstance();
        });

        it('should prevent against blowing away updates to challenge_scores made while saving', () => {
            jest.spyOn(LessonProgress, 'saveWithoutInstantiating').mockReturnValue(
                $q.when({
                    result: {
                        challenge_scores: {
                            a: 1,
                            b: 2,
                        },
                    },
                }),
            );

            // before saving, the user completed challenge a
            instance.challenge_scores = {
                a: 1,
            };
            instance.save();

            // after saving, but before the save call returns, the user
            // finishes challenge c
            instance.challenge_scores.c = 3;

            // when the save call returns, a and b get sent down from
            // the server.  Make sure c is not overridden
            $timeout.flush();
            expect(instance.challenge_scores).toEqual({
                a: 1,
                b: 2,
                c: 3,
            });
        });

        it('should prevent against blowing away updates to completed_frames made while saving', () => {
            jest.spyOn(LessonProgress, 'saveWithoutInstantiating').mockReturnValue(
                $q.when({
                    result: {
                        completed_frames: {
                            a: true,
                            b: true,
                        },
                    },
                }),
            );

            // before saving, the user completed frame a
            instance.completed_frames = {
                a: true,
            };
            instance.save();

            // after saving, but before the save call returns, the user
            // finishes frame c
            instance.completed_frames.c = true;

            // when the save call returns, a and b get sent down from
            // the server.  Make sure c is not overridden
            $timeout.flush();
            expect(instance.completed_frames).toEqual({
                a: true,
                b: true,
                c: true,
            });
        });

        describe('when assessment lesson', () => {
            beforeEach(() => {
                instance.for_assessment_lesson = true;
            });

            it('should not merge challenge_scores from server if absent in client object', () => {
                jest.spyOn(LessonProgress, 'saveWithoutInstantiating').mockReturnValue(
                    $q.when({
                        result: {
                            challenge_scores: {
                                a: 1,
                                b: 2,
                                c: 3,
                            },
                        },
                    }),
                );

                instance.challenge_scores = {
                    a: 1,
                };
                instance.save();

                $timeout.flush();
                expect(instance.challenge_scores).toEqual({
                    a: 1,
                });
            });

            it('should not merge completed_frames from server if absent in client object', () => {
                jest.spyOn(LessonProgress, 'saveWithoutInstantiating').mockReturnValue(
                    $q.when({
                        result: {
                            completed_frames: {
                                a: true,
                                b: true,
                                c: true,
                            },
                        },
                    }),
                );

                instance.completed_frames = {
                    a: true,
                };
                instance.save();

                $timeout.flush();
                expect(instance.completed_frames).toEqual({
                    a: true,
                });
            });
        });
    });
});
