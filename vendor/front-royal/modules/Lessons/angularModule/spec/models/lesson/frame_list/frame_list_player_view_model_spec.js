import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Editor/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/componentized_fixtures';
import 'Lessons/angularModule/spec/_mock/fixtures/lessons';
import 'Lessons/angularModule/spec/_mock/fixtures/streams';

/* eslint-disable no-shadow */
describe('Lessons::FrameList::FrameListPlayerViewModel', () => {
    let FrameList;
    let Frame;
    let Componentized;
    let SpecHelper;
    let Lesson;
    let lesson;
    let playerViewModel;
    let Event;
    let LessonProgress;
    let StreamProgress;
    let stream;
    let $location;
    let $injector;
    let duration;
    let $timeout;
    let $rootScope;
    let FrameListPlayerViewModel;
    let $q;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector => {
                $injector = _$injector;
                FrameList = $injector.get('Lesson.FrameList');
                Frame = $injector.get('Lesson.FrameList.Frame');
                Lesson = $injector.get('Lesson');
                Componentized = $injector.get('Lesson.FrameList.Frame.Componentized');
                SpecHelper = $injector.get('SpecHelper');
                LessonProgress = $injector.get('LessonProgress');
                StreamProgress = $injector.get('Lesson.StreamProgress');
                Event = $injector.get('EventLogger.Event');
                $location = $injector.get('$location');
                $injector.get('LessonFixtures');
                $injector.get('StreamFixtures');
                $injector.get('ComponentizedFixtures');
                $timeout = $injector.get('$timeout');
                $rootScope = $injector.get('$rootScope');
                FrameListPlayerViewModel = $injector.get('Lesson.FrameList.FrameListPlayerViewModel');
                $q = $injector.get('$q');

                SpecHelper.stubCurrentUser('learner');
                SpecHelper.stubEventLogging();

                duration = 42;
                jest.spyOn(Event.prototype, 'addDurationInfo').mockImplementation(function () {
                    this.properties.duration_total = duration;
                    return this;
                });

                lesson = FrameList.fixtures.getInstance({}, false, true);

                stream = lesson.stream();
                stream.createStreamViewModel();
                StreamProgress.expect('save');
                LessonProgress.expect('save');
                lesson.includeProgress();

                // ensure that mainUiComponent is defined on each frame, since it is
                // needed in the frame_completed callback
                lesson.reifyAllFrames();
                _.each(lesson.frames, frame => {
                    frame.mainUiComponent = frame.addChallenges();
                });

                jest.spyOn(lesson.lesson_progress, 'save');
                SpecHelper.stubFramePreloading();
                playerViewModel = lesson.createPlayerViewModel({
                    logProgress: true,
                });
                $timeout.flush(); // finish preloading first frame
            },
        ]);
    });

    describe('initialize', () => {
        describe('with frame id in query params', () => {
            beforeEach(() => {
                jest.spyOn($location, 'search').mockReturnValue({
                    frame: lesson.frames[1].id,
                });
                lesson.frame_bookmark_id = lesson.frames[2].id;
            });
            it('should start from the frame id in the query params if canNavigateBackToCompletedFrames', () => {
                jest.spyOn(
                    FrameListPlayerViewModel.prototype,
                    'canNavigateBackToCompletedFrames',
                    'get',
                ).mockReturnValue(true);
                const playerViewModel = lesson.createPlayerViewModel();
                $timeout.flush(); // finish preloading first frame
                expect(playerViewModel.activeFrameIndex).toBe(1);
            });
            it('should not start from the frame id in the query params if !canNavigateBackToCompletedFrames', () => {
                jest.spyOn(
                    FrameListPlayerViewModel.prototype,
                    'canNavigateBackToCompletedFrames',
                    'get',
                ).mockReturnValue(false);
                const playerViewModel = lesson.createPlayerViewModel();
                $timeout.flush(); // finish preloading first frame
                expect(playerViewModel.activeFrameIndex).toBe(2);
            });

            // On non-test lessons, we do want someone to be able to follow a link to a frame, even
            // if they have not made it there yet.  (At least that was the business decision on 2018/09/23)
            it('should start from the frame id in the query params if canNavigateBackToCompletedFrames even if that frame is ahead of the bookmark', () => {
                lesson.frame_bookmark_id = lesson.frames[0].id;
                jest.spyOn(
                    FrameListPlayerViewModel.prototype,
                    'canNavigateBackToCompletedFrames',
                    'get',
                ).mockReturnValue(true);
                const playerViewModel = lesson.createPlayerViewModel();
                $timeout.flush(); // finish preloading first frame
                expect(playerViewModel.activeFrameIndex).toBe(1);
            });
            it('should update the frame_bookmark_id in the lesson progress', () => {
                lesson.createPlayerViewModel({
                    logProgress: true,
                });
                $timeout.flush(); // finish preloading first frame
                expect(lesson.lesson_progress.frame_bookmark_id).toBe(lesson.frames[1].id);
            });
            it('should not touch progress if logProgress is false', () => {
                jest.spyOn(lesson, 'ensureLessonProgress').mockImplementation(() => {});
                lesson.createPlayerViewModel({
                    logProgress: false,
                });
                $timeout.flush(); // finish preloading first frame
                expect(lesson.ensureLessonProgress).not.toHaveBeenCalled();
            });
        });

        it('should not error if destroyed before preloading first frame is complete', () => {
            const playerViewModel = lesson.createPlayerViewModel();
            playerViewModel.destroy();
            $timeout.flush(); // finish preloading first frame
        });

        it('should start from the frame_bookmark_id in the lesson progress', () => {
            jest.spyOn($location, 'search').mockReturnValue({});
            lesson.frame_bookmark_id = lesson.frames[2].id;
            const playerViewModel = lesson.createPlayerViewModel();
            $timeout.flush(); // finish preloading first frame
            expect(playerViewModel.activeFrame).toBe(lesson.frames[2]);
        });

        it('should start from the beginning if there is no frame_bookmark_id', () => {
            lesson.frame_bookmark_id = undefined;
            const playerViewModel = lesson.createPlayerViewModel();
            $timeout.flush(); // finish preloading first frame
            expect(playerViewModel.activeFrameIndex).toBe(0);
        });
        it('should start from the beginning if the frame_bookmark_id is unknown', () => {
            lesson.frame_bookmark_id = 'unknown';
            const playerViewModel = lesson.createPlayerViewModel();
            $timeout.flush(); // finish preloading first frame
            expect(playerViewModel.activeFrameIndex).toBe(0);
        });

        it('should initialize frame_history to empty array if no frame_history', () => {
            lesson.createPlayerViewModel();
            $timeout.flush(); // finish preloading first frame
            expect(lesson.ensureLessonProgress().frame_history.length).toBe(0);
        });
    });

    describe('activeFrame', () => {
        describe('set', () => {
            it('should update the frame_bookmark_id if it has changed', () => {
                expect(playerViewModel.lesson.stream()).not.toBeUndefined(); // sanity check (next test tests to see if this works without a stream)
                const newFrame = lesson.frames[1];
                expect(playerViewModel.activeFrame).not.toBe(newFrame);
                expect(playerViewModel.lessonProgress.frame_bookmark_id).not.toBe(newFrame.id);
                stream.lesson_streams_progress.lesson_bookmark_id = 'something else';

                playerViewModel.activeFrame = newFrame;
                expect(playerViewModel.lessonProgress.frame_bookmark_id).toBe(newFrame.id);
                expect(playerViewModel.lessonProgress.save).toHaveBeenCalled();
                expect(stream.lesson_streams_progress.lesson_bookmark_id).toBe(lesson.id);
            });
            it('should not update the frame_bookmark_id if the player view model is already marked as complete', () => {
                expect(playerViewModel.lesson.stream()).not.toBeUndefined(); // sanity check (next test tests to see if this works without a stream)
                const newFrame = lesson.frames[1];
                expect(playerViewModel.activeFrame).not.toBe(newFrame);
                expect(playerViewModel.lessonProgress.frame_bookmark_id).not.toBe(newFrame.id);
                stream.lesson_streams_progress.lesson_bookmark_id = 'something else';

                playerViewModel._markAsComplete();
                expect(playerViewModel.lessonProgress.frame_bookmark_id).toBe(null);
                playerViewModel.activeFrame = newFrame;
                expect(playerViewModel.lessonProgress.frame_bookmark_id).toBe(null);
                expect(playerViewModel.lessonProgress.save).toHaveBeenCalled();
            });
            it('should modify the query params', () => {
                jest.spyOn($location, 'search').mockImplementation(() => {});
                const newFrame = lesson.frames[1];
                playerViewModel.activeFrame = newFrame;
                expect($location.search).toHaveBeenCalledWith('frame', newFrame.id);
            });
            it('should succeed in saving lessonProgress even if there is no stream', () => {
                const newFrame = lesson.frames[1];
                playerViewModel.lesson.$$embeddedIn = undefined;
                expect(playerViewModel.lesson.stream()).toBeUndefined(); // sanity check
                playerViewModel.activeFrame = newFrame;
                expect(playerViewModel.lessonProgress.save).toHaveBeenCalled();
            });
            it('should not save lessonProgress if nothing has changed', () => {
                // not sure how you get in this situation, but if you switch to
                // a frame that is already the frame_bookmark_id on the lessonProgress,
                // don't bother saving
                const newFrame = lesson.frames[1];
                expect(playerViewModel.activeFrame).not.toBe(newFrame);
                playerViewModel.lessonProgress.frame_bookmark_id = newFrame.id;
                playerViewModel.lessonProgress.save.mockClear();
                playerViewModel.activeFrame = newFrame;

                // nothing should have changed and we shouldn't save
                expect(playerViewModel.lessonProgress.frame_bookmark_id).toBe(newFrame.id);
                expect(playerViewModel.lessonProgress.save).not.toHaveBeenCalled();
            });
        });
    });

    describe('gotoPrev', () => {
        it('should goto previous frame if not using frame history', () => {
            // editorMode is one reason to ignore frame history
            playerViewModel.editorMode = true;

            playerViewModel.activeFrameIndex = 2;
            playerViewModel.gotoPrev();
            $timeout.flush();
            expect(playerViewModel.activeFrameIndex).toBe(1);
        });
        it('should go to previous frame if one exists in frame history', () => {
            // Start the frame play
            playerViewModel.started = true;

            // Advance to prev screen based on history
            const lessonProgress = LessonProgress.new();
            Lesson.prototype.ensureLessonProgress = jest.fn().mockReturnValue(lessonProgress);
            playerViewModel.lesson.ensureLessonProgress().frame_history = [playerViewModel.frames[0].id];
            jest.spyOn(playerViewModel, '_saveLessonAndStreamProgress').mockReturnValue($q.resolve());
            playerViewModel.gotoPrev();
            $timeout.flush();
            expect(playerViewModel.activeFrame.id).toBe(playerViewModel.frames[0].id);
            expect(playerViewModel.lesson.ensureLessonProgress().frame_history.length).toBe(0);
        });
        it('should show start screen if at beginning', () => {
            playerViewModel.started = true;
            expect(playerViewModel.activeFrameIndex).toBe(0);
            playerViewModel.gotoPrev();
            $timeout.flush();
            expect(playerViewModel.showStartScreen).toBe(true);
        });
        it('should hide finish screen if it is currently being shown', () => {
            playerViewModel.showFinishScreen = true;
            playerViewModel.gotoPrev();
            $timeout.flush();
            playerViewModel.showFinishScreen = false;
        });
        it('should gracefully handle an id in the history that does not map to a frame in the list', () => {
            // Start the frame play
            playerViewModel.started = true;

            // Advance to prev screen based on history
            const lessonProgress = LessonProgress.new();
            Lesson.prototype.ensureLessonProgress = jest.fn().mockReturnValue(lessonProgress);
            playerViewModel.lesson.ensureLessonProgress().frame_history = ['invalid_id'];
            jest.spyOn(playerViewModel, '_saveLessonAndStreamProgress').mockReturnValue($q.resolve());

            playerViewModel.activeFrameIndex = 2;
            playerViewModel.gotoPrev();
            $timeout.flush();

            // should have gone back to the previous frame in the list
            expect(playerViewModel.activeFrameIndex).toBe(1);
            expect(playerViewModel.lesson.ensureLessonProgress().frame_history.length).toBe(0);
        });
    });

    describe('_onSetShowFinishScreen', () => {
        it('should set activeFrame to false when showing finish screen', () => {
            playerViewModel.started = true;
            expect(playerViewModel.activeFrame).not.toBeUndefined();
            playerViewModel.showFinishScreen = true;
            expect(playerViewModel.activeFrame).toBeUndefined();
        });
    });

    describe('gotoNext', () => {
        it('should go to next active frame if one exists', () => {
            // Start the frame play
            playerViewModel.started = true;

            jest.spyOn(playerViewModel, 'gotoStreamCompleted').mockImplementation(() => {});
            jest.spyOn(playerViewModel, 'gotoChapterDashboard').mockImplementation(() => {});

            // Advance to next screen
            const currentFrame = playerViewModel.activeFrame;
            const lessonProgress = LessonProgress.new();
            Lesson.prototype.ensureLessonProgress = jest.fn().mockReturnValue(lessonProgress);
            playerViewModel.lesson.ensureLessonProgress().frame_history = [];
            jest.spyOn(LessonProgress.prototype, 'save').mockImplementation(() => {});

            // gotoFrame is defined in the PlayerViewModelWithFrames mixin,
            // and does not need to be tested here
            jest.spyOn(playerViewModel, 'gotoFrame').mockImplementation(() => {});
            playerViewModel.gotoNext();
            $timeout.flush();

            expect(playerViewModel.gotoFrame).toHaveBeenCalledWith(currentFrame.nextFrame());
            expect(playerViewModel.gotoStreamCompleted.mock.calls.length).toBe(0);
            expect(playerViewModel.gotoChapterDashboard.mock.calls.length).toBe(0);
            expect(playerViewModel.lesson.ensureLessonProgress().frame_history[0]).toBe(currentFrame.id);
            expect(playerViewModel.lesson.ensureLessonProgress().frame_history.length).toBe(1);
        });

        // in most cases, completed will already have been set to true once
        // the last frame was finished
        it('should call _finishSavingProgressThenMarkAsComplete when moving on from last frame', () => {
            playerViewModel.activeFrameIndex = playerViewModel.lesson.frames.length - 1;
            let resolveFinishSavingProgress;
            const promise = $q(resolve => {
                resolveFinishSavingProgress = resolve;
            });
            jest.spyOn(playerViewModel, '_finishSavingProgressThenMarkAsComplete').mockReturnValue(promise);
            playerViewModel.gotoNext();

            // while waiting for progress to save, we should
            // hide the active frame and show a spinner if necessary
            expect(playerViewModel.activeFrame).toBeUndefined();
            resolveFinishSavingProgress();
            $timeout.flush();
            expect(playerViewModel._finishSavingProgressThenMarkAsComplete).toHaveBeenCalled();
        });

        it('should set showFinishScreen to true after last frame if already completed', () => {
            // turn of progress logging to simplify the showFinishScreen logic
            playerViewModel.logProgress = false;
            jest.spyOn(playerViewModel, '_finishSavingProgressThenMarkAsComplete').mockReturnValue($q.resolve());
            playerViewModel.activeFrameIndex = playerViewModel.lesson.frames.length - 1;

            // gotoNext from the last frame should finish the lesson
            playerViewModel.gotoNext();
            $timeout.flush();
            expect(playerViewModel._finishSavingProgressThenMarkAsComplete).toHaveBeenCalled();
            $timeout.flush();
            expect(playerViewModel.showFinishScreen).toBe(true);

            // gotoPrev should hide the finish screen and show the last frame
            playerViewModel.gotoPrev();
            $timeout.flush();
            expect(playerViewModel.showFinishScreen).toBe(false);
            expect(playerViewModel.activeFrameIndex).toBe(playerViewModel.lesson.frames.length - 1);

            // gotoNext from the last frame again should not finish the lesson
            // again, but it should show the finish screen
            playerViewModel.gotoNext();
            $timeout.flush();
            expect(playerViewModel.showFinishScreen).toBe(true); // sanity check
            expect(playerViewModel.activeFrame).toBeUndefined();
        });
    });

    describe('_markAsComplete', () => {
        it('should reset the frame_bookmark_id and the call super', () => {
            playerViewModel.lessonProgress.frame_bookmark_id = 42;
            playerViewModel._markAsComplete();
            // see comment in code for why we do not set to undefined
            expect(playerViewModel.lessonProgress.frame_bookmark_id).toBe(null);
        });

        it('should set challengeScoresXOfY an array with two values', () => {
            Object.defineProperty(playerViewModel, 'challengeScores', {
                value: {
                    wrong: 0,
                    right: 1,
                },
            });
            playerViewModel._markAsComplete();
            expect(playerViewModel.challengeScoresXOfY).toEqual([1, 2]);
        });

        it('should set secondsInLesson', () => {
            Object.defineProperty(playerViewModel, 'frameDurations', {
                value: {
                    frame1: 42,
                    frame2: 13,
                },
            });
            playerViewModel._markAsComplete();
            expect(playerViewModel.secondsInLesson).toEqual(55);
        });

        it('should set the lessonScore', () => {
            Object.defineProperty(playerViewModel, 'challengeScores', {
                value: {
                    challenge1: 1,
                    challenge2: 0,
                },
            });
            playerViewModel._markAsComplete();
            expect(playerViewModel.lessonScore).toEqual(0.5);
            expect(playerViewModel.scoreMetadata).toEqual({
                totalScore: 1,
                totalChallenges: 2,
            });
        });
    });

    describe('_trackFrameChallengeStats', () => {
        it('should be called when activeFrame is completed', () => {
            playerViewModel.activeFrame = lesson.frames[0];
            jest.spyOn(playerViewModel, '_trackFrameChallengeStats').mockImplementation(() => {});

            playerViewModel.runCallbacks('frame_completed', () => {});
            expect(playerViewModel._trackFrameChallengeStats.mock.calls.length).toBe(1);
        });
    });

    describe('activeFrameId', () => {
        it('should be gettable', () => {
            expect(playerViewModel.activeFrameId).toBe(playerViewModel.activeFrame.id);
        });
        it('should be settable', () => {
            const newFrame = playerViewModel.lesson.frames[1];
            expect(newFrame).not.toBe(playerViewModel.activeFrame); // sanity check
            playerViewModel.activeFrameId = newFrame.id;
            expect(playerViewModel.activeFrame).toBe(newFrame);
        });
    });

    describe('swapFrame', () => {
        it('should throw if trying to swap out the active frame', () => {
            const lesson = FrameList.fixtures.getInstance();
            const playerViewModel = lesson.createPlayerViewModel();
            const newFrame = Frame.new();
            const frameToRemove = playerViewModel.activeFrame;
            expect(() => {
                playerViewModel.swapFrame(frameToRemove, newFrame);
            }).toThrow(
                new Error(
                    'Bad things will happen if you try to swap out the active frame.  See editFrameInfoPanelDirBase.',
                ),
            );
        });

        it('should swap out a non-active frame', () => {
            const lesson = FrameList.fixtures.getInstance();
            const playerViewModel = lesson.createPlayerViewModel();
            playerViewModel.activeFrameIndex = 0;
            const newFrame = Frame.new();
            const frameToRemove = lesson.frames[1];
            const activeFrame = playerViewModel.activeFrame;
            playerViewModel.swapFrame(frameToRemove, newFrame);
            expect(lesson.frames[1]).toBe(newFrame);
            expect(lesson.frames.indexOf(frameToRemove)).toBe(-1);
            expect(playerViewModel.activeFrame).toBe(activeFrame);
        });
    });

    describe('removeFrame', () => {
        it('should remove a frame that is not the active frame', () => {
            const playerViewModel = FrameList.fixtures.getInstance().createPlayerViewModel();
            const lesson = playerViewModel.lesson;
            playerViewModel.activeFrameIndex = 0;
            const originalActiveFrame = playerViewModel.activeFrame;

            const removeMe = lesson.frames[1];
            expect(removeMe).not.toBeUndefined(); // sanity check

            playerViewModel.removeFrame(removeMe);
            expect(lesson.frames.indexOf(removeMe)).toBe(-1);
            // activeFrame should not have changed
            expect(playerViewModel.activeFrame).toBe(originalActiveFrame);
        });

        it('should remove a frame that is active and the last in the list', () => {
            const playerViewModel = FrameList.fixtures.getInstance().reifyAllFrames().createPlayerViewModel();

            const lesson = playerViewModel.lesson;
            const originalActiveFrame = lesson.frames[lesson.frames.length - 1];
            playerViewModel.activeFrame = originalActiveFrame;

            playerViewModel.removeFrame(originalActiveFrame);
            expect(lesson.frames.indexOf(originalActiveFrame)).toBe(-1);
            expect(playerViewModel.activeFrame).toBe(lesson.frames[0]);
        });

        it('should remove a frame that is active and not the last in the list', () => {
            const playerViewModel = FrameList.fixtures.getInstance().reifyAllFrames().createPlayerViewModel();
            const lesson = playerViewModel.lesson;
            const originalActiveFrame = lesson.frames[1];
            const expectedActiveFrame = lesson.frames[2];
            playerViewModel.activeFrame = originalActiveFrame;
            expect(expectedActiveFrame).not.toBeUndefined();

            playerViewModel.removeFrame(originalActiveFrame);
            expect(playerViewModel.activeFrame).toBe(expectedActiveFrame);
        });
    });

    describe('frame_completed callback', () => {
        it('should call _trackFrameChallengeStats', () => {
            playerViewModel.activeFrameIndex = 0;
            jest.spyOn(playerViewModel, '_trackFrameChallengeStats').mockImplementation(() => {});
            playerViewModel.runCallbacks('frame_completed', () => {});
            expect(playerViewModel._trackFrameChallengeStats).toHaveBeenCalled();
        });

        it('should call _finishSavingProgressThenMarkAsComplete if on last frame', () => {
            expect(playerViewModel.completed).toBe(false); // sanity check
            jest.spyOn(playerViewModel, '_finishSavingProgressThenMarkAsComplete');
            playerViewModel.activeFrameIndex = 0;
            playerViewModel.runCallbacks('frame_completed', () => {});
            expect(playerViewModel.completed).toBe(false);

            playerViewModel.activeFrameIndex = playerViewModel.frames.length - 1;
            playerViewModel.runCallbacks('frame_completed', () => {});
            expect(playerViewModel._finishSavingProgressThenMarkAsComplete).toHaveBeenCalled();
        });

        it('should progress frameBookmark only if frame is configured to do so', () => {
            playerViewModel.activeFrameIndex = 0;
            playerViewModel.activeFrame.mainUiComponent.savesProgressOnComplete = false;

            // starting the first frame should set the bookmark
            playerViewModel.runCallbacks('frame_started', () => {});
            expect(playerViewModel.frameBookmarkId).toEqual(lesson.frames[0].id);

            // since savesProgressOnComplete is false, completing the frame should
            // not change the bookmark
            playerViewModel.runCallbacks('frame_completed', () => {});
            expect(playerViewModel.frameBookmarkId).toEqual(lesson.frames[0].id);

            playerViewModel.activeFrameIndex = 1;
            playerViewModel.activeFrame.mainUiComponent.savesProgressOnComplete = true;

            // starting the second frame should set the bookmark
            playerViewModel.runCallbacks('frame_started', () => {});
            expect(playerViewModel.frameBookmarkId).toEqual(lesson.frames[1].id);

            // since savesProgressOnComplete is true, completing the frame should
            //  change the bookmark
            playerViewModel.runCallbacks('frame_completed', () => {});
            expect(playerViewModel.frameBookmarkId).toEqual(lesson.frames[2].id);
        });
    });

    describe('_trackFrameChallengeStats', () => {
        let lesson;
        let playerViewModel;
        beforeEach(() => {
            lesson = FrameList.fixtures.getInstance();
            lesson.frames = [Componentized.fixtures.getInstance(), Componentized.fixtures.getInstance()];
            lesson.frames.forEach(frame => {
                frame.mainUiComponent = frame.addChallenges();
                frame.mainUiComponent.challenges = [frame.addChallenge().addDefaultReferences()];
            });
            playerViewModel = lesson.createPlayerViewModel();
            playerViewModel.activeFrameIndex = 0;
        });

        describe('with logProgress on', () => {
            beforeEach(() => {
                playerViewModel.logProgress = true;
                playerViewModel.lesson.ensureLessonProgress();
            });

            it('should update completedFrames', () => {
                const expectedFramesSeen = {};
                expect(playerViewModel.lesson.lesson_progress.completed_frames).toEqual(expectedFramesSeen);
                expect(playerViewModel.completedFrames).toEqual(expectedFramesSeen);

                expectedFramesSeen[playerViewModel.activeFrame.id] = true;
                playerViewModel._trackFrameChallengeStats();
                expect(playerViewModel.lesson.lesson_progress.completed_frames).toEqual(expectedFramesSeen);
                expect(playerViewModel.completedFrames).toEqual(expectedFramesSeen);
            });
        });

        describe('setChallengeScore', () => {
            let challengeViewModelA;
            let challengeViewModelB;
            let challengeAScore;
            let challengeBScore;

            beforeEach(() => {
                challengeViewModelA = {
                    getScore: jest.fn().mockImplementation(() => challengeAScore),
                    frame: {
                        id: 'frame',
                    },
                    model: {
                        id: 'a',
                    },
                };
                challengeViewModelB = {
                    getScore: jest.fn().mockImplementation(() => challengeBScore),
                    frame: {
                        id: 'frame',
                    },
                    model: {
                        id: 'b',
                    },
                };

                playerViewModel.lesson.ensureLessonProgress();
                jest.spyOn(playerViewModel.lessonProgress, 'save');
            });

            describe('with logProgress on', () => {
                beforeEach(() => {
                    playerViewModel.logProgress = true;
                });

                it('should do nothing if score is not set', () => {
                    challengeAScore = null;

                    expect(() => {
                        playerViewModel.setChallengeScore(challengeViewModelA);
                    }).not.toThrow();
                });

                describe('on regular lesson', () => {
                    it('should update if the score is changing', () => {
                        challengeAScore = 1;
                        challengeBScore = 0;
                        playerViewModel.setChallengeScore(challengeViewModelA);
                        playerViewModel.setChallengeScore(challengeViewModelB);
                        expect(playerViewModel.challengeScores).toEqual({
                            'frame,a': 1,
                            'frame,b': 0,
                        });

                        // We do not save here if this is not a test lesson
                        expect(playerViewModel.lessonProgress.save).not.toHaveBeenCalled();
                    });
                });

                describe('on testOrAssessment lesson', () => {
                    beforeEach(() => {
                        jest.spyOn(playerViewModel, 'testOrAssessment', 'get').mockReturnValue(true);
                    });

                    it('should update if there is no current score', () => {
                        challengeAScore = 1;
                        playerViewModel.setChallengeScore(challengeViewModelA);
                        playerViewModel.lessonProgress.wtf = 'wtf????';
                        expect(playerViewModel.challengeScores).toEqual({
                            'frame,a': 1,
                        });

                        expect(playerViewModel.lessonProgress.save).toHaveBeenCalled();
                    });

                    it('should not update if there is already a current score', () => {
                        playerViewModel.challengeScores['frame,a'] = 0;
                        challengeAScore = 1;
                        playerViewModel.setChallengeScore(challengeViewModelA);
                        expect(playerViewModel.challengeScores).toEqual({
                            'frame,a': 0,
                        });

                        expect(playerViewModel.lessonProgress.save).not.toHaveBeenCalled();
                    });
                });
            });

            it('should not save progress for a testOrAssessment when editorMode is true', () => {
                playerViewModel.editorMode = true;
                playerViewModel.logProgress = true; // prevent false-positives
                jest.spyOn(playerViewModel, 'testOrAssessment', 'get').mockReturnValue(true);

                challengeAScore = 1;
                playerViewModel.setChallengeScore(challengeViewModelA);
                expect(playerViewModel.lessonProgress.save).not.toHaveBeenCalled();
            });

            it('should not save progress for a testOrAssessment when logProgress is off', () => {
                playerViewModel.editorMode = false; // prevent false-positives
                playerViewModel.logProgress = false;
                jest.spyOn(playerViewModel, 'testOrAssessment', 'get').mockReturnValue(true);

                challengeAScore = 1;
                playerViewModel.setChallengeScore(challengeViewModelA);
                expect(playerViewModel.lessonProgress.save).not.toHaveBeenCalled();
            });
        });

        describe('with logProgress off', () => {
            beforeEach(() => {
                playerViewModel.logProgress = false;
                jest.spyOn(playerViewModel.lesson, 'ensureLessonProgress').mockImplementation(() => {});
            });

            afterEach(() => {
                expect(playerViewModel.lesson.ensureLessonProgress).not.toHaveBeenCalled();
            });

            it('should update completedFrames', () => {
                const expectedFramesSeen = {};
                expect(playerViewModel.completedFrames).toEqual(expectedFramesSeen);

                expectedFramesSeen[playerViewModel.activeFrame.id] = true;
                playerViewModel._trackFrameChallengeStats();
                expect(playerViewModel.completedFrames).toEqual(expectedFramesSeen);
            });

            it('should update challengeScores', () => {
                playerViewModel.activeFrame.mainUiComponent.challenges.forEach(challenge => {
                    const viewModel = playerViewModel.activeFrameViewModel.viewModelFor(challenge);
                    jest.spyOn(viewModel, 'getScore').mockReturnValue(1);
                });

                const frame = playerViewModel.activeFrame;
                playerViewModel._trackFrameChallengeStats();
                const expectedChallengeScores = {};
                expectedChallengeScores[`${frame.id},${frame.mainUiComponent.challenges[0].id}`] = 1;
            });
        });
    });

    describe('frameHistory', () => {
        let lesson;
        let playerViewModel;
        beforeEach(() => {
            lesson = FrameList.fixtures.getInstance();
            lesson.frames = [Componentized.fixtures.getInstance(), Componentized.fixtures.getInstance()];
            playerViewModel = lesson.createPlayerViewModel();
            playerViewModel.activeFrameIndex = 0;
        });

        describe('with logProgress on', () => {
            beforeEach(() => {
                playerViewModel.logProgress = true;
                playerViewModel.lesson.ensureLessonProgress();
            });

            it('should access the frame history from the lesson_progress', () => {
                expect(playerViewModel.frameHistory).toBe(playerViewModel.lesson.lesson_progress.frame_history);
                const newFrameHistory = [];
                playerViewModel.frameHistory = newFrameHistory;
                expect(playerViewModel.frameHistory).toBe(newFrameHistory);
                expect(playerViewModel.lesson.lesson_progress.frame_history).toBe(newFrameHistory);
            });
        });

        describe('with logProgress off', () => {
            beforeEach(() => {
                playerViewModel.logProgress = false;
                jest.spyOn(playerViewModel.lesson, 'ensureLessonProgress').mockImplementation(() => {});
            });

            it('should exist', () => {
                expect(playerViewModel.frameHistory).toEqual([]);
                const newFrameHistory = [];
                playerViewModel.frameHistory = newFrameHistory;
                expect(playerViewModel.frameHistory).toEqual([]);
            });

            afterEach(() => {
                expect(playerViewModel.lesson.ensureLessonProgress).not.toHaveBeenCalled();
            });
        });
    });

    describe('frameDurations', () => {
        let lesson;
        let playerViewModel;

        beforeEach(() => {
            duration = 42;
            lesson = FrameList.fixtures.getInstance();
            lesson.frames = [Componentized.fixtures.getInstance(), Componentized.fixtures.getInstance()];
            lesson.frames.forEach(frame => {
                frame.mainUiComponent = frame.addChallenges();
                frame.mainUiComponent.challenges = [frame.addChallenge().addDefaultReferences()];
            });
            playerViewModel = lesson.createPlayerViewModel();
            playerViewModel.activeFrameIndex = 0;
        });

        describe('with logProgress on', () => {
            beforeEach(() => {
                playerViewModel.logProgress = true;
                playerViewModel.lesson.ensureLessonProgress();
            });

            it('should update on frame unload', () => {
                const frame = playerViewModel.activeFrame;
                playerViewModel.activeFrameIndex = 1;
                expect(playerViewModel.frameDurations[frame.id]).toEqual(duration);
                playerViewModel.activeFrameIndex = 0;
                playerViewModel.activeFrameIndex = 1;
                expect(playerViewModel.frameDurations[frame.id]).toEqual(2 * duration);
            });

            it('should limit frame time to 120 seconds', () => {
                duration = 9999;
                const frame = playerViewModel.activeFrame;
                playerViewModel.activeFrameIndex = 1;
                expect(playerViewModel.frameDurations[frame.id]).toEqual(120);
            });
        });

        describe('with logProgress off', () => {
            beforeEach(() => {
                playerViewModel.logProgress = false;
                jest.spyOn(playerViewModel.lesson, 'ensureLessonProgress').mockImplementation(() => {});
            });

            afterEach(() => {
                expect(playerViewModel.lesson.ensureLessonProgress).not.toHaveBeenCalled();
            });

            it('should update on frame unload', () => {
                const frame = playerViewModel.activeFrame;
                playerViewModel.activeFrameIndex = 1;
                expect(playerViewModel.frameDurations[frame.id]).toEqual(duration);
                playerViewModel.activeFrameIndex = 0;
                playerViewModel.activeFrameIndex = 1;
                expect(playerViewModel.frameDurations[frame.id]).toEqual(2 * duration);
            });

            it('should limit frame time to 120 seconds', () => {
                duration = 9999;
                const frame = playerViewModel.activeFrame;
                playerViewModel.activeFrameIndex = 1;
                expect(playerViewModel.frameDurations[frame.id]).toEqual(120);
            });
        });
    });

    describe('shouldShowPreviousButton', () => {
        it('should be false for assessment lessons', () => {
            playerViewModel.showStartScreen = false;
            playerViewModel.lesson.assessment = false;
            expect(playerViewModel.shouldShowPreviousButton).toBe(true);
            playerViewModel.lesson.assessment = true;
            expect(playerViewModel.shouldShowPreviousButton).toBe(false);
        });

        it('should be false for test lessons', () => {
            playerViewModel.showStartScreen = false;
            playerViewModel.lesson.test = false;
            expect(playerViewModel.shouldShowPreviousButton).toBe(true);
            playerViewModel.lesson.assessment = true;
            expect(playerViewModel.shouldShowPreviousButton).toBe(false);
        });

        it('should be false on the start screen', () => {
            playerViewModel.showStartScreen = true;
            expect(playerViewModel.shouldShowPreviousButton).toBe(false);
            playerViewModel.showStartScreen = false;
            expect(playerViewModel.shouldShowPreviousButton).toBe(true);
        });

        it('should be true for assessment lessons in preview mode, except on start screen', () => {
            playerViewModel.$$previewMode = true;
            playerViewModel.showStartScreen = true;
            playerViewModel.lesson.assessment = true;

            // start screen
            expect(playerViewModel.shouldShowPreviousButton).toBe(false);
            playerViewModel.showStartScreen = false;
            // other screens
            expect(playerViewModel.shouldShowPreviousButton).toBe(true);
        });
    });

    describe('canNavigateFreely', () => {
        it('should work', () => {
            let canEdit = false;
            jest.spyOn($rootScope.currentUser, 'canEditLesson').mockImplementation(() => canEdit);
            expect(playerViewModel.canNavigateFreely).toBe(false);
            canEdit = true;
            expect(playerViewModel.canNavigateFreely).toBe(true);
            expect($rootScope.currentUser.canEditLesson).toHaveBeenCalledWith(playerViewModel.lesson);
        });
    });

    describe('canNavigateBackToCompletedFrames', () => {
        it('should be true if canNavigateFreely', () => {
            jest.spyOn(playerViewModel, 'canNavigateFreely', 'get').mockReturnValue(true);
            playerViewModel.lesson.test = true;
            expect(playerViewModel.canNavigateBackToCompletedFrames).toBe(true);
        });

        it('should be false only if if test/assessment lesson', () => {
            jest.spyOn(playerViewModel, 'canNavigateFreely', 'get').mockReturnValue(false);

            playerViewModel.lesson.test = true;
            playerViewModel.lesson.assessment = false;
            expect(playerViewModel.canNavigateBackToCompletedFrames).toBe(false);

            playerViewModel.lesson.test = false;
            playerViewModel.lesson.assessment = true;
            expect(playerViewModel.canNavigateBackToCompletedFrames).toBe(false);

            playerViewModel.lesson.test = false;
            playerViewModel.lesson.assessment = false;
            expect(playerViewModel.canNavigateBackToCompletedFrames).toBe(true);
        });
    });
});
