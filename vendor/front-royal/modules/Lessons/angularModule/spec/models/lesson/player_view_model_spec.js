import 'AngularSpecHelper';
import 'Lessons/angularModule';

describe('Lessons::Lesson::PlayerViewModel', () => {
    let playerViewModel;
    let AppHeaderViewModel;
    let SpecHelper;
    let $injector;
    let MessagingService;
    let $location;
    let PlayerViewModel;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector => {
                $injector = _$injector;
                MessagingService = $injector.get('Lesson.MessagingService');
                AppHeaderViewModel = $injector.get('Navigation.AppHeader.AppHeaderViewModel');
                SpecHelper = $injector.get('SpecHelper');
                $location = $injector.get('$location');
                PlayerViewModel = $injector.get('Lesson.PlayerViewModel');
                playerViewModel = new PlayerViewModel(null, {});

                Object.defineProperty(PlayerViewModel.prototype, 'sessionId', {
                    value: 'sessionId',
                });
            },
        ]);
    });

    describe('destroy', () => {
        it('should set destroyed to true', () => {
            expect(playerViewModel.destroyed).toBe(false);
            playerViewModel.destroy();
            expect(playerViewModel.destroyed).toBe(true);
        });
        it('should call delinkFromWindows', () => {
            jest.spyOn(playerViewModel, 'delinkFromWindows').mockImplementation(() => {});
            playerViewModel.destroy();
            expect(playerViewModel.delinkFromWindows).toHaveBeenCalled();
        });
        it('should only trigger destroy callback the first time called', () => {
            const callback = jest.fn();
            PlayerViewModel.setCallback('after', 'destroyed', callback);
            playerViewModel.destroy();
            playerViewModel.destroy();
            expect(callback.mock.calls.length).toBe(1);
        });
        it("should clear the user's progress cache", () => {
            const user = SpecHelper.stubCurrentUser('learner');
            jest.spyOn(user.progress, 'onPlayerViewModelDestroyed').mockImplementation(() => {});
            playerViewModel.destroy();
            expect(user.progress.onPlayerViewModelDestroyed).toHaveBeenCalled();
        });
    });

    describe('logInfo', () => {
        it('should work', () => {
            expect(playerViewModel.id).not.toBeUndefined();
            expect(playerViewModel.logInfo()).toEqual({
                lesson_play_id: playerViewModel.id,
                editorMode: false,
                previewMode: false,
                demoMode: false,
                lesson_player_session_id: 'sessionId',
            });
        });

        it('should add a player session when not in editor mode', () => {
            expect(playerViewModel.logInfo().lesson_player_session_id).not.toBeUndefined();
        });

        it('should add an editor session when not in editor mode', () => {
            playerViewModel.editorMode = true;
            const sessionId = playerViewModel.logInfo().lesson_editor_session_id;
            playerViewModel.editorMode = false;
            playerViewModel.previewMode = true;
            expect(playerViewModel.logInfo().lesson_editor_session_id).toEqual(sessionId);
        });
    });

    describe('previewMode', () => {
        it('should read initial value from query params', () => {
            jest.spyOn($location, 'search').mockReturnValue({
                preview: 'y',
            });
            const playerViewModel = new PlayerViewModel(null, {});
            expect(playerViewModel.previewMode).toBe(true);
        });
        it('should update query params when value changes', () => {
            jest.spyOn($location, 'search').mockImplementation(() => {});
            playerViewModel.previewMode = true;
            expect($location.search).toHaveBeenCalledWith('preview', 'y');
        });
    });

    describe('linkToWindow', () => {
        it('should link to app header view model', () => {
            // var chapter = playerViewModel.lesson.chapter();
            // var index = chapter.lessonIds.indexOf(playerViewModel.lesson.id);
            // var count = chapter.lessonIds.length;

            playerViewModel.linkToWindow($injector);
            expect(AppHeaderViewModel.playerViewModel).toBe(playerViewModel);

            // should also delink
            playerViewModel.delinkFromWindows();
            expect(AppHeaderViewModel.playerViewModel).toBeUndefined();
            expect(AppHeaderViewModel.textRows).toBeUndefined();
        });

        it('should link to the messaging service', () => {
            jest.spyOn(MessagingService, 'showMessage').mockImplementation(() => {});
            jest.spyOn(MessagingService, 'clearMessage').mockImplementation(() => {});
            playerViewModel.linkToWindow($injector);

            playerViewModel.showMessage('ok');
            expect(MessagingService.showMessage).toHaveBeenCalledWith('ok');
            playerViewModel.clearMessage();
            expect(MessagingService.clearMessage).toHaveBeenCalled();

            playerViewModel.delinkFromWindows();
            MessagingService.showMessage.mockClear();
            MessagingService.clearMessage.mockClear();
            playerViewModel.showMessage('ok');
            expect(MessagingService.showMessage).not.toHaveBeenCalled();
            playerViewModel.clearMessage();
            expect(MessagingService.clearMessage).not.toHaveBeenCalled();
        });
    });

    describe('setBodyBackground', () => {
        it('should set body background on AppHeaderViewModel', () => {
            playerViewModel.linkToWindow($injector);
            jest.spyOn(AppHeaderViewModel, 'setBodyBackground').mockImplementation(() => {});
            playerViewModel.setBodyBackground('fuschia');
            expect(AppHeaderViewModel.setBodyBackground).toHaveBeenCalledWith('fuschia');
        });
    });
});
