import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/lessons';

describe('Lessons::Lesson::Stream::Chapter', () => {
    let Lesson;
    let stream;
    let chapter;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'SpecHelper');

        angular.mock.inject([
            'Lesson.Stream',
            'Lesson.Stream.Chapter',
            'Lesson',
            'MockIguana',
            'LessonFixtures',
            'StreamFixtures',
            (Stream, _Chapter_, _Lesson_) => {
                Lesson = _Lesson_;
                Stream.setAdapter('Iguana.Mock.Adapter');
                stream = Stream.fixtures.getInstance();
                chapter = stream.chapters[0];
            },
        ]);
    });

    describe('lessons', () => {
        describe('get', () => {
            it('should look up the lessons on the stream', () => {
                expect(chapter.lessons.map(l => l.id)).toEqual(chapter.lessonIds);
            });
            it('should filter out missing lessons', () => {
                const origLessonIds = chapter.lessonIds.slice(0);
                chapter.lesson_hashes.push({
                    lesson_id: 'you will not find this lesson',
                });
                expect(chapter.lessons.map(l => l.id)).toEqual(origLessonIds);
            });
        });
    });

    describe('remove', () => {
        it('should delegate to the stream', () => {
            jest.spyOn(stream, 'removeChapter').mockImplementation(() => {});
            chapter.remove();
            expect(stream.removeChapter).toHaveBeenCalledWith(chapter);
        });

        it('should do nothing if there is no stream', () => {
            chapter.$$embeddedIn = undefined;
            // should not cause an error
            chapter.remove();
        });
    });

    describe('removeLesson', () => {
        it('should remove the lesson_id from lesson_hash', () => {
            const lesson = chapter.lessons[0];
            expect(chapter.lessonIds.indexOf(lesson.id)).not.toBe(-1);
            chapter.removeLesson(lesson);
            expect(chapter.lessonIds.indexOf(lesson.id)).toBe(-1);
        });
        it('should call remove lessons on the stream', () => {
            jest.spyOn(stream, 'removeLessonsIfUnused').mockImplementation(() => {});
            const lesson = chapter.lessons[0];
            chapter.removeLesson(lesson);
            expect(stream.removeLessonsIfUnused).toHaveBeenCalledWith([lesson]);
        });
    });

    describe('togglePending', () => {
        it('should work', () => {
            expect(chapter.pending).toBe(false);
            chapter.togglePending();
            expect(chapter.pending).toBe(true);
            chapter.togglePending();
            expect(chapter.pending).toBe(false);
        });
    });

    describe('addLesson', () => {
        it('should work', () => {
            const lesson = Lesson.fixtures.getInstance();
            chapter.addLesson(lesson);
            expect(_.last(chapter.lessonIds)).toBe(lesson.id);
            expect(_.last(chapter.lessons)).toBe(lesson);
            expect(_.last(chapter.stream().lessons)).toBe(lesson);
        });
    });

    describe('progressStatus', () => {
        it('should reflect the progress within the lessons', () => {
            // Make a total of 3 lessons to play with
            while (chapter.lessons.length < 3) {
                chapter.addLesson(Lesson.fixtures.getInstance());
            }

            mockLessonsWithStatuses(['not_started', 'not_started', 'not_started']);

            expect(chapter.progressStatus()).toBe('not_started');
            expect(chapter.started).toBe(false);
            expect(chapter.complete).toBe(false);

            mockLessonsWithStatuses(['in_progress', 'not_started', 'not_started']);

            expect(chapter.progressStatus()).toBe('in_progress');
            expect(chapter.started).toBe(true);
            expect(chapter.complete).toBe(false);

            mockLessonsWithStatuses(['completed', 'completed', 'in_progress']);

            expect(chapter.progressStatus()).toBe('in_progress');
            expect(chapter.started).toBe(true);
            expect(chapter.complete).toBe(false);

            mockLessonsWithStatuses(['completed', 'not_started', 'not_started']);

            expect(chapter.progressStatus()).toBe('in_progress');
            expect(chapter.started).toBe(true);
            expect(chapter.complete).toBe(false);

            mockLessonsWithStatuses(['completed', 'completed', 'completed']);

            expect(chapter.progressStatus()).toBe('completed');
            expect(chapter.started).toBe(true);
            expect(chapter.complete).toBe(true);
        });
    });

    describe('lessonPositionFor', () => {
        it('should return an object with information for the lesson', () => {
            expect(chapter.lessonPositionFor(chapter.lessons[0])).toEqual({
                index: 0,
                lessonCount: chapter.lessons.length,
            });
        });
        it('should throw if lesson is not in the chapter', () => {
            expect(() => {
                chapter.lessonPositionFor({});
            }).toThrow(new Error('Lesson not in chapter'));
        });
    });

    function mockLessonsWithStatuses(statuses) {
        let index = 0;
        chapter.lessons.forEach(lesson => {
            const status = index < statuses.length ? statuses[index] : 'not_started';
            if (lesson.progressStatus.and && lesson.progressStatus.mockReturnValue) {
                lesson.progressStatus.mockReturnValue(status);
            } else {
                jest.spyOn(lesson, 'progressStatus').mockReturnValue(status);
            }
            index++;
        });
    }
});
