import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';
import 'Lessons/angularModule/spec/_mock/fixtures/lessons';
import 'Playlists/angularModule/spec/_mock/fixtures/playlists';

/* eslint-disable no-shadow */
describe('Lessons::Lesson::Stream', () => {
    let Stream;
    let $injector;
    let $rootScope;
    let stream;
    let SpecHelper;
    let StreamProgress;
    let Lesson;
    let Playlist;
    let ContentAccessHelper;
    let frontRoyalStoreEnabled;
    let $timeout;
    let $q;
    let DialogModal;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'SpecHelper');

        angular.mock.module($provide => {
            const frontRoyalStore = {};
            Object.defineProperty(frontRoyalStore, 'enabled', {
                get() {
                    return frontRoyalStoreEnabled;
                },
            });
            $provide.value('frontRoyalStore', frontRoyalStore);
        });

        angular.mock.inject([
            '$injector',
            'MockIguana',
            'LessonFixtures',
            'StreamFixtures',
            'PlaylistFixtures',

            _$injector => {
                $injector = _$injector;
                Lesson = $injector.get('Lesson');
                Stream = $injector.get('Lesson.Stream');
                $rootScope = $injector.get('$rootScope');
                Stream.setAdapter('Iguana.Mock.Adapter');
                stream = Stream.fixtures.getInstance();
                SpecHelper = $injector.get('SpecHelper');
                StreamProgress = $injector.get('Lesson.StreamProgress');
                Playlist = $injector.get('Playlist');
                ContentAccessHelper = $injector.get('ContentAccessHelper');
                $timeout = $injector.get('$timeout');
                $q = $injector.get('$q');
                DialogModal = $injector.get('DialogModal');

                $injector.get('LessonFixtures');
                frontRoyalStoreEnabled = false;
            },
        ]);
    });

    it('should clear cache if user changes', () => {
        jest.spyOn(Stream, 'resetCache').mockImplementation(() => {});
        const user = SpecHelper.stubCurrentUser();
        $rootScope.$apply();

        $rootScope.currentUser = angular.copy(user);
        $rootScope.$apply();

        $rootScope.currentUser = angular.copy(user);
        $rootScope.$apply();
        expect(Stream.resetCache).toHaveBeenCalledTimes(2);
    });

    describe('setCache', () => {
        it('should work', () => {
            Stream.resetCache();
            expect(Stream._streamCache).toEqual({});
            const stream = Stream.fixtures.getInstance();
            Stream.setCache(stream);
            expect(Stream._streamCache[stream.id].streamId).toEqual(stream.id);
        });
    });

    describe('lessonForId', () => {
        it('should return a lesson if it finds one', () => {
            const stream = Stream.fixtures.getInstance();
            const guid = stream.lessons[1].id;
            expect(stream.lessonForId(guid)).toBe(stream.lessons[1]);
        });

        it('should return a new lesson added after it has been called once', () => {
            const stream = Stream.fixtures.getInstance();
            const newLesson = Stream.fixtures.getInstance().lessons[0];
            const guid = stream.lessons[1].id;
            expect(stream.lessonForId(guid)).toBe(stream.lessons[1]);
            stream.lessons.push(newLesson);
            expect(stream.lessonForId(newLesson.id)).toBe(newLesson);
        });

        it('should throw an error if it does not find one', () => {
            const stream = Stream.fixtures.getInstance();
            expect(() => {
                stream.lessonForId('MISSING_GUID');
            }).toThrow(new Error('No lesson found for lesson_id=MISSING_GUID.'));
        });
    });

    describe('savingProgress', () => {
        it('should be true if stream progress is saving', () => {
            expect(stream.savePromise).toBe(undefined);
            stream.lesson_streams_progress.$$savePromise = 'promise';
            expect(stream.savePromise).not.toBe(undefined);
        });

        it('should be true if lesson progress is saving', () => {
            expect(stream.savePromise).toBe(undefined);
            stream.lessons[0].ensureLessonProgress();
            stream.lessons[0].lesson_progress.$$savePromise = 'promise';
            expect(stream.savePromise).not.toBe(undefined);
        });
    });

    describe('removeChapter', () => {
        let stream;
        let chapter;

        beforeEach(() => {
            stream = Stream.fixtures.getInstance();
            chapter = stream.chapters[0];
            expect(chapter).not.toBeUndefined();
        });

        it('should remove the chapter', () => {
            stream.removeChapter(chapter);
            expect(stream.chapters.indexOf(chapter)).toBe(-1);
        });

        it('should remove lessons', () => {
            const firstLesson = chapter.lessons[0];
            expect(firstLesson).not.toBeUndefined();
            stream.removeChapter(chapter);
            expect(stream.lessons.indexOf(firstLesson)).toBe(-1);
        });
    });

    describe('removeLessonsIfUnused', () => {
        it('should not remove a lesson that is in the lessons list for a chapter', () => {
            const stream = Stream.fixtures.getInstance();
            const lesson = stream.chapters[0].lessons[0];
            stream.removeLessonsIfUnused([lesson]);
            expect(stream.lessons.indexOf(lesson)).not.toBe(-1);
        });
        it('should remove an unused lesson', () => {
            const stream = Stream.fixtures.getInstance();
            const lesson = stream.chapters[0].lessons[0];
            stream.chapters[0].lesson_hashes = _.filter(
                stream.chapters[0].lesson_hashes,
                lessonHash => lessonHash.lesson_id !== lesson.id,
            );
            stream.removeLessonsIfUnused([lesson]);
            expect(stream.lessons.indexOf(lesson)).toBe(-1);
        });
    });

    describe('addLesson', () => {
        it('should have a test', () => {
            const stream = Stream.fixtures.getInstance();
            const anotherLesson = Stream.fixtures.getInstance().lessons[0];
            stream.addLesson(anotherLesson);
            expect(_.last(stream.lessons)).toBe(anotherLesson);
            expect(anotherLesson.stream()).toBe(stream);
        });
    });

    describe('getCachedOrShow', () => {
        let stream;
        let callback;

        describe('with a currentUser', () => {
            beforeEach(() => {
                SpecHelper.stubCurrentUser();
                stream = Stream.fixtures.getInstance({
                    id: 'id',
                });
                Stream.resetCache();
                Stream.expect('show')
                    .toBeCalledWith('id', {
                        include_progress: true,
                        filters: {},
                    })
                    .returns(stream);
                callback = jest.fn();
            });

            it('should return an unresolved promise if there is not yet an active request', () => {
                Stream.getCachedOrShow('id').then(callback);
                expect(callback).not.toHaveBeenCalled();
                Stream.flush('show');
                expect(callback).toHaveBeenCalled();
                expect(callback.mock.calls[0][0].id).toBe(stream.id);
            });

            it('should return an unresolved promise if there is already an active request', () => {
                jest.spyOn($rootScope.currentUser.progress, 'replaceProgress').mockImplementation(stream => {
                    return $q.when(stream);
                });
                jest.spyOn(Stream, 'show').mockReturnValue($q.when({ result: stream }));
                const promise1 = Stream.getCachedOrShow('id');
                const promise2 = Stream.getCachedOrShow('id');
                promise2.then(callback);
                promise1.then(callback);
                expect(callback).not.toHaveBeenCalled();
                $timeout.flush();
                expect(callback).toHaveBeenCalled();
                expect(callback.mock.calls.length).toBe(2);
                expect(callback.mock.calls[0][0].id).toBe(stream.id);
                expect(Stream.show.mock.calls.length).toBe(1); // show should have only been called once
            });

            it('should return an already resolved promise if the stream is already loaded', () => {
                Stream.getCachedOrShow('id');
                Stream.flush('show');
                Stream.getCachedOrShow('id').then(callback);
                $rootScope.$digest();
                expect(callback).toHaveBeenCalled();
                expect(callback.mock.calls[0][0].id).toBe(stream.id);
            });

            it('should call replaceProgress', () => {
                jest.spyOn($rootScope.currentUser.progress, 'replaceProgress').mockReturnValue($q.when('clone'));
                Stream.getCachedOrShow('id').then(callback);
                Stream.flush('show');
                $timeout.flush();
                expect($rootScope.currentUser.progress.replaceProgress.mock.calls[0][0].id).toBe(stream.id);
                expect(callback).toHaveBeenCalledWith('clone');
            });
        });

        describe('with no currentUser', () => {
            beforeEach(() => {
                SpecHelper.stubNoCurrentUser();
                stream = Stream.fixtures.getInstance({
                    id: 'id',
                });
                Stream.resetCache();
            });

            it('should set appropriate options', () => {
                Stream.expect('show')
                    .toBeCalledWith('id', {
                        include_progress: false,
                        filters: {
                            view_as: 'cohort:PROMOTED_DEGREE',
                            in_locale_or_en: 'en',
                            user_can_see: null,
                            in_users_locale_or_en: null,
                        },
                    })
                    .returns(stream);
                callback = jest.fn();

                Stream.getCachedOrShow('id').then(callback);
                expect(callback).not.toHaveBeenCalled();
                Stream.flush('show');
                expect(callback).toHaveBeenCalled();
                expect(callback.mock.calls[0][0].id).toBe(stream.id);
            });

            it('should not save promise if result is a 404', () => {
                const params = {
                    include_progress: false,
                    filters: {
                        view_as: 'cohort:PROMOTED_DEGREE',
                        in_locale_or_en: 'en',
                        user_can_see: null,
                        in_users_locale_or_en: null,
                    },
                };

                // tricky: we disable the ApiErrorHandler to allow custom error handling
                const options = {
                    'FrontRoyal.ApiErrorHandler': {
                        skip: true,
                    },
                };

                Stream.expect('show')
                    .toBeCalledWith('id', params, options)
                    .returns({
                        error: {
                            status: 404,
                            config: {
                                url: '/api/',
                            },
                        },
                    });

                const successCallback = jest.fn();
                const errorCallback = jest.fn();

                Stream.getCachedOrShow('id', params, options).then(successCallback, errorCallback);
                Stream.flush('show');

                expect(successCallback).not.toHaveBeenCalled();
                expect(errorCallback).toHaveBeenCalled();
                expect(errorCallback.mock.calls[0][0].status).toBe(404);

                expect(Stream._streamCache.id).toBeUndefined();
            });
        });
    });

    describe('showWithFullContentForLesson', () => {
        let stream;
        let callback;
        let targetLesson;

        beforeEach(() => {
            stream = Stream.fixtures.getInstance();
            targetLesson = stream.lessons[0];
            callback = jest.fn();
        });

        it('should return a cached stream if all of the lesson content is loaded', () => {
            SpecHelper.stubCurrentUser();
            Object.defineProperty(targetLesson, 'allContentLoaded', {
                value: true,
            });

            jest.spyOn($rootScope.currentUser.progress, 'replaceProgress').mockReturnValue($q.when('clone'));
            Stream.showWithFullContentForLesson(stream.id, targetLesson.id).then(callback);
            $rootScope.$digest();
            expect($rootScope.currentUser.progress.replaceProgress).toHaveBeenCalledWith(stream);
            expect(callback).toHaveBeenCalledWith('clone');
        });

        it('should call getCachedOrShow if there is not a cached stream', () => {
            Stream.resetCache();
            jest.spyOn(Stream, 'getCachedOrShow').mockImplementation(() => {});
            Stream.showWithFullContentForLesson(stream.id, targetLesson.id);
            expect(Stream.getCachedOrShow).toHaveBeenCalledWith(
                stream.id,
                {
                    load_full_content_for_lesson_id: targetLesson.id,
                },
                undefined,
            );
        });

        it('should call reload if the cached stream has not loaded lesson content', () => {
            Object.defineProperty(targetLesson, 'allContentLoaded', {
                value: false,
            });

            jest.spyOn(Stream, 'getCachedOrShow').mockReturnValue('promise');
            const result = Stream.showWithFullContentForLesson(stream.id, targetLesson.id);
            expect(Stream.getCachedOrShow).toHaveBeenCalledWith(
                stream.id,
                {
                    load_full_content_for_lesson_id: targetLesson.id,
                },
                undefined,
            );
            expect(result).toBe('promise');
        });
    });

    describe('caching instances', () => {
        it('should cache an instance that is created with an id', () => {
            const stream = Stream.fixtures.getInstance();
            expect(Stream._streamCache[stream.id].stream).toBe(stream);
        });
        it('should not cache an instance that is created without an id', () => {
            Stream.resetCache();
            Stream.new();
            expect(Stream._streamCache).toEqual({});
        });
    });

    describe('ensureStreamProgress', () => {
        let stream;

        beforeEach(() => {
            stream = Stream.fixtures.getInstance();
            jest.spyOn(StreamProgress, 'startStream').mockReturnValue('mockStreamProgress');
        });
        it('should return an existing stream progress', () => {
            expect(stream.lesson_streams_progress).not.toBeUndefined();
            expect(stream.ensureStreamProgress()).toBe(stream.lesson_streams_progress);
            expect(StreamProgress.startStream).not.toHaveBeenCalled();
        });
        it('should create a new stream progress', () => {
            stream.lesson_streams_progress = undefined;
            const streamProgress = stream.ensureStreamProgress();
            expect(streamProgress).toBe('mockStreamProgress');
            expect(StreamProgress.startStream).toHaveBeenCalledWith(stream);
        });
    });

    describe('currentChapter', () => {
        it('should reflect progress of chapters', () => {
            const stream = Stream.fixtures.getInstance();

            // Basic cases
            mockChaptersWithStatuses(stream, ['not_started', 'not_started']);
            expect(stream.currentChapter().index).toBe(0);

            mockChaptersWithStatuses(stream, ['in_progress', 'not_started']);
            expect(stream.currentChapter().index).toBe(0);

            mockChaptersWithStatuses(stream, ['completed', 'not_started']);
            expect(stream.currentChapter().index).toBe(1);

            mockChaptersWithStatuses(stream, ['completed', 'in_progress']);
            expect(stream.currentChapter().index).toBe(1);

            mockChaptersWithStatuses(stream, ['completed', 'completed']);
            expect(stream.currentChapter().index).toBe(1);

            // Weird cases (i.e.: if you somehow are allowed to jump ahead)
            mockChaptersWithStatuses(stream, ['not_started', 'in_progress']);
            expect(stream.currentChapter().index).toBe(1);

            mockChaptersWithStatuses(stream, ['not_started', 'completed']);
            expect(stream.currentChapter().index).toBe(1);
        });
    });

    describe('setCompleteIfAllLessonsComplete', () => {
        let streamProgress;

        beforeEach(() => {
            streamProgress = stream.lesson_streams_progress;
        });

        it('should set complete and completed_at if allLessonsComplete', () => {
            streamProgress.complete = false;
            streamProgress.completed_at = undefined;

            jest.spyOn(Date.prototype, 'getTime').mockReturnValue(1000 * 1000 + 1);
            jest.spyOn(stream, 'allLessonsComplete').mockReturnValue(true);
            stream.setCompleteIfAllLessonsComplete();
            expect(streamProgress.complete).toBe(true);
            expect(streamProgress.completed_at).toBe(1000);
        });
        it('should do nothing if !allLessonsComplete', () => {
            streamProgress.complete = false;
            streamProgress.completed_at = undefined;

            jest.spyOn(stream, 'allLessonsComplete').mockReturnValue(false);
            stream.setCompleteIfAllLessonsComplete();
            expect(streamProgress.complete).toBe(false);
            expect(streamProgress.completed_at).toBe(undefined);
        });
        it('should do nothing if already complete', () => {
            streamProgress.complete = true;
            streamProgress.completed_at = 'something';

            jest.spyOn(stream, 'allLessonsComplete').mockReturnValue(true);
            stream.setCompleteIfAllLessonsComplete();
            expect(streamProgress.complete).toBe(true);
            expect(streamProgress.completed_at).toBe('something');
        });
        it('should set the official_test_score for an exam stream', () => {
            streamProgress.complete = false;

            jest.spyOn(Date.prototype, 'getTime').mockReturnValue(1000 * 1000 + 1);
            stream.exam = true;
            stream.lessons = [
                {
                    complete: true,
                    lesson_progress: {
                        officialTestScore: 0.4,
                    },
                },
                {
                    complete: true,
                    lesson_progress: {
                        officialTestScore: 0.44,
                    },
                },
            ];
            stream.setCompleteIfAllLessonsComplete();
            expect(Math.abs(0.42 - streamProgress.official_test_score) < 0.000001).toBe(true);
        });
        it('should warn when setting official test score if not all lessons have scores', () => {
            streamProgress.complete = false;
            const ErrorLogService = $injector.get('ErrorLogService');

            jest.spyOn(ErrorLogService, 'notifyInProd').mockImplementation();
            jest.spyOn(Date.prototype, 'getTime').mockReturnValue(1000 * 1000 + 1);
            stream.exam = true;
            stream.lessons = [
                {
                    complete: true,
                    lesson_progress: {
                        officialTestScore: null,
                    },
                },
                {
                    complete: true,
                    lesson_progress: {
                        officialTestScore: 0.44,
                    },
                },
            ];
            stream.setCompleteIfAllLessonsComplete();
            expect(streamProgress.official_test_score).toEqual(0.44);
            expect(ErrorLogService.notifyInProd).toHaveBeenCalledWith(
                'Setting official_test_score when not all lessons are complete.',
                undefined,
                {
                    streamLocalePackId: stream.localePackId,
                },
            );
        });
    });

    describe('complete', () => {
        it('should delegate to stream progress', () => {
            const stream = Stream.fixtures.getInstance();
            Object.defineProperty(stream.lesson_streams_progress, 'complete', {
                value: true,
            });
            expect(stream.complete).toBe(true);
            Object.defineProperty(stream.lesson_streams_progress, 'complete', {
                value: false,
            });
            expect(stream.complete).toBe(false);
        });
        it('should be false with no stream progress', () => {
            const stream = Stream.fixtures.getInstance();
            stream.lesson_streams_progress = undefined;
            expect(stream.complete).toBe(false);
        });
    });

    describe('gradable', () => {
        it('should be false if neither an exam nor contains any assessment lesson', () => {
            const stream = Stream.fixtures.getInstance({
                exam: false,
            });
            _.each(stream.lessons, lesson => {
                lesson.assessment = false;
            });
            expect(stream.gradable).toBe(false);
        });

        it('should be true if an exam', () => {
            const stream = Stream.fixtures.getInstance({
                exam: true,
            });
            expect(stream.gradable).toBe(true);
        });

        it('should be true if contains assessment lessons', () => {
            const stream = Stream.fixtures.getInstance({
                exam: false,
            });
            stream.lessons[0].assessment = true;
            expect(stream.gradable).toBe(true);
        });
    });

    describe('grade', () => {
        describe('exam', () => {
            it('should be the test score if an exam', () => {
                const stream = Stream.fixtures.getInstance({
                    exam: true,
                });
                jest.spyOn(stream, 'complete', 'get').mockReturnValue(true);
                stream.lesson_streams_progress.official_test_score = 89.9;
                expect(stream.grade).toBe(89.9);
            });

            it('should be undefined if an exam and no test score present', () => {
                const stream = Stream.fixtures.getInstance({
                    exam: true,
                });
                stream.lesson_streams_progress.official_test_score = undefined;
                expect(stream.grade).toBeUndefined();
            });
        });

        describe('assessments', () => {
            let stream;

            beforeEach(() => {
                stream = Stream.fixtures.getInstance({
                    exam: false,
                    lessons: [
                        Lesson.fixtures.getInstance(),
                        Lesson.fixtures.getInstance(),
                        Lesson.fixtures.getInstance(),
                    ],
                });
            });

            it('should be undefined if assessments present but none done', () => {
                _.each(stream.lessons, (lesson, i) => {
                    jest.spyOn(stream.lessons[i], 'bestScore', 'get').mockReturnValue(undefined);
                    stream.lessons[i].assessment = true;
                });
                expect(stream.grade).toBeUndefined();
            });

            it('should be undefined if only one assessment is done when there are multiple', () => {
                _.each(stream.lessons, (lesson, i) => {
                    if (i === 0) {
                        stream.lessons[i].assessment = true;
                        jest.spyOn(stream.lessons[i], 'bestScore', 'get').mockReturnValue(89.9);
                    } else {
                        jest.spyOn(stream.lessons[i], 'bestScore', 'get').mockReturnValue(undefined);
                        stream.lessons[i].assessment = true;
                    }
                });
                expect(stream.grade).toBeUndefined();
            });

            it('should be the assessment best score if only one assessment', () => {
                _.each(stream.lessons, (lesson, i) => {
                    if (i === 0) {
                        stream.lessons[i].assessment = true;
                        jest.spyOn(stream.lessons[i], 'bestScore', 'get').mockReturnValue(89.9);
                    } else {
                        stream.lessons[i].assessment = false;
                    }
                });
                jest.spyOn(stream, 'complete', 'get').mockReturnValue(true);
                expect(stream.grade).toBe(89.9);
            });

            it('should be an average of the assessment best scores if multiple assessments', () => {
                _.each(stream.lessons, (lesson, i) => {
                    if (i === 0) {
                        stream.lessons[i].assessment = true;
                        jest.spyOn(stream.lessons[i], 'bestScore', 'get').mockReturnValue(100);
                    } else if (i === 1) {
                        stream.lessons[i].assessment = true;
                        jest.spyOn(stream.lessons[i], 'bestScore', 'get').mockReturnValue(50);
                    } else if (i === 2) {
                        stream.lessons[i].assessment = true;
                        jest.spyOn(stream.lessons[i], 'bestScore', 'get').mockReturnValue(75);
                    }
                });
                jest.spyOn(stream, 'complete', 'get').mockReturnValue(true);
                expect(stream.grade).toBe(75);
            });

            it('should be an average of the assessment best scores if multiple assessments when a long decimal', () => {
                _.each(stream.lessons, (lesson, i) => {
                    if (i === 0) {
                        stream.lessons[i].assessment = true;
                        jest.spyOn(stream.lessons[i], 'bestScore', 'get').mockReturnValue(100);
                    } else if (i === 1) {
                        stream.lessons[i].assessment = true;
                        jest.spyOn(stream.lessons[i], 'bestScore', 'get').mockReturnValue(50);
                    } else if (i === 2) {
                        stream.lessons[i].assessment = true;
                        jest.spyOn(stream.lessons[i], 'bestScore', 'get').mockReturnValue(70.2);
                    }
                });

                // It's the job of whatever is using it to format the long decimal (with toFixed or an Angular filter)
                jest.spyOn(stream, 'complete', 'get').mockReturnValue(true);
                expect(stream.grade).toBeCloseTo(73.4);
            });

            it('should be undefined if assessments are done but stream is not complete yet', () => {
                _.each(stream.lessons, (lesson, i) => {
                    if (i === 0) {
                        stream.lessons[i].assessment = true;
                        jest.spyOn(stream.lessons[i], 'bestScore', 'get').mockReturnValue(89.9);
                    } else {
                        stream.lessons[i].assessment = false;
                    }
                });
                jest.spyOn(stream, 'complete', 'get').mockReturnValue(false);
                expect(stream.grade).toBeUndefined();
            });
        });

        it('should return undefined if not gradable', () => {
            const stream = Stream.fixtures.getInstance();
            jest.spyOn(stream, 'gradable', 'get').mockReturnValue(false);
            expect(stream.grade).toBeUndefined();
        });
    });

    describe('started', () => {
        it('should be true with stream progress', () => {
            const stream = Stream.fixtures.getInstance();
            expect(stream.lesson_streams_progress).not.toBeUndefined();
            expect(stream.started).toBe(true);
        });
        it('should be false with no stream progress', () => {
            const stream = Stream.fixtures.getInstance();
            stream.lesson_streams_progress = undefined;
            expect(stream.started).toBe(false);
        });
    });

    describe('logInfo', () => {
        it('should work', () => {
            const stream = Stream.fixtures.getInstance();
            expect(stream.id).not.toBeUndefined();
            expect(stream.version_id).not.toBeUndefined();
            expect(stream.logInfo()).toEqual({
                lesson_stream_id: stream.id,
                lesson_stream_complete: stream.complete,
                lesson_stream_version_id: stream.version_id,
                exam: stream.exam,
            });
        });
    });

    describe('lastProgressAt', () => {
        it('should be undefined with no lesson_streams_progress', () => {
            const stream = Stream.fixtures.getInstance();
            stream.lesson_streams_progress = undefined;
            expect(stream.lastProgressAt).toBeUndefined();
        });
        it('should be undefined with no lesson_streams_progress.last_progress_at', () => {
            const stream = Stream.fixtures.getInstance();
            stream.lesson_streams_progress.last_progress_at = undefined;
            expect(stream.lastProgressAt).toBeUndefined();
        });
        it('should be a Date with lesson_streams_progress.last_progress_at', () => {
            const stream = Stream.fixtures.getInstance();
            stream.lesson_streams_progress.last_progress_at = 12345;
            expect(stream.lastProgressAt.getTime()).toBe(12345000);
        });
    });

    describe('keepLearning', () => {
        it('should not display a course that cannot be launched', () => {
            const playlist = Playlist.fixtures.getInstance();
            expect(playlist.streams.length).not.toBeLessThan(1);
            jest.spyOn(ContentAccessHelper, 'canLaunch').mockReturnValue(false);
            expect(Stream.keepLearningStream(playlist, undefined)).toBeUndefined();
        });
    });

    describe('keepLearningStreamAndLesson', () => {
        let streams;
        let canLaunchLesson;

        beforeEach(() => {
            streams = [Stream.fixtures.getInstance(), Stream.fixtures.getInstance(), Stream.fixtures.getInstance()];

            canLaunchLesson = true;
            jest.spyOn(ContentAccessHelper.prototype, '_canLaunchLesson').mockImplementation(() => canLaunchLesson);
        });

        it('should return nothing if no streams have progress', () => {
            streams.forEach(stream => {
                Object.defineProperty(stream, 'lastProgressAt', {
                    value: undefined,
                });
            });
            expect(Stream.keepLearningStreamAndLesson(streams)).toBe(null);
        });

        it('should return nothing if all streams complete', () => {
            streams.forEach(stream => {
                Object.defineProperty(stream, 'complete', {
                    value: true,
                });
            });
            expect(Stream.keepLearningStreamAndLesson(streams)).toBe(null);
        });

        it('should return nothing if all streams beta and otherwise complete', () => {
            streams.forEach(stream => {
                Object.defineProperty(stream, 'completeExcludingComingSoonLessons', {
                    value: true,
                });
            });
            expect(Stream.keepLearningStreamAndLesson(streams)).toBe(null);
        });

        it('should return nothing if incomplete stream has no incomplete lessons', () => {
            streams.forEach(stream => {
                Object.defineProperty(stream, 'complete', {
                    value: false,
                });
                stream.lessons.forEach(lesson => {
                    Object.defineProperty(lesson, 'complete', {
                        value: true,
                    });
                });
            });
            expect(Stream.keepLearningStreamAndLesson(streams)).toBe(null);
        });

        it('should return incomplete stream with most recent progress', () => {
            streams.forEach((stream, i) => {
                Object.defineProperty(stream, 'complete', {
                    value: false,
                });
                Object.defineProperty(stream, 'lastProgressAt', {
                    value: new Date(i),
                });
                stream.lessons.forEach(lesson => {
                    Object.defineProperty(lesson, 'lastProgressAt', {
                        value: undefined,
                    });
                });
            });
            const expectedStream = _.last(streams);
            expect(Stream.keepLearningStreamAndLesson(streams)).toEqual({
                stream: expectedStream,
                lesson: expectedStream.lessons[0],
            });
        });

        it('should return first lesson in list if none started', () => {
            streams.forEach((stream, i) => {
                Object.defineProperty(stream, 'complete', {
                    value: false,
                });
                Object.defineProperty(stream, 'lastProgressAt', {
                    value: new Date(i),
                });
                stream.lessons.forEach(lesson => {
                    Object.defineProperty(lesson, 'lastProgressAt', {
                        value: undefined,
                    });
                });
            });
            const expectedStream = _.last(streams);
            expect(Stream.keepLearningStreamAndLesson(streams)).toEqual({
                stream: expectedStream,
                lesson: expectedStream.lessons[0],
            });
        });

        it('should return nothing if no streams can be launched', () => {
            streams.forEach((stream, i) => {
                Object.defineProperty(stream, 'complete', {
                    value: false,
                });
                Object.defineProperty(stream, 'lastProgressAt', {
                    value: new Date(i),
                });
                stream.lessons.forEach(lesson => {
                    Object.defineProperty(lesson, 'lastProgressAt', {
                        value: undefined,
                    });
                });
            });
            canLaunchLesson = false;
            expect(Stream.keepLearningStreamAndLesson(streams)).toBe(null);
        });

        it('should return next lesson in list if some completed', () => {
            streams.forEach((stream, i) => {
                Object.defineProperty(stream, 'complete', {
                    value: false,
                });
                Object.defineProperty(stream, 'lastProgressAt', {
                    value: new Date(i),
                });
                stream.orderedLessons.forEach((lesson, i) => {
                    Object.defineProperty(lesson, 'complete', {
                        value: i < 1,
                    });
                    Object.defineProperty(lesson, 'lastProgressAt', {
                        value: undefined,
                    });
                });
            });
            const expectedStream = _.last(streams);
            expect(Stream.keepLearningStreamAndLesson(streams)).toEqual({
                stream: expectedStream,
                lesson: expectedStream.orderedLessons[1],
            });
        });

        it('should return next lesson in list if some coming soon', () => {
            streams.forEach((stream, i) => {
                Object.defineProperty(stream, 'complete', {
                    value: false,
                });
                Object.defineProperty(stream, 'lastProgressAt', {
                    value: new Date(i),
                });
                stream.orderedLessons.forEach((lesson, i) => {
                    Object.defineProperty(lesson, 'comingSoon', {
                        value: i < 1,
                    });
                    Object.defineProperty(lesson, 'lastProgressAt', {
                        value: undefined,
                    });
                });
            });
            const expectedStream = _.last(streams);
            expect(Stream.keepLearningStreamAndLesson(streams)).toEqual({
                stream: expectedStream,
                lesson: expectedStream.orderedLessons[1],
            });
        });

        it('should return incomplete lesson with most recent progress', () => {
            streams.forEach((stream, i) => {
                Object.defineProperty(stream, 'complete', {
                    value: false,
                });
                Object.defineProperty(stream, 'lastProgressAt', {
                    value: new Date(i),
                });
                stream.lessons.forEach((lesson, j) => {
                    Object.defineProperty(lesson, 'complete', {
                        value: false,
                    });
                    Object.defineProperty(lesson, 'lastProgressAt', {
                        value: new Date(j),
                    });
                });
            });
            const expectedStream = _.last(streams);
            expect(Stream.keepLearningStreamAndLesson(streams)).toEqual({
                stream: expectedStream,
                lesson: _.last(expectedStream.lessons),
            });
        });
    });

    describe('getCachedForLocalePackId', () => {
        beforeEach(() => {
            Stream.resetCache();
        });

        it('should error if the requested stream is not available', () => {
            jest.spyOn(DialogModal, 'showFatalError').mockImplementation(() => {});
            expect(() => {
                Stream.getCachedForLocalePackId('asdasdasda');
            }).toThrow(new Error('No cached stream found for locale pack.'));
            expect(DialogModal.showFatalError).toHaveBeenCalled();
        });

        it('should not return a stream that has been uncached', () => {
            stream = Stream.fixtures.getInstance();
            stream.ensureLocalePack();
            expect(Stream.getCachedForLocalePackId(stream.localePackId)).toBe(stream);
            Stream.resetCache();
            expect(Stream.getCachedForLocalePackId(stream.localePackId, false)).toBeUndefined();
        });

        it('should return a stream and not rebuld the cache on a second request', () => {
            stream = Stream.fixtures.getInstance();
            stream.ensureLocalePack();

            jest.spyOn(Stream, '_rebuildStreamCacheByLocalePackId');
            expect(Stream.getCachedForLocalePackId(stream.localePackId)).toBe(stream);
            expect(Stream.getCachedForLocalePackId(stream.localePackId)).toBe(stream);
            expect(Stream._rebuildStreamCacheByLocalePackId.mock.calls.length).toBe(1);
        });
    });

    describe('approxStreamMinutes', () => {
        const hardcodedComingSoonEstimate = 7;
        const streamFixtureTimeEstimate = (3 * 3 * 21) / 60.0;

        it('should estimate approximate stream minutes', () => {
            const stream = Stream.fixtures.getInstance();
            expect(stream.approxStreamMinutes).toBeCloseTo(streamFixtureTimeEstimate, 3);
        });

        it('should be zero if no lessons', () => {
            const stream = Stream.fixtures.getInstance();
            stream.lessons = [];
            expect(stream.approxStreamMinutes).toEqual(0);
        });

        it('should estimate a fixed amount of time for coming soon lessons', () => {
            const lesson = Stream.fixtures.getInstance().lessons[0];
            Object.defineProperty(lesson, 'comingSoon', {
                configurable: true,
                value: true,
            });
            stream.lessons.push(lesson);
            expect(stream.approxStreamMinutes).toBeCloseTo(streamFixtureTimeEstimate + hardcodedComingSoonEstimate, 3);
        });
    });

    describe('exam properties', () => {
        ['time_limit_hours'].forEach(key => {
            it(`${key} should only be settable when exam is true`, () => {
                const stream = Stream.fixtures.getInstance();

                expect(stream.exam).toBe(false);
                stream[key] = 'val';
                expect(stream[key]).toBe(null);

                stream.exam = true;
                stream[key] = 'val';
                expect(stream[key]).toBe('val');

                stream.exam = false;
                expect(stream[key]).toBe(null);
            });
        });
    });

    describe('titleWithTag', () => {
        it('should return a title with a stream tag prefixed if available', () => {
            const stream = Stream.fixtures.getInstance();
            stream.tag = 'TAG';
            expect(stream.titleWithTag).toBe(`[TAG] ${stream.title}`);
        });
        it('should return a title only if no tag is available', () => {
            const stream = Stream.fixtures.getInstance();
            expect(stream.titleWithTag).toBe(stream.title);
        });
    });

    describe('examOpenTime', () => {
        it('should be null if this is not an exam', () => {
            const stream = Stream.fixtures.getInstance({
                exam: false,
            });
            expect(stream.examOpenTime()).toBe(null);
        });

        it('should be null if no relevant cohort', () => {
            const stream = Stream.fixtures.getInstance({
                exam: true,
            });
            expect(
                stream.examOpenTime({
                    relevant_cohort: null,
                }),
            ).toBe(null);
        });

        it('should be null if there is no relevant period', () => {
            const stream = Stream.fixtures.getInstance({
                exam: true,
            });
            const Cohort = $injector.get('Cohort');
            $injector.get('CohortFixtures');
            const cohort = Cohort.fixtures.getInstance({
                periods: [
                    {
                        days: 7,
                    },
                    {
                        stream_entries: [
                            {
                                required: true,
                                locale_pack_id: 'somethingElse',
                            },
                        ],
                    },
                ],
            });
            cohort.startDate = new Date();

            expect(
                stream.examOpenTime({
                    relevant_cohort: cohort,
                }),
            ).toBe(null);
        });

        describe('with cohort and period', () => {
            let cohort;
            let expectedDate;

            beforeEach(() => {
                stream = Stream.fixtures.getInstance({
                    exam: true,
                });
                const Cohort = $injector.get('Cohort');
                $injector.get('CohortFixtures');
                cohort = Cohort.fixtures.getInstance({
                    periods: [
                        {
                            days: 7,
                        },
                        {
                            stream_entries: [
                                {
                                    required: true,
                                    locale_pack_id: stream.localePackId,
                                },
                            ],
                        },
                    ],
                });
                cohort.startDate = new Date();
                expectedDate = cohort.periods[1].startDate;
            });

            it('should be null if cohort application has exam locking disabled', () => {
                expect(
                    stream.examOpenTime({
                        relevant_cohort: cohort,
                        acceptedCohortApplication: {
                            disable_exam_locking: true,
                        },
                    }),
                ).toBe(null);
            });

            it('should be the start date of the relevant period', () => {
                expect(
                    stream.examOpenTime({
                        relevant_cohort: cohort,
                    }),
                ).toEqual(expectedDate);
            });
        });
    });

    describe('beforeLaunchWindow', () => {
        it('should be false if no examOpenTime', () => {
            const stream = Stream.fixtures.getInstance();
            jest.spyOn(stream, 'examOpenTime').mockReturnValue(null);
            expect(stream.beforeLaunchWindow('user')).toBe(false);
            expect(stream.examOpenTime).toHaveBeenCalledWith('user');
        });
        it('should be true if examOpenTime is after now', () => {
            const stream = Stream.fixtures.getInstance();
            jest.spyOn(stream, 'examOpenTime').mockReturnValue(new Date(2018, 3, 15));
            jest.spyOn(stream, '_now').mockReturnValue(new Date(2018, 3, 14));
            expect(stream.beforeLaunchWindow('user')).toBe(true);
            expect(stream.examOpenTime).toHaveBeenCalledWith('user');
        });
        it('should be false if examOpenTime is before now', () => {
            const stream = Stream.fixtures.getInstance();
            jest.spyOn(stream, 'examOpenTime').mockReturnValue(new Date(2018, 3, 15));
            jest.spyOn(stream, '_now').mockReturnValue(new Date(2018, 3, 16));
            expect(stream.beforeLaunchWindow('user')).toBe(false);
            expect(stream.examOpenTime).toHaveBeenCalledWith('user');
        });
    });

    function mockChaptersWithStatuses(stream, statuses) {
        let index = 0;
        stream.chapters.forEach(chapter => {
            const status = index < statuses.length ? statuses[index] : 'not_started';
            if (chapter.progressStatus.and && chapter.progressStatus.mockReturnValue) {
                chapter.progressStatus.mockReturnValue(status);
            } else {
                jest.spyOn(chapter, 'progressStatus').mockReturnValue(status);
            }
            index += 1;
        });
    }
});
