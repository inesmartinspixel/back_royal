import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/streams';
import setSpecLocales from 'Translation/setSpecLocales';
import streamCompletedLocales from 'Lessons/locales/lessons/stream/stream_completed-en.json';

setSpecLocales(streamCompletedLocales);

describe('Lesson::Stream::StreamCompletedDir', () => {
    let $q;
    let $injector;
    let SpecHelper;
    let Stream;
    let stream;
    let elem;
    let scope;
    let $location;
    let $timeout;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                $q = $injector.get('$q');
                $injector.get('StreamFixtures');
                $location = $injector.get('$location');
                SpecHelper = $injector.get('SpecHelper');
                Stream = $injector.get('Lesson.Stream');
                $timeout = $injector.get('$timeout');

                // stub out front-royal-footer so that the continue button
                // does not get removed from the screen, since there is no
                // app-shell here
                SpecHelper.stubDirective('frontRoyalFooterContent');

                SpecHelper.stubDirective('frontRoyalSpinner');

                SpecHelper.stubEventLogging();
                SpecHelper.stubConfig();

                stream = Stream.fixtures.getInstance();
            },
        ]);
    });

    it('should not redirect if stream is complete', () => {
        const Event = $injector.get('EventLogger.Event');
        jest.spyOn(Event.prototype, '_addRouteInfo').mockImplementation(() => {}); // this calls location.url, which we want to count below, so stop that from happening
        jest.spyOn(stream, 'progressStatus').mockReturnValue('completed');
        jest.spyOn($location, 'url').mockImplementation(() => {});
        render();
        expect($location.url.mock.calls.length).toBe(0);
    });

    it('should redirect if stream is not complete', () => {
        jest.spyOn(stream, 'progressStatus').mockReturnValue('in_progress');
        jest.spyOn($location, 'url').mockImplementation(() => {});
        render();
        expect($location.url).toHaveBeenCalledWith('/course/1');
    });

    it('should go back to the stream dashboard when clicking the continue button', () => {
        jest.spyOn($location, 'url').mockImplementation(() => {});
        render();
        SpecHelper.click(elem, 'button.continue');
        expect($location.url).toHaveBeenCalledWith('/course/1');
    });

    it('should include data from stream', () => {
        stream.lesson_streams_progress.certificate_image = {
            formats: {
                original: {
                    url: 'stuff',
                },
                '510x200': {
                    url: 'stuff',
                },
            },
        };
        render();
        $timeout.flush();
        expect(stream.lesson_streams_progress.certificateImageSrc()).not.toBeUndefined();
        SpecHelper.expectEqual(
            stream.lesson_streams_progress.certificateImageSrc(),
            elem.find('.completion-badge').attr('src'),
        );
    });

    it('should display a loading indicator if lesson progress is saving', () => {
        const deferred = $q.defer();

        stream.lesson_streams_progress.$$savePromise = deferred.promise;
        render();
        SpecHelper.expectNoElement(elem, 'front-royal-spinner');

        jest.spyOn($location, 'url').mockImplementation(() => {});

        SpecHelper.click(elem, 'button.continue');
        expect($location.url).not.toHaveBeenCalled();
        SpecHelper.expectElement(elem, 'front-royal-spinner');

        deferred.resolve();
        scope.$digest();

        expect($location.url).toHaveBeenCalled();
    });

    describe('with exam stream', () => {
        it('should hide sharing and use exam-specific wording', () => {
            stream.exam = true;
            render();
            SpecHelper.expectNoElement(elem, '.share-button');
            SpecHelper.expectElementText(elem, '.top > h2', 'EXAM COMPLETE');
            SpecHelper.expectElementText(elem, 'button.continue', 'RETURN TO DASHBOARD');
            SpecHelper.expectElementText(elem, '.share-title', 'GOOD JOB!');
            SpecHelper.expectElementText(
                elem,
                '.share-description',
                'Your exam is complete. We will calculate your scores and factor them into your final grade.',
            );
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        const renderer = SpecHelper.renderer();
        const deferred = $q.defer();
        deferred.resolve(stream);
        Stream.expect('show').returns(stream);
        renderer.render('<stream-completed stream-id="1"></stream-completed>');
        Stream.flush('show');
        elem = renderer.elem;
        scope = elem.isolateScope();
    }
});
