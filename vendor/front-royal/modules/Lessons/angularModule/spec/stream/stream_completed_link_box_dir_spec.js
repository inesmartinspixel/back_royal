import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/streams';
import setSpecLocales from 'Translation/setSpecLocales';
import streamCompletedLinkBoxLocales from 'Lessons/locales/lessons/stream/stream_completed_link_box-en.json';

setSpecLocales(streamCompletedLinkBoxLocales);

describe('Lessons.Stream.StreamCompletedLinkBoxDir', () => {
    let $injector;
    let stream;
    let Stream;
    let EventLogger;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                Stream = $injector.get('Lesson.Stream');
                EventLogger = $injector.get('EventLogger');
                $injector.get('StreamFixtures');
            },
        ]);

        SpecHelper.stubConfig();
        SpecHelper.stubDirective('linkedinProfileButton');
    });

    function setupStream() {
        stream = Stream.fixtures.getInstance();
        stream.lesson_streams_progress.complete = true;
        stream.lesson_streams_progress.completed_at = new Date();
    }

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.stream = stream;
        renderer.render('<stream-completed-link-box stream="stream"></stream-link-box>');
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('certificate section', () => {
        beforeEach(() => {
            setupStream();
            expect(stream.lesson_streams_progress.completed_at).not.toBeUndefined();
            SpecHelper.stubCurrentUser('learner');
        });

        it('should open certificate when certificate icon is clicked', () => {
            render();

            jest.spyOn(scope, 'performStreamDownload').mockImplementation(() => {});
            jest.spyOn(scope, 'downloadCertificate');
            jest.spyOn(EventLogger.prototype, 'log').mockImplementation(() => {});
            SpecHelper.click(elem, '.certificate');
            expect(scope.downloadCertificate).toHaveBeenCalled();
            expect(scope.performStreamDownload).toHaveBeenCalled();
            expect(EventLogger.prototype.log).toHaveBeenCalledWith(
                'lesson:stream:download_certificate_click',
                stream.logInfo(),
            );
        });
    });

    describe('summary section', () => {
        beforeEach(() => {
            setupStream();
            expect(stream.summaries).not.toBeUndefined();
        });

        it('should show summaries if present', () => {
            render();
            scope.$digest();

            expect(stream.summaries.length).toEqual(2);
            SpecHelper.expectElements(elem, '.stream-summary', 2);
        });

        it('should open summary when summary is clicked', () => {
            render();
            scope.$digest();

            jest.spyOn(scope, 'onLessonSummaryClick').mockImplementation(() => {});
            SpecHelper.click(elem, '.stream-summary:first');
            expect(scope.onLessonSummaryClick).toHaveBeenCalled();
        });

        it('should not display a header if there are no summaries', () => {
            stream.summaries = [];
            render();
            scope.$digest();

            SpecHelper.expectNoElement(elem, '.summary-container');
        });
    });
});
