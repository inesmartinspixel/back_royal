import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/streams';
import setSpecLocales from 'Translation/setSpecLocales';
import studentDashboardCompletedCoursesLocales from 'Lessons/locales/lessons/stream/student_dashboard_completed_courses-en.json';
import streamCompletedLinkBoxLocales from 'Lessons/locales/lessons/stream/stream_completed_link_box-en.json';

setSpecLocales(studentDashboardCompletedCoursesLocales, streamCompletedLinkBoxLocales);

describe('Lessons.Stream.StudentDashboardCompletedCoursesDir', () => {
    let $injector;
    let streams;
    let Stream;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'SpecHelper');

        angular.mock.inject([
            '$injector',

            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                Stream = $injector.get('Lesson.Stream');

                $injector.get('StreamFixtures');
                SpecHelper.stubEventLogging();
                SpecHelper.stubConfig();
            },
        ]);

        SpecHelper.stubDirective('linkedinProfileButton');
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.completedStreams = streams;
        renderer.render(
            '<student-dashboard-completed-courses completed-streams="completedStreams"></student-dashboard-completed-courses>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    function setupStreams() {
        const mockedStreams = [
            Stream.fixtures.getInstance(),
            Stream.fixtures.getInstance(),
            Stream.fixtures.getInstance(),
        ];

        mockedStreams.forEach(stream => {
            stream.lesson_streams_progress.complete = true;
            stream.lesson_streams_progress.last_progress_at = Date.now() / 1000;
            stream.lesson_streams_progress.completed_at = stream.lesson_streams_progress.last_progress_at;
        });

        streams = mockedStreams;
    }

    describe('completed courses section', () => {
        it('should show generic certificate and message if no completed courses and null completedCourses', () => {
            streams = null;
            render();
            scope.$digest();
            expect(scope.showInfo.completedStreams).toBeNull();
            SpecHelper.expectElement(elem, '.certificate-generic');
        });

        it('should show generic certificate and message if no completed courses and empty completedCourses', () => {
            streams = [];
            render();
            scope.$digest();
            expect(scope.showInfo.completedStreams).toEqual([]);
            SpecHelper.expectElement(elem, '.certificate-generic');
        });

        it('should show completed list', () => {
            setupStreams();
            render();
            scope.$digest();
            expect(scope.showInfo.completedStreams.length).toBe(3);
            SpecHelper.expectElements(elem, 'stream-completed-link-box', 3);
        });

        it('should only show first three until "show all" is selected', () => {
            setupStreams(); // only makes three

            // add one more
            const stream = Stream.fixtures.getInstance();
            stream.lesson_streams_progress.complete = true;
            stream.lesson_streams_progress.last_progress_at = Date.now() / 1000;
            stream.lesson_streams_progress.completed_at = stream.lesson_streams_progress.last_progress_at;
            streams.push(stream);

            render();

            SpecHelper.expectElements(elem, 'stream-completed-link-box', 3);
            SpecHelper.click(elem, '.show-all-btn');
            SpecHelper.expectElements(elem, 'stream-completed-link-box', 4);
        });

        it('should not show "show all" button when three or less courses present', () => {
            setupStreams();
            render();
            SpecHelper.expectElements(elem, 'stream-completed-link-box', scope.showInfo.INITIAL_SHOW_COMPLETED);
            SpecHelper.expectNoElement(elem, '.show-all-btn');
        });
    });
});
