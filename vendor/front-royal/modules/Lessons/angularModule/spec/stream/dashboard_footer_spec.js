import 'AngularSpecHelper';
import 'Lessons/angularModule';

describe('Lessons.dashboardFooter', () => {
    let $injector;
    let SpecHelper;
    let elem;
    let scope;
    let $rootScope;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                $rootScope = $injector.get('$rootScope');
                SpecHelper = $injector.get('SpecHelper');
                SpecHelper.stubConfig();
                SpecHelper.stubCurrentUser();
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        const renderer = SpecHelper.renderer();
        renderer.render('<dashboard-footer></dashboard-footer>');
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    describe('availableFooterLinksMap', () => {
        it('should render default links', () => {
            render();
            SpecHelper.expectElements(elem, 'a.footer-link', 7);

            ['Library', 'About', 'Blog', 'Method', 'Press', 'FAQ', 'Contact Us'].forEach((title, i) => {
                SpecHelper.expectElementText(elem, `a.footer-link:eq(${i})`, title);
            });
        });

        it('should render cookies link before others when config.gdprAppliesToUser', () => {
            const config = $injector.get('ConfigFactory').getSync();
            jest.spyOn(config, 'gdprAppliesToUser').mockReturnValue(true);
            render();
            SpecHelper.expectElements(elem, 'a.footer-link', 8);
            SpecHelper.expectElementText(elem, `a.footer-link:eq(0)`, 'Cookies');
        });

        describe('when isMiyaMiya', () => {
            beforeEach(() => {
                jest.spyOn($rootScope.currentUser, 'isMiyaMiya', 'get').mockReturnValue(true);
            });

            it('should only render library and about links', () => {
                render();
                SpecHelper.expectElements(elem, 'a.footer-link', 2);

                ['Library', 'About'].forEach((title, i) => {
                    SpecHelper.expectElementText(elem, `a.footer-link:eq(${i})`, title);
                });
            });

            it('should redirect to miyamiya.org when about is clicked', () => {
                render();
                jest.spyOn(scope, 'loadUrl').mockImplementation(() => {});
                SpecHelper.click(elem, `a.footer-link`, 1);
                expect(scope.loadUrl).toHaveBeenCalledWith('https://miyamiya.org/', '_blank');
            });

            it('should render cookies link before others when config.gdprAppliesToUser', () => {
                const config = $injector.get('ConfigFactory').getSync();
                jest.spyOn(config, 'gdprAppliesToUser').mockReturnValue(true);
                render();
                SpecHelper.expectElements(elem, 'a.footer-link', 3);
                SpecHelper.expectElementText(elem, `a.footer-link:eq(0)`, 'Cookies');
            });
        });
    });

    describe('socialAccountName', () => {
        it('should default to config.social_name', () => {
            const config = $injector.get('ConfigFactory').getSync();
            config.social_name = 'foobar';
            render();
            expect(scope.socialAccountName).toEqual('foobar');
        });

        it('should be MiyaMiyaSchool when isMiyaMiya', () => {
            jest.spyOn($rootScope.currentUser, 'isMiyaMiya', 'get').mockReturnValue(true);
            render();
            expect(scope.socialAccountName).toEqual('MiyaMiyaSchool');
        });
    });

    describe('launchAppStoreiOS', () => {
        beforeEach(() => {
            render();
            jest.spyOn(scope, 'loadUrl').mockImplementation(() => {});
        });

        it('should load /smartly-ios by default', () => {
            scope.launchAppStoreiOS();
            expect(scope.loadUrl).toHaveBeenCalledWith('/smartly-ios');
        });

        it('should load /miyamiya-ios when isMiyaMiya', () => {
            jest.spyOn(scope.currentUser, 'isMiyaMiya', 'get').mockReturnValue(true);
            scope.launchAppStoreiOS();
            expect(scope.loadUrl).toHaveBeenCalledWith('/miyamiya-ios');
        });
    });

    describe('launchAppStoreAndroid', () => {
        beforeEach(() => {
            render();
            jest.spyOn(scope, 'loadUrl').mockImplementation(() => {});
        });

        it('should load /smartly-android by default', () => {
            scope.launchAppStoreAndroid();
            expect(scope.loadUrl).toHaveBeenCalledWith('/smartly-android');
        });

        it('should load /miyamiya-android when isMiyaMiya', () => {
            jest.spyOn(scope.currentUser, 'isMiyaMiya', 'get').mockReturnValue(true);
            scope.launchAppStoreAndroid();
            expect(scope.loadUrl).toHaveBeenCalledWith('/miyamiya-android');
        });
    });
});
