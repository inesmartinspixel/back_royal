import 'AngularSpecHelper';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';
import 'Lessons/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import acceptedApplicationLocales from 'Lessons/locales/lessons/stream/accepted_application-en.json';

setSpecLocales(acceptedApplicationLocales);

describe('Lessons.Stream.AcceptedApplication', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let DialogModal;
    let Cohort;
    let dateHelper;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                DialogModal = $injector.get('DialogModal');
                Cohort = $injector.get('Cohort');
                $injector.get('CohortFixtures');
                dateHelper = $injector.get('dateHelper');
            },
        ]);

        SpecHelper.stubConfig();
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.render('<application-accepted></application-accepted>');
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    afterEach(() => {
        SpecHelper.cleanup();
    });

    it('should close when get started is clicked', () => {
        const currentUser = SpecHelper.stubCurrentUser();
        Object.defineProperty(currentUser, 'relevant_cohort', {
            value: {
                start_date: parseInt(Date.now() / 1000, 10),
            },
        });
        Object.defineProperty(currentUser, 'programType', {
            value: 'mba',
        });

        jest.spyOn(DialogModal, 'hideAlerts').mockImplementation(() => {});
        render();
        SpecHelper.click(elem, '.get-started > span');
        expect(DialogModal.hideAlerts).toHaveBeenCalled();
    });

    it("should get the user's accepted date", () => {
        const currentUser = SpecHelper.stubCurrentUser();
        currentUser.relevant_cohort = Cohort.fixtures.getInstance({
            program_type: 'mba',
        });
        render();
        SpecHelper.expectElementText(elem, '.smartly-title', 'QUANTIC MBA');
        const expectedDateString = dateHelper.formattedUserFacingMonthYearLong(
            currentUser.relevant_cohort.start_date * 1000,
            false,
        );
        SpecHelper.expectElementText(elem, '.date-class', `${expectedDateString} CLASS`);
    });

    it('should show EMBA for emba users', () => {
        const currentUser = SpecHelper.stubCurrentUser();

        currentUser.relevant_cohort = Cohort.fixtures.getInstance({
            program_type: 'emba',
        });

        render();
        SpecHelper.expectElementText(elem, '.smartly-title', 'QUANTIC EMBA');
    });

    it('should show Certificate Program for Certificate cohort users', () => {
        const currentUser = SpecHelper.stubCurrentUser();

        currentUser.relevant_cohort = Cohort.fixtures.getInstance({
            program_type: 'the_business_certificate',
            title: 'Fundamentals of Business Certificate',
        });

        render();
        SpecHelper.expectElementText(elem, '.to-the', 'to the');
        expect(elem.find('.smartly-title').html()).toEqual(
            'FUNDAMENTALS OF BUSINESS <br class="hidden-xs">CERTIFICATE',
        );
        SpecHelper.expectNoElement(elem, '.date-class');
        SpecHelper.expectNoElement(elem, 'hr.below-program');
    });

    // See FIXME in schedulable.js#getCertificateConfigForProgramType
    it('should remove a leading the Certificate Program for Certificate cohort users', () => {
        const currentUser = SpecHelper.stubCurrentUser();

        currentUser.relevant_cohort = Cohort.fixtures.getInstance({
            program_type: 'the_business_certificate',
            title: 'The Fundamentals of Business Certificate',
        });

        render();
        SpecHelper.expectElementText(elem, '.to-the', 'to the');
        expect(elem.find('.smartly-title').html()).toEqual(
            'FUNDAMENTALS OF BUSINESS <br class="hidden-xs">CERTIFICATE',
        );
        SpecHelper.expectNoElement(elem, '.date-class');
        SpecHelper.expectNoElement(elem, 'hr.below-program');
    });

    describe('share', () => {
        let ShareService;

        it('should work with mba', () => {
            setup('mba');
            assertShare("I was accepted into Quantic's MBA program!", 'quantic.edu');
        });

        it('should work with emba', () => {
            setup('emba');
            assertShare("I was accepted into Quantic's EMBA program!", 'quantic.edu');
        });

        it('should work with certificates', () => {
            setup('the_business_certificate', 'Some Cohort');
            assertShare("I was accepted into Smartly's Some Cohort program!", 'smart.ly');
        });

        function assertShare(message, domain) {
            SpecHelper.click(elem, '.share-facebook');
            expect(ShareService.share).toHaveBeenCalledWith('accepted_application', 'facebook', {
                url: `https://${domain}`,
                title: message,
            });

            ShareService.share.mockClear();
            SpecHelper.click(elem, '.share-twitter');
            expect(ShareService.share).toHaveBeenCalledWith('accepted_application', 'twitter', {
                url: `https://${domain}`,
                title: message,
                description: message,
            });
        }

        function setup(programType, cohortTitle) {
            ShareService = $injector.get('Navigation.ShareService');
            jest.spyOn(ShareService, 'share').mockImplementation(() => {});
            const currentUser = SpecHelper.stubCurrentUser();

            currentUser.relevant_cohort = Cohort.fixtures.getInstance({
                program_type: programType,
                title: cohortTitle,
            });

            if (!currentUser.relevant_cohort.isDegreeProgram) {
                // We're mocking shouldSeeQuanticBranding here so that locale message says Smartly instead of Quantic, which is inline
                // with what we'd actually expect in the wild since the non-degree program types are only for Smartly.
                jest.spyOn(currentUser, 'shouldSeeQuanticBranding', 'get').mockReturnValue(false);
                SpecHelper.stubConfig({
                    domain: 'smart.ly',
                });
            }

            render();
        }
    });
});
