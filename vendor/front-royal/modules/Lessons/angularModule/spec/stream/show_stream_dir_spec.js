import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/lessons';
import 'Lessons/angularModule/spec/_mock/fixtures/streams';
import setSpecLocales from 'Translation/setSpecLocales';
import showStreamLocales from 'Lessons/locales/lessons/stream/show_stream-en.json';

setSpecLocales(showStreamLocales);

describe('Lessons.Stream.ShowStreamDir', () => {
    let sampleStream;
    let Stream;
    let SpecHelper;
    let Lesson;
    let Config;
    let elem;
    let scope;
    let ClientConfig;
    let $rootScope;
    let $injector;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;

                Stream = $injector.get('Lesson.Stream');
                SpecHelper = $injector.get('SpecHelper');
                $rootScope = $injector.get('$rootScope');
                $rootScope.currentUser = SpecHelper.stubCurrentUser('learner');
                ClientConfig = $injector.get('ClientConfig');
                Lesson = $injector.get('Lesson');
                Config = $injector.get('Config');

                const LessonProgress = $injector.get('LessonProgress');
                const StreamProgress = $injector.get('Lesson.StreamProgress');

                $injector.get('MockIguana');
                $injector.get('LessonFixtures');
                $injector.get('StreamFixtures');

                SpecHelper.mockCapabilities();
                SpecHelper.stubDirective('showLesson');

                Config.setAdapter('Iguana.Mock.Adapter');
                Config.expect('index', [], {
                    result: {},
                });

                sampleStream = Stream.fixtures.getInstance();
                angular.forEach(sampleStream.lessons, (lesson, i) => {
                    sampleStream.lessons[i].author = {
                        id: 1,
                        email: 'blarg@blarg.com',
                    };
                });

                // stub out lessonProgress saving
                jest.spyOn(LessonProgress.prototype, 'save').mockImplementation(() => {});
                StreamProgress.expect('save');

                SpecHelper.stubConfig();
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    it('should populate streams when accepting a stream-id', () => {
        render(false);
        SpecHelper.expectElement(elem, 'front-royal-spinner');
        Stream.flush('show');
        SpecHelper.expectNoElement(elem, 'front-royal-spinner');
        expect(scope.stream.id).not.toBeUndefined();
        expect(scope.stream.id).toEqual(sampleStream.id);
    });

    it('should provide a accessor for the streamViewModel on the scope', () => {
        render();
        expect(scope.streamViewModel).not.toBeUndefined();
        expect(scope.streamViewModel.activeLessonId).toBe(scope.stream.lessons[0].id);
    });

    it('should set logProgress to true by default', () => {
        render();
        expect(scope.streamViewModel).not.toBeUndefined();
        expect(scope.streamViewModel.activePlayerViewModel.logProgress).toBe(true);
    });

    it('should disable logging of progress if currentUser is in ghostMode', () => {
        $rootScope.currentUser.ghostMode = true;
        render();
        expect(scope.streamViewModel).not.toBeUndefined();
        expect(scope.streamViewModel.activePlayerViewModel.logProgress).toBe(false);
    });

    it('should update the activeLessonId if lesson-id is provided', () => {
        const lastId = sampleStream.lessons[1].id;
        render(true, lastId);
        expect(scope.streamViewModel).not.toBeUndefined();
        expect(scope.streamViewModel.activeLessonId).toBe(lastId);
    });

    describe('with no currentUser', () => {
        beforeEach(() => {
            SpecHelper.stubNoCurrentUser();
        });

        it('should work', () => {
            render(true);
            expect(scope.streamViewModel).not.toBeUndefined();
            expect(scope.streamViewModel.logProgress).toBe(false);
        });
    });

    describe('with lesson that is incompatible with current client', () => {
        beforeEach(() => {
            Object.defineProperty(Lesson.prototype, 'launchableWithCurrentClient', {
                value: false,
            });
            Object.defineProperty(ClientConfig.current, 'upgradeInstructions', {
                value: 'UPGRADE INSTRUCTIONS',
            });

            Object.defineProperty(ClientConfig.current, 'upgradeButtonText', {
                value: 'PUSH ME',
            });
            jest.spyOn(ClientConfig.current, 'upgrade').mockImplementation(() => {});
        });

        it('should work when lesson cannot be fixed with an upgrade', () => {
            Object.defineProperty(Lesson.prototype, 'launchableWithLatestAvailableVersionOfCurrentClient', {
                value: false,
            });
            render();
            SpecHelper.expectElementText(
                elem,
                '.unlaunchable',
                "We're sorry, but this lesson cannot be launched with your current version of Quantic. Go back",
            );
            SpecHelper.click(elem, '.unlaunchable button');
            expect(scope.loadRoute).toHaveBeenCalledWith(sampleStream.streamDashboardPath);
        });
        it('should work when lesson can be fixed with an upgrade', () => {
            Object.defineProperty(Lesson.prototype, 'launchableWithLatestAvailableVersionOfCurrentClient', {
                value: true,
            });
            render();
            SpecHelper.expectElementText(
                elem,
                '.unlaunchable',
                "We're sorry, but this lesson cannot be launched with your current version of Quantic. UPGRADE INSTRUCTIONS PUSH ME",
            );
            SpecHelper.click(elem, '.unlaunchable button');
            expect(ClientConfig.current.upgrade).toHaveBeenCalled();
        });
    });

    function render(flush, lessonId, showCallback) {
        if (showCallback) {
            showCallback();
        } else {
            Stream.expect('show').returns(sampleStream);
        }
        lessonId = lessonId || sampleStream.lessons[0].id;
        const renderer = SpecHelper.renderer();
        renderer.render(`<show-stream stream-id="${sampleStream.id}" lesson-id="${lessonId}"></show-stream>`);
        if (flush !== false) {
            Stream.flush('show');
        }
        elem = renderer.elem;
        scope = elem.isolateScope();
        jest.spyOn(scope, 'loadRoute').mockImplementation(() => {});
    }
});
