import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Playlists/angularModule/spec/_mock/fixtures/playlists';
import 'Lessons/angularModule/spec/_mock/fixtures/streams';
import streamLinkBoxLocales from 'Lessons/locales/lessons/stream/stream_link_box-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(streamLinkBoxLocales);

describe('FrontRoyal.Lessons.StudentDashboardLearningBoxCourseList', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let streams;
    let playlist;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
            },
        ]);

        SpecHelper = $injector.get('SpecHelper');
        const Stream = $injector.get('Lesson.Stream');
        $injector.get('StreamFixtures');
        const Playlist = $injector.get('Playlist');
        $injector.get('PlaylistFixtures');

        SpecHelper.stubConfig();
        SpecHelper.stubCurrentUser('learner');
        streams = [
            Stream.fixtures.getInstance(),
            Stream.fixtures.getInstance(),
            Stream.fixtures.getInstance(),
            Stream.fixtures.getInstance(),
        ];

        playlist = Playlist.fixtures.getInstance();
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    // eslint-disable-next-line no-shadow
    function render({ keepLearningStream, playlist }) {
        renderer = SpecHelper.renderer();
        renderer.scope.streams = streams;
        renderer.scope.keepLearningStream = keepLearningStream;
        renderer.scope.playlist = playlist;

        renderer.render(
            `
            <student-dashboard-learning-box-course-list
                streams="streams"
                keep-learning-stream="keepLearningStream"
                playlist="playlist"
            ></student-dashboard-learning-box-course-list>
            `,
        );

        elem = renderer.elem;
    }

    it('should show correct number of stream boxes', () => {
        render({});
        SpecHelper.expectElements(elem, 'stream-link-box', streams.length);
    });

    it('should show correct number of circles and lines', () => {
        render({ playlist });
        SpecHelper.expectElements(elem, '.circle', streams.length);
        SpecHelper.expectElements(elem, '.line', streams.length);
    });

    it('should show correct number of highlighted circles and lines', () => {
        streams[0].lesson_streams_progress.complete = true;
        render({ playlist });
        SpecHelper.expectElements(elem, '.complete .circle', 1);
        SpecHelper.expectElements(elem, '.complete .line', 1);
    });

    it('should show correct number of highlighted circles when completed courses are out of order', () => {
        streams[0].lesson_streams_progress.complete = true;
        streams[2].lesson_streams_progress.complete = true;
        render({ playlist });
        SpecHelper.expectElements(elem, '.complete .circle', 2);
        SpecHelper.expectElements(elem, '.complete .line', 2);
    });

    it('should set inActivePlaylist', () => {
        streams[0].lesson_streams_progress.complete = true;
        streams[2].lesson_streams_progress.complete = true;
        render({ playlist });
        SpecHelper.expectElements(elem, '.complete .circle', 2);
        SpecHelper.expectElements(elem, '.complete .line', 2);
    });
});
