import 'AngularSpecHelper';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohort_applications';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';
import 'Lessons/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import studentDashboardCertificateGraduationBoxLocales from 'Lessons/locales/lessons/stream/student_dashboard_certificate_graduation_box-en.json';

setSpecLocales(studentDashboardCertificateGraduationBoxLocales);

describe('FrontRoyal.Lessons.studentDashboardCertificateGraduationBox', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let Cohort;
    let CohortApplication;
    let currentUser;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                Cohort = $injector.get('Cohort');
                CohortApplication = $injector.get('CohortApplication');

                $injector.get('CohortFixtures');
                $injector.get('CohortApplicationFixtures');
            },
        ]);

        SpecHelper.stubConfig();

        currentUser = SpecHelper.stubCurrentUser();
        currentUser.relevant_cohort = Cohort.fixtures.getInstance();
        jest.spyOn(currentUser, 'lastCohortApplication', 'get').mockReturnValue(
            CohortApplication.fixtures.getInstance(),
        );
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.currentUser = currentUser;
        renderer.render(
            '<student-dashboard-certificate-graduation-box current-user="currentUser"></student-dashboard-certificate-graduation-box>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    describe('status', () => {
        it("should be 'not_graduated' if lastCohortApplication !isGraduated", () => {
            jest.spyOn(currentUser.lastCohortApplication, 'isGraduated', 'get').mockReturnValue(false);
            render();
            expect(scope.status).toBe('not_graduated');
        });

        it("should be 'graduated' if lastCohortApplication isGraduated", () => {
            jest.spyOn(currentUser.lastCohortApplication, 'isGraduated', 'get').mockReturnValue(true);
            render();
            expect(scope.status).toBe('graduated');
        });
    });

    describe("when status is 'not_graduated'", () => {
        it('should explain requirements for graduation', () => {
            render();
            jest.spyOn(scope, 'status', 'get').mockReturnValue('not_graduated');
            scope.$digest();

            SpecHelper.expectElementText(
                elem,
                '.sub-text',
                'To meet your graduation requirements, please make sure your ID is verified, transcripts are provided, and you’ve submitted all required projects.',
            );
        });
    });

    describe("when status is 'graduated'", () => {
        describe('on desktop', () => {
            it('should support certificate download', () => {
                jest.spyOn(currentUser.relevant_cohort, 'supportsCertificateDownload', 'get').mockReturnValue(true);
                render();
                jest.spyOn(scope, 'allowProgramCertificateDownload').mockReturnValue(true);
                jest.spyOn(scope, 'status', 'get').mockReturnValue('graduated');
                scope.$digest();

                SpecHelper.expectElement(elem, '[name="certificate_download"]');
                jest.spyOn(scope, 'downloadProgramCertificate').mockImplementation(() => {});
                SpecHelper.click(elem, '[name="certificate_download"]');
                expect(scope.downloadProgramCertificate).toHaveBeenCalledWith(currentUser.lastCohortApplication);
            });
        });

        describe('on mobile', () => {
            it('should not support certificate download', () => {
                render();
                jest.spyOn(scope, 'allowProgramCertificateDownload').mockReturnValue(false);
                jest.spyOn(scope, 'status', 'get').mockReturnValue('graduated');
                scope.$digest();

                SpecHelper.expectNoElement(elem, '[name="certificate_download"]');
            });
        });
    });
});
