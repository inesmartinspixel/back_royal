import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/streams';
import { DEFAULT_SIGNUP_LOCATION } from 'SignupLocations';
import setSpecLocales from 'Translation/setSpecLocales';
import streamLinkBoxLocales from 'Lessons/locales/lessons/stream/stream_link_box-en.json';

setSpecLocales(streamLinkBoxLocales);

describe('FrontRoyal.Lessons.StreamActivationHelperMixin', () => {
    let $injector;
    let SpecHelper;
    let Stream;
    let stream;
    let $location;
    let $timeout;
    let StreamActivationHelperMixin;
    let $rootScope;
    let scope;
    let isMobile;
    let DialogModal;
    let EventLogger;
    let ngToast;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'SpecHelper');

        angular.mock.module($provide => {
            isMobile = jest.fn();
            isMobile.mockReturnValue(true);
            $provide.value('isMobile', isMobile);
        });

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                $location = $injector.get('$location');
                $rootScope = $injector.get('$rootScope');
                $timeout = $injector.get('$timeout');
                DialogModal = $injector.get('DialogModal');
                EventLogger = $injector.get('EventLogger');
                ngToast = $injector.get('ngToast');
                SpecHelper = $injector.get('SpecHelper');
                Stream = $injector.get('Lesson.Stream');
                StreamActivationHelperMixin = $injector.get('StreamActivationHelperMixin');

                $injector.get('StreamFixtures');
            },
        ]);

        stream = Stream.fixtures.getInstance();
        scope = $rootScope.$new();
        scope.stream = stream;

        jest.spyOn($location, 'url').mockImplementation(() => {});
        SpecHelper.stubCurrentUser();
        SpecHelper.stubConfig();
    });

    function assertShowNativeConfirm(func, message) {
        expect(navigator.notification.confirm.mock.calls.length).toBe(0);
        func();
        expect(navigator.notification.confirm.mock.calls.length).toBe(1);
        if (message) {
            expect(navigator.notification.confirm.mock.calls[0][0]).toEqual(message);
        }
    }

    function link() {
        StreamActivationHelperMixin.onLink(scope);
        scope.$apply();
        $location.url.mockClear();
    }

    describe('handleClick', () => {
        beforeEach(() => {
            link();
            jest.spyOn(scope, 'showComingSoonModal').mockImplementation(() => {});
            jest.spyOn(scope, 'showLockedModal').mockImplementation(() => {});
        });

        it('should do nothing if navigation is disabled', () => {
            scope.navigationEnabled = false;
            scope.handleClick();
            expect(scope.showComingSoonModal).not.toHaveBeenCalled();
            expect(scope.showLockedModal).not.toHaveBeenCalled();
        });

        it('should call showComingSoonModal if contentAccessHelper.reason is coming_soon', () => {
            const spy = jest.spyOn(scope.contentAccessHelper, 'reason', 'get');

            spy.mockReturnValue('not_coming_soon');
            scope.handleClick();
            expect(scope.showComingSoonModal).not.toHaveBeenCalled();

            spy.mockReturnValue('coming_soon');
            scope.handleClick();
            expect(scope.showComingSoonModal).toHaveBeenCalled();
        });

        it('should call showLockedModal if only locked', () => {
            scope.locked = false;
            scope.handleClick();
            expect(scope.showLockedModal).not.toHaveBeenCalled();

            scope.locked = true;
            scope.handleClick();
            expect(scope.showLockedModal).toHaveBeenCalled();
        });

        it('should take user to the stream dashboard if not locked and not comingSoon', () => {
            jest.spyOn(scope.contentAccessHelper, 'reason', 'get').mockReturnValue('not_coming_soon');
            scope.locked = true;
            scope.handleClick();
            $timeout.flush();
            expect($location.url.mock.calls.length).toBe(0);

            scope.handleClick();
            $timeout.flush();
            expect($location.url.mock.calls.length).toBe(0);

            scope.locked = false;
            scope.handleClick();
            $timeout.flush();
            expect($location.url.mock.calls.length > 0).toBe(true);
            expect($location.url).toHaveBeenCalledWith(stream.streamDashboardPath);
        });

        it('should lock and unlock based on ID verification past-due status', () => {
            const spy = jest.spyOn(scope.currentUser, 'pastDueForIdVerification', 'get');
            spy.mockReturnValue(true);
            scope.$digest();
            expect(scope.locked).toBe(true);
            scope.handleClick();
            expect(scope.showLockedModal).toHaveBeenCalled();

            spy.mockReturnValue(false);
            scope.$digest();
            expect(scope.locked).toBe(false);
            scope.showLockedModal.mockClear();
            scope.handleClick();
            expect(scope.showLockedModal).not.toHaveBeenCalled();
        });
    });

    describe('notifications', () => {
        let $window;
        beforeEach(() => {
            $window = $injector.get('$window');
            $window.CORDOVA = true;
            // stub out native notifications plugin
            $window.navigator.notification = {
                confirm() {},
            };
            jest.spyOn($window.navigator.notification, 'confirm').mockImplementation(() => {});
        });

        afterEach(() => {
            $window.CORDOVA = false;
        });

        describe('showComingSoonModal', () => {
            describe('on mobile', () => {
                describe('Cordova', () => {
                    it('should log show a native confirm dialog', () => {
                        const message = `The Quantic content team is hard at work on ${stream.title}!\n\nWould you like to be notified when this course is available?`;
                        link();
                        assertShowNativeConfirm(scope.showComingSoonModal, message);
                    });

                    it('should show a native confirm dialog if not authenticated', () => {
                        const message = `The Quantic content team is hard at work on ${stream.title}!\n\nWould you like to be notified when this course is available?`;
                        SpecHelper.stubNoCurrentUser();
                        link();
                        assertShowNativeConfirm(scope.showComingSoonModal, message);
                    });
                });

                describe('web', () => {
                    beforeEach(() => {
                        $window.CORDOVA = false;
                    });

                    it('should open coming soon dialog modal if not on CORDOVA', () => {
                        jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
                        link();
                        scope.showComingSoonModal();
                        expect(DialogModal.alert).toHaveBeenCalled();
                    });
                });
            });
        });

        describe('showLockedModal', () => {
            describe('on mobile', () => {
                describe('Cordova', () => {
                    it('should show a native confirm dialog', () => {
                        link();
                        assertShowNativeConfirm(scope.showLockedModal);
                    });
                });

                describe('web', () => {
                    beforeEach(() => {
                        $window.CORDOVA = false;
                    });

                    it('should show dialog modal', () => {
                        jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
                        link();
                        scope.showLockedModal();
                        expect(DialogModal.alert).toHaveBeenCalled();
                    });
                });
            });
        });
    });

    describe('notifyMeClicked', () => {
        it('should hide any open dialog modal alerts', () => {
            jest.spyOn(DialogModal, 'hideAlerts').mockImplementation(() => {});
            link();
            scope.notifyMeClicked();
            expect(DialogModal.hideAlerts).toHaveBeenCalled();
        });

        it('should log an event and show a toast message if authenticated', () => {
            jest.spyOn(EventLogger.instance, 'log');
            jest.spyOn(ngToast, 'create').mockImplementation(() => {});
            link();
            scope.notifyMeClicked();

            expect(EventLogger.instance.log).toHaveBeenCalledWith('lesson:stream:notify-coming-soon', stream.logInfo());
            expect(ngToast.create).toHaveBeenCalledWith({
                content: "Thanks! We'll let you know when this course is available.",
                className: 'info',
            });
        });

        it('should redirect to join if not authenticated', () => {
            SpecHelper.stubNoCurrentUser();
            jest.spyOn(EventLogger.instance, 'log');
            jest.spyOn(ngToast, 'create').mockImplementation(() => {});
            link();
            jest.spyOn(scope, 'loadUrl').mockImplementation(() => {});
            scope.notifyMeClicked();

            expect(ngToast.create.mock.calls.length).toBe(0);
            expect(scope.loadUrl).toHaveBeenCalledWith(DEFAULT_SIGNUP_LOCATION);
            expect(EventLogger.instance.log).not.toHaveBeenCalledWith(
                'lesson:stream:notify-coming-soon',
                stream.logInfo(),
            );
        });
    });
});
