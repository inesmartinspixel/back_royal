import 'AngularSpecHelper';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import 'Careers/angularModule/spec/careers_spec_helper';
import 'Lessons/angularModule';
import 'Users/angularModule';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';
import 'Lessons/angularModule/spec/_mock/fixtures/lessons';
import 'Playlists/angularModule/spec/_mock/fixtures/playlists';
import 'Lessons/angularModule/spec/_mock/fixtures/streams';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohort_applications';
import setSpecLocales from 'Translation/setSpecLocales';
import studentDashboardLocales from 'Lessons/locales/lessons/stream/student_dashboard-en.json';
import hasToggleableDisplayModeLocales from 'Lessons/locales/lessons/stream/has_toggleable_display_mode-en.json';
import feedbackSidebarLocales from 'Feedback/locales/feedback/feedback_sidebar-en.json';
import toggleableCourseListButtonsLocales from 'Lessons/locales/lessons/stream/toggleable_course_list_buttons-en.json';
import sharingButtonsLocales from 'Navigation/locales/navigation/sharing_buttons-en.json';

setSpecLocales(studentDashboardLocales);
setSpecLocales(hasToggleableDisplayModeLocales);
setSpecLocales(feedbackSidebarLocales);
setSpecLocales(toggleableCourseListButtonsLocales);
setSpecLocales(sharingButtonsLocales);

describe('Lessons.Stream.StudentDashboardDir', () => {
    let $injector;
    let streams;
    let Stream;
    let SpecHelper;
    let CareersSpecHelper;
    let renderer;
    let elem;
    let scope;
    let user;
    let $rootScope;
    let $q;
    let $window;
    let isMobile;
    let ClientStorage;
    let StudentDashboard;
    let DialogModal;
    let User;
    let $timeout;
    let Cohort;
    let Playlist;
    let playlists;
    let playlistStreams;
    let activePlaylist;
    let relevantCohort;
    let CohortApplication;
    let LearnerContentCache;
    let CareerProfile;
    let offlineModeManager;
    let Institution;

    beforeEach(() => {
        angular.mock.module('CareersSpecHelper', 'FrontRoyal.Lessons', 'FrontRoyal.Institutions', 'SpecHelper');

        angular.mock.module($provide => {
            isMobile = jest.fn();
            isMobile.mockReturnValue(false);
            $provide.value('isMobile', isMobile);
        });

        angular.mock.inject([
            '$injector',

            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                CareersSpecHelper = $injector.get('CareersSpecHelper');
                Stream = $injector.get('Lesson.Stream');
                Cohort = $injector.get('Cohort');
                ClientStorage = $injector.get('ClientStorage');
                $rootScope = $injector.get('$rootScope');
                StudentDashboard = $injector.get('StudentDashboard');
                $q = $injector.get('$q');
                Playlist = $injector.get('Playlist');
                $window = $injector.get('$window');
                CohortApplication = $injector.get('CohortApplication');
                LearnerContentCache = $injector.get('LearnerContentCache');
                CareerProfile = $injector.get('CareerProfile');
                offlineModeManager = $injector.get('offlineModeManager');
                offlineModeManager.inOfflineMode = false;
                Institution = $injector.get('Institution');

                $injector.get('LessonFixtures');
                $injector.get('StreamFixtures');
                $injector.get('PlaylistFixtures');
                $injector.get('CohortFixtures');
                $injector.get('CohortApplicationFixtures');
                $injector.get('CareerProfileFixtures');

                $injector.get('MockIguana');
                DialogModal = $injector.get('DialogModal');
                User = $injector.get('User');
                $timeout = $injector.get('$timeout');

                SpecHelper.stubDirective('studentDashboardLearningBox');
                SpecHelper.stubDirective('studentDashboardWelcomeBox');
                SpecHelper.stubDirective('studentDashboardProgramBox');
                SpecHelper.stubDirective('studentDashboardSchedule');
                SpecHelper.stubDirective('studentDashboardEnrollment');
                SpecHelper.stubDirective('studentDashboardCertificateGraduationBox');
                SpecHelper.stubDirective('studentDashboardRecentCourses');
                SpecHelper.stubDirective('studentDashboardCompletedCourses');
                SpecHelper.stubDirective('studentDashboardSchedule');
                SpecHelper.stubDirective('studentDashboardScheduleInterview');
                SpecHelper.stubDirective('studentDashboardOfflineBox');
                SpecHelper.stubDirective('studentDashboardResearchResources');
                SpecHelper.stubDirective('applicationAccepted');
                SpecHelper.stubDirective('streamLinkBox');
            },
        ]);

        relevantCohort = Cohort.fixtures.getInstance();

        playlistStreams = [];
        streams = [];

        user = SpecHelper.stubCurrentUser('learner');
        const lastCohortApplication = CohortApplication.fixtures.getInstance();
        lastCohortApplication.$$embeddedIn = user;
        jest.spyOn(user, 'lastCohortApplication', 'get').mockReturnValue(lastCohortApplication);

        SpecHelper.stubConfig();

        User.expect('update');

        SpecHelper.stubEventLogging();
    });

    function setupStreams(inProgress, completed, requestedStreams) {
        const mockedStreams = [
            Stream.fixtures.getInstance(),
            Stream.fixtures.getInstance(),
            Stream.fixtures.getInstance(),
        ];

        mockedStreams[0].locale_pack.content_topics = [createTopic('topic1', 'topic1')];
        mockedStreams[1].locale_pack.content_topics = [createTopic('topic1', 'topic1')];
        mockedStreams[2].locale_pack.content_topics = [createTopic('topic2', 'topic2')];

        streams = requestedStreams || mockedStreams;

        // Mock out the inProgress/completed API for the lesson_streams_progress
        if (streams) {
            streams.forEach(stream => {
                if (!inProgress && !completed) {
                    stream.lesson_streams_progress = undefined;
                } else {
                    stream.lesson_streams_progress.complete = !!completed;
                    stream.lesson_streams_progress.last_progress_at = Date.now() / 1000;
                    stream.lesson_streams_progress.completed_at = stream.lesson_streams_progress.last_progress_at;
                }

                stream.favorite = true;
            });
        }
    }

    function setupPlaylists(numPlaylists) {
        // Create mock Playlists and Streams
        playlists = [];
        activePlaylist = undefined;
        if (numPlaylists > 0) {
            // eslint-disable-next-line no-plusplus
            for (let i = 0; i < numPlaylists; i++) {
                const playlist = Playlist.fixtures.getInstance();
                playlist.addLocalePackFixture();
                playlists.push(playlist);
            }

            playlistStreams = Playlist.fixtures.getStreamsForPlaylists(playlists);
            activePlaylist = playlists[0];
            user.active_playlist_locale_pack_id = activePlaylist.localePackId;
        }

        if (relevantCohort) {
            relevantCohort.playlist_collections = [
                {
                    title: '',
                    required_playlist_pack_ids: _.pluck(playlists, 'localePackId'),
                },
            ];
        }

        return playlists;
    }

    function getStudentDashboardCallReturnValue(streamParams, numPlaylists, hasAvailableIncompleteStreams) {
        hasAvailableIncompleteStreams = angular.isUndefined(hasAvailableIncompleteStreams)
            ? false
            : hasAvailableIncompleteStreams;
        numPlaylists = angular.isUndefined(numPlaylists) ? 3 : numPlaylists;

        const availablePlaylists = setupPlaylists(numPlaylists);

        return {
            result: [
                {
                    lesson_streams: _.union(streams, playlistStreams),
                    available_playlists: availablePlaylists,
                },
            ],
            meta: {
                has_available_incomplete_streams: hasAvailableIncompleteStreams,
            },
        };
    }

    function setupStudentDashboardCall(streamParams, returnValue, numPlaylists, hasAvailableIncompleteStreams) {
        returnValue =
            returnValue ||
            getStudentDashboardCallReturnValue(streamParams, numPlaylists, hasAvailableIncompleteStreams);
        jest.spyOn(LearnerContentCache, 'ensureStudentDashboard').mockReturnValue($q.when(returnValue));
    }

    function createTopic(title, id) {
        return {
            name: title,
            id,
            locales: {
                en: title,
            },
        };
    }

    function render() {
        renderer = SpecHelper.renderer();
        renderer.render('<student-dashboard></student-dashboard>');
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    afterEach(() => {
        SpecHelper.cleanup();
        ClientStorage.removeItem('toggleableCourseListStudentDashboard');
        offlineModeManager.inOfflineMode = false;
    });

    it('should set skip_onboarding in locale storage if in CORDOVA', () => {
        $window.CORDOVA = true;
        jest.spyOn(ClientStorage, 'setItem').mockImplementation(() => {});
        setupStreams(false, false, null);
        setupStudentDashboardCall();
        render();
        SpecHelper.stubCurrentUser();
        jest.spyOn(scope, 'showScheduleInterview', 'get').mockReturnValue(false);
        scope.$digest();
        expect(ClientStorage.setItem).toHaveBeenCalledWith('skip_onboarding', true);
        $window.CORDOVA = false;
    });

    it('should set activePlaylist if active_playlist_locale_pack_id changes', () => {
        setupStudentDashboardCall();
        activePlaylist = playlists[0];
        render();
        user.active_playlist_locale_pack_id = playlists[1].localePackId;
        scope.$digest();
        expect(scope.activePlaylist.title).toEqual(playlists[1].title);
    });

    it('should not launchVerificationModalIfLastIdologyVerificationFailed if user does not have a career_profile', () => {
        const UserIdVerificationViewModel = $injector.get('UserIdVerificationViewModel');
        jest.spyOn(
            UserIdVerificationViewModel.prototype,
            'launchVerificationModalIfLastIdologyVerificationFailed',
        ).mockImplementation(() => {});
        user.career_profile = undefined;
        setupStudentDashboardCall();
        render();
        expect(
            UserIdVerificationViewModel.prototype.launchVerificationModalIfLastIdologyVerificationFailed,
        ).not.toHaveBeenCalled();
    });

    it('should launchVerificationModalIfLastIdologyVerificationFailed if user has a career_profile', () => {
        const UserIdVerificationViewModel = $injector.get('UserIdVerificationViewModel');
        jest.spyOn(
            UserIdVerificationViewModel.prototype,
            'launchVerificationModalIfLastIdologyVerificationFailed',
        ).mockImplementation(() => {});
        user.career_profile = CareerProfile.fixtures.getInstance();
        setupStudentDashboardCall();
        render();
        expect(
            UserIdVerificationViewModel.prototype.launchVerificationModalIfLastIdologyVerificationFailed,
        ).toHaveBeenCalled();
    });

    describe('accepted into cohort', () => {
        beforeEach(() => {
            CareersSpecHelper.stubCareersNetworkViewModel('candidate');

            // so it won't trigger the welcome to dahsboard modal
            jest.spyOn($rootScope.currentUser, 'hasEverApplied', 'get').mockReturnValue(true);
        });

        it('should show modal message on first load after being accepted if in MBA', () => {
            Object.defineProperty($rootScope.currentUser, 'acceptedCohortApplication', {
                value: {
                    foo: 'bar',
                    program_type: 'mba',
                },
            });
            Object.defineProperty($rootScope.currentUser, 'programType', {
                value: 'mba',
            });

            $rootScope.currentUser.has_seen_accepted = false;

            jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
            setupStreams();
            setupStudentDashboardCall();
            render();
            expect(DialogModal.alert).toHaveBeenCalled();
        });

        it('should show modal message on first load after being accepted if in EMBA', () => {
            Object.defineProperty($rootScope.currentUser, 'acceptedCohortApplication', {
                value: {
                    foo: 'bar',
                    program_type: 'emba',
                },
            });
            Object.defineProperty($rootScope.currentUser, 'programType', {
                value: 'emba',
            });

            $rootScope.currentUser.has_seen_accepted = false;

            jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
            setupStreams();
            setupStudentDashboardCall();
            render();
            expect(DialogModal.alert).toHaveBeenCalled();
        });

        it('should not show modal message on first load if career network only', () => {
            const CareersNetworkViewModel = $injector.get('CareersNetworkViewModel');
            jest.spyOn(CareersNetworkViewModel.prototype, 'nagIfNecessary').mockImplementation(() => {});

            Object.defineProperty($rootScope.currentUser, 'acceptedCohortApplication', {
                value: {
                    foo: 'bar',
                    program_type: 'career_network_only',
                },
            });
            Object.defineProperty($rootScope.currentUser, 'programType', {
                value: 'career_network_only',
            });

            $rootScope.currentUser.has_seen_accepted = false;
            jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
            setupStreams();
            setupStudentDashboardCall();
            render();
            expect(DialogModal.alert).not.toHaveBeenCalled();
        });

        it('should not show modal message on subsequent loads after being accepted', () => {
            const CareersNetworkViewModel = $injector.get('CareersNetworkViewModel');
            jest.spyOn(CareersNetworkViewModel.prototype, 'nagIfNecessary').mockImplementation(() => {});
            Object.defineProperty($rootScope.currentUser, 'acceptedCohortApplication', {
                value: {
                    foo: 'bar',
                },
            });
            $rootScope.currentUser.has_seen_accepted = true;

            jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
            setupStreams();
            setupStudentDashboardCall();
            render();
            expect(DialogModal.alert).not.toHaveBeenCalled();
        });

        it('should update flag after first load after being accepted', () => {
            Object.defineProperty($rootScope.currentUser, 'acceptedCohortApplication', {
                value: {
                    foo: 'bar',
                    program_type: 'mba',
                },
            });
            Object.defineProperty($rootScope.currentUser, 'programType', {
                value: 'mba',
            });
            $rootScope.currentUser.hasSeenAccepted = false;

            jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
            setupStreams();
            setupStudentDashboardCall();
            render();
            expect($rootScope.currentUser.hasSeenAccepted).toBe(true);
        });

        it('should not update flag if in ghostMode (ex. log in as)', () => {
            Object.defineProperty($rootScope.currentUser, 'acceptedCohortApplication', {
                value: {
                    foo: 'bar',
                },
            });
            $rootScope.currentUser.hasSeenAccepted = false;
            $rootScope.currentUser.ghostMode = true;

            jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
            setupStreams();
            setupStudentDashboardCall();
            render();
            expect($rootScope.currentUser.hasSeenAccepted).toBe(false);
        });

        it('should show modal message on first load after being accepted if certificate program type', () => {
            const programType = 'the_business_certificate';

            jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
            Object.defineProperty($rootScope.currentUser, 'acceptedCohortApplication', {
                value: {
                    foo: 'bar',
                    program_type: programType,
                },
                configurable: true,
            });
            Object.defineProperty($rootScope.currentUser, 'programType', {
                value: programType,
                configurable: true,
            });

            $rootScope.currentUser.has_seen_accepted = false;
            setupStreams();
            setupStudentDashboardCall();
            render();
            expect(DialogModal.alert).toHaveBeenCalled();
        });

        it('should update showSchedule state when being accepted', () => {
            jest.spyOn(Cohort, 'supportsSchedule').mockReturnValue(true);
            $rootScope.currentUser.has_seen_accepted = false;
            setupStreams();
            setupStudentDashboardCall();
            render();

            expect(scope.showSchedule).toBeUndefined();
            jest.spyOn($rootScope.currentUser, 'acceptedCohortApplication', 'get').mockReturnValue(
                CohortApplication.fixtures.getInstance({
                    foo: 'bar',
                }),
            );
            scope.$digest();
            expect(scope.showSchedule).toBe(true);
        });
    });

    describe('cohort application rejected or failed', () => {
        it('should show mobile program-box if failed or rejected', () => {
            renderWithMobileProgramBox();
        });

        it('should hide mobile program-box if user previously dismissed', () => {
            Object.defineProperty($rootScope.currentUser, 'isRejectedOrExpelledOrDeferredOrFailed', {
                value: true,
            });

            jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
            jest.spyOn(ClientStorage, 'getItem').mockReturnValue('true');
            $rootScope.currentUser.relevant_cohort = relevantCohort;
            setupStreams();
            setupStudentDashboardCall();
            render();

            expect(scope._hideMobileProgramBox).toBeTruthy();
        });

        describe('hideMobileProgramBox', () => {
            it('should set _hideMobileProgramBox to false and set the rejected_message_hidden flag in ClientStorage', () => {
                renderWithMobileProgramBox();
                jest.spyOn(ClientStorage, 'setItem').mockImplementation(() => {});
                scope.hideMobileProgramBox();
                expect(scope._hideMobileProgramBox).toBe(true);
                expect(ClientStorage.setItem).toHaveBeenCalledWith('rejected_message_hidden', true);
            });
        });

        function renderWithMobileProgramBox() {
            Object.defineProperty($rootScope.currentUser, 'isRejectedOrExpelledOrDeferredOrFailed', {
                value: true,
            });

            jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
            jest.spyOn(ClientStorage, 'getItem').mockReturnValue(undefined);
            $rootScope.currentUser.relevant_cohort = relevantCohort;
            setupStreams();
            setupStudentDashboardCall();
            render();

            expect(scope._hideMobileProgramBox).toBe(false);
        }
    });

    describe('logout', () => {
        it('should remove everything if the user logs out', () => {
            setupStreams(false, false, null);
            setupStudentDashboardCall();
            render();
            $rootScope.currentUser = undefined;
            scope.$apply();
            SpecHelper.expectNoElement(elem, '*');
        });
    });

    describe('bookmarked courses flat list', () => {
        beforeEach(() => {
            ClientStorage.setItem('toggleableCourseListStudentDashboard', 'flat');
        });

        it('should show spinner until streams are loaded', () => {
            setupStreams(false, false, null);
            setupStudentDashboardCall();
            render();
            scope.dashboardLoading = true;
            scope.$digest();
            SpecHelper.expectElement(elem, 'front-royal-spinner');
            scope.dashboardLoading = false;
            scope.$digest();
            SpecHelper.expectNoElement(elem, 'front-royal-spinner');
        });

        it('should display the correct number of courses', () => {
            setupStreams();
            setupStudentDashboardCall();
            render();
            SpecHelper.expectElements(elem, '.courses-container stream-link-box', streams.length);
        });

        it('should display in not-started courses in My Courses', () => {
            setupStreams();
            setupStudentDashboardCall();
            render();
            expect(ClientStorage.getItem('toggleableCourseListStudentDashboard')).toEqual('flat');
            SpecHelper.expectElements(elem, '.courses-group', 1);
            SpecHelper.expectElementText(elem, '.courses-container .hr-caption > span:eq(0)', 'BOOKMARKED COURSES');
            SpecHelper.expectElements(elem, '.courses-container stream-link-box', streams.length);
        });

        it('should display in-progress streams in My Courses', () => {
            setupStreams(true);
            setupStudentDashboardCall();
            render();
            expect(ClientStorage.getItem('toggleableCourseListStudentDashboard')).toEqual('flat');
            SpecHelper.expectElements(elem, '.courses-group', 1);
            SpecHelper.expectElementText(elem, '.courses-container .hr-caption > span:eq(0)', 'BOOKMARKED COURSES');
            SpecHelper.expectElements(elem, '.courses-container stream-link-box', streams.length);
        });

        it('should not display a Completed Courses that is redundant with certificate display', () => {
            setupStreams(false, true);
            setupStudentDashboardCall(undefined, undefined, 0);
            render();
            expect(ClientStorage.getItem('toggleableCourseListStudentDashboard')).toEqual('flat');
            SpecHelper.expectNoElement(elem, '.courses-group > div');
            SpecHelper.expectNoElement(elem, '.courses-group .hr-caption');
            SpecHelper.expectNoElement(elem, '.courses-container stream-link-box', streams.length);
        });
    });

    describe('stream groups', () => {
        it('should sort completed courses by lastProgressAt DESC', () => {
            setupStreams(false, true);
            streams[0].lesson_streams_progress.last_progress_at = 10;
            streams[1].lesson_streams_progress.last_progress_at = 5;
            streams[2].lesson_streams_progress.last_progress_at = 15;

            setupStudentDashboardCall(undefined, undefined, 0);
            render();

            expect(_.map(scope.completedCoursesStreamGroup.streams, stream => stream.title)).toEqual(
                _.map([streams[2], streams[0], streams[1]], stream => stream.title),
            );
        });
    });

    describe('bookmarked courses topic list', () => {
        beforeEach(() => {
            ClientStorage.setItem('toggleableCourseListStudentDashboard', 'topic');
        });

        it('should display the correct number of courses in topic groups', () => {
            setupStreams();
            setupStudentDashboardCall();
            render();

            expect(ClientStorage.getItem('toggleableCourseListStudentDashboard')).toEqual('topic');
            SpecHelper.expectElements(elem, '.courses-container stream-link-box', streams.length);
            SpecHelper.expectElements(elem, '.topic-header', 2); // 2 topics

            SpecHelper.expectElement(elem, '.toggle-topic.active');
            SpecHelper.expectNoElement(elem, '.toggle-flat.active');

            // ensure they're in alphabetical order within each topic (topic 1: 0, 1, topic 2: 2)
            const streamIdsOnPage = [];
            // eslint-disable-next-line func-names
            elem.find('.courses-list [stream-id]').each(function () {
                streamIdsOnPage.push($(this).attr('stream-id'));
            });
            expect(streamIdsOnPage).toEqual([streams[0].id, streams[1].id, streams[2].id]);
        });
    });

    describe('bookmarked courses edge cases', () => {
        it('should hide bookmarked courses section', () => {
            $rootScope.currentUser.groups = [
                {
                    name: 'MBA',
                },
            ];
            streams = [];
            setupStudentDashboardCall(undefined, undefined, 1);
            render();
            expect(ClientStorage.getItem('toggleableCourseListStudentDashboard')).toEqual('topic');
            SpecHelper.expectNoElement(elem, '.courses-container .hr-caption-bookmark');
            SpecHelper.expectNoElement(elem, '.courses-container .hr-hexagon-bookmark');
            SpecHelper.expectNoElement(elem, '.courses-container stream-link-box');
            SpecHelper.expectNoElement(elem, '.courses-container .topic-header');
        });

        it('should not have a bookmarked course that is part of the active playlist', () => {
            setupStreams();
            setupStudentDashboardCall(undefined, undefined, 1);
            render();

            // Should have the default of 3 courses (not including any streams in the playlist)
            SpecHelper.expectElements(elem, '.courses-container stream-link-box', 3);
        });
    });

    describe('course list toggle', () => {
        it('should default to topic if previously set to playlist', () => {
            ClientStorage.setItem('toggleableCourseListStudentDashboard', 'playlist');
            $rootScope.currentUser.groups = [
                {
                    name: 'MBA',
                },
            ];
            setupStreams();
            setupStudentDashboardCall();
            render();
            expect(ClientStorage.getItem('toggleableCourseListStudentDashboard')).toEqual('topic');
            SpecHelper.expectElements(elem, '.courses-container stream-link-box', streams.length);
            SpecHelper.expectElements(elem, '.topic-header', 2); // 2 topics

            // cleanup
            ClientStorage.removeItem('toggleableCourseListStudentDashboard');
        });

        it('should default to topic for new users', () => {
            $rootScope.currentUser.groups = [
                {
                    name: 'MBA',
                },
            ];
            setupStreams();
            setupStudentDashboardCall();
            render();
            expect(ClientStorage.getItem('toggleableCourseListStudentDashboard')).toEqual('topic');
            SpecHelper.expectElements(elem, '.courses-container stream-link-box', streams.length);
            SpecHelper.expectElements(elem, '.topic-header', 2); // 2 topics
        });

        describe('with topic displayMode', () => {
            it('should filter out complete streams and show all courses complete message if all are complete', () => {
                $rootScope.currentUser.groups = [
                    {
                        name: 'MBA',
                    },
                ];
                setupStreams(false, true);
                setupStudentDashboardCall();
                render();
                expect(ClientStorage.getItem('toggleableCourseListStudentDashboard')).toEqual('topic');
                SpecHelper.expectNoElement(elem, '.courses-container .hr-caption-bookmark');
                SpecHelper.expectNoElement(elem, '.courses-container .hr-hexagon-bookmark');
                SpecHelper.expectNoElement(elem, '.courses-container stream-link-box');
                SpecHelper.expectNoElement(elem, '.courses-container .topic-header');
            });
        });

        it('should switch from topic to flat when clicked', () => {
            setupStreams();
            setupStudentDashboardCall();
            render();
            // verify initial topic grouping
            expect(ClientStorage.getItem('toggleableCourseListStudentDashboard')).toEqual('topic');
            SpecHelper.expectElements(elem, 'stream-link-box', streams.length);
            SpecHelper.expectElements(elem, '.topic-header', 2); // 2 topics

            // click to toggle
            SpecHelper.click(elem, '.toggle-flat');

            // verify flat listing
            expect(ClientStorage.getItem('toggleableCourseListStudentDashboard')).toEqual('flat');
            SpecHelper.expectElements(elem, '.flat-container stream-link-box', streams.length);
            SpecHelper.expectNoElement(elem, '.flat-container .topic-header');
        });

        it('should switch from topic to flat when clicked', () => {
            setupStreams();
            setupStudentDashboardCall();
            render();
            // verify initial topic grouping
            expect(ClientStorage.getItem('toggleableCourseListStudentDashboard')).toEqual('topic');
            SpecHelper.expectElements(elem, 'stream-link-box', streams.length);
            SpecHelper.expectElements(elem, '.topic-header', 2); // 2 topics

            // click to toggle to flat
            SpecHelper.click(elem, '.toggle-flat');

            // verify flat listing
            expect(ClientStorage.getItem('toggleableCourseListStudentDashboard')).toEqual('flat');
            SpecHelper.expectElements(elem, '.topic-container stream-link-box', streams.length);
        });
    });

    describe('top messaging section', () => {
        describe('with access to playlists', () => {
            it('should not show for users who have access to playlists', () => {
                setupStreams(false, false, []);
                setupStudentDashboardCall();
                render();
                SpecHelper.expectNoElement(elem, '.top-nav.container');
                SpecHelper.expectNoElement(elem, '.browse-links-section');
            });
        });

        describe('without access to playlists', () => {
            it('should show if no streams are bookmarked and some are in the library', () => {
                setupStreams(false, false, []);
                setupStudentDashboardCall(undefined, undefined, 0);
                render();
                SpecHelper.expectElement(elem, '.browse-links-section.prominent');
                SpecHelper.expectElementText(
                    elem,
                    '.browse-links-section.prominent .title',
                    'Uh oh! You have no courses.',
                );
                SpecHelper.expectElementText(
                    elem,
                    '.browse-links-section.prominent .sub-title',
                    'Browse the Library to add courses to your dashboard.',
                );
            });

            it('should display a special message when all courses are completed and there are no more in library', () => {
                setupStreams(true, true, undefined);
                setupStudentDashboardCall(undefined, undefined, 0);
                render();
                SpecHelper.expectElement(elem, '.browse-links-section.prominent.completed');
                SpecHelper.expectElementText(
                    elem,
                    '.browse-links-section.prominent.completed .title',
                    'All Courses Complete!',
                );
                SpecHelper.expectElementText(elem, '.browse-links-section.prominent.completed .sub-title', '');
            });

            it('should display a special message when all courses are completed but there are more in library', () => {
                setupStreams(true, true, undefined);
                setupStudentDashboardCall(undefined, undefined, 0, true);
                render();
                SpecHelper.expectElement(elem, '.browse-links-section.prominent.completed');
                SpecHelper.expectElementText(elem, '.browse-links-section.prominent.completed .title', 'Nice Work!');
                SpecHelper.expectElementText(
                    elem,
                    '.browse-links-section.prominent.completed .sub-title',
                    'You’ve completed all of your selected courses. Browse the Library for new things to learn.',
                );
            });
        });
    });

    describe('learning box mode behavior', () => {
        it('should be set to playlists if user is seeing cohort accepted message for the first time', () => {
            Object.defineProperty($rootScope.currentUser, 'acceptedCohortApplication', {
                value: {
                    foo: 'bar',
                    program_type: 'mba',
                },
            });
            Object.defineProperty($rootScope.currentUser, 'programType', {
                value: 'mba',
            });
            $rootScope.currentUser.hasSeenAccepted = false;

            jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
            setupStreams();
            setupStudentDashboardCall();
            CareersSpecHelper.stubCareersNetworkViewModel('candidate');
            render();

            expect(scope.learningBoxMode.mode).toEqual('playlists');
        });

        describe('with available playlists', () => {
            it('should show active_playlist if active playlist not complete', () => {
                setupStreams();
                setupStudentDashboardCall();
                render();

                expect(scope.learningBoxMode.mode).toEqual('active_playlist');
            });

            it('should show playlists if recommended playlist and active playlist complete', () => {
                setupStreams();
                setupStudentDashboardCall(undefined, undefined, undefined, undefined, true);
                jest.spyOn($rootScope.currentUser, 'hasPlaylists', 'get').mockReturnValue(true);
                activePlaylist.streams.forEach(stream => {
                    stream.lesson_streams_progress.complete = true;
                });
                render();

                expect(scope.learningBoxMode.mode).toEqual('playlists');
            });

            it('should activate recommended playlist when activatePlaylist is called', () => {
                setupStreams();
                setupStudentDashboardCall(undefined, undefined, 2);
                jest.spyOn($rootScope.currentUser, 'hasPlaylists', 'get').mockReturnValue(true);

                // mark active playlist as complete
                activePlaylist.streams.forEach(stream => {
                    stream.lesson_streams_progress.complete = true;
                });

                // get reference to second playlist
                const anotherPlaylist = playlists[1];

                const returnValue = {
                    result: [
                        {
                            active_playlist: activePlaylist,
                            lesson_streams: _.union(streams, playlistStreams),
                        },
                    ],
                };

                StudentDashboard.expect('index').returns(returnValue);
                Playlist.resetCache();

                render();

                expect(scope.learningBoxMode.mode).toEqual('playlists');

                jest.spyOn(scope.currentUser, 'save').mockReturnValue($q.when());

                // Mock second api return value
                const secondReturnValue = {
                    result: [
                        {
                            active_playlist: anotherPlaylist,
                            lesson_streams: _.union(streams, playlistStreams),
                        },
                    ],
                };

                StudentDashboard.expect('index').returns(secondReturnValue);
                Playlist.expect('index').returns(playlists);
                Playlist.resetCache();

                // Mock out activatePlaylist being called as a callback
                scope.activatePlaylist(anotherPlaylist);
                $timeout.flush();

                scope.$apply();

                expect(scope.learningBoxMode.mode).toEqual('active_playlist');
            });

            it('should hide the mode toggle if only one available playlist', () => {
                setupStreams();
                setupStudentDashboardCall(undefined, undefined, 1);
                render();

                expect(scope.learningBoxMode.mode).toEqual('active_playlist');
                SpecHelper.expectNoElement(elem, '.playlists-toggle-button');
            });
        });

        describe('with no available playlists', () => {
            it('should show course view', () => {
                setupStreams();
                setupStudentDashboardCall(undefined, undefined, 0);
                render();

                expect(scope.learningBoxMode.mode).toEqual('course');
            });
        });
    });

    describe('sidebar states', () => {
        function setupSmarterFree(playlistProgress) {
            $rootScope.currentUser.addGroup('SMARTER');
            $rootScope.currentUser.relevant_cohort = relevantCohort;
            $rootScope.currentUser.mba_enabled = true;
            $rootScope.currentUser.career_profile = CareerProfile.fixtures.getInstance();
            isMobile.mockReturnValue(false);
            setupStreams(false, false);
            setupStudentDashboardCall(undefined, undefined, 1);

            activePlaylist.streams.forEach(stream => {
                stream.lesson_streams_progress = playlistProgress;
            });
        }

        function setupBOSHIGH(bosProgress) {
            $rootScope.currentUser.groups = [
                {
                    name: 'BLUEOCEAN',
                },
            ];
            $rootScope.currentUser.relevant_cohort = null;
            $rootScope.currentUser.sign_up_code = 'BOSHIGH';
            $rootScope.currentUser.institutions = [
                Institution.new({
                    name: 'BOSHIGH',
                    id: 'BOSHIGH',
                }),
            ];
            const blueOceanStream = Stream.fixtures.getInstance();
            blueOceanStream.id = '87b85177-fdf6-4cf8-a29a-c44b0ef77530'; // BOS stream ID

            isMobile.mockReturnValue(false);
            setupStreams(false, false, [blueOceanStream]);
            setupStudentDashboardCall(undefined, undefined, 0);

            blueOceanStream.lesson_streams_progress = bosProgress;
        }

        function setupExternalInstitution(playlistProgress) {
            $rootScope.currentUser.groups = [
                {
                    name: 'GEORGETOWNMSB',
                },
            ];
            $rootScope.currentUser.sign_up_code = 'GEORGETOWNMSB';
            $rootScope.currentUser.institutions = [
                Institution.new({
                    name: 'GEORGETOWNMSB',
                    id: 'GEORGETOWNMSB',
                }),
            ];
            $rootScope.currentUser.relevant_cohort = null;
            isMobile.mockReturnValue(false);
            setupStreams(false, false);
            setupStudentDashboardCall(undefined, undefined, 1);

            activePlaylist.streams.forEach(stream => {
                stream.lesson_streams_progress = playlistProgress;
            });
        }

        it('should display sidebar if user is not accepted into a cohort and is a SMARTER user', () => {
            setupSmarterFree();
            $rootScope.currentUser.active_cohorts = undefined;
            render();
            SpecHelper.expectElement(elem, 'student-dashboard-welcome-box');
        });

        it('should not display sidebar if user has been accepted into a cohort and is a SMARTER user', () => {
            setupSmarterFree();
            $rootScope.currentUser.cohort_applications = [
                {
                    status: 'accepted',
                },
            ];
            CareersSpecHelper.stubCareersNetworkViewModel('candidate');
            render();
            SpecHelper.expectNoElement(elem, 'student-dashboard-welcome-box');
            SpecHelper.expectElements(elem, 'student-dashboard-program-box');
            SpecHelper.expectNoElement(elem, 'student-dashboard-recent-courses');
        });

        it('should not display welcome box if non-external-institution user has no relevant_cohort', () => {
            setupSmarterFree();
            $rootScope.currentUser.relevant_cohort = undefined;
            render();
            SpecHelper.expectNoElement(elem, 'student-dashboard-welcome-box');
        });

        describe('certificate graduation box', () => {
            it('should be visible if showCertificateGraduationBox', () => {
                setupSmarterFree();
                render();
                SpecHelper.expectNoElement(elem, 'student-dashboard-certificate-graduation-box');
                scope.showCertificateGraduationBox = true;
                scope.$digest();
                SpecHelper.expectElement(elem, 'student-dashboard-certificate-graduation-box');
            });
        });

        // There may be some test duplication here. I wanted to start thinking about the various
        // components' display logic individually.
        describe('welcome box', () => {
            it('should not show welcome box if user had a cohort application rejected', () => {
                setupSmarterFree();
                Object.defineProperty($rootScope.currentUser, 'isRejectedOrExpelledOrDeferred', {
                    value: true,
                });
                render();
                SpecHelper.expectNoElement(elem, 'student-dashboard-welcome-box');
            });

            it('should show welcome box if user is pending', () => {
                setupSmarterFree();
                $rootScope.currentUser.cohort_applications = [
                    {
                        registered: false,
                        program_type: 'FAKEDOESNOTMATTER',
                        status: 'pending',
                    },
                ];
                render();
                SpecHelper.expectElement(elem, 'student-dashboard-welcome-box');
            });

            it('should show welcome box if user is pre_accepted into EMBA', () => {
                setupSmarterFree();
                $rootScope.currentUser.cohort_applications = [
                    {
                        registered: false,
                        program_type: 'emba',
                        status: 'pre_accepted',
                    },
                ];
                render();
                SpecHelper.expectElement(elem, 'student-dashboard-welcome-box');
            });

            it('should show welcome box if user is pre_accepted into MBA', () => {
                setupSmarterFree();
                $rootScope.currentUser.cohort_applications = [
                    {
                        registered: false,
                        program_type: 'mba',
                        status: 'pre_accepted',
                    },
                ];
                render();
                SpecHelper.expectElement(elem, 'student-dashboard-welcome-box');
            });

            it('should not show welcome box if user is pre_accepted into The Biz Cert', () => {
                setupSmarterFree();
                $rootScope.currentUser.cohort_applications = [
                    {
                        registered: false,
                        program_type: 'the_business_certificate',
                        status: 'pre_accepted',
                    },
                ];
                $rootScope.currentUser.mba_enabled = false;
                render();
                SpecHelper.expectNoElement(elem, 'student-dashboard-welcome-box');
            });

            it('should not show welcome box if user is pre_accepted into career network only', () => {
                setupSmarterFree();
                $rootScope.currentUser.cohort_applications = [
                    {
                        registered: false,
                        program_type: 'career_network_only',
                        status: 'pre_accepted',
                    },
                ];
                $rootScope.currentUser.mba_enabled = false;
                render();
                SpecHelper.expectNoElement(elem, 'student-dashboard-welcome-box');
            });

            it('should not show welcome box if user is accepted into EMBA', () => {
                setupSmarterFree();
                $rootScope.currentUser.cohort_applications = [
                    {
                        registered: false,
                        program_type: 'emba',
                        status: 'accepted',
                    },
                ];
                render();
                SpecHelper.expectNoElement(elem, 'student-dashboard-welcome-box');
            });

            it('should not show welcome box if user is accepted into MBA', () => {
                setupSmarterFree();
                $rootScope.currentUser.cohort_applications = [
                    {
                        registered: false,
                        program_type: 'mba',
                        status: 'accepted',
                    },
                ];
                render();
                SpecHelper.expectNoElement(elem, 'student-dashboard-welcome-box');
            });

            it('should show welcome box if user is accepted into The Biz Cert', () => {
                setupSmarterFree();
                $rootScope.currentUser.cohort_applications = [
                    {
                        registered: false,
                        program_type: 'the_business_certificate',
                        status: 'accepted',
                    },
                ];
                $rootScope.currentUser.mba_enabled = false;
                render();
                SpecHelper.expectElement(elem, 'student-dashboard-welcome-box');
            });

            it('should show welcome box if user is accepted into career network only', () => {
                setupSmarterFree();
                $rootScope.currentUser.cohort_applications = [
                    {
                        registered: false,
                        program_type: 'career_network_only',
                        status: 'accepted',
                    },
                ];
                $rootScope.currentUser.mba_enabled = false;
                render();
                SpecHelper.expectElement(elem, 'student-dashboard-welcome-box');
            });

            it('should show welcome box if user has no application', () => {
                setupSmarterFree();
                $rootScope.currentUser.cohort_applications = [];
                render();
                SpecHelper.expectElement(elem, 'student-dashboard-welcome-box');
            });

            it('should show welcome box if user has external institution with no recent courses', () => {
                setupExternalInstitution();
                render();
                SpecHelper.expectElement(elem, 'student-dashboard-welcome-box');
            });
        });

        describe('program box area', () => {
            it('should only show program box', () => {
                assertDirectiveIsVisibility('student-dashboard-program-box', {});
            });

            it('should only show schedule box', () => {
                assertDirectiveIsVisibility('student-dashboard-schedule', { showSchedule: true });
            });

            it('should hide schedule box when offline', () => {
                assertDirectiveIsVisibility('student-dashboard-schedule', {
                    visible: false,
                    showSchedule: true,
                    inOfflineMode: true,
                });
            });

            it('should only show program complete box', () => {
                assertDirectiveIsVisibility('student-dashboard-certificate-graduation-box', {
                    showSchedule: true,
                    showCertificateGraduationBox: true,
                });
            });

            function assertDirectiveIsVisibility(
                expectedDirectiveSelector,
                { showSchedule, showCertificateGraduationBox, inOfflineMode, visible },
            ) {
                if (visible !== false) {
                    visible = true;
                }

                const directiveElementSelectors = [
                    'student-dashboard-program-box',
                    'student-dashboard-schedule',
                    'student-dashboard-certificate-graduation-box',
                ];
                setupSmarterFree();
                Object.defineProperty($rootScope.currentUser, 'lastCohortApplication', {
                    value: {
                        foo: 'bar',
                        program_type: 'qux',
                    },
                    configurable: true,
                });
                CareersSpecHelper.stubCareersNetworkViewModel('candidate');
                render();

                scope.showSchedule = !!showSchedule;
                scope.showCertificateGraduationBox = !!showCertificateGraduationBox;
                offlineModeManager.inOfflineMode = inOfflineMode;
                scope.$digest();

                if (visible) {
                    SpecHelper.expectElement(elem, expectedDirectiveSelector);
                } else {
                    SpecHelper.expectNoElement(elem, expectedDirectiveSelector);
                }

                // only the expected element directive should be present; the others shouldn't be rendered.
                _.each(
                    _.reject(directiveElementSelectors, selector => selector === expectedDirectiveSelector),
                    unexpectedDirective => {
                        SpecHelper.expectNoElement(elem, unexpectedDirective);
                    },
                );
            }
        });

        describe('enrollment box', () => {
            it('should be visible when showEnrollment is true', () => {
                setupSmarterFree();
                render();

                const showEnrollmentSpy = jest.spyOn(scope, 'showEnrollment', 'get').mockReturnValue(false);
                scope.$digest();
                SpecHelper.expectNoElement(elem, 'student-dashboard-enrollment');

                showEnrollmentSpy.mockReturnValue(true);
                scope.$digest();
                SpecHelper.expectElement(elem, 'student-dashboard-enrollment');
            });
        });

        describe('schedule interview box', () => {
            it('should be visible when showScheduleInterview is true', () => {
                setupSmarterFree();
                render();

                const showScheduleInterviewSpy = jest
                    .spyOn(scope, 'showScheduleInterview', 'get')
                    .mockReturnValue(false);
                scope.$digest();
                SpecHelper.expectNoElement(elem, 'student-dashboard-schedule-interview');

                showScheduleInterviewSpy.mockReturnValue(true);
                scope.$digest();
                SpecHelper.expectElement(elem, 'student-dashboard-schedule-interview');
            });
        });

        describe('first time experience', () => {
            it('should display the appropriate components for SMARTER FREE users', () => {
                setupSmarterFree();
                render();

                SpecHelper.expectElement(elem, 'student-dashboard-welcome-box');
                SpecHelper.expectElements(elem, 'student-dashboard-program-box');
                SpecHelper.expectNoElement(elem, 'student-dashboard-recent-courses');
            });

            it('should display the appropriate components for BOSHIGH users', () => {
                setupBOSHIGH();
                render();

                SpecHelper.expectElement(elem, 'student-dashboard-welcome-box');
                SpecHelper.expectNoElement(elem, 'student-dashboard-recent-courses');
                SpecHelper.expectNoElement(elem, 'student-dashboard-program-box');

                // cleanup
                $rootScope.currentUser.institutions = [];
            });

            it('should display the appropriate components for external-institution users', () => {
                setupExternalInstitution();
                render();

                SpecHelper.expectElement(elem, 'student-dashboard-welcome-box');
                SpecHelper.expectNoElement(elem, 'student-dashboard-recent-courses');
                SpecHelper.expectNoElement(elem, 'student-dashboard-program-box');

                // cleanup
                $rootScope.currentUser.institutions = [];
            });
        });

        describe('with some progress', () => {
            it('should display the appropriate sidebar for SMARTER FREE users', () => {
                setupSmarterFree({
                    complete: false,
                    last_progress_at: Date.now() / 1000,
                    created_at: Date.now() / 1000,
                });
                render();

                SpecHelper.expectElements(elem, 'student-dashboard-program-box');
                SpecHelper.expectElement(elem, 'student-dashboard-recent-courses');
            });

            it('should display the appropriate sidebar for BOSHIGH users', () => {
                setupBOSHIGH({
                    complete: false,
                    last_progress_at: Date.now() / 1000,
                    created_at: Date.now() / 1000,
                });
                render();

                SpecHelper.expectNoElement(elem, 'student-dashboard-welcome-box');
                SpecHelper.expectNoElement(elem, 'student-dashboard-program-box');
                SpecHelper.expectElement(elem, 'student-dashboard-recent-courses');
            });

            it('should display the appropriate sidebar for external-institution users', () => {
                setupExternalInstitution({
                    complete: false,
                    last_progress_at: Date.now() / 1000,
                    created_at: Date.now() / 1000,
                });
                render();

                SpecHelper.expectNoElement(elem, 'student-dashboard-welcome-box');
                SpecHelper.expectNoElement(elem, 'student-dashboard-program-box');
                SpecHelper.expectElement(elem, 'student-dashboard-recent-courses');
            });
        });

        describe('with all courses completed', () => {
            it('should display the appropriate sidebar for SMARTER FREE users', () => {
                setupSmarterFree({
                    complete: true,
                    last_progress_at: Date.now() / 1000,
                    created_at: Date.now() / 1000,
                });
                render();

                SpecHelper.expectElements(elem, 'student-dashboard-program-box');
                SpecHelper.expectElement(elem, 'student-dashboard-recent-courses');
            });

            it('should display no sidebar for BOSHIGH users', () => {
                setupBOSHIGH({
                    complete: true,
                    last_progress_at: Date.now() / 1000,
                    created_at: Date.now() / 1000,
                });
                render();

                SpecHelper.expectNoElement(elem, 'student-dashboard-welcome-box');
                SpecHelper.expectNoElement(elem, 'student-dashboard-program-box');
                SpecHelper.expectNoElement(elem, 'student-dashboard-recent-courses');

                // no learning box because there's no playlists for these people
                SpecHelper.expectNoElement(elem, 'course-recommendation-box');
                SpecHelper.expectElementText(
                    elem,
                    '.browse-links-section.prominent.completed .title',
                    'Course Completed!',
                );
                SpecHelper.expectElementText(
                    elem,
                    '.browse-links-section.prominent.completed .sub-title',
                    'Well done! You can download your certificate or review the course below.',
                );
            });

            it('should display the appropriate sidebar for external-institution users', () => {
                setupExternalInstitution({
                    complete: true,
                    last_progress_at: Date.now() / 1000,
                    created_at: Date.now() / 1000,
                });
                render();

                SpecHelper.expectNoElement(elem, 'student-dashboard-program-box');
                SpecHelper.expectElement(elem, 'student-dashboard-recent-courses');
            });

            it('should show mobile program box for ceritificate program if failed', () => {
                assertProgramBoxVisibilityForProgramTypeWithGradStatus('the_business_certificate', 'failed');
            });

            it('should show mobile program box for certificate program if graduated', () => {
                assertProgramBoxVisibilityForProgramTypeWithGradStatus('the_business_certificate', 'graduated');
            });

            function assertProgramBoxVisibilityForProgramTypeWithGradStatus(programType, graduationStatus) {
                setupSmarterFree({
                    complete: true,
                    last_progress_at: Date.now() / 1000,
                    created_at: Date.now() / 1000,
                });
                $rootScope.currentUser.cohort_applications = [
                    {
                        status: 'accepted',
                        program_type: programType,
                        completed_at: 1000,
                        graduation_status: graduationStatus,
                    },
                ];
                Object.defineProperty($rootScope.currentUser, 'programType', {
                    value: programType,
                    configurable: true,
                });
                CareersSpecHelper.stubCareersNetworkViewModel('candidate');
                render();
                expect(scope.showProgramBox).toBe(true);
                expect(scope._hideMobileProgramBox).toBe(false);
                SpecHelper.expectElement(elem, 'student-dashboard-program-box');
            }
        });
    });

    describe('view as', () => {
        it('should support viewAs', () => {
            setupStreams(false, false, null);

            StudentDashboard.expect('index')
                .toBeCalledWith({
                    get_has_available_incomplete_streams: true,
                    get_has_available_incomplete_playlists: true,
                    user_id: user.id,
                    do_not_include_progress: true,
                    filters: {
                        view_as: 'group:SOME_GROUP',
                        in_locale_or_en: 'en',
                        user_can_see: null,
                        in_users_locale_or_en: null,
                    },
                })
                .returns({
                    result: [
                        {
                            active_playlist: undefined,
                            lesson_streams: streams,
                        },
                    ],
                    meta: {
                        has_available_incomplete_streams: false,
                        has_available_incomplete_playlists: true,
                    },
                });

            $rootScope.viewAs = 'group:SOME_GROUP';
            render();
        });

        it('should set in_locale_or_en from client-side', () => {
            StudentDashboard.expect('index').toBeCalledWith({
                get_has_available_incomplete_streams: true,
                get_has_available_incomplete_playlists: true,
                user_id: user.id,
                filters: {
                    view_as: 'group:TEST VIEW AS GROUP',
                    in_locale_or_en: 'en',
                    user_can_see: null,
                    in_users_locale_or_en: null,
                },
            });

            $rootScope.viewAs = 'group:TEST VIEW AS GROUP';
            render();
        });
    });

    describe('recommendedPlaylist', () => {
        let nextPlaylist;
        let thirdPlaylist;
        let flushLoadStreamsCall;
        let canLaunchSpy;

        beforeEach(() => {
            const ContentAccessHelper = $injector.get('ContentAccessHelper');
            canLaunchSpy = jest.spyOn(ContentAccessHelper.prototype, 'canLaunch', 'get').mockImplementation(() => true);
        });

        describe('as cohortUser', () => {
            beforeEach(() => {
                $rootScope.currentUser.relevant_cohort = relevantCohort;
            });

            it('should recommend the next incomplete required playlist', () => {
                renderAndAssertLoadStreamsCalled();
                jest.spyOn(activePlaylist, 'complete', 'get').mockReturnValue(true);
                jest.spyOn(nextPlaylist, 'complete', 'get').mockReturnValue(false);
                flushLoadStreamsCall();
                expect(scope.recommendedPlaylist.localePackId).toEqual(nextPlaylist.localePackId);
            });
            it('should not recommend anything if the active playlist is incomplete', () => {
                renderAndAssertLoadStreamsCalled();
                jest.spyOn(activePlaylist, 'complete', 'get').mockReturnValue(false);
                flushLoadStreamsCall();
                expect(scope.recommendedPlaylist).toBeUndefined();
            });
            it('should not recommend a complete playlist', () => {
                renderAndAssertLoadStreamsCalled();
                jest.spyOn(activePlaylist, 'complete', 'get').mockReturnValue(true);
                jest.spyOn(nextPlaylist, 'complete', 'get').mockReturnValue(true);
                flushLoadStreamsCall();
                expect(scope.recommendedPlaylist.localePackId).toEqual(thirdPlaylist.localePackId);
            });
            it('should not recommend a specialization playlist', () => {
                renderAndAssertLoadStreamsCalled();
                jest.spyOn(activePlaylist, 'complete', 'get').mockReturnValue(true);

                user.relevant_cohort.specialization_playlist_pack_ids.push(nextPlaylist.locale_pack.id);
                jest.spyOn(nextPlaylist, 'complete', 'get').mockReturnValue(false);

                flushLoadStreamsCall();
                expect(scope.recommendedPlaylist.localePackId).toEqual(thirdPlaylist.localePackId);
            });
            it('should not recommend anything if all playlists are complete', () => {
                renderAndAssertLoadStreamsCalled();
                jest.spyOn(Playlist.prototype, 'complete', 'get').mockReturnValue(true);
                flushLoadStreamsCall();
                expect(scope.recommendedPlaylist).toBeUndefined();
            });
            it('should not recommend a locked playlist', () => {
                renderAndAssertLoadStreamsCalled();
                jest.spyOn(activePlaylist, 'complete', 'get').mockReturnValue(true);
                jest.spyOn(nextPlaylist, 'complete', 'get').mockReturnValue(false);

                // eslint-disable-next-line func-names
                canLaunchSpy.mockImplementation(function () {
                    return this.contentItem === thirdPlaylist;
                });

                flushLoadStreamsCall();
                expect(scope.recommendedPlaylist.localePackId).toEqual(thirdPlaylist.localePackId);
            });
            it('should not recommend foundations for accepted user when Cohort.excludesFoundationsPlaylistOnAcceptance', () => {
                $rootScope.currentUser.career_profile = CareerProfile.fixtures.getInstance();
                renderAndAssertLoadStreamsCalled();

                Object.defineProperty($rootScope.currentUser, 'lastCohortApplication', {
                    value: {
                        status: 'accepted',
                    },
                });
                Object.defineProperty($rootScope.currentUser, 'programType', {
                    value: 'mba',
                });
                Object.defineProperty(relevantCohort, 'foundationsPlaylistLocalePackId', {
                    value: nextPlaylist.locale_pack.id,
                });

                jest.spyOn(activePlaylist, 'complete', 'get').mockReturnValue(true);
                jest.spyOn(nextPlaylist, 'complete', 'get').mockReturnValue(false);
                flushLoadStreamsCall();
                expect(scope.recommendedPlaylist.localePackId).toEqual(thirdPlaylist.localePackId);
            });
        });
        describe('as non-cohortUser', () => {
            beforeEach(() => {
                $rootScope.currentUser.relevant_cohort = null;
            });

            it('should recommend the next incomplete required playlist, order by percentComplete', () => {
                renderAndAssertLoadStreamsCalled();
                jest.spyOn(activePlaylist, 'complete', 'get').mockReturnValue(true);
                jest.spyOn(nextPlaylist, 'complete', 'get').mockReturnValue(false);
                jest.spyOn(thirdPlaylist, 'complete', 'get').mockReturnValue(false);
                jest.spyOn(nextPlaylist, 'percentComplete', 'get').mockReturnValue(0.2);
                jest.spyOn(thirdPlaylist, 'percentComplete', 'get').mockReturnValue(0.42);
                flushLoadStreamsCall();
                expect(scope.recommendedPlaylist.localePackId).toEqual(thirdPlaylist.localePackId);
            });
            it('should not recommend anything if the active playlist is incomplete', () => {
                renderAndAssertLoadStreamsCalled();
                jest.spyOn(activePlaylist, 'complete', 'get').mockReturnValue(false);
                flushLoadStreamsCall();
                expect(scope.recommendedPlaylist).toBeUndefined();
            });
            it('should not recommend a complete playlist', () => {
                renderAndAssertLoadStreamsCalled();
                jest.spyOn(activePlaylist, 'complete', 'get').mockReturnValue(true);
                jest.spyOn(nextPlaylist, 'complete', 'get').mockReturnValue(true);
                flushLoadStreamsCall();
                expect(scope.recommendedPlaylist.localePackId).toEqual(thirdPlaylist.localePackId);
            });
            it('should not recommend anything if all playlists are complete', () => {
                renderAndAssertLoadStreamsCalled();
                jest.spyOn(Playlist.prototype, 'complete', 'get').mockReturnValue(true);
                flushLoadStreamsCall();
                expect(scope.recommendedPlaylist).toBeUndefined();
            });
            it('should not recommend a locked playlist', () => {
                renderAndAssertLoadStreamsCalled();
                jest.spyOn(activePlaylist, 'complete', 'get').mockReturnValue(true);
                jest.spyOn(nextPlaylist, 'complete', 'get').mockReturnValue(false);
                jest.spyOn(thirdPlaylist, 'complete', 'get').mockReturnValue(false);
                jest.spyOn(nextPlaylist, 'percentComplete', 'get').mockReturnValue(0.42);
                jest.spyOn(thirdPlaylist, 'percentComplete', 'get').mockReturnValue(0.2);

                // eslint-disable-next-line func-names
                canLaunchSpy.mockImplementation(function () {
                    return this.contentItem === thirdPlaylist;
                });

                flushLoadStreamsCall();
                expect(scope.recommendedPlaylist.localePackId).toEqual(thirdPlaylist.localePackId);
            });
        });

        function renderAndAssertLoadStreamsCalled() {
            setupStreams();
            setupStudentDashboardCall();

            jest.spyOn(Playlist, 'loadStreams').mockReturnValue(
                $q(resolve => {
                    flushLoadStreamsCall = () => {
                        resolve();
                        $timeout.flush();
                    };
                }),
            );
            render();
            expect(Playlist.loadStreams).toHaveBeenCalled();
            activePlaylist = scope.playlists[0];
            nextPlaylist = scope.playlists[1];
            thirdPlaylist = scope.playlists[2];

            // sanity checks.  setupPlaylists ensures these, but just
            // checking
            expect(scope.activePlaylist).toEqual(activePlaylist);
            expect(relevantCohort.requiredPlaylistPackIds).toEqual(_.pluck(playlists, 'localePackId'));
        }
    });

    describe('showResearchResources', () => {
        it('should be true when programType is supported and graduationStatus is pending', () => {
            jest.spyOn(user, 'programType', 'get').mockReturnValue('mba');
            jest.spyOn(user, 'graduationStatus', 'get').mockReturnValue('pending');
            setupStudentDashboardCall();
            render();
            expect(scope.showResearchResources).toBe(true);
            SpecHelper.expectElements(elem, 'student-dashboard-research-resources', 2); // 2 because there's a mobile and a desktop
        });

        it('should be false when programType is not supported', () => {
            jest.spyOn(user, 'programType', 'get').mockReturnValue('foo');
            jest.spyOn(user, 'graduationStatus', 'get').mockReturnValue('pending');
            setupStudentDashboardCall();
            render();
            expect(scope.showResearchResources).toBe(false);
            SpecHelper.expectNoElement(elem, 'student-dashboard-research-resources');
        });

        it('should be false when graduationStatus is not pending', () => {
            jest.spyOn(user, 'programType', 'get').mockReturnValue('mba');
            jest.spyOn(user, 'graduationStatus', 'get').mockReturnValue('foo');
            setupStudentDashboardCall();
            render();
            expect(scope.showResearchResources).toBe(false);
            SpecHelper.expectNoElement(elem, 'student-dashboard-research-resources');
        });

        it('should be true for supereditors / admins', () => {
            jest.spyOn(user, 'programType', 'get').mockReturnValue('mba');
            jest.spyOn(user, 'graduationStatus', 'get').mockReturnValue('foo');
            jest.spyOn(user, 'hasSuperEditorAccess', 'get').mockReturnValue(true);
            setupStudentDashboardCall();
            render();
            expect(scope.showResearchResources).toBe(true);
            SpecHelper.expectElements(elem, 'student-dashboard-research-resources', 2); // 2 because there's a mobile and a desktop
        });

        it('should be true for interviewers', () => {
            jest.spyOn(user, 'programType', 'get').mockReturnValue('mba');
            jest.spyOn(user, 'graduationStatus', 'get').mockReturnValue('foo');
            jest.spyOn(user, 'hasInterviewerAccess', 'get').mockReturnValue(true);
            setupStudentDashboardCall();
            render();
            expect(scope.showResearchResources).toBe(true);
            SpecHelper.expectElements(elem, 'student-dashboard-research-resources', 2); // 2 because there's a mobile and a desktop
        });

        it('should be true for interviewers who are demo users', () => {
            jest.spyOn(user, 'programType', 'get').mockReturnValue('demo');
            jest.spyOn(user, 'graduationStatus', 'get').mockReturnValue('foo');
            jest.spyOn(user, 'hasInterviewerAccess', 'get').mockReturnValue(true);
            setupStudentDashboardCall();
            render();
            expect(scope.showResearchResources).toBe(true);
            SpecHelper.expectElements(elem, 'student-dashboard-research-resources', 2); // 2 because there's a mobile and a desktop
        });

        it('should be true for supereditors / admins in an external institution', () => {
            jest.spyOn(user, 'programType', 'get').mockReturnValue('external');
            jest.spyOn(user, 'graduationStatus', 'get').mockReturnValue('foo');
            jest.spyOn(user, 'hasSuperEditorAccess', 'get').mockReturnValue(true);
            setupStudentDashboardCall();
            render();
            expect(scope.showResearchResources).toBe(true);
            SpecHelper.expectElements(elem, 'student-dashboard-research-resources', 2); // 2 because there's a mobile and a desktop
        });

        it('should be false when offline', () => {
            jest.spyOn(user, 'programType', 'get').mockReturnValue('mba');
            jest.spyOn(user, 'graduationStatus', 'get').mockReturnValue('pending');
            setupStudentDashboardCall();
            render();
            expect(scope.showResearchResources).toBe(true); // sanity check
            offlineModeManager.inOfflineMode = true;
            scope.$digest();
            expect(scope.showResearchResources).toBe(false); // sanity check
        });
    });

    describe('showEnrollment', () => {
        let needsToRegisterSpy;
        let lastCohortApplicationStatusSpy;
        let graduationStatusSpy;
        let programTypeSpy;

        beforeEach(() => {
            lastCohortApplicationStatusSpy = jest
                .spyOn(user, 'lastCohortApplicationStatus', 'get')
                .mockReturnValue(CohortApplication.VALID_ENROLLMENT_STATUSES[0]);
            needsToRegisterSpy = jest.spyOn(user, 'needsToRegister', 'get').mockReturnValue(false);
            programTypeSpy = jest.spyOn(user, 'programType', 'get').mockReturnValue('mba');
            graduationStatusSpy = jest.spyOn(user, 'graduationStatus', 'get').mockReturnValue('pending');
            user.relevant_cohort = relevantCohort;
            user.career_profile = CareerProfile.fixtures.getInstance();
        });

        it('should be true when recordsIndicateUserShouldUploadIdentificationDocument is true and identity_verified is false', () => {
            jest.spyOn(user, 'recordsIndicateUserShouldUploadIdentificationDocument', 'get').mockReturnValue(true);
            user.identity_verified = false;
            jest.spyOn(user, 'careerProfileIndicatesUserShouldProvideTranscripts', 'get').mockReturnValue(false);
            jest.spyOn(
                user,
                'careerProfileIndicatesUserShouldUploadEnglishLanguageProficiencyDocuments',
                'get',
            ).mockReturnValue(false);
            jest.spyOn(user, 'missingAddress', 'get').mockReturnValue(false);
            setupStudentDashboardCall();
            render();
            expect(scope.showEnrollment).toBe(true);
        });

        it('should be true when careerProfileIndicatesUserShouldProvideTranscripts is true and transcripts_verified is false', () => {
            jest.spyOn(user, 'recordsIndicateUserShouldUploadIdentificationDocument', 'get').mockReturnValue(false);
            jest.spyOn(user, 'careerProfileIndicatesUserShouldProvideTranscripts', 'get').mockReturnValue(true);
            user.transcripts_verified = false;
            jest.spyOn(
                user,
                'careerProfileIndicatesUserShouldUploadEnglishLanguageProficiencyDocuments',
                'get',
            ).mockReturnValue(false);
            jest.spyOn(user, 'missingAddress', 'get').mockReturnValue(false);
            setupStudentDashboardCall();
            render();
            expect(scope.showEnrollment).toBe(true);
        });

        it('should be true when careerProfileIndicatesUserShouldUploadEnglishLanguageProficiencyDocuments is true and english_language_proficiency_documents_approved is false', () => {
            jest.spyOn(user, 'recordsIndicateUserShouldUploadIdentificationDocument', 'get').mockReturnValue(false);
            jest.spyOn(user, 'careerProfileIndicatesUserShouldProvideTranscripts', 'get').mockReturnValue(false);
            jest.spyOn(
                user,
                'careerProfileIndicatesUserShouldUploadEnglishLanguageProficiencyDocuments',
                'get',
            ).mockReturnValue(true);
            user.english_language_proficiency_documents_approved = false;
            jest.spyOn(user, 'missingAddress', 'get').mockReturnValue(false);
            setupStudentDashboardCall();
            render();
            expect(scope.showEnrollment).toBe(true);
        });

        it('should be true when missingAddress is true ', () => {
            jest.spyOn(user, 'recordsIndicateUserShouldUploadIdentificationDocument', 'get').mockReturnValue(false);
            jest.spyOn(user, 'careerProfileIndicatesUserShouldProvideTranscripts', 'get').mockReturnValue(false);
            jest.spyOn(
                user,
                'careerProfileIndicatesUserShouldUploadEnglishLanguageProficiencyDocuments',
                'get',
            ).mockReturnValue(false);
            jest.spyOn(user, 'missingAddress', 'get').mockReturnValue(true);
            setupStudentDashboardCall();
            render();
            expect(scope.showEnrollment).toBe(true);
        });

        it('should be false when lastCohortApplicationStatus is not one of the VALID_ENROLLMENT_STATUSES', () => {
            lastCohortApplicationStatusSpy.mockReturnValue('foo');
            mockAllItemsRequired();
            setupStudentDashboardCall();
            render();
            expect(scope.showEnrollment).toBe(false);

            lastCohortApplicationStatusSpy.mockReturnValue(undefined);
            scope.$digest();
            expect(scope.showEnrollment).toBe(false);
        });

        it('should be false when needsToRegister is true', () => {
            needsToRegisterSpy.mockReturnValue(true);
            mockAllItemsRequired();
            setupStudentDashboardCall();
            render();
            expect(scope.showEnrollment).toBe(false);
        });

        it("should be false when there's no relevant_cohort", () => {
            user.relevant_cohort = null;
            mockAllItemsRequired();
            setupStudentDashboardCall();
            render();
            expect(scope.showEnrollment).toBe(false);
        });

        it('should be false when the user has graduated', () => {
            graduationStatusSpy.mockReturnValue('graduated');
            mockAllItemsRequired();
            setupStudentDashboardCall();
            render();
            expect(scope.showEnrollment).toBe(false);
        });

        it("should be true when the user has a graduationStatus of 'pending'", () => {
            graduationStatusSpy.mockReturnValue('pending');
            mockAllItemsRequired();
            setupStudentDashboardCall();
            render();
            expect(scope.showEnrollment).toBe(true);
        });

        it('should be true when the user has an unset graduationStatus', () => {
            graduationStatusSpy.mockReturnValue(null);
            mockAllItemsRequired();
            setupStudentDashboardCall();
            render();
            expect(scope.showEnrollment).toBe(true);
        });

        it('should be false when programType is not supported', () => {
            programTypeSpy.mockReturnValue('foo');
            mockAllItemsRequired();
            setupStudentDashboardCall();
            render();
            expect(scope.showEnrollment).toBe(false);
        });

        it('should be true when programType is supported', () => {
            programTypeSpy.mockReturnValue('emba');
            mockAllItemsRequired();
            setupStudentDashboardCall();
            render();
            expect(scope.showEnrollment).toBe(true);
        });

        it('should be true when hasActiveEnrollmentAgreementSigningLink', () => {
            user.identity_verified = false;
            jest.spyOn(user, 'careerProfileIndicatesUserShouldProvideTranscripts', 'get').mockReturnValue(false);
            jest.spyOn(
                user,
                'careerProfileIndicatesUserShouldUploadEnglishLanguageProficiencyDocuments',
                'get',
            ).mockReturnValue(false);
            jest.spyOn(user, 'missingAddress', 'get').mockReturnValue(false);
            setupStudentDashboardCall();
            render();

            // sanity check
            expect(scope.showEnrollment).not.toBe(true);
            jest.spyOn(user.lastCohortApplication, 'hasActiveEnrollmentAgreementSigningLink', 'get').mockReturnValue(
                true,
            );
            scope.$digest();
            expect(scope.showEnrollment).toBe(true);
        });

        it("should be true when the last_calculated_complete_percentage on the user's career_profile is not 100", () => {
            user.identity_verified = false;
            jest.spyOn(user, 'careerProfileIndicatesUserShouldProvideTranscripts', 'get').mockReturnValue(false);
            jest.spyOn(
                user,
                'careerProfileIndicatesUserShouldUploadEnglishLanguageProficiencyDocuments',
                'get',
            ).mockReturnValue(false);
            jest.spyOn(user, 'missingAddress', 'get').mockReturnValue(false);
            jest.spyOn(user.lastCohortApplication, 'hasActiveEnrollmentAgreementSigningLink', 'get').mockReturnValue(
                false,
            );
            user.career_profile.last_calculated_complete_percentage = 100;
            setupStudentDashboardCall();
            render();

            expect(scope.showEnrollment).not.toBe(true); // sanity check
            user.career_profile.last_calculated_complete_percentage = 99;
            scope.$digest();
            expect(scope.showEnrollment).toBe(true);
        });

        it('should be false in offline mode', () => {
            graduationStatusSpy.mockReturnValue('pending');
            mockAllItemsRequired();
            setupStudentDashboardCall();
            render();
            expect(scope.showEnrollment).toBe(true); // sanity check
            offlineModeManager.inOfflineMode = true;
            scope.$digest();
            expect(scope.showEnrollment).not.toBe(true);
        });

        function mockAllItemsRequired() {
            jest.spyOn(user, 'recordsIndicateUserShouldUploadIdentificationDocument', 'get').mockReturnValue(true);
            jest.spyOn(user, 'careerProfileIndicatesUserShouldProvideTranscripts', 'get').mockReturnValue(true);
            jest.spyOn(
                user,
                'careerProfileIndicatesUserShouldUploadEnglishLanguageProficiencyDocuments',
                'get',
            ).mockReturnValue(true);
            jest.spyOn(user, 'missingAddress', 'get').mockReturnValue(true);
            user.identity_verified = false;
            user.transcripts_verified = false;
            user.english_language_proficiency_documents_approved = false;
            user.career_profile.last_calculated_complete_percentage = 99;
        }
    });

    describe('enrollmentSidebarTodos', () => {
        let todo;

        function setupUserForEnrollment() {
            user.relevant_cohort = relevantCohort;
            user.career_profile = CareerProfile.fixtures.getInstance();
        }

        describe('todo_enrollment_agreement', () => {
            beforeEach(() => {
                setupUserForEnrollment();
                setupStudentDashboardCall();
                render();
                todo = scope.enrollmentSidebarTodos[0];
            });

            it('should be configured with the appropriate keys', () => {
                expect(todo).toEqual({
                    name: 'todo_enrollment_agreement',
                    localeKey: 'sign_enrollment_agreement',
                    isVisible: expect.any(Function),
                    isDisabled: expect.any(Function),
                    onClick: expect.any(Function),
                });
            });

            describe('isVisible callback', () => {
                it('should be true if hasSignedEnrollmentAgreement', () => {
                    const spy = jest
                        .spyOn(scope.currentUser.lastCohortApplication, 'hasSignedEnrollmentAgreement', 'get')
                        .mockReturnValue(false);
                    jest.spyOn(
                        scope.currentUser.lastCohortApplication,
                        'hasActiveEnrollmentAgreementSigningLink',
                        'get',
                    ).mockReturnValue(false); // prevent false positive
                    expect(todo.isVisible()).toBe(false);
                    spy.mockReturnValue(true);
                    expect(todo.isVisible()).toBe(true);
                });

                it('should be true if hasActiveEnrollmentAgreementSigningLink', () => {
                    const spy = jest
                        .spyOn(
                            scope.currentUser.lastCohortApplication,
                            'hasActiveEnrollmentAgreementSigningLink',
                            'get',
                        )
                        .mockReturnValue(false);
                    jest.spyOn(
                        scope.currentUser.lastCohortApplication,
                        'hasSignedEnrollmentAgreement',
                        'get',
                    ).mockReturnValue(false); // prevent false positive
                    expect(todo.isVisible()).toBe(false);
                    spy.mockReturnValue(true);
                    expect(todo.isVisible()).toBe(true);
                });
            });

            describe('isDisabled callback', () => {
                it('should be true if hasSignedEnrollmentAgreement', () => {
                    const spy = jest
                        .spyOn(scope.currentUser.lastCohortApplication, 'hasSignedEnrollmentAgreement', 'get')
                        .mockReturnValue(false);
                    expect(todo.isDisabled()).toBe(false);
                    spy.mockReturnValue(true);
                    expect(todo.isDisabled()).toBe(true);
                });
            });

            describe('onClick callback', () => {
                it('should loadUrl for activeEnrollmentAgreementSigningLink in a new tab', () => {
                    jest.spyOn(scope, 'loadUrl').mockImplementation(() => {});
                    jest.spyOn(
                        scope.currentUser.lastCohortApplication,
                        'activeEnrollmentAgreementSigningLink',
                        'get',
                    ).mockReturnValue('some_url');
                    todo.onClick();
                    expect(scope.loadUrl).toHaveBeenCalledWith('some_url', '_blank');
                });
            });
        });

        describe('todo_id', () => {
            beforeEach(() => {
                setupUserForEnrollment();
                setupStudentDashboardCall();
                render();
                todo = scope.enrollmentSidebarTodos[1];
            });

            it('should be configured with the appropriate keys', () => {
                expect(todo).toEqual({
                    name: 'todo_id',
                    localeKey: 'upload_your_id',
                    isVisible: expect.any(Function),
                    isDisabled: expect.any(Function),
                    inReview: expect.any(Function),
                    onClick: expect.any(Function),
                });
            });

            describe('isVisible callback', () => {
                it('should be true if recordsIndicateUserShouldUploadIdentificationDocument', () => {
                    const spy = jest
                        .spyOn(scope.currentUser, 'recordsIndicateUserShouldUploadIdentificationDocument', 'get')
                        .mockReturnValue(false);
                    expect(todo.isVisible()).toBe(false);
                    spy.mockReturnValue(true);
                    expect(todo.isVisible()).toBe(true);
                });
            });

            describe('isDisabled callback', () => {
                it('should be true if identity_verified', () => {
                    scope.currentUser.identity_verified = false;
                    expect(todo.isDisabled()).toBe(false);
                    scope.currentUser.identity_verified = true;
                    expect(todo.isDisabled()).toBe(true);
                });
            });

            describe('inReview callback', () => {
                it('should be true if !missingIdentificationDocument and !identity_verified', () => {
                    const spy = jest
                        .spyOn(scope.currentUser, 'missingIdentificationDocument', 'get')
                        .mockReturnValue(false);
                    scope.currentUser.identity_verified = false;
                    expect(todo.inReview()).toBe(true);

                    scope.currentUser.identity_verified = true;
                    expect(todo.inReview()).toBe(false);

                    scope.currentUser.identity_verified = false;
                    spy.mockReturnValue(true);
                    expect(todo.inReview()).toBe(false);
                });
            });

            describe('onClick callback', () => {
                it('should loadRoute to /settings/document', () => {
                    jest.spyOn(scope, 'loadRoute').mockImplementation(() => {});
                    todo.onClick();
                    expect(scope.loadRoute).toHaveBeenCalledWith('/settings/documents');
                });
            });
        });

        describe('todo_transcripts', () => {
            beforeEach(() => {
                setupUserForEnrollment();
                setupStudentDashboardCall();
                render();
                todo = scope.enrollmentSidebarTodos[2];
            });

            it('should be configured with the appropriate keys', () => {
                expect(todo).toEqual({
                    name: 'todo_transcripts',
                    localeKey: 'send_us_your_transcripts',
                    isVisible: expect.any(Function),
                    isDisabled: expect.any(Function),
                    inReview: expect.any(Function),
                    onClick: expect.any(Function),
                    subTodosConfig: {
                        isVisible: expect.any(Function),
                        getSubTodos: expect.any(Function),
                        isDisabled: expect.any(Function),
                        inReview: expect.any(Function),
                        getSubTodoText: expect.any(Function),
                    },
                });
            });

            describe('isVisible callback', () => {
                it('should be true if careerProfileIndicatesUserShouldProvideTranscripts', () => {
                    const spy = jest
                        .spyOn(scope.currentUser, 'careerProfileIndicatesUserShouldProvideTranscripts', 'get')
                        .mockReturnValue(false);
                    expect(todo.isVisible()).toBe(false);
                    spy.mockReturnValue(true);
                    expect(todo.isVisible()).toBe(true);
                });
            });

            describe('isDisabled callback', () => {
                it('should be true if transcripts_verified', () => {
                    scope.currentUser.transcripts_verified = false;
                    expect(todo.isDisabled()).toBe(false);
                    scope.currentUser.transcripts_verified = true;
                    expect(todo.isDisabled()).toBe(true);
                });
            });

            describe('inReview callback', () => {
                it('should be true if hasUploadedTranscripts, !missingTranscripts, and !transcripts_verified', () => {
                    const hasUploadedTranscriptsSpy = jest
                        .spyOn(scope.currentUser, 'hasUploadedTranscripts', 'get')
                        .mockReturnValue(true);
                    const missingTranscriptsSpy = jest
                        .spyOn(scope.currentUser, 'missingTranscripts', 'get')
                        .mockReturnValue(false);
                    scope.currentUser.transcripts_verified = false;
                    expect(todo.inReview()).toBe(true);

                    hasUploadedTranscriptsSpy.mockReturnValue(false);
                    expect(todo.inReview()).toBe(false);

                    hasUploadedTranscriptsSpy.mockReturnValue(true);
                    missingTranscriptsSpy.mockReturnValue(true);
                    expect(todo.inReview()).toBe(false);

                    missingTranscriptsSpy.mockReturnValue(false);
                    scope.currentUser.transcripts_verified = true;
                    expect(todo.inReview()).toBe(false);
                });
            });

            describe('onClick callback', () => {
                it('should loadUrl to support article in a new tab', () => {
                    jest.spyOn(scope, 'loadUrl').mockImplementation(() => {});
                    todo.onClick();
                    expect(scope.loadUrl).toHaveBeenCalledWith(
                        '/help/article/54-am-i-required-to-provide-official-documents-transcript-diploma-etc',
                        '_blank',
                    );
                });
            });

            describe('subTodosConfig', () => {
                describe('isVisible callback', () => {
                    it('should be true if careerProfileIndicatesUserShouldProvideTranscripts', () => {
                        const provideTranscriptsSpy = jest
                            .spyOn(scope.currentUser, 'careerProfileIndicatesUserShouldProvideTranscripts', 'get')
                            .mockReturnValue(true);
                        expect(todo.subTodosConfig.isVisible()).toBe(true);

                        provideTranscriptsSpy.mockReturnValue(false);
                        expect(todo.subTodosConfig.isVisible()).toBe(false);
                    });
                });

                describe('getSubTodos callback', () => {
                    it('should be return the educationExperiencesIndicatingTranscriptRequired', () => {
                        const mockEducaitonExperience = {};
                        const spy = jest
                            .spyOn(
                                scope.currentUser.career_profile,
                                'educationExperiencesIndicatingTranscriptRequired',
                                'get',
                            )
                            .mockReturnValue([mockEducaitonExperience]);
                        const returnValue = todo.subTodosConfig.getSubTodos();
                        expect(spy).toHaveBeenCalled();
                        expect(returnValue).toEqual([mockEducaitonExperience]);
                    });
                });

                describe('isDisabled callback', () => {
                    it('should be true if transcriptApprovedOrWaived', () => {
                        const mockEducationExperience = {
                            transcriptApprovedOrWaived: true,
                        };
                        scope.currentUser.transcripts_verified = false; // prevent false positive
                        expect(todo.subTodosConfig.isDisabled(mockEducationExperience)).toBe(true);

                        mockEducationExperience.transcriptApprovedOrWaived = false;
                        expect(todo.subTodosConfig.isDisabled(mockEducationExperience)).toBe(false);
                    });

                    it('should be true if transcripts_verified', () => {
                        const mockEducationExperience = {
                            transcriptApprovedOrWaived: false, // prevent false positive
                        };
                        scope.currentUser.transcripts_verified = true;
                        expect(todo.subTodosConfig.isDisabled(mockEducationExperience)).toBe(true);

                        scope.currentUser.transcripts_verified = false;
                        expect(todo.subTodosConfig.isDisabled(mockEducationExperience)).toBe(false);
                    });
                });

                describe('inReview callback', () => {
                    it('should be true if transcriptInReview for passed in education experience and !transcripts_verified', () => {
                        const mockEducationExperience = {
                            transcriptInReview: true,
                        };
                        scope.currentUser.transcripts_verified = false;
                        expect(todo.subTodosConfig.inReview(mockEducationExperience)).toBe(true);

                        mockEducationExperience.transcriptInReview = false;
                        expect(todo.subTodosConfig.inReview(mockEducationExperience)).toBe(false);

                        mockEducationExperience.transcriptInReview = true;
                        scope.currentUser.transcripts_verified = true;
                        expect(todo.subTodosConfig.inReview(mockEducationExperience)).toBe(false);
                    });
                });

                describe('getSubTodoText', () => {
                    it('should return the degreeAndOrgNameWithTranscriptRequirementString for the passed in education experience', () => {
                        const mockEducationExperience = {
                            degreeAndOrgNameWithTranscriptRequirementString: 'Some degree and org name',
                        };
                        expect(todo.subTodosConfig.getSubTodoText(mockEducationExperience)).toEqual(
                            'Some degree and org name',
                        );
                    });
                });
            });
        });

        describe('todo_english_language', () => {
            beforeEach(() => {
                setupUserForEnrollment();
                setupStudentDashboardCall();
                render();
                todo = scope.enrollmentSidebarTodos[3];
            });

            it('should be configured with the appropriate keys', () => {
                expect(todo).toEqual({
                    name: 'todo_english_language',
                    localeKey: 'upload_english_language_documents',
                    isVisible: expect.any(Function),
                    isDisabled: expect.any(Function),
                    inReview: expect.any(Function),
                    onClick: expect.any(Function),
                });
            });

            describe('isVisible callback', () => {
                it('should be true if careerProfileIndicatesUserShouldUploadEnglishLanguageProficiencyDocuments', () => {
                    const spy = jest
                        .spyOn(
                            scope.currentUser,
                            'careerProfileIndicatesUserShouldUploadEnglishLanguageProficiencyDocuments',
                            'get',
                        )
                        .mockReturnValue(false);
                    expect(todo.isVisible()).toBe(false);
                    spy.mockReturnValue(true);
                    expect(todo.isVisible()).toBe(true);
                });
            });

            describe('isDisabled callback', () => {
                it('should be true if english_language_proficiency_documents_approved', () => {
                    scope.currentUser.english_language_proficiency_documents_approved = false;
                    expect(todo.isDisabled()).toBe(false);
                    scope.currentUser.english_language_proficiency_documents_approved = true;
                    expect(todo.isDisabled()).toBe(true);
                });
            });

            describe('inReview callback', () => {
                it('should be true if !missingEnglishLanguageProficiencyDocuments and !english_language_proficiency_documents_approved', () => {
                    const spy = jest
                        .spyOn(scope.currentUser, 'missingEnglishLanguageProficiencyDocuments', 'get')
                        .mockReturnValue(false);
                    scope.currentUser.english_language_proficiency_documents_approved = false;
                    expect(todo.inReview()).toBe(true);

                    scope.currentUser.english_language_proficiency_documents_approved = true;
                    expect(todo.inReview()).toBe(false);

                    scope.currentUser.english_language_proficiency_documents_approved = false;
                    spy.mockReturnValue(true);
                    expect(todo.inReview()).toBe(false);
                });
            });

            describe('onClick callback', () => {
                it('should loadRoute to /settings/document', () => {
                    jest.spyOn(scope, 'loadRoute').mockImplementation(() => {});
                    todo.onClick();
                    expect(scope.loadRoute).toHaveBeenCalledWith('/settings/documents');
                });
            });
        });

        describe('todo_id_verify', () => {
            beforeEach(() => {
                setupUserForEnrollment();
                setupStudentDashboardCall();
                render();
                todo = scope.enrollmentSidebarTodos[4];
            });

            it('should be configured with the appropriate keys', () => {
                expect(todo).toEqual({
                    name: 'todo_id_verify',
                    localeKey: 'verify_your_identity',
                    isVisible: expect.any(Function),
                    isDisabled: expect.any(Function),
                    onClick: expect.any(Function),
                });
            });

            describe('isVisible callback', () => {
                it('should be true if activeIdVerificationPeriod', () => {
                    const spy = jest
                        .spyOn(scope.currentUser.relevant_cohort, 'activeIdVerificationPeriod', 'get')
                        .mockReturnValue(false);
                    expect(todo.isVisible()).toBe(false);
                    spy.mockReturnValue(true);
                    expect(todo.isVisible()).toBe(true);
                });
            });

            describe('isDisabled callback', () => {
                it('should be true if unverifiedForCurrentIdVerificationPeriod', () => {
                    const spy = jest
                        .spyOn(scope.currentUser, 'unverifiedForCurrentIdVerificationPeriod', 'get')
                        .mockReturnValue(true);
                    expect(todo.isDisabled()).toBe(false);
                    spy.mockReturnValue(false);
                    expect(todo.isDisabled()).toBe(true);
                });
            });

            describe('onClick callback', () => {
                it('should launchVerificationModal', () => {
                    const UserIdVerificationViewModel = $injector.get('UserIdVerificationViewModel');
                    jest.spyOn(
                        UserIdVerificationViewModel.prototype,
                        'launchVerificationModal',
                    ).mockImplementation(() => {});
                    todo.onClick();
                    expect(UserIdVerificationViewModel.prototype.launchVerificationModal).toHaveBeenCalled();
                });
            });
        });

        describe('todo_address', () => {
            beforeEach(() => {
                setupUserForEnrollment();
                setupStudentDashboardCall();
                render();
                todo = scope.enrollmentSidebarTodos[5];
            });

            it('should be configured with the appropriate keys', () => {
                expect(todo).toEqual({
                    name: 'todo_address',
                    localeKey: 'provide_mailing_address',
                    isVisible: expect.any(Function),
                    isDisabled: expect.any(Function),
                    onClick: expect.any(Function),
                });
            });

            describe('isVisible callback', () => {
                it('should be true', () => {
                    expect(todo.isVisible()).toBe(true);
                });
            });

            describe('isDisabled callback', () => {
                it('should be true if !missingAddress', () => {
                    const spy = jest.spyOn(scope.currentUser, 'missingAddress', 'get').mockReturnValue(true);
                    expect(todo.isDisabled()).toBe(false);
                    spy.mockReturnValue(false);
                    expect(todo.isDisabled()).toBe(true);
                });
            });

            describe('onClick callback', () => {
                it('should loadRoute to /settings/document', () => {
                    jest.spyOn(scope, 'loadRoute').mockImplementation(() => {});
                    todo.onClick();
                    expect(scope.loadRoute).toHaveBeenCalledWith('/settings/documents');
                });
            });
        });

        describe('todo_complete_student_profile', () => {
            function setupEditCareerProfileHelperAndProfileCompletionHelper() {
                const EditCareerProfileHelper = $injector.get('EditCareerProfileHelper');
                const ProfileCompletionHelper = $injector.get('ProfileCompletionHelper');
                const mockSteps = [
                    {
                        stepName: 'basic_info',
                    },
                    {
                        stepName: 'more_about_you',
                    },
                    {
                        stepName: 'career_preferences',
                    },
                    {
                        stepName: 'personal_summary',
                    },
                ];
                const mockStepsProgressMap = {
                    basic_info: 'complete',
                    more_about_you: 'incomplete',
                    career_preferences: 'incomplete',
                    personal_summary: 'complete',
                };
                jest.spyOn(EditCareerProfileHelper, 'getStepsForCareersForm').mockReturnValue(mockSteps);
                jest.spyOn(ProfileCompletionHelper.prototype, 'getStepsProgressMap').mockReturnValue(
                    mockStepsProgressMap,
                );
            }

            it('should be configured with the appropriate keys', () => {
                setupEditCareerProfileHelperAndProfileCompletionHelper();
                setupUserForEnrollment();
                setupStudentDashboardCall();
                render();
                todo = scope.enrollmentSidebarTodos[6];
                expect(todo).toEqual({
                    name: 'todo_complete_student_profile',
                    localeKey: 'complete_student_profile',
                    isVisible: expect.any(Function),
                    isDisabled: expect.any(Function),
                    onClick: expect.any(Function),
                });
            });

            describe('isVisible callback', () => {
                beforeEach(() => {
                    setupEditCareerProfileHelperAndProfileCompletionHelper();
                    setupUserForEnrollment();
                    setupStudentDashboardCall();
                    render();
                    todo = scope.enrollmentSidebarTodos[6];
                });

                it('should be true', () => {
                    expect(todo.isVisible()).toBe(true);
                });
            });

            describe('isDisabled callback', () => {
                beforeEach(() => {
                    setupEditCareerProfileHelperAndProfileCompletionHelper();
                    setupUserForEnrollment();
                    setupStudentDashboardCall();
                    render();
                    todo = scope.enrollmentSidebarTodos[6];
                });

                it("should be true if last_calculated_complete_percentage on the user's career_profile equals 100", () => {
                    scope.currentUser.career_profile.last_calculated_complete_percentage = 99;
                    expect(todo.isDisabled()).toBe(false);
                    scope.currentUser.career_profile.last_calculated_complete_percentage = 100;
                    expect(todo.isDisabled()).toBe(true);
                });
            });

            describe('onClick callback', () => {
                it('should loadRoute to the first incomplete form step page in /settings/my-profile if user has an incomplete career profile', () => {
                    setupEditCareerProfileHelperAndProfileCompletionHelper();
                    setupUserForEnrollment();
                    setupStudentDashboardCall();
                    user.career_profile.last_calculated_complete_percentage = 99;
                    render();
                    todo = scope.enrollmentSidebarTodos[6];
                    // The logic for determing the first incomplete form step page is triggered
                    // outside and before the onClick handler for performance reasons.
                    jest.spyOn(scope, 'loadRoute').mockImplementation(() => {});
                    todo.onClick();
                    // page=2 because the more_about_you step is the first incomplete step and the page param is 1-indexed
                    expect(scope.loadRoute).toHaveBeenCalledWith('/settings/my-profile?page=2');
                });
            });
        });
    });

    describe('onlyNeedsToCompleteCareerProfileForEnrollment', () => {
        beforeEach(() => {
            user.career_profile = CareerProfile.fixtures.getInstance();
            setupStudentDashboardCall();
            render();
            mockPropertiesSoThatUserOnlyNeedsToCompleteCareerProfileForEnrollment();
        });

        it('should return false if !showEnrollment', () => {
            jest.spyOn(scope, 'showEnrollment', 'get').mockReturnValue(false);
            expect(scope.onlyNeedsToCompleteCareerProfileForEnrollment).toBe(false);
        });

        describe('when showEnrollment', () => {
            it('should return false if "complete career profile" todo is complete', () => {
                const completeCareerProfileEnrollmentTodo = _.find(
                    scope.enrollmentSidebarTodos,
                    todo => todo.name === 'todo_complete_student_profile',
                );
                jest.spyOn(completeCareerProfileEnrollmentTodo, 'isVisible').mockReturnValue(false);
                jest.spyOn(completeCareerProfileEnrollmentTodo, 'isDisabled').mockReturnValue(true);
                expect(scope.onlyNeedsToCompleteCareerProfileForEnrollment).toBe(false);
            });

            it('should return false if another enrolllment todo is incomplete', () => {
                const someOtherEnrollmentTodo = _.find(
                    scope.enrollmentSidebarTodos,
                    todo => todo.name !== 'todo_complete_student_profile',
                );
                jest.spyOn(someOtherEnrollmentTodo, 'isVisible').mockReturnValue(true);
                jest.spyOn(someOtherEnrollmentTodo, 'isDisabled').mockReturnValue(false);
                expect(scope.onlyNeedsToCompleteCareerProfileForEnrollment).toBe(false);
            });

            it('should return true if user only needs to complete their career profile to finish enrollment', () => {
                expect(scope.onlyNeedsToCompleteCareerProfileForEnrollment).toBe(true);
            });
        });

        function mockPropertiesSoThatUserOnlyNeedsToCompleteCareerProfileForEnrollment() {
            jest.spyOn(scope, 'showEnrollment', 'get').mockReturnValue(true);

            // get the complete career profile todo and mock to it be incomplete
            const completeCareerProfileEnrollmentTodo = _.find(
                scope.enrollmentSidebarTodos,
                todo => todo.name === 'todo_complete_student_profile',
            );
            jest.spyOn(completeCareerProfileEnrollmentTodo, 'isVisible').mockReturnValue(true);
            jest.spyOn(completeCareerProfileEnrollmentTodo, 'isDisabled').mockReturnValue(false);

            // mock the other enrollment todos to be completed so that the user onlyNeedsToCompleteCareerProfileForEnrollment
            _.each(scope.enrollmentSidebarTodos, todo => {
                if (todo.name !== 'todo_complete_student_profile') {
                    jest.spyOn(todo, 'isVisible').mockReturnValue(false);
                    jest.spyOn(todo, 'isDisabled').mockReturnValue(true);
                }
            });
        }
    });

    describe('enrollmentSidebarPlacement', () => {
        it("should return 'below' if onlyNeedsToCompleteCareerProfileForEnrollment", () => {
            setupStudentDashboardCall();
            render();
            jest.spyOn(scope, 'onlyNeedsToCompleteCareerProfileForEnrollment', 'get').mockReturnValue(false);
            expect(scope.enrollmentSidebarPlacement).not.toEqual('below');
            jest.spyOn(scope, 'onlyNeedsToCompleteCareerProfileForEnrollment', 'get').mockReturnValue(true);
            expect(scope.enrollmentSidebarPlacement).toEqual('below');
        });

        it("should return 'above' if !onlyNeedsToCompleteCareerProfileForEnrollment", () => {
            setupStudentDashboardCall();
            render();
            jest.spyOn(scope, 'onlyNeedsToCompleteCareerProfileForEnrollment', 'get').mockReturnValue(true);
            expect(scope.enrollmentSidebarPlacement).not.toEqual('above');
            jest.spyOn(scope, 'onlyNeedsToCompleteCareerProfileForEnrollment', 'get').mockReturnValue(false);
            expect(scope.enrollmentSidebarPlacement).toEqual('above');
        });
    });

    describe('welcome to dashboard modal', () => {
        let hasExternalInstitutionSpy = {};
        let isDemoSpy = {};
        let hasEverAppliedSpy = {};

        beforeEach(() => {
            jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
            jest.spyOn(user, 'roleName').mockImplementation(() => {});
            hasExternalInstitutionSpy = jest.spyOn(user, 'hasExternalInstitution', 'get');
            isDemoSpy = jest.spyOn(user, 'isDemo', 'get');
            hasEverAppliedSpy = jest.spyOn(user, 'hasEverApplied', 'get');
        });

        it('should show when all conditions are met', () => {
            user.roleName.mockReturnValue('learner');
            hasExternalInstitutionSpy.mockReturnValue(false);
            isDemoSpy.mockReturnValue(false);
            hasEverAppliedSpy.mockReturnValue(false);

            setupStudentDashboardCall();
            render();
            expect(DialogModal.alert).toHaveBeenCalled();
        });

        it('should not show modal if user is NOT a learner', () => {
            user.roleName.mockReturnValue('editor'); // changed
            hasExternalInstitutionSpy.mockReturnValue(false);
            isDemoSpy.mockReturnValue(false);
            hasEverAppliedSpy.mockReturnValue(false);

            setupStudentDashboardCall();
            render();
            expect(DialogModal.alert).not.toHaveBeenCalled();
        });

        it('should not show modal if user has external institution', () => {
            user.roleName.mockReturnValue('learner');
            hasExternalInstitutionSpy.mockReturnValue(true); // changed
            isDemoSpy.mockReturnValue(false);
            hasEverAppliedSpy.mockReturnValue(false);

            setupStudentDashboardCall();
            render();
            expect(DialogModal.alert).not.toHaveBeenCalled();
        });

        it('should not show modal if user is demo', () => {
            user.roleName.mockReturnValue('learner');
            hasExternalInstitutionSpy.mockReturnValue(false);
            isDemoSpy.mockReturnValue(true); // changed
            hasEverAppliedSpy.mockReturnValue(false);

            setupStudentDashboardCall();
            render();
            expect(DialogModal.alert).not.toHaveBeenCalled();
        });

        it('should not show modal if user has applied', () => {
            user.roleName.mockReturnValue('learner');
            hasExternalInstitutionSpy.mockReturnValue(false);
            isDemoSpy.mockReturnValue(false);
            hasEverAppliedSpy.mockReturnValue(true); // changed

            setupStudentDashboardCall();
            render();
            expect(DialogModal.alert).not.toHaveBeenCalled();
        });
    });

    describe('OfflineMode', () => {
        it('should show offline mode learning box', () => {
            offlineModeManager.inOfflineMode = true;

            // We don't use setupStudentDashboardCall because we need to
            // make an assertion before the call returns, so we need an unresolved promise.
            const returnValue = getStudentDashboardCallReturnValue();
            let resolve;
            const promise = $q(_resolve => {
                resolve = _resolve.bind(null, returnValue);
            });
            jest.spyOn(LearnerContentCache, 'ensureStudentDashboard').mockReturnValue(promise);
            render();

            // assert that the learning box mode is set to offline_mode
            // before ensureStudentDashboard returns
            expect(scope.learningBoxMode.mode).toEqual('offline_mode');
            resolve();
            $timeout.flush();

            // assert that the learning box mode is set to offline_mode
            // after ensureStudentDashboard returns
            expect(scope.learningBoxMode.mode).toEqual('offline_mode');
        });

        it('should hide student-dashboard-program-box', () => {
            setupStudentDashboardCall();
            $rootScope.currentUser.relevant_cohort = relevantCohort;
            render();
            SpecHelper.expectElement(elem, 'student-dashboard-program-box'); // sanity check
            offlineModeManager.inOfflineMode = true;
            scope.$digest();
            SpecHelper.expectNoElement(elem, 'student-dashboard-program-box');
        });

        it('should hide student-dashboard-recent-courses', () => {
            setupStudentDashboardCall();
            $rootScope.currentUser.relevant_cohort = relevantCohort;
            render();
            SpecHelper.expectElement(elem, 'student-dashboard-recent-courses'); // sanity check
            offlineModeManager.inOfflineMode = true;
            scope.$digest();
            SpecHelper.expectNoElement(elem, 'student-dashboard-recent-courses');
        });

        it('should hide mobile student-dashboard-program-box', () => {
            SpecHelper.stubDirective('frontRoyalFooterContent');
            setupStudentDashboardCall();
            render();
            scope.showProgramBox = true;
            scope._hideMobileProgramBox = false;
            scope.showSchedule = false;
            scope.$digest();
            SpecHelper.expectElement(elem, 'front-royal-footer-content student-dashboard-program-box'); // sanity check
            offlineModeManager.inOfflineMode = true;
            scope.$digest();
            SpecHelper.expectNoElement(elem, 'front-royal-footer-content student-dashboard-program-box');
        });

        it('should update learningBoxMode when offlineMode changes', () => {
            offlineModeManager.inOfflineMode = false;
            setupStudentDashboardCall();
            $rootScope.currentUser.relevant_cohort = relevantCohort;
            render();
            expect(scope.learningBoxMode.mode).not.toEqual('offline_mode');
            offlineModeManager.inOfflineMode = true;
            scope.$digest();
            expect(scope.learningBoxMode.mode).toEqual('offline_mode');
        });
    });
});
