import 'AngularSpecHelper';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/lessons';
import 'Lessons/angularModule/spec/_mock/fixtures/streams';
import setSpecLocales from 'Translation/setSpecLocales';
import streamLibraryLocales from 'Lessons/locales/lessons/stream/stream_library-en.json';
import hasToggleableDisplayModeLocales from 'Lessons/locales/lessons/stream/has_toggleable_display_mode-en.json';
import toggleableCourseListButtonsLocales from 'Lessons/locales/lessons/stream/toggleable_course_list_buttons-en.json';
import feedbackSidebarLocales from 'Feedback/locales/feedback/feedback_sidebar-en.json';
import streamLinkBoxLocales from 'Lessons/locales/lessons/stream/stream_link_box-en.json';

setSpecLocales(streamLibraryLocales, toggleableCourseListButtonsLocales, feedbackSidebarLocales, streamLinkBoxLocales);
setSpecLocales(hasToggleableDisplayModeLocales);

describe('Lessons.Stream.StreamLibraryDir', () => {
    let $injector;
    let streams;
    let Stream;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let $filter;
    let guid;
    let Cohort;
    let AppHeaderViewModel;
    let topics;
    let progressOptions;
    let EventLogger;
    let $route;
    let $location;
    let ClientStorage;
    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'SpecHelper');

        $route = {
            current: {},
        };
        angular.mock.module($provide => {
            $provide.value('$route', $route);
        });

        angular.mock.inject([
            '$injector',

            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                Stream = $injector.get('Lesson.Stream');
                guid = $injector.get('guid');
                $filter = $injector.get('$filter');
                AppHeaderViewModel = $injector.get('Navigation.AppHeader.AppHeaderViewModel');
                $injector.get('LessonProgressFixtures');
                EventLogger = $injector.get('EventLogger');
                ClientStorage = $injector.get('ClientStorage');
                $location = $injector.get('$location');
                Cohort = $injector.get('Cohort');

                $injector.get('CohortFixtures');

                jest.spyOn($location, 'search').mockImplementation(() => ({}));

                topics = [0, 1, 2].map(i => ({
                    id: guid.generate(),
                    name: `topic ${i}`,

                    locales: {
                        en: `topic ${i}`,
                    },
                }));

                progressOptions = [
                    {
                        id: 'not-started',
                        name: 'Not Started',
                    },
                    {
                        id: 'in-progress',
                        name: 'In Progress',
                    },
                    {
                        id: 'completed',
                        name: 'Completed',
                    },
                ];

                $injector.get('LessonFixtures');
                $injector.get('StreamFixtures');

                SpecHelper.stubConfig();
            },
        ]);

        SpecHelper.stubCurrentUser('learner');

        SpecHelper.stubEventLogging();
    });

    function setupStreams(justAdded, comingSoon, beta, justUpdated, elective) {
        justAdded = angular.isDefined(justAdded) ? justAdded : true;
        comingSoon = angular.isDefined(comingSoon) ? comingSoon : true;

        streams = [Stream.fixtures.getInstance(), Stream.fixtures.getInstance(), Stream.fixtures.getInstance()];

        streams[0].locale_pack.content_topics = [topics[0], topics[1]];
        streams[1].locale_pack.content_topics = [topics[1], topics[2]];

        streams[0].lesson_streams_progress = undefined;
        streams[0].coming_soon = comingSoon;
        streams[1].lesson_streams_progress.completed_at = undefined;
        streams[1].just_added = justAdded;
        streams[2].lesson_streams_progress.completed_at = new Date();
        streams[2].lesson_streams_progress.writeKey('complete', true);

        // late-additions to this helper method: optionally add streams for beta and justUpdated
        if (beta === true) {
            const betaStream = Stream.fixtures.getInstance();
            betaStream.beta = true;
            betaStream.lesson_streams_progress = undefined;
            streams.push(betaStream);
        }

        if (justUpdated === true) {
            const justUpdatedStream = Stream.fixtures.getInstance();
            justUpdatedStream.just_updated = true;
            justUpdatedStream.lesson_streams_progress = undefined;
            streams.push(justUpdatedStream);
        }

        if (elective === true) {
            $injector.get('$rootScope').currentUser.relevant_cohort = Cohort.fixtures.getInstance();
        }

        Stream.expect('index').returns(streams);
    }

    function render(flush) {
        renderer = SpecHelper.renderer();
        renderer.render('<stream-library></stream-library>');
        elem = renderer.elem;
        scope = elem.isolateScope();

        scope.$apply();
        if (flush !== false) {
            Stream.flush('index');
        }
        scope.$apply();
    }

    afterEach(() => {
        SpecHelper.cleanup();
        ClientStorage.removeItem('toggleableCourseListLibrary');
    });

    describe('header', () => {
        it('should set the text to Library', () => {
            setupStreams();

            jest.spyOn(AppHeaderViewModel, 'setTitleHTML').mockImplementation(() => {});
            render();

            expect(AppHeaderViewModel.setTitleHTML).toHaveBeenCalledWith('LIBRARY');
        });
    });

    describe('spinner', () => {
        it('should show until streams are loaded', () => {
            setupStreams();

            render(false);

            SpecHelper.expectElement(elem, 'front-royal-spinner');
            Stream.flush('index');
            SpecHelper.expectNoElement(elem, 'front-royal-spinner');
        });
    });

    describe('courses info display', () => {
        it('should populate streams on the scope', () => {
            setupStreams();

            render();

            expect(scope.streams).not.toBeUndefined();
            expect(scope.streams.map(s => s.id)).toEqual($filter('orderBy')(streams, 'title').map(s => s.id));
        });

        it('should display the header correctly', () => {
            setupStreams();

            render();
            SpecHelper.expectElementText(elem, '.hr-caption > span:eq(0)', 'COURSES', 0);
        });

        it('should display the correct number of courses', () => {
            setupStreams();

            render();

            SpecHelper.expectElement(elem, '.toggle-topic.active');
            SpecHelper.expectNoElement(elem, '.toggle-flat.active');

            // topic view expect 5 streams: 2 for topic 0, 2 for topic 1, 1 for 'other' (no topic)
            SpecHelper.expectElements(elem, '.topic-container stream-link-box', 5);

            // toggle flat view
            SpecHelper.click(elem, '.toggle-flat');
            SpecHelper.expectElements(elem, '.flat-container stream-link-box', streams.length);
        });

        it('should disable bookmarking if the user is a Blue Ocean Strategy user', () => {
            setupStreams();

            render();
            Object.defineProperty(scope, 'blueOceanUser', {
                value: true,
            });
            scope.$digest();

            SpecHelper.expectElement(elem, '.toggle-topic.active');
            SpecHelper.expectNoElement(elem, '.toggle-flat.active');

            // topic view expect 5 streams: 2 for topic 0, 2 for topic 1, 1 for 'other' (no topic)
            SpecHelper.expectElements(elem, '.topic-container stream-link-box', 5);

            // toggle flat view
            SpecHelper.click(elem, '.toggle-flat');
            const streamBoxes = SpecHelper.expectElements(elem, '.flat-container stream-link-box', streams.length);
            expect(streamBoxes.has('.bookmark-ribbon').length).toBe(0);
        });
    });

    describe('with no currentUser', () => {
        it('should set in_locale_or_en from client-side', () => {
            Stream.expect('index')
                .toBeCalledWith({
                    filters: {
                        published: true,
                        view_as: 'cohort:PROMOTED_DEGREE',
                        in_locale_or_en: 'en',
                        user_can_see: null,
                        in_users_locale_or_en: null,
                    },
                    summary: true,
                    include_progress: false,
                })
                .returns([]);
            SpecHelper.stubNoCurrentUser();
            render();
        });

        it('should work with SMARTER', () => {
            Stream.expect('index')
                .toBeCalledWith({
                    filters: {
                        published: true,
                        view_as: 'cohort:PROMOTED_DEGREE',
                        in_locale_or_en: 'en',
                        user_can_see: null,
                        in_users_locale_or_en: null,
                    },
                    summary: true,
                    include_progress: false,
                })
                .returns([]);
            SpecHelper.stubNoCurrentUser();
            render();
            SpecHelper.expectElements(elem, '.container');
        });
    });

    describe('filtering', () => {
        beforeEach(() => {
            setupStreams();

            render();
        });

        it('should support topic filtering', () => {
            const anyTopicButton = SpecHelper.expectElement(elem, '.select-filters .topic[data-id="any-topic"]');
            SpecHelper.expectElementHasClass(anyTopicButton, 'selected');

            SpecHelper.click(elem, '.toggle-topic');

            // clicking a button should select it
            const topicButton0 = SpecHelper.click(elem, `.select-filters .topic[data-id="${topics[0].locales.en}"]`);
            SpecHelper.expectElementHasClass(topicButton0, 'selected');
            SpecHelper.expectElementDoesNotHaveClass(anyTopicButton, 'selected');
            expect(idsOnScreen('.stream')).toEqual([streams[0].id]);
            SpecHelper.click(elem, '.toggle-flat');
            expect(idsOnScreen('.stream')).toEqual([streams[0].id]);

            SpecHelper.click(elem, '.toggle-topic');

            // clicking another button should result in two buttons selected
            const topicButton1 = SpecHelper.click(elem, `.select-filters .topic[data-id="${topics[1].locales.en}"]`);
            SpecHelper.expectElementHasClass(topicButton1, 'selected');
            SpecHelper.expectElementHasClass(topicButton0, 'selected');
            expect(idsOnScreen('.stream')).toEqual([streams[0].id, streams[0].id, streams[1].id]); // 2 topics for stream 0, 1 for stream 1
            SpecHelper.click(elem, '.toggle-flat');
            expect(idsOnScreen('.stream')).toEqual([streams[0].id, streams[1].id]);

            SpecHelper.click(elem, '.toggle-topic');

            // clicking the selected buttons should deselect them
            topicButton0.click();
            topicButton1.click();
            SpecHelper.expectElementDoesNotHaveClass(topicButton0, 'selected');
            SpecHelper.expectElementDoesNotHaveClass(topicButton1, 'selected');
            SpecHelper.expectElementHasClass(anyTopicButton, 'selected');
            expect(idsOnScreen('.stream')).toEqual([
                streams[2].id,
                streams[0].id,
                streams[0].id,
                streams[1].id,
                streams[1].id,
            ]); // no topic for stream 2, 2 topics for stream 0, 2 for stream 1
            SpecHelper.click(elem, '.toggle-flat');
            expect(idsOnScreen('.stream')).toEqual([streams[0].id, streams[1].id, streams[2].id]);

            SpecHelper.click(elem, '.toggle-topic');

            // click a button again to select one, and the clicking the all button should
            // deselect it
            topicButton1.click();
            SpecHelper.expectElementHasClass(topicButton1, 'selected');
            anyTopicButton.click();
            SpecHelper.expectElementHasClass(anyTopicButton, 'selected');
            SpecHelper.expectElementDoesNotHaveClass(topicButton1, 'selected');
            expect(idsOnScreen('.stream')).toEqual([
                streams[2].id,
                streams[0].id,
                streams[0].id,
                streams[1].id,
                streams[1].id,
            ]); // no topic for stream 2, 2 topics for stream 0, 2 for stream 1
            SpecHelper.click(elem, '.toggle-flat');
            expect(idsOnScreen('.stream')).toEqual([streams[0].id, streams[1].id, streams[2].id]);
        });

        it('should support progress filtering', () => {
            const anyProgressButton = SpecHelper.expectElement(
                elem,
                '.select-filters .progress-button[data-id="any-progress"]',
            );
            SpecHelper.expectElementHasClass(anyProgressButton, 'selected');

            // use flat view since it's easier to count stream occurrences
            SpecHelper.click(elem, '.toggle-flat');

            // clicking a button should select it
            const progressButton0 = SpecHelper.click(elem, '.select-filters .progress-button[data-id="not_started"]');
            SpecHelper.expectElementHasClass(progressButton0, 'selected');
            SpecHelper.expectElementDoesNotHaveClass(anyProgressButton, 'selected');
            expect(idsOnScreen('.stream')).toEqual([streams[0].id]);

            // clicking another button should result in two filters selected
            const progressButton1 = SpecHelper.click(elem, '.select-filters .progress-button[data-id="in_progress"]');
            SpecHelper.expectElementHasClass(progressButton1, 'selected');
            SpecHelper.expectElementHasClass(progressButton0, 'selected');
            expect(idsOnScreen('.stream')).toEqual([streams[0].id, streams[1].id]);

            // completed should work too
            const progressButton2 = SpecHelper.click(elem, '.select-filters .progress-button[data-id="completed"]');
            SpecHelper.expectElementHasClass(progressButton2, 'selected');
            SpecHelper.expectElementHasClass(progressButton1, 'selected');
            expect(idsOnScreen('.stream').sort()).toEqual([streams[0].id, streams[1].id, streams[2].id].sort());

            // clicking the selected buttons should deselect them
            progressButton2.click();
            progressButton1.click();
            progressButton0.click();
            SpecHelper.expectElementDoesNotHaveClass(progressButton2, 'selected');
            SpecHelper.expectElementDoesNotHaveClass(progressButton1, 'selected');
            SpecHelper.expectElementHasClass(anyProgressButton, 'selected');
            expect(idsOnScreen('.stream')).toEqual([streams[0].id, streams[1].id, streams[2].id]);

            // click a button again to select one, and the clicking the all button should deselect it
            progressButton1.click();
            SpecHelper.expectElementHasClass(progressButton1, 'selected');

            anyProgressButton.click();
            SpecHelper.expectElementHasClass(anyProgressButton, 'selected');
            SpecHelper.expectElementDoesNotHaveClass(progressButton1, 'selected');
            expect(idsOnScreen('.stream')).toEqual([streams[0].id, streams[1].id, streams[2].id]);
        });

        it('should support status filtering', () => {
            setupStreams(true, true, true, true, true);

            render();

            const anyStatusButton = SpecHelper.expectElement(
                elem,
                '.select-filters .status-button[data-id="any_status"]',
            );
            SpecHelper.expectElementHasClass(anyStatusButton, 'selected');

            // use flat view since it's easier to count stream occurrences
            SpecHelper.click(elem, '.toggle-flat');

            // clicking new courses should select it
            const newCourseButton = SpecHelper.click(elem, '.select-filters .status-button[data-id="new_courses"]');
            SpecHelper.expectElementHasClass(newCourseButton, 'selected');
            SpecHelper.expectElementDoesNotHaveClass(anyStatusButton, 'selected');

            // clicking coming soon should also work
            const comingSoonButton = SpecHelper.click(elem, '.select-filters .status-button[data-id="coming_soon"]');
            SpecHelper.expectElementHasClass(comingSoonButton, 'selected');
            SpecHelper.expectElementDoesNotHaveClass(newCourseButton, 'selected');

            // clicking beta should also work
            const betaButton = SpecHelper.click(elem, '.select-filters .status-button[data-id="beta"]');
            SpecHelper.expectElementHasClass(betaButton, 'selected');
            SpecHelper.expectElementDoesNotHaveClass(comingSoonButton, 'selected');

            // clicking just updated should also work
            const justUpdatedButton = SpecHelper.click(elem, '.select-filters .status-button[data-id="updated"]');
            SpecHelper.expectElementHasClass(justUpdatedButton, 'selected');
            SpecHelper.expectElementDoesNotHaveClass(betaButton, 'selected');

            // clicking elective should also work
            const electiveButton = SpecHelper.click(elem, '.select-filters .status-button[data-id="elective"]');
            SpecHelper.expectElementHasClass(electiveButton, 'selected');
            SpecHelper.expectElementDoesNotHaveClass(justUpdatedButton, 'selected');

            // clicking the selected buttons should deselect them
            electiveButton.click();
            SpecHelper.expectElementDoesNotHaveClass(electiveButton, 'selected');
            SpecHelper.expectElementHasClass(anyStatusButton, 'selected');

            // click a button again to select one, and the clicking the all button should deselect it
            newCourseButton.click();
            SpecHelper.expectElementHasClass(newCourseButton, 'selected');
            anyStatusButton.click();
            SpecHelper.expectElementHasClass(anyStatusButton, 'selected');
            SpecHelper.expectElementDoesNotHaveClass(newCourseButton, 'selected');
        });

        it('should support filtering by progress and topic', () => {
            // use flat view since it's easier to count stream occurrences
            SpecHelper.click(elem, '.toggle-flat');

            SpecHelper.click(elem, `.select-filters .topic[data-id="${topics[1].locales.en}"]`);
            expect(idsOnScreen('.stream')).toEqual([streams[0].id, streams[1].id]);

            SpecHelper.click(elem, '.select-filters .progress-button[data-id="not_started"]');
            expect(idsOnScreen('.stream')).toEqual([streams[0].id]);

            SpecHelper.click(elem, `.select-filters .topic[data-id="${topics[2].locales.en}"]`);
            SpecHelper.click(elem, `.select-filters .topic[data-id="${topics[1].locales.en}"]`);
            expect(idsOnScreen('.stream')).toEqual([]);
        });

        it('should not display status filters if no coming_soon, just_added, beta, just_updated, or elective courses', () => {
            setupStreams(false, false, false, false);

            render();

            SpecHelper.expectNoElement(elem, '.select-filters .status-button');
        });

        it('should only display new courses status if no other statuses', () => {
            setupStreams(true, false, false, false);

            render();

            SpecHelper.expectElement(elem, '.select-filters .status-button[data-id="any_status"]');
            SpecHelper.expectElement(elem, '.select-filters .status-button[data-id="available"]');
            SpecHelper.expectElement(elem, '.select-filters .status-button[data-id="new_courses"]');
            SpecHelper.expectNoElement(elem, '.select-filters .status-button[data-id="coming_soon"]');
            SpecHelper.expectNoElement(elem, '.select-filters .status-button[data-id="beta"]');
            SpecHelper.expectNoElement(elem, '.select-filters .status-button[data-id="updated"]');
            SpecHelper.expectNoElement(elem, '.select-filters .status-button[data-id="elective"]');
        });

        it('should only display coming soon status if no other statuses', () => {
            setupStreams(false, true, false, false);

            render();

            SpecHelper.expectElement(elem, '.select-filters .status-button[data-id="any_status"]');
            SpecHelper.expectElement(elem, '.select-filters .status-button[data-id="available"]');
            SpecHelper.expectNoElement(elem, '.select-filters .status-button[data-id="new_courses"]');
            SpecHelper.expectElement(elem, '.select-filters .status-button[data-id="coming_soon"]');
            SpecHelper.expectNoElement(elem, '.select-filters .status-button[data-id="beta"]');
            SpecHelper.expectNoElement(elem, '.select-filters .status-button[data-id="updated"]');
            SpecHelper.expectNoElement(elem, '.select-filters .status-button[data-id="elective"]');
        });

        it('should only display beta status if no other statuses', () => {
            setupStreams(false, false, true, false);

            render();

            SpecHelper.expectElement(elem, '.select-filters .status-button[data-id="any_status"]');
            SpecHelper.expectElement(elem, '.select-filters .status-button[data-id="available"]');
            SpecHelper.expectNoElement(elem, '.select-filters .status-button[data-id="new_courses"]');
            SpecHelper.expectNoElement(elem, '.select-filters .status-button[data-id="coming_soon"]');
            SpecHelper.expectElement(elem, '.select-filters .status-button[data-id="beta"]');
            SpecHelper.expectNoElement(elem, '.select-filters .status-button[data-id="updated"]');
            SpecHelper.expectNoElement(elem, '.select-filters .status-button[data-id="elective"]');
        });

        it('should only display just updated status if no other statuses', () => {
            setupStreams(false, false, false, true);

            render();

            SpecHelper.expectElement(elem, '.select-filters .status-button[data-id="any_status"]');
            SpecHelper.expectElement(elem, '.select-filters .status-button[data-id="available"]');
            SpecHelper.expectNoElement(elem, '.select-filters .status-button[data-id="new_courses"]');
            SpecHelper.expectNoElement(elem, '.select-filters .status-button[data-id="coming_soon"]');
            SpecHelper.expectNoElement(elem, '.select-filters .status-button[data-id="beta"]');
            SpecHelper.expectElement(elem, '.select-filters .status-button[data-id="updated"]');
            SpecHelper.expectNoElement(elem, '.select-filters .status-button[data-id="elective"]');
        });

        it('should not display progress filters if the user is not authenticated', () => {
            $injector.get('$rootScope').currentUser = undefined;
            scope.$digest();
            SpecHelper.expectNoElement(elem, '.progress-buttons');
        });

        it('should show a message when no items are available for the filters', () => {
            SpecHelper.expectNoElement(elem, '.no-results-container');
            SpecHelper.click(elem, `.select-filters .topic[data-id="${topics[2].locales.en}"]`);
            SpecHelper.click(elem, '.select-filters .progress-button[data-id="completed"]');
            SpecHelper.expectElementText(
                elem,
                '.no-results-container > h1',
                'Sorry! No content found matching your filters.',
            );
        });

        it('should clear the filters and search when clear filters button is clicked', () => {
            // use flat view since it's easier to count stream occurrences
            SpecHelper.click(elem, '.toggle-flat');

            SpecHelper.expectNoElement(elem, '.no-results-container');
            SpecHelper.click(elem, `.select-filters .topic[data-id="${topics[2].locales.en}"]`);
            SpecHelper.click(elem, '.select-filters .progress-button[data-id="completed"]');
            SpecHelper.updateTextInput(elem, '[name="search-text"]:eq(0)', 'you will not find this');
            SpecHelper.click(elem, '.search-form button.search');

            SpecHelper.expectElementText(
                elem,
                '.no-results-container > h1',
                'Sorry! No content found matching your filters.',
            );
            SpecHelper.click(elem, '#clear-search-filters');

            expect(idsOnScreen('.stream')).toEqual(ids(streams, [0, 1, 2]));
        });

        it('should delay filtering until select filters form is saved in mobile case', () => {
            // use flat view since it's easier to count stream occurrences
            SpecHelper.click(elem, '.toggle-flat');

            expect(idsOnScreen('.stream')).toEqual(ids(streams, [0, 1, 2]));
            SpecHelper.click(elem, '[name="show-filter-select-on-mobile"]');
            SpecHelper.click(elem, `.select-filters .topic[data-id="${topics[0].locales.en}"]`);

            // the filter should not yet be applied, since the mobile form
            // is up
            expect(idsOnScreen('.stream')).toEqual(ids(streams, [0, 1, 2]));

            SpecHelper.click(elem, '[name="close-select-filters"]');
            // still not applied, since the close button was clicked
            expect(idsOnScreen('.stream')).toEqual(ids(streams, [0, 1, 2]));

            SpecHelper.click(elem, '[name="show-filter-select-on-mobile"]');
            SpecHelper.click(elem, `.select-filters .topic[data-id="${topics[0].locales.en}"]`);
            SpecHelper.click(elem, '[name="save-and-close-select-filters"]');

            // clicking save and close should apply the filter
            expect(idsOnScreen('.stream')).toEqual(ids(streams, [0]));
        });

        it('should respect the search form', () => {
            // use flat view since it's easier to count stream occurrences
            SpecHelper.click(elem, '.toggle-flat');

            assertSearch(streams[0], streams[0], 'title');
            assertSearch(streams[0], streams[0], 'description');
            assertSearch(streams[0], streams[0].locale_pack.content_topics[0].locales, 'en');
            assertSearch(streams[0], streams[0].chapters[0], 'title');
            assertSearch(streams[0], streams[0].lessons[0], 'title');
            assertSearch(streams[0], streams[0].lessons[0], 'description');
            assertSearch(streams[0], streams[0].lessons[0].key_terms, 0);
        });

        it('should rebuild topic filters when $rootScope.viewAs changes', () => {
            setupStreams();

            render();
            SpecHelper.expectElement(elem, `.select-filters .topic[data-id="${topics[0].locales.en}"]`); // sanity check

            const topic = streams[0].locale_pack.content_topics[0];
            const newTopicName = '--------';
            expect(topic.locales.en).not.toBe(newTopicName);
            topic.locales.en = newTopicName;

            const $rootScope = $injector.get('$rootScope');
            $rootScope.viewAs = 'xxxxx';

            Stream.expect('index').returns(streams);

            scope.$apply();
            Stream.flush('index');

            SpecHelper.expectElement(elem, `.select-filters .topic[data-id="${newTopicName}"]`); // sanity check
        });

        function assertSearch(stream, objToModify, property) {
            const selector = '.stream';
            const origVal = objToModify[property];
            objToModify[property] = 'LeapING, mARChing';
            scope.addFilters([stream]);

            SpecHelper.updateTextInput(elem, '[name="search-text"]:eq(0)', 'you will not find this');
            SpecHelper.click(elem, '.search-form button.search');
            expect(idsOnScreen(selector)).toEqual([]);

            SpecHelper.updateTextInput(elem, '[name="search-text"]:eq(0)', 'marCHED: leaPED');
            SpecHelper.click(elem, '.search-form button.search');
            expect(idsOnScreen(selector)).toEqual([stream.id]);

            SpecHelper.updateTextInput(elem, '[name="search-text"]:eq(0)', '');
            SpecHelper.click(elem, '.search-form button.search');
            expect(idsOnScreen(selector).length).toEqual(3);

            objToModify[property] = origVal;
            scope.addFilters([stream]);
        }
    });

    describe('toggle', () => {
        it('should default to topic if preference not available', () => {
            setupStreams();

            render();
            expect(scope.dashboardDisplayMode.key).toBe('topic');
        });

        it('should set to preference if available', () => {
            ClientStorage.setItem('toggleableCourseListLibrary', 'flat');
            setupStreams();

            render();
            expect(scope.dashboardDisplayMode.key).toBe('flat');
        });
    });

    describe('view as', () => {
        it('should set in_locale_or_en from client-side', () => {
            Stream.expect('index')
                .toBeCalledWith({
                    filters: {
                        published: true,
                        view_as: 'group:TEST VIEW AS GROUP',
                        in_locale_or_en: 'en',
                        user_can_see: null,
                        in_users_locale_or_en: null,
                    },
                    summary: true,
                    include_progress: true,
                })
                .returns([]);

            $injector.get('$rootScope').viewAs = 'group:TEST VIEW AS GROUP';

            render();
        });
    });

    describe('persisting searches', () => {
        function verifyFilters() {
            const anyProgressButton = SpecHelper.expectElement(
                elem,
                '.select-filters .progress-button[data-id="any-progress"]',
            );
            SpecHelper.expectElement(elem, '.select-filters .status-button[data-id="any_status"]');
            const anyTopicButton = SpecHelper.expectElement(elem, '.select-filters .topic[data-id="any-topic"]');

            // first topic button should be selected
            const topicButton0 = SpecHelper.expectElement(
                elem,
                `.select-filters .topic[data-id="${topics[0].locales.en}"]`,
            );
            SpecHelper.expectElementHasClass(topicButton0, 'selected');
            SpecHelper.expectElementDoesNotHaveClass(anyTopicButton, 'selected');

            // not started button should be selected
            const progressButton0 = SpecHelper.expectElement(
                elem,
                '.select-filters .progress-button[data-id="not_started"]',
            );
            SpecHelper.expectElementHasClass(progressButton0, 'selected');
            SpecHelper.expectElementDoesNotHaveClass(anyProgressButton, 'selected');

            // search text should be set to correct value
            expect(scope.searchText).toEqual('Stream');

            // defaults to fully inclusive, not just_added
            expect(scope.newCoursesFilter).toEqual(undefined);

            // defaults to fully inclusive, not coming_soon
            expect(scope.comingSoonFilter).toEqual(undefined);

            // defaults to fully inclusive, not beta
            expect(scope.betaFilter).toEqual(undefined);

            // defaults to fully inclusive, not just_updated
            expect(scope.updatedCoursesFilter).toEqual(undefined);

            // streams should be filtered
            SpecHelper.click(elem, '.toggle-topic');
            expect(idsOnScreen('.stream')).toEqual([streams[0].id]);
            SpecHelper.click(elem, '.toggle-flat');
            expect(idsOnScreen('.stream')).toEqual([streams[0].id]);
        }

        beforeEach(() => {
            setupStreams();
        });

        afterEach(() => {
            ClientStorage.setItem('librarySearchTopic', JSON.stringify(undefined));
            ClientStorage.setItem('librarySearchProgress', JSON.stringify(undefined));
            ClientStorage.setItem('librarySearchText', JSON.stringify(undefined));
            ClientStorage.setItem('librarySearchNewCourses', JSON.stringify(undefined));
            ClientStorage.setItem('librarySearchComingSoon', JSON.stringify(undefined));
            ClientStorage.setItem('librarySearchBeta', JSON.stringify(undefined));
            ClientStorage.setItem('librarySearchUpdated', JSON.stringify(undefined));
        });

        it('should default the filters and search based on query params', () => {
            $location.search.mockImplementation(() => ({
                'topics[]': [topics[0].name],
                'progress[]': [progressOptions[0].name],
                searchText: 'Stream',
                newCourses: undefined,
                comingSoon: undefined,
            }));
            render();

            verifyFilters();
        });

        it('should use previously saved query params when no search params are present', () => {
            ClientStorage.setItem('librarySearchTopic', JSON.stringify([topics[0].name]));
            ClientStorage.setItem('librarySearchProgress', JSON.stringify([progressOptions[0].name]));
            ClientStorage.setItem('librarySearchText', JSON.stringify('Stream'));
            render();

            verifyFilters();
        });

        it('should prefer query params to previously saved search params', () => {
            $location.search.mockImplementation(() => ({
                'topics[]': [topics[0].name],
                'progress[]': [progressOptions[0].name],
                searchText: 'Stream',
                newCourses: undefined,
                comingSoon: undefined,
            }));

            ClientStorage.setItem('librarySearchTopic', JSON.stringify([]));
            ClientStorage.setItem('librarySearchProgress', JSON.stringify([]));
            ClientStorage.setItem('librarySearchText', JSON.stringify('find nothing'));
            ClientStorage.setItem('librarySearchNewCourses', JSON.stringify(true));
            ClientStorage.setItem('librarySearchComingSoon', JSON.stringify(true));

            render();

            verifyFilters();
        });

        it('should save query params and update search location', () => {
            render();

            // first topic button should be selected
            const topicButton0 = SpecHelper.expectElement(
                elem,
                `.select-filters .topic[data-id="${topics[0].locales.en}"]`,
            );
            topicButton0.click();

            // not started button should be selected
            const progressButton0 = SpecHelper.expectElement(
                elem,
                '.select-filters .progress-button[data-id="not_started"]',
            );
            progressButton0.click();

            SpecHelper.click(elem, '.status-button[data-id="new_courses"]');

            SpecHelper.updateTextInput(elem, '[name="search-text"]:eq(0)', 'Stream');

            expect(JSON.parse(ClientStorage.getItem('librarySearchTopic'))).toEqual(['topic 0']);
            expect(JSON.parse(ClientStorage.getItem('librarySearchProgress'))).toEqual(['Not Started']);
            expect(JSON.parse(ClientStorage.getItem('librarySearchText'))).toEqual('Stream');
            expect(JSON.parse(ClientStorage.getItem('librarySelectedStatus'))).toEqual('new_courses');

            expect($location.search).toHaveBeenCalledWith('topics[]', ['topic 0']);
            expect($location.search).toHaveBeenCalledWith('progress[]', ['Not Started']);
            expect($location.search).toHaveBeenCalledWith('searchText', 'Stream');
            expect($location.search).toHaveBeenCalledWith('selectedStatus', 'new_courses');
        });

        it('should ignore saved just_added params if that filter option is not available now', () => {
            // flush the render so we can set up a new set of streams
            render();

            ClientStorage.setItem('librarySearchTopic', JSON.stringify([topics[0].name]));
            ClientStorage.setItem('librarySearchProgress', JSON.stringify([progressOptions[0].name]));
            ClientStorage.setItem('librarySearchText', JSON.stringify('Stream'));
            ClientStorage.setItem('librarySelectedStatus', 'new_courses'); // this should be ignored

            // render without just_added streams
            setupStreams(false, true);

            render();

            verifyFilters();
        });

        it('should ignore saved coming_soon param if that filter option is not available now', () => {
            // flush the render so we can set up a new set of streams
            render();

            ClientStorage.setItem('librarySearchTopic', JSON.stringify([topics[0].name]));
            ClientStorage.setItem('librarySearchProgress', JSON.stringify([progressOptions[0].name]));
            ClientStorage.setItem('librarySearchText', JSON.stringify('Stream'));
            ClientStorage.setItem('librarySelectedStatus', 'coming_soon'); // this should be ignored

            // render without coming_soon streams
            setupStreams(true, false);

            render();

            verifyFilters();
        });

        it('should ignore saved beta param if that filter option is not available now', () => {
            // flush the render so we can set up a new set of streams
            render();

            ClientStorage.setItem('librarySearchTopic', JSON.stringify([topics[0].name]));
            ClientStorage.setItem('librarySearchProgress', JSON.stringify([progressOptions[0].name]));
            ClientStorage.setItem('librarySearchText', JSON.stringify('Stream'));
            ClientStorage.setItem('librarySelectedStatus', 'beta'); // this should be ignored

            // render without beta streams
            setupStreams(true, true, false, true);

            render();

            verifyFilters();
        });

        it('should ignore saved just updated param if that filter option is not available now', () => {
            // flush the render so we can set up a new set of streams
            render();

            ClientStorage.setItem('librarySearchTopic', JSON.stringify([topics[0].locales.en]));
            ClientStorage.setItem('librarySearchProgress', JSON.stringify([progressOptions[0].name]));
            ClientStorage.setItem('librarySearchText', JSON.stringify('Stream'));
            ClientStorage.setItem('librarySelectedStatus', 'updated'); // this should be ignored

            // render without just updated streams
            setupStreams(true, true, true, false);

            render();

            verifyFilters();
        });
    });

    describe('searchAttempts', () => {
        let searchEvents;
        beforeEach(() => {
            const origLog = EventLogger.prototype.log;
            jest.spyOn(EventLogger.prototype, 'log').mockImplementation((eventType, obj) => {
                const event = origLog.apply(EventLogger.instance, [eventType, obj]);
                if (eventType.slice(0, 14) === 'library_search') {
                    searchEvents.push(event);
                }
                return event;
            });

            setupStreams();

            streams[0].title = 'I match the search';
        });

        it('should log expected events when a search leads to a stream launch', () => {
            trackEvents(() => {
                render();
                SpecHelper.updateTextInput(elem, '[name="search-text"]:eq(0)', 'match');
                SpecHelper.click(elem, '.search-form button.search');
                mockNavigatingToStreamDashboard(streams[0].id);
                mockLaunchingStreamViewModeler(streams[0].id, streams[0].lessons[0].id);
            });
            expectEventTypes(['library_search:start', 'library_search:finish']);

            expect(searchEvents[1].properties.aborted).toBe(false);
            expect(searchEvents[1].properties.launched_lesson_id).toEqual(streams[0].lessons[0].id);
            expect(searchEvents[1].properties.viewed_stream_dashboard_id).toEqual(streams[0].id);
            expect(searchEvents[1].properties.launched_stream_id).toEqual(streams[0].id);
        });

        it('should log expected events when a search leads to the stream dashboard but not a stream launch', () => {
            trackEvents(() => {
                render();
                SpecHelper.updateTextInput(elem, '[name="search-text"]:eq(0)', 'match');
                SpecHelper.click(elem, '.search-form button.search');
                mockNavigatingToStreamDashboard(streams[0].id);
                mockNavigatingSomewhereElse();
            });
            expectEventTypes(['library_search:start', 'library_search:finish']);

            expect(searchEvents[1].properties.aborted).toBe(false);
            expect(searchEvents[1].properties.launched_lesson_id).toBeUndefined();
            expect(searchEvents[1].properties.viewed_stream_dashboard_id).toEqual(streams[0].id);
            expect(searchEvents[1].properties.launched_stream_id).toBeUndefined();
        });

        it('should close a search attempt only when removing topic filters', () => {
            trackEvents(() => {
                render();
                // add 1 filter
                SpecHelper.click(elem, `.select-filters .topic[data-id="${topics[0].locales.en}"]`);
                // add a second filter; should not end the search attempt
                SpecHelper.click(elem, `.select-filters .topic[data-id="${topics[1].locales.en}"]`);
                // remove the first filter; should end the search attempt
                SpecHelper.click(elem, `.select-filters .topic[data-id="${topics[0].locales.en}"]`);
            });
            expectEventTypes([
                'library_search:start',
                'library_search:update',
                'library_search:finish',
                'library_search:start',
            ]);

            expect(searchEvents[2].properties.aborted).toBe(true);
        });

        it('should close a search attempt when changing search', () => {
            trackEvents(() => {
                render();
                SpecHelper.updateTextInput(elem, '[name="search-text"]:eq(0)', 'match');
                SpecHelper.click(elem, '.search-form button.search');
                SpecHelper.updateTextInput(elem, '[name="search-text"]:eq(0)', 'else');
                SpecHelper.click(elem, '.search-form button.search');
            });
            expectEventTypes(['library_search:start', 'library_search:finish', 'library_search:start']);

            expect(searchEvents[1].properties.aborted).toBe(true);
        });

        it('should keep a search attempt open when adding to the search', () => {
            trackEvents(() => {
                render();
                SpecHelper.updateTextInput(elem, '[name="search-text"]:eq(0)', 'match');
                SpecHelper.click(elem, '.search-form button.search');
                SpecHelper.updateTextInput(elem, '[name="search-text"]:eq(0)', 'match but more specific');
                SpecHelper.click(elem, '.search-form button.search');
            });
            expectEventTypes(['library_search:start', 'library_search:update']);

            expect(searchEvents[1].properties.aborted).toBeUndefined();
        });

        it('should close a search attempt when navigating away from library or stream dashboard', () => {
            trackEvents(() => {
                render();
                SpecHelper.updateTextInput(elem, '[name="search-text"]:eq(0)', 'match');
                SpecHelper.click(elem, '.search-form button.search');
                mockNavigatingSomewhereElse();
            });
            expectEventTypes(['library_search:start', 'library_search:finish']);

            expect(searchEvents[1].properties.aborted).toBe(true);
        });

        it('should close a search attempt when navigating to a stream that was not in the search results', () => {
            trackEvents(() => {
                render();
                SpecHelper.updateTextInput(elem, '[name="search-text"]:eq(0)', 'match');
                SpecHelper.click(elem, '.search-form button.search');
                mockNavigatingToStreamDashboard(streams[1].id);
            });
            expectEventTypes(['library_search:start', 'library_search:finish']);

            expect(searchEvents[1].properties.aborted).toBe(true);
        });

        it('should have expected default stuff in events', () => {
            trackEvents(() => {
                render();
                SpecHelper.updateTextInput(elem, '[name="search-text"]:eq(0)', 'match');
                SpecHelper.click(elem, `.select-filters .topic[data-id="${topics[0].locales.en}"]`);
                SpecHelper.click(elem, '.select-filters .progress-button[data-id="not_started"]');
            });

            const props = _.last(searchEvents).properties;
            expect(props.search_attempt_id).not.toBeUndefined();
            expect(props.topics_to_filter_by).toEqual([topics[0].locales.en]);
            expect(props.progress_options_to_filter_by).toEqual(['not_started']);
            expect(props.search_text).toEqual('match');
            expect(props.result_stream_ids).toEqual([streams[0].id]);
        });

        function mockNavigatingToStreamDashboard(streamId) {
            $route.current.params = {
                stream_id: streamId,
            };
            scope.$emit('$routeChangeSuccess', {
                $$route: {
                    directive: 'stream-dashboard',
                },
            });
        }

        function mockLaunchingStreamViewModeler(streamId, lessonId) {
            $route.current.params = {
                stream_id: streamId,
                lesson_id: lessonId,
            };
            scope.$emit('$routeChangeSuccess', {
                $$route: {
                    directive: 'show-stream',
                },
            });
        }

        function mockNavigatingSomewhereElse() {
            scope.$emit('$routeChangeSuccess', {});
        }

        function trackEvents(func) {
            searchEvents = [];
            func();
            return searchEvents;
        }

        function expectEventTypes(expectedTypes) {
            expect(searchEvents.map(event => event.event_type)).toEqual(expectedTypes);
        }
    });

    function idsOnScreen(selector) {
        selector = `:not(.ng-hide) ${selector}`;
        const _ids = [];
        elem.find(selector).each(function () {
            if ($(this).closest('.ng-hide').length === 0) {
                _ids.push($(this).attr('data-id'));
            }
        });
        return _ids;
    }

    function ids(list, indexes) {
        return indexes.map(i => list[i].id);
    }
});
