import 'AngularSpecHelper';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/streams';
import studentDashboardLearningBoxLocales from 'Lessons/locales/lessons/stream/student_dashboard_learning_box-en.json';
import streamDashboardLocales from 'Lessons/locales/lessons/stream/stream_dashboard-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(studentDashboardLearningBoxLocales, streamDashboardLocales);

describe('Lesson::Stream::StudentDashboardLessonLinkBoxDir', () => {
    let $rootScope;
    let $injector;
    let SpecHelper;
    let Stream;
    let stream;
    let elem;
    let isMobile;
    let DialogModal;
    let Cohort;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'SpecHelper');

        angular.mock.module($provide => {
            isMobile = jest.fn();
            isMobile.mockReturnValue(false);
            $provide.value('isMobile', isMobile);
        });

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            $rootScope = $injector.get('$rootScope');
            DialogModal = $injector.get('DialogModal');

            $injector.get('StreamFixtures');
            SpecHelper = $injector.get('SpecHelper');
            Stream = $injector.get('Lesson.Stream');
            SpecHelper.stubCurrentUser('learner');
            Cohort = $injector.get('Cohort');
            $injector.get('CohortFixtures');
            $rootScope.currentUser.relevant_cohort = Cohort.fixtures.getInstance();
            Object.defineProperty($rootScope.currentUser, 'programType', {
                value: 'mba',
            });

            stream = Stream.fixtures.getInstance();
        });

        SpecHelper.stubConfig();
    });

    describe('with exam stream', () => {
        let firstLesson;

        beforeEach(() => {
            SpecHelper.stubEventLogging();
            stream.exam = true;
            stream.time_limit_hours = 24;
            _.each(stream.lessons, lesson => {
                lesson.test = true;
            });
            firstLesson = stream.chapters[0].lessons[0];
        });

        describe('launch lesson with unstarted stream', () => {
            it('should show a confirm modal and launch if confirmed', () => {
                stream.lesson_streams_progress = null;

                const now = new Date('2016/01/02 00:00:00');
                jest.spyOn(stream, '_now').mockImplementation(() => now);

                render();

                jest.spyOn(firstLesson, 'launch').mockImplementation(() => {});
                jest.spyOn(DialogModal, 'alert');
                jest.spyOn(DialogModal, 'hideAlerts').mockImplementation(() => {});

                SpecHelper.click(elem, '.launch-button');
                expect(firstLesson.launch).not.toHaveBeenCalled();
                expect(DialogModal.hideAlerts).not.toHaveBeenCalled();

                const body = $('body');
                SpecHelper.expectElementText(
                    body,
                    '.modal',
                    'Ready to begin? You’ll have 24 hours from the time you start the exam to complete it.Start Exam',
                );
                SpecHelper.click(body, '.modal button.go');
                expect(firstLesson.launch).toHaveBeenCalled();
                expect(DialogModal.hideAlerts).toHaveBeenCalled();
            });
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render(options) {
        const renderer = SpecHelper.renderer();
        angular.extend(
            renderer.scope,
            {
                keepLearningStream: stream,
                keepLearningLesson: Stream.keepLearningLesson(stream),
            },
            options,
        );

        renderer.render(
            '<student-dashboard-lesson-link-box mode="\'course\'" keep-learning-stream="keepLearningStream" keep-learning-lesson="keepLearningLesson"></stream-dashboard>',
        );
        elem = renderer.elem;
    }
});
