import 'AngularSpecHelper';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/streams';
import setSpecLocales from 'Translation/setSpecLocales';
import streamDashboardLocales from 'Lessons/locales/lessons/stream/stream_dashboard-en.json';
import 'Users/angularModule/spec/_mock/fixtures/users';
import moment from 'moment-timezone';

setSpecLocales(streamDashboardLocales);

describe('Lesson::Stream::StreamDashboardDir', () => {
    let $timeout;
    let $rootScope;
    let $injector;
    let SpecHelper;
    let Stream;
    let stream;
    let elem;
    let scope;
    let isMobile;
    let ContentAccessHelper;
    let ClientStorage;
    let $location;
    let DialogModal;
    let $window;
    let $interval;
    let dateHelper;
    let Cohort;
    let offlineModeManager;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'SpecHelper');

        angular.mock.module($provide => {
            isMobile = jest.fn();
            isMobile.mockReturnValue(false);
            $provide.value('isMobile', isMobile);
        });

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            $timeout = $injector.get('$timeout');
            $rootScope = $injector.get('$rootScope');
            ContentAccessHelper = $injector.get('ContentAccessHelper');
            ClientStorage = $injector.get('ClientStorage');
            $location = $injector.get('$location');
            DialogModal = $injector.get('DialogModal');
            $window = $injector.get('$window');
            $interval = $injector.get('$interval');
            dateHelper = $injector.get('dateHelper');
            offlineModeManager = $injector.get('offlineModeManager');
            offlineModeManager.inOfflineMode = false;

            $injector.get('StreamFixtures');
            SpecHelper = $injector.get('SpecHelper');
            Stream = $injector.get('Lesson.Stream');
            SpecHelper.stubCurrentUser('learner');
            Cohort = $injector.get('Cohort');
            $injector.get('CohortFixtures');
            $rootScope.currentUser.relevant_cohort = Cohort.fixtures.getInstance();
            Object.defineProperty($rootScope.currentUser, 'programType', {
                value: 'mba',
            });

            stream = Stream.fixtures.getInstance();
        });

        SpecHelper.stubConfig();
        SpecHelper.stubDirective('linkedinProfileButton');
        SpecHelper.stubDirective('feedbackSidebar');
    });

    function expectOtherStreamsRequest(expectedIds, streams) {
        Stream.expect('index')
            .toBeCalledWith({
                filters: {
                    published: true,
                    id: expectedIds,
                },
                summary: true,
                include_progress: true,
            })
            .returns(streams);
        return streams;
    }

    describe('component display', () => {
        it('should have a list of chapters', () => {
            render();
            SpecHelper.expectElements(elem, '.chapter-index', stream.chapters.length);
        });

        it('should show what you will learn if available', () => {
            render();
            SpecHelper.expectElement(elem, '.what-you-will-learn-box');
        });

        it('should not show what you will learn if empty', () => {
            stream.what_you_will_learn = [];
            render();
            SpecHelper.expectNoElement(elem, '.what-you-will-learn-box');
        });

        it('should show recommended courses if available', () => {
            const otherStreams = [Stream.fixtures.getInstance()];
            const ids = [otherStreams[0].id, 'some_other_id'];
            stream.recommended_stream_ids = ids;
            expectOtherStreamsRequest(ids, otherStreams);
            render();
            Stream.flush('index');
            SpecHelper.expectElement(elem, '.recommended-courses-box');

            // only one small-course should be shown, since id that was not
            // returned from api call is filtered out
            SpecHelper.expectElements(elem, '.recommended-courses-box .small-course', 1);
        });

        it('should not show recommended courses if empty', () => {
            const otherStreams = [Stream.fixtures.getInstance()];
            stream.recommended_stream_ids = [];
            stream.related_stream_ids = [otherStreams[0].id];
            expectOtherStreamsRequest([otherStreams[0].id], otherStreams);
            render();
            Stream.flush('index');
            SpecHelper.expectNoElement(elem, '.recommended-courses-box');
        });

        it('should show related courses if available', () => {
            const otherStreams = [Stream.fixtures.getInstance()];
            const ids = [otherStreams[0].id, 'some_other_id'];
            stream.related_stream_ids = ids;
            expectOtherStreamsRequest(ids, otherStreams);
            render();
            Stream.flush('index');
            SpecHelper.expectElement(elem, '.related-courses-box');

            // only one small-course should be shown, since id that was not
            // returned from api call is filtered out
            SpecHelper.expectElements(elem, '.related-courses-box .small-course', 1);
        });

        it('should not show related courses if empty', () => {
            const otherStreams = [Stream.fixtures.getInstance()];
            stream.related_stream_ids = [];
            stream.recommended_stream_ids = [otherStreams[0].id];
            expectOtherStreamsRequest([otherStreams[0].id], otherStreams);
            render();
            Stream.flush('index');
            SpecHelper.expectNoElement(elem, '.related-courses-box');
        });

        it('should load related and recommended courses when not logged in', () => {
            const otherStreams = [Stream.fixtures.getInstance(), Stream.fixtures.getInstance()];
            stream.recommended_stream_ids = [otherStreams[0].id, 'some_other_id'];
            stream.related_stream_ids = [otherStreams[0].id, 'some_other_id'];
            Stream.expect('index')
                .toBeCalledWith({
                    filters: {
                        published: true,
                        id: stream.recommended_stream_ids.concat(stream.related_stream_ids),
                        view_as: 'cohort:PROMOTED_DEGREE', // when not logged in, we default to this group name
                        in_locale_or_en: 'en',
                        user_can_see: null,
                        in_users_locale_or_en: null,
                    },
                    summary: true,
                    include_progress: false,
                })
                .returns(otherStreams);
            SpecHelper.stubNoCurrentUser();
            render();
            Stream.flush('index');
            SpecHelper.expectElement(elem, '.recommended-courses-box');
            SpecHelper.expectElement(elem, '.related-courses-box');
            SpecHelper.expectElements(elem, '.recommended-courses-box .small-course', 1);
            SpecHelper.expectElements(elem, '.related-courses-box .small-course', 1);
        });

        it('should not show related or recommended courses that are still coming soon', () => {
            const coming_soon_stream = Stream.fixtures.getInstance();
            coming_soon_stream.coming_soon = true;
            const otherStreams = [coming_soon_stream];
            stream.related_stream_ids = [otherStreams[0].id];
            stream.recommended_stream_ids = [otherStreams[0].id];
            Stream.expect('index')
                .toBeCalledWith({
                    filters: {
                        published: true,
                        id: stream.recommended_stream_ids.concat(stream.related_stream_ids),
                    },
                    summary: true,
                    include_progress: true,
                })
                .returns(otherStreams);
            render();
            Stream.flush('index');

            // Shouldn't see any recommended or related streams, since both consist entirely of the coming soon course
            SpecHelper.expectNoElement(elem, '.recommended-courses-box');
            SpecHelper.expectNoElement(elem, '.related-courses-box');
        });

        it('should allow navigation to stream dashboard if not locked', () => {
            const otherStreams = [Stream.fixtures.getInstance()];
            const ids = [otherStreams[0].id, 'some_other_id'];
            stream.related_stream_ids = ids;
            expectOtherStreamsRequest(ids, otherStreams);
            Object.defineProperty(ContentAccessHelper.prototype, 'canLaunch', {
                value: true,
            });
            render();
            Stream.flush('index');

            SpecHelper.expectElements(elem, '.related-courses-box .small-course', 1);
            SpecHelper.expectNoElement(elem, '.related-courses-box .small-course.disabled');
        });

        it('should not allow navigation to stream dashboard if locked', () => {
            const otherStreams = [Stream.fixtures.getInstance()];
            const ids = [otherStreams[0].id, 'some_other_id'];
            stream.related_stream_ids = ids;
            expectOtherStreamsRequest(ids, otherStreams);
            Object.defineProperty(ContentAccessHelper.prototype, 'canLaunch', {
                value: false,
            });
            render();
            Stream.flush('index');

            SpecHelper.expectNoElement(elem, '.related-courses-box .small-course:not(.disabled)');
            SpecHelper.expectElements(elem, '.related-courses-box .small-course.disabled', 1);
        });

        it('should not make api call for other courses if unnnecessary', () => {
            jest.spyOn(Stream, 'index').mockImplementation(() => {});
            stream.related_stream_ids = [];
            stream.recommended_stream_ids = [];
            render();
            expect(Stream.index).not.toHaveBeenCalled();
        });

        it('should show credits if available', () => {
            render();
            SpecHelper.expectElement(elem, '.credits-box');
        });

        it('should not show credits if empty', () => {
            stream.credits = '';
            render();
            SpecHelper.expectNoElement(elem, '.credits-box');
        });

        it('should hide stuff until loaded on mobile', () => {
            isMobile.mockReturnValue(true);
            render();
            SpecHelper.expectNoElement(elem, '.chapter-index');
            SpecHelper.expectNoElement(elem, '.sidebar-column');
            SpecHelper.expectNoElement(elem, 'dashboard-footer');
            scope.$broadcast('RouteAnimationHelper.animationFinished');
            scope.$digest();
            SpecHelper.expectElements(elem, '.chapter-index', stream.chapters.length);
            SpecHelper.expectElement(elem, '.sidebar-column');
            SpecHelper.expectElement(elem, 'dashboard-footer');
        });

        it('should auto-expand any incomplete chapters', () => {
            jest.spyOn(stream.chapters[0], 'progressStatus').mockReturnValue('not_started');
            jest.spyOn(stream.chapters[1], 'progressStatus').mockReturnValue('in_progress');
            render();
            SpecHelper.expectElements(elem, '.lesson-info:not(.ng-hide)', 2);
        });

        it('should not auto-expand any completed chapters', () => {
            jest.spyOn(stream.chapters[0], 'progressStatus').mockReturnValue('completed');
            jest.spyOn(stream.chapters[1], 'progressStatus').mockReturnValue('completed');
            render();
            SpecHelper.expectElements(elem, '.lesson-info.ng-hide', 2);
        });

        it('should not display assessment-score-arc on non-exam, non-assessment lessons', () => {
            render();
            SpecHelper.expectNoElement(elem, 'assessment-score-arc');
        });

        it('should not display assessment-score-arc on non-exam, non-assessment lessons even when stream is complete', () => {
            SpecHelper.stubEventLogging();
            stream.ensureStreamProgress().complete = true;
            render();
            SpecHelper.expectNoElement(elem, 'assessment-score-arc');
        });
    });

    describe('key terms toggle', () => {
        it("should default to 'text' if preference not available", () => {
            render();
            expect(ClientStorage.getItem('toggleableKeyTermsList')).toBeUndefined();
            expect(scope.sortKeyTerms.key).toMatch('text');
        });

        it('should set to preference if available', () => {
            ClientStorage.setItem('toggleableKeyTermsList', 'index');
            render();
            expect(scope.sortKeyTerms.key).toMatch('index');
            ClientStorage.removeItem('toggleableKeyTermsList');
        });
    });

    describe('progess', () => {
        it('should not save progress if the stream is already started', () => {
            expect(stream.lesson_streams_progress).not.toBeUndefined();
            jest.spyOn(stream, 'ensureStreamProgress').mockImplementation(() => {});
            render();
            expect(stream.ensureStreamProgress).not.toHaveBeenCalled();
        });
    });

    describe('certificate', () => {
        it('should open certificate when view certificate is clicked', () => {
            const EventLogger = $injector.get('EventLogger');

            render();
            scope.stream.lesson_streams_progress.complete = true;
            jest.spyOn(scope.stream.lesson_streams_progress, 'certificateImageSrc').mockReturnValue('http://not.a.url');

            scope.$digest();

            jest.spyOn(scope, 'downloadCertificate');
            jest.spyOn(scope, 'performStreamDownload').mockImplementation(() => {});
            jest.spyOn(EventLogger.prototype, 'log').mockImplementation(() => {});
            SpecHelper.click(elem, '.sidebar-column .view-certificate');
            expect(scope.downloadCertificate).toHaveBeenCalled();
            expect(EventLogger.prototype.log).toHaveBeenCalledWith(
                'lesson:stream:download_certificate_click',
                scope.stream.logInfo(),
            );
            expect(scope.performStreamDownload).toHaveBeenCalled();
        });
    });

    describe('course summaries', () => {
        describe('locked', () => {
            beforeEach(() => {
                expect(stream.summaries).not.toBeUndefined(); // sanity check
                render();
            });
            it('should display a resources panel if course summaries are present', () => {
                SpecHelper.expectElements(elem, '.resources-box', 1);
                SpecHelper.expectElements(elem, '.resources-box .stream-summary', 2);
                SpecHelper.expectElementText(
                    elem,
                    '.resources-box .stream-summary div.locked',
                    `Complete "${stream.lessons[0].title}" OR "${stream.lessons[1].title}" to unlock! Summary1`,
                    0,
                );
                SpecHelper.expectElementText(
                    elem,
                    '.resources-box .stream-summary div.locked',
                    `Complete "${stream.lessons[2].title}" to unlock! Summary2`,
                    1,
                );
            });

            it('should display inline within the chapter lessons listing as well', () => {
                SpecHelper.expectElement(elem, '.lesson-box:eq(0) .unlockable-summaries');
                SpecHelper.expectElementText(
                    elem,
                    '.lesson-box:eq(0) .unlockable-summaries .stream-summary div.locked',
                    'Complete to unlock Summary1',
                );

                SpecHelper.expectElement(elem, '.lesson-box:eq(1) .unlockable-summaries');
                SpecHelper.expectElementText(
                    elem,
                    '.lesson-box:eq(1) .unlockable-summaries .stream-summary div.locked',
                    'Complete to unlock Summary2',
                );

                SpecHelper.expectElement(elem, '.lesson-box:eq(2) .unlockable-summaries');
                SpecHelper.expectElementText(
                    elem,
                    '.lesson-box:eq(2) .unlockable-summaries .stream-summary div.locked',
                    'Complete to unlock Summary1',
                );

                SpecHelper.expectNoElement(elem, '.lesson-box:eq(3) .unlockable-summaries');
            });
        });

        describe('unlocked', () => {
            beforeEach(setupUnlockedResources);
            it('should display a resources panel if course summaries are present', () => {
                assertOneResourceUnlocked();
            });

            it('should display inline within the chapter lessons listing as well', () => {
                SpecHelper.expectElement(elem, '.lesson-box:eq(0) .unlockable-summaries');
                SpecHelper.expectElementText(
                    elem,
                    '.lesson-box:eq(0) .unlockable-summaries .stream-summary div.unlocked',
                    'Unlocked Summary1!',
                );

                SpecHelper.expectElement(elem, '.lesson-box:eq(1) .unlockable-summaries');
                SpecHelper.expectElementText(
                    elem,
                    '.lesson-box:eq(1) .unlockable-summaries .stream-summary div.locked',
                    'Complete to unlock Summary2',
                );

                SpecHelper.expectElement(elem, '.lesson-box:eq(2) .unlockable-summaries');
                SpecHelper.expectElementText(
                    elem,
                    '.lesson-box:eq(2) .unlockable-summaries .stream-summary div.unlocked',
                    'Unlocked Summary1!',
                );

                SpecHelper.expectNoElement(elem, '.lesson-box:eq(3) .unlockable-summaries');
            });
        });
    });

    describe('favorite bookmarking', () => {
        function checkBookmarked(ribbon, bookmarked) {
            jest.spyOn(scope, 'toggleBookmark');
            jest.spyOn($rootScope.currentUser, 'toggleBookmark');

            ribbon.eq(0).click();
            scope.$digest();

            expect(scope.toggleBookmark).toHaveBeenCalled();
            expect($rootScope.currentUser.toggleBookmark).toHaveBeenCalled();
            expect(scope.stream.favorite).toBe(bookmarked);
            SpecHelper.expectElementText(
                elem,
                '.bookmark-button',
                bookmarked ? 'SAVED TO MY COURSES Tap to Remove' : 'ADD TO MY COURSES',
                0,
            );
        }

        it('should allow for bookmarking of a non-bookmarked stream', () => {
            render();
            scope.stream.favorite = false;
            scope.$digest();

            const ribbon = SpecHelper.expectElements(elem, '.bookmark-ribbon.dark-coral', 2);
            checkBookmarked(ribbon, true);
        });

        it('should allow for un-bookmarking of a bookmarked stream', () => {
            render();
            scope.stream.favorite = true;
            scope.$digest();

            const ribbon = SpecHelper.expectElements(elem, '.bookmark-ribbon.dark-blue', 2);
            checkBookmarked(ribbon, false);
        });

        it('should disable bookmarking if the user is a Blue Ocean Strategy user', () => {
            render();
            Object.defineProperty(scope, 'blueOceanUser', {
                value: true,
            });
            scope.$digest();
            SpecHelper.expectNoElement(elem, '.bookmark-ribbon');
        });
    });

    describe('locking', () => {
        afterEach(() => {
            window.CORDOVA = false;
            $window.CORDOVA = false;
        });

        it('should lessons and start button display as unlocked if canLaunch', () => {
            jest.spyOn(ContentAccessHelper.prototype, 'canLaunch', 'get').mockReturnValue(true);
            render();
            SpecHelper.expectNoElement(elem, '.start-course-box button .icon.lock');
            SpecHelper.expectNoElement(elem, '.lesson-progress-icon.locked');
        });

        it('should show lessons and start button display as locked if !canLaunch', () => {
            jest.spyOn(ContentAccessHelper.prototype, 'canLaunch', 'get').mockReturnValue(false);
            render();
            SpecHelper.expectElement(elem, '.start-course-box button .icon.lock');
            SpecHelper.expectElements(elem, '.lesson-progress-icon.locked', stream.lessons.length);
        });

        it('should display a modal instead of navigating to a course if not authenticated', () => {
            jest.spyOn(ContentAccessHelper.prototype, 'canLaunch', 'get').mockReturnValue(false);
            jest.spyOn(ContentAccessHelper.prototype, 'reason', 'get').mockReturnValue('no_user');

            render();
            jest.spyOn(scope, 'gotoMarketingHome').mockImplementation(() => {});
            jest.spyOn(DialogModal, 'alert');
            SpecHelper.click(elem, '.lesson-box a', 0);
            expect(DialogModal.alert).toHaveBeenCalled();
            const modal = SpecHelper.expectElement($('body'), '.modal');
            jest.spyOn($location, 'url').mockImplementation(() => {});
            SpecHelper.click(modal, 'button.flat.green');
            expect(scope.gotoMarketingHome).toHaveBeenCalled();
        });

        it('should should launch if lesson access is available', () => {
            Object.defineProperty(ContentAccessHelper.prototype, 'canLaunch', {
                value: true,
            });

            const targetLesson = stream.orderedLessons[0];
            jest.spyOn(targetLesson, 'launch').mockImplementation(() => {});
            for (let i = 0; i < stream.lessons.length; i++) {
                if (stream.lessons[i].id === targetLesson.id) {
                    stream.lessons[i] = targetLesson;
                    break;
                }
            }

            render();
            SpecHelper.expectNoElement(elem, '.start-course-box button .icon.lock');
            SpecHelper.click(elem, '.start-course-box button');
            expect(targetLesson.launch).toHaveBeenCalled();
        });
    });

    describe('prereq checking', () => {
        it('should call showModalIfPrereqsNotMet', () => {
            jest.spyOn(ContentAccessHelper.prototype, 'showModalIfPrereqsNotMet').mockImplementation(() => {});
            render();
            expect(ContentAccessHelper.prototype.showModalIfPrereqsNotMet).toHaveBeenCalled();
        });
    });

    describe('with assessment lesson', () => {
        it('should display assessment-score-arc', () => {
            // simplify things by having just one lesson
            stream.lessons = [stream.lessons[0]];
            render();
            SpecHelper.expectNoElement(elem, 'assessment-score-arc');

            stream.lessons[0].assessment = true;
            // have to render again since we're using bindOnce functionality
            render();
            SpecHelper.expectElements(elem, 'assessment-score-arc', 2); // two arcs; one for mobile, one for desktop
        });
    });

    describe('with exam stream', () => {
        let firstLesson;
        let examOpenTime;

        beforeEach(() => {
            SpecHelper.stubEventLogging();
            stream.exam = true;
            stream.time_limit_hours = 24;
            _.each(stream.lessons, lesson => {
                lesson.test = true;
            });
            examOpenTime = moment.utc('2016-01-01 22:00:00').toDate();
            jest.spyOn(stream, 'examOpenTime').mockReturnValue(examOpenTime);
            firstLesson = stream.chapters[0].lessons[0];
        });

        it('should tweak the top start / resume messaging', () => {
            render();
            SpecHelper.expectElementText(elem, '.start-course-box .callout', 'Ready to begin? Start your exam now.');
            SpecHelper.expectElementText(elem, '.start-course-box button', 'START');
        });

        it('should hide the sharing buttons and bookmark buttons', () => {
            render();
            SpecHelper.expectNoElement(elem, 'share-stream-sidebar');
            SpecHelper.expectNoElement(elem, '.bookmark-button');
            stream.exam = false;
            scope.$digest();
            SpecHelper.expectElements(elem, 'share-stream-sidebar', 3); // one for each breakpoint
            SpecHelper.expectElements(elem, '.bookmark-button', 2);
        });

        it('should show schedule', () => {
            dateHelper.formattedUserFacingDateTime = jest.fn(() => 'Formatted Time');
            render();
            scope.xsOrSm = false; // this would normally be set by isMobileMixin

            scope.$digest();
            SpecHelper.expectElements(elem, '.exam-schedule-box', 2); // one for each breakpoint
            SpecHelper.expectElementText(elem, '.exam-schedule-box:eq(0) .header', 'SCHEDULE');
            SpecHelper.expectElementText(
                elem,
                '.exam-schedule-box:eq(0) [name="exam-begins"] .header-2',
                'EXAM BEGINS',
            );
            expect(dateHelper.formattedUserFacingDateTime).toHaveBeenCalledWith(examOpenTime);
            SpecHelper.expectElementText(elem, '.exam-schedule-box:eq(0) [name="exam-begins"] p', 'Formatted Time');
            SpecHelper.expectElementText(
                elem,
                '.exam-schedule-box:eq(0) [name="time-limit"] .header-2',
                'TIME LIMIT',
                0,
            );
            SpecHelper.expectElementText(
                elem,
                '.exam-schedule-box:eq(0) [name="time-limit"] p',
                'The exam timer will begin when you launch your first lesson. After 24 hours, your exam will close, and no additional answers will be accepted.',
            );

            // On mobile, there is special stuff:
            // 1. it should be visible if the stream is not started
            stream.lesson_streams_progress = null;
            scope.$digest();
            SpecHelper.expectElements(elem, '.exam-schedule-box', 2);

            // 2. it should be visible on desktop even if the stream is started
            stream.ensureStreamProgress();
            scope.$digest();
            SpecHelper.expectElements(elem, '.exam-schedule-box', 2);

            // 3. it should be hidden on mobile if the stream is started
            scope.xsOrSm = true; // this would normally be set by isMobileMixin
            scope.$digest();
            SpecHelper.expectNoElement(elem, '.exam-schedule-box');
            SpecHelper.expectElements(elem, '[name="show-exam-schedule"]', 2); // one for each breakpoint

            // 4. it should show up once you click the button
            SpecHelper.click(elem, '[name="show-exam-schedule"]', 0);
            SpecHelper.expectNoElement(elem, '[name="show-exam-schedule"]');
            SpecHelper.expectElements(elem, '.exam-schedule-box', 2);
        });

        it('hide the certificate download', () => {
            render();
            SpecHelper.expectNoElement(elem, '.view-certificate');
        });

        it('should make changes to lesson box', () => {
            render();
            SpecHelper.expectHasClass(elem, '.lesson-box:eq(0) .lesson-details-box', 'test');
            SpecHelper.expectElementText(elem, '.lesson-box:eq(0) .lesson-special-type', 'TEST');
            SpecHelper.expectNoElement(elem, '.lesson-box:eq(0) .lesson-time-estimate');
        });

        describe('timer box', () => {
            it('should show as expected when stream is not yet started', () => {
                stream.lesson_streams_progress = null;
                render();
                SpecHelper.expectElementText(elem, '.timer-box:eq(0) .header', 'EXAM TIMER');
                SpecHelper.expectElementText(
                    elem,
                    '.timer-box:eq(0) .message',
                    'After launching the first lesson, you will have 24:00:00 to complete the exam.',
                );
            });

            it('should show as expected when stream is in progress', () => {
                stream.ensureStreamProgress().time_runs_out_at = new Date('2016/01/01 12:00:00').getTime() / 1000;
                let now = new Date('2016/01/01 00:00:00');
                jest.spyOn(stream.lesson_streams_progress, '_now').mockImplementation(() => now);
                render();
                SpecHelper.expectHasClass(elem, '.timer-box:eq(0)', 'in-progress');
                SpecHelper.expectElementText(elem, '.timer-box:eq(0) .header', 'EXAM TIMER');
                SpecHelper.expectElementText(elem, '.timer-box:eq(0) .message', 'YOU HAVE 12:00:00 REMAINING');

                now = new Date('2016/01/01 00:00:01');
                $interval.flush(1000);
                SpecHelper.expectElementText(elem, '.timer-box:eq(0) .message', 'YOU HAVE 11:59:59 REMAINING');

                SpecHelper.expectNoElement(elem, '');
            });

            it('should show as expected when stream is complete', () => {
                stream.ensureStreamProgress().complete = true;
                render();
                SpecHelper.expectNoElement(elem, '.timer-box');
            });

            it('should show as expected when stream is started but time_runs_out_at is null', () => {
                stream.ensureStreamProgress().time_runs_out_at = null;
                render();
                SpecHelper.expectElementText(elem, '.timer-box:eq(0) .header', 'EXAM TIMER');
                SpecHelper.expectElementText(
                    elem,
                    '.timer-box:eq(0) .message',
                    'After launching the next lesson, you will have 24:00:00 to complete the exam.',
                );
            });
        });

        describe('launch lesson with started stream', () => {
            it('should just launch if the stream is already started', () => {
                stream.ensureStreamProgress();

                const now = new Date('2016/01/02 00:00:00');
                jest.spyOn(stream, '_now').mockImplementation(() => now);

                render();

                jest.spyOn(firstLesson, 'launch').mockImplementation(() => {});
                SpecHelper.click(elem, '.lesson-box:eq(0) .launch-button');
                expect(firstLesson.launch).toHaveBeenCalled();
            });
        });

        describe('launch lesson with unstarted stream', () => {
            it('should show a confirm modal and launch if confirmed', () => {
                stream.lesson_streams_progress = null;

                const now = new Date('2016/01/02 00:00:00');
                jest.spyOn(stream, '_now').mockImplementation(() => now);

                render();

                jest.spyOn(firstLesson, 'launch').mockImplementation(() => {});
                jest.spyOn(DialogModal, 'alert');
                jest.spyOn(DialogModal, 'hideAlerts').mockImplementation(() => {});

                SpecHelper.click(elem, '.lesson-box:eq(0) .launch-button');
                expect(firstLesson.launch).not.toHaveBeenCalled();
                expect(DialogModal.hideAlerts).not.toHaveBeenCalled();

                const body = $('body');
                SpecHelper.expectElementText(
                    body,
                    '.modal',
                    'Ready to begin? You’ll have 24 hours from the time you start the exam to complete it.Start Exam',
                );
                SpecHelper.click(body, '.modal button.go');
                expect(firstLesson.launch).toHaveBeenCalled();
                expect(DialogModal.hideAlerts).toHaveBeenCalled();
            });
        });

        it('should show exam score based on official_test_Score', () => {
            const streamProgress = stream.ensureStreamProgress();
            streamProgress.complete = true;
            streamProgress.official_test_score = 0.62;
            render();
            SpecHelper.expectElementText(elem, '.exam-score-box', 'Exam Score 62%');
        });

        it('should NOT display assessment-score-arc when exam is incomplete', () => {
            render();
            SpecHelper.expectNoElement(elem, 'assessment-score-arc');
        });

        it('should display assessment-score-arc when exam is complete', () => {
            stream.ensureStreamProgress().complete = true;
            render();
            SpecHelper.expectElements(elem, 'assessment-score-arc', 6); // 3 lessons with two arcs; one for mobile, one for desktop
        });
    });

    describe('with coming soon lessons', () => {
        beforeEach(() => {
            // simplify things by having just one lesson
            stream.lessons = [stream.lessons[0]];
            stream.chapters[0].lesson_hashes[0].coming_soon = true;
        });

        afterEach(() => {
            // cleanup
            stream.chapters[0].lesson_hashes[0].coming_soon = false;
        });

        it('should show only coming soon stuff', () => {
            render();

            SpecHelper.expectNoElement(elem, '.lesson-time-estimate');
            SpecHelper.expectNoElement(elem, '.lesson-description');

            SpecHelper.expectElementText(elem, '.lesson-coming-soon-label', 'COMING SOON');
            SpecHelper.expectElementText(elem, '.coming-soon-block', 'COMINGSOON');
        });

        it('should show override assessment styling', () => {
            // should hide even assessment stuff
            stream.lessons[0].assessment = true;
            render();
            SpecHelper.expectNoElement(elem, 'assessment-score-arc');
        });

        it('should not be launchable', () => {
            render();
            jest.spyOn(scope, 'launchLesson').mockImplementation(() => {});
            jest.spyOn(stream.lessons[0], 'launch').mockImplementation(() => {});
            SpecHelper.click(elem, '.lesson-details-box', 0);
            expect(scope.launchLesson).toHaveBeenCalled();
            expect(stream.lessons[0].launch).not.toHaveBeenCalled();
        });

        it('should show a special message if all non-coming soon lessons are complete', () => {
            stream = Stream.fixtures.getInstance();
            const removedChapter = stream.chapters.pop();

            stream.lessons = stream.lessons.filter(lesson => removedChapter.lessonIds.includes(lesson.id));

            stream.chapters[0].lesson_hashes[1].coming_soon = true;

            render();

            SpecHelper.expectNoElement(elem, '.beta-complete-coming-soon');

            stream.lessons[0].lesson_progress = {
                complete: true,
            };
            render();

            SpecHelper.expectElement(elem, '.beta-complete-coming-soon');
        });
    });

    describe('with no currentUser', () => {
        it('should work', () => {
            SpecHelper.stubNoCurrentUser();
            render();
            SpecHelper.expectElements(elem, '.container');
        });

        it('should show a page if the course is in SMARTER', () => {
            SpecHelper.stubNoCurrentUser();
            render();
            SpecHelper.expectElement(elem, '.course-description-box');
        });
    });

    describe('launching lessons', () => {
        let lesson;

        beforeEach(() => {
            SpecHelper.stubCurrentUser('learner');
            lesson = stream.orderedLessons[0];
            render();
            jest.spyOn(lesson, 'launch').mockImplementation(() => {});
        });

        it('should set the correct linkId when clicking start course', () => {
            SpecHelper.click(elem, '.start-course-box button');
            expect(lesson.launch).toHaveBeenCalledWith('stream_dashboard_start_course');
        });

        it('should set the correct linkId when clicking a lesson box', () => {
            SpecHelper.click(elem, '.lesson-box button', 0);
            expect(lesson.launch).toHaveBeenCalledWith('stream_dashboard_START');
        });

        it('should disable lesson-box when canLaunch is false', () => {
            jest.spyOn(ContentAccessHelper.prototype, 'canLaunch', 'get').mockReturnValue(false);
            render();
            scope.$apply();
            SpecHelper.expectElementDisabled(elem, '.lesson-box a');
        });
    });

    describe('OfflineMode', () => {
        beforeEach(() => {
            SpecHelper.stubEventLogging();
        });

        it('should hide certificate download', () => {
            jest.spyOn(stream.lesson_streams_progress, 'certificateImageSrc').mockReturnValue('src');
            jest.spyOn(stream.lesson_streams_progress, 'complete', 'get').mockReturnValue(true);
            render();
            SpecHelper.expectElement(elem, '.certificate-download');
            offlineModeManager.inOfflineMode = true;
            scope.$digest();
            SpecHelper.expectNoElement(elem, '.certificate-download');
        });

        it('should disable summary links inside of lesson link boxes', () => {
            render();
            SpecHelper.expectDoesNotHaveClass(
                elem,
                '.lesson-box:eq(0) .unlockable-summaries .stream-summary',
                'disabled',
            );
            offlineModeManager.inOfflineMode = true;
            scope.$digest();
            SpecHelper.expectHasClass(elem, '.lesson-box:eq(0) .unlockable-summaries .stream-summary', 'disabled');
        });

        it('should disable links inside of resource_links', () => {
            setupUnlockedResources();
            assertOneResourceUnlocked();
            SpecHelper.expectElementEnabled(elem, '.resources-box .stream-summary a');
            SpecHelper.expectElementEnabled(elem, '.resources-box .downloads-list a');
            SpecHelper.expectElementEnabled(elem, '.resources-box .more-information a');
            SpecHelper.expectElement(elem, '.download-instructions');
            offlineModeManager.inOfflineMode = true;
            scope.$digest();
            SpecHelper.expectElementDisabled(elem, '.resources-box .stream-summary a');
            SpecHelper.expectElementDisabled(elem, '.resources-box .downloads-list a');
            SpecHelper.expectElementDisabled(elem, '.resources-box .more-information a');
            SpecHelper.expectNoElement(elem, '.download-instructions');
        });

        it('should hide share stream sidebar', () => {
            render();
            SpecHelper.expectElements(elem, 'share-stream-sidebar > div', 3);
            SpecHelper.expectElements(elem, '.bookmark-button', 2);

            offlineModeManager.inOfflineMode = true;
            scope.$digest();
            SpecHelper.expectNoElement(elem, 'share-stream-sidebar > div');
            SpecHelper.expectNoElement(elem, '.bookmark-button');
        });

        it('should not request recommended courses', () => {
            offlineModeManager.inOfflineMode = true;
            const otherStreams = [Stream.fixtures.getInstance()];
            stream.recommended_stream_ids = [otherStreams[0].id, 'some_other_id'];
            jest.spyOn(Stream, 'indexForCurrentUser');
            render();
            expect(Stream.indexForCurrentUser).not.toHaveBeenCalled();
        });

        it('should not request related courses', () => {
            offlineModeManager.inOfflineMode = true;
            const otherStreams = [Stream.fixtures.getInstance()];
            stream.related_stream_ids = [otherStreams[0].id, 'some_other_id'];
            jest.spyOn(Stream, 'indexForCurrentUser');
            render();
            expect(Stream.indexForCurrentUser).not.toHaveBeenCalled();
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
        offlineModeManager.inOfflineMode = false;
    });

    function render(showCallback) {
        Stream.resetCache(); // require the show call

        const renderer = SpecHelper.renderer();

        if (showCallback) {
            showCallback();
        } else {
            Stream.expect('show').returns(stream);
        }

        renderer.render(`<stream-dashboard stream-id="${stream.id}"></stream-dashboard>`);
        elem = renderer.elem;
        Stream.flush('show');
        scope = elem.isolateScope();

        // Stream.expect causes a copy of stream to get onto the scope.  Grab the real
        // one that's on the scope and make it available to these specs
        stream = elem.isolateScope().stream;
        $timeout.flush();
    }

    function setupUnlockedResources() {
        expect(stream.summaries).not.toBeUndefined(); // sanity check
        stream.lessons[0].lesson_progress = {
            complete: true,
        };
        render();
    }

    function assertOneResourceUnlocked() {
        SpecHelper.expectElements(elem, '.resources-box', 1);
        SpecHelper.expectElements(elem, '.resources-box .stream-summary', 2);

        SpecHelper.expectElementText(
            elem,
            '.resources-box .stream-summary a.unlocked',
            'Unlocked! Tap to download Summary1',
            0,
        );
        SpecHelper.expectElementText(
            elem,
            '.resources-box .stream-summary div.locked',
            `Complete "${stream.lessons[2].title}" to unlock! Summary2`,
            0,
        );
    }
});
