import 'AngularSpecHelper';
import 'Lessons/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import studentDashboardResearchResourcesLocales from 'Lessons/locales/lessons/stream/student_dashboard_research_resources-en.json';
import libraryServicesLocales from 'Lessons/locales/lessons/stream/library_services-en.json';

setSpecLocales(studentDashboardResearchResourcesLocales, libraryServicesLocales);

describe('FrontRoyal.Lessons.StudentDashboardResearchResources', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let DialogModal;
    let $location;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                DialogModal = $injector.get('DialogModal');
                $location = $injector.get('$location');
            },
        ]);

        SpecHelper.stubConfig();
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.render('<student-dashboard-research-resources></student-dashboard-research-resources>');
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    describe('viewLibraryServices', () => {
        it('should open modal', () => {
            render();
            jest.spyOn(DialogModal, 'alert');
            scope.viewLibraryServices();
            expect(DialogModal.alert).toHaveBeenCalled();
        });
    });

    describe('library services query param', () => {
        it('should open modal', () => {
            jest.spyOn(DialogModal, 'alert');
            jest.spyOn($location, 'search').mockReturnValue({
                libraryservices: '1',
            });
            render();
            expect(DialogModal.alert).toHaveBeenCalled();
        });
    });
});
