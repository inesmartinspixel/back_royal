import 'AngularSpecHelper';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';
import 'Lessons/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import studentDashboardProjectResourcesLocales from 'Lessons/locales/lessons/stream/student_dashboard_project_resources-en.json';

setSpecLocales(studentDashboardProjectResourcesLocales);

describe('Lessons.Stream.StudentDashboardProjectResourcesDir', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let Cohort;
    let cohort;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'SpecHelper');

        angular.mock.inject([
            '$injector',

            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                Cohort = $injector.get('Cohort');

                $injector.get('CohortFixtures');
            },
        ]);

        cohort = Cohort.new({
            program_type: 'paid_cert_data_analytics',
            name: 'A Random Cohort',
            project_submission_email: 'project_submission@pedago.com',
            learner_projects: [
                {
                    project_documents: [
                        {
                            file_file_name: 'foo.pdf',
                            title: 'foo',
                            url: 'foo.com',
                        },
                        {
                            file_file_name: 'bar.pdf',
                            title: 'bar',
                            url: 'bar.com',
                        },
                    ],
                },
            ],
        });
        render();
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.relevantCohort = cohort;
        renderer.render(
            '<student-dashboard-project-resources relevant-cohort="relevantCohort"></student-dashboard-project-resources>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    it('should display project_submission_email', () => {
        expect(scope.relevantCohort.getFormattedProjectSubmissionEmail()).toEqual(
            'project_submission+ARandomCohort@pedago.com',
        );
        expect(scope.mailtoLink).toEqual('mailto:project_submission+ARandomCohort@pedago.com');
        SpecHelper.expectElement(elem, '.submission-email-container');
        SpecHelper.expectElementAttr(elem, '.submission-email-container a', 'href', scope.mailtoLink);
    });

    it('should display project_documents', () => {
        SpecHelper.expectElements(elem, '.small-doc', 2);
        SpecHelper.expectElementAttr(
            elem,
            '.small-doc:eq(0)',
            'download',
            cohort.learner_projects[0].project_documents[0].file_file_name,
        );
        SpecHelper.expectElementAttr(
            elem,
            '.small-doc:eq(0)',
            'href',
            cohort.learner_projects[0].project_documents[0].url,
        );
        SpecHelper.expectElementText(elem, '.title-text:eq(0)', cohort.learner_projects[0].project_documents[0].title);
        SpecHelper.expectElementAttr(
            elem,
            '.small-doc:eq(1)',
            'download',
            cohort.learner_projects[0].project_documents[1].file_file_name,
        );
        SpecHelper.expectElementAttr(
            elem,
            '.small-doc:eq(1)',
            'href',
            cohort.learner_projects[0].project_documents[1].url,
        );
        SpecHelper.expectElementText(elem, '.title-text:eq(1)', cohort.learner_projects[0].project_documents[1].title);
    });
});
