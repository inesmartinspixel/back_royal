import 'AngularSpecHelper';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';
import 'Lessons/angularModule';
import 'Playlists/angularModule/spec/_mock/fixtures/playlists';
import 'Lessons/angularModule/spec/_mock/fixtures/streams';
import setSpecLocales from 'Translation/setSpecLocales';
import studentDashboardLearningBoxLocales from 'Lessons/locales/lessons/stream/student_dashboard_learning_box-en.json';
import streamLinkBoxLocales from 'Lessons/locales/lessons/stream/stream_link_box-en.json';

setSpecLocales(studentDashboardLearningBoxLocales, streamLinkBoxLocales);

describe('FrontRoyal.Lessons.StudentDashboardLearningBox', () => {
    let $injector;
    let $location;
    let $timeout;
    let Cohort;
    let RouteAnimationHelper;
    let Stream;
    let SpecHelper;
    let renderer;
    let elem;
    let user;
    let playlists;
    let activePlaylist;
    let keepLearningStream;
    let keepLearningLesson;
    let recommendedPlaylist;
    let activatePlaylist;
    let relevantCohort;
    let hasConcentrations;
    let offlineModeManager;
    let offlineStreams;
    let scope;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'FrontRoyal.Playlists', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                RouteAnimationHelper = $injector.get('RouteAnimationHelper');
                $location = $injector.get('$location');
                $timeout = $injector.get('$timeout');
                SpecHelper = $injector.get('SpecHelper');
                Stream = $injector.get('Lesson.Stream');
                Cohort = $injector.get('Cohort');
                offlineModeManager = $injector.get('offlineModeManager');
                offlineModeManager.inOfflineMode = false;
                $injector.get('ConfigFactory');
                $injector.get('PlaylistFixtures');
                $injector.get('StreamFixtures');
                $injector.get('CohortFixtures');

                SpecHelper.stubEventLogging();
            },
        ]);

        // Mock a user
        user = SpecHelper.stubCurrentUser('learner');

        SpecHelper.stubConfig();

        // Mock out resetCache so we don't blow away our mock data
        jest.spyOn(Stream, 'resetCache').mockImplementation(() => {});

        // Create mock Playlists and Streams
        relevantCohort = Cohort.fixtures.getInstance();
        user.relevant_cohort = relevantCohort;

        playlists = Cohort.fixtures.getPlaylistsForCohort(relevantCohort);

        activePlaylist = playlists[0];
        keepLearningStream = playlists[0].streams[0];
        keepLearningLesson = playlists[0].streams[0].lessons[0];
        recommendedPlaylist = undefined;
        hasConcentrations = false;

        activatePlaylist = () => {
            // no-op
        };
    });

    afterEach(() => {
        SpecHelper.cleanup();
        offlineModeManager.inOfflineMode = false;
    });

    function render(options) {
        renderer = SpecHelper.renderer();

        angular.extend(
            renderer.scope,
            {
                learningBoxMode: {
                    mode: 'active_playlist',
                },
                hasConcentrations,
                currentUser: user,
                playlists,
                playlist: activePlaylist,
                keepLearningStream,
                keepLearningLesson,
                recommendedPlaylist,
                activatePlaylist,
                relevantCohort,
                readyToShowPlaylistMap: true,
            },
            options,
        );

        renderer.render(
            '<student-dashboard-learning-box ' +
                'has-concentrations="hasConcentrations" ' +
                'learning-box-mode="learningBoxMode" ' +
                'current-user="currentUser" ' +
                'playlists="playlists" ' +
                'active-playlist="playlist" ' +
                'keep-learning-stream="keepLearningStream" ' +
                'keep-learning-lesson="keepLearningLesson" ' +
                'recommended-playlist="recommendedPlaylist" ' +
                'activate-playlist="activatePlaylist" ' +
                'relevant-cohort="relevantCohort" ' +
                'ready-to-show-playlist-map="readyToShowPlaylistMap" ' +
                'streams="streams"' +
                '</student-dashboard-learning-box>',
        );

        offlineStreams = [];
        const $q = $injector.get('$q');
        elem = renderer.elem;
        scope = elem.isolateScope();
        jest.spyOn(scope, 'getOfflineStreams').mockImplementation(() => $q.resolve(offlineStreams));

        $timeout.flush(); // flush the waitedABeat timeout
    }

    describe('course mode', () => {
        const renderOptions = {
            learningBoxMode: {
                mode: 'course',
            },
        };

        it('should display the expected course info', () => {
            render(renderOptions);

            SpecHelper.expectElementText(
                elem,
                '.course-recommendation-box .title-text',
                keepLearningStream.title.toUpperCase(),
            );
            SpecHelper.expectElementText(
                elem,
                '.course-recommendation-box .description',
                keepLearningStream.description,
            );

            SpecHelper.expectElement(elem, 'student-dashboard-lesson-link-box');
            SpecHelper.expectElementText(
                elem,
                'student-dashboard-lesson-link-box .lesson-index-box .chapter-index',
                `CHAPTER ${keepLearningLesson.chapterIndex() + 1}`,
            );
            SpecHelper.expectElementText(
                elem,
                'student-dashboard-lesson-link-box .lesson-index-box .lesson-index:eq(1)',
                `LESSON ${keepLearningLesson.chapterLessonsIndex() + 1}`,
            );
            SpecHelper.expectNoElement(elem, 'student-dashboard-lesson-link-box .stream-title');
            SpecHelper.expectElementText(
                elem,
                'student-dashboard-lesson-link-box .lesson-title',
                `NEXT UP: ${keepLearningLesson.title.toUpperCase()}`,
            );
        });

        it('should navigate to course page when title clicked', () => {
            render(renderOptions);

            jest.spyOn(RouteAnimationHelper.prototype, 'animatePathChange').mockImplementation(() => {});

            SpecHelper.click(elem, '.course-recommendation-box .title');
            expect(RouteAnimationHelper.prototype.animatePathChange).toHaveBeenCalledWith(
                keepLearningStream.streamDashboardPath,
                'slide-left',
            );
        });

        it('should navigate to lesson when button clicked', () => {
            render(renderOptions);

            jest.spyOn($location, 'url').mockImplementation(() => {});
            $location.url.mockClear();

            SpecHelper.click(elem, 'student-dashboard-lesson-link-box .launch-button');
            expect($location.url).toHaveBeenCalledWith(keepLearningLesson.launchUrl);
        });

        describe('hr caption', () => {
            it('should display correct text when learner has not started any courses', () => {
                jest.spyOn(Stream, 'keepLearningStreamAndLesson').mockReturnValue(false);
                renderOptions.streams = [Stream.fixtures.getInstance()];
                render(renderOptions);
                SpecHelper.expectElementText(elem, '.course-recommendation-box-mobile .hr-caption', 'START LEARNING');
            });

            it('should display correct text when learner has started any courses', () => {
                jest.spyOn(Stream, 'keepLearningStreamAndLesson').mockReturnValue(true);
                renderOptions.streams = [Stream.fixtures.getInstance()];
                render(renderOptions);
                SpecHelper.expectElementText(elem, '.course-recommendation-box-mobile .hr-caption', 'KEEP LEARNING');
            });
        });
    });

    describe('playlists mode', () => {
        const renderOptions = {
            learningBoxMode: {
                mode: 'playlists',
            },
        };

        beforeEach(() => {
            Object.defineProperty(user, 'programType', {
                value: 'mba',
                configurable: true,
            });
        });

        describe('with playlist', () => {
            it('should show basic info above playlists', () => {
                render(renderOptions);

                SpecHelper.expectElement(elem, '.title-text .playlist-title');
                SpecHelper.expectNoElement(elem, '.title-text .playlist-title.mba');

                SpecHelper.expectElement(elem, '.title-text .playlist-subtitle');
                SpecHelper.expectNoElement(elem, '.title-text .playlist-subtitle.mba');

                SpecHelper.expectElement(elem, '.playlists-top-box .description');
                SpecHelper.expectNoElement(elem, '.playlists-top-box .description.mba');

                SpecHelper.expectElement(elem, '.playlists-toggle-button');
            });

            it('should display the correct number of playlists', () => {
                render(renderOptions);

                SpecHelper.expectElements(elem, '.playlist-box', playlists.length);
            });

            it('should show active playlist highlighted', () => {
                render(renderOptions);

                SpecHelper.expectNoElement(elem, '.playlist-box .status-recommended');
                SpecHelper.expectElement(elem, '.playlist-box.active');
                SpecHelper.expectElementText(elem, '.playlist-box.active .title', activePlaylist.title.toUpperCase());
                SpecHelper.expectElement(elem, '.playlist-box.active .status-active');
            });

            it("should show active playlist over recommended playlist if they're the same", () => {
                recommendedPlaylist = activePlaylist;
                render(renderOptions);

                SpecHelper.expectNoElement(elem, '.playlist-box.recommended');
                SpecHelper.expectNoElement(elem, '.playlist-box .status-recommended');
                SpecHelper.expectElement(elem, '.playlist-box.active');
                SpecHelper.expectElementText(elem, '.playlist-box.active .title', activePlaylist.title.toUpperCase());
                SpecHelper.expectElement(elem, '.playlist-box.active .status-active');
            });

            it('should show recommended playlist highlighted', () => {
                recommendedPlaylist = activePlaylist;
                activePlaylist = undefined;

                render(renderOptions);

                SpecHelper.expectElement(elem, '.playlist-box.recommended');
                SpecHelper.expectElement(elem, '.playlist-box.recommended .status-recommended');
                SpecHelper.expectElementText(
                    elem,
                    '.playlist-box.recommended .title',
                    recommendedPlaylist.title.toUpperCase(),
                );
                SpecHelper.expectElement(elem, '.playlist-box.recommended .status-recommended');
            });

            it('should hide close button if no active playlist to force a selection', () => {
                recommendedPlaylist = activePlaylist;
                activePlaylist = undefined;

                render(renderOptions);

                SpecHelper.expectNoElement(elem, '.playlists-toggle-button');
            });

            describe('completion header', () => {
                beforeEach(() => {
                    activePlaylist.streams.forEach(stream => {
                        stream.lesson_streams_progress.complete = true;
                    });
                });

                it('should not show a completion header if no recommended playlist', () => {
                    recommendedPlaylist = undefined;

                    render(renderOptions);

                    SpecHelper.expectNoElement(elem, '.header-box.complete');
                });

                it('should not show a completion header if recommended playlist matches active one', () => {
                    recommendedPlaylist = activePlaylist;

                    render(renderOptions);

                    SpecHelper.expectNoElement(elem, '.header-box.complete');
                });

                it('should show a completion header', () => {
                    recommendedPlaylist = playlists[1];

                    render(renderOptions);

                    SpecHelper.expectElement(elem, '.header-box.complete');
                });
            });
        });

        describe('with concentration', () => {
            beforeEach(() => {
                user.addGroup('SMARTER');
                hasConcentrations = true;

                Object.defineProperty(user, 'isMBA', {
                    value: true,
                    configurable: true,
                });

                Object.defineProperty(user, 'programType', {
                    value: 'mba',
                    configurable: true,
                });

                user.mba_enabled = true;
            });

            it('should show basic info above concentrations', () => {
                render(renderOptions);

                SpecHelper.expectElement(elem, '.title-text .playlist-title.mba');
                SpecHelper.expectElement(elem, '.title-text .playlist-subtitle.mba');
                SpecHelper.expectElement(elem, '.playlists-top-box .description.mba');
                SpecHelper.expectElement(elem, '.playlists-toggle-button');
            });

            it('should show special messaging if user is rejected or expelled and in the MBA', () => {
                Object.defineProperty(user, 'isRejectedOrExpelled', {
                    value: true,
                });
                render(renderOptions);
                SpecHelper.expectElementText(elem, '.playlist-title', 'OPEN COURSES');
                SpecHelper.expectElementText(
                    elem,
                    '.description',
                    'We hope you enjoy our open business courses. Tap any concentration to activate it and start learning.',
                );
            });

            it('should show standard messaging if user is rejected or expelled for a Certificate', () => {
                Object.defineProperty(user, 'isRejectedOrExpelled', {
                    value: true,
                });
                Object.defineProperty(user, 'isMBA', {
                    value: false,
                    configurable: true,
                });
                Object.defineProperty(user, 'isEMBA', {
                    value: false,
                    configurable: true,
                });

                user.mba_enabled = false;

                user.relevant_cohort = Cohort.fixtures.getInstance({
                    program_type: 'the_business_certificate',
                    title: 'A Certificate Cohort',
                });
                render(renderOptions);
                SpecHelper.expectElementText(elem, '.playlist-title', 'A CERTIFICATE COHORT');
                SpecHelper.expectElementText(
                    elem,
                    '.description',
                    'The A Certificate Cohort program delivers the knowledge and skills you need to succeed in today’s business environment. Tap any concentration to activate it and start learning.',
                );
            });

            it('should show standard messaging if user is rejected or expelled for career_network_only', () => {
                Object.defineProperty(user, 'isRejectedOrExpelled', {
                    value: true,
                });
                Object.defineProperty(user, 'isMBA', {
                    value: false,
                    configurable: true,
                });
                Object.defineProperty(user, 'isEMBA', {
                    value: false,
                    configurable: true,
                });

                user.mba_enabled = false;

                user.relevant_cohort = Cohort.fixtures.getInstance({
                    program_type: 'career_network_only',
                    title: 'A Cohort',
                });
                render(renderOptions);
                SpecHelper.expectElementText(elem, '.playlist-title', 'OPEN COURSES');
                SpecHelper.expectElementText(
                    elem,
                    '.description',
                    'We hope you enjoy our open business courses. Tap any concentration to activate it and start learning.',
                );
            });

            it('should respect the custom ordering', () => {
                // sanity checks
                relevantCohort = Cohort.fixtures.getInstance({
                    playlists,
                });
                const playlistCollection = relevantCohort.playlist_collections[0];
                expect(playlists[0].locale_pack.id).toEqual(playlistCollection.required_playlist_pack_ids[0]);
                expect(playlists[2].locale_pack.id).toEqual(playlistCollection.required_playlist_pack_ids[2]);
                expect(playlists[0].locale_pack.id).not.toEqual(playlistCollection.required_playlist_pack_ids[2]);

                // swap playlist ordering in cohort
                let tmp = playlistCollection.required_playlist_pack_ids[0];
                playlistCollection.required_playlist_pack_ids[0] = playlistCollection.required_playlist_pack_ids[2];
                playlistCollection.required_playlist_pack_ids[2] = tmp;

                // render
                render(renderOptions);

                // prepare expectations
                const elements = SpecHelper.expectElements(elem, '.playlist-box .title');
                const titles = _.map(elements, item => SpecHelper.trimText($(item).text().toUpperCase()));
                const expectedTitles = playlists.map(playlist => playlist.title.toUpperCase());
                tmp = expectedTitles[0];
                expectedTitles[0] = expectedTitles[2];
                expectedTitles[2] = tmp;

                expect(expectedTitles).toEqual(titles);
            });
        });
    });

    describe('active playlist mode', () => {
        const renderOptions = {
            learningBoxMode: {
                mode: 'active_playlist',
            },
        };

        describe('with playlist', () => {
            beforeEach(() => {
                user.relevant_cohort = null;
                Object.defineProperty(user, 'programType', {
                    value: 'external',
                    configurable: true,
                });
            });

            it('should show playlist title, description, keep learning', () => {
                render(renderOptions);

                SpecHelper.expectElementText(elem, '.title-text .playlist-title', activePlaylist.title.toUpperCase());
                SpecHelper.expectElementText(
                    elem,
                    '.title-text .playlist-subtitle',
                    `${activePlaylist.streams.length} COURSES • 0 HOURS • 0% COMPLETE`,
                );
                SpecHelper.expectElementText(elem, '.active-playlist-box .description', activePlaylist.description);

                SpecHelper.expectElement(elem, 'student-dashboard-lesson-link-box');
                SpecHelper.expectNoElement(elem, 'student-dashboard-lesson-link-box .lesson-index-box');
                SpecHelper.expectElementText(
                    elem,
                    'student-dashboard-lesson-link-box .stream-title',
                    keepLearningStream.title.toUpperCase(),
                );
                SpecHelper.expectElementText(
                    elem,
                    'student-dashboard-lesson-link-box .lesson-title',
                    `NEXT UP: ${keepLearningLesson.title.toUpperCase()}`,
                );

                SpecHelper.expectNoElement(elem, '.header-box.complete');
            });

            it('should show button to switch to playlists view', () => {
                render(renderOptions);

                SpecHelper.expectElementText(elem, '.playlists-toggle-button', 'SHOW PLAYLISTS');
            });

            it('should show completed state when entire playlist is complete', () => {
                activePlaylist.streams.forEach(stream => {
                    stream.lesson_streams_progress.complete = true;
                });
                render(renderOptions);

                SpecHelper.expectElementText(elem, '.header-box.complete', 'Great job! You completed this playlist.');
                SpecHelper.expectNoElement(elem, 'student-dashboard-lesson-link-box');
            });

            it('should navigate to course page when title clicked', () => {
                render(renderOptions);

                jest.spyOn(RouteAnimationHelper.prototype, 'animatePathChange').mockImplementation(() => {});

                SpecHelper.click(elem, 'student-dashboard-lesson-link-box .stream-title');
                expect(RouteAnimationHelper.prototype.animatePathChange).toHaveBeenCalledWith(
                    keepLearningStream.streamDashboardPath,
                    'slide-left',
                );
            });

            it('should navigate to lesson when keep learning button clicked', () => {
                render(renderOptions);

                jest.spyOn($location, 'url').mockImplementation(() => {});
                $location.url.mockClear();

                SpecHelper.click(elem, 'student-dashboard-lesson-link-box .launch-button');
                expect($location.url).toHaveBeenCalledWith(keepLearningLesson.launchUrl);
            });

            it('should show list of courses', () => {
                render(renderOptions);
                const list = SpecHelper.expectElement(elem, 'student-dashboard-learning-box-course-list');
                expect(list.isolateScope().streams).toEqual(activePlaylist.availableStreams);
                expect(list.isolateScope().keepLearningStream).toEqual(keepLearningStream);
                expect(list.isolateScope().playlist).toEqual(activePlaylist);
            });
        });

        describe('with MBA concentration', () => {
            beforeEach(() => {
                user.addGroup('SMARTER');
                hasConcentrations = true;

                user.relevant_cohort = Cohort.fixtures.getInstance({
                    program_type: 'mba',
                });
            });

            it('should show button to switch to mba view', () => {
                render(renderOptions);
                SpecHelper.expectElementText(elem, '.playlists-toggle-button', 'CURRICULUM');
            });

            it('should show completed state when entire concentration is complete', () => {
                activePlaylist.streams.forEach(stream => {
                    stream.lesson_streams_progress.complete = true;
                });
                render(renderOptions);

                SpecHelper.expectElementText(
                    elem,
                    '.header-box.complete',
                    'Great job! You completed this concentration.',
                );
                SpecHelper.expectNoElement(elem, 'student-dashboard-lesson-link-box');
            });
        });

        describe('with EMBA concentration', () => {
            beforeEach(() => {
                hasConcentrations = true;

                user.relevant_cohort = Cohort.fixtures.getInstance({
                    program_type: 'emba',
                });
            });

            it('should show button to switch to emba view', () => {
                render(renderOptions);
                SpecHelper.expectElementText(elem, '.playlists-toggle-button', 'CURRICULUM');
            });
        });

        describe('with Career Network concentration', () => {
            beforeEach(() => {
                hasConcentrations = true;

                user.relevant_cohort = Cohort.fixtures.getInstance({
                    program_type: 'career_network_only',
                });
            });

            it('should show button to switch to curriculum view', () => {
                render(renderOptions);
                SpecHelper.expectElementText(elem, '.playlists-toggle-button', 'SHOW COURSES');
            });
        });

        describe('with Certificate concentration', () => {
            beforeEach(() => {
                hasConcentrations = true;
            });

            it('should show button to switch to curriculum view', () => {
                _.each(Cohort.CERTIFICATE_PROGRAM_TYPES, programType => {
                    assertButtonTextWithProgramType(programType);
                });
            });

            function assertButtonTextWithProgramType(programType) {
                user.relevant_cohort = Cohort.fixtures.getInstance({
                    program_type: programType,
                });
                render(renderOptions);
                SpecHelper.expectElementText(elem, '.playlists-toggle-button', 'CURRICULUM');
            }
        });
    });

    describe('offline mode', () => {
        const renderOptions = {
            learningBoxMode: {
                mode: 'offline_mode',
            },
        };

        it('should show list of courses', () => {
            offlineStreams = [Stream.fixtures.getInstance(), Stream.fixtures.getInstance()];

            // We have to initally set inOfflineMode to false so that getOfflineStreams
            // will be called after we've set offlineStreams to have some contents. (We could
            // fix this once modularization is complete by using jest.mock to mock out getOfflineStreams
            // at a higher level)
            offlineModeManager.inOfflineMode = false;
            offlineModeManager.offlineModeAvailable = true;
            render(renderOptions);
            offlineModeManager.inOfflineMode = true;
            scope.$digest();
            const list = SpecHelper.expectElement(elem, 'student-dashboard-learning-box-course-list');
            expect(list.isolateScope().streams).toEqual(offlineStreams);
            expect(list.isolateScope().keepLearningStream).toEqual(keepLearningStream);
            expect(list.isolateScope().playlist).toBeUndefined();
        });
    });
});
