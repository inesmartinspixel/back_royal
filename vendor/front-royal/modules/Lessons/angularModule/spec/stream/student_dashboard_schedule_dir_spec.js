import 'AngularSpecHelper';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/streams';
import setSpecLocales from 'Translation/setSpecLocales';
import studentDashboardScheduleLocales from 'Lessons/locales/lessons/stream/student_dashboard_schedule-en.json';
import moment from 'moment-timezone';

setSpecLocales(studentDashboardScheduleLocales);

describe('FrontRoyal.Lessons.StudentDashboardSchedule', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let currentUser;
    let Cohort;
    let relevantCohort;
    let Stream;
    let streams;
    let Period;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                Cohort = $injector.get('Cohort');
                $injector.get('CohortFixtures');
                Stream = $injector.get('Lesson.Stream');
                $injector.get('StreamFixtures');
                Period = $injector.get('Period');
            },
        ]);

        currentUser = SpecHelper.stubCurrentUser();
        Object.defineProperty(currentUser, 'programType', {
            value: 'mba',
        });
        relevantCohort = Cohort.fixtures.getInstance();
        streams = [Stream.fixtures.getInstance()];

        SpecHelper.stubConfig({
            social_hashtag: '#some_hashtag',
        });
    });

    function render(params = {}) {
        Object.defineProperty(currentUser, 'lastCohortApplication', {
            value: angular.extend(params.application || {}, {
                status: 'accepted',
            }),
        });

        renderer = SpecHelper.renderer();
        renderer.scope.relevantCohort = relevantCohort;
        renderer.scope.streams = streams;
        renderer.render(
            '<student-dashboard-schedule relevant-cohort="relevantCohort" streams="streams"></student-dashboard-schedule>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    describe('curriculum_status', () => {
        it('should set status', () => {
            currentUser.curriculum_status = 'foo_status';
            render();
            expect(scope.status).toBe('foo_status');
        });

        describe('showSchedule', () => {
            it('should set to true if there is a current period and status is week_1, on_track, on_track_finished, or not_on_track, almost_there', () => {
                jest.spyOn(relevantCohort, 'currentPeriod', 'get').mockReturnValue(
                    Period.new({
                        startDate: new Date('January 1, 2018'),
                        endDate: new Date('February 2, 2018'),
                    }),
                );
                currentUser.curriculum_status = 'week_1';
                render();
                expect(scope.showSchedule).toBe(true);

                currentUser.curriculum_status = 'on_track';
                scope.$digest();
                expect(scope.showSchedule).toBe(true);

                currentUser.curriculum_status = 'on_track_finished';
                scope.$digest();
                expect(scope.showSchedule).toBe(true);

                currentUser.curriculum_status = 'not_on_track';
                scope.$digest();
                expect(scope.showSchedule).toBe(true);

                currentUser.curriculum_status = 'almost_there';
                scope.$digest();
                expect(scope.showSchedule).toBe(true);

                currentUser.curriculum_status = 'foo_status';
                scope.$digest();
                expect(scope.showSchedule).toBe(false);
            });

            it('should set to false if there is not a current period', () => {
                jest.spyOn(relevantCohort, 'currentPeriod', 'get').mockReturnValue(null);
                currentUser.curriculum_status = 'not_on_track';
                render();
                expect(scope.showSchedule).toBe(false);
            });
        });

        describe('showDiplomaCheck', () => {
            it('should set to true if before diplomaGeneration date and status is finished or graduated', () => {
                const futureDate = new Date();
                futureDate.setDate(futureDate.getDate() + 10);
                jest.spyOn(relevantCohort, 'diplomaGeneration', 'get').mockReturnValue(futureDate);
                currentUser.curriculum_status = 'finished';
                render();
                expect(scope.showDiplomaCheck).toBe(true);

                currentUser.curriculum_status = 'graduated';
                scope.$digest();
                expect(scope.showDiplomaCheck).toBe(true);

                currentUser.curriculum_status = 'honors';
                scope.$digest();
                expect(scope.showDiplomaCheck).toBe(true);

                currentUser.curriculum_status = 'foo_status';
                scope.$digest();
                expect(scope.showDiplomaCheck).toBe(false);
            });

            it('should set to false if after diplomaGeneration date', () => {
                const pastDate = new Date();
                pastDate.setDate(pastDate.getDate() - 10);
                jest.spyOn(relevantCohort, 'diplomaGeneration', 'get').mockReturnValue(pastDate);
                currentUser.curriculum_status = 'finished';
                render();
                expect(scope.showDiplomaCheck).toBe(false);
            });
        });
    });

    describe('diploma check box', () => {
        it('should show', () => {
            Object.defineProperty(relevantCohort, 'diplomaGeneration', {
                value: moment(new Date()).add(10, 'days').toDate(), // force the date to always be in the future
            });
            currentUser.curriculum_status = 'graduated';
            render();
            expect(scope.status).toBe('graduated');
            SpecHelper.expectElement(elem, '.diploma-check');
            SpecHelper.expectElementText(elem, '.diploma-check .name', currentUser.name);
        });

        it('should not show', () => {
            Object.defineProperty(relevantCohort, 'diplomaGeneration', {
                value: moment(new Date()).subtract(10, 'days').toDate(), // force the date to always be in the past
            });
            currentUser.curriculum_status = 'graduated';
            render();
            expect(scope.status).toBe('graduated');
            SpecHelper.expectNoElement(elem, '.diploma-check');
        });

        it('should have button to take you to documents area in settings', () => {
            render();
            scope.showDiplomaCheck = true;
            scope.$digest();
            jest.spyOn(scope, 'loadRoute').mockImplementation(() => {});
            SpecHelper.click(elem, '[name="update_address"]');
            expect(scope.loadRoute).toHaveBeenCalledWith('settings/documents');
        });
    });

    describe('periodTitle', () => {
        it('should show omit Week N:', () => {
            Object.defineProperty(relevantCohort, 'currentPeriod', {
                value: Period.new({
                    startDate: new Date('January 1, 2019'),
                    endDate: new Date('January 7, 2019'),
                    style: 'specialization',
                    specialization_style: 'specialization_1',
                    title: 'Week 20: Foo',
                }),
            });
            render();
            expect(scope.proxy.periodTitle).toBe('<span>Week 20:</span>Foo');
        });
    });

    describe('period dates', () => {
        it('should show one month, days, and year', () => {
            Object.defineProperty(relevantCohort, 'currentPeriod', {
                value: Period.new({
                    startDate: new Date('January 1, 2018'),
                    endDate: new Date('January 2, 2018'),
                }),
            });
            render();
            expect(scope.proxy.periodDates).toBe('January 1–2, 2018');
        });

        it('should show different months, days and year', () => {
            Object.defineProperty(relevantCohort, 'currentPeriod', {
                value: Period.new({
                    startDate: new Date('January 1, 2018'),
                    endDate: new Date('February 2, 2018'),
                }),
            });
            render();
            expect(scope.proxy.periodDates).toBe('January 1–February 2, 2018');
        });

        it('should show two fully different dates', () => {
            Object.defineProperty(relevantCohort, 'currentPeriod', {
                value: Period.new({
                    startDate: new Date('December 31, 2017'),
                    endDate: new Date('January 2, 2018'),
                }),
            });
            render();
            expect(scope.proxy.periodDates).toBe('December 31, 2017–January 2, 2018');
        });

        it('should show a range of all specialization periods', () => {
            relevantCohort.periods = [
                Period.new({
                    startDate: new Date('January 1, 2019'),
                    endDate: new Date('January 7, 2019'),
                    style: 'specialization',
                    specialization_style: 'specialization_1',
                }),
                Period.new({
                    startDate: new Date('January 8, 2019'),
                    endDate: new Date('January 14, 2019'),
                    style: 'specialization',
                    specialization_style: 'specialization_1',
                }),
                Period.new({
                    startDate: new Date('January 15, 2019'),
                    endDate: new Date('January 21, 2019'),
                    style: 'specialization',
                    specialization_style: 'specialization_1',
                }),
            ];

            Object.defineProperty(relevantCohort, 'currentPeriod', {
                value: relevantCohort.periods[1],
            });
            render();
            expect(scope.proxy.periodDates).toBe('January 1–21, 2019');
        });
    });

    describe('schedule box', () => {
        it('should show project documents', () => {
            Object.defineProperty(relevantCohort.periods[0], 'learner_projects', {
                value: [
                    {
                        project_documents: [
                            {
                                url: '#',
                                title: 'Project Document',
                            },
                        ], // stub a documents array
                    },
                ],
            });
            Object.defineProperty(relevantCohort, 'currentPeriod', {
                value: relevantCohort.periods[0], // force the current period to be the first one in the stubbed cohort
            });
            currentUser.curriculum_status = 'week_1';
            render();
            SpecHelper.expectElement(elem, '.schedule-box .date'); // test for the first/main schedule box
            SpecHelper.expectElement(elem, '[name=project_documents]');
        });

        it('should show required courses', () => {
            Object.defineProperty(relevantCohort.periods[1], 'requiredStreamLocalePackIds', {
                value: [streams[0].localePackId], // force a required pack id to match one of our streams
            });
            Object.defineProperty(relevantCohort, 'currentPeriod', {
                value: relevantCohort.periods[1], // force the current period to be the second one in the stubbed cohort
            });
            currentUser.curriculum_status = 'week_1';
            render();
            SpecHelper.expectElement(elem, '.schedule-box .date'); // test for the first/main schedule box
            SpecHelper.expectElement(elem, '[name=required_courses]');
        });

        it('should show optional courses', () => {
            Object.defineProperty(relevantCohort.periods[1], 'optionalStreamLocalePackIds', {
                value: [streams[0].localePackId], // force a required pack id to match one of our streams
            });
            Object.defineProperty(relevantCohort, 'currentPeriod', {
                value: relevantCohort.periods[1], // force the current period to be the second one in the stubbed cohort
            });
            currentUser.curriculum_status = 'week_1';
            render();
            SpecHelper.expectElement(elem, '.schedule-box .date'); // test for the first/main schedule box
            SpecHelper.expectElement(elem, '[name=optional_courses]');
        });
    });

    describe('exercises', () => {
        it('should show exercises box', () => {
            Object.defineProperty(relevantCohort.periods[0], 'exercises', {
                value: [
                    {
                        date: new Date(),
                        channel: '#engineering',
                    },
                ], // stub an exercises array
            });
            Object.defineProperty(relevantCohort, 'currentPeriod', {
                value: relevantCohort.periods[0], // force the current period to be the first one in the stubbed cohort
            });
            currentUser.curriculum_status = 'week_1';
            render();
            SpecHelper.expectElement(elem, '.schedule-box [name=exercises]'); // test for the exercises schedule box
        });
    });
});
