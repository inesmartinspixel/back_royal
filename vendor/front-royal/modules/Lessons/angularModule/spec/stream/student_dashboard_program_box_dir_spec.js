import 'AngularSpecHelper';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohort_applications';
import 'Lessons/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import studentDashboardProgramBoxLocales from 'Lessons/locales/lessons/stream/student_dashboard_program_box-en.json';

setSpecLocales(studentDashboardProgramBoxLocales);

describe('Lessons.Stream.StudentDashboardProgramBoxDir', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let currentUser;
    let Cohort;
    let relevantCohort;
    let foundationPlaylist;
    let Stream;
    let ErrorLogService;
    let EventLogger;
    let $window;
    let CohortApplication;
    let onClose;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'SpecHelper');

        angular.mock.inject([
            '$injector',

            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                Cohort = $injector.get('Cohort');
                $injector.get('CohortFixtures');
                Stream = $injector.get('Lesson.Stream');
                ErrorLogService = $injector.get('ErrorLogService');
                EventLogger = $injector.get('EventLogger');
                $window = $injector.get('$window');

                CohortApplication = $injector.get('CohortApplication');
                $injector.get('CohortApplicationFixtures');
            },
        ]);

        relevantCohort = Cohort.fixtures.getInstance();
        foundationPlaylist = Cohort.fixtures.getPlaylistsForCohort(relevantCohort)[0];
        foundationPlaylist.title = 'Business Foundations';
        onClose = jest.fn();

        currentUser = SpecHelper.stubCurrentUser();
        currentUser.relevant_cohort = relevantCohort;
        SpecHelper.stubEventLogging();
        SpecHelper.stubConfig();
    });

    describe('with programBoxStatus not_applied', () => {
        function assertProgramBoxStatusWhenNotApplied(brandName) {
            currentUser.cohort_applications = [];
            ['mba', 'emba', 'career_network_only', 'the_business_certificate'].forEach(programType => {
                Object.defineProperty(currentUser, 'programType', {
                    value: programType,
                    configurable: true,
                });
                currentUser.relevant_cohort.program_type = programType;

                render();
                SpecHelper.expectNoElement(elem, '.application-progress');
                SpecHelper.expectElementText(elem, '.title', `${brandName} MBA Programs`);
                SpecHelper.expectElementText(elem, '.apply-button', 'Complete Application');
                SpecHelper.expectElementText(
                    elem,
                    '.sub-title',
                    'Accepting applications for the next admissions cycle now.',
                );
            });
        }

        it('should show as not applied regardless of program type with Quantic branding', () => {
            assertProgramBoxStatusWhenNotApplied('Quantic');
        });
    });

    describe('messaging', () => {
        // Note that some of these states won't ever be shown because the student dashboard hides the program box for specific combinations of state and program_type.
        // However, it's simpler here to test all of the combinations, and allows for easier extension in the future. It also ensures that we explicitly think about
        // each of the combinations (and was certainly helpful to me when implementing a refactor as part of this).
        [
            {
                type: 'mba',
                cohortTitle: 'Quantic MBA June 2017',
                startDate: new Date('2017/06/01'),
                titleHtml: 'The Quantic <br> MBA',
                subtitle_not_applied: 'Accepting applications for the next admissions cycle now.',
                subtitle_applied:
                    'Thanks for applying! Complete Business Foundations to demonstrate your readiness for the degree.',
                subtitle_applied_foundations_complete:
                    "Great work! You'll hear from us soon regarding your application.",
                subtitle_pre_accepted:
                    'Congratulations on your acceptance to the Quantic MBA! Your start date is June 1, 2017. You will receive access to courses closer to this date.',
                subtitle_accepted:
                    'Complete all 9 concentrations to qualify for the degree. For the program schedule and complete requirements, visit our degree FAQ.',
                subtitle_accepted_not_started:
                    'Welcome to the Quantic MBA! To get started, complete Business Foundations, the first of 9 concentrations in the MBA curriculum.Complete all 9 to qualify for the degree. For the program schedule and complete requirements, visit our degree FAQ.',
                subtitle_accepted_foundations_complete:
                    'Complete all 9 concentrations to qualify for the degree. For the program schedule and complete requirements, visit our degree FAQ.',
                subtitle_accepted_not_started_foundations_complete:
                    "Welcome to the Quantic MBA! You've already completed Business Foundations, the first of 9 concentrations in the MBA curriculum.Complete all 9 to qualify for the degree. For the program schedule and complete requirements, visit our degree FAQ.",
                subtitle_program_complete:
                    'Complete all 9 concentrations to qualify for the degree. For the program schedule and complete requirements, visit our degree FAQ.',
                subtitle_program_failed:
                    'Complete all 9 concentrations to qualify for the degree. For the program schedule and complete requirements, visit our degree FAQ.',
                subtitle_rejected:
                    'Sorry, your application wasn’t accepted to the degree program. However, you can continue learning with our open courses to advance your knowledge, absolutely free. Reapply for the next class!',
                subtitle_rejected_no_reapply_cta:
                    "Though you won't be joining our MBA degree program, you can continue learning with our open courses to advance your knowledge, absolutely free.",
                subtitle_rejected_after_pre_accepted:
                    'Sorry you won’t be able to join the class. However, you can continue learning with our open courses to advance your knowledge, absolutely free. Reapply for the next class!',
                subtitle_rejected_after_pre_accepted_no_reapply_cta:
                    "Though you won't be joining our MBA degree program, you can continue learning with our open courses to advance your knowledge, absolutely free.",
                subtitle_expelled:
                    'Your MBA status is no longer active. We hope you enjoy continued access to select courses from the full program.For more information, contact us at mba@quantic.edu',
                subtitle_deferred:
                    "You've deferred your MBA enrollment. Please note that you are only allowed to defer enrollment twice before you have to reapply to the MBA. If you have any questions, please email us at mba@quantic.edu.",
                expelled_status: 'Your enrollment is no longer active.',
                rejected_status: 'Sorry, you were notaccepted. Reapply!',
                rejected_status_no_reapply_cta: 'Sorry, you were notaccepted.',
                deferred_status: 'You’ve deferred your enrollment.',
                getter: 'isMBA',
            },
            {
                type: 'emba',
                cohortTitle: 'Quantic EMBA June 2017',
                startDate: new Date('2017/06/01'),
                titleHtml: 'The Quantic <br> EMBA',
                subtitle_not_applied: 'Accepting applications for the next admissions cycle now.',
                subtitle_applied:
                    'Thanks for applying! Complete Business Foundations to demonstrate your readiness for the degree.',
                subtitle_applied_foundations_complete:
                    "Great work! You'll hear from us soon regarding your application.",
                subtitle_pre_accepted_not_registered:
                    'Congratulations on your acceptance to the Quantic EMBA! To confirm your place in the class, please register by May 24, 2017.',
                subtitle_pre_accepted_registered:
                    'Congratulations on your acceptance to the Quantic EMBA! You have registered and secured your place in the upcoming class. Your start date is June 1, 2017. You will receive access to courses closer to this date.',
                subtitle_accepted:
                    'Complete all 9 concentrations to qualify for the degree. For the program schedule and complete requirements, visit our degree FAQ.',
                subtitle_accepted_not_started:
                    'Welcome to the Quantic EMBA! To get started, complete Business Foundations, the first of 9 concentrations in the EMBA curriculum.Complete all 9 to qualify for the degree. For the program schedule and complete requirements, visit our degree FAQ.',
                subtitle_accepted_foundations_complete:
                    'Complete all 9 concentrations to qualify for the degree. For the program schedule and complete requirements, visit our degree FAQ.',
                subtitle_accepted_not_started_foundations_complete:
                    "Welcome to the Quantic EMBA! You've already completed Business Foundations, the first of 9 concentrations in the EMBA curriculum.Complete all 9 to qualify for the degree. For the program schedule and complete requirements, visit our degree FAQ.",
                subtitle_program_complete:
                    'Complete all 9 concentrations to qualify for the degree. For the program schedule and complete requirements, visit our degree FAQ.',
                subtitle_program_failed:
                    'Complete all 9 concentrations to qualify for the degree. For the program schedule and complete requirements, visit our degree FAQ.',
                subtitle_rejected:
                    'Sorry, your application wasn’t accepted to the degree program. However, you can continue learning with our open courses to advance your knowledge, absolutely free. Reapply for the next class!',
                subtitle_rejected_no_reapply_cta:
                    "Though you won't be joining our EMBA degree program, you can continue learning with our open courses to advance your knowledge, absolutely free.",
                subtitle_rejected_after_pre_accepted:
                    'Sorry you won’t be able to join the class. However, you can continue learning with our open courses to advance your knowledge, absolutely free. Reapply for the next class!',
                subtitle_rejected_after_pre_accepted_no_reapply_cta:
                    "Though you won't be joining our EMBA degree program, you can continue learning with our open courses to advance your knowledge, absolutely free.",
                subtitle_expelled:
                    'Your EMBA status is no longer active. We hope you enjoy continued access to select courses from the full program.For more information, contact us at emba@quantic.edu',
                subtitle_deferred:
                    "You've deferred your EMBA enrollment. Please note that you are only allowed to defer enrollment twice before you have to reapply to the EMBA. If you have any questions, please email us at emba@quantic.edu.",
                expelled_status: 'Your enrollment is no longer active.',
                rejected_status: 'Sorry, you were notaccepted. Reapply!',
                rejected_status_no_reapply_cta: 'Sorry, you were notaccepted.',
                deferred_status: 'You’ve deferred your enrollment.',
                registered_status: 'You are registered for the class.',
                getter: 'isEMBA',
            },
            {
                type: 'career_network_only',
                cohortTitle: 'Smartly Career Network',
                titleHtml: 'Smartly <br> Talent',
                subtitle_not_applied: 'Accepting applications for the next hiring cycle now.',
                subtitle_applied:
                    "Thanks for applying - we're reviewing your profile now! You can always check your status in the Careers tab.",
                subtitle_applied_foundations_complete:
                    "Thanks for applying - we're reviewing your profile now! You can always check your status in the Careers tab.",
                subtitle_accepted:
                    'Welcome to the Career Network! You may sample our open courses while you wait for inquiries from companies.',
                subtitle_accepted_not_started:
                    'Welcome to the Career Network! You may sample our open courses while you wait for inquiries from companies.',
                subtitle_accepted_foundations_complete:
                    'Welcome to the Career Network! You may sample our open courses while you wait for inquiries from companies.',
                subtitle_accepted_not_started_foundations_complete:
                    'Welcome to the Career Network! You may sample our open courses while you wait for inquiries from companies.',
                subtitle_program_complete:
                    'Welcome to the Career Network! You may sample our open courses while you wait for inquiries from companies.',
                subtitle_program_failed:
                    'Welcome to the Career Network! You may sample our open courses while you wait for inquiries from companies.',
                subtitle_rejected: 'Sorry, your application wasn’t accepted to the Career Network.',
                subtitle_rejected_no_reapply_cta: 'Sorry, your application wasn’t accepted to the Career Network.',
                subtitle_rejected_after_pre_accepted: 'Sorry, your application wasn’t accepted to the Career Network.',
                subtitle_rejected_after_pre_accepted_no_reapply_cta:
                    'Sorry, your application wasn’t accepted to the Career Network.',
                subtitle_expelled: 'Your Career Network status is no longer active.',
                subtitle_deferred: 'Your Career Network status is no longer active.',
                expelled_status: 'Your enrollment is no longer active.',
                rejected_status: 'Sorry, you were notaccepted. Reapply!',
                rejected_status_no_reapply_cta: 'Sorry, you were notaccepted.',
                deferred_status: 'You’ve deferred your enrollment.',
                getter: 'isCareerNetworkOnly',
            },
            {
                type: 'the_business_certificate',
                cohortTitle: 'Fundamentals of Business Certificate',
                titleHtml: 'Fundamentals of Business Certificate',
                subtitle_not_applied: 'Accepting applications for the next admissions cycle now.',
                subtitle_applied:
                    'Thanks for applying! Complete Business Foundations to demonstrate your readiness for the program.',
                subtitle_applied_foundations_complete:
                    "Great work! You'll hear from us soon regarding your application.",
                subtitle_accepted:
                    'Complete all 9 concentrations to qualify for the certificate. For more details, visit our FAQ.',
                subtitle_accepted_not_started:
                    'Welcome to the Fundamentals of Business Certificate program! To get started, complete Business Foundations, the first of 9 concentrations in the curriculum.Complete all 9 to qualify for the certificate. For more details, visit our FAQ.',
                subtitle_accepted_foundations_complete:
                    'Complete all 9 concentrations to qualify for the certificate. For more details, visit our FAQ.',
                subtitle_accepted_not_started_foundations_complete:
                    "Welcome to the Fundamentals of Business Certificate program! You've already completed Business Foundations, the first of 9 concentrations in the curriculum.Complete all 9 to qualify for the certificate. For more details, visit our FAQ.",
                subtitle_program_complete: 'Congratulations on completing the Fundamentals of Business Certificate.',
                subtitle_program_failed:
                    'Unfortunately, your combined final score did not meet the minimum required for completion of the certificate. For more details visit our FAQ.',
                subtitle_rejected:
                    'Sorry, your application wasn’t accepted for the Fundamentals of Business Certificate program.',
                subtitle_rejected_no_reapply_cta:
                    'Sorry, your application wasn’t accepted for the Fundamentals of Business Certificate program.',
                subtitle_rejected_after_pre_accepted:
                    'Sorry, your application wasn’t accepted for the Fundamentals of Business Certificate program.',
                subtitle_rejected_after_pre_accepted_no_reapply_cta:
                    'Sorry, your application wasn’t accepted for the Fundamentals of Business Certificate program.',
                subtitle_expelled:
                    'Your enrollment is no longer active. We hope you enjoy continued access to select courses from the full program.',
                subtitle_deferred:
                    'Your enrollment is no longer active. We hope you enjoy continued access to select courses from the full program.',
                expelled_status: 'Your enrollment is no longer active.',
                rejected_status: 'Sorry, you were notaccepted. Reapply!',
                rejected_status_no_reapply_cta: 'Sorry, you were notaccepted.',
                deferred_status: 'You’ve deferred your enrollment.',
                failed_status: 'You received a failing score. Visit our FAQ.',
                getter: 'isBusinessCertificate',
            },
            {
                type: 'paid_cert_operations_management',
                cohortTitle: 'Operations Management Certificate',
                titleHtml: 'Operations Management Certificate',
                subtitle_not_applied: 'Accepting applications for the next admissions cycle now.',
                subtitle_applied:
                    'We hope you enjoy this sample of the Operations Management Certificate. Follow this link to complete your enrollment or to re-apply to another program.',
                subtitle_applied_foundations_complete:
                    'We hope you enjoy this sample of the Operations Management Certificate. Follow this link to complete your enrollment or to re-apply to another program.',
                subtitle_accepted:
                    'Complete all required courses to qualify for the certificate. For more details, visit our FAQ.',
                subtitle_accepted_not_started:
                    'Complete all required courses to qualify for the certificate. For more details, visit our FAQ.',
                subtitle_accepted_foundations_complete:
                    'Complete all required courses to qualify for the certificate. For more details, visit our FAQ.',
                subtitle_accepted_not_started_foundations_complete:
                    'Complete all required courses to qualify for the certificate. For more details, visit our FAQ.',
                subtitle_program_failed:
                    'Unfortunately, your combined final score did not meet the minimum required for completion of the certificate. For more details visit our FAQ.',
                subtitle_rejected:
                    'Sorry, your application wasn’t accepted for the Operations Management Certificate program.',
                subtitle_rejected_no_reapply_cta:
                    'Sorry, your application wasn’t accepted for the Operations Management Certificate program.',
                subtitle_rejected_after_pre_accepted:
                    'Sorry, your application wasn’t accepted for the Operations Management Certificate program.',
                subtitle_rejected_after_pre_accepted_no_reapply_cta:
                    'Sorry, your application wasn’t accepted for the Operations Management Certificate program.',
                subtitle_expelled:
                    'Your enrollment is no longer active. We hope you enjoy continued access to select courses from the full program.',
                subtitle_deferred:
                    'Your enrollment is no longer active. We hope you enjoy continued access to select courses from the full program.',
                expelled_status: 'Your enrollment is no longer active.',
                rejected_status: 'Sorry, you were notaccepted. Reapply!',
                rejected_status_no_reapply_cta: 'Sorry, you were notaccepted.',
                deferred_status: 'You’ve deferred your enrollment.',
                failed_status: 'You received a failing score. Visit our FAQ.',
                getter: 'isPaidCertificate',
            },
            {
                type: 'jordanian_math',
                cohortTitle: 'High School STEM',
                titleHtml: 'High School STEM',
                subtitle_not_applied: 'Accepting applications for the next admissions cycle now.',
                subtitle_applied:
                    'Thanks for applying! Complete Business Foundations to demonstrate your readiness for the program.',
                subtitle_applied_foundations_complete:
                    "Great work! You'll hear from us soon regarding your application.",
                subtitle_accepted: 'Welcome to the High School STEM program.',
                subtitle_accepted_not_started:
                    'Welcome to the High School STEM program! To get started, complete Business Foundations, the first of 9 concentrations in the curriculum.',
                subtitle_accepted_foundations_complete: 'Welcome to the High School STEM program.',
                subtitle_accepted_not_started_foundations_complete:
                    "Welcome to the High School STEM program! You've already completed Business Foundations, the first of 9 concentrations in the curriculum.",
                subtitle_program_complete: 'Congratulations on completing the High School STEM program.',
                subtitle_program_failed:
                    'Unfortunately, your combined final score did not meet the minimum required for completion of the program. For more details visit our FAQ.',
                subtitle_rejected: 'Sorry, your application wasn’t accepted for the High School STEM program.',
                subtitle_rejected_no_reapply_cta:
                    'Sorry, your application wasn’t accepted for the High School STEM program.',
                subtitle_rejected_after_pre_accepted:
                    'Sorry, your application wasn’t accepted for the High School STEM program.',
                subtitle_rejected_after_pre_accepted_no_reapply_cta:
                    'Sorry, your application wasn’t accepted for the High School STEM program.',
                subtitle_expelled:
                    'Your enrollment is no longer active. We hope you enjoy continued access to select courses from the full program.',
                subtitle_deferred:
                    'Your enrollment is no longer active. We hope you enjoy continued access to select courses from the full program.',
                expelled_status: 'Your enrollment is no longer active.',
                rejected_status: 'Sorry, you were notaccepted. Reapply!',
                rejected_status_no_reapply_cta: 'Sorry, you were notaccepted.',
                deferred_status: 'You’ve deferred your enrollment.',
                failed_status: 'You received a failing score. Visit our FAQ.',
                getter: 'isJordanianMath',
            },
        ].forEach(program => {
            describe(`for ${program.type}`, () => {
                beforeEach(() => {
                    SpecHelper.stubConfig();

                    relevantCohort.program_type = program.type;
                    relevantCohort.title = program.cohortTitle;
                    if (program.startDate) {
                        relevantCohort.startDate = program.startDate;
                    }

                    Object.defineProperty(currentUser, 'programType', {
                        value: program.type,
                    });
                    Object.defineProperty(foundationPlaylist, 'canCalculateComplete', {
                        value: true,
                    });
                });

                describe('with pending user', () => {
                    beforeEach(() => {
                        jest.spyOn(currentUser, 'lastCohortApplication', 'get').mockReturnValue(
                            CohortApplication.fixtures.getInstance({
                                status: 'pending',
                            }),
                        );
                    });

                    it('should show as applied with foundation incomplete', () => {
                        Object.defineProperty(foundationPlaylist, 'complete', {
                            value: false,
                        });

                        render();
                        SpecHelper.expectNoElement(elem, '.apply-button');

                        expect(elem.find('.title [ng-bind-html]')[0].innerHTML).toEqual(program.titleHtml);
                        SpecHelper.expectElementText(elem, '.sub-title', program.subtitle_applied);

                        if (relevantCohort.studentDashboardProgramBoxShowProgressBar) {
                            SpecHelper.expectElementText(elem, '.mobile-status', 'Application submitted!');
                            SpecHelper.expectElement(elem, '.application-progress');
                            SpecHelper.expectHasClass(elem, '.application-progress .segment.left', 'active', true);
                            SpecHelper.expectHasClass(elem, '.application-progress .segment.middle', 'active', false);
                            SpecHelper.expectHasClass(elem, '.application-progress .segment.right', 'active', false);
                            SpecHelper.expectHasClass(elem, '.application-progress .segment.left', 'filled', false);
                            SpecHelper.expectHasClass(elem, '.application-progress .segment.middle', 'filled', false);
                            SpecHelper.expectHasClass(elem, '.application-progress .segment.right', 'filled', false);
                        } else {
                            SpecHelper.expectNoElement(elem, '.application-progress');
                        }
                    });

                    it('should show as applied with foundation complete', () => {
                        Object.defineProperty(foundationPlaylist, 'complete', {
                            value: true,
                        });

                        render();
                        SpecHelper.expectNoElement(elem, '.apply-button');

                        expect(elem.find('.title [ng-bind-html]')[0].innerHTML).toEqual(program.titleHtml);
                        SpecHelper.expectElementText(elem, '.sub-title', program.subtitle_applied_foundations_complete);

                        if (relevantCohort.studentDashboardProgramBoxShowProgressBar) {
                            SpecHelper.expectElementText(elem, '.mobile-status', 'Application submitted!');
                            SpecHelper.expectElement(elem, '.application-progress');
                            SpecHelper.expectHasClass(elem, '.application-progress .segment.left', 'active', false);
                            SpecHelper.expectHasClass(elem, '.application-progress .segment.middle', 'active', true);
                            SpecHelper.expectHasClass(elem, '.application-progress .segment.right', 'active', false);
                            SpecHelper.expectHasClass(elem, '.application-progress .segment.left', 'filled', true);
                            SpecHelper.expectHasClass(elem, '.application-progress .segment.middle', 'filled', false);
                            SpecHelper.expectHasClass(elem, '.application-progress .segment.right', 'filled', false);
                        } else {
                            SpecHelper.expectNoElement(elem, '.application-progress');
                        }
                    });
                });

                describe('with pre_accepted user', () => {
                    beforeEach(() => {
                        jest.spyOn(currentUser, 'lastCohortApplication', 'get').mockReturnValue(
                            CohortApplication.fixtures.getInstance({
                                status: 'pre_accepted',
                            }),
                        );
                    });

                    it('should show as applied with foundation incomplete', () => {
                        Object.defineProperty(foundationPlaylist, 'complete', {
                            value: false,
                        });

                        render();
                        SpecHelper.expectNoElement(elem, '.apply-button');

                        expect(elem.find('.title [ng-bind-html]')[0].innerHTML).toEqual(program.titleHtml);

                        if (currentUser.isMBA) {
                            SpecHelper.expectNoElement(elem, '.application-progress');
                            SpecHelper.expectNoElement(elem, '.register-button');
                            SpecHelper.expectElementText(elem, '.sub-title', program.subtitle_pre_accepted);
                            SpecHelper.expectElementText(elem, '.pre-accepted-message', 'Application accepted!');
                        } else if (currentUser.isEMBA) {
                            SpecHelper.expectNoElement(elem, '.application-progress');
                            SpecHelper.expectElement(elem, '.register-button');
                            SpecHelper.expectElementText(
                                elem,
                                '.sub-title',
                                program.subtitle_pre_accepted_not_registered,
                            );
                            SpecHelper.expectNoElement(elem, '.pre-accepted-message');
                        } else if (relevantCohort.studentDashboardProgramBoxShowProgressBar) {
                            SpecHelper.expectElementText(elem, '.sub-title', program.subtitle_applied);
                            SpecHelper.expectElementText(elem, '.mobile-status', 'Application submitted!');
                            SpecHelper.expectElement(elem, '.application-progress');
                            SpecHelper.expectHasClass(elem, '.application-progress .segment.left', 'active', true);
                            SpecHelper.expectHasClass(elem, '.application-progress .segment.middle', 'active', false);
                            SpecHelper.expectHasClass(elem, '.application-progress .segment.right', 'active', false);
                            SpecHelper.expectHasClass(elem, '.application-progress .segment.left', 'filled', false);
                            SpecHelper.expectHasClass(elem, '.application-progress .segment.middle', 'filled', false);
                            SpecHelper.expectHasClass(elem, '.application-progress .segment.right', 'filled', false);
                        } else {
                            SpecHelper.expectElementText(elem, '.sub-title', program.subtitle_applied);
                            SpecHelper.expectNoElement(elem, '.application-progress');
                        }
                    });

                    it('should show as applied with foundation complete', () => {
                        Object.defineProperty(foundationPlaylist, 'complete', {
                            value: true,
                        });

                        render();
                        SpecHelper.expectNoElement(elem, '.apply-button');

                        expect(elem.find('.title [ng-bind-html]')[0].innerHTML).toEqual(program.titleHtml);

                        if (currentUser.isMBA) {
                            SpecHelper.expectNoElement(elem, '.application-progress');
                            SpecHelper.expectNoElement(elem, '.register-button');
                            SpecHelper.expectElementText(elem, '.sub-title', program.subtitle_pre_accepted);
                            SpecHelper.expectElementText(elem, '.pre-accepted-message', 'Application accepted!');
                        } else if (currentUser.isEMBA) {
                            SpecHelper.expectNoElement(elem, '.application-progress');
                            SpecHelper.expectElement(elem, '.register-button');
                            SpecHelper.expectElementText(
                                elem,
                                '.sub-title',
                                program.subtitle_pre_accepted_not_registered,
                            );
                            SpecHelper.expectNoElement(elem, '.pre-accepted-message');
                        } else if (relevantCohort.studentDashboardProgramBoxShowProgressBar) {
                            SpecHelper.expectElementText(
                                elem,
                                '.sub-title',
                                program.subtitle_applied_foundations_complete,
                            );
                            SpecHelper.expectElementText(elem, '.mobile-status', 'Application submitted!');
                            SpecHelper.expectElement(elem, '.application-progress');
                            SpecHelper.expectHasClass(elem, '.application-progress .segment.left', 'active', false);
                            SpecHelper.expectHasClass(elem, '.application-progress .segment.middle', 'active', true);
                            SpecHelper.expectHasClass(elem, '.application-progress .segment.right', 'active', false);
                            SpecHelper.expectHasClass(elem, '.application-progress .segment.left', 'filled', true);
                            SpecHelper.expectHasClass(elem, '.application-progress .segment.middle', 'filled', false);
                            SpecHelper.expectHasClass(elem, '.application-progress .segment.right', 'filled', false);
                        } else {
                            SpecHelper.expectElementText(
                                elem,
                                '.sub-title',
                                program.subtitle_applied_foundations_complete,
                            );
                            SpecHelper.expectNoElement(elem, '.application-progress');
                        }
                    });

                    it('should show as registered for EMBA pre_accepted', () => {
                        currentUser.lastCohortApplication.registered = true;

                        render();

                        if (currentUser.isEMBA) {
                            SpecHelper.expectNoElement(elem, '.application-progress');
                            SpecHelper.expectNoElement(elem, '.register-button');
                            SpecHelper.expectElementText(elem, '.sub-title', program.subtitle_pre_accepted_registered);
                            SpecHelper.expectElementText(elem, '.pre-accepted-message', program.registered_status);
                        }
                    });
                });

                describe('with accepted user', () => {
                    beforeEach(() => {
                        jest.spyOn(currentUser, 'lastCohortApplication', 'get').mockReturnValue(
                            CohortApplication.fixtures.getInstance({
                                status: 'accepted',
                            }),
                        );
                    });

                    it('should show as accepted with foundation incomplete', () => {
                        Object.defineProperty(foundationPlaylist, 'complete', {
                            value: false,
                        });

                        render();
                        expect(scope.showProgressBar).toBe(false); // hide progress bar
                        expect(scope.showFaqButton).toBe(true);
                        expect(scope.showCertificateDownloadArea).toBe(false);
                        SpecHelper.expectElementText(elem, '.sub-title', program.subtitle_accepted);
                    });

                    it('should show as accepted with foundation incomplete with first time message', () => {
                        Object.defineProperty(foundationPlaylist, 'complete', {
                            value: false,
                        });

                        // toggle the version shown only to first-timers
                        render({
                            showAcceptedMessage: true,
                        });
                        expect(scope.showProgressBar).toBe(false); // hide progress bar
                        expect(scope.showFaqButton).toBe(true);
                        expect(scope.showCertificateDownloadArea).toBe(false);
                        SpecHelper.expectElementText(elem, '.sub-title', program.subtitle_accepted_not_started);
                    });

                    it('should show as accepted with foundation complete', () => {
                        Object.defineProperty(foundationPlaylist, 'complete', {
                            value: true,
                        });

                        render();
                        expect(scope.showProgressBar).toBe(false); // hide progress bar
                        expect(scope.showFaqButton).toBe(true);
                        expect(scope.showCertificateDownloadArea).toBe(false);
                        SpecHelper.expectElementText(
                            elem,
                            '.sub-title',
                            program.subtitle_accepted_foundations_complete,
                        );
                    });

                    it('should show as accepted with foundation complete with first time message', () => {
                        Object.defineProperty(foundationPlaylist, 'complete', {
                            value: true,
                        });

                        // toggle the version shown only to first-timers
                        render({
                            showAcceptedMessage: true,
                        });
                        expect(scope.showProgressBar).toBe(false); // hide progress bar
                        expect(scope.showFaqButton).toBe(true);
                        expect(scope.showCertificateDownloadArea).toBe(false);
                        SpecHelper.expectElementText(
                            elem,
                            '.sub-title',
                            program.subtitle_accepted_not_started_foundations_complete,
                        );
                    });
                });

                describe('with rejected user', () => {
                    describe('when !rejected_after_pre_accepted', () => {
                        beforeEach(() => {
                            jest.spyOn(currentUser, 'lastCohortApplication', 'get').mockReturnValue(
                                CohortApplication.fixtures.getInstance({
                                    status: 'rejected',
                                    rejected_after_pre_accepted: false,
                                }),
                            );
                        });

                        it('should show as rejected from promoted cohort', () => {
                            Object.defineProperty(currentUser, 'isRejectedForPromotedCohort', {
                                value: true,
                            });

                            render();
                            expect(scope.showProgressBar).toBe(false);
                            SpecHelper.expectElementText(
                                elem,
                                '.rejected-status',
                                program.rejected_status_no_reapply_cta,
                            );
                            SpecHelper.expectElementText(
                                elem,
                                '.sub-title.hidden-xs',
                                program.subtitle_rejected_no_reapply_cta,
                            );
                        });

                        it('should show as rejected from previous cohort', () => {
                            jest.spyOn(currentUser.lastCohortApplication, 'canReapplyTo').mockImplementation(
                                () => true,
                            );
                            render();
                            expect(scope.showProgressBar).toBe(false);
                            SpecHelper.expectElementText(elem, '.rejected-status', program.rejected_status);
                            SpecHelper.expectElementText(elem, '.sub-title.hidden-xs', program.subtitle_rejected);
                        });
                    });

                    describe('when rejected_after_pre_accepted', () => {
                        beforeEach(() => {
                            jest.spyOn(currentUser, 'lastCohortApplication', 'get').mockReturnValue(
                                CohortApplication.fixtures.getInstance({
                                    status: 'rejected',
                                    rejected_after_pre_accepted: true,
                                }),
                            );
                        });

                        it('should show as rejected from promoted cohort', () => {
                            Object.defineProperty(currentUser, 'isRejectedForPromotedCohort', {
                                value: true,
                            });

                            render();
                            expect(scope.showProgressBar).toBe(false);
                            SpecHelper.expectElementText(
                                elem,
                                '.rejected-status',
                                program.rejected_status_no_reapply_cta,
                            );
                            SpecHelper.expectElementText(
                                elem,
                                '.sub-title.hidden-xs',
                                program.subtitle_rejected_after_pre_accepted_no_reapply_cta,
                            );
                        });

                        it('should show as rejected from previous cohort', () => {
                            jest.spyOn(currentUser.lastCohortApplication, 'canReapplyTo').mockImplementation(
                                () => true,
                            );

                            render();
                            expect(scope.showProgressBar).toBe(false);
                            SpecHelper.expectElementText(elem, '.rejected-status', program.rejected_status);
                            SpecHelper.expectElementText(
                                elem,
                                '.sub-title.hidden-xs',
                                program.subtitle_rejected_after_pre_accepted,
                            );
                        });
                    });
                });

                describe('with expelled user', () => {
                    beforeEach(() => {
                        jest.spyOn(currentUser, 'lastCohortApplication', 'get').mockReturnValue(
                            CohortApplication.fixtures.getInstance({
                                status: 'expelled',
                            }),
                        );
                    });

                    it('should show as expelled', () => {
                        render();
                        expect(scope.showProgressBar).toBe(false);
                        SpecHelper.expectElementText(elem, '.rejected-status', program.expelled_status);
                        SpecHelper.expectElementText(elem, '.sub-title.hidden-xs', program.subtitle_expelled);
                    });
                });

                describe('with deferred user', () => {
                    beforeEach(() => {
                        jest.spyOn(currentUser, 'lastCohortApplication', 'get').mockReturnValue(
                            CohortApplication.fixtures.getInstance({
                                status: 'deferred',
                            }),
                        );
                    });

                    it('should show as deferred', () => {
                        render();
                        expect(scope.showProgressBar).toBe(false);
                        SpecHelper.expectElementText(elem, '.rejected-status', program.deferred_status);
                        SpecHelper.expectElementText(elem, '.sub-title.hidden-xs', program.subtitle_deferred);
                    });
                });

                describe('with program complete user', () => {
                    beforeEach(() => {
                        jest.spyOn(currentUser, 'lastCohortApplication', 'get').mockReturnValue(
                            CohortApplication.fixtures.getInstance({
                                status: 'accepted',
                            }),
                        );
                    });

                    [true, false].forEach(onCordova => {
                        it(
                            `should show as accepted with program complete ${onCordova}` ? 'on Cordova' : 'on desktop',
                            () => {
                                // Certificate program types that require graduation for certificate download don't use the
                                // studentDashboardProgramBox directive; they use the studentDashboardCertificateGraduationBox directive instead.
                                if (
                                    _.contains(Cohort.CERTIFICATE_PROGRAM_TYPES, currentUser.programType) &&
                                    Cohort.requiresGraduationForCertificateDownload(currentUser.programType)
                                ) {
                                    return;
                                }

                                Object.defineProperty(currentUser, 'acceptedCohortApplication', {
                                    value: {
                                        cohort_id: currentUser.relevant_cohort.id,
                                        status: 'accepted',
                                        graduation_status: 'graduated',
                                    },
                                });

                                $window.CORDOVA = onCordova;

                                render({
                                    programComplete: true,
                                });

                                expect(scope.showProgressBar).toBe(false);
                                SpecHelper.expectElementText(elem, '.sub-title', program.subtitle_program_complete);

                                if (_.contains(Cohort.CERTIFICATE_PROGRAM_TYPES, currentUser.programType)) {
                                    expect(scope.showFaqButton).toBe(false);
                                    expect(scope.showCertificateDownloadArea).toBe(true);

                                    if (onCordova) {
                                        SpecHelper.expectElementText(
                                            elem,
                                            '.certificate-help-text',
                                            'Log in using a desktop computer to download your certificate.',
                                        );
                                    } else {
                                        jest.spyOn(scope, 'performProgramDownload').mockImplementation(() => {});
                                        jest.spyOn(scope, 'downloadProgramCertificate');
                                        jest.spyOn(EventLogger.prototype, 'log').mockImplementation(() => {});
                                        SpecHelper.click(elem, '.apply-button');
                                        expect(scope.downloadProgramCertificate).toHaveBeenCalled();
                                        expect(scope.performProgramDownload).toHaveBeenCalled();
                                        expect(EventLogger.prototype.log).toHaveBeenCalledWith(
                                            'student-dashboard:download-program-certificate',
                                            {
                                                program_type: program.type,
                                            },
                                        );
                                    }
                                } else {
                                    expect(scope.showFaqButton).toBe(true);
                                    expect(scope.showCertificateDownloadArea).toBe(false);
                                }
                            },
                        );
                    });

                    it('should show as accepted with program failed', () => {
                        Object.defineProperty(currentUser, 'acceptedCohortApplication', {
                            value: {
                                cohort_id: currentUser.relevant_cohort.id,
                                status: 'accepted',
                                graduation_status: 'failed',
                            },
                        });

                        render({
                            programComplete: true,
                        });

                        expect(scope.showProgressBar).toBe(false);
                        SpecHelper.expectElementText(elem, '.sub-title', program.subtitle_program_failed);

                        if (_.contains(Cohort.CERTIFICATE_PROGRAM_TYPES, currentUser.programType)) {
                            expect(scope.showFaqButton).toBe(false);
                            expect(scope.showCertificateDownloadArea).toBe(false);
                            SpecHelper.expectElementText(elem, '.rejected-status', program.failed_status);
                        } else {
                            expect(scope.showFaqButton).toBe(true);
                            expect(scope.showCertificateDownloadArea).toBe(false);
                        }
                    });
                });
            });
        });
    });

    it('should not throw a no cached stream error if the streams from foundationa playlist are not yet loaded', () => {
        jest.spyOn(currentUser, 'lastCohortApplication', 'get').mockReturnValue(
            CohortApplication.fixtures.getInstance({
                status: 'pending',
            }),
        );
        Object.defineProperty(currentUser, 'programType', {
            value: 'mba',
        });

        jest.spyOn(ErrorLogService, 'notify').mockImplementation(() => {});
        const streams = foundationPlaylist.streams;
        Stream.resetCache();
        render();
        expect(scope.foundationsComplete).toBe(null);
        expect(ErrorLogService.notify).not.toHaveBeenCalled();

        // fill up the stream cache again
        _.each(streams, stream => {
            Stream.new(stream.asJson());
        });
        scope.$apply();
        expect(scope.foundationsComplete).not.toBe(null);
        expect(ErrorLogService.notify).not.toHaveBeenCalled();
    });

    it('should call onClose when rejected message is closed', () => {
        jest.spyOn(currentUser, 'lastCohortApplication', 'get').mockReturnValue(
            CohortApplication.fixtures.getInstance({
                status: 'rejected',
            }),
        );
        Object.defineProperty(currentUser, 'programType', {
            value: 'mba',
        });

        render();
        expect(onClose).not.toHaveBeenCalled();
        SpecHelper.click(elem, '.rejected-status button.close');
        expect(onClose).toHaveBeenCalled();
    });

    describe('gotoMbaSettings', () => {
        beforeEach(() => {
            jest.spyOn(currentUser, 'lastCohortApplication', 'get').mockReturnValue(
                CohortApplication.fixtures.getInstance({
                    status: 'rejected',
                }),
            );
            Object.defineProperty(currentUser, 'programType', {
                value: 'mba',
            });
        });

        it('should redirect to application page if reapplyingOrEditingApplication', () => {
            jest.spyOn(currentUser, 'reapplyingOrEditingApplication', 'get').mockReturnValue(true);
            render();
            jest.spyOn(scope, 'loadRoute').mockImplementation(() => {});
            scope.gotoMbaSettings();
            expect(scope.loadRoute).toHaveBeenCalledWith('/settings/application');
        });

        it('should redirect to application status page if !reapplyingOrEditingApplication', () => {
            jest.spyOn(currentUser, 'reapplyingOrEditingApplication', 'get').mockReturnValue(false);
            render();
            jest.spyOn(scope, 'loadRoute').mockImplementation(() => {});
            scope.gotoMbaSettings();
            expect(scope.loadRoute).toHaveBeenCalledWith('/settings/application_status');
        });
    });

    function render(options) {
        renderer = SpecHelper.renderer();
        angular.extend(
            renderer.scope,
            {
                relevantCohort,
                currentUser,
                foundationPlaylist,
                onClose,
                programComplete: false,
                showAcceptedMessage: false,
            },
            options,
        );

        renderer.render(
            '<student-dashboard-program-box current-user="currentUser" num-playlists="9" relevant-cohort="relevantCohort" foundation-playlist="foundationPlaylist" program-complete="programComplete" show-accepted-message="showAcceptedMessage" on-close="onClose()"></student-dashboard-program-box>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    afterEach(() => {
        SpecHelper.cleanup();
    });
});
