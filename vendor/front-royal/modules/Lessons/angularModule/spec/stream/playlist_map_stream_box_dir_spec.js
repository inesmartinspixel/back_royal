import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/streams';

describe('FrontRoyal.Lessons.PlaylistMapStreamBoxDir', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let Stream;
    let stream;
    let ContentAccessHelper;
    let $location;
    let $timeout;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                $location = $injector.get('$location');
                $timeout = $injector.get('$timeout');
                SpecHelper = $injector.get('SpecHelper');
                Stream = $injector.get('Lesson.Stream');
                ContentAccessHelper = $injector.get('ContentAccessHelper');

                $injector.get('StreamFixtures');

                stream = Stream.fixtures.getInstance();
            },
        ]);

        jest.spyOn($location, 'url').mockImplementation(() => {});
        SpecHelper.stubCurrentUser();
        SpecHelper.stubEventLogging();
        SpecHelper.stubConfig();
    });

    function render(opts = {}) {
        renderer = SpecHelper.renderer();
        stream.coming_soon = angular.isDefined(opts.comingSoon) ? opts.comingSoon : stream.coming_soon;
        renderer.scope.stream = stream;
        renderer.render('<playlist-map-stream-box stream="stream"></playlist-map-stream-box>');
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
        $location.url.mockClear();
    }

    describe('with locked stream', () => {
        beforeEach(() => {
            // we delegate to ContentAccessHelper to determine if the stream is locked
            Object.defineProperty(ContentAccessHelper.prototype, 'canLaunch', {
                value: false,
                configurable: true,
            });
        });

        it('should show a tooltip with locked message when clicked if stream is locked', () => {
            render();
            expect(scope.locked).toBe(true);
            SpecHelper.expectNoElement(elem, '.locked-tooltip');
            SpecHelper.click(elem, '.playlist-map-stream-box');
            SpecHelper.expectElement(elem, '.locked-tooltip');
            $timeout.flush();
            expect($location.url.mock.calls.length).toBe(0);
            expect($location.url).not.toHaveBeenCalledWith(stream.streamDashboardPath);
        });

        it('should show a tooltip with locked message when clicked if stream is coming soon', () => {
            const opts = {
                comingSoon: true,
            };
            render(opts);
            expect(scope.locked).toBe(true);
            expect(scope.comingSoon).toBe(true);
            SpecHelper.expectNoElement(elem, '.locked-tooltip');
            SpecHelper.click(elem, '.playlist-map-stream-box');
            SpecHelper.expectElement(elem, '.locked-tooltip');
            $timeout.flush();
            expect($location.url.mock.calls.length > 0).toBe(false);
            expect($location.url).not.toHaveBeenCalledWith(stream.streamDashboardPath);
        });
    });

    describe('with an unlocked stream', () => {
        beforeEach(() => {
            // we delegate to ContentAccessHelper to determine if the stream is locked
            Object.defineProperty(ContentAccessHelper.prototype, 'canLaunch', {
                value: true,
                configurable: true,
            });
        });

        it('should not show tooltip when clicked for an unlocked stream', () => {
            render();
            SpecHelper.click(elem, '.playlist-map-stream-box');
            SpecHelper.expectNoElement(elem, '.locked-tooltip');
        });

        it('should take the user to the stream dashboard when clicked for an unlocked stream', () => {
            render();
            SpecHelper.click(elem, '.playlist-map-stream-box');
            $timeout.flush();
            expect($location.url.mock.calls.length > 0).toBe(true);
            expect($location.url).toHaveBeenCalledWith(stream.streamDashboardPath);
        });
    });
});
