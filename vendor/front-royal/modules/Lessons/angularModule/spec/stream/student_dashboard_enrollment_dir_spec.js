import 'AngularSpecHelper';
import 'Lessons/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import studentDashboardEnrollmentLocales from 'Lessons/locales/lessons/stream/student_dashboard_enrollment-en.json';

setSpecLocales(studentDashboardEnrollmentLocales);

describe('FrontRoyal.Lessons.StudentDashboardEnrollment', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let TranslationHelper;
    let enrollmentTodos;
    let enrollmentTodo1;
    let enrollmentTodo2;
    let enrollmentTodo3;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                TranslationHelper = $injector.get('TranslationHelper');
            },
        ]);

        SpecHelper.stubConfig();

        enrollmentTodo1 = {
            name: 'todo_enrollment_agreement',
            localeKey: 'sign_enrollment_agreement',
            isVisible: jest.fn(),
            isDisabled: jest.fn(),
            inReview: jest.fn(),
            onClick: jest.fn(),
        };

        enrollmentTodo2 = {
            name: 'todo_english_language',
            localeKey: 'upload_english_language_documents',
            isVisible: jest.fn(),
            isDisabled: jest.fn(),
            onClick: jest.fn(),
        };

        enrollmentTodo3 = {
            name: 'todo_transcripts',
            localeKey: 'send_us_your_transcripts',
            isVisible: jest.fn(),
            isDisabled: jest.fn(),
            onClick: jest.fn(),
            subTodosConfig: {
                isVisible: jest.fn(),
                getSubTodos: jest.fn(),
                inReview: jest.fn(),
                isDisabled: jest.fn(),
                getSubTodoText: jest.fn(),
            },
        };

        enrollmentTodos = [enrollmentTodo1, enrollmentTodo2, enrollmentTodo3];
    });

    function render(opts = {}) {
        renderer = SpecHelper.renderer();
        renderer.scope.enrollmentTodos = enrollmentTodos;
        renderer.scope.showHaveQuestions = opts.showHaveQuestions || false;
        renderer.render(
            '<student-dashboard-enrollment enrollment-todos="enrollmentTodos" show-have-questions="showHaveQuestions"></student-dashboard-enrollment>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    it('should only show an enrollment todo if the enrollment todo isVisible', () => {
        _.each(enrollmentTodos, enrollmentTodo => enrollmentTodo.isVisible.mockReturnValue(true));
        render();
        SpecHelper.expectElements(elem, '.todo', enrollmentTodos.length);

        _.each(enrollmentTodos, enrollmentTodo => enrollmentTodo.isVisible.mockReturnValue(false));
        scope.$digest();
        SpecHelper.expectNoElement(elem, '.todo');
    });

    it('should show sub-sub-text when showHaveQuestions', () => {
        render({
            showHaveQuestions: true,
        });
        expect(scope.showHaveQuestions).toBe(true);
        SpecHelper.expectElementText(elem, '.sub-sub-text', 'Questions? registrar@smart.ly');

        scope.showHaveQuestions = false;
        scope.$digest();
        SpecHelper.expectNoElement(elem, '.sub-sub-text');
    });

    describe('onTodoClick', () => {
        it('should', () => {
            enrollmentTodo1.isDisabled.mockReturnValue(true);
            render();
            scope.onTodoClick(enrollmentTodo1);
            expect(enrollmentTodo1.onClick).not.toHaveBeenCalled();

            enrollmentTodo1.isDisabled.mockReturnValue(false);
            scope.onTodoClick(enrollmentTodo1);
            expect(enrollmentTodo1.onClick).toHaveBeenCalled();
        });
    });

    describe('when enrollment todo isVisible', () => {
        beforeEach(() => {
            _.each(enrollmentTodos, enrollmentTodo => enrollmentTodo.isVisible.mockReturnValue(true));
        });

        it('should show the todo text', () => {
            render();
            const translationHelper = new TranslationHelper('lessons.stream.student_dashboard_enrollment');
            _.each(enrollmentTodos, enrollmentTodo => {
                SpecHelper.expectElementText(
                    elem,
                    `.todo[name="${enrollmentTodo.name}"] .todo-text`,
                    translationHelper.get(enrollmentTodo.localeKey),
                );
            });
        });

        it('should add the .disabled CSS class to the todo when the todo isDisabled', () => {
            enrollmentTodo1.isDisabled.mockReturnValue(false);
            render();
            const todoElemSelector = `.todo[name="${enrollmentTodo1.name}"]`;
            SpecHelper.expectDoesNotHaveClass(elem, todoElemSelector, 'disabled');

            enrollmentTodo1.isDisabled.mockReturnValue(true);
            scope.$digest();
            SpecHelper.expectHasClass(elem, todoElemSelector, 'disabled');
        });

        it('should call onTodoClick when todo is clicked', () => {
            render();
            jest.spyOn(scope, 'onTodoClick').mockImplementation(() => {});
            SpecHelper.click(elem, `.todo[name="${enrollmentTodo1.name}"]`);
            expect(scope.onTodoClick).toHaveBeenCalledWith(enrollmentTodo1);
        });

        it('should add the .in-review CSS class to the todo when the todo is inReview', () => {
            enrollmentTodo1.inReview.mockReturnValue(false);
            render();
            const todoElemSelector = `.todo[name="${enrollmentTodo1.name}"]`;
            SpecHelper.expectDoesNotHaveClass(elem, todoElemSelector, 'in-review');

            enrollmentTodo1.inReview.mockReturnValue(true);
            scope.$digest();
            SpecHelper.expectHasClass(elem, todoElemSelector, 'in-review');
        });

        it("should label the todo as '(In Review)' when the todo is inReview", () => {
            enrollmentTodo1.inReview.mockReturnValue(false);
            render();
            const todoElemTextSelector = `.todo[name="${enrollmentTodo1.name}"] .todo-text span:eq(1)`;
            SpecHelper.expectNoElement(elem, todoElemTextSelector);

            enrollmentTodo1.inReview.mockReturnValue(true);
            scope.$digest();
            SpecHelper.expectElementText(elem, todoElemTextSelector, '(In Review)');
        });

        describe('when enrollment todo has sub-todos', () => {
            let subTodos;

            beforeEach(() => {
                subTodos = [
                    {
                        text: 'sub-todo 1',
                    },
                    {
                        text: 'sub-todo 2',
                    },
                    {
                        text: 'sub-todo 3',
                    },
                ];
                enrollmentTodo3.subTodosConfig.getSubTodos.mockReturnValue(subTodos);
            });

            it('should only show the sub-todos when subTodosConfig isVisible', () => {
                enrollmentTodo3.subTodosConfig.isVisible.mockReturnValue(false);
                render();
                const subTodoElemSelector = `.todo[name="${enrollmentTodo3.name}"] ~ .sub-todo`;
                SpecHelper.expectNoElement(elem, subTodoElemSelector);

                enrollmentTodo3.subTodosConfig.isVisible.mockReturnValue(true);
                scope.$digest();
                SpecHelper.expectElements(elem, subTodoElemSelector, subTodos.length);
            });

            describe('when sub-todos are visible', () => {
                beforeEach(() => {
                    enrollmentTodo3.subTodosConfig.isVisible.mockReturnValue(true);
                });

                it('should show the sub-todo text', () => {
                    enrollmentTodo3.subTodosConfig.getSubTodoText.mockImplementation(subTodo => subTodo.text);
                    render();
                    const subTodoElemSelector = `.todo[name="${enrollmentTodo3.name}"] ~ .sub-todo`;
                    for (let i = 0; i < subTodos.length; i++) {
                        SpecHelper.expectElementText(
                            elem,
                            `${subTodoElemSelector}:eq(${i}) .todo-text`,
                            subTodos[i].text,
                        );
                    }
                });

                it('should add the .in-review CSS class to the sub-todo when the sub-todo is inReview', () => {
                    enrollmentTodo3.subTodosConfig.inReview.mockReturnValue(false);
                    render();

                    const subTodoElemSelector = `.todo[name="${enrollmentTodo3.name}"] ~ .sub-todo`;
                    for (let i = 0; i < subTodos.length; i++) {
                        SpecHelper.expectDoesNotHaveClass(elem, `${subTodoElemSelector}:eq(${i})`, 'in-review');
                    }

                    enrollmentTodo3.subTodosConfig.inReview.mockReset();
                    enrollmentTodo3.subTodosConfig.inReview.mockImplementation(
                        subTodo => subTodo.text === 'sub-todo 1',
                    );
                    scope.$digest();
                    SpecHelper.expectHasClass(elem, `${subTodoElemSelector}:eq(0)`, 'in-review');
                    for (let j = 1; j < subTodos.length; j++) {
                        SpecHelper.expectDoesNotHaveClass(elem, `${subTodoElemSelector}:eq(${j})`, 'in-review');
                    }
                });

                it("should label the sub-todo as '(In Review)' when the sub-todo is inReview", () => {
                    enrollmentTodo3.subTodosConfig.inReview.mockReturnValue(false);
                    render();

                    const subTodoElemSelector = `.todo[name="${enrollmentTodo3.name}"] ~ .sub-todo`;
                    for (let i = 0; i < subTodos.length; i++) {
                        SpecHelper.expectNoElement(elem, `${subTodoElemSelector}:eq(${i}) .todo-text span:eq(1)`);
                    }

                    enrollmentTodo3.subTodosConfig.inReview.mockReset();
                    enrollmentTodo3.subTodosConfig.inReview.mockImplementation(
                        subTodo => subTodo.text === 'sub-todo 1',
                    );
                    scope.$digest();
                    SpecHelper.expectElementText(
                        elem,
                        `${subTodoElemSelector}:eq(0) .todo-text span:eq(1)`,
                        '(In Review)',
                    );
                    for (let j = 1; j < subTodos.length; j++) {
                        SpecHelper.expectNoElement(elem, `${subTodoElemSelector}:eq(${j}) .todo-text span:eq(1)`);
                    }
                });

                it('should add the .disabled CSS class to the sub-todo when the sub-todo isDisabled', () => {
                    enrollmentTodo3.subTodosConfig.isDisabled.mockReturnValue(false);
                    render();

                    const subTodoElemSelector = `.todo[name="${enrollmentTodo3.name}"] ~ .sub-todo`;
                    for (let i = 0; i < subTodos.length; i++) {
                        SpecHelper.expectDoesNotHaveClass(elem, `${subTodoElemSelector}:eq(${i})`, 'disabled');
                    }

                    enrollmentTodo3.subTodosConfig.isDisabled.mockReset();
                    enrollmentTodo3.subTodosConfig.isDisabled.mockImplementation(
                        subTodo => subTodo.text === 'sub-todo 1',
                    );
                    scope.$digest();
                    SpecHelper.expectHasClass(elem, `${subTodoElemSelector}:eq(0)`, 'disabled');
                    for (let j = 1; j < subTodos.length; j++) {
                        SpecHelper.expectDoesNotHaveClass(elem, `${subTodoElemSelector}:eq(${j})`, 'disabled');
                    }
                });
            });
        });
    });
});
