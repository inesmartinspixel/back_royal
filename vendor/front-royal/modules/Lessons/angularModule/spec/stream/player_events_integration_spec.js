import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/componentized_fixtures';
import 'Lessons/angularModule/spec/_mock/fixtures/lessons';
import 'Lessons/angularModule/spec/_mock/fixtures/streams';
import 'Editor/angularModule';
import lessonPlayerViewModelLocales from 'Lessons/locales/lessons/models/lesson/lesson_player_view_model-en.json';
import showFramesPlayerLocales from 'Lessons/locales/lessons/lesson/show_frames_player-en.json';
import previousButtonDesktopLocales from 'Navigation/locales/navigation/previous_button_desktop-en.json';
import feedbackSidebarLocales from 'Feedback/locales/feedback/feedback_sidebar-en.json';
import continueButtonLocales from 'Lessons/locales/lessons/lesson/frame_list/frame/componentized/component/continue_button-en.json';
import lessonFinishScreenFooterLocales from 'Lessons/locales/lessons/lesson/lesson_finish_screen_footer-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(
    lessonPlayerViewModelLocales,
    showFramesPlayerLocales,
    previousButtonDesktopLocales,
    feedbackSidebarLocales,
    continueButtonLocales,
    lessonFinishScreenFooterLocales,
);

describe('Lessons.PlayerEventsIntegration', () => {
    let stream;
    let streamViewModel;
    let SpecHelper;
    let EventLogger;
    let $timeout;
    let scope;
    let elem;
    let lessonEvents;
    let $rootScope;
    let playerViewModel;
    let lesson;
    let $httpBackend;
    let LessonProgress;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                const Stream = $injector.get('Lesson.Stream');
                const FrameList = $injector.get('Lesson.FrameList');
                const Componentized = $injector.get('Lesson.FrameList.Frame.Componentized');
                const $location = $injector.get('$location');
                const $q = $injector.get('$q');
                const frontRoyalStore = $injector.get('frontRoyalStore');
                LessonProgress = $injector.get('LessonProgress');
                $injector.get('StreamFixtures');
                $injector.get('LessonFixtures');
                $injector.get('ComponentizedFixtures');
                SpecHelper = $injector.get('SpecHelper');
                $rootScope = $injector.get('$rootScope');
                $rootScope.currentUser = SpecHelper.stubCurrentUser('learner');
                SpecHelper.stubDirective('frontRoyalFooterContent'); // just let the footer content render on the screen
                $httpBackend = $injector.get('$httpBackend');

                // Ensure config is initialized so events can be flushed
                SpecHelper.stubConfig();

                EventLogger = $injector.get('EventLogger');
                $timeout = $injector.get('$timeout');
                jest.spyOn($location, 'search').mockReturnValue({}); // do not mess with the url. It's confusing
                SpecHelper.stubEventLogging();
                SpecHelper.stubFramePreloading();

                lesson = FrameList.fixtures.getInstance();

                lesson.frames = [
                    Componentized.fixtures.getMultipleChoiceInstance(),
                    Componentized.fixtures.getNoInteractionInstance(),
                    Componentized.fixtures.getNoIncorrectAnswersMultipleChoiceInstance(),
                ];
                lesson.frames.forEach(frame => {
                    frame.$$embeddedIn = lesson;
                });

                stream = Stream.fixtures.getInstance({
                    lessons: [lesson.asJson()],
                    chapters: [
                        {
                            title: 'Chapter 1',
                            lesson_hashes: [
                                {
                                    lesson_id: lesson.id,
                                    coming_soon: false,
                                },
                            ],
                            pending: false,
                        },
                    ],
                    lesson_streams_progress: undefined,
                });
                lessonEvents = [];

                Stream.expect('show').returns(stream);

                const origLog = EventLogger.prototype.log;
                jest.spyOn(EventLogger.prototype, 'log').mockImplementation((eventType, obj) => {
                    const event = origLog.apply(EventLogger.instance, [eventType, obj]);
                    if (eventType.slice(0, 6) === 'lesson') {
                        lessonEvents.push(event);
                    }
                    return event;
                });

                // stub out lessonProgress saving
                jest.spyOn(LessonProgress.prototype, 'save').mockReturnValue($q.resolve({}));

                // This is not totally realistic, since flush returns a
                // native promise.  But it works in the wild and it simplifies
                // the testing here if we return $q.
                jest.spyOn(frontRoyalStore, 'flush').mockImplementation(() => $q.resolve());
            },
        ]);

        $httpBackend.whenGET('sounds/button_scaling_click_1.mp3').respond({});
        $httpBackend.whenGET('sounds/button_scaling_click_2.mp3').respond({});
        $httpBackend.whenGET('sounds/button_select.mp3').respond({});
        $httpBackend.whenGET('sounds/button_incorrect.mp3').respond({});
        $httpBackend.whenGET('sounds/button_correct.mp3').respond({});

        SpecHelper.stubConfig();
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    it('should log all the correct events for different types of frames', () => {
        /* ******************* Render frame initially ********************* */
        let events = trackEvents(() => {
            render();
        });
        expectEventTypes(
            [
                // chapter and lesson start happen first now because frame_list_player_view_model.initialize
                // now calls lesson.ensureLessonProgress()
                'lesson:start', // lesson progress is created, since the user never did this one before
                'lesson:play', // lesson is loaded
                'lesson:stream:start', // stream progress is created, since the user never did anything with this stream before
            ],
            events,
        );

        // since this lesson has not previously been completed, it should be marked as such
        expect(events[0].properties.lesson_complete).toBe(false);

        /* ******************* Move on from start screen ********************* */
        events = trackEvents(() => {
            dismissStartScreen();
        });

        // we no longer log activate or play events
        expectEventTypes([], events);

        /* ******************* Play Frame 1: Multiple Choice ********************* */
        events = trackEvents(() => {
            clickAnswer();
        });
        expectEventTypes(
            [
                'lesson:challenge_validation', // a validation on the click
                'lesson:challenge_complete', // challenge is complete
            ],
            events,
        );

        // grtab some events that let us check duration info
        const frameActivateEvent = playerViewModel._frameActivateEvent;
        expect(frameActivateEvent).not.toBeUndefined();
        const framePlayEvent = playerViewModel._framePlayEvent;
        expect(framePlayEvent).not.toBeUndefined();
        const frameFinishEvent = playerViewModel._frameFinishEvent;
        expect(frameFinishEvent).not.toBeUndefined();

        // click continue button
        events = trackEvents(() => {
            clickContinueButton();
        });

        expectEventTypes(
            [
                'lesson:frame:unload', // first frame is removed
            ],
            events,
        );
        const frameUnloadEvent = events[0];

        expect(frameUnloadEvent.properties.duration_total).toBeCloseTo(
            frameUnloadEvent.clientTimestamp / 1000 - framePlayEvent.clientTimestamp / 1000,
            5,
        );
        expect(frameUnloadEvent.properties.time_to_finish.duration_total).toBeCloseTo(
            frameFinishEvent.clientTimestamp / 1000 - framePlayEvent.clientTimestamp / 1000,
            5,
        );
        expect(frameUnloadEvent.properties.time_to_activate.duration_total).toBeCloseTo(
            framePlayEvent.clientTimestamp / 1000 - frameActivateEvent.clientTimestamp / 1000,
            5,
        );

        /* ******************* Play Frame 2: No Interaction ********************* */
        // click continue button
        events = trackEvents(() => {
            clickContinueButton();
        });
        expectEventTypes(
            [
                'lesson:frame:unload', // 2nd frame is removed
            ],
            events,
        );

        /* ******************* Play Frame 3: No Incorrect Answers ********************* */
        events = trackEvents(() => {
            clickAnswer();
        });
        expectEventTypes([], events); // selecting an answer has not yet triggered validation, so no events fired

        // for no_incorrect_answers, clicking the continue
        // button does the challenge validation, completes the frame,
        // and moves on
        events = trackEvents(() => {
            clickContinueButton();
        });
        expectEventTypes(
            [
                'lesson:challenge_validation', // a validation on the click
                'lesson:challenge_complete', // challenge is complete
                'lesson:frame:unload',

                // and now lesson is complete
                'lesson:finish',
                'lesson:complete',
                'lesson:stream:complete',
            ],
            events,
        );

        /* ******************* Render completed lesson ********************* */
        events = trackEvents(() => {
            render();
        });
        expectEventTypes(
            [
                'lesson:play', // lesson is started

                // no lesson:start or lesson:stream:start because it's already been started before
            ],
            events,
        );

        // since this lesson has previously been completed, it should be marked as such
        expect(events[0].properties.lesson_complete).toBe(true);

        // goto 2nd frame
        dismissStartScreen();
        clickAnswer();
        clickContinueButton();
        $timeout.flush(1000);

        // goto 3rd frame
        clickContinueButton();

        // finish the lesson
        clickAnswer();
        events = trackEvents(() => {
            clickContinueButton();
        });
        expectEventTypes(
            [
                // this is the no_incorrect_answers frame, so the
                // challenge validation events happen when the
                // continue button is clicked
                'lesson:challenge_validation',
                'lesson:challenge_complete',
                'lesson:frame:unload', // frame is unloaded
                'lesson:finish', // the user has reached the end of the lesson in this lesson play
            ],
            events,
        );
    });

    function trackEvents(func) {
        lessonEvents = [];
        func();
        return lessonEvents;
    }

    function expectEventTypes(expectedTypes, events) {
        expect(events.map(event => event.event_type)).toEqual(expectedTypes);
    }

    function render() {
        const renderer = SpecHelper.renderer();

        renderer.render(`<show-stream stream-id="${stream.id}" lesson-id="${stream.lessons[0].id}"></show-stream>`);
        scope = renderer.scope;
        elem = renderer.elem;
        streamViewModel = elem.isolateScope().streamViewModel;
        playerViewModel = streamViewModel.activePlayerViewModel;
    }

    function clickAnswer() {
        // first answer button is the correct one
        SpecHelper.click(elem, '.cf_answer_list button', 0);
        $timeout.flush(1000);
    }

    function clickContinueButton() {
        const button = SpecHelper.expectElementEnabled(elem, '.cf-continue-button button');
        button.click();
        $timeout.flush(1000);
        $timeout.flush(1000);
        try {
            $httpBackend.flush();
        } catch (er) {
            // no-op
        }
    }

    function dismissStartScreen() {
        if (streamViewModel) {
            streamViewModel.activePlayerViewModel.showStartScreen = false;
        } else {
            playerViewModel.showStartScreen = false;
        }

        scope.$digest();
        $timeout.flush(1000);
        $timeout.flush(1000);
    }
});
