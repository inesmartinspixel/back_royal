import 'AngularSpecHelper';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';
import 'Lessons/angularModule';

import * as userAgentHelper from 'userAgentHelper';

describe('Lessons.Stream.CertificateHelperMixin', () => {
    let $injector;
    let $rootScope;
    let scope;
    let CertificateHelperMixin;
    let $window;
    let Cohort;
    let cohort;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                $rootScope = $injector.get('$rootScope');
                $window = $injector.get('$window');
                CertificateHelperMixin = $injector.get('Stream.CertificateHelperMixin');
                Cohort = $injector.get('Cohort');

                $injector.get('CohortFixtures');
            },
        ]);

        scope = $rootScope.$new();
        cohort = Cohort.fixtures.getInstance();
    });

    function link() {
        CertificateHelperMixin.onLink(scope);
    }

    describe('allowProgramCertificateDownload', () => {
        describe('on mobile', () => {
            it('should return false if on Corodva', () => {
                $window.CORDOVA = true;
                link(scope);
                expect(scope.allowProgramCertificateDownload(cohort)).toBe(false);
            });

            it('should return false if on iOS', () => {
                $window.CORDOVA = false;
                jest.spyOn(userAgentHelper, 'isiOSDevice').mockReturnValue(true);
                link(scope);
                expect(scope.allowProgramCertificateDownload(cohort)).toBe(false);
                expect(userAgentHelper.isiOSDevice).toHaveBeenCalled();
            });
        });

        describe('on desktop', () => {
            beforeEach(() => {
                $window.CORDOVA = false;
                jest.spyOn(userAgentHelper, 'isiOSDevice').mockReturnValue(false);
                link(scope);
            });

            it('should return true program_type supportsCertificateDownload', () => {
                jest.spyOn(Cohort, 'supportsCertificateDownload').mockReturnValue(true);
                expect(scope.allowProgramCertificateDownload(cohort)).toBe(true);
            });

            it('should return false if not Certificate cohort', () => {
                jest.spyOn(Cohort, 'supportsCertificateDownload').mockReturnValue(false);
                expect(scope.allowProgramCertificateDownload(cohort)).toBe(false);
            });
        });
    });
});
