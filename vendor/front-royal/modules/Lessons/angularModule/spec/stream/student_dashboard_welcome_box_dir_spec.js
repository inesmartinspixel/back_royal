import 'AngularSpecHelper';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';
import 'Lessons/angularModule';
import 'Playlists/angularModule/spec/_mock/fixtures/playlists';
import 'Lessons/angularModule/spec/_mock/fixtures/streams';
import setSpecLocales from 'Translation/setSpecLocales';
import studentDashboardWelcomeBoxLocales from 'Lessons/locales/lessons/stream/student_dashboard_welcome_box-en.json';
import 'Users/angularModule/spec/_mock/fixtures/users';
import moment from 'moment-timezone';

setSpecLocales(studentDashboardWelcomeBoxLocales);

describe('Lessons.Stream.StudentDashboardWelcomeBoxDir', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let relevantCohort;
    let Cohort;
    let Stream;
    let currentUser;
    let streams;
    let foundationPlaylist;
    let cohortApplication;
    let Institution;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'FrontRoyal.Institutions', 'SpecHelper');

        angular.mock.inject([
            '$injector',

            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                Cohort = $injector.get('Cohort');
                $injector.get('CohortFixtures');
                Stream = $injector.get('Lesson.Stream');
                Institution = $injector.get('Institution');

                $injector.get('StreamFixtures');
                $injector.get('PlaylistFixtures');
            },
        ]);

        SpecHelper.stubConfig();

        relevantCohort = Cohort.fixtures.getInstance();
        relevantCohort.registration_deadline_days_offset = -10;
        relevantCohort.startDate = moment('2049-12-25 12:00:00').toDate();
        foundationPlaylist = Cohort.fixtures.getPlaylistsForCohort(relevantCohort)[0];
        foundationPlaylist.title = 'Business Foundations';

        currentUser = SpecHelper.stubCurrentUser('learner');
        currentUser.relevant_cohort = relevantCohort;
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        renderer = SpecHelper.renderer();

        renderer.scope.foundationPlaylist = foundationPlaylist;
        renderer.scope.currentUser = currentUser;
        renderer.scope.streams = streams;
        renderer.render(
            '<student-dashboard-welcome-box current-user="currentUser" foundation-playlist="foundationPlaylist" streams="streams"></student-dashboard-welcome-box>',
        );
        elem = renderer.elem;
    }

    function setupBOSHIGH() {
        currentUser.groups = [
            {
                name: 'BLUEOCEAN',
            },
        ];
        currentUser.sign_up_code = 'BOSHIGH';
        const bosHighInstitution = Institution.new({
            name: 'BOSHIGH',
            id: 'BOSHIGH',
        });
        currentUser.institutions = [bosHighInstitution];
        currentUser.active_institution = bosHighInstitution;

        const blueOceanStream = Stream.fixtures.getInstance();
        blueOceanStream.id = '87b85177-fdf6-4cf8-a29a-c44b0ef77530'; // BOS stream ID

        streams = [blueOceanStream];
    }

    function setupExternalInstitution() {
        currentUser.groups = [
            {
                name: 'GEORGETOWNMSB',
            },
        ];
        currentUser.sign_up_code = 'GEORGETOWNMSB';

        const externalInstitution = Institution.new({
            name: 'GEORGETOWNMSB',
            id: 'GEORGETOWNMSB',
            external: true,
        });
        currentUser.institutions = [externalInstitution];
        currentUser.active_institution = externalInstitution;

        streams = [Stream.fixtures.getInstance(), Stream.fixtures.getInstance(), Stream.fixtures.getInstance()];
    }

    it('should display the appropriate welcome text for BOSHIGH users', () => {
        setupBOSHIGH();
        render();

        SpecHelper.expectElementText(elem, '.title.hidden-xs', 'Welcome to Smartly, Cool Brent!');
        SpecHelper.expectElementText(elem, '.title.visible-xs', 'Welcome to Smartly!');
        SpecHelper.expectElements(elem, '.sub-title .message', 1);
        SpecHelper.expectElement(elem, '.sub-title .message.blueocean');
    });

    it('should display the appropriate welcome text for external-institution users', () => {
        setupExternalInstitution();
        render();

        SpecHelper.expectElementText(elem, '.title.hidden-xs', 'Welcome to Smartly, Cool Brent!');
        SpecHelper.expectElementText(elem, '.title.visible-xs', 'Welcome to Smartly!');
        SpecHelper.expectElements(elem, '.sub-title .message', 1);
        SpecHelper.expectElement(elem, '.sub-title .message.external');
    });

    describe('SMARTER messaging', () => {
        it('should display the appropriate welcome text for users that have not yet started applying', () => {
            currentUser.cohort_applications = [];
            currentUser.program_type_confirmed = false;
            currentUser.fallback_program_type = 'mba';
            render();

            SpecHelper.expectElementText(elem, '.title', 'Welcome to Quantic, Cool Brent!');
            SpecHelper.expectElementText(
                elem,
                '.sub-title',
                'Sample these free introductory business courses. Then find more courses unlocked on the Library tab.Happy Learning!',
            );
        });

        it('should display fallback text when key not found for unknown cohort program type', () => {
            currentUser.cohort_applications = [
                {
                    status: 'pending',
                },
            ];
            currentUser.program_type_confirmed = true;
            relevantCohort.program_type = 'FAKEFAKE';

            expect(() => {
                render();
            }).toThrow(new Error('Missing translation "applied".  Using provided fallback.'));
        });

        it('should display not applied text for user without cohort', () => {
            currentUser.cohort_applications = [];
            currentUser.program_type_confirmed = false;
            currentUser.fallback_program_type = 'demo';
            currentUser.relevant_cohort = undefined;

            SpecHelper.expectElementText(elem, '.title', 'Welcome to Quantic, Cool Brent!');
            SpecHelper.expectElementText(
                elem,
                '.sub-title',
                'Sample these free introductory business courses. Then find more courses unlocked on the Library tab.Happy Learning!',
            );
        });

        [
            {
                type: 'mba',
                applying: {
                    title: 'Welcome to Quantic, Cool Brent!',
                    subtitle:
                        "Step 1: Complete Your ApplicationIt's fast and free to apply. The next deadline is November 24, 2049.Step 2: Start LearningTry out these Business Foundations courses. Progress is considered in evaluating your application.Step 3: Get Your DecisionMBA and Executive MBA decisions are sent out on December 3, 2049.",
                },
                applied: {
                    title: "You've applied, Cool Brent. What's next?",
                    subtitle:
                        "1: Complete Business FoundationsYour progress is considered when evaluating your application.2. Improve Your ApplicationFeel free to make changes here. We value thoughtful, thorough responses!3. Admissions InterviewIf selected, take it! It's a great chance to connect with our team.4. Get Your DecisionLook for your acceptance decision on December 3, 2049. Good luck!",
                },
                applied_foundations_complete: {
                    title: "You've applied, Cool Brent. What's next?",
                    subtitle:
                        "1: Business FoundationsYou've completed these introductory courses. Great!2. Improve Your ApplicationFeel free to make changes here. We value thoughtful, thorough responses!3. Admissions InterviewIf selected, take it! It's a great chance to connect with our team.4. Get Your DecisionLook for your acceptance decision on December 3, 2049. Good luck!",
                },
                pre_accepted: {
                    title: "Congratulations, Cool Brent! You're in!",
                    subtitle:
                        "Of the thousands of applications we received, you are among a select group to be admitted — this is a great achievement! You're about to join a class of exceptional students from around the world.Keep an eye out for notifications via email and on your dashboard regarding the start of classes on December 25, 2049.",
                },
            },
            {
                type: 'emba',
                applying: {
                    title: 'Welcome to Quantic, Cool Brent!',
                    subtitle:
                        "Step 1: Complete Your ApplicationIt's fast and free to apply. The next deadline is November 24, 2049.Step 2: Start LearningTry out these Business Foundations courses. Progress is considered in evaluating your application.Step 3: Get Your DecisionMBA and Executive MBA decisions are sent out on December 3, 2049.",
                },
                applied: {
                    title: "You've applied, Cool Brent. What's next?",
                    subtitle:
                        "1: Complete Business FoundationsYour progress is considered when evaluating your application.2. Improve Your ApplicationFeel free to make changes here. We value thoughtful, thorough responses!3. Admissions InterviewIf selected, take it! It's a great chance to connect with our team.4. Get Your DecisionLook for your acceptance decision on December 3, 2049. Good luck!",
                },
                applied_foundations_complete: {
                    title: "You've applied, Cool Brent. What's next?",
                    subtitle:
                        "1: Business FoundationsYou've completed these introductory courses. Great!2. Improve Your ApplicationFeel free to make changes here. We value thoughtful, thorough responses!3. Admissions InterviewIf selected, take it! It's a great chance to connect with our team.4. Get Your DecisionLook for your acceptance decision on December 3, 2049. Good luck!",
                },
                pre_accepted: {
                    title: "Congratulations, Cool Brent! You're in!",
                    subtitle:
                        "You're about to join a class of exceptional students from around the world.If you haven’t already, review the Program Guide for more details about the courses and schedule.Keep an eye out for notifications via email and on your dashboard regarding the start of classes on December 25, 2049.",
                },
            },
            {
                type: 'the_business_certificate',
                applying: {
                    title: 'Welcome to Smartly, Cool Brent!',
                    subtitle:
                        "Step 1: Complete Your ApplicationIt's fast and free to apply.Step 2: Start LearningTry out these Business Foundations courses. Progress is considered in evaluating your application.Step 3: Get Your DecisionDecisions are sent on a rolling basis, usually within 2 weeks.",
                },
                applied: {
                    title: "You've applied, Cool Brent. What's next?",
                    subtitle:
                        '1: Complete Business FoundationsYour progress is considered when evaluating your application.2. Improve Your ApplicationFeel free to make changes here. We value thoughtful, thorough responses!3. Get Your DecisionDecisions are sent on a rolling basis, usually within 2 weeks. Good luck!',
                },
                applied_foundations_complete: {
                    title: "You've applied, Cool Brent. What's next?",
                    subtitle:
                        "1: Business FoundationsYou've completed these introductory courses. Great!2. Improve Your ApplicationFeel free to make changes here. We value thoughtful, thorough responses!3. Get Your DecisionDecisions are sent on a rolling basis, usually within 2 weeks. Good luck!",
                },
                accepted: {
                    title: 'Did You Know?',
                    subtitle:
                        'Enrolled students like you are eligible for additional free and paid programs from Smartly! View them and contact admissions@smart.ly to find out more.Happy Learning!',
                },
            },
            {
                type: 'career_network_only',
                applying: {
                    title: 'Welcome to Smartly, Cool Brent!',
                    subtitle:
                        'Sample these free introductory business courses. Then find more courses unlocked on the Library tab.Happy Learning!',
                },
                applied: {
                    title: "You've applied, Cool Brent. What's next?",
                    subtitle:
                        '1: Improve Your ApplicationFeel free to make changes here. We value thoughtful, thorough responses!2. Sample CoursesTry our free introductory business courses while you wait!3. Get Your DecisionDecisions are sent on a rolling basis, usually within 2 weeks. Good luck!',
                },
                applied_foundations_complete: {
                    title: "You've applied, Cool Brent. What's next?",
                    subtitle:
                        "1: Improve Your ApplicationFeel free to make changes here. We value thoughtful, thorough responses!2. Sample CoursesYou've completed our introductory business courses. Well done! See more in the Library tab.3. Get Your DecisionDecisions are sent on a rolling basis, usually within 2 weeks. Good luck!",
                },
                accepted: {
                    title: 'Did You Know?',
                    subtitle:
                        'Smartly Talent members like you are eligible for additional free and paid programs from Smartly! View them and contact admissions@smart.ly to find out more.Happy Learning!',
                },
            },
            {
                type: 'paid_cert_data_analytics',
                applying: {
                    title: 'Welcome to Smartly, Cool Brent!',
                    subtitle:
                        "Step 1: Complete Your ApplicationIt's fast and free to apply.Step 2: Start LearningTry out these Business Foundations courses. Progress is considered in evaluating your application.Step 3: Get Your DecisionDecisions are sent on a rolling basis, usually within 2 weeks.",
                },
                applied: {
                    title: "You've applied, Cool Brent. What's next?",
                    subtitle:
                        '1: Complete Business FoundationsYour progress is considered when evaluating your application.2. Improve Your ApplicationFeel free to make changes here. We value thoughtful, thorough responses!3. Get Your DecisionDecisions are sent on a rolling basis, usually within 2 weeks. Good luck!',
                },
                applied_foundations_complete: {
                    title: "You've applied, Cool Brent. What's next?",
                    subtitle:
                        "1: Business FoundationsYou've completed these introductory courses. Great!2. Improve Your ApplicationFeel free to make changes here. We value thoughtful, thorough responses!3. Get Your DecisionDecisions are sent on a rolling basis, usually within 2 weeks. Good luck!",
                },
                accepted: {
                    title: 'Did You Know?', // won't actually be displayed. see also: `studentDashboard::showWelcomeBox`
                    subtitle:
                        'Enrolled students like you are eligible for additional free and paid programs from Smartly! View them and contact admissions@smart.ly to find out more.Happy Learning!',
                },
            },
        ].forEach(program => {
            describe(`for ${program.type}`, () => {
                beforeEach(() => {
                    relevantCohort.program_type = program.type;
                    currentUser.program_type_confirmed = true;

                    cohortApplication = {
                        status: 'pending',
                        applied_at: new Date().getTime() / 1000,
                    };

                    currentUser.cohort_applications = [cohortApplication];

                    Object.defineProperty(currentUser, 'programType', {
                        value: program.type,
                    });
                    Object.defineProperty(foundationPlaylist, 'canCalculateComplete', {
                        value: true,
                    });
                });

                describe('with applying user', () => {
                    beforeEach(() => {
                        currentUser.cohort_applications = [];
                    });

                    it('should show as applied with foundation incomplete', () => {
                        if (!relevantCohort.isDegreeProgram) {
                            // We're mocking shouldSeeQuanticBranding here so that locale message says Smartly instead of Quantic, which is
                            // inline with what we'd actually expect in the wild since non-degree program types are only for Smartly.
                            jest.spyOn(currentUser, 'shouldSeeQuanticBranding', 'get').mockReturnValue(false);
                        }
                        render();

                        SpecHelper.expectElementText(elem, '.title', program.applying.title);
                        SpecHelper.expectElementText(elem, '.sub-title', program.applying.subtitle);
                    });
                });

                describe('with applied user', () => {
                    it('should show as applied with foundation incomplete', () => {
                        if (!relevantCohort.isDegreeProgram) {
                            // We're mocking shouldSeeQuanticBranding here so that locale message says Smartly instead of Quantic, which is
                            // inline with what we'd actually expect in the wild since non-degree program types are only for Smartly.
                            jest.spyOn(currentUser, 'shouldSeeQuanticBranding', 'get').mockReturnValue(false);
                        }
                        Object.defineProperty(foundationPlaylist, 'complete', {
                            value: false,
                        });

                        render();

                        SpecHelper.expectElementText(elem, '.title', program.applied.title);
                        SpecHelper.expectElementText(elem, '.sub-title', program.applied.subtitle);
                    });

                    it('should show as applied with foundation complete', () => {
                        Object.defineProperty(foundationPlaylist, 'complete', {
                            value: true,
                        });

                        render();

                        SpecHelper.expectElementText(elem, '.title', program.applied_foundations_complete.title);
                        SpecHelper.expectElementText(elem, '.sub-title', program.applied_foundations_complete.subtitle);
                    });
                });

                describe('with pre_accepted user', () => {
                    beforeEach(() => {
                        cohortApplication.status = 'pre_accepted';
                    });

                    // skip those that don't have a message
                    if (program.pre_accepted) {
                        it('should show correct message', () => {
                            render();

                            SpecHelper.expectElementText(elem, '.title', program.pre_accepted.title);
                            SpecHelper.expectElementText(elem, '.sub-title', program.pre_accepted.subtitle);
                        });
                    }
                });

                describe('with accepted user', () => {
                    beforeEach(() => {
                        if (!relevantCohort.isDegreeProgram) {
                            // We're mocking shouldSeeQuanticBranding here so that locale message says Smartly instead of Quantic, which is
                            // inline with what we'd actually expect in the wild since non-degree program types are only for Smartly.
                            jest.spyOn(currentUser, 'shouldSeeQuanticBranding', 'get').mockReturnValue(false);
                        }
                        cohortApplication.status = 'accepted';
                    });

                    // skip those that don't have a message
                    if (program.accepted) {
                        it('should show correct message', () => {
                            render();

                            SpecHelper.expectElementText(elem, '.title', program.accepted.title);
                            SpecHelper.expectElementText(elem, '.sub-title', program.accepted.subtitle);
                        });
                    }
                });
            });
        });
    });
});
