import 'AngularSpecHelper';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/lessons';
import 'Lessons/angularModule/spec/_mock/fixtures/streams';

import { DEFAULT_SIGNUP_LOCATION } from 'SignupLocations';
import setSpecLocales from 'Translation/setSpecLocales';
import streamLinkBoxLocales from 'Lessons/locales/lessons/stream/stream_link_box-en.json';
import moment from 'moment-timezone';

setSpecLocales(streamLinkBoxLocales);

describe('Lessons.Stream.StreamLinkBoxDir', () => {
    let $injector;
    let stream;
    let Stream;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let $rootScope;
    let ngToast;
    let DialogModal;
    let EventLogger;
    let $location;
    let isMobile;
    let $timeout;
    let ContentAccessHelper;
    let RouteAnimationHelper;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'SpecHelper');

        angular.mock.module($provide => {
            isMobile = jest.fn();
            isMobile.mockReturnValue(false);
            $provide.value('isMobile', isMobile);
        });

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                Stream = $injector.get('Lesson.Stream');
                ngToast = $injector.get('ngToast');
                DialogModal = $injector.get('DialogModal');
                EventLogger = $injector.get('EventLogger');
                $rootScope = $injector.get('$rootScope');
                $location = $injector.get('$location');
                $timeout = $injector.get('$timeout');
                ContentAccessHelper = $injector.get('ContentAccessHelper');
                RouteAnimationHelper = $injector.get('RouteAnimationHelper');

                $injector.get('LessonFixtures');
                $injector.get('StreamFixtures');
                jest.spyOn($location, 'url').mockImplementation(() => {});
                jest.spyOn(ngToast, 'create').mockImplementation(() => {});
                jest.spyOn(DialogModal, 'alert');
                jest.spyOn(EventLogger.instance, 'log');
            },
        ]);

        SpecHelper.stubConfig();
        SpecHelper.stubEventLogging();
        SpecHelper.stubCurrentUser('learner');
        jest.spyOn(ContentAccessHelper.prototype, 'canLaunch', 'get').mockReturnValue(true);
    });

    function setupStream(opts = {}) {
        stream = Stream.fixtures.getInstance();

        if (!opts.inProgress && !opts.completed) {
            stream.lesson_streams_progress = undefined;
        } else {
            stream.lesson_streams_progress.complete = !!opts.completed;
            stream.lesson_streams_progress.completed_at = 0;
        }

        if (opts.favorite) {
            stream.favorite = true;
        }

        if (opts.comingSoon) {
            if (!opts.canLaunchComingSoon) {
                jest.spyOn(ContentAccessHelper.prototype, 'canLaunch', 'get').mockReturnValue(false);
                jest.spyOn(ContentAccessHelper.prototype, 'reason', 'get').mockReturnValue('coming_soon');
            }
            stream.coming_soon = true;
        }

        if (opts.newCourse) {
            stream.just_added = true;
        }

        if (opts.beta === true) {
            stream.beta = true;
        }

        if (opts.updated === true) {
            stream.just_updated = true;
        }
    }

    function render(navigationEnabled, keepLearning, playlistTrack) {
        renderer = SpecHelper.renderer();
        renderer.scope.stream = stream;
        renderer.scope.nav = navigationEnabled;
        renderer.scope.kl = keepLearning;
        renderer.scope.pt = playlistTrack;
        renderer.render(
            '<stream-link-box stream="stream" bookmark-enabled="true" navigation-enabled="nav" keep-learning="kl" playlist-track="pt"></stream-link-box>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();

        $location.url.mockClear();
    }

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('navigation', () => {
        it('should default to enabled', () => {
            setupStream();
            render();
            SpecHelper.click(elem, 'a');
            $timeout.flush();
            expect($location.url.mock.calls.length > 0).toBe(true);
            expect($location.url).toHaveBeenCalledWith(stream.streamDashboardPath);
        });

        it('should disable when set to false', () => {
            setupStream();
            render(false);
            SpecHelper.click(elem, 'a');
            $timeout.flush();
            expect($location.url.mock.calls.length > 0).toBe(false);
            expect($location.url).not.toHaveBeenCalledWith(stream.streamDashboardPath);
        });
    });

    describe('launch text', () => {
        it('should be correct for a not started stream', () => {
            setupStream();
            render();

            expectSharedStreamCardStuff(elem, stream);
            SpecHelper.expectElementText(elem, '.launch', 'START');
            SpecHelper.expectHasClass(elem, '.course-box', 'not_started');
        });

        it('should be correct for an in progress and favorited stream', () => {
            const streamOpts = {
                inProgress: true,
                favorite: true,
            };
            setupStream(streamOpts);
            render();

            expectSharedStreamCardStuff(elem, stream);
            SpecHelper.expectElementText(elem, '.launch', 'RESUME');
            SpecHelper.expectHasClass(elem, '.course-box', 'in_progress');
        });

        it('should be correct for a completed stream', () => {
            const streamOpts = {
                completed: true,
            };
            setupStream(streamOpts);
            render();

            expectSharedStreamCardStuff(elem, stream);
            SpecHelper.expectElementText(elem, '.launch', 'RETAKE');
            SpecHelper.expectHasClass(elem, '.course-box', 'completed');
        });
    });

    describe('favorite bookmarking', () => {
        function checkBookmarked(ribbon, bookmarked) {
            jest.spyOn(scope, 'toggleBookmark');
            jest.spyOn($rootScope.currentUser, 'toggleBookmark');
            jest.spyOn(scope, '$emit').mockImplementation(() => {});

            ribbon.click();
            scope.$digest();

            expect(scope.toggleBookmark).toHaveBeenCalled();
            expect($rootScope.currentUser.toggleBookmark).toHaveBeenCalled();
            expect(stream.favorite).toBe(bookmarked);
            SpecHelper.expectElementText(elem, '.tooltip', bookmarked ? 'Remove From My Courses' : 'Add to My Courses');
        }

        it('should allow for bookmarking of a non-bookmarked stream', () => {
            setupStream();
            render();

            expect(stream.favorite).toBe(false);

            const ribbon = SpecHelper.expectElement(elem, '.bookmark-ribbon');
            SpecHelper.expectElementDoesNotHaveClass(ribbon, 'enabled');
            checkBookmarked(ribbon, true);
        });

        it('should allow for un-bookmarking of a bookmarked stream', () => {
            const streamOpts = {
                favorite: true,
            };
            setupStream(streamOpts);
            render();

            expect(stream.favorite).toBe(true);

            const ribbon = SpecHelper.expectElement(elem, '.bookmark-ribbon');
            SpecHelper.expectElementHasClass(ribbon, 'enabled');
            checkBookmarked(ribbon, false);
        });

        it('should not display bookmarking if disabled', () => {
            const streamOpts = {
                favorite: true,
            };
            setupStream(streamOpts);
            render();

            SpecHelper.expectElement(elem, '.bookmark-ribbon');

            scope.bookmarkEnabled = false;
            scope.$digest();

            SpecHelper.expectNoElement(elem, '.bookmark-ribbon');
        });

        it('should not display bookmarking if enabled but without a currentUser', () => {
            const streamOpts = {
                favorite: true,
            };
            setupStream(streamOpts);
            $rootScope.currentUser = undefined;
            render();
            SpecHelper.expectNoElement(elem, '.bookmark-ribbon');
        });
    });

    describe('new course', () => {
        it('should display a new course ribbon', () => {
            const streamOpts = {
                newCourse: true,
            };
            setupStream(streamOpts);
            render();
            SpecHelper.expectElementText(elem, '.course-ribbon span', 'NEW');
        });

        it('should not display a new course ribbon if the course is still marked as coming soon', () => {
            const streamOpts = {
                comingSoon: true,
                newCourse: true,
            };
            setupStream(streamOpts);
            render();
            SpecHelper.expectNoElement(elem, '.course-ribbon');
        });
    });

    describe('updated course', () => {
        it('should display an updated course ribbon', () => {
            const streamOpts = {
                updated: true,
            };
            setupStream(streamOpts);
            render();
            SpecHelper.expectElementText(elem, '.course-ribbon span', 'UPDATED');
        });

        it('should not display an updated course ribbon if the course is still marked as coming soon', () => {
            const streamOpts = {
                comingSoon: true,
                updated: true,
            };
            setupStream(streamOpts);
            render();
            SpecHelper.expectNoElement(elem, '.course-ribbon');
        });
    });

    describe('beta course', () => {
        it('should display a beta course label', () => {
            const streamOpts = {
                beta: true,
            };
            setupStream(streamOpts);
            render();
            SpecHelper.expectElementText(elem, '.title > .topic.beta-course', 'BETA');
            SpecHelper.expectElementText(elem, '.topics-box > .topic.beta-course', 'BETA');
        });

        it('should not display a beta course label if the course is still marked as coming soon', () => {
            const streamOpts = {
                comingSoon: true,
                beta: true,
            };
            setupStream(streamOpts);
            render();
            SpecHelper.expectNoElement(elem, '.topic.beta-course');
        });
    });

    describe('coming soon', () => {
        beforeEach(() => {
            setupStream({
                comingSoon: true,
            });
        });

        it('should show coming soon styling even if it can be launched', () => {
            setupStream({
                comingSoon: true,
                canLaunchComingSoon: true,
            });
            render();

            SpecHelper.expectElement(elem, '.coming-soon-icon');
            SpecHelper.expectNoElement(elem, '.course-icon');
            jest.spyOn(RouteAnimationHelper.prototype, 'animatePathChange').mockImplementation(() => {});
            SpecHelper.click(elem, 'a');
            expect(RouteAnimationHelper.prototype.animatePathChange).not.toHaveBeenCalled();
        });

        describe('on desktop', () => {
            beforeEach(() => {
                isMobile.mockReturnValue(false);
                DialogModal.alert.mockClear();
                ngToast.create.mockClear();
                EventLogger.instance.log.mockClear();
                $location.url.mockClear();
            });

            it('should show a coming soon icon instead of normal icon', () => {
                render();

                SpecHelper.expectElement(elem, '.coming-soon-icon');
                SpecHelper.expectNoElement(elem, '.course-icon');
            });

            it('should log an event and show a toast message if authenticated', () => {
                render();
                jest.spyOn(scope, 'notifyMeClicked');
                SpecHelper.click(elem, '.notify-me > button');
                expect(scope.notifyMeClicked).toHaveBeenCalled();

                expect(DialogModal.alert.mock.calls.length).toBe(0);
                expect(ngToast.create).toHaveBeenCalledWith({
                    content: "Thanks! We'll let you know when this course is available.",
                    className: 'info',
                });
                expect(EventLogger.instance.log).toHaveBeenCalledWith(
                    'lesson:stream:notify-coming-soon',
                    stream.logInfo(),
                );
            });

            it('should redirect to join if not authenticated', () => {
                $rootScope.currentUser = undefined;
                render();
                jest.spyOn(scope, 'notifyMeClicked');
                jest.spyOn(scope, 'loadUrl').mockImplementation(() => {});
                SpecHelper.click(elem, '.notify-me > button');
                expect(scope.notifyMeClicked).toHaveBeenCalled();

                expect(DialogModal.alert.mock.calls.length).toBe(0);
                expect(ngToast.create.mock.calls.length).toBe(0);
                expect(scope.loadUrl).toHaveBeenCalledWith(DEFAULT_SIGNUP_LOCATION);
                expect(EventLogger.instance.log).not.toHaveBeenCalledWith(
                    'lesson:stream:notify-coming-soon',
                    stream.logInfo(),
                );
            });
        });

        describe('on mobile', () => {
            describe('web', () => {
                beforeEach(() => {
                    isMobile.mockReturnValue(true);
                    DialogModal.alert.mockClear();
                });

                it('should show a modal', () => {
                    render();
                    expect(DialogModal.alert.mock.calls.length).toBe(0);
                    SpecHelper.click(elem, 'a', 0);
                    expect(DialogModal.alert.mock.calls.length).toBe(1);
                });
            });

            describe('Cordova', () => {
                let $window;
                beforeEach(() => {
                    $window = $injector.get('$window');
                    isMobile.mockReturnValue(true);
                    $window.CORDOVA = true;
                    // stub out native notifications plugin
                    $window.navigator.notification = {
                        confirm() {},
                    };
                    jest.spyOn($window.navigator.notification, 'confirm').mockImplementation(() => {});
                });

                afterEach(() => {
                    $window.CORDOVA = false;
                });

                it('should show a native confirm dialog', () => {
                    assertShowNativeConfirm();
                });

                it('should show a native confirm dialog if not authenticated', () => {
                    $rootScope.currentUser = undefined;
                    assertShowNativeConfirm();
                });

                function assertShowNativeConfirm() {
                    render();
                    jest.spyOn(scope, 'showComingSoonModal');
                    expect(navigator.notification.confirm.mock.calls.length).toBe(0);
                    SpecHelper.click(elem, 'a');
                    expect(scope.showComingSoonModal).toHaveBeenCalled();
                    expect(navigator.notification.confirm.mock.calls.length).toBe(1);
                    expect(navigator.notification.confirm.mock.calls[0][0]).toEqual(
                        `The Quantic content team is hard at work on My Stream 1!\n\nWould you like to be notified when this course is available?`,
                    );
                }
            });
        });
    });

    describe('keep learning', () => {
        it('should have keep learning styles', () => {
            const streamOpts = {
                inProgress: true,
            };
            setupStream(streamOpts);
            render(true, true);
            SpecHelper.expectHasClass(elem, '.course-box', 'keep-learning');
        });
    });

    describe('playlist track', () => {
        it('should have proper styles when part of the playlist track', () => {
            setupStream();
            render(true, false, true);
            SpecHelper.expectHasClass(elem, '.stat-group:eq(0)', 'playlist-track-stat');
        });
    });

    describe('locking', () => {
        it('should show tooltip when user clicks a locked stream', () => {
            setupStream();
            Object.defineProperty(ContentAccessHelper.prototype, 'canLaunch', {
                value: false,
                configurable: true,
            });
            render();
            SpecHelper.expectNoElement(elem, '.locked-tooltip');
            SpecHelper.click(elem, 'a');
            SpecHelper.expectElement(elem, '.locked-tooltip');
            $timeout.flush();
            expect($location.url.mock.calls.length > 0).toBe(false);
            expect($location.url).not.toHaveBeenCalledWith(stream.streamDashboardPath);
        });

        it('should not show tooltip when user clicks an unlocked stream', () => {
            setupStream();
            render();
            SpecHelper.click(elem, 'a');
            SpecHelper.expectNoElement(elem, '.locked-tooltip');
        });
    });

    describe('with exam stream', () => {
        beforeEach(() => {
            setupStream();
            stream.exam = true;
            jest.spyOn(stream, '_now').mockReturnValue(moment.utc(new Date('2016/01/04 12:00')).toDate());
            Object.defineProperty(stream, 'examOpenTime', {
                value: moment.utc(new Date('2016/01/01 12:00')).toDate(),
                configurable: true,
            });
            Object.defineProperty(stream, 'examCloseTime', {
                value: moment.utc(new Date('2016/01/08 12:00')).toDate(),
                configurable: true,
            });
        });

        it('should hide lesson time when not started', () => {
            jest.spyOn(stream, 'progressStatus').mockReturnValue('not_started');
            render();
            SpecHelper.expectElements(elem, '.stat-group', 2);
            SpecHelper.expectElementText(elem, '.stat-group:eq(0)', `LESSONS ${stream.lessons.length}`);
            SpecHelper.expectElementText(elem, '.stat-group:eq(1)', 'START');
        });

        it('should hide lesson time when in progress', () => {
            jest.spyOn(stream, 'progressStatus').mockReturnValue('not_started');
            render();
            SpecHelper.expectElements(elem, '.stat-group', 2);
            SpecHelper.expectElementText(elem, '.stat-group:eq(0)', `LESSONS ${stream.lessons.length}`);
            SpecHelper.expectElementText(elem, '.stat-group:eq(1)', 'START');
        });
    });

    describe('available offline icon', () => {
        let offlineModeManager;
        let streamIsAvailableOffline;

        beforeEach(() => {
            setupStream();
            const $q = $injector.get('$q');
            streamIsAvailableOffline = true;
            offlineModeManager = $injector.get('offlineModeManager');

            // In the wild, streamIsAvailableOffline returns a native promise,
            // but it simplifies testing to return $q
            jest.spyOn(offlineModeManager, 'streamIsAvailableOffline').mockImplementation(() =>
                $q.resolve(streamIsAvailableOffline),
            );
        });

        it('should be in the header-box when there is a playlistTrack', () => {
            render(false, false, true);
            SpecHelper.expectElement(elem, '.header-box .available-offline-icon');
            SpecHelper.expectNoElement(elem, '.topics-box .available-offline-icon');
            SpecHelper.expectElement(elem, '.right-arrow .available-offline-icon'); // mobile icon
        });

        it('should be in the topics-box when there is no playlistTrack', () => {
            render(false, false, false);
            SpecHelper.expectNoElement(elem, '.header-box .available-offline-icon');
            SpecHelper.expectElement(elem, '.topics-box .available-offline-icon');
            SpecHelper.expectElement(elem, '.right-arrow .available-offline-icon'); // mobile icon
        });

        it('should be hidden when the stream is not available offline', () => {
            streamIsAvailableOffline = false;
            render(false, false, false);
            SpecHelper.expectNoElement(elem, '.header-box .available-offline-icon');
            SpecHelper.expectNoElement(elem, '.topics-box .available-offline-icon');
            SpecHelper.expectNoElement(elem, '.right-arrow .available-offline-icon'); // mobile icon
        });
    });

    // eslint-disable-next-line no-shadow
    function expectSharedStreamCardStuff(el, stream) {
        SpecHelper.expectElementText(el, '.content-box .title', stream.title.toUpperCase());
        SpecHelper.expectElementText(el, '.description', stream.description);
    }
});
