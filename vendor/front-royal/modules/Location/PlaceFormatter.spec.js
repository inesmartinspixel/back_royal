import PlaceFormatter from './PlaceFormatter';

const { locationString, placeCity, placeCountry, placeState } = PlaceFormatter;

describe('PlaceFormatter', () => {
    describe('locationString', () => {
        it('should be an empty string if no city in place_details and no mailing address city and state', () => {
            expect(locationString(null)).toBe('');
            expect(locationString({})).toBe('');

            const placeDetails = {
                administrative_area_level_1: {
                    long: 'Bar',
                },
            };
            expect(locationString(placeDetails)).toBe('');
        });

        describe('with place_details', () => {
            it('should work for US place', () => {
                const placeDetails = {
                    formatted_address: 'a formatted address',
                    locality: {
                        long: 'Foo',
                    },
                    administrative_area_level_1: {
                        short: 'Ba',
                        long: 'Bar',
                    },
                    country: {
                        short: 'US',
                    },
                };
                expect(locationString(placeDetails)).toBe('Foo, Ba');

                const city = 'Timbuktoo';
                const state = 'Test';
                const country = 'US';

                expect(locationString(placeDetails, city, state, country)).toBe('Foo, Ba');
            });

            it('should include country code if present and not in the US/USA', () => {
                let placeDetails = {
                    formatted_address: 'a formatted address',
                    locality: {
                        long: 'Foo',
                    },
                    administrative_area_level_1: {
                        short: 'Ba',
                        long: 'Bar',
                    },
                };
                expect(locationString(placeDetails)).toBe('Foo, Ba');

                placeDetails = {
                    formatted_address: 'a formatted address',
                    locality: {
                        long: 'Foo',
                    },
                    administrative_area_level_1: {
                        short: 'Ba',
                        long: 'Bar',
                    },
                    country: {
                        short: 'AA',
                    },
                };
                expect(locationString(placeDetails)).toBe('Foo, Ba, AA');
            });

            // Ex. 'Hong Kong', 'Singapore' (sometimes)
            it('should handle locations with only a locality', () => {
                const placeDetails = {
                    formatted_address: 'a formatted address',
                    locality: {
                        long: 'Foo',
                    },
                    country: {
                        short: 'ZZ',
                    },
                };
                expect(locationString(placeDetails)).toBe('Foo, ZZ');

                const city = 'Timbuktoo';
                const state = 'Test';
                const country = 'US';

                expect(locationString(placeDetails, city, state, country)).toBe('Foo, ZZ');
            });

            // Ex. Singapore (sometimes)
            it('should handle locations with only a country', () => {
                const placeDetails = {
                    country: {
                        long: 'Singapore',
                        short: 'SG',
                    },
                    formatted_address: 'Singapore',
                };
                expect(locationString(placeDetails)).toBe('Singapore');
            });
        });

        describe('with no place_details', () => {
            it('should use an amalgamated string if the place_details is not present', () => {
                const placeDetails = {
                    formatted_address: 'a formatted address',
                    locality: {
                        long: 'Foo',
                    },
                    administrative_area_level_1: {
                        short: 'Ba',
                        long: 'Bar',
                    },
                    country: {
                        short: 'AA',
                    },
                };
                expect(locationString(placeDetails)).toBe('Foo, Ba, AA');

                const city = 'Timbuktoo';
                const state = 'Test';
                const country = 'BB';

                expect(locationString(undefined, city, state, country)).toBe('Timbuktoo, Test, BB');
            });

            it('should only include country code if present and not in the US/USA', () => {
                const placeDetails = {
                    formatted_address: 'a formatted address',
                    locality: {
                        long: 'Foo',
                    },
                    administrative_area_level_1: {
                        short: 'Ba',
                        long: 'Bar',
                    },
                    country: {
                        short: 'AA',
                    },
                };
                expect(locationString(placeDetails)).toBe('Foo, Ba, AA');

                const city = 'Timbuktoo';
                const state = 'Test';
                let country = null;

                expect(locationString(undefined, city, state, country)).toBe('Timbuktoo, Test');

                country = 'BB';
                expect(locationString(undefined, city, state, country)).toBe('Timbuktoo, Test, BB');
            });
        });
    });
    describe('locations that require special handling', () => {
        it('should handle Singapore properly', () => {
            const placeDetails = {
                formatted_address: 'a formatted address',
                locality: {
                    long: 'Singapore',
                },
                administrative_area_level_1: {
                    short: 'Singapore',
                },
                country: {
                    short: 'SG',
                },
            };
            expect(locationString(placeDetails)).toBe('Singapore');
            expect(placeCity(placeDetails)).toBe('Singapore');
            expect(placeState(placeDetails)).toBe('Singapore');
            expect(placeCountry(placeDetails)).toBe('SG');

            const city = 'Singapore';
            const state = 'Singapore';
            const country = 'SG';

            expect(locationString(placeDetails, city, state, country)).toBe('Singapore');
        });

        it('should handle Hong Kong properly', () => {
            const placeDetails = {
                formatted_address: 'a formatted address',
                locality: {
                    long: 'Hong Kong',
                },
                country: {
                    short: 'HK',
                },
            };
            expect(locationString(placeDetails)).toBe('Hong Kong');
            expect(placeCity(placeDetails)).toBe('Hong Kong');
            expect(placeState(placeDetails)).toBeUndefined();
            expect(placeCountry(placeDetails)).toBe('HK');

            const city = 'Hong Kong';
            const country = 'HK';

            expect(locationString(placeDetails, city, undefined, country)).toBe('Hong Kong');
        });

        it('should handle Tokyo properly', () => {
            const placeDetails = {
                colloquial_area: {
                    short: 'Tokyo',
                    long: 'Tokyo',
                },
                administrative_area_level_1: {
                    short: 'Tokyo',
                    long: 'Tokyo',
                },
                country: {
                    short: 'JP',
                    long: 'Japan',
                },
                formatted_address: 'Tokyo, Japan',
            };

            expect(locationString(placeDetails)).toBe('Tokyo, JP');
            expect(placeCity(placeDetails)).toBe('Tokyo');
            expect(placeState(placeDetails)).toBe('Tokyo');
            expect(placeCountry(placeDetails)).toBe('JP');
        });
    });
});
