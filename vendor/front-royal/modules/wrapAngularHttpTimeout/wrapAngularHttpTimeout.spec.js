import wrapAngularHttpTimeout from './wrapAngularHttpTimeout';

describe('wrapAngularHttpTimeout', () => {
    let $timeout;
    let timeoutPromise;
    let injector;

    beforeEach(() => {
        $timeout = delay => {
            timeoutPromise = new Promise(resolve => {
                setTimeout(() => {
                    resolve();
                }, delay);
            });
            return timeoutPromise;
        };
        $timeout.cancel = jest.fn();

        injector = {
            get: key =>
                ({
                    $q: fn => {
                        return new Promise(fn);
                    },
                    $timeout,
                }[key]),
        };
    });

    it('should just pass through the provided value if there is no existing timeout', () => {
        const config = {};
        wrapAngularHttpTimeout(config, 'provided timeout', injector);
        expect(config.timeout).toEqual('provided timeout');
    });

    it('should wrap a promise and resolve when the original promise resolves', async () => {
        let cancelRequest;

        // Here is the config that is coming into $http. Someone set
        // timeout to a promise.  If they call `cancelRequest`, their
        // promise will be resolved and the request should be canceled.
        const config = {
            timeout: new Promise(resolve => {
                cancelRequest = resolve;
            }),
        };

        // The request hits our interceptor
        wrapAngularHttpTimeout(config, new Promise(() => {}), injector);

        // The creator of the request signals their desire to cancel the request
        // by calling cancelRequest, thus resolving their promise.
        cancelRequest();

        await expect(config.timeout).resolves.toBeUndefined();
    });

    it('should wrap a timeout and resolve when the original timeout elapses', async () => {
        const config = {
            timeout: 42,
        };
        wrapAngularHttpTimeout(config, 1000, injector);
        advanceTimersAndRunAllClicks(42);
        await expect(config.timeout).resolves.toBeUndefined();
    });

    it('should wrap an existing value with a promise and resolve when the new promise resolves', async () => {
        const config = {
            timeout: new Promise(() => {}),
        };
        let cancelRequest;
        wrapAngularHttpTimeout(
            config,
            new Promise(resolve => {
                cancelRequest = resolve;
            }),
            injector,
        );
        cancelRequest();
        await expect(config.timeout).resolves.toBeUndefined();
    });

    it('should wrap an existing value with a timeout and resolve when the new timeout elapses', async () => {
        const config = {
            timeout: new Promise(() => {}),
        };
        wrapAngularHttpTimeout(config, 42, injector);
        advanceTimersAndRunAllClicks(42);
        await expect(config.timeout).resolves.toBeUndefined();
    });

    async function advanceTimersAndRunAllClicks(time) {
        // timeouts (macros)
        await jest.advanceTimersByTime(time);

        // promises (micros)
        await jest.runAllTicks();
    }
});
