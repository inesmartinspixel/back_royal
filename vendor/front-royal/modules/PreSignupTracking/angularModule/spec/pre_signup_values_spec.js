import 'AngularSpecHelper';
import 'PreSignupTracking/angularModule';
import ClientStorage from 'ClientStorage';

describe('preSignupValues', () => {
    let $injector;
    let preSignupValues;

    beforeEach(() => {
        angular.mock.module('preSignupTracking', 'SpecHelper');

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            preSignupValues = $injector.get('preSignupValues');
        });
    });

    describe('getDynamicLandingPageMultiStepFormRegistrationInfo', () => {
        it('should return the registration info kept in ClientStorage for the dynamic landing page', () => {
            jest.spyOn(ClientStorage, 'getItem').mockReturnValue('foo');
            const registrationInfo = preSignupValues.getDynamicLandingPageMultiStepFormRegistrationInfo();
            expect(ClientStorage.getItem.mock.calls.flat()).toEqual([
                'primary_reason_for_applying',
                'survey_highest_level_completed_education_description',
                'survey_most_recent_role_description',
                'survey_years_full_time_experience',
                'salary',
                'area_of_study',
                'experiment_id',
            ]);
            expect(registrationInfo).toEqual({
                primary_reason_for_applying: 'foo',
                survey_highest_level_completed_education_description: 'foo',
                survey_most_recent_role_description: 'foo',
                survey_years_full_time_experience: 'foo',
                salary: 'foo',
                area_of_study: 'foo',
                experiment_id: 'foo',
            });
        });
    });

    describe('convertRegistrationInfoFromDLPToHumanReadableForm', () => {
        it('should convert the given registration info to the human readable version', () => {
            const registrationInfo = {
                primary_reason_for_applying: 'start_own_company',
                survey_highest_level_completed_education_description: 'high_school',
                survey_most_recent_role_description: 'senior_management_role',
                survey_years_full_time_experience: '0_2',
                salary: 'prefer_not_to_disclose',
                area_of_study: 'tech_engineering_math',
                experiment_id: 'h',
            };
            const humanReadableRegistrationInfo = preSignupValues.convertRegistrationInfoFromDLPToHumanReadableForm(
                registrationInfo,
            );
            expect(humanReadableRegistrationInfo).toEqual({
                primary_reason_for_applying: 'To start my own company',
                survey_highest_level_completed_education_description: 'High School',
                survey_most_recent_role_description: 'Senior Management (VP / C-level)',
                survey_years_full_time_experience: '0-2',
                salary: 'Prefer not to disclose',
                area_of_study: 'Tech / Engineering / Math',
                experiment_id: 'h',
            });
        });
    });

    describe('goodLead', () => {
        it('should be true only if every relevant "good lead" property in the given registration info has a value considered to be for a "good lead"', () => {
            expect(preSignupValues.GOOD_LEAD_PROPERTIES).toEqual([
                'survey_highest_level_completed_education_description',
                'survey_years_full_time_experience',
                'survey_most_recent_role_description',
            ]);
            const registrationInfo = {
                primary_reason_for_applying: 'start_own_company', // not a "good lead" property
                survey_highest_level_completed_education_description: 'high_school', // "good lead" property, but not a "good lead" value
                survey_most_recent_role_description: 'administrative_role', // "good lead" property, but not a "good lead" value
                survey_years_full_time_experience: '0_2', // "good lead" property, but not a "good lead" value
                salary: 'prefer_not_to_disclose', // not a "good lead" property
                area_of_study: 'tech_engineering_math', // not a "good lead" property
                experiment_id: 'h', // not a "good lead" property
            };
            expect(preSignupValues.goodLead(registrationInfo)).toBe(false);

            // Change each "good lead" property one at a time to be a "good lead" value
            // and verify that it's not calculated as a "good lead" until EVERY relevant
            // "good lead" property has a valid "good lead" value.
            registrationInfo.survey_highest_level_completed_education_description = 'undergraduate_bachelor'; // "good lead" value
            expect(preSignupValues.goodLead(registrationInfo)).toBe(false);

            registrationInfo.survey_most_recent_role_description = 'professional_role'; // "good lead" value
            expect(preSignupValues.goodLead(registrationInfo)).toBe(false);

            registrationInfo.survey_years_full_time_experience = '7_10'; // "good lead" value
            expect(preSignupValues.goodLead(registrationInfo)).toBe(true);

            // test some other "good lead" values
            registrationInfo.survey_highest_level_completed_education_description = 'masters'; // "good lead" value
            registrationInfo.survey_most_recent_role_description = 'management_role'; // "good lead" value
            registrationInfo.survey_years_full_time_experience = '11_15'; // "good lead" value
            expect(preSignupValues.goodLead(registrationInfo)).toBe(true);

            registrationInfo.survey_highest_level_completed_education_description = 'doctorate'; // "good lead" value
            registrationInfo.survey_most_recent_role_description = 'senior_management_role'; // "good lead" value
            registrationInfo.survey_years_full_time_experience = '16_plus'; // "good lead" value
            expect(preSignupValues.goodLead(registrationInfo)).toBe(true);
        });
    });
});
