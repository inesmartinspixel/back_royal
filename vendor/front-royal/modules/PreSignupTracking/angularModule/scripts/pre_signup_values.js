import ClientStorage from 'ClientStorage';
import angularModule from './pre_signup_tracking_module';

angularModule.factory('preSignupValues', [
    '$injector',
    function factory() {
        return {
            // The values of certain properties are used to determine whether or not
            // a user is considered a "good lead". See https://trello.com/c/sqglpXD2.
            GOOD_LEAD_PROPERTIES: [
                'survey_highest_level_completed_education_description',
                'survey_years_full_time_experience',
                'survey_most_recent_role_description',
            ],
            // See https://trello.com/c/sqglpXD2 for an explanation
            // of what the `gl` property is used for.
            DYNAMIC_LANDING_PAGE_MULTI_STEP_FORM_KEYS_TO_VALUES_MAP: {
                primary_reason_for_applying: {
                    start_own_company: { text: 'To start my own company' },
                    advance_in_current_company: { text: 'To advance in my current organization' },
                    get_better_job: { text: 'To change organizations / career' },
                    expand_business_knowledge: { text: 'To expand my business knowledge' },
                    more_effective_leader: { text: 'To become a more effective leader' },
                    other: { text: 'Other' },
                },
                survey_highest_level_completed_education_description: {
                    high_school: { text: 'High School' },
                    associates: { text: "Associate's Degree" },
                    undergraduate_bachelor: { text: 'Undergraduate University / Bachelor’s Degree', gl: true },
                    masters: { text: 'Master’s Degree', gl: true },
                    doctorate: { text: 'Doctorate', gl: true },
                },
                survey_most_recent_role_description: {
                    senior_management_role: { text: 'Senior Management (VP / C-level)', gl: true },
                    management_role: { text: 'Management', gl: true },
                    professional_role: { text: 'Professional', gl: true },
                    administrative_role: { text: 'Administrative' },
                    skilled_labor_role: { text: 'Skilled Labor' },
                    service_role: { text: 'Service' },
                    retail_role: { text: 'Retail' },
                },
                survey_years_full_time_experience: {
                    '0_2': { text: '0-2' },
                    '3_6': { text: '3-6' },
                    '7_10': { text: '7-10', gl: true },
                    '11_15': { text: '11-15', gl: true },
                    '16_plus': { text: '16+', gl: true },
                },
                salary: {
                    prefer_not_to_disclose: { text: 'Prefer not to disclose' },
                    less_than_40000: { text: 'Less than $40,000 USD' },
                    '40000_to_49999': { text: '$40,000 to $49,999 USD' },
                    '50000_to_59999': { text: '$50,000 to $59,999 USD' },
                    '60000_to_69999': { text: '$60,000 to $69,999 USD' },
                    '70000_to_79999': { text: '$70,000 to $79,999 USD' },
                    '80000_to_89999': { text: '$80,000 to $89,999 USD' },
                    '90000_to_99999': { text: '$90,000 to $99,999 USD' },
                    '100000_to_119999': { text: '$100,000 to $119,999 USD' },
                    '120000_to_149999': { text: '$120,000 to $149,999 USD' },
                    '150000_to_199999': { text: '$150,000 to $199,999 USD' },
                    over_200000: { text: 'Over $200,000 USD' },
                },
                area_of_study: {
                    tech_engineering_math: { text: 'Tech / Engineering / Math' },
                    bioscience_medicine_pharma: { text: 'Bioscience / Medicine / Pharma' },
                    business_economics: { text: 'Business / Economics' },
                    humanities_arts_languages: { text: 'Humanities / Arts / Languages' },
                    social_sciences: { text: 'Social Sciences' },
                    other: { text: 'Other' },
                },
                experiment_id: {
                    h: { text: 'h' },
                    s: { text: 's' },
                },
            },

            all() {
                return this.getDynamicLandingPageMultiStepFormRegistrationInfo();
            },

            clearAll() {
                _.chain(this.all())
                    .keys()
                    .each(key => {
                        ClientStorage.removeItem(key);
                    });
            },

            storeDynamicLandingPageMultiStepFormInClientStorage(preSignupForm) {
                if (_.any(preSignupForm)) {
                    _.each(preSignupForm, (value, key) => {
                        ClientStorage.setItem(key, value);
                    });
                }
            },

            getDynamicLandingPageMultiStepFormRegistrationInfo() {
                const registrationInfo = {};
                Object.keys(this.DYNAMIC_LANDING_PAGE_MULTI_STEP_FORM_KEYS_TO_VALUES_MAP).forEach(dbColumnName => {
                    registrationInfo[dbColumnName] = ClientStorage.getItem(dbColumnName);
                });
                return registrationInfo;
            },

            convertRegistrationInfoFromDLPToHumanReadableForm(registrationInfo) {
                const humanReadableValues = {};
                Object.keys(this.DYNAMIC_LANDING_PAGE_MULTI_STEP_FORM_KEYS_TO_VALUES_MAP).forEach(dbColumnName => {
                    const keyToValuesMap = this.DYNAMIC_LANDING_PAGE_MULTI_STEP_FORM_KEYS_TO_VALUES_MAP[dbColumnName];
                    const dbValue = registrationInfo[dbColumnName];
                    humanReadableValues[dbColumnName] = keyToValuesMap[dbValue]?.text;
                });
                return humanReadableValues;
            },

            // Calculates whether or not the registrationInfo is for a "good lead".
            // See https://trello.com/c/sqglpXD2.
            goodLead(registrationInfo) {
                return this.GOOD_LEAD_PROPERTIES.every(prop => {
                    const value = registrationInfo[prop];
                    return this.DYNAMIC_LANDING_PAGE_MULTI_STEP_FORM_KEYS_TO_VALUES_MAP[prop][value]?.gl;
                });
            },
        };
    },
]);
