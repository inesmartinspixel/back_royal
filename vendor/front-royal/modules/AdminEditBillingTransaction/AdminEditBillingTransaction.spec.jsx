// import React from 'react';
// import AdminEditBillingTransaction from './index';
// import { mount } from 'enzyme';
// import { Formik } from 'formik';

// describe('AdminEditBillingTransaction', () => {
//     let billingTransaction, savePromise, resolve, savedProxy;

//     function BillingTransaction(attrs){
//         Object.keys(attrs).forEach(key => {
//             this[key] = attrs[key];
//         });
//         console.log('init', this.asJson());
//     }

//     BillingTransaction.prototype.asJson = function() {
//         return JSON.parse(JSON.stringify(this));
//     };
//     BillingTransaction.new = function(attrs) {
//         console.log('new attrs;', attrs);
//         return new BillingTransaction(attrs);
//     };

//     beforeEach(() => {
//         billingTransaction = new BillingTransaction({
//             id: 'id',
//             transaction_type: 'payment'
//         });
//         savePromise = new Promise(_resolve => {
//             resolve = _resolve;
//         });

//         // We do not use an arrow function here because we need to
//         // access `this` inside of the function
//         BillingTransaction.prototype.save = jest.fn(function() {
//             savedProxy = this;
//             return savePromise;
//         });
//     });

//     it('should save the payment on submit', () => {
//         const wrapper = mount(<AdminEditBillingTransaction thing={billingTransaction} passthrough={{owner: {id: 'ownerId'}}} />);

//         console.log(wrapper.find('[type="submit"]').html())
//         console.log(wrapper.find('[name="provider"]').hostNodes().props());
//         wrapper.find('[name="provider"]').hostNodes().props().onChange({target: {name: 'provider', value: 'SVB - 38322'}});
//         wrapper.update();
//         console.log(wrapper.find('[name="provider"]').hostNodes().html())
//         wrapper.find('[name="provider"]').hostNodes().props().onChange({target: {name: 'provider', value: ''}});
//         wrapper.update();
//         console.log(wrapper.find('[name="provider"]').hostNodes().html())
//         wrapper.find('[type="submit"]').simulate('click');

//         // This doesn't really work as an integration spec, because you're not getting the values from the form.
//         //wrapper.find(Formik).props().onSubmit();

//         expect(BillingTransaction.prototype.save).toHaveBeenCalledWith({owner_id: 'ownerId'});
//         console.log('savedProxy', savedProxy.asJson());
//         flunk('finish');

//     });

// });

import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import AdminEditBillingTransaction from './index';
import 'babel-polyfill'; // needed for asynchronous specs.  expected? https://github.com/facebook/jest/issues/3126

// I haven't gotten this all working yet, or even really decided if it's a good
// idea, so xdescribe for now. (Note that I was working on this with most of the
// validation schema disabled so I could tell if the button was actually working.)
xdescribe('AdminEditBillingTransaction', () => {
    let billingTransaction;
    let expectSave;

    function BillingTransaction(attrs) {
        Object.keys(attrs).forEach(key => {
            this[key] = attrs[key];
        });
    }

    BillingTransaction.prototype.asJson = () => JSON.parse(JSON.stringify(this));
    BillingTransaction.new = attrs => new BillingTransaction(attrs);

    beforeEach(() => {
        billingTransaction = new BillingTransaction({
            id: 'id',
            transaction_type: 'payment',
        });

        expectSave = new Promise(resolve => {
            // We do not use an arrow function here because we need to
            // access `this` inside of the function
            BillingTransaction.prototype.save = jest.fn(function () {
                const proxy = this;
                resolve(proxy);
                return new Promise();
            });
        });
    });

    it('should save the payment on submit', async () => {
        const { getByText, getByLabelText } = render(
            <AdminEditBillingTransaction thing={billingTransaction} passthrough={{ owner: { id: 'ownerId' } }} />,
        );
        // const {getByText, getByLabelText} = render(
        //     <form onSubmit={() => console.log('submitted')}>
        //         <button type="submit">Save</button>
        //     </form>
        // );
        fireEvent.click(getByLabelText('Provider'));
        console.log(getByLabelText('Provider').innerHTML);
        getByLabelText('Provider').onChange({ value: 'transactionId' });
        fireEvent.change(getByLabelText('Provider Transaction ID'), { target: { value: 'transactionId' } });
        fireEvent.click(getByText('Save'));
        // console.log('submit button disabled? ' , getByText('Save').disabled);
        // fireEvent.submit(getByTestId('form'));
        // const start = new Date();
        // await wait(() => expect(new Date() - start > 100).toBe(true))
        // console.log(getByTestId('form'))

        // done();
        await expectSave.then(proxy => {
            console.log('SAVED!', proxy.asJson());
        });
        // await (new Promise(resolve => {
        //     console.log('setting timeout')
        //     setInterval(() => {
        //         console.log('timeout');
        //         resolve();
        //     }, 1000);
        // }));
    });
});
