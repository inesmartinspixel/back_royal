import React from 'react';
import { Formik, Form } from 'formik';
import BillingTransactionContext from './BillingTransactionContext';

// In order for `connect` to send a formik object down into
// our fields, we have to wrap these in a form
function MockForm(props) {
    return (
        <BillingTransactionContext.Provider value={props.billingTransaction}>
            <Formik
                initialValues={props.billingTransaction}
                render={formik => (
                    <Form className="front-royal-form-container admin-edit-billing-transaction">
                        {props.children}

                        {/* This is awkward.  Sometimes we need access to the values,
                        and I don't know any other way to get them besides somehow
                        sending them up from here.
                    */}
                        <button type="submit" name="submit" onClick={() => props.onSubmit(formik.values)} />
                    </Form>
                )}
            />
        </BillingTransactionContext.Provider>
    );
}

export { MockForm };
