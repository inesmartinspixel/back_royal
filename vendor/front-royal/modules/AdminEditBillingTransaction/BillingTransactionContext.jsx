import React from 'react';

const BillingTransactionContext = React.createContext();

export default BillingTransactionContext;
