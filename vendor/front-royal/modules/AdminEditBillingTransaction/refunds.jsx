import React, { useContext } from 'react';
import ReactIf from 'ReactIf';
import { Field, FieldRow, DatePicker, ErrorMessage } from 'FrontRoyalForm';
import { FieldArray, connect } from 'formik';
import { flow, sortBy, map } from 'lodash/fp';
import { PROVIDER_SVB, PROVIDER_PAYPAL, hasUnselectableProvider } from 'BillingTransaction';
import { generateGuid } from 'guid';
import disabledIfUnselectableProvider from './disabledIfUnselectableProvider';
import BillingTransactionContext from './BillingTransactionContext';
import CurrencyMarker from './CurrencyMarker';

const RefundsRow = connect(props => {
    const values = props.formik.values;
    const billingTransaction = useContext(BillingTransactionContext);
    const provider = values.provider;
    const mapWithIndex = map.convert({ cap: false });

    return (
        <FieldRow label="Refunds" required={false}>
            <FieldArray
                name="refunds"
                render={arrayHelpers => (
                    <>
                        {flow(
                            sortBy(refund => refund.created_at),
                            mapWithIndex((refund, index) => (
                                <RefundEditor key={refund.id} {...{ index, refund, arrayHelpers, provider }} />
                            )),
                        )(values.refunds)}
                        <ErrorMessage name="amount_refunded" className="amount_refunded" showWhenUntouched />

                        <button
                            className="flat blue auto-width"
                            type="button"
                            name="add-refund"
                            disabled={hasUnselectableProvider(billingTransaction)}
                            onClick={() =>
                                arrayHelpers.push({
                                    id: generateGuid(),
                                    refund_time: new Date(),
                                    amount: values.amount,
                                })
                            }
                        >
                            Add Refund
                        </button>
                    </>
                )}
            />
        </FieldRow>
    );
});

function RefundEditor(props) {
    const { provider, arrayHelpers, index } = props;

    return (
        <div className="refund-editor">
            <div className="inline-block">
                <CurrencyMarker />
                <Field
                    className="currency-input"
                    name={`refunds.${index}.amount`}
                    {...disabledIfUnselectableProvider()}
                />
            </div>
            &nbsp;
            <Field
                className="inline-block"
                component={DatePicker}
                name={`refunds.${index}.refund_time`}
                showTimeInput
                {...disabledIfUnselectableProvider()}
            />
            {/* This needs the div block to force a line break even when the date is unset and the DatePicker
            is small enought that it can all fit on one line */}
            <div>
                <Field
                    className="refund-transaction-id"
                    name={`refunds.${index}.provider_transaction_id`}
                    placeholder="Transaction ID"
                    {...disabledIfUnselectableProvider()}
                />
            </div>
            {/* We could add an external link here, but it would only be relevant in the paypal case, and
            I don't think we have ever issued a paypal refund.  In the stripe case, refunds do not get their
            own page, so the link above to the billing transaction would be the link to follow.  In the SVB case,
            there are no links at all.
         */}
            <div className="sub-text note">
                <ReactIf if={provider === PROVIDER_SVB}>
                    Use the <i>Case Number</i> for the refund.
                </ReactIf>
                <ReactIf if={provider === PROVIDER_PAYPAL}>
                    Use the <i>Transaction ID</i>.
                </ReactIf>
            </div>
            <button
                className="flat red remove"
                onClick={() => arrayHelpers.remove(index)}
                {...disabledIfUnselectableProvider()}
            >
                <i className="far fa-trash-alt" name="remove-item" />
            </button>
        </div>
    );
}

export { RefundsRow };
