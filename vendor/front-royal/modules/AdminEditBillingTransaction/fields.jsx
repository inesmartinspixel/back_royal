import React, { useContext } from 'react';
import { Field, DatePicker, Select, FieldRow } from 'FrontRoyalForm';
import ReactIf from 'ReactIf';
import {
    providerOptionsForSelectInput,
    TRANSACTION_TYPE_OPTIONS,
    externalLink,
    PROVIDER_SVB,
    PROVIDER_PAYPAL,
} from 'BillingTransaction';
import { connect } from 'formik';
import { generateGuid } from 'guid';
import BillingTransactionContext from './BillingTransactionContext';
import disabledIfUnselectableProvider from './disabledIfUnselectableProvider';
import CurrencyMarker from './CurrencyMarker';

function ProviderRow() {
    const id = generateGuid();
    const billingTransaction = useContext(BillingTransactionContext);
    return (
        <FieldRow label="Provider" for={id}>
            <Field
                id={id}
                name="provider"
                component={Select}
                options={providerOptionsForSelectInput(billingTransaction)}
                getOptionLabel={option => option.name}
                {...disabledIfUnselectableProvider()}
            />
        </FieldRow>
    );
}

// Need to use `connect` here in order to get access to props.formik
const ProviderTransactionIdRow = connect(props => {
    const values = props.formik.values;
    const billingTransaction = useContext(BillingTransactionContext);
    const _externalLink = externalLink(
        values.provider,
        values.provider_transaction_id,
        billingTransaction.stripe_livemode,
    );
    const provider = values.provider;
    const id = generateGuid();

    return (
        <FieldRow label="Provider Transaction ID" for={id}>
            <Field name="provider_transaction_id" type="text" id={id} {...disabledIfUnselectableProvider()} />

            {_externalLink && (
                <a href={_externalLink} target="_blank" rel="noopener noreferrer" name="external_link">
                    &nbsp;
                    <i className="fas fa-external-link-alt" />
                </a>
            )}

            <div className="sub-text note">
                <ReactIf if={provider === PROVIDER_SVB}>
                    Use the <i>Bank Reference</i> number.
                </ReactIf>
                <ReactIf if={provider === PROVIDER_PAYPAL}>
                    Use the <i>Transaction ID</i>.
                </ReactIf>
            </div>
        </FieldRow>
    );
});

function TransactionTypeRow() {
    return (
        <FieldRow label="Type">
            <Field
                name="transaction_type"
                options={TRANSACTION_TYPE_OPTIONS}
                component={Select}
                {...disabledIfUnselectableProvider()}
            />
        </FieldRow>
    );
}

function AmountRow() {
    return (
        <FieldRow label="Amount">
            <CurrencyMarker />
            <Field name="amount" {...disabledIfUnselectableProvider()} />
        </FieldRow>
    );
}

function TransactionTimeRow() {
    return (
        <FieldRow label="Time">
            <Field component={DatePicker} name="transaction_time" showTimeInput {...disabledIfUnselectableProvider()} />
        </FieldRow>
    );
}

function DescriptionRow() {
    return (
        <FieldRow label="Description" required={false}>
            <Field name="description" component="textarea" {...disabledIfUnselectableProvider()} />
        </FieldRow>
    );
}

export { ProviderRow, ProviderTransactionIdRow, TransactionTypeRow, TransactionTimeRow, AmountRow, DescriptionRow };
