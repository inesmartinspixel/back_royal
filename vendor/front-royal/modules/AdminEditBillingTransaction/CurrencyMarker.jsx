import React from 'react';

export default function CurrencyMarker() {
    return <span className="currency-marker">$</span>;
}
