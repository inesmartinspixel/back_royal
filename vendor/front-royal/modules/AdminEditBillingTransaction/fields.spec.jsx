import React from 'react';
import { mount } from 'enzyme';
import { PROVIDER_SVB, PROVIDER_PAYPAL, externalLink, hasUnselectableProvider } from 'BillingTransaction';
import { ProviderTransactionIdRow } from './fields';
import { MockForm } from './specHelper';

jest.mock('BillingTransaction', () => ({
    externalLink: jest.fn(),
    hasUnselectableProvider: jest.fn(),
    PROVIDER_SVB: 'svb',
    PROVIDER_PAYPAL: 'paypal',
}));

describe('AdminEditBillingTransaction._fields', () => {
    let billingTransaction;

    beforeEach(() => {
        billingTransaction = {};
    });

    describe('ProviderTransactionIdRow', () => {
        it('should show the externalLink if there is one', () => {
            externalLink.mockReturnValue('http://path/to/transaction');
            const wrapper = mount(
                <MockForm billingTransaction={billingTransaction}>
                    <ProviderTransactionIdRow />
                </MockForm>,
            );
            const aProps = wrapper.find('a').props();
            expect(aProps.href).toEqual('http://path/to/transaction');
        });

        it('should not show the externalLink if there is none', () => {
            externalLink.mockReturnValue(null);
            const wrapper = mount(
                <MockForm billingTransaction={billingTransaction}>
                    <ProviderTransactionIdRow />
                </MockForm>,
            );
            expect(wrapper.find('a').length).toEqual(0);
        });

        it('should show the appropriate billingTransaction_id_note', () => {
            assertNote(PROVIDER_SVB, 'Use the <i>Bank Reference</i> number.');
            assertNote(PROVIDER_PAYPAL, 'Use the <i>Transaction ID</i>.');
            assertNote('other', '');
        });

        function assertNote(provider, text) {
            billingTransaction.provider = provider;
            const wrapper = mount(
                <MockForm billingTransaction={billingTransaction}>
                    <ProviderTransactionIdRow />
                </MockForm>,
            );
            const html = wrapper.find('.note').html();
            expect(html).toEqual(`<div class="sub-text note">${text}</div>`);
        }
    });

    describe('disabledIfUnselectableProvider', () => {
        it('should be disabled if hasUnselectableProvider', () => {
            hasUnselectableProvider.mockReturnValue(true);
            const wrapper = mount(
                <MockForm billingTransaction={billingTransaction}>
                    <ProviderTransactionIdRow />
                </MockForm>,
            );
            const inputProps = wrapper.find('input').props();
            expect(inputProps.disabled).toBe(true);
        });

        it('should not be disabled if !hasUnselectableProvider', () => {
            hasUnselectableProvider.mockReturnValue(false);
            const wrapper = mount(
                <MockForm billingTransaction={billingTransaction}>
                    <ProviderTransactionIdRow />
                </MockForm>,
            );
            const inputProps = wrapper.find('input').props();
            expect(inputProps.disabled).toBe(false);
        });
    });
});
