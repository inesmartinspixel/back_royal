import React from 'react';
import { mount } from 'enzyme';
import { PROVIDER_SVB, PROVIDER_PAYPAL } from 'BillingTransaction';
import { RefundsRow } from './refunds';
import { MockForm } from './specHelper';

describe('RefundsRow', () => {
    it('should have a refund editor for each refund', () => {
        const billingTransaction = {
            refunds: [{ id: 'a' }, { id: 'b' }],
        };
        const wrapper = mount(
            <MockForm billingTransaction={billingTransaction}>
                <RefundsRow />
            </MockForm>,
        );
        expect(wrapper.find('.refund-editor').length).toEqual(2);
    });

    it('should have a button that adds a refund', () => {
        const billingTransaction = {
            amount: 42,
            refunds: [{ id: 'a' }],
        };

        // This function will run when the form is
        // submitted below
        function submit(values) {
            const newRefund = values.refunds[1];
            expect(newRefund.amount).toEqual(billingTransaction.amount);
            expect(new Date().getTime() - newRefund.refund_time.getTime() < 1000).toBe(true);
        }
        const wrapper = mount(
            <MockForm onSubmit={submit} billingTransaction={billingTransaction}>
                <RefundsRow />
            </MockForm>,
        );

        wrapper.find('button[name="add-refund"]').simulate('click');
        expect(wrapper.find('.refund-editor').length).toEqual(2);
        wrapper.find('button[name="submit"]').simulate('click');
    });

    describe('RefundEditor', () => {
        it('should show the appropriate billingTransaction_id_note', () => {
            assertNote(PROVIDER_SVB, 'Use the <i>Case Number</i> for the refund.');
            assertNote(PROVIDER_PAYPAL, 'Use the <i>Transaction ID</i>.');
            assertNote('other', '');
        });

        it('should have a remove button', () => {
            const billingTransaction = {
                refunds: [{ id: 'a' }],
            };
            const wrapper = mount(
                <MockForm billingTransaction={billingTransaction}>
                    <RefundsRow />
                </MockForm>,
            );
            expect(wrapper.find('.refund-editor').length).toEqual(1);
            wrapper.find('button.remove').simulate('click');
            expect(wrapper.find('.refund-editor').length).toEqual(0);
        });

        function assertNote(provider, text) {
            const billingTransaction = {
                refunds: [{ id: 'a' }],
                provider,
            };
            const wrapper = mount(
                <MockForm billingTransaction={billingTransaction}>
                    <RefundsRow />
                </MockForm>,
            );
            const html = wrapper.find('.note').html();
            expect(html).toEqual(`<div class="sub-text note">${text}</div>`);
        }
    });
});
