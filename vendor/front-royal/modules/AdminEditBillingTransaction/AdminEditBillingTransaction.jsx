import React from 'react';
import ReactIf from 'ReactIf';
import { BackButton, EditFormActionButtons, editFormHelper } from 'EditableThingsList';
import { Formik, Form } from 'formik';
import { toForm, fromForm, isPayment, validationSchema, hasUnselectableProvider } from 'BillingTransaction';
import {
    ProviderRow,
    ProviderTransactionIdRow,
    TransactionTypeRow,
    TransactionTimeRow,
    AmountRow,
    DescriptionRow,
} from './fields';
import BillingTransactionContext from './BillingTransactionContext';
import { RefundsRow } from './refunds';
import './AdminEditBillingTransaction.scss';

function AdminEditBillingTransaction(props) {
    const { thing: billingTransaction, editableThingsListViewModel, passthrough, $injector } = props;

    const [initialValues, onSubmit] = editFormHelper({
        thing: billingTransaction,
        editableThingsListViewModel,
        fromForm,
        toForm,
        $injector,
        metaData: () => ({ owner_id: passthrough.owner.id }),
    });

    return (
        <>
            <BackButton label="Back to transactions" editableThingsListViewModel={editableThingsListViewModel} />

            <BillingTransactionContext.Provider value={billingTransaction}>
                <Formik
                    initialValues={initialValues}
                    validationSchema={validationSchema}
                    onSubmit={onSubmit}
                    render={formik => (
                        <Form className="front-royal-form-container admin-edit-billing-transaction">
                            <TransactionTypeRow />

                            <ReactIf if={isPayment(formik.values)}>
                                <ProviderRow />
                                <ProviderTransactionIdRow />
                            </ReactIf>

                            <TransactionTimeRow />
                            <AmountRow />
                            <ReactIf if={isPayment(formik.values)}>
                                <RefundsRow />
                            </ReactIf>
                            <DescriptionRow />

                            <EditFormActionButtons
                                {...{
                                    thing: billingTransaction,
                                    formik,
                                    editableThingsListViewModel,
                                    disabled: hasUnselectableProvider(billingTransaction),
                                }}
                            />

                            {/* <pre>{JSON.stringify(formik.errors)}</pre> */}
                        </Form>
                    )}
                />
            </BillingTransactionContext.Provider>
        </>
    );
}

export default AdminEditBillingTransaction;
