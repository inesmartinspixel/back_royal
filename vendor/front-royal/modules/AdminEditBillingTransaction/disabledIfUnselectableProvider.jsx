import { useContext } from 'react';
import { hasUnselectableProvider } from 'BillingTransaction';
import BillingTransactionContext from './BillingTransactionContext';

function disabledIfUnselectableProvider() {
    const billingTransaction = useContext(BillingTransactionContext);
    return {
        disabled: hasUnselectableProvider(billingTransaction),
    };
}

export default disabledIfUnselectableProvider;
