String.prototype.bool = function () {
    return /^true$/i.test(this);
};

String.prototype.insertAt = function (index, string) {
    if (index > 0) {
        return this.substring(0, index) + string + this.substring(index, this.length);
    }

    return string + this;
};

String.prototype.camelize = function (capitalize) {
    if (capitalize === true) {
        return this.replace(/(?:^|[-_])(\w)/g, (_, c) => (c ? c.toUpperCase() : ''));
    } else {
        return this.replace(/(?:[-_])(\w)/g, (_, c) => (c ? c.toUpperCase() : ''));
    }
};

String.prototype.snakeCase = function (separator) {
    const SNAKE_CASE_REGEXP = /[A-Z]/g;
    separator = separator || '-';
    return this.replace(SNAKE_CASE_REGEXP, (letter, pos) => (pos ? separator : '') + letter.toLowerCase());
};

String.prototype.underscore = function () {
    if (this === '') {
        return this;
    }
    const lowerCased = this[0].toLowerCase() + this.substr(1, this.length);
    const dashesReplaced = lowerCased.replace('-', '_');
    return dashesReplaced.replace(/([A-Z])/g, $1 => `_${$1.toLowerCase()}`);
};

String.padNumber = (n, width, z = '0') => {
    n = `${n}`;
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
};
