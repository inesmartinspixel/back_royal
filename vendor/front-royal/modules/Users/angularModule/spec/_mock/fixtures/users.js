angular.module('FrontRoyal.Users.Fixtures', []).factory('UserFixtures', [
    '$injector',
    $injector => {
        const User = $injector.get('User');
        const Institution = $injector.get('Institution');
        const guid = $injector.get('guid');

        User.fixtures = {
            sampleAttrs(overrides = {}) {
                const id = overrides.id || guid.generate();
                const activeInstitution = Institution.new({
                    id: Institution.QUANTIC_ID,
                    name: 'Quantic',
                    branding: 'quantic',
                    external: false,
                });

                return angular.extend(
                    {
                        id,
                        email: `${guid.generate()}@things.com`,
                        name: 'Cool Brent',
                        created_at: '1505354032',
                        roles: [
                            {
                                id: 2,
                                name: 'learner',
                                resource_id: null,
                            },
                        ],
                        provider: 'email',
                        phone: null,
                        confirmed_profile_info: true,
                        groups: [
                            {
                                name: 'abc',
                            },
                        ],
                        institutions: [activeInstitution],
                        active_institution: activeInstitution,
                        pref_locale: 'en',
                        pref_show_photos_names: true,
                        pref_keyboard_shortcuts: true,
                        pref_one_click_reach_out: true,
                        pref_positions_candidate_list: true,

                        // if this is true, then you need a relevant cohort
                        // see also: content_access_helper_spec.js for explicit testing
                        mba_content_lockable: false,

                        cohort_applications: [],
                        hiring_application: null,
                        avatar_url: 'http://path/to/avatar',
                        has_seen_welcome: true,
                        subscriptions: [],
                        experiment_ids: [],
                    },
                    overrides,
                );
            },

            sampleLearnerAttrs(overrides) {
                return this.sampleAttrsForRole('learner', overrides);
            },

            sampleEditorAttrs(overrides) {
                return this.sampleAttrsForRole('learner', overrides);
            },

            sampleAdminAttrs(overrides) {
                return this.sampleAttrsForRole('learner', overrides);
            },

            sampleSuperEditorAttrs(overrides) {
                return this.sampleAttrsForRole('super_editor', overrides);
            },

            sampleAttrsForRole(role, overrides = {}) {
                overrides.roles = overrides.roles || {};
                overrides.roles = [
                    {
                        name: role,
                        resource_id: null,
                    },
                ];
                overrides.roles[0].id = {
                    admin: 1,
                    learner: 2,
                    editor: 3,
                    super_editor: 4,
                };
                return this.sampleAttrs(overrides);
            },

            getInstance(overrides) {
                return User.new(this.sampleAttrs(overrides));
            },

            getLearner(overrides) {
                return User.new(this.sampleAttrsForRole('learner', overrides));
            },

            getEditor(overrides) {
                return User.new(this.sampleAttrsForRole('editor', overrides));
            },

            getAdmin(overrides) {
                return User.new(this.sampleAttrsForRole('admin', overrides));
            },

            getSuperEditor(overrides) {
                return User.new(this.sampleAttrsForRole('super_editor', overrides));
            },

            getInstanceWithRole(role, overrides) {
                const attrs = this.sampleAttrsForRole(role, overrides);
                return this.getInstance(attrs);
            },
        };

        return {};
    },
]);
