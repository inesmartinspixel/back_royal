import 'AngularSpecHelper';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import 'Users/angularModule';
import { DisconnectedError } from 'FrontRoyalStore';

describe('CareerProfileInterceptor', () => {
    let SpecHelper;
    let $rootScope;
    let User;
    let CareerProfileInterceptor;
    let CareerProfile;
    let careerProfile;
    let now;
    let $injector;

    angular.mock.module.sharedInjector();

    beforeAll(() => {
        angular.mock.module('SpecHelper', 'FrontRoyal.Users');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                $rootScope = $injector.get('$rootScope');
                CareerProfile = $injector.get('CareerProfile');
                CareerProfileInterceptor = $injector.get('CareerProfileInterceptor');
                User = $injector.get('User');
                $injector.get('CareerProfileFixtures');
            },
        ]);
    });

    afterAll(() => {
        SpecHelper.cleanup();
    });

    describe('response', () => {
        it('should copy vanilla properties to career_profile', () => {
            $rootScope.currentUser = User.new({
                career_profile: {
                    a: 1,
                    b: 2,
                },
            });

            interceptResponse({
                b: 42,
            });
            expect($rootScope.currentUser.career_profile.a).toEqual(1);
            expect($rootScope.currentUser.career_profile.b).toEqual(42);
        });

        it('should not mess things up when there are other push messages', () => {
            $rootScope.currentUser = User.new({
                career_profile: {
                    a: 1,
                },
            });
            $rootScope.currentUser.career_profile.a = 'changed';

            interceptResponse({});

            expect($rootScope.currentUser.career_profile.a).toEqual('changed');
        });

        describe('refresh logic', () => {
            beforeEach(() => {
                now = 1000;
                $rootScope.currentUser = User.new({
                    career_profile: CareerProfile.fixtures.getInstance({
                        updated_at: now,
                        new_profile: false,
                    }),
                });
            });

            it('should not refresh the career_profile if updated_at the same', () => {
                interceptResponse({
                    updated_at: now,
                });

                // should not call CareerProfile.index
                expect($rootScope.currentUser.career_profile.new_profile).toBe(false);
            });

            it('should refresh the career_profile if updated_at is newer', () => {
                careerProfile = CareerProfile.fixtures.getInstance({
                    id: $rootScope.currentUser.career_profile.id,
                    updated_at: now,
                    new_profile: true,
                });
                expectShow().returns(careerProfile);

                interceptResponse({
                    updated_at: now + 1,
                });

                CareerProfile.flush('show');
                expect($rootScope.currentUser.career_profile.new_profile).toBe(true);
            });

            it('should handle disconnected error', () => {
                expectShow().fails(new DisconnectedError());

                interceptResponse({
                    prop: 'val',
                    updated_at: now + 1,
                });

                // initially the updated_at gets bumped, but later on
                // after the call fails it gets reverted back
                expect($rootScope.currentUser.career_profile.updated_at).toBe(now + 1);

                CareerProfile.flush('show');
                expect($rootScope.currentUser.career_profile.prop).toBe('val');
                expect($rootScope.currentUser.career_profile.updated_at).toBe(now);
            });

            function expectShow() {
                return CareerProfile.expect('show').toBeCalledWith(
                    $rootScope.currentUser.career_profile.id,
                    {
                        view: 'editable',
                    },
                    {
                        'FrontRoyal.ApiErrorHandler': { background: true },
                    },
                );
            }
        });

        function interceptResponse(careerProfileAttrs) {
            CareerProfileInterceptor.response({
                config: {
                    url: 'https://quantic.edu/api/ping/simple.json',
                },
                data: {
                    meta: {
                        push_messages: {
                            career_profile: careerProfileAttrs,
                        },
                    },
                },
            });
        }
    });
});
