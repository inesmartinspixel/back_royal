import 'AngularSpecHelper';
import 'Careers/angularModule/spec/_mock/fixtures/hiring_team_fixtures';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohort_applications';
import 'Users/angularModule';

describe('CurrentUserInterceptor', () => {
    let SpecHelper;
    let $rootScope;
    let User;
    let CurrentUserInterceptor;
    let CohortApplication;
    let $injector;
    let $window;
    let SignOutHelper;
    let ClientStorage;

    angular.mock.module.sharedInjector();

    beforeAll(() => {
        angular.mock.module('SpecHelper', 'FrontRoyal.Users');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                $rootScope = $injector.get('$rootScope');
                CurrentUserInterceptor = $injector.get('CurrentUserInterceptor');
                CohortApplication = $injector.get('CohortApplication');
                User = $injector.get('User');
                $window = $injector.get('$window');
                SignOutHelper = $injector.get('SignOutHelper');
                ClientStorage = $injector.get('ClientStorage');

                $injector.get('CohortApplicationFixtures');
            },
        ]);
    });

    afterAll(() => {
        SpecHelper.cleanup();
    });

    describe('updateUser', () => {
        it('should copy vanilla properties to user', () => {
            $rootScope.currentUser = User.new({
                a: 1,
                b: 2,
            });

            CurrentUserInterceptor.updateUser({
                data: {
                    meta: {
                        push_messages: {
                            current_user: {
                                b: 42,
                            },
                        },
                    },
                },
            });
            expect($rootScope.currentUser.a).toEqual(1);
            expect($rootScope.currentUser.b).toEqual(42);
        });

        it('should instantiate embedded iguana objects', () => {
            $rootScope.currentUser = User.new({
                cohort_applications: [
                    CohortApplication.new({
                        a: 1,
                    }),
                ],
            });
            const originalCohortApplication = $rootScope.currentUser.cohort_applications[0];
            expect(originalCohortApplication.isA(CohortApplication)).toBe(true);

            CurrentUserInterceptor.updateUser({
                data: {
                    meta: {
                        push_messages: {
                            current_user: {
                                cohort_applications: [
                                    {
                                        a: 42,
                                    },
                                ],
                            },
                        },
                    },
                },
            });
            expect($rootScope.currentUser.cohort_applications[0].isA(CohortApplication)).toBe(true);

            // Iguana will replace all the embedded objects.  In fact, this is unfortunate,
            // but that is what we expect.
            expect($rootScope.currentUser.cohort_applications[0]).not.toBe(originalCohortApplication);
            expect($rootScope.currentUser.cohort_applications[0].a).toEqual(42);
        });

        it('should not replace the hiring team', () => {
            const HiringTeam = $injector.get('HiringTeam');
            $injector.get('HiringTeamFixtures');

            $rootScope.currentUser = User.new({
                hiring_team: HiringTeam.fixtures.getInstance(),
            });
            const originalHiringTeam = $rootScope.currentUser.hiring_team;

            // If the hiring team pushed down has the same id as the existing
            // one, it's attributes should be merged into the hiring team
            CurrentUserInterceptor.updateUser({
                data: {
                    meta: {
                        push_messages: {
                            current_user: {
                                hiring_team: {
                                    ...$rootScope.currentUser.hiring_team.asJson(),
                                    new_prop: 'set',
                                },
                            },
                        },
                    },
                },
            });
            expect($rootScope.currentUser.hiring_team).toBe(originalHiringTeam);
            expect($rootScope.currentUser.hiring_team.new_prop).toEqual('set');

            // If the hiring team pushed down has a different id then this is a new
            // hiring team
            CurrentUserInterceptor.updateUser({
                data: {
                    meta: {
                        push_messages: {
                            current_user: {
                                hiring_team: HiringTeam.fixtures.getInstance().asJson(),
                            },
                        },
                    },
                },
            });
            expect($rootScope.currentUser.hiring_team).not.toBe(originalHiringTeam);
        });

        it('should not mess things up when there are other push messages', () => {
            $rootScope.currentUser = User.new({
                a: 1,
            });
            $rootScope.currentUser.a = 'changed';

            CurrentUserInterceptor.updateUser({
                data: {
                    meta: {
                        push_messages: {
                            something_else: {},
                        },
                    },
                },
            });

            // we had an issue where copyAttrs called with undefined would
            // blow this away
            expect($rootScope.currentUser.a).toEqual('changed');
        });

        it('should clear caches if program_type changes', () => {
            $rootScope.currentUser = User.new({
                fallback_program_type: 1,
            });

            const Playlist = $injector.get('Playlist');

            jest.spyOn(Playlist, 'resetCache').mockImplementation(() => {});

            CurrentUserInterceptor.updateUser({
                data: {
                    meta: {
                        push_messages: {
                            current_user: {
                                fallback_program_type: 'changed',
                            },
                        },
                    },
                },
            });

            expect($rootScope.currentUser.fallback_program_type).toEqual('changed');

            expect(Playlist.resetCache).toHaveBeenCalled();
        });

        it('should set reapplyingOrEditingApplication on currentUser to false if acceptedOrPreAcceptedCohortApplication', () => {
            $rootScope.currentUser = User.new({
                fallback_program_type: 1,
            });
            jest.spyOn($rootScope.currentUser, 'acceptedOrPreAcceptedCohortApplication', 'get').mockReturnValue(true);
            const reapplyingOrEditingApplicationSetterSpy = jest.spyOn(
                $rootScope.currentUser,
                'reapplyingOrEditingApplication',
                'set',
            );

            CurrentUserInterceptor.updateUser({
                data: {
                    meta: {
                        push_messages: {
                            current_user: {},
                        },
                    },
                },
            });

            expect(reapplyingOrEditingApplicationSetterSpy).toHaveBeenCalledWith(false);
        });

        it('should set reapplyingOrEditingApplication on currentUser to false if lastCohortApplication is rejected and cannot yet reapply', () => {
            $rootScope.currentUser = User.new({
                fallback_program_type: 1,
            });
            const application = CohortApplication.fixtures.getInstance({
                status: 'rejected',
            });
            jest.spyOn(application, 'canReapplyTo').mockImplementation(() => false);
            jest.spyOn($rootScope.currentUser, 'lastCohortApplication', 'get').mockReturnValue(application);
            const reapplyingOrEditingApplicationSpy = jest.spyOn(
                $rootScope.currentUser,
                'reapplyingOrEditingApplication',
                'set',
            );

            CurrentUserInterceptor.updateUser({
                data: {
                    meta: {
                        push_messages: {
                            current_user: {},
                        },
                    },
                },
            });
            expect(reapplyingOrEditingApplicationSpy).toHaveBeenCalledWith(false);
        });

        it('should sign out a deactivated user', () => {
            $rootScope.currentUser = User.new({
                deactivated: false,
            });

            jest.spyOn(SignOutHelper, 'signOut').mockImplementation(() => {});

            CurrentUserInterceptor.updateUser({
                data: {
                    meta: {
                        push_messages: {
                            current_user: {
                                deactivated: true,
                            },
                        },
                    },
                },
            });

            expect(SignOutHelper.signOut).toHaveBeenCalled();
        });

        it('should call setStreamBookmarks only if favorit', () => {
            const frontRoyalStore = $injector.get('frontRoyalStore');
            jest.spyOn(frontRoyalStore, 'setStreamBookmarks');
            $rootScope.currentUser = User.new({});

            const response = {
                data: {
                    meta: {
                        push_messages: {
                            current_user: {},
                        },
                    },
                },
            };

            CurrentUserInterceptor.updateUser(response);
            expect(frontRoyalStore.setStreamBookmarks).not.toHaveBeenCalled();

            response.data.meta.push_messages.current_user.favorite_lesson_stream_locale_packs = [];
            CurrentUserInterceptor.updateUser(response);
            expect(frontRoyalStore.setStreamBookmarks).toHaveBeenCalledWith($rootScope.currentUser);
        });
    });

    describe('response', () => {
        it('should call updateUser', () => {
            jest.spyOn(CurrentUserInterceptor, 'updateUser').mockImplementation(() => {});
            expect(CurrentUserInterceptor.response('someResponse')).toEqual('someResponse');
            expect(CurrentUserInterceptor.updateUser).toHaveBeenCalledWith('someResponse');
        });
    });

    describe('responseError', () => {
        it('should call updateUser', () => {
            jest.spyOn(CurrentUserInterceptor, 'updateUser').mockImplementation(() => {});
            expect(() => {
                CurrentUserInterceptor.responseError('someResponse');
            }).toThrow('someResponse');
            expect(CurrentUserInterceptor.updateUser).toHaveBeenCalledWith('someResponse');
        });
    });

    describe('request', () => {
        describe('ghost_mode', () => {
            it('should be true if $rootScope.currentUser.ghostMode', () => {
                $rootScope.currentUser = User.new({
                    a: 1,
                    b: 2,
                    ghostMode: true,
                });

                const config = {
                    url: `${$window.ENDPOINT_ROOT}/api/asdf`,
                };

                CurrentUserInterceptor.request(config);
                expect(config.params.ghost_mode).toBe(true);
            });

            it('should be true if logged_in_as', () => {
                $rootScope.currentUser = User.new();
                jest.spyOn(ClientStorage, 'getItem').mockReturnValue('true');

                const config = {
                    url: `${$window.ENDPOINT_ROOT}/api/asdf`,
                };

                CurrentUserInterceptor.request(config);
                expect(config.params.ghost_mode).toBe(true);
                expect(ClientStorage.getItem).toHaveBeenCalledWith('logged_in_as');
            });
        });
    });
});
