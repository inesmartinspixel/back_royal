import 'AngularSpecHelper';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import 'Careers/angularModule/spec/_mock/fixtures/hiring_application_fixtures';
import 'Careers/angularModule/spec/_mock/fixtures/hiring_team_fixtures';
import 'Careers/angularModule/spec/_mock/fixtures/open_position_fixtures';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohort_applications';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';
import 'Users/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/lessons';
import 'Users/angularModule/spec/_mock/fixtures/users';
import moment from 'moment-timezone';

/* eslint-disable no-shadow */
describe('User', () => {
    let User;
    let user;
    let Lesson;
    let $injector;
    let $window;
    let EditCareerProfileHelper;
    let Cohort;
    let CohortApplication;
    let CareerProfile;
    let ClientStorage;
    let cohortApplication;
    let hasSignedEnrollmentAgreement;
    let S3TranscriptAsset;
    let EducationExperience;
    let Institution;
    let preSignupValues;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Users', 'FrontRoyal.Lessons', 'FrontRoyal.Institutions', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;

                User = $injector.get('User');
                Lesson = $injector.get('Lesson');
                EditCareerProfileHelper = $injector.get('EditCareerProfileHelper');
                Cohort = $injector.get('Cohort');
                CohortApplication = $injector.get('CohortApplication');
                CareerProfile = $injector.get('CareerProfile');
                ClientStorage = $injector.get('ClientStorage');
                $window = $injector.get('$window');
                S3TranscriptAsset = $injector.get('S3TranscriptAsset');
                EducationExperience = $injector.get('EducationExperience');
                Institution = $injector.get('Institution');
                preSignupValues = $injector.get('preSignupValues');

                $injector.get('UserFixtures');
                $injector.get('LessonFixtures');
                $injector.get('CohortFixtures');
                $injector.get('CohortApplicationFixtures');
                $injector.get('CareerProfileFixtures');
                $injector.get('MockIguana');

                Lesson.setAdapter('Iguana.Mock.Adapter');
                User.setAdapter('Iguana.Mock.Adapter');

                user = User.fixtures.getInstanceWithRole('learner');
            },
        ]);
    });

    describe('hasExternalInstitution', () => {
        it('should be true if external institution associated with learner', () => {
            expect(User.fixtures.getLearner({}).hasExternalInstitution).toBe(false);

            expect(
                User.fixtures.getLearner({
                    institutions: [Institution.new({ external: false })],
                }).hasExternalInstitution,
            ).toBe(false);

            expect(
                User.fixtures.getLearner({
                    institutions: [Institution.new({ external: true })],
                }).hasExternalInstitution,
            ).toBe(true);
        });
    });

    describe('hasLearnerAccess', () => {
        it('should be true', () => {
            expect(User.fixtures.getInstanceWithRole('learner').hasLearnerAccess).toBe(true);
            expect(User.fixtures.getInstanceWithRole('editor').hasLearnerAccess).toBe(true);
            expect(User.fixtures.getInstanceWithRole('super_editor').hasLearnerAccess).toBe(true);
            expect(User.fixtures.getInstanceWithRole('admin').hasLearnerAccess).toBe(true);
        });
    });

    describe('hasAdminAccess', () => {
        it('should be true only if has admin role', () => {
            expect(User.fixtures.getInstanceWithRole('learner').hasAdminAccess).toBe(false);
            expect(User.fixtures.getInstanceWithRole('editor').hasAdminAccess).toBe(false);
            expect(User.fixtures.getInstanceWithRole('super_editor').hasAdminAccess).toBe(false);
            expect(User.fixtures.getInstanceWithRole('admin').hasAdminAccess).toBe(true);
        });
    });

    describe('hasEditorAccess', () => {
        it('should be true only if has admin or editor or super_editor role', () => {
            expect(User.fixtures.getInstanceWithRole('learner').hasEditorAccess).toBe(false);
            expect(User.fixtures.getInstanceWithRole('editor').hasEditorAccess).toBe(true);
            expect(User.fixtures.getInstanceWithRole('super_editor').hasEditorAccess).toBe(true);
            expect(User.fixtures.getInstanceWithRole('admin').hasEditorAccess).toBe(true);
        });
    });

    describe('hasCareersNetworkAccess', () => {
        it('should be true if user has an accepted cohort application and can_edit_career_profile', () => {
            user = User.fixtures.getInstance({
                cohort_applications: [
                    {
                        status: 'pending',
                    },
                ],
            });
            user.can_edit_career_profile = false;
            expect(user.hasCareersNetworkAccess).toBe(false);
            user.cohort_applications[0].status = 'accepted';
            expect(user.hasCareersNetworkAccess).toBe(false);
            user.can_edit_career_profile = true;
            expect(user.hasCareersNetworkAccess).toBe(true);
        });
    });

    describe('hasEditCareerProfileAccess', () => {
        describe('when Cohort.supportsEditingCareerProfile', () => {
            beforeEach(() => {
                jest.spyOn(Cohort, 'supportsEditingCareerProfile').mockReturnValue(true);
            });

            it('should be true if can_edit_career_profile', () => {
                user.can_edit_career_profile = true;
                expect(user.hasEditCareerProfileAccess).toBe(true);
            });

            it('should be true if isAccepted', () => {
                user.can_edit_career_profile = false;
                jest.spyOn(user, 'isAccepted', 'get').mockReturnValue(true);
                expect(user.hasEditCareerProfileAccess).toBe(true);
            });

            it('should be true if isDeferred', () => {
                user.can_edit_career_profile = false;
                jest.spyOn(user, 'isAccepted', 'get').mockReturnValue(false);
                jest.spyOn(user, 'isDeferred', 'get').mockReturnValue(true);
                expect(user.hasEditCareerProfileAccess).toBe(true);
            });

            it('should be true if isPreAccepted', () => {
                user.can_edit_career_profile = false;
                jest.spyOn(user, 'isAccepted', 'get').mockReturnValue(false);
                jest.spyOn(user, 'isDeferred', 'get').mockReturnValue(false);
                jest.spyOn(user, 'isPreAccepted', 'get').mockReturnValue(true);
                expect(user.hasEditCareerProfileAccess).toBe(true);
            });
        });

        it('should be false when not Cohort.supportsEditingCareerProfile', () => {
            jest.spyOn(Cohort, 'supportsEditingCareerProfile').mockReturnValue(false);
            jest.spyOn(user, 'isAccepted', 'get').mockReturnValue(true); // sanity
            expect(user.hasEditCareerProfileAccess).toBe(false);
        });
    });

    describe('hasCareersTabAccess', () => {
        describe('when Cohort.supportsCareersTabAccess', () => {
            beforeEach(() => {
                jest.spyOn(Cohort, 'supportsCareersTabAccess').mockReturnValue(true);
            });

            it('should be true if canApplyToSmartly', () => {
                jest.spyOn(user, 'canApplyToSmartly', 'get').mockReturnValue(true);
                expect(user.hasCareersTabAccess).toBe(true);
            });

            it('should be true if can_edit_career_profile', () => {
                jest.spyOn(user, 'canApplyToSmartly', 'get').mockReturnValue(false);
                user.can_edit_career_profile = true;
                expect(user.hasCareersTabAccess).toBe(true);
            });
        });

        it('should be false when not Cohort.supportsCareersTabAccess', () => {
            jest.spyOn(Cohort, 'supportsCareersTabAccess').mockReturnValue(false);
            user.can_edit_career_profile = true; // sanity
            expect(user.hasCareersTabAccess).toBe(false);
        });
    });

    describe('hasNetworkTabAccess', () => {
        describe('when Cohort.supportsStudentNetworkTabAccess', () => {
            beforeEach(() => {
                jest.spyOn(Cohort, 'supportsStudentNetworkTabAccess').mockReturnValue(true);
            });

            it('should be false if user has an external institution', () => {
                jest.spyOn(user, 'hasExternalInstitution', 'get').mockReturnValue(true);
                jest.spyOn(user, 'programType', 'get').mockReturnValue('external');
                expect(user.hasNetworkTabAccess).toBe(false);
            });

            it('should be false if user has a hiring_application', () => {
                user.hiring_application = {
                    foo: 'bar',
                };
                expect(user.hasNetworkTabAccess).toBe(false);
            });

            it('should be true otherwise', () => {
                jest.spyOn(user, 'programType', 'get').mockReturnValue('mba');
                delete user.hiring_application;
                expect(user.hasNetworkTabAccess).toBe(true);
            });
        });

        it('should be true when demo and not Cohort.supportsStudentNetworkTabAccess', () => {
            jest.spyOn(user, 'programType', 'get').mockReturnValue('demo');
            jest.spyOn(Cohort, 'supportsStudentNetworkTabAccess').mockReturnValue(false);
            expect(user.hasNetworkTabAccess).toBe(true);
        });

        it('should be true when not demo and Cohort.supportsStudentNetworkTabAccess', () => {
            jest.spyOn(user, 'programType', 'get').mockReturnValue('foobar');
            jest.spyOn(Cohort, 'supportsStudentNetworkTabAccess').mockReturnValue(true);
            expect(user.hasNetworkTabAccess).toBe(true);
        });

        it('should be false when not demo and not Cohort.supportsStudentNetworkTabAccess', () => {
            jest.spyOn(user, 'programType', 'get').mockReturnValue('foobar');
            jest.spyOn(Cohort, 'supportsStudentNetworkTabAccess').mockReturnValue(false);
            expect(user.hasNetworkTabAccess).toBe(false);
        });
    });

    describe('hasStudentNetworkAccess', () => {
        beforeEach(() => {
            user = User.fixtures.getInstance({
                cohort_applications: [
                    {
                        status: 'pending',
                    },
                ],
            });
            expect(user.cohort_applications.length).toBe(1); // sanity check
        });

        it('should be true if accepted to a cohort that supports it', () => {
            user.cohort_applications[0].status = 'accepted';
            jest.spyOn(Cohort, 'supportsNetworkAccess').mockReturnValue(true);
            expect(user.hasStudentNetworkAccess).toBe(true);
        });

        it('should be false if pre_accepted to a cohort that supports it', () => {
            user.cohort_applications[0].status = 'pre_accepted';
            jest.spyOn(Cohort, 'supportsNetworkAccess').mockReturnValue(true);
            expect(user.hasStudentNetworkAccess).toBe(false);
        });

        it('should be true if deferred to a cohort that supports it', () => {
            user.cohort_applications[0].status = 'deferred';
            jest.spyOn(Cohort, 'supportsNetworkAccess').mockReturnValue(true);
            expect(user.hasStudentNetworkAccess).toBe(true);
        });

        it('should be true if pre_accepted after having deferred', () => {
            user.cohort_applications[0].status = 'pre_accepted';
            user.cohort_applications.push({
                status: 'deferred',
            });
            jest.spyOn(Cohort, 'supportsNetworkAccess').mockReturnValue(true);
            expect(user.hasStudentNetworkAccess).toBe(true);
        });

        it('should be false if pre_accepted after having deferred then expelled', () => {
            user.cohort_applications[0].status = 'pre_accepted';
            user.cohort_applications.push({
                status: 'expelled',
            });
            user.cohort_applications.push({
                status: 'deferred',
            });
            jest.spyOn(Cohort, 'supportsNetworkAccess').mockReturnValue(true);
            expect(user.hasStudentNetworkAccess).toBe(false);
        });

        it('should be false if pending even if cohort supports it', () => {
            user.cohort_applications[0].status = 'pending';
            jest.spyOn(Cohort, 'supportsNetworkAccess').mockReturnValue(false);
            expect(user.hasStudentNetworkAccess).toBe(false);
        });

        it('should be false if cohort does not support it even if accepted', () => {
            user.cohort_applications[0].status = 'accepted';
            jest.spyOn(Cohort, 'supportsNetworkAccess').mockReturnValue(false);
            expect(user.hasStudentNetworkAccess).toBe(false);
        });

        it('should be false if no cohort_applications', () => {
            user = User.fixtures.getInstance({
                cohort_applications: null,
            });
            expect(user.hasStudentNetworkAccess).toBe(false);
        });

        it('should be false if failed', () => {
            user.cohort_applications[0].status = 'accepted';
            user.cohort_applications[0].graduation_status = 'failed';
            jest.spyOn(Cohort, 'supportsNetworkAccess').mockReturnValue(true);
            expect(user.hasStudentNetworkAccess).toBe(false);
        });
    });

    describe('hasHiringManagerAccess', () => {
        it('should be true if user has a non-rejected hiring manager application', () => {
            user = User.fixtures.getInstance({
                hiring_application: {
                    status: 'accepted',
                },
            });
            expect(user.hasHiringManagerAccess).toBe(true);
            user.hiring_application.status = 'pending';
            expect(user.hasHiringManagerAccess).toBe(true);
            user.hiring_application.status = 'rejected';
            expect(user.hasHiringManagerAccess).toBe(false);
        });

        it('should be false if user is past due', () => {
            user = User.fixtures.getInstance({
                hiring_application: {
                    status: 'accepted',
                },
                hiring_team: {
                    subscriptions: [],
                },
            });
            jest.spyOn(user, 'hasDisabledHiringManagerAccess', 'get').mockReturnValue(false);
            expect(user.hasHiringManagerAccess).toBe(true);
            jest.spyOn(user, 'noHiringManagerAccessDueToPastDuePayment', 'get').mockReturnValue(true);
            expect(user.hasHiringManagerAccess).toBe(false);
        });

        it('should be fasle if user.hasDisabledHiringManagerAccess', () => {
            user = User.fixtures.getInstance({
                hiring_application: {
                    status: 'accepted',
                },
            });
            jest.spyOn(user, 'hasDisabledHiringManagerAccess', 'get').mockReturnValue(true);
            expect(user.hasHiringManagerAccess).toBe(false);
        });
    });

    describe('hasAcceptedHiringManagerAccessPastDue', () => {
        it('should be true if user has a non-rejected hiring manager application and past due', () => {
            user = User.fixtures.getInstance({
                hiring_application: {
                    status: 'accepted',
                },
            });

            jest.spyOn(user, 'noHiringManagerAccessDueToPastDuePayment', 'get').mockReturnValue(true);

            expect(user.hasAcceptedHiringManagerAccessPastDue).toBe(true);
            user.hiring_application.status = 'pending';
            expect(user.hasAcceptedHiringManagerAccessPastDue).toBe(false);
            user.hiring_application.status = 'rejected';
            expect(user.hasAcceptedHiringManagerAccessPastDue).toBe(false);
        });

        it('should be false if user is not past due', () => {
            user = User.fixtures.getInstance({
                hiring_application: {
                    status: 'accepted',
                },
                hiring_team: {},
            });

            let pastDue = true;
            jest.spyOn(user, 'noHiringManagerAccessDueToPastDuePayment', 'get').mockImplementation(() => pastDue);
            expect(user.hasAcceptedHiringManagerAccessPastDue).toBe(true);
            pastDue = false;
            expect(user.hasAcceptedHiringManagerAccessPastDue).toBe(false);
        });
    });

    describe('defaultsToHiringExperience', () => {
        it('should be true if user has a hiring manager application', () => {
            user = User.fixtures.getInstance({
                hiring_application: 'foo',
            });
            expect(user.defaultsToHiringExperience).toBe(true);
        });
    });

    describe('hiringApplicationStatus', () => {
        it('should set hiring_application to null if set to null', () => {
            user = User.fixtures.getInstance({
                hiring_application: {
                    foo: 'bar',
                },
            });
            user.hiringApplicationStatus = null;
            expect(user.hiring_application).toBe(null);
        });

        it('should set hiring_application to null if set to __NONE__', () => {
            user = User.fixtures.getInstance({
                hiring_application: {
                    foo: 'bar',
                },
            });
            user.hiringApplicationStatus = '__NONE__';
            expect(user.hiring_application).toBe(null);
        });

        it('should create new HiringApplication if not __NONE__ or null and no hiring_application', () => {
            user = User.fixtures.getInstance();
            user.hiringApplicationStatus = 'foo';
            expect(user.hiring_application).toBeDefined();
        });
    });

    describe('special user types', () => {
        it('blueOcean should be true if the user has the blue ocean group', () => {
            user.groups = [
                {
                    name: 'BLUEOCEAN',
                },
            ];
            expect(user.blueOcean).toEqual(true);

            user.groups = [
                {
                    name: 'abc',
                },
            ];
            expect(user.blueOcean).toEqual(false);
        });

        it('beta should be true if the user has either the BETA or BETA2 group', () => {
            user.groups = [
                {
                    name: 'BETA',
                },
            ];
            expect(user.beta).toEqual(true);

            user.groups = [
                {
                    name: 'BETA2',
                },
            ];
            expect(user.beta).toEqual(true);

            user.groups = [
                {
                    name: 'BLUEOCEAN',
                },
            ];
            expect(user.beta).toEqual(false);
        });
    });

    describe('userGroupNames', () => {
        it('should have a test or two', () => {
            user.groups = [
                {
                    name: 'abc',
                },
                {
                    name: 'def',
                },
            ];
            expect(user.userGroupNames()).toEqual(['ABC', 'DEF']);
        });
    });

    describe('addGroup', () => {
        it('should do nothing if the group already exists', () => {
            const origGroups = user.groups.slice(0);
            expect(user.userGroupNames().indexOf('ABC')).not.toBe(-1); // sanity check
            user.addGroup('ABC');
            expect(user.groups).toEqual(origGroups);
        });
        it('should treat two names that are different only in case as the same', () => {
            const origGroups = user.groups.slice(0);
            expect(user.userGroupNames().indexOf('ABC')).not.toBe(-1); // sanity check
            user.addGroup('abc');
            expect(user.groups).toEqual(origGroups);
        });
        it('should add a new group', () => {
            user.groups = [];
            user.addGroup('abc');
            expect(user.groups).toEqual([
                {
                    name: 'abc',
                },
            ]);
        });
    });

    describe('removeGroup', () => {
        it('should do nothing if the group does not exist', () => {
            const origGroups = user.groups.slice(0);
            user.removeGroup({});
            expect(user.groups).toEqual(origGroups);
        });
        it('should remove an existing group', () => {
            expect(user.groups.length).toBe(1);
            user.removeGroup(user.groups[0]);
            expect(user.groups.length).toBe(0);
        });
    });

    describe('streamIsAssociatedWithAccessGroup', () => {
        it("should return true if stream locale pack id is in one of the user's access groups", () => {
            user.groups[0].stream_locale_pack_ids = ['foo', 'bar', 'baz'];
            expect(user.streamIsAssociatedWithAccessGroup('foo')).toBe(true);
        });
        it("should return false if stream locale pack id is not in one of the user's access groups", () => {
            user.groups[0].stream_locale_pack_ids = ['foo', 'bar', 'baz'];
            expect(user.streamIsAssociatedWithAccessGroup('qux')).toBe(false);
        });
    });

    describe('roleName', () => {
        it('should return the role name', () => {
            expect(User.fixtures.getInstanceWithRole('learner').roleName()).toBe('learner');
        });
    });

    describe('canEdit', () => {
        it('should be true for editors and admins', () => {
            expect(User.fixtures.getInstanceWithRole('learner').canEdit()).toBe(false);
            expect(User.fixtures.getInstanceWithRole('super_editor').canEdit()).toBe(true);
            expect(User.fixtures.getInstanceWithRole('admin').canEdit()).toBe(true);
        });
    });

    describe('canPublish', () => {
        it('should only be true for admins', () => {
            expect(User.fixtures.getInstanceWithRole('learner').canPublish()).toBe(false);
            expect(User.fixtures.getInstanceWithRole('editor').canPublish()).toBe(false);
            expect(User.fixtures.getInstanceWithRole('admin').canPublish()).toBe(true);
        });
    });

    describe('canEditLesson', () => {
        let user;
        let lesson;

        beforeEach(() => {
            user = User.fixtures.getInstance();
            lesson = Lesson.fixtures.getInstance();
        });

        it('should be false if lesson is not editable', () => {
            Object.defineProperty(lesson, 'editable', {
                value: false,
            });
            jest.spyOn(user, 'canEdit').mockImplementation(() => {});
            expect(user.canEditLesson(lesson)).toBe(false);
            expect(user.canEdit).not.toHaveBeenCalled();
        });

        it('should be true if canEdit is true', () => {
            Object.defineProperty(lesson, 'editable', {
                value: true,
            });
            jest.spyOn(user, 'canEdit').mockReturnValue(true);
            expect(user.canEditLesson(lesson)).toBe(true);
        });

        it('should be true if user is author', () => {
            Object.defineProperty(lesson, 'editable', {
                value: true,
            });
            jest.spyOn(user, 'isAuthor').mockReturnValue(true);
            expect(user.canEditLesson(lesson)).toBe(true);
            expect(user.isAuthor).toHaveBeenCalledWith(lesson);
        });

        it('should be true if user has lesson permissions', () => {
            Object.defineProperty(lesson, 'editable', {
                value: true,
            });
            const permissions = {};
            permissions[lesson.id] = 'lesson_editor';
            Object.defineProperty(user, 'lessonPermissions', {
                value: permissions,
            });
            jest.spyOn(user, 'roleName').mockReturnValue('editor');
            expect(user.canEditLesson(lesson)).toBe(true);
        });

        it('should be false otherwise', () => {
            Object.defineProperty(lesson, 'editable', {
                value: true,
            });
            jest.spyOn(user, 'roleName').mockReturnValue('editor');
            expect(user.canEditLesson(lesson)).toBe(false);
        });
    });

    describe('canArchiveLesson', () => {
        it('should be true if user can edit the lesson', () => {
            const lesson = Lesson.fixtures.getInstance();
            const user = User.fixtures.getInstanceWithRole('editor');
            user.roles.push({
                name: 'lesson_editor',
                resource_id: lesson.id,
            });
            expect(user.canArchiveLesson(lesson)).toBe(true);
        });

        it('should be false if user cannot edit the lesson', () => {
            const lesson = Lesson.fixtures.getInstance();
            const user = User.fixtures.getInstanceWithRole('editor');
            expect(user.canArchiveLesson(lesson)).toBe(false);
        });

        it('should be false if publishing status will change and user does not have rights', () => {
            const lesson = Lesson.fixtures.getInstance({
                published_at: new Date().getTime() / 1000,
            });
            const user = User.fixtures.getInstanceWithRole('editor');
            user.roles.push({
                name: 'lesson_editor',
                resource_id: lesson.id,
            });
            expect(user.canArchiveLesson(lesson)).toBe(false);
        });
    });

    describe('canPreviewLesson', () => {
        it('should be true when granted if user is not author', () => {
            const lesson = Lesson.fixtures.getInstance();
            const user = User.fixtures.getInstanceWithRole('editor');
            expect(user.canPreviewLesson(lesson)).toBe(false);

            user.roles.push({
                name: 'previewer',
                resource_id: lesson.id,
            });
            user.$$lessonPermissions = undefined;
            expect(user.canPreviewLesson(lesson)).toBe(true);
        });

        it('should always be true when user is author', () => {
            const lesson = Lesson.fixtures.getInstance();
            const user = User.fixtures.getInstanceWithRole('editor');
            lesson.author = {
                id: user.id,
            };
            expect(user.canPreviewLesson(lesson)).toBe(true);
        });
    });

    describe('canAddCommentsToLesson', () => {
        let user;
        let lesson;

        beforeEach(() => {
            user = User.fixtures.getInstance();
            lesson = Lesson.fixtures.getInstance();
        });

        it('should be false if lesson is not editable', () => {
            jest.spyOn(user, 'canEditLesson').mockImplementation(() => {});
            jest.spyOn(user, 'canReviewLesson').mockImplementation(() => {});
            Object.defineProperty(lesson, 'editable', {
                value: false,
            });
            expect(user.canAddCommentsToLesson(lesson)).toBe(false);
            expect(user.canEditLesson).not.toHaveBeenCalled();
            expect(user.canReviewLesson).not.toHaveBeenCalled();
        });

        it('should be false if user cannot edit or review', () => {
            jest.spyOn(user, 'canEditLesson').mockReturnValue(false);
            jest.spyOn(user, 'canReviewLesson').mockReturnValue(false);
            Object.defineProperty(lesson, 'editable', {
                value: true,
            });
            expect(user.canAddCommentsToLesson(lesson)).toBe(false);
        });

        it('should be true if user can edit', () => {
            jest.spyOn(user, 'canEditLesson').mockReturnValue(true);
            jest.spyOn(user, 'canReviewLesson').mockReturnValue(false);
            Object.defineProperty(lesson, 'editable', {
                value: true,
            });
            expect(user.canAddCommentsToLesson(lesson)).toBe(true);
        });

        it('should be true if user can review', () => {
            jest.spyOn(user, 'canEditLesson').mockReturnValue(false);
            jest.spyOn(user, 'canReviewLesson').mockReturnValue(true);
            Object.defineProperty(lesson, 'editable', {
                value: true,
            });
            expect(user.canAddCommentsToLesson(lesson)).toBe(true);
        });
    });

    describe('canReviewLesson', () => {
        it('should be true only when granted if user is not author', () => {
            const lesson = Lesson.fixtures.getInstance();
            const user = User.fixtures.getInstanceWithRole('editor');
            expect(user.canReviewLesson(lesson)).toBe(false);

            user.roles.push({
                name: 'reviewer',
                resource_id: lesson.id,
            });
            user.$$lessonPermissions = undefined;
            expect(user.canReviewLesson(lesson)).toBe(true);
        });

        it('should always be true when user is author', () => {
            const lesson = Lesson.fixtures.getInstance();
            const user = User.fixtures.getInstanceWithRole('editor');
            lesson.author = {
                id: user.id,
            };
            expect(user.canReviewLesson(lesson)).toBe(true);
        });
    });

    describe('updateLessonPermissions', () => {
        it('can hide a lesson', () => {});
        it('can grant editor access to lesson', () => {});
        it('can grant reviewer access to lesson', () => {});
        it('can grant previewer access to lesson', () => {});
    });

    describe('toggleBookmark', () => {
        let $timeout;
        let frontRoyalStore;

        beforeEach(() => {
            $timeout = $injector.get('$timeout');
            frontRoyalStore = $injector.get('frontRoyalStore');
            User.expect('update');
            jest.spyOn(frontRoyalStore, 'setStreamBookmarks');
        });

        it('should support adding a stream bookmark', () => {
            const stream = {
                id: 123,
                favorite: false,
                locale_pack: {
                    id: 456,
                },
            };

            expect(user.favorite_lesson_stream_locale_packs).toBeUndefined();
            user.toggleBookmark(stream);
            expect(stream.favorite).toBe(true);
            $timeout.flush();
            expect(user.favorite_lesson_stream_locale_packs).toEqual([
                {
                    id: 456,
                },
            ]);
            expect(frontRoyalStore.setStreamBookmarks).toHaveBeenCalledWith(user);
        });

        it('should support removing a stream bookmark', () => {
            const stream = {
                id: 123,
                favorite: false,
                locale_pack: {
                    id: 456,
                },
            };

            user.favorite_lesson_stream_locale_packs = [
                {
                    id: 456,
                },
            ];

            user.toggleBookmark(stream);
            expect(stream.favorite).toBe(true);
            $timeout.flush();
            expect(frontRoyalStore.setStreamBookmarks).toHaveBeenCalledWith(user);
        });
    });

    describe('clearAllProgress', () => {
        it('should make a call to proper endpoint and handle cached data cleanup', () => {
            const $httpBackend = $injector.get('$httpBackend');
            const $timeout = $injector.get('$timeout');
            const ClientConfigInterceptor = $injector.get('ClientConfigInterceptor');

            jest.spyOn(ClientConfigInterceptor, 'request').mockImplementation(config => config);

            jest.spyOn(user.progress, 'clear').mockImplementation(() => {});

            user.favorite_lesson_stream_locale_packs = [
                {
                    id: 123,
                },
            ];

            const clearProgressPath = `${$window.ENDPOINT_ROOT}/api/destroy_all_progress.json`;
            $httpBackend.when('DELETE', clearProgressPath).respond({});

            user.clearAllProgress();

            $httpBackend.expectDELETE(clearProgressPath);

            $timeout.flush();
            $httpBackend.flush();

            expect(user.favorite_lesson_stream_locale_packs).toEqual([]);
            expect(user.progress.clear).toHaveBeenCalled();
        });
    });

    describe('inGroup', () => {
        it('should work', () => {
            const user = User.new({
                groups: [
                    {
                        name: 'a',
                    },
                ],
            });
            expect(user.inGroup('a')).toBe(true);
            expect(user.inGroup('b')).toBe(false);
        });
    });

    describe('isRejectedForPromotedCohort', () => {
        it('should be true with rejected application for relevant cohort', () => {
            Object.defineProperty(user, 'lastCohortApplication', {
                value: {
                    cohort_id: 'relevant_cohort',
                    status: 'rejected',
                },
            });
            user.relevant_cohort = {
                id: 'relevant_cohort',
            };
            expect(user.isRejectedForPromotedCohort).toBe(true);
        });

        it('should be false with rejected application for non-relevant cohort', () => {
            Object.defineProperty(user, 'lastCohortApplication', {
                value: {
                    cohort_id: 'old_cohort',
                    status: 'rejected',
                },
            });
            user.relevant_cohort = {
                id: 'relevant_cohort',
            };
            expect(user.isRejectedForPromotedCohort).toBe(false);
        });

        it('should be false with no rejected application for non-relevant cohort', () => {
            Object.defineProperty(user, 'lastCohortApplication', {
                value: {
                    cohort_id: 'relevant_cohort',
                    status: 'pending',
                },
            });
            user.relevant_cohort = {
                id: 'relevant_cohort',
            };
            expect(user.isRejectedForPromotedCohort).toBe(false);
        });
    });

    describe('applyToCohort', () => {
        let cohort;

        beforeEach(() => {
            $injector.get('CohortFixtures');
            $injector.get('CohortApplicationFixtures');
            cohort = Cohort.fixtures.getInstance({
                program_type: 'foo',
            });
            user.career_profile = CareerProfile.fixtures.getInstance();
            jest.spyOn(user.career_profile, 'salaryRangeForEventPayload', 'get').mockReturnValue(0);
            jest.spyOn(EditCareerProfileHelper, 'logEventForApplication').mockImplementation(() => {});
        });

        it('should log a submit-application event', () => {
            user.cohort_applications = [];

            // For extra testing on the ev2 logic, mostly for the Iguana data-stitching logic.
            cohort = Cohort.fixtures.getInstance({
                program_type: 'emba',
            });
            jest.spyOn(cohort, 'isDegreeProgram', 'get').mockReturnValue(false);
            jest.spyOn(user.career_profile, 'placeState', 'get').mockReturnValue('VA');
            jest.spyOn(user.career_profile, 'placeCountry', 'get').mockReturnValue('US');
            user.career_profile.salary_range = 'prefer_not_to_disclose';

            applyToCohortAndAssertPayload({
                category: 'Submitted Application',
                label: 'emba',
                cohort_title: cohort.title,
                start_content_marketing_campaign: true,
                s_range: 0, // salaryRangeForEventPayload
                value: 563,
            });
        });

        it('should set start_content_marketing_campaign to false if there is already an application', () => {
            user.cohort_applications = [
                CohortApplication.fixtures.getInstance({
                    status: 'rejected',
                }),
            ];
            applyToCohortAndAssertPayload({
                category: 'Submitted Application',
                label: 'foo',
                cohort_title: cohort.title,
                start_content_marketing_campaign: false,
                s_range: 0, // salaryRangeForEventPayload
                value: 0,
            });
        });

        it('should logConversionGoalEvent', () => {
            jest.spyOn(user, 'logConversionGoalEvent').mockImplementation(() => {});
            CohortApplication.expect('create');
            user.applyToCohort(cohort);
            expect(user.logConversionGoalEvent).toHaveBeenCalled();
        });

        describe('when cohort.isDegreeProgram', () => {
            beforeEach(() => {
                jest.spyOn(cohort, 'isDegreeProgram', 'get').mockReturnValue(true);
            });

            it('should log an event based on program_type', () => {
                CohortApplication.expect('create');

                jest.spyOn(EditCareerProfileHelper, 'logEventForApplication').mockImplementation(() => {});
                cohort.program_type = 'mba';
                user.applyToCohort(cohort);
                expect(EditCareerProfileHelper.logEventForApplication).toBeCalledWith('mba-submit-application', true, {
                    label: 'MBA Application Submitted',
                });

                cohort.program_type = 'emba';
                user.applyToCohort(cohort);
                expect(EditCareerProfileHelper.logEventForApplication).toBeCalledWith('emba-submit-application', true, {
                    label: 'EMBA Application Submitted',
                });
            });
        });

        function applyToCohortAndAssertPayload(payload) {
            CohortApplication.expect('create');
            user.applyToCohort(cohort);
            expect(EditCareerProfileHelper.logEventForApplication).toHaveBeenCalledWith(
                'submit-application',
                true,
                payload,
            );
        }
    });

    describe('logConversionGoalEvent', () => {
        let EventLogger;

        beforeEach(() => {
            EventLogger = $injector.get('EventLogger');
            jest.spyOn(EventLogger.prototype, 'log').mockImplementation(() => {});
            user.career_profile = CareerProfile.fixtures.getInstance({
                salary_range: 'mock_salary_range',
            });
        });

        it('should log cohort:conversion_goal event with payload', () => {
            const conversionEquivalents = {
                mock_program_type: {
                    mock_salary_range: 'mock_conversion_equivalent',
                },
            };
            jest.spyOn(user, 'programType', 'get').mockReturnValue('mock_program_type');
            jest.spyOn($injector, 'get').mockReturnValueOnce(EventLogger).mockReturnValueOnce(conversionEquivalents);

            user.logConversionGoalEvent();

            expect($injector.get.mock.calls[0]).toEqual(['EventLogger']);
            expect($injector.get.mock.calls[1]).toEqual(['CAREERS_SALARY_CONVERSION_EQUIVALENTS']);
            expect(EventLogger.prototype.log).toHaveBeenCalledWith(
                'cohort:conversion_goal',
                {
                    label: 'Purchase',
                    user_id: user.id,
                    program_type: 'mock_program_type',
                    value: 'mock_conversion_equivalent',
                    currency: 'USD',
                },
                {
                    cordovaFacebook: !!$window.CORDOVA,
                },
            );
        });
    });

    describe('deleteCohortApplication', () => {
        it('should delete application and remove it from the list', () => {
            user.cohort_applications = [
                CohortApplication.fixtures.getInstance(),
                CohortApplication.fixtures.getInstance(),
                CohortApplication.fixtures.getInstance(),
            ];
            const expectedApplications = [user.cohort_applications[0], user.cohort_applications[2]];
            const application = user.cohort_applications[1];
            jest.spyOn(application, 'destroy').mockReturnValue('promise');
            expect(user.deleteCohortApplication(application)).toEqual('promise');
            expect(user.cohort_applications).toEqual(expectedApplications);
        });
    });

    describe('testCompleteMessageKey', () => {
        it('should work', () => {
            user.relevant_cohort = {
                id: 'relevant_cohort',
                supportsAutograding: false,
            };
            expect(user.testCompleteMessageKey).toBeNull();
            user.relevant_cohort.supportsAutograding = true;
            expect(user.testCompleteMessageKey).toBe('your_test_score_auto');
        });
    });

    describe('careerProfileIndicatesUserShouldProvideTranscripts', () => {
        it('should be false if no career_profile', () => {
            user.career_profile = null;
            expect(user.careerProfileIndicatesUserShouldProvideTranscripts).toBe(false);
        });

        it('should return true if career_profile.indicatesUserShouldProvideTranscripts', () => {
            user.career_profile = CareerProfile.fixtures.getInstance();
            const indicatesUserShouldProvideTranscriptsSpy = jest
                .spyOn(user.career_profile, 'indicatesUserShouldProvideTranscripts', 'get')
                .mockReturnValue(false);
            expect(user.careerProfileIndicatesUserShouldProvideTranscripts).toBe(false);
            indicatesUserShouldProvideTranscriptsSpy.mockReturnValue(true);
            expect(user.careerProfileIndicatesUserShouldProvideTranscripts).toBe(true);
        });
    });

    describe('careerProfileIndicatesUserShouldUploadEnglishLanguageProficiencyDocuments', () => {
        it('should be false if no career_profile', () => {
            user.career_profile = null;
            expect(user.careerProfileIndicatesUserShouldUploadEnglishLanguageProficiencyDocuments).toBe(false);
        });

        it('should return true if career_profile.indicatesUserShouldUploadEnglishLanguageProficiencyDocuments', () => {
            user.career_profile = CareerProfile.fixtures.getInstance();
            const indicatesUserShouldUploadEnglishLanguageProficiencyDocumentsSpy = jest
                .spyOn(user.career_profile, 'indicatesUserShouldUploadEnglishLanguageProficiencyDocuments', 'get')
                .mockReturnValue(false);
            expect(user.careerProfileIndicatesUserShouldUploadEnglishLanguageProficiencyDocuments).toBe(false);
            indicatesUserShouldUploadEnglishLanguageProficiencyDocumentsSpy.mockReturnValue(true);
            expect(user.careerProfileIndicatesUserShouldUploadEnglishLanguageProficiencyDocuments).toBe(true);
        });
    });

    describe('hasUploadedTranscripts', () => {
        it('should return false if there are not any education experiences with a transcript uploaded', () => {
            const educationExperiences = [EducationExperience.new(), EducationExperience.new()];
            jest.spyOn(educationExperiences[0], 'transcriptUploadedOrWaived', 'get').mockReturnValue(false);
            jest.spyOn(educationExperiences[1], 'transcriptUploadedOrWaived', 'get').mockReturnValue(false);
            user.career_profile = CareerProfile.fixtures.getInstance();
            user.career_profile.education_experiences = educationExperiences;
            user.s3_transcript_assets = [];
            expect(user.hasUploadedTranscripts).toBe(false);
        });

        it('should return true if there are any education experiences with a transcript uploaded', () => {
            user.career_profile = CareerProfile.fixtures.getInstance();
            jest.spyOn(user.career_profile, 'hasUploadedTranscripts', 'get').mockReturnValue(true);
            expect(user.hasUploadedTranscripts).toBe(true);
        });
    });

    describe('requiredTranscriptsUploadedColumnString', () => {
        beforeEach(() => {
            user.career_profile = CareerProfile.fixtures.getInstance();
        });

        it('should return a string when there are required, not-waived transcripts but none are uploaded', () => {
            jest.spyOn(user.career_profile, 'numRequiredTranscriptsUploaded', 'get').mockReturnValue(0);
            jest.spyOn(user.career_profile, 'numRequiredTranscriptsNotWaived', 'get').mockReturnValue(2);
            expect(user.requiredTranscriptsUploadedColumnString).toBe('0/2');
        });

        it('should return a dash when there are no uploaded nor required transcripts', () => {
            jest.spyOn(user.career_profile, 'numRequiredTranscriptsUploaded', 'get').mockReturnValue(0);
            jest.spyOn(user.career_profile, 'numRequiredTranscriptsNotWaived', 'get').mockReturnValue(0);
            expect(user.requiredTranscriptsUploadedColumnString).toBe('—');
        });

        it('should return a string when there are required, not-waived transcripts and some are uploaded', () => {
            jest.spyOn(user.career_profile, 'numRequiredTranscriptsUploaded', 'get').mockReturnValue(2);
            jest.spyOn(user.career_profile, 'numRequiredTranscriptsNotWaived', 'get').mockReturnValue(2);
            expect(user.requiredTranscriptsUploadedColumnString).toBe('2/2');
        });

        it('should return empty string if no career_profile', () => {
            user.career_profile = undefined;
            expect(user.requiredTranscriptsUploadedColumnString).toBe('');
        });
    });

    describe('requiredTranscriptsApprovedColumnString', () => {
        beforeEach(() => {
            user.career_profile = CareerProfile.fixtures.getInstance();
        });

        it('should return a string when there are required, not-waived transcripts but none are uploaded', () => {
            jest.spyOn(user.career_profile, 'numRequiredTranscriptsApproved', 'get').mockReturnValue(0);
            jest.spyOn(user.career_profile, 'numRequiredTranscriptsNotWaived', 'get').mockReturnValue(2);
            expect(user.requiredTranscriptsApprovedColumnString).toBe('0/2');
        });

        it('should return a dash when there are no uploaded nor required transcripts', () => {
            jest.spyOn(user.career_profile, 'numRequiredTranscriptsApproved', 'get').mockReturnValue(0);
            jest.spyOn(user.career_profile, 'numRequiredTranscriptsNotWaived', 'get').mockReturnValue(0);
            expect(user.requiredTranscriptsApprovedColumnString).toBe('—');
        });

        it('should return a string when there are required, not-waived transcripts and some are uploaded', () => {
            jest.spyOn(user.career_profile, 'numRequiredTranscriptsApproved', 'get').mockReturnValue(2);
            jest.spyOn(user.career_profile, 'numRequiredTranscriptsNotWaived', 'get').mockReturnValue(2);
            expect(user.requiredTranscriptsApprovedColumnString).toBe('2/2');
        });

        it('should return empty string if no career_profile', () => {
            user.career_profile = undefined;
            expect(user.requiredTranscriptsApprovedColumnString).toBe('');
        });
    });

    describe('legacyTranscripts', () => {
        it('should return transcripts that are on the user', () => {
            const transcripts = [S3TranscriptAsset.new(), S3TranscriptAsset.new()];
            user.s3_transcript_assets = transcripts;
            expect(user.legacyTranscripts).toEqual(transcripts);
        });
    });

    describe('hasUploadedEnglishLanguageProficiencyDocuments', () => {
        it("should return false if there aren't any s3_english_language_proficiency_documents", () => {
            user.s3_english_language_proficiency_documents = [];
            expect(user.hasUploadedEnglishLanguageProficiencyDocuments).toBe(false);
        });

        it('should return true if there are any s3_english_language_proficiency_documents', () => {
            user.s3_english_language_proficiency_documents = [{}];
            expect(user.hasUploadedEnglishLanguageProficiencyDocuments).toBe(true);
        });
    });

    describe('missingTranscripts', () => {
        it('should be true if not verified, supposed to have transcripts, but some are missing', () => {
            user.career_profile = CareerProfile.fixtures.getInstance();
            user.transcripts_verified = false;
            jest.spyOn(user, 'recordsIndicateUserShouldProvideTranscripts', 'get').mockReturnValue(true);
            jest.spyOn(user.career_profile, 'missingTranscripts', 'get').mockReturnValue(true);
            expect(user.missingTranscripts).toBe(true);
        });
    });

    describe('missingRequiredDocumentsLegacy', () => {
        beforeEach(() => {
            mockHasSignedEnrollmentAgreement();
        });

        it('should return false if not missing any documents for cohort enrollment', () => {
            user.identity_verified = false;
            jest.spyOn(user, 'missingIdentificationDocument', 'get').mockReturnValue(false);
            jest.spyOn(user, 'missingTranscripts', 'get').mockReturnValue(false);
            jest.spyOn(user, 'missingEnglishLanguageProficiencyDocuments', 'get').mockReturnValue(false);
            expect(user.missingRequiredDocumentsLegacy).toBe(false);
        });

        it('should return true if missing any documents for cohort enrollment', () => {
            const missingTranscriptsSpy = jest.spyOn(user, 'missingTranscripts', 'get').mockReturnValue(false);
            const missingEnglishLanguageProficiencyDocumentsSpy = jest
                .spyOn(user, 'missingEnglishLanguageProficiencyDocuments', 'get')
                .mockReturnValue(false);
            hasSignedEnrollmentAgreement = true;
            expect(user.missingRequiredDocumentsLegacy).toBe(false);

            missingTranscriptsSpy.mockReturnValue(true);
            expect(user.missingRequiredDocumentsLegacy).toBe(true);

            missingTranscriptsSpy.mockReturnValue(false);
            missingEnglishLanguageProficiencyDocumentsSpy.mockReturnValue(true);
            expect(user.missingRequiredDocumentsLegacy).toBe(true);

            // hasSignedEnrollmentAgreement should have no impact
            missingEnglishLanguageProficiencyDocumentsSpy.mockReturnValue(false);
            hasSignedEnrollmentAgreement = false;
            expect(user.missingRequiredDocumentsLegacy).toBe(false);

            hasSignedEnrollmentAgreement = true;
            expect(user.missingRequiredDocumentsLegacy).toBe(false);
        });
    });

    describe('missingRequiredDocuments', () => {
        it('should return false if not missingRequiredDocumentsLegacy and hasSignedEnrollmentAgreement', () => {
            jest.spyOn(user, 'missingRequiredDocumentsLegacy', 'get').mockReturnValue(false);
            mockHasSignedEnrollmentAgreement(true);
            expect(user.missingRequiredDocuments).toBe(false);
        });

        describe('when not missingRequiredDocumentsLegacy', () => {
            beforeEach(() => {
                jest.spyOn(user, 'missingRequiredDocumentsLegacy', 'get').mockReturnValue(false);
            });

            it('should return true if missingUserIdVerification', () => {
                jest.spyOn(user, 'missingUserIdVerification', 'get').mockReturnValue(true);
                expect(user.missingRequiredDocuments).toBe(true);
            });

            describe('when not missingUserIdVerification', () => {
                beforeEach(() => {
                    jest.spyOn(user, 'missingUserIdVerification', 'get').mockReturnValue(false);
                });

                it('should return false if !isAccepted', () => {
                    jest.spyOn(user, 'isAccepted', 'get').mockReturnValue(false);
                    expect(user.missingRequiredDocuments).toBe(false);
                });

                describe('when isAccepted', () => {
                    beforeEach(() => {
                        jest.spyOn(user, 'isAccepted', 'get').mockReturnValue(true);
                    });

                    it('should return true if !hasSignedEnrollmentAgreement', () => {
                        mockHasSignedEnrollmentAgreement(false);
                        expect(user.missingRequiredDocuments).toBe(true);
                    });

                    it('should return false if hasSignedEnrollmentAgreement', () => {
                        mockHasSignedEnrollmentAgreement(true);
                        expect(user.missingRequiredDocuments).toBe(false);
                    });
                });
            });
        });
    });

    describe('readyForApprovalLegacy', () => {
        it('should return false if user does not have required documents awaiting approval', () => {
            jest.spyOn(user, 'hasUploadedIdentification', 'get').mockReturnValue(true);
            user.identity_verified = true;
            jest.spyOn(user, 'hasUploadedTranscripts', 'get').mockReturnValue(true);
            jest.spyOn(user, 'careerProfileIndicatesUserShouldProvideTranscripts', 'get').mockReturnValue(true);
            user.transcripts_verified = true;
            jest.spyOn(user, 'hasUploadedEnglishLanguageProficiencyDocuments', 'get').mockReturnValue(true);
            jest.spyOn(
                user,
                'careerProfileIndicatesUserShouldUploadEnglishLanguageProficiencyDocuments',
                'get',
            ).mockReturnValue(true);
            user.english_language_proficiency_documents_approved = true;
            expect(user.readyForApprovalLegacy).toBe(false);
        });

        it('should return true if ID is ready for approval', () => {
            jest.spyOn(user, 'hasUploadedIdentification', 'get').mockReturnValue(true);
            user.identity_verified = false;
            expect(user.readyForApprovalLegacy).toBe(true);
        });

        describe('when ID is not ready for approval', () => {
            beforeEach(() => {
                jest.spyOn(user, 'hasUploadedIdentification', 'get').mockReturnValue(false);
            });

            it('should return true if transcripts are ready for approval', () => {
                jest.spyOn(user, 'hasUploadedTranscripts', 'get').mockReturnValue(true);
                jest.spyOn(user, 'careerProfileIndicatesUserShouldProvideTranscripts', 'get').mockReturnValue(true);
                user.transcripts_verified = false;
                expect(user.readyForApprovalLegacy).toBe(true);
            });

            describe('when transcripts are not ready for approval', () => {
                beforeEach(() => {
                    jest.spyOn(user, 'hasUploadedTranscripts', 'get').mockReturnValue(false);
                });

                it('should return true if english_language_proficiency_documents are ready for approval', () => {
                    jest.spyOn(user, 'hasUploadedEnglishLanguageProficiencyDocuments', 'get').mockReturnValue(true);
                    jest.spyOn(
                        user,
                        'careerProfileIndicatesUserShouldUploadEnglishLanguageProficiencyDocuments',
                        'get',
                    ).mockReturnValue(true);
                    user.english_language_proficiency_documents_approved = false;
                    expect(user.readyForApprovalLegacy).toBe(true);
                });
            });
        });
    });

    describe('readyForApproval', () => {
        it('should return false if not readyForApprovalLegacy and hasSignedEnrollmentAgreement', () => {
            jest.spyOn(user, 'readyForApprovalLegacy', 'get').mockReturnValue(false);
            mockHasSignedEnrollmentAgreement(true);
            expect(user.readyForApproval).toBe(false);
        });

        it('should return true if not readyForApprovalLegacy and not hasSignedEnrollmentAgreement', () => {
            jest.spyOn(user, 'readyForApprovalLegacy', 'get').mockReturnValue(false);
            mockHasSignedEnrollmentAgreement(false);
            expect(user.readyForApproval).toBe(true);
        });
    });

    describe('needsToProvideCohortBillingInfo', () => {
        it('should be true when user billing info is not in good standing', () => {
            const user = User.fixtures.getInstanceWithRole('learner');
            jest.spyOn(user, 'lastCohortApplication', 'get').mockReturnValue({
                cohort_id: 'relevant_cohort',
                status: 'accepted',
                in_good_standing: false,
                total_num_required_stripe_payments: 12,
            });
            expect(user.needsToProvideCohortBillingInfo).toBe(true);
        });
    });

    describe('needsToRegister', () => {
        it('should be false is user is failed', () => {
            const user = User.fixtures.getInstanceWithRole('learner');
            jest.spyOn(user, 'lastCohortApplication', 'get').mockReturnValue({
                cohort_id: 'relevant_cohort',
                status: 'accepted',
                in_good_standing: false,
                total_num_required_stripe_payments: 12,
            });
            user.relevant_cohort = Cohort.fixtures.getInstance();
            jest.spyOn(user.relevant_cohort, 'supportsPayments', 'get').mockReturnValue(true);
            user.lastCohortApplication.registered = false;
            user.lastCohortApplication.status = 'accepted';
            jest.spyOn(user, 'isFailed', 'get').mockReturnValue(true);
            expect(user.needsToRegister).toBe(false);
        });
    });

    describe('lockedDueToPastDuePayment', () => {
        it('should be true if acceptedCohortApplication is marked as locked_due_to_past_due_payment', () => {
            Object.defineProperty(user, 'acceptedCohortApplication', {
                value: {
                    locked_due_to_past_due_payment: true,
                },
            });
            expect(user.lockedDueToPastDuePayment).toBe(true);
        });

        it('should be false if acceptedCohortApplication is not marked as locked_due_to_past_due_payment', () => {
            Object.defineProperty(user, 'acceptedCohortApplication', {
                value: {
                    locked_due_to_past_due_payment: false,
                },
            });
            expect(user.lockedDueToPastDuePayment).toBe(false);
        });
    });

    describe('canEditCurrentCohortApplication', () => {
        beforeEach(() => {
            $injector.get('CohortFixtures');
            $injector.get('CohortApplicationFixtures');
        });

        it('should be false if no applications', () => {
            const user = User.fixtures.getInstanceWithRole('learner');
            user.cohort_applications = [];
            user.relevant_cohort = Cohort.fixtures.getInstance();
            jest.spyOn(user.relevant_cohort, 'admissionRoundDeadlineHasPassed').mockReturnValue(false);
            expect(user.canEditCurrentCohortApplication).toBe(false);
        });

        it('should be false if no relevant cohort', () => {
            const user = User.fixtures.getInstanceWithRole('learner');
            user.cohort_applications = [
                CohortApplication.fixtures.getInstance({
                    status: 'pending',
                    applied_at: 0,
                }),
            ];
            user.relevant_cohort = undefined;
            expect(user.canEditCurrentCohortApplication).toBe(false);
        });

        it('should be false if last application is accepted', () => {
            const user = User.fixtures.getInstanceWithRole('learner');
            user.cohort_applications = [
                CohortApplication.fixtures.getInstance({
                    status: 'pending',
                    applied_at: 0,
                }),
                CohortApplication.fixtures.getInstance({
                    status: 'accepted',
                    applied_at: Date.now() / 1000,
                }),
            ];
            user.relevant_cohort = Cohort.fixtures.getInstance();
            jest.spyOn(user.relevant_cohort, 'admissionRoundDeadlineHasPassed').mockReturnValue(false);
            expect(user.canEditCurrentCohortApplication).toBe(false);
        });

        it('should be false if last application is pre_accepted', () => {
            const user = User.fixtures.getInstanceWithRole('learner');
            user.cohort_applications = [
                CohortApplication.fixtures.getInstance({
                    status: 'pending',
                    applied_at: 0,
                }),
                CohortApplication.fixtures.getInstance({
                    status: 'pre_accepted',
                    applied_at: Date.now() / 1000,
                }),
            ];
            user.relevant_cohort = Cohort.fixtures.getInstance();
            jest.spyOn(user.relevant_cohort, 'admissionRoundDeadlineHasPassed').mockReturnValue(false);
            expect(user.canEditCurrentCohortApplication).toBe(false);
        });

        it('should be false if they are past deadline', () => {
            const user = User.fixtures.getInstanceWithRole('learner');
            user.cohort_applications = [
                CohortApplication.fixtures.getInstance({
                    status: 'pending',
                    applied_at: Date.now() / 1000,
                }),
            ];
            user.relevant_cohort = Cohort.fixtures.getInstance();
            jest.spyOn(user.relevant_cohort, 'admissionRoundDeadlineHasPassed').mockReturnValue(true);
            expect(user.canEditCurrentCohortApplication).toBe(false);
            expect(user.relevant_cohort.admissionRoundDeadlineHasPassed).toHaveBeenCalledWith(
                user.cohort_applications[0].appliedAt,
            );
        });

        it('should be true when their last application exists, is not accepted or pre_accepted, and is not past the deadline', () => {
            const user = User.fixtures.getInstanceWithRole('learner');
            user.cohort_applications = [
                CohortApplication.fixtures.getInstance({
                    status: 'pending',
                    applied_at: 0,
                }),
            ];
            user.relevant_cohort = Cohort.fixtures.getInstance();
            jest.spyOn(user.relevant_cohort, 'admissionRoundDeadlineHasPassed').mockReturnValue(false);
            expect(user.canEditCurrentCohortApplication).toBe(true);
        });
    });

    describe('onboardingComplete', () => {
        it('should be true if user can skip apply', () => {
            const user = User.fixtures.getInstanceWithRole('learner');
            user.skip_apply = true;
            expect(user.onboardingComplete).toBe(true);
        });

        it('should be true if user cannot apply', () => {
            const user = User.fixtures.getInstanceWithRole('learner');
            user.relevant_cohort = undefined;
            expect(user.onboardingComplete).toBe(true);
        });

        it('should be true for a user who has submitted an application', () => {
            const user = User.fixtures.getInstanceWithRole('learner');
            user.cohort_applications = [
                CohortApplication.fixtures.getInstance({
                    status: 'pending',
                    applied_at: 0,
                }),
            ];
            expect(user.onboardingComplete).toBe(true);
        });

        it('should be false for a user who has not submitted an application', () => {
            const user = User.fixtures.getInstanceWithRole('learner');
            jest.spyOn(user, 'canApplyToSmartly', 'get').mockReturnValue(true);
            user.relevant_cohort = Cohort.fixtures.getInstance();
            user.cohort_applications = [];
            expect(user.onboardingComplete).toBe(false);
        });
    });

    describe('shouldRedirectOnIncompleteOnboarding', () => {
        it('should be true for a normal learner', () => {
            expect(User.fixtures.getInstanceWithRole('learner').shouldRedirectOnIncompleteOnboarding).toBe(true);
        });

        it('should be false for demo user', () => {
            user = User.fixtures.getInstanceWithRole('learner');
            user.fallback_program_type = 'demo';
            expect(user.shouldRedirectOnIncompleteOnboarding).toBe(false);
        });

        it('should be false for external-institution user', () => {
            user = User.fixtures.getInstanceWithRole('learner');
            user.fallback_program_type = 'external';
            expect(user.shouldRedirectOnIncompleteOnboarding).toBe(false);
        });

        it('should be false for any non-learner', () => {
            expect(User.fixtures.getInstanceWithRole('editor').shouldRedirectOnIncompleteOnboarding).toBe(false);
            expect(User.fixtures.getInstanceWithRole('super_editor').shouldRedirectOnIncompleteOnboarding).toBe(false);
            expect(User.fixtures.getInstanceWithRole('admin').shouldRedirectOnIncompleteOnboarding).toBe(false);
        });
    });

    describe('shouldVisitApplicationStatus', () => {
        it('should be false if !shouldVisitRegistration and !showPaidCertSelection', () => {
            jest.spyOn(user, 'shouldVisitRegistration', 'get').mockReturnValue(false);
            jest.spyOn(user, 'showPaidCertSelection', 'get').mockReturnValue(false);
            expect(user.shouldVisitApplicationStatus).toBe(false);
        });

        it('should be false if !shouldVisitRegistration and showPaidCertSelection, but reapplyingOrEditingApplication', () => {
            jest.spyOn(user, 'shouldVisitRegistration', 'get').mockReturnValue(false);
            jest.spyOn(user, 'showPaidCertSelection', 'get').mockReturnValue(true);
            jest.spyOn(user, 'reapplyingOrEditingApplication', 'get').mockReturnValue(true);
            expect(user.shouldVisitApplicationStatus).toBe(false);
        });

        it('should be true if shouldVisitRegistration', () => {
            jest.spyOn(user, 'shouldVisitRegistration', 'get').mockReturnValue(true);
            expect(user.shouldVisitApplicationStatus).toBe(true);
        });

        it('should be true if showPaidCertSelection and !reapplyingOrEditingApplication', () => {
            jest.spyOn(user, 'showPaidCertSelection', 'get').mockReturnValue(true);
            jest.spyOn(user, 'reapplyingOrEditingApplication', 'get').mockReturnValue(false);
            expect(user.shouldVisitApplicationStatus).toBe(true);
        });
    });

    describe('shouldVisitRegistration', () => {
        it('should be true if after registration open date on relevant cohort and user still needs to register ', () => {
            user.relevant_cohort = Cohort.fixtures.getInstance();
            jest.spyOn(user.relevant_cohort, 'afterRegistrationOpenDate', 'get').mockReturnValue(true);
            jest.spyOn(user, 'needsToRegister', 'get').mockReturnValue(true);
            expect(user.shouldVisitRegistration).toBe(true);
        });

        it('should be true if after registration open date on relevant cohort and user still needs to provide billing info', () => {
            user.relevant_cohort = Cohort.fixtures.getInstance();
            jest.spyOn(user.relevant_cohort, 'afterRegistrationOpenDate', 'get').mockReturnValue(true);
            jest.spyOn(user, 'needsToProvideCohortBillingInfo', 'get').mockReturnValue(true);
            expect(user.shouldVisitRegistration).toBe(true);
        });
    });

    describe('showPaidCertSelection', () => {
        it('should return false if in Cordova', () => {
            Object.defineProperty($window, 'CORDOVA', {
                value: true,
                configurable: true,
            });

            const user = User.fixtures.getInstanceWithRole('learner');
            user.can_purchase_paid_certs = true;

            const paidCertApplication = CohortApplication.fixtures.getInstance({
                status: 'pre_accepted',
            });
            jest.spyOn(paidCertApplication, 'isPaidCert', 'get').mockReturnValue(true);
            user.cohort_applications = [paidCertApplication];

            expect(user.showPaidCertSelection).toBe(false);

            delete $window.CORDOVA;
        });

        it('should return true if can_purchase_paid_certs and last application was rejected or expelled', () => {
            const user = User.fixtures.getInstanceWithRole('learner');
            user.can_purchase_paid_certs = true;

            const application = CohortApplication.fixtures.getInstance({
                status: 'rejected',
            });
            jest.spyOn(application, 'isPaidCert', 'get').mockReturnValue(false);
            user.cohort_applications = [application];
            expect(user.showPaidCertSelection).toBe(true);

            application.status = 'expelled';
            expect(user.showPaidCertSelection).toBe(true);
        });

        it('should return true if can_purchase_paid_certs and last application is a non-accepted paid cert application', () => {
            const user = User.fixtures.getInstanceWithRole('learner');
            user.can_purchase_paid_certs = true;

            const paidCertApplication = CohortApplication.fixtures.getInstance({
                status: 'pre_accepted',
            });
            jest.spyOn(paidCertApplication, 'isPaidCert', 'get').mockReturnValue(true);
            user.cohort_applications = [paidCertApplication];

            expect(user.showPaidCertSelection).toBe(true);
        });
    });

    describe('reapplyingOrEditingApplication', () => {
        describe('getter', () => {
            it("should be false if reapplyingOrEditingApplication in ClientStorage is not 'true'", () => {
                const clientStorageGetItemSpy = jest.spyOn(ClientStorage, 'getItem');

                // when 'undefined'
                clientStorageGetItemSpy.mockReturnValue(undefined);
                expect(user.reapplyingOrEditingApplication).toBe(false);
                expect(clientStorageGetItemSpy).toHaveBeenCalledWith('reapplyingOrEditingApplication');

                clientStorageGetItemSpy.mockClear();

                // when not 'true'
                clientStorageGetItemSpy.mockReturnValue('not_true');
                expect(user.reapplyingOrEditingApplication).toBe(false);
                expect(clientStorageGetItemSpy).toHaveBeenCalledWith('reapplyingOrEditingApplication');
            });

            it("should be true if reapplyingOrEditingApplication in ClientStorage is 'true'", () => {
                jest.spyOn(ClientStorage, 'getItem').mockReturnValue('true');
                expect(user.reapplyingOrEditingApplication).toBe(true);
                expect(ClientStorage.getItem).toHaveBeenCalledWith('reapplyingOrEditingApplication');
            });
        });

        describe('setter', () => {
            it("should error if value is not of type 'boolean'", () => {
                jest.spyOn(ClientStorage, 'setItem').mockImplementation(() => {});

                expect(() => {
                    user.reapplyingOrEditingApplication = 'true';
                }).toThrowError(
                    Error,
                    'reapplyingOrEditingApplication must be set to a boolean; it cannot be set to type: string',
                );
                expect(ClientStorage.setItem).not.toHaveBeenCalled();

                expect(() => {
                    user.reapplyingOrEditingApplication = 'false';
                }).toThrowError(
                    Error,
                    'reapplyingOrEditingApplication must be set to a boolean; it cannot be set to type: string',
                );
                expect(ClientStorage.setItem).not.toHaveBeenCalled();

                expect(() => {
                    user.reapplyingOrEditingApplication = 1;
                }).toThrowError(
                    Error,
                    'reapplyingOrEditingApplication must be set to a boolean; it cannot be set to type: number',
                );
                expect(ClientStorage.setItem).not.toHaveBeenCalled();

                expect(() => {
                    user.reapplyingOrEditingApplication = {};
                }).toThrowError(
                    Error,
                    'reapplyingOrEditingApplication must be set to a boolean; it cannot be set to type: object',
                );
                expect(ClientStorage.setItem).not.toHaveBeenCalled();

                expect(() => {
                    user.reapplyingOrEditingApplication = true;
                }).not.toThrowError();
                expect(ClientStorage.setItem).toHaveBeenCalledWith('reapplyingOrEditingApplication', true);

                jest.spyOn(ClientStorage, 'removeItem').mockImplementation(() => {});

                expect(() => {
                    user.reapplyingOrEditingApplication = false;
                }).not.toThrowError();
                expect(ClientStorage.removeItem).toHaveBeenCalledWith('reapplyingOrEditingApplication');
            });

            it('should set reapplyingOrEditingApplication in ClientStorage', () => {
                jest.spyOn(ClientStorage, 'setItem').mockImplementation(() => {});
                user.reapplyingOrEditingApplication = true;
                expect(ClientStorage.setItem).toHaveBeenCalledWith('reapplyingOrEditingApplication', true);
            });
        });
    });

    describe('ensureLoginEvent', () => {
        let User;
        let EventLogger;
        let user;

        beforeEach(() => {
            User = $injector.get('User');
            EventLogger = $injector.get('EventLogger');

            user = User.fixtures.getInstanceWithRole('learner');
            jest.spyOn(EventLogger.prototype, 'log').mockImplementation(() => {});
        });

        afterEach(() => {
            window.CORDOVA = undefined;
        });

        it('should update has_logged_in flag on user', () => {
            user.has_logged_in = false;
            User.expect('update');
            user.ensureLoginEvent();
            User.flush('update');
            expect(user.has_logged_in).toBe(true);
        });

        it('should log to Facebook natively if in Cordova context', () => {
            window.CORDOVA = true;
            user.has_logged_in = false;
            User.expect('update');
            user.ensureLoginEvent();
            User.flush('update');
            expect(user.has_logged_in).toBe(true);
            expect(EventLogger.prototype.log).toHaveBeenCalledWith('user:logged_in_first_time', expect.anything(), {
                cordovaFacebook: true,
            });
        });

        it("should set label in event payload to 'gl' if the registration info from the dynamic landing page form tells us it's a 'good lead'", () => {
            user.has_logged_in = false;
            jest.spyOn(preSignupValues, 'getDynamicLandingPageMultiStepFormRegistrationInfo').mockReturnValue({
                foo: 'foo',
            });
            jest.spyOn(preSignupValues, 'convertRegistrationInfoFromDLPToHumanReadableForm').mockReturnValue({
                foo: 'FOO',
            });
            jest.spyOn(preSignupValues, 'goodLead').mockReturnValue(true);
            User.expect('update');
            user.ensureLoginEvent();
            expect(preSignupValues.getDynamicLandingPageMultiStepFormRegistrationInfo).toHaveBeenCalled();
            expect(preSignupValues.convertRegistrationInfoFromDLPToHumanReadableForm).toHaveBeenCalledWith({
                foo: 'foo',
            });
            expect(preSignupValues.goodLead).toHaveBeenCalledWith({ foo: 'foo' });
            User.flush('update');
            expect(EventLogger.prototype.log).toHaveBeenCalledWith(
                'user:logged_in_first_time',
                {
                    gl: true,
                    label: 'gl',
                    foo: 'FOO',
                },
                expect.anything(),
            );
        });

        it("should not include the label in event payload if the registration info from the dynamic landing page form tells us it's not a 'good lead'", () => {
            user.has_logged_in = false;
            jest.spyOn(preSignupValues, 'getDynamicLandingPageMultiStepFormRegistrationInfo').mockReturnValue({
                foo: 'foo',
            });
            jest.spyOn(preSignupValues, 'convertRegistrationInfoFromDLPToHumanReadableForm').mockReturnValue({
                foo: 'FOO',
            });
            jest.spyOn(preSignupValues, 'goodLead').mockReturnValue(false);
            User.expect('update');
            user.ensureLoginEvent();
            expect(preSignupValues.getDynamicLandingPageMultiStepFormRegistrationInfo).toHaveBeenCalled();
            expect(preSignupValues.convertRegistrationInfoFromDLPToHumanReadableForm).toHaveBeenCalledWith({
                foo: 'foo',
            });
            expect(preSignupValues.goodLead).toHaveBeenCalledWith({ foo: 'foo' });
            User.flush('update');
            expect(EventLogger.prototype.log).toHaveBeenCalledWith(
                'user:logged_in_first_time',
                {
                    // Note tha absence of the `label` in the event payload
                    gl: false,
                    foo: 'FOO',
                },
                expect.anything(),
            );
        });

        it('should do nothing if user.has_logged_in', () => {
            user.has_logged_in = true;
            user.ensureLoginEvent();
            expect(user.has_logged_in).toBe(true);
            expect(EventLogger.prototype.log).not.toHaveBeenCalled();
        });

        it('should do nothing if user.ghostMode', () => {
            user.has_logged_in = false; // prevent false positives
            user.ghostMode = true;
            user.ensureLoginEvent();
            expect(user.ghostMode).toBe(true);
            expect(EventLogger.prototype.log).not.toHaveBeenCalled();
        });
    });

    describe('ensureHasSeenWelcome watch', () => {
        let User;
        let EventLogger;
        let user;

        beforeEach(() => {
            User = $injector.get('User');
            EventLogger = $injector.get('EventLogger');

            user = User.fixtures.getInstanceWithRole('learner');
            jest.spyOn(EventLogger.prototype, 'log').mockImplementation(() => {});
        });

        afterEach(() => {
            window.CORDOVA = undefined;
        });

        it('should log to Facebook natively if in Cordova context', () => {
            window.CORDOVA = true;
            user.has_seen_welcome = false;
            User.expect('update');
            user.ensureHasSeenWelcome();
            User.flush('update');
            expect(user.has_seen_welcome).toBe(true);
            expect(EventLogger.prototype.log).toHaveBeenCalledWith(
                'onboarding:complete',
                {
                    user_id: user.id,
                },
                {
                    cordovaFacebook: true,
                },
            );
        });

        it('should do nothing if already set', () => {
            user.has_seen_welcome = true;
            user.ensureHasSeenWelcome();
            expect(user.has_seen_welcome).toBe(true);
            expect(EventLogger.prototype.log).not.toHaveBeenCalled();
        });
    });

    describe('unverifiedForCurrentIdVerificationPeriod', () => {
        let application;

        beforeEach(() => {
            user = User.fixtures.getInstance({
                user_id_verifications: [],
            });
            application = {
                status: 'accepted',
            };
            jest.spyOn(User.prototype, 'acceptedOrPreAcceptedCohortApplication', 'get').mockImplementation(
                () => application,
            );
            setupRelevantCohort();
        });

        function setupRelevantCohort() {
            user.relevant_cohort = Cohort.fixtures.getInstance({
                start_date: moment().subtract(1, 'day').toDate().getTime() / 1000,
                id_verification_periods: [
                    {
                        start_date_days_offset: 0,
                        due_date_days_offset: 10,
                    },
                ],
            });
            expect(user.relevant_cohort.activeIdVerificationPeriod).not.toBe(null); // sanity check
        }

        it('should be false if no verification period', () => {
            jest.spyOn(user.relevant_cohort, 'activeIdVerificationPeriod', 'get').mockReturnValue(null);
            expect(user.unverifiedForCurrentIdVerificationPeriod).toBe(false);
        });

        it('should be false if no accepted or pre-accepted application', () => {
            application = null;
            expect(user.unverifiedForCurrentIdVerificationPeriod).toBe(false);
        });

        it('should be false if the cohort supports a registration deadline and the application is not registered', () => {
            jest.spyOn(user.relevant_cohort, 'supportsRegistrationDeadline', 'get').mockReturnValue(true);
            application.registered = false;
            expect(user.unverifiedForCurrentIdVerificationPeriod).toBe(false);
        });

        it('should be true if no user_id_verification', () => {
            expect(user.unverifiedForCurrentIdVerificationPeriod).toBe(true);
        });

        it('should be true if no user_id_verification for this period', () => {
            user = User.fixtures.getInstance({
                user_id_verifications: [{}],
            });
            setupRelevantCohort();

            const userIdVerification = user.user_id_verifications[0];
            jest.spyOn(userIdVerification, 'forVerificationPeriod').mockReturnValue(false);
            expect(user.unverifiedForCurrentIdVerificationPeriod).toBe(true);
            expect(userIdVerification.forVerificationPeriod).toHaveBeenCalledWith(
                user.relevant_cohort.id_verification_periods[0],
            );
        });

        it('should be false if any user_id_verification exists for this period', () => {
            user = User.fixtures.getInstance({
                user_id_verifications: [{}, {}],
            });
            setupRelevantCohort();

            const userIdVerification = user.user_id_verifications[1]; // second element in the array
            jest.spyOn(userIdVerification, 'forVerificationPeriod').mockReturnValue(true);

            expect(user.unverifiedForCurrentIdVerificationPeriod).toBe(false);
            expect(userIdVerification.forVerificationPeriod).toHaveBeenCalledWith(
                user.relevant_cohort.id_verification_periods[0],
            );
        });
    });

    describe('setContinueApplicationInMarketingFlag', () => {
        let user;
        beforeEach(() => {
            user = User.fixtures.getInstance();
            expect(user.roleName()).toEqual('learner');
        });

        it('should set continueApplicationInMarketing to false if user is demo', () => {
            jest.spyOn(user, 'hasEverApplied', 'get').mockReturnValue(false);
            jest.spyOn(user, 'programType', 'get').mockReturnValue('demo');
            user.setContinueApplicationInMarketingFlag();
            expect(ClientStorage.getItem('continueApplicationInMarketing')).toBeUndefined();
        });

        it('should set continueApplicationInMarketing to false if user has an external institution', () => {
            jest.spyOn(user, 'hasEverApplied', 'get').mockReturnValue(false);
            jest.spyOn(user, 'programType', 'get').mockReturnValue('external');
            user.setContinueApplicationInMarketingFlag();
            expect(ClientStorage.getItem('continueApplicationInMarketing')).toBeUndefined();
        });

        it('should set continueApplicationInMarketing to false if user hasEverApplied', () => {
            jest.spyOn(user, 'hasEverApplied', 'get').mockReturnValue(true);
            user.setContinueApplicationInMarketingFlag();
            expect(ClientStorage.getItem('continueApplicationInMarketing')).toBeUndefined();
        });

        it('should set continueApplicationInMarketing to false if user is a non-learner', () => {
            jest.spyOn(user, 'roleName').mockReturnValue('admin');
            jest.spyOn(user, 'hasEverApplied', 'get').mockReturnValue(false);
            jest.spyOn(user, 'programType', 'get').mockReturnValue('mba');
            user.setContinueApplicationInMarketingFlag();
            expect(ClientStorage.getItem('continueApplicationInMarketing')).toBeUndefined();
        });

        it('should set continueApplicationInMarketing to true if user meets all conditions', () => {
            jest.spyOn(user, 'hasEverApplied', 'get').mockReturnValue(false);
            jest.spyOn(user, 'programType', 'get').mockReturnValue('mba');
            user.setContinueApplicationInMarketingFlag();
            expect(ClientStorage.getItem('continueApplicationInMarketing')).toEqual('true');
        });
    });

    describe('canDownloadTranscripts', () => {
        it('should be true for a user that is accepted and their programType supportsTranscriptDownload', () => {
            user = User.fixtures.getInstance();
            jest.spyOn(user, 'isAccepted', 'get').mockReturnValue(true);
            jest.spyOn(Cohort, 'supportsTranscriptDownload').mockReturnValue(true);
            expect(user.canDownloadTranscripts).toBe(true);
        });

        it('should be false if failed', () => {
            user = User.fixtures.getInstance();
            jest.spyOn(user, 'graduationStatus', 'get').mockReturnValue('failed');
            jest.spyOn(user, 'isAccepted', 'get').mockReturnValue(true);
            jest.spyOn(Cohort, 'supportsTranscriptDownload').mockReturnValueOnce(false).mockReturnValueOnce(true);
            expect(user.canDownloadTranscripts).toBe(false);
        });

        it('should be false if accepted but their programType !supportsTranscriptDownload', () => {
            user = User.fixtures.getInstance();
            jest.spyOn(user, 'isAccepted', 'get').mockReturnValue(true);
            jest.spyOn(Cohort, 'supportsTranscriptDownload').mockReturnValue(false);
            expect(user.canDownloadTranscripts).toBe(false);
        });
    });

    describe('isDeferredFromMbaOrEmba', () => {
        it('should be true if deferred from mba program', () => {
            user = User.fixtures.getInstance({
                cohort_applications: [
                    CohortApplication.fixtures.getInstance({
                        status: 'deferred',
                        program_type: 'mba',
                    }),
                ],
            });
            expect(user.isDeferredFromMbaOrEmba).toBe(true);
        });

        it('should be true if deferred from emba program', () => {
            user = User.fixtures.getInstance({
                cohort_applications: [
                    CohortApplication.fixtures.getInstance({
                        status: 'deferred',
                        program_type: 'emba',
                    }),
                ],
            });
            expect(user.isDeferredFromMbaOrEmba).toBe(true);
        });

        it('should not error when no cohort applications', () => {
            user = User.fixtures.getInstance({
                cohort_applications: null,
            });
            expect(user.isDeferredFromMbaOrEmba).toBe(false);
        });

        it('should be false if not deferred', () => {
            user = User.fixtures.getInstance({
                cohort_applications: [
                    CohortApplication.fixtures.getInstance({
                        status: 'pending',
                        program_type: 'mba',
                    }),
                ],
            });
            expect(user.isDeferredFromMbaOrEmba).toBe(false);
        });

        it('should be false if deferred but not from mba or emba program', () => {
            user = User.fixtures.getInstance({
                cohort_applications: [
                    CohortApplication.fixtures.getInstance({
                        status: 'deferred',
                        program_type: 'the_business_certificate',
                    }),
                ],
            });
            expect(user.isDeferredFromMbaOrEmba).toBe(false);
        });
    });

    describe('expirationCancellationOrRenewalDateForPosition', () => {
        let user;
        let position;
        let date;
        let subscription;

        beforeEach(() => {
            const OpenPosition = $injector.get('OpenPosition');
            $injector.get('OpenPositionFixtures');

            const Subscription = $injector.get('Subscription');
            subscription = Subscription.new();

            user = User.fixtures.getInstance();
            jest.spyOn(user, 'subscriptionForPosition').mockImplementation(() => subscription);

            position = OpenPosition.fixtures.getInstance();
            date = new Date();
        });

        it('should work with a subscription', () => {
            subscription.current_period_end = date.getTime() / 1000;
            expect(user.expirationCancellationOrRenewalDateForPosition(position)).toEqual(date);
            expect(user.subscriptionForPosition).toHaveBeenCalledWith(position);
        });

        it('should work with an auto_expiration_date in EDT', () => {
            subscription = null;
            position.auto_expiration_date = '2018-07-01';
            expect(user.expirationCancellationOrRenewalDateForPosition(position)).toEqual(
                new Date('2018-07-01 7:00 EDT'),
            );
        });

        it('should work with an auto_expiration_date in EST', () => {
            subscription = null;
            position.auto_expiration_date = '2018-01-01';
            expect(user.expirationCancellationOrRenewalDateForPosition(position)).toEqual(
                new Date('2018-01-01 7:00 EST'),
            );
        });

        it('should be null if no subscription', () => {
            subscription = null;
            position.auto_expiration_date = null;
            expect(user.expirationCancellationOrRenewalDateForPosition(position)).toEqual(null);
        });
    });

    describe('expirationMessageLocaleSuffix', () => {
        let user;
        let position;

        beforeEach(() => {
            const OpenPosition = $injector.get('OpenPosition');
            $injector.get('OpenPositionFixtures');
            user = User.fixtures.getInstance();
            position = OpenPosition.fixtures.getInstance({
                featured: true,
                archived: false,
            });
        });

        it('should work when subscription.hasActiveAutoRenewal', () => {
            jest.spyOn(user, 'subscriptionForPosition').mockReturnValue({
                hasActiveAutoRenewal: true,
            });
            expect(user.expirationMessageLocaleSuffix(position)).toEqual('renews');
            expect(user.subscriptionForPosition).toHaveBeenCalledWith(position);
        });

        it('should work when !subscription.hasActiveAutoRenewal', () => {
            jest.spyOn(user, 'subscriptionForPosition').mockReturnValue({
                hasActiveAutoRenewal: false,
            });
            expect(user.expirationMessageLocaleSuffix(position)).toEqual('cancels');
            expect(user.subscriptionForPosition).toHaveBeenCalledWith(position);
        });

        it('should work when manually archived', () => {
            jest.spyOn(user, 'subscriptionForPosition').mockReturnValue(null);
            position.archived = true;
            position.manually_archived = true;
            expect(user.expirationMessageLocaleSuffix(position)).toEqual(null);
        });

        it('should work automatically archived', () => {
            jest.spyOn(user, 'subscriptionForPosition').mockReturnValue(null);
            position.archived = true;
            position.manually_archived = false;
            expect(user.expirationMessageLocaleSuffix(position)).toEqual('expired');
        });

        it('should work when not archived', () => {
            jest.spyOn(user, 'subscriptionForPosition').mockReturnValue(null);
            position.archived = false;
            position.auto_expiration_date = '2018-01-01';
            expect(user.expirationMessageLocaleSuffix(position)).toEqual('expires');
        });

        it('should work when drafted', () => {
            jest.spyOn(user, 'subscriptionForPosition').mockReturnValue(null);
            position.featured = false;
            expect(user.expirationMessageLocaleSuffix(position)).toEqual(null);
        });
    });

    describe('positionExpiresOrCancelsSoon', () => {
        let user;
        let position;

        beforeEach(() => {
            user = User.fixtures.getInstance();
            jest.spyOn(user, 'expirationCancellationOrRenewalDateForPosition');
            position = {};
        });

        it('should be false if archived', () => {
            user.expirationCancellationOrRenewalDateForPosition.mockReturnValue(null);
            position.archived = true;
            expect(user.positionExpiresOrCancelsSoon(position)).toEqual(false);
            expect(user.expirationCancellationOrRenewalDateForPosition).not.toHaveBeenCalled();
        });

        it('should be false if no expiration date', () => {
            user.expirationCancellationOrRenewalDateForPosition.mockReturnValue(null);
            expect(user.positionExpiresOrCancelsSoon(position)).toEqual(false);
            expect(user.expirationCancellationOrRenewalDateForPosition).toHaveBeenCalledWith(position);
        });

        it('should be true if expirationDate in the next 10 days', () => {
            assertReturnValueForExpiration(false, moment().utc().startOf('day').subtract(1, 'day'));
            assertReturnValueForExpiration(true, moment().utc().startOf('day'));
            assertReturnValueForExpiration(true, moment().utc().startOf('day').add(1, 'days'));
            assertReturnValueForExpiration(true, moment().utc().startOf('day').add(9, 'days'));
            assertReturnValueForExpiration(false, moment().utc().startOf('day').add(12, 'days'));
        });

        function assertReturnValueForExpiration(expectedValue, expiration) {
            const formatted = expiration.format('YYYY-MM-DD');

            user.expirationCancellationOrRenewalDateForPosition.mockReset();
            user.expirationCancellationOrRenewalDateForPosition.mockReturnValue(new Date(formatted));
            expect(user.positionExpiresOrCancelsSoon(position)).toEqual(expectedValue);
            expect(user.expirationCancellationOrRenewalDateForPosition).toHaveBeenCalledWith(position);
        }
    });

    describe('hiring manager', () => {
        let HiringTeam;
        let HiringApplication;
        let user;

        beforeEach(() => {
            HiringTeam = $injector.get('HiringTeam');
            HiringApplication = $injector.get('HiringApplication');
            $injector.get('HiringTeamFixtures');
            $injector.get('HiringApplicationFixtures');

            user = User.fixtures.getInstance();
            user.hiring_application = HiringApplication.fixtures.getInstance();
            user.hiring_team = HiringTeam.fixtures.getInstance();
            jest.spyOn(user, 'hasHiringManagerAccess', 'get').mockReturnValue(true);
        });

        describe('hasSourcingAccess', () => {
            it('should be true when legacy hiring plan', () => {
                jest.spyOn(user, 'usingLegacyHiringPlan', 'get').mockReturnValue(true);
                jest.spyOn(user, 'hasSubscribedUnlimitedWithSourcingAccess', 'get').mockReturnValue(false);
                expect(user.hasSourcingAccess).toBe(true);
            });

            it('should be true when subscribed unlimited', () => {
                jest.spyOn(user, 'usingLegacyHiringPlan', 'get').mockReturnValue(false);
                jest.spyOn(user, 'hasSubscribedUnlimitedWithSourcingAccess', 'get').mockReturnValue(true);
                expect(user.hasSourcingAccess).toBe(true);
            });
        });

        describe('hasSourcingUpsellAccess', () => {
            it('should be true when user does not have sourcing access but is accepted unsubscribed unlimited', () => {
                jest.spyOn(user, 'hasSourcingAccess', 'get').mockReturnValue(false);
                jest.spyOn(user, 'usingUnlimitedWithSourcingHiringPlan', 'get').mockReturnValue(true);
                jest.spyOn(user, 'hasSubscribedUnlimitedWithSourcingAccess', 'get').mockReturnValue(false);
                user.hiring_application.status = 'accepted';
                expect(user.hasSourcingUpsellAccess).toBe(true);
            });

            it('should be true when user does not have sourcing access but is using pay-per-post', () => {
                jest.spyOn(user, 'hasSourcingAccess', 'get').mockReturnValue(false);
                jest.spyOn(user, 'usingUnlimitedWithSourcingHiringPlan', 'get').mockReturnValue(false);
                jest.spyOn(user, 'usingPayPerPostHiringPlan', 'get').mockReturnValue(true);
                user.hiring_application.status = 'accepted';
                expect(user.hasSourcingUpsellAccess).toBe(true);
            });

            it('should be false if not accepted unsubscribed unlimited nor pay-per-post', () => {
                jest.spyOn(user, 'hasSourcingAccess', 'get').mockReturnValue(false);
                jest.spyOn(user, 'usingUnlimitedWithSourcingHiringPlan', 'get').mockReturnValue(false);
                jest.spyOn(user, 'usingPayPerPostHiringPlan', 'get').mockReturnValue(false);
                user.hiring_application.status = 'accepted';
                expect(user.hasSourcingUpsellAccess).toBe(false);
            });
        });

        describe('hasHiringSourcingOrUpsellAccess', () => {
            it('should be true if hasSourcingAccess', () => {
                jest.spyOn(user, 'hasSourcingAccess', 'get').mockReturnValue(true);
                jest.spyOn(user, 'hasSourcingUpsellAccess', 'get').mockReturnValue(false);
                expect(user.hasHiringSourcingOrUpsellAccess).toBe(true);
            });

            it('should be true if hasSourcingAccess', () => {
                jest.spyOn(user, 'hasSourcingAccess', 'get').mockReturnValue(false);
                jest.spyOn(user, 'hasSourcingUpsellAccess', 'get').mockReturnValue(true);
                expect(user.hasHiringSourcingOrUpsellAccess).toBe(true);
            });

            it('should be false if both false', () => {
                jest.spyOn(user, 'hasSourcingAccess', 'get').mockReturnValue(false);
                jest.spyOn(user, 'hasSourcingUpsellAccess', 'get').mockReturnValue(false);
                expect(user.hasHiringSourcingOrUpsellAccess).toBe(false);
            });
        });

        describe('hasHiringChoosePlanAccess', () => {
            describe('when isHiringTeamOwner', () => {
                beforeEach(() => {
                    jest.spyOn(user, 'isHiringTeamOwner', 'get').mockReturnValue(true);
                });

                it('should be true if no hiring plan', () => {
                    jest.spyOn(user, 'hiringPlan', 'get').mockReturnValue(null);
                    jest.spyOn(user, 'usingLegacyHiringPlan', 'get').mockReturnValue(false);

                    // This makes no sense, but for the sake of preventing false positive
                    jest.spyOn(user, 'usingUnlimitedWithSourcingHiringPlan', 'get').mockReturnValue(true);
                    user.hiring_application.status = 'accepted';

                    expect(user.hasHiringChoosePlanAccess).toBe(true);
                });

                it('should be true if pending unlimited', () => {
                    jest.spyOn(user, 'hiringPlan', 'get').mockReturnValue('foo');
                    jest.spyOn(user, 'usingUnlimitedWithSourcingHiringPlan', 'get').mockReturnValue(true);
                    jest.spyOn(user, 'usingLegacyHiringPlan', 'get').mockReturnValue(false);
                    user.hiring_application.status = 'pending';
                    expect(user.hasHiringChoosePlanAccess).toBe(true);
                });

                it('should be false otherwise', () => {
                    jest.spyOn(user, 'hiringPlan', 'get').mockReturnValue('foo');
                    jest.spyOn(user, 'usingUnlimitedWithSourcingHiringPlan', 'get').mockReturnValue(false);
                    jest.spyOn(user, 'usingLegacyHiringPlan', 'get').mockReturnValue(false);
                    user.hiring_application.status = 'pending';
                    expect(user.hasHiringChoosePlanAccess).toBe(false);
                });

                it('should be false if legacy', () => {
                    jest.spyOn(user, 'hiringPlan', 'get').mockReturnValue(null);
                    jest.spyOn(user, 'usingUnlimitedWithSourcingHiringPlan', 'get').mockReturnValue(true);
                    user.hiring_application.status = 'pending';

                    // But false if legacy
                    jest.spyOn(user, 'usingLegacyHiringPlan', 'get').mockReturnValue(true);
                    expect(user.hasHiringChoosePlanAccess).toBe(false);
                });
            });

            it('should be false if !isHiringTeamOwner', () => {
                jest.spyOn(user, 'isHiringTeamOwner', 'get').mockReturnValue(false);
                expect(user.hasHiringChoosePlanAccess).toBe(false);
            });
        });

        describe('hasHiringPositionsAccess', () => {
            it('should be true if you do not have a hiring plan', () => {
                jest.spyOn(user, 'hiringPlan', 'get').mockReturnValue(null);
                jest.spyOn(user, 'usingPayPerPostHiringPlan', 'get').mockReturnValue(false);
                jest.spyOn(user, 'hasSubscribedUnlimitedWithSourcingAccess', 'get').mockReturnValue(false);
                jest.spyOn(user, 'usingLegacyHiringPlan', 'get').mockReturnValue(false);
                expect(user.hasHiringPositionsAccess).toBe(true);
            });

            it('should be true if pay-per-post', () => {
                jest.spyOn(user, 'hiringPlan', 'get').mockReturnValue('foo');
                jest.spyOn(user, 'usingPayPerPostHiringPlan', 'get').mockReturnValue(true);
                jest.spyOn(user, 'hasSubscribedUnlimitedWithSourcingAccess', 'get').mockReturnValue(false);
                jest.spyOn(user, 'usingLegacyHiringPlan', 'get').mockReturnValue(false);
                expect(user.hasHiringPositionsAccess).toBe(true);
            });

            it('should be true if subscribed and unlimited-with-sourcing', () => {
                jest.spyOn(user, 'hiringPlan', 'get').mockReturnValue('foo');
                jest.spyOn(user, 'usingPayPerPostHiringPlan', 'get').mockReturnValue(false);
                jest.spyOn(user, 'hasSubscribedUnlimitedWithSourcingAccess', 'get').mockReturnValue(true);
                jest.spyOn(user, 'usingLegacyHiringPlan', 'get').mockReturnValue(false);
                expect(user.hasHiringTrackerAccess).toBe(true);
            });

            it('should be true if legacy', () => {
                jest.spyOn(user, 'hiringPlan', 'get').mockReturnValue('foo');
                jest.spyOn(user, 'usingPayPerPostHiringPlan', 'get').mockReturnValue(false);
                jest.spyOn(user, 'hasSubscribedUnlimitedWithSourcingAccess', 'get').mockReturnValue(false);
                jest.spyOn(user, 'usingLegacyHiringPlan', 'get').mockReturnValue(true);
                expect(user.hasHiringPositionsAccess).toBe(true);
            });

            it('should be false otherwise', () => {
                jest.spyOn(user, 'hiringPlan', 'get').mockReturnValue('foo');
                jest.spyOn(user, 'usingPayPerPostHiringPlan', 'get').mockReturnValue(false);
                jest.spyOn(user, 'hasSubscribedUnlimitedWithSourcingAccess', 'get').mockReturnValue(false);
                jest.spyOn(user, 'usingLegacyHiringPlan', 'get').mockReturnValue(false);
                expect(user.hasHiringPositionsAccess).toBe(false);
            });
        });

        describe('hasHiringTrackerAccess', () => {
            it('should be true if pay-per-post', () => {
                jest.spyOn(user, 'usingPayPerPostHiringPlan', 'get').mockReturnValue(true);
                jest.spyOn(user, 'hasSubscribedUnlimitedWithSourcingAccess', 'get').mockReturnValue(false);
                jest.spyOn(user, 'usingLegacyHiringPlan', 'get').mockReturnValue(false);
                expect(user.hasHiringPositionsAccess).toBe(true);
            });

            it('should be true if subscribed and unlimited-with-sourcing', () => {
                jest.spyOn(user, 'usingPayPerPostHiringPlan', 'get').mockReturnValue(false);
                jest.spyOn(user, 'hasSubscribedUnlimitedWithSourcingAccess', 'get').mockReturnValue(true);
                jest.spyOn(user, 'usingLegacyHiringPlan', 'get').mockReturnValue(false);
                expect(user.hasHiringTrackerAccess).toBe(true);
            });

            it('should be true if legacy', () => {
                jest.spyOn(user, 'usingPayPerPostHiringPlan', 'get').mockReturnValue(false);
                jest.spyOn(user, 'hasSubscribedUnlimitedWithSourcingAccess', 'get').mockReturnValue(false);
                jest.spyOn(user, 'usingLegacyHiringPlan', 'get').mockReturnValue(true);
                expect(user.hasHiringTrackerAccess).toBe(true);
            });

            it('should be false otherwise', () => {
                jest.spyOn(user, 'usingPayPerPostHiringPlan', 'get').mockReturnValue(false);
                jest.spyOn(user, 'hasSubscribedUnlimitedWithSourcingAccess', 'get').mockReturnValue(false);
                jest.spyOn(user, 'usingLegacyHiringPlan', 'get').mockReturnValue(false);
                expect(user.hasHiringTrackerAccess).toBe(false);
            });
        });

        describe('primaryHiringSubscription', () => {
            it('should return hiring team primary subscription', () => {
                const mockSubscription = {
                    foo: true,
                };
                jest.spyOn(user.hiring_team, 'primarySubscription', 'get').mockReturnValue(mockSubscription);
                expect(user.primaryHiringSubscription).toBe(mockSubscription);
            });
        });

        describe('onboardingUnlimitedWithSourcing', () => {
            it('should be true if unlimited and no subscription', () => {
                jest.spyOn(user, 'usingUnlimitedWithSourcingHiringPlan', 'get').mockReturnValue(true);
                jest.spyOn(user, 'hasSubscribedUnlimitedWithSourcingAccess', 'get').mockReturnValue(false);
                expect(user.onboardingUnlimitedWithSourcing).toBe(true);
            });

            it('should be false if not unlimited', () => {
                jest.spyOn(user, 'usingUnlimitedWithSourcingHiringPlan', 'get').mockReturnValue(false);
                jest.spyOn(user, 'hasSubscribedUnlimitedWithSourcingAccess', 'get').mockReturnValue(false);
                expect(user.onboardingUnlimitedWithSourcing).toBe(false);
            });

            it('should be false once subscribed', () => {
                jest.spyOn(user, 'usingUnlimitedWithSourcingHiringPlan', 'get').mockReturnValue(true);
                jest.spyOn(user, 'hasSubscribedUnlimitedWithSourcingAccess', 'get').mockReturnValue(true);
                expect(user.onboardingUnlimitedWithSourcing).toBe(false);
            });
        });

        describe('hasSubscribedUnlimitedWithSourcingAccess', () => {
            let mockSubscription;

            beforeEach(() => {
                mockSubscription = {
                    foo: true,
                };
            });

            it('should be true if unlimited and subscribed', () => {
                jest.spyOn(user, 'usingUnlimitedWithSourcingHiringPlan', 'get').mockReturnValue(true);
                jest.spyOn(user, 'primaryHiringSubscription', 'get').mockReturnValue(mockSubscription);
                expect(user.hasSubscribedUnlimitedWithSourcingAccess).toBe(true);
            });

            it('should be true if subscriptions are not required', () => {
                jest.spyOn(user, 'usingUnlimitedWithSourcingHiringPlan', 'get').mockReturnValue(true);
                jest.spyOn(user, 'primaryHiringSubscription', 'get').mockReturnValue(null);
                user.hiring_team.subscription_required = false;
                expect(user.hasSubscribedUnlimitedWithSourcingAccess).toBe(true);
            });

            it('should be false if not unlimited', () => {
                jest.spyOn(user, 'usingUnlimitedWithSourcingHiringPlan', 'get').mockReturnValue(false);
                jest.spyOn(user, 'primaryHiringSubscription', 'get').mockReturnValue(mockSubscription);
                expect(user.hasSubscribedUnlimitedWithSourcingAccess).toBe(false);
            });

            it('should be false if no subscription', () => {
                jest.spyOn(user, 'usingUnlimitedWithSourcingHiringPlan', 'get').mockReturnValue(true);
                jest.spyOn(user, 'primaryHiringSubscription', 'get').mockReturnValue(null);
                expect(user.hasSubscribedUnlimitedWithSourcingAccess).toBe(false);
            });
        });
    });

    describe('missingRequiredStudentNetworkEmail', () => {
        beforeEach(() => {
            user.email = 'anon@privaterelay.appleid.com';
            user.pref_student_network_privacy = 'full';
            jest.spyOn(user, 'hasStudentNetworkAccess', 'get').mockReturnValue(true);
        });

        it('should be true for network-accessible users with full access, an invalid email domain, and no existing student_network_email', () => {
            expect(user.missingRequiredStudentNetworkEmail).toBe(true);
        });

        it('should be false when the primary email is valid exist', () => {
            user.email = 'non-@valid-domain.com';
            expect(user.missingRequiredStudentNetworkEmail).toBe(false);
        });

        it('should be false when the profile is hidden or anonymous exist', () => {
            user.pref_student_network_privacy = 'hidden';
            expect(user.missingRequiredStudentNetworkEmail).toBe(false);
            user.pref_student_network_privacy = 'anonymous';
            expect(user.missingRequiredStudentNetworkEmail).toBe(false);
        });

        it('should be false when the user has no network access', () => {
            jest.spyOn(user, 'hasStudentNetworkAccess', 'get').mockReturnValue(false);
            expect(user.missingRequiredStudentNetworkEmail).toBe(false);
        });

        it('should be false when a student_network_email exist', () => {
            user.student_network_email = 'non-@valid-domain.com';
            expect(user.missingRequiredStudentNetworkEmail).toBe(false);
        });
    });

    describe('progress', () => {
        let frontRoyalStore;

        beforeEach(() => {
            frontRoyalStore = $injector.get('frontRoyalStore');
        });

        it('should initialize a UserProgressLoader using the store', () => {
            frontRoyalStore.enabled = true;
            const initialValue = user.progress;

            // it should initialize a UserProgressLoader that uses the store
            expect(initialValue.usesFrontRoyalStore).toBe(true);

            // it should return the same UserProgressLoader when referenced again
            expect(user.progress).toBe(initialValue);
        });

        it('should initialize a UserProgressLoader not using the store', () => {
            frontRoyalStore.enabled = false;
            const initialValue = user.progress;

            // it should initialize a UserProgressLoader that does not use the store
            expect(initialValue.usesFrontRoyalStore).toBe(false);

            // it should return the same UserProgressLoader when referenced again
            expect(user.progress).toBe(initialValue);
        });

        it('should initialize a new UserProgressLoader if frontRoyalStoreEnabled has changed', () => {
            frontRoyalStore.enabled = true;
            const value1 = user.progress;

            frontRoyalStore.enabled = false;
            const value2 = user.progress;
            expect(value2).not.toBe(value1);

            frontRoyalStore.enabled = true;
            const value3 = user.progress;
            expect(value3).not.toBe(value2);
        });

        it('should initialize a new UserProgressLoader if the current one has been destroyed', () => {
            const value1 = user.progress;
            user.progress.destroy();
            expect(user.progress).not.toBe(value1);
        });
    });

    function mockHasSignedEnrollmentAgreement(val) {
        cohortApplication = CohortApplication.fixtures.getInstance();
        jest.spyOn(cohortApplication, 'hasSignedEnrollmentAgreement', 'get').mockImplementation(
            () => hasSignedEnrollmentAgreement,
        );
        jest.spyOn(user, 'lastCohortApplication', 'get').mockReturnValue(cohortApplication);
        hasSignedEnrollmentAgreement = val;
    }
});
