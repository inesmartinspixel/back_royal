import 'AngularSpecHelper';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';
import 'Users/angularModule';
import 'Users/angularModule/spec/_mock/fixtures/users';

describe('UserIdVerification', () => {
    let User;
    let Cohort;
    let $injector;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Users', 'FrontRoyal.Lessons', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;

                User = $injector.get('User');
                Cohort = $injector.get('Cohort');
                $injector.get('UserFixtures');
                $injector.get('CohortFixtures');
            },
        ]);
    });

    describe('forVerificationPeriod', () => {
        it('should be false if wrong cohort', () => {
            const user = User.fixtures.getInstance({
                user_id_verifications: [
                    {
                        cohort_id: 1,
                        id_verification_period_index: 0,
                    },
                ],
            });
            const cohort = Cohort.fixtures.getInstance({
                id: 2,
                id_verification_periods: [{}],
            });
            expect(user.user_id_verifications[0].forVerificationPeriod(cohort.id_verification_periods[0])).toBe(false);
        });

        it('should be true only if index matches', () => {
            const user = User.fixtures.getInstance({
                user_id_verifications: [
                    {
                        cohort_id: 1,
                        id_verification_period_index: 1,
                    },
                ],
            });
            const cohort = Cohort.fixtures.getInstance({
                id: 1,
                id_verification_periods: [
                    {
                        start_date_days_offset: 10,
                    },
                    {
                        start_date_days_offset: 20,
                    },
                ],
            });
            expect(user.user_id_verifications[0].forVerificationPeriod(cohort.id_verification_periods[0])).toBe(false);

            expect(user.user_id_verifications[0].forVerificationPeriod(cohort.id_verification_periods[1])).toBe(true);
        });
    });
});
