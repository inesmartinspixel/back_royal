import angularModule from 'Users/angularModule/scripts/users_module';
import { clearProgressForUser } from 'StoredProgress';
import { EstimatedValue } from 'EstimatedValue';
import { isNonRelayEmail } from 'EmailInput';
import 'ExtensionMethods/array';
import moment from 'moment-timezone';

// iguana service wrapper class
// currently only opening up /api/users.json which returns info about the current user
angularModule.factory('User', [
    '$injector',
    $injector => {
        const Iguana = $injector.get('Iguana');
        const $timeout = $injector.get('$timeout');
        const CohortApplication = $injector.get('CohortApplication');
        const $q = $injector.get('$q');
        const $http = $injector.get('$http');
        const HiringApplication = $injector.get('HiringApplication');
        const CareerProfile = $injector.get('CareerProfile');
        const Cohort = $injector.get('Cohort');
        const EditCareerProfileHelper = $injector.get('EditCareerProfileHelper');
        const $window = $injector.get('$window');
        const UserProgressLoader = $injector.get('UserProgressLoader');
        const ClientStorage = $injector.get('ClientStorage');
        const UserIdVerification = $injector.get('UserIdVerification');
        const ErrorLogService = $injector.get('ErrorLogService');
        const frontRoyalStore = $injector.get('frontRoyalStore');
        const preSignupValues = $injector.get('preSignupValues');

        return Iguana.subclass(function () {
            this.setCollection('users');
            this.alias('User');
            this.setIdProperty('id');
            this.embedsOne('relevant_cohort', 'Cohort');
            this.embedsOne('hiring_application', 'HiringApplication');
            this.embedsOne('career_profile', 'CareerProfile');
            this.embedsOne('s3_identification_asset', 'S3IdentificationAsset');
            this.embedsOne('hiring_team', 'HiringTeam');
            this.embedsOne('active_institution', 'Institution');
            this.embedsOne('active_deferral_link', 'DeferralLink'); // used in admin
            this.embedsMany('cohort_applications', 'CohortApplication');
            this.embedsMany('s3_transcript_assets', 'S3TranscriptAsset');
            this.embedsMany('s3_english_language_proficiency_documents', 'S3EnglishLanguageProficiencyDocument');
            this.embedsMany('user_id_verifications', 'UserIdVerification');
            this.embedsMany('signable_documents', 'SignableDocument');
            this.embedsMany('project_progress', 'ProjectProgress'); // used in admin_gradebook
            this.embedsMany('subscriptions', 'Subscription');

            // Since the filters for index calls can get really long,
            // we use a post to avoid having get urls that go beyond browser
            // limits.
            this.overrideAction('index', {
                method: 'POST',
                path: 'index',
            });

            this.setCallback('after', 'copyAttrsOnInitialize', function () {
                this.s3_english_language_proficiency_documents = this.s3_english_language_proficiency_documents || [];
                this.s3_transcript_assets = this.s3_transcript_assets || []; // legacy
            });

            this.extend({
                maxEnglishLanguageProficiencyDocuments: 7,

                // FIXME: See discussion regarding user.programType at https://trello.com/c/QFVa6l5W
                programTypes: Cohort.programTypes.concat([
                    {
                        key: 'demo',
                        label: 'Demo',
                    },
                    {
                        key: 'external',
                        label: 'External',
                    },
                ]),

                ensureCareerProfile(user) {
                    let promise = $q.when(user.career_profile);

                    // If User does not have a CareerProfile then make one and save it
                    if (!user.career_profile) {
                        const careerProfile = CareerProfile.new({
                            user_id: user.id,
                        });

                        promise = careerProfile.save().then(response => {
                            user.career_profile = response.result;
                        });
                    }

                    return $q.when(promise);
                },
                mapClientSortToServerSort(clientSort) {
                    switch (clientSort) {
                        case 'careerProfileCreatedAt':
                            return 'career_profiles.created_at';
                        case 'careerProfileLocation':
                            return 'formatted_location.city_state';
                        case 'careerProfileLastCalculatedCompletePercentage':
                            return 'career_profiles.last_calculated_complete_percentage';
                        case 'careerProfileDisabled':
                            return 'career_profiles.do_not_create_relationships';
                        case 'careerProfileFeedback':
                            return 'career_profiles.profile_feedback';
                        case 'careerProfileStatus':
                            return 'career_profiles.interested_in_joining_new_company';
                        case 'careerProfileLastConfirmedAtByStudent':
                            return 'career_profiles.last_confirmed_at_by_student';
                        default:
                            return clientSort;
                    }
                },

                willNeedSubscription(user, cohort, cohortApplication) {
                    if (!user || !cohort || !cohortApplication) {
                        return undefined;
                    }

                    // We know we'll only make a subscription if:
                    //  - There isn't one already
                    //  - The user hasn't paid in full already
                    //  - The cohort actually supports it
                    //  - The user doesn't a full scholarship
                    return (
                        !user.primarySubscription &&
                        cohortApplication.total_num_required_stripe_payments !== 0 &&
                        cohort.supportsPayments &&
                        !cohortApplication.hasFullScholarship
                    );
                },
            });

            Object.defineProperty(this.prototype, 'inEfficacyStudy', {
                get() {
                    return _.some(this.institutionNames(), name => name.indexOf('EFFICACY') === 0);
                },
            });

            Object.defineProperty(this.prototype, 'hasLearnerAccess', {
                value: true,
            });

            /* ------------------ Hiring Manager Access -------------------------- */

            /*
                These properties are listed here in order of how much access they
                grant someone.
            */

            // This can be thought of to mean "is this person a hiring manager".  The only
            // reason I didn't call it `isHiringManager` is to fit in a tiny bit better with
            // an imaginary world where a user can switch back and forth between the learner experience
            // and the hiring manager experience.
            Object.defineProperty(this.prototype, 'defaultsToHiringExperience', {
                get() {
                    return !!this.hiring_application;
                },
                configurable: true,
            });

            // When a hiring manager has been rejected, ze can only go to the settings pages and the /sorry page
            Object.defineProperty(this.prototype, 'hasRejectedHiringManagerAccess', {
                get() {
                    if (this.hiring_application && this.hiring_application.status === 'rejected') {
                        return true;
                    }
                    return false;
                },
                configurable: true,
            });

            // When a hiring manager (not the owner) is on a disabled team, they can only go to the settings pages
            // and the /disabled page
            Object.defineProperty(this.prototype, 'hasDisabledHiringManagerAccess', {
                get() {
                    return this.hiring_team && this.hiring_team.disabledForNonOwners && !this.isHiringTeamOwner;
                },
                configurable: true,
            });

            // When a hiring manager is past due, ze can only go to the settings pages (which includes the billing page)
            Object.defineProperty(this.prototype, 'noHiringManagerAccessDueToPastDuePayment', {
                get() {
                    return this.hiring_team && this.hiring_team.noHiringManagerAccessDueToPastDuePayment;
                },
                configurable: true,
            });

            // If a hiring manager has a pending or accepted application, then ze can navigate to all of the
            // hiring manager pages, though things will be locked down somewhat for those who do not have
            // the next two levels of access
            Object.defineProperty(this.prototype, 'hasHiringManagerAccess', {
                get() {
                    return (
                        _.contains(
                            ['pending', 'accepted'],
                            this.hiring_application && this.hiring_application.status,
                        ) &&
                        !this.noHiringManagerAccessDueToPastDuePayment &&
                        !this.hasDisabledHiringManagerAccess
                    );
                },
                configurable: true,
            });

            // Once a hiring manager is accepted, some things unlock.
            // The check on noHiringManagerAccessDueToPastDuePayment was added in order to disable
            // the teammate invite button, connections, and browse-candidates.
            //
            // But then we had to also add hasAcceptedHiringManagerAccessPastDue in order to enable
            // the billing page for people who are past due.  Not ideal naming, but I don't really
            // have a better solution.
            Object.defineProperty(this.prototype, 'hasAcceptedHiringManagerAccess', {
                get() {
                    return (
                        this.hiring_application &&
                        this.hiring_application.status === 'accepted' &&
                        !this.noHiringManagerAccessDueToPastDuePayment
                    );
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'canInviteCoworkers', {
                get() {
                    return this.hasAcceptedHiringManagerAccess || this.usingPayPerPostHiringPlan;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'hasAcceptedHiringManagerAccessPastDue', {
                get() {
                    return (
                        this.hiring_application &&
                        this.hiring_application.status === 'accepted' &&
                        this.noHiringManagerAccessDueToPastDuePayment
                    );
                },
                configurable: true,
            });

            // Once a hiring manager has paid (or if subscription_required is false), then everything
            // unlocks
            Object.defineProperty(this.prototype, 'hasFullHiringManagerAccess', {
                get() {
                    return this.hasAcceptedHiringManagerAccess && this.hiring_team.has_full_access;
                },
                configurable: true,
            });

            // This property determines whether a user can go to the sourcing page and see something other than
            // the upsell.  It is true for all legacy users, but not all legacy users can actually load up career profiles.
            // Those with an application in the pending state can go to that page but cannot view profiles.  See currentUserCanBrowseProfiles
            // in hiring-browse-candidates
            Object.defineProperty(this.prototype, 'hasSourcingAccess', {
                get() {
                    return (
                        this.hasHiringManagerAccess &&
                        (this.usingLegacyHiringPlan || this.hasSubscribedUnlimitedWithSourcingAccess)
                    );
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'hasSourcingUpsellAccess', {
                get() {
                    const acceptedUnsubscribedUnlimitedWithSourcing =
                        this.usingUnlimitedWithSourcingHiringPlan &&
                        this.hiring_application.status === 'accepted' &&
                        !this.hasSubscribedUnlimitedWithSourcingAccess;
                    return (
                        this.hasHiringManagerAccess &&
                        !this.hasSourcingAccess &&
                        (acceptedUnsubscribedUnlimitedWithSourcing || this.usingPayPerPostHiringPlan)
                    );
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'hasHiringSourcingOrUpsellAccess', {
                get() {
                    return this.hasSourcingAccess || this.hasSourcingUpsellAccess;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'hasHiringChoosePlanAccess', {
                get() {
                    // If we're showing the "Your team may already be on smartly talent"
                    // message, then we want the user to be on the hiring plan page
                    if (!this.hiring_team && this.hiring_application) {
                        return true;
                    }

                    const pendingUnlimitedWithSourcing =
                        this.usingUnlimitedWithSourcingHiringPlan && this.hiring_application.status === 'pending';
                    return (
                        this.isHiringTeamOwner &&
                        this.hasHiringManagerAccess &&
                        (!this.hiringPlan || pendingUnlimitedWithSourcing) &&
                        !this.usingLegacyHiringPlan
                    );
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'hasHiringPositionsAccess', {
                get() {
                    // You have access if:
                    // 1) You don't have a hiring plan then you can go to positions and pay to post to become a
                    //  pay_per_post hiring team.
                    // 2) You have already become pay_per_post
                    // 3) You are subscribed as unlimited_w_sourcing
                    // 4) You were on a legacy plan
                    return (
                        this.hasHiringManagerAccess &&
                        (!this.hiringPlan ||
                            this.usingPayPerPostHiringPlan ||
                            this.hasSubscribedUnlimitedWithSourcingAccess ||
                            this.usingLegacyHiringPlan)
                    );
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'hasHiringTrackerAccess', {
                get() {
                    // You have access if:
                    // 1) You are on the pay_per_post plan
                    // 3) You are subscribed as unlimited_w_sourcing
                    // 4) You were on a legacy plan
                    return (
                        this.hasHiringManagerAccess &&
                        (this.usingPayPerPostHiringPlan ||
                            this.hasSubscribedUnlimitedWithSourcingAccess ||
                            this.usingLegacyHiringPlan)
                    );
                },
            });

            // FIXME: This property is not completely accurate anymore after pay-per-post plan was introduced
            Object.defineProperty(this.prototype, 'requiresHiringSubscription', {
                get() {
                    return (
                        this.defaultsToHiringExperience && (!this.hiring_team || this.hiring_team.subscription_required)
                    );
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'hiringPlan', {
                get() {
                    return this.hiring_team && this.hiring_team.hiring_plan;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'postingJobsRequiresPayment', {
                get() {
                    return this.usingPayPerPostHiringPlan || !this.hiringPlan;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'usingPayPerPostHiringPlan', {
                get() {
                    return this.hiring_team && this.hiring_team.usingPayPerPostHiringPlan;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'usingUnlimitedWithSourcingHiringPlan', {
                get() {
                    return this.hiring_team && this.hiring_team.usingUnlimitedWithSourcingHiringPlan;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'usingLegacyHiringPlan', {
                get() {
                    // For now, when someone has no hiring team at all, because they signed up with
                    // an existing domain, we treat them like a legacy user.  When we switch the default,
                    // we will want to switch this.  I made a note in hiring_team#maybe_ensure_legacy
                    return !!this.hiring_team && this.hiring_team.usingLegacyHiringPlan;
                },
                configurable: true,
            });

            // This is just an alias for hasAcceptedHiringManagerAccess to make it easier for us
            // to change our minds about who can create featured positions
            Object.defineProperty(this.prototype, 'canCreateFeaturedPositions', {
                get() {
                    if (this.usingLegacyHiringPlan) {
                        return this.hasAcceptedHiringManagerAccess;
                    }
                    return true;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'primarySubscription', {
                get() {
                    // See comments on server-side primary_subscription definition
                    // NOTE: `subscriptions` should always be defined if this user came
                    // down from an api call, but not necessarily if it was created with User.new()
                    return this.subscriptions && this.subscriptions[0];
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'primaryHiringSubscription', {
                get() {
                    return this.hiring_team && this.hiring_team.primarySubscription;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'ownsPrimaryHiringSubscription', {
                get() {
                    return this.isHiringTeamOwner && this.hiring_team.primarySubscription;
                },
                configurable: true,
            });

            // FIXME: I can't wait to stop doing properties like this one when we can use
            // the optional chaining operator, but we're blocked by beautify supporting it.
            // See https://trello.com/c/8YO8Pjcl
            Object.defineProperty(this.prototype, 'hiringSubscriptionRequired', {
                get() {
                    return this.hiring_team && this.hiring_team.subscription_required;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'hiringTeamSeemsToHavePaymentSource', {
                get() {
                    return this.hiring_team && this.hiring_team.seemsToHavePaymentSource;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'ownsHiringTeamThatSeemsToHavePaymentSource', {
                get() {
                    return this.isHiringTeamOwner && this.hiringTeamSeemsToHavePaymentSource;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'ownsHiringTeamThatSeemsToHaveValidPaymentSource', {
                get() {
                    return this.ownsHiringTeamThatSeemsToHavePaymentSource && !this.hiring_team.hasPaymentTrouble;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'ownsHiringTeamThatSeemsToHaveBadPaymentSource', {
                get() {
                    return this.ownsHiringTeamThatSeemsToHavePaymentSource && this.hiring_team.hasPaymentTrouble;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'onboardingUnlimitedWithSourcing', {
                get() {
                    return this.usingUnlimitedWithSourcingHiringPlan && !this.hasSubscribedUnlimitedWithSourcingAccess;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'hasSubscribedUnlimitedWithSourcingAccess', {
                get() {
                    return (
                        this.usingUnlimitedWithSourcingHiringPlan &&
                        (!!this.primaryHiringSubscription || !this.hiringSubscriptionRequired)
                    );
                },
                configurable: true,
            });

            /* ------------------ Complete Hiring Manager Access -------------------------- */

            Object.defineProperty(this.prototype, 'hasCareersNetworkAccess', {
                get() {
                    return this.can_edit_career_profile;
                },
                configurable: true, // for tests
            });

            Object.defineProperty(this.prototype, 'hasEditCareerProfileAccess', {
                get() {
                    // FIXME: Ultimately I think we should move to a world with no concept of
                    // programType on a user directly. If and when we do, this will need
                    // to look at the user's last application instead.
                    // See https://trello.com/c/QFVa6l5W
                    return (
                        (this.can_edit_career_profile || this.isAccepted || this.isDeferred || this.isPreAccepted) &&
                        Cohort.supportsEditingCareerProfile(this.programType)
                    );
                },
                configurable: true, // for tests
            });

            Object.defineProperty(this.prototype, 'hasCareersTabAccess', {
                get() {
                    return (
                        (this.canApplyToSmartly || this.can_edit_career_profile) &&
                        Cohort.supportsCareersTabAccess(this.programType)
                    );
                },
                configurable: true, // for tests
            });

            // Note: this maps to user.rb#has_student_network_access?
            Object.defineProperty(this.prototype, 'hasNetworkTabAccess', {
                get() {
                    return (
                        !this.hasExternalInstitution &&
                        (this.programType === 'demo' || Cohort.supportsStudentNetworkTabAccess(this.programType)) &&
                        !this.hiring_application
                    );
                },
                configurable: true, // for tests
            });

            // Note: this maps to user.rb#has_full_student_network_access?
            Object.defineProperty(this.prototype, 'hasStudentNetworkAccess', {
                get() {
                    if (this.hasAdminAccess) {
                        return true;
                    }
                    if (this.graduationStatus === 'failed') {
                        return false;
                    }
                    if (this.isAccepted && Cohort.supportsNetworkAccess(this.programType)) {
                        return true;
                    }

                    // the following logic duplicates what is in sql at ControllerMixins::StudentNetworkFiltersMixin#join_relevant_cohort_applications
                    // as well as what's in user.rb#has_application_that_provides_student_network_inclusion?
                    // basically, you need an accepted application or a deferred application that does not have a rejected or expelled application after it
                    const statusesAfterDeferred = [];
                    const lastDeferredApplication = _.find(this.cohort_applications, ca => {
                        statusesAfterDeferred.push(ca.status);
                        return ca.status === 'deferred';
                    });

                    if (!lastDeferredApplication) {
                        return false;
                    }
                    if (!Cohort.supportsNetworkAccess(lastDeferredApplication.program_type)) {
                        return false;
                    }
                    return _.difference(statusesAfterDeferred, ['deferred', 'pre_accepted']).length === 0;
                },
                configurable: true, // for tests
            });

            Object.defineProperty(this.prototype, 'inIsolatedNetworkCohort', {
                get() {
                    return this.relevant_cohort && !!this.relevant_cohort.isolated_network;
                },
            });

            Object.defineProperty(this.prototype, 'canDownloadTranscripts', {
                get() {
                    if (this.graduationStatus === 'failed') {
                        return false;
                    }
                    if (this.isAccepted && Cohort.supportsTranscriptDownload(this.programType)) {
                        return true;
                    }

                    // the following logic duplicates what is in sql at ControllerMixins::StudentNetworkFiltersMixin#join_relevant_cohort_applications
                    // as well as what's in user.rb#has_application_that_provides_student_network_inclusion?
                    // basically, you need an accepted application or a deferred application that does not have a rejected or expelled application after it
                    const statusesAfterDeferred = [];
                    const lastDeferredApplication = _.find(this.cohort_applications, ca => {
                        statusesAfterDeferred.push(ca.status);
                        return ca.status === 'deferred';
                    });

                    if (!lastDeferredApplication) {
                        return false;
                    }
                    if (!Cohort.supportsTranscriptDownload(lastDeferredApplication.program_type)) {
                        return false;
                    }
                    return _.difference(statusesAfterDeferred, ['deferred', 'pre_accepted']).length === 0;
                },
                configurable: true, // for tests
            });

            Object.defineProperty(this.prototype, 'hasEverApplied', {
                get() {
                    return !_.isEmpty(this.cohort_applications);
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'hasAppliedOrHasAccessToCareers', {
                get() {
                    return this.hasCareersNetworkAccess || this.hasEverApplied;
                },
                configurable: true, // for tests
            });

            Object.defineProperty(this.prototype, 'hasValidEmail', {
                get() {
                    return this.email && this.emailDomain !== 'example.com';
                },
            });

            Object.defineProperty(this.prototype, 'isEmailValidForStudentNetwork', {
                get() {
                    return !isNonRelayEmail(this.email);
                },
                configurable: true, // for tests
            });

            Object.defineProperty(this.prototype, 'missingRequiredStudentNetworkEmail', {
                get() {
                    return !!(
                        this.pref_student_network_privacy === 'full' &&
                        !this.student_network_email &&
                        !this.isEmailValidForStudentNetwork &&
                        this.hasStudentNetworkAccess
                    );
                },
                configurable: true, // for tests
            });

            Object.defineProperty(this.prototype, 'defaultCard', {
                get() {
                    return this.$$defaultCard;
                },
                set(val) {
                    this.$$defaultCard = val;
                    return val;
                },
            });

            Object.defineProperty(this.prototype, 'freeTrialStarted', {
                get() {
                    return !!this.free_trial_started;
                },
            });

            Object.defineProperty(this.prototype, 'canViewLanguageSwitcher', {
                get() {
                    return this.hasSuperEditorAccess;
                },
            });

            Object.defineProperty(this.prototype, 'canLaunchExamOutsideOfLimits', {
                get() {
                    return this.hasAdminAccess;
                },
            });

            Object.defineProperty(this.prototype, 'canApplyToSmartly', {
                get() {
                    // Demo users are assigned the current mba cohort as their relevant cohort,
                    // but the cannot apply to smartly
                    return (
                        !!this.relevant_cohort &&
                        this.career_profile &&
                        Cohort.supportsApplyingToSmartly(this.programType)
                    );
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'canEditCurrentCohortApplication', {
                get() {
                    return (
                        !!this.lastCohortApplication &&
                        !_.contains(['accepted', 'pre_accepted'], this.lastCohortApplication.status) &&
                        !!this.relevant_cohort &&
                        !this.relevant_cohort.admissionRoundDeadlineHasPassed(this.lastCohortApplication.appliedAt)
                    );
                },
                configurable: true,
            });
            Object.defineProperty(this.prototype, 'canResetUsersExamTimer', {
                get() {
                    return this.hasAdminAccess;
                },
            });

            Object.defineProperty(this.prototype, 'onboardingComplete', {
                get() {
                    // anyone that can apply to smartly has onboarding; the rest skip it
                    if (this.skip_apply || !this.canApplyToSmartly) {
                        return true;
                    }

                    // Any user who has not submitted at least one application has incomplete onboarding
                    return !!this.lastCohortApplication;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'accountId', {
                get() {
                    if (this.provider === 'phone_no_password') {
                        return this.phone;
                    }
                    if (this.email) {
                        return this.email;
                    }
                    return this.id;
                },
            });

            Object.defineProperty(this.prototype, 'hasAdminAccess', {
                get() {
                    return this.roleName() === 'admin';
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'hasReportsAccess', {
                get() {
                    return this.hasSuperEditorAccess || this.is_institutional_reports_viewer;
                },
            });

            Object.defineProperty(this.prototype, 'hasSuperReportsAccess', {
                get() {
                    return this.hasSuperEditorAccess;
                },
            });

            Object.defineProperty(this.prototype, 'hasSuperEditorAccess', {
                get() {
                    return this.roleName() === 'admin' || this.roleName() === 'super_editor';
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'hasEditorAccess', {
                get() {
                    return (
                        this.roleName() === 'admin' ||
                        this.roleName() === 'editor' ||
                        this.roleName() === 'super_editor'
                    );
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'hasInterviewerAccess', {
                get() {
                    return this.roleName() === 'admin' || this.roleName() === 'interviewer';
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'hasSuperEditorOrInterviewerAccess', {
                get() {
                    return (
                        this.roleName() === 'admin' ||
                        this.roleName() === 'super_editor' ||
                        this.roleName() === 'interviewer'
                    );
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'isHiringTeamOwner', {
                get() {
                    return this.hiring_team && this.hiring_team.owner_id === this.id;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'canShare', {
                get() {
                    return !this.hasExternalInstitution && !this.beta;
                },
            });

            Object.defineProperty(this.prototype, 'loggedInWithSAML', {
                get() {
                    return this.hasExternalInstitution && this.inInstitution('JLL'); // TODO: should pass down data from rails, not hard-coded
                },
                configurable: true, // for tests
            });

            Object.defineProperty(this.prototype, 'hasExternalInstitution', {
                get() {
                    return this.institutions?.some(i => i.external);
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'shouldSeeQuanticBranding', {
                get() {
                    // hiring managers don't have an active_institution
                    return this.active_institution?.branding === 'quantic';
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'ghostMode', {
                get() {
                    return this.$$ghostMode;
                },
                set(val) {
                    this.$$ghostMode = val;
                },
            });

            Object.defineProperty(this.prototype, 'hideAllLessons', {
                get() {
                    return this.$$hideAllLessons || false;
                },
                set(val) {
                    this.$$hideAllLessons = val;
                },
            });

            Object.defineProperty(this.prototype, 'globalRole', {
                get() {
                    let existingGlobalRole;
                    if (this.roles) {
                        this.roles.forEach(role => {
                            if (role && role.resource_id === null && role.name !== 'cannot_create') {
                                existingGlobalRole = role;
                            }
                        });
                    }
                    return existingGlobalRole;
                },
                set(role) {
                    // remove any other global roles
                    let i = 0;
                    this.roles.forEach(_role => {
                        if (_role.resource_id === null && _role.name !== 'cannot_create') {
                            this.roles.splice(i, 1);
                        }
                        i += 1;
                    });
                    // add this global role
                    this.roles.push(role);
                },
            });

            Object.defineProperty(this.prototype, 'blueOcean', {
                get() {
                    const allGroups = this.groups.concat(this.groupsFromInstitutions);
                    return allGroups.length === 1 && allGroups[0].name === 'BLUEOCEAN';
                },
            });

            Object.defineProperty(this.prototype, 'beta', {
                get() {
                    const allGroups = this.groups.concat(this.groupsFromInstitutions);
                    return allGroups.length === 1 && (allGroups[0].name === 'BETA' || allGroups[0].name === 'BETA2');
                },
            });

            Object.defineProperty(this.prototype, 'lessonPermissions', {
                get() {
                    if (!this.$$lessonPermissions) {
                        // find any scoped roles for each lesson, return a hash keyed by lesson to populate
                        // into user admin page radio button selector per lesson
                        this.$$lessonPermissions = {};
                        this.roles.forEach(role => {
                            if (role && role.resource_id !== null && this.isScopedlessonPermission(role.name)) {
                                this.$$lessonPermissions[role.resource_id] = role.name;
                            }
                        });
                        return this.$$lessonPermissions;
                    }
                    return this.$$lessonPermissions;
                },
            });

            // global role: can create lessons
            Object.defineProperty(this.prototype, 'canCreateLessons', {
                get() {
                    let canCreate = true;
                    this.roles.forEach(role => {
                        // note: we only store a special role if access is not allowed (to save space)
                        if (role && role.resource_id === null && role.name === 'cannot_create') {
                            canCreate = false;
                        }
                    });
                    return canCreate;
                },
                set(canCreate) {
                    let i = 0;
                    let existingOverride = false;
                    this.roles.forEach(role => {
                        if (role && role.resource_id === null && role.name === 'cannot_create') {
                            existingOverride = true;
                            if (canCreate) {
                                // delete the cannot_create global override role if the user just re-enabled canCreate
                                this.roles.splice(i, 1);
                            }
                        }
                        i += 1;
                    });
                    // if the user is disabling can_create and the associated global override doesnt exist, add it
                    if (!canCreate && !existingOverride) {
                        // todo: iguana class role.new instead of hash?
                        const newRole = {
                            name: 'cannot_create',
                            resource_id: null,
                            resource_type: null,
                        };
                        this.roles.push(newRole);
                    }
                },
            });

            Object.defineProperty(this.prototype, 'groupsFromInstitutions', {
                get() {
                    const groupsSet = {};
                    (this.institutions || []).forEach(institution => {
                        (institution.groups || []).forEach(group => {
                            groupsSet[group.id] = group;
                        });
                    });
                    return Object.values(groupsSet) || [];
                },
            });

            Object.defineProperty(this.prototype, 'emailDomain', {
                get() {
                    return this.email ? this.email.split('@')[1] : undefined;
                },
            });

            Object.defineProperty(this.prototype, 'hasPendingCohort', {
                get() {
                    return !!this.pendingCohortApplication;
                },
            });

            Object.defineProperty(this.prototype, 'hasAcceptedCohort', {
                get() {
                    return !!this.acceptedCohortApplication;
                },
            });

            Object.defineProperty(this.prototype, 'isEMBA', {
                get() {
                    return this.programType === 'emba';
                },
                configurable: true, // for specs
            });

            Object.defineProperty(this.prototype, 'isMBA', {
                get() {
                    return this.programType === 'mba';
                },
                configurable: true, // for specs
            });

            Object.defineProperty(this.prototype, 'isCareerNetworkOnly', {
                get() {
                    return this.programType === 'career_network_only';
                },
                configurable: true, // specs
            });

            Object.defineProperty(this.prototype, 'isDemo', {
                get() {
                    return this.programType === 'demo';
                },
                configurable: true, // specs
            });

            Object.defineProperty(this.prototype, 'hasPendingMBACohortApplication', {
                get() {
                    const cohort_application = this.pendingCohortApplication;
                    return cohort_application && cohort_application.program_type === 'mba';
                },
            });

            Object.defineProperty(this.prototype, 'hasPendingEMBACohortApplication', {
                get() {
                    const cohort_application = this.pendingCohortApplication;
                    return cohort_application && cohort_application.program_type === 'emba';
                },
            });

            Object.defineProperty(this.prototype, 'hasPendingCareerNetworkOnlyCohortApplication', {
                get() {
                    const cohort_application = this.pendingCohortApplication;
                    return cohort_application && cohort_application.program_type === 'career_network_only';
                },
            });

            Object.defineProperty(this.prototype, 'hasPendingCohortApplication', {
                get() {
                    return !!_.findWhere(this.cohort_applications, {
                        status: 'pending',
                    });
                },
                configurable: true, // for specs
            });

            Object.defineProperty(this.prototype, 'hasPreAcceptedCohortApplication', {
                get() {
                    return !!_.findWhere(this.cohort_applications, {
                        status: 'pre_accepted',
                    });
                },
            });

            Object.defineProperty(this.prototype, 'hasPendingOrPreAcceptedCohortApplication', {
                get() {
                    return !!this.pendingOrPreAcceptedCohortApplication;
                },
                configurable: true, // specs
            });

            Object.defineProperty(this.prototype, 'pendingOrPreAcceptedCohortApplication', {
                get() {
                    return _.find(
                        this.cohort_applications,
                        cohort_application =>
                            cohort_application.status === 'pending' || cohort_application.status === 'pre_accepted',
                    );
                },
            });

            Object.defineProperty(this.prototype, 'hasPendingAcceptedOrPreAcceptedCohortApplication', {
                get() {
                    return !!this.pendingAcceptedOrPreAcceptedCohortApplication;
                },
            });

            Object.defineProperty(this.prototype, 'pendingAcceptedOrPreAcceptedCohortApplication', {
                get() {
                    return _.find(
                        this.cohort_applications,
                        cohort_application =>
                            cohort_application.status === 'pending' ||
                            cohort_application.status === 'accepted' ||
                            cohort_application.status === 'pre_accepted',
                    );
                },
            });

            Object.defineProperty(this.prototype, 'acceptedOrPreAcceptedCohortApplication', {
                get() {
                    return _.find(
                        this.cohort_applications,
                        cohort_application =>
                            cohort_application.status === 'accepted' || cohort_application.status === 'pre_accepted',
                    );
                },
                configurable: true, // for specs
            });

            Object.defineProperty(this.prototype, 'hasAdditionalDetails', {
                get() {
                    return (
                        this.school || this.professional_organization || this.job_title || this.phone || this.country
                    );
                },
            });

            Object.defineProperty(this.prototype, 'hasSeenAccepted', {
                get() {
                    return this.has_seen_accepted;
                },
                set(val) {
                    this.has_seen_accepted = val;
                },
            });

            Object.defineProperty(this.prototype, 'hasSeenFeaturedPositionsPage', {
                get() {
                    return this.has_seen_featured_positions_page;
                },
                set(val) {
                    this.has_seen_featured_positions_page = val;
                },
            });

            Object.defineProperty(this.prototype, 'acceptedCohortApplication', {
                get() {
                    // The DB currently enforces only one accepted or pending
                    return _.findWhere(this.cohort_applications, {
                        status: 'accepted',
                    });
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'preAcceptedCohortApplication', {
                get() {
                    // The DB currently enforces only one accepted or pending
                    return _.findWhere(this.cohort_applications, {
                        status: 'pre_accepted',
                    });
                },
            });

            Object.defineProperty(this.prototype, 'lockedDueToPastDuePayment', {
                get() {
                    return !!(
                        this.acceptedCohortApplication && this.acceptedCohortApplication.locked_due_to_past_due_payment
                    );
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'acceptedCohortName', {
                get() {
                    return this.acceptedCohortApplication && this.acceptedCohortApplication.cohort_name;
                },
            });

            Object.defineProperty(this.prototype, 'careerProfileEmploymentTypesOfInterest', {
                get() {
                    return this.career_profile && this.career_profile.employment_types_of_interest;
                },
            });

            Object.defineProperty(this.prototype, 'careerProfileInterestedInEmploymentPermanent', {
                get() {
                    return this.career_profile && this.career_profile.interestedInEmploymentType('permanent');
                },
            });

            Object.defineProperty(this.prototype, 'careerProfileInterestedInEmploymentPartTime', {
                get() {
                    return this.career_profile && this.career_profile.interestedInEmploymentType('part_time');
                },
            });

            Object.defineProperty(this.prototype, 'careerProfileInterestedInEmploymentContract', {
                get() {
                    return this.career_profile && this.career_profile.interestedInEmploymentType('contract');
                },
            });

            Object.defineProperty(this.prototype, 'careerProfileInterestedInEmploymentInternship', {
                get() {
                    return this.career_profile && this.career_profile.interestedInEmploymentType('internship');
                },
            });

            Object.defineProperty(this.prototype, 'careerProfileLocation', {
                get() {
                    return this.career_profile && this.career_profile.locationString;
                },
            });

            Object.defineProperty(this.prototype, 'careerProfilePrimaryAreasOfInterest', {
                get() {
                    return this.career_profile && this.career_profile.primary_areas_of_interest;
                },
            });

            Object.defineProperty(this.prototype, 'careerProfileCreatedAt', {
                get() {
                    return this.career_profile && this.career_profile.created_at;
                },
            });

            Object.defineProperty(this.prototype, 'careerProfileUpdatedAt', {
                get() {
                    return this.career_profile && this.career_profile.updated_at;
                },
            });

            Object.defineProperty(this.prototype, 'careerProfileLastConfirmedAtByStudent', {
                get() {
                    return this.career_profile && this.career_profile.last_confirmed_at_by_student;
                },
            });

            Object.defineProperty(this.prototype, 'careerProfileLastCalculatedCompletePercentage', {
                get() {
                    return this.career_profile && this.career_profile.last_calculated_complete_percentage;
                },
            });

            Object.defineProperty(this.prototype, 'careerProfileComplete', {
                get() {
                    return this.careerProfileLastCalculatedCompletePercentage === 100;
                },
            });

            Object.defineProperty(this.prototype, 'careerProfileDisabled', {
                get() {
                    return this.career_profile && this.career_profile.do_not_create_relationships;
                },
            });

            Object.defineProperty(this.prototype, 'careerProfileFeedback', {
                get() {
                    return this.career_profile && this.career_profile.profile_feedback;
                },
            });

            Object.defineProperty(this.prototype, 'careerProfileFeedbackLastSentAt', {
                get() {
                    return this.career_profile && this.career_profile.feedback_last_sent_at;
                },
            });

            Object.defineProperty(this.prototype, 'careerProfileStatus', {
                get() {
                    return this.career_profile && this.career_profile.interested_in_joining_new_company;
                },
            });

            Object.defineProperty(this.prototype, 'positionInterestCandidateStatus', {
                get() {
                    return this.position_interest && this.position_interest.candidate_status;
                },
            });

            Object.defineProperty(this.prototype, 'positionInterestHiringManagerStatus', {
                get() {
                    return this.position_interest && this.position_interest.hiring_manager_status;
                },
            });

            Object.defineProperty(this.prototype, 'positionInterestCreatedAt', {
                get() {
                    return this.position_interest && this.position_interest.created_at;
                },
            });

            Object.defineProperty(this.prototype, 'newlyAccepted', {
                get() {
                    // TODO: We could have this only show the message once the target start_date
                    // nears, but we also might not want to bake that into client logic and backfill instead.
                    return this.acceptedCohortApplication && !this.hasSeenAccepted;
                },
            });

            Object.defineProperty(this.prototype, 'cohortApplicationsStatusesString', {
                get() {
                    return _.chain(this.cohort_applications || [])
                        .sortBy('applied_at')
                        .map(application => application.statusString)
                        .value()
                        .join(', ');
                },
            });

            Object.defineProperty(this.prototype, 'cohortApplicationsByCohortId', {
                get() {
                    if (!this.$$cohortApplicationsByCohortId) {
                        this.$$cohortApplicationsByCohortId = _.indexBy(this.cohort_applications, 'cohort_id');
                    }
                    return this.$$cohortApplicationsByCohortId;
                },
            });

            Object.defineProperty(this.prototype, 'lastCohortApplication', {
                get() {
                    return _.chain(this.cohort_applications).sortBy('applied_at').last().value();
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'lastCohortApplicationStatus', {
                get() {
                    const lastCohortApplication = this.lastCohortApplication;
                    return lastCohortApplication && lastCohortApplication.status;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'lastCohortName', {
                get() {
                    const lastCohortApplication = this.lastCohortApplication;
                    return lastCohortApplication && lastCohortApplication.cohort_name;
                },
            });

            Object.defineProperty(this.prototype, 'isPending', {
                get() {
                    return this.lastCohortApplicationStatus === 'pending';
                },
                configurable: true, // specs
            });

            Object.defineProperty(this.prototype, 'isAccepted', {
                get() {
                    return !!this.acceptedCohortApplication;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'isPreAccepted', {
                get() {
                    if (this.acceptedCohortApplication) {
                        return false;
                    }
                    return this.lastCohortApplicationStatus === 'pre_accepted';
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'isDeferred', {
                get() {
                    if (this.acceptedCohortApplication) {
                        return false;
                    }
                    return this.lastCohortApplicationStatus === 'deferred';
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'isDeferredFromMbaOrEmba', {
                get() {
                    return (
                        this.isDeferred &&
                        this.lastCohortApplication &&
                        ['mba', 'emba'].includes(this.lastCohortApplication.program_type)
                    );
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'isRejected', {
                get() {
                    if (this.acceptedCohortApplication) {
                        return false;
                    }
                    return this.lastCohortApplicationStatus === 'rejected';
                },
                configurable: true, // for tests
            });

            Object.defineProperty(this.prototype, 'isRejectedForPromotedCohort', {
                get() {
                    return (
                        this.isRejected &&
                        this.lastCohortApplication &&
                        this.relevant_cohort &&
                        this.lastCohortApplication.cohort_id === this.relevant_cohort.id
                    );
                },
            });

            Object.defineProperty(this.prototype, 'isExpelledForPromotedCohort', {
                get() {
                    return (
                        this.isExpelled &&
                        this.lastCohortApplication &&
                        this.relevant_cohort &&
                        this.lastCohortApplication.cohort_id === this.relevant_cohort.id
                    );
                },
            });

            Object.defineProperty(this.prototype, 'isExpelled', {
                get() {
                    if (this.acceptedCohortApplication) {
                        return false;
                    }
                    return this.lastCohortApplicationStatus === 'expelled';
                },
                configurable: true, // for tests
            });

            Object.defineProperty(this.prototype, 'isFailed', {
                get() {
                    return this.lastCohortApplication?.graduation_status === 'failed';
                },
                configurable: true,
            });

            function isWithinStatusRange(range, context) {
                if (context.acceptedCohortApplication) {
                    return false;
                }
                // a little bit faster than `context.isRejected || context.isExpelled`
                return _.contains(range, context.lastCohortApplicationStatus);
            }

            Object.defineProperty(this.prototype, 'hasReApplied', {
                get() {
                    const apps = _.sortBy(this.cohort_applications, 'applied_at').reverse();
                    return apps && apps.length > 1 && apps[0].status === 'pending' && apps[1].status === 'rejected';
                },
            });

            Object.defineProperty(this.prototype, 'previousApplicationToRelevantCohort', {
                get() {
                    return (
                        this.relevant_cohort &&
                        this.cohort_applications &&
                        _.findWhere(this.cohort_applications, {
                            cohort_id: this.relevant_cohort.id,
                        })
                    );
                },
            });

            Object.defineProperty(this.prototype, 'needsToProvideCohortBillingInfo', {
                get() {
                    const cohortApplication = this.lastCohortApplication;
                    if (!cohortApplication) {
                        return false;
                    }

                    return (
                        !cohortApplication.in_good_standing &&
                        cohortApplication.total_num_required_stripe_payments &&
                        (cohortApplication.status === 'pre_accepted' || cohortApplication.status === 'accepted')
                    );
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'needsToRegister', {
                get() {
                    const cohortApplication = this.lastCohortApplication;
                    return (
                        this.relevant_cohort &&
                        this.relevant_cohort.supportsPayments &&
                        cohortApplication &&
                        !cohortApplication.registered &&
                        (cohortApplication.status === 'pre_accepted' || cohortApplication.status === 'accepted') &&
                        !this.isFailed
                    );
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'hasAppliedToRelevantCohort', {
                get() {
                    return !!this.previousApplicationToRelevantCohort;
                },
            });

            Object.defineProperty(this.prototype, 'isRejectedOrExpelled', {
                get() {
                    return isWithinStatusRange(['rejected', 'expelled'], this);
                },
                configurable: true, // specs
            });

            Object.defineProperty(this.prototype, 'isRejectedOrExpelledOrDeferred', {
                get() {
                    return isWithinStatusRange(['rejected', 'expelled', 'deferred'], this);
                },
            });

            Object.defineProperty(this.prototype, 'isRejectedOrExpelledOrDeferredOrFailed', {
                get() {
                    return (
                        isWithinStatusRange(['rejected', 'expelled', 'deferred'], this) ||
                        (this.lastCohortApplication && this.graduationStatus === 'failed')
                    );
                },
            });

            Object.defineProperty(this.prototype, 'graduationStatus', {
                get() {
                    return this.acceptedCohortApplication ? this.acceptedCohortApplication.graduation_status : null;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'isRejectedOrExpelledOrDeferredOrReApplied', {
                get() {
                    return this.isRejectedOrExpelledOrDeferred || this.hasReApplied;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'hasEverBeenRejected', {
                get() {
                    return !!_.find(
                        this.cohort_applications,
                        cohortApplication => cohortApplication.status === 'rejected',
                    );
                },
                configurable: true, // specs
            });

            Object.defineProperty(this.prototype, 'pendingCohortApplication', {
                // The DB currently enforces only one accepted or pending
                get() {
                    return _.findWhere(this.cohort_applications, {
                        status: 'pending',
                    });
                },
            });

            // FIXME: user.program_type and user.fallback_program_type are likely to be removed at some point
            // See discussion at https://trello.com/c/QFVa6l5W
            //
            // `programType` is a little strange because it will change immediately when an option
            // is selected on the program-choice-form even if it has not yet been saved.  See
            // https://bitbucket.org/pedago-ondemand/back_royal/pull-requests/3657 for an explanation
            // as to why.
            //
            // This is important because of
            // 1. application complete calculation (see PR linked above)
            // 2. help-scout-beacon needs to immediately respond to changes in the programType
            // 3. Maybe other things rely on this too?
            Object.defineProperty(this.prototype, 'programType', {
                // The DB currently enforces only one accepted or pending
                get() {
                    // This used to be "institutional" until we moved to a concept of all
                    // users, even normal learners, having an institution. While making that
                    // transition, the existing meaning of having an institution was replaced
                    // by having an "external" institution.
                    // See https://trello.com/c/QFVa6l5W
                    if (this.hasExternalInstitution) {
                        return 'external';
                    }
                    if (this.$$pendingProgramTypeSelected) {
                        return this.$$pendingProgramTypeSelected;
                    }
                    if (this.pendingAcceptedOrPreAcceptedCohortApplication) {
                        return this.pendingAcceptedOrPreAcceptedCohortApplication.program_type;
                    }
                    return this.fallback_program_type;
                },
                configurable: true, // for specs
            });

            Object.defineProperty(this.prototype, 'hiringApplicationStatus', {
                get() {
                    return this.hiring_application ? this.hiring_application.status : null;
                },
                set(status) {
                    if (!status || status === '__NONE__') {
                        this.hiring_application = null;
                        return;
                    }

                    if (!this.hiring_application) {
                        this.hiring_application = HiringApplication.new({
                            applied_at: new Date().getTime() / 1000,
                        });
                    }

                    this.hiring_application.status = status;
                },
            });

            Object.defineProperty(this.prototype, 'hasActiveCareerProfile', {
                get() {
                    return (
                        this.career_profile &&
                        this.can_edit_career_profile &&
                        this.career_profile.last_calculated_complete_percentage === 100 &&
                        _.contains(
                            CareerProfile.ACTIVE_INTEREST_LEVELS,
                            this.career_profile.interested_in_joining_new_company,
                        )
                    );
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'hasCompleteCareerProfile', {
                get() {
                    return this.career_profile && this.career_profile.complete;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'hasCompleteHiringApplication', {
                get() {
                    return this.hiring_application && this.hiring_application.complete;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'testCompleteMessageKey', {
                get() {
                    // see also: `lesson_finish_screen-*.json`
                    return this.relevant_cohort && this.relevant_cohort.supportsAutograding
                        ? 'your_test_score_auto'
                        : null;
                },
            });

            Object.defineProperty(this.prototype, 'referralUrl', {
                get() {
                    return {
                        full: `${$window.ENDPOINT_ROOT}/cn/1/referral?by=${this.id}`,
                        partial: `cn/1/referral?by=${this.id}`,
                    };
                },
            });

            Object.defineProperty(this.prototype, 'preferredName', {
                get() {
                    return this.nickname || this.name;
                },
            });

            Object.defineProperty(this.prototype, 'mailingAddress', {
                get() {
                    let address = this.address_line_1;
                    address += this.address_line_2 ? `<br>${this.address_line_2}` : '';
                    address += `<br>${this.city}`;
                    address += this.state ? `, ${this.state}` : `, ${this.country}`;
                    address += this.zip ? ` ${this.zip}` : '';

                    return address;
                },
            });

            Object.defineProperty(this.prototype, 'careerProfileIndicatesUserShouldProvideTranscripts', {
                get() {
                    return !!(this.career_profile && this.career_profile.indicatesUserShouldProvideTranscripts);
                },
                configurable: true,
            });

            Object.defineProperty(
                this.prototype,
                'careerProfileIndicatesUserShouldUploadEnglishLanguageProficiencyDocuments',
                {
                    get() {
                        return !!(
                            this.career_profile &&
                            this.career_profile.indicatesUserShouldUploadEnglishLanguageProficiencyDocuments
                        );
                    },
                    configurable: true,
                },
            );

            Object.defineProperty(this.prototype, 'lastCohortApplicationIndicatesUserShouldProvideTranscripts', {
                get() {
                    return !!(
                        this.lastCohortApplication && this.lastCohortApplication.indicatesUserShouldProvideTranscripts
                    );
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'hasUploadedTranscripts', {
                get() {
                    return this.career_profile?.hasUploadedTranscripts;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'requiredTranscriptsUploadedColumnString', {
                get() {
                    if (!this.career_profile) {
                        return '';
                    }

                    const numUploaded = this.career_profile.numRequiredTranscriptsUploaded;
                    const numNotWaived = this.career_profile.numRequiredTranscriptsNotWaived;

                    if (numNotWaived > 0) {
                        return `${numUploaded}/${numNotWaived}`;
                    }
                    if (numNotWaived === 0) {
                        return '—';
                    }
                    return '';
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'requiredTranscriptsApprovedColumnString', {
                get() {
                    if (!this.career_profile) {
                        return '';
                    }

                    const numApproved = this.career_profile.numRequiredTranscriptsApproved;
                    const numNotWaived = this.career_profile.numRequiredTranscriptsNotWaived;

                    if (numNotWaived > 0) {
                        return `${numApproved}/${numNotWaived}`;
                    }
                    if (numNotWaived === 0) {
                        return '—';
                    }
                    return '';
                },
                configurable: true,
            });

            // Transcripts are now associated with an education_experience, not the user itself
            // See https://trello.com/c/ErENgFKR
            Object.defineProperty(this.prototype, 'legacyTranscripts', {
                get() {
                    return this.s3_transcript_assets || [];
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'hasLegacyTranscripts', {
                get() {
                    return this.legacyTranscripts.length > 0;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'hasUploadedEnglishLanguageProficiencyDocuments', {
                get() {
                    return _.any(this.s3_english_language_proficiency_documents);
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'hasUploadedIdentification', {
                get() {
                    return !!this.s3_identification_asset;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'identifiedForEnrollment', {
                get() {
                    return !!(
                        this.relevant_cohort &&
                        ((this.relevant_cohort.requiresIdUpload && this.identity_verified) ||
                            _.any(this.user_id_verifications))
                    );
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'missingAddress', {
                get() {
                    return !this.address_line_1 || !this.city || !this.state || !this.country;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'recordsIndicateUserShouldUploadIdentificationDocument', {
                get() {
                    return !!(
                        this.relevant_cohort &&
                        this.relevant_cohort.requiresIdUpload &&
                        this.lastCohortApplication &&
                        this.lastCohortApplication.indicatesUserShouldUploadIdentificationDocument
                    );
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'recordsIndicateUserShouldProvideTranscripts', {
                get() {
                    return !!(
                        this.careerProfileIndicatesUserShouldProvideTranscripts &&
                        this.lastCohortApplicationIndicatesUserShouldProvideTranscripts
                    );
                },
                configurable: true,
            });

            Object.defineProperty(
                this.prototype,
                'recordsIndicateUserShouldUploadEnglishLanguageProficiencyDocuments',
                {
                    get() {
                        return !!(
                            this.career_profile &&
                            this.career_profile.indicatesUserShouldUploadEnglishLanguageProficiencyDocuments &&
                            this.lastCohortApplication &&
                            this.lastCohortApplication.indicatesUserShouldUploadEnglishLanguageProficiencyDocuments
                        );
                    },
                    configurable: true,
                },
            );

            Object.defineProperty(this.prototype, 'missingIdentificationDocument', {
                get() {
                    return !!(
                        !this.identity_verified &&
                        this.recordsIndicateUserShouldUploadIdentificationDocument &&
                        !this.s3_identification_asset
                    );
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'missingUserIdVerification', {
                get() {
                    return this.relevant_cohort && this.relevant_cohort.hasVerificationPeriods
                        ? !_.every(
                              this.relevant_cohort.previousAndCurrentlyActiveIdVerificationPeriods,
                              period => !!this.userIdVerificationForPeriod(period),
                          )
                        : false;
                },
                configurable: true,
            });

            // See also user.rb#missing_transcripts?
            Object.defineProperty(this.prototype, 'missingTranscripts', {
                get() {
                    const careerProfileMissingTranscripts =
                        this.career_profile && this.career_profile.missingTranscripts;
                    return !!(
                        !this.transcripts_verified &&
                        this.recordsIndicateUserShouldProvideTranscripts &&
                        careerProfileMissingTranscripts
                    );
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'missingEnglishLanguageProficiencyDocuments', {
                get() {
                    return (
                        !this.english_language_proficiency_documents_approved &&
                        this.recordsIndicateUserShouldUploadEnglishLanguageProficiencyDocuments &&
                        !this.hasUploadedEnglishLanguageProficiencyDocuments
                    );
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'missingRequiredDocumentsLegacy', {
                get() {
                    return (
                        this.missingIdentificationDocument ||
                        this.missingTranscripts ||
                        this.missingEnglishLanguageProficiencyDocuments
                    );
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'missingRequiredDocuments', {
                get() {
                    return (
                        this.missingRequiredDocumentsLegacy ||
                        this.missingUserIdVerification ||
                        // The enrollment agreement only becomes relevant when the user gets accepted into
                        // the program. Until then, the enrollment agreement isn't considered to be "required".
                        (this.isAccepted && !this.lastCohortApplication.hasSignedEnrollmentAgreement)
                    );
                },
            });

            Object.defineProperty(this.prototype, 'readyForApprovalLegacy', {
                get() {
                    return (
                        (this.hasUploadedIdentification && !this.identity_verified) ||
                        (this.hasUploadedTranscripts &&
                            this.careerProfileIndicatesUserShouldProvideTranscripts &&
                            !this.transcripts_verified) ||
                        (this.hasUploadedEnglishLanguageProficiencyDocuments &&
                            this.careerProfileIndicatesUserShouldUploadEnglishLanguageProficiencyDocuments &&
                            !this.english_language_proficiency_documents_approved)
                    );
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'readyForApproval', {
                get() {
                    return this.readyForApprovalLegacy || !this.lastCohortApplication.hasSignedEnrollmentAgreement;
                },
            });

            Object.defineProperty(this.prototype, 'shouldRedirectOnIncompleteOnboarding', {
                get() {
                    // We will force people to the application page if they are a normal
                    // learner without a fallback_program_type of 'demo' or 'external'
                    return (
                        this.roleName() === 'learner' && !_.contains(['demo', 'external'], this.fallback_program_type)
                    );
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'shouldVisitApplicationStatus', {
                get() {
                    /*
                        We will force people to the application status page if
                        1. it is time to register for a billable cohort and they have not done so yet
                        2. their payment failed and they need to update billing info
                        3. we are trying to sell them on paid certificates after being expelled or rejected,
                            but only if they aren't reapplyingOrEditingApplication
                    */
                    return (
                        this.shouldVisitRegistration ||
                        (this.showPaidCertSelection && !this.reapplyingOrEditingApplication)
                    );
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'shouldVisitRegistration', {
                get() {
                    return (
                        this.relevant_cohort &&
                        this.relevant_cohort.afterRegistrationOpenDate &&
                        (this.needsToRegister || this.needsToProvideCohortBillingInfo)
                    );
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'showPaidCertSelection', {
                get() {
                    return (
                        !$window.CORDOVA &&
                        this.can_purchase_paid_certs &&
                        this.lastCohortApplication &&
                        (_.contains(['rejected', 'expelled'], this.lastCohortApplication.status) ||
                            (this.lastCohortApplication.isPaidCert && this.lastCohortApplication.status !== 'accepted'))
                    );
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'reapplyingOrEditingApplication', {
                get() {
                    return ClientStorage.getItem('reapplyingOrEditingApplication') === 'true';
                },
                set(value) {
                    if (typeof value === 'boolean') {
                        if (value) {
                            ClientStorage.setItem('reapplyingOrEditingApplication', true);
                        } else {
                            ClientStorage.removeItem('reapplyingOrEditingApplication');
                        }
                    } else {
                        throw new Error(
                            `reapplyingOrEditingApplication must be set to a boolean; it cannot be set to type: ${typeof value}`,
                        );
                    }
                },
                configurable: true, // for specs
            });

            Object.defineProperty(this.prototype, 'acceptedToPaidCert', {
                get() {
                    return (
                        this.lastCohortApplication &&
                        this.lastCohortApplication.status === 'accepted' &&
                        this.lastCohortApplication.isPaidCert
                    );
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'progress', {
                get() {
                    let progress = !this.$$progress?.destroyed && this.$$progress;

                    // If the value for frontRoyalStore.enabled has changed, then
                    // we need to initialize a new userProgressLoader.  There is a watcher
                    // on currentUser in user_progress_loader.js that destroys the userProgressLoader
                    // when frontRoyalStore.enabled changes, but there can be a race condition where
                    // we hit this before that happens.  See the error related to `progressFetches`
                    // being null on https://trello.com/c/hFv8MgQx
                    if (progress && progress.usesFrontRoyalStore !== frontRoyalStore.enabled) {
                        progress.destroy();
                        progress = null;
                        this.$$progress = null;
                    }

                    if (!progress) {
                        this.$$progress = UserProgressLoader.initialize(this, frontRoyalStore.enabled);
                    }

                    return this.$$progress;
                },
            });

            Object.defineProperty(this.prototype, 'hasPlaylists', {
                get() {
                    if (this.relevant_cohort && _.any(this.relevant_cohort.playlistPackIds)) {
                        return true;
                    }

                    if (_.chain(this.institutions).pluck('playlist_pack_ids').flatten().any().value()) {
                        return true;
                    }

                    return false;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'unverifiedForCurrentIdVerificationPeriod', {
                get() {
                    const idVerificationPeriod =
                        this.relevant_cohort && this.relevant_cohort.activeIdVerificationPeriod;
                    if (!idVerificationPeriod) {
                        return false;
                    }

                    if (!this.acceptedOrPreAcceptedCohortApplication) {
                        return false;
                    }

                    // If the cohort supports the concept of registration (EMBA), we don't want to consider unregistered
                    // users as unverifiedForCurrentIdVerificationPeriod and show them student-dashboard-identity-verification.
                    if (
                        this.relevant_cohort.supportsRegistrationDeadline &&
                        !this.acceptedOrPreAcceptedCohortApplication.registered
                    ) {
                        return false;
                    }

                    // In uncommon situations, a user can end up with multiple user_id_verifications
                    // records. This can happen if they defer into a cohort and then again back into
                    // the original cohort. We should just check that the user has a verification for
                    // the current period in `user_id_verifications`.
                    // See also: https://trello.com/c/vaRO3Dv9
                    return !_.find(this.user_id_verifications, userIdVerification =>
                        userIdVerification.forVerificationPeriod(idVerificationPeriod),
                    );
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'pastDueForIdVerification', {
                get() {
                    return (
                        this.unverifiedForCurrentIdVerificationPeriod &&
                        this.relevant_cohort.activeIdVerificationPeriod.pastDue
                    );
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'lastIdologyVerification', {
                get() {
                    return this.idology_verifications && this.idology_verifications[0];
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'lastIdologyVerificationFailed', {
                get() {
                    return !!(
                        this.lastIdologyVerification &&
                        this.lastIdologyVerification.response_received_at &&
                        this.lastIdologyVerification.verified === false
                    );
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'hasStudentNetworkEventsAccess', {
                get() {
                    return !!this.canAccessNetworkEvents;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'canAccessNetworkEvents', {
                get() {
                    return !this.inIsolatedNetworkCohort;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'isMiyaMiya', {
                get() {
                    return this.active_institution?.id === 'a297c552-8cef-4c78-83b3-465c8711377c';
                },
                configurable: true,
            });

            this.defineSetter('pref_locale', function (val) {
                // We might have the wrong stream cached for a locale pack id
                // if the locale changes.
                $injector.get('Lesson.Stream').resetCache();
                $injector.get('Playlist').resetCache();

                this.writeKey('pref_locale', val);
            });

            return {
                // global editing permissions
                canEdit() {
                    return this.roleName() === 'admin' || this.roleName() === 'super_editor';
                },

                isAuthor(lesson) {
                    if (lesson && lesson.author) {
                        return this.id === lesson.author.id;
                    }
                    return false;
                },

                // editing permissions scoped to individual lesson
                canEditLesson(lesson) {
                    if (!lesson || !lesson.editable) {
                        return false;
                    }

                    return (
                        this.canEdit() ||
                        this.isAuthor(lesson) ||
                        (this.roleName() === 'editor' && this.lessonPermissions[lesson.id] === 'lesson_editor')
                    );
                },
                canPreviewLesson(lesson) {
                    if (lesson) {
                        return (
                            this.canEdit() ||
                            this.isAuthor(lesson) ||
                            (this.roleName() === 'editor' && this.lessonPermissions[lesson.id] === 'previewer')
                        );
                    }
                    return false;
                },
                canReviewLesson(lesson) {
                    if (lesson) {
                        return (
                            this.canEdit() ||
                            this.isAuthor(lesson) ||
                            (this.roleName() === 'editor' && this.lessonPermissions[lesson.id] === 'reviewer')
                        );
                    }
                    return false;
                },
                canAddCommentsToLesson(lesson) {
                    if (!lesson || !lesson.editable) {
                        return false;
                    }
                    return this.canEditLesson(lesson) || this.canReviewLesson(lesson);
                },

                // this is both "can archive" and "can unarchive"
                canArchiveLesson(lesson) {
                    if (!lesson) {
                        return false;
                    }

                    if (!this.canEditLesson(lesson)) {
                        return false;
                    }

                    // if a lesson is published, then archiving will change
                    // the published status.  Check publishing rights
                    if (lesson.published_at && !this.canPublish()) {
                        return false;
                    }

                    return true;
                },

                isScopedlessonPermission(roleName) {
                    return roleName === 'lesson_editor' || roleName === 'reviewer' || roleName === 'previewer';
                },

                roleName() {
                    if (this.globalRole) {
                        return this.globalRole.name;
                    }
                    return undefined;
                },

                userGroupNames() {
                    return _.chain(this.groups).pluck('name').invoke('toUpperCase').value().sort();
                },

                inGroup(groupName) {
                    return _.contains(this.userGroupNames(), groupName.toUpperCase());
                },

                addGroup(groupName) {
                    if (this.inGroup(groupName)) {
                        return;
                    }

                    const group = {
                        name: groupName,
                    };
                    if (!this.groups) {
                        this.groups = [];
                    }
                    this.groups.push(group);
                },

                removeGroup(group) {
                    Array.remove(this.groups, group);
                },

                // Checks if the streamLocalePackId passed in is associated with any of the
                // user's access groups.
                streamIsAssociatedWithAccessGroup(streamLocalePackId) {
                    for (let i = 0; i < this.groups.length; i++) {
                        if (_.contains(this.groups[i].stream_locale_pack_ids, streamLocalePackId)) {
                            return true;
                        }
                    }
                    return false;
                },

                institutionNames() {
                    return (this.institutions || []).map(institution => institution.name).sort();
                },

                inInstitution(institutionName) {
                    return _.contains(this.institutionNames(), institutionName);
                },

                addInstitution(institution) {
                    if (!this.institutions) {
                        this.institutions = [];
                    }
                    const institutionIds = this.institutions.map(i => i.id);
                    if (!institutionIds.includes(institution.id)) {
                        this.institutions.push(institution);
                    }
                },

                removeInstitution(institution) {
                    const institutions = this.institutions || [];
                    const index = institutions.indexOf(institution);
                    if (index === -1) {
                        throw new Error(`Institution '${institution.name}' is not present on user ${this.id}`);
                    }
                    institutions.splice(index, 1);
                },

                // end users use this method to apply to a cohort. It hits
                // the cohort_applications api endpoint
                applyToCohort(cohort, initialStatus) {
                    const self = this;
                    initialStatus = initialStatus || 'pending';

                    if (self.hasPendingAcceptedOrPreAcceptedCohortApplication) {
                        throw new Error('There is already a pending or accepted application for this user.');
                    }

                    const cohortApplication = CohortApplication.new({
                        cohort_id: cohort.id,
                        status: initialStatus,
                    });
                    cohortApplication.$$embeddedIn = self;
                    cohortApplication.program_type = cohort.program_type;

                    // We want to know in Google Analytics when a MBA or EMBA cohort application is submitted.
                    // Since Google Analytics only supports a limited number of identifying attributes on the
                    // event - an action key and a label key - we need to add the program_type to these fields
                    // so we can properly use these events when confiuring goals in Google Analytics.
                    // See https://trello.com/c/fsx3KByZ for more info.
                    if (cohort.isDegreeProgram) {
                        EditCareerProfileHelper.logEventForApplication(
                            `${cohort.program_type}-submit-application`,
                            true,
                            {
                                label: `${cohort.program_type.toUpperCase()} Application Submitted`,
                            },
                        );
                    }

                    // FIXME: Shouldn't this be a server event to make sure it happens transactionally
                    // with application saving?
                    //
                    // Years later this question came up again, and we decided to leave it be since we
                    // have so many analytics and customer.io campaigns currently using it. And we did some investigating
                    // to find that there are users that have logged the event twice (e.g., `4f064a50-5d6f-494c-a93f-ecc9eebe794e`);
                    // but in customer.io it did not appear that the campaign, for which this event triggers an
                    // email, sent more than one copy.
                    // See https://trello.com/c/GqjJaNCi
                    EditCareerProfileHelper.logEventForApplication('submit-application', true, {
                        cohort_title: cohort.title,
                        s_range: self.career_profile.salaryRangeForEventPayload, // see https://trello.com/c/5jz2ZNWl

                        // see 'Post-application content marketing drip campaign in customer.io' https://fly.customer.io/env/24965/v2/campaigns/1000133/overview/trigger
                        start_content_marketing_campaign: self.cohort_applications.length === 0,

                        // For GA -- see https://trello.com/c/GqjJaNCi
                        category: 'Submitted Application',
                        label: cohortApplication.program_type,
                        value: EstimatedValue.ev2(cohortApplication, self.career_profile, cohort.program_type),
                    });

                    self.logConversionGoalEvent();

                    return cohortApplication.save().then(response => {
                        cohortApplication.$$embeddedIn = self;
                        self.cohort_applications.push(response.result);
                        self.setContinueApplicationInMarketingFlag();
                        return response.result;
                    });
                },

                logConversionGoalEvent() {
                    // determine conversion value based on salary on career_profile
                    const programType = this.programType;

                    const EventLogger = $injector.get('EventLogger');
                    const conversionEquivalents = $injector.get('CAREERS_SALARY_CONVERSION_EQUIVALENTS');
                    const conversionValue =
                        (conversionEquivalents[programType] &&
                            conversionEquivalents[programType][this.career_profile.salary_range]) ||
                        0;

                    // log event with Cordova plugin support if available
                    EventLogger.log(
                        'cohort:conversion_goal',
                        {
                            label: 'Purchase',
                            user_id: this.id,
                            program_type: programType,
                            value: conversionValue,
                            currency: 'USD',
                        },
                        {
                            cordovaFacebook: !!$window.CORDOVA,
                        },
                    );
                },

                deleteCohortApplication(application) {
                    const self = this;
                    const index = this.cohort_applications.indexOf(application);
                    if (index > -1) {
                        self.cohort_applications.splice(index, 1);
                    }
                    return application.destroy();
                },

                canPublish() {
                    return this.roleName() === 'admin';
                },

                toggleBookmark(stream) {
                    stream.favorite = !stream.favorite;

                    // we actually want to allow the toggle to occur immediately, with
                    // the save prep and handling to execute asynchrnonously
                    $timeout(() => {
                        // ensure the user has favorite_lesson_stream_locale_packs collection
                        this.favorite_lesson_stream_locale_packs = this.favorite_lesson_stream_locale_packs || [];

                        // produce simple lookup map of unique locale packs
                        const existingFavoriteIds = _.chain(this.favorite_lesson_stream_locale_packs)
                            .map('id')
                            .unique()
                            .value();

                        // remove or add the stream locale pack
                        const index = existingFavoriteIds.indexOf(stream.locale_pack.id);
                        if (index > -1) {
                            this.favorite_lesson_stream_locale_packs.splice(index, 1);
                        } else {
                            this.favorite_lesson_stream_locale_packs.push({
                                id: stream.locale_pack.id,
                            });
                        }

                        // update the user
                        this.save();

                        frontRoyalStore.setStreamBookmarks(this);
                    });
                },

                clearAllProgress() {
                    const user = this;

                    return $http
                        .delete(`${$window.ENDPOINT_ROOT}/api/destroy_all_progress.json`)
                        .then(() => {
                            if (frontRoyalStore.enabled) {
                                return frontRoyalStore.retryAfterHandledError(db => clearProgressForUser(this.id, db));
                            }
                            return undefined;
                        })
                        .then(() => {
                            user.favorite_lesson_stream_locale_packs = [];
                            user.progress.clear();
                        });
                },

                avatarUploadUrl() {
                    return `${window.ENDPOINT_ROOT}/api/users/${this.id}/upload_avatar.json`;
                },

                resumeUploadUrl() {
                    return `${window.ENDPOINT_ROOT}/api/users/${this.id}/upload_resume.json`;
                },

                // This method logs the onboarding:complete event the
                // first time the user is routed to a page within front-royal.
                // See ensureLoginEvent for something similar
                ensureHasSeenWelcome() {
                    const user = this;
                    if (user.has_seen_welcome || user.ghostMode) {
                        return;
                    }

                    const EventLogger = $injector.get('EventLogger');
                    EventLogger.allowEmptyLabel('onboarding:complete');
                    EventLogger.log(
                        'onboarding:complete',
                        {
                            user_id: user.id,
                        },
                        {
                            cordovaFacebook: !!window.CORDOVA,
                        },
                    );

                    user.has_seen_welcome = true;
                    user.save();
                },

                // This method logs `user:logged_in_first_time` the first time
                // a user logs in.  This can be logged even when onboarding:complete is
                // not logged if the user completes the signup form at dynamic_landing_page
                // but never actually goes into the app (see ensureHasSeenWelcome)
                ensureLoginEvent() {
                    const user = this;

                    // This method is not idempotent and can result in duplicate events. See comment in
                    // `ValidationResponder.handleValidationSuccess` for why this can get called multiple
                    // times and why this check is necessary.
                    if (user.has_logged_in || user.ghostMode) {
                        return;
                    }

                    // The event below is used for optimization in ad platforms, which expects the event property
                    // values to be in human readable form (not sure why, tbh), so we get the raw registration info
                    // first and then convert it to human readable form. This is done is two steps so that we can
                    // use the raw registration info to more easily determine if the info provided by the user
                    // constitutes a "good lead". Then we attach this "good lead" boolean to the human readable
                    // registration info for use in our ad platforms.
                    const dlpRegistrationInfo = preSignupValues.getDynamicLandingPageMultiStepFormRegistrationInfo();
                    const registrationInfo = preSignupValues.convertRegistrationInfoFromDLPToHumanReadableForm(
                        dlpRegistrationInfo,
                    );
                    registrationInfo.gl = preSignupValues.goodLead(dlpRegistrationInfo);

                    // If we see that the sign up was for a "good lead", make sure to set the `label` property
                    // in the event payload. Our third party analytics tools depend on the `label` being present
                    // only for "good leads" so that we can identify them as such. We intentionally leave the
                    // `label` property blank otherwise as its absence may be depended on by some views in GA.
                    // We also set the label to something fairly indiscernible since users sometimes inspect
                    // the events we send. See https://trello.com/c/sqglpXD2.
                    if (registrationInfo.gl) {
                        registrationInfo.label = 'gl';
                    } else {
                        // At the time of this writing, we're not expecting the registrationInfo to have a
                        // `label` property when this code is executed; however, because some views in GA
                        // may be dependent on this property's absence, we proactively remove it completely
                        // from the event payload.
                        delete registrationInfo.label;
                    }

                    const EventLogger = $injector.get('EventLogger');
                    EventLogger.allowEmptyLabel('user:logged_in_first_time'); // empty if not a "good lead"
                    EventLogger.log('user:logged_in_first_time', registrationInfo, {
                        cordovaFacebook: !!window.CORDOVA,
                    });

                    user.has_logged_in = true;
                    user.save().then(() => {
                        // Now that we have used the preSignupValues, it is safe to
                        // clear it out
                        preSignupValues.clearAll();
                    });
                },

                userIdVerificationForPeriod(idVerificationPeriod) {
                    // FIXME: do we need caching here?
                    return _.detect(this.user_id_verifications, userIdVerification =>
                        userIdVerification.forVerificationPeriod(idVerificationPeriod),
                    );
                },

                addUserIdVerification(verificationPeriod) {
                    const userIdVerification = UserIdVerification.new({
                        cohort_id: verificationPeriod.cohortId,
                        id_verification_period_index: verificationPeriod.index,
                        user_id: this.id,
                        verification_method: 'verified_by_admin',
                    });
                    userIdVerification.$$embeddedIn = this;
                    this.user_id_verifications = this.user_id_verifications || [];
                    this.user_id_verifications.push(userIdVerification);
                },

                removeUserIdVerification(verificationPeriod) {
                    const userIdVerification = this.userIdVerificationForPeriod(verificationPeriod);
                    this.user_id_verifications = _.without(this.user_id_verifications, userIdVerification);
                },

                setContinueApplicationInMarketingFlag() {
                    if (
                        this.roleName() === 'learner' &&
                        !this.hasEverApplied &&
                        !_.contains(['demo', 'external'], this.programType)
                    ) {
                        ClientStorage.setItem('continueApplicationInMarketing', true);
                    } else {
                        ClientStorage.removeItem('continueApplicationInMarketing');
                    }
                },

                requestIdologyLink(params) {
                    // params should include a value for either `email` or `phone`
                    return $http.post(
                        `${$window.ENDPOINT_ROOT}/api/idology/request_idology_link.json`,
                        _.extend(params, {
                            user_id: this.id,
                        }),
                    );
                },

                subscriptionForPosition(position) {
                    if (!this.hiring_team) {
                        return null;
                    }
                    return _.findWhere(this.hiring_team.subscriptions, {
                        id: position.subscription_id,
                    });
                },

                hasActiveAutoRenewal(position) {
                    const subscription = this.subscriptionForPosition(position);
                    if (!subscription) {
                        throw new Error('Expected to find a subscription');
                    }

                    return subscription.hasActiveAutoRenewal;
                },

                positionExpiresOrCancelsSoon(position) {
                    if (position.archived) {
                        return false;
                    }
                    const expirationDate = this.expirationCancellationOrRenewalDateForPosition(position);
                    if (!expirationDate) {
                        return false;
                    }

                    // Be sure to use startOf so that we are comparing two dates with no
                    // time information, otherwise you get fractions and that can be confusing.
                    const startOfToday = moment().startOf('day');
                    const diffDays = startOfToday.diff(expirationDate, 'days');

                    // "soon" means that it expires in the next 10 days
                    return diffDays <= 0 && diffDays >= -10;
                },

                expirationCancellationOrRenewalDateForPosition(position) {
                    const subscription = this.subscriptionForPosition(position);
                    if (subscription && !position.archived) {
                        return new Date(1000 * subscription.current_period_end);
                    }
                    if (position.auto_expiration_date) {
                        // auto-expiration-date is a date string, like '2019-01-01', not
                        // a timestamp.  There is a job that is configured to run at 7am Eastern
                        // time that will expire all of the positions for that day.  So if we
                        // want the specific expiration time, it's around 7am Eastern.
                        return moment.tz(`${position.auto_expiration_date} 07:00`, 'America/New_york').toDate();
                    }
                    return null;
                },

                expirationMessageLocaleSuffix(position) {
                    const subscription = this.subscriptionForPosition(position);

                    // Do not show an expiration date for drafted positions (they do actually expire, but we
                    // have plans to change this at some point; see https://trello.com/c/Gzhy7szF for more info)
                    if (!position.featured || position.manually_archived) {
                        return null;
                    }
                    if (subscription && subscription.hasActiveAutoRenewal) {
                        return 'renews';
                    }
                    if (subscription) {
                        return 'cancels';
                    }
                    if (position.archived) {
                        return 'expired';
                    }
                    if (position.auto_expiration_date) {
                        return 'expires';
                    }

                    ErrorLogService.notifyInProd('Position has no auto_expiration_date', undefined, {
                        fingerprint: ['Position has no auto_expiration_date'],
                        positionId: position.id,
                    });

                    return null;
                },
            };
        });
    },
]);
