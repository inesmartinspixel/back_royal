import angularModule from 'Users/angularModule/scripts/users_module';

angularModule.factory('DeferralLink', [
    '$injector',
    $injector => {
        const Iguana = $injector.get('Iguana');

        return Iguana.subclass(function () {
            this.setCollection('deferral_links');
            this.alias('DeferralLink');
            this.setIdProperty('id');
            this.embeddedIn('user');
            this.embedsOne('from_cohort_application', 'CohortApplication');
            this.embedsOne('to_cohort_application', 'CohortApplication');

            Object.defineProperty(this.prototype, 'url', {
                get() {
                    return `${window.ENDPOINT_ROOT}/deferral/${this.id}`;
                },
                configurable: true,
            });

            return {};
        });
    },
]);
