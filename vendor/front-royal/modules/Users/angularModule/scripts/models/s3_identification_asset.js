import angularModule from 'Users/angularModule/scripts/users_module';
// iguana service wrapper class
// currently only opening up /api/users.json which returns info about the current user
angularModule.factory('S3IdentificationAsset', [
    '$injector',
    $injector => {
        const Iguana = $injector.get('Iguana');

        return Iguana.subclass(function () {
            this.setCollection('s3_identification_assets');
            this.alias('S3IdentificationAsset');
            this.embeddedIn('user');

            this.extend({
                UPLOAD_URL: `${window.ENDPOINT_ROOT}/api/s3_identification_assets.json`,
            });

            Object.defineProperty(this.prototype, 'downloadUrl', {
                get() {
                    return `${window.ENDPOINT_ROOT}/api/s3_identification_assets/${this.id}/file`;
                },
            });

            return {};
        });
    },
]);
