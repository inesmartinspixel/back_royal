import angularModule from 'Users/angularModule/scripts/users_module';

angularModule.factory('SignableDocument', [
    '$injector',
    $injector => {
        const Iguana = $injector.get('Iguana');

        return Iguana.subclass(function () {
            this.setCollection('signable_documents');
            this.alias('SignableDocument');
            this.setIdProperty('id');
            this.embeddedIn('user');

            this.extend({
                UPLOAD_URL: `${window.ENDPOINT_ROOT}/api/signable_documents.json`,
            });

            Object.defineProperty(this.prototype, 'downloadUrl', {
                get() {
                    return `${window.ENDPOINT_ROOT}/api/signable_documents/show_file/${this.id}`;
                },
            });

            return {};
        });
    },
]);
