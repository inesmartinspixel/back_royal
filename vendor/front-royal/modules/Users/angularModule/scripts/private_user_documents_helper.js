import angularModule from 'Users/angularModule/scripts/users_module';

angularModule.factory('PrivateUserDocumentsHelper', [
    '$injector',

    $injector => {
        const $window = $injector.get('$window');
        const $http = $injector.get('$http');
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
        const TranslationHelper = $injector.get('TranslationHelper');
        const DialogModal = $injector.get('DialogModal');
        const frontRoyalUpload = $injector.get('frontRoyalUpload');
        const S3IdentificationAsset = $injector.get('S3IdentificationAsset');

        // FIXME: Is this true about being unstable though?
        // Note: URL.createObjectURL is not yet considered stable but I used it anyway because it is
        // super convienant, works in all the major browsers (if you include the prefixed form), and this
        // is for admins only anyway.
        const createObjectURL = ($window.URL || $window.webkitURL || {}).createObjectURL || (() => {});
        const revokeObjectURL = ($window.URL || $window.webkitURL || {}).revokeObjectURL || (() => {});

        const translationHelper = new TranslationHelper('settings.application_status'); // Just let them live here

        return {
            createObjectURL,
            revokeObjectURL,

            downloadDocument(asset) {
                $http
                    .get(asset.downloadUrl, {
                        responseType: 'arraybuffer',
                    })
                    .then(response => {
                        const file = new Blob([response.data], {
                            type: asset.file_content_type,
                        });
                        const new_url = createObjectURL(file);
                        const a = $window.document.createElement('a');
                        a.href = new_url;
                        a.download = asset.file_file_name;
                        a.click();
                        revokeObjectURL(file);
                    });
            },

            viewDocument(asset) {
                $http
                    .get(asset.downloadUrl, {
                        responseType: 'arraybuffer',
                    })
                    .then(response => {
                        const file = new Blob([response.data], {
                            type: asset.file_content_type,
                        });
                        NavigationHelperMixin.loadUrl(this.createObjectURL(file), '_blank');
                    });
            },

            // Expose methods on the scope when we know we are dealing with both an original
            // and a proxy user object.
            // Note: I'd love to move the DialogModel logic to the pure function API, but it's
            // tough because it uses callbacks instead of promises.
            onLinkWhenUsingProxy(scope) {
                if (!scope.user || !scope.userProxy) {
                    throw 'Must have a user and userProxy object';
                }

                scope.downloadDocument = document => this.downloadDocument(document);
                scope.viewDocument = document => this.viewDocument(document);

                scope.onIdentificationSelect = ($file, $invalidFiles) => {
                    scope.idErrMessage = null;
                    scope.uploadingIdentification = true;

                    frontRoyalUpload
                        .handleNgfSelect($file, $invalidFiles, file => ({
                            url: S3IdentificationAsset.UPLOAD_URL,
                            data: {
                                record: {
                                    file,
                                    user_id: scope.userProxy.id,
                                },
                            },
                            supportedFormatsForErrorMessage: '.pdf, .jpg, .png',
                        }))
                        .then(response => {
                            const identificationAssetJson = _.first(response.data.contents.s3_identification_assets);

                            // Since this change happens independently of a save button, reflect
                            // it on the original object now.
                            scope.user.s3_identification_asset = S3IdentificationAsset.new(identificationAssetJson);
                            scope.userProxy.s3_identification_asset = S3IdentificationAsset.new(
                                identificationAssetJson,
                            );
                        })
                        .catch(err => {
                            if (err.status === 409) {
                                // Do nothing.  We tried to upload a document when identitiy_verified was already true.  The correct
                                // value will have been returned in the metadata and the UI should update automatically with the
                                // identity verified checkbox (FIXME: this does not work in the admin, because the information for
                                // the user who is being edited is not pushed down in the meta.  Instead, information for the current_user, the admin,
                                // is pushed down)
                            } else {
                                scope.idErrMessage = err && err.message;
                            }
                        })
                        .finally(() => {
                            scope.uploadingIdentification = false;
                        });
                };

                scope.deleteIdentification = () => {
                    const identificationAsset = scope.userProxy.s3_identification_asset;

                    DialogModal.confirm({
                        text: translationHelper.get('delete_identification', {
                            file_name: scope.user.s3_identification_asset.file_file_name,
                        }),
                        confirmCallback: () => {
                            scope.deletingIdentification = true;
                            identificationAsset
                                .destroy()
                                .then(
                                    () => {
                                        // Since this change happens independently of a save button, reflect
                                        // it on the original object now.
                                        scope.user.s3_identification_asset = null;
                                        scope.userProxy.s3_identification_asset = null;
                                    },
                                    err => {
                                        throw new Error(err);
                                    },
                                )
                                .finally(() => {
                                    scope.deletingIdentification = false;
                                });
                        },
                    });
                };
            },
        };
    },
]);
