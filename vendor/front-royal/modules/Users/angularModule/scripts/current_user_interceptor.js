import angularModule from 'Users/angularModule/scripts/users_module';

angularModule.factory('CurrentUserInterceptor', [
    '$injector',

    $injector => {
        const $rootScope = $injector.get('$rootScope');
        const $window = $injector.get('$window');
        const ClientStorage = $injector.get('ClientStorage');
        const frontRoyalStore = $injector.get('frontRoyalStore');

        const CurrentUserInterceptor = {
            response(response) {
                CurrentUserInterceptor.updateUser(response);
                return response;
            },
            responseError(response) {
                // We may send down updates to the user in the meta of an errored api call.  See
                // the way that the s3_identification_asset controller handles conflicts with identity_verified
                CurrentUserInterceptor.updateUser(response);
                throw response;
            },
            request(config) {
                if (config.url.includes(`${$window.ENDPOINT_ROOT}/api`)) {
                    // If this is a call to our API and ghostMode is set to true then let our server know
                    //
                    // NOTE: We need to check ClientStorage here to accommodate for any requests made
                    // in between performing the log-in-as and setting $rootScope.currentUser.ghostMode,
                    // which only happens after a successful request to the log-in-as endpoint. While
                    // we could completely rely on this ClientStorage check, we can avoid the storage
                    // lookup once `ghostMode` is set on the user.
                    if (
                        ($rootScope.currentUser && $rootScope.currentUser.ghostMode) ||
                        !!ClientStorage.getItem('logged_in_as')
                    ) {
                        config.params = config.params || {};
                        config.params.ghost_mode = true;
                    }
                }
                return config;
            },

            // public for testability
            updateUser(response) {
                let updatedProperties;
                try {
                    updatedProperties = response.data.meta.push_messages.current_user;
                    // eslint-disable-next-line no-empty
                } catch (e) {}

                if ($rootScope.currentUser && updatedProperties) {
                    const oldProgramType = $rootScope.currentUser.programType;

                    // We've seen issues where replacing the hiring team after a ping causes a new
                    // stripe checkout helper to get created in the middle of updating payment info,
                    // which is bad.  So, we go out of our way to copy the pushed down attrs into the
                    // existing hiring team, rather than replacing it.  Really, this should happen for all
                    // embedded objects, and it's really something that Iguana should take care of for us,
                    // but for now only hiring teams are causing problems, so just fixing that.
                    const hiringTeamAttrs = updatedProperties.hiring_team;
                    if (
                        hiringTeamAttrs &&
                        $rootScope.currentUser.hiring_team &&
                        $rootScope.currentUser.hiring_team.id === hiringTeamAttrs.id
                    ) {
                        $rootScope.currentUser.hiring_team.copyAttrs(hiringTeamAttrs);
                        delete updatedProperties.hiring_team;
                    }

                    // We use copyAttrs so that iguana will instantiate embedded objects,
                    // like cohort_applications
                    $rootScope.currentUser.copyAttrs(updatedProperties);

                    $rootScope.currentUser.setContinueApplicationInMarketingFlag();

                    // Ensure that a user who was logged in when they were deactivated
                    // cannot retain access to the platform by signing them out. They
                    // will not be able to sign back in until reactivated.
                    if ($rootScope.currentUser.deactivated) {
                        // don't bother injecting unless we're signing out
                        $injector.get('SignOutHelper').signOut();
                    }

                    // Ensure the reapplyingOrEditingApplication flag is set to false if the currentUser
                    // has an acceptedOrPreAcceptedCohortApplication, otherwise the user could have this
                    // flag set to true when they really shouldn't.
                    // NOTE: this logic should mirror the logic in settings_dir.js
                    // if ($rootScope.currentUser.acceptedOrPreAcceptedCohortApplication) {
                    if (
                        $rootScope.currentUser.acceptedOrPreAcceptedCohortApplication ||
                        ($rootScope.currentUser.lastCohortApplication?.status === 'rejected' &&
                            !$rootScope.currentUser.lastCohortApplication.canReapplyTo(
                                $rootScope.currentUser.relevant_cohort,
                            ))
                    ) {
                        $rootScope.currentUser.reapplyingOrEditingApplication = false;
                    }

                    // if program type changed, we need to clear some caches
                    if (!!oldProgramType && $rootScope.currentUser.programType !== oldProgramType) {
                        // I don't think it's possible for this codepath to run without these being available to inject
                        // because we're checking for the existence of a previously defined programType.
                        // But, just in case, wrap in a try-catch, since if they're not available we clearly don't
                        // yet have a cache to reset anyway, and can therefore ignore it and move on.
                        try {
                            const Playlist = $injector.get('Playlist');

                            // FIXME: learner_content_cache.js takes care of pushing new playlists into the cache,
                            // so we should in theory get rid of the Playlist cache.
                            // We used to clear the LearnerContentCache here, but it's unnecessary because the cache
                            // is watching currentUser.relevant_cohort.id, which is what drives the change in the programType
                            Playlist.resetCache();
                            // eslint-disable-next-line no-empty
                        } catch (e) {}
                    }

                    // setStreamBookmarks is also run in a watch on currentUser in
                    // the initialization of the frontRoyalStore module.
                    if (updatedProperties.favorite_lesson_stream_locale_packs) {
                        frontRoyalStore.setStreamBookmarks($rootScope.currentUser);
                    }
                }
            },
        };

        return CurrentUserInterceptor;
    },
]);

angularModule.config([
    '$httpProvider',
    $httpProvider => {
        $httpProvider.interceptors.push('CurrentUserInterceptor');
    },
]);
