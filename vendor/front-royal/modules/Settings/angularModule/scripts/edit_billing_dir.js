import angularModule from 'Settings/angularModule/scripts/settings_module';
import template from 'Settings/angularModule/views/edit_billing.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('editBilling', [
    '$injector',

    function factory($injector) {
        const $rootScope = $injector.get('$rootScope');

        return {
            restrict: 'E',
            templateUrl,
            scope: {},
            link(scope) {
                //-------------------------
                // Initialization
                //-------------------------

                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                Object.defineProperty(scope, 'shouldShowBilling', {
                    get() {
                        const cu = scope.currentUser;
                        return (
                            cu &&
                            cu.hiring_team &&
                            cu.hiring_team.subscription_required &&
                            (cu.hasAcceptedHiringManagerAccess ||
                                cu.hasAcceptedHiringManagerAccessPastDue ||
                                cu.usingPayPerPostHiringPlan)
                        );
                    },
                });
            },
        };
    },
]);
