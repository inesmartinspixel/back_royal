import angularModule from 'Settings/angularModule/scripts/settings_module';
import { setupBrandNameProperties, setupBrandEmailProperties } from 'AppBrandMixin';
import template from 'Settings/angularModule/views/transcripts.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('transcripts', [
    '$injector',

    function factory($injector) {
        const $rootScope = $injector.get('$rootScope');
        const isMobileMixin = $injector.get('isMobileMixin');
        const TranscriptHelper = $injector.get('TranscriptHelper');

        return {
            restrict: 'E',
            templateUrl,
            scope: {},
            link(scope) {
                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                setupBrandNameProperties($injector, scope);
                setupBrandEmailProperties($injector, scope, ['support']);
                isMobileMixin.onLink(scope);
                scope.transcriptHelper = new TranscriptHelper();

                // locale keys
                const userGraduated = _.contains(['graduated', 'honors'], scope.currentUser.curriculum_status);
                scope.headingKey = userGraduated
                    ? 'settings.transcripts.official_transcript'
                    : 'settings.transcripts.partial_transcript';
                scope.descKey = userGraduated
                    ? 'settings.transcripts.official_desc'
                    : 'settings.transcripts.partial_desc';
                scope.buttonKey = userGraduated
                    ? 'settings.transcripts.download_official_transcript'
                    : 'settings.transcripts.download_partial_transcript';
            },
        };
    },
]);
