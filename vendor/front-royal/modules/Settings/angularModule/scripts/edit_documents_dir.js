import angularModule from 'Settings/angularModule/scripts/settings_module';
import countries from 'countries';
import template from 'Settings/angularModule/views/edit_documents.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

import progressBadgeCompleteTurquoise from 'images/progress_badge_complete_turquoise.png';
import trashcanBeige from 'vectors/trashcan_beige.svg';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('editDocuments', [
    '$injector',

    function factory($injector) {
        const User = $injector.get('User');
        const Cohort = $injector.get('Cohort');
        const $window = $injector.get('$window');
        const ngToast = $injector.get('ngToast');
        const TranslationHelper = $injector.get('TranslationHelper');
        const PrivateUserDocumentsHelper = $injector.get('PrivateUserDocumentsHelper');
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                user: '<',
            },
            link(scope) {
                scope.progressBadgeCompleteTurquoise = progressBadgeCompleteTurquoise;
                scope.trashcanBeige = trashcanBeige;

                scope.userProxy = User.new(scope.user.asJson());

                NavigationHelperMixin.onLink(scope);
                PrivateUserDocumentsHelper.onLinkWhenUsingProxy(scope);

                scope.MAX_ENGLISH_LANGUAGE_PROFICIENCY_DOCUMENTS = 7;
                scope.EDITABLE_DOCUMENTS_ATTRIBUTES = [
                    // document upload related attributes
                    'identity_verified',
                    'transcripts_verified',
                    'english_language_proficiency_documents_approved',
                    'english_language_proficiency_documents_type',
                    'english_language_proficiency_comments',
                ];
                scope.REQUIRED_MAILING_ADDRESS_ATTRIBUTES = ['address_line_1', 'city', 'state', 'country', 'phone'];
                scope.EDITABLE_MAILING_ADDRESS_ATTRIBUTES = scope.REQUIRED_MAILING_ADDRESS_ATTRIBUTES.concat([
                    'address_line_2',
                    'zip',
                ]);
                scope.S3_DOCUMENT_ATTRIBUTES = [
                    's3_identification_asset',
                    's3_transcript_assets',
                    's3_english_language_proficiency_documents',
                ];

                const translationHelper = new TranslationHelper('settings.edit_documents');
                const cannotUploadOnThisDevice = !!($window.CORDOVA && $window.device.platform === 'iOS');

                Object.defineProperty(scope, 'showAnyDocumentSections', {
                    get() {
                        return (
                            (scope.user && scope.user.relevant_cohort && scope.user.relevant_cohort.requiresIdUpload) ||
                            scope.showEnglishLanguageProficiencySection
                        );
                    },
                    configurable: true,
                });

                Object.defineProperty(scope, 'identificationUploadIsDisabled', {
                    get() {
                        return (
                            scope.user &&
                            (scope.uploadingIdentification ||
                                scope.user.s3_identification_asset ||
                                !scope.user.lastCohortApplication ||
                                !scope.user.lastCohortApplication.indicatesUserShouldUploadIdentificationDocument)
                        );
                    },
                });

                Object.defineProperty(scope, 'hasUnsavedDocumentsSectionChanges', {
                    get() {
                        return hasUnsavedChanges(scope.EDITABLE_DOCUMENTS_ATTRIBUTES);
                    },
                    configurable: true,
                });

                Object.defineProperty(scope, 'hasUnsavedMailingAddressChanges', {
                    get() {
                        return hasUnsavedChanges(scope.EDITABLE_MAILING_ADDRESS_ATTRIBUTES);
                    },
                    configurable: true,
                });

                function hasUnsavedChanges(editableAttributes) {
                    return !!(
                        scope.user && _.find(editableAttributes, attr => scope.userProxy[attr] !== scope.user[attr])
                    );
                }

                function getCustomCheckboxIsCheckedHandler(indicatesUserShouldUploadProp) {
                    return docsVerifiedProp =>
                        scope.user && (scope.user[docsVerifiedProp] || !scope.user[indicatesUserShouldUploadProp]);
                }

                scope.customCheckboxIsCheckedHandlers = {
                    transcripts_verified: getCustomCheckboxIsCheckedHandler(
                        'careerProfileIndicatesUserShouldProvideTranscripts',
                    ),
                    english_language_proficiency_documents_approved: getCustomCheckboxIsCheckedHandler(
                        'careerProfileIndicatesUserShouldUploadEnglishLanguageProficiencyDocuments',
                    ),
                };

                scope.fieldIsDisabled = indicatesUserShouldUploadProp =>
                    !!(scope.user && !scope.user[indicatesUserShouldUploadProp]);

                scope.uploadIsRequired = (docsVerifiedProp, indicatesUserShouldUploadProp) =>
                    !!(scope.user && !scope.user[docsVerifiedProp] && scope.user[indicatesUserShouldUploadProp]);

                // Document upload/deletion happens in place on the main scope.user object, which means that
                // the S3 documents on the scope.userProxy object can be out of sync with the main scope.user
                // object, so this method copies the document attributes over to the userProxy to re-sync them.
                scope.copyS3DocumentsToUserProxy = () => {
                    scope.userProxy.copyAttrs(_.pick(scope.user.asJson(), scope.S3_DOCUMENT_ATTRIBUTES));
                };

                scope.save = (attributesToSave, attributesToOmit) => {
                    // Document upload/deletion happens in place on the main scope.user object, which means that
                    // the S3 documents on the scope.userProxy object can be out of sync with the main scope.user
                    // object, so before we save, we ensure that they're in sync with each other in that respect.
                    scope.copyS3DocumentsToUserProxy();

                    // We use a temporary user proxy here because the userProxy object acts as the model that's
                    // responsible for accepting all edits in the form, but since we have multiple save buttons
                    // that call this method and each button expects only certain attributes to be saved, we need
                    // to ensure that only the appropriate attributes are included in the save call and since
                    // the userProxy object contains ALL of the edits, we need to use a temporary proxy instead
                    // of calling save on the userProxy object directly.
                    const tempUserProxy = User.new(_.omit(scope.userProxy.asJson(), attributesToOmit));
                    scope.saving = true;

                    tempUserProxy
                        .save({
                            education_experiences:
                                scope.userProxy.career_profile.educationExperiencesIndicatingTranscriptRequired,
                        })
                        .then(response => {
                            // On the server when a user has their identity verified we automatically delete their
                            // s3_identification_asset, so we ensure that attributes that were updated as a direct
                            // result of this save all copied over to the main scope.user object as well as any
                            // changes that may have occured on the server as an indirect result of this update.
                            scope.user.copyAttrs(
                                _.pick(response.result, attributesToSave.concat(scope.S3_DOCUMENT_ATTRIBUTES)),
                            );
                            scope.copyS3DocumentsToUserProxy();

                            // Be sure to reflect updated_at change, otherwise we can trigger a 406
                            scope.userProxy.updated_at = response.result.updated_at;
                            scope.user.updated_at = scope.userProxy.updated_at;

                            const ngToastContent =
                                attributesToSave === scope.EDITABLE_DOCUMENTS_ATTRIBUTES
                                    ? translationHelper.get('documents_saved')
                                    : translationHelper.get('mailing_address_saved');
                            ngToast.create({
                                content: ngToastContent,
                                className: 'success',
                            });
                        })
                        .finally(() => {
                            scope.saving = false;
                        });
                };

                scope.uploadButtonText = translationHelper.get('upload');

                scope.showEnglishLanguageProficiencySection =
                    scope.user.careerProfileIndicatesUserShouldUploadEnglishLanguageProficiencyDocuments;
                scope.showMailingAddress =
                    Cohort.requiresMailingAddress(scope.user.programType) &&
                    (scope.user.isAccepted || scope.user.isPreAccepted);
                scope.showSignInOnDesktopToUploadDocsMessage =
                    cannotUploadOnThisDevice &&
                    (scope.user.missingIdentificationDocument ||
                        scope.user.missingTranscripts ||
                        scope.user.missingEnglishLanguageProficiencyDocuments);

                scope.identificationUploadInstructionsLocale = scope.fieldIsDisabled(
                    'recordsIndicateUserShouldUploadIdentificationDocument',
                )
                    ? 'identification_desc_when_cannot_upload'
                    : 'identification_desc_when_can_upload';

                scope.countryOptions = scope.showMailingAddress
                    ? _.map(countries, country => ({
                          value: country.value,
                          label: country.text,
                      }))
                    : null;
            },
        };
    },
]);
