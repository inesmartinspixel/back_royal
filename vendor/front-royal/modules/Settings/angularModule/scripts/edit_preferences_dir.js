import angularModule from 'Settings/angularModule/scripts/settings_module';
import { setupBrandNameProperties } from 'AppBrandMixin';
import template from 'Settings/angularModule/views/edit_preferences.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('editPreferences', [
    '$injector',

    function factory($injector) {
        const ngToast = $injector.get('ngToast');
        const $rootScope = $injector.get('$rootScope');
        const $window = $injector.get('$window');
        const TranslationHelper = $injector.get('TranslationHelper');
        const Locale = $injector.get('Locale');
        const safeApply = $injector.get('safeApply');

        return {
            restrict: 'E',
            templateUrl,
            scope: {},
            link(scope) {
                //-------------------------
                // Initialization
                //-------------------------

                setupBrandNameProperties($injector, scope);

                // Setup localization keys
                const translationHelper = new TranslationHelper('settings.edit_preferences');

                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                scope.frontRoyalStore = $injector.get('frontRoyalStore');

                if (scope.currentUser.defaultsToHiringExperience) {
                    scope.preferenceOptions = {
                        locale: {
                            type: 'select',
                            inputOptions: Locale.availablePreferenceLocales(scope.currentUser),
                        },
                        showPhotosNames: {
                            type: 'select',
                            inputOptions: [
                                {
                                    title: translationHelper.get('show_photos_names_enabled'),
                                    value: true,
                                },
                                {
                                    title: translationHelper.get('show_photos_names_disabled'),
                                    value: false,
                                },
                            ],
                        },
                        positionsCandidateList: {
                            type: 'select',
                            inputOptions: [
                                {
                                    title: translationHelper.get('positions_candidate_list_disabled'),
                                    value: false,
                                },
                                {
                                    title: translationHelper.get('positions_candidate_list_enabled'),
                                    value: true,
                                },
                            ],
                        },
                        oneClickReachOut: {
                            type: 'select',
                            inputOptions: [
                                {
                                    title: translationHelper.get('one_click_reach_out_disabled'),
                                    value: false,
                                },
                                {
                                    title: translationHelper.get('one_click_reach_out_enabled'),
                                    value: true,
                                },
                            ],
                        },
                    };
                    scope.preferences = {
                        locale: scope.currentUser.pref_locale,
                        showPhotosNames: scope.currentUser.pref_show_photos_names,
                        positionsCandidateList: scope.currentUser.pref_positions_candidate_list,
                        oneClickReachOut: scope.currentUser.pref_one_click_reach_out,
                    };
                } else {
                    scope.preferenceOptions = {
                        soundEnabled: {
                            type: 'checkbox',
                        },
                        keyboardShortcuts: {
                            type: 'checkbox',
                        },
                        decimalDelim: {
                            type: 'button-toggle',
                            inputOptions: [
                                {
                                    title: '1,234.56',
                                    value: '.',
                                },
                                {
                                    title: '1.234,56',
                                    value: ',',
                                },
                            ],
                        },
                        locale: {
                            type: 'button-toggle-wide',
                            inputOptions: Locale.availablePreferenceLocales(scope.currentUser),
                        },
                    };

                    scope.$watch('preferences.soundEnabled', () => {
                        // show help text about mute button if on cordova and sound is enabled
                        if (scope.preferences.soundEnabled && $window.CORDOVA) {
                            scope.preferenceOptions.soundEnabled.desc_extra = true;
                        } else {
                            scope.preferenceOptions.soundEnabled.desc_extra = false;
                        }
                    });

                    scope.preferences = {
                        soundEnabled: scope.currentUser.pref_sound_enabled,
                        keyboardShortcuts: scope.currentUser.pref_keyboard_shortcuts,
                        decimalDelim: scope.currentUser.pref_decimal_delim,
                        locale: scope.currentUser.pref_locale,
                    };
                }

                //-------------------------
                // Navigation / Actions
                //-------------------------

                scope.updatePreference = key => {
                    // eslint-disable-next-line no-shadow
                    function displayToast(success) {
                        const translatedSectionTitle = translationHelper.get(`${key.underscore()}_title`);

                        if (success) {
                            ngToast.create({
                                content: translationHelper.get('update_preference_success', {
                                    title: translatedSectionTitle,
                                }),
                                className: 'success',
                            });
                        } else {
                            ngToast.create({
                                content: translationHelper.get('update_preference_failure'),
                                className: 'danger',
                            });
                        }
                    }

                    function performSave(value) {
                        const column = `pref_${key.underscore()}`;
                        scope.currentUser[column] = scope.preferences[key];
                        scope.currentUser.save().then(
                            () => {
                                displayToast(true);
                            },
                            () => {
                                $injector
                                    .get('ErrorLogService')
                                    .notify(`Failed to update User preference for ${key} = ${value}`);
                                displayToast(false);
                            },
                        );
                    }

                    const value = scope.preferences[key];

                    // special handling
                    if (key === 'locale') {
                        const origLocale = scope.currentUser.pref_locale;
                        Locale.attemptToSetLanguage(value, true, true)
                            .then(() => {
                                Locale.preferredCode = value;
                                performSave(key, value);
                            })
                            .catch(() => {
                                scope.preferences[key] = origLocale;
                                displayToast(false);
                            });
                        return;
                    }

                    if (key === 'soundEnabled') {
                        const SoundManager = $injector.get('SoundManager');
                        SoundManager.enabled = value;
                    }

                    // handle user backed preferences
                    performSave(key, value);
                };

                // One day we might want to add a window.confirm
                // to this, but it's not really very dangerous right now.
                scope.clearLocalDatabase = async () => {
                    scope.clearingLocalDatabase = true;
                    await scope.frontRoyalStore.clearAllTables();
                    // eslint-disable-next-line require-atomic-updates
                    scope.clearingLocalDatabase = false;
                    ngToast.create({
                        content: translationHelper.get('database_cleared'),
                        className: 'success',
                    });
                    safeApply(scope); // since clearAllTables is outside of angular
                };

                // button-toggle

                scope.setPreference = (key, value, update) => {
                    scope.preferences[key] = value;
                    if (update) {
                        scope.updatePreference(key);
                    }
                };

                scope.isSelected = (key, value) => scope.preferences[key] === value;
            },
        };
    },
]);
