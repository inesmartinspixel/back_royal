import angularModule from 'Settings/angularModule/scripts/settings_module';
import { setupBrandEmailProperties } from 'AppBrandMixin';
import template from 'Settings/angularModule/views/tuition_and_registration/tuition_status_unregistered_alternative.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('tuitionStatusUnregisteredAlternative', [
    '$injector',

    function factory($injector) {
        const $rootScope = $injector.get('$rootScope');
        const dateHelper = $injector.get('dateHelper');
        const CohortRegistrationCheckoutHelper = $injector.get('Payments.CohortRegistrationCheckoutHelper');
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
        const $window = $injector.get('$window');
        const DialogModal = $injector.get('DialogModal');
        const TranslationHelper = $injector.get('TranslationHelper');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                cohortApplication: '<',
                launchProgramGuide: '&',
            },
            link(scope) {
                //-------------------------
                // Initialization
                //-------------------------

                NavigationHelperMixin.onLink(scope);
                setupBrandEmailProperties($injector, scope, ['emba', 'billing']);

                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                scope.checkoutHelper = new CohortRegistrationCheckoutHelper();

                if (scope.currentUser.relevant_cohort.afterRegistrationOpenDate) {
                    scope.registerEnabled = true;
                }

                Object.defineProperty(scope, 'buttonKey', {
                    get() {
                        if (scope.checkoutHelper.stripeProcessing) {
                            return 'settings.tuition_and_registration.processing';
                        }
                        if (scope.needsToOpenInBrowser) {
                            return 'settings.tuition_and_registration.launch_browser';
                        }
                        return 'settings.tuition_and_registration.register';
                    },
                });

                scope.needsToOpenInBrowser = $window.CORDOVA;
                scope.cohortTitle = scope.cohortApplication.cohort_title;

                scope.registrationDeadline = dateHelper.formattedUserFacingMonthDayYearLong(
                    scope.currentUser.relevant_cohort.registrationDeadline,
                );
                scope.registrationOpenDate = dateHelper.formattedUserFacingMonthDayYearLong(
                    scope.currentUser.relevant_cohort.registrationOpenDate,
                    false,
                );

                //-------------------------
                // Locale Keys
                //-------------------------

                // no scholarship
                if (scope.cohortApplication.hasNoScholarship) {
                    scope.narrativeKey = 'settings.tuition_and_registration.narrative_no_scholarship';
                    scope.graphicTitleKey = 'settings.tuition_and_registration.graphic_no_scholarship_title';
                    scope.graphicSubtitleKey = 'settings.tuition_and_registration.graphic_no_scholarship_subtitle';

                    // partial scholarship
                } else if (scope.cohortApplication.hasPartialScholarship) {
                    scope.narrativeKey = 'settings.tuition_and_registration.narrative_partial_scholarship';
                    scope.graphicTitleKey = 'settings.tuition_and_registration.graphic_partial_scholarship_title';
                    scope.graphicSubtitleKey = 'settings.tuition_and_registration.graphic_partial_scholarship_subtitle';

                    // full scholarship
                } else if (scope.cohortApplication.hasFullScholarship) {
                    scope.narrativeKey = 'settings.tuition_and_registration.narrative_full_scholarship';
                    scope.graphicTitleKey = 'settings.tuition_and_registration.graphic_full_scholarship_title';
                    scope.graphicSubtitleKey = 'settings.tuition_and_registration.graphic_full_scholarship_subtitle';

                    // special logic: we pre-check the profile share box for these folks
                    scope.cohortApplication.shareable_with_classmates = true;
                }

                scope.earlyRegistrationDate = dateHelper.formattedUserFacingMonthDayYearLong(
                    scope.currentUser.relevant_cohort.earlyRegistrationDeadline,
                );
                scope.earlyRegistrationShortDate = dateHelper.formattedUserFacingMonthDayShort(
                    scope.currentUser.relevant_cohort.earlyRegistrationDeadline,
                );
                scope.earlyRegistrationPassed = scope.cohortApplication.earlyRegistrationDeadlineHasPassed;

                // show partial scholarship heading
                scope.showScholarshipHeading =
                    scope.cohortApplication.hasPartialScholarship &&
                    !scope.cohortApplication.hasMadePayments &&
                    !scope.cohortApplication.tuitionAllPaid;

                scope.beforeRegistrationDeadline =
                    !scope.currentUser.relevant_cohort.registrationDeadlineHasPassed &&
                    !scope.currentUser.relevant_cohort.afterRegistrationOpenDate;

                if (!scope.cohortApplication.hasFullScholarship && !scope.cohortApplication.hasMadePayments) {
                    scope.wireTransferSmallprintKey = 'settings.tuition_and_registration.wire_transfer_smallprint';
                }

                if (scope.cohortApplication.cohort_program_guide_url) {
                    scope.subheaderKey = 'settings.tuition_and_registration.subheader';
                    scope.smallprintKey = 'settings.tuition_and_registration.smallprint';
                } else {
                    scope.subheaderKey = 'settings.tuition_and_registration.subheader_no_guide';
                    scope.smallprintKey = 'settings.tuition_and_registration.smallprint_no_guide';
                }

                // Additional text if user is in the Cordova app
                if (scope.needsToOpenInBrowser) {
                    scope.newBrowserKey = 'settings.tuition_and_registration.new_browser_register';
                }

                // Generate various locale strings for default / existing plan descriptions
                const existingOrDefaultPlan = scope.cohortApplication.existingOrDefaultPlan;
                scope.narrativeTranslateValues = {
                    // payment counts (generalized)
                    tuitionNumSuccessfulPayments: scope.cohortApplication.tuitionNumSuccessfulPayments,
                    tuitionNumPaymentsRemaining: scope.cohortApplication.tuitionNumPaymentsRemaining,

                    // default / promoted plan relevance
                    tuitionOriginalTotal: scope.cohortApplication.defaultPlanOriginalTotal,
                    tuitionScholarshipTotal: scope.cohortApplication.defaultPlanScholarshipTotal,

                    // existing or default plan relevance
                    tuitionNumPaymentIntervals: scope.cohortApplication.getNumIntervalsForPlan(existingOrDefaultPlan),
                    tuitionActualTotal:
                        scope.cohortApplication.defaultPlanOriginalTotal -
                        scope.cohortApplication.defaultPlanScholarshipTotal,
                    paymentPerInterval: scope.cohortApplication.getPlanPaymentPerIntervalForDisplay(
                        existingOrDefaultPlan,
                    ),
                };

                // Handle plan selection options and messaging
                const hasMultiplePlans =
                    scope.cohortApplication.stripe_plans && scope.cohortApplication.stripe_plans.length > 1;
                if (scope.cohortApplication.hasMadePayments) {
                    scope.supportsMultiPlan = false;
                    scope.planSelectionProxy = {
                        selectedPlanId: scope.cohortApplication.stripe_plan_id,
                    };

                    scope.narrativeKey = 'settings.tuition_and_registration.narrative_partial_payment';
                } else {
                    scope.supportsMultiPlan = hasMultiplePlans && !scope.cohortApplication.hasFullScholarship;
                    scope.planSelectionProxy = {
                        selectedPlanId: scope.cohortApplication.defaultPlan.id,
                    };
                }

                // if the user is all paid, just display a thank you note
                if (scope.cohortApplication.tuitionAllPaid) {
                    scope.narrativeKey = 'settings.tuition_and_registration.narrative_all_paid';
                }

                const translationHelper = new TranslationHelper('settings.tuition_and_registration');

                scope.openOptionsModal = () => {
                    const registrationText = translationHelper.get(
                        scope.earlyRegistrationPassed ? 'make_single_payment' : 'make_single_payment_and_save',
                        {
                            savings: scope.cohortApplication.additionalSavingsPerIntervalPerPlan(
                                scope.cohortApplication.defaultPlan,
                            ),
                            date: scope.earlyRegistrationDate,
                        },
                    );
                    DialogModal.alert({
                        title: translationHelper.get('choose_tuition'),
                        content: `<p><b translate-once="settings.tuition_and_registration.option_one"></b><br />${registrationText}</p><p><b translate-once="settings.tuition_and_registration.option_two"></b><br />${translationHelper.get(
                            scope.earlyRegistrationPassed ? 'make_twelve_payments' : 'make_twelve_payments_and_save',
                        )}</p><p translate-once="settings.tuition_and_registration.option_bank_wire"></p><p translate-once="settings.tuition_and_registration.refund_policy"></p>`,
                        classes: ['emba-billing-options'],
                    });
                };

                //-------------------------
                // Display Helpers
                //-------------------------

                scope.register = () => {
                    if (scope.needsToOpenInBrowser) {
                        scope.loadUrl(`${$window.ENDPOINT_ROOT}/settings/application_status`, '_blank');
                    } else {
                        scope.checkoutHelper.registerForCohort(scope.planSelectionProxy.selectedPlanId);
                    }
                };

                scope.orderByPlanInterval = stripePlan => (stripePlan.frequency === 'once' ? 0 : 1);
            },
        };
    },
]);
