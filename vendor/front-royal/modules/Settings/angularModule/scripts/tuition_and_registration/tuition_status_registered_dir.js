import angularModule from 'Settings/angularModule/scripts/settings_module';
import { setupBrandEmailProperties } from 'AppBrandMixin';
import template from 'Settings/angularModule/views/tuition_and_registration/tuition_status_registered.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

import iconWarningWhite from 'vectors/icon-warning-white.svg';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('tuitionStatusRegistered', [
    '$injector',

    function factory($injector) {
        const StripeCheckoutHelper = $injector.get('Payments.CohortRegistrationCheckoutHelper');
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
        const $window = $injector.get('$window');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                cohortApplication: '<',
                launchProgramGuide: '&',
            },
            link(scope) {
                scope.iconWarningWhite = iconWarningWhite;

                //-------------------------
                // Initialization
                //-------------------------

                setupBrandEmailProperties($injector, scope, ['emba']);
                NavigationHelperMixin.onLink(scope);

                scope.checkoutHelper = new StripeCheckoutHelper();
                scope.modifyEnabled = true;

                Object.defineProperty(scope, 'fixPaymentButtonKey', {
                    get() {
                        if (scope.checkoutHelper.stripeProcessing) {
                            return 'settings.tuition_and_registration.processing';
                        }
                        if (scope.needsToOpenInBrowser) {
                            return 'settings.tuition_and_registration.launch_browser';
                        }
                        return 'settings.tuition_and_registration.registered_fix_payment';
                    },
                });

                Object.defineProperty(scope, 'modifyPaymentButtonKey', {
                    get() {
                        if (scope.checkoutHelper.stripeProcessing) {
                            return 'settings.tuition_and_registration.processing';
                        }
                        if (scope.needsToOpenInBrowser) {
                            return 'settings.tuition_and_registration.launch_browser';
                        }
                        return 'settings.tuition_and_registration.registered_modify_payment';
                    },
                });

                scope.cohortTitle = scope.cohortApplication.cohort_title;
                scope.needsToOpenInBrowser = $window.CORDOVA;

                //-------------------------
                // Display Helpers
                //-------------------------

                scope.modify = () => {
                    if (scope.needsToOpenInBrowser) {
                        scope.loadUrl(`${$window.ENDPOINT_ROOT}/settings/application_status`, '_blank');
                    } else {
                        scope.checkoutHelper.modifyPaymentDetails();
                    }
                };

                //-------------------------
                // Locale Keys
                //-------------------------

                // Additional text if user is in the Cordova app
                if (scope.needsToOpenInBrowser) {
                    scope.newBrowserKey = 'settings.tuition_and_registration.new_browser_payment_details';
                }

                // Small print message can vary based on plan type
                scope.smallprintKey = 'settings.tuition_and_registration.registered_payment_smallprint';
                if (scope.cohortApplication.existingPlan && !scope.cohortApplication.hasFullScholarship) {
                    if (scope.cohortApplication.existingPlan.frequency === 'bi_annual') {
                        scope.smallprintKey += '_bi_annual';
                    }
                    // 'once' is not applicable here
                }
            },
        };
    },
]);
