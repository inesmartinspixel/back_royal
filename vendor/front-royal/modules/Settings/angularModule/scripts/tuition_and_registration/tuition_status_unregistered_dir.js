import angularModule from 'Settings/angularModule/scripts/settings_module';
import { setupBrandEmailProperties } from 'AppBrandMixin';
import template from 'Settings/angularModule/views/tuition_and_registration/tuition_status_unregistered.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

import scholarshipRibbonBlue from 'vectors/scholarship_ribbon_blue.svg';
import planSaveBanner from 'vectors/plan_save_banner.svg';
import planSaveBannerMobile from 'vectors/plan_save_banner_mobile.svg';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('tuitionStatusUnregistered', [
    '$injector',

    function factory($injector) {
        const $rootScope = $injector.get('$rootScope');
        const dateHelper = $injector.get('dateHelper');
        const StripeCheckoutHelper = $injector.get('Payments.CohortRegistrationCheckoutHelper');
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
        const $window = $injector.get('$window');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                cohortApplication: '<',
                launchProgramGuide: '&',
            },
            link(scope) {
                scope.scholarshipRibbonBlue = scholarshipRibbonBlue;
                scope.planSaveBanner = planSaveBanner;
                scope.planSaveBannerMobile = planSaveBannerMobile;

                //-------------------------
                // Initialization
                //-------------------------

                setupBrandEmailProperties($injector, scope, ['emba', 'billing']);
                NavigationHelperMixin.onLink(scope);

                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                scope.checkoutHelper = new StripeCheckoutHelper();
                scope.$watch('currentUser.relevant_cohort.afterRegistrationOpenDate', val => {
                    scope.registerEnabled = !!val;
                });

                Object.defineProperty(scope, 'buttonKey', {
                    get() {
                        if (scope.checkoutHelper.stripeProcessing) {
                            return 'settings.tuition_and_registration.processing';
                        }
                        if (scope.needsToOpenInBrowser) {
                            return 'settings.tuition_and_registration.launch_browser';
                        }
                        return 'settings.tuition_and_registration.register';
                    },
                });

                scope.needsToOpenInBrowser = $window.CORDOVA;
                scope.cohortTitle = scope.cohortApplication.cohort_title;

                scope.registrationDeadline = dateHelper.formattedUserFacingMonthDayYearLong(
                    scope.currentUser.relevant_cohort.registrationDeadline,
                );
                scope.registrationOpenDate = dateHelper.formattedUserFacingMonthDayYearLong(
                    scope.currentUser.relevant_cohort.registrationOpenDate,
                    false,
                );

                //-------------------------
                // Locale Keys
                //-------------------------

                // no scholarship
                if (scope.cohortApplication.hasNoScholarship) {
                    scope.headerKey = 'settings.tuition_and_registration.header_choose_your_tuition_plan';
                    scope.narrativeKey = 'settings.tuition_and_registration.narrative_no_scholarship';
                    scope.graphicTitleKey = 'settings.tuition_and_registration.graphic_no_scholarship_title';
                    scope.graphicSubtitleKey = 'settings.tuition_and_registration.graphic_no_scholarship_subtitle';
                }
                // partial scholarship
                else if (scope.cohortApplication.hasPartialScholarship) {
                    scope.headerKey = 'settings.tuition_and_registration.header_choose_your_tuition_plan';
                    scope.narrativeKey = 'settings.tuition_and_registration.narrative_partial_scholarship';
                    scope.graphicTitleKey = 'settings.tuition_and_registration.graphic_partial_scholarship_title';
                    scope.graphicSubtitleKey = 'settings.tuition_and_registration.graphic_partial_scholarship_subtitle';
                }
                // full scholarship
                else if (scope.cohortApplication.hasFullScholarship) {
                    scope.headerKey = 'settings.tuition_and_registration.header_register';
                    scope.narrativeKey = 'settings.tuition_and_registration.narrative_full_scholarship';
                    scope.graphicTitleKey = 'settings.tuition_and_registration.graphic_full_scholarship_title';
                    scope.graphicSubtitleKey = 'settings.tuition_and_registration.graphic_full_scholarship_subtitle';

                    // special logic: we pre-check the profile share box for these folks
                    scope.cohortApplication.shareable_with_classmates = true;
                } else {
                    throw new Error(
                        `Invalid scholarship amount: ${scope.cohortApplication.defaultPlanScholarshipTotal} for original tuition ${scope.cohortApplication.defaultPlanOriginalTotal}`,
                    );
                }

                // set this to false if it is null
                if (!scope.cohortApplication.shareable_with_classmates) {
                    scope.cohortApplication.shareable_with_classmates = false;
                }

                // show partial scholarship heading
                scope.showScholarshipHeading =
                    (scope.cohortApplication.hasPartialScholarship || scope.cohortApplication.hasFullScholarship) &&
                    !scope.cohortApplication.hasMadePayments &&
                    !scope.cohortApplication.tuitionAllPaid;

                if (
                    scope.currentUser.relevant_cohort.registrationDeadlineHasPassed &&
                    scope.currentUser.relevant_cohort.afterRegistrationOpenDate
                ) {
                    scope.confirmMessageKey = 'settings.tuition_and_registration.confirm_message_past_deadline';
                } else if (scope.currentUser.relevant_cohort.afterRegistrationOpenDate) {
                    scope.confirmMessageKey = 'settings.tuition_and_registration.confirm_message';
                } else {
                    // if registration has not yet started, then the message is up in the header
                }

                if (!scope.cohortApplication.hasFullScholarship && !scope.cohortApplication.hasMadePayments) {
                    scope.wireTransferSmallprintKey = 'settings.tuition_and_registration.wire_transfer_smallprint';
                }

                if (scope.cohortApplication.cohort_program_guide_url) {
                    scope.smallprintKey = 'settings.tuition_and_registration.smallprint';
                } else {
                    scope.smallprintKey = 'settings.tuition_and_registration.smallprint_no_guide';
                }

                // Additional text if user is in the Cordova app
                if (scope.needsToOpenInBrowser) {
                    scope.newBrowserKey = 'settings.tuition_and_registration.new_browser_register';
                }

                // Generate various locale strings for default / existing plan descriptions
                const existingOrDefaultPlan = scope.cohortApplication.existingOrDefaultPlan;
                scope.narrativeTranslateValues = {
                    // payment counts (generalized)
                    tuitionNumSuccessfulPayments: scope.cohortApplication.tuitionNumSuccessfulPayments,
                    tuitionNumPaymentsRemaining: scope.cohortApplication.tuitionNumPaymentsRemaining,

                    // default / promoted plan relevance
                    tuitionOriginalTotal: scope.cohortApplication.defaultPlanOriginalTotal,
                    tuitionScholarshipTotal: scope.cohortApplication.defaultPlanScholarshipTotal,

                    // existing or default plan relevance
                    tuitionNumPaymentIntervals: scope.cohortApplication.getNumIntervalsForPlan(existingOrDefaultPlan),
                    tuitionActualTotal: scope.cohortApplication.getPlanTuitionActualTotal(existingOrDefaultPlan),
                    paymentPerInterval: scope.cohortApplication.getPlanPaymentPerIntervalForDisplay(
                        existingOrDefaultPlan,
                    ),
                };

                const existingPlanIsBiAnnual =
                    scope.cohortApplication.existingPlan &&
                    scope.cohortApplication.existingPlan.frequency === 'bi_annual';
                if (existingPlanIsBiAnnual && !scope.cohortApplication.hasFullScholarship) {
                    scope.graphicTitleKey += '_bi_annual';
                }

                // Handle plan selection options and messaging
                const hasMultiplePlans =
                    scope.cohortApplication.stripe_plans && scope.cohortApplication.stripe_plans.length > 1;
                if (scope.cohortApplication.hasMadePayments) {
                    scope.supportsMultiPlan = false;
                    scope.planSelectionProxy = {
                        selectedPlanId: scope.cohortApplication.stripe_plan_id,
                    };

                    // override messaging with partial-payment messaging
                    if (existingPlanIsBiAnnual) {
                        scope.narrativeKey = 'settings.tuition_and_registration.narrative_partial_payment_bi_annual';
                    } else {
                        scope.narrativeKey = 'settings.tuition_and_registration.narrative_partial_payment';
                    }
                } else {
                    scope.supportsMultiPlan = hasMultiplePlans && !scope.cohortApplication.hasFullScholarship;
                    scope.planSelectionProxy = {
                        selectedPlanId: scope.cohortApplication.defaultPlan.id,
                    };
                }

                // if the user is all paid, just display a thank you note
                if (scope.cohortApplication.tuitionAllPaid) {
                    scope.headerKey = 'settings.tuition_and_registration.header_register';
                    scope.narrativeKey = 'settings.tuition_and_registration.narrative_all_paid';
                }

                //-------------------------
                // Display Helpers
                //-------------------------

                scope.register = () => {
                    if (scope.needsToOpenInBrowser) {
                        scope.loadUrl(`${$window.ENDPOINT_ROOT}/settings/application_status`, '_blank');
                    } else {
                        scope.checkoutHelper.registerForCohort(scope.planSelectionProxy.selectedPlanId);
                    }
                };
            },
        };
    },
]);
