import angularModule from 'Settings/angularModule/scripts/settings_module';
import { setupBrandNameProperties, setupScopeProperties } from 'AppBrandMixin';
import template from 'Settings/angularModule/views/tuition_and_registration/emba_welcome_package.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

import arrowDownFadedWhite from 'vectors/arrow_down_faded_white.svg';
import programGuideBadge from 'vectors/program-guide-badge-gray.svg';
import programGuideBadgeQuantic from 'vectors/program-guide-badge-gray_quantic.svg';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('embaWelcomePackage', [
    '$injector',

    function factory($injector) {
        const $sce = $injector.get('$sce');
        const $rootScope = $injector.get('$rootScope');
        const CareerProfile = $injector.get('CareerProfile');
        const WorkExperience = $injector.get('WorkExperience');
        const EducationExperience = $injector.get('EducationExperience');
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                cohortApplication: '<',
                launchProgramGuide: '&',
            },
            link(scope) {
                scope.arrowDownFadedWhite = arrowDownFadedWhite;

                //-------------------------
                // Initialization
                //-------------------------

                NavigationHelperMixin.onLink(scope);
                setupBrandNameProperties($injector, scope);
                setupScopeProperties($injector, scope, [
                    { prop: 'programGuideBadgeImgSrc', quantic: programGuideBadgeQuantic, fallback: programGuideBadge },
                    {
                        prop: 'welcomeVideoSrc',
                        quantic: $sce.trustAsResourceUrl(
                            'https://www.youtube.com/embed/plxX1MYx-CY?rel=0&amp;showinfo=0',
                        ),
                        fallback: $sce.trustAsResourceUrl(
                            'https://www.youtube.com/embed/UHdUtaaB9ng?rel=0&amp;showinfo=0',
                        ),
                    },
                ]);

                // needed for AppBrandMixin
                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                // hard-coded data for now; fetch from server later
                function mockStudent(name, locationString, jobTitle, company, university, degree, avatarUrl) {
                    const profile = CareerProfile.new({
                        name,
                        work_experiences: [
                            WorkExperience.new({
                                professional_organization: {
                                    text: company,
                                },
                                job_title: jobTitle,
                                featured: true,
                            }),
                        ],
                        education_experiences: [
                            EducationExperience.new({
                                educational_organization: {
                                    text: university,
                                },
                                degree,
                                degree_program: true,
                                will_not_complete: false,
                            }),
                        ],
                        avatar_url: avatarUrl,
                    });
                    Object.defineProperty(profile, 'locationString', {
                        value: locationString,
                    });
                    return profile;
                }

                scope.students = [
                    // dubrovskiyad@gmail.com
                    mockStudent(
                        'Alexey Dubrovskiy',
                        'Zürich, CH',
                        'Senior Software Engineer',
                        'Google',
                        'Lomonosov Moscow State University',
                        'Ph.D.',
                        'https://uploads.smart.ly/emba_welcome_package/profiles/bf5b23539980d40437fbe8d0a9b5e7f5.1571041476.jpeg',
                    ),

                    // catherine.richards@icloud.com
                    mockStudent(
                        'Catherine Richards',
                        'London, England, GB',
                        'Researcher',
                        'Centre for the Study of Existential Risk',
                        'University of Cambridge',
                        'Ph.D.',
                        'https://uploads.smart.ly/emba_welcome_package/profiles/907c23654e7d65ff78508126bbb1f6a0.1563114165.CERHeadshot.jpg',
                    ),

                    // angelhuiqi@hotmail.com
                    mockStudent(
                        'Huiqi Wang',
                        'Princeton, NJ, US',
                        'Assistant Vice President',
                        'Bank of America',
                        'Rutgers University',
                        'M.S.',
                        'https://uploads.smart.ly/emba_welcome_package/profiles/c39dc803c9eda10518f3da1e4183d873.1579571698.jpeg',
                    ),

                    // kdeluwa@gmail.com
                    mockStudent(
                        'Kelechi David Eluwa',
                        'Oro Valley, AZ, US',
                        'Senior Manager, Systems Integration',
                        'ROCHE',
                        'Johns Hopkins University',
                        'M.S.',
                        'https://uploads.smart.ly/emba_welcome_package/profiles/1016d224d004e0323d4418fe1ba58195.1574877586.Professional_Photo_1.jpg',
                    ),

                    // jessicawatson11@googlemail.com
                    mockStudent(
                        'Jessica Watson',
                        'Canterbury, England, GB',
                        'Sales Strategy Leader E-Commerce',
                        'Procter & Gamble',
                        'University of Oxford',
                        'B.A.Sc.',
                        'https://uploads.smart.ly/emba_welcome_package/profiles/jessica.png',
                    ),

                    // bryan.chuah@gmail.com
                    mockStudent(
                        'Bryan Chuah',
                        'Singapore, SG',
                        'Regional Director',
                        'AIG',
                        'University of Michigan',
                        'M.Sc.',
                        'https://uploads.smart.ly/emba_welcome_package/profiles/91ab4427f250aebc3b75d80986511c1b.1575107717.jpeg',
                    ),
                ];

                //-------------------------
                // Locale Keys
                //-------------------------

                // if we have students to preview for your class
                if (scope.cohortApplication.cohort_name === 'EMBA14') {
                    scope.headlineKey = 'settings.emba_welcome_package.headline_few_of_your_registered_classmates';
                    // otherwise just show recent students
                } else {
                    scope.headlineKey = 'settings.emba_welcome_package.headline_these_students_registered';
                }
            },
        };
    },
]);
