import angularModule from 'Settings/angularModule/scripts/settings_module';
import { setupBrandNameProperties, setupBrandEmailProperties } from 'AppBrandMixin';
import template from 'Settings/angularModule/views/edit_account.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('editAccount', [
    '$injector',

    function factory($injector) {
        const $auth = $injector.get('$auth');
        const $rootScope = $injector.get('$rootScope');
        const ngToast = $injector.get('ngToast');
        const isMobileMixin = $injector.get('isMobileMixin');
        const ValidationResponder = $injector.get('ValidationResponder');
        const TranslationHelper = $injector.get('TranslationHelper');
        const scrollHelper = $injector.get('scrollHelper');
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');

        return {
            restrict: 'E',
            templateUrl,
            scope: {},
            link(scope) {
                //-------------------------
                // Initialization
                //-------------------------

                NavigationHelperMixin.onLink(scope);
                setupBrandNameProperties($injector, scope);
                setupBrandEmailProperties($injector, scope, ['support']);

                // Setup localization keys
                const translationHelper = new TranslationHelper('settings.edit_account');

                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                //-------------------------
                // Navigation / Actions
                //-------------------------

                scope.preventSubmit = false;

                scope.form = {
                    name: scope.currentUser.name,
                    nickname: scope.currentUser.nickname,
                    email: scope.currentUser.email,
                    phone: scope.currentUser.phone,
                };

                scope.updateAccount = () => {
                    scope.preventSubmit = true;

                    $auth.updateAccount(scope.form).then(
                        () => {
                            scope.preventSubmit = false;
                            ngToast.create({
                                content: translationHelper.get('profile_update_success'),
                                className: 'success',
                            });

                            // see https://trello.com/c/xa4zY5EU/260-0-5-iguana-provisional-changes
                            // for maybe a nicer way of handling this
                            scope.currentUser.name = scope.form.name;
                            scope.currentUser.nickname = scope.form.nickname;
                            scope.currentUser.email = scope.form.email;
                            scope.currentUser.phone = scope.form.phone;

                            // re-identify
                            ValidationResponder.identifyCurrentUser();
                        },
                        response => {
                            let message = translationHelper.get('profile_update_failure');
                            if (
                                response &&
                                response.data &&
                                response.data.errors &&
                                response.data.errors.full_messages &&
                                response.data.errors.full_messages.length > 0
                            ) {
                                message = response.data.errors.full_messages[0];
                            }
                            scope.preventSubmit = false;
                            ngToast.create({
                                content: message,
                                className: 'danger',
                            });
                        },
                    );
                };

                scope.openChangePassword = () => {
                    scope.showChangePasswordForm = true;
                    scope.showProfileForm = false;
                    scrollHelper.scrollToTop();
                };

                scope.closeChangePassword = () => {
                    scope.showChangePasswordForm = false;
                    scope.showProfileForm = true;
                    scrollHelper.scrollToTop();
                };

                //-------------------------
                // Display Helpers
                //-------------------------

                isMobileMixin.onLink(scope);

                const providersAllowingEmailUpdates = [
                    'email',
                    'facebook',
                    'google_oauth2',
                    'apple_quantic',
                    'apple_smartly',
                ];

                function updateViewHelpers() {
                    // prevent RTE on logout
                    if (!scope.currentUser) {
                        return;
                    }

                    scope.allowEmailUpdate =
                        providersAllowingEmailUpdates.includes(scope.currentUser.provider) &&
                        !scope.currentUser.loggedInWithSAML;
                    scope.phoneNoPassword = scope.currentUser.provider === 'phone_no_password';

                    // conditional display based on user type
                    scope.showProfileForm = true;
                    scope.showChangePasswordForm = !scope.isMobile;
                    scope.userCanHavePassword = !scope.currentUser.loggedInWithSAML && !scope.phoneNoPassword;
                    scope.showDeferInfo =
                        scope.currentUser.acceptedOrPreAcceptedCohortApplication &&
                        scope.currentUser.relevant_cohort &&
                        scope.currentUser.relevant_cohort.supportsSchedule &&
                        scope.brandNameShort &&
                        scope.brandSupportEmail;
                    scope.showAccountDeletionInfo = scope.brandNameShort && scope.brandSupportEmail;
                }

                // easier to test if this is in a watch, even though it will not change
                scope.$watch('currentUser.provider', updateViewHelpers);
                updateViewHelpers();

                scope.$watch('isMobile', (newVal, oldVal) => {
                    if (newVal && !oldVal) {
                        // hide the change password if we're transitioning to mobile
                        scope.showChangePasswordForm = false;
                    } else if (!newVal && oldVal) {
                        // otherwise show everything if we're transitioning to desktop
                        scope.showChangePasswordForm = true;
                        scope.showProfileForm = true;
                    }

                    // On mobile, the submit button for the password form says "Save Changes",
                    // On desktop, since there is already a button that says "Save Changes",
                    // it is "Change Password".
                    if (newVal) {
                        scope.changePasswordSubmitText = translationHelper.get('save_changes');
                    } else {
                        scope.changePasswordSubmitText = translationHelper.get('change_password');
                    }
                });
            },
        };
    },
]);
