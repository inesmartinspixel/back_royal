import angularModule from 'Settings/angularModule/scripts/settings_module';
import { setupBrandNameProperties, setupBrandEmailProperties } from 'AppBrandMixin';
import template from 'Settings/angularModule/views/settings.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('settings', [
    '$injector',
    function factory($injector) {
        const $location = $injector.get('$location');
        const $rootScope = $injector.get('$rootScope');
        const SiteMetadata = $injector.get('SiteMetadata');
        const isMobileMixin = $injector.get('isMobileMixin');
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
        const AppHeaderViewModel = $injector.get('Navigation.AppHeader.AppHeaderViewModel');
        const TranslationHelper = $injector.get('TranslationHelper');
        const EditCareerProfileHelper = $injector.get('EditCareerProfileHelper');
        const ProfileCompletionHelper = $injector.get('ProfileCompletionHelper');
        const ClientStorage = $injector.get('ClientStorage');
        const dateHelper = $injector.get('dateHelper');
        const $sce = $injector.get('$sce');
        const Cohort = $injector.get('Cohort');
        const $window = $injector.get('$window');
        const TranscriptHelper = $injector.get('TranscriptHelper');
        const scopeTimeout = $injector.get('scopeTimeout');
        const CohortApplication = $injector.get('CohortApplication');

        return {
            restrict: 'E',
            scope: {
                section: '@', // see also: availableSections,
                page: '@',
            },
            templateUrl,

            link(scope, elem) {
                setupBrandNameProperties($injector, scope);
                setupBrandEmailProperties($injector, scope, ['emba']);

                // convenience getter
                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                Object.defineProperty(scope, 'showPreviewProfileStatusMessage', {
                    get() {
                        return (
                            scope.section === 'my-profile' &&
                            scope.currentUser &&
                            (!scope.currentUser.can_edit_career_profile ||
                                (_.contains(
                                    CohortApplication.VALID_ENROLLMENT_STATUSES,
                                    scope.currentUser.lastCohortApplicationStatus,
                                ) &&
                                    Cohort.isDegreeProgram(scope.currentUser.programType) &&
                                    scope.currentUser.career_profile.last_calculated_complete_percentage !== 100))
                        );
                    },
                    configurable: true,
                });

                scope.transcriptHelper = new TranscriptHelper();

                // If we are direct linking to the application page and the user is still allowed to edit it then automatically
                // enable reapplyingOrEditingApplication. This allows us to support direct links to the application after submission
                // (see https://trello.com/c/7YfFm51d). Similarly, if a user's lastCohortApplication indicates that they can_convert_to_emba,
                // automatically enable this flag so that they're shown the program lockdown/conversion screen immediately, bypassing the
                // need for them to click the "Edit Application" button (see https://trello.com/c/a4lgBXgW).
                if (
                    (scope.currentUser &&
                        $location.path() === '/settings/application' &&
                        scope.currentUser.canEditCurrentCohortApplication) ||
                    (scope.currentUser.lastCohortApplication &&
                        scope.currentUser.lastCohortApplication.can_convert_to_emba)
                ) {
                    scope.currentUser.reapplyingOrEditingApplication = true;
                }

                // Be proactive about setting this flag back to false if the user has changed to accepted, pre_accepted, or rejected (and can't yet reapply).
                // We have seen issues where users have reapplyingOrEditingApplication set to `true` in localStorage, because they clicked "edit application"
                // on /application_status, and thus saw the application page then they should not have.
                // We have a ticket to check the more general case of not being able to edit the application, but that will require some
                // work and thought since we use this flag also for the case where a rejected user is reapplying (which would fail the deadline check).
                // Ultimately, we might consider moving this flag to the database. See https://trello.com/c/8auFB4fn.
                // NOTE: this logic should mirror the logic in current_user_interceptor.js#response
                if (
                    scope.currentUser.acceptedOrPreAcceptedCohortApplication ||
                    (scope.currentUser.lastCohortApplication?.status === 'rejected' &&
                        !scope.currentUser.lastCohortApplication.canReapplyTo(scope.currentUser.relevant_cohort))
                ) {
                    scope.currentUser.reapplyingOrEditingApplication = false;
                }

                scope.dateHelper = dateHelper;
                NavigationHelperMixin.onLink(scope);
                isMobileMixin.onLink(scope);

                // support legacy URLs for awhile
                if (scope.section === 'mba') {
                    scope.section = 'application_status';
                }

                // Setup localization keys
                const translationHelper = new TranslationHelper('settings.settings');
                const editCareerProfileTranslationHelper = new TranslationHelper('careers.edit_career_profile');

                AppHeaderViewModel.setBodyBackground('beige');
                AppHeaderViewModel.setTitleHTML(translationHelper.get('settings_title'));
                AppHeaderViewModel.showAlternateHomeButton = false;

                // We hide the app header on mobile on this screen
                scope.$watch('isMobile', () => {
                    AppHeaderViewModel.toggleVisibility(!scope.isMobile);
                });

                // Default title
                SiteMetadata.updateHeaderMetadata();

                // reapplyingToMba was the old name of this item, so we keep it to support clients who saved it
                // before the update
                if (ClientStorage.getItem('reapplyingToMba') === 'true') {
                    scope.currentUser.reapplyingOrEditingApplication = true;
                    ClientStorage.removeItem('reapplyingToMba');
                }

                const hiringManagerProfileHeaders = {
                    company: scope.currentUser.hiring_application
                        ? scope.currentUser.hiring_application.company_name
                        : null,
                    team: translationHelper.get('team_header'),
                    you: scope.currentUser.name,
                    candidates: translationHelper.get('candidates_header'),
                    preview: translationHelper.get('preview_header'),
                };
                const hiringManagerProfileSubHeaders = {
                    company: translationHelper.get('company_sub_header'),
                    team: translationHelper.get('team_sub_header'),
                    you: translationHelper.get('you_sub_header'),
                    candidates: translationHelper.get('candidates_sub_header'),
                    preview: translationHelper.get('preview_sub_header'),
                };

                const applicationSubHeaders = {
                    mba_application_questions: editCareerProfileTranslationHelper.get(
                        'mba_application_questions_sub_header',
                    ),
                    emba_application_questions: editCareerProfileTranslationHelper.get(
                        'emba_application_questions_sub_header',
                    ),
                    cert_application_questions: editCareerProfileTranslationHelper.get(
                        'cert_application_questions_sub_header',
                    ),
                    select_skills: editCareerProfileTranslationHelper.get('select_skills_desc'),
                    resume_and_links: editCareerProfileTranslationHelper.get('resume_and_links_desc'),
                    student_network_settings: editCareerProfileTranslationHelper.get('student_network_settings_desc'),
                };

                const careerProfileSubHeaders = {
                    preview_profile: editCareerProfileTranslationHelper.get('hiring_managers_will_see_this'),
                    select_skills: editCareerProfileTranslationHelper.get('select_skills_desc'),
                    resume_and_links: editCareerProfileTranslationHelper.get('resume_and_links_desc'),
                    student_network_settings: editCareerProfileTranslationHelper.get('student_network_settings_desc'),
                };

                //-------------------------
                // Build Section Menu
                //-------------------------

                scope.hiringManagerApplicationFormSteps = $injector.get('HIRING_MANAGER_FORM_STEPS');

                scope.availableSections = [];

                if (scope.currentUser.canApplyToSmartly) {
                    // show mba application if user has no cohort applications, i.e.: they're new here, OR if explicitly editing (but not accepted)
                    if (
                        !scope.currentUser.lastCohortApplicationStatus ||
                        (scope.currentUser.reapplyingOrEditingApplication && !scope.currentUser.isAccepted)
                    ) {
                        scope.availableSections.push({
                            name: 'application',
                            subsections: EditCareerProfileHelper.getStepsForApplicationForm(scope.currentUser),
                        });
                    } else {
                        scope.availableSections.push({
                            name: 'application_status',
                        });
                    }
                }

                scope.availableSections.push({
                    name: 'account',
                });

                if (scope.currentUser.defaultsToHiringExperience) {
                    scope.availableSections.push({
                        name: 'profile',
                        subsections: scope.hiringManagerApplicationFormSteps,
                    });
                }

                if (scope.currentUser.hasEditCareerProfileAccess || scope.currentUser.hasAdminAccess) {
                    scope.availableSections.push({
                        name: 'my-profile',
                        subsections: EditCareerProfileHelper.getStepsForCareersForm(scope.currentUser),
                    });
                }

                if (scope.currentUser.requiresHiringSubscription) {
                    scope.availableSections.push({
                        name: 'billing',
                    });
                }

                // Don't add notifications section for users without email capability
                if (scope.currentUser.provider !== 'phone_no_password') {
                    scope.availableSections.push({
                        name: 'notifications',
                    });
                }

                scope.availableSections.push({
                    name: 'preferences',
                });

                // only add Application section if user is Application enabled
                if (
                    scope.currentUser.canApplyToSmartly &&
                    Cohort.supportsDocumentUpload(scope.currentUser.programType) &&
                    scope.currentUser.hasEverApplied
                ) {
                    scope.availableSections.push({
                        name: 'documents',
                    });
                }

                // Will be re-enabled after https://trello.com/c/IpsPdjYT
                // if (scope.transcriptHelper.allowTranscriptDownload(scope.currentUser)) {
                //     scope.availableSections.push({
                //         name: 'transcripts'
                //     });
                // }

                scope.availableSections.push({
                    name: 'sign_out',
                });

                //---------------------------------------------------------
                // Steps & Percent Complete handling - HiringApplication
                //---------------------------------------------------------

                // NOTE: on scope for specs
                scope.setHiringProfileStepsProgressMap = () => {
                    // prevent error if there is no hiring manager
                    if (!scope.currentUser || !scope.currentUser.hiring_application) {
                        return;
                    }

                    scope.hiringProfileStepsProgressMap = scope.currentUser.hiring_application.getStepsProgressMap();
                    scope.hiringProfilePercentComplete = scope.currentUser.hiring_application.percentComplete();
                };

                scope.setHiringProfileStepsProgressMap(); // ensure hiringProfileStepsProgressMap is set
                scope.$on('savedHiringManagerProfile', scope.setHiringProfileStepsProgressMap); // see edit_hiring_manager_profile_dir.js for where this is emitted

                //------------------------------------------------------------
                // Steps & Percent Complete handling - Cohort Application
                //------------------------------------------------------------

                function setInitialStep() {
                    // default to the Program Choice screen
                    let defaultCurrentStep = '1';
                    if (!scope.currentUser) {
                        scope.initialStep = defaultCurrentStep;
                        return;
                    }

                    // if page is requested and valid, use that for initialStep
                    if (
                        scope.page &&
                        scope.cohortApplicationProfileFormSteps &&
                        scope.cohortApplicationProfileFormSteps[scope.page - 1]
                    ) {
                        scope.initialStep = scope.page;
                    } else {
                        // if the user cannot convert_to_emba, and the cohort supports it, then we want to bypass the Program Choice screen
                        if (
                            scope.section === 'application' &&
                            (!scope.currentUser.lastCohortApplication ||
                                !scope.currentUser.lastCohortApplication.can_convert_to_emba) &&
                            scope.currentUser.career_profile &&
                            Cohort.supportsSkippingProgramChoice(scope.currentUser.career_profile.program_type)
                        ) {
                            defaultCurrentStep = '2';
                        }

                        scope.initialStep = defaultCurrentStep;
                    }
                }

                const applicationProfileHeaders = {}; // see scope.getApplicationHeaderText

                function updateSectionHeaderAndSubsectionLocales() {
                    _.each(scope.cohortApplicationProfileFormSteps, step => {
                        applicationProfileHeaders[step.stepName] = editCareerProfileTranslationHelper.get(
                            step.stepName,
                        );
                    });
                    const cohortApplicationStep = _.findWhere(scope.availableSections, {
                        name: 'application',
                    });
                    if (cohortApplicationStep) {
                        cohortApplicationStep.subsections = scope.cohortApplicationProfileFormSteps;
                    }
                }

                scope.setCohortApplicationProfileFormStepsAndSetCompletionPercentage = () => {
                    // prevent errors if not a learner
                    if (
                        !scope.currentUser ||
                        !scope.currentUser.career_profile ||
                        !scope.currentUser.canApplyToSmartly
                    ) {
                        return;
                    }
                    // get and set the new form steps, if user hasEditCareerProfileAccess then we need to get
                    // career form steps, otherwise, get application steps
                    scope.cohortApplicationProfileFormSteps = EditCareerProfileHelper.isCareerProfile()
                        ? EditCareerProfileHelper.getStepsForCareersForm(scope.currentUser)
                        : EditCareerProfileHelper.getStepsForApplicationForm(scope.currentUser);
                    if (scope.completionHelper) {
                        scope.completionHelper.setSteps(scope.cohortApplicationProfileFormSteps);
                    } else {
                        scope.completionHelper = new ProfileCompletionHelper(scope.cohortApplicationProfileFormSteps);
                    }
                    scope.cohortApplicationProfileStepsProgressMap = scope.completionHelper.getStepsProgressMap(
                        scope.currentUser.career_profile,
                    );

                    // The cohort application has different completion requirements than the career profile
                    // and the required fields for the cohort application can change depending on the program
                    // type the user has selected, so we need to recalculate the completion percentage every
                    // time the user saves their cohort application
                    scope.cohortApplicationPercentComplete = scope.completionHelper.getPercentComplete(
                        scope.currentUser.career_profile,
                    );

                    // ensure that the section locales are updated as well
                    updateSectionHeaderAndSubsectionLocales();
                };

                // invoke immediately to ensure `cohortApplicationProfileFormSteps` and `stepsProgressMap` gets
                // set prior to other necessary init tasks (such as `setInitialStep`)
                scope.setCohortApplicationProfileFormStepsAndSetCompletionPercentage();
                setInitialStep();

                scope.$on('savedCareerProfile', scope.setCohortApplicationProfileFormStepsAndSetCompletionPercentage); // see edit_career_profile_dir.js for where this is emitted

                scope.$on('savedCareerProfileWithStepChange', (e, stepName) => {
                    scope.setCohortApplicationProfileFormStepsAndSetCompletionPercentage();

                    const step = _.findIndex(
                        scope.cohortApplicationProfileFormSteps,
                        _step => _step.stepName === stepName,
                    );
                    scope.initialStep = step + 1;

                    // force a refresh
                    scope.section = 'account';
                    scopeTimeout(
                        scope,
                        () => {
                            scope.section = 'application';
                        },
                        1,
                    );
                }); // see submit_application_form_dir.js for where this is emitted

                // We need to ensure the documents section is present or removed when appropriate.
                // This is determined in part by the user's fallback_program_type, which they can
                // change via the program choice form, so we need to deteect when it changes and
                // add or remove the documents section as needed.
                scope.$watch('currentUser.fallback_program_type', () => {
                    if (scope.currentUser && scope.currentUser.hasEverApplied) {
                        // ensure documents section is present if necessary
                        const documentsStepIndex = _.findIndex(
                            scope.availableSections,
                            section => section.name === 'documents',
                        );
                        if (Cohort.supportsDocumentUpload(scope.currentUser.programType)) {
                            // insert documents section right before Sign Out section
                            if (documentsStepIndex === -1) {
                                scope.availableSections.splice(scope.availableSections.length - 1, 0, {
                                    name: 'documents',
                                });
                            }
                        } else if (documentsStepIndex !== -1) {
                            // remove the documents step if included
                            scope.availableSections.splice(documentsStepIndex, 1);
                        }
                    }
                });

                scope.$watch('currentUser.programType', () => {
                    if (scope.currentUser) {
                        if (scope.currentUser.isEMBA) {
                            applicationSubHeaders.personal_summary = editCareerProfileTranslationHelper.get(
                                'the_following_emba',
                            );
                            applicationSubHeaders.career_preferences = applicationSubHeaders.personal_summary;
                        } else if (scope.currentUser.isMBA) {
                            applicationSubHeaders.personal_summary = editCareerProfileTranslationHelper.get(
                                'the_following',
                            );
                            applicationSubHeaders.career_preferences = applicationSubHeaders.personal_summary;
                        } else {
                            applicationSubHeaders.personal_summary = undefined;
                            applicationSubHeaders.career_preferences = applicationSubHeaders.personal_summary;
                        }
                    }
                });

                //-------------------------
                // Navigation / Actions
                //-------------------------

                scope.gotoSection = section => {
                    if (section === 'sign_out') {
                        // don't bother injecting unless we're signing out
                        $injector.get('SignOutHelper').signOut();
                    } else if (_.pluck(scope.availableSections, 'name').includes(section)) {
                        $location.search({});

                        // see also: http://stackoverflow.com/questions/19012915/angularjs-redirect-without-pushing-a-history-state
                        $location.path(`/settings/${section}`).replace();
                    }

                    scope.mobileNavExpanded = false;
                };

                scope.gotoEditHiringManagerProfileStep = $index => {
                    scope.$broadcast('gotoFormStep', $index);
                    scope.mobileNavExpanded = false;
                };

                scope.gotoEditCareerProfileStep = $index => {
                    scope.$broadcast('gotoFormStep', $index);
                    scope.mobileNavExpanded = false;
                };

                scope.gotoComparePrograms = () => {
                    NavigationHelperMixin.loadUrl('/prices/students', '_blank');
                };

                scope.getHiringManagerProfileHeaderText = () =>
                    scope.hiringManagerProfileActiveStep && scope.hiringManagerProfileActiveStep.stepName
                        ? hiringManagerProfileHeaders[scope.hiringManagerProfileActiveStep.stepName]
                        : '';

                scope.getHiringManagerProfileSubHeaderText = () =>
                    scope.hiringManagerProfileActiveStep && scope.hiringManagerProfileActiveStep.stepName
                        ? hiringManagerProfileSubHeaders[scope.hiringManagerProfileActiveStep.stepName]
                        : '';

                scope.getApplicationHeaderText = () =>
                    scope.applicationProfileActiveStep && scope.applicationProfileActiveStep.stepName
                        ? applicationProfileHeaders[scope.applicationProfileActiveStep.stepName]
                        : '';

                scope.getApplicationProfileSubHeaderText = subheaders => {
                    let text;
                    if (subheaders === 'applicationSubHeaders') {
                        text =
                            scope.applicationProfileActiveStep && scope.applicationProfileActiveStep.stepName
                                ? applicationSubHeaders[scope.applicationProfileActiveStep.stepName]
                                : '';
                    } else if (subheaders === 'careerProfileSubHeaders') {
                        text =
                            scope.applicationProfileActiveStep && scope.applicationProfileActiveStep.stepName
                                ? careerProfileSubHeaders[scope.applicationProfileActiveStep.stepName]
                                : '';
                    }
                    $sce.trustAsHtml(text);
                    return text;
                };

                // This sets a viewModel object representing the state of the header, subheader, and checkmark for the
                // application_status header bar.
                scope.setApplicationStatusHeaderViewModel = () => {
                    if (
                        !(
                            scope.currentUser &&
                            scope.currentUser.lastCohortApplication &&
                            scope.currentUser.relevant_cohort
                        )
                    ) {
                        return null;
                    }

                    const viewModel = {
                        header: translationHelper.get('application_status'),
                        subheader: '',
                        showCheckmark: false,
                    };

                    const lastApplication = scope.currentUser.lastCohortApplication;

                    if (scope.currentUser.isFailed) {
                        viewModel.header = translationHelper.get('graduation_requirements_not_met');
                        viewModel.showCheckmark = false;
                    } else if (
                        _.contains(['pre_accepted', 'accepted'], lastApplication.status) &&
                        scope.currentUser.relevant_cohort.supportsRecurringPayments
                    ) {
                        if (lastApplication.registered && lastApplication.in_good_standing) {
                            viewModel.header = translationHelper.get('you_are_now_registered');
                            viewModel.subheader = translationHelper.get(
                                lastApplication.cohort_program_guide_url
                                    ? 'you_have_secured_enrollment_in_x'
                                    : 'you_have_secured_enrollment_in_x_no_guide',
                                {
                                    brandEmail: scope.brandEmbaEmail,
                                    cohortTitle: scope.currentUser.relevant_cohort.title,
                                },
                            );
                            viewModel.showCheckmark = true;
                        } else if (lastApplication.locked_due_to_past_due_payment) {
                            viewModel.header = translationHelper.get('congratulations_you_are_in');
                            viewModel.showCheckmark = true;
                        } else if (
                            !lastApplication.registered &&
                            !scope.currentUser.relevant_cohort.afterRegistrationOpenDate
                        ) {
                            // generally only users deferred from a previous cohort will show up here before
                            // the registration date
                            viewModel.header = translationHelper.get('welcome_back');
                            viewModel.subheader = translationHelper.get('registration_opens_on', {
                                registrationOpen: dateHelper.formattedUserFacingMonthDayYearLong(
                                    scope.currentUser.relevant_cohort.registrationOpenDate,
                                    false,
                                ),
                            });
                            viewModel.showCheckmark = false;
                        } else if (!lastApplication.registered && lastApplication.in_good_standing === false) {
                            // This means the user was once registered and has become in_good_standing=false due to a missed payment or something
                            if (lastApplication.showAlternateRegistration) {
                                viewModel.header = translationHelper.get('accepted_application_status', {
                                    cohortTitle: scope.currentUser.relevant_cohort.title,
                                });
                            } else {
                                viewModel.header = translationHelper.get('welcome_back');
                                viewModel.subheader = translationHelper.get(
                                    lastApplication.cohort_program_guide_url
                                        ? 'now_secure_place_in_x'
                                        : 'now_secure_place_in_x_no_guide',
                                    {
                                        cohortTitle: scope.currentUser.relevant_cohort.title,
                                    },
                                );
                            }
                            viewModel.showCheckmark = false;
                        } else if (lastApplication.showAlternateRegistration) {
                            viewModel.header = translationHelper.get('accepted_application_status', {
                                cohortTitle: scope.currentUser.relevant_cohort.title,
                            });
                        } else {
                            viewModel.header = translationHelper.get('congratulations_you_are_in');
                            viewModel.subheader = translationHelper.get(
                                lastApplication.cohort_program_guide_url
                                    ? 'now_secure_place_in_x'
                                    : 'now_secure_place_in_x_no_guide',
                                {
                                    cohortTitle: scope.currentUser.relevant_cohort.title,
                                },
                            );
                            viewModel.showCheckmark = true;
                        }
                    } else if (
                        lastApplication.status === 'pre_accepted' &&
                        scope.currentUser.relevant_cohort.supportsSchedule
                    ) {
                        viewModel.subheader = translationHelper.get('class_begins_on_x', {
                            startDate: dateHelper.formattedUserFacingMonthDayYearLong(
                                scope.currentUser.relevant_cohort.startDate,
                                false,
                            ),
                        });
                    } else if (lastApplication.status === 'accepted') {
                        viewModel.header = translationHelper.get('accepted_application_status', {
                            cohortTitle: scope.currentUser.relevant_cohort.title,
                        });
                    } else if (lastApplication.status === 'deferred') {
                        viewModel.header = translationHelper.get('deferred_application_status_x', {
                            programTitle: scope.currentUser.relevant_cohort.programTitle,
                        });
                    }

                    scope.applicationStatusHeaderViewModel = viewModel;
                    return viewModel;
                };

                scope.$watchGroup(
                    [
                        'currentUser.lastCohortApplication',
                        'currentUser.lastCohortApplication.status',
                        'currentUser.lastCohortApplication.graduation_status',
                        'currentUser.lastCohortApplication.in_good_standing',
                        'currentUser.lastCohortApplication.registered',
                        'currentUser.lastCohortApplication.locked_due_to_past_due_payment',
                    ],
                    scope.setApplicationStatusHeaderViewModel,
                );

                let pageListener;
                let pageListenerInitialized;

                function registerPageListener() {
                    pageListener = scope.$watch(
                        () => $location.search().page,
                        (newVal, oldVal) => {
                            if (!pageListenerInitialized || newVal !== oldVal) {
                                pageListenerInitialized = true;
                                scope.activePageIndex = parseInt($location.search().page, 10) || 1;

                                // hiringManager
                                if (scope.section === 'profile') {
                                    const profileSection = _.findWhere(scope.availableSections, {
                                        name: 'profile',
                                    });
                                    scope.hiringManagerProfileActiveStep =
                                        profileSection &&
                                        profileSection.subsections &&
                                        profileSection.subsections[scope.activePageIndex - 1];
                                }

                                // application
                                else if (_.contains(['application', 'my-profile'], scope.section)) {
                                    const applicationOrProfileSection = _.findWhere(scope.availableSections, {
                                        name: scope.section,
                                    });
                                    scope.applicationProfileActiveStep =
                                        applicationOrProfileSection &&
                                        applicationOrProfileSection.subsections &&
                                        applicationOrProfileSection.subsections[scope.activePageIndex - 1];
                                } else {
                                    scope.hiringManagerProfileActiveStep = undefined;
                                    scope.applicationProfileActiveStep = undefined;
                                }
                            }
                        },
                    );
                }

                function deregisterPageListener() {
                    if (pageListener) {
                        pageListener();
                        pageListener = undefined;
                    }
                    pageListenerInitialized = false;
                }

                function updateFooterStatus() {
                    scope.showResponsiveFooter = !!$window.CORDOVA;
                    scope.showMainFooter = (!scope.section || scope.section === 'account') && !!$window.CORDOVA;
                }

                scope.$watch('section', section => {
                    updateFooterStatus();

                    if (!_.contains(_.pluck(scope.availableSections, 'name'), section)) {
                        // if we were linked to the mobile section and this is mobile, expand the nav by default
                        if (scope.isMobile && section === 'mobile') {
                            scope.mobileNavExpanded = true;
                            // force this to "account" so it will match the default account view we are rendering underneath the nav
                            scope.titleLocaleKey = 'account';
                        } else if (
                            section === 'application_status' &&
                            scope.currentUser.reapplyingOrEditingApplication
                        ) {
                            scope.gotoSection('application');
                        } else {
                            scope.gotoSection('account');
                        }

                        return; // terminate early; the rest of this watcher is about correctly rendering the current section
                    }

                    // if the page supports page navigation, re-enable the page listeners
                    if (_.contains(['profile', 'application', 'my-profile'], scope.section)) {
                        registerPageListener();
                    } else {
                        deregisterPageListener();
                        $location.search('page', null);
                    }

                    // Log an event if they are navigating to the MBA application
                    if (section === 'application') {
                        EditCareerProfileHelper.logEventForApplication('start-application');
                    }

                    scope.titleLocaleKey = section;
                });

                scope.$watch('mobileNavExpanded', () => {
                    const navEl = elem.find('.responsive-nav').get(0);
                    scope.responsiveNavScrolling = navEl.offsetHeight && navEl.scrollHeight > navEl.offsetHeight;
                });

                Object.defineProperty(scope, 'sectionNavigationTitle', {
                    get() {
                        // build up the section navigation title here
                        const parts = [translationHelper.get('settings_title')];

                        const currentSection =
                            _.findWhere(scope.availableSections, {
                                name: scope.section,
                            }) ||
                            _.findWhere(scope.availableSections, {
                                name: 'account',
                            });

                        parts.push(translationHelper.get(currentSection.name, { brandName: scope.brandNameShort }));

                        // TODO: support any sub-sections here, e.g.: MBA application form

                        return parts.join(' – ');
                    },
                });

                scope.launchProgramGuide = () => {
                    if (scope.currentUser) {
                        $window.open(
                            scope.currentUser.lastCohortApplication.cohort_program_guide_url,
                            $window.CORDOVA ? '_system' : '_blank',
                        );
                    }
                };

                scope.$on('$destroy', () => {
                    $location.search('page', null);
                    AppHeaderViewModel.toggleVisibility(true);
                });
            },
        };
    },
]);
