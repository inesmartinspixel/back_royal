import angularModule from 'Settings/angularModule/scripts/settings_module';
import * as userAgentHelper from 'userAgentHelper';
import { saveAs } from 'file-saver';
import moment from 'moment-timezone';

import digitalFrontFull from 'images/transcripts/DigitalFrontFull.png';
import digitalFrontFullQuantic from 'images/transcripts/DigitalFrontFull_quantic.png';
import digitalBack from 'images/transcripts/DigitalBack.png';
import digitalBackQuantic from 'images/transcripts/DigitalBack_quantic.png';
import DigitalFrontPartial from 'images/transcripts/DigitalFrontPartial.png';
import DigitalFrontPartialQuantic from 'images/transcripts/DigitalFrontPartial_quantic.png';
import printableFrontFull from 'images/transcripts/PrintableFrontFull.png';
import printableFrontFullQuantic from 'images/transcripts/PrintableFrontFull_quantic.png';
import printableBack from 'images/transcripts/PrintableBack.png';
import printableBackQuantic from 'images/transcripts/PrintableBack_quantic.png';
import PrintableFrontPartial from 'images/transcripts/PrintableFrontPartial.png';
import PrintableFrontPartialQuantic from 'images/transcripts/PrintableFrontPartial_quantic.png';

angularModule.factory('TranscriptHelper', [
    '$injector',

    $injector => {
        const SuperModel = $injector.get('SuperModel');
        const $q = $injector.get('$q');
        const $http = $injector.get('$http');
        const $window = $injector.get('$window');
        const $ocLazyLoad = $injector.get('$ocLazyLoad');
        const safeDigest = $injector.get('safeDigest');
        const User = $injector.get('User');
        const Playlist = $injector.get('Playlist');
        const Stream = $injector.get('Lesson.Stream');
        const $rootScope = $injector.get('$rootScope');
        const TextToImageHelper = $injector.get('TextToImageHelper');
        const StreamProgress = $injector.get('Lesson.StreamProgress');

        return SuperModel.subclass(() => ({
            initialize() {
                this.downloadingTranscript = false;
                this.textToImageHelper = new TextToImageHelper();
            },

            allowTranscriptDownload(user) {
                return !($window.CORDOVA || userAgentHelper.isiOSDevice()) && user.canDownloadTranscripts;
            },

            downloadTranscript(user, format, graded) {
                const self = this;

                format = format || 'digital';
                graded = angular.isDefined(graded) ? graded : false;

                if (!self.allowTranscriptDownload(user)) {
                    return;
                }

                self.downloadingTranscript = true;

                // In the admin we need to grab more data than the User index endpoint provides, so do
                // a show call if asking for the transcript of a different user than the currentUser.
                let userPromise;
                if (user.id !== $rootScope.currentUser.id) {
                    userPromise = User.show(user.id).then(response => {
                        user = response.result;
                        return user;
                    });
                } else {
                    userPromise = $q.when($rootScope.currentUser);
                }

                userPromise
                    .then(() => self._buildTranscriptData(user, graded))
                    .then(transcriptData => self._performDownload(user, transcriptData, format, graded));
            },

            // Note: This could be optimized further for regular users by using the playlists and
            // streams already cached from the LearnerContentAccess calls, but that would require enough
            // work that I decided not to for something that should not be used frequently. It is
            // actually probably better from an accidental breakage perspective to not have this tied to
            // the caching logic, and since we want to use English titles regardless of user's locale we'd
            // have to make sure that the English title always gets returned in the LearnerContentAccess calls.
            _buildTranscriptData(fullUser, graded) {
                const self = this;

                const relevantCohort = fullUser.relevant_cohort;
                let requiredStreamPackIds = null;
                let optionalStreamPackIds = null;
                let streamPackIdsFromCompleteSpecializationPlaylists = null;
                let avgTestScore = null;
                let completeSpecializationPlaylists = null;

                const initialRequests = [];

                // Grab a few pieces of data from the backend that we calculate in mat views
                initialRequests.push(
                    $http.get(`${$window.ENDPOINT_ROOT}/api/users/${fullUser.id}/show_transcript_data.json`),
                );

                // Grab the full specialization playlists in the user's locale or en. Ask for percent_complete
                // so we can filter down to just the completed ones for the transcript
                initialRequests.push(
                    Playlist.index({
                        user_id: fullUser.id,
                        filters: {
                            locale_pack_id: relevantCohort.specialization_playlist_pack_ids,
                            locale: 'en',
                        },
                        'fields[]': ['id', 'title', 'stream_entries', 'locale_pack', 'percent_complete'],
                    }),
                );

                return $q
                    .all(initialRequests)
                    .then(responses => {
                        const serverTranscriptData = responses[0].data;
                        const specializationPlaylists = responses[1].result;

                        requiredStreamPackIds = serverTranscriptData.required_stream_locale_pack_ids;
                        optionalStreamPackIds = serverTranscriptData.optional_stream_locale_pack_ids;
                        avgTestScore = serverTranscriptData.avg_test_score;

                        completeSpecializationPlaylists = _.filter(
                            specializationPlaylists,
                            playlist => playlist.percent_complete === 1,
                        );

                        streamPackIdsFromCompleteSpecializationPlaylists = _.chain(completeSpecializationPlaylists)
                            .pluck('stream_entries')
                            .flatten()
                            .map(streamEntry => streamEntry.locale_pack_id)
                            .uniq()
                            .value();

                        let curriculumStreamPackIds = requiredStreamPackIds
                            .concat(optionalStreamPackIds)
                            .concat(streamPackIdsFromCompleteSpecializationPlaylists);

                        curriculumStreamPackIds = _.uniq(curriculumStreamPackIds);

                        // Grab the full streams in the user's locale or en for all required, optional, and
                        // specialization playlists streams
                        return Stream.index({
                            include_progress: true,
                            include_transcript_data: true,
                            'fields[]': [
                                'id',
                                'title',
                                'lessons',
                                'chapters',
                                'image',
                                'locale_pack',
                                'lesson_streams_progress',
                                'time_limit_hours',
                                'exam',
                            ],
                            'lesson_fields[]': ['id', 'title', 'assessment', 'locale_pack', 'lesson_progress', 'test'],
                            user_id: fullUser.id,
                            filters: {
                                locale_pack_id: curriculumStreamPackIds,
                                locale: 'en',
                            },
                        });
                    })
                    .then(response => {
                        const curriculumStreams = response.result;

                        const streamsToCurriculumTypeMap = {
                            required: [],
                            specialization: [],
                            optional: [],
                        };

                        _.each(curriculumStreams, stream => {
                            const isRequiredStream = _.contains(requiredStreamPackIds, stream.localePackId);
                            const isSpecializationStream = _.contains(
                                streamPackIdsFromCompleteSpecializationPlaylists,
                                stream.localePackId,
                            );

                            // I write it this way because it is possible to have a specialization stream that is
                            // also required, and with the way the transcript is laid out it feels like we should
                            // put a course like that in both the schedule and under its corresponding specialization
                            // playlist.
                            if (isRequiredStream || isSpecializationStream) {
                                if (isRequiredStream) {
                                    streamsToCurriculumTypeMap.required.push(stream);
                                }
                                if (isSpecializationStream) {
                                    streamsToCurriculumTypeMap.specialization.push(stream);
                                }
                            } else {
                                // Any course not in required or specialization is deemed optional
                                streamsToCurriculumTypeMap.optional.push(stream);
                            }
                        });

                        // Sort required streams by the order they appear in the periods if the cohort
                        // supports a schedule, or the title otherwise. Do a sort on the title in both cases since
                        // it is possible to have required streams not in the periods. E.g., a stream is in the foundations
                        // playlist and thus required, but isn't actually specified in the periods. This happened with Blue
                        // Ocean Strategy in EMBA1).
                        let sortedRequiredStreams = _.sortBy(
                            streamsToCurriculumTypeMap.required,
                            stream => stream.title,
                        );

                        if (fullUser.relevant_cohort.supportsSchedule) {
                            const requiredStreamPackIdsFromPeriods = fullUser.relevant_cohort.getRequiredStreamPackIdsFromPeriods();
                            sortedRequiredStreams = _.sortBy(sortedRequiredStreams, stream => {
                                // Make sure we put streams not found in the periods at the end of the list
                                const index = _.indexOf(requiredStreamPackIdsFromPeriods, stream.localePackId);
                                return index > -1 ? index : Number.MAX_SAFE_INTEGER;
                            });
                        }

                        // Sort optional streams by their title
                        // Note: We are ensuring that the optional stream is not required in a period or included in
                        // a specialization playlist.
                        const sortedOptionalStreams = _.sortBy(
                            streamsToCurriculumTypeMap.optional,
                            stream => stream.title,
                        );

                        // Sort the completed specialization playlists in the same order as the student dashboard, which is
                        // by their index in the specialization_playlist_pack_ids array.
                        const sortedCompleteSpecializationPlaylists = _.sortBy(
                            completeSpecializationPlaylists,
                            playlist =>
                                _.indexOf(relevantCohort.specialization_playlist_pack_ids, playlist.localePackId),
                        );

                        // Build out playlist transcript entries for each of the completed specialization playlists
                        const completeSpecializationPlaylistTranscriptEntries = [];
                        const specializationStreamsMap = _.indexBy(
                            streamsToCurriculumTypeMap.specialization,
                            'localePackId',
                        );

                        // Build each specialization playlist's stream transcript entries
                        _.each(sortedCompleteSpecializationPlaylists, playlist => {
                            const streams = [];

                            _.each(playlist.stream_entries, streamEntry => {
                                streams.push(specializationStreamsMap[streamEntry.locale_pack_id]);
                            });

                            completeSpecializationPlaylistTranscriptEntries.push({
                                title: playlist.title,
                                streamEntries: self._getStreamTranscriptEntries(streams, graded),
                            });
                        });

                        const cumulativeGradeTextLookup = {
                            pending: 'IN PROGRESS',
                            failed: 'FAIL',
                            graduated: 'PASS',
                            honors: 'PASS WITH HONORS',
                        };

                        return {
                            requiredStreamTranscriptEntries: self._getStreamTranscriptEntries(
                                sortedRequiredStreams,
                                graded,
                            ),
                            optionalStreamTranscriptEntries: self._getStreamTranscriptEntries(
                                sortedOptionalStreams,
                                graded,
                            ),
                            completeSpecializationPlaylistTranscriptEntries,
                            avgTestScore,
                            cumulativeGradeText:
                                cumulativeGradeTextLookup[fullUser.lastCohortApplication.graduation_status],
                            graduationStatus: fullUser.lastCohortApplication.graduation_status,
                            graduationDateText: self._getGraduationDateText(fullUser),
                            curriculumStatus: fullUser.curriculum_status,
                            lastCohortApplicationStatus: fullUser.lastCohortApplicationStatus,
                            programType: fullUser.programType,
                            transcriptProgramTitle: fullUser.relevant_cohort.transcriptProgramTitle,
                            name: fullUser.name,
                            email: fullUser.email,
                            addressLine1: fullUser.address_line_1,
                            addressLine2: fullUser.address_line_2,
                            city: fullUser.city,
                            state: fullUser.state,
                            zip: fullUser.zip,
                            country: fullUser.country,
                        };
                    });
            },

            _getGraduationDateText(fullUser) {
                const status = fullUser.lastCohortApplicationStatus;
                const graduationStatus = fullUser.lastCohortApplication.graduation_status;

                if (
                    status === 'deferred' ||
                    graduationStatus === 'failed' ||
                    !fullUser.relevant_cohort.supportsSchedule
                ) {
                    return 'N/A';
                }
                // Regardless of when the user actually graduated from the program, we use the graduationDate
                // on their relevant_cohort and ignore the graduated_at value on their lastCohortApplication.
                return moment(fullUser.relevant_cohort.graduationDate).format('MMMM D, YYYY');
            },

            _getStreamTranscriptEntries(streams, graded) {
                // Remove any uncompleted streams
                streams = _.filter(streams, stream => stream.complete);

                // Remove any streams with "Exam Practice" or "Coffee Break" in the title
                streams = _.reject(streams, stream => {
                    const lowerTitle = stream.title.toLowerCase();
                    return (
                        lowerTitle.includes('exam practice') ||
                        lowerTitle.includes('coffee break') ||
                        lowerTitle.includes('photography basics') ||
                        lowerTitle.includes('why blended learning matters')
                    );
                });

                // Remove any streams that were artificially completed due to
                // the exam being completed in a prior cohort, and for which the user
                // didn't take the initiative to go complete themselves (we unset waiver
                // if they do)
                // See https://trello.com/c/6xyHPQuo
                streams = _.reject(
                    streams,
                    stream => stream.progressWaiver === StreamProgress.WAIVER_EXAM_ALREADY_COMPLETED,
                );

                const streamEntries = [];
                _.map(streams, stream => {
                    const entry = {
                        title: stream.title,
                    };
                    if (graded) {
                        entry.grade = stream.grade;
                    }
                    streamEntries.push(entry);
                });
                return streamEntries;
            },

            _performDownload(user, transcriptData, format, graded) {
                const self = this;

                const requests = [];
                let backgroundTranscriptResponse;
                let backPageTranscriptResponse;
                const textColor = '#000000';
                const isDigital = format === 'digital';
                const isPrintable = format === 'printable';
                let frontPageBackgroundImagePath;
                let backPageImagePath;

                const graduated = !!_.contains(['graduated', 'honors'], transcriptData.curriculumStatus);
                const useQuanticBranding = user.shouldSeeQuanticBranding;

                // full paths must be used here because of cache busting
                if (graduated && isDigital) {
                    frontPageBackgroundImagePath = useQuanticBranding ? digitalFrontFullQuantic : digitalFrontFull;
                    backPageImagePath = useQuanticBranding ? digitalBackQuantic : digitalBack;
                } else if (!graduated && isDigital) {
                    frontPageBackgroundImagePath = useQuanticBranding
                        ? DigitalFrontPartialQuantic
                        : DigitalFrontPartial;
                    backPageImagePath = useQuanticBranding ? digitalBackQuantic : digitalBack;
                } else if (graduated && isPrintable) {
                    frontPageBackgroundImagePath = useQuanticBranding ? printableFrontFullQuantic : printableFrontFull;
                    backPageImagePath = useQuanticBranding ? printableBackQuantic : printableBack;
                } else if (!graduated && isPrintable) {
                    frontPageBackgroundImagePath = useQuanticBranding
                        ? PrintableFrontPartialQuantic
                        : PrintableFrontPartial;
                    backPageImagePath = useQuanticBranding ? printableBackQuantic : printableBack;
                } else {
                    throw new Error(`${format} format not supported!`);
                }

                // get background image
                requests.push(
                    $http
                        .get(window.ENDPOINT_ROOT + frontPageBackgroundImagePath, {
                            responseType: 'arraybuffer',
                        })
                        .then(response => {
                            backgroundTranscriptResponse = response;
                        }),
                );

                // get back page
                requests.push(
                    $http
                        .get(window.ENDPOINT_ROOT + backPageImagePath, {
                            responseType: 'arraybuffer',
                        })
                        .then(response => {
                            backPageTranscriptResponse = response;
                        }),
                );

                // grab the needed assets in parallel and cache them for rendering the PDF
                requests.push($ocLazyLoad.load(window.webpackManifest['certificates.js'].replace(/^\//, '')));

                $q.all(requests).then(() => {
                    // Order matters and there doesn't seem to be a way to specify layers so used the cached
                    // assets to assemble the PDF here in the correct order.

                    // create the pdfkit Document
                    const doc = new $window.PDFDocument({
                        size: [612, 793],
                        margin: 0,
                    });
                    const pdfStream = doc.pipe($window.blobStream());

                    // set the background
                    doc.image(backgroundTranscriptResponse.data, 0, 0, {
                        width: 612,
                        height: 793,
                    });

                    // header text
                    doc.font('Courier-Bold')
                        .fontSize(9)
                        .fillColor(textColor)
                        .text(transcriptData.transcriptProgramTitle, 270, 36, {
                            width: 156,
                        })
                        .text('Graduate', 270, 56);

                    // Conditionally add the name in the header as text or an image depending
                    // on if it contains characters outside of extended ASCII
                    if (self.textToImageHelper.isASCII(transcriptData.name)) {
                        doc.fontSize(9).text(transcriptData.name, 270, 66, {
                            width: 156,
                        });
                    } else {
                        self.textToImageHelper.addImageTextToPdf(doc, transcriptData.name, 270, 66, {
                            width: 156,
                            fontSize: 9,
                            font: 'Courier-Bold',
                        });
                    }

                    const addressLocation = `${transcriptData.city}, ${
                        transcriptData.state ? transcriptData.state : transcriptData.country
                    } ${transcriptData.zip ? transcriptData.zip : ''}`;

                    // Combine all of the "Issued To" information to test if any of it contains characters outside of
                    // extended ASCII.
                    let issuedToText = `${transcriptData.name}\n`;

                    if (transcriptData.addressLine1) {
                        issuedToText += `${transcriptData.addressLine1}\n`;

                        if (transcriptData.addressLine2) {
                            issuedToText += `${transcriptData.addressLine2}\n`;
                        }

                        issuedToText += `${addressLocation}\n`;
                    } else {
                        issuedToText += 'Mailing address unavailable\n';
                    }

                    issuedToText += transcriptData.email;

                    // Conditionally add "Issued To" information as text or an image depending
                    // on if it contains characters outside of extended ASCII
                    if (self.textToImageHelper.isASCII(issuedToText)) {
                        doc.fontSize(10)
                            .text(transcriptData.name, 94, 115, {
                                width: 200,
                            })
                            .moveDown(0);

                        if (transcriptData.addressLine1) {
                            doc.text(transcriptData.addressLine1, {
                                width: 200,
                            })
                                .moveDown(0)
                                .text(transcriptData.addressLine2 ? transcriptData.addressLine2 : addressLocation, {
                                    width: 200,
                                })
                                .moveDown(0)
                                .text(transcriptData.addressLine2 ? addressLocation : '', {
                                    width: 200,
                                });
                        } else {
                            doc.text('Mailing address unavailable', {
                                width: 200,
                            });
                        }

                        doc.moveDown().text(transcriptData.email, {
                            width: 200,
                        });
                    } else {
                        self.textToImageHelper.addImageTextToPdf(doc, issuedToText, 94, 115, {
                            width: 200,
                            fontSize: 10,
                            font: 'Courier-Bold',
                        });
                    }
                    doc.text(moment().format('MMMM D, YYYY'), 397, 112).text(
                        transcriptData.graduationDateText,
                        397,
                        151,
                    );

                    // sizing and positioning constants for the 2-column layout
                    const columnWidth = 300;
                    const columnTop = 216;
                    const leftColumnX = 18;
                    const rightColumnX = 313;

                    // check to see if courses will exceed the height of the right column
                    // if they do, make the font size smaller
                    const leftColumnFontSize = transcriptData.requiredStreamTranscriptEntries.length > 55 ? 7 : 8;

                    // left column text
                    doc.font('Courier-Bold')
                        .fontSize(10)
                        .fillColor(textColor)
                        .text('*************** COURSE SCHEDULE ***************', leftColumnX, columnTop, {
                            width: columnWidth,
                        })
                        .moveDown(0.5)
                        .font('Courier')
                        .fontSize(leftColumnFontSize);

                    _.each(transcriptData.requiredStreamTranscriptEntries, (entry, index) => {
                        doc.text(
                            self._indexToStringWithSpaces(index) +
                                self._wrapTextGracefully(entry.title, graded) +
                                (graded ? self._formatGrade(entry.grade, true) : ''),
                            {
                                width: columnWidth,
                            },
                        ).moveDown(0);
                    });

                    // combine all list items into one array to see if they will exceed the height of the right column
                    // if they do, make the font size smaller
                    const listItems = _.flatten(
                        _.pluck(transcriptData.completeSpecializationPlaylistTranscriptEntries, 'streamEntries'),
                    ).concat(transcriptData.optionalStreamTranscriptEntries);
                    const listFontSize = listItems.length >= 40 ? 7 : 8;

                    // right column text
                    if (transcriptData.completeSpecializationPlaylistTranscriptEntries.length) {
                        doc.font('Courier-Bold')
                            .fontSize(10)
                            .fillColor(textColor)
                            .text('*************** SPECIALIZATIONS ***************', rightColumnX, columnTop, {
                                width: columnWidth,
                            })
                            .moveDown(0.5)
                            .font('Courier')
                            .fontSize(listFontSize);

                        _.each(transcriptData.completeSpecializationPlaylistTranscriptEntries, playlist => {
                            doc.text(playlist.title.toUpperCase(), {
                                width: columnWidth,
                            }).moveDown(0);

                            _.each(playlist.streamEntries, (entry, index) => {
                                doc.text(
                                    self._indexToStringWithSpaces(index) +
                                        self._wrapTextGracefully(entry.title, graded) +
                                        (graded ? self._formatGrade(entry.grade, true) : ''),
                                    {
                                        width: columnWidth,
                                    },
                                ).moveDown(0);
                            });

                            doc.moveDown();
                        });

                        doc.moveDown()
                            .font('Courier-Bold')
                            .fontSize(10)
                            .text('****************** ELECTIVES ******************', {
                                width: columnWidth,
                            });

                        // if there are no specializations, we need to position this text, not just use `moveDown()`
                    } else {
                        doc.font('Courier-Bold')
                            .fontSize(10)
                            .fillColor(textColor)
                            .text('****************** ELECTIVES ******************', rightColumnX, columnTop, {
                                width: columnWidth,
                            });
                    }

                    doc.moveDown(0.5).font('Courier').fontSize(listFontSize);

                    _.each(transcriptData.optionalStreamTranscriptEntries, (entry, index) => {
                        doc.text(
                            self._indexToStringWithSpaces(index) +
                                self._wrapTextGracefully(entry.title, graded) +
                                (graded ? self._formatGrade(entry.grade, true) : ''),
                            {
                                width: columnWidth,
                            },
                        ).moveDown(0);
                    });

                    doc.moveDown()
                        .moveDown()
                        .font('Courier-Bold')
                        .fontSize(10)
                        .text('************** TRANSCRIPT SUMMARY *************', {
                            width: columnWidth,
                        })
                        .moveDown(0.5)
                        .font('Courier')
                        .fontSize(listFontSize);

                    if (graded) {
                        doc.text(
                            `CUMULATIVE EXAM SCORE: ${self._formatAvgTestScore(transcriptData.avgTestScore, false)}`,
                            {
                                width: columnWidth,
                            },
                        ).moveDown(0);
                    }

                    doc.text(`CUMULATIVE GRADE: ${transcriptData.cumulativeGradeText}`, {
                        width: columnWidth,
                    });

                    // back of document
                    doc.addPage().image(backPageTranscriptResponse.data, 0, 0, {
                        width: 612,
                        height: 793,
                    });

                    // set the finish event
                    pdfStream.on('finish', () => {
                        const blob = pdfStream.toBlob('application/pdf');
                        try {
                            const fileName = `${[transcriptData.name, transcriptData.programType, 'Transcript'].join(
                                '-',
                            )}.pdf`;

                            // support IE11 blob download, defaulting to HTML5 saveAs
                            if ($window.navigator && $window.navigator.msSaveOrOpenBlob) {
                                $window.navigator.msSaveOrOpenBlob(blob, fileName);
                            } else {
                                saveAs(blob, fileName);
                            }

                            self.downloadingTranscript = false;
                            safeDigest($rootScope);
                        } catch (e) {
                            $injector
                                .get('ErrorLogService')
                                .notify('Failed to write certificate blob data to window!', e);
                        }
                    });

                    doc.end();
                });
            },

            _wrapTextGracefully(text, graded) {
                // how many characters can fit after the index with spaces (5 chars)
                const maxLength = 56;
                const withGradeMaxLength = 49; // and leave room for grade (6-7 chars)

                if (graded && text.length < withGradeMaxLength) {
                    return text;
                }

                if (graded && _.contains(_.range(withGradeMaxLength, maxLength + 1), text.length)) {
                    // range maxes at one less than the second number given
                    return `${text}\n    `; // forces grade to new line with indention spaces
                }

                if (!graded && text.length <= maxLength) {
                    return text;
                }

                const textArray = text.split(' ');
                let textString = '';
                let breakAdded = false;

                _.each(textArray, (t, index) => {
                    // if string is less than maxLength AND adding the next word is less than maxLength
                    if (textString.length < maxLength && (textString + t).length < maxLength) {
                        textString += t;
                    } else if (!breakAdded) {
                        textString += `\n     ${t}`; // break with 5 spaces
                        breakAdded = true;
                    } else {
                        textString += t;
                    }

                    if (index !== textArray.length - 1) {
                        textString += ' ';
                    }
                });

                return textString;
            },

            _indexToStringWithSpaces(index) {
                index += 1; // because arrays start at 0

                if (index < 10) {
                    return `${index}.  `;
                }
                return `${index}. `;
            },

            _formatGrade(grade, useDash) {
                let gradeText;
                if (grade) {
                    gradeText = `${Math.round(100 * grade)}%`;
                } else {
                    gradeText = 'P';
                }

                return (useDash ? ' - ' : '') + gradeText;
            },

            _formatAvgTestScore(avgTestScore, useDash) {
                let avgTestScoreText;
                if (avgTestScore) {
                    avgTestScoreText = `${Math.round(100 * avgTestScore)}%`;
                } else {
                    avgTestScoreText = 'N/A';
                }

                return (useDash ? ' - ' : '') + avgTestScoreText;
            },
        }));
    },
]);
