import angularModule from 'Settings/angularModule/scripts/settings_module';
import casperMode from 'casperMode';
import template from 'Settings/angularModule/views/paid_certificate_checkout.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('paidCertificateCheckout', [
    '$injector',

    function factory($injector) {
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
        const $rootScope = $injector.get('$rootScope');
        const isMobileMixin = $injector.get('isMobileMixin');
        const Coupon = $injector.get('Coupon');
        const ErrorLogService = $injector.get('ErrorLogService');
        const $window = $injector.get('$window');
        const StripeCheckoutHelper = $injector.get('Payments.CohortRegistrationCheckoutHelper');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                promotedCohorts: '<',
            },
            link(scope) {
                NavigationHelperMixin.onLink(scope);
                isMobileMixin.onLink(scope);

                // HACK: temporarily disabling coupons for now; we'll re-enable this later
                scope.couponsEnabled = false;

                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                if (!scope.currentUser || !scope.currentUser.lastCohortApplication) {
                    return;
                }

                if (!casperMode()) {
                    scope.checkoutHelper = new StripeCheckoutHelper();
                }

                scope.proxy = {};
                scope.$watchGroup(
                    ['currentUser.acceptedToPaidCert', 'currentUser.lastCohortApplication.isPaidCert'],
                    () => {
                        if (!scope.currentUser) {
                            return;
                        }
                        if (scope.currentUser.acceptedToPaidCert) {
                            scope.view = 'accepted';
                        } else if (
                            scope.currentUser.lastCohortApplication &&
                            scope.currentUser.lastCohortApplication.isPaidCert
                        ) {
                            scope.view = 'checkout';
                        } else {
                            scope.view = 'select';
                        }
                    },
                );

                scope.$watch('promotedCohorts', promotedCohorts => {
                    scope.paidCertCohorts = _.chain(promotedCohorts)
                        .where({
                            isPaidCert: true,
                        })
                        .sortBy('title')
                        .value();

                    if (scope.currentUser && scope.currentUser.lastCohortApplication.isPaidCert) {
                        scope.proxy.cohortId = scope.currentUser.lastCohortApplication.cohort_id;
                    }
                });

                scope.$watch('proxy.cohortId', cohortId => {
                    scope.selectedPaidCertCohort = _.findWhere(scope.paidCertCohorts, {
                        id: cohortId,
                    });

                    if (cohortId && !scope.selectedPaidCertCohort) {
                        ErrorLogService.notify(
                            `Cohort from application (${cohortId}) is not one of the available paidCertCohorts: ${_.pluck(
                                scope.paidCertCohorts,
                                'id',
                            ).join(',')}`,
                        );
                    }
                });

                scope.applyToPaidCert = (cohort, checkIsMobile) => {
                    if (checkIsMobile && !scope.isMobile) {
                        return;
                    }
                    scope.requestInFlight = true;
                    scope.view = 'checkout';
                    scope.currentUser.applyToCohort(cohort, 'pre_accepted').then(() => {
                        scope.requestInFlight = false;
                    });
                };

                scope.cancelPaidCert = () => {
                    scope.requestInFlight = true;
                    const paidCertApplication = _.findWhere(scope.currentUser.cohort_applications, {
                        isPaidCert: true,
                        status: 'pre_accepted',
                    });
                    scope.view = 'select';
                    if (paidCertApplication) {
                        scope.currentUser.deleteCohortApplication(paidCertApplication).then(() => {
                            scope.requestInFlight = false;
                        });
                    }
                };

                scope.applyCoupon = () => {
                    scope.loadingCoupon = true;
                    Coupon.show(scope.proxy.couponCode, {
                        filters: {
                            cohort_id: scope.selectedPaidCertCohort.id,
                        },
                    }).then(response => {
                        scope.loadingCoupon = false;
                        scope.coupon = response.result;
                        scope.proxy.validCoupon = !!scope.coupon;
                        if (scope.coupon) {
                            scope.subtotal = scope.selectedPaidCertCohort.price;
                            if (scope.coupon.percent_off) {
                                scope.discountPercentage = scope.coupon.percent_off;
                                scope.discountAmount = scope.subtotal * (scope.discountPercentage / 100);
                            } else {
                                scope.discountAmount = scope.coupon.amount_off / 100;
                            }

                            scope.total = scope.subtotal - scope.discountAmount;
                        }
                    });
                };

                scope.register = () => {
                    // you can't get here in cordova, but just checking.  See
                    // tuition-status-unregistered for something kind of similar.
                    //
                    // In the emba case, we got permission from Apple to send them to a webpage.
                    // Here, we did not.  Se we just prevent cordova people from getting here.
                    if ($window.CORDOVA) {
                        ErrorLogService.notify('Trying to register for a paid cert in cordova.');
                        return;
                    }

                    scope.checkoutHelper.registerForCohort(scope.selectedPaidCertCohort.onlyStripePlanId, scope.coupon);
                };

                scope.goHome = $rootScope.goHome;
            },
        };
    },
]);
