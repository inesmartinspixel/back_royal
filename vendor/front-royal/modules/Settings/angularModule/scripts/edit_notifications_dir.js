import angularModule from 'Settings/angularModule/scripts/settings_module';
import { setupBrandNameProperties } from 'AppBrandMixin';
import template from 'Settings/angularModule/views/edit_notifications.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('editNotifications', [
    '$injector',

    function factory($injector) {
        const $rootScope = $injector.get('$rootScope');
        const ngToast = $injector.get('ngToast');
        const TranslationHelper = $injector.get('TranslationHelper');
        const $window = $injector.get('$window');
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');

        return {
            restrict: 'E',
            templateUrl,
            scope: {},
            link(scope) {
                NavigationHelperMixin.onLink(scope);
                setupBrandNameProperties($injector, scope);

                //-------------------------
                // Initialization
                //-------------------------

                // Setup localization keys
                const translationHelper = new TranslationHelper('settings.edit_notifications');

                scope.preferencesProxy = {};

                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                Object.defineProperty(scope, 'careerProfile', {
                    get() {
                        return $rootScope.currentUser.career_profile;
                    },
                });

                function getSectionConfig(sectionKey, opts = {}) {
                    const entry = _.extend(
                        {
                            controlTitleTranslationKey: `settings.edit_notifications.${sectionKey}`,
                            notificationPreferences: [
                                {
                                    key: sectionKey,
                                    desc: `${sectionKey}_desc`,
                                    type: 'checkbox',
                                },
                            ], // if the section requires multiple checkboxes, supply an array of the notification properties that belong to the checkbox group
                            requiresConfirmationToToggleOff: false, // controls whether a confirmation dialog modal should pop up with custom messaging when attempting to update the preference
                            requiresConfirmationToToggleOn: false,
                            thingToSave: 'currentUser',
                            showThanks: true, // used in confirm popup
                            get(key) {
                                return scope.currentUser[key];
                            },
                            set(key, val) {
                                scope.currentUser[key] = val;
                            },
                        },
                        opts,
                    );

                    _.each(entry.notificationPreferences, preference => {
                        scope.preferencesProxy[preference.key] = entry.get(preference.key);
                    });
                    return entry;
                }

                scope.$watch('currentUser', () => {
                    if (!scope.currentUser) {
                        return;
                    }

                    if (scope.currentUser.defaultsToHiringExperience) {
                        scope.notificationConfigs = [
                            getSectionConfig('notify_hiring_updates'),
                            getSectionConfig('notify_hiring_content'),
                            getSectionConfig('notify_hiring_candidate_activity', {
                                requiresConfirmationToToggleOff: true,
                            }),
                            getSectionConfig('notify_hiring_spotlights', {
                                notificationPreferences: [
                                    {
                                        key: 'notify_hiring_spotlights_technical',
                                        desc: 'notify_hiring_spotlights_technical_desc',
                                        type: 'checkbox',
                                    },
                                    {
                                        key: 'notify_hiring_spotlights_business',
                                        desc: 'notify_hiring_spotlights_business_desc',
                                        type: 'checkbox',
                                    },
                                ],
                            }),
                        ];
                    } else {
                        const newsLetterSectionKey = `notify_email_newsletter${
                            scope.currentUser.isMiyaMiya ? '_miya_miya' : ''
                        }`;
                        scope.notificationConfigs = [
                            getSectionConfig(newsLetterSectionKey, {
                                notificationPreferences: [
                                    {
                                        key: 'notify_email_newsletter',
                                        title: newsLetterSectionKey,
                                        desc: `${newsLetterSectionKey}_desc`,
                                        type: 'button-toggle',
                                        inputOptions: [
                                            {
                                                value: true,
                                                label: 'enable',
                                            },
                                            {
                                                value: false,
                                                label: 'disable',
                                            },
                                        ],
                                        translationValues: { brandName: scope.brandNameShort },
                                    },
                                ],
                                translationValues: { brandName: scope.brandNameShort },
                            }),
                        ];

                        // If this user is on a mobile device, show the push notification
                        // preference (hiring managers don't have a mobile application
                        // and Miya Miya users have push notifications disabled).
                        if ($window.CORDOVA && !scope.currentUser.isMiyaMiya) {
                            scope.notificationConfigs = scope.notificationConfigs.concat([
                                getSectionConfig('pref_allow_push_notifications', {
                                    notificationPreferences: [
                                        {
                                            key: 'pref_allow_push_notifications',
                                            desc: 'pref_allow_push_notifications_desc',
                                            type: 'button-toggle',
                                            inputOptions: [
                                                {
                                                    value: true,
                                                    label: 'enable',
                                                },
                                                {
                                                    value: false,
                                                    label: 'disable',
                                                },
                                            ],
                                        },
                                    ],
                                }),
                            ]);
                        }

                        if (scope.currentUser.hasCareersNetworkAccess) {
                            scope.notificationConfigs = scope.notificationConfigs.concat([
                                getSectionConfig('notify_employer_communication_updates', {
                                    requiresConfirmationToToggleOff: true,
                                    requiresConfirmationToToggleOn: true,
                                    thingToSave: 'careerProfile',
                                    showThanks: false,
                                    notificationPreferences: [
                                        {
                                            key: 'notify_employer_communication_updates',
                                            desc: 'notify_employer_communication_updates_desc',
                                            type: 'button-toggle',
                                            inputOptions: [
                                                {
                                                    value: true,
                                                    label: 'enable',
                                                },
                                                {
                                                    value: false,
                                                    label: 'disable',
                                                },
                                            ],
                                        },
                                    ],
                                    get() {
                                        return (
                                            scope.careerProfile.interested_in_joining_new_company !== 'not_interested'
                                        );
                                    },
                                    set(key, val) {
                                        // the key here is always notify_employer_communication_updates, so we ignore it
                                        if (!val) {
                                            scope.careerProfile.interested_in_joining_new_company = 'not_interested';
                                        } else {
                                            scope.careerProfile.interested_in_joining_new_company = 'neutral';
                                        }
                                    },
                                }),
                                getSectionConfig('notify_featured_positions_updates', {
                                    notificationPreferences: [
                                        {
                                            key: 'notify_candidate_positions',
                                            desc: 'notify_candidate_positions_desc',
                                            type: 'button-toggle',
                                            inputOptions: [
                                                {
                                                    value: true,
                                                    label: 'weekly',
                                                },
                                                {
                                                    value: false,
                                                    label: 'never',
                                                },
                                            ],
                                        },
                                        {
                                            key: 'notify_candidate_positions_recommended',
                                            desc: 'notify_candidate_positions_recommended_desc',
                                            type: 'button-toggle',
                                            inputOptions: [
                                                {
                                                    value: 'daily',
                                                    label: 'daily',
                                                },
                                                {
                                                    value: 'bi_weekly',
                                                    label: 'bi_weekly',
                                                },
                                                {
                                                    value: 'never',
                                                    label: 'never',
                                                },
                                            ],
                                            infoKey: 'recommended_positions_info',
                                        },
                                    ],
                                }),
                            ]);
                        }
                    }
                });

                scope.isSelected = function (key, value) {
                    return scope.preferencesProxy[key] === value;
                };

                //-------------------------
                // Navigation / Actions
                //-------------------------

                function performUpdate(key) {
                    const notificationConfig = _.find(scope.notificationConfigs, config =>
                        _.contains(_.pluck(config.notificationPreferences, 'key'), key),
                    );
                    const notificationPreference = notificationConfig.notificationPreferences.find(
                        pref => pref.key === key,
                    );

                    // actually apply the change in the users model
                    // or the career profile model
                    const val = scope.preferencesProxy[key];
                    notificationConfig.set(key, val);

                    // save the model over the api
                    const thingToSave = scope[notificationConfig.thingToSave];
                    thingToSave.save().then(() => {
                        const translatedSectionTitle = translationHelper.get(
                            // Most of the time we can just use the `key`, but the `notificationPreference`
                            // can optionally supply a separate `title` property, which will be used as the
                            // notification preference name in the toast message.
                            notificationPreference.title || notificationPreference.key,
                            notificationConfig.translationValues,
                        );
                        ngToast.create({
                            content: translationHelper.get('update_account_success', {
                                title: translatedSectionTitle,
                            }),
                            className: 'success',
                        });
                    });
                }

                scope.updateAccount = (key, value) => {
                    const notificationConfig = _.find(scope.notificationConfigs, config =>
                        _.contains(_.pluck(config.notificationPreferences, 'key'), key),
                    );

                    if (!notificationConfig) {
                        throw new Error(`Unrecognized notification key: ${key}`);
                    }

                    // checkbox types use ng-model instead of passing in a value. button-toggle types pass in a value
                    if (angular.isDefined(value)) {
                        scope.preferencesProxy[key] = value;
                    }

                    const val = scope.preferencesProxy[key];

                    // handle special-cases where the notification preference requiresConfirmationToToggleOff
                    const shouldConfirm =
                        (notificationConfig.requiresConfirmationToToggleOff && !val) ||
                        (notificationConfig.requiresConfirmationToToggleOn && val);

                    if (shouldConfirm) {
                        const DialogModal = $injector.get('DialogModal');

                        const options = {
                            showThanks: notificationConfig.showThanks,
                            text: translationHelper.get(`${key}_confirm_${val ? 'on' : 'off'}`),
                            confirmCallback() {
                                performUpdate(key);
                            },
                            cancelCallback() {
                                scope.preferencesProxy[key] = !val; // undo change in proxy
                            },
                        };

                        DialogModal.confirm(options);
                    } else {
                        // otherwise, just apply the change
                        performUpdate(key);
                    }
                };
            },
        };
    },
]);
