import angularModule from 'Settings/angularModule/scripts/settings_module';
import { setupBrandNameProperties, setupBrandEmailProperties } from 'AppBrandMixin';
import template from 'Settings/angularModule/views/application_status.html';
import cacheAngularTemplate from 'cacheAngularTemplate';
import moment from 'moment-timezone';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('applicationStatus', [
    '$injector',

    function factory($injector) {
        const $rootScope = $injector.get('$rootScope');
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
        const TranslationHelper = $injector.get('TranslationHelper');
        const Cohort = $injector.get('Cohort');
        const DialogModal = $injector.get('DialogModal');

        return {
            restrict: 'E',
            templateUrl,
            scope: {},
            link(scope) {
                //-------------------------
                // Initialization
                //-------------------------

                NavigationHelperMixin.onLink(scope);
                setupBrandNameProperties($injector, scope);
                setupBrandEmailProperties($injector, scope, ['support']);
                const translationHelper = new TranslationHelper('settings.application_status');

                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                Object.defineProperty(scope, 'showPaidCertCheckout', {
                    get() {
                        return (
                            !!scope.currentUser &&
                            !!(scope.currentUser.showPaidCertSelection || scope.currentUser.acceptedToPaidCert)
                        );
                    },
                });

                Object.defineProperty(scope, 'paymentRequired', {
                    get() {
                        const cohortApplication = scope.currentUser && scope.currentUser.lastCohortApplication;

                        if (!cohortApplication) {
                            return false;
                        }

                        // see: https://trello.com/c/O2HLeRj1
                        if (cohortApplication.in_good_standing && !scope.currentUser.primarySubscription) {
                            return false;
                        }

                        // if you're registered and have a single-payment plan, then you must have paid already
                        // see: https://trello.com/c/0iEd4cEw
                        if (cohortApplication.existingPlan && cohortApplication.existingPlan.frequency === 'once') {
                            return false;
                        }

                        return !!(
                            cohortApplication.total_num_required_stripe_payments &&
                            cohortApplication.total_num_required_stripe_payments > 0 &&
                            !cohortApplication.hasFullScholarship
                        );
                    },
                    configurable: true,
                });

                Object.defineProperty(scope, 'showTuitionAndRegistration', {
                    get() {
                        return (
                            scope.currentUser &&
                            !scope.currentUser.isFailed &&
                            (scope.currentUser.isAccepted || scope.currentUser.isPreAccepted) &&
                            Cohort.supportsRecurringPayments(scope.currentUser.lastCohortApplication.program_type) &&
                            (scope.showTuitionStatusUnregisteredAlternative ||
                                scope.showTuitionStatusUnregistered ||
                                scope.showTuitionStatusRegistered)
                        );
                    },
                    configurable: true,
                });

                Object.defineProperty(scope, 'showTuitionStatusUnregisteredAlternative', {
                    get() {
                        const cohortApplication = scope.currentUser && scope.currentUser.lastCohortApplication;
                        return (
                            cohortApplication &&
                            !cohortApplication.registered &&
                            cohortApplication.showAlternateRegistration
                        );
                    },
                    configurable: true,
                });

                Object.defineProperty(scope, 'showTuitionStatusUnregistered', {
                    get() {
                        const cohortApplication = scope.currentUser && scope.currentUser.lastCohortApplication;
                        return (
                            cohortApplication &&
                            !cohortApplication.registered &&
                            !cohortApplication.showAlternateRegistration
                        );
                    },
                    configurable: true,
                });

                Object.defineProperty(scope, 'showTuitionStatusRegistered', {
                    get() {
                        const cohortApplication = scope.currentUser && scope.currentUser.lastCohortApplication;
                        return cohortApplication && cohortApplication.registered && scope.paymentRequired;
                    },
                    configurable: true,
                });

                // Being defensive here: it's possible for this directive to get rendered briefly if someone navigates directly to it
                // if no previous cohort application found, simply return to avoid possibility of RTEs
                if (!scope.currentUser.lastCohortApplication) {
                    return;
                }

                scope.launchProgramGuide = () => {
                    scope.loadUrl(scope.currentUser.lastCohortApplication.cohort_program_guide_url, '_blank');
                };

                function onApplicationsAndCohortsLoaded() {
                    if (!scope.currentUser) {
                        return;
                    }

                    // Set reapply state
                    scope.canReapply = false;
                    if (!scope.currentUser.showPaidCertSelection) {
                        const relevantCohort = scope.currentUser.relevant_cohort;

                        // I realize this could be cleaner. I'm leaving as-is though, because we have
                        // a ton of specs in application_status_dir that are improperly mocking out
                        // cohort applications, and it's easier to leave this line than it is to refactor
                        // that entire spec suite. We should remove this when that suite gets refactored.
                        // FIXME: https://trello.com/c/YliAbUZG
                        scope.canReapply =
                            scope.currentUser?.lastCohortApplication?.canReapplyTo &&
                            scope.currentUser.lastCohortApplication.canReapplyTo(relevantCohort);
                        scope.cohortToReapplyTo = relevantCohort;
                    } else {
                        // When showPaidCertSelection is true we need to look at either the last application or the
                        // next-to-last application to determine reapply status because the user can be in a state
                        // where they have created a pre_accepted application to a paid-cert but haven't registered yet,
                        // in which case we need to calculate the reapply logic based on the next-to-last application's cohort
                        // rather than the relevant_cohort like we have done before now.
                        _.chain(scope.currentUser.cohort_applications)
                            .sortBy('applied_at')
                            .last(2)
                            .detect(application => {
                                const cohort = _.findWhere(scope.promotedCohorts, {
                                    program_type: application.program_type,
                                });

                                // In general we've tried to get away from using flags like isPaidCert
                                // in favor of naming certain behaviors and enabling them in the configuration
                                // for the program type.  In this case though, I don't know what else to name
                                // the kind of program that allows you to re-apply to the last one you applied
                                // to instead of re-applying yo itself, so we go with isPaidCert.
                                if (cohort && !application.isPaidCert) {
                                    scope.canReapply = application.canReapplyTo(cohort);
                                    scope.cohortToReapplyTo = cohort;
                                }
                            });
                    }

                    // Set translation keys
                    scope.pendingPrimaryKey = `pending_primary_message_${
                        scope.currentUser.relevant_cohort && scope.currentUser.relevant_cohort.supportsAdmissionRounds
                            ? 'rounds'
                            : 'rolling'
                    }`;
                    scope.pendingSecondaryKey =
                        scope.currentUser.relevant_cohort.applicationStatusPendingSecondaryMessageKey;

                    if (!scope.currentUser.showPaidCertSelection) {
                        if (scope.currentUser.lastCohortApplicationStatus === 'expelled') {
                            scope.rejectedPrimaryKey = 'expelled_primary_message';
                        } else if (
                            scope.currentUser.lastCohortApplication.rejected_after_pre_accepted &&
                            scope.currentUser.relevant_cohort.isDegreeProgram
                        ) {
                            scope.rejectedPrimaryKey = scope.canReapply
                                ? scope.currentUser.relevant_cohort
                                      .applicationStatusRejectedAfterPreAcceptedPrimaryMessageKey
                                : scope.currentUser.relevant_cohort
                                      .applicationStatusRejectedAfterPreAcceptedPrimaryMessageKeyCantReapply;
                        } else if (
                            scope.currentUser.relevant_cohort.applicationStatusRejectedPrimaryMessageKeyEnableQuantic
                        ) {
                            scope.rejectedPrimaryKey =
                                scope.currentUser.relevant_cohort.applicationStatusRejectedPrimaryMessageKeyEnableQuantic;
                        } else if (
                            !scope.canReapply &&
                            Cohort.supportsAdmissionRounds(scope.currentUser.relevant_cohort.program_type)
                        ) {
                            scope.rejectedPrimaryKey =
                                scope.currentUser.relevant_cohort.applicationStatusRejectedPrimaryMessageKeyCantReapply;
                        } else {
                            scope.rejectedPrimaryKey =
                                scope.currentUser.relevant_cohort.applicationStatusRejectedPrimaryMessageKey;
                        }
                    } else {
                        scope.rejectedPrimaryKey = 'expelled_primary_message_simple';
                    }

                    if (scope.currentUser.isPreAccepted) {
                        scope.preAcceptedPrimaryKey =
                            scope.currentUser.relevant_cohort.applicationStatusPreAcceptedPrimaryMessageKey;
                    } else if (scope.currentUser.isAccepted) {
                        scope.acceptedPrimaryKey = scope.currentUser.isFailed
                            ? 'failed_unfortunately'
                            : scope.currentUser.relevant_cohort.applicationStatusAcceptedPrimaryMessageKey;

                        if (scope.showTuitionAndRegistration) {
                            scope.acceptedPrimaryKey = null;
                        }
                    }

                    if (scope.cohortToReapplyTo) {
                        _setReapplyMessaging(scope.cohortToReapplyTo);
                    }
                }

                function _setReapplyMessaging(cohort) {
                    const appliedAt = scope.currentUser.lastCohortApplication.appliedAt;
                    const pendingDeadlineMessage = cohort.getApplicationDeadlineMessage(appliedAt);
                    const lastAppCohortIsPromoted =
                        scope.currentUser.relevant_cohort.id === scope.currentUser.lastCohortApplication.cohort_id;
                    const rejectedDeadlineMessage = lastAppCohortIsPromoted
                        ? cohort.getReapplyAfterDeadlineMessage()
                        : cohort.getApplicationDeadlineMessage();

                    // get the localized foundation playlist for suggestions
                    if (cohort.foundations_playlist_locale_pack) {
                        scope.foundationsPlaylistLocaleString = cohort.localizedFoundationsPlaylist.title;
                    }

                    if (cohort.admissionRoundDeadlineHasPassed(appliedAt)) {
                        scope.pendingPrimaryKey = 'deadline_passed_with_pending_application';
                    }

                    scope.pendingApplicationPrimaryMessage = translationHelper.get(
                        scope.pendingPrimaryKey,
                        {
                            brandName: scope.brandNameShort,
                            brandEmail: scope.brandSupportEmail,
                            applicationDeadlineWithDecision: pendingDeadlineMessage,
                        },
                        null,
                    );

                    scope.reapplyMessage = translationHelper.get(
                        scope.rejectedPrimaryKey,
                        {
                            reapplyAfterDeadline: rejectedDeadlineMessage,
                            cohortTitle: cohort.title,
                            programType: cohort.program_type.toUpperCase(),
                        },
                        null,
                    );
                }

                scope.$watch('showPaidCertCheckout', val => {
                    scope.promotedCohorts = null;
                    if (val) {
                        Cohort.index({
                            filters: {
                                promoted: true,
                                published: true,
                            },
                        }).then(response => {
                            scope.promotedCohorts = response.result;
                            onApplicationsAndCohortsLoaded();
                        });
                    }
                });

                scope.$watch('currentUser.lastCohortApplication', () => {
                    onApplicationsAndCohortsLoaded();
                });

                //-------------------------
                // Pending
                //-------------------------

                scope.reapplyOrEditApplication = () => {
                    const paidCertApplication = _.findWhere(scope.currentUser.cohort_applications, {
                        isPaidCert: true,
                        status: 'pre_accepted',
                    });
                    if (paidCertApplication) {
                        paidCertApplication.destroy();
                        const index = scope.currentUser.cohort_applications.indexOf(paidCertApplication);
                        scope.currentUser.cohort_applications.splice(index, 1);
                    }
                    scope.currentUser.reapplyingOrEditingApplication = true;
                    const page =
                        scope.currentUser.lastCohortApplication &&
                        scope.currentUser.lastCohortApplication.can_convert_to_emba
                            ? '1'
                            : '2';
                    $injector.get('$location').path('/settings/application').search({
                        page,
                    });
                };

                function updateVisibilityToggles() {
                    if (!scope.currentUser) {
                        return;
                    }

                    // Tuition and Billing
                    scope.showingUnregisteredState =
                        scope.showTuitionAndRegistration && scope.currentUser.needsToRegister;
                }

                // this can change, so watch it...
                scope.$watchGroup(
                    [
                        'currentUser.needsToRegister',
                        'currentUser.lastCohortApplication.program_type',
                        'currentUser.lastCohortApplication.status',
                        'currentUser.lastCohortApplication.in_good_standing',
                    ],
                    () => {
                        updateVisibilityToggles();
                        onApplicationsAndCohortsLoaded();
                    },
                );
                updateVisibilityToggles();

                //-------------------------
                // Pre-Accepted - EMBA modal
                //-------------------------
                const stopWatchingIsPreAccepted = scope.$watch('currentUser.isPreAccepted', isPreAccepted => {
                    // Determine if the application accepted message should be displayed
                    if (
                        isPreAccepted &&
                        !scope.currentUser.hasSeenAccepted &&
                        Cohort.supportsAcceptanceMessageOnApplicationPage(scope.currentUser.programType)
                    ) {
                        DialogModal.alert({
                            content: '<application-accepted></application-accepted>',
                            size: 'fullscreen',
                            hideCloseButton: true,
                            closeOnClick: false,
                            classes: ['accepted-application-modal'],
                        });

                        if (isPreAccepted && !scope.currentUser.ghostMode) {
                            scope.currentUser.hasSeenAccepted = true;
                            scope.currentUser.save();
                        }

                        stopWatchingIsPreAccepted();
                    }
                });

                scope.$on('$destroy', () => {
                    DialogModal.hideAlerts();
                });
            },
        };
    },
]);
