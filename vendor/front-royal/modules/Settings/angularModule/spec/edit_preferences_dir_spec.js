import 'AngularSpecHelper';
import 'Settings/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import editPreferencesLocales from 'Settings/locales/settings/edit_preferences-en.json';

setSpecLocales(editPreferencesLocales);

describe('Settings::EditPreferences', () => {
    let renderer;
    let elem;
    let SpecHelper;
    let $injector;
    let ngToast;
    let currentUser;
    let User;
    let $translate;
    let $q;
    let frontRoyalStore;

    beforeEach(() => {
        window.IGNORE_TRANSLATION_ERRORS = true;

        angular.mock.module('FrontRoyal.Settings', 'SpecHelper');

        angular.mock.module($provide => {
            frontRoyalStore = {
                enabled: false,
            };
            $provide.value('frontRoyalStore', frontRoyalStore);
        });

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            SpecHelper = $injector.get('SpecHelper');
            ngToast = $injector.get('ngToast');
            User = $injector.get('User');
            $translate = $injector.get('$translate');
            $q = $injector.get('$q');
        });

        SpecHelper.stubConfig();
        currentUser = SpecHelper.stubCurrentUser('learner');
    });

    afterEach(() => {
        window.IGNORE_TRANSLATION_ERRORS = false;
    });

    describe('standard view', () => {
        beforeEach(() => {
            currentUser.pref_sound_enabled = true;
            currentUser.pref_decimal_delim = '.';
            currentUser.pref_locale = 'en';

            render();

            jest.spyOn(ngToast, 'create').mockImplementation(() => {});
        });

        it('should allow for the enabling or disabling of sound effects', () => {
            // initial expectations
            expect(currentUser.pref_sound_enabled).toBe(true);
            User.expect('update');
            SpecHelper.toggleCheckbox(elem, 'form #option_0');
            User.flush('update');

            expect(ngToast.create).toHaveBeenCalledWith({
                content: "'Sound Effects' successfully updated.",
                className: 'success',
            });

            User.expect('update');
            SpecHelper.toggleCheckbox(elem, 'form #option_0');
            User.flush('update');

            expect(ngToast.create).toHaveBeenCalledWith({
                content: "'Sound Effects' successfully updated.",
                className: 'success',
            });
        });

        it('should allow for the enabling or disabling of keyboard shortcuts', () => {
            // initial expectations
            expect(currentUser.pref_keyboard_shortcuts).toBe(true);
            User.expect('update');
            SpecHelper.toggleCheckbox(elem, 'form #option_1');
            User.flush('update');

            expect(ngToast.create).toHaveBeenCalledWith({
                content: "'Keyboard Shortcuts' successfully updated.",
                className: 'success',
            });
            expect(currentUser.pref_keyboard_shortcuts).toBe(false);

            User.expect('update');
            SpecHelper.toggleCheckbox(elem, 'form #option_1');
            User.flush('update');
            expect(ngToast.create).toHaveBeenCalledWith({
                content: "'Keyboard Shortcuts' successfully updated.",
                className: 'success',
            });
            expect(currentUser.pref_keyboard_shortcuts).toBe(true);
        });

        it('should allow for the toggling of decimal input delimeters', () => {
            const button1 = SpecHelper.expectElement(elem, 'form .btn-group:eq(0) button:eq(0).active');
            const button2 = SpecHelper.expectElement(elem, 'form .btn-group:eq(0) button:eq(1):not(.active)');

            User.expect('update');
            button2.click();
            expect(currentUser.pref_decimal_delim).toEqual(',');
            User.flush('update');
            expect(ngToast.create).toHaveBeenCalledWith({
                content: "'Decimal Input' successfully updated.",
                className: 'success',
            });

            User.expect('update');
            button1.click();
            User.flush('update');
            expect(currentUser.pref_decimal_delim).toEqual('.');
            expect(ngToast.create).toHaveBeenCalledWith({
                content: "'Decimal Input' successfully updated.",
                className: 'success',
            });
        });

        it('should allow for the toggling of locale', () => {
            const enButton = SpecHelper.expectElementText(elem, 'form .btn-group:eq(1) button:eq(0).active', 'English');
            const esButton = SpecHelper.expectElementText(
                elem,
                'form .btn-group:eq(1) button:eq(1):not(.active)',
                'Español',
            );

            // FIXME: For some reason, our spec_helper.js translation setup destroys
            // our translation tables when calling $translate.refresh().
            // See also: https://trello.com/c/ISkBg2uX
            jest.spyOn($translate, 'refresh').mockImplementation(() => $q.when());

            User.expect('update');
            esButton.click();
            expect(currentUser.pref_locale).toEqual('es');
            expect($('html').attr('lang')).toEqual('es');
            User.flush('update');

            // See comment near expect(ngToast.create).toHaveBeenCalled(); in
            // 'should allow for the toggling of locale via select'
            expect(ngToast.create).toHaveBeenCalledWith({
                className: 'success',
                content: "'Preferred Language' successfully updated.",
            });
            ngToast.create.mockClear();

            User.expect('update');
            enButton.click();
            expect(currentUser.pref_locale).toEqual('en');
            // If we try to flush this second update, it fails due to some odd
            // interaction between iguana-mock and the setter on pref_locale
        });

        it('should gracefully handle locale loading failures', () => {
            const esButton = SpecHelper.expectElementText(
                elem,
                'form .btn-group:eq(1) button:eq(1):not(.active)',
                'Español',
            );

            jest.spyOn($translate, 'use').mockImplementation(code => {
                if (!code) {
                    return $q.resolve('success');
                }
                return $q.reject('load failure');
            });

            esButton.click();
            expect(currentUser.pref_locale).not.toEqual('es');
            expect($('html').attr('lang')).not.toEqual('es');

            expect(ngToast.create).toHaveBeenCalledWith({
                className: 'danger',
                content: 'There was a problem updating your preferences. Please try again later.',
            });
        });
    });

    describe('hiring manager view', () => {
        beforeEach(() => {
            Object.defineProperty(currentUser, 'defaultsToHiringExperience', {
                value: true,
            });
            currentUser.pref_locale = 'en';
            currentUser.pref_show_photos_names = true;
            currentUser.pref_positions_candidate_list = true;
            render();
            jest.spyOn(ngToast, 'create').mockImplementation(() => {});
        });

        it('should allow for the toggling of locale via select', () => {
            // FIXME: For some reason, our spec_helper.js translation setup destroys
            // our translation tables when calling $translate.refresh().
            // See also: https://trello.com/c/ISkBg2uX
            jest.spyOn($translate, 'refresh').mockImplementation(() => $q.when());

            User.expect('update');

            SpecHelper.updateSelect(
                elem,
                'select[name="option_0"]',
                elem.find('select:eq(0) option:eq(1)').val().replace('string:', ''),
            );
            expect(currentUser.pref_locale).toEqual('es');
            expect($('html').attr('lang')).toEqual('es');
            User.flush('update');

            // We used to check that the string here had the Spanish locale,
            // but after modularizing, the full language switching was no
            // longer working.  Probably wrong anyway for this test to be testing
            // things so far outside of its purview as angular translate language
            // switching, so I just removed the assertion
            expect(ngToast.create).toHaveBeenCalled();
            ngToast.create.mockClear();

            User.expect('update');
            SpecHelper.updateSelect(
                elem,
                'select:eq(0)',
                elem.find('select:eq(0) option:eq(0)').val().replace('string:', ''),
            );
            expect(currentUser.pref_locale).toEqual('en');
        });

        it('should allow for the toggling of photos and names display via select', () => {
            User.expect('update');

            SpecHelper.updateSelect(
                elem,
                'select[name="option_1"]',
                elem.find('select:eq(1) option:eq(1)').val().replace('boolean:', '') === 'true',
            );
            expect(currentUser.pref_show_photos_names).toEqual(false);
            User.flush('update');
            expect(ngToast.create).toHaveBeenCalledWith({
                content: "'Candidate Photos &amp; Names' successfully updated.",
                className: 'success',
            });
            ngToast.create.mockClear();

            User.expect('update');
            SpecHelper.updateSelect(
                elem,
                'select[name="option_1"]',
                elem.find('select:eq(1) option:eq(0)').val().replace('boolean:', '') === 'true',
            );
            expect(currentUser.pref_show_photos_names).toEqual(true);
        });

        it('should allow for the toggling of positions candidate list review style via select', () => {
            User.expect('update');

            SpecHelper.updateSelect(
                elem,
                'select[name="option_2"]',
                elem.find('select:eq(1) option:eq(1)').val().replace('boolean:', '') === 'true',
            );
            expect(currentUser.pref_positions_candidate_list).toEqual(false);
            User.flush('update');
            expect(ngToast.create).toHaveBeenCalledWith({
                content: "'Position Applicant View' successfully updated.",
                className: 'success',
            });
            ngToast.create.mockClear();

            User.expect('update');
            SpecHelper.updateSelect(
                elem,
                'select[name="option_2"]',
                elem.find('select:eq(1) option:eq(0)').val().replace('boolean:', '') === 'true',
            );
            expect(currentUser.pref_positions_candidate_list).toEqual(true);
        });
    });

    describe('clear local database', () => {
        it('should be hidden if !frontRoyalStore.enabled', () => {
            frontRoyalStore.enabled = false;
            render();
            SpecHelper.expectNoElement(elem, '.clear-local-database-section');
        });

        it('should work if frontRoyalStore.enabled', async () => {
            jest.spyOn(ngToast, 'create').mockImplementation(() => {});
            let resolveClearDb;
            const promise = new Promise(resolve => {
                resolveClearDb = resolve;
            });
            frontRoyalStore.enabled = true;
            frontRoyalStore.retryAfterHandledError = jest.fn(); // preventing error in ConfigFactory watcher
            render();
            frontRoyalStore.clearAllTables = jest.fn().mockReturnValue(promise);
            SpecHelper.expectNoElement(elem, '.clear-local-database-section front-royal-spinner');

            // clicking button should call _clearDb and show spinner
            SpecHelper.click(elem, '.clear-local-database-section button');
            expect(frontRoyalStore.clearAllTables).toHaveBeenCalled();
            SpecHelper.expectElement(elem, '.clear-local-database-section front-royal-spinner');
            SpecHelper.expectNoElement(elem, '.clear-local-database-section button');
            expect(ngToast.create).not.toHaveBeenCalled();

            // Once db is cleared spinner goes away and button comes back
            resolveClearDb();
            await jest.advanceTimersByTime(1);
            expect(ngToast.create).toHaveBeenCalledWith({
                className: 'success',
                content: 'Database cleared',
            });
            SpecHelper.expectNoElement(elem, '.clear-local-database-section front-royal-spinner');
            SpecHelper.expectElement(elem, '.clear-local-database-section button');
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.render('<edit-preferences></edit-preferences>');
        elem = renderer.elem;
    }
});
