import 'AngularSpecHelper';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohort_applications';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';
import 'Settings/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import applicationStatusLocales from 'Settings/locales/settings/application_status-en.json';
import shareButtonsLocales from 'FrontRoyalForm/locales/front_royal_form/share_buttons-en.json';

setSpecLocales(applicationStatusLocales, shareButtonsLocales);

describe('Settings::applicationStatus', () => {
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let $injector;
    let currentUser;
    let Cohort;
    let CareerProfile;
    let CohortApplication;
    let DialogModal;
    let User;
    let $rootScope;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Settings', 'SpecHelper');

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            SpecHelper = $injector.get('SpecHelper');
            Cohort = $injector.get('Cohort');
            CareerProfile = $injector.get('CareerProfile');
            CohortApplication = $injector.get('CohortApplication');
            DialogModal = $injector.get('DialogModal');
            User = $injector.get('User');
            $rootScope = $injector.get('$rootScope');

            $injector.get('CareerProfileFixtures');
            $injector.get('CohortFixtures');
            $injector.get('CohortApplicationFixtures');
        });

        currentUser = SpecHelper.stubCurrentUser('learner');
        SpecHelper.stubConfig();
        SpecHelper.stubStripeCheckout();
        SpecHelper.stubDirective('paidCertificateCheckout');
        SpecHelper.stubDirective('tuitionStatusUnregisteredAlternative');
        SpecHelper.stubDirective('tuitionStatusUnregistered');
        SpecHelper.stubDirective('tuitionStatusRegistered');

        currentUser.relevant_cohort = Cohort.fixtures.getInstance({
            program_type: 'mba',
            foundations_playlist_locale_pack: {
                content_items: [
                    {
                        locale: 'en',
                        title: 'Business Foundations',
                    },
                ],
            },
        });

        currentUser.career_profile = CareerProfile.fixtures.getInstance();
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.render('<application-status></application-status>');
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    describe('paymentRequired', () => {
        it('should be false if there is no currentUser', () => {
            render();
            $rootScope.currentUser = null;
            expect(scope.paymentRequired).toBe(false);
        });

        describe('when currentUser', () => {
            it('should be false if the currentUser has no lastCohortApplication', () => {
                jest.spyOn(currentUser, 'lastCohortApplication', 'get').mockReturnValue(null);
                render();
                expect(scope.paymentRequired).toBe(false);
            });

            describe('when lastCohortApplication is present', () => {
                let cohortApplication;
                let existingPlan;

                beforeEach(() => {
                    cohortApplication = CohortApplication.fixtures.getInstance();
                    existingPlan = {
                        frequency: 'not_once',
                    };
                    jest.spyOn(currentUser, 'lastCohortApplication', 'get').mockReturnValue(cohortApplication);
                    cohortApplication.in_good_standing = false;
                });

                it('should be false if in_good_standing and the currentUser has no primary_subscription', () => {
                    cohortApplication.in_good_standing = true;
                    jest.spyOn(currentUser, 'primarySubscription', 'get').mockReturnValue(null);
                    render();
                    expect(scope.paymentRequired).toBe(false);
                });

                it('should return false if existingPlan is for single-payment', () => {
                    existingPlan.frequency = 'once';
                    jest.spyOn(cohortApplication, 'existingPlan', 'get').mockReturnValue(existingPlan);
                    render();
                    expect(scope.paymentRequired).toBe(false);
                });

                it('should return false if total_num_required_stripe_payments is null', () => {
                    cohortApplication.total_num_required_stripe_payments = null;
                    render();
                    expect(scope.paymentRequired).toBe(false);
                });

                it('should return false if total_num_required_stripe_payments is 0', () => {
                    cohortApplication.total_num_required_stripe_payments = 0;
                    render();
                    expect(scope.paymentRequired).toBe(false);
                });

                describe('when total_num_required_stripe_payments is greater than 0', () => {
                    beforeEach(() => {
                        cohortApplication.total_num_required_stripe_payments = 1;
                    });

                    it('should return false if hasFullScholarship', () => {
                        jest.spyOn(cohortApplication, 'hasFullScholarship', 'get').mockReturnValue(true);
                        render();
                        expect(scope.paymentRequired).toBe(false);
                    });

                    it('should return true if !hasFullScholarship', () => {
                        jest.spyOn(cohortApplication, 'hasFullScholarship', 'get').mockReturnValue(false);
                        render();
                        expect(scope.paymentRequired).toBe(true);
                    });
                });
            });
        });
    });

    describe('showTuitionAndRegistration', () => {
        const showTuitionAndRegistrationFlags = [
            'showTuitionStatusUnregisteredAlternative',
            'showTuitionStatusUnregistered',
            'showTuitionStatusRegistered',
        ];

        beforeEach(() => {
            jest.spyOn(currentUser, 'lastCohortApplication', 'get').mockReturnValue({});
            render();
        });

        ['isPreAccepted', 'isAccepted'].forEach(statusFlag => {
            showTuitionAndRegistrationFlags.forEach(showTuitionAndRegistrationFlag => {
                it(`should be true if the currentUser ${statusFlag}, their cohort supportsRecurringPayments, and ${showTuitionAndRegistrationFlag}`, () => {
                    jest.spyOn(currentUser, statusFlag, 'get').mockReturnValue(true);
                    jest.spyOn(Cohort, 'supportsRecurringPayments').mockReturnValue(true);
                    _.each(showTuitionAndRegistrationFlags, _showTuitionAndRegistrationFlag => {
                        jest.spyOn(scope, _showTuitionAndRegistrationFlag, 'get').mockReturnValue(
                            _showTuitionAndRegistrationFlag === showTuitionAndRegistrationFlag,
                        );
                    });
                    scope.$digest();
                    expect(scope.showTuitionAndRegistration).toBe(true);
                });
            });
        });

        it('should be false if failed', () => {
            jest.spyOn(currentUser, 'isAccepted', 'get').mockReturnValue(true);
            jest.spyOn(Cohort, 'supportsRecurringPayments').mockReturnValue(true);
            jest.spyOn(currentUser, 'isFailed', 'get').mockReturnValue(true);
            jest.spyOn(scope, 'showTuitionStatusUnregisteredAlternative', 'get').mockReturnValue(true);
            scope.$digest();
            expect(scope.showTuitionAndRegistration).toBe(false);
        });
    });

    describe('showTuitionStatusUnregisteredAlternative', () => {
        it("should be true if the currentUser's lastCohortApplication is !registered and showAlternateRegistration", () => {
            const cohortApplication = CohortApplication.fixtures.getInstance({
                registered: true,
            });
            const showAlternateRegistrationSpy = jest
                .spyOn(cohortApplication, 'showAlternateRegistration', 'get')
                .mockReturnValue(false);
            jest.spyOn(currentUser, 'lastCohortApplication', 'get').mockReturnValue(cohortApplication);
            render();
            expect(scope.showTuitionStatusUnregisteredAlternative).toBe(false);

            scope.currentUser.lastCohortApplication.registered = false;
            expect(scope.showTuitionStatusUnregisteredAlternative).toBe(false);

            scope.currentUser.lastCohortApplication.registered = true;
            showAlternateRegistrationSpy.mockReturnValue(true);
            expect(scope.showTuitionStatusUnregisteredAlternative).toBe(false);

            scope.currentUser.lastCohortApplication.registered = false;
            expect(scope.showTuitionStatusUnregisteredAlternative).toBe(true);
        });
    });

    describe('showTuitionStatusUnregistered', () => {
        it("should be true if the currentUser's lastCohortApplication is !registered and !showAlternateRegistration", () => {
            const cohortApplication = CohortApplication.new({
                registered: true,
            });
            const showAlternateRegistrationSpy = jest
                .spyOn(cohortApplication, 'showAlternateRegistration', 'get')
                .mockReturnValue(true);
            jest.spyOn(currentUser, 'lastCohortApplication', 'get').mockReturnValue(cohortApplication);
            render();
            expect(scope.showTuitionStatusUnregistered).toBe(false);

            scope.currentUser.lastCohortApplication.registered = false;
            expect(scope.showTuitionStatusUnregistered).toBe(false);

            scope.currentUser.lastCohortApplication.registered = true;
            showAlternateRegistrationSpy.mockReturnValue(false);
            expect(scope.showTuitionStatusUnregistered).toBe(false);

            scope.currentUser.lastCohortApplication.registered = false;
            expect(scope.showTuitionStatusUnregistered).toBe(true);
        });
    });

    describe('showTuitionStatusRegistered', () => {
        it("should be true if the currentUser's lastCohortApplication is registered and paymentRequired", () => {
            const cohortApplication = CohortApplication.new({
                registered: false,
            });
            jest.spyOn(currentUser, 'lastCohortApplication', 'get').mockReturnValue(cohortApplication);
            render();
            const paymentRequiredSpy = jest.spyOn(scope, 'paymentRequired', 'get').mockReturnValue(false);
            expect(scope.showTuitionStatusRegistered).toBe(false);

            scope.currentUser.lastCohortApplication.registered = true;
            expect(scope.showTuitionStatusRegistered).toBe(false);

            scope.currentUser.lastCohortApplication.registered = false;
            paymentRequiredSpy.mockReturnValue(true);
            expect(scope.showTuitionStatusRegistered).toBe(false);

            scope.currentUser.lastCohortApplication.registered = true;
            expect(scope.showTuitionStatusRegistered).toBe(true);
        });
    });

    describe('Pending Application', () => {
        beforeEach(() => {
            Object.defineProperty(currentUser, 'hasPendingCohort', {
                value: true,
            });
        });

        it('should go home if button is pressed', () => {
            currentUser.cohort_applications.push({
                status: 'pending',
                program_type: 'mba',
            });
            render();
            jest.spyOn(scope, 'loadRoute').mockImplementation(() => {});
            SpecHelper.click(elem, '[name="home"]');
            expect(scope.loadRoute).toHaveBeenCalledWith('/home');
        });

        it('should set expected pending application message for MBA if application deadline has not passed', () => {
            currentUser.relevant_cohort = Cohort.fixtures.getInstance({
                program_type: 'mba',
            });

            const expectedAdmissionRound = currentUser.relevant_cohort.admission_rounds[0];
            jest.spyOn(currentUser.relevant_cohort, 'getApplicableAdmissionRound').mockReturnValue(
                expectedAdmissionRound,
            );
            jest.spyOn(currentUser.relevant_cohort, 'admissionRoundDeadlineHasPassed').mockReturnValue(false);

            currentUser.cohort_applications.push({
                status: 'pending',
                program_type: 'mba',
                appliedAt: expectedAdmissionRound.applicationDeadline - 1,
            });

            render();
            const dateHelper = $injector.get('dateHelper');
            const formattedDeadline = dateHelper.formattedUserFacingMonthDayYearLong(
                expectedAdmissionRound.applicationDeadline,
            );
            const formattedDecision = dateHelper.formattedUserFacingMonthDayYearLong(
                expectedAdmissionRound.decisionDate,
            );

            SpecHelper.expectElementText(
                elem,
                '#primary-message',
                `Thank you for applying to Quantic. Applications will be reviewed at the end of the current submission cycle. Round 1 applications close ${formattedDeadline}. Admissions decisions will be delivered by email on ${formattedDecision}.`,
            );
            SpecHelper.expectElement(elem, 'button[name="edit-application"]');
            SpecHelper.expectElementText(elem, 'h2[name="title"]', 'Quantic MBA Application');
        });

        it('should set expected pending application message for EMBA if application deadline has not passed', () => {
            currentUser.relevant_cohort = Cohort.fixtures.getInstance({
                program_type: 'emba',
            });

            const expectedAdmissionRound = currentUser.relevant_cohort.admission_rounds[0];
            jest.spyOn(currentUser.relevant_cohort, 'getApplicableAdmissionRound').mockReturnValue(
                expectedAdmissionRound,
            );
            jest.spyOn(currentUser.relevant_cohort, 'admissionRoundDeadlineHasPassed').mockReturnValue(false);

            currentUser.cohort_applications.push({
                status: 'pending',
                program_type: 'emba',
                appliedAt: expectedAdmissionRound.applicationDeadline - 1,
            });

            render();
            const dateHelper = $injector.get('dateHelper');
            const formattedDeadline = dateHelper.formattedUserFacingMonthDayYearLong(
                expectedAdmissionRound.applicationDeadline,
            );
            const formattedDecision = dateHelper.formattedUserFacingMonthDayYearLong(
                expectedAdmissionRound.decisionDate,
            );

            SpecHelper.expectElementText(
                elem,
                '#primary-message',
                `Thank you for applying to Quantic. Applications will be reviewed at the end of the current submission cycle. Round 1 applications close ${formattedDeadline}. Admissions decisions will be delivered by email on ${formattedDecision}.`,
            );
            SpecHelper.expectElement(elem, 'button[name="edit-application"]');
            SpecHelper.expectElementText(elem, 'h2[name="title"]', 'Quantic EMBA Application');
        });

        it('should set expected pending application message if application deadline has passed', () => {
            const expectedAdmissionRound = currentUser.relevant_cohort.admission_rounds[0];
            jest.spyOn(currentUser.relevant_cohort, 'getApplicableAdmissionRound').mockReturnValue(
                expectedAdmissionRound,
            );
            jest.spyOn(currentUser.relevant_cohort, 'admissionRoundDeadlineHasPassed').mockReturnValue(true);

            currentUser.cohort_applications.push({
                program_type: 'mba',
                status: 'pending',
                appliedAt: expectedAdmissionRound.applicationDeadline - 1,
            });

            render();
            const dateHelper = $injector.get('dateHelper');
            const formattedDeadline = dateHelper.formattedUserFacingMonthDayYearLong(
                expectedAdmissionRound.applicationDeadline,
            );
            const formattedDecision = dateHelper.formattedUserFacingMonthDayYearLong(
                expectedAdmissionRound.decisionDate,
            );

            SpecHelper.expectElementText(
                elem,
                '#primary-message',
                `Thank you for applying to Quantic. Round 1 applications closed on ${formattedDeadline} and are now in review. Admissions decisions will be delivered by email on ${formattedDecision}.`,
            );
            SpecHelper.expectElement(elem, 'button[name="edit-application"]');
        });

        it('should set expected pending application message if application deadline has passed and it is not a multi-round Cohort', () => {
            const expectedAdmissionRound = currentUser.relevant_cohort.admission_rounds[0];
            currentUser.relevant_cohort.admission_rounds = [expectedAdmissionRound];

            jest.spyOn(currentUser.relevant_cohort, 'getApplicableAdmissionRound').mockReturnValue(
                expectedAdmissionRound,
            );
            jest.spyOn(currentUser.relevant_cohort, 'admissionRoundDeadlineHasPassed').mockReturnValue(true);

            currentUser.cohort_applications.push({
                program_type: 'mba',
                status: 'pending',
                appliedAt: expectedAdmissionRound.applicationDeadline - 1,
            });

            render();
            const dateHelper = $injector.get('dateHelper');
            const formattedDeadline = dateHelper.formattedUserFacingMonthDayYearLong(
                expectedAdmissionRound.applicationDeadline,
            );
            const formattedDecision = dateHelper.formattedUserFacingMonthDayYearLong(
                expectedAdmissionRound.decisionDate,
            );

            SpecHelper.expectElementText(
                elem,
                '#primary-message',
                `Thank you for applying to Quantic. Applications closed on ${formattedDeadline} and are now in review. Admissions decisions will be delivered by email on ${formattedDecision}.`,
            );
            SpecHelper.expectElement(elem, 'button[name="edit-application"]');
        });

        it('should show thank you and share with friends for Career Network Only', () => {
            currentUser.relevant_cohort = Cohort.fixtures.getInstance({
                program_type: 'career_network_only',
            });
            currentUser.relevant_cohort.admission_rounds = undefined;

            currentUser.cohort_applications.push({
                status: 'pending',
                program_type: 'career_network_only',
            });

            render();

            SpecHelper.expectNoElement(elem, '#secondary-message');
            SpecHelper.expectNoElement(elem, '#primary-message');
            SpecHelper.expectElement(elem, '.referral');
            SpecHelper.expectNoElement(elem, 'h2[name="title"]');
        });

        it('should set expected pending application message for cohorts with rolling admissions', () => {
            Object.defineProperty(currentUser.relevant_cohort, 'supportsAdmissionRounds', {
                value: false,
            });

            currentUser.cohort_applications = [
                {
                    status: 'pending',
                    program_type: currentUser.relevant_cohort.program_type,
                },
            ];

            render();

            SpecHelper.expectElementText(
                elem,
                '#primary-message',
                'Thank you for applying to Quantic. Your application will be reviewed soon.',
            );
            SpecHelper.expectElement(elem, 'button[name="edit-application"]');
        });

        it('should show proper pending secondary messaging for Certificate cohorts', () => {
            const message =
                'In the meantime, you may explore our free courses in the introductory Business Foundations concentration, available on the home screen.';
            const programType = 'the_business_certificate';
            currentUser.cohort_applications.push({
                status: 'pending',
                program_type: programType,
            });
            currentUser.relevant_cohort.program_type = programType;
            currentUser.relevant_cohort.title = 'Some Cohort';

            render();
            expect(currentUser.relevant_cohort.localizedFoundationsPlaylist.title).toEqual('Business Foundations'); // sanity check
            SpecHelper.expectElementText(elem, '#secondary-message', message);
            SpecHelper.expectElementText(elem, 'h2[name="title"]', 'Some Cohort Application');
        });

        describe('edit application button', () => {
            let searchSpy;
            let $location;
            let button;

            beforeEach(() => {
                Object.defineProperty(currentUser, 'cohort_applications', {
                    value: [
                        {
                            cohort_id: 9999,
                            program_type: 'mba',
                            status: 'pending',
                        },
                    ],
                });
                expect(currentUser.hasPendingCohort).toBe(true);
                jest.spyOn(currentUser.relevant_cohort, 'admissionRoundDeadlineHasPassed').mockReturnValue(false);

                render();

                searchSpy = jest.fn();
                $location = $injector.get('$location');
                jest.spyOn($location, 'path').mockReturnValue({
                    search: searchSpy,
                });
                jest.spyOn(scope, 'reapplyOrEditApplication');

                button = SpecHelper.expectElementText(elem, '[name="edit-application"]button', 'Edit Application');
            });

            it('should work and redirect to page 2 of the application', () => {
                jest.spyOn(scope.currentUser, 'lastCohortApplication', 'get').mockReturnValue(
                    CohortApplication.fixtures.getInstance({
                        can_convert_to_emba: false,
                    }),
                );
                button.click();
                scope.$digest();
                expect(scope.reapplyOrEditApplication).toHaveBeenCalled();

                expect($location.path).toHaveBeenCalledWith('/settings/application');
                expect(searchSpy).toHaveBeenCalledWith({
                    page: '2',
                });
            });

            it('should redirect to page 1 of the application', () => {
                jest.spyOn(scope.currentUser, 'lastCohortApplication', 'get').mockReturnValue(
                    CohortApplication.fixtures.getInstance({
                        can_convert_to_emba: true,
                    }),
                );
                button.click();
                scope.$digest();
                expect(scope.reapplyOrEditApplication).toHaveBeenCalled();

                expect($location.path).toHaveBeenCalledWith('/settings/application');
                expect(searchSpy).toHaveBeenCalledWith({
                    page: '1',
                });
            });
        });
    });

    describe('Rejected Application', () => {
        it('should show primary messaging for Career Network Only applications and contain a working reapply button', () => {
            // We're mocking shouldSeeQuanticBranding here so that locale message says Smartly instead of Quantic, which is inline
            // with what we'd actually expect in the wild since the career_network_only program type is only for Smartly.
            jest.spyOn(currentUser, 'shouldSeeQuanticBranding', 'get').mockReturnValue(false);

            const programType = 'career_network_only';
            currentUser.relevant_cohort = Cohort.fixtures.getInstance({
                program_type: programType,
                title: 'Some Cohort',
            });
            currentUser.relevant_cohort.admission_rounds = undefined;

            jest.spyOn(currentUser, 'lastCohortApplication', 'get').mockReturnValue(
                CohortApplication.fixtures.getInstance({
                    status: 'rejected',
                    program_type: programType,
                }),
            );

            render();

            SpecHelper.expectElementText(elem, 'h2[name="title"]', 'Some Cohort Application');
            SpecHelper.expectElementText(
                elem,
                '#primary-message',
                'We’re sorry we couldn’t accommodate your profile in the Career Network. We’ll be in touch as soon as we can provide you with quality opportunities.In the meantime, you can use the button below to re-apply if your profile has changed, or if you’d like to apply to one of our MBA programs.',
            );
            scope.reapplyOrEditApplication = jest.fn(); // MbaApplicationMixin integration
            const button = SpecHelper.expectElementText(elem, '[name="reapply"]button', 'Reapply to Smartly');
            SpecHelper.expectElementEnabled(elem, '[name="reapply"]button');
            button.click();
            scope.$digest();
            expect(scope.reapplyOrEditApplication).toHaveBeenCalled();
        });

        it('should show primary messaging for Certificate cohort applications and contain a working reapply button', () => {
            // We're mocking shouldSeeQuanticBranding here so that locale message says Smartly instead of Quantic, which is inline
            // with what we'd actually expect in the wild since the the_business_certificate program type is only for Smartly.
            jest.spyOn(currentUser, 'shouldSeeQuanticBranding', 'get').mockReturnValue(false);

            const programType = 'the_business_certificate';
            currentUser.relevant_cohort = Cohort.fixtures.getInstance({
                program_type: programType,
                title: 'Some Cohort',
            });
            currentUser.relevant_cohort.admission_rounds = undefined;

            jest.spyOn(currentUser, 'lastCohortApplication', 'get').mockReturnValue(
                CohortApplication.fixtures.getInstance({
                    status: 'rejected',
                    program_type: programType,
                }),
            );

            render();

            SpecHelper.expectElementText(elem, 'h2[name="title"]', 'Some Cohort Application');
            SpecHelper.expectElementText(
                elem,
                '#primary-message',
                "We're sorry your application wasn't accepted into the Some Cohort program. You can use the button below to re-apply to one of our other programs, or to revise your profile and try applying again to the certificate program.",
            );
            scope.reapplyOrEditApplication = jest.fn(); // MbaApplicationMixin integration
            const button = SpecHelper.expectElementText(elem, '[name="reapply"]button', 'Reapply to Smartly');
            SpecHelper.expectElementEnabled(elem, '[name="reapply"]button');
            button.click();
            scope.$digest();
            expect(scope.reapplyOrEditApplication).toHaveBeenCalled();
        });

        it('should show primary messaging for other application types and contain a working reapply button if canReapplyTo', () => {
            currentUser.relevant_cohort = Cohort.fixtures.getInstance({
                program_type: 'emba',
            });

            const expectedAdmissionRound = currentUser.relevant_cohort.admission_rounds[0];
            jest.spyOn(currentUser.relevant_cohort, 'getApplicableAdmissionRound').mockReturnValue(
                expectedAdmissionRound,
            );
            jest.spyOn(currentUser.relevant_cohort, 'admissionRoundDeadlineHasPassed').mockReturnValue(false);
            jest.spyOn(currentUser, 'lastCohortApplication', 'get').mockReturnValue(
                CohortApplication.fixtures.getInstance({
                    status: 'rejected',
                    program_type: 'emba',
                }),
            );
            jest.spyOn(currentUser.lastCohortApplication, 'appliedAt', 'get').mockReturnValue(
                expectedAdmissionRound.applicationDeadline - 1,
            );
            jest.spyOn(currentUser.lastCohortApplication, 'canReapplyTo').mockImplementation(() => true);

            render();
            const dateHelper = $injector.get('dateHelper');
            const formattedDeadline = dateHelper.formattedUserFacingMonthDayYearLong(
                expectedAdmissionRound.applicationDeadline,
            );

            SpecHelper.expectElementText(elem, 'h2[name="title"]', 'Quantic EMBA Application');
            SpecHelper.expectElementText(
                elem,
                '#primary-message',
                `The next class will be starting soon! Although you weren’t accepted for the current class, take the opportunity to reapply for the next one. Round 1 applications close ${formattedDeadline}.`,
            );
            scope.reapplyOrEditApplication = jest.fn(); // MbaApplicationMixin integration
            const button = SpecHelper.expectElementText(elem, '[name="reapply"]button', 'Reapply to Quantic');
            button.click();
            scope.$digest();
            expect(scope.reapplyOrEditApplication).toHaveBeenCalled();
        });

        it('should show primary messaging for mba/emba and contain a disabled reapply button if !canReapplyTo', () => {
            currentUser.relevant_cohort = Cohort.fixtures.getInstance({
                program_type: 'emba',
            });

            expect(currentUser.relevant_cohort.admission_rounds.length).toEqual(2); // sanity check
            jest.spyOn(currentUser.relevant_cohort, 'admissionRoundDeadlineHasPassed').mockReturnValue(false);
            jest.spyOn(currentUser, 'lastCohortApplication', 'get').mockReturnValue(
                CohortApplication.fixtures.getInstance({
                    status: 'rejected',
                    program_type: 'emba',
                }),
            );
            jest.spyOn(currentUser.lastCohortApplication, 'canReapplyTo').mockImplementation(() => false);

            render();

            SpecHelper.expectElementText(elem, 'h2[name="title"]', 'Quantic EMBA Application');
            SpecHelper.expectElementText(
                elem,
                '#primary-message',
                'Though you won’t be joining our upcoming EMBA class, we wish you well in your learning and career goals! Check back soon to reapply for the next class.',
            );
            SpecHelper.expectElementDisabled(elem, '[name="reapply"]button');
        });

        describe('when rejected_after_pre_accepted for degree program', () => {
            let expectedAdmissionRound;

            beforeEach(() => {
                currentUser.relevant_cohort = Cohort.fixtures.getInstance({
                    program_type: 'emba',
                });
                jest.spyOn(currentUser.relevant_cohort, 'isDegreeProgram', 'get').mockReturnValue(true);
                expectedAdmissionRound = currentUser.relevant_cohort.admission_rounds[0];

                jest.spyOn(currentUser, 'lastCohortApplication', 'get').mockReturnValue(
                    CohortApplication.fixtures.getInstance({
                        status: 'rejected',
                        program_type: 'emba',
                        rejected_after_pre_accepted: true,
                    }),
                );
            });

            it('should show primary messaging and contain a working reapply button if canReapplyTo', () => {
                jest.spyOn(currentUser.relevant_cohort, 'getApplicableAdmissionRound').mockReturnValue(
                    expectedAdmissionRound,
                );
                jest.spyOn(currentUser.relevant_cohort, 'admissionRoundDeadlineHasPassed').mockReturnValue(false);
                jest.spyOn(currentUser.lastCohortApplication, 'canReapplyTo').mockImplementation(() => true);

                render();
                const dateHelper = $injector.get('dateHelper');
                const formattedDeadline = dateHelper.formattedUserFacingMonthDayYearLong(
                    expectedAdmissionRound.applicationDeadline,
                );

                SpecHelper.expectElementText(elem, 'h2[name="title"]', 'Quantic EMBA Application');
                SpecHelper.expectElementText(
                    elem,
                    '#primary-message',
                    `The next class will be starting soon! Although you weren’t able to join us for the current class, take the opportunity to reapply for the next one. Round 1 applications close ${formattedDeadline}.`,
                );
                scope.reapplyOrEditApplication = jest.fn(); // MbaApplicationMixin integration
                const button = SpecHelper.expectElementText(elem, '[name="reapply"]button', 'Reapply to Quantic');
                button.click();
                scope.$digest();
                expect(scope.reapplyOrEditApplication).toHaveBeenCalled();
            });

            it('should show primary message and contain disabled reapply button if !canReapplyTo', () => {
                expect(currentUser.relevant_cohort.admission_rounds.length).toEqual(2); // sanity check
                expectedAdmissionRound = currentUser.relevant_cohort.admission_rounds[1];
                jest.spyOn(currentUser.relevant_cohort, 'admissionRoundDeadlineHasPassed').mockReturnValue(false);
                jest.spyOn(currentUser.lastCohortApplication, 'canReapplyTo').mockImplementation(() => false);

                render();

                SpecHelper.expectElementText(elem, 'h2[name="title"]', 'Quantic EMBA Application');
                SpecHelper.expectElementText(
                    elem,
                    '#primary-message',
                    'Though you won’t be joining our upcoming EMBA class, we wish you well in your learning and career goals! Check back soon to reapply for the next class.',
                );
                SpecHelper.expectElementDisabled(elem, '[name="reapply"]button');
            });
        });
    });

    describe('Expelled Application', () => {
        it('should show primary messaging for other application types and contain a working reapply button if canReapplyTo', () => {
            currentUser.relevant_cohort = Cohort.fixtures.getInstance({
                program_type: 'emba',
            });

            const expectedAdmissionRound = currentUser.relevant_cohort.admission_rounds[0];
            jest.spyOn(currentUser.relevant_cohort, 'getApplicableAdmissionRound').mockReturnValue(
                expectedAdmissionRound,
            );
            jest.spyOn(currentUser.relevant_cohort, 'admissionRoundDeadlineHasPassed').mockReturnValue(false);

            jest.spyOn(currentUser, 'lastCohortApplication', 'get').mockReturnValue(
                CohortApplication.fixtures.getInstance({
                    status: 'expelled',
                    program_type: 'emba',
                }),
            );
            jest.spyOn(currentUser.lastCohortApplication, 'canReapplyTo').mockImplementation(() => true);

            render();
            const dateHelper = $injector.get('dateHelper');
            const formattedDeadline = dateHelper.formattedUserFacingMonthDayYearLong(
                expectedAdmissionRound.applicationDeadline,
            );

            SpecHelper.expectElementText(
                elem,
                '#primary-message',
                `The next class will be starting soon! Although you were unable to complete the current class, take the opportunity to reapply for the next one. Round 1 applications close ${formattedDeadline}.`,
            );
            scope.reapplyOrEditApplication = jest.fn(); // MbaApplicationMixin integration
            const button = SpecHelper.expectElementText(elem, '[name="reapply"]button', 'Reapply to Quantic');
            button.click();
            scope.$digest();
            expect(scope.reapplyOrEditApplication).toHaveBeenCalled();
        });

        it('should show primary messaging for other application types and contain a disabled reapply button if !canReapplyTo', () => {
            currentUser.relevant_cohort = Cohort.fixtures.getInstance({
                program_type: 'emba',
            });
            expect(currentUser.relevant_cohort.admission_rounds.length).toEqual(2); // sanity check

            const expectedAdmissionRound = currentUser.relevant_cohort.admission_rounds[1];
            jest.spyOn(currentUser.relevant_cohort, 'getApplicableAdmissionRound').mockReturnValue(
                expectedAdmissionRound,
            );
            jest.spyOn(currentUser.relevant_cohort, 'admissionRoundDeadlineHasPassed').mockReturnValue(false);

            jest.spyOn(currentUser, 'lastCohortApplication', 'get').mockReturnValue(
                CohortApplication.fixtures.getInstance({
                    status: 'expelled',
                    program_type: 'emba',
                    cohort_id: currentUser.relevant_cohort.id,
                }),
            );
            jest.spyOn(currentUser.lastCohortApplication, 'appliedAt', 'get').mockReturnValue(
                expectedAdmissionRound.applicationDeadline - 1,
            );
            jest.spyOn(currentUser.lastCohortApplication, 'canReapplyTo').mockImplementation(() => false);

            render();
            const dateHelper = $injector.get('dateHelper');
            const formattedDeadline = dateHelper.formattedUserFacingMonthDayYearLong(
                expectedAdmissionRound.applicationDeadline,
            );

            SpecHelper.expectElementText(
                elem,
                '#primary-message',
                `The next class will be starting soon! Although you were unable to complete the current class, take the opportunity to reapply for the next one. You will be able to re-apply after the round 2 application deadline of ${formattedDeadline}.`,
            );
            SpecHelper.expectElementDisabled(elem, '[name="reapply"]button');
        });
    });

    describe('Accepted Application', () => {
        beforeEach(() => {
            currentUser.career_profile = CareerProfile.fixtures.getInstance({
                email: 'foo@bar.com',
            });
        });

        it('should show primary messaging for EMBA applications when !showTuitionAndRegistration', () => {
            const cohortApplication = ensureUserHasAcceptedCohortApplication('emba');
            cohortApplication.registered = true;
            render();
            const spy = jest.spyOn(scope, 'showTuitionAndRegistration', 'get').mockReturnValue(true);
            scope.$digest();
            SpecHelper.expectNoElement(elem, '#primary-message');

            spy.mockReturnValue(false);
            scope.$digest();
            SpecHelper.expectElementText(
                elem,
                '#primary-message',
                'Congratulations on being accepted to the Quantic EMBA program!',
            );
        });

        it('should show messaging for failed user', () => {
            const cohortApplication = ensureUserHasAcceptedCohortApplication('emba');
            cohortApplication.graduation_status = 'failed';
            render();
            SpecHelper.expectElementText(
                elem,
                '#primary-message',
                'Unfortunately, you did not meet the graduation requirements. You will retain access to your course library. For more information, contact us at support@quantic.edu.',
            );
        });

        it('should show primary messaging for MBA applications', () => {
            assertAcceptedPrimaryMessageForProgramType(
                'mba',
                undefined,
                'Congratulations on being accepted to the Quantic MBA program!',
            );
        });

        it('should show primary messaging for Career Network Only applications', () => {
            // We're mocking shouldSeeQuanticBranding here so that locale message says Smartly instead of Quantic, which is inline
            // with what we'd actually expect in the wild since the career_network_only program type is only for Smartly.
            jest.spyOn(currentUser, 'shouldSeeQuanticBranding', 'get').mockReturnValue(false);

            assertAcceptedPrimaryMessageForProgramType(
                'career_network_only',
                'Career Network',
                'Congratulations on being accepted to the Smartly Career Network!',
            );
        });

        it('should show primary messaging for Certificate cohort applications', () => {
            assertAcceptedPrimaryMessageForProgramType(
                'the_business_certificate',
                'Some Cohort',
                'Congratulations on being accepted to the Some Cohort program!',
            );
        });

        describe('tuition and registration section', assertTuitionAndRegistrationSection);

        function ensureUserHasAcceptedCohortApplication(programType, cohortTitle) {
            const cohort = Cohort.fixtures.getInstance({
                id: 123,
                program_type: programType,
                title: cohortTitle,
            });
            const cohortApplication = CohortApplication.new({
                status: 'accepted',
                program_type: programType,
                cohort_id: cohort.id,
            });

            currentUser.relevant_cohort = cohort;
            currentUser.cohort_applications = [cohortApplication];
            return cohortApplication;
        }

        function assertAcceptedPrimaryMessageForProgramType(programType, cohortTitle, message) {
            ensureUserHasAcceptedCohortApplication(programType, cohortTitle);
            render();
            SpecHelper.expectElementText(elem, '#primary-message', message);
        }
    });

    describe('Pre-Accepted Application', () => {
        beforeEach(() => {
            currentUser.career_profile = CareerProfile.fixtures.getInstance({
                email: 'foo@bar.com',
            });
            currentUser.hasSeenAccepted = true;
        });

        function assertPrimaryMessageForProgramType(programType) {
            currentUser.relevant_cohort = Cohort.fixtures.getInstance({
                program_type: programType,
            });
            jest.spyOn(currentUser, 'lastCohortApplication', 'get').mockReturnValue({
                status: 'pre_accepted',
                program_type: programType,
            });
            render();
            const spy = jest.spyOn(scope, 'showTuitionAndRegistration', 'get').mockReturnValue(true);
            scope.$digest();
            SpecHelper.expectNoElement(elem, '#primary-message');

            spy.mockReturnValue(false);
            scope.$digest();
            SpecHelper.expectElementText(
                elem,
                '#primary-message',
                `Congratulations on being accepted to the Quantic ${programType.toUpperCase()} program!`,
            );
        }

        it('should show primary message for EMBA user if !showTuitionAndRegistration', () => {
            assertPrimaryMessageForProgramType('emba');
        });

        it('should show primary message for MBA user if !showTuitionAndRegistration', () => {
            assertPrimaryMessageForProgramType('mba');
        });

        describe('pre_accepted application modal', () => {
            describe('when EMBA', () => {
                beforeEach(() => {
                    Object.defineProperty(currentUser, 'programType', {
                        value: 'emba',
                    });
                    Object.defineProperty(currentUser, 'isPreAccepted', {
                        value: true,
                        configurable: true,
                    });
                    Object.defineProperty(currentUser, 'lastCohortApplication', {
                        value: {},
                        configurable: true,
                    });
                    currentUser.hasSeenAccepted = false;

                    jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
                });

                it('should show modal and update flag after first load after being pre_accepted', () => {
                    User.expect('update');
                    render();
                    User.flush('update');
                    expect(currentUser.hasSeenAccepted).toBe(true);
                    expect(DialogModal.alert).toHaveBeenCalled();
                });

                it('should show modal and not update flag if in ghostMode (ex. log in as)', () => {
                    currentUser.ghostMode = true;
                    render();
                    expect(currentUser.hasSeenAccepted).toBe(false);
                    expect(DialogModal.alert).toHaveBeenCalled();
                });

                it('should not show modal message if currentUser.hasSeenAccepted', () => {
                    currentUser.has_seen_accepted = true;
                    render();
                    expect(DialogModal.alert).not.toHaveBeenCalled();
                });

                it('should not show modal message if not pre_accepted', () => {
                    Object.defineProperty(currentUser, 'isPreAccepted', {
                        value: false,
                    });
                    render();
                    expect(DialogModal.alert).not.toHaveBeenCalled();
                });
            });

            describe('when not EMBA', () => {
                _.each(
                    ['mba', 'career_network_only', 'the_business_certificate', 'paid_cert_competitive_strategy'],
                    programType => {
                        it(`should not show modal message after being pre_accepted for ${programType}`, () => {
                            Object.defineProperty(currentUser, 'programType', {
                                value: programType,
                            });
                            Object.defineProperty(currentUser, 'isPreAccepted', {
                                value: true,
                            });
                            Object.defineProperty(currentUser, 'lastCohortApplication', {
                                value: {},
                            });
                            currentUser.hasSeenAccepted = false;

                            jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
                            render();
                            expect(DialogModal.alert).not.toHaveBeenCalled();
                        });
                    },
                );
            });

            describe('tuition and registration section', assertTuitionAndRegistrationSection);
        });
    });

    describe('when showPaidCertCheckout', () => {
        it('should load promoted and published cohorts then set the translation and reapply state appropriately', () => {
            jest.spyOn(currentUser, 'showPaidCertSelection', 'get').mockReturnValue(true);

            const rejectedMbaCohort = Cohort.fixtures.getInstance({
                program_type: 'mba',
            });
            const cohortToReapplyTo = Cohort.fixtures.getInstance({
                program_type: 'mba',
            });
            const paidCertCohort = Cohort.fixtures.getInstance({
                program_type: 'paid_cert_data_analytics',
            });
            const rejectedMBACohortApplication = CohortApplication.fixtures.getInstance({
                status: 'rejected',
                program_type: rejectedMbaCohort.program_type,
                applied_at: 0,
                cohort_id: rejectedMbaCohort.id,
            });
            jest.spyOn(rejectedMBACohortApplication, 'canReapplyTo').mockReturnValue(true);
            jest.spyOn(cohortToReapplyTo, 'getReapplyAfterDeadlineMessage').mockReturnValue(
                'You can reapply after the round 2 application deadline of September 20, 2018.',
            );

            currentUser.cohort_applications.push(rejectedMBACohortApplication);
            currentUser.cohort_applications.push(
                CohortApplication.fixtures.getInstance({
                    status: 'pre_accepted',
                    program_type: paidCertCohort.program_type,
                    applied_at: rejectedMBACohortApplication.applied_at + 1000,
                    cohort_id: paidCertCohort.id,
                }),
            );
            currentUser.relevant_cohort = paidCertCohort; // on the server their relevant_cohort is derived from the user's pre_accepted application if possible

            renderAndFlushPromotedCohorts([paidCertCohort, cohortToReapplyTo]);
            expect(rejectedMBACohortApplication.canReapplyTo.mock.calls[0][0].id).toEqual(cohortToReapplyTo.id);
            expect(cohortToReapplyTo.getReapplyAfterDeadlineMessage).toHaveBeenCalled();

            SpecHelper.expectElementText(elem, 'h2[name="title"]', 'Quantic MBA Application');
            SpecHelper.expectElementText(
                elem,
                '#primary-message',
                'Take the opportunity to reapply for the next program. You can reapply after the round 2 application deadline of September 20, 2018.',
            );
            SpecHelper.expectElementText(elem, 'button[name="reapply"]', 'Reapply to Quantic');
            SpecHelper.expectElementEnabled(elem, 'button[name="reapply"]');

            scope.reapplyOrEditApplication = jest.fn(); // MbaApplicationMixin integration
            SpecHelper.click(elem, 'button[name="reapply"]');
            expect(scope.reapplyOrEditApplication).toHaveBeenCalled();
        });

        it('should disable reapply when no application is canReapplyTo', () => {
            jest.spyOn(currentUser, 'showPaidCertSelection', 'get').mockReturnValue(true);
            const application = CohortApplication.fixtures.getInstance();
            jest.spyOn(application, 'canReapplyTo').mockReturnValue(false);
            currentUser.cohort_applications = [application];

            renderAndFlushPromotedCohorts();
            SpecHelper.expectElementDisabled(elem, '[name="reapply"]button');
        });

        it('should show the right thing in accepted state', () => {
            jest.spyOn(currentUser, 'acceptedToPaidCert', 'get').mockReturnValue(true);

            const application = CohortApplication.fixtures.getInstance({
                status: 'accepted',
                program_type: 'paid_cert_data_analytics',
            });
            currentUser.cohort_applications = [application];
            currentUser.relevant_cohort.program_type = 'paid_cert_data_analytics';
            renderAndFlushPromotedCohorts();

            SpecHelper.expectElement(elem, 'paid-certificate-checkout');
            SpecHelper.expectNoElement(elem, '[name="paid-certs"] .sub-text');

            SpecHelper.expectNoElement(elem, '[name="accepted-text"]');
        });

        it('should keep the same element when transitioning from the checkout to the accepted state', () => {
            // Fixing a bug where after being accepted the screen would redraw because the directive was recreated
            const showPaidCertSelectionSpy = jest
                .spyOn(currentUser, 'showPaidCertSelection', 'get')
                .mockReturnValue(true);
            const acceptedToPaidCertSpy = jest.spyOn(currentUser, 'acceptedToPaidCert', 'get').mockReturnValue(false);

            const application = CohortApplication.fixtures.getInstance();
            currentUser.cohort_applications = [application];
            renderAndFlushPromotedCohorts();

            const checkoutElem = SpecHelper.expectElement(elem, 'paid-certificate-checkout');

            showPaidCertSelectionSpy.mockReturnValue(false);
            acceptedToPaidCertSpy.mockReturnValue(true);
            scope.$apply();
            const newCheckoutElem = SpecHelper.expectElement(elem, 'paid-certificate-checkout');
            expect(newCheckoutElem[0]).toBe(checkoutElem[0]);
        });

        function renderAndFlushPromotedCohorts(
            promotedCohortResponse = [Cohort.fixtures.getInstance(), Cohort.fixtures.getInstance()],
        ) {
            Cohort.expect('index')
                .toBeCalledWith({
                    filters: {
                        promoted: true,
                        published: true,
                    },
                })
                .returns(promotedCohortResponse);
            render();
            Cohort.flush('index');
        }
    });

    function assertTuitionAndRegistrationSection() {
        beforeEach(() => {
            render();
            jest.spyOn(scope, 'showTuitionAndRegistration', 'get').mockReturnValue(true);
            scope.$digest();
        });

        it('should show tuition-status-unregistered-alternative directive if showTuitionStatusUnregisteredAlternative', () => {
            jest.spyOn(scope, 'showTuitionStatusUnregisteredAlternative', 'get').mockReturnValue(true);
            jest.spyOn(scope, 'showTuitionStatusUnregistered', 'get').mockReturnValue(false);
            jest.spyOn(scope, 'showTuitionStatusRegistered', 'get').mockReturnValue(false);
            scope.$digest();
            SpecHelper.expectElement(elem, 'tuition-status-unregistered-alternative');
            SpecHelper.expectNoElement(elem, 'tuition-status-unregistered');
            SpecHelper.expectNoElement(elem, 'tuition-status-registered');
        });

        it('should show tuition-status-unregistered directive if showTuitionStatusUnregistered', () => {
            jest.spyOn(scope, 'showTuitionStatusUnregisteredAlternative', 'get').mockReturnValue(false);
            jest.spyOn(scope, 'showTuitionStatusUnregistered', 'get').mockReturnValue(true);
            jest.spyOn(scope, 'showTuitionStatusRegistered', 'get').mockReturnValue(false);
            scope.$digest();
            SpecHelper.expectNoElement(elem, 'tuition-status-unregistered-alternative');
            SpecHelper.expectElement(elem, 'tuition-status-unregistered');
            SpecHelper.expectNoElement(elem, 'tuition-status-registered');
        });

        it('should show tuition-status-registered directive if showTuitionStatusRegistered', () => {
            jest.spyOn(scope, 'showTuitionStatusUnregisteredAlternative', 'get').mockReturnValue(false);
            jest.spyOn(scope, 'showTuitionStatusUnregistered', 'get').mockReturnValue(false);
            jest.spyOn(scope, 'showTuitionStatusRegistered', 'get').mockReturnValue(true);
            scope.$digest();
            SpecHelper.expectNoElement(elem, 'tuition-status-unregistered-alternative');
            SpecHelper.expectNoElement(elem, 'tuition-status-unregistered');
            SpecHelper.expectElement(elem, 'tuition-status-registered');
        });
    }
});
