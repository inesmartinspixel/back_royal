import 'AngularSpecHelper';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';
import 'Settings/angularModule';
import tuitionAndRegistrationLocales from 'Settings/locales/settings/tuition_and_registration-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(tuitionAndRegistrationLocales);

describe('Settings::TuitionAndRegistration::TuitionStatusUnregisteredAlternative', () => {
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let $injector;
    let currentUser;
    let $window;
    let Cohort;
    let CohortApplication;
    let launchProgramGuide;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Settings', 'SpecHelper');

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            SpecHelper = $injector.get('SpecHelper');
            $window = $injector.get('$window');
            Cohort = $injector.get('Cohort');
            CohortApplication = $injector.get('CohortApplication');

            $injector.get('CohortFixtures');
        });

        currentUser = SpecHelper.stubCurrentUser('learner');
        SpecHelper.stubConfig();
        SpecHelper.stubStripeCheckout();

        const cohort = Cohort.fixtures.getInstance({
            program_type: 'emba',
            id: 123,
            registration_deadline_days_offset: -4,
            start_date: new Date('2117/07/01').getTime() / 1000,
        });

        const promoted = CohortApplication.new({
            status: 'pre_accepted',
            program_type: 'emba',
            cohort_id: cohort.id,
            applied_at: new Date().getTime() / 1000,
            cohort,
            cohort_title: cohort.title,
            total_num_required_stripe_payments: undefined,
            stripe_plans: [
                {
                    id: 'monthly_plan',
                    frequency: 'monthly',
                    amount: 80000,
                },
            ],
            scholarship_level: {
                name: 'Level 1',
                standard: {
                    monthly_plan: {
                        id: 'discount_150',
                        amount_off: 15000,
                        percent_off: undefined,
                    },
                },
                early: {
                    monthly_plan: {
                        id: 'discount_150',
                        amount_off: 15000,
                        percent_off: undefined,
                    },
                },
            },
        });
        currentUser.relevant_cohort = promoted.cohort;
        currentUser.cohort_applications.push(promoted);

        launchProgramGuide = jest.fn();

        SpecHelper.stubDirective('embaWelcomePackage');
    });

    afterEach(() => {
        delete $window.CORDOVA;
    });

    it('should show the standard elements', () => {
        render();
        SpecHelper.expectElementText(
            elem,
            '[name="header-caption"]',
            "Congratulations, you're in! Now, join your classmates by securing your place in the Cohort Title 2 class.",
        );
        SpecHelper.expectElement(elem, 'emba-welcome-package');
    });

    describe('no scholarship', () => {
        beforeEach(() => {
            const cohortApplication = _.last(currentUser.cohort_applications);
            jest.spyOn(cohortApplication, 'getPlanPaymentPerIntervalForDisplay').mockReturnValue(800);
            jest.spyOn(cohortApplication, 'getPlanTuitionActualTotal').mockReturnValue(9600);
            jest.spyOn(currentUser.relevant_cohort, 'afterRegistrationOpenDate', 'get').mockReturnValue(true);
        });

        it('should display proper text and UI', () => {
            render();
            SpecHelper.expectNoElement(elem, '.registration-narrative h3');
            SpecHelper.expectElementText(
                elem,
                '.registration-narrative',
                'Tuition of $9600 USD is payable as a one-time charge or through a 12-month payment plan over the course of the program.',
            );
            SpecHelper.expectElementText(elem, '.registration-date', 'Early Registration discount ends June 18, 2117');
            SpecHelper.expectElementText(elem, '.register-btn', 'Register');
            SpecHelper.expectElementText(elem, '.graphic-title', 'Congratulations');
            SpecHelper.expectElementText(elem, '.graphic-subtitle', 'Register to enroll');

            // no graphic shown when no scholarship to celebrate
            SpecHelper.expectNoElement(elem, '.registration-title');
            SpecHelper.expectNoElement(elem, '.registration-subtitle');

            SpecHelper.expectNoElement(elem, '.plan-selection-container');

            SpecHelper.expectElementText(
                elem,
                '.sub-text.small:eq(0)',
                'Prefer to register and pay by wire transfer? Email us at billing@quantic.edu for instructions.',
            );
            SpecHelper.expectElementText(
                elem,
                '.sub-text.small:eq(1)',
                'You may defer or cancel your enrollment at any time. Questions? Email us at emba@quantic.edu',
            );

            SpecHelper.expectCheckboxUnchecked(elem, '[name="shareable_with_classmates"]');
        });

        it('should start registration after clicking button', () => {
            render();
            jest.spyOn(scope, 'register').mockImplementation(() => {});

            SpecHelper.click(elem, '.register-btn');
            expect(scope.register).toHaveBeenCalled();
        });
    });

    describe('partial scholarship', () => {
        beforeEach(() => {
            const cohortApplication = _.last(currentUser.cohort_applications);
            jest.spyOn(currentUser.relevant_cohort, 'afterRegistrationOpenDate', 'get').mockReturnValue(true);
            jest.spyOn(cohortApplication, 'getPlanPaymentPerIntervalForDisplay').mockImplementation(
                plan =>
                    ({
                        monthly_plan: 450,
                        bi_annual_plan: 4200,
                    }[plan.id]),
            );
            jest.spyOn(cohortApplication, 'additionalSavingsForPlan').mockImplementation(
                plan =>
                    ({
                        monthly_plan: 0,
                        bi_annual_plan: 42,
                    }[plan.id]),
            );

            jest.spyOn(cohortApplication, 'getPlanTuitionActualTotal').mockImplementation(
                plan =>
                    ({
                        monthly_plan: 450 * 12,
                        bi_annual_plan: 4200 * 2,
                    }[plan.id]),
            );
        });

        it('should display proper text and UI', () => {
            render({
                defaultPlanOriginalTotal: 9600,
                defaultPlanScholarshipTotal: 4200,
            });
            SpecHelper.expectElementText(elem, '.registration-narrative h3', 'Scholarship Award');
            SpecHelper.expectElementText(
                elem,
                '.registration-narrative .sub-text',
                'You have been awarded a scholarship toward the full tuition of $9600 USD. Your tuition due is $5400 USD. Congratulations!',
            );
            SpecHelper.expectElementText(elem, '.registration-date', 'Early Registration discount ends June 18, 2117');
            SpecHelper.expectElementText(elem, '.register-btn', 'Register');
            SpecHelper.expectElementText(elem, '.graphic-title', '$4200 USD');
            SpecHelper.expectElementText(elem, '.graphic-subtitle', 'Scholarship award');
            SpecHelper.expectNoElement(elem, '.plan-selection-container');

            SpecHelper.expectElementText(
                elem,
                '.sub-text.small:eq(0)',
                'Prefer to register and pay by wire transfer? Email us at billing@quantic.edu for instructions.',
            );
            SpecHelper.expectElementText(
                elem,
                '.sub-text.small:eq(1)',
                'You may defer or cancel your enrollment at any time. Questions? Email us at emba@quantic.edu',
            );

            SpecHelper.expectCheckboxUnchecked(elem, '[name="shareable_with_classmates"]');
        });

        it('should start registration after clicking button', () => {
            render({
                defaultPlanOriginalTotal: 9600,
                defaultPlanScholarshipTotal: 4200,
            });
            jest.spyOn(scope, 'register').mockImplementation(() => {});

            SpecHelper.click(elem, '.register-btn');
            expect(scope.register).toHaveBeenCalled();
        });
    });

    describe('full scholarship', () => {
        beforeEach(() => {
            jest.spyOn(currentUser.relevant_cohort, 'afterRegistrationOpenDate', 'get').mockReturnValue(true);
        });

        it('should display proper text and UI', () => {
            render({
                defaultPlanOriginalTotal: 9600,
                defaultPlanScholarshipTotal: 9600,
            });
            SpecHelper.expectNoElement(elem, '.registration-narrative h3');
            SpecHelper.expectElementText(
                elem,
                '.registration-narrative',
                'You have been awarded a full scholarship toward the tuition of $9600 USD. Congratulations on this special achievement.',
            );
            SpecHelper.expectElementText(elem, '.registration-date', 'Early Registration discount ends June 18, 2117');
            SpecHelper.expectElementText(elem, '.register-btn', 'Register');
            SpecHelper.expectElementText(elem, '.graphic-title', 'Free Tuition');
            SpecHelper.expectElementText(elem, '.graphic-subtitle', 'Scholarship Award');
            SpecHelper.expectNoElement(elem, '.plan-selection-container');

            SpecHelper.expectElementText(
                elem,
                '.sub-text.small',
                'You may defer or cancel your enrollment at any time. Questions? Email us at emba@quantic.edu',
            );

            SpecHelper.expectCheckboxChecked(elem, '[name="shareable_with_classmates"]');
        });

        it('should register immediately after clicking button', () => {
            render({
                defaultPlanOriginalTotal: 9600,
                defaultPlanScholarshipTotal: 9600,
            });
            jest.spyOn(scope, 'register').mockImplementation(() => {});

            SpecHelper.click(elem, '.register-btn');
            expect(scope.register).toHaveBeenCalled();

            // TODO: mock and flush the API call, and verify that currentUser.registeredForClass goes from false to true
        });
    });

    describe('partial payments', () => {
        beforeEach(() => {
            jest.spyOn(currentUser.relevant_cohort, 'afterRegistrationOpenDate', 'get').mockReturnValue(true);
        });

        it('should display proper text and UI for monthly plans', () => {
            const cohortApplication = _.last(currentUser.cohort_applications);
            jest.spyOn(cohortApplication, 'existingPlan', 'get').mockReturnValue({
                id: 'monthly_plan',
                frequency: 'monthly',
            });

            jest.spyOn(cohortApplication, 'getPlanPaymentPerIntervalForDisplay').mockReturnValue(450);
            jest.spyOn(cohortApplication, 'getPlanTuitionActualTotal').mockReturnValue(5400);

            render({
                defaultPlanOriginalTotal: 9600,
                defaultPlanScholarshipTotal: 4200,
                tuitionNumSuccessfulPayments: 2,
                tuitionNumPaymentsRemaining: 10,
            });
            SpecHelper.expectNoElement(elem, '.registration-narrative h3');
            SpecHelper.expectElementText(
                elem,
                '.registration-narrative',
                'You have made 2 payments of $450 USD towards your total tuition of $5400 USD. You will make an additional 10 payments over the duration of the program.',
            );

            SpecHelper.expectElementText(
                elem,
                '.sub-text.small',
                'You may defer or cancel your enrollment at any time. Questions? Email us at emba@quantic.edu',
            );

            SpecHelper.expectElementText(elem, '.registration-date', 'Early Registration discount ends June 18, 2117');
            SpecHelper.expectElementText(elem, '.register-btn', 'Register');
            SpecHelper.expectElementText(elem, '.graphic-title', '$4200 USD');
            SpecHelper.expectElementText(elem, '.graphic-subtitle', 'Scholarship award');
        });

        it('should start registration after clicking button', () => {
            render({
                defaultPlanOriginalTotal: 9600,
                defaultPlanScholarshipTotal: 4200,
                tuitionNumSuccessfulPayments: 1,
                tuitionNumPaymentsRemaining: 11,
            });

            jest.spyOn(scope, 'register').mockImplementation(() => {});
            SpecHelper.click(elem, '.register-btn');
            expect(scope.register).toHaveBeenCalled();
        });
    });

    describe('completed payments', () => {
        beforeEach(() => {
            jest.spyOn(currentUser.relevant_cohort, 'afterRegistrationOpenDate', 'get').mockReturnValue(true);
        });

        it('should display proper text and UI for monthly plans', () => {
            const cohortApplication = _.last(currentUser.cohort_applications);
            jest.spyOn(cohortApplication, 'existingPlan', 'get').mockReturnValue({
                id: 'monthly_plan',
                frequency: 'monthly',
            });

            jest.spyOn(cohortApplication, 'getPlanPaymentPerIntervalForDisplay').mockReturnValue(450);
            jest.spyOn(cohortApplication, 'getPlanTuitionActualTotal').mockReturnValue(5400);

            render({
                defaultPlanOriginalTotal: 9600,
                defaultPlanScholarshipTotal: 4200,
                tuitionNumSuccessfulPayments: 12,
                tuitionNumPaymentsRemaining: 0,
            });
            SpecHelper.expectNoElement(elem, '.registration-narrative h3');
            SpecHelper.expectElementText(
                elem,
                '.registration-narrative',
                'You have paid your tuition in full. Thank you!',
            );

            SpecHelper.expectElementText(
                elem,
                '.sub-text.small',
                'You may defer or cancel your enrollment at any time. Questions? Email us at emba@quantic.edu',
            );

            SpecHelper.expectElementText(elem, '.registration-date', 'Early Registration discount ends June 18, 2117');
            SpecHelper.expectElementText(elem, '.register-btn', 'Register');
            SpecHelper.expectNoElement(elem, '.graphic-title');
            SpecHelper.expectNoElement(elem, '.graphic-subtitle');
        });

        it('should start registration after clicking button', () => {
            render({
                defaultPlanOriginalTotal: 9600,
                defaultPlanScholarshipTotal: 9600,
                tuitionNumSuccessfulPayments: 12,
                tuitionNumPaymentsRemaining: 0,
            });

            jest.spyOn(scope, 'register').mockImplementation(() => {});
            SpecHelper.click(elem, '.register-btn');
            expect(scope.register).toHaveBeenCalled();
        });
    });

    describe('in CORDOVA app', () => {
        beforeEach(() => {
            jest.spyOn(currentUser.relevant_cohort, 'afterRegistrationOpenDate', 'get').mockReturnValue(true);
            $window.CORDOVA = true;
            render();
        });

        it('should notify user a new broswer will be opened', () => {
            SpecHelper.expectElementText(
                elem,
                '.new-browser',
                'To register, you will need to use your system web browser.',
            );
        });

        it('should launch system browser if modify payment button clicked', () => {
            jest.spyOn(scope, 'register');
            jest.spyOn(scope, 'loadUrl').mockImplementation(() => {});

            SpecHelper.expectElementText(elem, '.register-btn', 'Launch Web Browser');

            SpecHelper.click(elem, '.register-btn');
            expect(scope.register).toHaveBeenCalled();
            expect(scope.loadUrl).toHaveBeenCalled();
        });
    });

    describe('before registration', () => {
        it('should display proper text and disable registration button', () => {
            render();
            SpecHelper.expectElementText(elem, '.registration-date', 'Registration opens on May 16, 2117.');
            SpecHelper.expectElementDisabled(elem, '.register-btn');
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render(opts) {
        opts = _.extend(
            {
                defaultPlanOriginalTotal: 9600,
                defaultPlanScholarshipTotal: 0,
                tuitionNumPaymentIntervals: 12,
                tuitionNumSuccessfulPayments: 0,
                tuitionNumPaymentsRemaining: 0,
            },
            opts || {},
        );

        const cohortApplication = _.last(currentUser.cohort_applications);

        jest.spyOn(cohortApplication, 'defaultPlan', 'get').mockReturnValue({
            id: 'monthly_plan',
            frequency: 'monthly',
        });

        if (opts.defaultPlanOriginalTotal === opts.defaultPlanScholarshipTotal) {
            jest.spyOn(cohortApplication, 'hasFullScholarship', 'get').mockReturnValue(true);
            jest.spyOn(cohortApplication, 'hasPartialScholarship', 'get').mockReturnValue(false);
            jest.spyOn(cohortApplication, 'hasNoScholarship', 'get').mockReturnValue(false);
        } else if (opts.defaultPlanScholarshipTotal > 0) {
            jest.spyOn(cohortApplication, 'hasFullScholarship', 'get').mockReturnValue(false);
            jest.spyOn(cohortApplication, 'hasPartialScholarship', 'get').mockReturnValue(true);
            jest.spyOn(cohortApplication, 'hasNoScholarship', 'get').mockReturnValue(false);
        } else {
            jest.spyOn(cohortApplication, 'hasFullScholarship', 'get').mockReturnValue(false);
            jest.spyOn(cohortApplication, 'hasPartialScholarship', 'get').mockReturnValue(false);
            jest.spyOn(cohortApplication, 'hasNoScholarship', 'get').mockReturnValue(true);
        }

        Object.defineProperty(cohortApplication, 'defaultPlanOriginalTotal', {
            value: opts.defaultPlanOriginalTotal,
            configurable: true,
        });

        Object.defineProperty(cohortApplication, 'defaultPlanScholarshipTotal', {
            value: opts.defaultPlanScholarshipTotal,
            configurable: true,
        });

        Object.defineProperty(cohortApplication, 'defaultPlanNumPaymentIntervals', {
            value: opts.defaultPlanNumPaymentIntervals,
            configurable: true,
        });

        Object.defineProperty(cohortApplication, 'tuitionNumSuccessfulPayments', {
            value: opts.tuitionNumSuccessfulPayments,
            configurable: true,
        });

        Object.defineProperty(cohortApplication, 'tuitionNumPaymentsRemaining', {
            value: opts.tuitionNumPaymentsRemaining,
            configurable: true,
        });

        renderer = SpecHelper.renderer();
        renderer.scope.launchProgramGuide = launchProgramGuide;
        renderer.scope.cohortApplication = cohortApplication;

        renderer.render(
            '<tuition-status-unregistered-alternative cohort-application="cohortApplication" launch-program-guide="launchProgramGuide()"></tuition-status-unregistered-alternative>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
    }
});
