import 'AngularSpecHelper';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';
import 'Settings/angularModule';
import tuitionAndRegistrationLocales from 'Settings/locales/settings/tuition_and_registration-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(tuitionAndRegistrationLocales);

describe('Settings::TuitionAndRegistration::TuitionStatusUnregistered', () => {
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let $injector;
    let currentUser;
    let $window;
    let Cohort;
    let CohortApplication;
    let launchProgramGuide;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Settings', 'SpecHelper');

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            SpecHelper = $injector.get('SpecHelper');
            $window = $injector.get('$window');
            Cohort = $injector.get('Cohort');
            CohortApplication = $injector.get('CohortApplication');

            $injector.get('CohortFixtures');
        });

        currentUser = SpecHelper.stubCurrentUser('learner');
        SpecHelper.stubConfig();
        SpecHelper.stubStripeCheckout();

        const cohort = Cohort.fixtures.getInstance({
            program_type: 'emba',
            id: 123,
            registration_deadline_days_offset: -4,
            start_date: new Date('2117/07/01').getTime() / 1000,
        });

        const promoted = CohortApplication.new({
            status: 'pre_accepted',
            program_type: 'emba',
            cohort_id: cohort.id,
            applied_at: new Date().getTime() / 1000,
            cohort,
            cohort_title: cohort.title,
            total_num_required_stripe_payments: undefined,
            stripe_plans: [
                {
                    id: 'monthly_plan',
                    frequency: 'monthly',
                    amount: 80000,
                },
                {
                    id: 'bi_annual_plan',
                    frequency: 'bi_annual',
                    amount: 480000,
                },
                {
                    id: 'once_plan',
                    frequency: 'once',
                    amount: 960000,
                },
            ],
            scholarship_level: {
                name: 'Level 1',
                coupons: {
                    monthly_plan: {
                        id: 'discount_150',
                        amount_off: 15000,
                        percent_off: undefined,
                    },
                },
                standard: {
                    monthly_plan: {
                        id: 'discount_150',
                        amount_off: 15000,
                        percent_off: undefined,
                    },
                },
                early: {
                    monthly_plan: {
                        id: 'discount_150',
                        amount_off: 15000,
                        percent_off: undefined,
                    },
                },
            },
        });
        currentUser.relevant_cohort = promoted.cohort;
        currentUser.cohort_applications.push(promoted);

        launchProgramGuide = jest.fn();

        SpecHelper.stubDirective('embaWelcomePackage');
    });

    afterEach(() => {
        delete $window.CORDOVA;
    });

    it('should show the standard elements', () => {
        render();
        SpecHelper.expectElementText(
            elem,
            '[name="header-caption"]',
            'Tuition fees for the 12-month EMBA program are $9600 USD.',
        );
        SpecHelper.expectElement(elem, 'emba-welcome-package');
    });

    describe('no scholarship', () => {
        beforeEach(() => {
            const cohortApplication = _.last(currentUser.cohort_applications);
            jest.spyOn(cohortApplication, 'getPlanPaymentPerIntervalForDisplay').mockReturnValue(800);
            jest.spyOn(cohortApplication, 'getPlanTuitionActualTotal').mockReturnValue(9600);
            jest.spyOn(cohortApplication, 'additionalSavingsForPlan').mockReturnValue(100);
            jest.spyOn(currentUser.relevant_cohort, 'afterRegistrationOpenDate', 'get').mockReturnValue(true);
        });

        it('should display proper text and UI', () => {
            render();
            SpecHelper.expectElementText(
                elem,
                '[name="subheader-no-scholarship"]',
                'Tuition fees for the 12-month EMBA program are $9600 USD.',
            );
            SpecHelper.expectElementText(
                elem,
                '[name="subheader-confirm-place"]',
                'To confirm your place in the class, please register by June 26, 2117.',
            );

            SpecHelper.expectElements(elem, '.plan-selection-option-container', 3);
            SpecHelper.expectElementText(elem, '.plan-description-container:eq(0)', '$800 USD /mo Charged 12 times');
            SpecHelper.expectElementText(elem, '.plan-description-container:eq(1)', '$800 USD /6-mo Charged twice');
            SpecHelper.expectElementText(elem, '.plan-description-container:eq(2)', '$800 USD Charged once');
            SpecHelper.expectElementText(elem, '.save-amount:eq(0)', 'SAVE $100 USD');
            SpecHelper.expectElementText(elem, '.save-amount:eq(1)', 'SAVE $100 USD');

            SpecHelper.expectElementText(
                elem,
                '.sub-text:eq(0)',
                'Prefer to register and pay by wire transfer? Email us at billing@quantic.edu for instructions.',
            );
            SpecHelper.expectElementText(
                elem,
                '.sub-text:eq(1)',
                'You may defer or cancel your enrollment at any time. Questions? Email us at emba@quantic.edu',
            );

            SpecHelper.expectCheckboxUnchecked(elem, '[name="shareable_with_classmates"]');
            SpecHelper.expectElementText(elem, '.register-btn', 'Register');
        });

        it('should start registration after clicking button', () => {
            render();
            jest.spyOn(scope, 'register').mockImplementation(() => {});

            SpecHelper.click(elem, '.register-btn');
            expect(scope.register).toHaveBeenCalled();
        });

        it('should disable the radio inputs while stripe registration is occuring', () => {
            render();
            SpecHelper.expectElements(elem, '.plan-selection-option-container', 3);
            scope.checkoutHelper.stripeProcessing = true;
            scope.$digest();
            SpecHelper.expectElementDisabled(elem, '.plan-selection-option-container input:eq(0)');
            SpecHelper.expectElementDisabled(elem, '.plan-selection-option-container input:eq(1)');
            SpecHelper.expectElementDisabled(elem, '.plan-selection-option-container input:eq(2)');
            scope.checkoutHelper.stripeProcessing = false;
            scope.$digest();
            SpecHelper.expectElementEnabled(elem, '.plan-selection-option-container input:eq(0)');
            SpecHelper.expectElementEnabled(elem, '.plan-selection-option-container input:eq(1)');
            SpecHelper.expectElementEnabled(elem, '.plan-selection-option-container input:eq(2)');
        });
    });

    describe('partial scholarship', () => {
        beforeEach(() => {
            const cohortApplication = _.last(currentUser.cohort_applications);
            jest.spyOn(currentUser.relevant_cohort, 'afterRegistrationOpenDate', 'get').mockReturnValue(true);
            jest.spyOn(cohortApplication, 'getPlanPaymentPerIntervalForDisplay').mockImplementation(
                plan =>
                    ({
                        monthly_plan: 450,
                        bi_annual_plan: 4200,
                        once_plan: 8000,
                    }[plan.id]),
            );
            jest.spyOn(cohortApplication, 'additionalSavingsForPlan').mockImplementation(
                plan =>
                    ({
                        monthly_plan: 0,
                        bi_annual_plan: 42,
                        once_plan: 100,
                    }[plan.id]),
            );

            jest.spyOn(cohortApplication, 'getPlanTuitionActualTotal').mockImplementation(
                plan =>
                    ({
                        monthly_plan: 450 * 12,
                        bi_annual_plan: 4200 * 2,
                        once_plan: 8000,
                    }[plan.id]),
            );
        });

        it('should display proper text and UI', () => {
            render({
                defaultPlanOriginalTotal: 9600,
                defaultPlanScholarshipTotal: 4200,
            });
            SpecHelper.expectElementText(
                elem,
                '[name="scholarship-narrative"]:eq(0)',
                'You have been awarded a scholarship award of $4200 USD off the total tuition of $9600 USD. Congratulations on this achievement!',
            );
            SpecHelper.expectElementText(
                elem,
                '[name="subheader-confirm-place"]',
                'To confirm your place in the class, please register by June 26, 2117.',
            );

            SpecHelper.expectElements(elem, '.plan-selection-option-container', 3);
            SpecHelper.expectElementText(elem, '.plan-description-container:eq(0)', '$450 USD /mo Charged 12 times');
            SpecHelper.expectElementText(elem, '.plan-description-container:eq(1)', '$4200 USD /6-mo Charged twice');
            SpecHelper.expectElementText(elem, '.plan-description-container:eq(2)', '$8000 USD Charged once');
            SpecHelper.expectElementText(elem, '.save-amount:eq(0)', 'SAVE $42 USD');
            SpecHelper.expectElementText(elem, '.save-amount:eq(1)', 'SAVE $42 USD');

            SpecHelper.expectElementText(
                elem,
                '.sub-text:eq(0)',
                'Prefer to register and pay by wire transfer? Email us at billing@quantic.edu for instructions.',
            );
            SpecHelper.expectElementText(
                elem,
                '.sub-text:eq(1)',
                'You may defer or cancel your enrollment at any time. Questions? Email us at emba@quantic.edu',
            );

            SpecHelper.expectCheckboxUnchecked(elem, '[name="shareable_with_classmates"]');
            SpecHelper.expectElementText(elem, '.register-btn', 'Register');
        });

        it('should start registration after clicking button', () => {
            render({
                defaultPlanOriginalTotal: 9600,
                defaultPlanScholarshipTotal: 4200,
            });
            jest.spyOn(scope, 'register').mockImplementation(() => {});

            SpecHelper.click(elem, '.register-btn');
            expect(scope.register).toHaveBeenCalled();
        });
    });

    describe('full scholarship', () => {
        beforeEach(() => {
            jest.spyOn(currentUser.relevant_cohort, 'afterRegistrationOpenDate', 'get').mockReturnValue(true);
        });

        it('should display proper text and UI', () => {
            render({
                defaultPlanOriginalTotal: 9600,
                defaultPlanScholarshipTotal: 9600,
            });

            SpecHelper.expectElementText(
                elem,
                '[name="scholarship-narrative"]:eq(0)',
                'You have been awarded a Full Scholarship to the program. No tuition is due. Congratulations on this achievement!',
            );
            SpecHelper.expectElementText(
                elem,
                '[name="subheader-confirm-place"]',
                'To confirm your place in the class, please register by June 26, 2117.',
            );

            SpecHelper.expectElementText(
                elem,
                '.sub-text',
                'You may defer or cancel your enrollment at any time. Questions? Email us at emba@quantic.edu',
            );

            SpecHelper.expectCheckboxChecked(elem, '[name="shareable_with_classmates"]');
            SpecHelper.expectElementText(elem, '.register-btn', 'Register');
        });

        it('should register immediately after clicking button', () => {
            render({
                defaultPlanOriginalTotal: 9600,
                defaultPlanScholarshipTotal: 9600,
            });
            jest.spyOn(scope, 'register').mockImplementation(() => {});

            SpecHelper.click(elem, '.register-btn');
            expect(scope.register).toHaveBeenCalled();

            // TODO: mock and flush the API call, and verify that currentUser.registeredForClass goes from false to true
        });
    });

    describe('partial payments', () => {
        beforeEach(() => {
            jest.spyOn(currentUser.relevant_cohort, 'afterRegistrationOpenDate', 'get').mockReturnValue(true);
        });

        it('should display proper text and UI for monthly plans', () => {
            const cohortApplication = _.last(currentUser.cohort_applications);
            cohortApplication.total_num_required_stripe_payments = 5;
            jest.spyOn(cohortApplication, 'existingPlan', 'get').mockReturnValue({
                id: 'monthly_plan',
                frequency: 'monthly',
            });

            jest.spyOn(cohortApplication, 'getPlanPaymentPerIntervalForDisplay').mockReturnValue(450);
            jest.spyOn(cohortApplication, 'getPlanTuitionActualTotal').mockReturnValue(5400);

            render({
                defaultPlanOriginalTotal: 9600,
                defaultPlanScholarshipTotal: 4200,
                tuitionNumSuccessfulPayments: 2,
                tuitionNumPaymentsRemaining: 10,
            });
            SpecHelper.expectNoElement(elem, '.scholarship-narrative');
            SpecHelper.expectElementText(
                elem,
                '.has-made-payments',
                'You are currently enrolled in the Monthly Tuition Plan billed at $450 USD per month. You have made 2 out of 5 total payments. To confirm your place in the class, please register now.',
            );

            SpecHelper.expectElementText(
                elem,
                '.sub-text',
                'You may defer or cancel your enrollment at any time. Questions? Email us at emba@quantic.edu',
            );

            SpecHelper.expectCheckboxUnchecked(elem, '[name="shareable_with_classmates"]');
            SpecHelper.expectElementText(elem, '.register-btn', 'Register');
        });

        it('should display proper text and UI for bi_annual plans', () => {
            const cohortApplication = _.last(currentUser.cohort_applications);
            cohortApplication.total_num_required_stripe_payments = 2;
            const biAnnualPlan = {
                id: 'bi_annual_plan',
                frequency: 'bi_annual',
                amount: 480000,
            };

            jest.spyOn(cohortApplication, 'existingPlan', 'get').mockReturnValue(biAnnualPlan);

            cohortApplication.stripe_plans.push(biAnnualPlan);
            cohortApplication.scholarship_level = {
                coupons: {
                    bi_annual_plan: {
                        id: 'discount_1400',
                        amount_off: 140000,
                        percent_off: undefined,
                    },
                },
                standard: {
                    bi_annual_plan: {
                        id: 'discount_1400',
                        amount_off: 140000,
                        percent_off: undefined,
                    },
                },
                early: {
                    bi_annual_plan: {
                        id: 'discount_1400',
                        amount_off: 140000,
                        percent_off: undefined,
                    },
                },
            };

            render({
                defaultPlanOriginalTotal: 9600,
                defaultPlanScholarshipTotal: 4200,
                tuitionNumSuccessfulPayments: 1,
                tuitionNumPaymentsRemaining: 1,
            });

            SpecHelper.expectNoElement(elem, '.scholarship-narrative');
            SpecHelper.expectElementText(
                elem,
                '.has-made-payments',
                'You are currently enrolled in the Bi-annual Tuition Plan billed twice at $3400 USD. You have made 1 out of 2 total payments. To confirm your place in the class, please register now.',
            );

            SpecHelper.expectElementText(
                elem,
                '.sub-text',
                'You may defer or cancel your enrollment at any time. Questions? Email us at emba@quantic.edu',
            );

            SpecHelper.expectCheckboxUnchecked(elem, '[name="shareable_with_classmates"]');
            SpecHelper.expectElementText(elem, '.register-btn', 'Register');
        });

        it('should start registration after clicking button', () => {
            render({
                defaultPlanOriginalTotal: 9600,
                defaultPlanScholarshipTotal: 4200,
                tuitionNumSuccessfulPayments: 1,
                tuitionNumPaymentsRemaining: 11,
            });

            jest.spyOn(scope, 'register').mockImplementation(() => {});
            SpecHelper.click(elem, '.register-btn');
            expect(scope.register).toHaveBeenCalled();
        });
    });

    describe('completed payments', () => {
        beforeEach(() => {
            jest.spyOn(currentUser.relevant_cohort, 'afterRegistrationOpenDate', 'get').mockReturnValue(true);
        });

        it('should display proper text and UI', () => {
            const cohortApplication = _.last(currentUser.cohort_applications);
            jest.spyOn(cohortApplication, 'existingPlan', 'get').mockReturnValue({
                id: 'monthly_plan',
                frequency: 'monthly',
            });

            jest.spyOn(cohortApplication, 'getPlanPaymentPerIntervalForDisplay').mockReturnValue(450);
            jest.spyOn(cohortApplication, 'getPlanTuitionActualTotal').mockReturnValue(5400);

            render({
                defaultPlanOriginalTotal: 9600,
                defaultPlanScholarshipTotal: 4200,
                tuitionNumSuccessfulPayments: 12,
                tuitionNumPaymentsRemaining: 0,
            });
            SpecHelper.expectNoElement(elem, '.scholarship-narrative');
            SpecHelper.expectElementText(
                elem,
                '.has-made-payments',
                'You have paid your tuition in full. Thank you! To confirm your place in the class, please register now.',
            );
            SpecHelper.expectCheckboxUnchecked(elem, '[name="shareable_with_classmates"]');
            SpecHelper.expectElementText(elem, '.register-btn', 'Register');
        });

        it('should start registration after clicking button', () => {
            render({
                defaultPlanOriginalTotal: 9600,
                defaultPlanScholarshipTotal: 9600,
                tuitionNumSuccessfulPayments: 12,
                tuitionNumPaymentsRemaining: 0,
            });

            jest.spyOn(scope, 'register').mockImplementation(() => {});
            SpecHelper.click(elem, '.register-btn');
            expect(scope.register).toHaveBeenCalled();
        });
    });

    describe('past registration deadline', () => {
        it('should display text without deadline date when past registration open date', () => {
            jest.spyOn(currentUser.relevant_cohort, 'registrationDeadlineHasPassed', 'get').mockReturnValue(true);
            jest.spyOn(currentUser.relevant_cohort, 'afterRegistrationOpenDate', 'get').mockReturnValue(true);
            render();
            SpecHelper.expectElementText(
                elem,
                '[name="subheader-confirm-place"]',
                'To confirm your place in the class, please register now.',
            );
            SpecHelper.expectElementEnabled(elem, '.register-btn');
        });

        it('should not show any confirm message before the registration deadline', () => {
            Object.defineProperty(currentUser.lastCohortApplication, 'hasMadePayments', {
                value: false,
            });
            jest.spyOn(currentUser.relevant_cohort, 'registrationDeadlineHasPassed', 'get').mockReturnValue(true);
            jest.spyOn(currentUser.relevant_cohort, 'afterRegistrationOpenDate', 'get').mockReturnValue(false);
            render();
            SpecHelper.expectNoElement(elem, '[name="subheader-confirm-place"]');
            SpecHelper.expectNoElement(elem, '.confirm-register-now');
            SpecHelper.expectElementDisabled(elem, '.register-btn');
            SpecHelper.expectHasClass(elem, '.choose-your-plan-box', 'disabled');
        });

        it('should not show any confirm message before the registration deadline even if the user has made payments', () => {
            Object.defineProperty(currentUser.lastCohortApplication, 'hasMadePayments', {
                value: true,
            });
            jest.spyOn(currentUser.relevant_cohort, 'registrationDeadlineHasPassed', 'get').mockReturnValue(true);
            jest.spyOn(currentUser.relevant_cohort, 'afterRegistrationOpenDate', 'get').mockReturnValue(false);
            render();
            SpecHelper.expectNoElement(elem, '[name="subheader-confirm-place"]');
            SpecHelper.expectNoElement(elem, '.confirm-register-now');
            SpecHelper.expectElementDisabled(elem, '.register-btn');
            SpecHelper.expectHasClass(elem, '.choose-your-plan-box', 'disabled');
        });
    });

    describe('in CORDOVA app', () => {
        beforeEach(() => {
            jest.spyOn(currentUser.relevant_cohort, 'afterRegistrationOpenDate', 'get').mockReturnValue(true);
            $window.CORDOVA = true;
            render();
        });

        it('should notify user a new broswer will be opened', () => {
            SpecHelper.expectElementText(
                elem,
                '.new-browser',
                'To register, you will need to use your system web browser.',
            );
        });

        it('should launch system browser if modify payment button clicked', () => {
            jest.spyOn(scope, 'register');
            jest.spyOn(scope, 'loadUrl').mockImplementation(() => {});

            SpecHelper.expectElementText(elem, '.register-btn', 'Launch Web Browser');

            SpecHelper.click(elem, '.register-btn');
            expect(scope.register).toHaveBeenCalled();
            expect(scope.loadUrl).toHaveBeenCalled();
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render(opts) {
        opts = _.extend(
            {
                defaultPlanOriginalTotal: 9600,
                defaultPlanScholarshipTotal: 0,
                tuitionNumPaymentIntervals: 12,
                tuitionNumSuccessfulPayments: 0,
                tuitionNumPaymentsRemaining: 0,
            },
            opts || {},
        );

        const cohortApplication = _.last(currentUser.cohort_applications);

        jest.spyOn(cohortApplication, 'defaultPlan', 'get').mockReturnValue({
            id: 'monthly_plan',
            frequency: 'monthly',
        });

        if (opts.defaultPlanOriginalTotal === opts.defaultPlanScholarshipTotal) {
            jest.spyOn(cohortApplication, 'hasFullScholarship', 'get').mockReturnValue(true);
            jest.spyOn(cohortApplication, 'hasPartialScholarship', 'get').mockReturnValue(false);
            jest.spyOn(cohortApplication, 'hasNoScholarship', 'get').mockReturnValue(false);
        } else if (opts.defaultPlanScholarshipTotal > 0) {
            jest.spyOn(cohortApplication, 'hasFullScholarship', 'get').mockReturnValue(false);
            jest.spyOn(cohortApplication, 'hasPartialScholarship', 'get').mockReturnValue(true);
            jest.spyOn(cohortApplication, 'hasNoScholarship', 'get').mockReturnValue(false);
        } else {
            jest.spyOn(cohortApplication, 'hasFullScholarship', 'get').mockReturnValue(false);
            jest.spyOn(cohortApplication, 'hasPartialScholarship', 'get').mockReturnValue(false);
            jest.spyOn(cohortApplication, 'hasNoScholarship', 'get').mockReturnValue(true);
        }

        Object.defineProperty(cohortApplication, 'defaultPlanOriginalTotal', {
            value: opts.defaultPlanOriginalTotal,
            configurable: true,
        });

        Object.defineProperty(cohortApplication, 'defaultPlanScholarshipTotal', {
            value: opts.defaultPlanScholarshipTotal,
            configurable: true,
        });

        Object.defineProperty(cohortApplication, 'defaultPlanNumPaymentIntervals', {
            value: opts.defaultPlanNumPaymentIntervals,
            configurable: true,
        });

        Object.defineProperty(cohortApplication, 'tuitionNumSuccessfulPayments', {
            value: opts.tuitionNumSuccessfulPayments,
            configurable: true,
        });

        Object.defineProperty(cohortApplication, 'tuitionNumPaymentsRemaining', {
            value: opts.tuitionNumPaymentsRemaining,
            configurable: true,
        });

        renderer = SpecHelper.renderer();
        renderer.scope.launchProgramGuide = launchProgramGuide;
        renderer.scope.cohortApplication = cohortApplication;

        renderer.render(
            '<tuition-status-unregistered cohort-application="cohortApplication" launch-program-guide="launchProgramGuide()"></tuition-status-unregistered>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
    }
});
