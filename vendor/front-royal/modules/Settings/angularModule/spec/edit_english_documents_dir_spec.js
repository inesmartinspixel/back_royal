import 'AngularSpecHelper';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';
import 'Settings/angularModule';
import editDocumentsLocales from 'Settings/locales/settings/edit_documents-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(editDocumentsLocales);

describe('editEnglishDocuments', () => {
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let User;
    let user;
    let $q;
    let $http;
    let Cohort;
    let CareerProfile;
    let S3EnglishLanguageProficiencyDocument;
    let EducationExperience;
    let DialogModal;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Settings', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            $q = $injector.get('$q');
            $http = $injector.get('$http');
            Cohort = $injector.get('Cohort');
            CareerProfile = $injector.get('CareerProfile');
            S3EnglishLanguageProficiencyDocument = $injector.get('S3EnglishLanguageProficiencyDocument');
            EducationExperience = $injector.get('EducationExperience');
            User = $injector.get('User');
            DialogModal = $injector.get('DialogModal');

            $injector.get('CohortFixtures');
            $injector.get('CareerProfileFixtures');
        });

        user = SpecHelper.stubCurrentUser();
        user.career_profile = CareerProfile.fixtures.getInstance();
        user.relevant_cohort = Cohort.fixtures.getInstance();
        user.english_language_proficiency_documents_type = 'duolingo';
        user.s3_english_language_proficiency_documents = [];
        user.career_profile.education_experiences = [EducationExperience.new(), EducationExperience.new()];
        user.s3_transcript_assets = [];

        jest.spyOn(DialogModal, 'confirm').mockImplementation(() => {});
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.user = user;
        renderer.scope.userProxy = User.new(user.asJson());
        renderer.render('<edit-english-documents user="user" user-proxy="userProxy"></edit-english-documents>');
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$digest();
    }

    it('should set disabled class and disable if not required', () => {
        render();
        jest.spyOn(
            scope.userProxy,
            'recordsIndicateUserShouldUploadEnglishLanguageProficiencyDocuments',
            'get',
        ).mockReturnValue(false);
        scope.$digest();

        SpecHelper.expectElementDisabled(elem, '.english_language_proficiency_documents_type');

        // If for some reason the user had a type set then became recordsIndicate = false, these would be
        // disabled too.
        scope.userProxy.english_language_proficiency_documents_type = 'foo';
        scope.$digest();

        SpecHelper.expectElementDisabled(elem, '.english_language_proficiency_documents_type');
        SpecHelper.expectElementDisabled(elem, '[name="english-language-proficiency-comments"]');
        SpecHelper.expectElementDisabled(elem, '[name="english-proficiency-upload"]');
        SpecHelper.expectNoElement(elem, '[name="english-proficiency-container"] .trashcan');
    });

    it('should support selecting english_language_proficiency_documents_type', () => {
        render();
        expect(scope.userProxy.english_language_proficiency_documents_type).toEqual('duolingo');
        SpecHelper.updateSelect(elem, '.english_language_proficiency_documents_type', 'other');
        expect(scope.userProxy.english_language_proficiency_documents_type).toEqual('other');
    });

    it('should only show upload and comments section if userProxy.english_language_proficiency_documents_type is present', () => {
        render();
        SpecHelper.expectElement(elem, 'input[name="english-proficiency-upload"]');
        SpecHelper.expectElement(elem, 'textarea[name="english-language-proficiency-comments"]');

        scope.userProxy.english_language_proficiency_documents_type = null;
        scope.$digest();
        SpecHelper.expectNoElement(elem, 'input[name="english-proficiency-upload"]');
        SpecHelper.expectNoElement(elem, 'textarea[name="english-language-proficiency-comments"]');
    });

    describe('documentation type select', () => {
        it("should be disabled if user's !recordsIndicateUserShouldUploadEnglishLanguageProficiencyDocuments", () => {
            render();
            const spy = jest
                .spyOn(scope.userProxy, 'recordsIndicateUserShouldUploadEnglishLanguageProficiencyDocuments', 'get')
                .mockReturnValue(true);
            scope.$digest();
            SpecHelper.expectElementEnabled(elem, '.english_language_proficiency_documents_type');

            spy.mockReturnValue(false);
            scope.$digest();
            SpecHelper.expectElementDisabled(elem, '.english_language_proficiency_documents_type');
        });

        it('should only be required if recordsIndicateUserShouldUploadEnglishLanguageProficiencyDocuments', () => {
            render();
            const spy = jest
                .spyOn(scope.userProxy, 'recordsIndicateUserShouldUploadEnglishLanguageProficiencyDocuments', 'get')
                .mockReturnValue(false);
            scope.$digest();
            SpecHelper.expectElementNotRequired(elem, '.english_language_proficiency_documents_type');

            spy.mockReturnValue(true);
            scope.$digest();
            SpecHelper.expectElementRequired(elem, '.english_language_proficiency_documents_type');
        });
    });

    describe('upload area', () => {
        beforeEach(() => {
            user.english_language_proficiency_documents_type = 'foo'; // uploaded documents won't show up unless this is set
            user.s3_english_language_proficiency_documents = [S3EnglishLanguageProficiencyDocument.new()];
        });

        it('should apply clickable class to file-name if adminMode', () => {
            render();
            scope.adminMode = true;
            scope.$digest();
            SpecHelper.expectHasClass(elem, '.file-name', 'clickable');
        });

        it('should allow viewing document if adminMode', () => {
            render();
            scope.adminMode = true;
            scope.$digest();
            jest.spyOn(scope, 'viewDocument');
            jest.spyOn($http, 'get').mockReturnValue(
                $q.when({
                    data: {},
                }),
            );
            SpecHelper.click(elem, '.file-name');
            expect(scope.viewDocument).toHaveBeenCalledWith(
                scope.userProxy.s3_english_language_proficiency_documents[0],
            );
            expect($http.get).toHaveBeenCalled();
        });

        it('should not allow viewing document if !adminMode', () => {
            render();
            scope.adminMode = false;
            scope.$digest();
            jest.spyOn($http, 'get').mockImplementation(() => {});
            SpecHelper.click(elem, '.file-name');
            expect($http.get).not.toHaveBeenCalled();
        });

        it('should support deleting document', () => {
            render();
            jest.spyOn(scope, 'deleteEnglishLanguageProficiencyDocument').mockImplementation(() => {});
            SpecHelper.click(elem, '.trashcan');
            expect(scope.deleteEnglishLanguageProficiencyDocument).toHaveBeenCalledWith(0);
        });

        it('should apply disabled class to trashcan when deletingEnglishLanguageProficiencyDocument and prevent destroy call', () => {
            render();
            SpecHelper.expectDoesNotHaveClass(elem, '.trashcan', 'disabled');
            scope.deletingEnglishLanguageProficiencyDocument = true;
            scope.$digest();
            SpecHelper.expectHasClass(elem, '.trashcan', 'disabled');
            jest.spyOn(user.s3_english_language_proficiency_documents[0], 'destroy').mockImplementation(() => {});
            SpecHelper.click(elem, '.trashcan');
            expect(user.s3_english_language_proficiency_documents[0].destroy).not.toHaveBeenCalled();
        });

        it('should show a spinner when deletingEnglishLanguageProficiencyDocument', () => {
            render();
            SpecHelper.expectNoElement(elem, 'front-royal-spinner');
            scope.deletingEnglishLanguageProficiencyDocument = true;
            scope.$digest();
            SpecHelper.expectElement(elem, 'front-royal-spinner');
        });

        it('should show a spinner when uploadingEnglishLanguageProficiencyDocument', () => {
            render();
            SpecHelper.expectNoElement(elem, 'front-royal-spinner');
            scope.uploadingEnglishLanguageProficiencyDocument = true;
            scope.$digest();
            SpecHelper.expectElement(elem, 'front-royal-spinner');
        });

        it('should show a spinner when deletingEnglishLanguageProficiencyDocument', () => {
            render();
            SpecHelper.expectNoElement(elem, 'front-royal-spinner');
            scope.deletingEnglishLanguageProficiencyDocument = true;
            scope.$digest();
            SpecHelper.expectElement(elem, 'front-royal-spinner');
        });

        it('should show message if there is an error', () => {
            render();
            SpecHelper.expectNoElement(elem, '.file-error-message');
            scope.englishLanguageProficiencyDocumentErrMessage = 'foo message';
            scope.$digest();
            SpecHelper.expectElementText(elem, '.file-error-message', 'foo message');
        });

        describe('upload input', () => {
            it('should be disabled if MAX_ENGLISH_LANGUAGE_PROFICIENCY_DOCUMENTS limit has been reached', () => {
                render();
                jest.spyOn(
                    scope.userProxy,
                    'recordsIndicateUserShouldUploadEnglishLanguageProficiencyDocuments',
                    'get',
                ).mockReturnValue(true);
                scope.$digest();
                SpecHelper.expectElementEnabled(elem, 'input[name="english-proficiency-upload"]');
                scope.userProxy.s3_english_language_proficiency_documents = [{}, {}, {}, {}, {}, {}, {}];
                scope.$digest();
                SpecHelper.expectElementDisabled(elem, 'input[name="english-proficiency-upload"]');
            });
        });
    });

    describe('comments textarea', () => {
        beforeEach(() => {
            // ensures the textarea is visible
            user.english_language_proficiency_documents_type = 'foo';
        });

        it("should be disabled if user's !recordsIndicateUserShouldUploadEnglishLanguageProficiencyDocuments", () => {
            jest.spyOn(
                user,
                'recordsIndicateUserShouldUploadEnglishLanguageProficiencyDocuments',
                'get',
            ).mockReturnValue(false);
            render();
            SpecHelper.expectHasClass(elem, '.english-language-proficiency-comments-container label', 'disabled');
            SpecHelper.expectElementDisabled(elem, '.english-language-proficiency-comments-container textarea');
        });

        describe('when enabled', () => {
            beforeEach(() => {
                jest.spyOn(
                    user,
                    'recordsIndicateUserShouldUploadEnglishLanguageProficiencyDocuments',
                    'get',
                ).mockReturnValue(true);
            });

            it('should be wired to userProxy.english_language_proficiency_comments', () => {
                user.english_language_proficiency_comments = null;
                render();
                expect(scope.userProxy.english_language_proficiency_comments).not.toEqual('foo');
                SpecHelper.updateTextArea(elem, 'textarea[name="english-language-proficiency-comments"]', 'foo');
                expect(scope.userProxy.english_language_proficiency_comments).toEqual('foo');
            });
        });
    });
});
