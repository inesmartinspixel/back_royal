import 'AngularSpecHelper';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';
import 'Settings/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import embaWelcomePackageLocales from 'Settings/locales/settings/emba_welcome_package-en.json';

setSpecLocales(embaWelcomePackageLocales);

describe('Settings::TuitionAndRegistration::EmbaWelcomePackage', () => {
    let renderer;
    let elem;
    let SpecHelper;
    let $injector;
    let currentUser;
    let Cohort;
    let CohortApplication;
    let launchProgramGuide;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Settings', 'SpecHelper');

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            SpecHelper = $injector.get('SpecHelper');
            Cohort = $injector.get('Cohort');
            CohortApplication = $injector.get('CohortApplication');

            $injector.get('CohortFixtures');
        });

        SpecHelper.stubConfig();
        currentUser = SpecHelper.stubCurrentUser('learner');

        const cohort = Cohort.fixtures.getInstance({
            program_type: 'emba',
            id: 123,
            registration_deadline_days_offset: -4,
            name: 'EMBA14',
            start_date: new Date('2117/07/01').getTime() / 1000,
        });

        const promoted = CohortApplication.new({
            status: 'pre_accepted',
            program_type: 'emba',
            cohort_id: cohort.id,
            applied_at: new Date().getTime() / 1000,
            cohort,
            cohort_title: cohort.title,
            cohort_name: cohort.name,
            cohort_program_guide_url: 'program_guide_url',
            total_num_required_stripe_payments: 12,
        });
        currentUser.relevant_cohort = promoted.cohort;
        currentUser.cohort_applications = [promoted];

        launchProgramGuide = jest.fn();
    });

    it('should display the normal elements', () => {
        render();

        SpecHelper.expectElementText(elem, '.classmates .headline', 'Here are a few of your registered classmates.');

        SpecHelper.expectElements(elem, '.student-profile', 6); // hard-coded profiles for now

        SpecHelper.expectElementText(
            elem,
            '.program-guide .headline',
            'Meet additional students and get all the details',
        );

        SpecHelper.expectElementText(elem, '.video .headline', 'Our students explain the Quantic difference');
        SpecHelper.expectElement(elem, '.video iframe');
    });

    describe('with or without classmates', () => {
        it('should display proper text when we have classmates to preview', () => {
            render();
            SpecHelper.expectElementText(
                elem,
                '.classmates .headline',
                'Here are a few of your registered classmates.',
            );
        });

        it('should display proper text when we do not have classmates to preview', () => {
            // HACK: for now, EMBA7 is hard-coded as the current cohort
            currentUser.relevant_cohort.name = 'EMBA3';
            _.last(currentUser.cohort_applications).cohort_name = currentUser.relevant_cohort.name;

            render();
            SpecHelper.expectElementText(
                elem,
                '.classmates .headline',
                'These students have recently registered in the EMBA',
            );
        });
    });

    describe('program guide', () => {
        it('should hide program guide section when we do not have a program guide', () => {
            render();

            SpecHelper.click(elem, '.program-guide-download');
            expect(launchProgramGuide).toHaveBeenCalled();
        });

        it('should hide program guide section when we do not have a program guide', () => {
            _.last(currentUser.cohort_applications).cohort_program_guide_url = undefined;
            render();
            SpecHelper.expectNoElement(elem, '.program-guide');
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        const cohortApplication = _.last(currentUser.cohort_applications);

        renderer = SpecHelper.renderer();
        renderer.scope.launchProgramGuide = launchProgramGuide;
        renderer.scope.cohortApplication = cohortApplication;

        renderer.render(
            '<emba-welcome-package cohort-application="cohortApplication" launch-program-guide="launchProgramGuide()"></emba-welcome-package>',
        );
        elem = renderer.elem;
    }
});
