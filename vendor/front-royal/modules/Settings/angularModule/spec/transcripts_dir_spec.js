import 'AngularSpecHelper';
import 'Settings/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import transcriptsLocales from 'Settings/locales/settings/transcripts-en.json';

setSpecLocales(transcriptsLocales);

describe('Settings::transcripts', () => {
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let $injector;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Settings', 'SpecHelper');

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            SpecHelper = $injector.get('SpecHelper');
        });

        SpecHelper.stubConfig();
        SpecHelper.stubCurrentUser('learner');
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.render('<transcripts></transcripts>');
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    describe('on desktop', () => {
        it('should support transcript download', () => {
            render();
            SpecHelper.expectElement(elem, '[name="download_transcript"]');
            jest.spyOn(scope.transcriptHelper, 'downloadTranscript').mockImplementation(() => {});
            SpecHelper.click(elem, '[name="download_transcript"]');
            expect(scope.transcriptHelper.downloadTranscript).toHaveBeenCalledWith(scope.currentUser, 'digital', false);
        });

        it('should show a spinner when transcript state shows downloading', () => {
            render();
            jest.spyOn(scope.transcriptHelper, 'downloadTranscript').mockImplementation(() => {});

            SpecHelper.expectNoElement(elem, 'front-royal-spinner');
            scope.transcriptHelper.downloadingTranscript = true;
            scope.$digest();
            SpecHelper.expectElement(elem, 'front-royal-spinner');
        });
    });

    describe('on mobile', () => {
        it('should not support transcript download', () => {
            render();
            scope.isMobile = true;
            scope.$digest();
            SpecHelper.expectNoElement(elem, '[name="download_transcript"]');
        });
    });
});
