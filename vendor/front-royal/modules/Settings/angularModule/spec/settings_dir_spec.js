import moment from 'moment-timezone';
import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import 'Careers/angularModule/spec/_mock/fixtures/hiring_application_fixtures';

import 'Careers/angularModule/spec/_mock/fixtures/hiring_team_fixtures';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohort_applications';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';
import 'Settings/angularModule';

import setSpecLocales from 'Translation/setSpecLocales';
import settingsLocales from 'Settings/locales/settings/settings-en.json';
import editCareerProfileLocales from 'Careers/locales/careers/edit_career_profile-en.json';
import editAccountLocales from 'Settings/locales/settings/edit_account-en.json';
import previewProfileStatusMessageLocales from 'Careers/locales/careers/preview_profile_status_message-en.json';
import editHiringManagerProfileLocales from 'Careers/locales/careers/edit_hiring_manager_profile-en.json';
import companyFormLocales from 'Careers/locales/careers/edit_hiring_manager_profile/company_form-en.json';
import fieldOptionsLocales from 'Careers/locales/careers/field_options-en.json';
import applicationStatusLocales from 'Settings/locales/settings/application_status-en.json';
import tuitionAndRegistrationLocales from 'Settings/locales/settings/tuition_and_registration-en.json';
import embaWelcomPackageLocales from 'Settings/locales/settings/emba_welcome_package-en.json';
import youFormLocales from 'Careers/locales/careers/edit_hiring_manager_profile/you_form-en.json';
import writeTextAboutLocales from 'FrontRoyalForm/locales/front_royal_form/inputs/write_text_about-en.json';
import privacyTermsFooterLocales from 'Navigation/locales/navigation/privacy_terms_footer-en.json';

setSpecLocales(
    settingsLocales,
    editCareerProfileLocales,
    editAccountLocales,
    previewProfileStatusMessageLocales,
    editHiringManagerProfileLocales,
    companyFormLocales,
    fieldOptionsLocales,
    applicationStatusLocales,
    tuitionAndRegistrationLocales,
    embaWelcomPackageLocales,
    youFormLocales,
    writeTextAboutLocales,
    privacyTermsFooterLocales,
);

describe('Settings::Settings', () => {
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let $injector;
    let currentUser;
    let $location;
    let HiringApplication;
    let CohortApplication;
    let ClientStorage;
    let CareerProfile;
    let EditCareerProfileHelper;
    let Cohort;
    let ProfileCompletionHelper;
    let isMobileValue = false;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Settings', 'SpecHelper', 'FrontRoyal.Lessons', 'FrontRoyal.Careers');

        angular.mock.module($provide => {
            const isMobile = jest.fn();
            isMobile.mockImplementation(() => isMobileValue);
            $provide.value('isMobile', isMobile);
        });

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            SpecHelper = $injector.get('SpecHelper');
            $location = $injector.get('$location');
            HiringApplication = $injector.get('HiringApplication');
            CohortApplication = $injector.get('CohortApplication');
            ClientStorage = $injector.get('ClientStorage');
            CareerProfile = $injector.get('CareerProfile');
            EditCareerProfileHelper = $injector.get('EditCareerProfileHelper');
            Cohort = $injector.get('Cohort');
            ProfileCompletionHelper = $injector.get('ProfileCompletionHelper');

            $injector.get('HiringApplicationFixtures');
            $injector.get('CohortApplicationFixtures');
            $injector.get('CareerProfileFixtures');
            $injector.get('CohortFixtures');
        });

        SpecHelper.stubConfig();
        currentUser = SpecHelper.stubCurrentUser('learner');
        currentUser.career_profile = CareerProfile.fixtures.getInstance({
            program_type: 'mba',
        });
        SpecHelper.stubDirective('locationAutocomplete'); // mock out google places api
        SpecHelper.stubDirective('uploadAvatar');
        SpecHelper.stubDirective('editCareerProfile');
        SpecHelper.stubStripeCheckout();
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render(section = 'account', page) {
        renderer = SpecHelper.renderer();
        const pageRenderString = page ? ` page="${page}" ` : '';
        renderer.render(`<settings section="${section}"${pageRenderString}></settings>`);
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    describe('mobileNavExpanded', () => {
        beforeEach(() => {
            isMobileValue = true;
            render('mobile');
        });

        it('should be true in mobile section when isMobile is true', () => {
            expect(scope.isMobile).toBe(true);
            expect(scope.mobileNavExpanded).toBe(true);
        });

        it('should set to false when menu is closed', () => {
            SpecHelper.click(elem, '.mobile-nav-toggle-btn');
            expect(scope.mobileNavExpanded).toBe(false);
        });
    });

    describe('reapplyingOrEditingApplication', () => {
        it('should be set to false if the acceptedOrPreAcceptedCohortApplication', () => {
            jest.spyOn(currentUser, 'acceptedOrPreAcceptedCohortApplication', 'get').mockReturnValue({}); // just needs to return something truthy
            const reapplyingOrEditingApplicationSpy = jest.spyOn(currentUser, 'reapplyingOrEditingApplication', 'set');
            render();
            expect(reapplyingOrEditingApplicationSpy).toHaveBeenCalledWith(false);
        });

        it('should be set to false if lastCohortApplication is rejected and cannot yet reapply', () => {
            const application = CohortApplication.fixtures.getInstance({
                status: 'rejected',
            });
            jest.spyOn(application, 'canReapplyTo').mockImplementation(() => false);
            jest.spyOn(currentUser, 'lastCohortApplication', 'get').mockReturnValue(application);
            const reapplyingOrEditingApplicationSpy = jest.spyOn(currentUser, 'reapplyingOrEditingApplication', 'set');
            render();
            expect(reapplyingOrEditingApplicationSpy).toHaveBeenCalledWith(false);
        });

        it('should be true if lastCohortApplication can_convert_to_emba', () => {
            // prevent false positives
            jest.spyOn(currentUser, 'acceptedOrPreAcceptedCohortApplication', 'get').mockReturnValue(undefined);
            jest.spyOn(currentUser, 'canEditCurrentCohortApplication', 'get').mockReturnValue(false);

            const lastCohortApplication = CohortApplication.fixtures.getInstance({
                can_convert_to_emba: true,
            });
            jest.spyOn(currentUser, 'lastCohortApplication', 'get').mockReturnValue(lastCohortApplication);
            render();
            expect(scope.currentUser.lastCohortApplication.can_convert_to_emba).toBe(true);
            expect(currentUser.reapplyingOrEditingApplication).toBe(true);
        });

        describe('direct linking', () => {
            it('should be set to true', () => {
                jest.spyOn($location, 'path').mockReturnValue('/settings/application');
                jest.spyOn(currentUser, 'canEditCurrentCohortApplication', 'get').mockReturnValue(true);
                expect(currentUser.reapplyingOrEditingApplication).not.toBe(true);
                render();
                expect(currentUser.reapplyingOrEditingApplication).toBe(true);
            });

            it('should not be set to true if the canEditCurrentCohortApplication is false', () => {
                jest.spyOn($location, 'path').mockReturnValue('/settings/application');
                jest.spyOn(currentUser, 'canEditCurrentCohortApplication', 'get').mockReturnValue(false);
                render();
                expect(currentUser.reapplyingOrEditingApplication).toBe(false);
            });
        });
    });

    describe('initialStep', () => {
        beforeEach(() => {
            jest.spyOn(EditCareerProfileHelper, 'isCareerProfile').mockReturnValue(false);
            jest.spyOn(EditCareerProfileHelper, 'isApplication').mockReturnValue(true);
            jest.spyOn(EditCareerProfileHelper, 'getStepsForApplicationForm').mockReturnValue([
                {
                    stepName: 'program_choice',
                },
                {
                    stepName: 'basic_info',
                },
                {
                    stepName: 'more_about_you',
                },
            ]);
            jest.spyOn(currentUser, 'canApplyToSmartly', 'get').mockReturnValue(true);
        });

        it('should skip program selection page when career_network_only is preselected', () => {
            currentUser.career_profile = CareerProfile.fixtures.getInstance({
                program_type: 'career_network_only',
            });
            render('application');
            expect(scope.initialStep).toBe('2');
        });

        it('should skip program selection page when biz cert is preselected', () => {
            currentUser.career_profile = CareerProfile.fixtures.getInstance({
                program_type: 'the_business_certificate',
            });
            render('application');
            expect(scope.initialStep).toBe('2');
        });

        it('should skip program selection page when emba is preselected', () => {
            currentUser.career_profile = CareerProfile.fixtures.getInstance({
                program_type: 'emba',
            });
            render('application');
            expect(scope.initialStep).toBe('2');
        });

        it('should not skip the first step if in profile view', () => {
            jest.spyOn(EditCareerProfileHelper, 'isCareerProfile').mockReturnValue(true);
            jest.spyOn(EditCareerProfileHelper, 'isApplication').mockReturnValue(false);
            jest.spyOn(EditCareerProfileHelper, 'getStepsForApplicationForm').mockReturnValue([
                {
                    stepName: 'basic_info',
                },
                {
                    stepName: 'more_about_you',
                },
            ]);
            currentUser.career_profile = CareerProfile.fixtures.getInstance({
                program_type: 'emba',
            });
            render('my-profile');
            expect(scope.initialStep).toBe('1');
        });

        it('should NOT skip program selection page when lastCohortApplication indicates user can_convert_to_emba (even if program_type supportsSkippingProgramChoice)', () => {
            const lastCohortApplication = CohortApplication.fixtures.getInstance({
                can_convert_to_emba: true,
            });
            jest.spyOn(currentUser, 'lastCohortApplication', 'get').mockReturnValue(lastCohortApplication);
            jest.spyOn(Cohort, 'supportsSkippingProgramChoice').mockReturnValue(true);
            render('application');
            expect(scope.initialStep).toBe('1');
        });

        it('should NOT skip program selection page when biz cert is preselected AND page is defined', () => {
            currentUser.career_profile = CareerProfile.fixtures.getInstance({
                program_type: 'the_business_certificate',
            });
            render('application', 1);
            expect(scope.initialStep).toBe('1');
        });

        it('should NOT skip program selection page when mba is preselected', () => {
            currentUser.career_profile = CareerProfile.fixtures.getInstance({
                program_type: 'mba',
            });
            render('application');
            expect(scope.initialStep).toBe('1');
        });

        it('should handle the page param', () => {
            currentUser.career_profile = CareerProfile.fixtures.getInstance({
                program_type: 'emba',
            });
            render('application', 3);
            expect(scope.initialStep).toBe('3');
        });

        it('should handle out of bounds page params with defaults', () => {
            currentUser.career_profile = CareerProfile.fixtures.getInstance({
                program_type: 'emba',
            });

            jest.spyOn(EditCareerProfileHelper, 'getStepsForApplicationForm').mockReturnValue([
                {
                    stepName: 'program_choice',
                },
                {
                    stepName: 'basic_info',
                },
            ]);

            render('application', 3);
            expect(scope.initialStep).toBe('2');
        });
    });

    describe('header', () => {
        it('should set the text to Library', () => {
            const AppHeaderViewModel = $injector.get('Navigation.AppHeader.AppHeaderViewModel');

            jest.spyOn(AppHeaderViewModel, 'setTitleHTML').mockImplementation(() => {});
            render();

            expect(AppHeaderViewModel.setTitleHTML).toHaveBeenCalledWith('SETTINGS');
        });
    });

    ['navigation-container', 'navigation-container-mobile'].forEach(container => {
        describe(container, () => {
            it('should navigate to a settings section when clicked', () => {
                renderAndShowMobileNav();
                const li = SpecHelper.expectElement(elem, '[name="account"]');

                jest.spyOn(scope, 'gotoSection');
                jest.spyOn($location, 'path').mockReturnValue({
                    replace: jest.fn(),
                });

                li.click();
                scope.$digest();

                expect(scope.gotoSection).toHaveBeenCalledWith('account');
                expect($location.path).toHaveBeenCalledWith('/settings/account');
            });

            it('should sign out when clicked', () => {
                renderAndShowMobileNav();
                const li = SpecHelper.expectElement(elem, '[name="sign_out"]');

                const SignOutHelper = $injector.get('SignOutHelper');
                const $rootScope = $injector.get('$rootScope');
                const ValidationResponder = $injector.get('ValidationResponder');

                ValidationResponder.initialize();

                jest.spyOn(SignOutHelper, 'signOut').mockImplementation(() => {});

                li.click();
                scope.$digest();

                expect(SignOutHelper.signOut).toHaveBeenCalled();
                jest.spyOn(ClientStorage, 'removeItem').mockImplementation(() => {});

                // $window.location.replace = jest.fn(); // cannot spy on this in chrome, see also: validation_responder for TEST check

                $rootScope.$broadcast('auth:logout-success'); // mock the ng-token-auth logout event

                // NOTE: see also - validation_responser.js -> TRANSIENT_STORAGE_KEYS
                expect(ClientStorage.removeItem).toHaveBeenCalledWith('librarySearchProgress');
                expect(ClientStorage.removeItem).toHaveBeenCalledWith('librarySearchText');
                expect(ClientStorage.removeItem).toHaveBeenCalledWith('librarySearchTopic');
                expect(ClientStorage.removeItem).toHaveBeenCalledWith('librarySearchProgress');
                expect(ClientStorage.removeItem).toHaveBeenCalledWith('toggleableCourseListLibrary');
                expect(ClientStorage.removeItem).toHaveBeenCalledWith('toggleableCourseListLibrary');
                expect(ClientStorage.removeItem).toHaveBeenCalledWith('toggleableCourseListStudentDashboard');
                expect(ClientStorage.removeItem).toHaveBeenCalledWith('soundEnabled');
                expect(ClientStorage.removeItem).toHaveBeenCalledWith('amplitude_sessionId');
                expect(ClientStorage.removeItem).toHaveBeenCalledWith('amplitude_unsent');
                expect(ClientStorage.removeItem).toHaveBeenCalledWith('amplitude_lastEventId');
                expect(ClientStorage.removeItem).toHaveBeenCalledWith('amplitude_lastEventTime');

                // FIXME: See if this should move to somewhere else
                // expect($window.location.replace).toHaveBeenCalledWith('/');
            });
        });

        function renderAndShowMobileNav() {
            render();
            if (container === 'navigation-container-mobile') {
                scope.showMobileNav = true;
                scope.$digest();
            }
        }
    });

    describe('sub-sections', () => {
        beforeEach(() => {
            jest.spyOn(currentUser, 'canApplyToSmartly', 'get').mockReturnValue(true);
            render();
        });

        it('should have sub-header if defined in settingsSubHeaders and not the profile, application, or application_status', () => {
            render();
            SpecHelper.expectElements(elem, '.main-box-header > span', 1);

            scope.section = 'profile';
            scope.$apply();
            SpecHelper.expectElements(elem, '.main-box-header > span', 0);

            scope.section = 'application';
            scope.$apply();
            SpecHelper.expectElements(elem, '.main-box-header > span', 0);
        });

        it('should only have Apply to Quantic section if user has a valid programType and either does not have an application or is reapplyingOrEditingApplication', () => {
            currentUser.career_profile = CareerProfile.fixtures.getInstance();
            const reapplyingOrEditingApplicationSpy = jest.spyOn(currentUser, 'reapplyingOrEditingApplication', 'get');
            reapplyingOrEditingApplicationSpy.mockReturnValue(true);

            render();
            SpecHelper.expectElement(elem, '[name="application"]');

            reapplyingOrEditingApplicationSpy.mockReturnValue(false);
            currentUser.cohort_applications = [];
            render();
            SpecHelper.expectElement(elem, '[name="application"]');

            Object.defineProperty(currentUser, 'lastCohortApplicationStatus', {
                value: true,
                configurable: true,
            });
            currentUser.cohort_applications = [
                {
                    foo: 'bar',
                },
            ];
            render();
            SpecHelper.expectNoElement(elem, '[name="application"]');
        });

        it('should not have Apply to Quantic section if user has a valid programType but has been accepted', () => {
            currentUser.career_profile = CareerProfile.fixtures.getInstance();
            // simulating a situation where the user had started to reapply in one browser, finished it in another,
            // and then got accepted and returned to the first browser that still has the local storage value set
            currentUser.cohort_applications = [
                {
                    cohort_name: 'FAKEMBA1',
                    status: 'rejected',
                    applied_at: new Date().getTime() / 1000 - 10000,
                },
                {
                    cohort_name: 'FAKEMBA2',
                    status: 'accepted',
                    applied_at: new Date().getTime() / 1000,
                },
            ];
            jest.spyOn(ClientStorage, 'getItem').mockReturnValue('true'); // force $$reapplyingToMBA to be true

            render();
            SpecHelper.expectNoElement(elem, '[name="application"]');
            SpecHelper.expectElement(elem, '[name="application_status"]');
        });

        it('should only have Application Status section if user has a valid programType and has an application and is not trying to reapply', () => {
            Object.defineProperty(currentUser, 'lastCohortApplicationStatus', {
                value: true,
                configurable: true,
            });
            render();
            SpecHelper.expectElement(elem, '[name="application_status"]');

            jest.spyOn(currentUser, 'reapplyingOrEditingApplication', 'get').mockReturnValue(false);
            render();
            SpecHelper.expectElement(elem, '[name="application_status"]');
        });

        it('should render appropriate header text when on application status based on program and status', () => {
            const expectations = [
                {
                    programType: 'mba',
                    title: 'Deferred for Quantic MBA',
                    status: 'deferred',
                },
                {
                    programType: 'emba',
                    title: 'Deferred for Quantic EMBA',
                    status: 'deferred',
                },
                {
                    // we do not expect this to ever be set to deferred.
                    // But check that we have a fallback
                    // (it will log a warning to sentry)
                    programType: 'the_business_certificate',
                    cohortTitle: 'Some Business Certificate Cohort',
                    title: 'Deferred for Some Business Certificate Cohort',
                    status: 'deferred',
                },
                {
                    programType: 'mba',
                    cohortTitle: 'Some Cohort',
                    title: 'Accepted to Some Cohort',
                    status: 'accepted',
                },
            ];

            const timestamp = 1474653600; // 9/23/2016 UTC

            jest.spyOn($location, 'search').mockReturnValue({
                page: 1,
            });

            const cohort = Cohort.new({
                foundations_playlist_locale_pack: {
                    content_items: [
                        {
                            locale: 'en',
                            title: 'Business Foundations',
                        },
                    ],
                },
                application_deadline: timestamp,
            });

            expectations.forEach(config => {
                currentUser.relevant_cohort = cohort;
                cohort.program_type = config.programType;
                currentUser.relevant_cohort.title = config.cohortTitle;

                currentUser.cohort_applications = [
                    CohortApplication.new({
                        status: config.status,
                        program_type: config.programType,
                        cohort_application_deadline: timestamp,
                        total_num_required_stripe_payments: 12,
                        cohort,
                        cohort_title: cohort.title,
                        stripe_plans: [
                            {
                                id: 'monthly_plan',
                                frequency: 'monthly',
                            },
                        ],
                        scholarship_level: {
                            id: 'coupon1',
                            standard: {
                                monthly_plan: {
                                    amount_off: 0,
                                },
                            },
                            early: {
                                monthly_plan: {
                                    amount_off: 0,
                                },
                            },
                            coupons: {
                                monthly_plan: {
                                    amount_off: 0,
                                },
                            },
                        },
                    }),
                ];

                currentUser.fallback_program_type = config.programType;
                currentUser.has_seen_accepted = true;

                CareerProfile.expect('create');
                render('application_status');
                SpecHelper.expectElementText(elem, '.main-box-header', config.title);
            });
        });

        it('should log an event if they navigate to the Apply to Quantic section', () => {
            jest.spyOn(EditCareerProfileHelper, 'logEventForApplication').mockImplementation(() => {});
            render('application');

            scope.$digest();
            expect(EditCareerProfileHelper.logEventForApplication).toHaveBeenCalledWith('start-application');
        });

        it('should set class if profile or application is activated', () => {
            render('application');
            SpecHelper.expectHasClass(elem, '[name="application"]', 'expanded');
        });

        describe('edit-account', () => {
            it('should be the default section', () => {
                expect(scope.section).toEqual('account');
            });

            it('should render the edit-account component', () => {
                SpecHelper.expectElement(elem, 'edit-account');
            });
        });
    });

    describe('student without a cohort application', () => {
        beforeEach(() => {
            jest.spyOn(currentUser, 'canApplyToSmartly', 'get').mockReturnValue(true);
            currentUser.career_profile = CareerProfile.fixtures.getInstance();
        });

        it('should not include Documents section', () => {
            render();
            expect(
                _.findWhere(scope.availableSections, {
                    name: 'documents',
                }),
            ).toBeUndefined();
            SpecHelper.expectNoElement(elem, '[name="documents"]');
        });

        it('should not add Documents section when changing fallback_program_type', () => {
            let documentsStep;
            currentUser.fallback_program_type = 'the_business_certificate';
            render();
            documentsStep = _.findWhere(scope.availableSections, {
                name: 'documents',
            });
            expect(documentsStep).toBeUndefined();

            scope.currentUser.fallback_program_type = 'mba';
            scope.$digest();
            documentsStep = _.findWhere(scope.availableSections, {
                name: 'documents',
            });
            expect(documentsStep).toBeUndefined();
        });

        it('should render "Apply to Quantic" section instead of "Application Status" section', () => {
            Object.defineProperty(currentUser, 'programType', {
                value: 'mba',
            });
            delete currentUser.cohort_applications;
            render();
            SpecHelper.expectElement(elem, '[name="application"]');
            SpecHelper.expectNoElement(elem, '[name="application_status"]');
        });

        it('should render appropriate header text when on "Apply to Quantic" program choice section', () => {
            jest.spyOn(EditCareerProfileHelper, 'getStepsForApplicationForm').mockReturnValue([
                {
                    stepName: 'program_choice',
                },
            ]);
            jest.spyOn($location, 'search').mockReturnValue({
                page: 1,
            });
            render('application');
            SpecHelper.expectElementText(elem, '[name="application-header-text"]', 'Choose Your Program');
        });

        it('should render appropriate header text when on "Apply to Quantic" basic info section', () => {
            jest.spyOn(EditCareerProfileHelper, 'getStepsForApplicationForm').mockReturnValue([
                {
                    stepName: 'basic_info',
                },
            ]);
            jest.spyOn($location, 'search').mockReturnValue({
                page: 1,
            });
            render('application');
            SpecHelper.expectElementText(elem, '[name="application-header-text"]', 'Basic Info');
        });

        it('should render appropriate header text when on MBA Application Questions section', () => {
            jest.spyOn(EditCareerProfileHelper, 'getStepsForApplicationForm').mockReturnValue([
                {
                    stepName: 'mba_application_questions',
                },
            ]);
            jest.spyOn($location, 'search').mockReturnValue({
                page: 1,
            });
            render('application');
            SpecHelper.expectElementText(elem, '[name="application-header-text"]', 'MBA Application Questions');
        });

        it('should render appropriate sub-header text when on MBA Application Questions section', () => {
            jest.spyOn(EditCareerProfileHelper, 'getStepsForApplicationForm').mockReturnValue([
                {
                    stepName: 'mba_application_questions',
                },
            ]);
            jest.spyOn($location, 'search').mockReturnValue({
                page: 1,
            });
            render('application');
            SpecHelper.expectElementText(
                elem,
                '.sub-header',
                'Personalize your application with your answers below. You may be invited to an online interview to elaborate.',
            );
        });

        it('should render appropriate header text when on EMBA Application Questions section', () => {
            Object.defineProperty(currentUser, 'programType', {
                value: 'emba',
                configurable: true,
            });
            jest.spyOn(EditCareerProfileHelper, 'getStepsForApplicationForm').mockReturnValue([
                {
                    stepName: 'emba_application_questions',
                },
            ]);
            jest.spyOn($location, 'search').mockReturnValue({
                page: 1,
            });
            render('application');
            SpecHelper.expectElementText(elem, '[name="application-header-text"]', 'EMBA Application Questions');
        });

        it('should render appropriate sub-header text when on EMBA Application Questions section', () => {
            Object.defineProperty(currentUser, 'programType', {
                value: 'emba',
                configurable: true,
            });
            jest.spyOn(EditCareerProfileHelper, 'getStepsForApplicationForm').mockReturnValue([
                {
                    stepName: 'emba_application_questions',
                },
            ]);
            jest.spyOn($location, 'search').mockReturnValue({
                page: 1,
            });
            render('application');
            SpecHelper.expectElementText(
                elem,
                '.sub-header',
                'We encourage you to personalize your application with the required and optional questions below. You may be invited to an online interview to elaborate.',
            );
        });

        // Note: The way we change pages for the application is a little different than the HiringManager profile
        it('should have working sub-nav', () => {
            render('application');
            jest.spyOn(scope, '$broadcast').mockImplementation(() => {});
            SpecHelper.click(elem, '.responsive-nav .nav-2-item:eq(1)');
            expect(scope.$broadcast).toHaveBeenCalledWith('gotoFormStep', 1);
        });

        it('should set mobile nav state on sub-nav item click', () => {
            render('application');
            SpecHelper.click(elem, '.responsive-nav .nav-2-item:eq(2)');
            expect(scope.mobileNavExpanded).toBe(false);
        });

        it('should show progress nav', () => {
            render('application');
            const progressNav = SpecHelper.expectElement(elem, '.main-box progress-nav');
            expect(progressNav.find('.percentage .circle').text()).toEqual(
                `${scope.completionHelper.getPercentComplete(scope.currentUser.career_profile)}%`,
            ); // extra space because the html creates one
        });

        it('should add a blue dot after the edit profile section name if section is incomplete', () => {
            // NOTE: the blue dot is present only if the incomplete class is applied.
            // See settings.html

            jest.spyOn(ProfileCompletionHelper.prototype, 'getStepsProgressMap').mockReturnValue({
                basic_info: 'incomplete',
            });
            render('application');
            expect(ProfileCompletionHelper.prototype.getStepsProgressMap).toHaveBeenCalledWith(
                scope.currentUser.career_profile,
            );

            let applicationBasicInfo = SpecHelper.expectElement(elem, '[name="application_basic_info"]');
            SpecHelper.expectElementHasClass(applicationBasicInfo, 'incomplete');

            ProfileCompletionHelper.prototype.getStepsProgressMap.mockClear();
            ProfileCompletionHelper.prototype.getStepsProgressMap.mockReturnValue({
                basic_info: 'complete',
            });

            scope.setCohortApplicationProfileFormStepsAndSetCompletionPercentage(); // this is what updates the steps progress map for the career profile
            scope.$digest();

            expect(ProfileCompletionHelper.prototype.getStepsProgressMap).toHaveBeenCalledWith(
                scope.currentUser.career_profile,
            );
            applicationBasicInfo = SpecHelper.expectElement(elem, '[name="application_basic_info"]');
            SpecHelper.expectElementHasClass(applicationBasicInfo, 'complete');
        });

        it('should add a checkmark after the edit profile section name if section is complete', () => {
            // NOTE: the checkmark is present only if the complete class is applied.
            // See settings.html

            // ensure a required basic_info field is filled
            jest.spyOn(ProfileCompletionHelper.prototype, 'getStepsProgressMap').mockReturnValue({
                basic_info: 'complete',
            });

            render('application');

            SpecHelper.expectElementHasClass(elem.find('[name="application_basic_info"]'), 'complete');
        });

        it('should remove the page query parameter on navigation to another section', () => {
            render('application');
            jest.spyOn($location, 'search').mockImplementation(() => {});
            scope.section = 'account';
            scope.$digest();
            expect($location.search).toHaveBeenCalledWith('page', null);
        });

        it('should remove the page query parameter on navigation away from settings', () => {
            render('application');
            jest.spyOn($location, 'search').mockImplementation(() => {});
            scope.$destroy();
            expect($location.search).toHaveBeenCalledWith('page', null);
        });

        it('should handle attempting to navigate to non-existent page', () => {
            jest.spyOn($location, 'path');

            render('profile'); // hiring manager profile
            scope.$digest();

            expect($location.path).toHaveBeenCalledWith('/settings/account');
        });

        it('should handle attempting to navigate to application_status when application is visible', () => {
            jest.spyOn($location, 'path');
            render('application_status');
            scope.$digest();

            expect($location.path).toHaveBeenCalledWith('/settings/account');
        });

        describe('setCohortApplicationProfileFormStepsAndSetCompletionPercentage', () => {
            // spying on setCohortApplicationProfileFormStepsAndSetCompletionPercentage and then asserting that it got
            // called wasn't working for some reason, so I had to jest.spyOn the savedCareerProfile event listener
            // instead and assert that it really was the setCohortApplicationProfileFormStepsAndSetCompletionPercentage
            // function on the scope
            it('should be called on savedCareerProfile event emission', () => {
                render('profile');

                // this is the assertion that ties the savedCareerProfile event listener
                // to the setCohortApplicationProfileFormStepsAndSetCompletionPercentage function on the scope...
                expect(scope.setCohortApplicationProfileFormStepsAndSetCompletionPercentage).toBe(
                    scope.$$listeners.savedCareerProfile[0],
                );

                jest.spyOn(scope.$$listeners.savedCareerProfile, '0');
                scope.$emit('savedCareerProfile');

                // ...and here is the actual assertion that the same listener
                // (scope.setCohortApplicationProfileFormStepsAndSetCompletionPercentage) gets called
                expect(scope.$$listeners.savedCareerProfile[0]).toHaveBeenCalled();
            });

            it('should be called on savedCareerProfileWithStepChange event emission', () => {
                render('application');
                jest.spyOn(
                    scope,
                    'setCohortApplicationProfileFormStepsAndSetCompletionPercentage',
                ).mockImplementation(() => {});
                jest.spyOn(scope.$$listeners.savedCareerProfileWithStepChange, '0');
                scope.$emit('savedCareerProfileWithStepChange', scope.cohortApplicationProfileFormSteps[0].stepName);

                // ...and here is the actual assertion that the same listener
                // (scope.setCohortApplicationProfileFormStepsAndSetCompletionPercentage) gets called
                expect(scope.$$listeners.savedCareerProfileWithStepChange[0]).toHaveBeenCalled();
                expect(scope.setCohortApplicationProfileFormStepsAndSetCompletionPercentage).toHaveBeenCalled();
                expect(scope.initialStep).toBe(1); // we called the first stepName, so it should use the index of 0+1=1
            });

            it('should set process cohort application steps', () => {
                render('profile');

                const stepsForApplicationForm = [];
                const stepsProgressMap = {};
                jest.spyOn(EditCareerProfileHelper, 'getStepsForApplicationForm').mockReturnValue(
                    stepsForApplicationForm,
                );
                jest.spyOn(ProfileCompletionHelper.prototype, 'setSteps').mockReturnValue(stepsProgressMap);
                jest.spyOn(ProfileCompletionHelper.prototype, 'getStepsProgressMap').mockReturnValue(stepsProgressMap);

                scope.setCohortApplicationProfileFormStepsAndSetCompletionPercentage();

                expect(EditCareerProfileHelper.getStepsForApplicationForm).toHaveBeenCalledWith(scope.currentUser);
                expect(scope.cohortApplicationProfileFormSteps).toBe(stepsForApplicationForm);
                expect(ProfileCompletionHelper.prototype.setSteps).toHaveBeenCalledWith(
                    scope.cohortApplicationProfileFormSteps,
                );
                expect(ProfileCompletionHelper.prototype.getStepsProgressMap).toHaveBeenCalledWith(
                    scope.currentUser.career_profile,
                );

                // cohortApplicationProfileStepsProgressMap should reference whatever is returned by getStepsProgressMap
                expect(scope.cohortApplicationProfileStepsProgressMap).toBe(stepsProgressMap);
            });

            it('should set cohortApplicationPercentComplete', () => {
                render('profile');

                const percentComplete = 'foo';
                jest.spyOn(ProfileCompletionHelper.prototype, 'getPercentComplete').mockReturnValue(percentComplete);

                scope.setCohortApplicationProfileFormStepsAndSetCompletionPercentage();

                expect(scope.cohortApplicationPercentComplete).toEqual(percentComplete);
            });

            it('should be invoked immediately', () => {
                const stepsProgressMap = {};
                jest.spyOn(ProfileCompletionHelper.prototype, 'getStepsProgressMap').mockReturnValue(stepsProgressMap);

                render('profile');

                // cohortApplicationProfileStepsProgressMap should reference whatever is returned by getStepsProgressMap
                expect(scope.cohortApplicationProfileStepsProgressMap).toBe(stepsProgressMap);
            });
        });
    });

    describe('with a cohort application', () => {
        beforeEach(() => {
            jest.spyOn(currentUser, 'canApplyToSmartly', 'get').mockReturnValue(true);
            currentUser.relevant_cohort = Cohort.fixtures.getInstance();
            currentUser.cohort_applications = [CohortApplication.fixtures.getInstance()];
        });

        it('should render "Application Status" section instead of "Apply to Quantic" section when !can_convert_to_emba', () => {
            currentUser.lastCohortApplication.can_convert_to_emba = false;
            render();
            SpecHelper.expectElement(elem, '[name="application_status"]');
            SpecHelper.expectNoElement(elem, '[name="application"]');
        });

        it('should render "Apply to Quantic" section instead of "Application Status" section when can_convert_to_emba', () => {
            currentUser.lastCohortApplication.can_convert_to_emba = true;
            render();
            SpecHelper.expectElement(elem, '[name="application"]');
            SpecHelper.expectNoElement(elem, '[name="application_status"]');
        });

        it('should handle attempting to navigate to application when application_status is visible', () => {
            jest.spyOn($location, 'path');
            render('application');
            expect($location.path).toHaveBeenCalledWith('/settings/account');
        });

        it('should redirect to application page when attempting to navigate directly to application_status when can_convert_to_emba', () => {
            currentUser.lastCohortApplication.can_convert_to_emba = true;
            jest.spyOn($location, 'path');
            render('application_status');
            expect($location.path).toHaveBeenCalledWith('/settings/application');
        });

        it('should not include Documents section if cohort !supportsDocumentUpload', () => {
            jest.spyOn(Cohort, 'supportsDocumentUpload').mockReturnValue(false);
            render();
            expect(
                _.findWhere(scope.availableSections, {
                    name: 'documents',
                }),
            ).toBeUndefined();
            SpecHelper.expectNoElement(elem, '[name="documents"]');
        });

        it('should include Documents section if cohort supportsDocumentUpload', () => {
            jest.spyOn(Cohort, 'supportsDocumentUpload').mockReturnValue(true);
            render();
            expect(
                _.findWhere(scope.availableSections, {
                    name: 'documents',
                }),
            ).toBeDefined();
            SpecHelper.expectElement(elem, '[name="documents"]');
        });

        it('should add Documents section if fallback_program_type changes to cohort that supportsDocumentUpload', () => {
            let documentsStep;
            const supportsDocumentUploadSpy = jest.spyOn(Cohort, 'supportsDocumentUpload').mockReturnValue(false);
            currentUser.fallback_program_type = 'some_program_type';
            render();

            documentsStep = _.findWhere(scope.availableSections, {
                name: 'documents',
            });
            expect(documentsStep).toBeUndefined();

            supportsDocumentUploadSpy.mockReturnValue(true);
            currentUser.fallback_program_type = 'some_other_program_type';
            scope.$digest();
            documentsStep = _.findWhere(scope.availableSections, {
                name: 'documents',
            });
            expect(documentsStep).toBeDefined();
        });

        describe('setApplicationStatusHeaderViewModel', () => {
            let lastApplication;

            function assertViewModelState(state) {
                expect(scope.applicationStatusHeaderViewModel.header).toBe(state.header);
                expect(scope.applicationStatusHeaderViewModel.subheader).toBe(state.subheader);
                expect(scope.applicationStatusHeaderViewModel.showCheckmark).toBe(state.showCheckmark);
            }

            beforeEach(() => {
                lastApplication = CohortApplication.fixtures.getInstance();
                currentUser.cohort_applications = [lastApplication];
            });

            describe('supportsRecurringPayments cohort', () => {
                beforeEach(() => {
                    jest.spyOn(currentUser.relevant_cohort, 'supportsRecurringPayments', 'get').mockReturnValue('true');
                });

                describe('accepted', () => {
                    beforeEach(() => {
                        lastApplication.status = 'accepted';
                        Object.defineProperty(currentUser.relevant_cohort, 'registrationOpenDate', {
                            value: moment().subtract(1, 'day').toDate(),
                            configurable: true,
                        });
                    });

                    it('should set proper state when registered and in_good_standing and the cohort has a program guide', () => {
                        lastApplication.registered = true;
                        lastApplication.in_good_standing = true;
                        lastApplication.cohort_program_guide_url = 'http://foo.com';
                        render();
                        assertViewModelState({
                            header: 'You are now registered!',
                            subheader:
                                'You have secured your enrollment in the <a ng-click="launchProgramGuide()">Cohort Title 2</a> class.<br class="hidden-xs"> If you have any questions, please email us at <a href="mailto:emba@quantic.edu">emba@quantic.edu</a>.',
                            showCheckmark: true,
                        });
                    });

                    it('should set proper state when registered and in_good_standing and the cohort does not have a program guide', () => {
                        lastApplication.registered = true;
                        lastApplication.in_good_standing = true;
                        lastApplication.cohort_program_guide_url = undefined;
                        render();
                        assertViewModelState({
                            header: 'You are now registered!',
                            subheader:
                                'You have secured your enrollment in the Cohort Title 2 class.<br>If you have any questions, please email us at <a href="mailto:emba@quantic.edu">emba@quantic.edu</a>.',
                            showCheckmark: true,
                        });
                    });

                    it('should set proper state when locked_due_to_past_due_payment', () => {
                        lastApplication.locked_due_to_past_due_payment = true;
                        render();
                        assertViewModelState({
                            header: "Congratulations, you're in!",
                            subheader: '',
                            showCheckmark: true,
                        });
                    });

                    it('should set proper state when not registered and in_good_standing has become false', () => {
                        lastApplication.registered = false;
                        lastApplication.in_good_standing = false;
                        render();
                        assertViewModelState({
                            header: 'Welcome Back!',
                            subheader: 'Now join your classmates by securing your place in the Cohort Title 2 class.',
                            showCheckmark: false,
                        });
                    });

                    it('should set proper state when pre-accepted before registration opens', () => {
                        const dateHelper = $injector.get('dateHelper');
                        lastApplication.registered = false;
                        lastApplication.in_good_standing = false;
                        Object.defineProperty(currentUser.relevant_cohort, 'registrationOpenDate', {
                            value: moment().add(1, 'day').toDate(),
                        });
                        currentUser.relevant_cohort.start_date = moment().add(1, 'day').toDate().getTime();
                        render();
                        assertViewModelState({
                            header: 'Welcome Back!',
                            subheader: `Registration opens on <strong>${dateHelper.formattedUserFacingMonthDayYearLong(
                                currentUser.relevant_cohort.registrationOpenDate,
                                false,
                            )}</strong>.`,
                            showCheckmark: false,
                        });
                    });

                    it('should set proper state when not registered and in_good_standing is not set yet and cohort has a program guide', () => {
                        lastApplication.registered = false;
                        lastApplication.in_good_standing = undefined;
                        lastApplication.cohort_program_guide_url = 'http://foo.com';
                        render();
                        assertViewModelState({
                            header: "Congratulations, you're in!",
                            subheader:
                                'Now join your classmates by securing your place in the <a ng-click="launchProgramGuide()">Cohort Title 2</a> class.',
                            showCheckmark: true,
                        });
                    });

                    it('should set proper state when not registered and in_good_standing is not set yet and cohort does not have a program guide', () => {
                        lastApplication.registered = false;
                        lastApplication.in_good_standing = undefined;
                        render();
                        assertViewModelState({
                            header: "Congratulations, you're in!",
                            subheader: 'Now join your classmates by securing your place in the Cohort Title 2 class.',
                            showCheckmark: true,
                        });
                    });
                });

                describe('pre_accepted', () => {
                    beforeEach(() => {
                        lastApplication.status = 'pre_accepted';
                        Object.defineProperty(currentUser.relevant_cohort, 'registrationOpenDate', {
                            value: moment().subtract(1, 'day').toDate(),
                            configurable: true,
                        });
                    });

                    it('should set proper state when registered and in_good_standing and the cohort has a program guide', () => {
                        lastApplication.registered = true;
                        lastApplication.in_good_standing = true;
                        lastApplication.cohort_program_guide_url = 'http://foo.com';
                        render();
                        assertViewModelState({
                            header: 'You are now registered!',
                            subheader:
                                'You have secured your enrollment in the <a ng-click="launchProgramGuide()">Cohort Title 2</a> class.<br class="hidden-xs"> If you have any questions, please email us at <a href="mailto:emba@quantic.edu">emba@quantic.edu</a>.',
                            showCheckmark: true,
                        });
                    });

                    it('should set proper state when registered and in_good_standing and the cohort does not have a program guide', () => {
                        lastApplication.registered = true;
                        lastApplication.in_good_standing = true;
                        lastApplication.cohort_program_guide_url = undefined;
                        render();
                        assertViewModelState({
                            header: 'You are now registered!',
                            subheader:
                                'You have secured your enrollment in the Cohort Title 2 class.<br>If you have any questions, please email us at <a href="mailto:emba@quantic.edu">emba@quantic.edu</a>.',
                            showCheckmark: true,
                        });
                    });

                    it('should set proper state when locked_due_to_past_due_payment', () => {
                        lastApplication.locked_due_to_past_due_payment = true;
                        render();
                        assertViewModelState({
                            header: "Congratulations, you're in!",
                            subheader: '',
                            showCheckmark: true,
                        });
                    });

                    it('should set proper state when not registered and in_good_standing has become false', () => {
                        lastApplication.registered = false;
                        lastApplication.in_good_standing = false;
                        render();
                        assertViewModelState({
                            header: 'Welcome Back!',
                            subheader: 'Now join your classmates by securing your place in the Cohort Title 2 class.',
                            showCheckmark: false,
                        });
                    });

                    it('should set proper state when not registered and in_good_standing is not set yet and cohort has a program guide', () => {
                        lastApplication.registered = false;
                        lastApplication.in_good_standing = undefined;
                        lastApplication.cohort_program_guide_url = 'http://foo.com';
                        render();
                        assertViewModelState({
                            header: "Congratulations, you're in!",
                            subheader:
                                'Now join your classmates by securing your place in the <a ng-click="launchProgramGuide()">Cohort Title 2</a> class.',
                            showCheckmark: true,
                        });
                    });

                    it('should set proper state when not registered and in_good_standing is not set yet and cohort does not have a program guide', () => {
                        lastApplication.registered = false;
                        lastApplication.in_good_standing = undefined;
                        render();
                        assertViewModelState({
                            header: "Congratulations, you're in!",
                            subheader: 'Now join your classmates by securing your place in the Cohort Title 2 class.',
                            showCheckmark: true,
                        });
                    });
                });
            });

            it('should set proper state when failed', () => {
                lastApplication.status = 'accepted';
                jest.spyOn(currentUser, 'isFailed', 'get').mockReturnValue(true);
                render();
                assertViewModelState({
                    header: 'Graduation requirements not met',
                    subheader: '',
                    showCheckmark: false,
                });
            });

            it('should set proper state when pre_accepted, cohort does not supportsRecurringPayments, and cohort does supportsSchedule', () => {
                lastApplication.status = 'pre_accepted';
                jest.spyOn(currentUser.relevant_cohort, 'supportsRecurringPayments', 'get').mockReturnValue(false);
                jest.spyOn(currentUser.relevant_cohort, 'supportsSchedule', 'get').mockReturnValue(true);
                render();
                assertViewModelState({
                    header: 'Application Status',
                    subheader: `Class begins on ${moment(currentUser.relevant_cohort.startDate).format('LL')}`,
                    showCheckmark: false,
                });
            });

            it('should set proper state when accepted and cohort does not supportsRecurringPayments', () => {
                lastApplication.status = 'accepted';
                render();
                assertViewModelState({
                    header: 'Accepted to Cohort Title 2',
                    subheader: '',
                    showCheckmark: false,
                });
            });

            it('should set proper state when deferred', () => {
                lastApplication.status = 'deferred';
                render();
                assertViewModelState({
                    header: 'Deferred for Quantic MBA',
                    subheader: '',
                    showCheckmark: false,
                });
            });

            it('should show header, subheader, and checkbox when the state is set', () => {
                render();
                scope.section = 'application_status';
                scope.applicationStatusHeaderViewModel = {
                    header: 'foo header',
                    subheader: 'foo subheader',
                    showCheckmark: true,
                };
                scope.$digest();
                SpecHelper.expectElementText(elem, '[name="application-status-header"]', 'foo header');
                SpecHelper.expectElementText(elem, '.application-status-subheader', 'foo subheader');
                SpecHelper.expectElement(elem, '.congrats-circle');
            });
        });
    });

    describe('with a hiring application', () => {
        beforeEach(() => {
            currentUser.hiring_application = HiringApplication.fixtures.getInstance();
            Object.defineProperty(currentUser, 'defaultsToHiringExperience', {
                value: true,
            });
            currentUser.fallback_program_type = 'demo';
        });

        it('should have working sub-nav', () => {
            render('profile');
            jest.spyOn($location, 'url').mockImplementation(() => {});
            SpecHelper.click(elem, '.responsive-nav .nav-2-item:eq(1)');
            expect($location.url).toHaveBeenCalledWith('/settings/profile?page=2');
        });

        it('should set mobile nav state on sub-nav item click', () => {
            render('profile');
            SpecHelper.click(elem, '.responsive-nav .nav-2-item:eq(2)');
            expect(scope.mobileNavExpanded).toBe(false);
        });

        it('should show progress nav', () => {
            jest.spyOn(currentUser.hiring_application, 'percentComplete').mockReturnValue(42);
            render('profile');
            const progressNav = SpecHelper.expectElement(elem, '.main-box progress-nav');
            expect(progressNav.find('.percentage .circle').text()).toBe('42%');
        });

        it('should add a blue dot after the edit profile section name if section is incomplete', () => {
            // NOTE: the blue dot is present only if the incomplete class is applied.
            // See settings.html

            render('profile');
            expect(currentUser.hiring_application.percentComplete()).toBeLessThan(100);
            const els = SpecHelper.expectElements(elem, '.nav-2-item.incomplete');
            let profile_you = SpecHelper.expectElement(elem, '[name="profile_you"]');
            SpecHelper.expectElementHasClass(profile_you, 'incomplete');
            SpecHelper.click(elem, '[name="profile_you"]');

            // update the personal_summary section to make it complete and save the career profile
            currentUser.hiring_application.phone = '+12345678';
            currentUser.hiring_application.nickname = 'Bossmane';
            HiringApplication.expect('update');
            currentUser.hiring_application.save();
            HiringApplication.flush('update');

            // check that the edit career profile section doesn't have the incomplete class
            render('profile');
            profile_you = SpecHelper.expectElement(elem, '[name="profile_you"]');
            SpecHelper.expectElementDoesNotHaveClass(profile_you, 'incomplete');
            SpecHelper.expectElements(elem, '.nav-2-item.incomplete', els.length - 1);
        });

        it('should remove the page query parameter on navigation to another section', () => {
            render('profile');
            jest.spyOn($location, 'search').mockImplementation(() => {});
            scope.section = 'account';
            scope.$digest();
            expect($location.search).toHaveBeenCalledWith('page', null);
        });

        it('should remove the page query parameter on navigation away from settings', () => {
            render('profile');
            jest.spyOn($location, 'search').mockImplementation(() => {});
            scope.$destroy();
            expect($location.search).toHaveBeenCalledWith('page', null);
        });

        it('should handle attempting to navigate to non-existent page', () => {
            jest.spyOn($location, 'path');

            render('application');
            scope.$digest();

            expect($location.path).toHaveBeenCalledWith('/settings/account');
        });

        describe('billing', () => {
            it('should add billing section if necessary', () => {
                const HiringTeam = $injector.get('HiringTeam');
                $injector.get('HiringTeamFixtures');

                // When no team, we show it
                currentUser.hiring_team = null;
                scope.$digest();
                render('profile');
                expect(_.chain(scope.availableSections).pluck('name').contains('billing').value()).toBe(true);

                // When team has subscription_required, we show it
                currentUser.hiring_team = HiringTeam.fixtures.getInstance({
                    subscription_required: true,
                });
                scope.$digest();
                render('profile');
                expect(_.chain(scope.availableSections).pluck('name').contains('billing').value()).toBe(true);

                // When team does not have subscription_required, we hide it
                currentUser.hiring_team = HiringTeam.fixtures.getInstance({
                    subscription_required: false,
                });
                scope.$digest();
                render('profile');
                expect(_.chain(scope.availableSections).pluck('name').contains('billing').value()).toBe(false);
            });

            it('should show billing if requiresHiringSubscription', () => {
                jest.spyOn(currentUser, 'requiresHiringSubscription', 'get').mockReturnValue(true);
                render('profile');
                expect(_.chain(scope.availableSections).pluck('name').contains('billing').value()).toBe(true);
            });
        });

        describe('setHiringProfileStepsProgressMap', () => {
            // spying on setHiringProfileStepsProgressMap and then asserting that it got called wasn't working
            // for some reason, so I had to jest.spyOn the savedHiringManagerProfile event listener instead and assert
            // that it really was the setHiringProfileStepsProgressMap function on the scope
            it('should be called on savedHiringManagerProfile event emission', () => {
                render('profile');

                // this is the assertion that ties the savedHiringManagerProfile event listener
                // to the setHiringProfileStepsProgressMap function on the scope...
                expect(scope.setHiringProfileStepsProgressMap).toBe(scope.$$listeners.savedHiringManagerProfile[0]);

                jest.spyOn(scope.$$listeners.savedHiringManagerProfile, '0');
                scope.$emit('savedHiringManagerProfile');

                // ...and here is the actual assertion that the same listener
                // (scope.setHiringProfileStepsProgressMap) gets called
                expect(scope.$$listeners.savedHiringManagerProfile[0]).toHaveBeenCalled();
            });

            it('should set hiringProfileStepsProgressMap', () => {
                render('profile');

                const stepsProgressMap = {};
                jest.spyOn(scope.currentUser.hiring_application, 'getStepsProgressMap').mockReturnValue(
                    stepsProgressMap,
                );

                scope.setHiringProfileStepsProgressMap();

                expect(scope.currentUser.hiring_application.getStepsProgressMap).toHaveBeenCalled();

                // hiringProfileStepsProgressMap should reference whatever is returned by getStepsProgressMap
                expect(scope.hiringProfileStepsProgressMap).toBe(stepsProgressMap);
            });

            it('should be invoked immediately', () => {
                const stepsProgressMap = {};
                jest.spyOn(HiringApplication.prototype, 'getStepsProgressMap').mockReturnValue(stepsProgressMap);

                render('profile');

                // hiringProfileStepsProgressMap should reference whatever is returned by getStepsProgressMap
                expect(scope.hiringProfileStepsProgressMap).toBe(stepsProgressMap);
            });
        });
    });

    describe('showPreviewProfileStatusMessage', () => {
        it('should be true if section if my-profile and user !can_edit_career_profile', () => {
            jest.spyOn(currentUser, 'lastCohortApplicationStatus', 'get').mockReturnValue('pending'); // prevent false positive
            currentUser.can_edit_career_profile = false;

            render('my-profile');
            expect(scope.section).toEqual('my-profile'); // sanity check
            expect(scope.showPreviewProfileStatusMessage).toBe(true);

            scope.currentUser.can_edit_career_profile = true;
            expect(scope.showPreviewProfileStatusMessage).toBe(false);

            scope.currentUser.can_edit_career_profile = false;
            scope.section = 'application_status';
            expect(scope.showPreviewProfileStatusMessage).toBe(false);
        });

        // See CohortApplication::VALID_ENROLLMENT_STATUSES
        _.each(['pre_accepted', 'accepted'], status => {
            it(`should be true if section is my-profile and user is ${status} to a degreeProgram with an incomplete profile`, () => {
                currentUser.can_edit_career_profile = true; // prevent false positive
                const lastCohortApplicationStatusSpy = jest
                    .spyOn(currentUser, 'lastCohortApplicationStatus', 'get')
                    .mockReturnValue(status);
                jest.spyOn(Cohort, 'isDegreeProgram').mockReturnValue(true);
                currentUser.career_profile.last_calculated_complete_percentage = 99;

                render('my-profile');
                expect(scope.section).toEqual('my-profile'); // sanity check
                expect(scope.showPreviewProfileStatusMessage).toBe(true);

                scope.currentUser.career_profile.last_calculated_complete_percentage = 100;
                expect(scope.showPreviewProfileStatusMessage).toBe(false);

                scope.currentUser.career_profile.last_calculated_complete_percentage = 99;
                Cohort.isDegreeProgram.mockReturnValue(false);
                expect(scope.showPreviewProfileStatusMessage).toBe(false);

                Cohort.isDegreeProgram.mockReturnValue(true);
                lastCohortApplicationStatusSpy.mockReturnValue('pending');
                expect(scope.showPreviewProfileStatusMessage).toBe(false);

                lastCohortApplicationStatusSpy.mockReturnValue(status);
                scope.section = 'application_status';
                expect(scope.showPreviewProfileStatusMessage).toBe(false);
            });
        });
    });

    describe('preview-profile-status-message', () => {
        it('should only be rendered if showPreviewProfileStatusMessage', () => {
            render();
            const spy = jest.spyOn(scope, 'showPreviewProfileStatusMessage', 'get').mockReturnValue(false);
            scope.$digest();
            SpecHelper.expectNoElement(elem, 'preview-profile-status-message');

            spy.mockReturnValue(true);
            scope.$digest();
            SpecHelper.expectElement(elem, 'preview-profile-status-message');
        });
    });

    describe('my-profile', () => {
        describe('progress-nav', () => {
            it('should render properly when editing', () => {
                render('my-profile');

                SpecHelper.expectElement(elem, 'progress-nav');
                elem.find('progress-nav').isolateScope();
            });

            it('should not be rendered when not editing', () => {
                render('application_status');
                SpecHelper.expectNoElement(elem, 'progress-nav');
            });
        });
    });

    describe('privacy-terms-footer', () => {
        let $window;

        beforeEach(() => {
            $window = $injector.get('$window');
        });

        afterEach(() => {
            $window.CORDOVA = undefined;
        });

        it('should display a privacy-terms-footer on the responsive navigation when in Cordova', () => {
            $window.CORDOVA = true;
            render();
            SpecHelper.expectElement(elem, '.responsive-nav .footer privacy-terms-footer');
        });

        it('should not display a privacy-terms-footer on the responsive navigation when in Cordova', () => {
            render();
            SpecHelper.expectNoElement(elem, '.responsive-nav privacy-terms-footer');
        });

        it('should display a privacy-terms-footer under the main box when in Cordova and on the account section', () => {
            $window.CORDOVA = true;
            render();
            scope.section = 'profile';
            scope.$digest();
            SpecHelper.expectNoElement(elem, '.main-box-footer privacy-terms-footer');
            scope.section = 'account';
            scope.$digest();
            SpecHelper.expectElement(elem, '.main-box-footer privacy-terms-footer');
        });

        it('should not display a privacy-terms-footer on the responsive navigation when not in Cordova', () => {
            render();
            scope.section = 'profile';
            scope.$digest();
            SpecHelper.expectNoElement(elem, '.main-box-footer privacy-terms-footer');
            scope.section = 'account';
            scope.$digest();
            SpecHelper.expectNoElement(elem, '.main-box-footer privacy-terms-footer');
        });
    });

    // describe('transcripts', function() {
    //     it('should show transcript section if allowTranscriptDownload', function() {
    //         throw 'WRITE ME';
    //     });
    // });
});
