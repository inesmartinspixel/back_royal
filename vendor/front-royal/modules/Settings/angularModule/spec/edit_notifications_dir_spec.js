import 'AngularSpecHelper';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import 'Settings/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import editNotificationsLocales from 'Settings/locales/settings/edit_notifications-en.json';

setSpecLocales(editNotificationsLocales);

describe('Settings::EditNotifications', () => {
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let $injector;
    let ngToast;
    let $rootScope;
    let $q;
    let DialogModal;
    let $window;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Settings', 'SpecHelper');

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            SpecHelper = $injector.get('SpecHelper');
            ngToast = $injector.get('ngToast');
            $rootScope = $injector.get('$rootScope');
            $q = $injector.get('$q');
            DialogModal = $injector.get('DialogModal');
            $window = $injector.get('$window');
        });

        SpecHelper.stubConfig();
        SpecHelper.stubCurrentUser('learner');
    });

    describe('form submission', () => {
        let deferred;

        beforeEach(() => {
            render();
        });

        it('should handle successes during update, using the notification preference title if supplied', () => {
            // success without notification preference title
            deferred = $q.defer();
            jest.spyOn(ngToast, 'create').mockImplementation(() => {});
            jest.spyOn($rootScope.currentUser, 'save').mockReturnValue(deferred.promise);
            SpecHelper.click(elem, 'button[name="button-notify_email_newsletter-enable"]:eq(0)');
            deferred.resolve();
            scope.$digest();
            expect(ngToast.create).toHaveBeenCalledWith({
                content: "'Quantic Updates' subscription successfully updated.",
                className: 'success',
            });

            // success with notification preference title
            const notificationConfig = scope.notificationConfigs.find(config =>
                _.pluck(config.notificationPreferences, 'key').includes('notify_email_newsletter'),
            );
            const notificationPreference = notificationConfig.notificationPreferences.find(
                pref => pref.key === 'notify_email_newsletter',
            );
            notificationPreference.title = 'notify_email_newsletter_miya_miya';
            deferred = $q.defer();
            SpecHelper.click(elem, 'button[name="button-notify_email_newsletter-enable"]:eq(0)');
            deferred.resolve();
            scope.$digest();
            expect(ngToast.create).toHaveBeenCalledWith({
                content: "'Miya Miya Updates' subscription successfully updated.",
                className: 'success',
            });
        });
    });

    describe('notify_email_newsletter', () => {
        describe('when isMiyaMiyaMi', () => {
            beforeEach(() => {
                jest.spyOn($rootScope.currentUser, 'isMiyaMiya', 'get').mockReturnValue(true);
            });

            it('should have contextual messaging', () => {
                render();
                SpecHelper.expectElementText(elem, '.form-group .control-title', 'Miya Miya Updates');
                SpecHelper.expectElementText(
                    elem,
                    '.split-group.left .sub-text',
                    'I would like to receive occasional news updates about Miya Miya.',
                );
            });
        });

        describe('when !isMiyaMiya', () => {
            beforeEach(() => {
                jest.spyOn($rootScope.currentUser, 'isMiyaMiya', 'get').mockReturnValue(false);
            });

            it('should have contextual messaging', () => {
                render();
                SpecHelper.expectElementText(elem, '.form-group .control-title', 'Quantic Updates');
                SpecHelper.expectElementText(
                    elem,
                    '.split-group.left .sub-text',
                    'I would like to receive updates about new course offerings and occasional news from Quantic. Note: if you have applied to Quantic and opt not to receive Quantic Updates, you will still receive a notification with your admission decision.',
                );
            });
        });
    });

    describe('learners without career network access', () => {
        it('should only see the newsletter option', () => {
            render();
            SpecHelper.expectElements(elem, '.btn-group', 1);
        });
    });

    describe('learners on Cordova builds', () => {
        let isMiyaMiyaSpy;

        describe('pref_allow_push_notificaitons', () => {
            beforeEach(() => {
                $window.CORDOVA = true;
                $window.facebookConnectPlugin = {};
                isMiyaMiyaSpy = jest.spyOn($rootScope.currentUser, 'isMiyaMiya', 'get').mockReturnValue(false);
            });
            afterEach(() => {
                $window.CORDOVA = undefined;
                $window.facebookConnectPlugin = undefined;
            });
            it('should only show when $window.CORDOVA and not isMiyaMiya', () => {
                render();
                SpecHelper.expectElements(elem, '.btn-group', 2);
                SpecHelper.expectElements(elem, 'button[name="button-pref_allow_push_notifications-enable"]', 2);
                SpecHelper.expectElements(elem, 'button[name="button-pref_allow_push_notifications-disable"]', 2);

                isMiyaMiyaSpy.mockReturnValue(true);
                render();
                SpecHelper.expectNoElement(elem, 'button[name="button-pref_allow_push_notifications-enable"]');
                SpecHelper.expectNoElement(elem, 'button[name="button-pref_allow_push_notifications-disable"]');

                isMiyaMiyaSpy.mockReturnValue(false);
                render();
                SpecHelper.expectElements(elem, '.btn-group', 2);
                SpecHelper.expectElements(elem, 'button[name="button-pref_allow_push_notifications-enable"]', 2);
                SpecHelper.expectElements(elem, 'button[name="button-pref_allow_push_notifications-disable"]', 2);

                $window.CORDOVA = undefined;
                $window.facebookConnectPlugin = undefined;
                render();
                SpecHelper.expectNoElement(elem, 'button[name="button-pref_allow_push_notifications-enable"]');
                SpecHelper.expectNoElement(elem, 'button[name="button-pref_allow_push_notifications-disable"]');
            });
            it('should toggle value and save', () => {
                $rootScope.currentUser.pref_allow_push_notifications = true;
                jest.spyOn($rootScope.currentUser, 'save').mockReturnValue($q.when());
                render();

                SpecHelper.expectElementText(elem, '.control-title', 'Push Notifications', 1); // sanity check

                // disable it
                SpecHelper.click(elem, 'button[name="button-pref_allow_push_notifications-disable"]:eq(0)');
                expect($rootScope.currentUser.save).toHaveBeenCalled();
                expect($rootScope.currentUser.pref_allow_push_notifications).toEqual(false);

                // enable it
                $rootScope.currentUser.save.mockClear();
                SpecHelper.click(elem, 'button[name="button-pref_allow_push_notifications-enable"]:eq(0)');
                expect($rootScope.currentUser.save).toHaveBeenCalled();
                expect($rootScope.currentUser.pref_allow_push_notifications).toEqual(true);
            });
        });
    });

    describe('learners with career network access', () => {
        beforeEach(() => {
            const CareerProfile = $injector.get('CareerProfile');
            $injector.get('CareerProfileFixtures');
            $rootScope.currentUser.career_profile = CareerProfile.fixtures.getInstance();

            jest.spyOn($rootScope.currentUser, 'hasCareersNetworkAccess', 'get').mockReturnValue(true);
        });

        it('should see expected options', () => {
            render();
            SpecHelper.expectElements(elem, '.btn-group', 4);
        });

        describe('simple preferences', () => {
            // 'simple preferences' as in ones that are just columns on the user, not special
            // logic in career profile like notify_employer_communication_updates is

            it('should toggle notify_candidate_positions value and save', () => {
                $rootScope.currentUser.notify_candidate_positions = true;
                jest.spyOn($rootScope.currentUser, 'save').mockReturnValue($q.when());
                render();

                SpecHelper.expectElementText(elem, '.control-title', 'Career Network Updates', 2); // sanity check

                // set to never
                SpecHelper.click(elem, 'button[name="button-notify_candidate_positions-never"]:eq(0)');
                expect($rootScope.currentUser.save).toHaveBeenCalled();
                expect($rootScope.currentUser.notify_candidate_positions).toEqual(false);

                // set to weekly
                $rootScope.currentUser.save.mockClear();
                SpecHelper.click(elem, 'button[name="button-notify_candidate_positions-weekly"]:eq(0)');
                expect($rootScope.currentUser.save).toHaveBeenCalled();
                expect($rootScope.currentUser.notify_candidate_positions).toEqual(true);
            });

            it('should toggle notify_candidate_positions_recommended value and save', () => {
                $rootScope.currentUser.notify_candidate_positions_recommended = 'daily';
                jest.spyOn($rootScope.currentUser, 'save').mockReturnValue($q.when());
                render();

                // set to never
                SpecHelper.click(elem, 'button[name="button-notify_candidate_positions_recommended-never"]:eq(0)');
                expect($rootScope.currentUser.save).toHaveBeenCalled();
                expect($rootScope.currentUser.notify_candidate_positions_recommended).toEqual('never');

                // set to bi_weekly
                $rootScope.currentUser.save.mockClear();
                SpecHelper.click(elem, 'button[name="button-notify_candidate_positions_recommended-bi_weekly"]:eq(0)');
                expect($rootScope.currentUser.save).toHaveBeenCalled();
                expect($rootScope.currentUser.notify_candidate_positions_recommended).toEqual('bi_weekly');

                // set to daily
                $rootScope.currentUser.save.mockClear();
                SpecHelper.click(elem, 'button[name="button-notify_candidate_positions_recommended-daily"]:eq(0)');
                expect($rootScope.currentUser.save).toHaveBeenCalled();
                expect($rootScope.currentUser.notify_candidate_positions_recommended).toEqual('daily');
            });
        });

        describe('notify_employer_communication_updates', () => {
            it('should set interest level back to neutral when turning back on', () => {
                $rootScope.currentUser.career_profile.interested_in_joining_new_company = 'not_interested';
                jest.spyOn($rootScope.currentUser.career_profile, 'save').mockReturnValue($q.when());
                jest.spyOn(DialogModal, 'confirm').mockImplementation(() => {});
                render();

                SpecHelper.expectElementText(elem, '.control-title', 'Employer Communications', 1); // sanity check
                SpecHelper.click(elem, 'button[name="button-notify_employer_communication_updates-enable"]:eq(0)');

                expect(DialogModal.confirm).toHaveBeenCalledWith({
                    showThanks: false,
                    text:
                        'Enabling employer communications will make your profile visible to employers in the Career Network.',
                    confirmCallback: expect.anything(),
                    cancelCallback: expect.anything(),
                });
                expect($rootScope.currentUser.career_profile.save).not.toHaveBeenCalled();
                DialogModal.confirm.mock.calls[0][0].confirmCallback();
                expect($rootScope.currentUser.career_profile.save).toHaveBeenCalled();
                expect($rootScope.currentUser.career_profile.interested_in_joining_new_company).toEqual('neutral');
            });

            it('should do nothing if not confirming notify_employer_communication_updates on', () => {
                $rootScope.currentUser.career_profile.interested_in_joining_new_company = 'not_interested';
                jest.spyOn($rootScope.currentUser.career_profile, 'save').mockReturnValue($q.when());
                jest.spyOn(DialogModal, 'confirm').mockImplementation(() => {});
                render();

                SpecHelper.expectElementText(elem, '.control-title', 'Employer Communications', 1); // sanity check
                SpecHelper.click(elem, 'button[name="button-notify_employer_communication_updates-enable"]:eq(0)');

                expect(DialogModal.confirm).toHaveBeenCalled();
                expect($rootScope.currentUser.career_profile.save).not.toHaveBeenCalled();
                DialogModal.confirm.mock.calls[0][0].cancelCallback();
                expect($rootScope.currentUser.career_profile.save).not.toHaveBeenCalled();
                expect($rootScope.currentUser.career_profile.interested_in_joining_new_company).toEqual(
                    'not_interested',
                );
            });

            it('should set career profile to inactive after confirming notify_employer_communication_updates off', () => {
                $rootScope.currentUser.career_profile.interested_in_joining_new_company = 'neutral';
                jest.spyOn($rootScope.currentUser.career_profile, 'save').mockReturnValue($q.when());
                jest.spyOn(DialogModal, 'confirm').mockImplementation(() => {});
                render();

                SpecHelper.expectElementText(elem, '.control-title', 'Employer Communications', 1); // sanity check
                SpecHelper.click(elem, 'button[name="button-notify_employer_communication_updates-disable"]:eq(0)');

                expect(DialogModal.confirm).toHaveBeenCalledWith({
                    showThanks: false,
                    text: 'By opting out of employer communications your profile will be made inactive.',
                    confirmCallback: expect.anything(),
                    cancelCallback: expect.anything(),
                });
                expect($rootScope.currentUser.career_profile.save).not.toHaveBeenCalled();
                DialogModal.confirm.mock.calls[0][0].confirmCallback();
                expect($rootScope.currentUser.career_profile.save).toHaveBeenCalled();
                expect($rootScope.currentUser.career_profile.interested_in_joining_new_company).toEqual(
                    'not_interested',
                );
            });

            it('should do nothing if not confirming notify_employer_communication_updates off', () => {
                $rootScope.currentUser.career_profile.interested_in_joining_new_company = 'neutral';
                jest.spyOn($rootScope.currentUser.career_profile, 'save').mockReturnValue($q.when());
                jest.spyOn(DialogModal, 'confirm').mockImplementation(() => {});
                render();

                SpecHelper.expectElementText(elem, '.control-title', 'Employer Communications', 1); // sanity check
                SpecHelper.click(elem, 'button[name="button-notify_employer_communication_updates-disable"]:eq(0)');

                expect(DialogModal.confirm).toHaveBeenCalled();
                expect($rootScope.currentUser.career_profile.save).not.toHaveBeenCalled();
                DialogModal.confirm.mock.calls[0][0].cancelCallback();
                expect($rootScope.currentUser.career_profile.save).not.toHaveBeenCalled();
                expect($rootScope.currentUser.career_profile.interested_in_joining_new_company).toEqual('neutral');
            });
        });
    });

    describe('hiring managers', () => {
        beforeEach(() => {
            jest.spyOn($rootScope.currentUser, 'defaultsToHiringExperience', 'get').mockReturnValue(true);
        });

        it('should see several notification options', () => {
            render();
            SpecHelper.expectElements(elem, 'input[type="checkbox"]', 5);
        });

        describe('notify_hiring_candidate_activity', () => {
            let promise;

            beforeEach(() => {
                promise = $q.when();
            });

            it('should save without confirmation if toggled on', () => {
                jest.spyOn($rootScope.currentUser, 'save').mockReturnValue(promise);
                jest.spyOn(DialogModal, 'confirm').mockImplementation(() => {});
                render();

                SpecHelper.expectElementText(elem, '.control-title', 'Candidate Activity', 2); // sanity check
                SpecHelper.checkCheckbox(elem, 'input[type="checkbox"]', 2);

                expect(DialogModal.confirm).not.toHaveBeenCalled();
                expect($rootScope.currentUser.save).toHaveBeenCalled();
            });

            it('should ask for confirmation if toggling off', () => {
                $rootScope.currentUser.notify_hiring_candidate_activity = true;
                jest.spyOn($rootScope.currentUser, 'save').mockReturnValue(promise);
                jest.spyOn(DialogModal, 'confirm').mockImplementation(() => {});
                render();

                SpecHelper.expectElementText(elem, '.control-title', 'Candidate Activity', 2); // sanity check
                SpecHelper.uncheckCheckbox(elem, 'input[type="checkbox"]', 2);

                expect(DialogModal.confirm).toHaveBeenCalledWith({
                    showThanks: true,
                    text:
                        'Are you sure you no longer want to receive notifications when you connect with or receive messages from Candidates?',
                    confirmCallback: expect.anything(),
                    cancelCallback: expect.anything(),
                });
                expect($rootScope.currentUser.save).not.toHaveBeenCalled();
                DialogModal.confirm.mock.calls[0][0].confirmCallback();
                expect($rootScope.currentUser.save).toHaveBeenCalled();
            });
        });

        describe('notify_hiring_spotlights', () => {
            let candidateSpotlightsSection;

            beforeEach(() => {
                render();
                candidateSpotlightsSection = SpecHelper.expectElement(elem, '.checkbox-group:last-of-type');
                SpecHelper.expectElementText(candidateSpotlightsSection, ' > .control-title', 'Candidate Spotlights'); // sanity check
            });

            it('should have a checkbox for notify_hiring_spotlights_business', () => {
                // assert the checkbox descriptions text
                SpecHelper.expectElementText(
                    candidateSpotlightsSection,
                    'input[type="checkbox"][id="notify_hiring_spotlights_business"] + .checkbox-group-label-wrapper .sub-text',
                    'I would like to receive weekly emails highlighting new business talent (e.g., business analysis, consulting/strategy, finance, human resources, management, marketing, operations, sales).',
                );
            });

            it('shoudl have a checkbox for notify_hiring_spotlights_technical', () => {
                // assert the checkbox descriptions text
                SpecHelper.expectElementText(
                    candidateSpotlightsSection,
                    'input[type="checkbox"][id="notify_hiring_spotlights_technical"] + .checkbox-group-label-wrapper .sub-text',
                    'I would like to receive weekly emails highlighting new technical talent (e.g., software developers, data scientists, product managers, designers, engineering talent).',
                );
            });
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.render('<edit-notifications></edit-notifications>');
        elem = renderer.elem;
        scope = elem.isolateScope();
    }
});
