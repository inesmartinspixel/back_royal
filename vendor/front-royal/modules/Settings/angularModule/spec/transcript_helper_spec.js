import 'AngularSpecHelper';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohort_applications';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';
import 'Settings/angularModule';

import * as userAgentHelper from 'userAgentHelper';
import moment from 'moment-timezone';

describe('Settings.TranscriptHelper', () => {
    let $injector;
    let SpecHelper;
    let $rootScope;
    let TranscriptHelper;
    let transcriptHelper;
    let $window;
    let Cohort;
    let CohortApplication;
    let User;
    let user;
    let Stream;
    let StreamProgress;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Settings', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                $rootScope = $injector.get('$rootScope');
                $window = $injector.get('$window');
                TranscriptHelper = $injector.get('TranscriptHelper');
                Cohort = $injector.get('Cohort');
                CohortApplication = $injector.get('CohortApplication');
                User = $injector.get('User');
                Stream = $injector.get('Lesson.Stream');
                StreamProgress = $injector.get('Lesson.StreamProgress');

                $injector.get('CohortFixtures');
                $injector.get('CohortApplicationFixtures');
            },
        ]);

        transcriptHelper = new TranscriptHelper();
        user = SpecHelper.stubCurrentUser();
    });

    describe('allowTranscriptDownload', () => {
        describe('on mobile', () => {
            it('should return false if on Corodva', () => {
                $window.CORDOVA = true;
                expect(transcriptHelper.allowTranscriptDownload(user)).toBe(false);
            });

            it('should return false if on iOS', () => {
                $window.CORDOVA = false;
                jest.spyOn(userAgentHelper, 'isiOSDevice').mockReturnValue(true);
                expect(transcriptHelper.allowTranscriptDownload(user)).toBe(false);
                expect(userAgentHelper.isiOSDevice).toHaveBeenCalled();
            });
        });

        describe('on desktop', () => {
            beforeEach(() => {
                $window.CORDOVA = false;
                jest.spyOn(userAgentHelper, 'isiOSDevice').mockReturnValue(false);
            });

            it('should return true user canDownloadTranscripts', () => {
                jest.spyOn(user, 'canDownloadTranscripts', 'get').mockReturnValue(true);
                expect(transcriptHelper.allowTranscriptDownload(user)).toBe(true);
            });

            it('should return false if user !canDownloadTranscripts', () => {
                jest.spyOn(user, 'canDownloadTranscripts', 'get').mockReturnValue(false);
                expect(transcriptHelper.allowTranscriptDownload(user)).toBe(false);
            });
        });
    });

    describe('downloadTranscript', () => {
        it('should make a show call if asking for a different user than the currentUser', () => {
            const adminUser = User.fixtures.getInstance({
                id: 1,
                name: 'Admin McTest',
            });
            const anotherUser = User.fixtures.getInstance({
                id: 2,
                name: 'Another McUser',
            });
            $rootScope.currentUser = adminUser;
            jest.spyOn(transcriptHelper, 'allowTranscriptDownload').mockReturnValue(true);
            User.expect('show').toBeCalledWith(2);
            transcriptHelper.downloadTranscript(anotherUser);
        });
    });

    describe('_getGraduationDateText', () => {
        it('should be N/A if deferred', () => {
            user.cohort_applications = [
                CohortApplication.fixtures.getInstance({
                    status: 'deferred',
                }),
            ];
            user.relevant_cohort = Cohort.fixtures.getInstance();

            expect(transcriptHelper._getGraduationDateText(user)).toEqual('N/A');
        });

        it('should be N/A if failed', () => {
            user.cohort_applications = [
                CohortApplication.fixtures.getInstance({
                    graduation_status: 'failed',
                }),
            ];
            user.relevant_cohort = Cohort.fixtures.getInstance();
            expect(transcriptHelper._getGraduationDateText(user)).toEqual('N/A');
        });

        it('should be N/A if relevant_cohort does not support schedule (ex. biz cert)', () => {
            user.cohort_applications = [CohortApplication.fixtures.getInstance()];
            user.relevant_cohort = Cohort.fixtures.getInstance();
            jest.spyOn(user.relevant_cohort, 'supportsSchedule', 'get').mockReturnValue(false);
            expect(transcriptHelper._getGraduationDateText(user)).toEqual('N/A');
        });

        describe('when not deferred or failed', () => {
            it("should use the formatted graduationDate on the user's relevant_cohort", () => {
                user.cohort_applications = [
                    CohortApplication.fixtures.getInstance({
                        status: 'not_deferred_or_failed',
                    }),
                ];
                user.relevant_cohort = Cohort.fixtures.getInstance();
                expect(transcriptHelper._getGraduationDateText(user)).toEqual(
                    moment(user.relevant_cohort.graduationDate).format('MMMM D, YYYY'),
                );
            });
        });
    });

    describe('_getStreamTranscriptEntries', () => {
        it('should filter out some courses by name', () => {
            const streams = [
                Stream.fixtures.getInstance({
                    title: 'Accounting Exam Practice',
                }),
                Stream.fixtures.getInstance({
                    title: 'Coffee Break I',
                }),
                Stream.fixtures.getInstance({
                    title: 'Photography Basics',
                }),
                Stream.fixtures.getInstance({
                    title: 'Why Blended Learning Matters',
                }),
                Stream.fixtures.getInstance({
                    title: 'M&A I: Theory and Practice',
                }),
            ];
            streams.forEach(stream => {
                jest.spyOn(stream, 'complete', 'get').mockReturnValue(true);
            });
            const entries = transcriptHelper._getStreamTranscriptEntries(streams, false);
            expect(entries).toEqual([
                {
                    title: 'M&A I: Theory and Practice',
                },
            ]);
        });

        it('should filter out courses that have an EXAM_ALREADY_COMPLETED waiver', () => {
            const waivedStream = Stream.fixtures.getInstance();
            jest.spyOn(waivedStream, 'progressWaiver', 'get').mockReturnValue(
                StreamProgress.WAIVER_EXAM_ALREADY_COMPLETED,
            );

            const streams = [
                waivedStream,
                Stream.fixtures.getInstance({
                    title: 'M&A I: Theory and Practice',
                }),
            ];
            streams.forEach(stream => {
                jest.spyOn(stream, 'complete', 'get').mockReturnValue(true);
            });

            const entries = transcriptHelper._getStreamTranscriptEntries(streams, false);
            expect(entries).toEqual([
                {
                    title: 'M&A I: Theory and Practice',
                },
            ]);
        });
    });

    it('should format grades correctly', () => {
        expect(transcriptHelper._formatGrade('0.99', false)).toBe('99%');
        expect(transcriptHelper._formatGrade('0.99', true)).toBe(' - 99%');
        expect(transcriptHelper._formatGrade(null, false)).toBe('P');
        expect(transcriptHelper._formatGrade(null, true)).toBe(' - P');
    });

    it('should format average test score correctly', () => {
        expect(transcriptHelper._formatAvgTestScore('0.99', false)).toBe('99%');
        expect(transcriptHelper._formatAvgTestScore('0.99', true)).toBe(' - 99%');
        expect(transcriptHelper._formatAvgTestScore(null, false)).toBe('N/A');
        expect(transcriptHelper._formatAvgTestScore(null, true)).toBe(' - N/A');
    });
});
