import 'AngularSpecHelper';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';
import 'Settings/angularModule';
import tuitionAndRegistrationLocales from 'Settings/locales/settings/tuition_and_registration-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(tuitionAndRegistrationLocales);

describe('Settings::TuitionAndRegistration::TuitionStatusRegistered', () => {
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let $injector;
    let currentUser;
    let $window;
    let Cohort;
    let CohortApplication;
    let launchProgramGuide;
    let promoted;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Settings', 'SpecHelper');

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            SpecHelper = $injector.get('SpecHelper');
            $window = $injector.get('$window');
            Cohort = $injector.get('Cohort');
            CohortApplication = $injector.get('CohortApplication');

            $injector.get('CohortFixtures');
        });

        currentUser = SpecHelper.stubCurrentUser('learner');
        SpecHelper.stubConfig();
        SpecHelper.stubStripeCheckout();

        const cohort = Cohort.fixtures.getInstance({
            program_type: 'emba',
            id: 123,
            registration_deadline_days_offset: -4,
            start_date: new Date('2117/07/01').getTime() / 1000,
        });

        promoted = CohortApplication.new({
            status: 'pre_accepted',
            program_type: 'emba',
            cohort_id: cohort.id,
            applied_at: new Date().getTime() / 1000,
            cohort,
            cohort_title: cohort.title,
            total_num_required_stripe_payments: 12,
            in_good_standing: true,
            stripe_plan_id: 'monthly_plan',
            stripe_plans: [
                {
                    id: 'monthly_plan',
                    frequency: 'monthly',
                    amount: 80000,
                },
                {
                    id: 'bi_annual_plan',
                    frequency: 'bi_annual',
                    amount: 420000,
                },
                {
                    id: 'single_payment_plan',
                    frequency: 'once',
                    amount: 650000,
                },
            ],
            scholarship_level: {
                name: 'No Scholarship',
                coupons: {
                    monthly_plan: {
                        id: 'none',
                        amount_off: 0,
                        percent_off: undefined,
                    },
                    bi_annual_plan: {
                        id: 'none',
                        amount_off: 0,
                        percent_off: undefined,
                    },
                    single_payment_plan: {
                        id: 'none',
                        amount_off: 0,
                        percent_off: undefined,
                    },
                },
                standard: {
                    monthly_plan: {
                        id: 'none',
                        amount_off: 0,
                        percent_off: undefined,
                    },
                    bi_annual_plan: {
                        id: 'none',
                        amount_off: 0,
                        percent_off: undefined,
                    },
                    single_payment_plan: {
                        id: 'none',
                        amount_off: 0,
                        percent_off: undefined,
                    },
                },
                early: {
                    monthly_plan: {
                        id: 'none',
                        amount_off: 0,
                        percent_off: undefined,
                    },
                    bi_annual_plan: {
                        id: 'none',
                        amount_off: 0,
                        percent_off: undefined,
                    },
                    single_payment_plan: {
                        id: 'none',
                        amount_off: 0,
                        percent_off: undefined,
                    },
                },
            },
        });

        currentUser.relevant_cohort = promoted.cohort;
        currentUser.cohort_applications.push(promoted);

        launchProgramGuide = jest.fn();
    });

    describe('in good standing', () => {
        beforeEach(() => {
            promoted.in_good_standing = true;
        });

        describe('with tuition', () => {
            const opts = {
                defaultPlanOriginalTotal: 9600,
                defaultPlanScholarshipTotal: 0,
            };

            beforeEach(() => {
                jest.spyOn(currentUser, 'primarySubscription', 'get').mockReturnValue({});
            });

            describe('in web app', () => {
                beforeEach(() => {});

                it('should display modify payment button and payment footer', () => {
                    render(opts);
                    SpecHelper.expectElementText(elem, '.modify-btn', 'Modify Payment Details');
                    SpecHelper.expectElementText(
                        elem,
                        '[name="payment-footer"]',
                        'You will be charged $800 per month during the one year course of study.',
                    );
                });

                it('should display modify payment button and payment footer with appropriate bi_annual text', () => {
                    opts.frequency = 'bi_annual';
                    render(opts);
                    SpecHelper.expectElementText(elem, '.modify-btn', 'Modify Payment Details');
                    SpecHelper.expectElementText(
                        elem,
                        '[name="payment-footer"]',
                        'You will be charged $4200 twice during the one year course of study.',
                    );
                });

                it('should launch stripe if modify payment button clicked', () => {
                    render(opts);
                    jest.spyOn(scope, 'modify').mockImplementation(() => {});

                    SpecHelper.click(elem, '.modify-btn');
                    expect(scope.modify).toHaveBeenCalled();
                });
            });

            describe('in CORDOVA app', () => {
                beforeEach(() => {
                    $window.CORDOVA = true;
                    render(opts);
                });

                afterEach(() => {
                    delete $window.CORDOVA;
                });

                it('should notify user a new broswer will be opened', () => {
                    SpecHelper.expectElementText(
                        elem,
                        '.new-browser',
                        'To modify your payment details, you will need to use your system web browser.',
                    );
                });

                it('should launch system browser if modify payment button clicked', () => {
                    jest.spyOn(scope, 'modify');
                    jest.spyOn(scope, 'loadUrl').mockImplementation(() => {});

                    SpecHelper.expectElementText(elem, '.modify-btn', 'Launch Web Browser');

                    SpecHelper.click(elem, '.modify-btn');
                    expect(scope.modify).toHaveBeenCalled();
                    expect(scope.loadUrl).toHaveBeenCalled();
                });
            });
        });
    });

    describe('not in good standing', () => {
        beforeEach(() => {
            promoted.in_good_standing = false;

            render({
                defaultPlanOriginalTotal: 9600,
                defaultPlanScholarshipTotal: 0,
            });
        });

        it('should show fix payment UI and hide thank you message when monthly', () => {
            SpecHelper.expectElementText(
                elem,
                '[name="billed-at"]',
                'You are currently enrolled in the Monthly Tuition Plan billed at $800 USD per month.',
            );
            SpecHelper.expectElementText(elem, '.modify-btn', 'Fix Payment Details');
        });

        it('should show fix payment UI and hide thank you message when bi-annual', () => {
            scope.cohortApplication.existingPlan.frequency = 'bi_annual';
            scope.$digest();
            SpecHelper.expectElementText(
                elem,
                '[name="billed-at"]',
                'You are currently enrolled in the Bi-annual Tuition Plan billed twice at $800 USD during the course of study.',
            );
            SpecHelper.expectElementText(elem, '.modify-btn', 'Fix Payment Details');
        });

        it('should launch stripe if fix payment button clicked', () => {
            jest.spyOn(scope, 'modify').mockImplementation(() => {});

            SpecHelper.click(elem, '.modify-btn');
            expect(scope.modify).toHaveBeenCalled();
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render(opts) {
        opts = _.extend(
            {
                defaultPlanOriginalTotal: 9600,
                defaultPlanScholarshipTotal: 0,
                defaultPlanNumPaymentIntervals: 12,
            },
            opts || {},
        );

        const cohortApplication = _.last(currentUser.cohort_applications);

        if (opts.frequency === 'once') {
            jest.spyOn(cohortApplication, 'existingPlan', 'get').mockReturnValue(promoted.stripe_plans[2]);
        } else if (opts.frequency === 'bi_annual') {
            jest.spyOn(cohortApplication, 'existingPlan', 'get').mockReturnValue(promoted.stripe_plans[1]);
        } else {
            jest.spyOn(cohortApplication, 'existingPlan', 'get').mockReturnValue(promoted.stripe_plans[0]);
        }

        if (opts.defaultPlanOriginalTotal === opts.defaultPlanScholarshipTotal) {
            jest.spyOn(cohortApplication, 'hasFullScholarship', 'get').mockReturnValue(true);
            jest.spyOn(cohortApplication, 'hasPartialScholarship', 'get').mockReturnValue(false);
            jest.spyOn(cohortApplication, 'hasNoScholarship', 'get').mockReturnValue(false);
        } else if (opts.defaultPlanScholarshipTotal > 0) {
            jest.spyOn(cohortApplication, 'hasFullScholarship', 'get').mockReturnValue(false);
            jest.spyOn(cohortApplication, 'hasPartialScholarship', 'get').mockReturnValue(true);
            jest.spyOn(cohortApplication, 'hasNoScholarship', 'get').mockReturnValue(false);
        } else {
            jest.spyOn(cohortApplication, 'hasFullScholarship', 'get').mockReturnValue(false);
            jest.spyOn(cohortApplication, 'hasPartialScholarship', 'get').mockReturnValue(false);
            jest.spyOn(cohortApplication, 'hasNoScholarship', 'get').mockReturnValue(true);
        }

        Object.defineProperty(cohortApplication, 'defaultPlanOriginalTotal', {
            value: opts.defaultPlanOriginalTotal,
            configurable: true,
        });

        Object.defineProperty(cohortApplication, 'defaultPlanScholarshipTotal', {
            value: opts.defaultPlanScholarshipTotal,
            configurable: true,
        });

        Object.defineProperty(cohortApplication, 'defaultPlanNumPaymentIntervals', {
            value: opts.defaultPlanNumPaymentIntervals,
            configurable: true,
        });

        renderer = SpecHelper.renderer();
        renderer.scope.launchProgramGuide = launchProgramGuide;
        renderer.scope.cohortApplication = cohortApplication;

        renderer.render(
            '<tuition-status-registered cohort-application="cohortApplication" launch-program-guide="launchProgramGuide()"></tuition-status-registered>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
    }
});
