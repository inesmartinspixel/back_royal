import 'AngularSpecHelper';
import 'Careers/angularModule/spec/_mock/fixtures/hiring_team_fixtures';
import 'Settings/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import editBillingLocales from 'Settings/locales/settings/edit_billing-en.json';
import careersLocales from 'Careers/locales/careers/careers-en.json';
import hiringBillingLocales from 'Careers/locales/careers/hiring_billing-en.json';

setSpecLocales(editBillingLocales, careersLocales, hiringBillingLocales);

describe('Settings::editBilling', () => {
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let $injector;
    let currentUser;
    let HiringTeam;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Settings', 'SpecHelper');

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            SpecHelper = $injector.get('SpecHelper');
            HiringTeam = $injector.get('HiringTeam');
            $injector.get('HiringTeamFixtures');
        });

        currentUser = SpecHelper.stubCurrentUser('learner');
        SpecHelper.stubConfig();
        SpecHelper.stubStripeCheckout();
        render();
    });

    describe('as a hiring manager', () => {
        beforeEach(() => {
            currentUser.hiring_application = {};
            currentUser.hiring_team = HiringTeam.fixtures.getInstance();
        });

        it('should show billing page to accepted hm with subscriptions required', () => {
            jest.spyOn(currentUser, 'hasAcceptedHiringManagerAccess', 'get').mockReturnValue(true);
            jest.spyOn(currentUser, 'hasAcceptedHiringManagerAccessPastDue', 'get').mockReturnValue(false);
            currentUser.hiring_team = HiringTeam.fixtures.getInstance({
                subscription_required: true,
            });
            scope.$digest();
            SpecHelper.expectElement(elem, 'hiring-billing');
            SpecHelper.expectNoElement(elem, '.we-are-reviewing');
        });

        it('should show billing page to accepted hm with subscriptions required but past due', () => {
            jest.spyOn(currentUser, 'hasAcceptedHiringManagerAccess', 'get').mockReturnValue(true);
            jest.spyOn(currentUser, 'hasAcceptedHiringManagerAccessPastDue', 'get').mockReturnValue(true);
            currentUser.hiring_team = HiringTeam.fixtures.getInstance({
                subscription_required: true,
            });
            scope.$digest();
            SpecHelper.expectElement(elem, 'hiring-billing');
            SpecHelper.expectNoElement(elem, '.we-are-reviewing');
        });

        it('should show message to non-accepted hm', () => {
            jest.spyOn(currentUser, 'hasAcceptedHiringManagerAccess', 'get').mockReturnValue(false);
            jest.spyOn(currentUser, 'hasAcceptedHiringManagerAccessPastDue', 'get').mockReturnValue(false);
            scope.$digest();
            SpecHelper.expectNoElement(elem, 'hiring-billing');
            SpecHelper.expectElementText(
                elem,
                '.we-are-reviewing',
                "We're reviewing your profile Our team will be in touch soon. Thank you for joining Smartly Talent!",
            );
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.render('<edit-billing></edit-billing>');
        elem = renderer.elem;
        scope = elem.isolateScope();
    }
});
