import 'AngularSpecHelper';
import 'Settings/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import editAccountLocales from 'Settings/locales/settings/edit_account-en.json';
import changePasswordLocales from 'Settings/locales/settings/change_password-en.json';

setSpecLocales(editAccountLocales, changePasswordLocales);

describe('Settings::editAccount', () => {
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let $injector;
    let $auth;
    let ngToast;
    let isMobile;
    let currentUser;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Settings', 'SpecHelper');

        angular.mock.module($provide => {
            isMobile = jest.fn();
            isMobile.mockReturnValue(false);
            $provide.value('isMobile', isMobile);
        });

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            SpecHelper = $injector.get('SpecHelper');
            $auth = $injector.get('$auth');
            ngToast = $injector.get('ngToast');
        });

        SpecHelper.stubConfig();
        currentUser = SpecHelper.stubCurrentUser('learner');
    });

    describe('responsive design', () => {
        it('should change name of password submit button as needed', () => {
            render();
            // on desktop, the button says CHANGE PASSWORD
            SpecHelper.expectElementText(elem, '[name="save_password_changes"]', 'CHANGE PASSWORD');

            scope.isMobile = true;
            scope.$digest();

            // on mobile, the button saus SAVE_CHANGES
            SpecHelper.click(elem, '[name="change_password"]');
            SpecHelper.expectElementText(elem, '[name="save_password_changes"]', 'SAVE CHANGES');
        });

        it('should toggle between desktop and mobile mode', () => {
            render();
            expect(scope.showProfileForm).toBe(true);
            expect(scope.showChangePasswordForm).toBe(true);

            scope.isMobile = true;
            scope.$digest();

            expect(scope.showProfileForm).toBe(true);
            expect(scope.showChangePasswordForm).toBe(false);
        });

        it('should re-call isMobile during window resizes', () => {
            render();
            isMobile.mockClear();
            const $window = $injector.get('$window');
            $($window).trigger('resize');
            expect(isMobile.mock.calls.length).toBe(1);
        });

        describe('desktop mode', () => {
            beforeEach(() => {
                isMobile.mockReturnValue(false);
            });

            it('should have name, email, and change password form inputs', () => {
                render();

                scope.$digest();

                const inputs = SpecHelper.expectElements(elem, 'input', 5);

                let inputIndex = 0;
                expect(inputs.eq(inputIndex).attr('name')).toEqual('name');
                expect(inputs.eq(inputIndex).attr('type')).toEqual('text');
                expect(inputs.eq(inputIndex).attr('required')).toEqual('required');

                inputIndex += 1;
                expect(inputs.eq(inputIndex).attr('name')).toEqual('nickname');
                expect(inputs.eq(inputIndex).attr('type')).toEqual('text');

                inputIndex += 1;
                expect(inputs.eq(inputIndex).attr('name')).toEqual('email');
                expect(inputs.eq(inputIndex).attr('type')).toEqual('email');
                expect(inputs.eq(inputIndex).attr('required')).toEqual('required');

                // see also change_password_dir.js
                inputIndex += 1;
                expect(inputs.eq(inputIndex).attr('name')).toEqual('password');
                expect(inputs.eq(inputIndex).attr('type')).toEqual('password');
                expect(inputs.eq(inputIndex).attr('required')).toEqual('required');
                expect(inputs.eq(inputIndex).attr('autocomplete')).toEqual('off_even_chrome');
                expect(inputs.eq(inputIndex).attr('autocorrect')).toEqual('off');
                expect(inputs.eq(inputIndex).attr('autocapitalize')).toEqual('off');
                expect(inputs.eq(inputIndex).attr('spellcheck')).toEqual('false');

                inputIndex += 1;
                expect(inputs.eq(inputIndex).attr('name')).toEqual('password_confirmation');
                expect(inputs.eq(inputIndex).attr('type')).toEqual('password');
                expect(inputs.eq(inputIndex).attr('required')).toEqual('required');
                expect(inputs.eq(inputIndex).attr('autocomplete')).toEqual('off_even_chrome');
                expect(inputs.eq(inputIndex).attr('autocorrect')).toEqual('off');
                expect(inputs.eq(inputIndex).attr('autocapitalize')).toEqual('off');
                expect(inputs.eq(inputIndex).attr('spellcheck')).toEqual('false');
            });
        });

        describe('mobile mode', () => {
            beforeEach(() => {
                isMobile.mockReturnValue(true);
                render();
            });

            it('should have name, nickname, email, but no change password form inputs', () => {
                const inputs = SpecHelper.expectElements(elem, 'input', 3);

                let inputIndex = 0;
                expect(inputs.eq(inputIndex).attr('name')).toEqual('name');
                expect(inputs.eq(inputIndex).attr('type')).toEqual('text');
                expect(inputs.eq(inputIndex).attr('required')).toEqual('required');

                inputIndex += 1;
                expect(inputs.eq(inputIndex).attr('name')).toEqual('nickname');
                expect(inputs.eq(inputIndex).attr('type')).toEqual('text');

                inputIndex += 1;
                expect(inputs.eq(inputIndex).attr('name')).toEqual('email');
                expect(inputs.eq(inputIndex).attr('type')).toEqual('email');
                expect(inputs.eq(inputIndex).attr('required')).toEqual('required');
            });

            it('should have change password / show profile navigation buttons', () => {
                const scrollHelper = $injector.get('scrollHelper');
                const changePasswordButton = SpecHelper.expectElementText(
                    elem,
                    '[name="change_password"]',
                    'CHANGE PASSWORD',
                    0,
                );
                SpecHelper.expectNoElement(
                    elem,
                    '.form-item-container.visible-xs > .input-container > button.arrow-left',
                );

                jest.spyOn(scrollHelper, 'scrollToTop').mockImplementation(() => {});

                changePasswordButton.click();
                scope.$digest();
                expect(scrollHelper.scrollToTop.mock.calls.length).toBe(1);

                const backToProfileButton = SpecHelper.expectElementText(
                    elem,
                    '[name="back_to_account"]',
                    'BACK TO ACCOUNT',
                    0,
                );
                backToProfileButton.click();
                scope.$digest();
                expect(scrollHelper.scrollToTop.mock.calls.length).toBe(2);

                SpecHelper.expectNoElement(
                    elem,
                    '.form-item-container.visible-xs > .input-container > button.arrow-left',
                );
            });
        });
    });

    describe('form submission', () => {
        let submit;
        let $q;

        beforeEach(() => {
            $q = $injector.get('$q');

            render();
            submit = SpecHelper.expectElement(elem, 'button[name="save_changes"]:eq(0)');

            SpecHelper.updateTextInput(elem, 'input[name="name"]', 'Some Name');
            expect(scope.form.name).toEqual('Some Name');

            SpecHelper.updateTextInput(elem, 'input[name="email"]', 'someuser@email.com');
            expect(scope.form.email).toEqual('someuser@email.com');
        });

        it('should disable the submit button during submissions', () => {
            let deferred;

            // failure
            deferred = $q.defer();
            jest.spyOn($auth, 'updateAccount').mockReturnValue(deferred.promise);
            submit.click();
            SpecHelper.expectElementDisabled(elem, 'button[type="submit"]');
            deferred.reject();
            scope.$digest();
            SpecHelper.expectElementEnabled(elem, 'button[type="submit"]');

            // success
            deferred = $q.defer();
            $auth.updateAccount.mockReturnValue(deferred.promise);
            submit.click();
            SpecHelper.expectElementDisabled(elem, 'button[type="submit"]');
            deferred.resolve();
            scope.$digest();
            SpecHelper.expectElementEnabled(elem, 'button[type="submit"]');
        });

        it('should handle valid profile edits', () => {
            const deferred = $q.defer();
            jest.spyOn($auth, 'updateAccount').mockReturnValue(deferred.promise);
            jest.spyOn(ngToast, 'create').mockImplementation(() => {});
            submit.click();
            expect($auth.updateAccount).toHaveBeenCalledWith(scope.form);
            deferred.resolve();
            scope.$digest();
            expect(ngToast.create).toHaveBeenCalledWith({
                content: 'Profile details successfully updated.',
                className: 'success',
            });
        });

        it('should display a toast message upon invalid profile edits', () => {
            jest.spyOn($auth, 'updateAccount').mockReturnValue($q.reject());
            jest.spyOn(ngToast, 'create').mockImplementation(() => {});
            submit.click();
            expect($auth.updateAccount).toHaveBeenCalledWith(scope.form);
            expect(ngToast.create).toHaveBeenCalledWith({
                content: 'There was a problem updating your profile. Please check your info and try again.',
                className: 'danger',
            });
        });

        it('should disable submission until all required fields are completed', () => {
            // clear out inputs from beforeEach
            SpecHelper.updateTextInput(elem, 'input[name="name"]', '');
            SpecHelper.updateTextInput(elem, 'input[name="email"]', '');

            SpecHelper.expectElementDisabled(elem, 'button[name="save_changes"]');

            SpecHelper.updateTextInput(elem, 'input[name="name"]', 'Joe User');
            SpecHelper.expectElementDisabled(elem, 'button[name="save_changes"]');

            SpecHelper.updateTextInput(elem, 'input[name="email"]', 'joeuser@pedago.com');
            SpecHelper.expectElementEnabled(elem, 'button[name="save_changes"]');
        });
    });

    describe('non-saml oauth learners', () => {
        it('should enable the email field for non-saml oauth registered users', () => {
            render();
            SpecHelper.expectElementEnabled(elem, 'input[type="email"]'); // sanity check
            currentUser.provider = 'google';
            scope.$digest();
            SpecHelper.expectElementDisabled(elem, 'input[type="email"]');
        });
    });

    describe('saml oauth learners', () => {
        beforeEach(() => {
            currentUser.institutions = ['fake institution to turn on saml'];
            Object.defineProperty(currentUser, 'loggedInWithSAML', {
                value: true,
            });
            render();
        });

        it('should disable the email field', () => {
            render();
            SpecHelper.expectElementDisabled(elem, 'input[type="email"]');
        });

        describe('desktop mode', () => {
            beforeEach(() => {
                isMobile.mockReturnValue(false);
                render();
            });

            it('should not have change password form inputs', () => {
                render();

                scope.$digest();

                const inputs = SpecHelper.expectElements(elem, 'input', 3);

                let inputIndex = 0;
                expect(inputs.eq(inputIndex).attr('name')).toEqual('name');
                expect(inputs.eq(inputIndex).attr('type')).toEqual('text');
                expect(inputs.eq(inputIndex).attr('required')).toEqual('required');

                inputIndex += 1;
                expect(inputs.eq(inputIndex).attr('name')).toEqual('nickname');
                expect(inputs.eq(inputIndex).attr('type')).toEqual('text');

                inputIndex += 1;
                expect(inputs.eq(inputIndex).attr('name')).toEqual('email');
                expect(inputs.eq(inputIndex).attr('type')).toEqual('email');
                expect(inputs.eq(inputIndex).attr('required')).toEqual('required');
            });
        });

        describe('mobile mode', () => {
            beforeEach(() => {
                isMobile.mockReturnValue(true);
                render();
            });

            it('should not have change password button', () => {
                SpecHelper.expectNoElement(
                    elem,
                    '.form-item-container.visible-xs > .input-container > button.arrow-right',
                );
                SpecHelper.expectNoElement(
                    elem,
                    '.form-item-container.visible-xs > .input-container > button.arrow-left',
                );
            });
        });
    });

    describe('with phone_no_password provider', () => {
        it('should show phone field and hide email field and password field', () => {
            currentUser.provider = 'phone_no_password';
            render();
            SpecHelper.expectNoElement(elem, '[name="email"]');
            SpecHelper.expectNoElement(elem, '[name="changePasswordSection"]');
            SpecHelper.updateSelect(elem, 'select[name="countryCode"]', 'US');
            SpecHelper.updateTextInput(elem, 'input[name="phone"]', '201-555-5555');
            expect(scope.form.phone).toEqual('+12015555555');
        });
    });

    describe('deletion text', () => {
        it('should show deletion text for all users', () => {
            render();
            SpecHelper.expectElementText(elem, '.delete-account > h2', 'Delete Account');
            SpecHelper.expectElementText(
                elem,
                '.delete-account > p',
                'Should you wish to delete your account and all associated data from Quantic, please contact support@quantic.edu with the subject line "Account Deletion Request." Please note that account deletion requests are permanent and irreversible.',
            );
        });

        it('should change brand name in locale message to Miya Miya for Miya Miya users', () => {
            jest.spyOn(currentUser, 'isMiyaMiya', 'get').mockReturnValue(true);
            render();
            SpecHelper.expectElementText(elem, '.delete-account > h2', 'Delete Account');
            SpecHelper.expectElementText(
                elem,
                '.delete-account > p',
                'Should you wish to delete your account and all associated data from Miya Miya, please contact support@quantic.edu with the subject line "Account Deletion Request." Please note that account deletion requests are permanent and irreversible.',
            );
        });

        it('should show deferral for pre-accepted or accepted schedulable cohort users', () => {
            currentUser.cohort_applications = [
                {
                    status: 'accepted',
                },
            ];
            currentUser.relevant_cohort = {
                supportsSchedule: true,
            };
            render();
            SpecHelper.expectElementText(
                elem,
                '.delete-account p:eq(0)',
                'If you\'re having issues staying on track with your cohort or you simply need a break from Quantic, you should ask for a deferral before proceeding with the account deletion process. Simply email us at support@quantic.edu with the subject "Deferral Request" to begin the deferral process. You can read more about deferring here.',
            );
            SpecHelper.expectElementText(
                elem,
                '.delete-account p:eq(1)',
                'Should you wish to delete your account and all associated data from Quantic, please contact support@quantic.edu with the subject line "Account Deletion Request." Please note that account deletion requests are permanent and irreversible.',
            );
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.render('<edit-account></edit-account>');
        elem = renderer.elem;
        scope = elem.isolateScope();
    }
});
