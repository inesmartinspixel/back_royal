import 'AngularSpecHelper';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohort_applications';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';
import 'Settings/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import paidCertificateCheckoutLocales from 'Settings/locales/settings/paid_certificate_checkout-en.json';

setSpecLocales(paidCertificateCheckoutLocales);

describe('paidCertificateCheckoutDir', () => {
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let $injector;
    let Cohort;
    let CohortApplication;
    let currentUser;
    let $q;
    let promotedCohorts;
    let $rootScope;
    let StripeCheckoutHelper;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Settings', 'SpecHelper');

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            SpecHelper = $injector.get('SpecHelper');
            Cohort = $injector.get('Cohort');
            CohortApplication = $injector.get('CohortApplication');
            $q = $injector.get('$q');
            $rootScope = $injector.get('$rootScope');
            StripeCheckoutHelper = $injector.get('Payments.StripeCheckoutHelper');

            $injector.get('CohortFixtures');
            $injector.get('CohortApplicationFixtures');
        });

        currentUser = SpecHelper.stubCurrentUser('learner');
        currentUser.cohort_applications = [CohortApplication.fixtures.getInstance()];

        promotedCohorts = [
            Cohort.fixtures.getInstance({
                program_type: 'paid_cert_data_analytics',
                title: 'foo',
            }),
            Cohort.fixtures.getInstance({
                program_type: 'paid_cert_data_analytics',
                title: 'baz',
            }),
            Cohort.fixtures.getInstance({
                program_type: 'paid_cert_data_analytics',
                title: 'bar',
            }),
        ];
        jest.spyOn(promotedCohorts[0], 'isPaidCert', 'get').mockReturnValue(true);
        jest.spyOn(promotedCohorts[1], 'isPaidCert', 'get').mockReturnValue(true);

        promotedCohorts[0].stripe_plans = promotedCohorts[1].stripe_plans = [
            {
                id: 'default_plan_id',
            },
        ];

        jest.spyOn(promotedCohorts[2], 'isPaidCert', 'get').mockReturnValue(false);
        SpecHelper.stubConfig();
        SpecHelper.stubStripeCheckout();
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.promotedCohorts = promotedCohorts;

        renderer.render('<paid-certificate-checkout promoted-cohorts="promotedCohorts"></paid-certificate-checkout>');
        elem = renderer.elem;
        scope = elem.isolateScope();

        // HACK: we disabled coupons temporarily for marketing; re-enable them
        scope.couponsEnabled = true;
        scope.$apply();
    }

    it('should configure StripeCheckoutHelper with status toggling support', () => {
        const deferred = $q.defer();
        jest.spyOn(StripeCheckoutHelper, 'configure').mockReturnValue(deferred.promise);
        render();
        expect(StripeCheckoutHelper.configure).toHaveBeenCalled();
    });

    it('should set paidCertPrograms correctly and sort them by title', () => {
        render();
        expect(scope.paidCertCohorts).toEqual([promotedCohorts[1], promotedCohorts[0]]);
    });

    it('should initialize proxy.cohortId if user has started the paid cert process', () => {
        const paidCertApplication = CohortApplication.fixtures.getInstance({
            cohort_id: 'foo-id',
        });
        jest.spyOn(paidCertApplication, 'isPaidCert', 'get').mockReturnValue(true);
        currentUser.cohort_applications = [paidCertApplication];
        render();
        expect(scope.proxy.cohortId).toBe('foo-id');
    });

    describe('view', () => {
        it('should set view to select if user has not created a paid cert application', () => {
            const paidCertApplication = CohortApplication.fixtures.getInstance();
            jest.spyOn(paidCertApplication, 'isPaidCert', 'get').mockReturnValue(false);
            currentUser.cohort_applications = [paidCertApplication];
            render();
            expect(scope.view).toBe('select');
            SpecHelper.expectElements(elem, '.certificate-options > li', 2);
        });

        it('should set view to checkout if user has created a paid cert application', () => {
            const paidCertApplication = CohortApplication.fixtures.getInstance();
            jest.spyOn(paidCertApplication, 'isPaidCert', 'get').mockReturnValue(true);
            currentUser.cohort_applications = [paidCertApplication];
            render();
            expect(scope.view).toBe('checkout');
            SpecHelper.expectElement(elem, '[name="cancel"]');
            SpecHelper.expectElement(elem, '[name="enroll"]');
        });

        it('should set view to accepted if user has an accepted paid cert application', () => {
            const paidCertApplication = CohortApplication.fixtures.getInstance();
            jest.spyOn(paidCertApplication, 'isPaidCert', 'get').mockReturnValue(true);
            paidCertApplication.status = 'accepted';
            currentUser.cohort_applications = [paidCertApplication];
            jest.spyOn($rootScope, 'goHome').mockImplementation(() => {});
            render();
            expect(scope.view).toBe('accepted');
            SpecHelper.expectNoElement(elem, '[name="cancel"]');
            SpecHelper.expectNoElement(elem, '[name="enroll"]');
            SpecHelper.expectElementText(elem, 'footer .sub-text', 'Enrollment complete!');
            SpecHelper.click(elem, '[name="start-learning"]');
            expect($rootScope.goHome).toHaveBeenCalled();
        });
    });

    describe('coupon', () => {
        let Coupon;
        beforeEach(() => {
            Coupon = $injector.get('Coupon');
            render();
            scope.view = 'checkout';
            scope.selectedPaidCertCohort = promotedCohorts[0];
            scope.$digest();
        });

        it('should show the price after applying a valid coupon with an amount off', () => {
            jest.spyOn(scope.selectedPaidCertCohort, 'price', 'get').mockReturnValue(10000);
            Coupon.expect('show').returns({
                amount_off: 420000,
            });
            SpecHelper.click(elem, '[name="enter-coupon"] a');
            SpecHelper.updateTextInput(elem, '[name="code"]', 'some-coupon');
            SpecHelper.click(elem, '[name="apply-coupon"]');
            Coupon.flush('show');
            SpecHelper.expectNoElement(elem, '[name="coupon-error"]');

            SpecHelper.expectNoElement(elem, '.checkout-totals .discount-percentage');
            SpecHelper.expectElementText(elem, '.checkout-totals .discount-amount', '$4,200');
            SpecHelper.expectElementText(elem, '.checkout-totals .total-amount', '$5,800');
        });

        it('should show the price after applying a valid coupon with a percent off', () => {
            jest.spyOn(scope.selectedPaidCertCohort, 'price', 'get').mockReturnValue(10000);
            Coupon.expect('show').returns({
                percent_off: 42,
            });
            SpecHelper.click(elem, '[name="enter-coupon"] a');
            SpecHelper.updateTextInput(elem, '[name="code"]', 'some-coupon');
            SpecHelper.click(elem, '[name="apply-coupon"]');
            Coupon.flush('show');
            SpecHelper.expectNoElement(elem, '[name="coupon-error"]');

            SpecHelper.expectElementText(elem, '.checkout-totals .discount-percentage', '42% Discount:');
            SpecHelper.expectElementText(elem, '.checkout-totals .discount-amount', '$4,200');
            SpecHelper.expectElementText(elem, '.checkout-totals .total-amount', '$5,800');
        });

        it('should show a message if the server says a coupon is invalid', () => {
            Coupon.expect('show').returns([]);
            SpecHelper.click(elem, '[name="enter-coupon"] a');
            SpecHelper.updateTextInput(elem, '[name="code"]', 'some-coupon');
            SpecHelper.click(elem, '[name="apply-coupon"]');
            Coupon.flush('show');
            SpecHelper.expectElementText(elem, '[name="coupon-error"]', 'Invalid coupon code');
            SpecHelper.expectNoElement(elem, '.checkout-totals');
        });
    });

    describe('applyToCohort', () => {
        function assertApply(deferred) {
            SpecHelper.expectElement(elem, 'front-royal-spinner');
            deferred.resolve(CohortApplication.fixtures.getInstance());
            scope.$digest();

            SpecHelper.expectNoElement(elem, 'front-royal-spinner');
            expect(scope.selectedPaidCertCohort).toEqual(promotedCohorts[1]); // remember sortBy title here
            expect(currentUser.applyToCohort).toHaveBeenCalledWith(promotedCohorts[1], 'pre_accepted');
            expect(scope.view).toBe('checkout');
        }

        it('should work when next is clicked', () => {
            render();
            const deferred = $q.defer();
            jest.spyOn(currentUser, 'applyToCohort').mockReturnValue(deferred.promise);

            SpecHelper.toggleRadio(elem, 'input[type="radio"]', 0);
            SpecHelper.click(elem, '[name="apply"]');

            assertApply(deferred);
        });

        it('should work when clicking an item on mobile', () => {
            render();
            scope.isMobile = true; // fake isMobileMixin
            const deferred = $q.defer();
            jest.spyOn(currentUser, 'applyToCohort').mockReturnValue(deferred.promise);

            SpecHelper.toggleRadio(elem, 'input[type="radio"]', 0);

            assertApply(deferred);
        });
    });

    it('should cancel the application process and destroy the pending application if user goes back', () => {
        const oldApplication = CohortApplication.fixtures.getInstance();
        const paidCertApplication = CohortApplication.fixtures.getInstance({
            status: 'pre_accepted',
        });
        jest.spyOn(paidCertApplication, 'isPaidCert', 'get').mockReturnValue(true);
        currentUser.cohort_applications = [oldApplication, paidCertApplication];

        render();

        CohortApplication.expect('destroy');
        SpecHelper.click(elem, '[name="cancel"]');
        SpecHelper.expectElement(elem, 'front-royal-spinner');
        CohortApplication.flush('destroy');

        SpecHelper.expectNoElement(elem, 'front-royal-spinner');
        expect(currentUser.cohort_applications).toEqual([oldApplication]);
        expect(scope.view).toBe('select');
    });

    describe('register', () => {
        let $window;

        beforeEach(() => {
            const paidCertApplication = CohortApplication.fixtures.getInstance();
            jest.spyOn(paidCertApplication, 'isPaidCert', 'get').mockReturnValue(true);
            currentUser.cohort_applications = [paidCertApplication];
            $window = $injector.get('$window');
        });

        afterEach(() => {
            delete $window.CORDOVA;
        });

        it("should call into StripeCheckoutHelper with the cohort's stripe plan id", () => {
            let resolveRegister;
            render();
            jest.spyOn(scope.checkoutHelper, 'registerForCohort').mockImplementation(function () {
                const checkoutHelper = this;
                checkoutHelper.stripeProcessing = true;
                return $q(resolve => {
                    resolveRegister = () => {
                        checkoutHelper.stripeProcessing = false;
                        currentUser.lastCohortApplication.status = 'accepted';
                        resolve();
                        scope.$apply();
                    };
                });
            });
            scope.selectedPaidCertCohort = promotedCohorts[0];
            SpecHelper.expectElement(elem, '[name="checkout-buttons"]');
            SpecHelper.click(elem, '[name="enroll"]');
            expect(scope.checkoutHelper.registerForCohort).toHaveBeenCalledWith(
                promotedCohorts[0].stripe_plans[0].id,
                undefined,
            );

            SpecHelper.expectElement(elem, 'front-royal-spinner');
            SpecHelper.expectNoElement(elem, '[name="checkout-buttons"]');

            resolveRegister();
            expect(scope.view).toEqual('accepted');
        });

        it('should not call into StripeCheckoutHelper if in Cordova', () => {
            $window.CORDOVA = true;
            const ErrorLogService = $injector.get('ErrorLogService');
            jest.spyOn(ErrorLogService, 'notify').mockImplementation(() => {});
            render();
            jest.spyOn(scope.checkoutHelper, 'registerForCohort').mockImplementation(() => {});
            scope.selectedPaidCertCohort = promotedCohorts[0];
            SpecHelper.click(elem, '[name="enroll"]');
            expect(scope.checkoutHelper.registerForCohort).not.toHaveBeenCalled();
            expect(ErrorLogService.notify).toHaveBeenCalledWith('Trying to register for a paid cert in cordova.');
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });
});
