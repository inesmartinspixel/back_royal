import 'AngularSpecHelper';
import 'Settings/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import changePasswordLocales from 'Settings/locales/settings/change_password-en.json';

setSpecLocales(changePasswordLocales);

describe('Settings::ChangePassword', () => {
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let $q;
    let $auth;
    let ngToast;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Settings', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            $q = $injector.get('$q');
            $auth = $injector.get('$auth');
            ngToast = $injector.get('ngToast');
        });
    });

    describe('form fields', () => {
        it('should have password inputs', () => {
            render();

            const inputs = SpecHelper.expectElements(elem, 'input', 2);

            expect(inputs.eq(0).attr('name')).toEqual('password');
            expect(inputs.eq(0).attr('type')).toEqual('password');
            expect(inputs.eq(0).attr('required')).toEqual('required');
            expect(inputs.eq(0).attr('autocomplete')).toEqual('off_even_chrome');
            expect(inputs.eq(0).attr('autocorrect')).toEqual('off');
            expect(inputs.eq(0).attr('autocapitalize')).toEqual('off');
            expect(inputs.eq(0).attr('spellcheck')).toEqual('false');

            expect(inputs.eq(1).attr('name')).toEqual('password_confirmation');
            expect(inputs.eq(1).attr('type')).toEqual('password');
            expect(inputs.eq(1).attr('required')).toEqual('required');
            expect(inputs.eq(1).attr('autocomplete')).toEqual('off_even_chrome');
            expect(inputs.eq(1).attr('autocorrect')).toEqual('off');
            expect(inputs.eq(1).attr('autocapitalize')).toEqual('off');
            expect(inputs.eq(1).attr('spellcheck')).toEqual('false');
        });

        it('should disable the submit button until valid', () => {
            render();

            SpecHelper.expectElementDisabled(elem, 'button[type="submit"]');

            SpecHelper.updateTextInput(elem, 'input[name="password"]', '        ');
            SpecHelper.updateTextInput(elem, 'input[name="password_confirmation"]', '        ');
            SpecHelper.expectElementDisabled(elem, 'button[type="submit"]');

            SpecHelper.updateTextInput(elem, 'input[name="password"]', '1234567');
            SpecHelper.updateTextInput(elem, 'input[name="password_confirmation"]', '1234567');
            SpecHelper.expectElementDisabled(elem, 'button[type="submit"]');

            SpecHelper.updateTextInput(elem, 'input[name="password"]', 'password');
            SpecHelper.updateTextInput(elem, 'input[name="password_confirmation"]', 'password');

            SpecHelper.expectElementEnabled(elem, 'button[type="submit"]');
        });
    });

    describe('form submission', () => {
        let submit;

        beforeEach(() => {
            render();
            submit = SpecHelper.expectElement(elem, 'button[type="submit"]');

            SpecHelper.updateTextInput(elem, 'input[name="password"]', 'password');
            expect(scope.form.password).toEqual('password');

            SpecHelper.updateTextInput(elem, 'input[name="password_confirmation"]', 'password');
            expect(scope.form.password).toEqual('password');
        });

        it('should handle valid changes', () => {
            SpecHelper.expectNoElement(elem, 'form.ng-pristine');
            const deferred = $q.defer();
            jest.spyOn($auth, 'updatePassword').mockReturnValue(deferred.promise);
            jest.spyOn(ngToast, 'create').mockImplementation(() => {});
            submit.click();
            expect($auth.updatePassword).toHaveBeenCalledWith(scope.form);
            deferred.resolve();
            scope.$digest();
            expect(ngToast.create).toHaveBeenCalledWith({
                content: 'Password successfully updated.',
                className: 'success',
            });
            expect(scope.form).toEqual({});
            SpecHelper.expectElement(elem, 'form.ng-pristine');
        });

        it('should disable the submit button during submissions', () => {
            let deferred;

            // failure
            deferred = $q.defer();
            jest.spyOn($auth, 'updatePassword').mockReturnValue(deferred.promise);
            SpecHelper.updateTextInput(elem, 'input[name="password"]', 'password');
            SpecHelper.updateTextInput(elem, 'input[name="password_confirmation"]', 'password');
            submit.click();
            SpecHelper.expectElementDisabled(elem, 'button[type="submit"]');
            deferred.reject();
            scope.$digest();
            SpecHelper.expectElementEnabled(elem, 'button[type="submit"]');

            // success
            deferred = $q.defer();
            $auth.updatePassword.mockReturnValue(deferred.promise);
            SpecHelper.updateTextInput(elem, 'input[name="password"]', 'password');
            SpecHelper.updateTextInput(elem, 'input[name="password_confirmation"]', 'password');
            submit.click();
            SpecHelper.expectElementDisabled(elem, 'button[type="submit"]');
            deferred.resolve();
            scope.$digest();

            // inputs will be cleared now, and the form will be invalid, so still disabled
            SpecHelper.expectElementDisabled(elem, 'button[type="submit"]');
        });

        it('should display a toast message upon invalid changes for known errors', () => {
            const errorResponse = {
                errors: {
                    password: ['is too janky!', 'is too tall'],
                    password_confirmation: ["doesn't match"],
                },
            };
            jest.spyOn($auth, 'updatePassword').mockReturnValue($q.reject(errorResponse));
            jest.spyOn(ngToast, 'create').mockImplementation(() => {});
            submit.click();
            expect($auth.updatePassword).toHaveBeenCalledWith(scope.form);
            expect(ngToast.create).toHaveBeenCalledWith({
                content: "Password is too janky!<br>Password is too tall<br>Password Confirmation doesn't match",
                className: 'danger',
            });
        });

        it('should display a toast message upon invalid changes for unknown errors', () => {
            jest.spyOn($auth, 'updatePassword').mockReturnValue($q.reject());
            jest.spyOn(ngToast, 'create').mockImplementation(() => {});
            submit.click();
            expect($auth.updatePassword).toHaveBeenCalledWith(scope.form);
            expect(ngToast.create).toHaveBeenCalledWith({
                content: 'Please check your password and confirmation and try again.',
                className: 'danger',
            });
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.render('<change-password></change-password>');
        elem = renderer.elem;
        scope = elem.isolateScope();
    }
});
