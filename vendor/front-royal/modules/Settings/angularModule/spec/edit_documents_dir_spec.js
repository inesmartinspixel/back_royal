import 'AngularSpecHelper';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';
import 'Settings/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import editDocumentsLocales from 'Settings/locales/settings/edit_documents-en.json';
import applicationStatusLocales from 'Settings/locales/settings/application_status-en.json';

setSpecLocales(editDocumentsLocales, applicationStatusLocales);

describe('Settings::editDocuments', () => {
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let user;
    let User;
    let ngToast;
    let Cohort;
    let CareerProfile;
    let S3IdentificationAsset;
    let EducationExperience;
    let DialogModal;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Settings', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            User = $injector.get('User');
            ngToast = $injector.get('ngToast');
            Cohort = $injector.get('Cohort');
            CareerProfile = $injector.get('CareerProfile');
            S3IdentificationAsset = $injector.get('S3IdentificationAsset');
            EducationExperience = $injector.get('EducationExperience');
            DialogModal = $injector.get('DialogModal');

            $injector.get('CohortFixtures');
            $injector.get('CareerProfileFixtures');

            SpecHelper.stubDirective('editEnglishDocuments');
        });

        user = SpecHelper.stubCurrentUser();
        user.career_profile = CareerProfile.fixtures.getInstance();
        user.relevant_cohort = Cohort.fixtures.getInstance();
        user.s3_english_language_proficiency_documents = [];
        user.career_profile.education_experiences = [EducationExperience.new(), EducationExperience.new()];
        user.s3_transcript_assets = [];
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.user = user;
        renderer.render('<edit-documents user="user"></edit-documents>');
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$digest();
    }

    describe('Identification section', () => {
        it('should be hidden if !user.relevant_cohort.requiresIdUpload', () => {
            const requiresIdUploadSpy = jest
                .spyOn(user.relevant_cohort, 'requiresIdUpload', 'get')
                .mockReturnValue(true);
            render();
            SpecHelper.expectElement(elem, '[id="identification-section"]');

            requiresIdUploadSpy.mockReturnValue(false);
            scope.$digest();
            SpecHelper.expectNoElement(elem, '[id="identification-section"]');
        });

        describe('when visible', () => {
            beforeEach(() => {
                user.relevant_cohort = Cohort.fixtures.getInstance();
                jest.spyOn(user.relevant_cohort, 'requiresIdUpload', 'get').mockReturnValue(true);
            });

            it('should show progress_badge_complete_turquoise box if identity_verified', () => {
                user.identity_verified = true;
                render();
                SpecHelper.expectElement(elem, '[id="identification-section"] .box img.progress-complete-turqoise');
                SpecHelper.expectElementText(
                    elem,
                    '[id="identification-section"] .box img + .sub-text',
                    'Identification Verified',
                );
            });

            it('should show document upload if !identity_verified', () => {
                user.identity_verified = false;
                render();
                SpecHelper.expectElement(elem, '[id="identification-section"] input[name="identification-upload"]');
            });

            it('should support deleting document', () => {
                user.s3_identification_asset = S3IdentificationAsset.new();
                render();
                jest.spyOn(scope, 'deleteIdentification').mockImplementation(() => {});
                SpecHelper.click(elem, '[id="identification-section"] .trashcan');
                expect(scope.deleteIdentification).toHaveBeenCalledWith(scope.user);
            });

            it('should apply disabled class to trashcan when deletingIdentification and prevent destroy call', () => {
                jest.spyOn(DialogModal, 'confirm').mockImplementation(() => {});
                user.s3_identification_asset = S3IdentificationAsset.new();
                render();
                SpecHelper.expectDoesNotHaveClass(elem, '[id="identification-section"] .trashcan', 'disabled');
                scope.deletingIdentification = true;
                scope.$digest();
                SpecHelper.expectHasClass(elem, '[id="identification-section"] .trashcan', 'disabled');
                jest.spyOn(user.s3_identification_asset, 'destroy').mockImplementation(() => {});
                SpecHelper.click(elem, '[id="identification-section"] .trashcan');
                expect(user.s3_identification_asset.destroy).not.toHaveBeenCalled();
            });

            it('should show a spinner when deletingIdentification', () => {
                render();
                SpecHelper.expectNoElement(elem, '[id="identification-section"] front-royal-spinner');
                scope.deletingIdentification = true;
                scope.$digest();
                SpecHelper.expectElement(elem, '[id="identification-section"] front-royal-spinner');
            });

            it('should show a spinner when uploadingIdentification', () => {
                render();
                SpecHelper.expectNoElement(elem, '[id="identification-section"] front-royal-spinner');
                scope.uploadingIdentification = true;
                scope.$digest();
                SpecHelper.expectElement(elem, '[id="identification-section"] front-royal-spinner');
            });

            it('should show message if there is an error', () => {
                render();
                SpecHelper.expectNoElement(elem, '.file-error-message');
                scope.idErrMessage = 'foo message';
                scope.$digest();
                SpecHelper.expectElementText(elem, '.file-error-message', 'foo message');
            });

            describe('instructions', () => {
                it('should be contextual when field is disabled', () => {
                    jest.spyOn(user, 'recordsIndicateUserShouldUploadIdentificationDocument', 'get').mockReturnValue(
                        false,
                    ); // when false, the field should be disabled
                    render();
                    SpecHelper.expectElementText(
                        elem,
                        '[id="identification-section"] .col-sm-4 .sub-text',
                        "Upon acceptance to the program, please upload a clear image of your driver's license or passport so we can verify your identity.",
                    );
                });

                it('should be contextual when field is enabled', () => {
                    jest.spyOn(user, 'recordsIndicateUserShouldUploadIdentificationDocument', 'get').mockReturnValue(
                        true,
                    ); // when true, the field should be enabled
                    render();
                    SpecHelper.expectElementText(
                        elem,
                        '[id="identification-section"] .col-sm-4 .sub-text',
                        "Please upload a clear image of your driver's license or passport so we can verify your identity.",
                    );
                });
            });
        });
    });

    describe('English Language Proficiency Documentation section', () => {
        it("should be visible if user's careerProfileIndicatesUserShouldUploadEnglishLanguageProficiencyDocuments", () => {
            jest.spyOn(
                user,
                'careerProfileIndicatesUserShouldUploadEnglishLanguageProficiencyDocuments',
                'get',
            ).mockReturnValue(true);
            render();
            expect(scope.showEnglishLanguageProficiencySection).toBe(true);
            SpecHelper.expectElement(elem, '[id="english-language-proficiency-documentation-section"]');
        });

        describe('when visible', () => {
            beforeEach(() => {
                jest.spyOn(
                    user,
                    'careerProfileIndicatesUserShouldUploadEnglishLanguageProficiencyDocuments',
                    'get',
                ).mockReturnValue(true);
            });

            describe('left column', () => {
                it('should be disabled if !recordsIndicateUserShouldUploadEnglishLanguageProficiencyDocuments', () => {
                    const spy = jest
                        .spyOn(user, 'recordsIndicateUserShouldUploadEnglishLanguageProficiencyDocuments', 'get')
                        .mockReturnValue(true);
                    render();
                    SpecHelper.expectDoesNotHaveClass(
                        elem,
                        '[id="english-language-proficiency-documentation-section"] .col-sm-4',
                        'disabled',
                    );

                    spy.mockReturnValue(false);
                    scope.$digest();
                    SpecHelper.expectHasClass(
                        elem,
                        '[id="english-language-proficiency-documentation-section"] .col-sm-4',
                        'disabled',
                    );
                });
            });
        });
    });

    describe('Mailing Address section', () => {
        it('should be visible if Cohort.requiresMailingAddress and user.isAccepted', () => {
            jest.spyOn(user, 'isAccepted', 'get').mockReturnValue(true);
            jest.spyOn(Cohort, 'requiresMailingAddress').mockReturnValue(true);
            render();
            expect(scope.showMailingAddress).toBe(true);
            SpecHelper.expectElement(elem, '[id="mailing-address-section"]');
        });

        it('should be visible if Cohort.requiresMailingAddress and user.isPreAccepted', () => {
            jest.spyOn(user, 'isPreAccepted', 'get').mockReturnValue(true);
            jest.spyOn(Cohort, 'requiresMailingAddress').mockReturnValue(true);
            render();
            expect(scope.showMailingAddress).toBe(true);
            SpecHelper.expectElement(elem, '[id="mailing-address-section"]');
        });
    });

    describe('customCheckboxIsCheckedHandlers', () => {
        it('should have a handler function for transcripts_verified and english_language_proficiency_documents_approved', () => {
            const expectedHandlerKeys = ['transcripts_verified', 'english_language_proficiency_documents_approved'];
            render();
            expect(_.keys(scope.customCheckboxIsCheckedHandlers)).toEqual(expectedHandlerKeys);
        });

        describe('customCheckboxIsCheckedHandler', () => {
            [
                {
                    docsVerifiedProp: 'transcripts_verified',
                    indicatesUserShouldUploadProp: 'careerProfileIndicatesUserShouldProvideTranscripts',
                },
                {
                    docsVerifiedProp: 'english_language_proficiency_documents_approved',
                    indicatesUserShouldUploadProp:
                        'careerProfileIndicatesUserShouldUploadEnglishLanguageProficiencyDocuments',
                },
            ].forEach(config => {
                it(`should return true if ${config.docsVerifiedProp}`, () => {
                    user[config.docsVerifiedProp] = true;
                    render();
                    expect(
                        scope.customCheckboxIsCheckedHandlers[config.docsVerifiedProp](config.docsVerifiedProp),
                    ).toBe(true);
                });

                it(`should return true if !${config.indicatesUserShouldUploadProp}`, () => {
                    user[config.docsVerifiedProp] = false;
                    jest.spyOn(user, config.indicatesUserShouldUploadProp, 'get').mockReturnValue(false);
                    render();
                    expect(
                        scope.customCheckboxIsCheckedHandlers[config.docsVerifiedProp](config.docsVerifiedProp),
                    ).toBe(true);
                });
            });
        });
    });

    describe('fieldIsDisabled', () => {
        [
            {
                indicatesUserShouldUploadProp: 'careerProfileIndicatesUserShouldProvideTranscripts',
            },
            {
                indicatesUserShouldUploadProp:
                    'careerProfileIndicatesUserShouldUploadEnglishLanguageProficiencyDocuments',
            },
        ].forEach(config => {
            it(`should return true if !user.${config.indicatesUserShouldUploadProp}`, () => {
                render();

                const indicatesUserShouldUploadPropSpy = jest
                    .spyOn(scope.user, config.indicatesUserShouldUploadProp, 'get')
                    .mockReturnValue(true);
                expect(scope.fieldIsDisabled(config.indicatesUserShouldUploadProp)).toBe(false);

                indicatesUserShouldUploadPropSpy.mockReturnValue(true);
                expect(scope.fieldIsDisabled(config.indicatesUserShouldUploadProp)).toBe(false);
            });
        });
    });

    describe('Save Documents button', () => {
        it('should be visible if showEnglishLanguageProficiencySection', () => {
            render();
            scope.showEnglishLanguageProficiencySection = true; // ensure documents section visible
            scope.$digest();
            SpecHelper.expectElement(elem, '[name="save-documents"]');
        });

        describe('when visible', () => {
            beforeEach(() => {
                render();
                scope.showEnglishLanguageProficiencySection = true; // ensure documents section visible
                scope.$digest();
                SpecHelper.expectElement(elem, '[name="save-documents"]');
            });

            it('should be disabled if saving or if !hasUnsavedDocumentsSectionChanges or if editDocuments form is $invalid', () => {
                const hasUnsavedDocumentsSectionChangesSpy = jest
                    .spyOn(scope, 'hasUnsavedDocumentsSectionChanges', 'get')
                    .mockReturnValue(true);
                scope.editDocuments.$invalid = false;
                scope.saving = false;
                scope.$digest();
                SpecHelper.expectElementEnabled(elem, '[name="save-documents"]');

                scope.saving = true;
                scope.$digest();
                SpecHelper.expectElementDisabled(elem, '[name="save-documents"]');

                scope.saving = false;
                scope.$digest();
                SpecHelper.expectElementEnabled(elem, '[name="save-documents"]');

                hasUnsavedDocumentsSectionChangesSpy.mockReturnValue(false);
                scope.$digest();
                SpecHelper.expectElementDisabled(elem, '[name="save-documents"]');

                hasUnsavedDocumentsSectionChangesSpy.mockReturnValue(true);
                scope.$digest();
                SpecHelper.expectElementEnabled(elem, '[name="save-documents"]');

                scope.editDocuments.$invalid = true;
                scope.$digest();
                SpecHelper.expectElementDisabled(elem, '[name="save-documents"]');
            });

            describe('when enabled', () => {
                beforeEach(() => {
                    // this should ensure that the save-documents button is enabled
                    jest.spyOn(scope, 'hasUnsavedDocumentsSectionChanges', 'get').mockReturnValue(true);
                    scope.editDocuments.$invalid = false;
                    scope.$digest();
                    SpecHelper.expectElementEnabled(elem, '[name="save-documents"]');
                });

                it('should call save with EDITABLE_DOCUMENTS_ATTRIBUTES and EDITABLE_MAILING_ADDRESS_ATTRIBUTES as arguments, respectively', () => {
                    jest.spyOn(scope, 'save').mockImplementation(() => {});
                    SpecHelper.click(elem, '[name="save-documents"]');
                    expect(scope.save).toHaveBeenCalledWith(
                        scope.EDITABLE_DOCUMENTS_ATTRIBUTES,
                        scope.EDITABLE_MAILING_ADDRESS_ATTRIBUTES,
                    );
                });
            });
        });
    });

    describe('Save Mailing Address button', () => {
        beforeEach(() => {
            render();
            scope.showMailingAddress = true;
            scope.$digest();
            SpecHelper.expectElement(elem, '[name="save-mailing-address"]');
        });

        it('should be disabled if saving or if !hasUnsavedMailingAddressChanges or editMailingAddress form is $invalid', () => {
            const hasUnsavedMailingAddressChangesSpy = jest
                .spyOn(scope, 'hasUnsavedMailingAddressChanges', 'get')
                .mockReturnValue(true);
            scope.editMailingAddress.$invalid = false;
            scope.saving = false;
            scope.$digest();
            SpecHelper.expectElementEnabled(elem, '[name="save-mailing-address"]');

            scope.saving = true;
            scope.$digest();
            SpecHelper.expectElementDisabled(elem, '[name="save-mailing-address"]');

            scope.saving = false;
            scope.$digest();
            SpecHelper.expectElementEnabled(elem, '[name="save-mailing-address"]');

            hasUnsavedMailingAddressChangesSpy.mockReturnValue(false);
            scope.$digest();
            SpecHelper.expectElementDisabled(elem, '[name="save-mailing-address"]');

            scope.editMailingAddress.$invalid = true;
            scope.$digest();
            SpecHelper.expectElementDisabled(elem, '[name="save-mailing-address"]');
        });

        describe('when enabled', () => {
            beforeEach(() => {
                // this should ensure that the save-mailing-address button is enabled
                jest.spyOn(scope, 'hasUnsavedMailingAddressChanges', 'get').mockReturnValue(true);
                scope.editMailingAddress.$invalid = false;
                scope.$digest();
                SpecHelper.expectElementEnabled(elem, '[name="save-mailing-address"]');
            });

            it('should call save with EDITABLE_MAILING_ADDRESS_ATTRIBUTES and EDITABLE_DOCUMENTS_ATTRIBUTES as arguments, respectively', () => {
                jest.spyOn(scope, 'save').mockImplementation(() => {});
                SpecHelper.click(elem, '[name="save-mailing-address"]');
                expect(scope.save).toHaveBeenCalledWith(
                    scope.EDITABLE_MAILING_ADDRESS_ATTRIBUTES,
                    scope.EDITABLE_DOCUMENTS_ATTRIBUTES,
                );
            });
        });
    });

    describe('save', () => {
        it('should copyS3DocumentsToUserProxy before and after save', () => {
            render();
            jest.spyOn(scope, 'copyS3DocumentsToUserProxy').mockImplementation(() => {});
            User.expect('save');
            scope.save(scope.EDITABLE_DOCUMENTS_ATTRIBUTES, scope.EDITABLE_MAILING_ADDRESS_ATTRIBUTES);
            expect(scope.copyS3DocumentsToUserProxy).toHaveBeenCalled();
            scope.copyS3DocumentsToUserProxy.mockClear();
            User.flush('save');
            expect(scope.copyS3DocumentsToUserProxy).toHaveBeenCalled();
        });

        it('should save a temporary user proxy while omitting specified attributes', () => {
            render();
            jest.spyOn(User, 'new');
            jest.spyOn(scope.user, 'save').mockImplementation(() => {});
            jest.spyOn(scope.userProxy, 'save').mockImplementation(() => {});
            User.expect('save');

            scope.save(scope.EDITABLE_DOCUMENTS_ATTRIBUTES, scope.EDITABLE_MAILING_ADDRESS_ATTRIBUTES);
            expect(User.new).toHaveBeenCalledWith(
                _.omit(scope.userProxy.asJson(), scope.EDITABLE_MAILING_ADDRESS_ATTRIBUTES),
            );

            expect(scope.user.save).not.toHaveBeenCalled();
            expect(scope.userProxy.save).not.toHaveBeenCalled();
            User.flush('save');
        });

        it('should apply changes to user object from temporary user proxy on success', () => {
            render();
            scope.userProxy.identity_verified = !scope.user.identity_verified;
            scope.userProxy.transcripts_verified = !scope.user.transcripts_verified;
            scope.userProxy.english_language_proficiency_documents_approved = !scope.user
                .english_language_proficiency_documents_approved;
            scope.userProxy.english_language_proficiency_documents_type = 'some_documents_type';
            scope.userProxy.english_language_proficiency_comments = 'some comments';

            User.expect('save');
            scope.save(scope.EDITABLE_DOCUMENTS_ATTRIBUTES, scope.EDITABLE_MAILING_ADDRESS_ATTRIBUTES);

            expect(scope.user.identity_verified).not.toEqual(scope.userProxy.identity_verified);
            expect(scope.user.transcripts_verified).not.toEqual(scope.userProxy.transcripts_verified);
            expect(scope.user.english_language_proficiency_documents_approved).not.toEqual(
                scope.userProxy.english_language_proficiency_documents_approved,
            );
            expect(scope.user.english_language_proficiency_documents_type).not.toEqual(
                scope.userProxy.english_language_proficiency_documents_type,
            );
            expect(scope.user.english_language_proficiency_comments).not.toEqual(
                scope.userProxy.english_language_proficiency_comments,
            );

            User.flush('save');

            expect(scope.user.identity_verified).toEqual(scope.userProxy.identity_verified);
            expect(scope.user.transcripts_verified).toEqual(scope.userProxy.transcripts_verified);
            expect(scope.user.english_language_proficiency_documents_approved).toEqual(
                scope.userProxy.english_language_proficiency_documents_approved,
            );
            expect(scope.user.english_language_proficiency_documents_type).toEqual(
                scope.userProxy.english_language_proficiency_documents_type,
            );
            expect(scope.user.english_language_proficiency_comments).toEqual(
                scope.userProxy.english_language_proficiency_comments,
            );
        });

        it('should whitelist saved attributes and S3_DOCUMENT_ATTRIBUTES before applying changes to user object', () => {
            render();
            jest.spyOn(scope.user, 'copyAttrs').mockImplementation(() => {});

            scope.userProxy.identity_verified = !scope.user.identity_verified;
            scope.userProxy.transcripts_verified = !scope.user.transcripts_verified;
            scope.userProxy.english_language_proficiency_documents_approved = !scope.user
                .english_language_proficiency_documents_approved;
            scope.userProxy.english_language_proficiency_documents_type = 'some_documents_type';
            scope.userProxy.english_language_proficiency_comments = 'some comments';

            scope.userProxy.s3_identification_asset = null;
            scope.userProxy.s3_transcript_assets = [];
            scope.userProxy.s3_english_language_proficiency_documents = [];

            User.expect('save');

            scope.save(scope.EDITABLE_DOCUMENTS_ATTRIBUTES, []);

            User.flush('save');

            expect(scope.user.copyAttrs).toHaveBeenCalledWith({
                identity_verified: true,
                transcripts_verified: true,
                english_language_proficiency_documents_approved: true,
                english_language_proficiency_documents_type: 'some_documents_type',
                english_language_proficiency_comments: 'some comments',
                s3_identification_asset: null,
                s3_transcript_assets: [],
                s3_english_language_proficiency_documents: [],
            });
        });

        it('should create contextual ngToast when documentation info has been successfully saved', () => {
            jest.spyOn(ngToast, 'create').mockImplementation(() => {});
            render();
            User.expect('save');
            scope.save(scope.EDITABLE_DOCUMENTS_ATTRIBUTES, scope.EDITABLE_MAILING_ADDRESS_ATTRIBUTES);
            expect(ngToast.create).not.toHaveBeenCalled();
            User.flush('save');
            expect(ngToast.create).toHaveBeenCalledWith({
                content: 'Documents Saved',
                className: 'success',
            });
        });

        it('should create contextual ngToast when mailing address info has been successfully saved', () => {
            jest.spyOn(ngToast, 'create').mockImplementation(() => {});
            render();
            User.expect('save');
            scope.save(scope.EDITABLE_MAILING_ADDRESS_ATTRIBUTES, scope.EDITABLE_DOCUMENTS_ATTRIBUTES);
            expect(ngToast.create).not.toHaveBeenCalled();
            User.flush('save');
            expect(ngToast.create).toHaveBeenCalledWith({
                content: 'Mailing Address Saved',
                className: 'success',
            });
        });
    });
});
