import 'Careers/angularModule';

angular.module('FrontRoyal.Careers').run([
    '$injector',
    $injector => {
        const $document = $injector.get('$document');
        const $rootScope = $injector.get('$rootScope');
        const WorkExperience = $injector.get('WorkExperience');
        const EducationExperience = $injector.get('EducationExperience');
        const ngToast = $injector.get('ngToast');
        const $route = $injector.get('$route');

        $($document).on('keydown', $event => {
            if ($event.ctrlKey && $event.key === 'p') {
                fillOutCareerProfile();
            }
        });

        function fillOutCareerProfile() {
            angular.merge($rootScope.currentUser.career_profile, careerProfileAttrs());
            _.each(
                {
                    name: 'Lorenzo Ligato',
                    nickname: 'Lorenzo',
                    phone: '+12038043565',
                    avatar_url:
                        'https://uploads.smart.ly/avatars/cf559227c198690392bca23d2466071d/original/cf559227c198690392bca23d2466071d.png',
                    sex: null,
                    ethnicity: null,
                    race: [],
                    how_did_you_hear_about_us: 'other',
                    address_line_1: 'asdfsadf',
                    address_line_2: null,
                    city: 'New York',
                    state: 'New York',
                    zip: 22222,
                    country: 'US',
                    birthdate: '1988-11-01',
                    anything_else_to_tell_us: null,
                    long_term_goal:
                        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
                },
                (val, key) => {
                    $rootScope.currentUser[key] = $rootScope.currentUser[key] || val;
                    $rootScope.currentUser.career_profile[key] = $rootScope.currentUser[key];
                },
            );
            $rootScope.currentUser.career_profile.save().then(() => {
                ngToast.create({
                    content: 'Career profile updated',
                    className: 'success',
                });

                $route.reload();
            });
        }

        function careerProfileAttrs() {
            const mockWorks = [
                WorkExperience.new({
                    professional_organization: {
                        text: 'Foo Inc.',
                    },
                    industry: 'education',
                    role: 'graphic_design',
                    job_title: 'Foo Handler',
                    start_date: new Date().getTime() + 1000, // rails will send us back a timestamp in seconds
                    end_date: new Date().getTime() + 1000, // rails will send us back a timestamp in seconds
                    responsibilities: ['foo', 'bar', 'baz'],
                    featured: true,
                }),
                WorkExperience.new({
                    professional_organization: {
                        text: 'Bar LLC',
                    },
                    industry: 'consumer_products',
                    role: 'devops',
                    job_title: 'Bar Maintainer',
                    start_date: new Date().getTime(), // rails will send us back a timestamp in seconds
                    end_date: new Date().getTime(), // rails will send us back a timestamp in seconds
                    responsibilities: ['res1', 'res2'],
                }),
            ];

            const mockEducations = [
                EducationExperience.new({
                    educational_organization: {
                        text: 'Bar State University',
                    },
                    graduation_year: 2010,
                    degree: 'Doctorate in Bar',
                    major: 'Bar Science',
                    minor: 'Handling Bar',
                    gpa: '4.0',
                    degree_program: true,
                    will_not_complete: false,
                }),
                EducationExperience.new({
                    educational_organization: {
                        text: 'University of Foo',
                    },
                    graduation_year: 1999,
                    degree: 'Bachelor of Foo',
                    major: 'Foo Science',
                    minor: 'Bar Engineering',
                    gpa: '3.5',
                    degree_program: true,
                    will_not_complete: false,
                }),
            ];

            return {
                program_type: 'mba',
                score_on_gmat: null,
                score_on_sat: '1200',
                score_on_act: null,
                short_answers: {},
                willing_to_relocate: true,
                open_to_remote_work: true,
                authorized_to_work_in_us: 'f1visa',
                top_mba_subjects: ['finance_and_accounting', 'strategy_and_innovation', 'entrepreneurship'],
                top_personal_descriptors: ['creative', 'driven', 'meticulous'],
                top_motivations: ['personal_growth', 'career_advancement', 'helping_others'],
                top_workplace_strengths: ['business_acumen', 'problem_solving', 'communication'],
                bio:
                    'I possess a strong sense of entrepreneurship, a solid work ethic and the courage to challenge established thinking. I want to build on these skills to drive innovation and effect positive change.',
                personal_fact: 'I can read Latin & Ancient Greek.',
                preferred_company_culture_descriptors: ['results_oriented', 'stable', 'merit_based'],
                li_profile_url: 'https://www.linkedin.com/in/ligatolorenzo',
                resume_id: '369d4249-4094-4904-939b-18637b0e5c31',
                personal_website_url: null,
                blog_url: null,
                tw_profile_url: null,
                fb_profile_url: null,
                job_sectors_of_interest: ['consulting', 'financial', 'technology'],
                employment_types_of_interest: ['internship', 'part_time', 'contract', 'permanent'],
                company_sizes_of_interest: ['25-99', '100-499', '500_plus', '500-4999', '5000_plus'],
                primary_areas_of_interest: ['finance', 'general_management', 'sales'],
                place_id: 'ChIJOwg_06VPwokRYv534QaPC8g',
                place_details: {
                    formatted_address: 'Memphis, TN, USA',
                    administrative_area_level_1: {
                        short: 'TN',
                    },
                    country: {
                        short: 'US',
                    },
                },
                last_calculated_complete_percentage: 100.0,
                industry: 'consulting',
                sat_max_score: 1600,
                score_on_gre_verbal: null,
                score_on_gre_quantitative: null,
                score_on_gre_analytical: null,
                do_not_create_relationships: false,
                education_experiences: mockEducations,
                work_experiences: mockWorks,
                skills: [
                    {
                        id: '472c76aa-6fe0-4dc3-aa2e-39250fdba0fa',
                        created_at: '2016-09-26T22:46:28.748Z',
                        updated_at: '2016-09-26T22:46:28.748Z',
                        type: 'skill',
                        text: 'Software Engineering',
                        locale: 'en',
                        suggest: true,
                    },
                    {
                        id: 'f66aa3fd-5b4b-497c-a34e-4017ae3527b0',
                        created_at: '2016-09-26T22:46:28.748Z',
                        updated_at: '2016-09-26T22:46:28.748Z',
                        type: 'skill',
                        text: 'Ingeniería de software',
                        locale: 'es',
                        suggest: true,
                    },
                    {
                        id: '54b63a35-4c91-4032-9d99-688a34e1bb37',
                        created_at: '2016-09-26T22:46:28.748Z',
                        updated_at: '2016-09-26T22:46:28.748Z',
                        type: 'skill',
                        text: '软件工程',
                        locale: 'zh',
                        suggest: true,
                    },
                    {
                        id: '67198e13-bcda-4b19-aaaa-737dd5717853',
                        created_at: '2016-09-26T22:46:28.748Z',
                        updated_at: '2016-09-26T22:46:28.748Z',
                        type: 'skill',
                        text: 'Management',
                        locale: 'en',
                        suggest: true,
                    },
                ],
                awards_and_interests: [
                    {
                        id: 'ec18b747-97cb-4d79-abb1-7b42e567d5cf',
                        created_at: '2016-09-30T22:35:48.818Z',
                        updated_at: '2016-09-30T22:35:48.818Z',
                        type: 'awards_and_interest',
                        text: 'Soccer',
                        locale: 'en',
                        suggest: false,
                    },
                    {
                        id: 'acd8871f-ae7e-4f24-acb6-47c2dfc36b86',
                        created_at: '2016-10-01T01:16:08.252Z',
                        updated_at: '2016-10-01T01:16:08.252Z',
                        type: 'awards_and_interest',
                        text: 'Crossfit',
                        locale: 'en',
                        suggest: false,
                    },
                    {
                        id: '5fcc1bcb-8b1d-4de3-92e5-db45a184fa2e',
                        created_at: '2016-10-01T01:16:08.269Z',
                        updated_at: '2016-10-01T01:16:08.269Z',
                        type: 'awards_and_interest',
                        text: 'Rugby',
                        locale: 'en',
                        suggest: false,
                    },
                    {
                        id: 'e046493a-8c14-4bd0-97bf-77cd2847642f',
                        created_at: '2016-10-01T01:16:08.277Z',
                        updated_at: '2016-10-01T01:16:08.277Z',
                        type: 'awards_and_interest',
                        text: 'Reading',
                        locale: 'en',
                        suggest: false,
                    },
                ],
                short_answer_why_pursuing: 'asdasd',
                short_answer_use_skills_to_advance: 'asdasd',
                short_answer_ask_professional_advice: 'asdasd',
                short_answer_greatest_achievement: 'asdasd',
                short_answer_leadership_challenge: 'asdasd',
                primary_reason_for_applying: 'expand_business_knowledge',
                salary_range: 'over_200000',
                locations_of_interest: ['denver'],
                interested_in_joining_new_company: 'neutral',
                consider_early_decision: true,
                native_english_speaker: true,
                earned_accredited_degree_in_english: null,
                has_no_formal_education: false,
                survey_years_full_time_experience: '3_6',
                survey_most_recent_role_description: 'professional_role',
                survey_highest_level_completed_education_description: 'undergraduate_bachelor',
            };
        }
    },
]);
