import angularModule from 'StudentNetwork/angularModule/scripts/student_network_module';
import { setupBrandNameProperties } from 'AppBrandMixin';
import template from 'StudentNetwork/angularModule/views/partial_access_modal.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('partialAccessModal', [
    '$injector',

    function factory($injector) {
        const $rootScope = $injector.get('$rootScope');
        const dateHelper = $injector.get('dateHelper');
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                dismiss: '&',
            },
            link(scope) {
                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                NavigationHelperMixin.onLink(scope);
                setupBrandNameProperties($injector, scope);

                scope.showApply =
                    (!scope.currentUser.hasEverApplied ||
                        scope.currentUser.isRejected ||
                        scope.currentUser.isExpelled) &&
                    (scope.currentUser.programType === 'mba' || scope.currentUser.programType === 'emba');

                if (scope.showApply) {
                    const applicableAdmissionRound = scope.currentUser.relevant_cohort.getApplicableAdmissionRound();
                    scope.admissionDeadline =
                        applicableAdmissionRound &&
                        dateHelper.formattedUserFacingMonthDayLong(applicableAdmissionRound.applicationDeadline);
                }
            },
        };
    },
]);
