import angularModule from 'StudentNetwork/angularModule/scripts/student_network_module';

angularModule.factory('StudentNetworkMapViewModel', [
    '$injector',

    function factory($injector) {
        const SuperModel = $injector.get('SuperModel');
        const StudentsMapLayer = $injector.get('StudentsMapLayer');
        const EventsMapLayer = $injector.get('EventsMapLayer');
        const $window = $injector.get('$window');
        const GoogleOverlayView = $injector.get('GoogleOverlayView');
        const guid = $injector.get('guid');
        const $timeout = $injector.get('$timeout');
        const Cohort = $injector.get('Cohort');
        const DialogModal = $injector.get('DialogModal');
        const $location = $injector.get('$location');
        const StudentNetworkEvent = $injector.get('StudentNetworkEvent');
        const $q = $injector.get('$q');

        let googleMaps;

        const StudentNetworkMapViewModel = SuperModel.subclass(function () {
            this.extend({
                // There are some things that cannot be initialized until
                // the google maps api is loaded.  network-map-dir is responsible
                // for doing this before initializing a StudentNetworkMapViewModel
                onGoogleApiLoaded() {
                    if (!this.ranOnGoogleApiLoaded) {
                        GoogleOverlayView.onGoogleApiLoaded();
                        this.ranOnGoogleApiLoaded = true;
                        googleMaps = $window.google.maps;
                    }
                },
            });

            Object.defineProperty(this.prototype, 'loadingData', {
                get() {
                    return this.dataLayers.some(layer => layer.loading);
                },
            });

            return {
                initialize(map, eventId) {
                    if (!StudentNetworkMapViewModel.ranOnGoogleApiLoaded) {
                        throw new Error('onGoogleApiLoaded has not yet been called.');
                    }
                    this.map = map;
                    this._id = guid.generate();
                    this.eventId = eventId;

                    this.studentsMapLayer = new StudentsMapLayer(map);
                    this.eventsMapLayer = new EventsMapLayer(map, this);

                    this.dataLayers = [this.studentsMapLayer, this.eventsMapLayer];

                    // set up event listeners
                    this._listeners = [
                        googleMaps.event.addListener(map, 'idle', _.debounce(this._onIdleEvent.bind(this), 500)),
                    ];
                    $($window).on(`resize.${this._id}`, this._updateMapSize.bind(this));

                    // initially fetch and render the data
                    $timeout(() => {
                        this.setDefaultActiveLayer();
                    }, 500);
                },

                setDefaultActiveLayer() {
                    // If the user navigated to the student network via a URL
                    // with an `event-id` query parameter (passed in through
                    // $routeProvider), then we need to set the default layer
                    // to the "events" layer, and kick off the process of fetching
                    // and showing that particular event'd details.
                    if (this.eventId) {
                        this.setActiveMapLayer(this.eventsMapLayer);
                        this.fetchEventAndShowDetails();
                    } else {
                        this.setActiveMapLayer(this.studentsMapLayer);
                    }
                },

                setActiveMapLayer(layer) {
                    if (this.activeMapLayer === layer) {
                        return;
                    }

                    this.activeMapLayer = layer;
                    this.activeMapLayer.activate();
                    this.activeMapLayer.refresh();
                    _.chain(this.dataLayers).without(this.activeMapLayer).invoke('deactivate');
                },

                updateMyClassCohortId(currentUser) {
                    const application = currentUser && currentUser.acceptedOrPreAcceptedCohortApplication;

                    // Note: if you are deferred and pre-accepted, we don't want you to be able to filter by My Class and potentially very few
                    // people in the network (since you might be pre-accepted for a future cohort we haven't yet pre-accepted most people for)
                    // This could change if we changed our policy of accepting everybody at once.
                    // Note: someone can be both accepted and !hasStudentNetworkAccess if their graduation_status is failed
                    if (
                        currentUser &&
                        currentUser.hasStudentNetworkAccess &&
                        application &&
                        application.status === 'accepted' &&
                        Cohort.canFilterNetworkForMyClass(application.program_type)
                    ) {
                        _.each(this.dataLayers, layer => {
                            layer.myClassCohortId = application.cohort_id;
                        });
                    } else {
                        _.each(this.dataLayers, layer => {
                            layer.myClassCohortId = null;
                        });
                    }
                },

                destroy() {
                    _.invoke(this.dataLayers, 'deactivate');
                    _.invoke(this._listeners, 'remove');
                    $($window).off(`resize.${this._id}`);
                    DialogModal.hideAlerts();
                },

                //--------------------
                // Event details
                //--------------------

                fetchEventAndShowDetails() {
                    this.fetchEventForId(this.eventId)
                        .then(response => {
                            if (response.result[0]) {
                                this.showEventDetailsModal(response.result[0]);
                            } else {
                                return $q.reject();
                            }
                        })
                        .catch(() => {
                            $location.search('event-id', null);
                        });
                },

                fetchEventForId(eventId) {
                    return StudentNetworkEvent.index({
                        filters: {
                            id: eventId,
                            start_time: 0,
                            end_time: new Date('2099/01/01').getTime(),
                            include_tbd: true,
                        },
                    })
                        .then(response => $q.resolve(response))
                        .catch(() => $q.reject());
                },

                showEventDetailsModal(event) {
                    if (!event) {
                        return;
                    }

                    $location.search('event-id', event.id);
                    DialogModal.alert({
                        content:
                            '<student-network-event-details event="event" container-selector="containerSelector"></student-network-event-details>',
                        blurTargetSelector: '#app-main-container',
                        classes: [
                            'no-body-padding',
                            'small-close-button',
                            'student-network-event-details',
                            'clickable-app-header-menu',
                        ],
                        animated: false,
                        closeOnOutsideClick: true,
                        close: () => {
                            $location.search('event-id', null);
                        },
                        scope: {
                            event,
                            containerSelector: 'dialog-modal-alert .modal-content',
                        },
                    });
                },

                //--------------------
                // Map Interactions
                //--------------------

                _onIdleEvent() {
                    if (!this.activeMapLayer.supportsRefreshOnIdleMapEvent) {
                        return;
                    }

                    this.activeMapLayer.refresh();
                },

                //--------------------
                // Map Resizing
                //--------------------

                _updateMapSize() {
                    const center = this.map.getCenter();
                    googleMaps.event.trigger(this.map, 'resize');
                    this.map.setCenter(center);
                },
            };
        });

        return StudentNetworkMapViewModel;
    },
]);
