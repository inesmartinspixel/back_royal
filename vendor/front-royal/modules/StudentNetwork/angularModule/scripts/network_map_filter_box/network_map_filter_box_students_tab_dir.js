import angularModule from 'StudentNetwork/angularModule/scripts/student_network_module';
import template from 'StudentNetwork/angularModule/views/network_map_filter_box_students_tab.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('networkMapFilterBoxStudentsTab', [
    '$injector',

    function factory($injector) {
        const $rootScope = $injector.get('$rootScope');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                studentsMapLayer: '<',
            },
            link(scope) {
                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                // shown if "All Students" top-level filter selected
                scope.programTypeFilters = [
                    {
                        key: 'mba',
                        label: 'MBA',
                    },
                    {
                        key: 'emba',
                        label: 'EMBA',
                    },
                ];

                //--------------------
                // Watches
                //--------------------

                // on the scope to enable testing
                scope.applyFilters = value => {
                    scope.studentsMapLayer.applyFilters(value);
                };
                const _applyFiltersDebounced = _.debounce(scope.applyFilters, 250);
                scope.$watch('studentsMapLayer.filters', _applyFiltersDebounced, true);

                //--------------------
                // Advanced Searching / Filtering
                //--------------------

                scope.openAdvancedSearch = () => {
                    scope.studentsMapLayer.inAdvancedSearchMode = true;
                    if (scope.isMobile) {
                        scope.studentsMapLayer.mobileState.expanded = true;
                    }
                };
            },
        };
    },
]);
