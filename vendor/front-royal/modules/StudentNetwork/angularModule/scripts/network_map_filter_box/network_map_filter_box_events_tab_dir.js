import angularModule from 'StudentNetwork/angularModule/scripts/student_network_module';
import template from 'StudentNetwork/angularModule/views/network_map_filter_box_events_tab.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

import iconAdvancedSearch from 'vectors/icon-advanced-search.svg';
import iconAdvancedSearchWhite from 'vectors/icon-advanced-search-white.svg';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('networkMapFilterBoxEventsTab', [
    '$injector',

    function factory($injector) {
        const $rootScope = $injector.get('$rootScope');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                eventsMapLayer: '<',
            },
            link(scope) {
                scope.iconAdvancedSearch = iconAdvancedSearch;
                scope.iconAdvancedSearchWhite = iconAdvancedSearchWhite;

                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });
            },
        };
    },
]);
