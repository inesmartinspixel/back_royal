import angularModule from 'StudentNetwork/angularModule/scripts/student_network_module';
import template from 'StudentNetwork/angularModule/views/network_map_filter_box_advanced_student_search.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

import studentsSvg from 'vectors/students.svg';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('networkMapFilterBoxAdvancedStudentSearch', [
    '$injector',

    function factory($injector) {
        const $rootScope = $injector.get('$rootScope');
        const isMobileMixin = $injector.get('isMobileMixin');
        const TranslationHelper = $injector.get('TranslationHelper');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                studentsMapLayer: '<',
            },
            link(scope) {
                scope.studentsSvg = studentsSvg;

                //--------------------
                // Setup and Initialization
                // -------------------

                isMobileMixin.onLink(scope);

                // proxy is used to track whether the student profile list is showing profiles or not
                scope.proxy = {};

                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                Object.defineProperty(scope, 'totalCount', {
                    get() {
                        const focusedClusterCount =
                            scope.studentsMapLayer.focusedClusterFeature &&
                            scope.studentsMapLayer.focusedClusterFeature.properties.count;
                        const plus = scope.studentsMapLayer.studentProfileFilterSet.initialTotalCountIsMin ? '+' : '';
                        return (
                            focusedClusterCount ||
                            (scope.studentsMapLayer.studentProfileFilterSet.any
                                ? scope.studentsMapLayer.studentProfileFilterSet.initialTotalCount + plus
                                : 0)
                        );
                    },
                });

                const translationHelper = new TranslationHelper('student_network.network_map_filter_box');

                scope.classFilterOptions = [
                    {
                        value: null,
                        label: translationHelper.get('all_classes'),
                    },
                    {
                        value: 'mba',
                        label: 'MBA',
                    },
                    {
                        value: 'emba',
                        label: 'EMBA',
                    },
                ];

                scope.graduationStatusFilterOptions = [
                    {
                        value: null,
                        label: translationHelper.get('graduation_status'),
                    },
                    {
                        value: true,
                        label: translationHelper.get('graduated'),
                    },
                    {
                        value: false,
                        label: translationHelper.get('not_graduated'),
                    },
                ];

                //--------------------
                // Watches
                // -------------------

                // Watch for this event and refresh the advanced search
                scope.$on('searchByInterestOnly', (event, interest) => {
                    scope.studentsMapLayer.searchByInterestOnly(interest);
                });

                scope.$watch('studentsMapLayer.myClassCohortId', cohortId => {
                    if (cohortId) {
                        // add it to advanced filter options but only once. This gets called
                        // with every ping because of the watch it's on
                        if (
                            !_.findWhere(scope.classFilterOptions, {
                                value: 'mine',
                            })
                        ) {
                            scope.classFilterOptions.splice(1, 0, {
                                value: 'mine',
                                label: translationHelper.get('mine'),
                            });
                        }
                    }
                });

                function applyFilters(value) {
                    scope.studentsMapLayer.applyFilters(value);
                }
                const _applyFiltersDebounced = _.debounce(applyFilters, 250);
                scope.$watch('studentsMapLayer.filters', _applyFiltersDebounced, true);

                //--------------------
                // Methods called from the UI
                // -------------------

                scope.removeFocusedCluster = () => {
                    scope.studentsMapLayer.focusedClusterFeature = null;
                };

                scope.applyAdvancedFilters = () => {
                    scope.studentsMapLayer.applyAdvancedFilters();
                };

                scope.returnToMapCallback = () => {
                    scope.studentsMapLayer.inAdvancedSearchMode = false;
                };
            },
        };
    },
]);
