import angularModule from 'StudentNetwork/angularModule/scripts/student_network_module';
import template from 'StudentNetwork/angularModule/views/student_network_event_list.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('studentNetworkEventList', [
    '$injector',
    function factory($injector) {
        const $rootScope = $injector.get('$rootScope');
        const TranslationHelper = $injector.get('TranslationHelper');

        return {
            restrict: 'E',
            scope: {
                events: '<',
                eventsMapLayer: '<',
                listContainerClasses: '<?',
                listItemClasses: '<?',
                reverseOrder: '<?',
            },
            templateUrl,

            link(scope) {
                const dateTbdLabel = new TranslationHelper('student_network.student_network_event_list').get(
                    'date_tbd_label',
                );

                scope.reverseOrder = scope.reverseOrder || false;

                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                function getDateLabelForEvent(event) {
                    return event.date_tbd
                        ? dateTbdLabel
                        : event.formattedRelativeStartDayOrDateTime('dddd, MMMM D, YYYY', scope.currentUser.timezone);
                }

                scope.$watch('events', () => {
                    // We need to sort by `start_time` so we can display events in chronological order.
                    // We need to group by the relative date so we can group events on the same day together.
                    let sortedEvents = _.sortBy(scope.events, e => e.start_time || Number.MAX_SAFE_INTEGER);
                    sortedEvents = scope.reverseOrder ? sortedEvents.reverse() : sortedEvents;
                    scope.indexedAndGroupedEvents = _.groupBy(sortedEvents, event => getDateLabelForEvent(event));
                    scope.eventDates = Object.keys(scope.indexedAndGroupedEvents);
                });
            },
        };
    },
]);
