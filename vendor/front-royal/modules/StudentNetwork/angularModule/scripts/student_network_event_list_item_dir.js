import angularModule from 'StudentNetwork/angularModule/scripts/student_network_module';
import template from 'StudentNetwork/angularModule/views/student_network_event_list_item.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('studentNetworkEventListItem', [
    '$injector',
    function factory($injector) {
        const $rootScope = $injector.get('$rootScope');
        const safeApply = $injector.get('safeApply');

        return {
            restrict: 'E',
            scope: {
                event: '<',
                eventsMapLayer: '<?',
            },
            templateUrl,

            link(scope, elem) {
                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                Object.defineProperty(scope, 'wideLayout', {
                    get() {
                        return elem.hasClass('wide');
                    },
                });

                Object.defineProperty(scope, 'formattedDate', {
                    get() {
                        if (scope.event.multiDay) {
                            return `${scope.event.formattedStartDateTime(
                                'ddd, MMM D',
                                scope.currentUser.timezone,
                            )} — ${scope.event.formattedEndDateTime('ddd, MMM D', scope.currentUser.timezone)}`;
                        }
                        if (scope.event.anonymized) {
                            return scope.event.formattedStartDateTime('ddd, MMMM D', scope.currentUser.timezone);
                        }
                        return scope.event.formattedStartDateTime('ddd, MMMM D h:mm A z', scope.currentUser.timezone);
                    },
                });

                //----------------------------
                // Image/placeholder handling
                //----------------------------

                scope.$watch('event.imageSrc', src => {
                    if (!src) {
                        scope.imageLoadError = true;
                        scope.eventListItemImageLoaded = true;
                    } else {
                        scope.imageLoadError = false;
                        scope.eventListItemImageLoaded = false;
                    }
                });

                // `imageLoadError` is set by the on-image-load directive, which is used
                // on the event image in the header
                scope.$watch('imageLoadError', () => {
                    if (scope.imageLoadError) {
                        scope.onEventListItemImageLoad();
                    }
                });

                scope.onEventListItemImageLoad = () => {
                    // The `eventListItemImageLoaded` flag is used to control the visibility of the list item's details
                    // and footer sections. When the flag gets flipped to true, we render the event those sections.
                    scope.eventListItemImageLoaded = true;
                    safeApply(scope);
                };

                //----------------------------
                // List item interactions
                //----------------------------

                scope.onViewEvent = () => {
                    // We don't pass in an eventsMapLayer
                    // in the admin.
                    if (scope.eventsMapLayer && !scope.event.anonymized) {
                        scope.eventsMapLayer.onViewEvent(scope.event);
                    }
                };
            },
        };
    },
]);
