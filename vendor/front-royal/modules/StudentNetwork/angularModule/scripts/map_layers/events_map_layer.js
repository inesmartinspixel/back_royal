import angularModule from 'StudentNetwork/angularModule/scripts/student_network_module';
import 'FrontRoyalUiBootstrap/popover';
import getDistanceBetweenElementCenters from 'getDistanceBetweenElementCenters';

/*
    This class is responsible for loading and setting up the map data layer representing student events.
*/
angularModule.factory('EventsMapLayer', [
    '$injector',
    $injector => {
        const SuperModel = $injector.get('SuperModel');
        const TranslationHelper = $injector.get('TranslationHelper');
        const StudentNetworkEvent = $injector.get('StudentNetworkEvent');
        const $timeout = $injector.get('$timeout');
        const $window = $injector.get('$window');
        const $document = $injector.get('$document');
        const isMobile = $injector.get('isMobile');
        const $q = $injector.get('$q');

        return SuperModel.subclass(function () {
            Object.defineProperty(this.prototype, 'eventTypesAvailableForMapFilters', {
                get() {
                    if (!_.any(this.$$eventTypesAvailableForMapFilters)) {
                        this._setEventTypesAvailableForMapFilters();
                    }
                    return this.$$eventTypesAvailableForMapFilters;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'showRecommendedEventsList', {
                get() {
                    return (
                        this.activated && ($window.innerWidth >= 992 || this.mobileState.showingRecommendedEventsList)
                    );
                },
            });

            // The StudentNetworkMapViewModel triggers a refresh of the active map layer when the
            // user has stopped interacting with the map for a certain amount of time i.e. when the
            // map has become "idle". This is useful in the Students & Alumni tab because the data
            // in the map is dependent on map zoom level and the sidebar depends on this data as
            // well. However, for the Events tab, the map’s zoom level doesn’t mean that we have to
            // trigger a refresh because the data on the map is loaded up all at once when the events
            // map layer is first activated. This flag disables this behavior so that we can be more
            // performant and avoid unnecessary API calls here for the events map layer.
            Object.defineProperty(this.prototype, 'supportsRefreshOnIdleMapEvent', {
                value: false,
            });

            Object.defineProperty(this.prototype, 'currentRecommendedEventsListTab', {
                get() {
                    if (!this.$$currentRecommendedEventsListTab) {
                        this.$$currentRecommendedEventsListTab = 'upcoming';
                    }
                    return this.$$currentRecommendedEventsListTab;
                },
                set(val) {
                    this.$$currentRecommendedEventsListTab = val;
                },
                configurable: true,
            });

            return {
                initialize(map, studentNetworkMapViewModel) {
                    this.map = map;
                    this.studentNetworkMapViewModel = studentNetworkMapViewModel;
                    this.name = 'events';

                    this._clearEventsForRecommendedEventsList();

                    // There are two separate sets of filters that the EventsMapLayer needs to keep track of.
                    // The `serverFilters` object is intended for server-side filtering via the recommended
                    // events list, while the `mapFilters` object is intended for client-side filtering of
                    // the events displayed on the map.
                    this.resetServerFilters();
                    this.mapFilters = {};
                    this.refreshFilters = {
                        include_tbd: true,
                    };

                    this.loading = 0;

                    this.advancedSearchOpen = false;

                    // state tracking for the mobile view
                    this.mobileState = {
                        // On desktop, the list is always active (there is no
                        // way to show/hide it). However, on mobile, the list
                        // is manually shown by clicking on 'recommended events'
                        // in the filter box.
                        showingRecommendedEventsList: false,
                    };

                    // Note: unlike the students map layer, the events shown on the map are always independent of the events shown on the right-hand
                    // recommended events sidebar. Need to keep this in mind when manage data loading in this model. For example, we might do something
                    // like initially load all of the events the user can see, then split it into those that are viewable on the map (eventFeatures) and
                    // those that are viewable in the recommended events sidebar (everything, including online events). From there on out, any changes to
                    // recommended event filters or map filters would indepdently refresh their respective lists of events. Or, maybe we never refresh the
                    // map's list of events, instead doing client-side filtering by event type, and only refresh the sidebar event list from there on out?

                    this.translationHelper = new TranslationHelper('student_network.network_map_filter_box');
                    this._updateSelectedMapFiltersSummary();
                },

                deactivate() {
                    this.activated = false;
                    this._clearEventFeatures();
                    this._clearEventsForRecommendedEventsList();
                    this._clearEventTypesAvailableForMapFilters();
                    this.loading = 0;

                    this.mobileState.showingRecommendedEventsList = false;

                    this.closeAllAmbiguousEventMarkerPopovers();
                    this._enableMapGestureHandling();

                    this.currentRecommendedEventsListTab = null;
                },

                activate() {
                    // this is where we'd set up listeners, if necessary
                    this.activated = true;
                },

                refresh() {
                    if (this.loading || !this.activated) {
                        return;
                    }

                    return this._loadEvents()
                        .catch(err => {
                            if (err) {
                                this.loading -= 1;
                            }
                            return null;
                        })
                        .then(response => {
                            if (response && this.activated) {
                                this.events = response.result;

                                this.resetMapFilters();
                                this._updateSelectedMapFiltersSummary();

                                // Compute the `eventTypesAvailableForMapFilters` ahead of time so that
                                // they're ready for use if/when the `showEventTypeFilters` method is called.
                                this._setEventTypesAvailableForMapFilters();

                                this.resetServerFilters();
                                this._updateEventsForRecommendedEventsList(this.events);

                                this.loading -= 1;
                            }
                        });
                },

                resetServerFilters() {
                    this.serverFilters = {
                        include_tbd: true,
                        end_time: undefined,
                        keyword_search: undefined,
                        places: undefined, // the server expects this filter to be plural, but the UI only supports filtering by a single place
                        event_type: [], // the server expects this filter to be singular, but the UI supports filtering by multiple event types
                    };
                },

                resetMapFilters() {
                    this._updateAndApplyMapFilters({
                        eventTypes: [],
                    });
                },

                //--------------------
                // Event marker interactions
                //--------------------

                // When an event-marker is clicked, one of three things happens.
                // 1. Nothing happens becuse the event is anonymized, or
                // 2. A modal popup with event details is shown, or
                // 3. A popover with any visually ambiguous event features is shown
                onEventMarkerClick(eventFeature) {
                    if (eventFeature.anonymized) {
                        return;
                    }

                    const event = this._getEventForEventId(eventFeature.uid);
                    const visuallyAmbiguousEvents = this._getVisuallyAmbiguousEvents(eventFeature.uid);

                    if (_.any(visuallyAmbiguousEvents)) {
                        // If we found any ambiguous events, we also want to be sure to include the event
                        // the user actually clicked on (which won't be in visuallyAmbiguousEventIds).
                        visuallyAmbiguousEvents.unshift(event);
                        eventFeature.ambiguousEvents = visuallyAmbiguousEvents;
                        const showPopover = () => {
                            this.openAmbiguousEventMarkerPopover(eventFeature);
                        };

                        if (isMobile()) {
                            showPopover();
                        } else {
                            // There's a chance the event-marker isn't in an optimal location. Just in case,
                            // we want to move the map such that the ambiguous popover isn't cut-off
                            const placeDetails = event.place_details || event.place_details_anonymized;
                            const latlng = new $window.google.maps.LatLng(placeDetails.lat, placeDetails.lng);

                            // Get the pixel-based point so we can construct a bounds in screen-space
                            const scale = 2 ** this.map.getZoom();
                            const worldCoordinateEventMarker = this.map.getProjection().fromLatLngToPoint(latlng);

                            // Construct a bounds object based on the SW and NE corner of the popover
                            // See student_network_event_ambiguous_popover.scss for these dimensions
                            const popoverWidth = 317;
                            const popoverHeight = 479;
                            const popoverArrowWidth = 20;
                            const worldCoordinateSW = new $window.google.maps.Point(
                                worldCoordinateEventMarker.x,
                                worldCoordinateEventMarker.y - (popoverHeight * 0.5) / scale,
                            );
                            const worlCoordinateNE = new $window.google.maps.Point(
                                worldCoordinateEventMarker.x + (popoverWidth + popoverArrowWidth) / scale,
                                worldCoordinateEventMarker.y + (popoverHeight * 0.5) / scale,
                            );
                            const latLngBounds = new $window.google.maps.LatLngBounds(
                                this.map.getProjection().fromPointToLatLng(worldCoordinateSW),
                                this.map.getProjection().fromPointToLatLng(worlCoordinateNE),
                            );
                            const padding = {
                                bottom: 10,
                                left: 10,
                                right: 10,
                                top: 10,
                            };

                            // Strategy: listen for the idle event from the map to know when the panToBounds is done.
                            // However, if panToBounds doesn't move the map, there will be no idle event dispatched.
                            // So, also measure the center now, and check it again soon after. If it hasn't changed,
                            // we can know with high certainty it's not going to. In that case, cancel the idle listener
                            // and just invoke showPopover.
                            const idleListener = $window.google.maps.event.addListenerOnce(
                                this.map,
                                'idle',
                                showPopover,
                            );
                            const oldCenter = this.map.getCenter();

                            // finally: animate the map to make the popover have enough space
                            this.map.panToBounds(latLngBounds, padding);

                            $timeout(() => {
                                const newCenter = this.map.getCenter();
                                if (newCenter.equals(oldCenter)) {
                                    $window.google.maps.event.removeListener(idleListener);
                                    showPopover();
                                }
                            }, 100);
                        }
                    } else {
                        this.closeAmbiguousEventMarkerPopover(eventFeature);
                        this.showEventDetails(event);
                    }
                },

                //--------------------
                // Event details
                //--------------------

                onViewEvent(event) {
                    // If we're viewing a mappable event, there's a chance we clicked
                    // "view" from inside a popover. The popover does not necessarily belong
                    // to the corresponding event-marker for the event we want to view so,
                    // to be safe, we just close any open popovers.
                    if (event.mappable) {
                        this.closeAllAmbiguousEventMarkerPopovers();
                    }
                    this.showEventDetails(event);
                },

                showEventDetails(event) {
                    if (!event || !_.any(this.events)) {
                        return;
                    }

                    this.showingEventTypeFiltersModal = false;

                    this.studentNetworkMapViewModel.showEventDetailsModal(event);
                },

                showEventTypeFilters() {
                    this.showingEventTypeFiltersModal = true;
                },

                applyEventTypeFilters(newEventTypes) {
                    this.showingEventTypeFiltersModal = false;
                    this._updateAndApplyMapFilters({
                        eventTypes: newEventTypes,
                    });
                },

                //--------------------
                // Popover handling
                //--------------------

                openAmbiguousEventMarkerPopover(eventFeature) {
                    $document.one(
                        'mousedown.events.disambiguation.popover',
                        this.handeClickOnDocumentWhilePopoverVisible.bind(this),
                    );
                    $timeout(() => {
                        eventFeature.popoverIsOpen = true;
                        this.handleGestureControlsOnPopoverStateChange();
                    });
                },

                closeAmbiguousEventMarkerPopover(eventFeature) {
                    $document.off('mousedown.events.disambiguation.popover');

                    $timeout(() => {
                        eventFeature.popoverIsOpen = false;
                        this.handleGestureControlsOnPopoverStateChange();
                    });
                },

                closeAllAmbiguousEventMarkerPopovers() {
                    _.each(this.eventFeatures, eventFeature => {
                        if (eventFeature.popoverIsOpen) {
                            this.closeAmbiguousEventMarkerPopover(eventFeature);
                        }
                    });
                },

                handeClickOnDocumentWhilePopoverVisible(event) {
                    const popover = $('.student-network-event-ambiguous-popover.popover');

                    // make sure the tooltip/popover link or tool tooltip/popover itself were not clicked
                    if (popover && popover.length && !popover[0].contains(event.target)) {
                        this.closeAllAmbiguousEventMarkerPopovers();
                    }
                },

                handleGestureControlsOnPopoverStateChange() {
                    // If any popovers are showing, we should disable gesture controls.
                    // If no popovers are showing, gesture controls should be enabled.
                    if (
                        !_.findWhere(this.eventFeatures, {
                            popoverIsOpen: true,
                        })
                    ) {
                        this._enableMapGestureHandling();
                    } else {
                        this._disableMapGestureHandling();
                    }
                },

                //---------------------
                // Data Loading and Map Features
                //---------------------

                onSearchEventsClick() {
                    this.advancedSearchOpen = true;

                    // We've already loaded up all of the events that would be shown in the 'upcoming' tab
                    // because those are the events that are loaded by the events map layer by default,
                    // so we don't need to bother fetching any events from the server, but for the 'past_events'
                    // tab, we may not have any of the events that would be shown in the events list, so we
                    // need to fetch the events from the server.
                    if (this.currentRecommendedEventsListTab === 'past_events') {
                        this.fetchPastEvents();
                    } else {
                        this._updateEventsForRecommendedEventsList(this.events);
                    }
                },

                fetchCurrentAndUpcomingEvents() {
                    this.serverFilters.end_time = undefined;
                    this.serverFilters.include_tbd = true;
                    this.applyServerFilters();
                },

                fetchPastEvents() {
                    this.serverFilters.end_time = Math.floor(Date.now() / 1000);
                    this.serverFilters.include_tbd = false;
                    this.applyServerFilters();
                },

                applyServerFilters() {
                    return this._loadEvents(this.serverFilters)
                        .catch(err => {
                            if (err) {
                                this.loading -= 1;
                            }
                            return null;
                        })
                        .then(response => {
                            if (response && this.activated) {
                                const fetchedEvents = response.result;
                                this._mergeEventsIntoDataStore(fetchedEvents);
                                this._updateEventsForRecommendedEventsList(fetchedEvents);

                                // Some new events may be been returned from the server that have an event_type
                                // that may not have been included in the `eventTypesAvailableForMapFilters` cache,
                                // so we reset this value to make sure that any new event types are available.
                                this._setEventTypesAvailableForMapFilters();

                                this.loading -= 1;
                            }
                        });
                },

                _loadEvents(filters = this.refreshFilters) {
                    if (!this.map) {
                        // Return false wrapped in a promise to indicate to the caller that we didn't
                        // trigger an API call and therefore didn't increment the loading flag. Therefore,
                        // callers of this method should catch the rejected _loadEvents call and appropriately
                        // decrement the loading flag depending on the rejected value.
                        return $q.reject(false);
                    }

                    this.loading += 1;

                    return StudentNetworkEvent.index({
                        filters,
                    });
                },

                _mergeEventsIntoDataStore(events) {
                    if (!this.events) {
                        this.events = events;
                    } else {
                        this.events = _.uniq(this.events.concat(events), false, event => event.id);
                    }
                },

                _addEventMarkersToMap(events = []) {
                    if (!this.map) {
                        return;
                    }

                    // trigger the view refresh
                    // NOTE: Not all events can be placed on the map, so we need to make sure
                    // to only get the mappable events and the current and upcoming events.
                    const mappableEvents = this._filterForMappableEvents(events);
                    const eventsForEventFeatures = this._filterForCurrentAndUpcomingEvents(mappableEvents);
                    this.eventFeatures = eventsForEventFeatures.map(event => {
                        // We add the uid so that when we refresh data, we don't replace
                        // existing features with identical replacements.  See also the track-by in network_map.html
                        const feature = _.clone(event);

                        // we're not clustering, so in theory the event ID should be good enough here?
                        // The only edge case this wouldn't cover is an event that changes location between refreshes; the dot wouldn't move
                        feature.uid = event.id;

                        return feature;
                    });
                },

                //-------------------------------
                // Map Filter Helper Methods
                //-------------------------------

                _updateAndApplyMapFilters(filters) {
                    this._updateMapFilters(filters);
                    this._updateSelectedMapFiltersSummary();
                    this._applyMapFilters();
                },

                _updateMapFilters(newFilters) {
                    _.extend(this.mapFilters, newFilters);
                },

                _updateSelectedMapFiltersSummary() {
                    const selectedFiltersSummaryParts = [];
                    const studentNetworkEventTranslationHelper = new TranslationHelper(
                        'student_network.student_network_event',
                    );

                    _.each(this.mapFilters, (value, key) => {
                        if (_.any(value)) {
                            if (key === 'eventTypes') {
                                _.each(value, val =>
                                    selectedFiltersSummaryParts.push(
                                        `<span>${studentNetworkEventTranslationHelper.get(val)}</span>`,
                                    ),
                                );
                            }
                        }
                    });

                    this.selectedMapFiltersSummary = _.any(selectedFiltersSummaryParts)
                        ? selectedFiltersSummaryParts.join(' ')
                        : `<span>${this.translationHelper.get('showing_all_events')}</span>`;
                },

                _applyMapFilters() {
                    const events = this._filterEventsByEventType(this.events, this.mapFilters);
                    this._addEventMarkersToMap(events);
                },

                // Filters the passed in `events` for all of the events with an `event_type`
                // contained in the `eventTypes` array property on the passed in `filters`.
                _filterEventsByEventType(events, filters) {
                    if (!filters || !_.any(filters.eventTypes)) {
                        return events;
                    }

                    // create a map of the event types to filter by for better performance
                    const eventTypesMap = _.object(
                        filters.eventTypes,
                        filters.eventTypes.map(() => true),
                    );
                    return _.filter(events, event => eventTypesMap[event.event_type]);
                },

                _filterForMappableEvents(events) {
                    return _.filter(events, event => event.mappable);
                },

                _filterForCurrentAndUpcomingEvents(events) {
                    const now = Date.now();
                    return _.filter(events, event => event.date_tbd || event.end_time * 1000 > now);
                },

                _filterForPastEvents(events) {
                    const now = Date.now();
                    return _.filter(events, event => event.end_time * 1000 <= now);
                },

                _filterForRecommendedEvents(events) {
                    return _.filter(events, event => event.recommended);
                },

                _setEventTypesAvailableForMapFilters() {
                    // Get the event types that should be shown in the event type filters box UI.
                    const eventTypesFromEvents = _.chain(this.events || [])
                        .pluck('event_type')
                        .uniq()
                        .value();
                    const eventTypesFromEventsMap = _.object(
                        eventTypesFromEvents,
                        eventTypesFromEvents.map(() => true),
                    );
                    this.$$eventTypesAvailableForMapFilters = _.chain(
                        Object.keys(StudentNetworkEvent.EVENT_TYPE_CONFIGS_MAP),
                    )
                        .filter(eventType => {
                            const eventTypeConfig = StudentNetworkEvent.EVENT_TYPE_CONFIGS_MAP[eventType];
                            if (!eventTypeConfig.mappable) {
                                return false;
                            }

                            // In this context, certain event types are always shown in the filter box UI,
                            // while others are only made visible if there are any events available with that `event_type`.
                            return eventTypeConfig.visibleInEventTypeFiltersOnlyWhenAny
                                ? eventTypesFromEventsMap[eventType]
                                : true;
                        })
                        .sortBy(
                            eventType => StudentNetworkEvent.EVENT_TYPE_CONFIGS_MAP[eventType].eventTypeFiltersOrder,
                        )
                        .value();
                },

                //------------------------------
                // Additional Helper Methods
                //------------------------------

                // remove old features
                _clearEventFeatures() {
                    this.eventFeatures = undefined;
                },

                _clearEventsForRecommendedEventsList() {
                    this.eventsForRecommendedEventsList = [];
                },

                _clearEventTypesAvailableForMapFilters() {
                    this.$$eventTypesAvailableForMapFilters = undefined;
                },

                _getEventsForEventIds(eventIds) {
                    return _.map(eventIds, eventId => this._getEventForEventId(eventId));
                },

                _getEventForEventId(eventId) {
                    return _.findWhere(this.events, {
                        id: eventId,
                    });
                },

                _getEventFeatureForEventId(eventId) {
                    return _.findWhere(this.eventFeatures, feature => feature.uid === eventId);
                },

                _getVisuallyAmbiguousEvents(eventId) {
                    // This threshold is deliberate. Currently, the icons for event-markers
                    // have dimensions of 20px X 20px. Each icon therefore has a radius of
                    // 10px. If the computed distance between the markers is less than 20,
                    // the icons are definitely overlapping.
                    const distanceThreshold = 20;
                    const thisMarker = $(`event-marker[data-id="${eventId}"]`).get(0);
                    const visuallyAmbiguousEventMarkers = _.reject(
                        $('event-marker').get(),
                        marker => $(marker).attr('data-id') === eventId,
                    ) // Don't include the one that was clicked
                        .filter(marker => getDistanceBetweenElementCenters(thisMarker, marker) <= distanceThreshold); // Do include any other markers within the threshold
                    const visuallyAmbiguousEventIds = _.map(visuallyAmbiguousEventMarkers, marker =>
                        $(marker).attr('data-id'),
                    );
                    return this._getEventsForEventIds(visuallyAmbiguousEventIds);
                },

                // This method determines what events are shown in the main event list in the student network
                // based on the map layer's current state. NOTE: Even though the recommended events list may
                // sound like it's only responsibility is to show the events to the user that are "recommended",
                // it's also responsible allowing the user to filter through ALL current and past events regardless
                // of whether or not they're "recommended" for the user.
                _updateEventsForRecommendedEventsList(events) {
                    const filterMethod =
                        this.currentRecommendedEventsListTab === 'upcoming'
                            ? '_filterForCurrentAndUpcomingEvents'
                            : '_filterForPastEvents';
                    const eventsForCurrentRecommendedEventsListTab = this[filterMethod](events);
                    this.eventsForRecommendedEventsList = this.advancedSearchOpen
                        ? eventsForCurrentRecommendedEventsListTab
                        : this._filterForRecommendedEvents(eventsForCurrentRecommendedEventsListTab);
                },

                //--------------------
                // Ngmap Gesture controls
                //--------------------

                _disableMapGestureHandling() {
                    // Whenever we have a popover open, we disable gesture handling on the map
                    // because the map will usurp any scroll events. It also doesn't make sense
                    // to allow gestures inside of Cordova when a popover is open, because the
                    // popover occupies the entire screen.
                    this.map.setOptions({
                        gestureHandling: 'none',
                    });
                },

                _enableMapGestureHandling() {
                    // Whenever a popover's scope is destroyed, or we deactivate this map layer,
                    // we want to be sure to re-enable gesture handling.
                    this.map.setOptions({
                        gestureHandling: 'auto',
                    });
                },
            };
        });
    },
]);
