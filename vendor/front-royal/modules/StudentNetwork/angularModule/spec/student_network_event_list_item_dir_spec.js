import 'AngularSpecHelper';
import 'StudentNetwork/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import studentNetworkEventListItemLocales from 'StudentNetwork/locales/student_network/student_network_event_list_item-en.json';
import studentNetworkEventLocales from 'StudentNetwork/locales/student_network/student_network_event-en.json';
import moment from 'moment-timezone';

setSpecLocales(studentNetworkEventListItemLocales, studentNetworkEventLocales);

describe('FrontRoyal.StudentNetwork.studentNetworkEventListItem', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let StudentNetworkEvent;
    let event;
    let eventsMapLayer;
    let safeApplySpy;
    let safeApply;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.StudentNetwork', 'SpecHelper');

        angular.mock.module($provide => {
            safeApplySpy = jest.fn();
            safeApply = theScope => {
                safeApplySpy(theScope);
                theScope.$apply();
            };
            $provide.value('safeApply', safeApply);
        });

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                StudentNetworkEvent = $injector.get('StudentNetworkEvent');
            },
        ]);

        SpecHelper.stubCurrentUser();

        event = StudentNetworkEvent.new({
            title: 'foobar',
            event_type: 'meetup',
            location_name: 'A van down by the river',
            start_time: moment.tz('2100-01-01 00:00:00', 'America/New_York').toDate().getTime() / 1000,
            end_time: moment.tz('2100-01-01 02:00:00', 'America/New_York').toDate().getTime() / 1000,
            timezone: 'America/New_York',
        });
        jest.spyOn(event, 'imageSrc', 'get').mockReturnValue('https://127.0.0.1/foo.png/');
        jest.spyOn(event, 'icon', 'get').mockReturnValue({
            src: 'https://127.0.0.1/bar.png/',
            width: 25,
            height: 25,
        });

        eventsMapLayer = {
            onViewEvent: jest.fn(),
        };
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.event = event;
        renderer.scope.eventsMapLayer = eventsMapLayer;
        renderer.render(
            '<student-network-event-list-item event="event" events-map-layer="eventsMapLayer"></student-network-event-list-item>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    describe('onViewEvent', () => {
        it('should call eventsMapLayer.onViewEvent if there is an eventsMapLayer and the event is not anonymized', () => {
            event.anonymized = false;
            render();

            // sanity checks
            expect(scope.event.anonymized).toBe(false);
            expect(scope.eventsMapLayer).toBeTruthy();

            jest.spyOn(eventsMapLayer, 'onViewEvent').mockImplementation(() => {});
            scope.onViewEvent();
            expect(eventsMapLayer.onViewEvent).toHaveBeenCalledWith(scope.event);
            eventsMapLayer.onViewEvent.mockClear();

            scope.event.anonymized = true;
            scope.onViewEvent();
            expect(eventsMapLayer.onViewEvent).not.toHaveBeenCalled();

            scope.event.anonymized = false;
            scope.eventsMapLayer = undefined;
            scope.onViewEvent();
            expect(eventsMapLayer.onViewEvent).not.toHaveBeenCalled();
        });
    });

    describe('list item UI', () => {
        function renderAndSetEventListItemImageLoaded() {
            render();
            scope.eventListItemImageLoaded = true;
            scope.$digest();
        }

        describe('details section', () => {
            beforeEach(() => {
                renderAndSetEventListItemImageLoaded();
            });

            it('should show a thumbnail of the image', () => {
                const img = SpecHelper.expectElement(elem, '.thumbnail img');
                SpecHelper.expectEqual(scope.event.imageSrc, img.prop('src'));
            });

            it('should show the title', () => {
                SpecHelper.expectElementText(elem, 'span.title', 'foobar');
            });

            describe('formatted date', () => {
                it('should show full date if event is not anonymized', () => {
                    SpecHelper.expectElementText(elem, 'span.date', 'Fri, January 1 12:00 AM EST');
                });

                it('should show date without time if event is anonymized', () => {
                    event.anonymized = true;
                    scope.$digest();
                    SpecHelper.expectElementText(elem, 'span.date', 'Fri, January 1');
                });

                it('should show the date TBD description if date TBD', () => {
                    event.date_tbd = true;
                    event.date_tbd_description = 'this date is yet to be determined';
                    event.start_time = undefined;
                    event.end_time = undefined;
                    scope.$digest();
                    SpecHelper.expectElementText(elem, 'span.date', event.date_tbd_description);
                });

                it('should show a multi-day formatted date when applicable', () => {
                    event.end_time = moment.tz('2100-01-02 00:00:00', 'America/New_York').toDate().getTime() / 1000;
                    scope.$digest();
                    SpecHelper.expectElementText(elem, 'span.date', 'Fri, Jan 1 — Sat, Jan 2');
                });
            });

            it('should show location_name', () => {
                SpecHelper.expectElementText(elem, 'span.location', 'A van down by the river');
            });
        });

        describe('footer section', () => {
            it('should show icon URL', () => {
                renderAndSetEventListItemImageLoaded();
                const img = SpecHelper.expectElement(elem, '.footer-flex-container img');
                SpecHelper.expectEqual(scope.event.icon.src, img.prop('src'));
            });

            it('should show translated event_type', () => {
                renderAndSetEventListItemImageLoaded();
                SpecHelper.expectElementText(elem, 'span.type', 'Meetup');
            });

            it('should have a working view button', () => {
                renderAndSetEventListItemImageLoaded();
                const button = SpecHelper.expectElement(elem, '.footer-flex-container button');
                SpecHelper.expectElementText(elem, button, 'View');
                SpecHelper.click(elem, button);
                expect(scope.eventsMapLayer.onViewEvent).toHaveBeenCalled();
            });

            it('should not have a tooltip if event is not anonymized', () => {
                event.anonymized = false;
                renderAndSetEventListItemImageLoaded();
                SpecHelper.expectNoElement(elem, '.footer-flex-container button .tooltip');
            });

            it('should have a tooltip if event is anonymized', () => {
                event.anonymized = true;
                renderAndSetEventListItemImageLoaded();
                SpecHelper.expectElementText(
                    elem,
                    '.footer-flex-container button .tooltip',
                    'Event details are only available to MBA and Executive MBA students and alumni.',
                );
            });

            it('should call eventsMapLayer.onViewEvent when clicked', () => {
                renderAndSetEventListItemImageLoaded();
                SpecHelper.click(elem, '.footer-flex-container button');
                expect(scope.eventsMapLayer.onViewEvent).toHaveBeenCalled();
            });
        });
    });

    describe('image/placeholder handling', () => {
        beforeEach(() => {
            render();
        });

        it('should block the render of details and footer until image is loaded', () => {
            SpecHelper.expectNoElement(elem, '.details-flex-container');
            SpecHelper.expectNoElement(elem, '.footer-flex-container');

            scope.eventListItemImageLoaded = true;
            scope.$digest();

            SpecHelper.expectElement(elem, '.details-flex-container');
            SpecHelper.expectElement(elem, '.footer-flex-container');
        });

        it('should set eventListItemImageLoaded to true and safeApply on image load', () => {
            render();
            const eventListImage = elem.find('img');
            expect(scope.eventListItemImageLoaded).not.toBe(true);
            expect(safeApplySpy).not.toHaveBeenCalled();
            eventListImage.trigger('load');
            expect(scope.eventListItemImageLoaded).toBe(true);
            expect(safeApplySpy).toHaveBeenCalled();
        });

        it('should call onEventListItemImageLoad when imageLoadError', () => {
            render();
            jest.spyOn(scope, 'onEventListItemImageLoad').mockImplementation(() => {});
            expect(scope.imageLoadError).not.toBe(true);
            expect(scope.onEventListItemImageLoad).not.toHaveBeenCalled();
            scope.imageLoadError = true;
            scope.$digest();
            expect(scope.onEventListItemImageLoad).toHaveBeenCalled();
        });

        describe('onEventListItemImageLoad method', () => {
            it('should set eventListItemImageLoaded to true and safeApply', () => {
                render();
                expect(scope.eventListItemImageLoaded).not.toBe(true);
                scope.onEventListItemImageLoad();
                expect(scope.eventListItemImageLoaded).toBe(true);
                expect(safeApplySpy).toHaveBeenCalled();
            });
        });
    });
});
