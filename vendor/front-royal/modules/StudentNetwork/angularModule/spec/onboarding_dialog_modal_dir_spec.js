import 'AngularSpecHelper';
import 'StudentNetwork/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import stubSpecLocale from 'Translation/stubSpecLocale';
import onboardingDialogModalLocales from 'StudentNetwork/locales/student_network/onboarding_dialog_modal-en.json';

stubSpecLocale('email_input.email_validation.please_use_non_relay_email');
setSpecLocales(onboardingDialogModalLocales);

describe('FrontRoyal.StudentNetwork.OnboardingDialogModal', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let user;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.StudentNetwork', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
            },
        ]);

        SpecHelper.stubConfig();

        user = SpecHelper.stubCurrentUser();
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.dimiss = jest.fn();
        renderer.render('<onboarding-dialog-modal dismiss="dismiss()"></onboarding-dialog-modal>');
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    describe('intro', () => {
        it('should work', () => {
            render();
            SpecHelper.expectElement(elem, '.main');
            SpecHelper.expectElement(elem, '.main .title');
            SpecHelper.expectElement(elem, '.main .image');
            SpecHelper.expectElement(elem, '.main .body');
            SpecHelper.expectElement(elem, '.main .dots');
            SpecHelper.expectElement(elem, '.main button');
            SpecHelper.expectElements(elem, '.main .dots li', 4);
        });

        it('should move on to steps two, three, four', () => {
            render();
            SpecHelper.expectHasClass(elem, '.main', 'step-one');
            SpecHelper.click(elem, 'button');
            SpecHelper.expectHasClass(elem, '.main', 'step-two');
            SpecHelper.click(elem, 'button');
            SpecHelper.expectHasClass(elem, '.main', 'step-three');
            SpecHelper.click(elem, 'button');
            SpecHelper.expectHasClass(elem, '.main', 'step-four');
        });

        it('should dismiss after step four if the user is NOT anonymous', () => {
            user.pref_student_network_privacy = 'full';
            render();
            scope.currentStep = 3;
            scope.$digest();
            jest.spyOn(scope, 'dismiss').mockImplementation(() => {});
            SpecHelper.expectHasClass(elem, '.main', 'step-four');
            SpecHelper.click(elem, 'button');
            expect(scope.dismiss).toHaveBeenCalled();
        });

        it('should have proper first image', () => {
            render();
            expect($(elem).find('.image img').eq(0).attr('src')).toEqual('student-network-onboarding-one.png');
        });

        it('should have appropriate image on step 4 for EMBA', () => {
            jest.spyOn(user, 'programType', 'get').mockReturnValue('emba');
            render();
            scope.currentStep = 3;
            scope.$digest();
            expect($(elem).find('.image img').eq(0).attr('src')).toEqual('student-network-onboarding-four.png');
        });

        it('should have appropriate image on step 4 for MBA', () => {
            jest.spyOn(user, 'programType', 'get').mockReturnValue('mba');
            render();
            scope.currentStep = 3;
            scope.$digest();
            expect($(elem).find('.image img').eq(0).attr('src')).toEqual(
                'student-network-onboarding-four-no-conference.png',
            );
        });
    });

    describe('activation', () => {
        beforeEach(() => {
            user.pref_student_network_privacy = 'anonymous';
            render();
            scope.currentStep = 3;
            scope.$digest();
            SpecHelper.expectHasClass(elem, '.main', 'step-four');
            SpecHelper.click(elem, 'button');
        });

        it('should move on to secondary modal after step four if user is anonymous', () => {
            SpecHelper.expectElementText(
                elem,
                '.body',
                'Your student network profile is currently set to anonymous. Would you like to make your profile visible to your peers?',
            );
            SpecHelper.expectElement(elem, '.main .actions');
        });

        it('should activate their profile', () => {
            jest.spyOn(scope, 'activate').mockImplementation(() => {});
            SpecHelper.click(elem, 'button');
            expect(scope.activate).toHaveBeenCalled();
        });
    });

    describe('email', () => {
        it('should present itself as the only step for users where missingRequiredStudentNetworkEmail is true and have already seen the intro', () => {
            jest.spyOn(user, 'missingRequiredStudentNetworkEmail', 'get').mockReturnValue(true);
            user.has_seen_student_network = true;
            render();
            expect(scope.steps).toEqual([{ mode: 'email' }]);
            SpecHelper.expectElementText(
                elem,
                '.body',
                "Just one more thing – please confirm the email address you'd like to receive messages from students and alumni!",
            );
        });

        it('should present itself for users missingRequiredStudentNetworkEmail, have not seen the intro, and elect to transition to full', () => {
            // see also: User::missingRequiredStudentNetworkEmail
            jest.spyOn(user, 'hasStudentNetworkAccess', 'get').mockReturnValue(true);
            user.email = 'anon@privaterelay.appleid.com';
            user.student_network_email = null;
            user.has_seen_student_network = false;
            user.pref_student_network_privacy = 'anonymous';
            user.career_profile = {};

            render();
            expect(scope.steps.length).toBe(5);
            scope.currentStep = 4;
            scope.$digest();

            SpecHelper.expectElementText(
                elem,
                '.body',
                'Your student network profile is currently set to anonymous. Would you like to make your profile visible to your peers?',
            );

            jest.spyOn(scope, 'activate');
            SpecHelper.click(elem, 'button');
            expect(scope.activate).toHaveBeenCalled();
            expect(scope.steps.length).toBe(6);

            SpecHelper.expectElementText(
                elem,
                '.body',
                "Just one more thing – please confirm the email address you'd like to receive messages from students and alumni!",
            );
        });

        it('should not present itself for users missingRequiredStudentNetworkEmail, have not seen the intro, and remain hidden or anonymous', () => {
            // see also: User::missingRequiredStudentNetworkEmail
            jest.spyOn(user, 'hasStudentNetworkAccess', 'get').mockReturnValue(true);
            user.email = 'anon@privaterelay.appleid.com';
            user.student_network_email = null;
            user.has_seen_student_network = false;
            user.pref_student_network_privacy = 'anonymous';
            user.career_profile = {};

            render();
            expect(scope.steps.length).toBe(5);
            scope.currentStep = 4;
            scope.$digest();

            SpecHelper.expectElementText(
                elem,
                '.body',
                'Your student network profile is currently set to anonymous. Would you like to make your profile visible to your peers?',
            );

            jest.spyOn(scope, 'activate');
            jest.spyOn(scope, 'complete').mockImplementation(() => {});
            SpecHelper.click(elem, 'span');
            expect(scope.activate).toHaveBeenCalled();
            expect(scope.steps.length).toBe(5);
            expect(scope.complete).toHaveBeenCalled();
        });

        it('should be valid if supplying a non-blacklisted email', () => {
            jest.spyOn(user, 'missingRequiredStudentNetworkEmail', 'get').mockReturnValue(true);
            user.has_seen_student_network = true;
            user.career_profile = {};
            render();
            SpecHelper.updateTextInput(elem, 'input[name="student_network_email"]', 'user@valid-domain.com');
            SpecHelper.expectElementEnabled(elem, 'button');
            jest.spyOn(scope, 'complete').mockImplementation(() => {});
            SpecHelper.click(elem, 'button');
            expect(scope.complete).toHaveBeenCalled();
        });

        it('should be invalid if supplying a blacklisted email', () => {
            jest.spyOn(user, 'missingRequiredStudentNetworkEmail', 'get').mockReturnValue(true);
            user.has_seen_student_network = true;
            user.career_profile = {};
            render();
            SpecHelper.updateTextInput(elem, 'input[name="student_network_email"]', 'anon@privaterelay.appleid.com');
            SpecHelper.expectElementDisabled(elem, 'button');
        });
    });
});
