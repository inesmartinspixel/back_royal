import 'AngularSpecHelper';
import 'StudentNetwork/angularModule';
import studentNetworkEventListLocales from 'StudentNetwork/locales/student_network/student_network_event_list-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(studentNetworkEventListLocales);

describe('FrontRoyal.StudentNetwork.studentNetworkEventAmbiguousPopover', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let eventsMapLayer;
    let $timeout;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.StudentNetwork', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                $timeout = $injector.get('$timeout');
            },
        ]);

        SpecHelper.stubCurrentUser();

        eventsMapLayer = {
            closeAmbiguousEventMarkerPopover: jest.fn(),
            handleGestureControlsOnPopoverStateChange: jest.fn(),
        };
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.feature = {
            event_type: 'meetup',
        };
        renderer.scope.events = [];
        renderer.scope.eventsMapLayer = eventsMapLayer;
        renderer.render(
            '<student-network-event-ambiguous-popover feature="feature" events="events" events-map-layer="eventsMapLayer"></student-network-event-ambiguous-popover>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    it('should call eventsMapLayer.closeAmbiguousEventMarkerPopover when close button in header is clicked', () => {
        render();
        SpecHelper.click(elem, 'header i', 0);
        expect(scope.eventsMapLayer.closeAmbiguousEventMarkerPopover).toHaveBeenCalledWith(scope.feature);
    });

    it('should turn off window resize event handler on scope $destroy', () => {
        render();
        const mockWindow = $(window);
        jest.spyOn(mockWindow, 'off').mockImplementation(() => {});
        jest.spyOn(window, '$').mockReturnValue(mockWindow);
        scope.$destroy();
        expect(mockWindow.off).toHaveBeenCalledWith(`resize.${scope.$id}`);
    });

    it('should call eventsMapLayer.handleGestureControlsOnPopoverStateChange when scope is destroyed', () => {
        render();
        scope.$destroy();
        expect(scope.eventsMapLayer.handleGestureControlsOnPopoverStateChange).toHaveBeenCalledWith(scope.feature);
    });

    it('should render a student-network-event-list', () => {
        render();
        SpecHelper.expectElements(elem, 'student-network-event-list', 1);
    });

    describe('ensureEventListContainerScrollable', () => {
        it('should be called after a timeout', () => {
            render();
            jest.spyOn(scope, 'ensureEventListContainerScrollable').mockImplementation(() => {});
            expect(scope.ensureEventListContainerScrollable).not.toHaveBeenCalled();
            $timeout.flush();
            expect(scope.ensureEventListContainerScrollable).toHaveBeenCalled();
        });

        it('should be called on window resize', () => {
            render();
            jest.spyOn(scope, 'ensureEventListContainerScrollable').mockImplementation(() => {});
            $(window).trigger(`resize.${scope.$id}`);
            expect(scope.ensureEventListContainerScrollable).toHaveBeenCalled();
        });
    });
});
