import 'AngularSpecHelper';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import 'StudentNetwork/angularModule';
import networkMapFilterBoxLocales from 'StudentNetwork/locales/student_network/network_map_filter_box-en.json';
import fieldOptionsLocales from 'StudentNetwork/locales/student_network/field_options-en.json';
import fieldOptionsLocalesCareers from 'Careers/locales/careers/field_options-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(networkMapFilterBoxLocales, fieldOptionsLocales, fieldOptionsLocalesCareers);

describe('FrontRoyal.StudentNetwork.StudentsMapLayer', () => {
    let $injector;
    let SpecHelper;
    let CareerProfile;
    let StudentNetworkFilterSet;
    let studentsMapLayer;
    let studentNetworkMapViewModel;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.StudentNetwork', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                StudentNetworkFilterSet = $injector.get('StudentNetworkFilterSet');

                CareerProfile = $injector.get('CareerProfile');
                $injector.get('CareerProfileFixtures');
            },
        ]);

        SpecHelper.stubCurrentUser();
        studentNetworkMapViewModel = SpecHelper.stubStudentNetworkMapViewModel();
        studentsMapLayer = studentNetworkMapViewModel.studentsMapLayer;

        jest.spyOn(StudentNetworkFilterSet.prototype, 'ensureStudentProfilesPreloaded').mockImplementation(() => {});
        jest.spyOn(studentsMapLayer, 'refresh').mockImplementation();
    });

    describe('_degreesToDms', () => {
        it('should convert lat/long decimal to degrees-minutes-seconds', () => {
            expect(studentsMapLayer._degreesToDms(36.0381607)).toBe('36° 2\' 17" E');
            expect(studentsMapLayer._degreesToDms(-95.8810732)).toBe('95° 52\' 51" W');
        });
    });

    describe('loading data', () => {
        it('should refresh but not fetch career profiles when applyFilters is called outside of advanced search', () => {
            studentsMapLayer.applyFilters({
                program_type: ['mba', 'emba'],
                only_local: true,
            });

            expect(studentsMapLayer.refresh).toHaveBeenCalled();
            expect(studentsMapLayer.studentProfileFilterSet).toBeUndefined();
            expect(StudentNetworkFilterSet.prototype.ensureStudentProfilesPreloaded).not.toHaveBeenCalled();
        });
    });

    describe('focusedClusterFeature', () => {
        beforeEach(() => {
            jest.spyOn(studentsMapLayer, '_degreesToDms').mockImplementation();

            CareerProfile.expect('index');
        });

        it('should work when assigning an initial feature', () => {
            studentsMapLayer.focusedClusterFeature = {
                geometry: {
                    coordinates: [0, 1],
                },
            };

            expect(studentsMapLayer.inAdvancedSearchMode).toBe(true);
            expect(StudentNetworkFilterSet.prototype.ensureStudentProfilesPreloaded).not.toHaveBeenCalled();
            // Note: when initially entering advanced search, this won't be defined; the UI will fetch it initially
            expect(studentsMapLayer.studentProfileFilterSet).toBeUndefined();
        });

        it('should work when changing features', () => {
            studentsMapLayer.focusedClusterFeature = {
                geometry: {
                    coordinates: [0, 1],
                },
            };

            expect(studentsMapLayer.inAdvancedSearchMode).toBe(true);
            expect(studentsMapLayer.studentProfileFilterSet).toBeUndefined();
            expect(StudentNetworkFilterSet.prototype.ensureStudentProfilesPreloaded).not.toHaveBeenCalled();

            studentsMapLayer.focusedClusterFeature = {
                geometry: {
                    coordinates: [0, 2],
                },
            };

            expect(studentsMapLayer.inAdvancedSearchMode).toBe(true);
            expect(studentsMapLayer.studentProfileFilterSet).toBeDefined();
            expect(StudentNetworkFilterSet.prototype.ensureStudentProfilesPreloaded).toHaveBeenCalled();
        });
    });

    describe('setClassFilter', () => {
        beforeEach(() => {
            jest.spyOn(studentsMapLayer, 'setClassFilter');
        });

        it('should add selected program type to filters', () => {
            ['mba', 'emba'].forEach(programType => {
                studentsMapLayer.setClassFilter(programType);
                expect(studentsMapLayer.setClassFilter).toHaveBeenCalledWith(programType);
                expect(studentsMapLayer.filters.program_type).toEqual(programType);
                expect(studentsMapLayer.advancedFilters.class).toEqual(programType); // filters should copy over
                expect(studentsMapLayer.currentClassFilter).toBe(programType);
                expect(studentsMapLayer.filters.cohort_id).not.toBeDefined();
            });
        });

        it('should set selected class to filters', () => {
            studentsMapLayer.myClassCohortId = 123; // stub this out so that the "mine" filter shows up

            studentsMapLayer.setClassFilter('all');
            expect(studentsMapLayer.setClassFilter).toHaveBeenCalledWith('all');
            expect(studentsMapLayer.filters.cohort_id).not.toBeDefined();
            expect(studentsMapLayer.currentClassFilter).toBe('all');
            expect(studentsMapLayer.filters.program_type).not.toBeDefined();
            expect(studentsMapLayer.advancedFilters.class).toEqual(null); // filters should copy over

            studentsMapLayer.setClassFilter('mine');
            expect(studentsMapLayer.setClassFilter).toHaveBeenCalledWith('mine');
            expect(studentsMapLayer.filters.cohort_id).toBe(studentsMapLayer.myClassCohortId);
            expect(studentsMapLayer.currentClassFilter).toBe('mine');
            expect(studentsMapLayer.filters.program_type).not.toBeDefined();
            expect(studentsMapLayer.advancedFilters.class).toEqual('mine'); // filters should copy over
        });
    });

    describe('resetAdvancedFilters', () => {
        it('should reset filters and advanced filters', () => {
            CareerProfile.expect('index');
            studentsMapLayer.advancedFilters = {
                keyword_search: 'blah',
                class: 'mba',
                student_network_looking_for: ['co_founder'],
                student_network_interests: ['ai'],
                industries: ['consulting'],
                alumni: true,
                places: [
                    {
                        the: 'bahamas',
                    },
                ],
            };

            studentsMapLayer.applyAdvancedFilters();
            studentsMapLayer.resetAdvancedFilters();

            expect(studentsMapLayer.advancedFilters).toEqual({
                keyword_search: undefined,
                class: 'mba', // this doesn't get reset because it always matches the basic filter
                student_network_looking_for: [],
                student_network_interests: [],
                industries: [],
                alumni: null,
                places: [],
            });
            expect(studentsMapLayer.filters).toEqual({
                program_type: 'mba', // this value shouldn't change
                only_local: true,
            });
        });
    });

    it('should apply advanced filters', () => {
        const EventLogger = $injector.get('EventLogger');
        const HasLocation = $injector.get('HasLocation');
        jest.spyOn(EventLogger.instance, 'log').mockImplementation(() => {});
        jest.spyOn(HasLocation, 'locationString').mockReturnValue('The formatted bahamas');

        const place = {
            the: 'bahamas',
            lat: 42,
            lng: 42,
        };

        studentsMapLayer.advancedFilters = {
            keyword_search: 'blah',
            class: 'mba',
            student_network_looking_for: ['co_founder'],
            student_network_interests: ['ai'],
            industries: ['consulting'],
            alumni: true,
            places: [place],
        };
        studentsMapLayer.applyAdvancedFilters();
        expect(studentsMapLayer.filters).toEqual({
            keyword_search: 'blah',
            student_network_looking_for: ['co_founder'],
            student_network_interests: ['ai'],
            industries: ['consulting'],
            alumni: true,
            places: [place],
            program_type: 'mba',
            only_local: true,
        });

        studentsMapLayer.advancedFilters.alumni = false;
        studentsMapLayer.applyAdvancedFilters();
        expect(studentsMapLayer.filters.alumni).toBe(false);

        expect(EventLogger.instance.log).toHaveBeenCalledWith('student_network:applied_search_filters', {
            filters_places: ['The formatted bahamas'],
            filters_keyword_search: studentsMapLayer.filters.keyword_search,
            filters_student_network_looking_for: studentsMapLayer.filters.student_network_looking_for,
            filters_student_network_interests: studentsMapLayer.filters.student_network_interests,
            filters_industries: studentsMapLayer.filters.industries,
            filters_alumni: studentsMapLayer.filters.alumni,
            filters_program_type: studentsMapLayer.filters.program_type,
        });
        expect(HasLocation.locationString).toHaveBeenCalledWith(place);

        studentsMapLayer.advancedFilters.alumni = null;
        studentsMapLayer.applyAdvancedFilters();
        expect(studentsMapLayer.filters.alumni).toBeUndefined();
    });

    describe('searchByInterestOnly', () => {
        it('should reset filters and advanced filters, add the interest, and update the hasAdvancedFilters status', () => {
            CareerProfile.expect('index');
            studentsMapLayer.advancedFilters = {
                keyword_search: 'blah',
                class: 'mba',
                student_network_looking_for: ['co_founder'],
                student_network_interests: ['ai'],
                industries: ['consulting'],
                alumni: true,
                places: [
                    {
                        the: 'bahamas',
                    },
                ],
            };
            studentsMapLayer.applyAdvancedFilters();
            studentsMapLayer.searchByInterestOnly('alligator wrestling');
            expect(studentsMapLayer.advancedFilters).toEqual({
                keyword_search: undefined,
                class: 'mba', // this doesn't get reset because it always matches the basic filter
                student_network_looking_for: [],
                student_network_interests: ['alligator wrestling'],
                industries: [],
                alumni: null,
                places: [],
            });
            expect(studentsMapLayer.filters).toEqual({
                only_local: true,
                program_type: 'mba',
                student_network_interests: ['alligator wrestling'],
            });
        });
    });

    describe('hasAdvancedFilters', () => {
        beforeEach(() => {
            CareerProfile.expect('index');
        });

        it('should be false when no advanced filters are set', () => {
            studentsMapLayer.advancedFilters = {};
            studentsMapLayer.applyAdvancedFilters();
            expect(studentsMapLayer.hasAdvancedFilters).toBe(false);
        });

        it('should be false when class filter is set', () => {
            studentsMapLayer.advancedFilters = {
                class: 'mba',
            };
            studentsMapLayer.applyAdvancedFilters();
            expect(studentsMapLayer.hasAdvancedFilters).toBe(false);
        });

        it('should be true when advanced filters are set', () => {
            studentsMapLayer.advancedFilters = {
                keyword_search: 'blah',
            };
            studentsMapLayer.applyAdvancedFilters();
            expect(studentsMapLayer.hasAdvancedFilters).toBe(true);
        });
    });
});
