import 'AngularSpecHelper';
import 'StudentNetwork/angularModule';
import networkMapFilterBoxLocales from 'StudentNetwork/locales/student_network/network_map_filter_box-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(networkMapFilterBoxLocales);

describe('FrontRoyal.StudentNetwork.StudentsMapLayer', () => {
    let $injector;
    let SpecHelper;
    let currentUser;
    let $timeout;
    let StudentNetworkFilterSet;
    let studentNetworkMapViewModel;
    let StudentNetworkEvent;
    let $q;
    let $location;
    let DialogModal;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.StudentNetwork', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                $timeout = $injector.get('$timeout');
                StudentNetworkFilterSet = $injector.get('StudentNetworkFilterSet');
                StudentNetworkEvent = $injector.get('StudentNetworkEvent');
                $q = $injector.get('$q');
                $location = $injector.get('$location');
                DialogModal = $injector.get('DialogModal');
            },
        ]);

        currentUser = SpecHelper.stubCurrentUser();
        studentNetworkMapViewModel = SpecHelper.stubStudentNetworkMapViewModel();

        jest.spyOn(StudentNetworkFilterSet.prototype, 'ensureStudentProfilesPreloaded').mockImplementation(() => {});
        studentNetworkMapViewModel.dataLayers.forEach(layer => {
            jest.spyOn(layer, 'activate').mockImplementation();
            jest.spyOn(layer, 'deactivate').mockImplementation();
            jest.spyOn(layer, 'refresh').mockImplementation();
        });
    });

    describe('setDefaultActiveLayer', () => {
        beforeEach(() => {
            jest.spyOn(studentNetworkMapViewModel, 'setDefaultActiveLayer');
            jest.spyOn(studentNetworkMapViewModel, 'setActiveMapLayer').mockImplementation(() => {});
        });

        it('should be called on initialize', () => {
            $timeout.flush();
            expect(studentNetworkMapViewModel.setDefaultActiveLayer).toHaveBeenCalled();
        });

        describe('without eventId', () => {
            it('should set the students map layer as the active layer', () => {
                $timeout.flush();
                expect(studentNetworkMapViewModel.setActiveMapLayer).toHaveBeenCalledWith(
                    studentNetworkMapViewModel.studentsMapLayer,
                );
            });
        });

        describe('with eventId', () => {
            beforeEach(() => {
                studentNetworkMapViewModel.eventId = 'some_uuid';
                jest.spyOn(studentNetworkMapViewModel, 'fetchEventAndShowDetails').mockImplementation(() => {});
                $timeout.flush();
            });

            it('should set the events map layer as the active layer', () => {
                expect(studentNetworkMapViewModel.setActiveMapLayer).toHaveBeenCalledWith(
                    studentNetworkMapViewModel.eventsMapLayer,
                );
            });

            it('should call fetchEventAndShowDetails', () => {
                expect(studentNetworkMapViewModel.fetchEventAndShowDetails).toHaveBeenCalled();
            });
        });
    });

    describe('fetchEventAndShowDetails', () => {
        beforeEach(() => {
            studentNetworkMapViewModel.eventId = 'some_uuid';
            jest.spyOn(studentNetworkMapViewModel, 'fetchEventForId').mockImplementation(() =>
                $q.resolve({
                    result: ['foobar'],
                }),
            );
            jest.spyOn(studentNetworkMapViewModel, 'showEventDetailsModal').mockImplementation(() => {});
        });

        it('should call fetchEventForId', () => {
            studentNetworkMapViewModel.fetchEventAndShowDetails();
            expect(studentNetworkMapViewModel.fetchEventForId).toHaveBeenCalledWith('some_uuid');
        });

        it('should call showEventDetailsModal when fetchEventForId resolves with a result', () => {
            studentNetworkMapViewModel.fetchEventAndShowDetails();
            $timeout.flush();
            expect(studentNetworkMapViewModel.showEventDetailsModal).toHaveBeenCalledWith('foobar');
        });

        it("should clear 'event-id' query param when fetchEventForId resolves without a result", () => {
            jest.spyOn($location, 'search').mockImplementation(() => {});
            jest.spyOn(studentNetworkMapViewModel, 'fetchEventForId').mockImplementation(() =>
                $q.resolve({
                    result: [],
                }),
            );
            studentNetworkMapViewModel.fetchEventAndShowDetails();
            $timeout.flush();
            expect(studentNetworkMapViewModel.showEventDetailsModal).not.toHaveBeenCalled();
            expect($location.search).toHaveBeenCalledWith('event-id', null);
        });

        it("should clear 'event-id' query param when fetchEventForId rejects", () => {
            jest.spyOn($location, 'search').mockImplementation(() => {});
            jest.spyOn(studentNetworkMapViewModel, 'fetchEventForId').mockImplementation(() => $q.reject());
            studentNetworkMapViewModel.fetchEventAndShowDetails();
            $timeout.flush();
            expect(studentNetworkMapViewModel.showEventDetailsModal).not.toHaveBeenCalled();
            expect($location.search).toHaveBeenCalledWith('event-id', null);
        });
    });

    describe('fetchEventForId', () => {
        it('should index StudentNetworkEvent with appropriate filters', () => {
            StudentNetworkEvent.expect('index').toBeCalledWith({
                filters: {
                    id: 'some_uuid',
                    start_time: 0,
                    end_time: new Date('2099/01/01').getTime(),
                },
            });
            studentNetworkMapViewModel.fetchEventForId('some_uuid');
        });
    });

    describe('showEventDetailsModal', () => {
        let event;

        beforeEach(() => {
            event = StudentNetworkEvent.new({
                id: '1',
            });
            jest.spyOn($location, 'search').mockImplementation(() => {});
            jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
        });

        it('should return if no event', () => {
            studentNetworkMapViewModel.showEventDetailsModal();
            expect($location.search).not.toHaveBeenCalled();
            expect(DialogModal.alert).not.toHaveBeenCalled();
        });

        it("should set 'event-id' query parameter", () => {
            studentNetworkMapViewModel.showEventDetailsModal(event);
            expect($location.search).toHaveBeenCalledWith('event-id', '1');
        });

        it('should call DialogModal.alert with appropriate arguments', () => {
            studentNetworkMapViewModel.showEventDetailsModal(event);
            expect(DialogModal.alert).toHaveBeenCalledWith({
                content:
                    '<student-network-event-details event="event" container-selector="containerSelector"></student-network-event-details>',
                blurTargetSelector: '#app-main-container',
                classes: [
                    'no-body-padding',
                    'small-close-button',
                    'student-network-event-details',
                    'clickable-app-header-menu',
                ],
                animated: false,
                closeOnOutsideClick: true,
                close: expect.anything(), // don't know why i can't use jest.fn() here
                scope: {
                    event,
                    containerSelector: 'dialog-modal-alert .modal-content',
                },
            });
        });
    });

    it('should deactivate and activate layers when calling setActiveMapLayer', () => {
        $timeout.flush(); // finish setting up the default layer
        studentNetworkMapViewModel.setActiveMapLayer(studentNetworkMapViewModel.eventsMapLayer);
        expect(studentNetworkMapViewModel.eventsMapLayer.activate).toHaveBeenCalled();
        expect(studentNetworkMapViewModel.eventsMapLayer.refresh).toHaveBeenCalled();
        expect(studentNetworkMapViewModel.studentsMapLayer.deactivate).toHaveBeenCalled();
    });

    it('should enable or disable the myClassCohortId based on application', () => {
        const Cohort = $injector.get('Cohort');

        // setup the variables that we can tweak
        let hasStudentNetworkAccess;

        let canFilterNetworkForMyClass;
        let application;

        // set up all the spies that let us tweak the variables
        jest.spyOn(currentUser, 'acceptedOrPreAcceptedCohortApplication', 'get').mockImplementation(() => application);
        jest.spyOn(currentUser, 'hasStudentNetworkAccess', 'get').mockImplementation(() => hasStudentNetworkAccess);
        jest.spyOn(Cohort, 'canFilterNetworkForMyClass').mockImplementation(programType => {
            if (programType !== currentUser.acceptedOrPreAcceptedCohortApplication.program_type) {
                throw new Error('Unexpected program_type');
            }
            return canFilterNetworkForMyClass;
        });

        // tweak things and assert the button goes away
        tweakAndAssertNoMyClassCohortId(() => {
            hasStudentNetworkAccess = false;
        });
        tweakAndAssertNoMyClassCohortId(() => {
            application = null;
        });
        tweakAndAssertNoMyClassCohortId(() => {
            application.status = 'pre_accepted';
        });
        tweakAndAssertNoMyClassCohortId(() => {
            canFilterNetworkForMyClass = false;
        });

        function tweakAndAssertNoMyClassCohortId(fn) {
            hasStudentNetworkAccess = true;
            canFilterNetworkForMyClass = true;
            application = {
                status: 'accepted',
                program_type: 'something',
                cohort_id: 123,
            };
            studentNetworkMapViewModel.updateMyClassCohortId(currentUser);
            expect(studentNetworkMapViewModel.studentsMapLayer.myClassCohortId).toEqual(application.cohort_id);
            fn();
            application = _.clone(application); // trigger watcher
            studentNetworkMapViewModel.updateMyClassCohortId(currentUser);
            expect(studentNetworkMapViewModel.studentsMapLayer.myClassCohortId).toBeNull();
        }
    });

    describe('_onIdleEvent', () => {
        it('should not refresh the activeMapLayer if the !activeMapLayer.supportsRefreshOnIdleMapEvent', () => {
            studentNetworkMapViewModel.activeMapLayer = studentNetworkMapViewModel.eventsMapLayer;
            expect(studentNetworkMapViewModel.eventsMapLayer.supportsRefreshOnIdleMapEvent).toBe(false); // sanity check
            jest.spyOn(studentNetworkMapViewModel.eventsMapLayer, 'refresh');
            studentNetworkMapViewModel._onIdleEvent();
            expect(studentNetworkMapViewModel.eventsMapLayer.refresh).not.toHaveBeenCalled();
        });

        it('should refresh the activeMapLayer if the activeMapLayer.supportsRefreshOnIdleMapEvent', () => {
            studentNetworkMapViewModel.activeMapLayer = studentNetworkMapViewModel.studentsMapLayer;
            expect(studentNetworkMapViewModel.studentsMapLayer.supportsRefreshOnIdleMapEvent).toBe(true); // sanity check
            jest.spyOn(studentNetworkMapViewModel.studentsMapLayer, 'refresh').mockImplementation(() => {});
            studentNetworkMapViewModel._onIdleEvent();
            expect(studentNetworkMapViewModel.studentsMapLayer.refresh).toHaveBeenCalled();
        });
    });

    describe('destroy', () => {
        it('should invoke deactivate method on dataLayers', () => {
            studentNetworkMapViewModel.destroy();
            studentNetworkMapViewModel.dataLayers.forEach(layer => {
                expect(layer.deactivate.mock.calls.length).toBe(1);
            });
        });

        it('should invoke deactivate method on dataLayers', () => {
            studentNetworkMapViewModel._listeners = [
                {
                    remove: jest.fn(),
                },
                {
                    remove: jest.fn(),
                },
            ];
            studentNetworkMapViewModel.destroy();
            studentNetworkMapViewModel._listeners.forEach(listener => {
                expect(listener.remove).toHaveBeenCalled();
            });
        });

        it('should turn off resize listener', () => {
            const mockWindow = $(window);
            jest.spyOn(mockWindow, 'off').mockImplementation(() => {});
            jest.spyOn(window, '$').mockReturnValue(mockWindow);
            studentNetworkMapViewModel.destroy();
            expect(mockWindow.off).toHaveBeenCalledWith(`resize.${studentNetworkMapViewModel._id}`);
        });

        it('should call DialogModal.hideAlerts', () => {
            jest.spyOn(DialogModal, 'hideAlerts');
            studentNetworkMapViewModel.destroy();
            expect(DialogModal.hideAlerts).toHaveBeenCalled();
        });
    });
});
