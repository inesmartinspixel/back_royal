import 'AngularSpecHelper';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';
import 'StudentNetwork/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import studentProfileListLocales from 'StudentNetwork/locales/student_network/student_profile_list-en.json';
import candidateListLocales from 'Careers/locales/careers/candidate_list-en.json';
import jobPreferencesFormLocales from 'Careers/locales/careers/edit_career_profile/job_preferences_form-en.json';
import fieldOptionsLocales from 'Careers/locales/careers/field_options-en.json';
import candidateListCardLocales from 'Careers/locales/careers/candidate_list_card-en.json';

setSpecLocales(
    studentProfileListLocales,
    candidateListLocales,
    jobPreferencesFormLocales,
    fieldOptionsLocales,
    candidateListCardLocales,
);

describe('FrontRoyal.StudentNetwork.StudentProfileListDir', () => {
    let $injector;
    let CareerProfile;
    let careerProfiles;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let getProfilesSpy;
    let currentUser;
    let Cohort;
    let removeLatLong;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.StudentNetwork', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                CareerProfile = $injector.get('CareerProfile');
                $injector.get('CareerProfileFixtures');
                Cohort = $injector.get('Cohort');
                $injector.get('CohortFixtures');
            },
        ]);

        SpecHelper.stubConfig();

        currentUser = SpecHelper.stubCurrentUser();
        currentUser.relevant_cohort = Cohort.fixtures.getInstance();
    });

    function render(opts = {}) {
        careerProfiles = [];
        for (let i = 0; i < (opts.numProfiles || 5); i++) {
            careerProfiles.push(CareerProfile.fixtures.getInstance());
        }
        getProfilesSpy = jest.fn();
        removeLatLong = jest.fn();

        renderer = SpecHelper.renderer();
        renderer.scope.getProfiles = getProfilesSpy;
        renderer.scope.careerProfiles = opts.careerProfiles || careerProfiles;
        renderer.scope.listLimit = opts.listLimit || 4;
        renderer.scope.offset = angular.isDefined(opts.offset) ? opts.offset : 0;
        renderer.scope.noMoreAvailable = opts.noMoreAvailable || false;
        renderer.scope.clusterLat = opts.clusterLat;
        renderer.scope.clusterLong = opts.clusterLong;
        renderer.scope.removeLatLong = removeLatLong;
        renderer.render(
            '<student-profile-list career-profiles="careerProfiles" get-profiles="getProfiles(action, listLimit, offset)" list-limit="listLimit" offset="offset" no-more-available="noMoreAvailable" cluster-lat="clusterLat" cluster-long="clusterLong" remove-lat-long="removeLatLong()"></student-profile-list>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    it('should be hidden if careerProfiles.length is not greater than 0', () => {
        render({
            careerProfiles: [],
        });
        SpecHelper.expectNoElement(elem, 'student-profile-list-card');
    });

    it('should limit visible profiles to listLimit', () => {
        const limit = 5;
        render({
            numProfiles: 8, // just needs to be greater than or equal to limit
            listLimit: limit,
        });
        expect(scope.careerProfiles.length).toBeGreaterThan(limit);
        SpecHelper.expectElements(elem, 'student-profile-list-card', limit);
    });

    it('should begin visible profiles at offset', () => {
        const offset = 2;
        render({
            offset,
        });
        SpecHelper.expectElements(elem, 'student-profile-list-card', careerProfiles.length - offset);

        // the first 2 profiles should have been skipped over because of the offset,
        // so the first candidateListCard should be the profile at the offset index
        const firstCandidateListCardScope = SpecHelper.expectElement(
            elem,
            'student-profile-list-card:eq(0)',
        ).isolateScope();
        expect(firstCandidateListCardScope.careerProfile.id).toEqual(careerProfiles[offset].id);
    });

    it('should not show focused cluster location when not provided', () => {
        render();
        SpecHelper.expectNoElement(elem, '.cluster-location');
    });

    describe('showingProfiles', () => {
        it('should work', () => {
            render();
            expect(scope.showingProfiles).toBe(true);
            scope.offset = 999999;
            scope.$digest();
            expect(scope.showingProfiles).toBe(false);
        });
    });

    describe('pagination', () => {
        it('should be disabled when not showing any profiles', () => {
            render();
            SpecHelper.expectElementEnabled(elem, 'list-pagination [name="next-page"]');
            scope.offset = 999999;
            scope.$digest();
            expect(scope.showingProfiles).toBe(false); // sanity check
            SpecHelper.expectElementDisabled(elem, 'list-pagination [name="next-page"]');
        });
    });

    describe('cluster-location', () => {
        it('should show focused cluster lat and long when provided', () => {
            render({
                clusterLat: '123',
                clusterLong: '456',
            });

            SpecHelper.expectElementText(elem, '.cluster-location p', '123, 456');
            SpecHelper.click(elem, '.cluster-location button');
            expect(removeLatLong).toHaveBeenCalled();
        });

        it('should remove cluster-location when button is clicked', () => {
            render({
                clusterLat: '123',
                clusterLong: '456',
            });

            SpecHelper.click(elem, '.cluster-location button');
            expect(removeLatLong).toHaveBeenCalled();
        });
    });
});
