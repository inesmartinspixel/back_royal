import 'AngularSpecHelper';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';
import 'StudentNetwork/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import partialAccessModalLocales from 'StudentNetwork/locales/student_network/partial_access_modal-en.json';

setSpecLocales(partialAccessModalLocales);

describe('FrontRoyal.StudentNetwork.PartialAccessModal', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let Cohort;
    let user;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.StudentNetwork', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');

                Cohort = $injector.get('Cohort');
                $injector.get('CohortFixtures');
            },
        ]);

        SpecHelper.stubConfig();

        user = SpecHelper.stubCurrentUser();
        user.relevant_cohort = Cohort.fixtures.getInstance();
        jest.spyOn(user.relevant_cohort, 'getApplicableAdmissionRound').mockReturnValue(
            user.relevant_cohort.admission_rounds[0],
        );
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.dismiss = jest.fn();
        renderer.render('<partial-access-modal dismiss="dismiss"></partial-access-modal>');
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    it('should render main content', () => {
        render();
        SpecHelper.expectElement(elem, '.main-content');
    });

    describe('apply content', () => {
        it('should not render if user has applied', () => {
            jest.spyOn(user, 'hasEverApplied', 'get').mockReturnValue(true);
            render();
            SpecHelper.expectNoElement(elem, '.apply-content');
        });

        it('should not render if user is not mba or emba', () => {
            jest.spyOn(user, 'hasEverApplied', 'get').mockReturnValue(false);
            jest.spyOn(user, 'programType', 'get').mockReturnValue('foo');
            render();
            SpecHelper.expectNoElement(elem, '.apply-content');
        });

        it('should render if user has not applied', () => {
            jest.spyOn(user, 'hasEverApplied', 'get').mockReturnValue(false);
            jest.spyOn(user, 'programType', 'get').mockReturnValue('mba');
            render();
            SpecHelper.expectElement(elem, '.apply-content');
        });

        it('should render if user has been rejected', () => {
            jest.spyOn(user, 'isRejected', 'get').mockReturnValue(true);
            jest.spyOn(user, 'programType', 'get').mockReturnValue('mba');
            render();
            SpecHelper.expectElement(elem, '.apply-content');
        });

        it('should render if user has been expelled', () => {
            jest.spyOn(user, 'isExpelled', 'get').mockReturnValue(true);
            jest.spyOn(user, 'programType', 'get').mockReturnValue('mba');
            render();
            SpecHelper.expectElement(elem, '.apply-content');
        });
    });
});
