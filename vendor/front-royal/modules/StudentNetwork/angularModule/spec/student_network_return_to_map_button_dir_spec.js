import 'AngularSpecHelper';
import 'StudentNetwork/angularModule';
import networkMapFilterBoxLocales from 'StudentNetwork/locales/student_network/network_map_filter_box-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(networkMapFilterBoxLocales);

describe('FrontRoyal.StudentNetwork.studentNetworkReturnToMapButton', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.StudentNetwork', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
            },
        ]);
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.returnToMapCallback = jest.fn();
        renderer.render(
            '<student-network-return-to-map-button return-to-map-callback="returnToMapCallback"></student-network-return-to-map-button>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    it('should render a button', () => {
        render();
        const button = SpecHelper.expectElement(elem, 'button.mobile-exit-search');
        SpecHelper.expectElementText(elem, button, 'Return to Map');
    });

    it('should call returnToMapCallback when the button is clicked', () => {
        render();
        jest.spyOn(scope, 'returnToMapCallback');
        const button = SpecHelper.expectElement(elem, 'button.mobile-exit-search');
        SpecHelper.click(elem, button);
        expect(scope.returnToMapCallback).toHaveBeenCalled();
    });
});
