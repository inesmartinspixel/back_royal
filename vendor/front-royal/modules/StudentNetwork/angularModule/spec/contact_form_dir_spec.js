import 'AngularSpecHelper';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import 'StudentNetwork/angularModule';

import * as userAgentHelper from 'userAgentHelper';
import setSpecLocales from 'Translation/setSpecLocales';
import stubSpecLocale from 'Translation/stubSpecLocale';
import contactFormLocales from 'StudentNetwork/locales/student_network/contact_form-en.json';
import jobPreferencesFormLocales from 'Careers/locales/careers/edit_career_profile/job_preferences_form-en.json';
import fieldOptionsLocales from 'Careers/locales/careers/field_options-en.json';
import candidateListCardLocales from 'Careers/locales/careers/candidate_list_card-en.json';

setSpecLocales(contactFormLocales, jobPreferencesFormLocales, fieldOptionsLocales, candidateListCardLocales);
stubSpecLocale('email_input.email_validation.please_enter_valid_email');

describe('FrontRoyal.StudentNetwork.ContactForm', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let CareerProfile;
    let user;
    let candidateListCardViewHelper;
    let $q;
    let $timeout;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.StudentNetwork', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                candidateListCardViewHelper = $injector.get('candidateListCardViewHelper');
                $q = $injector.get('$q');
                $timeout = $injector.get('$timeout');

                CareerProfile = $injector.get('CareerProfile');
                $injector.get('CareerProfileFixtures');
            },
        ]);

        SpecHelper.stubConfig();

        user = SpecHelper.stubCurrentUser();
        user.career_profile = CareerProfile.fixtures.getInstance();
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.careerProfile = CareerProfile.fixtures.getInstance({
            nickname: 'Recipient',
        });
        renderer.scope.userInfo = candidateListCardViewHelper.getSummarizedProfileInfo(renderer.scope.careerProfile);
        renderer.scope.onFinish = jest.fn();
        renderer.render(
            '<contact-form career-profile="careerProfile" user-info="userInfo" on-finish="onFinish"></contact-form>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    it('should render form if message is not sent', () => {
        render();
        SpecHelper.expectElement(elem, 'careers-avatar');
        SpecHelper.expectElement(elem, '.front-royal-form-container');
    });

    it('should render checkmark if message is sent', () => {
        render();
        scope.messageSent = true;
        scope.$digest();
        SpecHelper.expectElement(elem, 'careers-avatar');
        SpecHelper.expectElement(elem, '.sent-message');
    });

    describe('message placeholder', () => {
        beforeEach(() => {
            user.nickname = 'Sender';
        });

        it('should use mobile safari text', () => {
            jest.spyOn(userAgentHelper, 'isSafariOrIsiOSDevice').mockReturnValue(true);
            render();
            expect(scope.messageBodyPlaceholder).toBe('student_network.contact_form.message_placeholder_mobile_safari');
            SpecHelper.expectElementPlaceholder(
                elem,
                '[name="message_body"]',
                `Hi ${scope.careerProfile.nickname}, I wanted to reach out to introduce myself...`,
            );
        });

        it('should use the local text', () => {
            jest.spyOn(user.career_profile, 'distanceTo').mockReturnValue(50);
            render();
            expect(scope.messageBodyPlaceholder).toBe('student_network.contact_form.message_placeholder_local');
            SpecHelper.expectElementPlaceholder(
                elem,
                '[name="message_body"]',
                `Hi ${scope.careerProfile.nickname}, I saw that you're located in my area and wanted to reach out and introduce myself as a fellow Quantic student! I’d love to connect to... Best, ${user.nickname}`,
            );
        });

        describe('shared attributes text', () => {
            beforeEach(() => {
                jest.spyOn(user.career_profile, 'distanceTo').mockReturnValue(150);
            });

            it('should be used when interests are shared', () => {
                assertAttrsText('hasSharedStudentNetworkInterests');
            });

            it('should be used when they both work at the same company', () => {
                assertAttrsText('hasSameCompany');
            });

            it('should be used when they both have the same role', () => {
                assertAttrsText('hasSameRole');
            });

            it('should be used when they both went to the same school', () => {
                assertAttrsText('hasSharedSchools');
            });

            function assertAttrsText(attrMethod) {
                jest.spyOn(user.career_profile, attrMethod).mockReturnValue(true);
                render();
                expect(scope.messageBodyPlaceholder).toBe('student_network.contact_form.message_placeholder_attrs');
                SpecHelper.expectElementPlaceholder(
                    elem,
                    '[name="message_body"]',
                    `Hi ${scope.careerProfile.nickname}, I wanted to reach out after seeing your profile – it looks like we have a few things in common beyond being Quantic students! I’d love to connect to... Best, ${user.nickname}`,
                );
            }
        });

        it('should use the default text', () => {
            user.nickname = 'Sender';
            jest.spyOn(user.career_profile, 'distanceTo').mockReturnValue(150);
            jest.spyOn(user.career_profile, 'hasSharedStudentNetworkInterests').mockReturnValue(false);
            jest.spyOn(user.career_profile, 'hasSameCompany').mockReturnValue(false);
            jest.spyOn(user.career_profile, 'hasSameRole').mockReturnValue(false);
            jest.spyOn(user.career_profile, 'hasSharedSchools').mockReturnValue(false);

            render();
            expect(scope.messageBodyPlaceholder).toBe('student_network.contact_form.message_placeholder_default');
            SpecHelper.expectElementPlaceholder(
                elem,
                '[name="message_body"]',
                `Hi ${scope.careerProfile.nickname}, Your profile looks interesting, and I wanted to reach out and introduce myself as a fellow Quantic student! I’d love to connect to... Best, ${user.nickname}`,
            );
        });
    });

    describe('message', () => {
        it('should build the message defaults', () => {
            render();
            expect(scope.message.recipient_id).toEqual(scope.careerProfile.user_id);
            expect(scope.message.reply_to).toEqual(scope.currentUser.email);
            expect(scope.message.subject).toEqual(`[Quantic Network] Message from ${scope.currentUser.name}`);
        });

        it('should prioritize student_network_email for reply-to', () => {
            user.student_network_email = 'student_network_email@valid-domain.com';
            render();
            expect(scope.message.reply_to).toEqual('student_network_email@valid-domain.com');
        });

        it('should validate reply_to as email address', () => {
            render();
            SpecHelper.updateTextInput(elem, 'input:eq(1)', 'foo bar');
            scope.message.message_body = 'foobar foobar';

            scope.$digest();
            SpecHelper.expectHasClass(elem, 'input:eq(1)', 'ng-invalid');
            SpecHelper.expectElementDisabled(elem, 'button:eq(0)');

            SpecHelper.updateTextInput(elem, 'input:eq(1)', 'foo@bar.com');
            scope.$digest();
            SpecHelper.expectDoesNotHaveClass(elem, 'input:eq(1)', 'ng-invalid');
            SpecHelper.expectElementEnabled(elem, 'button:eq(0)');
        });
    });

    it('should send the message', () => {
        render();

        let resolveSave;
        jest.spyOn(scope.message, 'save').mockReturnValue(
            $q(resolve => {
                resolveSave = resolve;
            }),
        );

        jest.spyOn(scope, 'sendMessage');

        SpecHelper.updateTextInput(elem, '[name="message_body"]', 'some message');
        SpecHelper.expectElementEnabled(elem, 'button');
        SpecHelper.submitForm(elem);

        expect(scope.sendMessage).toHaveBeenCalled();
        resolveSave();

        $timeout.flush(0);

        SpecHelper.expectNoElement(elem, '.front-royal-form-container');
        SpecHelper.expectElement(elem, '.sent-message');

        $timeout.flush(1749);
        expect(scope.onFinish).not.toHaveBeenCalled();
        $timeout.flush(1);
        expect(scope.onFinish).toHaveBeenCalled();
    });
});
