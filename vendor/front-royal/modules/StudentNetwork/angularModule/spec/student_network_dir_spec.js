import 'AngularSpecHelper';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohort_applications';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';
import 'StudentNetwork/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import studentNetworkLocales from 'StudentNetwork/locales/student_network/student_network-en.json';
import onboardingDialogModalLocales from 'StudentNetwork/locales/student_network/onboarding_dialog_modal-en.json';

setSpecLocales(studentNetworkLocales, onboardingDialogModalLocales);

describe('FrontRoyal.StudentNetwork.StudentNetwork', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let Cohort;
    let user;
    let $timeout;
    let CohortApplication;
    let AppHeaderViewModel;
    let isMobile;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.StudentNetwork', 'SpecHelper');

        angular.mock.module($provide => {
            isMobile = jest.fn();
            isMobile.mockReturnValue(true);
            $provide.value('isMobile', isMobile);
        });

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                $timeout = $injector.get('$timeout');
                Cohort = $injector.get('Cohort');
                CohortApplication = $injector.get('CohortApplication');
                AppHeaderViewModel = $injector.get('Navigation.AppHeader.AppHeaderViewModel');

                $injector.get('CareerProfileFixtures');
                $injector.get('CohortFixtures');
                $injector.get('CohortApplicationFixtures');
            },
        ]);

        SpecHelper.stubConfig();
        SpecHelper.stubDirective('networkMap');
        SpecHelper.stubDirective('partialAccessModal');

        user = SpecHelper.stubCurrentUser();
        user.cohort_applications.push(
            CohortApplication.fixtures.getInstance({
                status: 'accepted',
            }),
        );
        user.relevant_cohort = Cohort.fixtures.getInstance({
            program_type: 'mba',
        });
        user.mba_enabled = true;
    });

    function render(section) {
        renderer = SpecHelper.renderer();
        renderer.scope.section = section;

        if (section) {
            renderer.render(`<student-network section="${section}"></student-network>`);
        } else {
            renderer.render('<student-network></student-network>');
        }

        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
        $timeout.flush();
    }

    it('should show basic page', () => {
        render();
        SpecHelper.expectElement(elem, '.world');
    });

    it('should hide the app header and replace it on destroy', () => {
        jest.spyOn(AppHeaderViewModel, 'toggleVisibility').mockImplementation(() => {});
        render();
        expect(AppHeaderViewModel.toggleVisibility).toHaveBeenCalledWith(false);
        AppHeaderViewModel.toggleVisibility.mockClear();
        scope.$destroy();
        expect(AppHeaderViewModel.toggleVisibility).toHaveBeenCalledWith(true);
    });

    describe('onboarding', () => {
        it('should show the onboarding modal if it has not been seen before', () => {
            jest.spyOn(user, 'hasStudentNetworkAccess', 'get').mockReturnValue(true);
            user.has_seen_student_network = false;
            render();
            SpecHelper.expectElement(elem, 'onboarding-dialog-modal');
        });

        it('should show the onboarding modal if the user requires a student_network_email', () => {
            jest.spyOn(user, 'missingRequiredStudentNetworkEmail', 'get').mockReturnValue(true);
            user.has_seen_student_network = true;
            render();
            SpecHelper.expectElement(elem, 'onboarding-dialog-modal');
        });

        it('should not show the onboarding modal if the is not in the network', () => {
            jest.spyOn(user, 'hasStudentNetworkAccess', 'get').mockReturnValue(false);
            user.has_seen_student_network = false;
            render();
            SpecHelper.expectNoElement(elem, 'onboarding-dialog-modal');
        });

        it('should not show the onboarding modal if the user has seen it and does not need a student_network_email', () => {
            jest.spyOn(user, 'hasStudentNetworkAccess', 'get').mockReturnValue(true);
            jest.spyOn(user, 'missingRequiredStudentNetworkEmail', 'get').mockReturnValue(false);
            user.has_seen_student_network = true;
            render();
            SpecHelper.expectNoElement(elem, 'onboarding-dialog-modal');
        });
    });
});
