import 'AngularSpecHelper';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';
import 'StudentNetwork/angularModule';
import jobPreferencesFormLocales from 'Careers/locales/careers/edit_career_profile/job_preferences_form-en.json';
import fieldOptionsLocales from 'Careers/locales/careers/field_options-en.json';
import candidateListCardLocales from 'Careers/locales/careers/candidate_list_card-en.json';
import studentProfileListLocales from 'StudentNetwork/locales/student_network/student_profile_list-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(jobPreferencesFormLocales, fieldOptionsLocales, candidateListCardLocales, studentProfileListLocales);

describe('FrontRoyal.StudentNetwork.StudentProfileListCard', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let careerProfile;
    let currentUser;
    let Cohort;
    let year;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.StudentNetwork', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                const CareerProfile = $injector.get('CareerProfile');
                $injector.get('CareerProfileFixtures');
                Cohort = $injector.get('Cohort');
                $injector.get('CohortFixtures');

                careerProfile = CareerProfile.fixtures.getInstance({
                    github_profile_url: 'http://github.com',
                });
            },
        ]);

        SpecHelper.stubConfig();

        currentUser = SpecHelper.stubCurrentUser();
        currentUser.relevant_cohort = Cohort.fixtures.getInstance();
    });

    function render(preview) {
        renderer = SpecHelper.renderer();
        renderer.scope.careerProfile = careerProfile;
        renderer.scope.preview = angular.isDefined(preview) ? preview : true;
        renderer.render(
            '<student-profile-list-card career-profile="careerProfile" preview="preview"></student-profile-list-card>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    describe('pref_student_network_privacy', () => {
        describe('full', () => {
            beforeEach(() => {
                careerProfile.pref_student_network_privacy = 'full';
                careerProfile.anonymized = false;
            });

            it("should show the student's full name", () => {
                render();
                expect(scope.userInfo.displayName).toEqual(scope.careerProfile.name);
            });

            it("should show the candidate's avatar", () => {
                render();
                expect(scope.userInfo.avatarUrl).toBe(scope.careerProfile.avatar_url);
            });

            it('should show the contact button if profile is not anonymized', () => {
                render(false);
                SpecHelper.expectElement(elem, '.contact');
            });

            it('should not show the contact button if profile is anonymized', () => {
                careerProfile.anonymized = true;
                render(false);
                SpecHelper.expectNoElement(elem, '.contact');
            });

            it('should show the contact button if user does not have full network access but is in preview', () => {
                careerProfile.anonymized = true;
                render(true);
                SpecHelper.expectElement(elem, '.contact');
            });

            it('should show editProfile buttons when in preview', () => {
                render(true);
                SpecHelper.expectElement(elem, '.work .unstyled-button .fa-pencil');
                SpecHelper.expectElement(elem, '.location .unstyled-button .fa-pencil');
                SpecHelper.expectElement(elem, '.links .unstyled-button .fa-pencil');
                SpecHelper.expectElement(elem, '#education .unstyled-button .fa-pencil');
            });

            it('should show a linkedin link', () => {
                render();
                SpecHelper.expectElement(elem, '.fa-linkedin');
            });

            it('should show a github link', () => {
                render();
                SpecHelper.expectElement(elem, '.fa-github');
            });

            describe('work experiences', () => {
                beforeEach(() => {
                    // We create work experiences in fixtures/career_profiles.js with start/end dates of
                    // `now`. This means that the following specs asserting that the text displaying those
                    // start/end dates should be asserting something different every new year. So, we just
                    // grab the current year here and use template syntax.
                    year = new Date().getFullYear();
                });

                it('should work with one experience', () => {
                    careerProfile.work_experiences = [careerProfile.work_experiences[0]];
                    render();
                    SpecHelper.expectElementText(elem, '.current-experience', 'Formerly Foo Handler @ Foo Inc.');
                });

                it('should show the plus n text when more than two work experiences', () => {
                    render();
                    SpecHelper.expectElementText(
                        elem,
                        '.current-experience',
                        `Formerly Foo Handler @ Foo Inc. (+ 1 more) Bar Maintainer - Bar LLC (${year} — ${year})`,
                    );
                });

                it('should show additional experience in a hover element', () => {
                    render();
                    SpecHelper.expectElementText(elem, '.tooltip', `Bar Maintainer - Bar LLC (${year} — ${year})`);
                });
            });
        });

        describe('anonymous', () => {
            beforeEach(() => {
                careerProfile.pref_student_network_privacy = 'anonymous';
                careerProfile.anonymized = true;
            });

            it("should show the candidate's initials", () => {
                render();
                expect(scope.userInfo.displayName).toEqual(scope.careerProfile.userInitials);
            });

            it('should show default profile avatar', () => {
                render();
                expect(scope.userInfo.avatarUrl).toEqual(null);
            });

            it('should not show the contact button in preview', () => {
                render();
                SpecHelper.expectNoElement(elem, '.contact');
            });

            it('should not show the contact button outside of preview', () => {
                render(false);
                SpecHelper.expectNoElement(elem, '.contact');
            });

            it('should not show a linkedin link', () => {
                render();
                SpecHelper.expectNoElement(elem, '.fa-linkedin-square');
            });

            it('should not show a github link', () => {
                render();
                SpecHelper.expectNoElement(elem, '.fa-github');
            });

            it('should only show the most recent work experience', () => {
                render();
                SpecHelper.expectElementText(elem, '.current-experience', 'Formerly Foo Handler @ Foo Inc.');
            });

            it('should change data when privacy is anonymous', () => {
                careerProfile.pref_student_network_privacy = 'full';
                render();
                SpecHelper.expectElementText(elem, '.name-links', scope.careerProfile.sanitizedName);
                expect(elem.find('careers-avatar').isolateScope().src).toEqual(scope.careerProfile.sanitizedAvatarSrc);
                scope.careerProfile.pref_student_network_privacy = 'anonymous';
                scope.$apply();
                SpecHelper.expectElementText(elem, '.name-links', scope.careerProfile.userInitials);
                expect(elem.find('careers-avatar').isolateScope().src).toEqual(null);
            });
        });
    });
});
