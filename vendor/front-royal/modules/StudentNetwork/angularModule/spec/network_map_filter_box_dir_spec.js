import 'AngularSpecHelper';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import 'StudentNetwork/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import networkMapFilterBoxLocales from 'StudentNetwork/locales/student_network/network_map_filter_box-en.json';

setSpecLocales(networkMapFilterBoxLocales);

describe('FrontRoyal.StudentNetwork.NetworkMapFilterBox', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let CareerProfile;
    let studentNetworkMapViewModel;
    let StudentNetworkFilterSet;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.StudentNetwork', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                StudentNetworkFilterSet = $injector.get('StudentNetworkFilterSet');

                CareerProfile = $injector.get('CareerProfile');
                $injector.get('CareerProfileFixtures');
            },
        ]);

        SpecHelper.stubDirective('networkMapFilterBoxMini');
        SpecHelper.stubDirective('networkMapFilterBoxAdvancedStudentSearch');
        SpecHelper.stubCurrentUser();
        studentNetworkMapViewModel = SpecHelper.stubStudentNetworkMapViewModel();
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.studentNetworkMapViewModel = studentNetworkMapViewModel;
        renderer.render(
            '<network-map-filter-box student-network-map-view-model="studentNetworkMapViewModel"></network-map-filter-box>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    it('should show advanced search at appropriate times', () => {
        jest.spyOn(StudentNetworkFilterSet.prototype, 'ensureStudentProfilesPreloaded').mockImplementation(() => {});
        CareerProfile.expect('index');
        render();

        SpecHelper.expectNoElement(elem, 'network-map-filter-box-advanced-student-search');
        SpecHelper.expectElement(elem, 'network-map-filter-box-mini');

        scope.studentNetworkMapViewModel.studentsMapLayer.inAdvancedSearchMode = true;
        scope.$apply();

        SpecHelper.expectElement(elem, 'network-map-filter-box-advanced-student-search');
        SpecHelper.expectNoElement(elem, 'network-map-filter-box-mini');
    });
});
