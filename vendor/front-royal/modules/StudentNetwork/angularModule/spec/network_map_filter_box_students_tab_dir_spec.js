import 'AngularSpecHelper';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import 'StudentNetwork/angularModule';
import networkMapFilterBoxLocales from 'StudentNetwork/locales/student_network/network_map_filter_box-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(networkMapFilterBoxLocales);

describe('FrontRoyal.StudentNetwork.NetworkMapFilterBox', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let currentUser;
    let studentNetworkMapViewModel;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.StudentNetwork', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                $injector.get('CareerProfileFixtures');
            },
        ]);

        currentUser = SpecHelper.stubCurrentUser();
        studentNetworkMapViewModel = SpecHelper.stubStudentNetworkMapViewModel();
        jest.spyOn(studentNetworkMapViewModel.studentsMapLayer, 'refresh').mockImplementation(() => {});
        jest.spyOn(studentNetworkMapViewModel.studentsMapLayer, 'applyFilters').mockImplementation(() => {});
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.studentNetworkMapViewModel = studentNetworkMapViewModel;
        renderer.render(
            '<network-map-filter-box-students-tab students-map-layer="studentNetworkMapViewModel.studentsMapLayer"></network-map-filter-box-students-tab>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    describe('applyFilters', () => {
        it('should call through to studentsMapLayer', () => {
            render();

            scope.applyFilters(studentNetworkMapViewModel.studentsMapLayer.filters);
            expect(scope.studentsMapLayer.applyFilters).toHaveBeenCalledWith(
                studentNetworkMapViewModel.studentsMapLayer.filters,
            );
        });
    });

    describe('class filter', () => {
        it('should hide myClass filter based on presence of myClassCohortId', () => {
            render();
            scope.studentsMapLayer.myClassCohortId = 123;
            scope.$digest();
            SpecHelper.expectElement(elem, '[name="mine"]');
            scope.studentsMapLayer.myClassCohortId = null;
            scope.$digest();
            SpecHelper.expectNoElement(elem, '[name="mine"]');
        });

        it('should hide class filter when in an isolated cohort', () => {
            render();
            SpecHelper.expectElement(elem, '[name="class-filter"]');
            currentUser.relevant_cohort = {
                isolated_network: true,
            };
            scope.$digest();
            SpecHelper.expectNoElement(elem, '[name="class-filter"]');
        });
    });

    describe('advance search active', () => {
        it('should show when advanced filters are set', () => {
            render();
            SpecHelper.expectElement(elem, '[name="basic_search"]');

            scope.studentsMapLayer.advancedFilters = {
                keyword_search: 'blah',
            };
            scope.studentsMapLayer.applyAdvancedFilters();
            scope.$digest();

            SpecHelper.expectNoElement(elem, '[name="basic_search"]');
            SpecHelper.expectElementText(elem, '.summary-pills', '"blah"');
        });
    });
});
