import 'AngularSpecHelper';
import 'StudentNetwork/angularModule';

import * as userAgentHelper from 'userAgentHelper';
import setSpecLocales from 'Translation/setSpecLocales';
import studentNetworkEventDetailsLocales from 'StudentNetwork/locales/student_network/student_network_event_details-en.json';
import studentNetworkEventLocales from 'StudentNetwork/locales/student_network/student_network_event-en.json';
import addToCalendarWidgetLocales from 'AddToCalendarWidget/locales/add_to_calendar_widget/add_to_calendar_widget-en.json';
import 'StudentNetwork/angularModule/spec/_mock/fixtures/student_network_events';

setSpecLocales(studentNetworkEventDetailsLocales, studentNetworkEventLocales, addToCalendarWidgetLocales);

describe('FrontRoyal.StudentNetwork.StudentNetworkEventDetails', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let event;
    let StudentNetworkEvent;
    let safeApply;
    let safeApplySpy;
    let NavigationHelperMixin;
    let ErrorLogService;
    let $window;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.StudentNetwork', 'SpecHelper');

        angular.mock.module($provide => {
            safeApplySpy = jest.fn();
            safeApply = theScope => {
                safeApplySpy(theScope);
                theScope.$apply();
            };
            $provide.value('safeApply', safeApply);
        });

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                StudentNetworkEvent = $injector.get('StudentNetworkEvent');
                safeApply = $injector.get('safeApply');
                NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
                ErrorLogService = $injector.get('ErrorLogService');
                $window = $injector.get('$window');
                const RouteAssetLoader = $injector.get('Navigation.RouteAssetLoader');
                const $q = $injector.get('$q');

                $injector.get('StudentNetworkEventFixtures');

                jest.spyOn(RouteAssetLoader, 'loadGooglePlacesDependencies').mockReturnValue($q.when());
            },
        ]);

        event = StudentNetworkEvent.fixtures.getInstance();

        SpecHelper.stubNgMap();
        SpecHelper.stubCurrentUser();
    });

    function render(opts = {}) {
        renderer = SpecHelper.renderer();
        renderer.scope.event = event;
        renderer.scope.containerSelector = angular.isDefined(opts.containerSelector)
            ? opts.containerSelector
            : '.student-network-event-details-container';
        renderer.render(
            `<div class="student-network-event-details-container"><student-network-event-details event="event" ${
                renderer.scope.containerSelector ? 'container-selector="containerSelector"' : ''
            }></student-network-event-details></div>`,
        );
        elem = renderer.elem;
        scope = elem.find('student-network-event-details').isolateScope();
        scope.$apply();
    }

    it('should hide details section and footer, showing only a spinner until eventDetailsImageLoaded', () => {
        render();
        scope.eventDetailsImageLoaded = false;
        scope.$digest();
        SpecHelper.expectElementHidden(elem, '.main-container');

        // rendered, but inside of .main-container, which isn't visible
        SpecHelper.expectElement(elem, '.main-container section.details');
        SpecHelper.expectElement(elem, '.main-container footer');

        SpecHelper.expectElement(elem, 'front-royal-spinner');
        scope.eventDetailsImageLoaded = true;
        scope.$digest();
        SpecHelper.expectElementNotHidden(elem, '.main-container');
        SpecHelper.expectNoElement(elem, 'front-royal-spinner');
    });

    it('should ensureDetailsContentScrollable on window resize', () => {
        render();
        jest.spyOn(scope, 'ensureDetailsContentScrollable').mockImplementation(() => {});
        $(window).trigger(`resize.${scope.$id}`);
        expect(scope.ensureDetailsContentScrollable).toHaveBeenCalled();
    });

    it('should turn off window resize event handler on scope $destroy', () => {
        render();
        const mockWindow = $(window);
        jest.spyOn(mockWindow, 'off').mockImplementation(() => {});
        jest.spyOn(window, '$').mockReturnValue(mockWindow);
        scope.$destroy();
        expect(mockWindow.off).toHaveBeenCalledWith(`resize.${scope.$id}`);
    });

    it('should set eventDetailsImageLoaded to true and safeApply on image load', () => {
        render();
        const eventDetailsImage = elem.find('#event-details-image');
        expect(scope.eventDetailsImageLoaded).not.toBe(true);
        expect(safeApplySpy).not.toHaveBeenCalled();
        eventDetailsImage.trigger('load');
        expect(scope.eventDetailsImageLoaded).toBe(true);
        expect(safeApplySpy).toHaveBeenCalled();
    });

    it('should onEventDetailsImageLoad when imageLoadError', () => {
        render();
        jest.spyOn(scope, 'onEventDetailsImageLoad').mockImplementation(() => {});
        expect(scope.imageLoadError).not.toBe(true);
        expect(scope.onEventDetailsImageLoad).not.toHaveBeenCalled();
        scope.imageLoadError = true;
        scope.$digest();
        expect(scope.onEventDetailsImageLoad).toHaveBeenCalled();
    });

    it('should show .event-details-image-placeholder instead of #event-details-image when imageLoadError', () => {
        render();
        jest.spyOn(scope, 'onEventDetailsImageLoad').mockImplementation(() => {}); // mock this to prevent digest already in progress error
        expect(scope.imageLoadError).toBeFalsy();
        SpecHelper.expectNoElement(elem, '#event-details-image-placeholder');
        scope.imageLoadError = true;
        scope.$digest();
        SpecHelper.expectElement(elem, '#event-details-image-placeholder');
    });

    describe('when no containerSelector is present', () => {
        it('should not bother to ensureDetailsContentScrollable after eventDetailsImageLoaded if no containerSelector', () => {
            render({
                containerSelector: null,
            });
            jest.spyOn(scope, 'ensureDetailsContentScrollable').mockImplementation(() => {});
            expect(scope.eventDetailsImageLoaded).not.toBe(true);
            scope.eventDetailsImageLoaded = true;
            scope.$digest();
            expect(scope.ensureDetailsContentScrollable).not.toHaveBeenCalled();
        });
    });

    describe('onEventDetailsImageLoad method', () => {
        it('should set eventDetailsImageLoaded to true, safeApply, and then ensureDetailsContentScrollable', () => {
            render();
            jest.spyOn(scope, 'ensureDetailsContentScrollable').mockImplementation(() => {});
            expect(scope.eventDetailsImageLoaded).not.toBe(true);
            scope.onEventDetailsImageLoad();
            expect(scope.eventDetailsImageLoaded).toBe(true);
            expect(safeApplySpy).toHaveBeenCalled();
            expect(scope.ensureDetailsContentScrollable).toHaveBeenCalled();
        });
    });

    describe('toggleDescriptionExpanded method', () => {
        it('should toggle descriptionExpanded scope property', () => {
            render();
            expect(scope.descriptionExpanded).not.toBe(true);
            scope.toggleDescriptionExpanded();
            expect(scope.descriptionExpanded).toBe(true);
            scope.toggleDescriptionExpanded();
            expect(scope.descriptionExpanded).toBe(false);
            scope.toggleDescriptionExpanded();
            expect(scope.descriptionExpanded).toBe(true);
        });
    });

    describe('rsvpForEvent method', () => {
        it("should load the event's external_rsvp_url in a new tab", () => {
            render();
            jest.spyOn(NavigationHelperMixin, 'loadUrl').mockImplementation(() => {});
            scope.rsvpForEvent();
            expect(NavigationHelperMixin.loadUrl).toHaveBeenCalledWith(scope.event.external_rsvp_url, '_blank');
        });
    });

    describe('when eventDetailsImageLoaded', () => {
        function renderAndSetEventDetailsImageLoaded() {
            render();
            scope.eventDetailsImageLoaded = true;
            scope.$digest();
        }

        describe('header section', () => {
            it('should display just the location_name', () => {
                event.location_name = 'Capital Ale House';
                jest.spyOn(event, 'placeCity', 'get').mockReturnValue('Harrisonburg');
                renderAndSetEventDetailsImageLoaded();
                SpecHelper.expectElementText(elem, 'header hgroup h3', 'Capital Ale House');
            });
        });

        describe('date and time section', () => {
            beforeEach(() => {
                event.timezone = 'America/New_York';
                event.start_time = 1571774400; // Tuesday, October 22 @ 4:00 PM EDT
                event.end_time = 1571778000; // Tuesday, October 22 @ 5:00 PM EDT
            });

            it('should show the start date', () => {
                jest.spyOn(event, 'formattedStartDateTime');
                renderAndSetEventDetailsImageLoaded();
                expect(event.formattedStartDateTime).toHaveBeenCalledWith('dddd, MMMM D', scope.currentUser.timezone);
                SpecHelper.expectElementText(
                    elem,
                    'section.details section.date-and-time .event-date',
                    'Tuesday, October 22',
                );
            });

            it("should show the start time and end time in the event's configured timezone and show the timezone abbreviation for clarity", () => {
                jest.spyOn(event, 'formattedStartDateTime');
                jest.spyOn(event, 'formattedEndDateTime');
                renderAndSetEventDetailsImageLoaded();
                expect(event.formattedStartDateTime).toHaveBeenCalledWith('h:mm A', scope.currentUser.timezone);
                expect(event.formattedEndDateTime).toHaveBeenCalledWith('h:mm A z', scope.currentUser.timezone);
                SpecHelper.expectElementText(
                    elem,
                    'section.details section.date-and-time .event-time',
                    '4:00 PM – 5:00 PM EDT',
                );
            });

            it('should show the multi-day form when the end_date spans another date', () => {
                event.end_time = 1571803200; // Wednesday, October 23 @ 12:00 AM EDT
                renderAndSetEventDetailsImageLoaded();
                SpecHelper.expectElementText(
                    elem,
                    'section.details section.date-and-time .event-date',
                    'Tuesday, October 22 4:00 PM EDT —',
                    0,
                );
                SpecHelper.expectElementText(
                    elem,
                    'section.details section.date-and-time .event-date',
                    'Wednesday, October 23 12:00 AM EDT',
                    1,
                );
            });

            it('should handle indeterminate dates', () => {
                event.date_tbd = true;
                event.date_tbd_description = 'this date is yet to be determined';
                event.start_time = undefined;
                event.end_time = undefined;
                renderAndSetEventDetailsImageLoaded();
                SpecHelper.expectElementText(
                    elem,
                    'section.details section.date-and-time .event-date',
                    event.date_tbd_description,
                );
            });

            it('should not show add-to-calendar-widget when indeterminate dates', () => {
                event.date_tbd = true;
                event.date_tbd_description = 'this date is yet to be determined';
                event.start_time = undefined;
                event.end_time = undefined;
                renderAndSetEventDetailsImageLoaded();
                SpecHelper.expectElement(elem, '.date-and-time');
                SpecHelper.expectNoElement(elem, '.date-and-time add-to-calendar-widget');
            });

            it('should show add-to-calendar-widget when dates are not indeterminate', () => {
                renderAndSetEventDetailsImageLoaded();
                SpecHelper.expectElement(elem, '.date-and-time add-to-calendar-widget');
            });
        });

        describe('location details section', () => {
            it('should not show the location details section if the event is not mappable', () => {
                jest.spyOn(event, 'mappable', 'get').mockReturnValue(false);
                renderAndSetEventDetailsImageLoaded();
                SpecHelper.expectNoElement(elem, 'section.details section.location');
            });

            it('should not show the location details section if the event is mappable, but !hasPlaceDetails', () => {
                jest.spyOn(event, 'mappable', 'get').mockReturnValue(true);
                jest.spyOn(event, 'hasPlaceDetails', 'get').mockReturnValue(false);
                renderAndSetEventDetailsImageLoaded();
                SpecHelper.expectNoElement(elem, 'section.details section.location');
            });

            it('should show the location details section if the event is mappable and hasPlaceDetails', () => {
                jest.spyOn(event, 'mappable', 'get').mockReturnValue(true);
                jest.spyOn(event, 'hasPlaceDetails', 'get').mockReturnValue(true);
                renderAndSetEventDetailsImageLoaded();
                SpecHelper.expectElement(elem, 'section.details section.location');
            });

            describe('getDirectionsToEvent', () => {
                it('should support the ability to getDirectionsToEvent', () => {
                    renderAndSetEventDetailsImageLoaded();
                    jest.spyOn(scope, 'getDirectionsToEvent').mockImplementation(() => {});
                    SpecHelper.click(elem, 'section.details section.location .action');
                    expect(scope.getDirectionsToEvent).toHaveBeenCalled();
                });

                it('should alert if an event does not have coordinates', () => {
                    event.place_details.lat = null;
                    event.place_details.lng = 13.37;
                    jest.spyOn(ErrorLogService, 'notify').mockImplementation(() => {});
                    renderAndSetEventDetailsImageLoaded();
                    scope.getDirectionsToEvent(event);
                    expect(ErrorLogService.notify).toHaveBeenCalledWith(
                        'Student Network Event missing coordinates',
                        null,
                        {
                            event_id: event.id,
                        },
                    );
                });

                it('should launch google maps using http if not on mobile', () => {
                    jest.spyOn(userAgentHelper, 'isiOSDevice').mockReturnValue(false);
                    jest.spyOn(userAgentHelper, 'isAndroidDevice').mockReturnValue(false);
                    event.place_id = '1337';
                    event.place_details.formatted_address = '123 Timbuktoo Ln';
                    jest.spyOn(NavigationHelperMixin, 'loadUrl').mockImplementation(() => {});
                    renderAndSetEventDetailsImageLoaded();
                    scope.getDirectionsToEvent(event);
                    expect(NavigationHelperMixin.loadUrl).toHaveBeenCalledWith(
                        'https://www.google.com/maps/dir/?api=1&origin=My+Location&destination_place_id=1337&destination=123%20Timbuktoo%20Ln',
                        '_blank',
                    );
                });

                describe('mobile', () => {
                    beforeEach(() => {
                        event.place_id = '1337';
                        event.place_details.name = 'Timbuktoo';
                        event.place_details.lat = 13.37;
                        event.place_details.lng = 42;
                        event.place_details.formatted_address = '123 Timbuktoo Ln';
                        jest.spyOn($window, 'open').mockImplementation(() => {});
                        jest.spyOn(NavigationHelperMixin, 'loadUrl').mockImplementation(() => {});
                    });

                    describe('ios device', () => {
                        beforeEach(() => {
                            jest.spyOn(userAgentHelper, 'isiOSDevice').mockReturnValue(true);
                        });

                        it('should launch native maps app when in cordova', () => {
                            window.CORDOVA = true;
                            renderAndSetEventDetailsImageLoaded();
                            scope.getDirectionsToEvent(event);
                            expect(NavigationHelperMixin.loadUrl).toHaveBeenCalledWith(
                                'maps://?q=13.37,42(Timbuktoo)',
                                '_blank',
                            );
                            delete window.CORDOVA;
                        });

                        it('should launch native maps app when in mobile safari', () => {
                            jest.spyOn(userAgentHelper, 'isMobileSafari').mockReturnValue(true);
                            renderAndSetEventDetailsImageLoaded();
                            scope.getDirectionsToEvent(event);
                            expect(NavigationHelperMixin.loadUrl).toHaveBeenCalledWith(
                                'maps://?q=13.37,42(Timbuktoo)',
                                '_blank',
                            );
                        });

                        it('should launch maps app in browser tab otherwise', () => {
                            jest.spyOn(userAgentHelper, 'isMobileSafari').mockReturnValue(false);
                            renderAndSetEventDetailsImageLoaded();
                            scope.getDirectionsToEvent(event);
                            expect(NavigationHelperMixin.loadUrl).toHaveBeenCalledWith(
                                'https://www.google.com/maps/dir/?api=1&origin=My+Location&destination_place_id=1337&destination=123%20Timbuktoo%20Ln',
                                '_blank',
                            );
                        });
                    });

                    describe('android device', () => {
                        beforeEach(() => {
                            jest.spyOn(userAgentHelper, 'isAndroidDevice').mockReturnValue(true);
                        });

                        it('should launch native maps app when in cordova', () => {
                            window.CORDOVA = true;
                            renderAndSetEventDetailsImageLoaded();
                            scope.getDirectionsToEvent(event);
                            expect(NavigationHelperMixin.loadUrl).toHaveBeenCalledWith(
                                'geo:13.37,42?q=13.37,42(Timbuktoo)',
                                '_blank',
                            );
                            delete window.CORDOVA;
                        });

                        it('should launch native maps app when in mobile chrome', () => {
                            jest.spyOn(userAgentHelper, 'isChrome').mockReturnValue(true);
                            renderAndSetEventDetailsImageLoaded();
                            scope.getDirectionsToEvent(event);
                            expect(NavigationHelperMixin.loadUrl).toHaveBeenCalledWith(
                                'geo:13.37,42?q=13.37,42(Timbuktoo)',
                                '_blank',
                            );
                        });

                        it('should launch maps app in browser tab otherwise', () => {
                            jest.spyOn(userAgentHelper, 'isChrome').mockReturnValue(false);
                            renderAndSetEventDetailsImageLoaded();
                            scope.getDirectionsToEvent(event);
                            expect(NavigationHelperMixin.loadUrl).toHaveBeenCalledWith(
                                'https://www.google.com/maps/dir/?api=1&origin=My+Location&destination_place_id=1337&destination=123%20Timbuktoo%20Ln',
                                '_blank',
                            );
                        });
                    });
                });
            });

            it("should show the location for the event on a map, centered on the event coordinates with the map controls disabled, and a marker showing the event's exact position", () => {
                renderAndSetEventDetailsImageLoaded();
                const mapSelector = `section.details section.location ng-map[center="[${event.coordinates}]"][fullscreen-control="false"][map-type-control="false"][street-view-control="false"]`;
                SpecHelper.expectElement(elem, mapSelector);
                SpecHelper.expectElement(elem, `${mapSelector} marker[position="[${event.coordinates}]"]`);
            });
        });

        describe('footer RSVP button', () => {
            describe("when rsvp_status is 'required'", () => {
                beforeEach(() => {
                    event.rsvp_status = 'required';
                    renderAndSetEventDetailsImageLoaded();
                });

                it('should show a clickable button to rsvpForEvent', () => {
                    SpecHelper.expectElementText(elem, 'footer button', 'RSVP');
                    jest.spyOn(scope, 'rsvpForEvent').mockImplementation(() => {});
                    SpecHelper.click(elem, 'footer button');
                    expect(scope.rsvpForEvent).toHaveBeenCalled();
                });
            });

            describe("when rsvp_status is 'not_required'", () => {
                beforeEach(() => {
                    event.rsvp_status = 'not_required';
                    renderAndSetEventDetailsImageLoaded();
                });

                it("should show a disabled button that says 'RSVP Not Required'", () => {
                    SpecHelper.expectElementDisabled(elem, 'footer button');
                    SpecHelper.expectElementText(elem, 'footer button', 'RSVP Not Required');
                });
            });

            describe("when rsvp_status is 'closed'", () => {
                beforeEach(() => {
                    event.rsvp_status = 'closed';
                    renderAndSetEventDetailsImageLoaded();
                });

                it("should show a disabled button that says 'RSVP Closed'", () => {
                    SpecHelper.expectElementDisabled(elem, 'footer button');
                    SpecHelper.expectElementText(elem, 'footer button', 'RSVP Closed');
                });
            });
        });
    });
});
