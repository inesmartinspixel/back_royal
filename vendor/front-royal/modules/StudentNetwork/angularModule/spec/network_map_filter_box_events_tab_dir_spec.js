import 'AngularSpecHelper';
import 'StudentNetwork/angularModule';
import networkMapFilterBoxLocales from 'StudentNetwork/locales/student_network/network_map_filter_box-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(networkMapFilterBoxLocales);

describe('FrontRoyal.StudentNetwork.NetworkMapFilterBox', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let eventsMapLayer;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.StudentNetwork', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
            },
        ]);

        SpecHelper.stubCurrentUser();

        eventsMapLayer = {
            selectedMapFiltersSummary: '<span>Showing all events</span>',
            showEventTypeFilters: jest.fn(),
            resetMapFilters: jest.fn(),
            mobileState: {},
        };
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.eventsMapLayer = eventsMapLayer;
        renderer.render(
            '<network-map-filter-box-events-tab events-map-layer="eventsMapLayer"></network-map-filter-box-events-tab>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    it('should show events tab', () => {
        render();
        SpecHelper.expectElementText(elem, '.events-row [name="edit"]', 'Filter Map...');
    });

    it("should show the eventsMapLayer's selectedMapFiltersSummary", () => {
        render();
        SpecHelper.expectElementText(elem, '.summary-pills.events-filters', 'Showing all events');
        scope.eventsMapLayer.selectedMapFiltersSummary = '<span>Foo Bar Baz</span>';
        scope.$digest();
        SpecHelper.expectElementText(elem, '.summary-pills.events-filters', 'Foo Bar Baz');
    });

    it('should call eventsMapLayer.showEventTypeFilters when edit button is clicked', () => {
        render();
        expect(eventsMapLayer.showEventTypeFilters).not.toHaveBeenCalled();
        SpecHelper.click(elem, '.events-row [name="edit"]');
        expect(eventsMapLayer.showEventTypeFilters).toHaveBeenCalled();
    });

    it('should call eventsMapLayer.resetMapFilters when reset filters button is clicked', () => {
        render();
        expect(eventsMapLayer.resetMapFilters).not.toHaveBeenCalled();
        SpecHelper.click(elem, '.events-row [name="reset"]');
        expect(eventsMapLayer.resetMapFilters).toHaveBeenCalled();
    });

    it('should set eventsMapLayer.mobileState.showingRecommendedEventsList to true when recommended events button is clicked', () => {
        render();
        expect(eventsMapLayer.mobileState.showingRecommendedEventsList).toBeUndefined();
        SpecHelper.click(elem, 'button.advanced-search.recommended-events');
        expect(eventsMapLayer.mobileState.showingRecommendedEventsList).toBe(true);
    });
});
