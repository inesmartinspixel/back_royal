import 'AngularSpecHelper';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import 'StudentNetwork/angularModule';
import networkMapFilterBoxLocales from 'StudentNetwork/locales/student_network/network_map_filter_box-en.json';
import chooseAnItemLocales from 'FrontRoyalForm/locales/front_royal_form/inputs/choose_an_item-en.json';
import fieldOptionsLocales from 'StudentNetwork/locales/student_network/field_options-en.json';
import fieldOptionsLocalesCareers from 'Careers/locales/careers/field_options-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(networkMapFilterBoxLocales, chooseAnItemLocales, fieldOptionsLocales, fieldOptionsLocalesCareers);

describe('FrontRoyal.StudentNetwork.NetworkMapFilterBox', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let CareerProfile;
    let currentUser;
    let StudentNetworkFilterSet;
    let studentNetworkMapViewModel;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.StudentNetwork', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');

                StudentNetworkFilterSet = $injector.get('StudentNetworkFilterSet');
                CareerProfile = $injector.get('CareerProfile');
                $injector.get('CareerProfileFixtures');
            },
        ]);

        SpecHelper.stubDirective('locationAutocomplete');
        SpecHelper.stubDirective('studentProfileList');
        currentUser = SpecHelper.stubCurrentUser();
        studentNetworkMapViewModel = SpecHelper.stubStudentNetworkMapViewModel();
        jest.spyOn(studentNetworkMapViewModel.studentsMapLayer, 'refresh').mockImplementation(() => {});
        jest.spyOn(StudentNetworkFilterSet.prototype, 'ensureStudentProfilesPreloaded');
        CareerProfile.expect('index').returns([
            {
                id: 123,
            },
        ]);
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.studentNetworkMapViewModel = studentNetworkMapViewModel;
        renderer.render(
            '<network-map-filter-box-advanced-student-search students-map-layer="studentNetworkMapViewModel.studentsMapLayer"></network-map-filter-box-advanced-student-search>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
        studentNetworkMapViewModel.studentsMapLayer.inAdvancedSearchMode = true;
        // this would normally be triggered outside of this component via a watcher; emulate it here
        studentNetworkMapViewModel.studentsMapLayer.applyFilters(studentNetworkMapViewModel.studentsMapLayer.filters);
        scope.$apply();
        CareerProfile.flush('index');
    }

    it('should show advanced search UI', () => {
        render();
        SpecHelper.expectElement(elem, '.responsive-nav');
        SpecHelper.expectElement(elem, 'student-profile-list');
        expect(scope.studentsMapLayer.studentProfileFilterSet).toBeDefined();
        expect(StudentNetworkFilterSet.prototype.ensureStudentProfilesPreloaded).toHaveBeenCalled();
    });

    it('should hide class filter when in an isolated cohort', () => {
        render();
        SpecHelper.expectElement(elem, '[name="class"]');
        currentUser.relevant_cohort = {
            isolated_network: true,
        };
        scope.$digest();
        SpecHelper.expectNoElement(elem, '[name="class"]');
    });

    describe('applyAdvancedFilters', () => {
        beforeEach(() => {
            render();
            scope.isMobile = true;
            scope.$digest();
        });

        it('should be triggered by clicking the button', () => {
            jest.spyOn(scope, 'applyAdvancedFilters').mockImplementation(() => {});
            SpecHelper.click(elem, '[name="apply-advanced-filters"]');
            expect(scope.applyAdvancedFilters).toHaveBeenCalled();
        });

        it('should be triggered by <enter> within keyword_search', () => {
            jest.spyOn(scope, 'applyAdvancedFilters').mockImplementation(() => {});
            const e = $.Event('keypress');
            e.which = 13;
            elem.find('[name="keyword_search"]').trigger(e);
            expect(scope.applyAdvancedFilters).toHaveBeenCalled();
        });

        describe('mobile header', () => {
            it('should describe selected filters', () => {
                SpecHelper.expectElementText(elem, '.navigation-header-span', 'No Filters Selected');

                // keyword_search
                scope.studentsMapLayer.advancedFilters.keyword_search = 'test';
                scope.applyAdvancedFilters();
                scope.$digest();
                SpecHelper.expectElementText(elem, '.navigation-header-span', '"test"');

                // class
                scope.studentsMapLayer.advancedFilters.class = 'mba';
                scope.applyAdvancedFilters();
                scope.$digest();
                SpecHelper.expectElementText(elem, '.navigation-header-span', '"test" MBA');

                // student_network_looking_for
                scope.studentsMapLayer.advancedFilters.student_network_looking_for = ['co_founder'];
                scope.applyAdvancedFilters();
                scope.$digest();
                SpecHelper.expectElementText(elem, '.navigation-header-span', '"test" MBA Co-founder');

                scope.studentsMapLayer.advancedFilters.keyword_search = undefined;
                scope.studentsMapLayer.advancedFilters.class = null;
                scope.studentsMapLayer.advancedFilters.student_network_looking_for = ['co_founder', 'study_partner'];
                scope.applyAdvancedFilters();
                scope.$digest();
                SpecHelper.expectElementText(elem, '.navigation-header-span', 'Co-founder (+1 option)');

                scope.studentsMapLayer.advancedFilters.student_network_looking_for = [
                    'co_founder',
                    'study_partner',
                    'new_friends',
                ];
                scope.applyAdvancedFilters();
                scope.$digest();
                SpecHelper.expectElementText(elem, '.navigation-header-span', 'Co-founder (+2 options)');

                // student_network_interests
                scope.studentsMapLayer.advancedFilters.student_network_interests = ['AI'];
                scope.applyAdvancedFilters();
                scope.$digest();
                SpecHelper.expectElementText(elem, '.navigation-header-span', 'Co-founder (+2 options) AI');

                scope.studentsMapLayer.advancedFilters.student_network_interests = ['AI', 'Art'];
                scope.applyAdvancedFilters();
                scope.$digest();
                SpecHelper.expectElementText(
                    elem,
                    '.navigation-header-span',
                    'Co-founder (+2 options) AI (+1 interest)',
                );

                // industries
                scope.studentsMapLayer.advancedFilters.industries = ['consulting'];
                scope.applyAdvancedFilters();
                scope.$digest();
                SpecHelper.expectElementText(
                    elem,
                    '.navigation-header-span',
                    'Co-founder (+2 options) AI (+1 interest) Consulting',
                );

                scope.studentsMapLayer.advancedFilters.industries = ['consulting', 'energy'];
                scope.applyAdvancedFilters();
                scope.$digest();
                SpecHelper.expectElementText(
                    elem,
                    '.navigation-header-span',
                    'Co-founder (+2 options) AI (+1 interest) Consulting (+1 industry)',
                );

                // alumni
                scope.studentsMapLayer.advancedFilters.alumni = true;
                scope.applyAdvancedFilters();
                scope.$digest();
                SpecHelper.expectElementText(
                    elem,
                    '.navigation-header-span',
                    'Co-founder (+2 options) AI (+1 interest) Consulting (+1 industry) Alumni',
                );

                // places
                scope.studentsMapLayer.advancedFilters.places = [
                    {
                        formatted_address: 'a formatted address',
                        locality: {
                            long: 'Foo',
                        },
                        administrative_area_level_1: {
                            short: 'Ba',
                            long: 'Bar',
                        },
                        country: {
                            short: 'US',
                        },
                    },
                    {
                        another: 'place',
                    },
                ];
                scope.applyAdvancedFilters();
                scope.$digest();
                SpecHelper.expectElementText(
                    elem,
                    '.navigation-header-span',
                    'Foo, Ba (+1 location) Co-founder (+2 options) AI (+1 interest) Consulting (+1 industry) Alumni',
                );
            });

            it('should reset filters back to the currently active ones if closed without resetting', () => {
                // set some filters and apply them
                scope.studentsMapLayer.advancedFilters.industries = ['consulting'];
                scope.applyAdvancedFilters();

                // expand the mobile menu, change filters,
                // and then close it
                scope.studentsMapLayer.mobileState.expanded = true;
                scope.$digest();
                scope.studentsMapLayer.advancedFilters.locations = ['consulting', 'energy'];
                scope.$digest();
                scope.studentsMapLayer.mobileState.expanded = false;
                scope.$digest();

                // filters should go back
                expect(scope.studentsMapLayer.advancedFilters.industries).toEqual(['consulting']);
            });

            it('should say ‘Advanced Search Filters…’ while open', () => {
                scope.studentsMapLayer.mobileState.expanded = true;
                scope.$digest();
                SpecHelper.expectElementText(elem, '.navigation-header-span', 'Advanced Search Filters…');
            });
        });
    });

    describe('searchByInterestOnly event', () => {
        it('should trigger method on studentsMapLayer', () => {
            render();
            jest.spyOn(scope.studentsMapLayer, 'searchByInterestOnly').mockImplementation();
            scope.$emit('searchByInterestOnly', 'alligator wrestling');
            expect(scope.studentsMapLayer.searchByInterestOnly).toHaveBeenCalledWith('alligator wrestling');
        });
    });
});
