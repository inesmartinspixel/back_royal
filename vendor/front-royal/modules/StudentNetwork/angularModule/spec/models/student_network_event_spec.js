import 'AngularSpecHelper';
import 'StudentNetwork/angularModule';
import moment from 'moment-timezone';

describe('FrontRoyal.StudentNetwork.StudentNetworkEvent', () => {
    let $injector;
    let StudentNetworkEvent;
    let event;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.StudentNetwork', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector => {
                $injector = _$injector;
                StudentNetworkEvent = $injector.get('StudentNetworkEvent');
            },
        ]);

        event = StudentNetworkEvent.new();
    });

    describe('EVENT_TYPE_CONFIGS', () => {
        describe('icon config setting', () => {
            it('should be set for all event types', () => {
                _.each(StudentNetworkEvent.EVENT_TYPE_CONFIGS_MAP, config => {
                    expect(config.icon).toBeTruthy();
                });
            });
        });

        describe('mappable config setting', () => {
            it('should only be true for mappable event types', () => {
                const expectedMappableEventTypes = [
                    'admissions_event',
                    'career_fair',
                    'company_visit',
                    'conference',
                    'meetup',
                    'special_event',
                ];
                const expectedNonMappableEventTypes = ['book_club', 'online_event'];
                const actualNonMappableEventTypes = getNonMappableEventTypes();
                expect(StudentNetworkEvent.MAPPABLE_EVENT_TYPES.sort()).toEqual(expectedMappableEventTypes.sort());
                expect(actualNonMappableEventTypes.sort()).toEqual(expectedNonMappableEventTypes.sort());
            });
        });

        describe('eventTypeFiltersOrder config setting', () => {
            it('should not have a duplicate value between any event type configs', () => {
                const values = _.chain(StudentNetworkEvent.EVENT_TYPE_CONFIGS_MAP)
                    .map(config => config.eventTypeFiltersOrder)
                    .compact()
                    .value();
                expect(_.uniq(values)).toEqual(values);
            });

            it('should only be set for all mappable event types', () => {
                _.each(StudentNetworkEvent.EVENT_TYPE_CONFIGS_MAP, config => {
                    if (config.mappable) {
                        expect(config.eventTypeFiltersOrder).toBeTruthy();
                    } else {
                        expect(_.has(config, 'eventTypeFiltersOrder')).toBe(false);
                    }
                });
            });

            it('should order the event types in the expected ordering', () => {
                const expectedOrdering = [
                    'meetup',
                    'conference',
                    'company_visit',
                    'special_event',
                    'admissions_event',
                    'career_fair',
                ];
                const actualOrdering = _.sortBy(
                    StudentNetworkEvent.MAPPABLE_EVENT_TYPES,
                    eventType => StudentNetworkEvent.EVENT_TYPE_CONFIGS_MAP[eventType].eventTypeFiltersOrder,
                );
                expect(actualOrdering).toEqual(expectedOrdering);
            });
        });

        describe('visibleInEventTypeFiltersOnlyWhenAny config setting', () => {
            it('should only be set for specific event types', () => {
                const expectedEventTypes = ['admissions_event', 'career_fair'];
                const actualEventTypes = _.filter(Object.keys(StudentNetworkEvent.EVENT_TYPE_CONFIGS_MAP), eventType =>
                    _.has(
                        StudentNetworkEvent.EVENT_TYPE_CONFIGS_MAP[eventType],
                        'visibleInEventTypeFiltersOnlyWhenAny',
                    ),
                );
                expect(actualEventTypes.sort()).toEqual(expectedEventTypes.sort());
            });
        });

        const getNonMappableEventTypes = () =>
            _.without(
                Object.keys(StudentNetworkEvent.EVENT_TYPE_CONFIGS_MAP),
                ...StudentNetworkEvent.MAPPABLE_EVENT_TYPES,
            );
    });

    describe('MAPPABLE_EVENT_TYPES', () => {
        it('should only contain event types that are mappable', () => {
            _.each(StudentNetworkEvent.MAPPABLE_EVENT_TYPES, eventType => {
                expect(StudentNetworkEvent.EVENT_TYPE_CONFIGS_MAP[eventType].mappable).toBe(true);
            });
        });
    });

    [
        {
            method: 'formattedStartDateTime',
            attr: 'start_time',
        },
        {
            method: 'formattedEndDateTime',
            attr: 'end_time',
        },
    ].forEach(config => {
        describe(`${config.method}`, () => {
            beforeEach(() => {
                event[config.attr] = 1571774400; // Tuesday, October 22 @ 4:00 PM EDT
            });

            it(`should return the ${config.attr} in the event's configured timezone, formatted based on the passed in formatting scheme`, () => {
                jest.spyOn(moment.tz, 'guess');
                event.timezone = 'America/New_York';
                expect(event[config.method]('dddd, MMMM D @ h:mm A z')).toEqual('Tuesday, October 22 @ 4:00 PM EDT');
                expect(moment.tz.guess).not.toHaveBeenCalled();

                // change the timezone and verify that the return value is
                // in the new timezone event and honors the requested format
                event.timezone = 'America/Los_Angeles';
                expect(event[config.method]('ddd, MMM D @ k:mm a z', 'America/Chicago')).toEqual(
                    'Tue, Oct 22 @ 13:00 pm PDT',
                );
                expect(moment.tz.guess).not.toHaveBeenCalled();
            });

            it("should use the event's configured timezone, when present, even if a fallbackTimezone is passed in", () => {
                event.timezone = 'America/Los_Angeles';
                expect(event[config.method]('ddd, MMM D @ k:mm a z', 'America/New_York')).toEqual(
                    'Tue, Oct 22 @ 13:00 pm PDT',
                );
            });

            it('should fallback to passed in fallbackTimezone if no timezone is present on the event', () => {
                event.timezone = undefined;
                jest.spyOn(moment.tz, 'guess');
                expect(event[config.method]('dddd, MMMM D @ h:mm A z', 'America/New_York')).toEqual(
                    'Tuesday, October 22 @ 4:00 PM EDT',
                );
                expect(moment.tz.guess).not.toHaveBeenCalled();
            });

            it("should fallback even further to a guess at the user's timezone if no timezone is present on the event and no passed in fallbackTimezone", () => {
                event.timezone = undefined;
                jest.spyOn(moment.tz, 'guess').mockReturnValue('America/New_York');
                expect(event[config.method]('dddd, MMMM D @ h:mm A z', undefined)).toEqual(
                    'Tuesday, October 22 @ 4:00 PM EDT',
                );
                expect(moment.tz.guess).toHaveBeenCalled();
            });
        });
    });

    describe('multiDay', () => {
        it('should be false for single day events', () => {
            event.timezone = 'America/New_York';
            event.start_time = 1571774400; // Tuesday, October 22 @ 4:00 PM EDT
            event.end_time = 1571778000; // Tuesday, October 22 @ 5:00 PM EDT
            expect(event.multiDay).toBe(false);
        });

        it('should be true for multi-day events that are less than 24 hours', () => {
            event.timezone = 'America/New_York';
            event.start_time = 1571774400; // Tuesday, October 22 @ 4:00 PM EDT
            event.end_time = 1571803200; // Wednesday, October 23 @ 12:00 AM EDT
            expect(event.multiDay).toBe(true);
        });

        it('should be false for anonymized events', () => {
            event.timezone = 'America/New_York';
            event.start_time = 1571774400; // Tuesday, October 22 @ 4:00 PM EDT
            event.end_time = undefined;
            event.anonymized = true;
            expect(event.multiDay).toBe(false);
        });
    });
});
