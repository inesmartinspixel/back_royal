import 'AngularSpecHelper';
import 'StudentNetwork/angularModule';
import studentNetworkRecommendedEventListLocales from 'StudentNetwork/locales/student_network/student_network_recommended_event_list-en.json';
import studentNetworkEventLocales from 'StudentNetwork/locales/student_network/student_network_event-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(studentNetworkRecommendedEventListLocales, studentNetworkEventLocales);

describe('FrontRoyal.StudentNetwork.studentNetworkRecommendedEventListFilters', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let eventsMapLayer;
    let isMobile;
    let $window;
    let $timeout;
    let StudentNetworkEvent;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.StudentNetwork', 'SpecHelper');

        angular.mock.module($provide => {
            isMobile = jest.fn();
            isMobile.mockReturnValue(false);
            $provide.value('isMobile', isMobile);
        });

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                $window = $injector.get('$window');
                $timeout = $injector.get('$timeout');
                StudentNetworkEvent = $injector.get('StudentNetworkEvent');
            },
        ]);

        jest.spyOn($window, 'alert').mockImplementation(() => {});

        SpecHelper.stubCurrentUser();
        eventsMapLayer = {
            onSearchEventsClick: jest.fn(),
            applyServerFilters: jest.fn(),
            fetchCurrentAndUpcomingEvents: jest.fn(),
            fetchPastEvents: jest.fn(),
            resetServerFilters: jest.fn(),
            serverFilters: {
                keyword_search: undefined,
                places: undefined,
                event_type: [],
            },
        };

        SpecHelper.stubDirective('locationAutocomplete');
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.eventsMapLayer = eventsMapLayer;
        renderer.render(
            '<student-network-recommended-event-list-filters events-map-layer="eventsMapLayer"></student-network-recommended-event-list-filters>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.isMobile = false;
        scope.$apply();
    }

    it('should set searchKey based on scope.isMobile', () => {
        render();
        expect(scope.searchKey).toEqual('search_all');
        scope.isMobile = true;
        scope.$digest();
        expect(scope.searchKey).toEqual('search_short');
    });

    describe('eventTypeFilterOptions', () => {
        it('should be sorted alphabetically', () => {
            render();
            expect(scope.eventTypeFilterOptions).toEqual(StudentNetworkEvent.EVENT_TYPES.sort());
        });
    });

    describe('upcoming and past filters UI', () => {
        it('should render tab anchors', () => {
            render();
            const anchorLeft = SpecHelper.expectElement(elem, 'a.left');
            SpecHelper.expectElementText(elem, anchorLeft, 'Upcoming');
            const anchorRight = SpecHelper.expectElement(elem, 'a.right');
            SpecHelper.expectElementText(elem, anchorRight, 'Past Events');
        });

        it('should call showUpcomingEvents when upcoming button is clicked', () => {
            render();
            jest.spyOn(scope, 'showUpcomingEvents').mockImplementation(() => {});
            SpecHelper.click(elem, 'a.left');
            expect(scope.showUpcomingEvents).toHaveBeenCalled();
        });

        it('should call showPastEvents when past events button is clicked', () => {
            render();
            jest.spyOn(scope, 'showPastEvents');
            SpecHelper.click(elem, 'a.right');
            expect(scope.showPastEvents).toHaveBeenCalled();
        });
    });

    describe('search button UI', () => {
        it('should render search button', () => {
            render();
            SpecHelper.expectElementText(elem, 'button.advanced-search', 'Search all events...');
        });

        it('should call onSearchEventsClick when clicked', () => {
            render();
            jest.spyOn(scope.eventsMapLayer, 'onSearchEventsClick').mockImplementation(() => {});
            SpecHelper.click(elem, 'button.advanced-search');
            expect(scope.eventsMapLayer.onSearchEventsClick).toHaveBeenCalled();
        });
    });

    describe('when eventsMapLayer.advancedSearchOpen', () => {
        it('should replace the header text', () => {
            render();
            SpecHelper.expectElementText(elem, 'header h2', 'Recommended Events');
            scope.eventsMapLayer.advancedSearchOpen = true;
            scope.$digest();
            SpecHelper.expectElementText(elem, 'header h2', 'Search All Events');
        });

        it('should replace the search button with the cancel button', () => {
            render();
            SpecHelper.expectElement(elem, 'button.advanced-search');
            SpecHelper.expectNoElement(elem, 'button.cancel');
            scope.eventsMapLayer.advancedSearchOpen = true;
            scope.$digest();
            SpecHelper.expectNoElement(elem, 'button.advanced-search');
            SpecHelper.expectElement(elem, 'button.cancel');
        });

        it('should call cancel when cancel button is clicked', () => {
            render();
            scope.eventsMapLayer.advancedSearchOpen = true;
            scope.$digest();
            jest.spyOn(scope, 'cancel').mockImplementation(() => {});
            SpecHelper.click(elem, 'button.cancel');
            expect(scope.cancel).toHaveBeenCalled();
        });

        it('should show filter inputs', () => {
            render();
            SpecHelper.expectNoElement(elem, '.filters-container input.keyword-search');
            SpecHelper.expectNoElement(elem, '.filters-container location-autocomplete');
            SpecHelper.expectNoElement(elem, '.filters-container multi-select');
            scope.eventsMapLayer.advancedSearchOpen = true;
            scope.$digest();
            SpecHelper.expectElement(elem, '.filters-container input.keyword-search');
            SpecHelper.expectElement(elem, '.filters-container location-autocomplete');
            SpecHelper.expectElement(elem, '.filters-container multi-select');
        });

        it("should show 'All Event Types' pill when no event_type filter options have been selected", () => {
            eventsMapLayer.serverFilters.event_type = [];
            render();
            scope.eventsMapLayer.advancedSearchOpen = true;
            scope.$digest();
            SpecHelper.expectElementText(
                elem,
                '.filters-container ul.filter-options li:not(.removable)',
                'All Event Types',
            );
            eventsMapLayer.serverFilters.event_type.push('meetup');
            scope.$digest();
            SpecHelper.expectNoElement(elem, '.filters-container ul.filter-options li:not(.removable)');
            eventsMapLayer.serverFilters.event_type.splice(0, 1);
            scope.$digest();
            SpecHelper.expectElementText(
                elem,
                '.filters-container ul.filter-options li:not(.removable)',
                'All Event Types',
            );
        });

        it("should not have a X icon for pill removal for the 'All Event Types' pill", () => {
            eventsMapLayer.serverFilters.event_type = [];
            render();
            scope.eventsMapLayer.advancedSearchOpen = true;
            scope.$digest();
            SpecHelper.expectElementText(
                elem,
                '.filters-container ul.filter-options li:not(.removable)',
                'All Event Types',
            );
            SpecHelper.expectNoElement(elem, '.filters-container ul.filter-options li:not(.removable) i');
        });

        it('should call removeSelectedEventTypeFilterOption with the event type when X icon', () => {
            eventsMapLayer.serverFilters.event_type = ['meetup'];
            render();
            scope.eventsMapLayer.advancedSearchOpen = true;
            scope.$digest();
            SpecHelper.expectElementText(elem, '.filters-container ul.filter-options li', 'Meetup');
            jest.spyOn(scope, 'removeSelectedEventTypeFilterOption').mockImplementation(() => {});
            SpecHelper.click(elem, '.filters-container ul.filter-options li i');
            expect(scope.removeSelectedEventTypeFilterOption).toHaveBeenCalledWith('meetup');
        });
    });

    describe('when eventsMapLayer.advancedSearchOpen changes to true', () => {
        beforeEach(() => {
            eventsMapLayer.advancedSearchOpen = false;
            render();
            jest.spyOn(scope, 'focusKeywordSearch').mockImplementation(() => {});
        });

        describe('when !isMobile', () => {
            beforeEach(() => {
                scope.isMobile = false;
                scope.$digest();
            });

            it('should focus on the keyword-search filter input', () => {
                eventsMapLayer.advancedSearchOpen = true;
                scope.$digest();
                $timeout.flush();
                expect(scope.focusKeywordSearch).toHaveBeenCalled();
            });
        });

        describe('when isMobile', () => {
            beforeEach(() => {
                scope.isMobile = true;
                scope.$digest();
            });

            it('should not focus on the keyword-search filter input', () => {
                eventsMapLayer.advancedSearchOpen = true;
                scope.$digest();
                $timeout.flush();
                expect(scope.focusKeywordSearch).not.toHaveBeenCalled();
            });
        });
    });

    describe('cancel', () => {
        it('should set eventsMapLayer.advancedSearchOpen to false, resetServerFilters, and call showUpcomingEvents with true', () => {
            render();
            scope.eventsMapLayer.advancedSearchOpen = true;
            jest.spyOn(scope, 'showUpcomingEvents').mockImplementation(() => {});
            scope.cancel();
            expect(scope.eventsMapLayer.advancedSearchOpen).toBe(false);
            expect(scope.eventsMapLayer.resetServerFilters).toHaveBeenCalled();
            expect(scope.showUpcomingEvents).toHaveBeenCalledWith(true);
        });
    });

    describe('showUpcomingEvents', () => {
        describe("when eventsMapLayer.currentRecommendedEventsListTab is not 'upcoming'", () => {
            beforeEach(() => {
                render();
                scope.eventsMapLayer.currentRecommendedEventsListTab = 'past_events';
                scope.$digest();
                expect(scope.eventsMapLayer.currentRecommendedEventsListTab).toEqual('past_events');
            });

            it("should fetchCurrentAndUpcomingEvents and set eventsMapLayer.currentRecommendedEventsListTab to 'upcoming'", () => {
                scope.showUpcomingEvents();
                expect(scope.eventsMapLayer.fetchCurrentAndUpcomingEvents).toHaveBeenCalled();
                expect(scope.eventsMapLayer.currentRecommendedEventsListTab).toEqual('upcoming');
            });
        });

        describe("when eventsMapLayer.currentRecommendedEventsListTab is 'upcoming'", () => {
            beforeEach(() => {
                render();
                scope.eventsMapLayer.currentRecommendedEventsListTab = 'upcoming';
                scope.$digest();
                expect(scope.eventsMapLayer.currentRecommendedEventsListTab).toEqual('upcoming');
            });

            it("should not fetchCurrentAndUpcomingEvents if not forced and set eventsMapLayer.currentRecommendedEventsListTab to 'upcoming'", () => {
                scope.showUpcomingEvents();
                expect(scope.eventsMapLayer.fetchCurrentAndUpcomingEvents).not.toHaveBeenCalled();
                expect(scope.eventsMapLayer.currentRecommendedEventsListTab).toEqual('upcoming');
            });

            it("should fetchCurrentAndUpcomingEvents if forced and set eventsMapLayer.currentRecommendedEventsListTab to 'upcoming'", () => {
                scope.showUpcomingEvents(true);
                expect(scope.eventsMapLayer.fetchCurrentAndUpcomingEvents).toHaveBeenCalled();
                expect(scope.eventsMapLayer.currentRecommendedEventsListTab).toEqual('upcoming');
            });
        });
    });

    describe('removeSelectedEventTypeFilterOption', () => {
        it('should not do anything if no eventType has been passed in', () => {
            render();
            scope.eventsMapLayer.serverFilters.event_type = ['foo', 'bar'];
            jest.spyOn(scope, 'onFilterChange');
            scope.removeSelectedEventTypeFilterOption();
            expect(scope.eventsMapLayer.serverFilters.event_type).toEqual(['foo', 'bar']);
            expect(scope.onFilterChange).not.toHaveBeenCalled();
        });

        it('should not do anything if the passed in eventType is not contained in the serverFilters.event_type filter', () => {
            render();
            scope.eventsMapLayer.serverFilters.event_type = ['foo', 'bar'];
            jest.spyOn(scope, 'onFilterChange');
            scope.removeSelectedEventTypeFilterOption('baz');
            expect(scope.eventsMapLayer.serverFilters.event_type).toEqual(['foo', 'bar']);
            expect(scope.onFilterChange).not.toHaveBeenCalled();
        });

        it('should remove the eventType from the serverFilters.event_type filter in place', () => {
            render();
            // Due to the order of how things are executed in the directive, mocking the onFilterChange method
            // won't be enough because of the debounce for the keyword_search filter $watch handler, so we also
            // need to mock applyServerFilters.
            jest.spyOn(scope.eventsMapLayer, 'applyServerFilters').mockImplementation(() => {});
            const eventTypes = ['foo', 'bar'];
            scope.eventsMapLayer.serverFilters.event_type = eventTypes;
            scope.$digest();

            jest.spyOn(scope, 'onFilterChange').mockImplementation(() => {});
            scope.removeSelectedEventTypeFilterOption('bar');
            expect(scope.eventsMapLayer.serverFilters.event_type).toEqual(['foo']);
            expect(scope.eventsMapLayer.serverFilters.event_type).toBe(eventTypes);
        });
    });

    describe('onFilterChange', () => {
        it('should not applyServerFilters if !eventsMapLayer.advancedSearchOpen', () => {
            render();
            jest.spyOn(scope.eventsMapLayer, 'applyServerFilters').mockImplementation(() => {});
            scope.eventsMapLayer.advancedSearchOpen = false;
            scope.onFilterChange();
            expect(scope.eventsMapLayer.applyServerFilters).not.toHaveBeenCalled();
        });

        it('should not applyServerFilters if newValue is equal to oldValue', () => {
            render();
            scope.eventsMapLayer.advancedSearchOpen = true; // prevent false positive
            jest.spyOn(scope.eventsMapLayer, 'applyServerFilters').mockImplementation(() => {});
            scope.onFilterChange([], []);
            expect(scope.eventsMapLayer.applyServerFilters).not.toHaveBeenCalled();
        });

        describe('when eventsMapLayer.advancedSearchOpen and newValue is not equal to oldValue', () => {
            it('should applyServerFilters if eventsMapLayer.advancedSearchOpen and newValue is not equal to oldValue', () => {
                render();
                scope.eventsMapLayer.advancedSearchOpen = true;
                jest.spyOn(scope.eventsMapLayer, 'applyServerFilters').mockImplementation(() => {});
                scope.onFilterChange(['foo'], []);
                expect(scope.eventsMapLayer.applyServerFilters).toHaveBeenCalled();
            });
        });
    });
});
