import 'AngularSpecHelper';
import 'StudentNetwork/angularModule';
import networkMapFilterBoxLocales from 'StudentNetwork/locales/student_network/network_map_filter_box-en.json';
import studentNetworkEventLocales from 'StudentNetwork/locales/student_network/student_network_event-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(networkMapFilterBoxLocales, studentNetworkEventLocales);

describe('FrontRoyal.StudentNetwork.EventsMapLayer', () => {
    let $injector;
    let $timeout;
    let SpecHelper;
    let eventsMapLayer;
    let studentNetworkMapViewModel;
    let StudentNetworkEvent;
    let studentNetworkEvents;
    let $window;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.StudentNetwork', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                StudentNetworkEvent = $injector.get('StudentNetworkEvent');
                $window = $injector.get('$window');
                $timeout = $injector.get('$timeout');
            },
        ]);

        SpecHelper.stubCurrentUser();
        studentNetworkMapViewModel = SpecHelper.stubStudentNetworkMapViewModel();
        eventsMapLayer = studentNetworkMapViewModel.eventsMapLayer;

        jest.spyOn(studentNetworkMapViewModel.studentsMapLayer, 'refresh').mockImplementation();

        studentNetworkEvents = [
            StudentNetworkEvent.new({
                id: 1,
                event_type: 'meetup',
            }),
            StudentNetworkEvent.new({
                id: 2,
                event_type: 'conference',
            }),
            StudentNetworkEvent.new({
                id: 3,
                event_type: 'admissions_event',
            }),
        ];
    });

    describe('eventTypesAvailableForMapFilters', () => {
        it('should cache the value from _setEventTypesAvailableForMapFilters', () => {
            const expectedReturnValue = ['foo', 'bar'];
            jest.spyOn(eventsMapLayer, '_setEventTypesAvailableForMapFilters').mockImplementation(() => {
                eventsMapLayer.$$eventTypesAvailableForMapFilters = expectedReturnValue;
            });
            let actualReturnValue = eventsMapLayer.eventTypesAvailableForMapFilters;
            expect(eventsMapLayer._setEventTypesAvailableForMapFilters).toHaveBeenCalled();
            expect(actualReturnValue).toEqual(expectedReturnValue);

            eventsMapLayer._setEventTypesAvailableForMapFilters.mockClear();
            actualReturnValue = eventsMapLayer.eventTypesAvailableForMapFilters;
            expect(eventsMapLayer._setEventTypesAvailableForMapFilters).not.toHaveBeenCalled();
            expect(actualReturnValue).toEqual(expectedReturnValue);
        });
    });

    describe('currentRecommendedEventsListTab', () => {
        describe('get', () => {
            describe('when $$currentRecommendedEventsListTab has not been set yet', () => {
                beforeEach(() => {
                    eventsMapLayer.$$currentRecommendedEventsListTab = undefined;
                });

                it("should return 'upcoming'", () => {
                    expect(eventsMapLayer.currentRecommendedEventsListTab).toEqual('upcoming');
                });
            });

            describe('when $$currentRecommendedEventsListTab has been set', () => {
                beforeEach(() => {
                    eventsMapLayer.$$currentRecommendedEventsListTab = 'past_events';
                });

                it('should return $$currentRecommendedEventsListTab', () => {
                    expect(eventsMapLayer.currentRecommendedEventsListTab).toEqual('past_events');
                    eventsMapLayer.$$currentRecommendedEventsListTab = 'upcoming';
                    expect(eventsMapLayer.currentRecommendedEventsListTab).toEqual('upcoming');
                });
            });
        });
    });

    describe('supportsRefreshOnIdleMapEvent', () => {
        it('should return false', () => {
            expect(eventsMapLayer.supportsRefreshOnIdleMapEvent).toBe(false);
        });
    });

    describe('deactivate', () => {
        it('should _clearEventFeatures', () => {
            jest.spyOn(eventsMapLayer, '_clearEventFeatures').mockImplementation(() => {});
            eventsMapLayer.deactivate();
            expect(eventsMapLayer._clearEventFeatures).toHaveBeenCalled();
        });

        it('should _clearEventsForRecommendedEventsList', () => {
            jest.spyOn(eventsMapLayer, '_clearEventsForRecommendedEventsList').mockImplementation(() => {});
            eventsMapLayer.deactivate();
            expect(eventsMapLayer._clearEventsForRecommendedEventsList).toHaveBeenCalled();
        });

        it('should set mobileState.showingRecommendedEventsList to false', () => {
            eventsMapLayer.mobileState.showingRecommendedEventsList = true;
            eventsMapLayer.deactivate();
            expect(eventsMapLayer.mobileState.showingRecommendedEventsList).toBe(false);
        });

        it('should _clearEventTypesAvailableForMapFilters', () => {
            jest.spyOn(eventsMapLayer, '_clearEventTypesAvailableForMapFilters').mockImplementation(() => {});
            eventsMapLayer.deactivate();
            expect(eventsMapLayer._clearEventTypesAvailableForMapFilters).toHaveBeenCalled();
        });

        it('should call closeAllAmbiguousEventMarkerPopovers', () => {
            jest.spyOn(eventsMapLayer, 'closeAllAmbiguousEventMarkerPopovers').mockImplementation(() => {});
            eventsMapLayer.deactivate();
            expect(eventsMapLayer.closeAllAmbiguousEventMarkerPopovers).toHaveBeenCalled();
        });

        it('should call _enableMapGestureHandling', () => {
            jest.spyOn(eventsMapLayer, '_enableMapGestureHandling').mockImplementation(() => {});
            eventsMapLayer.deactivate();
            expect(eventsMapLayer._enableMapGestureHandling).toHaveBeenCalled();
        });

        it('should clear currentRecommendedEventsListTab', () => {
            const spy = jest
                .spyOn(eventsMapLayer, 'currentRecommendedEventsListTab', 'set')
                .mockImplementation(() => {});
            eventsMapLayer.deactivate();
            expect(spy).toHaveBeenCalledWith(null);
        });
    });

    describe('refresh', () => {
        it('should not do anything if already loading', () => {
            jest.spyOn(eventsMapLayer, '_loadEvents');
            eventsMapLayer.loading = true;
            eventsMapLayer.activated = true; // prevent false positive
            eventsMapLayer.refresh();
            expect(eventsMapLayer._loadEvents).not.toHaveBeenCalled();
        });

        it('should not do anything if !activated', () => {
            eventsMapLayer.loading = false; // prevent false positive
            eventsMapLayer.activated = false;
            jest.spyOn(eventsMapLayer, '_loadEvents');
            eventsMapLayer.refresh();
            expect(eventsMapLayer._loadEvents).not.toHaveBeenCalled();
        });

        describe('when !loading and activated', () => {
            beforeEach(() => {
                eventsMapLayer.loading = 0;
                eventsMapLayer.activated = true;
                jest.spyOn(eventsMapLayer, 'resetMapFilters').mockImplementation(() => {});
                jest.spyOn(eventsMapLayer, '_updateSelectedMapFiltersSummary').mockImplementation(() => {});
                jest.spyOn(eventsMapLayer, '_setEventTypesAvailableForMapFilters').mockImplementation(() => {});
                jest.spyOn(eventsMapLayer, 'resetServerFilters').mockImplementation(() => {});
                jest.spyOn(eventsMapLayer, '_updateEventsForRecommendedEventsList').mockImplementation(() => {});
            });

            describe('when _loadEvents call is rejected', () => {
                it('should catch and not decrement loading flag if rejected value is false and not set events on eventsMapLayer, resetMapFilters, _updateSelectedMapFiltersSummary, _setEventTypesAvailableForMapFilters, resetServerFilters, or _updateEventsForRecommendedEventsList', () => {
                    jest.spyOn(eventsMapLayer, '_loadEvents').mockRejectedValue(false);
                    eventsMapLayer.refresh().then(() => {
                        expect(eventsMapLayer.loading).toEqual(0);
                        expect(eventsMapLayer._loadEvents).toHaveBeenCalled();
                        expect(eventsMapLayer.events).toBeUndefined();
                        expect(eventsMapLayer.resetMapFilters).not.toHaveBeenCalled();
                        expect(eventsMapLayer._updateSelectedMapFiltersSummary).not.toHaveBeenCalled();
                        expect(eventsMapLayer._setEventTypesAvailableForMapFilters).not.toHaveBeenCalled();
                        expect(eventsMapLayer.resetServerFilters).not.toHaveBeenCalled();
                        expect(eventsMapLayer._updateEventsForRecommendedEventsList).not.toHaveBeenCalled();
                    });
                });

                it('should catch and decrement loading flag if there is a rejected value and not set events on eventsMapLayer, resetMapFilters, _updateSelectedMapFiltersSummary, _setEventTypesAvailableForMapFilters, resetServerFilters, or _updateEventsForRecommendedEventsList', () => {
                    jest.spyOn(eventsMapLayer, '_loadEvents').mockRejectedValue(new Error('some error'));
                    eventsMapLayer.refresh().then(() => {
                        expect(eventsMapLayer.loading).toEqual(-1);
                        expect(eventsMapLayer._loadEvents).toHaveBeenCalled();
                        expect(eventsMapLayer.events).toBeUndefined();
                        expect(eventsMapLayer.resetMapFilters).not.toHaveBeenCalled();
                        expect(eventsMapLayer._updateSelectedMapFiltersSummary).not.toHaveBeenCalled();
                        expect(eventsMapLayer._setEventTypesAvailableForMapFilters).not.toHaveBeenCalled();
                        expect(eventsMapLayer.resetServerFilters).not.toHaveBeenCalled();
                        expect(eventsMapLayer._updateEventsForRecommendedEventsList).not.toHaveBeenCalled();
                    });
                });
            });

            describe('when _loadEvents is resolved', () => {
                let responseResult;

                beforeEach(() => {
                    responseResult = [
                        {
                            foo: 'foo',
                        },
                        {
                            bar: 'bar',
                        },
                    ];
                    const response = {
                        result: responseResult,
                    };
                    jest.spyOn(eventsMapLayer, '_loadEvents').mockResolvedValue(response);
                });

                it('should set events on eventsMapLayer, resetMapFilters, _updateSelectedMapFiltersSummary, _setEventTypesAvailableForMapFilters, resetServerFilters, _updateEventsForRecommendedEventsList, and decrement loading flag', () => {
                    eventsMapLayer.refresh().then(() => {
                        expect(eventsMapLayer._loadEvents).toHaveBeenCalled();
                        expect(eventsMapLayer.events).toEqual(responseResult);
                        expect(eventsMapLayer.resetMapFilters).toHaveBeenCalled();
                        expect(eventsMapLayer._updateSelectedMapFiltersSummary).toHaveBeenCalled();
                        expect(eventsMapLayer._setEventTypesAvailableForMapFilters).toHaveBeenCalled();
                        expect(eventsMapLayer.resetServerFilters).toHaveBeenCalled();
                        expect(eventsMapLayer._updateEventsForRecommendedEventsList).toHaveBeenCalledWith(
                            responseResult,
                        );
                        expect(eventsMapLayer.loading).toEqual(-1);
                    });
                });
            });
        });
    });

    describe('resetServerFilters', () => {
        it('should reset all serverFilters', () => {
            eventsMapLayer.serverFilters = {
                end_time: 1000,
                keyword_search: 'foo',
                places: {
                    lat: 43,
                    lng: 21,
                },
                event_type: ['bar', 'baz'],
            };
            eventsMapLayer.resetServerFilters();
            expect(eventsMapLayer.serverFilters).toEqual({
                include_tbd: true,
                end_time: undefined,
                keyword_search: undefined,
                places: undefined,
                event_type: [],
            });
        });
    });

    describe('resetMapFilters', () => {
        it('should _updateAndApplyMapFilters with no eventTypes', () => {
            jest.spyOn(eventsMapLayer, '_updateAndApplyMapFilters').mockImplementation(() => {});
            eventsMapLayer.resetMapFilters();
            expect(eventsMapLayer._updateAndApplyMapFilters).toHaveBeenCalledWith({
                eventTypes: [],
            });
        });
    });

    describe('event marker interactions', () => {
        describe('onEventMarkerClick', () => {
            let eventFeature;
            let firstEvent;

            beforeEach(() => {
                eventFeature = {
                    uid: studentNetworkEvents[0].id,
                };
                firstEvent = studentNetworkEvents[0];
                jest.spyOn(eventsMapLayer, '_getEventForEventId').mockReturnValue(firstEvent);
            });

            it('should not openAmbiguousEventMarkerPopover or showEventDetails if event feature has been anonymized', () => {
                eventFeature.anonymized = true;
                jest.spyOn(eventsMapLayer, 'openAmbiguousEventMarkerPopover');
                jest.spyOn(eventsMapLayer, 'showEventDetails');
                eventsMapLayer.onEventMarkerClick(eventFeature);
                expect(eventsMapLayer.openAmbiguousEventMarkerPopover).not.toHaveBeenCalled();
                expect(eventsMapLayer.showEventDetails).not.toHaveBeenCalled();
            });

            describe('when visuallyAmbiguousEvents', () => {
                let secondEvent;

                beforeEach(() => {
                    firstEvent.place_details = {
                        lat: 1,
                        lng: 1,
                    };
                    secondEvent = studentNetworkEvents[1];
                    jest.spyOn(eventsMapLayer, '_getVisuallyAmbiguousEvents').mockReturnValue([secondEvent]);
                });

                it('should center map on lat/lng of clicked event', () => {
                    const mockLatLngInstance = {
                        foo: 'foo',
                    };
                    $window.google.maps.LatLng.mockImplementation(() => mockLatLngInstance);
                    eventsMapLayer.onEventMarkerClick(eventFeature);
                    expect($window.google.maps.LatLng).toHaveBeenCalledWith(1, 1);
                    expect(eventsMapLayer.map.panToBounds).toHaveBeenCalled();
                });

                it('should set ambiguousEvents on passed-in eventFeature', () => {
                    expect(eventFeature.ambiguousEvents).toBeUndefined();
                    eventsMapLayer.onEventMarkerClick(eventFeature);
                    expect(eventsMapLayer._getVisuallyAmbiguousEvents).toHaveBeenCalled();
                    expect(eventFeature.ambiguousEvents).not.toBeUndefined();
                    expect(eventFeature.ambiguousEvents).toEqual([firstEvent, secondEvent]);
                });

                it('should call openAmbiguousEventMarkerPopover', () => {
                    jest.spyOn(eventsMapLayer, 'openAmbiguousEventMarkerPopover').mockImplementation(() => {});
                    eventsMapLayer.onEventMarkerClick(eventFeature);
                    $timeout.flush();
                    expect(eventsMapLayer.openAmbiguousEventMarkerPopover).toHaveBeenCalledWith(eventFeature);
                });
            });

            describe('when no visuallyAmbiguousEvents', () => {
                beforeEach(() => {
                    jest.spyOn(eventsMapLayer, '_getVisuallyAmbiguousEvents').mockReturnValue([]);
                });

                it('should call closeAmbiguousEventMarkerPopover', () => {
                    jest.spyOn(eventsMapLayer, 'closeAmbiguousEventMarkerPopover').mockImplementation(() => {});
                    eventsMapLayer.onEventMarkerClick(eventFeature);
                    expect(eventsMapLayer.closeAmbiguousEventMarkerPopover).toHaveBeenCalledWith(eventFeature);
                });

                it('should call showEventDetails', () => {
                    jest.spyOn(eventsMapLayer, 'showEventDetails').mockImplementation(() => {});
                    eventsMapLayer.onEventMarkerClick(eventFeature);
                    expect(eventsMapLayer.showEventDetails).toHaveBeenCalledWith(firstEvent);
                });
            });
        });
    });

    describe('event details', () => {
        describe('onViewEvent', () => {
            it('should call closeAllAmbiguousEventMarkerPopovers if passed in event is mappable', () => {
                jest.spyOn(eventsMapLayer, 'closeAllAmbiguousEventMarkerPopovers').mockImplementation(() => {});
                const event = studentNetworkEvents[0];
                jest.spyOn(event, 'mappable', 'get').mockReturnValue(true);
                eventsMapLayer.onViewEvent(event);
                expect(eventsMapLayer.closeAllAmbiguousEventMarkerPopovers).toHaveBeenCalled();

                eventsMapLayer.closeAllAmbiguousEventMarkerPopovers.mockClear();
                jest.spyOn(event, 'mappable', 'get').mockReturnValue(false);
                eventsMapLayer.onViewEvent(event);
                expect(eventsMapLayer.closeAllAmbiguousEventMarkerPopovers).not.toHaveBeenCalled();
            });

            it('should call showEventDetails', () => {
                const event = studentNetworkEvents[0];
                jest.spyOn(eventsMapLayer, 'showEventDetails').mockImplementation(() => {});
                eventsMapLayer.onViewEvent(event);
                expect(eventsMapLayer.showEventDetails).toHaveBeenCalledWith(event);
            });
        });

        describe('showEventDetails', () => {
            beforeEach(() => {
                jest.spyOn(
                    eventsMapLayer.studentNetworkMapViewModel,
                    'showEventDetailsModal',
                ).mockImplementation(() => {});
            });

            it('should do nothing if no event was passed in', () => {
                eventsMapLayer.showEventDetails();
                expect(eventsMapLayer.studentNetworkMapViewModel.showEventDetailsModal).not.toHaveBeenCalled();
            });

            it("should do nothing if there aren't any events", () => {
                eventsMapLayer.events = [];
                eventsMapLayer.showEventDetails();
                expect(eventsMapLayer.studentNetworkMapViewModel.showEventDetailsModal).not.toHaveBeenCalled();
            });

            describe('when eventFeature is passed in and events are present', () => {
                it('should set showingEventTypeFiltersModal to false call showEventDetailsModal on the viewmodel', () => {
                    eventsMapLayer.events = studentNetworkEvents;
                    const event = studentNetworkEvents[0];
                    eventsMapLayer.showEventDetails(event);
                    expect(eventsMapLayer.showingEventTypeFiltersModal).toBe(false);
                    expect(eventsMapLayer.studentNetworkMapViewModel.showEventDetailsModal).toHaveBeenCalledWith(event);
                });
            });
        });
    });

    describe('showEventTypeFilters', () => {
        it('should set showingEventTypeFiltersModal to true', () => {
            eventsMapLayer.events = studentNetworkEvents;
            eventsMapLayer.mapFilters.eventTypes = ['foo', 'bar'];
            jest.spyOn(eventsMapLayer, 'eventTypesAvailableForMapFilters', 'get').mockReturnValue([
                'foo',
                'bar',
                'baz',
                'qux',
            ]);
            expect(eventsMapLayer.showingEventTypeFiltersModal).not.toBe(true);

            eventsMapLayer.showEventTypeFilters();

            expect(eventsMapLayer.showingEventTypeFiltersModal).toBe(true);
        });
    });

    describe('applyEventTypeFilters', () => {
        it('should set showingEventTypeFiltersModal to false, and _updateAndApplyMapFilters with the passed in filters', () => {
            eventsMapLayer.showEventTypeFilters();

            expect(eventsMapLayer.showingEventTypeFiltersModal).not.toBe(false);
            jest.spyOn(eventsMapLayer, '_updateAndApplyMapFilters').mockImplementation(() => {});

            const newEventTypes = ['foo', 'bar'];
            eventsMapLayer.applyEventTypeFilters(newEventTypes);

            expect(eventsMapLayer.showingEventTypeFiltersModal).toBe(false);
            expect(eventsMapLayer._updateAndApplyMapFilters).toHaveBeenCalledWith({
                eventTypes: newEventTypes,
            });
        });
    });

    describe('popover handling', () => {
        describe('handleGestureControlsOnPopoverStateChange', () => {
            it('should call _enableMapGestureHandling when there are no event features with popoverIsOpen', () => {
                jest.spyOn(eventsMapLayer, '_enableMapGestureHandling').mockImplementation(() => {});
                eventsMapLayer.eventFeatures = [
                    {
                        popoverIsOpen: false,
                    },
                ];
                eventsMapLayer.handleGestureControlsOnPopoverStateChange();
                expect(eventsMapLayer._enableMapGestureHandling).toHaveBeenCalled();
            });

            it('should call _disableMapGestureHandling when there are event features with popoverIsOpen', () => {
                jest.spyOn(eventsMapLayer, '_disableMapGestureHandling').mockImplementation(() => {});
                eventsMapLayer.eventFeatures = [
                    {
                        popoverIsOpen: true,
                    },
                ];
                eventsMapLayer.handleGestureControlsOnPopoverStateChange();
                expect(eventsMapLayer._disableMapGestureHandling).toHaveBeenCalled();
            });
        });
    });

    describe('onSearchEventsClick', () => {
        beforeEach(() => {
            eventsMapLayer.advancedSearchOpen = false;
        });

        describe("when currentRecommendedEventsListTab is not 'past_events'", () => {
            beforeEach(() => {
                jest.spyOn(eventsMapLayer, 'currentRecommendedEventsListTab', 'get').mockReturnValue('past_events');
            });

            it('should set advancedSearchOpen to true and fetchPastEvents instead of calling _updateEventsForRecommendedEventsList', () => {
                jest.spyOn(eventsMapLayer, 'fetchPastEvents').mockImplementation(() => {});
                jest.spyOn(eventsMapLayer, '_updateEventsForRecommendedEventsList');
                eventsMapLayer.onSearchEventsClick();
                expect(eventsMapLayer.advancedSearchOpen).toBe(true);
                expect(eventsMapLayer.fetchPastEvents).toHaveBeenCalled();
                expect(eventsMapLayer._updateEventsForRecommendedEventsList).not.toHaveBeenCalled();
            });
        });

        describe("when currentRecommendedEventsListTab is not 'past_events'", () => {
            beforeEach(() => {
                jest.spyOn(eventsMapLayer, 'currentRecommendedEventsListTab', 'get').mockReturnValue('upcoming');
            });

            it('should set advancedSearchOpen to true and _updateEventsForRecommendedEventsList instead of calling fetchPastEvents', () => {
                jest.spyOn(eventsMapLayer, '_updateEventsForRecommendedEventsList').mockImplementation(() => {});
                jest.spyOn(eventsMapLayer, 'fetchPastEvents');
                eventsMapLayer.onSearchEventsClick();
                expect(eventsMapLayer.advancedSearchOpen).toBe(true);
                expect(eventsMapLayer._updateEventsForRecommendedEventsList).toHaveBeenCalled();
                expect(eventsMapLayer.fetchPastEvents).not.toHaveBeenCalled();
            });
        });
    });

    describe('fetchCurrentAndUpcomingEvents', () => {
        it('should clear serverFilters.end_time and applyServerFilters', () => {
            eventsMapLayer.serverFilters.end_time = 10000;
            jest.spyOn(eventsMapLayer, 'applyServerFilters').mockImplementation(() => {});
            eventsMapLayer.fetchCurrentAndUpcomingEvents();
            expect(eventsMapLayer.serverFilters.end_time).toBeUndefined();
            expect(eventsMapLayer.applyServerFilters).toHaveBeenCalled();
        });
    });

    describe('fetchPastEvents', () => {
        it('should set serverFilters.end_time and applyServerFilters', () => {
            eventsMapLayer.serverFilters.end_time = undefined;
            jest.spyOn(eventsMapLayer, 'applyServerFilters').mockImplementation(() => {});
            jest.spyOn(Date, 'now').mockReturnValue(1234);
            eventsMapLayer.fetchPastEvents();

            // the server expects this to be in seconds, not milliseconds, so this filter value gets divided by 1000 and then floored
            expect(eventsMapLayer.serverFilters.end_time).toEqual(1);
            expect(eventsMapLayer.applyServerFilters).toHaveBeenCalled();
        });
    });

    describe('applyServerFilters', () => {
        let serverFilters;

        beforeEach(() => {
            serverFilters = {
                foo: 'foo',
                bar: 'bar',
                baz: 'baz',
            };
            eventsMapLayer.serverFilters = serverFilters;
            eventsMapLayer.loading = 1;
            jest.spyOn(eventsMapLayer, '_mergeEventsIntoDataStore').mockImplementation(() => {});
            jest.spyOn(eventsMapLayer, '_setEventTypesAvailableForMapFilters').mockImplementation(() => {});
            jest.spyOn(eventsMapLayer, '_updateEventsForRecommendedEventsList').mockImplementation(() => {});
        });

        it('should _loadEvents with serverFilters', () => {
            jest.spyOn(eventsMapLayer, '_loadEvents').mockResolvedValue();
            eventsMapLayer.applyServerFilters();
            expect(eventsMapLayer._loadEvents).toHaveBeenCalledWith(serverFilters);
        });

        describe('when _loadEvents call is rejected', () => {
            it('should catch and not decrement loading flag if rejected value is false and not _mergeEventsIntoDataStore, _setEventTypesAvailableForMapFilters, or _updateEventsForRecommendedEventsList even if activated', () => {
                eventsMapLayer.activated = true;
                jest.spyOn(eventsMapLayer, '_loadEvents').mockRejectedValue(false);
                eventsMapLayer.applyServerFilters().then(() => {
                    expect(eventsMapLayer._loadEvents).toHaveBeenCalled();
                    expect(eventsMapLayer.loading).toEqual(1);
                    expect(eventsMapLayer._mergeEventsIntoDataStore).not.toHaveBeenCalled();
                    expect(eventsMapLayer._setEventTypesAvailableForMapFilters).not.toHaveBeenCalled();
                    expect(eventsMapLayer._updateEventsForRecommendedEventsList).not.toHaveBeenCalled();
                });
            });

            it('should catch and decrement loading flag if there is a rejected value and not _mergeEventsIntoDataStore, _setEventTypesAvailableForMapFilters, or _updateEventsForRecommendedEventsList even if activated', () => {
                eventsMapLayer.activated = true;
                jest.spyOn(eventsMapLayer, '_loadEvents').mockRejectedValue(new Error('some error'));
                eventsMapLayer.applyServerFilters().then(() => {
                    expect(eventsMapLayer._loadEvents).toHaveBeenCalled();
                    expect(eventsMapLayer.loading).toEqual(0);
                    expect(eventsMapLayer._mergeEventsIntoDataStore).not.toHaveBeenCalled();
                    expect(eventsMapLayer._setEventTypesAvailableForMapFilters).not.toHaveBeenCalled();
                    expect(eventsMapLayer._updateEventsForRecommendedEventsList).not.toHaveBeenCalled();
                });
            });
        });

        describe('when _loadEvents call is resolved', () => {
            let responseResult;

            beforeEach(() => {
                responseResult = [
                    {
                        foo: 'foo',
                    },
                    {
                        bar: 'bar',
                    },
                ];
                const response = {
                    result: responseResult,
                };
                jest.spyOn(eventsMapLayer, '_loadEvents').mockResolvedValue(response);
            });

            it('should not decrement loading flag and not _mergeEventsIntoDataStore, _setEventTypesAvailableForMapFilters, and _updateEventsForRecommendedEventsList if !activated', () => {
                eventsMapLayer.activated = false;
                eventsMapLayer.applyServerFilters().then(() => {
                    expect(eventsMapLayer._loadEvents).toHaveBeenCalled();
                    expect(eventsMapLayer._mergeEventsIntoDataStore).not.toHaveBeenCalled();
                    expect(eventsMapLayer._setEventTypesAvailableForMapFilters).not.toHaveBeenCalled();
                    expect(eventsMapLayer._updateEventsForRecommendedEventsList).not.toHaveBeenCalled();
                    expect(eventsMapLayer.loading).toEqual(1);
                });
            });

            it('should catch and decrement loading flag and _mergeEventsIntoDataStore, _setEventTypesAvailableForMapFilters, and _updateEventsForRecommendedEventsList if activated', () => {
                eventsMapLayer.activated = true;
                eventsMapLayer.applyServerFilters().then(() => {
                    expect(eventsMapLayer._loadEvents).toHaveBeenCalled();
                    expect(eventsMapLayer._mergeEventsIntoDataStore).toHaveBeenCalledWith(responseResult);
                    expect(eventsMapLayer._setEventTypesAvailableForMapFilters).toHaveBeenCalled();
                    expect(eventsMapLayer._updateEventsForRecommendedEventsList).toHaveBeenCalledWith(responseResult);
                    expect(eventsMapLayer.loading).toEqual(0);
                });
            });
        });
    });

    describe('_loadEvents', () => {
        let filters;

        beforeEach(() => {
            filters = {
                foo: 'bar',
            };
        });

        describe('when there is no map', () => {
            beforeEach(() => {
                eventsMapLayer.map = undefined;
            });

            it('should return a rejected promise with the value false', () => {
                expect(eventsMapLayer.loading).toEqual(0);
                const promise = eventsMapLayer._loadEvents();
                expect(promise.$$state.status).toEqual(2); // rejected
                expect(promise.$$state.value).toEqual(false);
                expect(eventsMapLayer.loading).toEqual(0); // shouldn't have changed
            });
        });

        describe('when there is a map', () => {
            beforeEach(() => {
                eventsMapLayer.map = {}; // just needs to be truthy
            });

            it('should increase loading flag', () => {
                StudentNetworkEvent.expect('index');
                expect(eventsMapLayer.loading).toEqual(0);
                eventsMapLayer._loadEvents();
                expect(eventsMapLayer.loading).toEqual(1);
            });

            it('should index student network events', () => {
                StudentNetworkEvent.expect('index').toBeCalledWith({
                    filters,
                });
                eventsMapLayer._loadEvents(filters);
                StudentNetworkEvent.flush('index');
            });
        });
    });

    describe('_mergeEventsIntoDataStore', () => {
        it("should set events on eventsMapLayer to the passed in events if the events on the eventsMapLayer haven't been set yet", () => {
            eventsMapLayer.events = undefined;
            const events = [
                {
                    id: 1,
                },
                {
                    id: 2,
                },
            ];
            eventsMapLayer._mergeEventsIntoDataStore(events);
            expect(eventsMapLayer.events).toBe(events);
        });

        it('should add passed in events into events data store on eventsMapLayer, removing any duplicates', () => {
            eventsMapLayer.events = [
                {
                    id: 1,
                },
                {
                    id: 3,
                },
            ];
            const events = [
                {
                    id: 1,
                },
                {
                    id: 2,
                },
            ];
            eventsMapLayer._mergeEventsIntoDataStore(events);
            expect(eventsMapLayer.events).toEqual([
                {
                    id: 1,
                },
                {
                    id: 3,
                },
                {
                    id: 2,
                },
            ]);
        });
    });

    describe('_addEventMarkersToMap', () => {
        it('should work when no events are passed in', () => {
            eventsMapLayer.map = {}; // stub this to be truthy
            jest.spyOn(eventsMapLayer, '_filterForMappableEvents').mockReturnValue([]);
            jest.spyOn(eventsMapLayer, '_filterForCurrentAndUpcomingEvents').mockReturnValue([]);
            expect(eventsMapLayer.eventFeatures).toBeUndefined();
            eventsMapLayer._addEventMarkersToMap();
            expect(eventsMapLayer._filterForMappableEvents).toHaveBeenCalledWith([]);
            expect(eventsMapLayer._filterForCurrentAndUpcomingEvents).toHaveBeenCalledWith([]);
            expect(eventsMapLayer.eventFeatures).toEqual([]);
        });

        it('should set eventFeatures from mappable events, ensuring uid property is set on each event feature', () => {
            eventsMapLayer.map = {}; // stub this to be truthy
            const mappableEvent = _.first(studentNetworkEvents);
            jest.spyOn(eventsMapLayer, '_filterForMappableEvents').mockReturnValue([mappableEvent]);
            jest.spyOn(eventsMapLayer, '_filterForCurrentAndUpcomingEvents').mockReturnValue([mappableEvent]);

            eventsMapLayer._addEventMarkersToMap(studentNetworkEvents);

            expect(eventsMapLayer._filterForMappableEvents).toHaveBeenCalledWith(studentNetworkEvents);
            expect(eventsMapLayer._filterForCurrentAndUpcomingEvents).toHaveBeenCalledWith([mappableEvent]);
            expect(_.pluck(eventsMapLayer.eventFeatures, 'id')).toEqual([mappableEvent.id]);
            expect(_.pluck(eventsMapLayer.eventFeatures, 'uid')).toEqual([mappableEvent.id]);
        });
    });

    describe('gesture handling', () => {
        describe('_disableMapGestureHandling', () => {
            it('should call map.setOptions with appropriate arguments', () => {
                eventsMapLayer._disableMapGestureHandling();
                expect(eventsMapLayer.map.setOptions).toHaveBeenCalledWith({
                    gestureHandling: 'none',
                });
            });
        });

        describe('_enableMapGestureHandling', () => {
            it('should call map.setOptions with appropriate arguments', () => {
                eventsMapLayer._enableMapGestureHandling();
                expect(eventsMapLayer.map.setOptions).toHaveBeenCalledWith({
                    gestureHandling: 'auto',
                });
            });
        });
    });

    describe('_updateAndApplyMapFilters', () => {
        it('should _updateMapFilters, _updateSelectedMapFiltersSummary, and then _applyMapFilters', () => {
            jest.spyOn(eventsMapLayer, '_updateMapFilters').mockImplementation(() => {});
            jest.spyOn(eventsMapLayer, '_updateSelectedMapFiltersSummary').mockImplementation(() => {});
            jest.spyOn(eventsMapLayer, '_applyMapFilters').mockImplementation(() => {});
            const filters = {
                foo: 'foo',
            };
            eventsMapLayer._updateAndApplyMapFilters(filters);
            expect(eventsMapLayer._updateMapFilters).toHaveBeenCalledWith(filters);
            expect(eventsMapLayer._updateSelectedMapFiltersSummary).toHaveBeenCalled();
            expect(eventsMapLayer._applyMapFilters).toHaveBeenCalled();
        });
    });

    describe('_updateMapFilters', () => {
        it('should update the mapFilters with the passed in filters', () => {
            const newFilters = {
                eventTypes: ['meetup', 'conference', 'admissions_event', 'career_fair'],
            };
            eventsMapLayer.mapFilters = {
                eventTypes: ['meetup', 'conference'],
            };
            eventsMapLayer._updateMapFilters(newFilters);
            expect(eventsMapLayer.mapFilters).toEqual({
                eventTypes: ['meetup', 'conference', 'admissions_event', 'career_fair'],
            });
        });
    });

    describe('_updateSelectedMapFiltersSummary', () => {
        it("should set the selectedMapFiltersSummary to 'Showing all events' HTML string if no eventTypes in mapFilters", () => {
            const showingAllEventsHTML = '<span>Showing all events</span>';
            eventsMapLayer.mapFilters = {
                eventTypes: ['meetup'],
            };
            eventsMapLayer._updateSelectedMapFiltersSummary();
            expect(eventsMapLayer.selectedMapFiltersSummary).not.toEqual(showingAllEventsHTML);

            eventsMapLayer.mapFilters = {
                eventTypes: [],
            };
            eventsMapLayer._updateSelectedMapFiltersSummary();
            expect(eventsMapLayer.selectedMapFiltersSummary).toEqual(showingAllEventsHTML);
        });

        it('should set the selectedMapFiltersSummary based on the eventTypes in the mapFilters', () => {
            const expectedHTML = '<span>Meetup</span> <span>Career Fair</span>';
            eventsMapLayer.mapFilters = {
                eventTypes: ['meetup', 'career_fair'],
            };
            eventsMapLayer._updateSelectedMapFiltersSummary();
            expect(eventsMapLayer.selectedMapFiltersSummary).toEqual(expectedHTML);
        });
    });

    describe('_applyMapFilters', () => {
        it('should _filterEventsByEventType and then _addEventMarkersToMap', () => {
            const mockEvents = [{}, {}];
            jest.spyOn(eventsMapLayer, '_filterEventsByEventType').mockReturnValue(mockEvents);
            jest.spyOn(eventsMapLayer, '_addEventMarkersToMap').mockImplementation(() => {});
            eventsMapLayer._applyMapFilters();
            expect(eventsMapLayer._filterEventsByEventType).toHaveBeenCalledWith(
                eventsMapLayer.events,
                eventsMapLayer.mapFilters,
            );
            expect(eventsMapLayer._addEventMarkersToMap).toHaveBeenCalledWith(mockEvents);
        });
    });

    describe('_filterEventsByEventType', () => {
        it('should return the passed in events if no passed in filters', () => {
            const events = eventsMapLayer._filterEventsByEventType(studentNetworkEvents);
            expect(events).toBe(studentNetworkEvents);
        });

        it('should return the passed in events if no eventTypes on passed in filters', () => {
            const filters = {};
            const events = eventsMapLayer._filterEventsByEventType(studentNetworkEvents, filters);
            expect(events).toBe(studentNetworkEvents);
        });

        it('should return passed in events filtered by event_type', () => {
            const filters = {
                eventTypes: ['meetup'],
            };
            const events = eventsMapLayer._filterEventsByEventType(studentNetworkEvents, filters);
            expect(events).toEqual(_.filter(studentNetworkEvents, event => event.event_type === 'meetup'));
        });
    });

    describe('_filterForMappableEvents', () => {
        it('should return the mappable events from the passed in events', () => {
            const mockEvents = [
                {
                    id: '1',
                    mappable: true,
                },
                {
                    id: '2',
                    mappable: false,
                },
                {
                    id: '3',
                    mappable: true,
                },
            ];
            const events = eventsMapLayer._filterForMappableEvents(mockEvents);
            expect(_.pluck(events, 'id')).toEqual(['1', '3']);
        });
    });

    describe('_filterForCurrentAndUpcomingEvents', () => {
        it('should filter for current and upcoming events (i.e. events with end_time values in the future)', () => {
            const now = Date.now();
            jest.spyOn(Date, 'now').mockReturnValue(now);
            // the server returns end_time values in seconds, not milliseconds, so we have to divide by 1000 here
            const events = [
                {
                    id: 1,
                    end_time: Math.floor(now / 1000) + 3, // in the future
                },
                {
                    id: 2,
                    end_time: Math.floor(now / 1000) + 2, // in the future
                },
                {
                    id: 3,
                    end_time: Math.floor(now / 1000) + 1, // in the future
                },
                {
                    id: 4,
                    end_time: Math.floor(now / 1000), // just ended now
                },
                {
                    id: 5,
                    end_time: Math.floor(now / 1000) - 1, // in the past
                },
            ];
            const filteredEvents = eventsMapLayer._filterForCurrentAndUpcomingEvents(events);
            expect(_.pluck(filteredEvents, 'id')).toEqual([1, 2, 3]);
        });
    });

    describe('_filterForPastEvents', () => {
        it('should filter for current and upcoming events (i.e. events with end_time values in the future)', () => {
            const now = Date.now();
            jest.spyOn(Date, 'now').mockReturnValue(now);
            // the server returns end_time values in seconds, not milliseconds, so we have to divide by 1000 here
            const events = [
                {
                    id: 1,
                    end_time: Math.floor(now / 1000) + 3, // in the future
                },
                {
                    id: 2,
                    end_time: Math.floor(now / 1000) + 2, // in the future
                },
                {
                    id: 3,
                    end_time: Math.floor(now / 1000) + 1, // in the future
                },
                {
                    id: 4,
                    end_time: Math.floor(now / 1000), // just ended now
                },
                {
                    id: 5,
                    end_time: Math.floor(now / 1000) - 1, // in the past
                },
            ];
            const filteredEvents = eventsMapLayer._filterForPastEvents(events);
            expect(_.pluck(filteredEvents, 'id')).toEqual([4, 5]);
        });
    });

    describe('_setEventTypesAvailableForMapFilters', () => {
        it('should work if there are no events', () => {
            eventsMapLayer.events = undefined;
            expect(eventsMapLayer.$$eventTypesAvailableForMapFilters).toBeUndefined();
            eventsMapLayer._setEventTypesAvailableForMapFilters();

            // see `eventTypeFiltersOrder` config setting in student_network_event.js for more info on expected ordering
            const expectedEventTypes = ['meetup', 'conference', 'company_visit', 'special_event'];
            expect(eventsMapLayer.$$eventTypesAvailableForMapFilters).toEqual(expectedEventTypes);
        });

        it('should set $$eventTypesAvailableForMapFilters to an ordered array that includes certain mappable event types', () => {
            eventsMapLayer.events = [];
            expect(eventsMapLayer.$$eventTypesAvailableForMapFilters).toBeUndefined();
            eventsMapLayer._setEventTypesAvailableForMapFilters();

            // see `eventTypeFiltersOrder` config setting in student_network_event.js for more info on expected ordering
            const expectedEventTypes = ['meetup', 'conference', 'company_visit', 'special_event'];
            expect(eventsMapLayer.$$eventTypesAvailableForMapFilters).toEqual(expectedEventTypes);
            assertEventTypesAreMappable(expectedEventTypes);
        });

        it('should only include certain mappable event types if there are any events with that event type', () => {
            eventsMapLayer.events = [
                StudentNetworkEvent.new({
                    event_type: 'admissions_event',
                }),
            ];
            expect(eventsMapLayer.$$eventTypesAvailableForMapFilters).toBeUndefined();
            eventsMapLayer._setEventTypesAvailableForMapFilters();

            let expectedEventTypes = ['meetup', 'conference', 'company_visit', 'special_event', 'admissions_event'];
            expect(eventsMapLayer.$$eventTypesAvailableForMapFilters).toEqual(expectedEventTypes);
            assertEventTypesAreMappable(expectedEventTypes);

            eventsMapLayer.events = [
                StudentNetworkEvent.new({
                    event_type: 'career_fair',
                }),
            ];
            eventsMapLayer._setEventTypesAvailableForMapFilters();

            expectedEventTypes = ['meetup', 'conference', 'company_visit', 'special_event', 'career_fair'];
            expect(eventsMapLayer.$$eventTypesAvailableForMapFilters).toEqual(expectedEventTypes);
            assertEventTypesAreMappable(expectedEventTypes);

            eventsMapLayer.events = [
                StudentNetworkEvent.new({
                    event_type: 'career_fair',
                }),
                StudentNetworkEvent.new({
                    event_type: 'admissions_event',
                }),
                StudentNetworkEvent.new({
                    event_type: 'book_club', // should not be included since it's not a mappable event type (see student_network_event.js)
                }),
            ];
            eventsMapLayer._setEventTypesAvailableForMapFilters();

            expectedEventTypes = [
                'meetup',
                'conference',
                'company_visit',
                'special_event',
                'admissions_event',
                'career_fair',
            ];
            expect(eventsMapLayer.$$eventTypesAvailableForMapFilters).toEqual(expectedEventTypes);
            assertEventTypesAreMappable(expectedEventTypes);
        });

        const assertEventTypesAreMappable = eventTypes => {
            _.each(eventTypes, eventType => {
                expect(StudentNetworkEvent.EVENT_TYPE_CONFIGS_MAP[eventType].mappable).toBe(true);
            });
        };
    });

    describe('_filterForRecommendedEvents', () => {
        it('should return the recommended events from the this.events', () => {
            const mockEvents = [
                {
                    id: '1',
                    recommended: true,
                },
                {
                    id: '2',
                    recommended: true,
                },
                {
                    id: '3',
                    recommended: false,
                },
            ];
            const recommendedEvents = eventsMapLayer._filterForRecommendedEvents(mockEvents);
            expect(_.pluck(recommendedEvents, 'id')).toEqual(['1', '2']);
        });
    });

    describe('_updateEventsForRecommendedEventsList', () => {
        describe("when currentRecommendedEventsListTab is 'upcoming'", () => {
            beforeEach(() => {
                jest.spyOn(eventsMapLayer, 'currentRecommendedEventsListTab', 'get').mockReturnValue('upcoming');
            });

            describe('when advancedSearchOpen', () => {
                beforeEach(() => {
                    eventsMapLayer.advancedSearchOpen = true;
                });

                it('should set eventsForRecommendedEventsList to return value of _filterForCurrentAndUpcomingEvents and should not _filterForPastEvents or _filterForRecommendedEvents', () => {
                    const mockEvents = [
                        {
                            id: 1,
                        },
                        {
                            id: 2,
                        },
                    ];
                    jest.spyOn(eventsMapLayer, '_filterForCurrentAndUpcomingEvents').mockReturnValue([mockEvents[0]]);
                    jest.spyOn(eventsMapLayer, '_filterForRecommendedEvents');
                    jest.spyOn(eventsMapLayer, '_filterForPastEvents');
                    eventsMapLayer._updateEventsForRecommendedEventsList(mockEvents);
                    expect(eventsMapLayer.eventsForRecommendedEventsList).toEqual([mockEvents[0]]);
                    expect(eventsMapLayer._filterForCurrentAndUpcomingEvents).toHaveBeenCalledWith(mockEvents);
                    expect(eventsMapLayer._filterForRecommendedEvents).not.toHaveBeenCalled();
                    expect(eventsMapLayer._filterForPastEvents).not.toHaveBeenCalled();
                });
            });

            describe('when !advancedSearchOpen', () => {
                beforeEach(() => {
                    eventsMapLayer.advancedSearchOpen = false;
                });

                it('should _filterForCurrentAndUpcomingEvents and _filterForRecommendedEvents and set eventsForRecommendedEventsList to that value and should not _filterForPastEvents', () => {
                    const mockEvents = [
                        {
                            id: 1,
                        },
                        {
                            id: 2,
                        },
                    ];
                    jest.spyOn(eventsMapLayer, '_filterForCurrentAndUpcomingEvents').mockReturnValue(mockEvents);
                    jest.spyOn(eventsMapLayer, '_filterForRecommendedEvents').mockReturnValue([mockEvents[1]]);
                    jest.spyOn(eventsMapLayer, '_filterForPastEvents');
                    eventsMapLayer._updateEventsForRecommendedEventsList(mockEvents);
                    expect(eventsMapLayer.eventsForRecommendedEventsList).toEqual([mockEvents[1]]);
                    expect(eventsMapLayer._filterForCurrentAndUpcomingEvents).toHaveBeenCalledWith(mockEvents);
                    expect(eventsMapLayer._filterForRecommendedEvents).toHaveBeenCalledWith(mockEvents);
                    expect(eventsMapLayer._filterForPastEvents).not.toHaveBeenCalled();
                });
            });
        });

        describe("when currentRecommendedEventsListTab is not 'upcoming'", () => {
            beforeEach(() => {
                jest.spyOn(eventsMapLayer, 'currentRecommendedEventsListTab', 'get').mockReturnValue('past_events');
            });

            describe('when advancedSearchOpen', () => {
                beforeEach(() => {
                    eventsMapLayer.advancedSearchOpen = true;
                });

                it('should set eventsForRecommendedEventsList to return value of _filterForPastEvents and should not _filterForRecommendedEvents or _filterForCurrentAndUpcomingEvents', () => {
                    const mockEvents = [
                        {
                            id: 1,
                        },
                        {
                            id: 2,
                        },
                    ];
                    jest.spyOn(eventsMapLayer, '_filterForPastEvents').mockReturnValue([mockEvents[0]]);
                    jest.spyOn(eventsMapLayer, '_filterForRecommendedEvents');
                    jest.spyOn(eventsMapLayer, '_filterForCurrentAndUpcomingEvents');
                    eventsMapLayer._updateEventsForRecommendedEventsList(mockEvents);
                    expect(eventsMapLayer.eventsForRecommendedEventsList).toEqual([mockEvents[0]]);
                    expect(eventsMapLayer._filterForPastEvents).toHaveBeenCalledWith(mockEvents);
                    expect(eventsMapLayer._filterForRecommendedEvents).not.toHaveBeenCalled();
                    expect(eventsMapLayer._filterForCurrentAndUpcomingEvents).not.toHaveBeenCalled();
                });
            });

            describe('when !advancedSearchOpen', () => {
                beforeEach(() => {
                    eventsMapLayer.advancedSearchOpen = false;
                });

                it('should _filterForPastEvents and _filterForRecommendedEvents and set eventsForRecommendedEventsList to that value and should not _filterForCurrentAndUpcomingEvents', () => {
                    const mockEvents = [
                        {
                            id: 1,
                        },
                        {
                            id: 2,
                        },
                    ];
                    jest.spyOn(eventsMapLayer, '_filterForPastEvents').mockReturnValue(mockEvents);
                    jest.spyOn(eventsMapLayer, '_filterForRecommendedEvents').mockReturnValue([mockEvents[1]]);
                    jest.spyOn(eventsMapLayer, '_filterForCurrentAndUpcomingEvents');
                    eventsMapLayer._updateEventsForRecommendedEventsList(mockEvents);
                    expect(eventsMapLayer.eventsForRecommendedEventsList).toEqual([mockEvents[1]]);
                    expect(eventsMapLayer._filterForPastEvents).toHaveBeenCalledWith(mockEvents);
                    expect(eventsMapLayer._filterForRecommendedEvents).toHaveBeenCalledWith(mockEvents);
                    expect(eventsMapLayer._filterForCurrentAndUpcomingEvents).not.toHaveBeenCalled();
                });
            });
        });
    });
});
