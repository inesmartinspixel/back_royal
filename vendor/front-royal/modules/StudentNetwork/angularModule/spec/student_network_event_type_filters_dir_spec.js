import 'AngularSpecHelper';
import 'StudentNetwork/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import studentNetworkEventTypeFiltersLocales from 'StudentNetwork/locales/student_network/student_network_event_type_filters-en.json';
import networkMapFilterBoxLocales from 'StudentNetwork/locales/student_network/network_map_filter_box-en.json';
import studentNetworkEventLocales from 'StudentNetwork/locales/student_network/student_network_event-en.json';

setSpecLocales(studentNetworkEventTypeFiltersLocales, networkMapFilterBoxLocales, studentNetworkEventLocales);

describe('FrontRoyal.StudentNetwork.StudentNetworkEventTypeFilters', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let eventsMapLayer;
    let studentNetworkMapViewModel;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.StudentNetwork', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
            },
        ]);

        studentNetworkMapViewModel = SpecHelper.stubStudentNetworkMapViewModel();
        eventsMapLayer = studentNetworkMapViewModel.eventsMapLayer;

        jest.spyOn(eventsMapLayer, 'applyEventTypeFilters');
        jest.spyOn(eventsMapLayer, 'refresh').mockImplementation();
        jest.spyOn(studentNetworkMapViewModel.studentsMapLayer, 'refresh').mockImplementation();

        jest.spyOn($injector.get('TranslationHelper').prototype, 'get').mockImplementation(key => `localized ${key}`);
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.eventsMapLayer = eventsMapLayer;
        renderer.render(
            '<student-network-event-type-filters events-map-layer="eventsMapLayer"></student-network-event-type-filters>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    it('should apply selected CSS class to .event-type-filter element when eventTypeSelected', () => {
        render();
        jest.spyOn(scope, 'eventTypeSelected').mockReturnValue(true);
        scope.$digest();
        SpecHelper.expectElements(
            elem,
            '.event-type-filter.selected',
            eventsMapLayer.eventTypesAvailableForMapFilters.length,
        );

        jest.spyOn(scope, 'eventTypeSelected').mockReturnValue(false);
        scope.$digest();
        SpecHelper.expectElements(elem, '.event-type-filter.selected', 0);
    });

    it('should call onFilterButtonClick with the appropriate event type when an .event-type-filter element is clicked', () => {
        render();
        jest.spyOn(scope, 'onFilterButtonClick').mockImplementation(() => {});
        SpecHelper.click(elem, '.event-type-filter:eq(0)');
        expect(scope.onFilterButtonClick).toHaveBeenCalledWith(
            _.first(eventsMapLayer.eventTypesAvailableForMapFilters),
        );
    });

    it('should call eventsMapLayer.applyEventFilters with the appliedFiltersProxy when apply-filters button is clicked', () => {
        render();
        scope.appliedFiltersProxy = ['foo', 'bar', 'baz'];
        scope.$digest();
        expect(eventsMapLayer.applyEventTypeFilters).not.toHaveBeenCalled();
        SpecHelper.click(elem, 'button[name="apply-filters"]');
        expect(eventsMapLayer.applyEventTypeFilters).toHaveBeenCalled();
        expect(eventsMapLayer.applyEventTypeFilters).toHaveBeenCalledWith(['foo', 'bar', 'baz']);
    });

    describe('onFilterButtonClick', () => {
        it("should add the event type to the appliedFiltersProxy (not the appliedFilters) if the event type isn't already in the appliedFiltersProxy", () => {
            render();
            const eventType = _.first(eventsMapLayer.eventTypesAvailableForMapFilters);
            expect(eventsMapLayer.mapFilters.eventTypes).toBeUndefined();
            expect(scope.appliedFiltersProxy).toEqual([]);
            scope.onFilterButtonClick(eventType);
            expect(eventsMapLayer.mapFilters.eventTypes).toBeUndefined();
            expect(scope.appliedFiltersProxy).toEqual([eventType]);
        });

        it('should remove the event type from the appliedFiltersProxy (not the appliedFilters) if the event type is already in the appliedFiltersProxy', () => {
            const eventType = _.first(eventsMapLayer.eventTypesAvailableForMapFilters);
            eventsMapLayer.applyEventTypeFilters([eventType]);

            render();

            expect(eventsMapLayer.mapFilters.eventTypes).toEqual([eventType]);
            expect(scope.appliedFiltersProxy).toEqual([eventType]);

            scope.onFilterButtonClick(eventType);
            expect(eventsMapLayer.mapFilters.eventTypes).toEqual([eventType]);
            expect(scope.appliedFiltersProxy).toEqual([]);
        });
    });
});
