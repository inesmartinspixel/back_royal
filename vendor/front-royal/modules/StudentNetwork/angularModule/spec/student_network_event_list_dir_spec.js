import 'AngularSpecHelper';
import 'StudentNetwork/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import studentNetworkEventListLocales from 'StudentNetwork/locales/student_network/student_network_event_list-en.json';
import studentNetworkEventLocales from 'StudentNetwork/locales/student_network/student_network_event-en.json';
import studentNetworkEventListItemLocales from 'StudentNetwork/locales/student_network/student_network_event_list_item-en.json';
import moment from 'moment-timezone';

setSpecLocales(studentNetworkEventListLocales, studentNetworkEventLocales, studentNetworkEventListItemLocales);

describe('FrontRoyal.StudentNetwork.studentNetworkEventList', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let StudentNetworkEvent;
    let events;
    let eventsMapLayer;
    beforeEach(() => {
        angular.mock.module('FrontRoyal.StudentNetwork', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                StudentNetworkEvent = $injector.get('StudentNetworkEvent');
            },
        ]);

        SpecHelper.stubCurrentUser();

        events = [
            StudentNetworkEvent.new({
                id: '1',
                event_type: 'meetup',
                start_time: moment().toDate().getTime() / 1000,
                end_time: moment().add(1, 'hour').toDate().getTime() / 1000,
                timezone: 'America/New_York',
            }),
            StudentNetworkEvent.new({
                id: '2',
                event_type: 'meetup',
                start_time: moment().toDate().getTime() / 1000,
                end_time: moment().add(1, 'hour').toDate().getTime() / 1000,
                timezone: 'America/New_York',
            }),
            StudentNetworkEvent.new({
                id: '3',
                event_type: 'meetup',
                start_time: moment().add(1, 'day').toDate().getTime() / 1000,
                end_time: moment().add(2, 'day').toDate().getTime() / 1000,
                timezone: 'America/New_York',
            }),
            StudentNetworkEvent.new({
                id: '4',
                event_type: 'meetup',
                start_time: moment().add(1, 'day').toDate().getTime() / 1000,
                end_time: moment().add(2, 'day').toDate().getTime() / 1000,
                timezone: 'America/New_York',
            }),
            StudentNetworkEvent.new({
                id: '5',
                event_type: 'meetup',
                start_time: moment('2100-01-01 12:00:00').toDate().getTime() / 1000,
                end_time: moment('2100-01-01 13:00:00').toDate().getTime() / 1000,
                timezone: 'America/New_York',
            }),
            StudentNetworkEvent.new({
                id: '6',
                event_type: 'meetup',
                date_tbd: true,
                date_tbd_desription: 'This date has yet to be determined',
                timezone: 'America/New_York',
            }),
        ];

        eventsMapLayer = {};
    });

    function render(opts = {}) {
        renderer = SpecHelper.renderer();
        renderer.scope.events = events;
        renderer.scope.eventsMapLayer = eventsMapLayer;
        renderer.scope.reverseOrder = opts.reverseOrder || false;
        renderer.render(
            '<student-network-event-list events="events" events-map-layer="eventsMapLayer" reverse-order="reverseOrder"></student-network-event-list>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    describe('event feature indexing and grouping', () => {
        it('should index and group event features', () => {
            render();
            expect(_.pluck(scope.indexedAndGroupedEvents.today, 'id')).toEqual(['1', '2']);
            expect(_.pluck(scope.indexedAndGroupedEvents.tomorrow, 'id')).toEqual(['3', '4']);
            expect(_.pluck(scope.indexedAndGroupedEvents['Friday, January 1, 2100'], 'id')).toEqual(['5']);
        });

        it('should use grouped formattedRelativeStartDayOrDateTimes for scope.eventDates', () => {
            render();
            expect(scope.eventDates).toEqual(['today', 'tomorrow', 'Friday, January 1, 2100', 'Date TBD']);
        });

        it('should support reverseOrder', () => {
            render({
                reverseOrder: true,
            });
            expect(scope.eventDates).toEqual(['Date TBD', 'Friday, January 1, 2100', 'tomorrow', 'today']);
        });
    });

    it('should render a date-separator for each unique eventDate', () => {
        render();
        SpecHelper.expectElements(elem, '.date-separator', 4);
        SpecHelper.expectElementText(elem, '.date-separator:eq(0)', 'today');
        SpecHelper.expectElementText(elem, '.date-separator:eq(1)', 'tomorrow');
        SpecHelper.expectElementText(elem, '.date-separator:eq(2)', 'Friday, January 1, 2100');
        SpecHelper.expectElementText(elem, '.date-separator:eq(3)', 'Date TBD');
    });

    it('should render an student-network-event-list-item for each event in indexedAndGroupedEvents', () => {
        render();
        SpecHelper.expectElements(elem, 'student-network-event-list-item', 6);
    });

    it('should show a separate UI when no events', () => {
        render();
        SpecHelper.expectNoElement(elem, '.no-events-found-container i.fa-search');
        SpecHelper.expectNoElement(elem, '.no-events-found-container .sub-text');
        SpecHelper.expectElements(elem, 'student-network-event-list-item', 6);
        scope.events = [];
        scope.$digest();
        SpecHelper.expectNoElement(elem, 'student-network-event-list-item');
        SpecHelper.expectElement(elem, '.no-events-found-container i.fa-search');
        SpecHelper.expectElementText(elem, '.no-events-found-container .sub-text', 'No Events Found');
    });
});
