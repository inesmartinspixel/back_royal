import 'AngularSpecHelper';
import 'StudentNetwork/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import studentNetworkRecommendedEventListLocales from 'StudentNetwork/locales/student_network/student_network_recommended_event_list-en.json';
import studentNetworkEventListLocales from 'StudentNetwork/locales/student_network/student_network_event_list-en.json';
import networkMapFilterBoxLocales from 'StudentNetwork/locales/student_network/network_map_filter_box-en.json';
import moment from 'moment-timezone';

setSpecLocales(studentNetworkRecommendedEventListLocales, studentNetworkEventListLocales, networkMapFilterBoxLocales);

describe('FrontRoyal.StudentNetwork.studentNetworkRecommendedEventList', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let eventsMapLayer;
    let isMobile;
    let StudentNetworkEvent;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.StudentNetwork', 'SpecHelper');

        angular.mock.module($provide => {
            isMobile = jest.fn();
            isMobile.mockReturnValue(false);
            $provide.value('isMobile', isMobile);
        });

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                StudentNetworkEvent = $injector.get('StudentNetworkEvent');
            },
        ]);

        SpecHelper.stubCurrentUser();
        eventsMapLayer = {
            recommendedEvents: [
                StudentNetworkEvent.new({
                    event_type: 'meetup',
                    start_time: moment().toDate().getTime() / 1000,
                    end_time: moment().add(1, 'hour').toDate().getTime() / 1000,
                    timezone: 'America/New_York',
                }),
            ],
            mobileState: {
                showingRecommendedEventsList: true,
            },
        };
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.eventsMapLayer = eventsMapLayer;
        renderer.render(
            '<student-network-recommended-event-list events-map-layer="eventsMapLayer"></student-network-recommended-event-list>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.isMobile = false;
        scope.$apply();
    }

    it('should show a spinner if eventsMapLayer is loading', () => {
        render();
        SpecHelper.expectNoElement(elem, 'front-royal-spinner');
        scope.eventsMapLayer.loading = true;
        scope.$digest();
        SpecHelper.expectElement(elem, 'front-royal-spinner');
    });

    it('should render filters directive', () => {
        render();
        SpecHelper.expectElement(elem, 'student-network-recommended-event-list-filters');
    });

    it('should render return to map button if scope.isMobile and scope.eventsMapLayer.mobileState.showingRecommendedEventsList', () => {
        render();
        SpecHelper.expectNoElement(elem, 'student-network-return-to-map-button');

        eventsMapLayer.mobileState.showingRecommendedEventsList = true;
        scope.isMobile = true;
        scope.$digest();
        SpecHelper.expectElement(elem, 'student-network-return-to-map-button');
    });

    it('should render event list', () => {
        render();
        SpecHelper.expectElement(elem, 'student-network-event-list');
    });

    describe('returnToMapCallback', () => {
        it('should set eventsMapLayer.mobileState.showingRecommendedEventsList to false', () => {
            render();
            expect(scope.eventsMapLayer.mobileState.showingRecommendedEventsList).toBe(true);
            scope.returnToMapCallback();
            expect(scope.eventsMapLayer.mobileState.showingRecommendedEventsList).toBe(false);
        });
    });
});
