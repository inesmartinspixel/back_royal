import 'StudentNetwork/angularModule';

angular.module('FrontRoyal.StudentNetwork').factory('StudentNetworkEventFixtures', [
    '$injector',
    $injector => {
        const StudentNetworkEvent = $injector.get('StudentNetworkEvent');
        const guid = $injector.get('guid');

        StudentNetworkEvent.fixtures = {
            sampleAttrs(overrides = {}) {
                overrides.id = overrides.id || guid.generate();

                return angular.extend(
                    {
                        id: 'd7311d08-d25d-493c-bba1-4684aa55441c',
                        created_at: 1566504767,
                        updated_at: 1566920586,
                        start_time: 1571774400, // Tuesday, October 22, 2019 @ 4:00 PM EDT when multipled by 1000
                        end_time: 1571778000, // Tuesday, October 22, 2019 @ 5:00 PM EDT when multipled by 1000
                        timezone: 'America/New_York',
                        title: 'Harrisonburg Meetup',
                        description: 'A super cool meetup!',
                        event_type: 'meetup',
                        image_id: '4f34bec8-81ec-40ff-8b61-0cb184a8d772',
                        rsvp_status: 'required',
                        external_rsvp_url: 'https://rsvp.here',
                        place_id: 'ChIJ1Q_VWM6StIkRM_O-2dKr5EA',
                        place_details: {
                            formatted_address: 'Harrisonburg, VA, USA',
                            utc_offset: -240,
                            lat: 38.4498575,
                            lng: -78.8697159,
                            adr_address:
                                '<span class="street-address">41-A Court Square</span>, <span class="locality">Harrisonburg</span>, <span class="region">VA</span> <span class="postal-code">22801</span>, <span class="country-name">USA</span>',
                        },
                        place_id_anonymized: 'ChIJVXEHP8OStIkRX3u92rllTgg',
                        place_details_anonymized: {
                            formatted_address: 'Harrisonburg, VA, USA',
                            utc_offset: -300,
                            lat: 38.44956880000001,
                            lng: -78.8689155,
                            adr_address:
                                '<span class="locality">Harrisonburg</span>, <span class="region">VA</span>, <span class="country-name">USA</span>',
                        },
                        location_name: 'Capital Ale House',
                        location_anonymized: '0101000020E610000033DFC14F9CB753C0A8A66E788B394340',
                        recommended: false,
                        image: {
                            formats: {
                                original: {
                                    url:
                                        'https://uploads-development.smart.ly/images/b734e6cc63622874b867c459f19f80a97de61ebec007a447da1fd8dd8d9492af/original/b734e6cc63622874b867c459f19f80a97de61ebec007a447da1fd8dd8d9492af.jpg',
                                },
                            },
                        },
                    },
                    overrides,
                );
            },

            getInstance(overrides) {
                return StudentNetworkEvent.new(this.sampleAttrs(overrides));
            },
        };

        return {};
    },
]);
