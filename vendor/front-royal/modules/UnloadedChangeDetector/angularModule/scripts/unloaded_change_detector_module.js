import 'guid/angularModule';
import 'PrioritizedInterceptors/angularModule';
import 'super-model/dist/super_model';

export default angular.module('UnloadedChangeDetector', ['SuperModel', 'prioritizedInterceptors', 'guid']);
