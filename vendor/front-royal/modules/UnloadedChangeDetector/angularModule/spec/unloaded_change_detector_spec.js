import 'AngularSpecHelper';
import 'UnloadedChangeDetector/angularModule';

describe('UnloadedChangeDetector', () => {
    let SpecHelper;
    let MyUnloadedChangeDetector;
    let $injector;
    let myUnloadedChangeDetector;
    let callback;
    let UnloadedChangeDetectorInterceptor;

    beforeEach(() => {
        angular.mock.module('UnloadedChangeDetector', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                const UnloadedChangeDetector = $injector.get('UnloadedChangeDetector');
                UnloadedChangeDetectorInterceptor = $injector.get('UnloadedChangeDetectorInterceptor');
                MyUnloadedChangeDetector = UnloadedChangeDetector.subclass(() => ({
                    getLastUpdatedAtBeforeChanges(response) {
                        return response.data.meta.push_messages.path.to.last_updated_at_before_changes;
                    },

                    getLastUpdatedAt(response) {
                        return response.data.meta.push_messages.path.to.last_updated_at;
                    },
                }));

                callback = jest.fn();
                myUnloadedChangeDetector = new MyUnloadedChangeDetector().onChangeDetected(callback);
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
        if (myUnloadedChangeDetector) {
            myUnloadedChangeDetector.destroy();
        }
    });

    function response(obj) {
        const _response = {
            data: {
                meta: {
                    push_messages: {
                        path: {
                            to: obj,
                        },
                    },
                },
            },
        };

        UnloadedChangeDetectorInterceptor.response(_response);
    }

    it('should trigger when lastUpdatedAt is later than what we know about', () => {
        response({
            last_updated_at: 1,
        });
        expect(callback).not.toHaveBeenCalled();
        response({
            last_updated_at: 2,
        });
        expect(callback).toHaveBeenCalledWith(1);
    });

    it('should trigger when lastUpdatedAtBeforeChanges is later than what we know about', () => {
        response({
            last_updated_at: 1,
        });
        expect(callback).not.toHaveBeenCalled();
        response({
            last_updated_at_before_changes: 2,
            last_updated_at: 2,
        });
        expect(callback).toHaveBeenCalledWith(1);
    });

    it('should trigger if a null value came down previously', () => {
        response({
            last_updated_at: null,
        });
        expect(callback).not.toHaveBeenCalled();
        response({
            last_updated_at: 1,
        });
        expect(callback).toHaveBeenCalledWith(0);
    });

    it('should not trigger after being destroyed', () => {
        response({
            last_updated_at: 1,
        });
        expect(callback).not.toHaveBeenCalled();
        myUnloadedChangeDetector.destroy();
        response({
            last_updated_at: 2,
        });
        expect(callback).not.toHaveBeenCalled();
    });

    it('should not trigger when the value coming down is the same a what we already know about', () => {
        response({
            last_updated_at: 1,
        });
        expect(callback).not.toHaveBeenCalled();
        response({
            last_updated_at: 1,
        });
        expect(callback).not.toHaveBeenCalled();
    });

    it('should not trigger when lastUpdatedAt is later than we know about if lastUpdatedAtBeforeChanges is the same', () => {
        response({
            last_updated_at: 1,
        });
        expect(callback).not.toHaveBeenCalled();
        response({
            last_updated_at: 2,
            last_updated_at_before_changes: 1,
        });
        expect(callback).not.toHaveBeenCalled();

        // And now the local last_updated_at should be 2,
        // so this should not trigger
        callback.mockClear();
        response({
            last_updated_at: 2,
        });
        expect(callback).not.toHaveBeenCalled();
    });
});
