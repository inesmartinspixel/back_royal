angular.module('FrontRoyal.Mailbox').factory('MessageFixtures', [
    '$injector',
    $injector => {
        const Message = $injector.get('Mailbox.Message');
        const guid = $injector.get('guid');

        Message.fixtures = {
            sampleAttrs(overrides = {}, sender, recipient) {
                const senderId = sender ? sender.id : guid.generate();

                return angular.extend(
                    {
                        id: guid.generate(),
                        sender_id: senderId,
                        sender_type: 'Message from me to you',
                        body: 'hey yo!',
                        updated_at: Date.now(),
                        created_at: Date.now(),
                        receipts: [
                            {
                                receiver_id: senderId,
                                id: guid.generate(),
                                is_read: true,
                                trashed: false,
                                deleted: false,
                                mailbox_type: 'inbox',
                            },
                            {
                                receiver_id: recipient ? recipient.id : guid.generate(),
                                id: guid.generate(),
                                is_read: overrides.is_read === false ? overrides.is_read : true,
                                trashed: false,
                                deleted: false,
                                mailbox_type: 'inbox',
                            },
                        ],
                    },
                    overrides,
                );
            },

            getInstance(overrides, sender, recipient) {
                return Message.new(this.sampleAttrs(overrides, sender, recipient));
            },
        };

        return {};
    },
]);
