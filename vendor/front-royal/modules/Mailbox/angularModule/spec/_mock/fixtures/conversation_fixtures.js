angular.module('FrontRoyal.Mailbox').factory('ConversationFixtures', [
    '$injector',
    $injector => {
        const Conversation = $injector.get('Mailbox.Conversation');
        const guid = $injector.get('guid');

        Conversation.fixtures = {
            sampleAttrs(overrides = {}, user, otherUser) {
                let userId;
                let userName;
                let avatarUrl;
                if (user) {
                    userId = user.id;
                    userName = user.email;
                    avatarUrl = user.avatar_url;
                } else {
                    userId = guid.generate();
                    userName = 'user@pedago.com';
                }

                if (!avatarUrl) {
                    avatarUrl = 'http://path/to/avatar';
                }

                otherUser = otherUser || {};
                if (!otherUser.id) {
                    otherUser.id = guid.generate();
                }
                if (!otherUser.display_name) {
                    otherUser.display_name = `Connection ${otherUser.id}`;
                }
                return angular.extend(
                    {
                        id: guid.generate(),
                        created_at: 1470725524,
                        updated_at: 1470725524,
                        subject: 'Placeholder: conversation subject. Necessary?',
                        recipients: [
                            {
                                type: 'user',
                                id: userId,
                                display_name: userName,
                                avatar_url: avatarUrl,
                            },
                            {
                                type: 'user',
                                id: otherUser.id,
                                display_name: otherUser.display_name,
                                avatar_url: avatarUrl,
                            },
                        ],
                        messages: [
                            {
                                id: guid.generate(),
                                sender_id: userId,
                                sender_type: 'Message from me to you',
                                body: 'hey yo!',
                                updated_at: 1470725524,
                                created_at: 1470725524,
                                receipts: [
                                    {
                                        receiver_id: userId,
                                        id: guid.generate(),
                                        is_read: true,
                                        trashed: false,
                                        deleted: false,
                                        mailbox_type: 'inbox',
                                    },
                                    {
                                        receiver_id: otherUser.id,
                                        id: guid.generate(),
                                        is_read: true,
                                        trashed: false,
                                        deleted: false,
                                        mailbox_type: 'sentbox',
                                    },
                                ],
                            },
                            {
                                id: guid.generate(),
                                sender_id: otherUser.id,
                                sender_type: 'Message from you to me',
                                body: 'hey yo!',
                                updated_at: 1470705524,
                                created_at: 1470705524,
                                receipts: [
                                    {
                                        receiver_id: userId,
                                        id: guid.generate(),
                                        is_read: true,
                                        trashed: false,
                                        deleted: false,
                                        mailbox_type: 'inbox',
                                    },
                                    {
                                        receiver_id: otherUser.id,
                                        id: guid.generate(),
                                        is_read: true,
                                        trashed: false,
                                        deleted: false,
                                        mailbox_type: 'sentbox',
                                    },
                                ],
                            },
                        ],
                    },
                    overrides,
                );
            },

            getInstance(overrides, user, otherUser) {
                return Conversation.new(this.sampleAttrs(overrides, user, otherUser));
            },

            getInstanceForUser(user, overrides) {
                return Conversation.new(this.sampleAttrs(overrides, user));
            },
        };

        return {};
    },
]);
