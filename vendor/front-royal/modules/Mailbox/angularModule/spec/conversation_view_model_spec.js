import 'AngularSpecHelper';
import 'Careers/angularModule/spec/_mock/fixtures/hiring_relationship_fixtures';
import 'Careers/angularModule/spec/careers_spec_helper';
import 'Mailbox/angularModule';
import 'Users/angularModule/spec/_mock/fixtures/users';

describe('Mailbox::ConversationViewModel', () => {
    let CareersSpecHelper;
    let $injector;
    let ConversationViewModel;
    let Conversation;
    let user;
    let conversation;
    let careersNetworkViewModel;
    let conversationViewModel;
    let User;
    let otherUser;

    beforeEach(() => {
        angular.mock.module('CareersSpecHelper', 'FrontRoyal.Mailbox', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector => {
                $injector = _$injector;
                CareersSpecHelper = $injector.get('CareersSpecHelper');
                Conversation = $injector.get('Mailbox.Conversation');
                ConversationViewModel = $injector.get('ConversationViewModel');
                $injector.get('ConversationFixtures');
                User = $injector.get('User');
                $injector.get('UserFixtures');
                $injector.get('HiringRelationshipFixtures');

                user = User.fixtures.getInstance();
                conversation = Conversation.fixtures.getInstanceForUser(user);
            },
        ]);

        careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
        conversationViewModel = new ConversationViewModel(conversation, user, careersNetworkViewModel);

        const anotherRecipient = _.find(
            conversationViewModel.conversation.recipients,
            recipient => recipient.id !== conversationViewModel.currentUserId,
        );
        otherUser = User.fixtures.getInstance({
            id: anotherRecipient.id,
        });
    });

    describe('addMessage', () => {
        it('should add a new message', () => {
            const origRecipients = _.clone(conversation.recipients);
            const origMessageCount = conversation.messages.length;
            conversationViewModel.addMessage('a new message!');

            expect(conversation.messages.length).toBe(origMessageCount + 1);
            expect(_.first(conversation.messages).body).toEqual('a new message!');
            expect(conversation.recipients).toEqual(origRecipients);
        });

        it('should add a new recipient', () => {
            careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager', {
                teammateId: 'teammate',
            });
            const user = careersNetworkViewModel.user;
            const hiringRelationshipViewModel = careersNetworkViewModel.testHelper.getHiringRelationshipViewModelWithConversation();
            const conversationViewModel = hiringRelationshipViewModel.conversationViewModel;
            const conversation = hiringRelationshipViewModel.conversation;

            // sanity check
            expect(_.chain(conversation.recipients).pluck('id').contains(user.id).value()).toBe(false);
            conversationViewModel.addMessage('a new message!');

            const participantEntry = _.findWhere(conversation.recipients, {
                id: user.id,
            });
            expect(participantEntry).not.toBeUndefined();
            expect(participantEntry.avatar_url).toEqual(user.avatar_url);
            expect(participantEntry.display_name).toEqual(user.preferredName);
            expect(participantEntry.type).toEqual('user');
        });
    });

    describe('allUnread', () => {
        it('should work', () => {
            const conversation = conversationViewModel.conversation;

            // false with no messages
            conversation.messages = [];
            expect(conversationViewModel.allUnread).toBe(false);

            // false with a message from me
            conversationViewModel.addMessage('message from me');
            expect(conversationViewModel.allUnread).toBe(false);

            // true with an unread message from the other
            const receipt = addMessageFromOther().receipt;
            expect(conversationViewModel.allUnread).toBe(true);

            // false with a read message from the other
            receipt.is_read = true;
            expect(conversationViewModel.allUnread).toBe(false);

            // false with both a read message and an unread message from the other
            addMessageFromOther();
            expect(conversationViewModel.allUnread).toBe(false);
        });
    });

    describe('unreadMessageCount', () => {
        it('should work', () => {
            const conversation = conversationViewModel.conversation;

            // 0 with no messages
            conversation.messages = [];
            expect(conversationViewModel.unreadMessageCount).toBe(0);

            // 0 with a message from me
            conversationViewModel.addMessage('message from me');
            expect(conversationViewModel.unreadMessageCount).toBe(0);

            // 1 with an unread message from the other
            const receipt = addMessageFromOther().receipt;
            expect(conversationViewModel.unreadMessageCount).toBe(1);

            // 0 with a read message from the other
            receipt.is_read = true;
            expect(conversationViewModel.unreadMessageCount).toBe(0);

            // 1 with both a read message and an unread message from the other
            addMessageFromOther();
            expect(conversationViewModel.unreadMessageCount).toBe(1);

            // 2 with another unread message
            addMessageFromOther();
            expect(conversationViewModel.unreadMessageCount).toBe(2);
        });
    });

    function addMessageFromOther() {
        const Message = $injector.get('Mailbox.Message');
        const conversation = conversationViewModel.conversation;
        const message = conversation.addMessage(Message.new({}).setSender(otherUser));
        // We do not create receipts for the other people client-side.
        // We let the server do it.
        const receipt = {
            receiver_id: conversationViewModel.currentUserId,
            is_read: false,
        };
        message.receipts.push(receipt);
        return {
            message,
            receipt,
        };
    }
});
