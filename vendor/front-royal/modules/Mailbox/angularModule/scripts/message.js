import angularModule from 'Mailbox/angularModule/scripts/mailbox_module';

angularModule.factory('Mailbox.Message', [
    '$injector',
    $injector => {
        const Iguana = $injector.get('Iguana');

        return Iguana.subclass(function () {
            this.setCollection('messages');
            this.alias('Mailbox.Message');
            this.embeddedIn('conversation');

            this.setCallback('after', 'copyAttrsOnInitialize', function () {
                this.receipts = this.receipts || [];
            });

            Object.defineProperty(this.prototype, 'sender', {
                get() {
                    if (!this.$$sender) {
                        this.$$sender = _.findWhere(this.conversation().recipients, {
                            id: this.sender_id,
                        });
                    }
                    return this.$$sender;
                },
            });

            Object.defineProperty(this.prototype, 'recipient', {
                get() {
                    if (!this.$$recipient) {
                        this.$$recipient = _.find(this.conversation().recipients, r => r.id !== this.sender_id);
                    }
                    return this.$$recipient;
                },
            });

            Object.defineProperty(this.prototype, 'senderName', {
                get() {
                    return this.sender && this.sender.display_name;
                },
            });

            Object.defineProperty(this.prototype, 'recipientName', {
                get() {
                    return this.recipient && this.recipient.display_name;
                },
            });

            Object.defineProperty(this.prototype, 'sentAt', {
                get() {
                    return new Date(this.created_at * 1000);
                },
            });

            Object.defineProperty(this.prototype, 'avatarUrl', {
                get() {
                    return this.sender && this.sender.avatar_url;
                },
            });

            Object.defineProperty(this.prototype, 'paragraphs', {
                get() {
                    if (!this.$$paragraphs || this.$$paragraphText !== this.body) {
                        this.$$paragraphText = this.body;
                        this.$$paragraphs = this.body.split(/\n+/);
                    }
                    return this.$$paragraphs;
                },
            });

            return {
                setSender(type, id) {
                    this.sender_id = id;
                    this.sender_type = type; // see recipient_types on the server (this is always 'user' right now)
                    this.receipts.push({
                        receiver_id: id,
                        is_read: true,
                        trashed: false,
                        deleted: false,
                        mailbox_type: 'sentbox',
                    });
                    return this;
                },

                receiptFor(receiverId) {
                    return _.findWhere(this.receipts, {
                        receiver_id: receiverId,
                    });
                },
            };
        });
    },
]);
