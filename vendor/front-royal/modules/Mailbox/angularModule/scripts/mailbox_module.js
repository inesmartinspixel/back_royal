import 'FrontRoyalApiErrorHandler/angularModule';
import 'Users/angularModule';

export default angular.module('FrontRoyal.Mailbox', ['FrontRoyal.Users', 'FrontRoyal.ApiErrorHandler']);
