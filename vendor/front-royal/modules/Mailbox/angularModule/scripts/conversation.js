import angularModule from 'Mailbox/angularModule/scripts/mailbox_module';

angularModule.factory('Mailbox.Conversation', [
    '$injector',
    $injector => {
        const Iguana = $injector.get('Iguana');
        const Message = $injector.get('Mailbox.Message');

        return Iguana.subclass(function () {
            this.setCollection('conversations');
            this.alias('Mailbox.Conversation');
            this.embedsMany('messages', 'Mailbox.Message');

            this.setCallback('after', 'copyAttrsOnInitialize', function () {
                if (!this.created_at) {
                    this.created_at = new Date().getTime() / 1000;
                    this.updated_at = new Date().getTime() / 1000;
                }

                this.recipients = this.recipients || [];
                this.messages = this.messages || [];
            });

            Object.defineProperty(this.prototype, 'lastMessage', {
                get() {
                    return this.messages[0];
                },
            });

            Object.defineProperty(this.prototype, 'hasMessages', {
                get() {
                    return !!this.messages[0];
                },
            });

            Object.defineProperty(this.prototype, 'lastMessageAtTimestamp', {
                get() {
                    return this.lastMessage && this.lastMessage.updated_at * 1000;
                },
            });

            Object.defineProperty(this.prototype, 'previewText', {
                get() {
                    return this.lastMessage && this.lastMessage.body;
                },
            });

            return {
                isRecipient(userId) {
                    const existingRecipient = _.findWhere(this.recipients, {
                        id: userId,
                    });
                    return !!existingRecipient;
                },

                addUser(id, displayName, avatarUrl) {
                    this.recipients.push({
                        type: 'user',
                        id,
                        display_name: displayName,
                        avatar_url: avatarUrl,
                    });
                    this.recipients = _.uniq(this.recipients, 'id');
                    return this;
                },

                ensureUser(user) {
                    if (!this.isRecipient(user.id)) {
                        this.addUser(user.id, user.preferredName, user.avatar_url);
                    }
                },

                addMessage(message) {
                    if (!message || !message.isA || !message.isA(Message)) {
                        throw new Error('Cannot add object that is not a Message');
                    }
                    message.$$embeddedIn = this;
                    this.messages.unshift(message);
                    return message;
                },
            };
        });
    },
]);
