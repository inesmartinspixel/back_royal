import angularModule from 'Mailbox/angularModule/scripts/mailbox_module';

angularModule.factory('ConversationViewModel', [
    '$injector',

    function factory($injector) {
        const SuperModel = $injector.get('SuperModel');
        const Message = $injector.get('Mailbox.Message');
        const User = $injector.get('User');

        const ConversationViewModel = SuperModel.subclass(function () {
            Object.defineProperty(this.prototype, 'myReceipts', {
                get() {
                    const self = this;
                    return _.chain(this.conversation.messages)
                        .map(message => self._myReceipt(message))
                        .compact()
                        .value();
                },
            });

            Object.defineProperty(this.prototype, 'messagesFromOthers', {
                get() {
                    const self = this;
                    return _.select(self.conversation.messages, message => message.sender_id !== self.currentUserId);
                },
            });

            // filters out receipts for messages that are from me
            Object.defineProperty(this.prototype, 'myReceiptsFromOthers', {
                get() {
                    const self = this;
                    return _.chain(self.messagesFromOthers)
                        .map(message => self._myReceipt(message))
                        .compact()
                        .value();
                },
            });

            Object.defineProperty(this.prototype, 'unreadMessageCount', {
                get() {
                    return _.where(this.myReceiptsFromOthers, {
                        is_read: false,
                    }).length;
                },
            });

            Object.defineProperty(this.prototype, 'hasUnread', {
                get() {
                    return this.unreadMessageCount > 0;
                },
            });

            Object.defineProperty(this.prototype, 'allUnread', {
                get() {
                    const unreadMessageCount = this.unreadMessageCount;
                    return unreadMessageCount > 0 && unreadMessageCount === this.myReceiptsFromOthers.length;
                },
            });

            Object.defineProperty(this.prototype, 'currentUserId', {
                get() {
                    return this.currentUser.id;
                },
            });

            Object.defineProperty(this.prototype, 'mostRecentMessage', {
                get() {
                    // messages are ordered in reverse chronological
                    return _.first(this.conversation.messages);
                },
            });

            return {
                initialize(conversation, currentUser, careersNetworkViewModel) {
                    this.careersNetworkViewModel = careersNetworkViewModel;
                    this.conversation = conversation;
                    if (!currentUser || !currentUser.isA || !currentUser.isA(User)) {
                        throw new Error('currentUser must be a User');
                    }
                    this.currentUser = currentUser;
                    this.expanded = false;
                },

                addMessage(body, metadata) {
                    const self = this;

                    const now = Date.now() / 1000;
                    const message = Message.new({
                        updated_at: now,
                        created_at: now,
                        body,
                        metadata,
                    }).setSender('user', this.currentUserId);

                    self.conversation.ensureUser(this.currentUser);
                    self.conversation.addMessage(message);
                    return message;
                },

                markAllAsRead() {
                    let changedSomething = false;
                    _.each(this.myReceipts, receipt => {
                        if (!receipt.is_read) {
                            changedSomething = true;
                        }
                        receipt.is_read = true;
                    });
                    return changedSomething;
                },

                updateAllReceipts(obj) {
                    _.chain(this.myReceipts).each(receipt => {
                        _.extend(receipt, obj);
                    });
                },

                messagesFrom(senderId) {
                    return _.select(this.conversation.messages, message => message.sender_id === senderId);
                },

                allUnreadFrom(senderId, receiverId) {
                    const self = this;
                    return _.chain(self.messagesFrom(senderId))
                        .map(message => {
                            // It is an edge case, but it is possible that there are
                            // messages for which I do not have a receipt.  This can happen
                            // if we've transferred relationships from one hiring manager to
                            // another.  See transfer_hiring_relationships rake task.
                            //
                            // A message is only considered unread if I have a receipt for it
                            // and the receipt is not marked as read
                            const ourReceipt = self._ourReceipt(message, receiverId);
                            return ourReceipt && !ourReceipt.is_read;
                        })
                        .all()
                        .value();
                },

                _myReceipt(message) {
                    return _.findWhere(message.receipts, {
                        receiver_id: this.currentUserId,
                    });
                },

                _ourReceipt(message, receiverId) {
                    return _.findWhere(message.receipts, {
                        receiver_id: receiverId,
                    });
                },
            };
        });

        return ConversationViewModel;
    },
]);
