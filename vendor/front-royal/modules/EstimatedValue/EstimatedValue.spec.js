import { EstimatedValue } from 'EstimatedValue';

let cohortApplication;
let careerProfile;
let programType;

describe('EstimatedValue', () => {
    beforeEach(() => {
        // FIXME: Make this user our fixtures once we have access to them
        // from the modules side.
        cohortApplication = {};
        careerProfile = {
            salary_range: null,
        };

        Object.defineProperty(careerProfile, 'placeCountry', {
            get() {
                return null;
            },
            configurable: true,
        });

        Object.defineProperty(careerProfile, 'placeState', {
            get() {
                return null;
            },
            configurable: true,
        });
    });

    describe('ev2', () => {
        beforeEach(() => {
            EstimatedValue.stateBlacklist = ['SB'];
            EstimatedValue.cat2CountryWhitelist = ['CW'];
            EstimatedValue.cat3CountryBlacklist = ['CB'];
        });

        describe('mba', () => {
            beforeEach(() => {
                programType = 'mba';
            });

            describe('US', () => {
                beforeEach(() => {
                    jest.spyOn(careerProfile, 'placeCountry', 'get').mockReturnValue('US');
                });

                describe('non-blacklisted state', () => {
                    beforeEach(() => {
                        jest.spyOn(careerProfile, 'placeState', 'get').mockReturnValue('NB');
                    });

                    it('should work', () => {
                        careerProfile.salary_range = 'less_than_40000';
                        expect(EstimatedValue.ev2(cohortApplication, careerProfile, programType)).toBe(15);
                    });

                    it('should call valueForSalaryRange properly', () => {
                        careerProfile.salary_range = 'over_200000';
                        jest.spyOn(EstimatedValue, 'valueForSalaryRange');

                        // eslint-disable-next-line
                        EstimatedValue.ev2(cohortApplication, careerProfile, programType);

                        expect(EstimatedValue.valueForSalaryRange).toBeCalledWith('over_200000', [
                            15,
                            24,
                            121,
                            253,
                            140,
                        ]);
                    });
                });

                it('should return zero for blacklisted state', () => {
                    jest.spyOn(careerProfile, 'placeState', 'get').mockReturnValue('SB');
                    careerProfile.salary_range = 'less_than_40000';
                    expect(EstimatedValue.ev2(cohortApplication, careerProfile, programType)).toBe(0);
                });
            });

            describe('in cat2CountryWhitelist', () => {
                beforeEach(() => {
                    jest.spyOn(careerProfile, 'placeCountry', 'get').mockReturnValue('CW');
                });

                it('should work', () => {
                    careerProfile.salary_range = '70000_to_79999';
                    expect(EstimatedValue.ev2(cohortApplication, careerProfile, programType)).toBe(158);
                });

                it('should call valueForSalaryRange properly', () => {
                    careerProfile.salary_range = 'over_200000';
                    jest.spyOn(EstimatedValue, 'valueForSalaryRange');

                    // eslint-disable-next-line
                    EstimatedValue.ev2(cohortApplication, careerProfile, programType);

                    expect(EstimatedValue.valueForSalaryRange).toBeCalledWith('over_200000', [15, 45, 158, 253, 83]);
                });
            });

            describe('not in cat3CountryBlacklist', () => {
                beforeEach(() => {
                    jest.spyOn(careerProfile, 'placeCountry', 'get').mockReturnValue('NB');
                });

                it('should work', () => {
                    careerProfile.salary_range = 'prefer_not_to_disclose';
                    expect(EstimatedValue.ev2(cohortApplication, careerProfile, programType)).toBe(25);
                });

                it('should call valueForSalaryRange properly', () => {
                    careerProfile.salary_range = 'over_200000';
                    jest.spyOn(EstimatedValue, 'valueForSalaryRange');

                    // eslint-disable-next-line
                    EstimatedValue.ev2(cohortApplication, careerProfile, programType);

                    expect(EstimatedValue.valueForSalaryRange).toBeCalledWith('over_200000', [13, 28, 98, 81, 25]);
                });
            });

            it('should return 0 if in cat3CountryBlacklist', () => {
                jest.spyOn(careerProfile, 'placeCountry', 'get').mockReturnValue('CB');
                expect(EstimatedValue.ev2(cohortApplication, careerProfile, programType)).toBe(0);
            });
        });

        describe('emba', () => {
            beforeEach(() => {
                programType = 'emba';
            });

            describe('US', () => {
                beforeEach(() => {
                    jest.spyOn(careerProfile, 'placeCountry', 'get').mockReturnValue('US');
                });

                describe('non-blacklisted state', () => {
                    beforeEach(() => {
                        jest.spyOn(careerProfile, 'placeState', 'get').mockReturnValue('NB');
                    });

                    it('should work', () => {
                        careerProfile.salary_range = 'less_than_40000';
                        expect(EstimatedValue.ev2(cohortApplication, careerProfile, programType)).toBe(200);
                    });

                    it('should call valueForSalaryRange properly', () => {
                        careerProfile.salary_range = 'over_200000';
                        jest.spyOn(EstimatedValue, 'valueForSalaryRange');

                        // eslint-disable-next-line
                        EstimatedValue.ev2(cohortApplication, careerProfile, programType);

                        expect(EstimatedValue.valueForSalaryRange).toBeCalledWith('over_200000', [
                            200,
                            342,
                            717,
                            1202,
                            563,
                        ]);
                    });
                });

                it('should return zero for blacklisted state', () => {
                    jest.spyOn(careerProfile, 'placeState', 'get').mockReturnValue('SB');
                    careerProfile.salary_range = 'less_than_40000';
                    expect(EstimatedValue.ev2(cohortApplication, careerProfile, programType)).toBe(0);
                });
            });

            describe('in cat2CountryWhitelist', () => {
                beforeEach(() => {
                    jest.spyOn(careerProfile, 'placeCountry', 'get').mockReturnValue('CW');
                });

                it('should work', () => {
                    careerProfile.salary_range = '70000_to_79999';
                    expect(EstimatedValue.ev2(cohortApplication, careerProfile, programType)).toBe(772);
                });

                it('should call valueForSalaryRange properly', () => {
                    careerProfile.salary_range = 'over_200000';
                    jest.spyOn(EstimatedValue, 'valueForSalaryRange');

                    // eslint-disable-next-line
                    EstimatedValue.ev2(cohortApplication, careerProfile, programType);

                    expect(EstimatedValue.valueForSalaryRange).toBeCalledWith('over_200000', [228, 229, 772, 995, 555]);
                });
            });

            describe('not in cat3CountryBlacklist', () => {
                beforeEach(() => {
                    jest.spyOn(careerProfile, 'placeCountry', 'get').mockReturnValue('NB');
                });

                it('should work', () => {
                    careerProfile.salary_range = 'prefer_not_to_disclose';
                    expect(EstimatedValue.ev2(cohortApplication, careerProfile, programType)).toBe(231);
                });

                it('should call valueForSalaryRange properly', () => {
                    careerProfile.salary_range = 'over_200000';
                    jest.spyOn(EstimatedValue, 'valueForSalaryRange');

                    // eslint-disable-next-line
                    EstimatedValue.ev2(cohortApplication, careerProfile, programType);

                    expect(EstimatedValue.valueForSalaryRange).toBeCalledWith('over_200000', [88, 218, 350, 463, 231]);
                });
            });

            it('should return 0 if in cat3CountryBlacklist', () => {
                jest.spyOn(careerProfile, 'placeCountry', 'get').mockReturnValue('CB');
                expect(EstimatedValue.ev2(cohortApplication, careerProfile, programType)).toBe(0);
            });
        });

        it('should return 32 for biz cert', () => {
            programType = 'the_business_certificate';
            expect(EstimatedValue.ev2(cohortApplication, careerProfile, programType)).toBe(32);
        });

        it('should return 0 for other programs', () => {
            programType = 'some_other_program';
            expect(EstimatedValue.ev2(cohortApplication, careerProfile, programType)).toBe(0);
        });
    });
});
