const EstimatedValue = {
    // prettier-ignore
    stateBlacklist: ['AL','AK','CA','DE','GA','MD','MT','NM','NY','ND','OR','RI','WY'],

    // prettier-ignore
    cat2CountryWhitelist: ['GB', 'SG', 'DE', 'AT', 'SE', 'NO', 'DK', 'FI', 'IE', 'AU', 'CA', 'CH', 'BE', 'NL', 'FR', 'NZ', 'LU', 'JP'],

    // prettier-ignore
    cat3CountryBlacklist: ['US','GB', 'SG', 'DE', 'AT', 'SE', 'NO', 'DK', 'FI', 'IE', 'AU', 'CA', 'CH', 'BE', 'NL', 'FR', 'NZ', 'LU', 'JP','BE','IR','SD','SY','KP','CU'],

    valueForSalaryRange: (salaryRange, rangeValues) => {
        if (salaryRange === 'less_than_40000') {
            return rangeValues[0];
        }
        if (['40000_to_49999', '50000_to_59999', '60000_to_69999'].includes(salaryRange)) {
            return rangeValues[1];
        }
        if (['70000_to_79999', '80000_to_89999', '90000_to_99999', '100000_to_119999'].includes(salaryRange)) {
            return rangeValues[2];
        }
        if (['120000_to_149999', '150000_to_199999', 'over_200000'].includes(salaryRange)) {
            return rangeValues[3];
        }
        if (salaryRange === 'prefer_not_to_disclose') {
            return rangeValues[4];
        }

        throw new Error('salaryRange not found');
    },

    // See https://trello.com/c/GqjJaNCi
    ev2: (cohortApplication, careerProfile, programType) => {
        const salaryRange = careerProfile.salary_range;

        // Note: I made a decision to just look at the career_profile's
        // place_details, since the user can change the state value to anything
        // in the Documents UI, but we want the abbreviation. Country would be okay
        // to use from the user because it is a select input, but I wanted to be
        // consistent.
        const country = careerProfile.placeCountry;
        const state = careerProfile.placeState;

        let value = 0;

        if (programType === 'mba') {
            if (country === 'US') {
                if (!EstimatedValue.stateBlacklist.includes(state)) {
                    value = EstimatedValue.valueForSalaryRange(salaryRange, [15, 24, 121, 253, 140]);
                }
            } else if (EstimatedValue.cat2CountryWhitelist.includes(country)) {
                value = EstimatedValue.valueForSalaryRange(salaryRange, [15, 45, 158, 253, 83]);
            } else if (!EstimatedValue.cat3CountryBlacklist.includes(country)) {
                value = EstimatedValue.valueForSalaryRange(salaryRange, [13, 28, 98, 81, 25]);
            }
        } else if (programType === 'emba') {
            if (country === 'US') {
                if (!EstimatedValue.stateBlacklist.includes(state)) {
                    value = EstimatedValue.valueForSalaryRange(salaryRange, [200, 342, 717, 1202, 563]);
                }
            } else if (EstimatedValue.cat2CountryWhitelist.includes(country)) {
                value = EstimatedValue.valueForSalaryRange(salaryRange, [228, 229, 772, 995, 555]);
            } else if (!EstimatedValue.cat3CountryBlacklist.includes(country)) {
                value = EstimatedValue.valueForSalaryRange(salaryRange, [88, 218, 350, 463, 231]);
            }
        } else if (programType === 'the_business_certificate') {
            value = 32;
        }

        return value;
    },
};

export default EstimatedValue;
