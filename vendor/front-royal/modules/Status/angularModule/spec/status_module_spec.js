import 'AngularSpecHelper';
import 'Status/angularModule';

describe('FrontRoyal.Status', () => {
    let $injector;
    let $timeout;
    let $http;
    let $q;
    let HttpQueue;
    let StatusPoller;
    let $rootScope;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Status', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector => {
                $injector = _$injector;
                $timeout = $injector.get('$timeout');
                $http = $injector.get('$http');
                $q = $injector.get('$q');
                HttpQueue = $injector.get('HttpQueue');
                StatusPoller = $injector.get('FrontRoyal.StatusPoller');
                $rootScope = $injector.get('$rootScope');
            },
        ]);

        StatusPoller.activate();
    });

    afterEach(() => {
        StatusPoller.deactivate();
    });

    describe('polling', () => {
        it('should hit the simple endpoint at a regular interval', () => {
            let resolve;
            $rootScope.currentUser = {};

            jest.spyOn($http, 'get').mockReturnValue(
                $q(_resolve => {
                    resolve = _resolve;
                }),
            );
            $timeout.flush(61 * 1000);

            expect($http.get).toHaveBeenCalled();
            resolve();
            $timeout.flush(0);
            $http.get.mockClear();

            $timeout.flush(30 * 1000);
            expect($http.get).not.toHaveBeenCalled();
            $timeout.flush(61 * 1000);
            expect($http.get).toHaveBeenCalled();
        });

        it('should hit an exponential backoff on an error', () => {
            let reject;
            $rootScope.currentUser = {};
            jest.spyOn(HttpQueue.prototype, 'unfreezeAfterError').mockImplementation();

            jest.spyOn($http, 'get').mockReturnValue(
                $q((_resolve, _reject) => {
                    reject = () => {
                        _reject({
                            config: {},
                        });
                        $timeout.flush(0);
                        expect(HttpQueue.prototype.unfreezeAfterError).toHaveBeenCalled();
                        HttpQueue.prototype.unfreezeAfterError.mockClear();
                    };
                }),
            );
            $timeout.flush(61 * 1000);

            expect($http.get).toHaveBeenCalled();
            reject();
            $http.get.mockClear();

            // first failure
            let failureCount = 1;
            let delay = 60 * 1000 * failureCount ** 2;
            $timeout.flush(delay - 2);
            expect($http.get).not.toHaveBeenCalled();
            $timeout.flush(3);
            expect($http.get).toHaveBeenCalled();
            $http.get.mockClear();

            // second failure
            failureCount = 2;
            delay = 60 * 1000 * failureCount ** 2;
            $timeout.flush(delay - 2);
            expect($http.get).not.toHaveBeenCalled();
            $timeout.flush(3);
            expect($http.get).toHaveBeenCalled();
        });
    });
});
