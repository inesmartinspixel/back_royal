import 'AngularHttpQueueAndRetry/angularModule';
import 'OfflineMode/angularModule';

export default angular
    .module('FrontRoyal.Status', ['HttpQueueAndRetry', 'OfflineMode'])
    .factory('FrontRoyal.StatusPoller', [
        '$injector',
        $injector => {
            const $http = $injector.get('$http');
            const HttpQueue = $injector.get('HttpQueue');
            const $timeout = $injector.get('$timeout');
            const $document = $injector.get('$document');
            const offlineModeManager = $injector.get('offlineModeManager');
            const baseDelay = 60 * 1000;
            const oneHour = 1000 * 60 * 60;
            let pollingPromise;
            let failureCount;

            return {
                activate() {
                    failureCount = 0;

                    // global hotkey
                    $($document).on('keydown.StatusPoller', $event => {
                        if ($event.ctrlKey && $event.key === 'g') {
                            this.send();
                        }
                    });

                    // rolling status polling
                    this.reset();
                },

                deactivate() {
                    this._cancel();
                    $($document).off('.StatusPoller');
                },

                send() {
                    if (offlineModeManager.inOfflineMode) {
                        this.reset();
                        return Promise.resolve();
                    }

                    this._cancel();

                    return $http
                        .get(`${window.ENDPOINT_ROOT}/api/status/simple.json`, {
                            'FrontRoyal.ApiErrorHandler': {
                                skip: true,
                            },
                        })
                        .then(() => {
                            failureCount = 0;
                        })
                        .catch(response => {
                            failureCount += 1;

                            // See https://sentry.io/pedago/front-royal/issues/312756058/?query=config
                            // We know that the changes made here will cause a new Sentry error due to changes in the
                            // fingerprint.
                            if (!response.config) {
                                const error = new Error('response.config undefined');
                                error.extra = {
                                    responseKeys: _.keys(response),
                                    responseType: typeof response,
                                };
                                if (typeof response === 'string') {
                                    error.extra.responseContent = response.slice(0, 1000);
                                }

                                if (response && response.status) {
                                    error.extra.status = response.status;
                                }

                                $injector.get('DialogModal').showFatalError();
                                $injector.get('ErrorLogService').notifyInProd(error);
                                return;
                            }

                            // We don't care if this fails, we'll try again later. So catch the errors
                            HttpQueue.unfreezeAfterError(response.config);
                        })
                        .finally(() => {
                            this.reset();
                        });
                },

                reset() {
                    this._cancel();

                    let delay = failureCount ? baseDelay * failureCount ** 2 : baseDelay; // exponential backoff

                    // never wait longer than an hour, so that if we go offline for a while,
                    // after coming back the pings eventually start up again.
                    delay = Math.min(delay, oneHour);

                    pollingPromise = $timeout(() => {
                        this.send();
                    }, delay);
                },

                _cancel() {
                    if (pollingPromise) {
                        $timeout.cancel(pollingPromise);
                        pollingPromise = null;
                        return true;
                    }
                    return false;
                },
            };
        },
    ]);
