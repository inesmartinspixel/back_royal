// angular-ui-bootstrap comes from a custom build inside /modules
import 'FrontRoyalUiBootstrap/angular-ui-boostrap';
import 'angular-bootstrap-datetimepicker';

export default angular.module('FrontRoyal.UiBootstrap', ['ui.bootstrap']);
