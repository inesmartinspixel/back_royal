/*
 * angular-ui-bootstrap
 * http://angular-ui.github.io/bootstrap/

 * Version: 2.5.0 - 2017-01-28
 * License: MIT
 */ angular.module(
    'ui.bootstrap',
    [
        'ui.bootstrap.tpls',
        'ui.bootstrap.carousel',
        'ui.bootstrap.popover',
        'ui.bootstrap.tooltip',
        'ui.bootstrap.position',
        'ui.bootstrap.stackedMap',
        'ui.bootstrap.typeahead',
        'ui.bootstrap.debounce',
    ],
);
angular.module('ui.bootstrap.tpls', [
    'uib/template/carousel/carousel.html',
    'uib/template/carousel/slide.html',
    'uib/template/popover/popover-html.html',
    'uib/template/popover/popover-template.html',
    'uib/template/popover/popover.html',
    'uib/template/tooltip/tooltip-html-popup.html',
    'uib/template/tooltip/tooltip-popup.html',
    'uib/template/tooltip/tooltip-template-popup.html',
    'uib/template/typeahead/typeahead-match.html',
    'uib/template/typeahead/typeahead-popup.html',
]);
angular
    .module('ui.bootstrap.carousel', [])

    .controller('UibCarouselController', [
        '$scope',
        '$element',
        '$interval',
        '$timeout',
        '$animate',
        function ($scope, $element, $interval, $timeout, $animate) {
            const self = this;
            const slides = (self.slides = $scope.slides = []);
            const SLIDE_DIRECTION = 'uib-slideDirection';
            let currentIndex = $scope.active;
            let currentInterval;
            let isPlaying;

            let destroyed = false;
            $element.addClass('carousel');

            self.addSlide = function (slide, element) {
                slides.push({
                    slide,
                    element,
                });
                slides.sort(function (a, b) {
                    return +a.slide.index - +b.slide.index;
                });
                // if this is the first slide or the slide is set to active, select it
                if (slide.index === $scope.active || (slides.length === 1 && !angular.isNumber($scope.active))) {
                    if ($scope.$currentTransition) {
                        $scope.$currentTransition = null;
                    }

                    currentIndex = slide.index;
                    $scope.active = slide.index;
                    setActive(currentIndex);
                    self.select(slides[findSlideIndex(slide)]);
                    if (slides.length === 1) {
                        $scope.play();
                    }
                }
            };

            self.getCurrentIndex = function () {
                for (let i = 0; i < slides.length; i++) {
                    if (slides[i].slide.index === currentIndex) {
                        return i;
                    }
                }
            };

            self.next = $scope.next = function () {
                const newIndex = (self.getCurrentIndex() + 1) % slides.length;

                if (newIndex === 0 && $scope.noWrap()) {
                    $scope.pause();
                    return;
                }

                return self.select(slides[newIndex], 'next');
            };

            self.prev = $scope.prev = function () {
                const newIndex = self.getCurrentIndex() - 1 < 0 ? slides.length - 1 : self.getCurrentIndex() - 1;

                if ($scope.noWrap() && newIndex === slides.length - 1) {
                    $scope.pause();
                    return;
                }

                return self.select(slides[newIndex], 'prev');
            };

            self.removeSlide = function (slide) {
                const index = findSlideIndex(slide);

                // get the index of the slide inside the carousel
                slides.splice(index, 1);
                if (slides.length > 0 && currentIndex === index) {
                    if (index >= slides.length) {
                        currentIndex = slides.length - 1;
                        $scope.active = currentIndex;
                        setActive(currentIndex);
                        self.select(slides[slides.length - 1]);
                    } else {
                        currentIndex = index;
                        $scope.active = currentIndex;
                        setActive(currentIndex);
                        self.select(slides[index]);
                    }
                } else if (currentIndex > index) {
                    currentIndex--;
                    $scope.active = currentIndex;
                }

                // clean the active value when no more slide
                if (slides.length === 0) {
                    currentIndex = null;
                    $scope.active = null;
                }
            };

            /* direction: "prev" or "next" */
            self.select = $scope.select = function (nextSlide, direction) {
                const nextIndex = findSlideIndex(nextSlide.slide);
                // Decide direction if it's not given
                if (direction === undefined) {
                    direction = nextIndex > self.getCurrentIndex() ? 'next' : 'prev';
                }
                // Prevent this user-triggered transition from occurring if there is already one in progress
                if (nextSlide.slide.index !== currentIndex && !$scope.$currentTransition) {
                    goNext(nextSlide.slide, nextIndex, direction);
                }
            };

            /* Allow outside people to call indexOf on slides array */
            $scope.indexOfSlide = function (slide) {
                return +slide.slide.index;
            };

            $scope.isActive = function (slide) {
                return $scope.active === slide.slide.index;
            };

            $scope.isPrevDisabled = function () {
                return $scope.active === 0 && $scope.noWrap();
            };

            $scope.isNextDisabled = function () {
                return $scope.active === slides.length - 1 && $scope.noWrap();
            };

            $scope.pause = function () {
                if (!$scope.noPause) {
                    isPlaying = false;
                    resetTimer();
                }
            };

            $scope.play = function () {
                if (!isPlaying) {
                    isPlaying = true;
                    restartTimer();
                }
            };

            $element.on('mouseenter', $scope.pause);
            $element.on('mouseleave', $scope.play);

            $scope.$on('$destroy', function () {
                destroyed = true;
                resetTimer();
            });

            $scope.$watch('noTransition', function (noTransition) {
                $animate.enabled($element, !noTransition);
            });

            $scope.$watch('interval', restartTimer);

            $scope.$watchCollection('slides', resetTransition);

            $scope.$watch('active', function (index) {
                if (angular.isNumber(index) && currentIndex !== index) {
                    for (let i = 0; i < slides.length; i++) {
                        if (slides[i].slide.index === index) {
                            index = i;
                            break;
                        }
                    }

                    const slide = slides[index];
                    if (slide) {
                        setActive(index);
                        self.select(slides[index]);
                        currentIndex = index;
                    }
                }
            });

            function getSlideByIndex(index) {
                for (let i = 0, l = slides.length; i < l; ++i) {
                    if (slides[i].index === index) {
                        return slides[i];
                    }
                }
            }

            function setActive(index) {
                for (let i = 0; i < slides.length; i++) {
                    slides[i].slide.active = i === index;
                }
            }

            function goNext(slide, index, direction) {
                if (destroyed) {
                    return;
                }

                angular.extend(slide, { direction });
                angular.extend(slides[currentIndex].slide || {}, { direction });
                if (
                    $animate.enabled($element) &&
                    !$scope.$currentTransition &&
                    slides[index].element &&
                    self.slides.length > 1
                ) {
                    slides[index].element.data(SLIDE_DIRECTION, slide.direction);
                    const currentIdx = self.getCurrentIndex();

                    if (angular.isNumber(currentIdx) && slides[currentIdx].element) {
                        slides[currentIdx].element.data(SLIDE_DIRECTION, slide.direction);
                    }

                    $scope.$currentTransition = true;
                    $animate.on('addClass', slides[index].element, function (element, phase) {
                        if (phase === 'close') {
                            $scope.$currentTransition = null;
                            $animate.off('addClass', element);
                        }
                    });
                }

                $scope.active = slide.index;
                currentIndex = slide.index;
                setActive(index);

                // every time you change slides, reset the timer
                restartTimer();
            }

            function findSlideIndex(slide) {
                for (let i = 0; i < slides.length; i++) {
                    if (slides[i].slide === slide) {
                        return i;
                    }
                }
            }

            function resetTimer() {
                if (currentInterval) {
                    $interval.cancel(currentInterval);
                    currentInterval = null;
                }
            }

            function resetTransition(slides) {
                if (!slides.length) {
                    $scope.$currentTransition = null;
                }
            }

            function restartTimer() {
                resetTimer();
                const interval = +$scope.interval;
                if (!isNaN(interval) && interval > 0) {
                    currentInterval = $interval(timerFn, interval);
                }
            }

            function timerFn() {
                const interval = +$scope.interval;
                if (isPlaying && !isNaN(interval) && interval > 0 && slides.length) {
                    $scope.next();
                } else {
                    $scope.pause();
                }
            }
        },
    ])

    .directive('uibCarousel', function () {
        return {
            transclude: true,
            controller: 'UibCarouselController',
            controllerAs: 'carousel',
            restrict: 'A',
            templateUrl(element, attrs) {
                return attrs.templateUrl || 'uib/template/carousel/carousel.html';
            },
            scope: {
                active: '=',
                interval: '=',
                noTransition: '=',
                noPause: '=',
                noWrap: '&',
            },
        };
    })

    .directive('uibSlide', [
        '$animate',
        function ($animate) {
            return {
                require: '^uibCarousel',
                restrict: 'A',
                transclude: true,
                templateUrl(element, attrs) {
                    return attrs.templateUrl || 'uib/template/carousel/slide.html';
                },
                scope: {
                    actual: '=?',
                    index: '=?',
                },
                link(scope, element, attrs, carouselCtrl) {
                    element.addClass('item');
                    carouselCtrl.addSlide(scope, element);
                    // when the scope is destroyed then remove the slide from the current slides array
                    scope.$on('$destroy', function () {
                        carouselCtrl.removeSlide(scope);
                    });

                    scope.$watch('active', function (active) {
                        $animate[active ? 'addClass' : 'removeClass'](element, 'active');
                    });
                },
            };
        },
    ])

    .animation('.item', [
        '$animateCss',
        function ($animateCss) {
            const SLIDE_DIRECTION = 'uib-slideDirection';

            function removeClass(element, className, callback) {
                element.removeClass(className);
                if (callback) {
                    callback();
                }
            }

            return {
                beforeAddClass(element, className, done) {
                    if (className === 'active') {
                        let stopped = false;
                        const direction = element.data(SLIDE_DIRECTION);
                        const directionClass = direction === 'next' ? 'left' : 'right';
                        const removeClassFn = removeClass.bind(this, element, `${directionClass} ${direction}`, done);
                        element.addClass(direction);

                        $animateCss(element, { addClass: directionClass }).start().done(removeClassFn);

                        return function () {
                            stopped = true;
                        };
                    }
                    done();
                },
                beforeRemoveClass(element, className, done) {
                    if (className === 'active') {
                        let stopped = false;
                        const direction = element.data(SLIDE_DIRECTION);
                        const directionClass = direction === 'next' ? 'left' : 'right';
                        const removeClassFn = removeClass.bind(this, element, directionClass, done);

                        $animateCss(element, { addClass: directionClass }).start().done(removeClassFn);

                        return function () {
                            stopped = true;
                        };
                    }
                    done();
                },
            };
        },
    ]);

/**
 * The following features are still outstanding: popup delay, animation as a
 * function, placement as a function, inside, support for more triggers than
 * just mouse enter/leave, and selector delegatation.
 */
angular
    .module('ui.bootstrap.popover', ['ui.bootstrap.tooltip'])

    .directive('uibPopoverTemplatePopup', function () {
        return {
            restrict: 'A',
            scope: { uibTitle: '@', contentExp: '&', originScope: '&' },
            templateUrl: 'uib/template/popover/popover-template.html',
        };
    })

    .directive('uibPopoverTemplate', [
        '$uibTooltip',
        function ($uibTooltip) {
            return $uibTooltip('uibPopoverTemplate', 'popover', 'click', {
                useContentExp: true,
            });
        },
    ])

    .directive('uibPopoverHtmlPopup', function () {
        return {
            restrict: 'A',
            scope: { contentExp: '&', uibTitle: '@' },
            templateUrl: 'uib/template/popover/popover-html.html',
        };
    })

    .directive('uibPopoverHtml', [
        '$uibTooltip',
        function ($uibTooltip) {
            return $uibTooltip('uibPopoverHtml', 'popover', 'click', {
                useContentExp: true,
            });
        },
    ])

    .directive('uibPopoverPopup', function () {
        return {
            restrict: 'A',
            scope: { uibTitle: '@', content: '@' },
            templateUrl: 'uib/template/popover/popover.html',
        };
    })

    .directive('uibPopover', [
        '$uibTooltip',
        function ($uibTooltip) {
            return $uibTooltip('uibPopover', 'popover', 'click');
        },
    ]);

/**
 * The following features are still outstanding: animation as a
 * function, placement as a function, inside, support for more triggers than
 * just mouse enter/leave, html tooltips, and selector delegation.
 */
angular
    .module('ui.bootstrap.tooltip', ['ui.bootstrap.position', 'ui.bootstrap.stackedMap'])

    /**
     * The $tooltip service creates tooltip- and popover-like directives as well as
     * houses global options for them.
     */
    .provider('$uibTooltip', function () {
        // The default options tooltip and popover.
        const defaultOptions = {
            placement: 'top',
            placementClassPrefix: '',
            animation: true,
            popupDelay: 0,
            popupCloseDelay: 0,
            useContentExp: false,
        };

        // Default hide triggers for each show trigger
        const triggerMap = {
            mouseenter: 'mouseleave',
            click: 'click',
            outsideClick: 'outsideClick',
            focus: 'blur',
            none: '',
        };

        // The options specified to the provider globally.
        const globalOptions = {};

        /**
         * `options({})` allows global configuration of all tooltips in the
         * application.
         *
         *   var app = angular.module( 'App', ['ui.bootstrap.tooltip'], function( $tooltipProvider ) {
         *     // place tooltips left instead of top by default
         *     $tooltipProvider.options( { placement: 'left' } );
         *   });
         */
        this.options = function (value) {
            angular.extend(globalOptions, value);
        };

        /**
         * This allows you to extend the set of trigger mappings available. E.g.:
         *
         *   $tooltipProvider.setTriggers( { 'openTrigger': 'closeTrigger' } );
         */
        this.setTriggers = function setTriggers(triggers) {
            angular.extend(triggerMap, triggers);
        };

        /**
         * This is a helper function for translating camel-case to snake_case.
         */
        function snake_case(name) {
            const regexp = /[A-Z]/g;
            const separator = '-';
            return name.replace(regexp, function (letter, pos) {
                return (pos ? separator : '') + letter.toLowerCase();
            });
        }

        /**
         * Returns the actual instance of the $tooltip service.
         * TODO support multiple triggers
         */
        this.$get = [
            '$window',
            '$compile',
            '$timeout',
            '$document',
            '$uibPosition',
            '$interpolate',
            '$rootScope',
            '$parse',
            '$$stackedMap',
            function (
                $window,
                $compile,
                $timeout,
                $document,
                $position,
                $interpolate,
                $rootScope,
                $parse,
                $$stackedMap,
            ) {
                const openedTooltips = $$stackedMap.createNew();
                $document.on('keyup', keypressListener);

                $rootScope.$on('$destroy', function () {
                    $document.off('keyup', keypressListener);
                });

                function keypressListener(e) {
                    if (e.which === 27) {
                        let last = openedTooltips.top();
                        if (last) {
                            last.value.close();
                            last = null;
                        }
                    }
                }

                return function $tooltip(ttType, prefix, defaultTriggerShow, options) {
                    options = angular.extend({}, defaultOptions, globalOptions, options);

                    /**
                     * Returns an object of show and hide triggers.
                     *
                     * If a trigger is supplied,
                     * it is used to show the tooltip; otherwise, it will use the `trigger`
                     * option passed to the `$tooltipProvider.options` method; else it will
                     * default to the trigger supplied to this directive factory.
                     *
                     * The hide trigger is based on the show trigger. If the `trigger` option
                     * was passed to the `$tooltipProvider.options` method, it will use the
                     * mapped trigger from `triggerMap` or the passed trigger if the map is
                     * undefined; otherwise, it uses the `triggerMap` value of the show
                     * trigger; else it will just use the show trigger.
                     */
                    function getTriggers(trigger) {
                        const show = (trigger || options.trigger || defaultTriggerShow).split(' ');
                        const hide = show.map(function (trigger) {
                            return triggerMap[trigger] || trigger;
                        });
                        return {
                            show,
                            hide,
                        };
                    }

                    const directiveName = snake_case(ttType);

                    const startSym = $interpolate.startSymbol();
                    const endSym = $interpolate.endSymbol();
                    const template =
                        `<div ${directiveName}-popup ` +
                        `uib-title="${startSym}title${endSym}" ${
                            options.useContentExp
                                ? 'content-exp="contentExp()" '
                                : `content="${startSym}content${endSym}" `
                        }origin-scope="origScope" ` +
                        `class="uib-position-measure ${prefix}" ` +
                        `tooltip-animation-class="fade"` +
                        `uib-tooltip-classes ` +
                        `ng-class="{ in: isOpen }" ` +
                        `>` +
                        `</div>`;

                    return {
                        compile(tElem, tAttrs) {
                            const tooltipLinker = $compile(template);

                            return function link(scope, element, attrs, tooltipCtrl) {
                                let tooltip;
                                let tooltipLinkedScope;
                                let transitionTimeout;
                                let showTimeout;
                                let hideTimeout;
                                let positionTimeout;
                                let adjustmentTimeout;
                                let appendToBody = angular.isDefined(options.appendToBody)
                                    ? options.appendToBody
                                    : false;
                                let triggers = getTriggers(undefined);
                                const hasEnableExp = angular.isDefined(attrs[`${prefix}Enable`]);
                                let ttScope = scope.$new(true);
                                let repositionScheduled = false;
                                const isOpenParse = angular.isDefined(attrs[`${prefix}IsOpen`])
                                    ? $parse(attrs[`${prefix}IsOpen`])
                                    : false;
                                const contentParse = options.useContentExp ? $parse(attrs[ttType]) : false;
                                const observers = [];
                                let lastPlacement;

                                const positionTooltip = function () {
                                    // check if tooltip exists and is not empty
                                    if (!tooltip || !tooltip.html()) {
                                        return;
                                    }

                                    if (!positionTimeout) {
                                        positionTimeout = $timeout(
                                            function () {
                                                const ttPosition = $position.positionElements(
                                                    element,
                                                    tooltip,
                                                    ttScope.placement,
                                                    appendToBody,
                                                );
                                                const initialHeight = angular.isDefined(tooltip.offsetHeight)
                                                    ? tooltip.offsetHeight
                                                    : tooltip.prop('offsetHeight');
                                                const elementPos = appendToBody
                                                    ? $position.offset(element)
                                                    : $position.position(element);
                                                tooltip.css({
                                                    top: `${ttPosition.top}px`,
                                                    left: `${ttPosition.left}px`,
                                                });
                                                const placementClasses = ttPosition.placement.split('-');

                                                if (!tooltip.hasClass(placementClasses[0])) {
                                                    tooltip.removeClass(lastPlacement.split('-')[0]);
                                                    tooltip.addClass(placementClasses[0]);
                                                }

                                                if (
                                                    !tooltip.hasClass(
                                                        options.placementClassPrefix + ttPosition.placement,
                                                    )
                                                ) {
                                                    tooltip.removeClass(options.placementClassPrefix + lastPlacement);
                                                    tooltip.addClass(
                                                        options.placementClassPrefix + ttPosition.placement,
                                                    );
                                                }

                                                adjustmentTimeout = $timeout(
                                                    function () {
                                                        const currentHeight = angular.isDefined(tooltip.offsetHeight)
                                                            ? tooltip.offsetHeight
                                                            : tooltip.prop('offsetHeight');
                                                        const adjustment = $position.adjustTop(
                                                            placementClasses,
                                                            elementPos,
                                                            initialHeight,
                                                            currentHeight,
                                                        );
                                                        if (adjustment) {
                                                            tooltip.css(adjustment);
                                                        }
                                                        adjustmentTimeout = null;
                                                    },
                                                    0,
                                                    false,
                                                );

                                                // first time through tt element will have the
                                                // uib-position-measure class or if the placement
                                                // has changed we need to position the arrow.
                                                if (tooltip.hasClass('uib-position-measure')) {
                                                    $position.positionArrow(tooltip, ttPosition.placement);
                                                    tooltip.removeClass('uib-position-measure');
                                                } else if (lastPlacement !== ttPosition.placement) {
                                                    $position.positionArrow(tooltip, ttPosition.placement);
                                                }
                                                lastPlacement = ttPosition.placement;

                                                positionTimeout = null;
                                            },
                                            0,
                                            false,
                                        );
                                    }
                                };

                                // Set up the correct scope to allow transclusion later
                                ttScope.origScope = scope;

                                // By default, the tooltip is not open.
                                // TODO add ability to start tooltip opened
                                ttScope.isOpen = false;

                                function toggleTooltipBind() {
                                    if (!ttScope.isOpen) {
                                        showTooltipBind();
                                    } else {
                                        hideTooltipBind();
                                    }
                                }

                                // Show the tooltip with delay if specified, otherwise show it immediately
                                function showTooltipBind() {
                                    if (hasEnableExp && !scope.$eval(attrs[`${prefix}Enable`])) {
                                        return;
                                    }

                                    cancelHide();
                                    prepareTooltip();

                                    if (ttScope.popupDelay) {
                                        // Do nothing if the tooltip was already scheduled to pop-up.
                                        // This happens if show is triggered multiple times before any hide is triggered.
                                        if (!showTimeout) {
                                            showTimeout = $timeout(show, ttScope.popupDelay, false);
                                        }
                                    } else {
                                        show();
                                    }
                                }

                                function hideTooltipBind() {
                                    cancelShow();

                                    if (ttScope.popupCloseDelay) {
                                        if (!hideTimeout) {
                                            hideTimeout = $timeout(hide, ttScope.popupCloseDelay, false);
                                        }
                                    } else {
                                        hide();
                                    }
                                }

                                // Show the tooltip popup element.
                                function show() {
                                    cancelShow();
                                    cancelHide();

                                    // Don't show empty tooltips.
                                    if (!ttScope.content) {
                                        return angular.noop;
                                    }

                                    createTooltip();

                                    // And show the tooltip.
                                    ttScope.$evalAsync(function () {
                                        ttScope.isOpen = true;
                                        assignIsOpen(true);
                                        positionTooltip();
                                    });
                                }

                                function cancelShow() {
                                    if (showTimeout) {
                                        $timeout.cancel(showTimeout);
                                        showTimeout = null;
                                    }

                                    if (positionTimeout) {
                                        $timeout.cancel(positionTimeout);
                                        positionTimeout = null;
                                    }
                                }

                                // Hide the tooltip popup element.
                                function hide() {
                                    if (!ttScope) {
                                        return;
                                    }

                                    // First things first: we don't show it anymore.
                                    ttScope.$evalAsync(function () {
                                        if (ttScope) {
                                            ttScope.isOpen = false;
                                            assignIsOpen(false);
                                            // And now we remove it from the DOM. However, if we have animation, we
                                            // need to wait for it to expire beforehand.
                                            // FIXME: this is a placeholder for a port of the transitions library.
                                            // The fade transition in TWBS is 150ms.
                                            if (ttScope.animation) {
                                                if (!transitionTimeout) {
                                                    transitionTimeout = $timeout(removeTooltip, 150, false);
                                                }
                                            } else {
                                                removeTooltip();
                                            }
                                        }
                                    });
                                }

                                function cancelHide() {
                                    if (hideTimeout) {
                                        $timeout.cancel(hideTimeout);
                                        hideTimeout = null;
                                    }

                                    if (transitionTimeout) {
                                        $timeout.cancel(transitionTimeout);
                                        transitionTimeout = null;
                                    }
                                }

                                function createTooltip() {
                                    // There can only be one tooltip element per directive shown at once.
                                    if (tooltip) {
                                        return;
                                    }

                                    tooltipLinkedScope = ttScope.$new();
                                    tooltip = tooltipLinker(tooltipLinkedScope, function (tooltip) {
                                        if (appendToBody) {
                                            $document.find('body').append(tooltip);
                                        } else {
                                            element.after(tooltip);
                                        }
                                    });

                                    openedTooltips.add(ttScope, {
                                        close: hide,
                                    });

                                    prepObservers();
                                }

                                function removeTooltip() {
                                    cancelShow();
                                    cancelHide();
                                    unregisterObservers();

                                    if (tooltip) {
                                        tooltip.remove();

                                        tooltip = null;
                                        if (adjustmentTimeout) {
                                            $timeout.cancel(adjustmentTimeout);
                                        }
                                    }

                                    openedTooltips.remove(ttScope);

                                    if (tooltipLinkedScope) {
                                        tooltipLinkedScope.$destroy();
                                        tooltipLinkedScope = null;
                                    }
                                }

                                /**
                                 * Set the initial scope values. Once
                                 * the tooltip is created, the observers
                                 * will be added to keep things in sync.
                                 */
                                function prepareTooltip() {
                                    ttScope.title = attrs[`${prefix}Title`];
                                    if (contentParse) {
                                        ttScope.content = contentParse(scope);
                                    } else {
                                        ttScope.content = attrs[ttType];
                                    }

                                    ttScope.popupClass = attrs[`${prefix}Class`];
                                    ttScope.placement = angular.isDefined(attrs[`${prefix}Placement`])
                                        ? attrs[`${prefix}Placement`]
                                        : options.placement;
                                    const placement = $position.parsePlacement(ttScope.placement);
                                    lastPlacement = placement[1] ? `${placement[0]}-${placement[1]}` : placement[0];

                                    const delay = parseInt(attrs[`${prefix}PopupDelay`], 10);
                                    const closeDelay = parseInt(attrs[`${prefix}PopupCloseDelay`], 10);
                                    ttScope.popupDelay = !isNaN(delay) ? delay : options.popupDelay;
                                    ttScope.popupCloseDelay = !isNaN(closeDelay) ? closeDelay : options.popupCloseDelay;
                                }

                                function assignIsOpen(isOpen) {
                                    if (isOpenParse && angular.isFunction(isOpenParse.assign)) {
                                        isOpenParse.assign(scope, isOpen);
                                    }
                                }

                                ttScope.contentExp = function () {
                                    return ttScope.content;
                                };

                                /**
                                 * Observe the relevant attributes.
                                 */
                                attrs.$observe('disabled', function (val) {
                                    if (val) {
                                        cancelShow();
                                    }

                                    if (val && ttScope.isOpen) {
                                        hide();
                                    }
                                });

                                if (isOpenParse) {
                                    scope.$watch(isOpenParse, function (val) {
                                        if (ttScope && !val === ttScope.isOpen) {
                                            toggleTooltipBind();
                                        }
                                    });
                                }

                                function prepObservers() {
                                    observers.length = 0;

                                    if (contentParse) {
                                        observers.push(
                                            scope.$watch(contentParse, function (val) {
                                                ttScope.content = val;
                                                if (!val && ttScope.isOpen) {
                                                    hide();
                                                }
                                            }),
                                        );

                                        observers.push(
                                            tooltipLinkedScope.$watch(function () {
                                                if (!repositionScheduled) {
                                                    repositionScheduled = true;
                                                    tooltipLinkedScope.$$postDigest(function () {
                                                        repositionScheduled = false;
                                                        if (ttScope && ttScope.isOpen) {
                                                            positionTooltip();
                                                        }
                                                    });
                                                }
                                            }),
                                        );
                                    } else {
                                        observers.push(
                                            attrs.$observe(ttType, function (val) {
                                                ttScope.content = val;
                                                if (!val && ttScope.isOpen) {
                                                    hide();
                                                } else {
                                                    positionTooltip();
                                                }
                                            }),
                                        );
                                    }

                                    observers.push(
                                        attrs.$observe(`${prefix}Title`, function (val) {
                                            ttScope.title = val;
                                            if (ttScope.isOpen) {
                                                positionTooltip();
                                            }
                                        }),
                                    );

                                    observers.push(
                                        attrs.$observe(`${prefix}Placement`, function (val) {
                                            ttScope.placement = val || options.placement;
                                            if (ttScope.isOpen) {
                                                positionTooltip();
                                            }
                                        }),
                                    );
                                }

                                function unregisterObservers() {
                                    if (observers.length) {
                                        angular.forEach(observers, function (observer) {
                                            observer();
                                        });
                                        observers.length = 0;
                                    }
                                }

                                // hide tooltips/popovers for outsideClick trigger
                                function bodyHideTooltipBind(e) {
                                    if (!ttScope || !ttScope.isOpen || !tooltip) {
                                        return;
                                    }
                                    // make sure the tooltip/popover link or tool tooltip/popover itself were not clicked
                                    if (!element[0].contains(e.target) && !tooltip[0].contains(e.target)) {
                                        hideTooltipBind();
                                    }
                                }

                                // KeyboardEvent handler to hide the tooltip on Escape key press
                                function hideOnEscapeKey(e) {
                                    if (e.which === 27) {
                                        hideTooltipBind();
                                    }
                                }

                                const unregisterTriggers = function () {
                                    triggers.show.forEach(function (trigger) {
                                        if (trigger === 'outsideClick') {
                                            element.off('click', toggleTooltipBind);
                                        } else {
                                            element.off(trigger, showTooltipBind);
                                            element.off(trigger, toggleTooltipBind);
                                        }
                                        element.off('keypress', hideOnEscapeKey);
                                    });
                                    triggers.hide.forEach(function (trigger) {
                                        if (trigger === 'outsideClick') {
                                            $document.off('click', bodyHideTooltipBind);
                                        } else {
                                            element.off(trigger, hideTooltipBind);
                                        }
                                    });
                                };

                                function prepTriggers() {
                                    const showTriggers = [];
                                    const hideTriggers = [];
                                    const val = scope.$eval(attrs[`${prefix}Trigger`]);
                                    unregisterTriggers();

                                    if (angular.isObject(val)) {
                                        Object.keys(val).forEach(function (key) {
                                            showTriggers.push(key);
                                            hideTriggers.push(val[key]);
                                        });
                                        triggers = {
                                            show: showTriggers,
                                            hide: hideTriggers,
                                        };
                                    } else {
                                        triggers = getTriggers(val);
                                    }

                                    if (triggers.show !== 'none') {
                                        triggers.show.forEach(function (trigger, idx) {
                                            if (trigger === 'outsideClick') {
                                                element.on('click', toggleTooltipBind);
                                                $document.on('click', bodyHideTooltipBind);
                                            } else if (trigger === triggers.hide[idx]) {
                                                element.on(trigger, toggleTooltipBind);
                                            } else if (trigger) {
                                                element.on(trigger, showTooltipBind);
                                                element.on(triggers.hide[idx], hideTooltipBind);
                                            }
                                            element.on('keypress', hideOnEscapeKey);
                                        });
                                    }
                                }

                                prepTriggers();

                                const animation = scope.$eval(attrs[`${prefix}Animation`]);
                                ttScope.animation = angular.isDefined(animation) ? !!animation : options.animation;

                                let appendToBodyVal;
                                const appendKey = `${prefix}AppendToBody`;
                                if (appendKey in attrs && attrs[appendKey] === undefined) {
                                    appendToBodyVal = true;
                                } else {
                                    appendToBodyVal = scope.$eval(attrs[appendKey]);
                                }

                                appendToBody = angular.isDefined(appendToBodyVal) ? appendToBodyVal : appendToBody;

                                // Make sure tooltip is destroyed and removed.
                                scope.$on('$destroy', function onDestroyTooltip() {
                                    unregisterTriggers();
                                    removeTooltip();
                                    ttScope = null;
                                });
                            };
                        },
                    };
                };
            },
        ];
    })

    // This is mostly ngInclude code but with a custom scope
    .directive('uibTooltipTemplateTransclude', [
        '$animate',
        '$sce',
        '$compile',
        '$templateRequest',
        function ($animate, $sce, $compile, $templateRequest) {
            return {
                link(scope, elem, attrs) {
                    const origScope = scope.$eval(attrs.tooltipTemplateTranscludeScope);

                    let changeCounter = 0;
                    let currentScope;
                    let previousElement;
                    let currentElement;

                    const cleanupLastIncludeContent = function () {
                        if (previousElement) {
                            previousElement.remove();
                            previousElement = null;
                        }

                        if (currentScope) {
                            currentScope.$destroy();
                            currentScope = null;
                        }

                        if (currentElement) {
                            $animate.leave(currentElement).then(function () {
                                previousElement = null;
                            });
                            previousElement = currentElement;
                            currentElement = null;
                        }
                    };

                    scope.$watch($sce.parseAsResourceUrl(attrs.uibTooltipTemplateTransclude), function (src) {
                        const thisChangeId = ++changeCounter;

                        if (src) {
                            // set the 2nd param to true to ignore the template request error so that the inner
                            // contents and scope can be cleaned up.
                            $templateRequest(src, true).then(
                                function (response) {
                                    if (thisChangeId !== changeCounter) {
                                        return;
                                    }
                                    const newScope = origScope.$new();
                                    const template = response;

                                    const clone = $compile(template)(newScope, function (clone) {
                                        cleanupLastIncludeContent();
                                        $animate.enter(clone, elem);
                                    });

                                    currentScope = newScope;
                                    currentElement = clone;

                                    currentScope.$emit('$includeContentLoaded', src);
                                },
                                function () {
                                    if (thisChangeId === changeCounter) {
                                        cleanupLastIncludeContent();
                                        scope.$emit('$includeContentError', src);
                                    }
                                },
                            );
                            scope.$emit('$includeContentRequested', src);
                        } else {
                            cleanupLastIncludeContent();
                        }
                    });

                    scope.$on('$destroy', cleanupLastIncludeContent);
                },
            };
        },
    ])

    /**
     * Note that it's intentional that these classes are *not* applied through $animate.
     * They must not be animated as they're expected to be present on the tooltip on
     * initialization.
     */
    .directive('uibTooltipClasses', [
        '$uibPosition',
        function ($uibPosition) {
            return {
                restrict: 'A',
                link(scope, element, attrs) {
                    // need to set the primary position so the
                    // arrow has space during position measure.
                    // tooltip.positionTooltip()
                    if (scope.placement) {
                        // // There are no top-left etc... classes
                        // // in TWBS, so we need the primary position.
                        const position = $uibPosition.parsePlacement(scope.placement);
                        element.addClass(position[0]);
                    }

                    if (scope.popupClass) {
                        element.addClass(scope.popupClass);
                    }

                    if (scope.animation) {
                        element.addClass(attrs.tooltipAnimationClass);
                    }
                },
            };
        },
    ])

    .directive('uibTooltipPopup', function () {
        return {
            restrict: 'A',
            scope: { content: '@' },
            templateUrl: 'uib/template/tooltip/tooltip-popup.html',
        };
    })

    .directive('uibTooltip', [
        '$uibTooltip',
        function ($uibTooltip) {
            return $uibTooltip('uibTooltip', 'tooltip', 'mouseenter');
        },
    ])

    .directive('uibTooltipTemplatePopup', function () {
        return {
            restrict: 'A',
            scope: { contentExp: '&', originScope: '&' },
            templateUrl: 'uib/template/tooltip/tooltip-template-popup.html',
        };
    })

    .directive('uibTooltipTemplate', [
        '$uibTooltip',
        function ($uibTooltip) {
            return $uibTooltip('uibTooltipTemplate', 'tooltip', 'mouseenter', {
                useContentExp: true,
            });
        },
    ])

    .directive('uibTooltipHtmlPopup', function () {
        return {
            restrict: 'A',
            scope: { contentExp: '&' },
            templateUrl: 'uib/template/tooltip/tooltip-html-popup.html',
        };
    })

    .directive('uibTooltipHtml', [
        '$uibTooltip',
        function ($uibTooltip) {
            return $uibTooltip('uibTooltipHtml', 'tooltip', 'mouseenter', {
                useContentExp: true,
            });
        },
    ]);

angular
    .module('ui.bootstrap.position', [])

    /**
     * A set of utility methods for working with the DOM.
     * It is meant to be used where we need to absolute-position elements in
     * relation to another element (this is the case for tooltips, popovers,
     * typeahead suggestions etc.).
     */
    .factory('$uibPosition', [
        '$document',
        '$window',
        function ($document, $window) {
            /**
             * Used by scrollbarWidth() function to cache scrollbar's width.
             * Do not access this variable directly, use scrollbarWidth() instead.
             */
            let SCROLLBAR_WIDTH;
            /**
             * scrollbar on body and html element in IE and Edge overlay
             * content and should be considered 0 width.
             */
            let BODY_SCROLLBAR_WIDTH;
            const OVERFLOW_REGEX = {
                normal: /(auto|scroll)/,
                hidden: /(auto|scroll|hidden)/,
            };
            const PLACEMENT_REGEX = {
                auto: /\s?auto?\s?/i,
                primary: /^(top|bottom|left|right)$/,
                secondary: /^(top|bottom|left|right|center)$/,
                vertical: /^(top|bottom)$/,
            };
            const BODY_REGEX = /(HTML|BODY)/;

            return {
                /**
                 * Provides a raw DOM element from a jQuery/jQLite element.
                 *
                 * @param {element} elem - The element to convert.
                 *
                 * @returns {element} A HTML element.
                 */
                getRawNode(elem) {
                    return elem.nodeName ? elem : elem[0] || elem;
                },

                /**
                 * Provides a parsed number for a style property.  Strips
                 * units and casts invalid numbers to 0.
                 *
                 * @param {string} value - The style value to parse.
                 *
                 * @returns {number} A valid number.
                 */
                parseStyle(value) {
                    value = parseFloat(value);
                    return isFinite(value) ? value : 0;
                },

                /**
                 * Provides the closest positioned ancestor.
                 *
                 * @param {element} element - The element to get the offest parent for.
                 *
                 * @returns {element} The closest positioned ancestor.
                 */
                offsetParent(elem) {
                    elem = this.getRawNode(elem);

                    let offsetParent = elem.offsetParent || $document[0].documentElement;

                    function isStaticPositioned(el) {
                        return ($window.getComputedStyle(el).position || 'static') === 'static';
                    }

                    while (
                        offsetParent &&
                        offsetParent !== $document[0].documentElement &&
                        isStaticPositioned(offsetParent)
                    ) {
                        offsetParent = offsetParent.offsetParent;
                    }

                    return offsetParent || $document[0].documentElement;
                },

                /**
                 * Provides the scrollbar width, concept from TWBS measureScrollbar()
                 * function in https://github.com/twbs/bootstrap/blob/master/js/modal.js
                 * In IE and Edge, scollbar on body and html element overlay and should
                 * return a width of 0.
                 *
                 * @returns {number} The width of the browser scollbar.
                 */
                scrollbarWidth(isBody) {
                    if (isBody) {
                        if (angular.isUndefined(BODY_SCROLLBAR_WIDTH)) {
                            const bodyElem = $document.find('body');
                            bodyElem.addClass('uib-position-body-scrollbar-measure');
                            BODY_SCROLLBAR_WIDTH = $window.innerWidth - bodyElem[0].clientWidth;
                            BODY_SCROLLBAR_WIDTH = isFinite(BODY_SCROLLBAR_WIDTH) ? BODY_SCROLLBAR_WIDTH : 0;
                            bodyElem.removeClass('uib-position-body-scrollbar-measure');
                        }
                        return BODY_SCROLLBAR_WIDTH;
                    }

                    if (angular.isUndefined(SCROLLBAR_WIDTH)) {
                        const scrollElem = angular.element('<div class="uib-position-scrollbar-measure"></div>');
                        $document.find('body').append(scrollElem);
                        SCROLLBAR_WIDTH = scrollElem[0].offsetWidth - scrollElem[0].clientWidth;
                        SCROLLBAR_WIDTH = isFinite(SCROLLBAR_WIDTH) ? SCROLLBAR_WIDTH : 0;
                        scrollElem.remove();
                    }

                    return SCROLLBAR_WIDTH;
                },

                /**
                 * Provides the padding required on an element to replace the scrollbar.
                 *
                 * @returns {object} An object with the following properties:
                 *   <ul>
                 *     <li>**scrollbarWidth**: the width of the scrollbar</li>
                 *     <li>**widthOverflow**: whether the the width is overflowing</li>
                 *     <li>**right**: the amount of right padding on the element needed to replace the scrollbar</li>
                 *     <li>**rightOriginal**: the amount of right padding currently on the element</li>
                 *     <li>**heightOverflow**: whether the the height is overflowing</li>
                 *     <li>**bottom**: the amount of bottom padding on the element needed to replace the scrollbar</li>
                 *     <li>**bottomOriginal**: the amount of bottom padding currently on the element</li>
                 *   </ul>
                 */
                scrollbarPadding(elem) {
                    elem = this.getRawNode(elem);

                    const elemStyle = $window.getComputedStyle(elem);
                    const paddingRight = this.parseStyle(elemStyle.paddingRight);
                    const paddingBottom = this.parseStyle(elemStyle.paddingBottom);
                    const scrollParent = this.scrollParent(elem, false, true);
                    const scrollbarWidth = this.scrollbarWidth(BODY_REGEX.test(scrollParent.tagName));

                    return {
                        scrollbarWidth,
                        widthOverflow: scrollParent.scrollWidth > scrollParent.clientWidth,
                        right: paddingRight + scrollbarWidth,
                        originalRight: paddingRight,
                        heightOverflow: scrollParent.scrollHeight > scrollParent.clientHeight,
                        bottom: paddingBottom + scrollbarWidth,
                        originalBottom: paddingBottom,
                    };
                },

                /**
                 * Checks to see if the element is scrollable.
                 *
                 * @param {element} elem - The element to check.
                 * @param {boolean=} [includeHidden=false] - Should scroll style of 'hidden' be considered,
                 *   default is false.
                 *
                 * @returns {boolean} Whether the element is scrollable.
                 */
                isScrollable(elem, includeHidden) {
                    elem = this.getRawNode(elem);

                    const overflowRegex = includeHidden ? OVERFLOW_REGEX.hidden : OVERFLOW_REGEX.normal;
                    const elemStyle = $window.getComputedStyle(elem);
                    return overflowRegex.test(elemStyle.overflow + elemStyle.overflowY + elemStyle.overflowX);
                },

                /**
                 * Provides the closest scrollable ancestor.
                 * A port of the jQuery UI scrollParent method:
                 * https://github.com/jquery/jquery-ui/blob/master/ui/scroll-parent.js
                 *
                 * @param {element} elem - The element to find the scroll parent of.
                 * @param {boolean=} [includeHidden=false] - Should scroll style of 'hidden' be considered,
                 *   default is false.
                 * @param {boolean=} [includeSelf=false] - Should the element being passed be
                 * included in the scrollable llokup.
                 *
                 * @returns {element} A HTML element.
                 */
                scrollParent(elem, includeHidden, includeSelf) {
                    elem = this.getRawNode(elem);

                    const overflowRegex = includeHidden ? OVERFLOW_REGEX.hidden : OVERFLOW_REGEX.normal;
                    const documentEl = $document[0].documentElement;
                    const elemStyle = $window.getComputedStyle(elem);
                    if (
                        includeSelf &&
                        overflowRegex.test(elemStyle.overflow + elemStyle.overflowY + elemStyle.overflowX)
                    ) {
                        return elem;
                    }
                    let excludeStatic = elemStyle.position === 'absolute';
                    let scrollParent = elem.parentElement || documentEl;

                    if (scrollParent === documentEl || elemStyle.position === 'fixed') {
                        return documentEl;
                    }

                    while (scrollParent.parentElement && scrollParent !== documentEl) {
                        const spStyle = $window.getComputedStyle(scrollParent);
                        if (excludeStatic && spStyle.position !== 'static') {
                            excludeStatic = false;
                        }

                        if (
                            !excludeStatic &&
                            overflowRegex.test(spStyle.overflow + spStyle.overflowY + spStyle.overflowX)
                        ) {
                            break;
                        }
                        scrollParent = scrollParent.parentElement;
                    }

                    return scrollParent;
                },

                /**
                 * Provides read-only equivalent of jQuery's position function:
                 * http://api.jquery.com/position/ - distance to closest positioned
                 * ancestor.  Does not account for margins by default like jQuery position.
                 *
                 * @param {element} elem - The element to caclulate the position on.
                 * @param {boolean=} [includeMargins=false] - Should margins be accounted
                 * for, default is false.
                 *
                 * @returns {object} An object with the following properties:
                 *   <ul>
                 *     <li>**width**: the width of the element</li>
                 *     <li>**height**: the height of the element</li>
                 *     <li>**top**: distance to top edge of offset parent</li>
                 *     <li>**left**: distance to left edge of offset parent</li>
                 *   </ul>
                 */
                position(elem, includeMagins) {
                    elem = this.getRawNode(elem);

                    const elemOffset = this.offset(elem);
                    if (includeMagins) {
                        const elemStyle = $window.getComputedStyle(elem);
                        elemOffset.top -= this.parseStyle(elemStyle.marginTop);
                        elemOffset.left -= this.parseStyle(elemStyle.marginLeft);
                    }
                    const parent = this.offsetParent(elem);
                    let parentOffset = { top: 0, left: 0 };

                    if (parent !== $document[0].documentElement) {
                        parentOffset = this.offset(parent);
                        parentOffset.top += parent.clientTop - parent.scrollTop;
                        parentOffset.left += parent.clientLeft - parent.scrollLeft;
                    }

                    return {
                        width: Math.round(angular.isNumber(elemOffset.width) ? elemOffset.width : elem.offsetWidth),
                        height: Math.round(angular.isNumber(elemOffset.height) ? elemOffset.height : elem.offsetHeight),
                        top: Math.round(elemOffset.top - parentOffset.top),
                        left: Math.round(elemOffset.left - parentOffset.left),
                    };
                },

                /**
                 * Provides read-only equivalent of jQuery's offset function:
                 * http://api.jquery.com/offset/ - distance to viewport.  Does
                 * not account for borders, margins, or padding on the body
                 * element.
                 *
                 * @param {element} elem - The element to calculate the offset on.
                 *
                 * @returns {object} An object with the following properties:
                 *   <ul>
                 *     <li>**width**: the width of the element</li>
                 *     <li>**height**: the height of the element</li>
                 *     <li>**top**: distance to top edge of viewport</li>
                 *     <li>**right**: distance to bottom edge of viewport</li>
                 *   </ul>
                 */
                offset(elem) {
                    elem = this.getRawNode(elem);

                    const elemBCR = elem.getBoundingClientRect();
                    return {
                        width: Math.round(angular.isNumber(elemBCR.width) ? elemBCR.width : elem.offsetWidth),
                        height: Math.round(angular.isNumber(elemBCR.height) ? elemBCR.height : elem.offsetHeight),
                        top: Math.round(elemBCR.top + ($window.pageYOffset || $document[0].documentElement.scrollTop)),
                        left: Math.round(
                            elemBCR.left + ($window.pageXOffset || $document[0].documentElement.scrollLeft),
                        ),
                    };
                },

                /**
                 * Provides offset distance to the closest scrollable ancestor
                 * or viewport.  Accounts for border and scrollbar width.
                 *
                 * Right and bottom dimensions represent the distance to the
                 * respective edge of the viewport element.  If the element
                 * edge extends beyond the viewport, a negative value will be
                 * reported.
                 *
                 * @param {element} elem - The element to get the viewport offset for.
                 * @param {boolean=} [useDocument=false] - Should the viewport be the document element instead
                 * of the first scrollable element, default is false.
                 * @param {boolean=} [includePadding=true] - Should the padding on the offset parent element
                 * be accounted for, default is true.
                 *
                 * @returns {object} An object with the following properties:
                 *   <ul>
                 *     <li>**top**: distance to the top content edge of viewport element</li>
                 *     <li>**bottom**: distance to the bottom content edge of viewport element</li>
                 *     <li>**left**: distance to the left content edge of viewport element</li>
                 *     <li>**right**: distance to the right content edge of viewport element</li>
                 *   </ul>
                 */
                viewportOffset(elem, useDocument, includePadding) {
                    elem = this.getRawNode(elem);
                    includePadding = includePadding !== false;

                    const elemBCR = elem.getBoundingClientRect();
                    const offsetBCR = { top: 0, left: 0, bottom: 0, right: 0 };

                    const offsetParent = useDocument ? $document[0].documentElement : this.scrollParent(elem);
                    const offsetParentBCR = offsetParent.getBoundingClientRect();

                    offsetBCR.top = offsetParentBCR.top + offsetParent.clientTop;
                    offsetBCR.left = offsetParentBCR.left + offsetParent.clientLeft;
                    if (offsetParent === $document[0].documentElement) {
                        offsetBCR.top += $window.pageYOffset;
                        offsetBCR.left += $window.pageXOffset;
                    }
                    offsetBCR.bottom = offsetBCR.top + offsetParent.clientHeight;
                    offsetBCR.right = offsetBCR.left + offsetParent.clientWidth;

                    if (includePadding) {
                        const offsetParentStyle = $window.getComputedStyle(offsetParent);
                        offsetBCR.top += this.parseStyle(offsetParentStyle.paddingTop);
                        offsetBCR.bottom -= this.parseStyle(offsetParentStyle.paddingBottom);
                        offsetBCR.left += this.parseStyle(offsetParentStyle.paddingLeft);
                        offsetBCR.right -= this.parseStyle(offsetParentStyle.paddingRight);
                    }

                    return {
                        top: Math.round(elemBCR.top - offsetBCR.top),
                        bottom: Math.round(offsetBCR.bottom - elemBCR.bottom),
                        left: Math.round(elemBCR.left - offsetBCR.left),
                        right: Math.round(offsetBCR.right - elemBCR.right),
                    };
                },

                /**
                 * Provides an array of placement values parsed from a placement string.
                 * Along with the 'auto' indicator, supported placement strings are:
                 *   <ul>
                 *     <li>top: element on top, horizontally centered on host element.</li>
                 *     <li>top-left: element on top, left edge aligned with host element left edge.</li>
                 *     <li>top-right: element on top, lerightft edge aligned with host element right edge.</li>
                 *     <li>bottom: element on bottom, horizontally centered on host element.</li>
                 *     <li>bottom-left: element on bottom, left edge aligned with host element left edge.</li>
                 *     <li>bottom-right: element on bottom, right edge aligned with host element right edge.</li>
                 *     <li>left: element on left, vertically centered on host element.</li>
                 *     <li>left-top: element on left, top edge aligned with host element top edge.</li>
                 *     <li>left-bottom: element on left, bottom edge aligned with host element bottom edge.</li>
                 *     <li>right: element on right, vertically centered on host element.</li>
                 *     <li>right-top: element on right, top edge aligned with host element top edge.</li>
                 *     <li>right-bottom: element on right, bottom edge aligned with host element bottom edge.</li>
                 *   </ul>
                 * A placement string with an 'auto' indicator is expected to be
                 * space separated from the placement, i.e: 'auto bottom-left'  If
                 * the primary and secondary placement values do not match 'top,
                 * bottom, left, right' then 'top' will be the primary placement and
                 * 'center' will be the secondary placement.  If 'auto' is passed, true
                 * will be returned as the 3rd value of the array.
                 *
                 * @param {string} placement - The placement string to parse.
                 *
                 * @returns {array} An array with the following values
                 * <ul>
                 *   <li>**[0]**: The primary placement.</li>
                 *   <li>**[1]**: The secondary placement.</li>
                 *   <li>**[2]**: If auto is passed: true, else undefined.</li>
                 * </ul>
                 */
                parsePlacement(placement) {
                    const autoPlace = PLACEMENT_REGEX.auto.test(placement);
                    if (autoPlace) {
                        placement = placement.replace(PLACEMENT_REGEX.auto, '');
                    }

                    placement = placement.split('-');

                    placement[0] = placement[0] || 'top';
                    if (!PLACEMENT_REGEX.primary.test(placement[0])) {
                        placement[0] = 'top';
                    }

                    placement[1] = placement[1] || 'center';
                    if (!PLACEMENT_REGEX.secondary.test(placement[1])) {
                        placement[1] = 'center';
                    }

                    if (autoPlace) {
                        placement[2] = true;
                    } else {
                        placement[2] = false;
                    }

                    return placement;
                },

                /**
                 * Provides coordinates for an element to be positioned relative to
                 * another element.  Passing 'auto' as part of the placement parameter
                 * will enable smart placement - where the element fits. i.e:
                 * 'auto left-top' will check to see if there is enough space to the left
                 * of the hostElem to fit the targetElem, if not place right (same for secondary
                 * top placement).  Available space is calculated using the viewportOffset
                 * function.
                 *
                 * @param {element} hostElem - The element to position against.
                 * @param {element} targetElem - The element to position.
                 * @param {string=} [placement=top] - The placement for the targetElem,
                 *   default is 'top'. 'center' is assumed as secondary placement for
                 *   'top', 'left', 'right', and 'bottom' placements.  Available placements are:
                 *   <ul>
                 *     <li>top</li>
                 *     <li>top-right</li>
                 *     <li>top-left</li>
                 *     <li>bottom</li>
                 *     <li>bottom-left</li>
                 *     <li>bottom-right</li>
                 *     <li>left</li>
                 *     <li>left-top</li>
                 *     <li>left-bottom</li>
                 *     <li>right</li>
                 *     <li>right-top</li>
                 *     <li>right-bottom</li>
                 *   </ul>
                 * @param {boolean=} [appendToBody=false] - Should the top and left values returned
                 *   be calculated from the body element, default is false.
                 *
                 * @returns {object} An object with the following properties:
                 *   <ul>
                 *     <li>**top**: Value for targetElem top.</li>
                 *     <li>**left**: Value for targetElem left.</li>
                 *     <li>**placement**: The resolved placement.</li>
                 *   </ul>
                 */
                positionElements(hostElem, targetElem, placement, appendToBody) {
                    hostElem = this.getRawNode(hostElem);
                    targetElem = this.getRawNode(targetElem);

                    // need to read from prop to support tests.
                    const targetWidth = angular.isDefined(targetElem.offsetWidth)
                        ? targetElem.offsetWidth
                        : targetElem.prop('offsetWidth');
                    const targetHeight = angular.isDefined(targetElem.offsetHeight)
                        ? targetElem.offsetHeight
                        : targetElem.prop('offsetHeight');

                    placement = this.parsePlacement(placement);

                    const hostElemPos = appendToBody ? this.offset(hostElem) : this.position(hostElem);
                    const targetElemPos = { top: 0, left: 0, placement: '' };

                    if (placement[2]) {
                        const viewportOffset = this.viewportOffset(hostElem, appendToBody);

                        const targetElemStyle = $window.getComputedStyle(targetElem);
                        const adjustedSize = {
                            width:
                                targetWidth +
                                Math.round(
                                    Math.abs(
                                        this.parseStyle(targetElemStyle.marginLeft) +
                                            this.parseStyle(targetElemStyle.marginRight),
                                    ),
                                ),
                            height:
                                targetHeight +
                                Math.round(
                                    Math.abs(
                                        this.parseStyle(targetElemStyle.marginTop) +
                                            this.parseStyle(targetElemStyle.marginBottom),
                                    ),
                                ),
                        };

                        placement[0] =
                            placement[0] === 'top' &&
                            adjustedSize.height > viewportOffset.top &&
                            adjustedSize.height <= viewportOffset.bottom
                                ? 'bottom'
                                : placement[0] === 'bottom' &&
                                  adjustedSize.height > viewportOffset.bottom &&
                                  adjustedSize.height <= viewportOffset.top
                                ? 'top'
                                : placement[0] === 'left' &&
                                  adjustedSize.width > viewportOffset.left &&
                                  adjustedSize.width <= viewportOffset.right
                                ? 'right'
                                : placement[0] === 'right' &&
                                  adjustedSize.width > viewportOffset.right &&
                                  adjustedSize.width <= viewportOffset.left
                                ? 'left'
                                : placement[0];

                        placement[1] =
                            placement[1] === 'top' &&
                            adjustedSize.height - hostElemPos.height > viewportOffset.bottom &&
                            adjustedSize.height - hostElemPos.height <= viewportOffset.top
                                ? 'bottom'
                                : placement[1] === 'bottom' &&
                                  adjustedSize.height - hostElemPos.height > viewportOffset.top &&
                                  adjustedSize.height - hostElemPos.height <= viewportOffset.bottom
                                ? 'top'
                                : placement[1] === 'left' &&
                                  adjustedSize.width - hostElemPos.width > viewportOffset.right &&
                                  adjustedSize.width - hostElemPos.width <= viewportOffset.left
                                ? 'right'
                                : placement[1] === 'right' &&
                                  adjustedSize.width - hostElemPos.width > viewportOffset.left &&
                                  adjustedSize.width - hostElemPos.width <= viewportOffset.right
                                ? 'left'
                                : placement[1];

                        if (placement[1] === 'center') {
                            if (PLACEMENT_REGEX.vertical.test(placement[0])) {
                                const xOverflow = hostElemPos.width / 2 - targetWidth / 2;
                                if (
                                    viewportOffset.left + xOverflow < 0 &&
                                    adjustedSize.width - hostElemPos.width <= viewportOffset.right
                                ) {
                                    placement[1] = 'left';
                                } else if (
                                    viewportOffset.right + xOverflow < 0 &&
                                    adjustedSize.width - hostElemPos.width <= viewportOffset.left
                                ) {
                                    placement[1] = 'right';
                                }
                            } else {
                                const yOverflow = hostElemPos.height / 2 - adjustedSize.height / 2;
                                if (
                                    viewportOffset.top + yOverflow < 0 &&
                                    adjustedSize.height - hostElemPos.height <= viewportOffset.bottom
                                ) {
                                    placement[1] = 'top';
                                } else if (
                                    viewportOffset.bottom + yOverflow < 0 &&
                                    adjustedSize.height - hostElemPos.height <= viewportOffset.top
                                ) {
                                    placement[1] = 'bottom';
                                }
                            }
                        }
                    }

                    switch (placement[0]) {
                        case 'top':
                            targetElemPos.top = hostElemPos.top - targetHeight;
                            break;
                        case 'bottom':
                            targetElemPos.top = hostElemPos.top + hostElemPos.height;
                            break;
                        case 'left':
                            targetElemPos.left = hostElemPos.left - targetWidth;
                            break;
                        case 'right':
                            targetElemPos.left = hostElemPos.left + hostElemPos.width;
                            break;
                    }

                    switch (placement[1]) {
                        case 'top':
                            targetElemPos.top = hostElemPos.top;
                            break;
                        case 'bottom':
                            targetElemPos.top = hostElemPos.top + hostElemPos.height - targetHeight;
                            break;
                        case 'left':
                            targetElemPos.left = hostElemPos.left;
                            break;
                        case 'right':
                            targetElemPos.left = hostElemPos.left + hostElemPos.width - targetWidth;
                            break;
                        case 'center':
                            if (PLACEMENT_REGEX.vertical.test(placement[0])) {
                                targetElemPos.left = hostElemPos.left + hostElemPos.width / 2 - targetWidth / 2;
                            } else {
                                targetElemPos.top = hostElemPos.top + hostElemPos.height / 2 - targetHeight / 2;
                            }
                            break;
                    }

                    targetElemPos.top = Math.round(targetElemPos.top);
                    targetElemPos.left = Math.round(targetElemPos.left);
                    targetElemPos.placement =
                        placement[1] === 'center' ? placement[0] : `${placement[0]}-${placement[1]}`;

                    return targetElemPos;
                },

                /**
                 * Provides a way to adjust the top positioning after first
                 * render to correctly align element to top after content
                 * rendering causes resized element height
                 *
                 * @param {array} placementClasses - The array of strings of classes
                 * element should have.
                 * @param {object} containerPosition - The object with container
                 * position information
                 * @param {number} initialHeight - The initial height for the elem.
                 * @param {number} currentHeight - The current height for the elem.
                 */
                adjustTop(placementClasses, containerPosition, initialHeight, currentHeight) {
                    if (placementClasses.indexOf('top') !== -1 && initialHeight !== currentHeight) {
                        return {
                            top: `${containerPosition.top - currentHeight}px`,
                        };
                    }
                },

                /**
                 * Provides a way for positioning tooltip & dropdown
                 * arrows when using placement options beyond the standard
                 * left, right, top, or bottom.
                 *
                 * @param {element} elem - The tooltip/dropdown element.
                 * @param {string} placement - The placement for the elem.
                 */
                positionArrow(elem, placement) {
                    elem = this.getRawNode(elem);

                    const innerElem = elem.querySelector('.tooltip-inner, .popover-inner');
                    if (!innerElem) {
                        return;
                    }

                    const isTooltip = angular.element(innerElem).hasClass('tooltip-inner');

                    const arrowElem = isTooltip ? elem.querySelector('.tooltip-arrow') : elem.querySelector('.arrow');
                    if (!arrowElem) {
                        return;
                    }

                    const arrowCss = {
                        top: '',
                        bottom: '',
                        left: '',
                        right: '',
                    };

                    placement = this.parsePlacement(placement);
                    if (placement[1] === 'center') {
                        // no adjustment necessary - just reset styles
                        angular.element(arrowElem).css(arrowCss);
                        return;
                    }

                    const borderProp = `border-${placement[0]}-width`;
                    const borderWidth = $window.getComputedStyle(arrowElem)[borderProp];

                    let borderRadiusProp = 'border-';
                    if (PLACEMENT_REGEX.vertical.test(placement[0])) {
                        borderRadiusProp += `${placement[0]}-${placement[1]}`;
                    } else {
                        borderRadiusProp += `${placement[1]}-${placement[0]}`;
                    }
                    borderRadiusProp += '-radius';
                    const borderRadius = $window.getComputedStyle(isTooltip ? innerElem : elem)[borderRadiusProp];

                    switch (placement[0]) {
                        case 'top':
                            arrowCss.bottom = isTooltip ? '0' : `-${borderWidth}`;
                            break;
                        case 'bottom':
                            arrowCss.top = isTooltip ? '0' : `-${borderWidth}`;
                            break;
                        case 'left':
                            arrowCss.right = isTooltip ? '0' : `-${borderWidth}`;
                            break;
                        case 'right':
                            arrowCss.left = isTooltip ? '0' : `-${borderWidth}`;
                            break;
                    }

                    arrowCss[placement[1]] = borderRadius;

                    angular.element(arrowElem).css(arrowCss);
                },
            };
        },
    ]);

angular
    .module('ui.bootstrap.stackedMap', [])
    /**
     * A helper, internal data structure that acts as a map but also allows getting / removing
     * elements in the LIFO order
     */
    .factory('$$stackedMap', function () {
        return {
            createNew() {
                const stack = [];

                return {
                    add(key, value) {
                        stack.push({
                            key,
                            value,
                        });
                    },
                    get(key) {
                        for (let i = 0; i < stack.length; i++) {
                            if (key === stack[i].key) {
                                return stack[i];
                            }
                        }
                    },
                    keys() {
                        const keys = [];
                        for (let i = 0; i < stack.length; i++) {
                            keys.push(stack[i].key);
                        }
                        return keys;
                    },
                    top() {
                        return stack[stack.length - 1];
                    },
                    remove(key) {
                        let idx = -1;
                        for (let i = 0; i < stack.length; i++) {
                            if (key === stack[i].key) {
                                idx = i;
                                break;
                            }
                        }
                        return stack.splice(idx, 1)[0];
                    },
                    removeTop() {
                        return stack.pop();
                    },
                    length() {
                        return stack.length;
                    },
                };
            },
        };
    });
angular
    .module('ui.bootstrap.typeahead', ['ui.bootstrap.debounce', 'ui.bootstrap.position'])

    /**
     * A helper service that can parse typeahead's syntax (string provided by users)
     * Extracted to a separate service for ease of unit testing
     */
    .factory('uibTypeaheadParser', [
        '$parse',
        function ($parse) {
            //                      000001111111100000000000002222222200000000000000003333333333333330000000000044444444000
            const TYPEAHEAD_REGEXP = /^\s*([\s\S]+?)(?:\s+as\s+([\s\S]+?))?\s+for\s+(?:([\$\w][\$\w\d]*))\s+in\s+([\s\S]+?)$/;
            return {
                parse(input) {
                    const match = input.match(TYPEAHEAD_REGEXP);
                    if (!match) {
                        throw new Error(
                            `${
                                'Expected typeahead specification in form of "_modelValue_ (as _label_)? for _item_ in _collection_"' +
                                ' but got "'
                            }${input}".`,
                        );
                    }

                    return {
                        itemName: match[3],
                        source: $parse(match[4]),
                        viewMapper: $parse(match[2] || match[1]),
                        modelMapper: $parse(match[1]),
                    };
                },
            };
        },
    ])

    .controller('UibTypeaheadController', [
        '$scope',
        '$element',
        '$attrs',
        '$compile',
        '$parse',
        '$q',
        '$timeout',
        '$document',
        '$window',
        '$rootScope',
        '$$debounce',
        '$uibPosition',
        'uibTypeaheadParser',
        function (
            originalScope,
            element,
            attrs,
            $compile,
            $parse,
            $q,
            $timeout,
            $document,
            $window,
            $rootScope,
            $$debounce,
            $position,
            typeaheadParser,
        ) {
            const HOT_KEYS = [9, 13, 27, 38, 40];
            const eventDebounceTime = 200;
            let modelCtrl;
            let ngModelOptions;
            // SUPPORTED ATTRIBUTES (OPTIONS)

            // minimal no of characters that needs to be entered before typeahead kicks-in
            let minLength = originalScope.$eval(attrs.typeaheadMinLength);
            if (!minLength && minLength !== 0) {
                minLength = 1;
            }

            originalScope.$watch(attrs.typeaheadMinLength, function (newVal) {
                minLength = !newVal && newVal !== 0 ? 1 : newVal;
            });

            // minimal wait time after last character typed before typeahead kicks-in
            const waitTime = originalScope.$eval(attrs.typeaheadWaitMs) || 0;

            // should it restrict model values to the ones selected from the popup only?
            let isEditable = originalScope.$eval(attrs.typeaheadEditable) !== false;
            originalScope.$watch(attrs.typeaheadEditable, function (newVal) {
                isEditable = newVal !== false;
            });

            // binding to a variable that indicates if matches are being retrieved asynchronously
            const isLoadingSetter = $parse(attrs.typeaheadLoading).assign || angular.noop;

            // a function to determine if an event should cause selection
            const isSelectEvent = attrs.typeaheadShouldSelect
                ? $parse(attrs.typeaheadShouldSelect)
                : function (scope, vals) {
                      const evt = vals.$event;
                      return evt.which === 13 || evt.which === 9;
                  };

            // a callback executed when a match is selected
            const onSelectCallback = $parse(attrs.typeaheadOnSelect);

            // should it select highlighted popup value when losing focus?
            const isSelectOnBlur = angular.isDefined(attrs.typeaheadSelectOnBlur)
                ? originalScope.$eval(attrs.typeaheadSelectOnBlur)
                : false;

            // binding to a variable that indicates if there were no results after the query is completed
            const isNoResultsSetter = $parse(attrs.typeaheadNoResults).assign || angular.noop;

            const inputFormatter = attrs.typeaheadInputFormatter ? $parse(attrs.typeaheadInputFormatter) : undefined;

            const appendToBody = attrs.typeaheadAppendToBody ? originalScope.$eval(attrs.typeaheadAppendToBody) : false;

            const appendTo = attrs.typeaheadAppendTo ? originalScope.$eval(attrs.typeaheadAppendTo) : null;

            const focusFirst = originalScope.$eval(attrs.typeaheadFocusFirst) !== false;

            // If input matches an item of the list exactly, select it automatically
            const selectOnExact = attrs.typeaheadSelectOnExact
                ? originalScope.$eval(attrs.typeaheadSelectOnExact)
                : false;

            // binding to a variable that indicates if dropdown is open
            const isOpenSetter = $parse(attrs.typeaheadIsOpen).assign || angular.noop;

            const showHint = originalScope.$eval(attrs.typeaheadShowHint) || false;

            // INTERNAL VARIABLES

            // model setter executed upon match selection
            const parsedModel = $parse(attrs.ngModel);
            const invokeModelSetter = $parse(`${attrs.ngModel}($$$p)`);
            const $setModelValue = function (scope, newValue) {
                if (angular.isFunction(parsedModel(originalScope)) && ngModelOptions.getOption('getterSetter')) {
                    return invokeModelSetter(scope, { $$$p: newValue });
                }

                return parsedModel.assign(scope, newValue);
            };

            // expressions used by typeahead
            const parserResult = typeaheadParser.parse(attrs.uibTypeahead);

            let hasFocus;

            // Used to avoid bug in iOS webview where iOS keyboard does not fire
            // mousedown & mouseup events
            // Issue #3699
            let selected;

            // create a child scope for the typeahead directive so we are not polluting original scope
            // with typeahead-specific data (matches, query etc.)
            const scope = originalScope.$new();
            const offDestroy = originalScope.$on('$destroy', function () {
                scope.$destroy();
            });
            scope.$on('$destroy', offDestroy);

            // WAI-ARIA
            const popupId = `typeahead-${scope.$id}-${Math.floor(Math.random() * 10000)}`;
            element.attr({
                'aria-autocomplete': 'list',
                'aria-expanded': false,
                'aria-owns': popupId,
            });

            let inputsContainer;
            let hintInputElem;
            // add read-only input to show hint
            if (showHint) {
                inputsContainer = angular.element('<div></div>');
                inputsContainer.css('position', 'relative');
                element.after(inputsContainer);
                hintInputElem = element.clone();
                hintInputElem.attr('placeholder', '');
                hintInputElem.attr('tabindex', '-1');
                hintInputElem.val('');
                hintInputElem.css({
                    position: 'absolute',
                    top: '0px',
                    left: '0px',
                    'border-color': 'transparent',
                    'box-shadow': 'none',
                    opacity: 1,
                    background: 'none 0% 0% / auto repeat scroll padding-box border-box rgb(255, 255, 255)',
                    color: '#999',
                });
                element.css({
                    position: 'relative',
                    'vertical-align': 'top',
                    'background-color': 'transparent',
                });

                if (hintInputElem.attr('id')) {
                    hintInputElem.removeAttr('id'); // remove duplicate id if present.
                }
                inputsContainer.append(hintInputElem);
                hintInputElem.after(element);
            }

            // pop-up element used to display matches
            const popUpEl = angular.element('<div uib-typeahead-popup></div>');
            popUpEl.attr({
                id: popupId,
                matches: 'matches',
                active: 'activeIdx',
                select: 'select(activeIdx, evt)',
                'move-in-progress': 'moveInProgress',
                query: 'query',
                position: 'position',
                'assign-is-open': 'assignIsOpen(isOpen)',
                debounce: 'debounceUpdate',
            });
            // custom item template
            if (angular.isDefined(attrs.typeaheadTemplateUrl)) {
                popUpEl.attr('template-url', attrs.typeaheadTemplateUrl);
            }

            if (angular.isDefined(attrs.typeaheadPopupTemplateUrl)) {
                popUpEl.attr('popup-template-url', attrs.typeaheadPopupTemplateUrl);
            }

            const resetHint = function () {
                if (showHint) {
                    hintInputElem.val('');
                }
            };

            const resetMatches = function () {
                scope.matches = [];
                scope.activeIdx = -1;
                element.attr('aria-expanded', false);
                resetHint();
            };

            const getMatchId = function (index) {
                return `${popupId}-option-${index}`;
            };

            // Indicate that the specified match is the active (pre-selected) item in the list owned by this typeahead.
            // This attribute is added or removed automatically when the `activeIdx` changes.
            scope.$watch('activeIdx', function (index) {
                if (index < 0) {
                    element.removeAttr('aria-activedescendant');
                } else {
                    element.attr('aria-activedescendant', getMatchId(index));
                }
            });

            const inputIsExactMatch = function (inputValue, index) {
                if (scope.matches.length > index && inputValue) {
                    return inputValue.toUpperCase() === scope.matches[index].label.toUpperCase();
                }

                return false;
            };

            const getMatchesAsync = function (inputValue, evt) {
                const locals = { $viewValue: inputValue };
                isLoadingSetter(originalScope, true);
                isNoResultsSetter(originalScope, false);
                $q.when(parserResult.source(originalScope, locals)).then(
                    function (matches) {
                        // it might happen that several async queries were in progress if a user were typing fast
                        // but we are interested only in responses that correspond to the current view value
                        const onCurrentRequest = inputValue === modelCtrl.$viewValue;
                        if (onCurrentRequest && hasFocus) {
                            if (matches && matches.length > 0) {
                                scope.activeIdx = focusFirst ? 0 : -1;
                                isNoResultsSetter(originalScope, false);
                                scope.matches.length = 0;

                                // transform labels
                                for (let i = 0; i < matches.length; i++) {
                                    locals[parserResult.itemName] = matches[i];
                                    scope.matches.push({
                                        id: getMatchId(i),
                                        label: parserResult.viewMapper(scope, locals),
                                        model: matches[i],
                                    });
                                }

                                scope.query = inputValue;
                                // position pop-up with matches - we need to re-calculate its position each time we are opening a window
                                // with matches as a pop-up might be absolute-positioned and position of an input might have changed on a page
                                // due to other elements being rendered
                                recalculatePosition();

                                element.attr('aria-expanded', true);

                                // Select the single remaining option if user input matches
                                if (selectOnExact && scope.matches.length === 1 && inputIsExactMatch(inputValue, 0)) {
                                    if (
                                        angular.isNumber(scope.debounceUpdate) ||
                                        angular.isObject(scope.debounceUpdate)
                                    ) {
                                        $$debounce(
                                            function () {
                                                scope.select(0, evt);
                                            },
                                            angular.isNumber(scope.debounceUpdate)
                                                ? scope.debounceUpdate
                                                : scope.debounceUpdate.default,
                                        );
                                    } else {
                                        scope.select(0, evt);
                                    }
                                }

                                if (showHint) {
                                    const firstLabel = scope.matches[0].label;
                                    if (
                                        angular.isString(inputValue) &&
                                        inputValue.length > 0 &&
                                        firstLabel.slice(0, inputValue.length).toUpperCase() ===
                                            inputValue.toUpperCase()
                                    ) {
                                        hintInputElem.val(inputValue + firstLabel.slice(inputValue.length));
                                    } else {
                                        hintInputElem.val('');
                                    }
                                }
                            } else {
                                resetMatches();
                                isNoResultsSetter(originalScope, true);
                            }
                        }
                        if (onCurrentRequest) {
                            isLoadingSetter(originalScope, false);
                        }
                    },
                    function () {
                        resetMatches();
                        isLoadingSetter(originalScope, false);
                        isNoResultsSetter(originalScope, true);
                    },
                );
            };

            // bind events only if appendToBody params exist - performance feature
            if (appendToBody) {
                angular.element($window).on('resize', fireRecalculating);
                $document.find('body').on('scroll', fireRecalculating);
            }

            // Declare the debounced function outside recalculating for
            // proper debouncing
            const debouncedRecalculate = $$debounce(function () {
                // if popup is visible
                if (scope.matches.length) {
                    recalculatePosition();
                }

                scope.moveInProgress = false;
            }, eventDebounceTime);

            // Default progress type
            scope.moveInProgress = false;

            function fireRecalculating() {
                if (!scope.moveInProgress) {
                    scope.moveInProgress = true;
                    scope.$digest();
                }

                debouncedRecalculate();
            }

            // recalculate actual position and set new values to scope
            // after digest loop is popup in right position
            function recalculatePosition() {
                scope.position = appendToBody ? $position.offset(element) : $position.position(element);
                scope.position.top += element.prop('offsetHeight');
            }

            // we need to propagate user's query so we can higlight matches
            scope.query = undefined;

            // Declare the timeout promise var outside the function scope so that stacked calls can be cancelled later
            let timeoutPromise;

            const scheduleSearchWithTimeout = function (inputValue) {
                timeoutPromise = $timeout(function () {
                    getMatchesAsync(inputValue);
                }, waitTime);
            };

            const cancelPreviousTimeout = function () {
                if (timeoutPromise) {
                    $timeout.cancel(timeoutPromise);
                }
            };

            resetMatches();

            scope.assignIsOpen = function (isOpen) {
                isOpenSetter(originalScope, isOpen);
            };

            scope.select = function (activeIdx, evt) {
                // called from within the $digest() cycle
                const locals = {};
                let model;
                let item;

                selected = true;
                locals[parserResult.itemName] = item = scope.matches[activeIdx].model;
                model = parserResult.modelMapper(originalScope, locals);
                $setModelValue(originalScope, model);
                modelCtrl.$setValidity('editable', true);
                modelCtrl.$setValidity('parse', true);

                onSelectCallback(originalScope, {
                    $item: item,
                    $model: model,
                    $label: parserResult.viewMapper(originalScope, locals),
                    $event: evt,
                });

                resetMatches();

                // return focus to the input element if a match was selected via a mouse click event
                // use timeout to avoid $rootScope:inprog error
                if (scope.$eval(attrs.typeaheadFocusOnSelect) !== false) {
                    $timeout(
                        function () {
                            element[0].focus();
                        },
                        0,
                        false,
                    );
                }
            };

            // bind keyboard events: arrows up(38) / down(40), enter(13) and tab(9), esc(27)
            element.on('keydown', function (evt) {
                // typeahead is open and an "interesting" key was pressed
                if (scope.matches.length === 0 || HOT_KEYS.indexOf(evt.which) === -1) {
                    return;
                }

                const shouldSelect = isSelectEvent(originalScope, { $event: evt });

                /**
                 * if there's nothing selected (i.e. focusFirst) and enter or tab is hit
                 * or
                 * shift + tab is pressed to bring focus to the previous element
                 * then clear the results
                 */
                if ((scope.activeIdx === -1 && shouldSelect) || (evt.which === 9 && !!evt.shiftKey)) {
                    resetMatches();
                    scope.$digest();
                    return;
                }

                evt.preventDefault();
                let target;
                switch (evt.which) {
                    case 27: // escape
                        evt.stopPropagation();

                        resetMatches();
                        originalScope.$digest();
                        break;
                    case 38: // up arrow
                        scope.activeIdx = (scope.activeIdx > 0 ? scope.activeIdx : scope.matches.length) - 1;
                        scope.$digest();
                        target = popUpEl[0].querySelectorAll('.uib-typeahead-match')[scope.activeIdx];
                        target.parentNode.scrollTop = target.offsetTop;
                        break;
                    case 40: // down arrow
                        scope.activeIdx = (scope.activeIdx + 1) % scope.matches.length;
                        scope.$digest();
                        target = popUpEl[0].querySelectorAll('.uib-typeahead-match')[scope.activeIdx];
                        target.parentNode.scrollTop = target.offsetTop;
                        break;
                    default:
                        if (shouldSelect) {
                            scope.$apply(function () {
                                if (angular.isNumber(scope.debounceUpdate) || angular.isObject(scope.debounceUpdate)) {
                                    $$debounce(
                                        function () {
                                            scope.select(scope.activeIdx, evt);
                                        },
                                        angular.isNumber(scope.debounceUpdate)
                                            ? scope.debounceUpdate
                                            : scope.debounceUpdate.default,
                                    );
                                } else {
                                    scope.select(scope.activeIdx, evt);
                                }
                            });
                        }
                }
            });

            element.on('focus', function (evt) {
                hasFocus = true;
                if (minLength === 0 && !modelCtrl.$viewValue) {
                    $timeout(function () {
                        getMatchesAsync(modelCtrl.$viewValue, evt);
                    }, 0);
                }
            });

            element.on('blur', function (evt) {
                if (isSelectOnBlur && scope.matches.length && scope.activeIdx !== -1 && !selected) {
                    selected = true;
                    scope.$apply(function () {
                        if (angular.isObject(scope.debounceUpdate) && angular.isNumber(scope.debounceUpdate.blur)) {
                            $$debounce(function () {
                                scope.select(scope.activeIdx, evt);
                            }, scope.debounceUpdate.blur);
                        } else {
                            scope.select(scope.activeIdx, evt);
                        }
                    });
                }
                if (!isEditable && modelCtrl.$error.editable) {
                    modelCtrl.$setViewValue();
                    scope.$apply(function () {
                        // Reset validity as we are clearing
                        modelCtrl.$setValidity('editable', true);
                        modelCtrl.$setValidity('parse', true);
                    });
                    element.val('');
                }
                hasFocus = false;
                selected = false;
            });

            // Keep reference to click handler to unbind it.
            const dismissClickHandler = function (evt) {
                // Issue #3973
                // Firefox treats right click as a click on document
                if (element[0] !== evt.target && evt.which !== 3 && scope.matches.length !== 0) {
                    resetMatches();
                    if (!$rootScope.$$phase) {
                        originalScope.$digest();
                    }
                }
            };

            $document.on('click', dismissClickHandler);

            originalScope.$on('$destroy', function () {
                $document.off('click', dismissClickHandler);
                if (appendToBody || appendTo) {
                    $popup.remove();
                }

                if (appendToBody) {
                    angular.element($window).off('resize', fireRecalculating);
                    $document.find('body').off('scroll', fireRecalculating);
                }
                // Prevent jQuery cache memory leak
                popUpEl.remove();

                if (showHint) {
                    inputsContainer.remove();
                }
            });

            var $popup = $compile(popUpEl)(scope);

            if (appendToBody) {
                $document.find('body').append($popup);
            } else if (appendTo) {
                angular.element(appendTo).eq(0).append($popup);
            } else {
                element.after($popup);
            }

            this.init = function (_modelCtrl) {
                modelCtrl = _modelCtrl;
                ngModelOptions = extractOptions(modelCtrl);

                scope.debounceUpdate = $parse(ngModelOptions.getOption('debounce'))(originalScope);

                // plug into $parsers pipeline to open a typeahead on view changes initiated from DOM
                // $parsers kick-in on all the changes coming from the view as well as manually triggered by $setViewValue
                modelCtrl.$parsers.unshift(function (inputValue) {
                    hasFocus = true;

                    if (minLength === 0 || (inputValue && inputValue.length >= minLength)) {
                        if (waitTime > 0) {
                            cancelPreviousTimeout();
                            scheduleSearchWithTimeout(inputValue);
                        } else {
                            getMatchesAsync(inputValue);
                        }
                    } else {
                        isLoadingSetter(originalScope, false);
                        cancelPreviousTimeout();
                        resetMatches();
                    }

                    if (isEditable) {
                        return inputValue;
                    }

                    if (!inputValue) {
                        // Reset in case user had typed something previously.
                        modelCtrl.$setValidity('editable', true);
                        return null;
                    }

                    modelCtrl.$setValidity('editable', false);
                    return undefined;
                });

                modelCtrl.$formatters.push(function (modelValue) {
                    let candidateViewValue;
                    let emptyViewValue;
                    const locals = {};

                    // The validity may be set to false via $parsers (see above) if
                    // the model is restricted to selected values. If the model
                    // is set manually it is considered to be valid.
                    if (!isEditable) {
                        modelCtrl.$setValidity('editable', true);
                    }

                    if (inputFormatter) {
                        locals.$model = modelValue;
                        return inputFormatter(originalScope, locals);
                    }

                    // it might happen that we don't have enough info to properly render input value
                    // we need to check for this situation and simply return model value if we can't apply custom formatting
                    locals[parserResult.itemName] = modelValue;
                    candidateViewValue = parserResult.viewMapper(originalScope, locals);
                    locals[parserResult.itemName] = undefined;
                    emptyViewValue = parserResult.viewMapper(originalScope, locals);

                    return candidateViewValue !== emptyViewValue ? candidateViewValue : modelValue;
                });
            };

            function extractOptions(ngModelCtrl) {
                let ngModelOptions;

                if (angular.version.minor < 6) {
                    // in angular < 1.6 $options could be missing
                    // guarantee a value
                    ngModelOptions = ngModelCtrl.$options || {};

                    // mimic 1.6+ api
                    ngModelOptions.getOption = function (key) {
                        return ngModelOptions[key];
                    };
                } else {
                    // in angular >=1.6 $options is always present
                    ngModelOptions = ngModelCtrl.$options;
                }

                return ngModelOptions;
            }
        },
    ])

    .directive('uibTypeahead', function () {
        return {
            controller: 'UibTypeaheadController',
            require: ['ngModel', 'uibTypeahead'],
            link(originalScope, element, attrs, ctrls) {
                ctrls[1].init(ctrls[0]);
            },
        };
    })

    .directive('uibTypeaheadPopup', [
        '$$debounce',
        function ($$debounce) {
            return {
                scope: {
                    matches: '=',
                    query: '=',
                    active: '=',
                    position: '&',
                    moveInProgress: '=',
                    select: '&',
                    assignIsOpen: '&',
                    debounce: '&',
                },
                replace: true,
                templateUrl(element, attrs) {
                    return attrs.popupTemplateUrl || 'uib/template/typeahead/typeahead-popup.html';
                },
                link(scope, element, attrs) {
                    scope.templateUrl = attrs.templateUrl;

                    scope.isOpen = function () {
                        const isDropdownOpen = scope.matches.length > 0;
                        scope.assignIsOpen({ isOpen: isDropdownOpen });
                        return isDropdownOpen;
                    };

                    scope.isActive = function (matchIdx) {
                        return scope.active === matchIdx;
                    };

                    scope.selectActive = function (matchIdx) {
                        scope.active = matchIdx;
                    };

                    scope.selectMatch = function (activeIdx, evt) {
                        const debounce = scope.debounce();
                        if (angular.isNumber(debounce) || angular.isObject(debounce)) {
                            $$debounce(
                                function () {
                                    scope.select({ activeIdx, evt });
                                },
                                angular.isNumber(debounce) ? debounce : debounce.default,
                            );
                        } else {
                            scope.select({ activeIdx, evt });
                        }
                    };
                },
            };
        },
    ])

    .directive('uibTypeaheadMatch', [
        '$templateRequest',
        '$compile',
        '$parse',
        function ($templateRequest, $compile, $parse) {
            return {
                scope: {
                    index: '=',
                    match: '=',
                    query: '=',
                },
                link(scope, element, attrs) {
                    const tplUrl =
                        $parse(attrs.templateUrl)(scope.$parent) || 'uib/template/typeahead/typeahead-match.html';
                    $templateRequest(tplUrl).then(function (tplContent) {
                        const tplEl = angular.element(tplContent.trim());
                        element.replaceWith(tplEl);
                        $compile(tplEl)(scope);
                    });
                },
            };
        },
    ])

    .filter('uibTypeaheadHighlight', [
        '$sce',
        '$injector',
        '$log',
        function ($sce, $injector, $log) {
            let isSanitizePresent;
            isSanitizePresent = $injector.has('$sanitize');

            function escapeRegexp(queryToEscape) {
                // Regex: capture the whole query string and replace it with the string that will be used to match
                // the results, for example if the capture is "a" the result will be \a
                return queryToEscape.replace(/([.?*+^$[\]\\(){}|-])/g, '\\$1');
            }

            function containsHtml(matchItem) {
                return /<.*>/g.test(matchItem);
            }

            return function (matchItem, query) {
                if (!isSanitizePresent && containsHtml(matchItem)) {
                    $log.warn('Unsafe use of typeahead please use ngSanitize'); // Warn the user about the danger
                }
                matchItem = query
                    ? `${matchItem}`.replace(new RegExp(escapeRegexp(query), 'gi'), '<strong>$&</strong>')
                    : matchItem; // Replaces the capture string with a the same string inside of a "strong" tag
                if (!isSanitizePresent) {
                    matchItem = $sce.trustAsHtml(matchItem); // If $sanitize is not present we pack the string in a $sce object for the ng-bind-html directive
                }
                return matchItem;
            };
        },
    ]);

angular
    .module('ui.bootstrap.debounce', [])
    /**
     * A helper, internal service that debounces a function
     */
    .factory('$$debounce', [
        '$timeout',
        function ($timeout) {
            return function (callback, debounceTime) {
                let timeoutPromise;

                return function () {
                    const self = this;
                    const args = Array.prototype.slice.call(arguments);
                    if (timeoutPromise) {
                        $timeout.cancel(timeoutPromise);
                    }

                    timeoutPromise = $timeout(function () {
                        callback.apply(self, args);
                    }, debounceTime);
                };
            };
        },
    ]);

angular.module('uib/template/carousel/carousel.html', []).run([
    '$templateCache',
    function ($templateCache) {
        $templateCache.put(
            'uib/template/carousel/carousel.html',
            '<div class="carousel-inner" ng-transclude></div>\n' +
                '<a role="button" href class="left carousel-control" ng-click="prev()" ng-class="{ disabled: isPrevDisabled() }" ng-show="slides.length > 1">\n' +
                '  <span aria-hidden="true" class="glyphicon glyphicon-chevron-left"></span>\n' +
                '  <span class="sr-only">previous</span>\n' +
                '</a>\n' +
                '<a role="button" href class="right carousel-control" ng-click="next()" ng-class="{ disabled: isNextDisabled() }" ng-show="slides.length > 1">\n' +
                '  <span aria-hidden="true" class="glyphicon glyphicon-chevron-right"></span>\n' +
                '  <span class="sr-only">next</span>\n' +
                '</a>\n' +
                '<ol class="carousel-indicators" ng-show="slides.length > 1">\n' +
                '  <li ng-repeat="slide in slides | orderBy:indexOfSlide track by $index" ng-class="{ active: isActive(slide) }" ng-click="select(slide)">\n' +
                '    <span class="sr-only">slide {{ $index + 1 }} of {{ slides.length }}<span ng-if="isActive(slide)">, currently active</span></span>\n' +
                '  </li>\n' +
                '</ol>\n' +
                '',
        );
    },
]);

angular.module('uib/template/carousel/slide.html', []).run([
    '$templateCache',
    function ($templateCache) {
        $templateCache.put('uib/template/carousel/slide.html', '<div class="text-center" ng-transclude></div>\n' + '');
    },
]);

angular.module('uib/template/popover/popover-html.html', []).run([
    '$templateCache',
    function ($templateCache) {
        $templateCache.put(
            'uib/template/popover/popover-html.html',
            '<div class="arrow"></div>\n' +
                '\n' +
                '<div class="popover-inner">\n' +
                '    <h3 class="popover-title" ng-bind="uibTitle" ng-if="uibTitle"></h3>\n' +
                '    <div class="popover-content" ng-bind-html="contentExp()"></div>\n' +
                '</div>\n' +
                '',
        );
    },
]);

angular.module('uib/template/popover/popover-template.html', []).run([
    '$templateCache',
    function ($templateCache) {
        $templateCache.put(
            'uib/template/popover/popover-template.html',
            '<div class="arrow"></div>\n' +
                '\n' +
                '<div class="popover-inner">\n' +
                '    <h3 class="popover-title" ng-bind="uibTitle" ng-if="uibTitle"></h3>\n' +
                '    <div class="popover-content"\n' +
                '      uib-tooltip-template-transclude="contentExp()"\n' +
                '      tooltip-template-transclude-scope="originScope()"></div>\n' +
                '</div>\n' +
                '',
        );
    },
]);

angular.module('uib/template/popover/popover.html', []).run([
    '$templateCache',
    function ($templateCache) {
        $templateCache.put(
            'uib/template/popover/popover.html',
            '<div class="arrow"></div>\n' +
                '\n' +
                '<div class="popover-inner">\n' +
                '    <h3 class="popover-title" ng-bind="uibTitle" ng-if="uibTitle"></h3>\n' +
                '    <div class="popover-content" ng-bind="content"></div>\n' +
                '</div>\n' +
                '',
        );
    },
]);

angular.module('uib/template/tooltip/tooltip-html-popup.html', []).run([
    '$templateCache',
    function ($templateCache) {
        $templateCache.put(
            'uib/template/tooltip/tooltip-html-popup.html',
            '<div class="tooltip-arrow"></div>\n' +
                '<div class="tooltip-inner" ng-bind-html="contentExp()"></div>\n' +
                '',
        );
    },
]);

angular.module('uib/template/tooltip/tooltip-popup.html', []).run([
    '$templateCache',
    function ($templateCache) {
        $templateCache.put(
            'uib/template/tooltip/tooltip-popup.html',
            '<div class="tooltip-arrow"></div>\n' + '<div class="tooltip-inner" ng-bind="content"></div>\n' + '',
        );
    },
]);

angular.module('uib/template/tooltip/tooltip-template-popup.html', []).run([
    '$templateCache',
    function ($templateCache) {
        $templateCache.put(
            'uib/template/tooltip/tooltip-template-popup.html',
            '<div class="tooltip-arrow"></div>\n' +
                '<div class="tooltip-inner"\n' +
                '  uib-tooltip-template-transclude="contentExp()"\n' +
                '  tooltip-template-transclude-scope="originScope()"></div>\n' +
                '',
        );
    },
]);

angular.module('uib/template/typeahead/typeahead-match.html', []).run([
    '$templateCache',
    function ($templateCache) {
        $templateCache.put(
            'uib/template/typeahead/typeahead-match.html',
            '<a href\n' +
                '   tabindex="-1"\n' +
                '   ng-bind-html="match.label | uibTypeaheadHighlight:query"\n' +
                '   ng-attr-title="{{match.label}}"></a>\n' +
                '',
        );
    },
]);

angular.module('uib/template/typeahead/typeahead-popup.html', []).run([
    '$templateCache',
    function ($templateCache) {
        $templateCache.put(
            'uib/template/typeahead/typeahead-popup.html',
            '<ul class="dropdown-menu" ng-show="isOpen() && !moveInProgress" ng-style="{top: position().top+\'px\', left: position().left+\'px\'}" role="listbox" aria-hidden="{{!isOpen()}}">\n' +
                '    <li class="uib-typeahead-match" ng-repeat="match in matches track by $index" ng-class="{active: isActive($index) }" ng-mouseenter="selectActive($index)" ng-click="selectMatch($index, $event)" role="option" id="{{::match.id}}">\n' +
                '        <div uib-typeahead-match index="$index" match="match" query="query" template-url="templateUrl"></div>\n' +
                '    </li>\n' +
                '</ul>\n' +
                '',
        );
    },
]);
angular.module('ui.bootstrap.carousel').run(function () {
    !angular.$$csp().noInlineStyle &&
        !angular.$$uibCarouselCss &&
        angular
            .element(document)
            .find('head')
            .prepend(
                '<style type="text/css">.ng-animate.item:not(.left):not(.right){-webkit-transition:0s ease-in-out left;transition:0s ease-in-out left}</style>',
            );
    angular.$$uibCarouselCss = true;
});
angular.module('ui.bootstrap.tooltip').run(function () {
    !angular.$$csp().noInlineStyle &&
        !angular.$$uibTooltipCss &&
        angular
            .element(document)
            .find('head')
            .prepend(
                '<style type="text/css">[uib-tooltip-popup].tooltip.top-left > .tooltip-arrow,[uib-tooltip-popup].tooltip.top-right > .tooltip-arrow,[uib-tooltip-popup].tooltip.bottom-left > .tooltip-arrow,[uib-tooltip-popup].tooltip.bottom-right > .tooltip-arrow,[uib-tooltip-popup].tooltip.left-top > .tooltip-arrow,[uib-tooltip-popup].tooltip.left-bottom > .tooltip-arrow,[uib-tooltip-popup].tooltip.right-top > .tooltip-arrow,[uib-tooltip-popup].tooltip.right-bottom > .tooltip-arrow,[uib-tooltip-html-popup].tooltip.top-left > .tooltip-arrow,[uib-tooltip-html-popup].tooltip.top-right > .tooltip-arrow,[uib-tooltip-html-popup].tooltip.bottom-left > .tooltip-arrow,[uib-tooltip-html-popup].tooltip.bottom-right > .tooltip-arrow,[uib-tooltip-html-popup].tooltip.left-top > .tooltip-arrow,[uib-tooltip-html-popup].tooltip.left-bottom > .tooltip-arrow,[uib-tooltip-html-popup].tooltip.right-top > .tooltip-arrow,[uib-tooltip-html-popup].tooltip.right-bottom > .tooltip-arrow,[uib-tooltip-template-popup].tooltip.top-left > .tooltip-arrow,[uib-tooltip-template-popup].tooltip.top-right > .tooltip-arrow,[uib-tooltip-template-popup].tooltip.bottom-left > .tooltip-arrow,[uib-tooltip-template-popup].tooltip.bottom-right > .tooltip-arrow,[uib-tooltip-template-popup].tooltip.left-top > .tooltip-arrow,[uib-tooltip-template-popup].tooltip.left-bottom > .tooltip-arrow,[uib-tooltip-template-popup].tooltip.right-top > .tooltip-arrow,[uib-tooltip-template-popup].tooltip.right-bottom > .tooltip-arrow,[uib-popover-popup].popover.top-left > .arrow,[uib-popover-popup].popover.top-right > .arrow,[uib-popover-popup].popover.bottom-left > .arrow,[uib-popover-popup].popover.bottom-right > .arrow,[uib-popover-popup].popover.left-top > .arrow,[uib-popover-popup].popover.left-bottom > .arrow,[uib-popover-popup].popover.right-top > .arrow,[uib-popover-popup].popover.right-bottom > .arrow,[uib-popover-html-popup].popover.top-left > .arrow,[uib-popover-html-popup].popover.top-right > .arrow,[uib-popover-html-popup].popover.bottom-left > .arrow,[uib-popover-html-popup].popover.bottom-right > .arrow,[uib-popover-html-popup].popover.left-top > .arrow,[uib-popover-html-popup].popover.left-bottom > .arrow,[uib-popover-html-popup].popover.right-top > .arrow,[uib-popover-html-popup].popover.right-bottom > .arrow,[uib-popover-template-popup].popover.top-left > .arrow,[uib-popover-template-popup].popover.top-right > .arrow,[uib-popover-template-popup].popover.bottom-left > .arrow,[uib-popover-template-popup].popover.bottom-right > .arrow,[uib-popover-template-popup].popover.left-top > .arrow,[uib-popover-template-popup].popover.left-bottom > .arrow,[uib-popover-template-popup].popover.right-top > .arrow,[uib-popover-template-popup].popover.right-bottom > .arrow{top:auto;bottom:auto;left:auto;right:auto;margin:0;}[uib-popover-popup].popover,[uib-popover-html-popup].popover,[uib-popover-template-popup].popover{display:block !important;}</style>',
            );
    angular.$$uibTooltipCss = true;
});
angular.module('ui.bootstrap.position').run(function () {
    !angular.$$csp().noInlineStyle &&
        !angular.$$uibPositionCss &&
        angular
            .element(document)
            .find('head')
            .prepend(
                '<style type="text/css">.uib-position-measure{display:block !important;visibility:hidden !important;position:absolute !important;top:-9999px !important;left:-9999px !important;}.uib-position-scrollbar-measure{position:absolute !important;top:-9999px !important;width:50px !important;height:50px !important;overflow:scroll !important;}.uib-position-body-scrollbar-measure{overflow:scroll !important;}</style>',
            );
    angular.$$uibPositionCss = true;
});
angular.module('ui.bootstrap.typeahead').run(function () {
    !angular.$$csp().noInlineStyle &&
        !angular.$$uibTypeaheadCss &&
        angular
            .element(document)
            .find('head')
            .prepend('<style type="text/css">[uib-typeahead-popup].dropdown-menu{display:block;}</style>');
    angular.$$uibTypeaheadCss = true;
});
