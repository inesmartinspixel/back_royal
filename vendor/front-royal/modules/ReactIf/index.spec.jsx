import React from 'react';
import { shallow } from 'enzyme';
import ReactIf from './index';

describe('<ReactIf>', () => {
    let elem;

    afterEach(() => {
        elem.unmount();
    });

    it('should render if true', () => {
        elem = shallow(
            <ReactIf if>
                <div />
            </ReactIf>,
        );
        expect(elem.contains(<div />)).toBe(true);
    });

    it('should not render if false', () => {
        elem = shallow(
            <ReactIf if={false}>
                <div />
            </ReactIf>,
        );
        expect(elem.contains(<div />)).toBe(false);
    });
});
