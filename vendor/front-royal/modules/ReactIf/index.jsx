/*
    Usage:

    <ReactIf if={trueOrFalse}>
        Some stuff that only shows up if `trueOrFalse` is true
    </ReactIf>

    In many cases, it's just as good to skip this and do

    {trueOrFalse && <MyComponent/>}

    But in some cases it's cleaner to wrap it in <ReactIf>
*/

import React from 'react';

function ReactIf(props) {
    return <>{props.if ? props.children : ''}</>;
}
export default ReactIf;
