import 'AngularSpecHelper';
import 'LearnerProjects/angularModule';

describe('LearnerProject', () => {
    let $injector;
    let SpecHelper;
    let LearnerProject;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.LearnerProjects', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector => {
                $injector = _$injector;
                SpecHelper = $injector.get('SpecHelper');
                LearnerProject = $injector.get('LearnerProject');
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('project documents', () => {
        let project_documents;

        beforeEach(() => {
            project_documents = [
                {
                    file_file_name: 'foo.pdf',
                    title: 'foo',
                    url: 'foo.com',
                },
                {
                    title: 'bar',
                    url: 'bar.com',
                },
            ];
        });

        describe('addProjectDocument', () => {
            it('should work', () => {
                const instance = LearnerProject.new();
                instance.project_documents = [];
                instance.addProjectDocument(project_documents[0]);
                expect(instance.project_documents.length).toEqual(1);
                expect(instance.project_documents[0].title).toEqual('foo.pdf');
            });
        });

        describe('removeProjectDocument', () => {
            it('should work', () => {
                const instance = LearnerProject.new();
                instance.project_documents = project_documents;
                instance.removeProjectDocument(project_documents[0]);
                expect(instance.project_documents.length).toEqual(1);
                expect(instance.project_documents[0].title).toEqual('bar');
            });
        });
    });

    describe('getScoreWeight', () => {
        let instance;

        beforeEach(() => {
            instance = LearnerProject.new();
        });

        describe('standard projects', () => {
            beforeEach(() => {
                instance.project_type = 'standard';
            });

            it('should return 2 when emba programType', () => {
                expect(instance.getScoringWeight('emba')).toEqual(2);
            });

            it('should return 4 otherwise', () => {
                expect(instance.getScoringWeight('foobar')).toEqual(4);
            });
        });

        it('should return 4 for capstones', () => {
            instance.project_type = 'capstone';
            expect(instance.getScoringWeight('foobar')).toEqual(4);
        });

        it('should return 1 for presentations', () => {
            instance.project_type = 'presentation';
            expect(instance.getScoringWeight('foobar')).toEqual(1);
        });
    });

    describe('getPassingScore', () => {
        let instance;
        let cohort;

        beforeEach(() => {
            instance = LearnerProject.new();
            cohort = {};
        });

        describe('standard projects', () => {
            beforeEach(() => {
                instance.project_type = 'standard';
            });

            it('should return 1 for legacy emba cohort', () => {
                cohort.startDate = new Date('2019/07/01');
                cohort.program_type = 'emba';
                expect(instance.getPassingScore(cohort)).toEqual(1);
            });

            it('should return 1 for legacy mba cohort', () => {
                cohort.startDate = new Date('2019/07/29');
                cohort.program_type = 'mba';
                expect(instance.getPassingScore(cohort)).toEqual(1);
            });

            it('should return 2 for emba current', () => {
                cohort.startDate = new Date('2019/07/02');
                cohort.program_type = 'emba';
                expect(instance.getPassingScore(cohort)).toEqual(2);
            });

            it('should return 2 for mba current', () => {
                cohort.startDate = new Date('2019/07/30');
                cohort.program_type = 'mba';
                expect(instance.getPassingScore(cohort)).toEqual(2);
            });
        });

        it('should return 3 for capstones', () => {
            instance.project_type = 'capstone';
            expect(instance.getPassingScore('foobar')).toEqual(3);
        });

        it('should return 2 for presentations', () => {
            instance.project_type = 'presentation';
            expect(instance.getPassingScore('foobar')).toEqual(2);
        });
    });
});
