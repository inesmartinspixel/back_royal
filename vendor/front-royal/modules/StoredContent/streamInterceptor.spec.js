import openTestDb from 'FrontRoyalStore/openTestDb';
import streamInterceptor from './streamInterceptor';

describe('streamInterceptor', () => {
    const db = openTestDb();

    const frontRoyalStore = { retryAfterHandledError: fn => fn(db) };
    Object.defineProperty(frontRoyalStore, 'enabled', { get: () => true, configurable: true });
    let enabledSpy;
    let storeEnabled;

    const $injector = {
        get: key => ({ frontRoyalStore }[key]),
    };
    const streamId = '3b351cd4-82d2-4e76-8148-17ff3c40a4a3';

    beforeEach(() => {
        storeEnabled = true;
        enabledSpy = jest.spyOn(frontRoyalStore, 'enabled', 'get').mockImplementation(() => storeEnabled);
    });

    it('should not handle index requests', async () => {
        const handleRequest = await streamInterceptor(
            {
                method: 'get',
                url: `http://path/to/api/lesson_streams.json`,
            },
            $injector,
        );

        expect(handleRequest).toBe(null);
        expect(enabledSpy).not.toHaveBeenCalled();
    });

    it('should not handle requests when the store is disabled', async () => {
        storeEnabled = false;
        const handleRequest = await streamInterceptor(
            {
                method: 'get',
                url: `http://path/to/api/lesson_streams/${streamId}.json`,
            },
            $injector,
        );

        expect(handleRequest).toBe(null);
        expect(enabledSpy).toHaveBeenCalled();
    });

    it('should not handle requests when the stream is not available offline', async () => {
        await db.publishedStreams.put({ id: streamId, all_content_stored: 0 });
        const handleRequest = await streamInterceptor(
            {
                method: 'get',
                url: `http://path/to/api/lesson_streams/${streamId}.json`,
            },
            $injector,
        );

        expect(handleRequest).toBe(null);
    });

    it('should handle requests when the stream is not available offline', async () => {
        const frames = 'some frames';
        const lessonId = 'lessonId';
        const streamAttrs = {
            id: streamId,
            all_content_stored: 1,
            lessons: [{ id: 'anotherLesson' }, { id: lessonId }],
        };
        await db.publishedStreams.put(streamAttrs);
        await db.publishedLessonContent.put({ stream_id: streamId, id: lessonId, frames });
        const handleRequest = await streamInterceptor(
            {
                method: 'get',
                url: `http://path/to/api/lesson_streams/${streamId}.json`,
                params: {
                    load_full_content_for_lesson_id: lessonId,
                },
            },
            $injector,
        );

        const response = await handleRequest();
        streamAttrs.lessons[1].frames = frames;
        expect(response.data.contents.lesson_streams[0]).toEqual(streamAttrs);
    });
});
