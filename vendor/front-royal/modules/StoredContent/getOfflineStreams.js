// Returns the list of streams that are available to be viewed offline
export default async function getOfflineStreams(injector) {
    const frontRoyalStore = injector.get('frontRoyalStore');
    if (!frontRoyalStore.enabled) {
        return Promise.resolve([]);
    }
    const Stream = injector.get('Lesson.Stream');
    const streams = await frontRoyalStore.retryAfterHandledError(db =>
        db.publishedStreams.where({ all_content_stored: 1 }).toArray(),
    );
    return streams.map(stream => Stream.new(stream));
}
