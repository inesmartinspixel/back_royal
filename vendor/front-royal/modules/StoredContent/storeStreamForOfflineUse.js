import promiseRetry from 'promise-retry';
import delay from 'delay';
import fetchImageBlobsForLesson from './fetchImageBlobsForLesson';

async function saveImageBatch(images, injector) {
    const frontRoyalStore = injector.get('frontRoyalStore');
    const ErrorLogService = injector.get('ErrorLogService');
    // See note down near where this is called for info about why we're retrying here.
    // Since we're batching the calls to bulkPut, we don't really expect any errors
    // here that will trigger a retry, so we log to sentry on every retry (for now),
    // just so we know if it's happening in the wild.
    return promiseRetry(
        retry => {
            return frontRoyalStore.retryAfterHandledError(db =>
                db.images.bulkPut(images).catch(err => {
                    if (err.message?.match(/DataError/)) {
                        ErrorLogService.notifyInProd(`failed to store image batch.  Retrying`, err, {
                            level: 'warn',
                        });
                        return retry();
                    }
                    throw err;
                }),
            );
        },
        { retries: 3 },
    );
}

// By using a transaction here, we can be sure that either all of the
// content for the stream was saved or none of it was.  We could avoid a transaction
// if we saved images first, then lessonContent, then the stream.  But then we would
// need to search for orphaned images and lessonContent when we were deleting
// content that is no longer needed offline (theoretically, at least).
async function saveRecords(stream, lessonContentRecords, images, injector) {
    const frontRoyalStore = injector.get('frontRoyalStore');
    await frontRoyalStore.retryAfterHandledError(db =>
        db.transaction('rw', db.publishedStreams, db.publishedLessonContent, db.images, () => {
            const promises = [
                db.publishedLessonContent.bulkPut(lessonContentRecords),
                db.publishedStreams.put({ ...stream, all_content_stored: 1 }),
            ];

            // Before I split these image saves up into batches, I would sometimes get
            // `DataError: Failed to write blobs.'` I found the following thread about it,
            // which suggested retrying.  Breaking things up into smaller batches seems
            // to help.  We're also retrying inside of saveImageBatch, just in case.
            // https://bugs.chromium.org/p/chromium/issues/detail?id=338800
            while (images.length > 0) {
                promises.push(saveImageBatch(images.splice(0, 10), injector));
            }

            return Promise.all(promises);
        }),
    );
}

export default async function storeStreamForOfflineUse(stream, injector) {
    const frontRoyalStore = injector.get('frontRoyalStore');

    // clone since we're going to remove the frames
    stream = {
        ...stream,
        lessons: stream.lessons.slice(0),
    };

    // We store the stream json without the full frames content
    // in the db.streams table.  We store the full frames content
    // in the publishedLessonContent table.  That way, we can pull streams
    // for the student dashboard, stream dashboard, etc., without having
    // to pull all that lesson content, which we only need for the player.
    const images = [];
    const lessonContentRecords = [];
    for (let i = 0; i < stream.lessons.length; i += 1) {
        const lesson = stream.lessons[i];
        const frames = lesson.frames;

        const lessonContentRecord = {
            id: lesson.id,
            stream_id: stream.id,
            frames: JSON.parse(JSON.stringify(frames)), // FIXME: try to remove parsing
        };

        // FIXME: we are currently doing this consecutively
        // instead of in parallel to prevent locking up the UI,
        // even though it's awkward.  See https://trello.com/c/5yQMSVoI
        // eslint-disable-next-line no-await-in-loop
        await frontRoyalStore.retryAfterHandledError(db =>
            fetchImageBlobsForLesson(lesson, db, injector).then(imagesForLesson => {
                images.splice(0, 0, ...imagesForLesson);
                lessonContentRecord.image_urls = imagesForLesson.map(img => img.url);
            }),
        );
        // eslint-disable-next-line no-await-in-loop
        await delay(200 * Math.random());

        // imagePromises.push(promise);

        stream.lessons[i] = {
            ...lesson,
            frames: undefined,
        };
        lessonContentRecords.push(lessonContentRecord);
    }

    // Let all the images load first, before we open up the transaction,
    // so that we don't keep it open for a really long time
    await saveRecords(stream, lessonContentRecords, images, injector);
}
