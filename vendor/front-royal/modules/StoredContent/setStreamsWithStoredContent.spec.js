import { sortBy } from 'lodash/fp';
import openTestDb from 'FrontRoyalStore/openTestDb';
import setStreamsWithStoredContent from './setStreamsWithStoredContent';
import storeStreamForOfflineUse from './storeStreamForOfflineUse';
import unstoreStreamForOfflineUse from './unstoreStreamForOfflineUse';

jest.mock('./storeStreamForOfflineUse', () => jest.fn());
jest.mock('./unstoreStreamForOfflineUse', () => jest.fn());

describe('setStreamsWithStoredContent', () => {
    const db = openTestDb();
    const Lesson = {};
    const locale = 'xx';
    const frontRoyalStore = { retryAfterHandledError: fn => fn(db) };
    let $http;
    let injector;
    const origEndpointRoot = window.ENDPOINT_ROOT;

    beforeAll(() => {
        window.ENDPOINT_ROOT = 'endpointRoot';
    });
    afterAll(() => {
        window.ENDPOINT_ROOT = origEndpointRoot;
    });

    beforeEach(() => {
        storeStreamForOfflineUse.mockReset();
        unstoreStreamForOfflineUse.mockReset();
        $http = jest.fn();

        injector = {
            get: name => ({ Lesson, $http, frontRoyalStore }[name]),
        };
    });

    it('should remove streams that are no longer needed', async () => {
        await setupCurrentlyAvailableStreams([
            { id: 'currentlyAvailableId', locale_pack: { id: 'currentlyAvailableLocalePackId' }, locale },
        ]);
        await setStreamsWithStoredContent([], locale, injector);
        expect(unstoreStreamForOfflineUse).toHaveBeenCalledWith('currentlyAvailableId', db);
    });

    it('should replace a stream that is available offline in the wrong locale', async () => {
        await setupCurrentlyAvailableStreams([
            { id: 'currentlyAvailableId', locale_pack: { id: 'shouldBeAvailable' }, locale: 'someOtherLocale' },
        ]);

        // the stream should be fetched and stored in the appropriate locale
        await assertStreamsFetchedAndStored(['shouldBeAvailable'], async () => {
            await setStreamsWithStoredContent(['shouldBeAvailable'], locale, injector);
        });

        // the stream in the wrong locale should be unstored
        expect(unstoreStreamForOfflineUse).toHaveBeenCalledWith('currentlyAvailableId', db);
    });

    it('should add streams that are not yet stored', async () => {
        await setupCurrentlyAvailableStreams([]);
        await assertStreamsFetchedAndStored(['shouldBeAvailable'], async () => {
            await setStreamsWithStoredContent(['shouldBeAvailable'], locale, injector);
        });
    });

    it('should check for updated versions of streams that are already stored', async () => {
        // NOTE: the logic here is similar to assertStreamsFetchedAndStored,
        // but we're hitting the get_outdated endpoint instead of the index
        // endpoint and only returning outdated streams
        const streams = [
            {
                id: 'shouldBeAvailableId',
                locale_pack: { id: 'shouldBeAvailable' },
                locale,
                published_at: 4,
                lessons: [{ published_at: 5 }],
            },

            // This one should use the published_at of the
            // stream, since it is later
            {
                id: 'outdatedStream1Id',
                locale_pack: { id: 'outdatedStream1' },
                locale,
                published_at: 42,
                lessons: [{ published_at: 41 }],
            },

            // This one should use the published_at of the lesson,
            // since it is later
            {
                id: 'outdatedStream2Id',
                locale_pack: { id: 'outdatedStream2' },
                locale,
                published_at: 37,
                lessons: [{ published_at: 38 }],
            },
        ];
        const outdatedStreams = [streams[1], streams[1]];
        await setupCurrentlyAvailableStreams(streams);

        $http.mockReturnValue(Promise.resolve({ data: { contents: { lesson_streams: outdatedStreams } } }));
        await setStreamsWithStoredContent(
            ['shouldBeAvailable', 'outdatedStream1', 'outdatedStream2'],
            locale,
            injector,
        );

        let expectedStreamEntries = [
            {
                id: 'shouldBeAvailableId',
                content_updated_at: 5,
                load_full_content: true,
            },
            {
                id: 'outdatedStream1Id',
                content_updated_at: 42,
                load_full_content: true,
            },
            {
                id: 'outdatedStream2Id',
                content_updated_at: 38,
                load_full_content: true,
            },
        ];

        // This is annoying, but we don't know which order the
        // entries are going to be in the params that get sent to the
        // server, so we have to use the actual value to get the expected
        // value into the correct order.
        const actualEntryIds = $http.mock.calls[0][0].data.streams.map(s => s.id);
        expectedStreamEntries = sortBy(e => actualEntryIds.indexOf(e.id))(expectedStreamEntries);

        expect($http).toHaveBeenCalledWith({
            method: 'POST',
            headers: {
                Accept: 'application/json',
            },
            httpQueueOptions: {
                priority: -10,
            },
            data: {
                streams: expectedStreamEntries,
            },
            url: `${window.ENDPOINT_ROOT}/api/lesson_streams/get_outdated.json`,
            'FrontRoyal.ApiErrorHandler': {
                background: true,
            },
        });

        expect(storeStreamForOfflineUse.mock.calls.length).toEqual(outdatedStreams.length);
        outdatedStreams.forEach(s => expect(storeStreamForOfflineUse).toHaveBeenCalledWith(s, injector));
    });

    async function assertStreamsFetchedAndStored(streamLocalePackIds, fn) {
        const streams = streamLocalePackIds.map(localePackId => ({
            id: `idFor: ${localePackId}`,
            locale_pack: { id: localePackId },
        }));
        $http.mockReturnValue(Promise.resolve({ data: { contents: { lesson_streams: streams } } }));
        await fn();
        expect($http).toHaveBeenCalledWith({
            method: 'GET',
            headers: {
                Accept: 'application/json',
            },
            httpQueueOptions: {
                priority: -10,
            },
            params: {
                filters: {
                    in_users_locale_or_en: true,
                    locale_pack_id: streamLocalePackIds,
                },
                load_full_content_for_all_lessons: true,
            },
            url: `${window.ENDPOINT_ROOT}/api/lesson_streams.json`,
            'FrontRoyal.ApiErrorHandler': {
                background: true,
            },
        });
        expect(storeStreamForOfflineUse.mock.calls.length).toEqual(streams.length);
        streams.forEach(s => expect(storeStreamForOfflineUse).toHaveBeenCalledWith(s, injector));
    }

    async function setupCurrentlyAvailableStreams(streams) {
        db.publishedStreams.bulkPut(
            streams.map(stream => ({
                ...stream,
                all_content_stored: 1,
            })),
        );
    }
});
