import openTestDb from 'FrontRoyalStore/openTestDb';
import { uniqBy } from 'lodash/fp';
import storeStreamForOfflineUse from './storeStreamForOfflineUse';
import fetchImageBlobsForLesson from './fetchImageBlobsForLesson';

import 'jest-extended';

jest.mock('./fetchImageBlobsForLesson', () => jest.fn());

// skip over the delays
jest.mock('delay', () => async () => {});

describe('storeStreamForOfflineUse', () => {
    const db = openTestDb();
    const frontRoyalStore = { retryAfterHandledError: fn => fn(db) };
    const Lesson = {};
    const $http = {};
    const injector = {
        get: name => ({ Lesson, $http, frontRoyalStore }[name]),
    };
    const stream = {
        id: 'stream_id',
        lessons: [
            { id: 'lesson_1', frames: [{ id: 'frame_1' }] },
            { id: 'lesson_2', frames: [{ id: 'frame_2' }] },
        ],
    };
    const imagesForLesson1 = [
        { url: 'some/image', data_url: 'imageDataUrl' },
        { url: 'some/shared/image', data_url: 'imageDataUrl' },
    ];
    const imagesForLesson2 = [
        { url: 'some/other/image', data_url: 'imageDataUrl' },
        { url: 'some/shared/image', data_url: 'imageDataUrl' },
    ];
    beforeEach(() => {
        fetchImageBlobsForLesson.mockReset();
        fetchImageBlobsForLesson.mockImplementation(lesson => {
            let images = [];
            if (lesson.id === 'lesson_1') {
                images = imagesForLesson1;
            }

            if (lesson.id === 'lesson_2') {
                images = imagesForLesson2;
            }

            return Promise.resolve(images);
        });
    });

    it('should store streams, lessons, and images', async () => {
        await storeStreamForOfflineUse(stream, injector);

        // assert that publishedLessonContent was stored
        const storedPublishedLessonContents = await db.publishedLessonContent.toArray();
        expect(storedPublishedLessonContents).toEqual([
            {
                stream_id: stream.id,
                id: stream.lessons[0].id,
                frames: stream.lessons[0].frames,
                image_urls: imagesForLesson1.map(img => img.url),
            },
            {
                stream_id: stream.id,
                id: stream.lessons[1].id,
                frames: stream.lessons[1].frames,
                image_urls: imagesForLesson2.map(img => img.url),
            },
        ]);

        // assert that fetchImageBlobsForLesson was called and the
        // returned images were stored
        const storedImages = await db.images.toArray();
        expect(storedImages).toIncludeSameMembers(uniqBy('url')(imagesForLesson1.concat(imagesForLesson2)));
        expect(fetchImageBlobsForLesson.mock.calls.length).toEqual(2);
        expect(fetchImageBlobsForLesson.mock.calls[0]).toEqual([stream.lessons[0], db, injector]);
        expect(fetchImageBlobsForLesson.mock.calls[1]).toEqual([stream.lessons[1], db, injector]);

        // assert that stream was stored
        const storedStreams = await db.publishedStreams.toArray();
        expect(storedStreams[0].id).toEqual(stream.id);
        expect(storedStreams[0].all_content_stored).toBe(1);
    });
});
