import { difference, max } from 'lodash/fp';
import { LESS_URGENT_THAN_NORMAL } from 'HttpQueue';
import storeStreamForOfflineUse from './storeStreamForOfflineUse';
import unstoreStreamForOfflineUse from './unstoreStreamForOfflineUse';

function fetchStreams(streamLocalePackIds, $http) {
    return $http({
        method: 'GET',
        url: `${window.ENDPOINT_ROOT}/api/lesson_streams.json`,
        params: {
            filters: {
                locale_pack_id: streamLocalePackIds,
                in_users_locale_or_en: true,
            },
            load_full_content_for_all_lessons: true,
        },
        headers: {
            Accept: 'application/json',
        },
        httpQueueOptions: {
            priority: LESS_URGENT_THAN_NORMAL,
        },
        'FrontRoyal.ApiErrorHandler': {
            background: true,
        },
    });
}

function fetchOutdatedStreams(streams, $http) {
    const streamParams = streams.map(stream => {
        const publishedAts = [stream.published_at].concat(stream.lessons.map(l => l.published_at));
        return {
            id: stream.id,
            content_updated_at: max(publishedAts),
            load_full_content: true,
        };
    });
    return $http({
        method: 'POST',
        url: `${window.ENDPOINT_ROOT}/api/lesson_streams/get_outdated.json`,
        data: {
            streams: streamParams,
        },
        headers: {
            Accept: 'application/json',
        },
        httpQueueOptions: {
            priority: LESS_URGENT_THAN_NORMAL,
        },
        'FrontRoyal.ApiErrorHandler': {
            background: true,
        },
    });
}

// NOTE: currentUserLocale has to be the current user's locale,
// because the api will use in_users_locale_or_en
export default async function setStreamsWithStoredContent(streamLocalePackIds, currentUserLocale, injector) {
    const frontRoyalStore = injector.get('frontRoyalStore');
    const $http = injector.get('$http');

    const streamLocalePackIdsSet = new Set(streamLocalePackIds);

    const currentlyStoredStreams = await frontRoyalStore.retryAfterHandledError(db =>
        db.publishedStreams.where({ all_content_stored: 1 }).toArray(),
    );

    const streamsToUnstore = [];
    const localePackIdsAvailableInLocale = [];
    const currentlyStoredStreamsInLocale = [];

    currentlyStoredStreams.forEach(stream => {
        if (stream.locale !== currentUserLocale || !streamLocalePackIdsSet.has(stream.locale_pack.id)) {
            streamsToUnstore.push(stream);
        } else {
            localePackIdsAvailableInLocale.push(stream.locale_pack.id);
            currentlyStoredStreamsInLocale.push(stream);
        }
    });

    const streamLocalePackIdsToFetch = difference(streamLocalePackIds, localePackIdsAvailableInLocale);
    let streamsToStore = [];
    if (streamLocalePackIdsToFetch.length > 0) {
        const response = await fetchStreams(streamLocalePackIdsToFetch, $http);
        streamsToStore = streamsToStore.concat(response.data.contents.lesson_streams);
    }

    if (currentlyStoredStreamsInLocale.length > 0) {
        const response = await fetchOutdatedStreams(currentlyStoredStreamsInLocale, $http);
        streamsToStore = streamsToStore.concat(response.data.contents.lesson_streams);
    }

    const unstorePromises = await frontRoyalStore.retryAfterHandledError(db =>
        streamsToUnstore.map(s => unstoreStreamForOfflineUse(s.id, db)),
    );
    await Promise.all(unstorePromises);

    // FIXME: it would be nicer to just call storeStreamForOfflineUse once
    // for each stream and then wait with Promise.all, but doing all that
    // work at once can lock up the UI, so we have to do them conescutively
    // instead of in parallel.  See https://trello.com/c/5yQMSVoI
    // const start = new Date();
    // console.log(`Storing ${streamsToStore.length} streams`);
    for (let i = 0; i < streamsToStore.length; i += 1) {
        const stream = streamsToStore[i];
        // eslint-disable-next-line no-await-in-loop
        await storeStreamForOfflineUse(stream, injector);
        // console.log(
        //     `stored stream ${i + 1} of ${streamsToStore.length} in ${Math.round(
        //         (new Date() - start) / 1000
        //     )} total seconds (${stream.title} / ${stream.id})`
        // );
    }
}
