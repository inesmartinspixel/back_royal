import buildApiResponse from 'buildApiResponse';

async function handleRequest(stream, config, frontRoyalStore) {
    /*
        `include_progress` will be true in this request, but we can safely ignore
        that because we will only be using this when frontRoyalStore is enabled.  When
        the store is enabled, showWithFullContentForLesson will pull the
        progress from the store anyway.
    */

    const lessonId = config.params.load_full_content_for_lesson_id;
    if (lessonId) {
        const lessonContent = await frontRoyalStore.retryAfterHandledError(db =>
            db.publishedLessonContent.where({ stream_id: stream.id, id: lessonId }).first(),
        );
        stream.lessons.forEach(lesson => {
            if (lesson.id === lessonId) {
                lesson.frames = lessonContent.frames;
            }
        });
    }

    return buildApiResponse(config, {
        lesson_streams: [stream],
    });
}

export default async function streamInterceptor(config, $injector) {
    const isGet = config.method?.toLowerCase() === 'get';
    if (!isGet) {
        return null;
    }

    const match = config.url.match(/\/api\/lesson_streams\/(\w{8}-\w{4}-\w{4}-\w{4}-\w{12})\.json/);
    const streamId = match && match[1];

    if (!streamId) {
        return null;
    }

    const frontRoyalStore = $injector.get('frontRoyalStore');

    if (!frontRoyalStore.enabled) {
        return null;
    }

    // If the stream is available offline, then serve it from the store rather
    // than making an http call.
    const stream = await frontRoyalStore.retryAfterHandledError(db =>
        db.publishedStreams.where({ id: streamId }).first(),
    );
    if (stream?.all_content_stored !== 1) {
        return null;
    }

    return () => handleRequest(stream, config, frontRoyalStore);
}
