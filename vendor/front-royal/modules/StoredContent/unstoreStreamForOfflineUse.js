function urlsFromLessons(lessons) {
    return lessons.map(l => l.image_urls).flat();
}

function lessonsForStream(streamId, db) {
    return db.publishedLessonContent.where('[stream_id+id]').between([streamId, 0], [streamId, 'z']);
}

async function unstoreImages(streamId, db) {
    // get all the image urls from this stream
    const lessons = await lessonsForStream(streamId, db).toArray();
    const urlsFromStream = urlsFromLessons(lessons);

    // find any other lessons that use any of the same images as this stream
    const lessonsWithImages = await db.publishedLessonContent
        .where('image_urls')
        .anyOf(urlsFromStream)
        .distinct()
        .toArray();
    const otherLessonsWithImages = lessonsWithImages.filter(lesson => lesson.stream_id !== streamId);
    const urlsInOtherLessons = new Set(urlsFromLessons(otherLessonsWithImages));

    // Look for any urls that are also used in other lessons outside of this
    // stream.  After removing those from the list, delete the rest of the
    // images from the store.
    const urlsToDelete = urlsFromStream.filter(url => !urlsInOtherLessons.has(url));
    return db.images.where('url').anyOf(urlsToDelete).delete();
}

export default async function unstoreStreamForOfflineUse(streamId, db) {
    return db.transaction('rw', db.publishedStreams, db.publishedLessonContent, db.images, async () => {
        // before deleting
        await unstoreImages(streamId, db);

        return Promise.all([
            // mark stream as being no longer available offline
            db.publishedStreams.where({ id: streamId }).modify({ all_content_stored: 0 }),

            // delete all the published lesson content records
            lessonsForStream(streamId, db).delete(),
        ]);
    });
}
