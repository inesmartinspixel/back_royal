import { uniq } from 'lodash/fp';

async function fetchBlob(url, injector) {
    const $http = injector.get('$http');
    const HttpQueue = injector.get('HttpQueue');

    /*
        I'm not totally sure why we decided to
        set shouldQueue here to `true`.  Maybe we are just
        trying to avoid eating up all of the browser's network
        connections.  But, if it was just that, then it seems
        like we could have just done the image loads in sequence
        rather than in parallel.  Shoulda written a comment when
        we first did it.
    */
    return $http({
        method: 'GET',
        url,
        responseType: 'blob',
        imageRequest: true, // custom key used in OfflineModeManager#ensureDefaultStreamsAvailableOffline
        httpQueueOptions: {
            shouldQueue: true,
            priority: -10,
        },
    }).then(
        response => ({
            url,
            data: response.data,
        }),
        errorResponse => {
            // Since we are using shouldQueue, and this request
            // will not be handled by FrontRoyalApiErrorHandler (since it's
            // not an api request), we need to handle errors and unfreeze
            // the queue.
            HttpQueue.unfreezeAfterError(errorResponse.config);

            // We still throw the error in order to stop the loading of offline
            // content (which makes sense assuming this is a network error. FIXME:
            // see https://trello.com/c/Cw5ajLHA where we will make this tighter)
            // This error will be caught in OfflineModeManager#ensureDefaultStreamsAvailableOffline
            // FIXME: it seemed like maybe this was still ending up being logged as an
            // unhandled rejection even though we've handled it.
            // FIXME: all the images that have been sent to the queue are going to have
            // to try and fail.  It might be nice to cancel them once the first one
            // has failed.
            throw errorResponse;
        },
    );
}

export default async function fetchImageBlobsForLesson(lessonAttrs, db, injector) {
    const Lesson = injector.get('Lesson');

    // FIXME:  it would be nice to do this without a playerViewModel, and
    // without componentized iguana models, but
    // it seemed like it would require quite a bit of refactoring.
    const lesson = Lesson.new(lessonAttrs);
    const playerViewModel = lesson.createPlayerViewModel({
        logProgress: false,
    });

    const urls = await playerViewModel.getAllImageUrlsForLesson();
    const uniqUrls = uniq(urls);
    const result = [];

    await Promise.all(
        uniqUrls.map(async url => {
            const fetchedImage = await fetchBlob(url, injector);
            result.push(fetchedImage);
        }),
    );

    return result;
}
