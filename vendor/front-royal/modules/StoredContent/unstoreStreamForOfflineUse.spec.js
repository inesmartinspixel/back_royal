import openTestDb from 'FrontRoyalStore/openTestDb';
import { map } from 'lodash/fp';
import unstoreStreamForOfflineUse from './unstoreStreamForOfflineUse';

import 'jest-extended';

describe('unstoreStreamForOfflineUse', () => {
    const db = openTestDb();
    const stream = {
        id: 'stream_id',
        lessons: [
            { id: 'lesson_1', frames: [{ id: 'frame_1' }] },
            { id: 'lesson_2', frames: [{ id: 'frame_2' }] },
        ],
        all_content_stored: 1,
    };

    it('should remove lessons and images and mark stream as unavailable', async () => {
        await setupRecordsInDb(db, stream);

        await unstoreStreamForOfflineUse(stream.id, db);

        const storedStream = await db.publishedStreams.where({ id: stream.id }).first();
        expect(storedStream.all_content_stored).toEqual(0);

        const publishedLessonContent = await db.publishedLessonContent.toArray();
        expect(map('id')(publishedLessonContent)).toEqual(['another_lesson']);

        const images = await db.images.toArray();
        expect(map('url')(images)).toIncludeSameMembers([
            'image/shared/between/two/lessons',
            'image/for/another/lesson',
        ]);
    });
});

async function setupRecordsInDb(db, stream) {
    await db.publishedStreams.bulkPut([stream, { id: 'another_stream', all_content_stored: 1 }]);
    await db.publishedLessonContent.bulkPut([
        {
            id: 'lesson',
            stream_id: stream.id,
            frames: {},
            image_urls: ['image/for/just/this/lesson', 'image/shared/between/two/lessons'],
        },
        {
            id: 'another_lesson',
            stream_id: 'another_stream',
            frames: {},
            image_urls: ['image/for/another/lesson', 'image/shared/between/two/lessons'],
        },
    ]);
    await db.images.bulkPut([
        {
            url: 'image/for/just/this/lesson',
        },
        {
            url: 'image/shared/between/two/lessons',
        },
        {
            url: 'image/for/another/lesson',
        },
    ]);
}
