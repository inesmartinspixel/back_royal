import { useEffect, useState, useCallback, useContext } from 'react';
import AngularContext from 'AngularContext';
import './GooglePlaces.scss';
import memoizeOne from 'memoize-one';
import { isEqual } from 'lodash/fp';
import attachGooglePlacesToInput from './attachGooglePlacesToInput';

const memoizeOptions = memoizeOne(options => options, isEqual);

export default function useGooglePlaces({ autoCompleteOptions, onPlaceChanged }) {
    const $injector = useContext(AngularContext);

    const [input, setInput] = useState();

    const inputRef = useCallback(node => {
        let _input;

        if (!node) {
            return;
        }

        // Find the input element that we want to attach google places to
        if (node.nodeName.toLowerCase() === 'input') {
            _input = node;
        } else {
            _input = node.querySelector('input');
        }
        if (!_input) {
            throw new Error('No input element found in referenced DOM element.');
        }

        setInput(_input);
    }, []);

    const [addListener, setAddListener] = useState();
    const _autoCompleteOptions = memoizeOptions(autoCompleteOptions);

    useEffect(() => {
        if (input) {
            const _addListener = attachGooglePlacesToInput({
                $injector,
                input,
                options: {
                    autoCompleteOptions: _autoCompleteOptions,
                },
            });

            // We have to wrap _addListener in a function.  Othewise,
            // react will call _addListener() rather than assigning it
            setAddListener(() => _addListener);
        } else {
            setAddListener(null);
        }
    }, [$injector, input, _autoCompleteOptions]);

    useEffect(() => {
        let cancelListener;
        if (addListener) {
            cancelListener = addListener(onPlaceChanged);
        }
        return () => cancelListener && cancelListener();
    }, [addListener, onPlaceChanged]);

    return inputRef;
}
