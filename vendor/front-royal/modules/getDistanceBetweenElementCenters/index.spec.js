import getDistanceBetweenElementCenters from './index';

describe('<getDistanceBetweenElementCenters>', () => {
    let elemA;
    let elemB;

    beforeEach(() => {
        elemA = document.createElement('div');
        elemB = document.createElement('div');

        // We have to mock this for JSDOM
        elemA.getBoundingClientRect = () => ({
            top: 0,
            height: 100,
            left: 0,
            width: 100,
        });
        elemB.getBoundingClientRect = () => ({
            top: 100,
            height: 100,
            left: 100,
            width: 100,
        });
    });

    it('should return distance (hypotenuse) between the centers of two elements', () => {
        expect(Math.floor(getDistanceBetweenElementCenters(elemA, elemB))).toEqual(141);
    });

    it('should return undefined on error', () => {
        elemB.getBoundingClientRect = () => {
            throw new Error('foobar');
        };
        expect(getDistanceBetweenElementCenters(elemA, elemB)).toBe(undefined);
    });
});
