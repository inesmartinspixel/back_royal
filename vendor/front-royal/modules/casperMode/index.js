import ClientStorage from 'ClientStorage';

// Some Casper specs do a hard reload or a redirect, in which case we would need to
// preserve the casperMode query parameter. Rather than doing that, let us set it in
// localStorage and not worry about it any more. We do not set it in localStorage
// in the Casper code because we would need to have loaded the page already by the time we
// could do that.
if (window.localStorage && window.location.search.match('casperMode')) {
    window.localStorage.setItem('casperMode', 'true'); // localStorage only stores strings
}

export default function casperMode() {
    return ClientStorage && ClientStorage.getItem('casperMode') === 'true';
}
