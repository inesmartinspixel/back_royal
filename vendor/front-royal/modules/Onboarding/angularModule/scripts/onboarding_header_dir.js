import angularModule from 'Onboarding/angularModule/scripts/onboarding_module';
import template from 'Onboarding/angularModule/views/onboarding_header.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('onboardingHeader', [
    '$injector',
    function factory($injector) {
        const ConfigFactory = $injector.get('ConfigFactory');

        return {
            restrict: 'E',
            templateUrl,
            transclude: true,
            link(scope) {
                const config = ConfigFactory.getSync();

                scope.logoClass = () => {
                    return config.isQuantic() ? 'img_logo-quantic' : 'img_logo-smartly';
                };
            },
        };
    },
]);
