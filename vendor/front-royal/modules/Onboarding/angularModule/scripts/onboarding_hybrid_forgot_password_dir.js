import angularModule from 'Onboarding/angularModule/scripts/onboarding_module';
import { setupBrandNameProperties } from 'AppBrandMixin';
import template from 'Onboarding/angularModule/views/onboarding.hybrid.forgot_password.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

import logoSmall from 'images/onboarding/Logo-Small@2x.png';
import logoSmallQuantic from 'images/onboarding/Logo-Small@2x_quantic.png';
import miyaMiyaLogo from 'images/miyamiya/miyamiya-logo.png';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('onboardingHybridForgotPassword', [
    '$injector',
    function factory($injector) {
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
        const $window = $injector.get('$window');
        const ConfigFactory = $injector.get('ConfigFactory');

        return {
            restrict: 'E',
            templateUrl,
            scope: {},
            link(scope) {
                NavigationHelperMixin.onLink(scope);

                const config = ConfigFactory.getSync();
                setupBrandNameProperties($injector, scope, config);

                // Normally we would use the `setupScopeProperties` method from the `AppBrandMixin` to
                // setup this scope property, but because of the Miya Miya app we special case the custom
                // handling here rather than generalizing it inside of the `setupScopeProperties` method
                // in the `AppBrandMixin` for fear of messing things up in other areas of the app.
                Object.defineProperty(scope, 'logoSmall', {
                    get() {
                        if ($window.CORDOVA?.miyaMiya) {
                            return miyaMiyaLogo;
                        }
                        if (config.isQuantic()) {
                            return logoSmallQuantic;
                        }
                        return logoSmall;
                    },
                });
            },
            controller: [
                '$scope',
                $scope => {
                    $scope.goBack = () => {
                        $window.history.back();
                    };

                    $scope.focus = () => {
                        $('.onboarding__form form input:first-of-type').bind('focusin focus', e => {
                            e.preventDefault();
                        });
                        $('.onboarding__form form input:first-of-type').focus();
                        $scope.recoverPasswordFormOptions = {
                            debounce: {
                                default: 0,
                                blur: 0,
                            },
                        };
                    };
                },
            ],
        };
    },
]);
