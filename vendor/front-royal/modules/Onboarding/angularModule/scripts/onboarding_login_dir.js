import angularModule from 'Onboarding/angularModule/scripts/onboarding_module';
import { setupBrandNameProperties } from 'AppBrandMixin';
import { DEFAULT_SIGNUP_LOCATION } from 'SignupLocations';
import template from 'Onboarding/angularModule/views/onboarding_login.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('onboardingLogin', [
    '$injector',
    function factory($injector) {
        const scopeTimeout = $injector.get('scopeTimeout');
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
        const ConfigFactory = $injector.get('ConfigFactory');

        return {
            restrict: 'E',
            templateUrl,
            scope: {},
            link(scope) {
                NavigationHelperMixin.onLink(scope);
                setupBrandNameProperties($injector, scope, ConfigFactory.getSync());

                scopeTimeout(
                    scope,
                    () => {
                        scope.ready = true;
                    },
                    10,
                );

                // Initial settings.
                scope.step = 4;
                scope.ready = false;

                scope.isReady = () => scope.ready && scope.step === 4;

                scope.goToPageJoinPage = () => {
                    scope.loadUrl(DEFAULT_SIGNUP_LOCATION);
                };
            },
        };
    },
]);
