import 'AngularSpecHelper';
import 'Onboarding/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import onboardingHybridQuestionaryLocales from 'Onboarding/locales/onboarding/onboarding_hybrid_questionary-en.json';

setSpecLocales(onboardingHybridQuestionaryLocales);

describe('Onboarding.OnboardingHyridQuestionary', () => {
    let SpecHelper;
    let elem;
    let scope;
    let ClientStorage;
    let $timeout;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Onboarding', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            ClientStorage = $injector.get('ClientStorage');
            $timeout = $injector.get('$timeout');
        });

        SpecHelper.stubConfig();
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        const renderer = SpecHelper.renderer();
        renderer.render('<onboarding-hybrid-questionary></onboarding-hybrid-questionary>');
        elem = renderer.elem;
        scope = elem.isolateScope();

        // To deal with the jQuery calls -- http://stackoverflow.com/a/29608746/1747491
        elem.appendTo(document.body);

        $timeout.flush();
    }

    it('should have four onboarding questions', () => {
        render();
        expect(scope.questions.length).toBe(4);
    });

    it('should set answers to onboarding questions in locale storage', () => {
        render();
        jest.spyOn(ClientStorage, 'setItem').mockImplementation(() => {});
        SpecHelper.click(elem, '.questionary__buttons_wrapper button:first');

        const questions = {};
        questions[scope.questions[0]] = false;
        const questionsString = JSON.stringify(questions);
        expect(ClientStorage.setItem).toHaveBeenCalledWith('onboardingQuestions', questionsString);
    });

    // FIXME: Can't get onComplete to call
    // it('should transition to register when last card', function() {
    //     render();
    //     jest.spyOn(scope, 'transitionToRegister');
    //     scope.questions = [{
    //         image: 'foo/bar',
    //         question: 'test question'
    //     }];
    //     scope.$digest();
    //     $timeout.flush();
    //     SpecHelper.click(elem, '.questionary__buttons_wrapper button:first');
    //     $timeout.flush();
    //     expect(scope.transitionToRegister).toHaveBeenCalled();
    // });
});
