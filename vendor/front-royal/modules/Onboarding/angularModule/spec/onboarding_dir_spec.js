import 'AngularSpecHelper';
import 'Onboarding/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import onboardingLocales from 'Onboarding/locales/onboarding/onboarding-en.json';
import onboardingDialogContentLocales from 'Onboarding/locales/onboarding/onboarding_dialog_content-en.json';

setSpecLocales(onboardingLocales, onboardingDialogContentLocales);

describe('Onboarding.Onboarding', () => {
    let SpecHelper;
    let elem;
    let scope;
    let $timeout;
    let Modal;
    let ClientStorage;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Onboarding', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            $timeout = $injector.get('$timeout');
            Modal = $injector.get('Onboarding.Modal');
            ClientStorage = $injector.get('ClientStorage');
        });

        SpecHelper.stubConfig();
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        const renderer = SpecHelper.renderer();
        renderer.render('<onboarding></onboarding>');
        elem = renderer.elem;
        scope = elem.isolateScope();
        $timeout.flush();
    }

    it('should show intro modal', () => {
        jest.spyOn(Modal, 'show').mockImplementation(() => {});
        render();
        expect(Modal.show).toHaveBeenCalled();
    });

    it('should have four questions', () => {
        render();
        expect(scope.questions.length).toBe(4);
    });

    it('should get next question when yes or no is clicked', () => {
        render();
        jest.spyOn(scope, 'getNextQuestion').mockImplementation(() => {});
        SpecHelper.click(elem, '.onboarding_questionary .btn-confirm');
        expect(scope.getNextQuestion).toHaveBeenCalled();
    });

    describe('function getNextQuestion', () => {
        it('should increment the current question', () => {
            render();
            SpecHelper.click(elem, '.onboarding_questionary .btn-confirm');
            expect(scope.currentQuestion).toBe(1);
        });

        it('should store answers in locale storage', () => {
            render();
            jest.spyOn(ClientStorage, 'setItem').mockImplementation(() => {});
            SpecHelper.click(elem, '.onboarding_questionary .btn-confirm');

            const questions = {};
            questions[scope.questions[0]] = true;
            const questionsString = JSON.stringify(questions);
            expect(ClientStorage.setItem).toHaveBeenCalledWith('onboardingQuestions', questionsString);
        });

        // FIXME: Need to figure out how to mock the $animate leave event
        // it('should load register route if done with the last question', function() {
        //     render();
        //     jest.spyOn(scope, 'loadRoute');
        //     scope.currentQuestion = scope.questions.length - 1;
        //     SpecHelper.click(elem, '.onboarding_questionary .btn-confirm');
        //     $animate.broadcast('leave');
        //     $timeout.flush();
        //     expect(scope.loadRoute).toHaveBeenCalledWith('/register');
        // });
    });
});
