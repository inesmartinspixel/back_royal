import 'AngularSpecHelper';
import 'Onboarding/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import onboardingHybridLoginLocales from 'Onboarding/locales/onboarding/onboarding_hybrid_login-en.json';
import privacyTermsFooterLocales from 'Navigation/locales/navigation/privacy_terms_footer-en.json';

setSpecLocales(onboardingHybridLoginLocales, privacyTermsFooterLocales);

describe('Onboarding.OnboardingHyridLogin', () => {
    let SpecHelper;
    let elem;
    let scope;
    let $routeParams;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Onboarding', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            $routeParams = $injector.get('$routeParams');
        });

        SpecHelper.stubConfig();
        SpecHelper.stubDirective('signInForm');
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        const renderer = SpecHelper.renderer();
        renderer.render('<onboarding-hybrid-login></onboarding-hybrid-login>');
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    it('should show back when coming from the register or start routes', () => {
        $routeParams.previous = 'register';
        render();
        expect(scope.showBack).toBe(true);
    });

    it('should not show back when arriving after a logout', () => {
        $routeParams.previous = 'foobar';
        render();
        expect(scope.showBack).toBe(false);
    });

    it('should go back to register when back caret is pressed and previously came from register', () => {
        $routeParams.previous = 'register';
        render();
        jest.spyOn(scope, 'loadRoute').mockImplementation(() => {});
        SpecHelper.click(elem, '.onboarding__back__button');
        expect(scope.loadRoute).toHaveBeenCalledWith('/onboarding/hybrid/register');
    });

    it('should go back to last screen of the onboarding when back caret pressed and previously came from onboarding', () => {
        $routeParams.previous = 'start';
        render();
        jest.spyOn(scope, 'loadRoute').mockImplementation(() => {});
        SpecHelper.click(elem, '.onboarding__back__button');
        expect(scope.loadRoute).toHaveBeenCalledWith('/onboarding/hybrid?finished');
    });

    it('should show the privacy and terms footer', () => {
        render();
        SpecHelper.expectElement(elem, 'privacy-terms-footer');
    });
});
