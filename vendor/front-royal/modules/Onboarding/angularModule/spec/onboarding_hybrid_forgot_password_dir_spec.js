import 'AngularSpecHelper';
import 'Onboarding/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import onboardingHybridForgotPasswordLocales from 'Onboarding/locales/onboarding/onboarding_hybrid_forgot_password-en.json';
import forgotPasswordLocales from 'Authentication/locales/authentication/forgot_password-en.json';

setSpecLocales(onboardingHybridForgotPasswordLocales, forgotPasswordLocales);

describe('Onboarding.OnboardingHyridForgotPassword', () => {
    let SpecHelper;
    let elem;
    let $window;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Onboarding', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            $window = $injector.get('$window');
            SpecHelper.stubConfig();
        });

        SpecHelper.stubConfig();
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        const renderer = SpecHelper.renderer();
        renderer.render('<onboarding-hybrid-forgot-password></onboarding-hybrid-forgot-password>');
        elem = renderer.elem;
    }

    it('should go back when back caret is pressed', () => {
        render();
        jest.spyOn($window.history, 'back').mockImplementation(() => {});
        SpecHelper.click(elem, '.onboarding__back__button');
        expect($window.history.back).toHaveBeenCalled();
    });
});
