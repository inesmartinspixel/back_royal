import 'AngularSpecHelper';
import 'Onboarding/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import onboardingForgotPasswordLocales from 'Onboarding/locales/onboarding/onboarding_forgot_password-en.json';
import forgotPasswordLocales from 'Authentication/locales/authentication/forgot_password-en.json';

setSpecLocales(onboardingForgotPasswordLocales, forgotPasswordLocales);

describe('Onboarding.OnboardingForgotPassword', () => {
    let SpecHelper;
    let elem;
    let scope;
    let $timeout;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Onboarding', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            $timeout = $injector.get('$timeout');
        });

        SpecHelper.stubConfig();
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        const renderer = SpecHelper.renderer();
        renderer.render('<onboarding-forgot-password></onboarding-forgot-password>');
        elem = renderer.elem;
        scope = elem.isolateScope();
        $timeout.flush();
    }

    it('should initialize to step four to set proper style classes', () => {
        render();
        expect(scope.step).toBe(4);
    });

    it('should go to sign-in when link is clicked', () => {
        render();
        jest.spyOn(scope, 'loadRoute').mockImplementation(() => {});
        SpecHelper.click(elem, '.onboarding_footer__register a');
        $timeout.flush();
        expect(scope.loadRoute).toHaveBeenCalledWith('/sign-in');
    });
});
