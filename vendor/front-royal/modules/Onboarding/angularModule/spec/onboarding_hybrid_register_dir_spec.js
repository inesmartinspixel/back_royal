import 'AngularSpecHelper';
import 'Onboarding/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import onboardingHybridRegisterLocales from 'Onboarding/locales/onboarding/onboarding_hybrid_register-en.json';
import privacyTermsFooterLocales from 'Navigation/locales/navigation/privacy_terms_footer-en.json';

setSpecLocales(onboardingHybridRegisterLocales, privacyTermsFooterLocales);

describe('Onboarding.OnboardingHyridRegister', () => {
    let SpecHelper;
    let elem;
    let scope;
    let $routeParams;
    let $timeout;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Onboarding', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            $routeParams = $injector.get('$routeParams');
            $timeout = $injector.get('$timeout');
        });

        SpecHelper.stubConfig();
        SpecHelper.stubDirective('signUpForm');
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        const renderer = SpecHelper.renderer();
        renderer.render('<onboarding-hybrid-register></onboarding-hybrid-register>');
        elem = renderer.elem;
        scope = elem.isolateScope();
        $timeout.flush();
    }

    it('should call function to startInitialAnimations if animate param is true', () => {
        $routeParams.animate = true;
        render();
        expect(scope.animated).toBe(true);
    });

    it('should not call function to startInitialAnimations if no animate param', () => {
        $routeParams.animate = false;
        render();
        expect(scope.animated).toBe(false);
    });

    it('should show the privacy and terms footer', () => {
        render();
        SpecHelper.expectElement(elem, 'privacy-terms-footer');
    });
});
