import 'AngularSpecHelper';
import 'Onboarding/angularModule';

import { DEFAULT_SIGNUP_LOCATION } from 'SignupLocations';
import setSpecLocales from 'Translation/setSpecLocales';
import onboardingLoginLocales from 'Onboarding/locales/onboarding/onboarding_login-en.json';

setSpecLocales(onboardingLoginLocales);

describe('Onboarding.OnboardingLogin', () => {
    let SpecHelper;
    let elem;
    let scope;
    let $timeout;
    let $injector;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Onboarding', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            __$injector => {
                $injector = __$injector;
                SpecHelper = $injector.get('SpecHelper');
                $timeout = $injector.get('$timeout');
            },
        ]);

        SpecHelper.stubConfig();
        SpecHelper.stubDirective('signInForm');
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        const renderer = SpecHelper.renderer();
        renderer.render('<onboarding-login></onboarding-login>');
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    it('should initialize step to four to add proper style class', () => {
        render();
        expect(scope.step).toBe(4);
    });

    it('should go to register page if link is clicked', () => {
        render();
        jest.spyOn(scope, 'loadUrl').mockImplementation(() => {});
        $timeout.flush();
        SpecHelper.click(elem, '.onboarding_footer__register a');
        $timeout.flush();
        expect(scope.loadUrl).toHaveBeenCalledWith(DEFAULT_SIGNUP_LOCATION);
    });
});
