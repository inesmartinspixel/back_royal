import { omit } from 'lodash/fp';

/*

    This function is useful when you have a component
    that uses a few things from the passed-in `props`
    and then wants to pass everything else through
    to a sub-component.

    Example:

    function MyComponent(props) {
        const [{
            label
        }, passthrough] = extractProperties(props, 'label');

        return <>
            {label}: <SomeThirdPartyComponent {...props} />
        </>;
    }

*/
export default function extractProperties(props, ...names) {
    const extracted = {};
    names.forEach(name => {
        extracted[name] = props[name];
    });
    const passthrough = omit(names)(props);

    return [extracted, passthrough];
}
