import 'AngularSpecHelper';
import 'Reports/angularModule';
import reportsLocales from 'Reports/locales/reports/reports-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(reportsLocales);

describe('Reports::ReportDateRangeInputDir', () => {
    let SpecHelper;
    let $injector;
    let report;
    let elem;
    let scope;
    let now;

    angular.mock.module.sharedInjector();

    beforeAll(() => {
        angular.mock.module('FrontRoyal.Reports', 'SpecHelper');

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            SpecHelper = $injector.get('SpecHelper');
            const Report = $injector.get('Report');
            const ReportDateRange = $injector.get('ReportDateRange');

            report = Report.new();

            now = new Date('2015/12/3 12:00:00');
            Object.defineProperty(ReportDateRange.prototype, 'now', {
                value: now,
            });
        });
    });

    it('should show label', () => {
        render();
        SpecHelper.expectElementText(elem, '.current', report.dateRange.label);
    });

    describe('focused', () => {
        it('should show and hide dropdown as expected', () => {
            // this needs to happen in the dom since it relies
            // on click handlers on the body
            SpecHelper.inDom(stage => {
                render();
                stage.append(elem);
                SpecHelper.expectNoElement(elem, '.date-range-dropdown');

                // clicking the current div should show the dropdown
                SpecHelper.click(elem, '.current');
                SpecHelper.expectElement(elem, '.date-range-dropdown');

                // clicking within the dropdown should not hide it
                SpecHelper.click(elem, '.date-range-dropdown div:eq(0)');
                SpecHelper.expectElement(elem, '.date-range-dropdown');

                // clicking outside the dropdown should hide it
                $('body').click();
                SpecHelper.expectNoElement(elem, '.date-range-dropdown');

                // bring it back
                SpecHelper.click(elem, '.current');
                SpecHelper.expectElement(elem, '.date-range-dropdown');

                // clicking the current div should hide it
                SpecHelper.click(elem, '.current');
                SpecHelper.expectNoElement(elem, '.date-range-dropdown');
            });
        });

        it('should destroy click handler on body', () => {
            SpecHelper.inDom(stage => {
                render();
                stage.append(elem);
                jest.spyOn(scope, 'spyOnMe').mockImplementation(() => {});
                $('body').click();
                expect(scope.spyOnMe).toHaveBeenCalled();
                expect(scope.spyOnMe.mock.calls.length).toBe(1);
                scope.$destroy();
                $('body').click();
                expect(scope.spyOnMe.mock.calls.length).toBe(1);
            });
        });
    });

    describe('last type', () => {
        it('should work', () => {
            renderAndOpenDropdown();
            SpecHelper.updateTextInput(elem, '[name="value"]', '10');
            assertApply();
            expect(report.dateRange.value).toBe(10);
            expect(report.dateRange.start_day).toBeUndefined();
            expect(report.dateRange.finish_day).toBeUndefined();
        });
    });

    describe('since type', () => {
        it('should work', () => {
            renderAndOpenDropdown();
            SpecHelper.updateSelectize(elem, '[name="type"]', 'since');
            setDateTo1stVisibleOnCalendar('.date-range-start');
            assertApply();
            expect(report.dateRange.start_day).toEqual('2015-11-01');
            expect(report.dateRange.finish_day).toBeUndefined();
        });
    });

    describe('between type', () => {
        it('should work', () => {
            renderAndOpenDropdown();
            SpecHelper.updateSelectize(elem, '[name="type"]', 'between');
            setDateTo1stVisibleOnCalendar('.date-range-start');
            setDateTo1stVisibleOnCalendar('.date-range-finish');
            assertApply();
            expect(report.dateRange.start_day).toEqual('2015-11-01');
            expect(report.dateRange.finish_day).toEqual('2015-11-29');
        });
    });

    function assertApply() {
        expect(scope.focused).toBe(true);
        // SpecHelper.click(elem, 'button.apply');
        SpecHelper.submitForm(elem);
        expect(scope.focused).toBe(false);
    }

    function openDropdown() {
        scope.focused = true;
        scope.$digest();
    }

    function setDateTo1stVisibleOnCalendar(selector) {
        setDateToDay(selector, 0);
    }

    function setDateToDay(selector, index) {
        const inputWrapper = elem.find(selector);
        SpecHelper.click(inputWrapper, '.dropdown-toggle');
        SpecHelper.click(inputWrapper, '.datetimepicker .day', index);
    }

    function renderAndOpenDropdown() {
        render();
        openDropdown();
    }

    function render() {
        const renderer = SpecHelper.renderer();
        renderer.scope.report = report;
        renderer.render('<report-date-range-input ng-model="report.date_range"></report-date-range-input>');
        elem = renderer.elem;
        scope = renderer.childScope;
    }
});
