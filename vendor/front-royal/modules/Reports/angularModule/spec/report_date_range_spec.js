import 'AngularSpecHelper';
import 'Reports/angularModule';
import reportsLocales from 'Reports/locales/reports/reports-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(reportsLocales);

describe('Reports::ReportDateRange', () => {
    let $injector;
    let ReportDateRange;
    let reportDateRange;
    let now;
    let $dateFilter;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Reports', 'SpecHelper');

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            ReportDateRange = $injector.get('ReportDateRange');
            $dateFilter = $injector.get('$filter')('date');

            now = new Date();
            Object.defineProperty(ReportDateRange.prototype, 'now', {
                value: now,
            });

            reportDateRange = ReportDateRange.new();
        });
    });

    it('should have reasonable defaults', () => {
        reportDateRange = ReportDateRange.new();
        expect(reportDateRange.type).toEqual('last');
        expect(reportDateRange.value).toEqual(30);
        expect(reportDateRange.unit).toEqual('day');
    });

    describe('unit setter', () => {
        it('should default to day', () => {
            reportDateRange.unit = 'week';
            reportDateRange.unit = 'disallowedValue';
            expect(reportDateRange.unit).toEqual('day');
        });

        describe('with type=last', () => {
            it('should call _convertValue', () => {
                reportDateRange.type = 'last';
                reportDateRange.unit = 'day';
                jest.spyOn(reportDateRange, '_convertValue').mockReturnValue(42);
                reportDateRange.value = 17;
                reportDateRange.unit = 'week';
                expect(reportDateRange._convertValue).toHaveBeenCalledWith(17, 'day', 'week');
                expect(reportDateRange.value).toBe(42);
            });
        });
    });

    describe('type setter', () => {
        describe('from last to since', () => {
            it('should set the startTime', () => {
                reportDateRange.type = 'last';
                reportDateRange.unit = 'day';
                reportDateRange.value = 1;

                // Note: this will fail when run in the 24 hours after a DST switchover
                // during that period of time, "1 day ago" isn't the same as "24 hours ago",
                // so the test will fail. Since we don't often run tests on Sundays, we're gonna
                // let this slide for now.
                const expectedStartTimestamp = now.getTime() - 24 * 60 * 60 * 1000;
                reportDateRange.type = 'since';
                expect(reportDateRange.startTime.getTime()).toEqual(expectedStartTimestamp);
            });
        });
        describe('from last to between', () => {
            it('should set the startTime', () => {
                reportDateRange.type = 'last';
                reportDateRange.unit = 'day';
                reportDateRange.value = 1;

                const expectedStartTimestamp = now.getTime() - 24 * 60 * 60 * 1000;
                reportDateRange.type = 'between';
                expect(reportDateRange.startTime.getTime()).toEqual(expectedStartTimestamp);
            });
        });
        describe('to last', () => {
            it('should default unit to day', () => {
                reportDateRange.type = 'since';
                reportDateRange.unit = undefined;
                reportDateRange.type = 'last';
                expect(reportDateRange.unit).toEqual('day');
            });
            it('should set value based on old startTime', () => {
                reportDateRange.type = 'since';
                const _17daysago = new Date(now.getTime() - 17 * 24 * 60 * 60 * 1000);
                reportDateRange.startTime = _17daysago;
                reportDateRange.type = 'last';
                expect(reportDateRange.unit).toEqual('day');
                expect(reportDateRange.value).toEqual(17);
            });
        });
        describe('to between', () => {
            it('should set the finishTime to now', () => {
                reportDateRange.type = 'last';
                expect(reportDateRange.finish_day).toBeUndefined();
                reportDateRange.type = 'between';
                expect(reportDateRange.finish_day).toEqual($dateFilter(now, 'yyyy-MM-dd'));
            });
        });

        describe('to all', () => {
            // FIXME: add tests
        });
    });

    describe('startTime', () => {
        describe('getter', () => {
            it('should return calculate startTime with type=last', () => {
                reportDateRange.type = 'last';
                reportDateRange.value = 1;
                expect(reportDateRange.startTime.getTime()).toEqual(now.getTime() - 24 * 60 * 60 * 1000);
            });
            it('should return start time with type=between or type=since', () => {
                reportDateRange.type = 'between';
                const start = (reportDateRange.startTime = new Date('2015/07/01'));
                reportDateRange.finishTime = new Date('2015/08/01');
                expect(reportDateRange.startTime).toEqual(start);

                reportDateRange.type = 'since';
                expect(reportDateRange.startTime).toEqual(start);
            });
        });

        describe('setter', () => {
            it('should throw with type = last', () => {
                reportDateRange.type = 'last';
                const expectedErr = new Error('startTime is not settable with the "last" type.');
                expect(() => {
                    reportDateRange.startTime = new Date();
                }).toThrow(expectedErr);
            });
            it('should set start_day with type=between', () => {
                reportDateRange.type = 'between';
                const start = (reportDateRange.startTime = new Date('2015/08/01'));
                reportDateRange.startTime = start;
                expect(reportDateRange.start_day).toEqual($dateFilter(start, 'yyyy-MM-dd'));
            });
            it('should set start_day with type=since', () => {
                reportDateRange.type = 'since';
                const start = (reportDateRange.startTime = new Date('2015/08/01'));
                reportDateRange.startTime = start;
                expect(reportDateRange.start_day).toEqual($dateFilter(start, 'yyyy-MM-dd'));
            });
            it('should enforce max startTime with type=between', () => {
                reportDateRange.type = 'between';
                const origStartTime = (reportDateRange.startTime = new Date('2015/07/01'));
                reportDateRange.finishTime = new Date('2015/08/01');

                // cannot set start after finish
                reportDateRange.startTime = new Date('4999/01/01');
                expect(reportDateRange.startTime).toEqual(origStartTime);
            });
            it('should enforce max startTime with type=since', () => {
                reportDateRange.type = 'since';
                const origStartTime = (reportDateRange.startTime = new Date('2015/07/01'));

                // cannot set start after now
                reportDateRange.startTime = new Date('4999/01/01');
                expect(reportDateRange.startTime).toEqual(origStartTime);
            });
        });
    });

    describe('finishTime', () => {
        describe('getter', () => {
            it('should return now with type=last or type=since', () => {
                reportDateRange.type = 'last';
                expect(reportDateRange.finishTime).toEqual(now);
                reportDateRange.type = 'since';
                expect(reportDateRange.finishTime).toEqual(now);
            });
            it('should return finish time with type=between', () => {
                reportDateRange.type = 'between';
                reportDateRange.startTime = new Date('2015/07/01');
                const finish = (reportDateRange.finishTime = new Date('2015/08/01'));
                expect(reportDateRange.finishTime).toEqual(finish);
            });
        });

        describe('setter', () => {
            it('should throw with type != between', () => {
                reportDateRange.type = 'last';
                const expectedErr = new Error('finishTime is only settable with the "between" type.');
                expect(() => {
                    reportDateRange.finishTime = new Date();
                }).toThrow(expectedErr);
                reportDateRange.type = 'since';
                expect(() => {
                    reportDateRange.finishTime = new Date();
                }).toThrow(expectedErr);
            });
            it('should set finish_day with type=between', () => {
                reportDateRange.type = 'between';
                reportDateRange.startTime = new Date('2015/07/01');
                const finish = (reportDateRange.finishTime = new Date('2015/08/01'));
                reportDateRange.finishTime = finish;
                expect(reportDateRange.finish_day).toEqual($dateFilter(finish, 'yyyy-MM-dd'));
            });
            it('should enforce min finishTime', () => {
                reportDateRange.type = 'between';
                reportDateRange.startTime = new Date('2015/07/01');
                const origFinishTime = reportDateRange.finishTime;

                // cannot set finish before start
                reportDateRange.finishTime = new Date('2011/01/01');
                expect(reportDateRange.finishTime).toEqual(origFinishTime);
            });
            it('should enforce max finishTime', () => {
                reportDateRange.type = 'between';
                reportDateRange.startTime = new Date('2015/07/01');
                const origFinishTime = (reportDateRange.finishTime = new Date('2015/08/01'));

                // cannot set finish after now
                reportDateRange.finishTime = new Date('4999/01/01');
                expect(reportDateRange.finishTime).toEqual(origFinishTime);
            });
        });
    });

    describe('label', () => {
        it('should work with type=all', () => {
            // FIXME: add tests
        });

        it('should work with type=last', () => {
            reportDateRange.type = 'last';
            reportDateRange.value = 30;
            reportDateRange.unit = 'day';
            expect(reportDateRange.label).toEqual('Last 30 days');
        });

        it('should work with type=since', () => {
            reportDateRange.type = 'since';
            reportDateRange.startTime = new Date('2015/07/01');
            expect(reportDateRange.label).toEqual('Since Jul 1');
        });

        it('should work with type=between', () => {
            reportDateRange.type = 'between';
            reportDateRange.startTime = new Date('2015/07/01');
            reportDateRange.finishTime = new Date('2015/08/01');
            expect(reportDateRange.label).toEqual('Between Jul 1 and Aug 1');
        });
    });

    describe('_convertValue', () => {
        it('should convert from day to week', () => {
            expect(reportDateRange._convertValue(7, 'day', 'week')).toEqual(1);
            expect(reportDateRange._convertValue(8, 'day', 'week')).toEqual(1);
            expect(reportDateRange._convertValue(12, 'day', 'week')).toEqual(2);
            expect(reportDateRange._convertValue(21, 'day', 'week')).toEqual(3);
        });

        it('should convert from day to month', () => {
            expect(reportDateRange._convertValue(7, 'day', 'month')).toEqual(1);
            expect(reportDateRange._convertValue(29, 'day', 'month')).toEqual(1);
            expect(reportDateRange._convertValue(36, 'day', 'month')).toEqual(1);
            expect(reportDateRange._convertValue(59, 'day', 'month')).toEqual(2);
        });

        it('should convert from week to day', () => {
            expect(reportDateRange._convertValue(1, 'week', 'day')).toEqual(7);
            expect(reportDateRange._convertValue(3, 'week', 'day')).toEqual(21);
        });

        it('should convert from week to month', () => {
            expect(reportDateRange._convertValue(1, 'week', 'month')).toEqual(1);
            expect(reportDateRange._convertValue(4, 'week', 'month')).toEqual(1);
            expect(reportDateRange._convertValue(5, 'week', 'month')).toEqual(1);
            expect(reportDateRange._convertValue(7, 'week', 'month')).toEqual(2);
            expect(reportDateRange._convertValue(12, 'week', 'month')).toEqual(3);
        });

        it('should convert from month to week', () => {
            expect(reportDateRange._convertValue(1, 'month', 'week')).toEqual(4);
            expect(reportDateRange._convertValue(3, 'month', 'week')).toEqual(Math.round(90 / 7));
        });

        it('should convert from month to day', () => {
            expect(reportDateRange._convertValue(1, 'month', 'day')).toEqual(30);
            expect(reportDateRange._convertValue(3, 'month', 'day')).toEqual(90);
        });
    });
});
