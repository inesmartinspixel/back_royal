import 'AngularSpecHelper';
import 'Reports/angularModule';
import reportsLocales from 'Reports/locales/reports/reports-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(reportsLocales);

describe('Report::ReportsUserDir', () => {
    let elem;
    let scope;
    let SpecHelper;
    let $injector;
    let MyReport;
    let report;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Reports', 'SpecHelper');

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            SpecHelper = $injector.get('SpecHelper');
            const BaseTabularReport = $injector.get('BaseTabularReport');
            jest.spyOn($injector.get('TranslationHelper').prototype, 'get').mockImplementation(
                key => `localized ${key}`,
            );

            SpecHelper.stubConfig();
            SpecHelper.stubCurrentUser('admin');
            SpecHelper.stubDirective('reportsFilterBar');

            MyReport = BaseTabularReport.subclass(function () {
                this.addColumns([
                    ['col_a', 'keyA'],
                    ['col_b', 'keyB'],
                ]);

                this.addGroupedColumns('group1', [
                    ['col_c', 'keyC'],
                    ['col_d', 'keyD'],
                ]);

                this.addGroupedColumns('group2', [
                    ['col_e', 'keyE'],
                    ['col_f', 'keyF'],
                ]);

                this.addColumns([['col_g', 'keyG']]);

                this.addGroupedColumns('group3', [
                    ['col_h', 'keyH'],
                    ['col_i', 'keyI'],
                ]);

                this._alias = this.alias('MyReport');
                this.$$titleKey = 'titleKey';
            });

            const data = [];
            for (let i = 0; i < 42; i++) {
                data.push([1, 1, 1, 1, 1, 1, 1, 1, 1]);
            }
            data.push(['search for me', 1, 1, 1, 1, 1, 1, 1, 1]);

            report = MyReport.new({
                tabular_data: data,
            });
        });
    });

    describe('results table', () => {
        beforeEach(() => {
            render();
            scope.currentReport = report;
            scope.$digest();
        });

        it('should render headers', () => {
            const head = SpecHelper.expectElement(elem, 'thead');

            const groupHeaders = [];
            head.find('tr:eq(0) th').each((i, th) => {
                groupHeaders.push(SpecHelper.trimText($(th)));
            });
            expect(groupHeaders).toEqual(['', '', 'localized group1', 'localized group2', '', 'localized group3']);
            expect(head.find('tr:eq(0) th:eq(2)').attr('colSpan')).toEqual('2');

            const colHeaders = [];
            head.find('tr:eq(1) th').each((i, th) => {
                colHeaders.push(SpecHelper.trimText($(th)));
            });
            expect(colHeaders).toEqual([
                'localized keyA',
                'localized keyB',
                'localized keyC',
                'localized keyD',
                'localized keyE',
                'localized keyF',
                'localized keyG',
                'localized keyH',
                'localized keyI',
            ]);
        });

        it('should render rows', () => {
            expect(scope.itemsPerPage < report.tabular_data.length).toBe(true); // sanity check
            SpecHelper.expectElements(elem, 'tbody tr', scope.itemsPerPage);

            const row = [];
            elem.find('tbody tr:eq(0) td').each((i, td) => {
                row.push($(td).text());
            });

            expect(row).toEqual(['1', '1', '1', '1', '1', '1', '1', '1', '1']);
        });

        it('should render pagination controls', () => {
            SpecHelper.expectElements(elem, '.results-found', 2);
            SpecHelper.expectElementText(elem, '.results-found', `${report.tabular_data.length} results found`, 0);
        });

        it('should filter by searchValue', () => {
            scope.searchValue = 'search';
            scope.$digest();
            SpecHelper.expectElements(elem, 'tbody tr', 1);
            SpecHelper.expectElementText(elem, 'tbody tr td:eq(0)', 'search for me');

            SpecHelper.click(elem, '.search-form .btn');
            SpecHelper.expectElements(elem, 'tbody tr', scope.itemsPerPage);
        });
    });

    function render() {
        const renderer = SpecHelper.renderer();
        renderer.render('<reports-activity ></reports-activity>');
        elem = renderer.elem;
        scope = renderer.childScope;
    }
});
