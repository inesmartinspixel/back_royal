import 'AngularSpecHelper';
import 'Reports/angularModule';
import reportsLocales from 'Reports/locales/reports/reports-en.json';
import setSpecLocales from 'Translation/setSpecLocales';
import moment from 'moment-timezone';

setSpecLocales(reportsLocales);

describe('Reports::Report', () => {
    let SpecHelper;
    let $injector;
    let Report;
    let report;
    let startTime;
    let $window;
    let $timeout;

    function setDateRange(startTimeStr, finishTimeStr) {
        const dateRange = report.date_range;
        dateRange.type = 'between';
        dateRange.startTime = new Date(startTimeStr);
        dateRange.finishTime = new Date(finishTimeStr);
        startTime = moment(dateRange.startTime).utc();
    }

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Reports', 'SpecHelper');

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            SpecHelper = $injector.get('SpecHelper');
            Report = $injector.get('Report');
            $window = $injector.get('$window');
            $timeout = $injector.get('$timeout');
        });

        report = Report.new({
            date_zoom_id: 'day',
            title: 'Things',
        });
        report.time_series_data = {};
        setDateRange('2015/01/01 12:00:00', '2015/01/10 12:00:00');
        report.date_zoom_id = 'day';
    });

    describe('date_zoom_id setter', () => {
        it('should update the date range', () => {
            report.date_zoom_id = 'day';
            expect(report.dateRange.unit).toEqual('day');
            report.date_zoom_id = 'week';
            expect(report.dateRange.unit).toEqual('week');
        });
    });

    describe('initialization', () => {
        it('should should support unlimited date ranges if enabled', () => {
            Object.defineProperty(Report.prototype, 'unlimitedRangeEnabled', {
                get() {
                    return true;
                },
            });

            report = Report.new({
                date_zoom_id: 'day',
                title: 'Things',
            });
            expect(report.dateRange.unit).toBeUndefined();
            expect(report.dateRange.type).toEqual('all');
        });
    });

    describe('_getLineChartSeries', () => {
        describe('with dateZoom = day', () => {
            beforeEach(() => {
                setDateRange('2015/01/01 12:00:00', '2015/01/10 12:00:00');
                report.date_zoom_id = 'day';
                addDataPoint(startTime, 42, 'day');
                const thirdDay = moment(startTime).add(2, 'days');
                addDataPoint(thirdDay, 63, 'day');
            });

            it('should assign data to appropriate dates, setting empty ones to 0', () => {
                expect(report._getLineChartSeries().series[0].data).toEqual([42, 0, 63, 0, 0, 0, 0, 0, 0, 0]);
            });

            it('should assign appropriate dates for x-axis labels', () => {
                expect(_.invoke(report._getLineChartSeries().dates, 'toDate')).toEqual([
                    new Date('2015/01/1'),
                    new Date('2015/01/2'),
                    new Date('2015/01/3'),
                    new Date('2015/01/4'),
                    new Date('2015/01/5'),
                    new Date('2015/01/6'),
                    new Date('2015/01/7'),
                    new Date('2015/01/8'),
                    new Date('2015/01/9'),
                    new Date('2015/01/10'),
                ]);
            });
        });

        describe('with dateZoom = week', () => {
            beforeEach(() => {
                setDateRange('2015/01/01 12:00:00', '2015/01/24 12:00:00');
                report.date_zoom_id = 'week';
                addDataPoint(startTime, 42, 'week');
                const thirdWeek = moment(startTime).add(2, 'weeks');
                addDataPoint(thirdWeek, 63, 'week');
            });

            it('should assign data to appropriate dates, setting empty ones to 0', () => {
                expect(report._getLineChartSeries().series[0].data).toEqual([42, 0, 63, 0]);
            });

            it('should assign appropriate dates for x-axis labels', () => {
                expect(_.invoke(report._getLineChartSeries().dates, 'toDate')).toEqual([
                    new Date('2014/12/29'),
                    new Date('2015/01/05'),
                    new Date('2015/01/12'),
                    new Date('2015/01/19'),
                ]);
            });
        });

        describe('with dateZoom = month', () => {
            beforeEach(() => {
                setDateRange('2015/01/01 12:00:00', '2015/04/01 12:00:00');
                report.date_zoom_id = 'month';
                addDataPoint(startTime, 42, 'month');
                const thirdMonth = moment(startTime).add(2, 'months');
                addDataPoint(thirdMonth, 63, 'month');
            });

            it('should assign data to appropriate dates, setting empty ones to 0', () => {
                expect(report._getLineChartSeries().series[0].data).toEqual([42, 0, 63, 0]);
            });

            it('should assign appropriate dates for x-axis labels', () => {
                expect(_.invoke(report._getLineChartSeries().dates, 'toDate')).toEqual([
                    new Date('2015/01/01'),
                    new Date('2015/02/01'),
                    new Date('2015/03/01'),
                    new Date('2015/04/01'),
                ]);
            });
        });

        it('should handle a single data series with ungrouped data', () => {
            addDataPoint(startTime, 42, 'day');
            const dataset0 = report._getLineChartSeries().series[0];

            expect(dataset0.label).toEqual(report.title);
        });

        it('should handle multiple data series with grouped data', () => {
            addDataPoint(startTime, 42, 'day', 'group1');
            addDataPoint(startTime, 23, 'day', 'group2');
            const thirdDay = moment(startTime).add(2, 'days');
            addDataPoint(thirdDay, 63, 'day', 'group1');
            addDataPoint(thirdDay, 12, 'day', 'group2');

            const dataset0 = report._getLineChartSeries().series[0];
            const dataset1 = report._getLineChartSeries().series[1];

            expect(dataset0.data).toEqual([42, 0, 63, 0, 0, 0, 0, 0, 0, 0]);
            expect(dataset0.label).toEqual('group1');

            expect(dataset1.data).toEqual([23, 0, 12, 0, 0, 0, 0, 0, 0, 0]);
            expect(dataset1.label).toEqual('group2');
        });
    });

    describe('plotlyTimeSeriesChartData', () => {
        it('should convert series as expected', () => {
            const lineChartData = {
                dates: [moment('2015-01-01'), moment('2015-01-02')],
                series: [
                    {
                        label: 'series 1',
                        data: [1, 2],
                    },
                    {
                        label: 'series 2',
                        data: [3, 4],
                    },
                ],
            };
            const dates = _.invoke(lineChartData.dates, 'toDate');
            report.plotlyChartType = 'plotly_chart_type';
            jest.spyOn(report, '_getLineChartSeries').mockReturnValue(lineChartData);
            SpecHelper.expectEqualObjects(
                [
                    {
                        x: dates,
                        y: lineChartData.series[0].data,
                        name: 'series 1',
                        type: 'scatter',
                        line: Report.PLOTLY_LINE_CONFIGS[0],
                    },
                    {
                        x: dates,
                        y: lineChartData.series[1].data,
                        name: 'series 2',
                        type: 'scatter',
                        line: Report.PLOTLY_LINE_CONFIGS[1],
                    },
                ],
                report.plotlyTimeSeriesChartData,
            );
        });

        it('should start colors over if there are too many series', () => {
            const colorCount = Report.PLOTLY_LINE_CONFIGS.length;
            const lineChartData = {
                dates: [moment('2015-01-01'), moment('2015-01-02')],
                series: [],
            };
            for (let i = 0; i <= colorCount; i++) {
                lineChartData.series.push({
                    label: 'series',
                    data: [1, 2],
                });
            }
            jest.spyOn(report, '_getLineChartSeries').mockReturnValue(lineChartData);
            SpecHelper.expectEqual(Report.PLOTLY_LINE_CONFIGS[0], _.last(report.plotlyTimeSeriesChartData).line);
        });

        it('should handle bar charts', () => {
            const colorCount = Report.PLOTLY_LINE_CONFIGS.length;
            const lineChartData = {
                dates: [moment('2015-01-01'), moment('2015-01-02')],
                series: [],
            };
            for (let i = 0; i <= colorCount; i++) {
                lineChartData.series.push({
                    label: 'series',
                    data: [1, 2],
                });
            }
            jest.spyOn(report, '_getLineChartSeries').mockReturnValue(lineChartData);
            report.chartType = 'bar';
            SpecHelper.expectEqual('bar', _.last(report.plotlyTimeSeriesChartData).type);
            SpecHelper.expectEqual(Report.PLOTLY_BAR_CONFIGS[0], _.last(report.plotlyTimeSeriesChartData).marker);
        });

        it('should be cached and only re-created if time_series_data changes', () => {
            addDataPoint(startTime, 42, 'day');
            const origData = report.plotlyTimeSeriesChartData;
            expect(report.plotlyTimeSeriesChartData).toBe(origData);
            report.time_series_data = {};
            addDataPoint(startTime, 42, 'day');
            const newData = report.plotlyTimeSeriesChartData;
            expect(newData).not.toBe(origData);
        });
    });

    describe('getFilterOptions', () => {
        let $httpBackend;
        const options = [
            {
                text: 'a',
                value: 'a',
            },
            {
                text: 'b',
                value: 'b',
            },
        ];

        beforeEach(() => {
            $httpBackend = $injector.get('$httpBackend');
        });

        it('should work with a user that has hasSuperReportsAccess', () => {
            const callback = expectGet(`${$window.ENDPOINT_ROOT}/api/reports/filter_options.json?report_type=Report`);
            Report.getFilterOptions('some_filter', {
                hasSuperReportsAccess: true,
            }).then(callback);
            $httpBackend.flush();
            expect(callback).toHaveBeenCalledWith(options);
        });

        it('should work with an institutional reports viewer', () => {
            const callback = expectGet(
                `${$window.ENDPOINT_ROOT}/api/reports/filter_options.json?institution_id=my_institution&report_type=Report`,
            );
            Report.getFilterOptions('some_filter', {
                hasSuperReportsAccess: false,
                institutions: [
                    {
                        id: 'my_institution',
                    },
                ],
            }).then(callback);
            $httpBackend.flush();
            expect(callback).toHaveBeenCalledWith(options);
        });

        it('should not reload if called again for the same user', () => {
            SpecHelper.stubEventLogging();

            const callback = expectGet(`${$window.ENDPOINT_ROOT}/api/reports/filter_options.json?report_type=Report`);
            Report.getFilterOptions('some_filter', {
                id: 'user_id',
                hasSuperReportsAccess: true,
            }).then(callback);
            $httpBackend.flush();
            expect(callback).toHaveBeenCalledWith(options);

            callback.mockClear();
            Report.getFilterOptions('some_filter', {
                id: 'user_id',
                hasSuperReportsAccess: true,
            }).then(callback);
            $timeout.flush();
            expect(callback).toHaveBeenCalledWith(options);
        });

        it('should reload if called with a different user', () => {
            let callback = expectGet(`${$window.ENDPOINT_ROOT}/api/reports/filter_options.json?report_type=Report`);
            Report.getFilterOptions('some_filter', {
                id: 'user_id',
                hasSuperReportsAccess: true,
            }).then(callback);
            $httpBackend.flush();
            expect(callback).toHaveBeenCalledWith(options);

            callback.mockClear();
            callback = expectGet(`${$window.ENDPOINT_ROOT}/api/reports/filter_options.json?report_type=Report`);
            Report.getFilterOptions('some_filter', {
                id: 'another_user_id',
                hasSuperReportsAccess: true,
            }).then(callback);
            $httpBackend.flush();
            expect(callback).toHaveBeenCalledWith(options);
        });

        function expectGet(url) {
            const callback = jest.fn();

            $httpBackend.expectGET(url).respond({
                contents: {
                    filter_options: [
                        {
                            filter_type: 'some_filter',
                            options,
                        },
                    ],
                },
            });
            return callback;
        }
    });

    function addDataPoint(mDate, value, dateZoomId, group) {
        // mDate is a moment instance
        const unit = dateZoomId === 'week' ? 'isoWeek' : dateZoomId;
        const timestamp = Math.floor(mDate.startOf(unit).valueOf() / 1000);
        let key;
        if (!group) {
            key = timestamp;
        } else {
            key = JSON.stringify([group, timestamp]);
        }
        report.time_series_data[key] = value;
    }
});
