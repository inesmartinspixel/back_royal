import 'AngularSpecHelper';
import 'Reports/angularModule';
import reportsLocales from 'Reports/locales/reports/reports-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(reportsLocales);

describe('Report::ReportsOverviewDir', () => {
    let elem;
    let scope;
    let SpecHelper;
    let $injector;
    let Report;
    let GenericListFilter;
    let ThingsReport;
    let OtherThingsReport;
    let $location;
    let HasCurrentReportMixin;
    let ErrorLogService;
    let resolveGetFilterOptions;
    let $rootScope;
    let $timeout;
    let $q;
    let Institution;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Reports', 'FrontRoyal.Users', 'FrontRoyal.Institutions', 'SpecHelper');

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            Report = $injector.get('Report');
            SpecHelper = $injector.get('SpecHelper');
            $location = $injector.get('$location');
            const ListFilter = $injector.get('ListFilter');
            const OrderedHash = $injector.get('OrderedHash');
            ErrorLogService = $injector.get('ErrorLogService');
            $rootScope = $injector.get('$rootScope');
            $timeout = $injector.get('$timeout');
            $q = $injector.get('$q');
            HasCurrentReportMixin = $injector.get('HasCurrentReportMixin');
            Institution = $injector.get('Institution');

            const currentUser = SpecHelper.stubCurrentUser();
            Object.defineProperty(currentUser, 'hasSuperReportsAccess', {
                value: true,
                configurable: true,
            });

            GenericListFilter = ListFilter.subclass(function () {
                this.alias('GenericListFilter');
                this.description = 'have Property';
                this.column = 'property';
            });

            jest.spyOn(Report, 'getFilterOptions').mockImplementation(() =>
                $q(_resolve => {
                    resolveGetFilterOptions = () => {
                        _resolve([
                            {
                                value: 'a',
                                text: 'A',
                            },
                            {
                                value: 'b',
                                text: 'B',
                            },
                            {
                                value: 'c',
                                text: 'C',
                            },
                        ]);
                    };
                }),
            );

            Report.GROUP_BY_KEYS = {
                prop1: 'filter_group_by_role',
                prop2: 'filter_group_by_group',
                prop3: 'filter_group_by_sign_up_code',
            };

            ThingsReport = Report.subclass(function () {
                this.$$titleKey = 'report_title_placeholder';
                this.alias('ThingsReport');
                this.availableFilterKlasses = [GenericListFilter];
                this.availableGroupByIdentifiers = ['prop1', 'prop2', 'prop3'];
            });

            OtherThingsReport = Report.subclass(function () {
                this.$$titleKey = 'report_title_placeholder';
                this.alias('OtherThingsReport');
            });

            Report.$$reportTypes = OrderedHash.create([
                ['ThingsReport', ThingsReport],
                ['OtherThingsReport', OtherThingsReport],
            ]);

            const methodFake = ((original, _scope) => {
                original(_scope, 'ThingsReport');
            }).bind(HasCurrentReportMixin, HasCurrentReportMixin.onLink);

            jest.spyOn(HasCurrentReportMixin, 'onLink').mockImplementation(methodFake);

            SpecHelper.stubEventLogging();

            SpecHelper.stubConfig();

            // spyOn(Report, 'getFilterOptions').mockReturnValue($q.when());
        });
    });

    describe('initialize', () => {
        it('should assign the initial report', () => {
            render();
            expect(scope.currentReport.title).toBe('Report');
            expect(scope.currentReport.date_range.type).toEqual('last');
            expect(scope.currentReport.date_range.value).toEqual(30);
            expect(scope.currentReport.date_range.unit).toEqual('day');
        });
    });

    describe('report configuration', () => {
        it('should allow for selecting a report type', () => {
            render();
            SpecHelper.updateSelectize(elem, '[name="selected_report_type"]', OtherThingsReport.alias());
            expect(scope.currentReport.title).toBe('Report');
        });

        it('should allow for setting the date zoom', () => {
            render();
            SpecHelper.updateSelect(elem, '[name="date_zoom_id"]', 'week');
            expect(scope.currentReport.date_zoom_id).toEqual('week');
            expect(scope.currentReport.dateZoom.id).toEqual('week');
        });

        it('should allow for setting the group bys', () => {
            render();
            SpecHelper.updateSelectize(elem, '[name="group_bys_select"]', ['prop1', 'prop3']);
            expect(scope.currentReport.group_bys).toEqual(['prop1', 'prop3']);
        });

        it('should have a date range input', () => {
            render();
            const input = SpecHelper.expectElement(elem, 'report-date-range-input');
            expect(input.controller('ngModel').$modelValue).toBe(scope.currentReport.date_range);
        });

        describe('filters', () => {
            it('should be disabled until options are loaded', () => {
                render();
                SpecHelper.expectElementDisabled(elem, '.filter .selectize-control input', true, 0);
                expect(Report.getFilterOptions).toHaveBeenCalledWith('GenericListFilter', $rootScope.currentUser);
                resolveGetFilterOptions();
                $timeout.flush();
                SpecHelper.expectElementDisabled(elem, '.filter .selectize-control input', false, 0);
            });

            it('should be settable', () => {
                render();
                resolveGetFilterOptions();
                $timeout.flush();
                SpecHelper.updateSelectize(elem, '.filter selectize', 'b');
                expect(scope.currentReport.filters.length).toEqual(1);
                expect(scope.currentReport.filters[0].asJson()).toEqual({
                    filter_type: 'GenericListFilter',
                    value: ['b'],
                });
            });
        });

        describe('with institutional reports viewer', () => {
            beforeEach(() => {
                Object.defineProperty($rootScope.currentUser, 'hasSuperReportsAccess', {
                    value: false,
                });
                $rootScope.currentUser.institutions = [
                    Institution.new({
                        id: 'my_institution',
                    }),
                ];
            });

            it('should hide unavailable filters', () => {
                GenericListFilter.availableToInstitutionalReportsViewers = true;
                render();
                SpecHelper.expectElement(elem, '.filter', 0);

                GenericListFilter.availableToInstitutionalReportsViewers = false;
                render();
                SpecHelper.expectNoElement(elem, '.filter');
            });

            it('should hide unavailable group bys', () => {
                render();
                SpecHelper.expectNoElement(elem, '[name="group_bys_select"]');
            });

            it('should force a value for the institutional filter', () => {
                render();
                expect(scope.currentReport.filters.length).toEqual(1);
                expect(scope.currentReport.filters[0].asJson()).toEqual({
                    filter_type: 'InstitutionFilter',
                    value: ['my_institution'],
                });
            });
        });
    });

    describe('reload', () => {
        let TransientClientStorage;

        beforeEach(() => {
            TransientClientStorage = $injector.get('TransientClientStorage');
            render();
            jest.spyOn($location, 'search').mockImplementation(() => {});
            jest.spyOn(TransientClientStorage, 'setItem').mockImplementation(() => {});
            jest.spyOn(scope.currentReport, 'save').mockImplementation(() =>
                $q(resolve => {
                    resolve({
                        meta: {
                            reporting_maintenance: 'false',
                        },
                    });
                }),
            );
            SpecHelper.click(elem, '[name="reload"]');
        });

        it('should save report to query param', () => {
            expect($location.search).toHaveBeenCalledWith('report_data', scope.currentReport.toJson());
        });

        it('should save report to TransientClientStorage', () => {
            expect(TransientClientStorage.setItem).toHaveBeenCalledWith(
                scope.currentReport.report_type,
                scope.currentReport.toJson(),
            );
        });

        it('should save the report', () => {
            expect(scope.currentReport.save).toHaveBeenCalledWith();
        });
    });

    describe('with persisted report searches', () => {
        let report;
        let TransientClientStorage;
        beforeEach(() => {
            report = ThingsReport.new();
            TransientClientStorage = $injector.get('TransientClientStorage');
            jest.spyOn(ThingsReport.prototype, 'save').mockImplementation(() =>
                $q(resolve => {
                    resolve({
                        meta: {
                            reporting_maintenance: 'false',
                        },
                    });
                }),
            );
            report.addFilter(GenericListFilter);
        });

        it('should create a report from the query param', () => {
            jest.spyOn($location, 'search').mockReturnValue({
                report_data: report.toJson(),
            });

            // errors parsing json are captured.  Throw these to
            // make it easier to debug
            jest.spyOn(ErrorLogService, 'notify').mockImplementation(e => {
                throw e;
            });
            render();
            expect(scope.currentReport.isA(ThingsReport)).toBe(true);
            expect(scope.currentReport.filters[0].isA(GenericListFilter)).toBe(true);
            expect(ThingsReport.prototype.save).toHaveBeenCalled();
        });

        it('should create a report from the TransientClientStorage', () => {
            jest.spyOn(TransientClientStorage, 'getItem').mockReturnValue(report.toJson());

            // errors parsing json are captured.  Throw these to
            // make it easier to debug
            jest.spyOn(ErrorLogService, 'notify').mockImplementation(e => {
                throw e;
            });
            render();
            expect(scope.currentReport.isA(ThingsReport)).toBe(true);
            expect(scope.currentReport.filters[0].isA(GenericListFilter)).toBe(true);
            expect(ThingsReport.prototype.save).toHaveBeenCalled();
        });
    });

    describe('maintenance mode handling', () => {
        it('should display special messaging on maintenance mode', () => {
            jest.spyOn(ThingsReport.prototype, 'save').mockImplementation(() =>
                $q(resolve => {
                    resolve({
                        meta: {
                            reporting_maintenance: 'true',
                        },
                    });
                }),
            );
            render();
            SpecHelper.click(elem, '[name="reload"]');
            SpecHelper.expectElementText(
                elem,
                '.empty-placeholder-section',
                'Reports are currently undergoing routine maintenance. Please try again shortly! Refresh',
            );
        });
    });

    function render() {
        const renderer = SpecHelper.renderer();
        renderer.render(
            "<reports-overview available-report-types=\"['ThingsReport', 'OtherThingsReport']\"></reports-overview>",
        );
        elem = renderer.elem;
        scope = renderer.childScope;
    }
});
