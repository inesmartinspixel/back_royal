import 'AngularSpecHelper';
import 'Reports/angularModule';
import reportsLocales from 'Reports/locales/reports/reports-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(reportsLocales);

describe('Reports::ListFilter', () => {
    let $injector;
    let Report;
    let $q;
    let MyListFilter;
    let $timeout;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Reports', 'SpecHelper');

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            Report = $injector.get('Report');
            $q = $injector.get('$q');
            $timeout = $injector.get('$timeout');
            const ListFilter = $injector.get('ListFilter');

            MyListFilter = ListFilter.subclass(function () {
                this.alias('MyListFilter');
            });
        });
    });

    describe('ensureOptions', () => {
        let resolveGetFilterOptions;

        beforeEach(() => {
            jest.spyOn(Report, 'getFilterOptions').mockImplementation(() =>
                $q(_resolve => {
                    resolveGetFilterOptions = _resolve;
                }),
            );
        });
        it('should load up some options', () => {
            const callback = jest.fn();
            MyListFilter.ensureOptions('ActiveUsersReport', 'user').then(callback);
            expect(Report.getFilterOptions).toHaveBeenCalledWith('MyListFilter', 'user');
            resolveGetFilterOptions('result');
            $timeout.flush();
            expect(callback).toHaveBeenCalledWith('result');
            expect(MyListFilter.options).toEqual('result');
        });
    });
});
