import moment from 'moment-timezone';
import 'AngularSpecHelper';
import 'Reports/angularModule';

describe('Reports::BaseTabularReport', () => {
    let $injector;
    let MyReport;
    let $filter;

    beforeEach(() => {
        window.IGNORE_TRANSLATION_ERRORS = true;

        angular.mock.module('FrontRoyal.Reports', 'SpecHelper');

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            $filter = $injector.get('$filter');
            const BaseTabularReport = $injector.get('BaseTabularReport');

            MyReport = BaseTabularReport.subclass(function () {
                this._alias = this.alias('MyReport');
                this.$$titleKey = 'titleKey';
            });
        });
    });

    afterEach(() => {
        window.IGNORE_TRANSLATION_ERRORS = false;
    });

    describe('addColumns', () => {
        it('should work', () => {
            jest.spyOn(MyReport, 'addGroupedColumns').mockImplementation(() => {});

            MyReport.addColumns([
                ['col_a', 'keyA'],
                ['col_b', 'keyB'],
            ]);

            // calls MyReport.addGroupedColumns for each entry
            expect(MyReport.addGroupedColumns).toHaveBeenCalledTimes(2);
            expect(MyReport.addGroupedColumns.mock.calls[0]).toEqual([undefined, [['col_a', 'keyA']]]);
            expect(MyReport.addGroupedColumns.mock.calls[1]).toEqual([undefined, [['col_b', 'keyB']]]);
        });
    });

    describe('addGroupedColumns', () => {
        it('should work', () => {
            jest.spyOn(MyReport, '_addFieldFieldConfigs');
            jest.spyOn(MyReport, '_addColumnGroup');
            jest.spyOn(MyReport, '_addEntriesToColumns').mockImplementation(() => {});

            const entries = [
                ['col_a', 'keyA'],
                ['col_b', 'keyB'],
            ];
            const titleKey = 'groupKey';
            MyReport.addGroupedColumns(titleKey, entries);

            expect(MyReport._addFieldFieldConfigs).toHaveBeenCalledWith(entries);
            expect(MyReport._addColumnGroup).toHaveBeenCalledWith(entries, `reports.reports.${titleKey}`);
            expect(MyReport._addEntriesToColumns).toHaveBeenCalledWith(entries, `reports.reports.${titleKey}`);
        });

        it('should pass undefined as groupTitle if no titleKey', () => {
            jest.spyOn(MyReport, '_addFieldFieldConfigs');
            jest.spyOn(MyReport, '_addColumnGroup');
            jest.spyOn(MyReport, '_addEntriesToColumns').mockImplementation(() => {});

            const entries = [
                ['col_a', 'keyA'],
                ['col_b', 'keyB'],
            ];
            MyReport.addGroupedColumns(undefined, entries);

            expect(MyReport._addFieldFieldConfigs).toHaveBeenCalledWith(entries);
            expect(MyReport._addColumnGroup).toHaveBeenCalledWith(entries, undefined);
            expect(MyReport._addEntriesToColumns).toHaveBeenCalledWith(entries, undefined);
        });
    });

    describe('addHiddenCsvOnlyColumns', () => {
        it('should work', () => {
            jest.spyOn(MyReport, '_addFieldFieldConfigs');
            jest.spyOn(MyReport, '_addEntriesToColumns').mockImplementation(() => {});

            expect(MyReport.COLUMN_GROUPS).toBeUndefined();
            MyReport.addHiddenCsvOnlyColumns(['col_a', 'col_b']);

            const entries = [
                ['col_a', 'col_a', undefined, expect.any(Function)],
                ['col_b', 'col_b', undefined, expect.any(Function)],
            ];
            expect(MyReport._addFieldFieldConfigs).toHaveBeenCalledWith(entries);
            expect(MyReport._addEntriesToColumns).toHaveBeenCalledWith(entries);
            expect(MyReport.COLUMN_GROUPS).toBeUndefined(); // should be unchanged
        });
    });

    describe('addHiddenColumns', () => {
        it('should work', () => {
            jest.spyOn(MyReport, '_addFieldFieldConfigs').mockImplementation(() => {});

            expect(MyReport.COLUMNS).toBeUndefined();
            expect(MyReport.COLUMN_GROUPS).toBeUndefined();

            MyReport.addHiddenColumns(['col_a', 'col_b']);

            expect(MyReport._addFieldFieldConfigs).toHaveBeenCalledWith([['col_a'], ['col_b']]);
            expect(MyReport.COLUMNS).toBeUndefined(); // should be unchanged
            expect(MyReport.COLUMN_GROUPS).toBeUndefined(); // should be unchanged
        });
    });

    describe('_addColumnGroup', () => {
        it('should work', () => {
            const entries = [
                ['col_a', 'keyA'],
                ['col_b', 'keyB'],
            ];
            expect(MyReport.COLUMN_GROUPS).toBeUndefined();

            MyReport._addColumnGroup(entries, 'groupTitle');

            expect(MyReport.COLUMN_GROUPS).toEqual([
                {
                    title: 'groupTitle',
                    size: entries.length,
                },
            ]);
        });

        it('should default column group title to an empty string if no group title is provided', () => {
            const entries = [
                ['col_a', 'keyA'],
                ['col_b', 'keyB'],
            ];
            expect(MyReport.COLUMN_GROUPS).toBeUndefined();

            MyReport._addColumnGroup(entries);

            expect(MyReport.COLUMN_GROUPS).toEqual([
                {
                    title: '',
                    size: entries.length,
                },
            ]);
        });
    });

    describe('_addEntriesToColumns', () => {
        it('should work', () => {
            function showIfFunction() {
                // noop
            }
            const entries = [
                ['col_a', 'keyA', undefined, showIfFunction],
                ['col_b', 'keyB', undefined, showIfFunction],
            ];
            expect(MyReport.COLUMNS).toBeUndefined();

            MyReport._addEntriesToColumns(entries, 'groupTitle');

            expect(
                _.map(MyReport.COLUMNS, col => _.pick(col, 'key', 'title', 'csvTitle', 'formatter', 'showIf')),
            ).toEqual([
                {
                    key: 'col_a',
                    title: 'reports.reports.keyA',
                    csvTitle: 'groupTitle - reports.reports.keyA',
                    formatter: MyReport.formatters.K, // defaults to K
                    showIf: showIfFunction,
                },
                {
                    key: 'col_b',
                    title: 'reports.reports.keyB',
                    csvTitle: 'groupTitle - reports.reports.keyB',
                    formatter: MyReport.formatters.K, // defaults to K
                    showIf: showIfFunction,
                },
            ]);
        });

        it('should default csvTitle to title if no groupTitle is provided', () => {
            const entries = [
                ['col_a', 'keyA'],
                ['col_b', 'keyB'],
            ];
            expect(MyReport.COLUMNS).toBeUndefined();

            MyReport._addEntriesToColumns(entries);

            expect(
                _.map(MyReport.COLUMNS, col => _.pick(col, 'key', 'title', 'csvTitle', 'formatter', 'showIf')),
            ).toEqual([
                {
                    key: 'col_a',
                    title: 'reports.reports.keyA',
                    csvTitle: 'reports.reports.keyA',
                    formatter: MyReport.formatters.K, // defaults to K
                    showIf: undefined,
                },
                {
                    key: 'col_b',
                    title: 'reports.reports.keyB',
                    csvTitle: 'reports.reports.keyB',
                    formatter: MyReport.formatters.K, // defaults to K
                    showIf: undefined,
                },
            ]);
        });

        it('should set column formatter if provided', () => {
            const entries = [
                ['col_a', 'keyA', 'number'],
                ['col_b', 'keyB', 'date'],
            ];
            expect(MyReport.COLUMNS).toBeUndefined();

            MyReport._addEntriesToColumns(entries, 'groupTitle');

            expect(
                _.map(MyReport.COLUMNS, col => _.pick(col, 'key', 'title', 'csvTitle', 'formatter', 'showIf')),
            ).toEqual([
                {
                    key: 'col_a',
                    title: 'reports.reports.keyA',
                    csvTitle: 'groupTitle - reports.reports.keyA',
                    formatter: MyReport.formatters.number,
                    showIf: undefined,
                },
                {
                    key: 'col_b',
                    title: 'reports.reports.keyB',
                    csvTitle: 'groupTitle - reports.reports.keyB',
                    formatter: MyReport.formatters.date,
                    showIf: undefined,
                },
            ]);
        });

        it('should throw error if no formatter found', () => {
            const entries = [['col_a', 'keyA', 'invalidFormatter']];
            expect(() => {
                MyReport._addEntriesToColumns(entries, 'groupTitle');
            }).toThrow(new Error('No formatter found for "invalidFormatter"'));
        });
    });

    describe('_addFieldFieldConfigs', () => {
        it('should work', () => {
            const entries = [
                ['col_a', 'keyA'],
                ['col_b', 'keyB'],
            ];
            expect(MyReport.FIELD_CONFIG).toBeUndefined();

            MyReport._addFieldFieldConfigs(entries);

            expect(MyReport.FIELD_CONFIG).toEqual(entries);
            expect(MyReport.DEFAULT_FIELDS).toEqual(['col_a', 'col_b']);
            expect(MyReport.INDEXES_FOR_COLUMNS).toEqual({
                col_a: 0,
                col_b: 1,
            });
            expect(MyReport.TITLE_KEYS_FOR_COLUMN).toEqual({
                col_a: 'keyA',
                col_b: 'keyB',
            });
            expect(MyReport.prototype.TITLE_KEYS_FOR_COLUMN).toEqual({
                col_a: 'keyA',
                col_b: 'keyB',
            });
            expect(MyReport.prototype.INDEXES_FOR_COLUMNS).toEqual({
                col_a: 0,
                col_b: 1,
            });
        });
    });

    describe('getCsvColumns', () => {
        it('should work', () => {
            const myReport = MyReport.new();
            jest.spyOn(myReport, 'getColumns').mockImplementation(() => {});

            myReport.getCsvColumns();

            const forCsv = true;
            expect(myReport.getColumns).toHaveBeenCalledWith(undefined, forCsv);
        });
    });

    describe('getColumns', () => {
        it('should work', () => {
            MyReport.addColumns([
                ['col_a', 'keyA'], // should be returned
                ['col_b', 'keyB'], // should be returned
            ]);
            MyReport.addHiddenCsvOnlyColumns(['col_c']); // should NOT be returned
            MyReport.addHiddenColumns(['user_id']); // should NOT be returned

            const myReport = MyReport.new();

            const columns = myReport.getColumns();
            expect(columns).toEqual([
                {
                    key: 'col_a',
                    title: 'reports.reports.keyA',
                    csvTitle: 'reports.reports.keyA',
                    formatter: MyReport.formatters.K,
                    showIf: undefined,
                },
                {
                    key: 'col_b',
                    title: 'reports.reports.keyB',
                    csvTitle: 'reports.reports.keyB',
                    formatter: MyReport.formatters.K,
                    showIf: undefined,
                },
            ]);
        });

        it('should return columns that have truthy showIf functions', () => {
            const aShowIf = jest.fn();
            const bShowIf = jest.fn();
            aShowIf.mockReturnValue(false);
            bShowIf.mockReturnValue(true);
            MyReport.addColumns([
                ['col_a', 'keyA', undefined, aShowIf], // should NOT be returned
                ['col_b', 'keyB', undefined, bShowIf], // should be returned
            ]);

            const myReport = MyReport.new();

            const columns = myReport.getColumns();
            expect(aShowIf).toHaveBeenCalledWith(myReport, undefined, undefined);
            expect(bShowIf).toHaveBeenCalledWith(myReport, undefined, undefined);
            expect(columns).toEqual([
                {
                    key: 'col_b',
                    title: 'reports.reports.keyB',
                    csvTitle: 'reports.reports.keyB',
                    formatter: MyReport.formatters.K,
                    showIf: bShowIf,
                },
            ]);
        });

        it('should also include hidden CSV only columns if forCsv', () => {
            MyReport.addColumns([
                ['col_a', 'keyA'], // should be returned
                ['col_b', 'keyB'], // should be returned
            ]);
            MyReport.addHiddenCsvOnlyColumns(['col_c']); // should be returned
            MyReport.addHiddenColumns(['user_id']); // should NOT be returned

            const myReport = MyReport.new();

            const forCsv = true;
            const columns = myReport.getColumns(undefined, forCsv);
            expect(columns).toEqual([
                {
                    key: 'col_a',
                    title: 'reports.reports.keyA',
                    csvTitle: 'reports.reports.keyA',
                    formatter: MyReport.formatters.K,
                    showIf: undefined,
                },
                {
                    key: 'col_b',
                    title: 'reports.reports.keyB',
                    csvTitle: 'reports.reports.keyB',
                    formatter: MyReport.formatters.K,
                    showIf: undefined,
                },
                {
                    key: 'col_c',
                    title: 'reports.reports.col_c',
                    csvTitle: 'reports.reports.col_c',
                    formatter: MyReport.formatters.K,
                    showIf: expect.any(Function),
                },
            ]);
        });
    });

    describe('getTabularRowValue', () => {
        const userId = 'some_user_id';
        const fourMonthsAgo = moment().subtract(4, 'months').toDate();

        [
            // formatter, raw, html, csv
            ['K', 'a', 'a', 'a'],
            [
                'accountIdLink',
                'email@example.com',
                `<a ng-click="gotoSection({section: 'user', userId: '${userId}'})">email@example.com</a>`,
                'email@example.com',
            ],
            ['clientType', 'mobile_app', 'Mobile App', 'Mobile App'],
            ['clientType', 'desktop', 'Desktop', 'Desktop'],
            ['clientType', 'mobile_web', 'Mobile Web', 'Mobile Web'],
            ['clientType', 'unknown', 'Unknown', 'Unknown'],
            ['clientType', null, '???', '???'],
            ['date', new Date('2016/01/01 00:00:00 +0000').getTime() / 1000, 'Jan 1 2016', '2016/01/01 12:00 am'],
            [
                'dateWithDetails',
                new Date('2016/01/01 00:00:00 +0000').getTime() / 1000,
                '<span title-full-date="1451606400">Jan 1 2016</span>',
                '2016/01/01 12:00 am',
            ],
            [
                'time',
                new Date('2016/01/01 00:00:00 +0000').getTime() / 1000,
                'Jan 1 2016 00:00:00',
                '2016/01/01 12:00 am',
            ],
            [
                'amTimeAgo',
                fourMonthsAgo.getTime() / 1000,
                '4 months ago',
                () => $filter('amDateFormat')($filter('amUtc')(fourMonthsAgo.getTime()), 'YYYY/MM/DD h:mm a'),
            ],
            ['amDurationFormat', 60, 'a minute', 60],
            ['percent', 0.42, '42%', '42%'],
            ['bool', true, '<i class="fa fa-check"></i>', true],
            ['bool', false, '-', false],
            ['number', 5, '5', '5'],
            ['number', undefined, '', ''],
            ['number', 0, '0', '0'],
        ].forEach(params => {
            const formatter = params[0];
            const raw = params[1];
            let expectedHtml = params[2];
            let expectedCsv = params[3];
            describe(`with ${params[0]} formatter and value=${params[1]}`, () => {
                it('should work', () => {
                    expectedHtml = typeof expectedHtml === 'function' ? expectedHtml() : expectedHtml;
                    expectedCsv = typeof expectedCsv === 'function' ? expectedCsv() : expectedCsv;
                    MyReport.addColumns([['col_a', 'keyA', formatter]]);

                    MyReport.addHiddenColumns(['user_id']);

                    const myReport = MyReport.new({
                        tabular_data: [[raw, userId]],
                    });
                    const row = myReport.tabular_data[0];

                    expect(myReport.getTabularRowValue(row, 'col_a', 'raw')).toEqual(raw);
                    expect(myReport.getTabularRowValue(row, 'col_a', 'html')).toEqual(expectedHtml);
                    expect(myReport.getTabularRowValue(row, 'col_a', 'csv')).toEqual(expectedCsv);
                });
            });
        });

        // withLessonLocale: {
        //     html: function(value, row, ReportKlass) {
        //         return this.csv(value, row, ReportKlass);
        //     },
        //     csv: function(value, row, ReportKlass) {
        //         var locale;
        //         // FIXME: I don't like that we reference rootScope and current USer here, but eh
        //         if ($rootScope.currentUser && $rootScope.currentUser.hasSuperReportsAccess) {
        //             locale = ReportKlass.getTabularRowValue(row, 'lesson_locale', 'raw');
        //         }
        //         return (locale && locale !== 'en') ? value + ' (' + locale + ')' : value;
        //     }
        // }
    });
});
