import 'AngularSpecHelper';
import 'Reports/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import reportsLocales from 'Reports/locales/reports/reports-en.json';

setSpecLocales(reportsLocales);

describe('Report::ReportsDir', () => {
    let scope;
    let SpecHelper;
    let $injector;
    let Report;
    let AppHeaderViewModel;
    let $q;
    let $location;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Reports', 'FrontRoyal.Users', 'SpecHelper', 'FrontRoyal.Lessons');

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            Report = $injector.get('Report');
            SpecHelper = $injector.get('SpecHelper');
            AppHeaderViewModel = $injector.get('Navigation.AppHeader.AppHeaderViewModel');
            $location = $injector.get('$location');
            $q = $injector.get('$q');

            const currentUser = SpecHelper.stubCurrentUser();
            Object.defineProperty(currentUser, 'hasSuperReportsAccess', {
                value: true,
                configurable: true,
            });

            jest.spyOn(Report, 'getFilterOptions').mockImplementation(() =>
                $q(_resolve => {
                    () => {
                        _resolve([
                            {
                                value: 'a',
                                text: 'A',
                            },
                            {
                                value: 'b',
                                text: 'B',
                            },
                            {
                                value: 'c',
                                text: 'C',
                            },
                        ]);
                    };
                }),
            );

            SpecHelper.stubConfig();
        });
    });

    describe('initialize', () => {
        it('should set the title in the header', () => {
            jest.spyOn(AppHeaderViewModel, 'setTitleHTML').mockImplementation(() => {});
            render();
            expect(AppHeaderViewModel.setTitleHTML).toHaveBeenCalledWith('REPORTS');
        });

        it('should assign the initial section and location', () => {
            jest.spyOn($location, 'search');
            render();
            expect(scope.section).toBe('overview');
            expect($location.search).toHaveBeenCalledWith('report', 'overview');
        });
    });

    function render() {
        const renderer = SpecHelper.renderer();
        renderer.render('<reports></reports>');
        scope = renderer.childScope;
    }
});
