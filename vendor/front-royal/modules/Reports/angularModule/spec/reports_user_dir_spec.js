import 'AngularSpecHelper';
import 'Reports/angularModule';

import Papa from 'papaparse';
import reportsLocales from 'Reports/locales/reports/reports-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(reportsLocales);

describe('Report::ReportsUserDir', () => {
    let elem;
    let scope;
    let SpecHelper;
    let $injector;
    let Report;
    let GenericListFilter;
    let ThingsReport;
    let OtherThingsReport;
    let $location;
    let HasCurrentReportMixin;
    let $q;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Reports', 'FrontRoyal.Users', 'SpecHelper', 'FrontRoyal.Lessons');

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            Report = $injector.get('Report');
            SpecHelper = $injector.get('SpecHelper');
            $location = $injector.get('$location');
            const ListFilter = $injector.get('ListFilter');
            const OrderedHash = $injector.get('OrderedHash');
            $q = $injector.get('$q');
            HasCurrentReportMixin = $injector.get('HasCurrentReportMixin');

            const currentUser = SpecHelper.stubCurrentUser();
            Object.defineProperty(currentUser, 'hasSuperReportsAccess', {
                value: true,
                configurable: true,
            });

            GenericListFilter = ListFilter.subclass(function () {
                this.alias('GenericListFilter');
                this.description = 'have Property';
                this.column = 'property';
            });

            jest.spyOn(Report, 'getFilterOptions').mockImplementation(() =>
                $q(_resolve => {
                    () => {
                        _resolve([
                            {
                                value: 'a',
                                text: 'A',
                            },
                            {
                                value: 'b',
                                text: 'B',
                            },
                            {
                                value: 'c',
                                text: 'C',
                            },
                        ]);
                    };
                }),
            );

            Report.GROUP_BYS = {
                prop1: 'Prop 1',
                prop2: 'Prop 2',
                prop3: 'Prop 3',
            };

            ThingsReport = Report.subclass(function () {
                this.title = 'Things';
                this.alias('ThingsReport');
                this.availableFilterKlasses = [GenericListFilter];
                this.availableGroupByIdentifiers = ['prop1', 'prop2', 'prop3'];
            });

            OtherThingsReport = Report.subclass(function () {
                this.title = 'Other Things';
                this.alias('OtherThingsReport');
            });

            Report.$$reportTypes = OrderedHash.create([
                ['ThingsReport', ThingsReport],
                ['OtherThingsReport', OtherThingsReport],
            ]);

            ((original, scope) => {
                original(scope, 'ThingsReport');
            }).bind(HasCurrentReportMixin, HasCurrentReportMixin.onLink);

            SpecHelper.stubConfig();

            // spyOn(Report, 'getFilterOptions').mockReturnValue($q.when());
        });
    });

    describe('no user', () => {
        beforeEach(() => {
            render();
        });

        it('should show placeholder message', () => {
            SpecHelper.expectElementText(
                elem,
                'p.message',
                "To use this report, search for users in the Activity Report, then click a user's email to view their details here.",
            );
        });

        it('should switch back to Activity report if button is clicked', () => {
            jest.spyOn($location, 'search').mockImplementation(() => {});
            SpecHelper.click(elem, 'a.btn');
            // integration testing?
        });
    });

    describe('exportProgressCSV', () => {
        beforeEach(() => {
            jest.spyOn(window.URL, 'revokeObjectURL').mockImplementation(() => {});
            jest.spyOn(Papa, 'unparse').mockImplementation(() => {});
            render();
        });

        it('should not fail during translation', () => {
            scope.userLessonProgressReport = $injector.get('UserLessonProgressReport').new();
            expect(() => {
                scope.exportProgressCSV();
            }).not.toThrow();
        });
    });

    describe('user section', () => {
        // test progress?
    });

    //------------------------
    // MISSING TESTS
    //------------------------

    // describe('reload', function() {

    //     // beforeEach(function() {
    //     //     render();
    //     //     jest.spyOn($location, 'search');
    //     //     jest.spyOn(scope.currentReport, 'save').mockImplementation(function() {
    //     //         return $q(function(resolve) {
    //     //             resolve({
    //     //                 meta: {
    //     //                     reporting_maintenance: 'false'
    //     //                 }
    //     //             });
    //     //         });
    //     //     });
    //     //     // SpecHelper.click(elem, '[name="reload"]'); // no longer
    //     // });

    //     // it('should save report to query param', function() {
    //     //     expect($location.search).toHaveBeenCalledWith('report_data', scope.currentReport.toJson());
    //     // });

    //     // it('should save the report', function() {
    //     //     expect(scope.currentReport.save).toHaveBeenCalledWith();
    //     // });
    // });

    // describe('course activity', function() {

    //     it('should show a section for each started course', function() {

    //     });

    //     it('should show not started courses when button is pressed', function() {

    //     });

    //     describe('course section', function() {

    //         it('should check off completed lessons', function() {

    //         });

    //         it('should show a label if completed', function() {

    //         });

    //         it('should export CSV when button is clicked', function() {

    //         });
    //     });
    // });

    // describe('maintenance mode handling', function() {
    //     it('should display special messaging on maintenance mode', function() {
    //         jest.spyOn(ThingsReport.prototype, 'save').mockImplementation(function() {
    //             return $q(function(resolve) {
    //                 resolve({
    //                     meta: {
    //                         reporting_maintenance: 'true'
    //                     }
    //                 });
    //             });
    //         });
    //         render();
    //         SpecHelper.click(elem, '[name="reload"]');
    //         SpecHelper.expectElementText(elem, '.empty-placeholder-section', 'Reports are currently undergoing routine maintenance. Please try again shortly! Refresh');
    //     });
    // });

    function render() {
        const renderer = SpecHelper.renderer();
        renderer.render(
            "<reports-user available-report-types=\"['ThingsReport', 'OtherThingsReport']\"></reports-user>",
        );
        elem = renderer.elem;
        scope = renderer.childScope;
    }
});
