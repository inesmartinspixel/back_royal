import angularModule from 'Reports/angularModule/scripts/reports_module';
import template from 'Reports/angularModule/views/reports_user_course.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('reportsUserCourse', [
    '$injector',

    function factory($injector) {
        const $rootScope = $injector.get('$rootScope');
        const TranslationHelper = $injector.get('TranslationHelper');
        const $window = $injector.get('$window');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                stream: '<',
                userLessonProgressReport: '<',
                started: '<?',
                userId: '<',
            },
            link(scope) {
                // This is needed to use Math in bindings -- http://stackoverflow.com/a/12740497/1747491
                scope.Math = window.Math;
                const translationHelper = new TranslationHelper('reports.reports');

                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                function setLessonDetails() {
                    scope.lessonDetails = {};
                    scope.streamLessonProgressMap = {};
                    scope.streamLessonStartedMap = {};
                    scope.streamLessonCompletedMap = {};
                    // Grab the needed lesson progress report rows
                    _.each(scope.stream.lessons, lesson => {
                        const lessonProgressReportRow = scope.userLessonProgressReport.rowForUserIdAndLocalePackId(
                            scope.userId,
                            lesson.localePackId,
                        );
                        const lesson_progress = lesson.lesson_progress;
                        scope.lessonDetails[lesson.localePackId] = {
                            reportRow: lessonProgressReportRow,
                            completed: !!(lesson_progress && lesson_progress.complete),
                            started: !!lesson_progress,
                        };

                        const lastLessonResetAt =
                            lessonProgressReportRow &&
                            scope.userLessonProgressReport.getTabularRowValue(
                                lessonProgressReportRow,
                                'last_lesson_reset_at',
                                'raw',
                            );
                        if (lastLessonResetAt) {
                            scope.lastLessonResetAt = Math.max(scope.lastLessonResetAt || 0, lastLessonResetAt);
                        }

                        _.each(scope.userLessonProgressReport.tabular_data, lessonProgressReportRow => {
                            if (
                                lesson.localePackId ===
                                scope.userLessonProgressReport.getTabularRowValue(
                                    lessonProgressReportRow,
                                    'locale_pack_id',
                                    'raw',
                                )
                            ) {
                                scope.streamLessonProgressMap[lesson.localePackId] = lessonProgressReportRow;
                                scope.streamLessonStartedMap[
                                    lesson.localePackId
                                ] = scope.userLessonProgressReport.getTabularRowValue(
                                    lessonProgressReportRow,
                                    'started_at',
                                    'raw',
                                );
                                scope.streamLessonCompletedMap[
                                    lesson.localePackId
                                ] = scope.userLessonProgressReport.getTabularRowValue(
                                    lessonProgressReportRow,
                                    'completed_at',
                                    'raw',
                                );
                            }
                        });
                    });
                }

                setLessonDetails();

                // FIXME: ideally, this would be true if the stream had ever been
                // completed (even if it has been reset). That would be most consistent.
                // For now, however, this is hard to do because we don;t have an etl table
                // for stream progress.  See also getStreamCompletedTimestamp below
                scope.isCompleted = () => scope.stream.complete;

                scope.hasStreamProgress = () => !!scope.stream.lesson_streams_progress;

                scope.resetStream = () => {
                    if (!$window.confirm(translationHelper.get('reset_course_confirm'))) {
                        return;
                    }

                    if (scope.stream.lesson_streams_progress) {
                        scope.stream.lesson_streams_progress.destroy().then(() => {
                            setLessonDetails();

                            // a bit of a hack, but make this show up right away
                            scope.lastLessonResetAt = new Date().getTime() / 1000;
                        });
                    }
                };

                scope.resetTimer = () => {
                    if (!$window.confirm(translationHelper.get('reset_timer_confirm'))) {
                        return;
                    }

                    scope.stream.lesson_streams_progress.time_runs_out_at = null;
                    scope.stream.lesson_streams_progress.save();
                };

                scope.getStreamStartedTimestamp = () => {
                    let started;
                    _.each(scope.streamLessonStartedMap, timestamp => {
                        if (!started || timestamp < started) {
                            started = timestamp;
                        }
                    });

                    return started;
                };

                scope.getStreamCompletedTimestamp = () => {
                    let completed;

                    // We're assuming that the completed at is the last lesson complete at.
                    // Really, we should get this from an etl table, but there is none.  See
                    // isCompleted above.
                    _.each(scope.streamLessonCompletedMap, timestamp => {
                        if (!completed || timestamp > completed) {
                            completed = timestamp;
                        }
                    });

                    return completed;
                };

                scope.getTotalTime = () => {
                    let totalTime = 0;

                    _.each(scope.streamLessonStartedMap, (startedTimestamp, i) => {
                        const completedTimestamp = scope.streamLessonCompletedMap[i];
                        if (completedTimestamp) {
                            totalTime += completedTimestamp - startedTimestamp;
                        }
                    });

                    return totalTime;
                };
            },
        };
    },
]);
