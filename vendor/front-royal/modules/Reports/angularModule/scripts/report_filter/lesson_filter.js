import angularModule from 'Reports/angularModule/scripts/reports_module';

angularModule.factory('LessonFilter', [
    '$injector',
    $injector => {
        const ListFilter = $injector.get('ListFilter');

        return ListFilter.subclass(function () {
            this.alias('LessonFilter');
            this.$$placeholderKey = 'filter_placeholder_lessons';
            this.availableToInstitutionalReportsViewers = true;
        });
    },
]);
