import angularModule from 'Reports/angularModule/scripts/reports_module';

angularModule.factory('PlaylistCompletedFilter', [
    '$injector',
    $injector => {
        const ListFilter = $injector.get('ListFilter');

        return ListFilter.subclass(function () {
            this.alias('PlaylistCompletedFilter');
            this.$$placeholderKey = 'filter_placeholder_playlists_completed';
            this.availableToInstitutionalReportsViewers = true;
        });
    },
]);
