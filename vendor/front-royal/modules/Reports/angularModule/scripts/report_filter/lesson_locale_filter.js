import angularModule from 'Reports/angularModule/scripts/reports_module';

angularModule.factory('LessonLocaleFilter', [
    '$injector',
    $injector => {
        const ListFilter = $injector.get('ListFilter');

        return ListFilter.subclass(function () {
            this.alias('LessonLocaleFilter');
            this.$$placeholderKey = 'filter_placeholder_locale';
            this.availableToInstitutionalReportsViewers = false;
        });
    },
]);
