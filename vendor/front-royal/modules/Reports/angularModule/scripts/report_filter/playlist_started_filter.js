import angularModule from 'Reports/angularModule/scripts/reports_module';

angularModule.factory('PlaylistStartedFilter', [
    '$injector',
    $injector => {
        const ListFilter = $injector.get('ListFilter');

        return ListFilter.subclass(function () {
            this.alias('PlaylistStartedFilter');
            this.$$placeholderKey = 'filter_placeholder_playlists_started';
            this.availableToInstitutionalReportsViewers = true;
        });
    },
]);
