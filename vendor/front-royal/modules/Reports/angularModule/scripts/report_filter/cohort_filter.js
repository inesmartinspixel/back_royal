import angularModule from 'Reports/angularModule/scripts/reports_module';

angularModule.factory('CohortFilter', [
    '$injector',
    $injector => {
        const ListFilter = $injector.get('ListFilter');

        return ListFilter.subclass(function () {
            this.alias('CohortFilter');
            this.$$placeholderKey = 'filter_placeholder_cohorts';
        });
    },
]);
