import angularModule from 'Reports/angularModule/scripts/reports_module';

angularModule.factory('ApplicationRegisteredFilter', [
    '$injector',
    $injector => {
        const BoolFilter = $injector.get('BoolFilter');

        return BoolFilter.subclass(function () {
            this.alias('ApplicationRegisteredFilter');
            this.$$placeholderKey = 'filter_placeholder_application_registered';
            this.availableToInstitutionalReportsViewers = false;
        });
    },
]);
