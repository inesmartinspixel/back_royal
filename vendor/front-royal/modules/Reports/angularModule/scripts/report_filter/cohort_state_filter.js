import angularModule from 'Reports/angularModule/scripts/reports_module';

angularModule.factory('CohortStatusFilter', [
    '$injector',
    $injector => {
        const ListFilter = $injector.get('ListFilter');

        return ListFilter.subclass(function () {
            this.alias('CohortStatusFilter');
            this.$$placeholderKey = 'filter_placeholder_cohort_status';
        });
    },
]);
