import StoredProgress from 'StoredProgress';
import StoredEvent from 'StoredEvent';
import { requestInterceptor, responseErrorInterceptor } from './interceptors';
import BlockedByFrontRoyalStoreError from './BlockedByFrontRoyalStoreError';

jest.mock('StoredProgress', () => ({}));
jest.mock('StoredEvent', () => ({}));

describe('interceptors', () => {
    let $injector;
    let config;
    let lessonProgressInterceptor;
    let eventInterceptor;
    let frontRoyalStore;
    let $http;
    const endpointRoot = 'http://path/to/site';

    beforeEach(() => {
        // Since these mocks are setup outside of the fdescribe,
        // we have to clear them manually (I guess?  Is there a better way to do this?)
        // lessonProgressInterceptor.mockReset();
        // eventInterceptor.mockReset();
        StoredProgress.lessonProgressInterceptor = jest.fn();
        lessonProgressInterceptor = StoredProgress.lessonProgressInterceptor;

        StoredEvent.eventInterceptor = jest.fn();
        eventInterceptor = StoredEvent.eventInterceptor;
        frontRoyalStore = {
            enabled: true,
        };
        $http = jest.fn();
        $injector = {
            get: factoryName =>
                ({
                    $window: {
                        ENDPOINT_ROOT: endpointRoot,
                    },
                    $q: Promise,
                    // report unexpected errors
                    ErrorLogService: err => {
                        throw err;
                    },
                    frontRoyalStore,
                    $http,
                }[factoryName]),
        };
        config = {
            url: `${endpointRoot}/api/something`,
        };
    });

    describe('requestInterceptor', () => {
        it('should do nothing if !frontRoyalStore.enabled', async () => {
            frontRoyalStore.enabled = false;
            expect(await requestInterceptor(config, $injector)).toBe(config);
        });

        it('should do nothing on non-api routes', () => {
            config.url = 'http://not/endpoint/root';
            expect(requestInterceptor(config, $injector)).toBe(config);

            config.url = `${endpointRoot}/notApi`;
            expect(requestInterceptor(config, $injector)).toBe(config);
        });

        describe('when running interceptors', () => {
            it('should return config if interceptors return nothing', async () => {
                lessonProgressInterceptor.mockReturnValue(null);
                eventInterceptor.mockReturnValue(null);
                const result = await requestInterceptor(config, $injector);
                expect(result).toBe(config);
                expect(lessonProgressInterceptor).toHaveBeenCalled();
                expect(eventInterceptor).toHaveBeenCalled();
            });

            it('should reject if eventInterceptor returns a handler', async () => {
                const handler = jest.fn();
                eventInterceptor.mockReturnValue(handler);
                let err;
                try {
                    await requestInterceptor(config, $injector);
                } catch (_err) {
                    err = _err;
                }

                expect(eventInterceptor).toHaveBeenCalled();
                expect(lessonProgressInterceptor).not.toHaveBeenCalled();
                expect(err.constructor).toBe(BlockedByFrontRoyalStoreError);
                expect(err.config).toBe(config);
                expect(err.handleRequest).toBe(handler);
            });

            it('should reject if an interceptor in the main list returns a handler', async () => {
                const handler = jest.fn();
                lessonProgressInterceptor.mockReturnValue(handler);
                let err;
                try {
                    await requestInterceptor(config, $injector);
                } catch (_err) {
                    err = _err;
                }
                expect(eventInterceptor).toHaveBeenCalled();
                expect(lessonProgressInterceptor).toHaveBeenCalled();
                expect(err.constructor).toBe(BlockedByFrontRoyalStoreError);
                expect(err.config).toBe(config);
                expect(err.handleRequest).toBe(handler);
            });
        });
    });

    describe('responseErrorInterceptor', () => {
        it('should handle a BlockedByFrontRoyalStore error', async () => {
            const returnValue = {};
            const handleRequest = jest.fn().mockReturnValue(Promise.resolve(returnValue));
            const err = new BlockedByFrontRoyalStoreError(handleRequest);
            const result = await responseErrorInterceptor(err, $injector);
            expect(result).toBe(returnValue);
            expect(handleRequest).toHaveBeenCalled();
        });

        it('should pass along any other error', () => {
            const $q = $injector.get('$q');
            jest.spyOn($q, 'reject').mockReturnValue('rejectedPromise');
            const errorResponse = {};
            expect(responseErrorInterceptor(errorResponse, $injector)).toBe('rejectedPromise');
            expect($q.reject).toHaveBeenCalledWith(errorResponse);
        });

        it('should retry a request if frontRoyalStore.handleError returns true', async () => {
            const returnValue = {};
            const handleRequest = jest.fn().mockReturnValue(Promise.reject(new Error('busted')));
            frontRoyalStore.handleError = jest.fn().mockReturnValue(Promise.resolve(true));
            $http.mockReturnValue(returnValue);
            const err = new BlockedByFrontRoyalStoreError(handleRequest);
            const result = await responseErrorInterceptor(err, $injector);
            expect(result).toBe(returnValue);
            expect($http).toHaveBeenCalled();
        });

        it('should not retry a request if frontRoyalStore.handleError returns true', async () => {
            const handleErrorFailure = new Error('busted');
            const handleRequest = jest.fn().mockReturnValue(Promise.reject(handleErrorFailure));
            frontRoyalStore.handleError = jest.fn().mockReturnValue(Promise.resolve(false));
            const err = new BlockedByFrontRoyalStoreError(handleRequest);
            let result;
            try {
                await responseErrorInterceptor(err, $injector);
            } catch (e) {
                result = e;
            }
            expect(result).toBe(handleErrorFailure);
            expect($http).not.toHaveBeenCalled();
        });
    });
});
