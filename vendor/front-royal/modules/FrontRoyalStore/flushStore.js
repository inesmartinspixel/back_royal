import { flushStoredLessonProgress } from 'StoredProgress';
import { flushStoredEvents } from 'StoredEvent';
import { flushStoredSentryErrors } from 'ErrorLogging';
import NetworkConnection from 'NetworkConnection';
import DisconnectedError from './DisconnectedError';

export default async function flushStore($injector) {
    // Regardless of whether or not we have actually entered offline mode,
    // we do not need to send these requests if we are offline.  We can just
    // return false.
    if (NetworkConnection.offline) {
        return false;
    }

    // These will reject with DisconnectedError if we lose the connection
    // while they are in flight (See handling of requests with
    // flushingFrontRoyalStore in front_rotal_api_error_handler)
    try {
        await Promise.all([
            flushStoredLessonProgress($injector),
            flushStoredEvents($injector),
            flushStoredSentryErrors($injector),
        ]);
    } catch (err) {
        // If we could not flush to the server, return false.  We don't treat this as an
        // error, but whoever called flushStore might want to know that we
        // didn't actually flush
        if (err.constructor === DisconnectedError) {
            return false;
        }

        throw err;
    }

    return true;
}
