import Dexie from 'dexie';
import getDexieConfig from './getDexieConfig';
import openDb from './openDb';

// port of Dexie exists method.  Need a special one where we can pass
// in dexieConfig. See https://github.com/dfahlander/Dexie.js/blob/master/src/classes/dexie/dexie-static-props.ts
function dbExists(name, dexieConfig) {
    return new Dexie(name, { addons: [], ...dexieConfig })
        .open()
        .then(db => {
            // db.close is syncronous
            db.close();
            return true;
        })
        .catch('NoSuchDatabaseError', () => false);
}

export default async function openDbIfAlreadyExists(dbTechnology) {
    let frontRoyalDbExists;
    try {
        frontRoyalDbExists = await dbExists('FrontRoyal', getDexieConfig(dbTechnology));
    } catch (err) {
        // If FF private browsing mode, for example, we will get
        // this error.  If that's the case, then it means the
        // database does not exist and we can return false here.
        if (err.name !== 'InvalidStateError') {
            throw err;
        }
    }

    if (!frontRoyalDbExists) {
        return null;
    }

    return openDb(dbTechnology);
}
