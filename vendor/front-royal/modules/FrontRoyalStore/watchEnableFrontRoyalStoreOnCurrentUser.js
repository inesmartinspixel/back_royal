export default function watchEnableFrontRoyalStoreOnCurrentUser(injector) {
    const $rootScope = injector.get('$rootScope');
    const frontRoyalStore = injector.get('frontRoyalStore');

    $rootScope.$watchGroup(['currentUser', 'currentUser.enable_front_royal_store'], () => {
        const user = $rootScope.currentUser;

        frontRoyalStore.enabled = !!user?.enable_front_royal_store;

        if (!user) {
            return;
        }

        // The current_user_interceptor also watches for changes to
        // the favorite_lesson_stream_locale_packs list and calls setStreamBookmarks
        // when necessary.
        frontRoyalStore.setStreamBookmarks(user);

        // If there is a user and the store is enabled, then store the user
        if (user.enable_front_royal_store) {
            frontRoyalStore.storeCurrentUser(user.asJson());
        }

        // If there is a user and the store is disabled, delete the
        // database.  Note that we only do this when there is a user.  We don't want
        // to do anything if there is no user.  This is important because anytime
        // the page is refreshed there will be some time before the currentUser
        // is set.
        else {
            frontRoyalStore.ensureFlushedAndDeleteDb();
        }
    });
}
