import * as userAgentHelper from 'userAgentHelper';
import casperMode from 'casperMode';

export const INDEXED_DB = 'indexedDB';
export const FAKE_INDEXED_DB = 'fake-indexeddb';
export const SQLITE_PLUGIN = 'sqlitePlugin';

export default function supportedDbTechnology() {
    // We have to explicitly check casperMode here because otherwise when
    // using the puppet debugger it will actually have indexedDb available
    if (global.RUNNING_IN_TEST_MODE || casperMode()) {
        return FAKE_INDEXED_DB;
    }

    let supportsPersist = navigator.storage && navigator.storage.persist;

    // FIXME: see this checkbox on the Offline Mode ticket:
    // Fix sqlLite: Storing images leads to a DataCloneError.  SqlLite is currently disabled.
    supportsPersist = true;

    // Hopefully we can remove (window.CORDOVA || userAgentHelper.isChrome())
    // eventually, but we've seen things fail in Safari desktop and so
    // are scared of other browsers are the moment.  See https://github.com/dfahlander/Dexie.js/issues/938
    const hasIndexedDBOnSupportedBrowser =
        window.indexedDB && (window.CORDOVA || userAgentHelper.isChrome() || userAgentHelper.isFirefox());

    // The caniuse tables report partial support for IndexedDB in Edge and IE11.
    // From the Dexie docs and from our own testing, we can see that compound indexes
    // are not supported, and we use those.  So just treating it as unsupported in these
    // browsers.
    if (userAgentHelper.isIE() || userAgentHelper.isEdge()) {
        return FAKE_INDEXED_DB;
    }

    // In offline mode, we need to be sure that our data never gets
    // wiped (see https://github.com/scottjehl/Device-Bugs/issues/64).
    // So, either we need a browser that supports persistent storage (Chrome)
    // or we need to use the sqlLite plugin (in cordova on ios).  If we are
    // in an environment without persistent storage and without sqlLite (safari)
    // then we can still store stuff in IndexedDB, but we will not allow the
    // user to enter offline mode, so there will not be an expectation of lots
    // of data being stored forever.
    if (supportsPersist && hasIndexedDBOnSupportedBrowser) {
        return INDEXED_DB;
    }
    if (window.sqlitePlugin) {
        return SQLITE_PLUGIN;
    }
    if (hasIndexedDBOnSupportedBrowser) {
        return INDEXED_DB;
    }

    return FAKE_INDEXED_DB;
}
