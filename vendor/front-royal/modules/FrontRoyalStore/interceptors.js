import { lessonProgressInterceptor } from 'StoredProgress';
import { eventInterceptor } from 'StoredEvent';
import { streamInterceptor } from 'StoredContent';
import { compact } from 'lodash/fp';
import BlockedByFrontRoyalStoreError from './BlockedByFrontRoyalStoreError';

async function runInterceptor(interceptor, config, $injector) {
    const handler = await interceptor(config, $injector);

    if (handler) {
        throw new BlockedByFrontRoyalStoreError(handler, config);
    }
}

async function runInterceptors(config, $injector) {
    const $q = $injector.get('$q');

    // Since events can be bundled into other calls, and the eventInterceptor
    // will remove them from config, we let the eventInterceptor run and finish
    // first before running any other interceptors
    await runInterceptor(eventInterceptor, config, $injector);
    let promises = [lessonProgressInterceptor, streamInterceptor].map(interceptor =>
        runInterceptor(interceptor, config, $injector),
    );

    promises = compact(promises);

    return $q.all(promises).then(() => config);
}

export function requestInterceptor(config, $injector) {
    const $window = $injector.get('$window');
    const frontRoyalStore = $injector.get('frontRoyalStore');

    // Some requests get messed up if we return a promise
    // from this interceptor (like templates, maybe?).  So,
    // we short-circuit early unless this is a request to our api.
    if (!config.url.match(`${$window.ENDPOINT_ROOT}/api`)) {
        return config;
    }

    if (!frontRoyalStore.enabled) {
        return config;
    }

    return runInterceptors(config, $injector);
}

export function responseErrorInterceptor(response, $injector) {
    const $q = $injector.get('$q');
    if (response && response.constructor === BlockedByFrontRoyalStoreError) {
        const frontRoyalStore = $injector.get('frontRoyalStore');
        const $http = $injector.get('$http');

        return response.handleRequest().catch(async err => {
            // if frontRoyalStore.handleError
            // returns true, then we should be able to handle
            // this request on a retry.  Try it again
            if (await frontRoyalStore.handleError(err)) {
                return $http(response.config);
            }
            throw err;
        });
    }
    return $q.reject(response);
}
