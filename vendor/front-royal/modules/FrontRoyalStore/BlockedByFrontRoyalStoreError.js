export default class BlockedByFrontRoyalStoreError extends Error {
    constructor(handleRequest, config) {
        super('Request blocked by FrontRoyalStore');
        this.name = 'BlockedByFrontRoyalStoreError';
        this.handleRequest = handleRequest;
        this.config = config;
        this.headers = () => ({}); // make this look like an api response so ng-token-auth doesn't freak out
    }
}
