import { INDEXED_DB, SQLITE_PLUGIN } from './supportedDbTechnology';

export default function dbHasPersistentStorage(db) {
    return [INDEXED_DB, SQLITE_PLUGIN].includes(db?.dbTechnology);
}
