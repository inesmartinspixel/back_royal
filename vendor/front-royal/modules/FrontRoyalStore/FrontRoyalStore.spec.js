import openTestDb from 'FrontRoyalStore/openTestDb';
import FrontRoyalStore from './FrontRoyalStore';
import openDbIfAlreadyExists from './openDbIfAlreadyExists';
import flushStore from './flushStore';

jest.mock('./openDbIfAlreadyExists', () => jest.fn());
jest.mock('./flushStore', () => jest.fn().mockResolvedValue());

describe('FrontRoyalStore', () => {
    const db = openTestDb();

    beforeEach(() => {
        openDbIfAlreadyExists.mockReset();
        flushStore.mockReset();
    });

    describe('getDb', () => {
        it('should initialize a database if store is enabled', async () => {
            const frontRoyalStore = new FrontRoyalStore();
            frontRoyalStore.enabled = true;
            const _db1 = await frontRoyalStore.getDb();
            expect(_db1.name).toEqual(db.name);
            expect(openDbIfAlreadyExists).not.toHaveBeenCalled();

            // We should not re-initialize the db.  We should get the
            // same one again
            openDbIfAlreadyExists.mockReset();
            const _db2 = await frontRoyalStore.getDb();
            expect(_db2).toBe(_db1);
        });

        it('should initialize a database if store is disabled but the database already exists', async () => {
            openDbIfAlreadyExists.mockReturnValue(Promise.resolve('someDb'));
            const frontRoyalStore = new FrontRoyalStore();
            frontRoyalStore.enabled = false;
            let _db = await frontRoyalStore.getDb();
            expect(_db).toEqual('someDb');
            expect(openDbIfAlreadyExists).toHaveBeenCalled();

            // After calling openDbIfAlreadyExists once, we should
            // not have to call it again
            openDbIfAlreadyExists.mockReset();
            _db = await frontRoyalStore.getDb();
            expect(_db).toEqual('someDb');
            expect(openDbIfAlreadyExists).not.toHaveBeenCalled();
        });

        it('should only make one call to openDbIfAlreadyExists if there are multiple calls to getDb', async () => {
            openDbIfAlreadyExists.mockReturnValue(Promise.resolve('someDb'));
            const frontRoyalStore = new FrontRoyalStore();
            frontRoyalStore.enabled = false;
            let _db1;
            let _db2;
            await Promise.all([
                frontRoyalStore.getDb().then(_db => {
                    _db1 = _db;
                }),
                frontRoyalStore.getDb().then(_db => {
                    _db2 = _db;
                }),
            ]);
            expect(_db1).toEqual('someDb');
            expect(_db2).toEqual('someDb');
            expect(openDbIfAlreadyExists.mock.calls.length).toBe(1);
        });

        it('should not initialize a database if store is disabled and no database already exists', async () => {
            openDbIfAlreadyExists.mockReturnValue(Promise.resolve(null));
            const frontRoyalStore = new FrontRoyalStore();
            frontRoyalStore.enabled = false;
            let _db = await frontRoyalStore.getDb();
            expect(_db).toEqual(null);
            expect(openDbIfAlreadyExists).toHaveBeenCalled();

            // After calling openDbIfAlreadyExists once, we should
            // not have to call it again
            openDbIfAlreadyExists.mockReset();
            _db = await frontRoyalStore.getDb();
            expect(_db).toEqual(null);
            expect(openDbIfAlreadyExists).not.toHaveBeenCalled();
        });

        it('should initialize a database if the store is enabled after openDbIfAlreadyExists does not open a database', async () => {
            openDbIfAlreadyExists.mockReturnValue(Promise.resolve(null));
            const frontRoyalStore = new FrontRoyalStore();
            frontRoyalStore.enabled = false;
            const _db1 = await frontRoyalStore.getDb();
            expect(_db1).toEqual(null);

            frontRoyalStore.enabled = true;
            const _db2 = await frontRoyalStore.getDb();
            expect(_db2).not.toBe(null);
        });
    });

    describe('getSingleRecord', () => {
        let ErrorLogService;
        let frontRoyalStore;

        beforeEach(() => {
            ErrorLogService = {
                notifyInProd: jest.fn(),
            };
            frontRoyalStore = new FrontRoyalStore({
                get: key => ({ ErrorLogService }[key]),
            });

            // ensure there is a db
            frontRoyalStore.enabled = true;
        });

        it('should return one record', async () => {
            const user = { id: 'userId' };
            await db.currentUsers.put(user);
            const result = await frontRoyalStore.getSingleRecord('currentUsers');
            expect(result).toEqual(user);
        });

        it('should return undefined', async () => {
            const result = await frontRoyalStore.getSingleRecord('currentUsers');
            expect(result).toBe(undefined);
        });

        it('should error if there are multiple records', async () => {
            await db.currentUsers.bulkPut([{ id: 1 }, { id: 2 }]);
            const result = await frontRoyalStore.getSingleRecord('currentUsers');
            expect(result).toBe(undefined);
            expect(ErrorLogService.notifyInProd).toHaveBeenCalledWith(
                'There are multiple records in the currentUsers table',
                null,
                {
                    extra: { ids: [1, 2] },
                },
            );
        });
    });

    describe('ensureFlushedAndDeleteDb', () => {
        it('should delete the db in such a way that it can be setup again when the store is re-enabled', async () => {
            const frontRoyalStore = getEnabledStore();
            let deleteDatabase;

            // sanity check.  initially there is a database
            await frontRoyalStore.retryAfterHandledError(_db => {
                expect(_db).not.toBeNull();
                deleteDatabase = jest.spyOn(_db, 'delete').mockResolvedValue();
            });

            // when we disabled the store and call
            // ensureFlushedAndDeleteDb, the database should
            // be deleted
            frontRoyalStore.enabled = false;
            await frontRoyalStore.ensureFlushedAndDeleteDb();
            expect(flushStore).toHaveBeenCalledWith(frontRoyalStore.injector);
            expect(deleteDatabase).toHaveBeenCalled();
            await frontRoyalStore.retryAfterHandledError(_db => {
                expect(_db).toBeNull();
            });

            // After the store is re-enabled, there should be a
            // db again
            frontRoyalStore.enabled = true;
            await frontRoyalStore.retryAfterHandledError(_db => {
                expect(_db).not.toBeNull();
            });
        });
    });

    function getEnabledStore() {
        const frontRoyalStore = new FrontRoyalStore();
        frontRoyalStore.enabled = true;
        return frontRoyalStore;
    }
});
