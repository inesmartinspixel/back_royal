export default class DisconnectedError extends Error {
    constructor(response) {
        super('Network is disconnected');
        this.response = response;
        this.name = 'DisconnectedError';
    }
}
