export default function clearAllTables(db) {
    const promises = db.tables.map(table => table.clear());
    return Promise.all(promises).then(() => db);
}
