import Dexie from 'dexie';
import 'indexeddbshim';
import getDexieConfig from './getDexieConfig';

export default function openDb(dbTechnology) {
    const dexieConfig = getDexieConfig(dbTechnology);
    const db = new Dexie('FrontRoyal', dexieConfig);
    db.dbTechnology = dbTechnology;

    // For information on setting up Dexie tables and indexes, see
    // see https://dexie.org/docs/Version/Version.stores()#schema-syntax
    // and https://dexie.org/docs/Tutorial/Design#database-versioning
    //
    // Versions here are like migrations in rails.  Each new version specifies
    // changes to the database.
    db.version(1).stores({
        streams: '&id',
        images: '&url',
        lessonProgress: '&[user_id+locale_pack_id],synced_to_server,&fr_version',
        streamProgress: '&[user_id+locale_pack_id],synced_to_server,&fr_version',
        events: '&id,client_utc_timestamp',
    });

    db.version(2).stores({
        // See ensureUserProgressFetched.  If we have ever fetched progress for this
        // user from the database, we record a record in progressFetches so we
        // know we can use the local store and do not need to hit the server again.
        progressFetches: '&user_id',
        bookmarkedStreams: '&[user_id+locale_pack_id]',

        // adding user_id+updated_at indexes
        lessonProgress: '&[user_id+locale_pack_id],synced_to_server,&fr_version,[user_id+updated_at]',
        streamProgress: '&[user_id+locale_pack_id],synced_to_server,&fr_version,[user_id+updated_at]',
    });

    db.version(3).stores({
        streams: null,
        publishedStreams: '&id,all_content_stored',

        // publishedLessonContent holds the full frame content for each lesson.
        // It is keyed off of the stream_id and the lesson_id so that we can reliably
        // delete a record from publishedLessonContent when we want to make a stream no
        // longer available offline.  Since, theoretically, lessons can be shared between
        // streams, this would not be safe otherwise.
        publishedLessonContent: '&[stream_id+id], *image_urls',
    });

    db.version(4).stores({
        configRecords: '&id',
        currentUsers: '&id',
    });

    db.version(5).upgrade(tx => {
        // Since we did not previous put serverClientTimeOffset in configRecords,
        // we have to remove any existing records
        return tx.table('configRecords').clear();
    });

    db.version(6).stores({
        sentryErrors: '&id',
    });

    return db;
}
