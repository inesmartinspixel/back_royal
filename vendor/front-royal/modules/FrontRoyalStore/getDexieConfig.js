import fakeIndexedDb from 'fake-indexeddb';
import FDBKeyRange from 'fake-indexeddb/lib/FDBKeyRange';
import { FAKE_INDEXED_DB, SQLITE_PLUGIN } from './supportedDbTechnology';

export default function getDexieConfig(dbTechnology) {
    if (!dbTechnology) {
        throw new Error('dbTechnology must be provided');
    }

    if (!dbTechnology && global.RUNNING_IN_TEST_MODE) {
        dbTechnology = FAKE_INDEXED_DB;
    }

    const dexieConfig = {};

    if (dbTechnology === FAKE_INDEXED_DB) {
        dexieConfig.indexedDB = fakeIndexedDb;
        dexieConfig.IDBKeyRange = FDBKeyRange;
    } else if (dbTechnology === SQLITE_PLUGIN) {
        const shimIndexedDB = window.shimIndexedDB;
        window.openDatabase = window.sqlitePlugin.openDatabase;
        // shimIndexedDB.__debug(true); // for debugging
        shimIndexedDB.__useShim();
        dexieConfig.indexedDB = shimIndexedDB;
        dexieConfig.IDBKeyRange = window.IDBKeyRange;
    } else {
        dexieConfig.indexedDB = window.indexedDB;
    }

    return dexieConfig;
}
