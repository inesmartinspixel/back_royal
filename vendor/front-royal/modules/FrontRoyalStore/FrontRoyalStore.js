import Dexie from 'dexie';
import { setStreamBookmarks } from 'StoredProgress';
import { map } from 'lodash/fp';
import { getDataUrlIfStored } from 'StoredImage';
import openDb from './openDb';
import clearAllTables from './clearAllTables';
import flushStore from './flushStore';
import supportedDbTechnology, { FAKE_INDEXED_DB } from './supportedDbTechnology';
import openDbIfAlreadyExists from './openDbIfAlreadyExists';

export default class FrontRoyalStore {
    constructor(injector) {
        this.injector = injector;
        this.enabled = false;
        ensureLoggingUnhandledRejections();
    }

    async getDb() {
        if (this._db) {
            return this._db;
        }

        // If we've already triggered openDbIfAlreadyExists, wait for it
        // to finish and then see if we have a db
        if (this._setDbIfAlreadyExistsPromise) {
            await this._setDbIfAlreadyExistsPromise;
        }

        // initialize the database if either
        // * the store has been enabled (because the
        //      current user has enable_front_royal_store=true)
        // * the db is already there, meaning that in the last session
        //      the current user had the store enabled, but now we don't
        //      have a current user yet.  This is necessary in case there
        //      is no network connection and we need to pull the config
        //      and current user from the database
        if (!this._db && this.enabled) {
            this._db = openDb(supportedDbTechnology());
            return this._db;
        }

        // Check for undefined instead of null so we don't have to call
        // openDbIfAlreadyExists more than once.  It will return null
        // the first time if there is no db
        if (!this._setDbIfAlreadyExistsPromise) {
            this._setDbIfAlreadyExistsPromise = openDbIfAlreadyExists(supportedDbTechnology()).then(db => {
                this._db = db;
                return db;
            });
            return this._setDbIfAlreadyExistsPromise;
        }

        return this._db || null;
    }

    getCurrentUser() {
        return this.getSingleRecord('currentUsers');
    }

    getConfig() {
        return this.getSingleRecord('configRecords');
    }

    async getSingleRecord(table) {
        // This can be called even when the store is not enabled,
        // since we need to load up config and user during initialization before
        // we have authenticated a current user. For that reason,
        // `db` might be null inside of the block, so we need a null
        // check.
        return this.retryAfterHandledError(async db => {
            if (!db) {
                return null;
            }
            const records = await db[table].toArray();
            if (records.length > 1) {
                const ErrorLogService = this.injector.get('ErrorLogService');
                ErrorLogService.notifyInProd(`There are multiple records in the ${table} table`, null, {
                    extra: {
                        ids: records.map(r => r.id),
                    },
                });
                await db[table].clear();
                return undefined;
            }
            return records[0];
        });
    }

    async storeCurrentUser(record) {
        return this.retryAfterHandledError(db => db.currentUsers.put(record));
    }

    // eslint-disable-next-line class-methods-use-this
    destroy() {
        // We used to call removeAllListeners here.  For now this is no longer
        // necessary, but leaving this instead of going and removing this from
        // whatever specs are calling it.  If we still don't need this
        // after all this code settles down more, we can consider removing it.
    }

    async retryAfterHandledError(fn) {
        try {
            const db = await this.getDb();
            return await fn(db);
        } catch (err) {
            const db = await this.handleError(err);
            if (db) {
                return fn(db);
            }
            throw err;
        }
    }

    async handleError(err) {
        // This happens, for example, when in FF private browsing mode
        if (err.name === 'OpenFailedError' && err.message.match('InvalidStateError')) {
            // It's possible that there were two failures concurrently,
            // and the other one already switched us over to FAKE_INDEXED_DB
            if (this._db?.dbTechnology !== FAKE_INDEXED_DB) {
                this._db = openDb(FAKE_INDEXED_DB);
            }
            return this._db;
        }

        // In dev mode, you might switch to a branch that has an earlier version
        // of the database.  In that case, we catch the VersionError, delete the
        // existing database, and re-initializing using the code that you are
        // currently using. (We cannot use our normal strategy of checking for dev
        // mode here, because we might not have config yet). FIXME: do we need
        // to add UpgradeError here?
        const onDev = window.location.hostname.match(/localhost/) || window.location.hostname.match(/ngrok/);
        if (onDev && err.name === 'OpenFailedError' && err.message.match('VersionError')) {
            await Dexie.delete('FrontRoyal');
            this._db = openDb(supportedDbTechnology());
            return this._db;
        }

        return false;
    }

    clearAllTables() {
        return this.retryAfterHandledError(db => clearAllTables(db));
    }

    setStreamBookmarks(user) {
        if (!this.enabled || !user) {
            return Promise.resolve;
        }

        return this.retryAfterHandledError(db =>
            setStreamBookmarks(user.id, map('id')(user.favorite_lesson_stream_locale_packs), db),
        );
    }

    async flush() {
        // If the store is not enabled, that means that there
        // should be nothing to flush, so we can return true
        if (!this.enabled) {
            return true;
        }

        // flushStore will return true if it succeeds in flushing,
        // false if we are in offlineMode
        return flushStore(this.injector);
    }

    beforeUnsettingCurrentUser() {
        this.enabled = false;
        return this.ensureFlushedAndDeleteDb();
    }

    async getDataUrlIfStored(url) {
        if (!this.enabled) {
            return url;
        }

        return this.retryAfterHandledError(db => getDataUrlIfStored(url, db));
    }

    async ensureFlushedAndDeleteDb() {
        if (this._db) {
            // Do not call this.flushToServer because we want
            // to flush even if we're disabled at this point.
            await flushStore(this.injector);
            await this.retryAfterHandledError(async db => {
                if (db) {
                    await db.delete();
                }
            });
        }
        this._db = undefined;
        this._setDbIfAlreadyExistsPromise = Promise.resolve();
    }
}

let loggingUnhandledRejections;
function ensureLoggingUnhandledRejections() {
    if (loggingUnhandledRejections) {
        return;
    }
    // FIXME: This can't be right. The first error below shows
    // up in the console even if I don't do this.  The second one, though,
    // requires this (This code goes in StoredContent#toggleAvailability):
    //
    // See https://stackoverflow.com/questions/58398215/why-are-errors-in-a-dexie-promise-resolution-not-shown
    /*
        new Promise((resolve, reject) => {
            resolve();
        }).then(() => {
            throw new Error('vanilla promise');
        });

        const query = db.streams.where('id').equals(stream.id);

        return query.delete()
            .then(() => {
                throw new Error('dexie callback');
            });
    */
    window.addEventListener('unhandledrejection', ev => {
        console.error('unhandledrejection', ev.reason);
    });
    loggingUnhandledRejections = true;
}
