import angular from 'angular';
import FrontRoyalStore from './FrontRoyalStore';
import { requestInterceptor, responseErrorInterceptor } from './interceptors';
import 'PrioritizedInterceptors/angularModule';

const FrontRoyalStoreModule = angular.module('FrontRoyalStore', ['prioritizedInterceptors']);

FrontRoyalStoreModule.factory('frontRoyalStore', [
    '$injector',
    $injector => {
        const frontRoyalStore = new FrontRoyalStore($injector);
        const $rootScope = $injector.get('$rootScope');

        // See watchEnableFrontRoyalStoreOnCurrentUser, which is called in app.js,
        // to see how we enable/disable the store based on the flag on the user.  It
        // would kind of make sense to call that here, but it makes it simpler in
        // the OfflineMode specs if that is not getting set up here automatially.

        $rootScope.$on('$destroy', () => {
            frontRoyalStore.destroy();
        });

        return frontRoyalStore;
    },
]);

FrontRoyalStoreModule.factory('FrontRoyalStoreInterceptor', [
    '$injector',
    $injector => ({
        request: config => {
            return requestInterceptor(config, $injector);
        },

        responseError: response => {
            return responseErrorInterceptor(response, $injector);
        },
    }),
]);

// See comment near the call tp setupFrontRoyalStoreInterceptors in app.js
// in order to understand why this is setup this way.
FrontRoyalStoreModule.constant('setupFrontRoyalStoreInterceptors', function setupFrontRoyalStoreInterceptors(
    $injector,
) {
    const PrioritizedInterceptorsProvider = $injector.get('PrioritizedInterceptorsProvider');
    PrioritizedInterceptorsProvider.addInterceptor(0, 'FrontRoyalStoreInterceptor');
});

export default FrontRoyalStoreModule;
