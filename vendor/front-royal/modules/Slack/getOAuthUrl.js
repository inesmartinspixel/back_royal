import SLACK_OAUTH_PATH from './constants';

export default function getOAuthUrl(slackRoom) {
    return `${SLACK_OAUTH_PATH}?slack_room_id=${slackRoom.id}&request_origin_url=${encodeURIComponent(
        window.location.href,
    )}`;
}
