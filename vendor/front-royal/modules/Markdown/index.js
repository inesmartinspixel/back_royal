import MarkdownPreview from './MarkdownPreview';
import './markdown-0.6.0-beta1';

// ./markdown-0.6.0-beta1 doesn't export markdown. It just
// shoves it onto the window.
// (This came from https://github.com/evilstreak/markdown-js)
const markdown = window.markdown;

export { MarkdownPreview, markdown };
