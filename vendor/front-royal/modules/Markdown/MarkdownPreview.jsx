import React, { useState, useContext, useEffect, useRef } from 'react';
import AngularContext from 'AngularContext';
import debounceRender from 'react-debounce-render';

function InnerMarkdownPreview({ text }) {
    const $injector = useContext(AngularContext);
    const processedTextCache = useRef({}).current;
    const [processedText, setProcessedText] = useState('');

    useEffect(() => {
        const FormatsText = $injector.get('FormatsText');
        const processedText = FormatsText.getCachedOrFormatted(text, processedTextCache);
        setProcessedText(processedText);
    }, [text, processedTextCache, $injector]);

    return (
        <div>
            <div dangerouslySetInnerHTML={{ __html: processedText }} />
        </div>
    );
}

const MarkdownPreview = React.memo(InnerMarkdownPreview, (prevProps, nextProps) => prevProps.text === nextProps.text);

export default debounceRender(MarkdownPreview, 500);
