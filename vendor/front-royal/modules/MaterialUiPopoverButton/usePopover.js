import { useState } from 'react';

export default function usePopover() {
    const [open, setOpen] = useState(false);

    return {
        open,
        setOpen,
    };
}
