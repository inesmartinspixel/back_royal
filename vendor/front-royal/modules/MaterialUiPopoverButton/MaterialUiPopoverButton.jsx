import React, { useRef } from 'react';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import Popover from '@material-ui/core/Popover';
import { generateGuid } from 'guid';

export default function MaterialUiPopoverButton(props) {
    const { title, label, children, icon, open, setOpen } = props;
    const buttonRef = useRef();
    let { anchorOrigin, transformOrigin } = props;
    anchorOrigin = anchorOrigin || {
        vertical: 'bottom',
        horizontal: 'center',
    };
    transformOrigin = transformOrigin || {
        vertical: 'top',
        horizontal: 'center',
    };

    const id = useRef(generateGuid());

    function handleClose() {
        setOpen(false);
    }

    return (
        <>
            <Tooltip title={title}>
                <IconButton ref={buttonRef} aria-label={label || title} onClick={() => setOpen(!open)}>
                    {icon}
                </IconButton>
            </Tooltip>
            <Popover
                id={id}
                open={open}
                anchorEl={buttonRef.current}
                onClose={handleClose}
                anchorOrigin={anchorOrigin}
                transformOrigin={transformOrigin}
            >
                {children}
            </Popover>
        </>
    );
}
