export default angular
    .module('FrontRoyal.AdminEditorReportsShared', [])
    .constant('EDITOR_FORM_CLASSES', [
        'selectize-triangle-after',
        'selectize-36h',
        'selectize-font-13',
        'selectize-max-width-in-table',
        'admin-editor-report-shared',
    ]);
