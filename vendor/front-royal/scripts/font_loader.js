import casperMode from 'casperMode';

// see also: http://bdadam.com/blog/loading-webfonts-with-high-performance.html
// see also: https://gist.github.com/hdragomir/8f00ce2581795fd7b1b7
(() => {
    // totally unnecessary for CORDOVA bundled assets (I think)
    if (window.CORDOVA) {
        return;
    }

    const HAVE_LOCAL_STORAGE = window.Modernizr && window.Modernizr.localstorage;

    // once cached, the css file is stored on the client forever unless
    // the URL below is changed. Any change will invalidate the cache
    const css_href = window.webpackManifest['font-families.css'];

    // a simple event handler wrapper
    function on(el, ev, callback) {
        if (el.addEventListener) {
            el.addEventListener(ev, callback, false);
        } else if (el.attachEvent) {
            el.attachEvent(`on${ev}`, callback);
        }
    }

    // quick way to determine whether a css file has been cached locally
    function fileIsCached(href) {
        return (
            HAVE_LOCAL_STORAGE &&
            window.localStorage &&
            localStorage.font_css_cache &&
            localStorage.font_css_cache_file === href
        );
    }

    // this is the simple utitily that injects the cached or loaded css text
    function injectRawStyle(text) {
        const head = document.head || document.getElementsByTagName('head')[0];
        const style = document.createElement('style');
        style.type = 'text/css';
        if (style.styleSheet) {
            // IE
            // This is stupid. IE requires us to wait until the element is in the DOM before setting this value.
            // http://www.phpied.com/dynamic-script-and-style-elements-in-ie/
            head.appendChild(style);
            style.styleSheet.cssText = text;
        } else {
            style.appendChild(document.createTextNode(text));
            head.appendChild(style);
        }
    }

    // time to get the actual css file
    function injectFontsStylesheet() {
        // if this is an older browser
        if (!HAVE_LOCAL_STORAGE || !window.localStorage || !window.XMLHttpRequest) {
            const stylesheet = document.createElement('link');
            stylesheet.href = css_href;
            stylesheet.rel = 'stylesheet';
            stylesheet.type = 'text/css';
            document.getElementsByTagName('head')[0].appendChild(stylesheet);
            // just use the native browser cache
            // this requires a good expires header on the server
            document.cookie = 'font_css_cache';
        } else {
            // if this isn't an old browser ...

            if (fileIsCached(css_href)) {
                // use the cached version if we already have it
                injectRawStyle(localStorage.font_css_cache);
            } else {
                // otherwise, load it with ajax
                const xhr = new XMLHttpRequest();
                xhr.open('GET', css_href, true);
                on(xhr, 'load', () => {
                    if (xhr.readyState === 4) {
                        // once we have the content, quickly inject the css rules
                        injectRawStyle(xhr.responseText);

                        // and cache the text content for further use
                        // notice that this overwrites anything that might have already been previously cached
                        localStorage.font_css_cache = xhr.responseText;
                        localStorage.font_css_cache_file = css_href;
                    }
                });

                xhr.send();
            }
        }
    }

    // if we have the fonts in localStorage or if we've cached them using the native browser cache
    if (
        (HAVE_LOCAL_STORAGE && window.localStorage && localStorage.font_css_cache) ||
        document.cookie.includes('font_css_cache')
    ) {
        // just use the cached version
        injectFontsStylesheet();
    }

    // in casper mode, block until fonts are loaded
    else if (casperMode()) {
        injectFontsStylesheet();
    }

    // otherwise, don't block the loading of the page; wait until it's done.
    else {
        on(window, 'load', injectFontsStylesheet);
    }
})();
