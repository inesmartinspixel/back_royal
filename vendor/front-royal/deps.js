import 'styles/main/front-royal.scss';

//---------------------------
// External Dependencies
//---------------------------

// FIXME: this is a hack for now.  See https://trello.com/c/B82ynZ6V
require('marketing/scripts/video-modal.js');

//---------------------------
// Generated Files
//---------------------------

//---------------------------
// FrontRoyal Source Globbing
//---------------------------

/**
 * `require` all modules in the given webpack context
 */
const requireFiles = context => {
    context.keys().forEach(context);
};

// Collect scripts and controllers
requireFiles(
    require.context(
        './scripts',
        true, // use subdirectories
        /^(?!.*spec\/).*\.js$/, // exclude specs
    ),
);
