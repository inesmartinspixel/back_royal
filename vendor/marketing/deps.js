import 'marketing/styles/marketing.scss';

//---------------------------
// External dependencies
//---------------------------

import 'bootstrap-sass/assets/javascripts/bootstrap/transition';
import 'bootstrap-sass/assets/javascripts/bootstrap/collapse';
import 'bootstrap-sass/assets/javascripts/bootstrap/tooltip';
import 'bootstrap-sass/assets/javascripts/bootstrap/modal';

//---------------------------
// Marketing Source Globbing
//---------------------------

import 'marketing/scripts/sm-form';
import 'marketing/scripts/slick.min'; // Consider using package: https://www.npmjs.com/package/slick-carousel
import 'marketing/scripts/events';
import 'marketing/scripts/smartlyCustom';
import 'marketing/scripts/app';
import 'marketing/scripts/retina';
import 'marketing/scripts/menu';
import 'marketing/scripts/hiring-form';
import 'marketing/scripts/accordion';
import 'marketing/scripts/general-toggle';
import 'marketing/scripts/main';
import 'marketing/scripts/content-tabs';
import 'marketing/scripts/forms';
import 'marketing/scripts/video-modal';
import 'marketing/scripts/hiring-scroll';
import 'marketing/scripts/recruit-our-candidates';
import 'marketing/scripts/display-toggle';
import 'marketing/scripts/price-table';
