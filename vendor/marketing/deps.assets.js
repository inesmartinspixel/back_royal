/**
 *
 * The sole intention of this file is to require a generated file, `tmp/marketing/marketing_assets.js`
 * which includes a `require` statement for each image or vector referenced in our embedded ruby files.
 * This is so Webpack will evaluate each `require` statement and use the appropriate loader to process
 * the file that has been required.
 * Using this dependency file as an entrypoint results in Webpack outputting a bundle with the evaluated
 * contents of `tmp/marketing/marketing_assets.js`. However, we do not care about that bundle, and delete
 * it after it is emitted. We only care that Webpack processed the files with an appropriate loader.
 *
 */

//---------------------------
// Marketing assets
//---------------------------

// See also: webpack/webpack.assets.js
require('tmp/marketing/marketing_assets.js');
