'use strict';

/*
 * Input email validation
 */
function isValidEmailAddress(email) {
    /*
     * Email address compliant with RFC2822
     */
    const PATTERN = /^([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22))*\x40([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d))*$/;
    return PATTERN.test(email);
}

$(() => {

    const formErrorClass = 'form--error';
    let email;

    // Validation
    $('#form--hiring .input')
        .blur(function() {
        const $input = $(this);
        const inputValue = $input.val();
        const $form = $input.parent();

        if (inputValue.length === 0) {
            return false;
        }

        if (isValidEmailAddress(inputValue)) {
            email = inputValue;
            $form.removeClass(formErrorClass);
        } else {
            email = undefined;
            $form.addClass(formErrorClass);
        }
    })
        .on('change paste keyup', function() {
            $(this).parent().removeClass(formErrorClass);
        });

    // Submission
    $('#form--hiring .button')
        .click(function() {
            if (email !== undefined) {
                try {
                    window.localStorage.setItem('prefilledEmail', email);
                } catch (e) {
                    // noop
                }

                console.log('redirect to: ', $(this).attr('data-url'));
                window.location.href = $(this).attr('data-url');
            }
        });
});