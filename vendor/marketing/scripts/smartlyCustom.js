/*
 * Smooth scroll to sections
 */
// Cache root element to increase performance
const $rootElememt = $('html, body');

$('#scroll-down').click(function (event) {
    event.preventDefault();

    $rootElememt.animate(
        {
            scrollTop: $($.attr(this, 'href')).offset().top,
        },
        'slow',
    );
});

/*
 * End smooth scroll to sections
 */
function scrollIntoView(selector, duration, onComplete, offset) {
    const elem = $(selector);
    duration = duration || 0.5;
    offset = offset || 0;

    $rootElememt.animate(
        {
            scrollTop: elem.offset().top - offset,
        },
        duration,
        'linear',
        onComplete,
    );
}

// On some of the pages, we dynamically load content after pageload
// In order to support hash URLs that scroll to certain elements, we hack
// in these scrollIntoView invocations.
//
// Note: last argument = enough spacing for header and tabs-header to not block section
const hash = window.location.hash;
setTimeout(() => {
    const width = window.innerWidth;
    const offset = width > 767 ? 140 : 180;

    if (hash.includes('events')) {
        scrollIntoView('#events', 0.5, null, offset);
    }

    if (hash.includes('specializations')) {
        $('#specializations-toggle').trigger('click');
        scrollIntoView('#curriculum', 0.5, null, offset);
    }
}, 500);

// scroll down helper

$('*[data-scroll-link]').click(function () {
    const targetSelector = $(this).data('scroll-link');
    scrollIntoView(targetSelector);
});

// STICKY NAVBAR

(() => {
    // $('ul li.dropdown').hover(function() {
    //     $(this).addClass('open');
    //     //$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
    // }, function() {
    //     $(this).removeClass('open');
    //     //$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
    // });

    const docElem = $(document);

    let didScroll = false;
    const changeHeaderOn = 75;

    function init() {
        $(window).scroll(() => {
            if (!didScroll) {
                didScroll = true;
                setTimeout(scrollPage, 0);
            }
        });
    }

    function scrollPage() {
        const sy = scrollY();
        if (sy >= changeHeaderOn) {
            $('#sticky-navbar').addClass('header-shrink');
        } else {
            $('#sticky-navbar').removeClass('header-shrink');
        }
        didScroll = false;
    }

    function scrollY() {
        return window.pageYOffset || docElem.scrollTop();
    }

    init();
})();
