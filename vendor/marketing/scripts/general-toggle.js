(() => {
  $('.general-toggle a').click(function(event) {
    event.preventDefault();

    $(this).parent().children().each((index, item) => {
      $(item).removeClass('active')
      const target = $(item).attr('href');
      $(target).hide();
    });

    $($(this).attr('href')).show();
    $(this).addClass('active')
  })
})();
