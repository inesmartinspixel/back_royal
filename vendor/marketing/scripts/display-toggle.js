import { TweenMax } from 'FrontRoyalGsap';

/*
 *  Display Toggle to show/hide full content
 */
(function() {
  var displayToggleTrigger = $('.display-toggle__trigger');

  function scrollToBeginning(container) {
    $('html,body').animate({
      scrollTop: container.closest('.display-toggle').parent().offset().top - 85
    }, 750);
  }

  function animateHeight(container) {
    var fullHeight = 0;
    var containerChildren = container.children();
    var containerParent = container.closest('.display-toggle');

    containerChildren.each(function() {
      fullHeight = fullHeight + $(this).height();
    });

    TweenMax.to(container, .1, {
      maxHeight: fullHeight,
      onComplete: function() {
        containerParent.attr('data-toggle-state', 'showing');
      }
    });
  }

  displayToggleTrigger.on('click', function() {
    var displayToggle = $(this).closest('.display-toggle');
    var displayToggleContainer = displayToggle.find('.display-toggle__container');
    var displayToggleState = displayToggle.attr('data-toggle-state');

    if (displayToggleState == 'hidden') {
      animateHeight(displayToggleContainer);
    } else {
      scrollToBeginning(displayToggleContainer);
      displayToggle.attr('data-toggle-state', 'hidden');

      setTimeout(function() {
        displayToggleContainer.removeAttr('style');
      }, 350);
    }
  });

  $(window).on('resize', function() {
    if (displayToggleTrigger.length === 0) return false;

    displayToggleTrigger.each(function(index, element) {
      var displayToggle = $(this).closest('.display-toggle');
      var displayToggleContainer = displayToggle.find('.display-toggle__container');
      var displayToggleState = displayToggle.attr('data-toggle-state');

      if (displayToggleState == 'showing') animateHeight(displayToggleContainer);
    });
  });
})();