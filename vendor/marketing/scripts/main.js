'use strict';

/*
 * Smooth scroll to sections
 */
// Cache root element to increase performance
const $rootElement = $('html, body');

$('#scroll-down').click(function(event) {
    event.preventDefault();

    $rootElement.animate(
        {
            scrollTop: $($.attr(this, 'href')).offset().top
        },
        'slow'
    );
});

// Angular intercepts any # (hash) URLs and converts them to #/ paths
// This is because we have not configured the marketing Angular app to be html5mode
// We can't do that, because it would then intercept all normal URLs and try to route
// them within the angular app, instead of reloading the page.
// Rather than refactor the entire marketing site, for expediency we've decided to
// manually intercept the hash URLs and attempt to scroll directly to the named
// element with some extra offset for the header and banner.
if (!!window.location.hash) {
    // hack: remove slash that angular adds
    const hash = window.location.hash.replace('#/', '#');
    let offset;
    try {
        offset = $(hash).offset();
    } catch (e) {
        // noop
    }
    if (!!offset) {
        $rootElement.scrollTop(offset.top - 140); // extra space for the header and banner
    }
}

/*
 * Executive MBA table slider
 */
(() => {
    const desktopTableSlider = $('.table-slider--degree-comparison-desktop');
    const hasDesktopTableSlider = desktopTableSlider.length;
    const mobileTableSlider = $('.table-slider--degree-comparison-mobile');
    const hasMobileTableSlider = mobileTableSlider.length;

    if (hasDesktopTableSlider) {
        const columnsSlider = desktopTableSlider.find('.table-slider__columns');
        const scrollPane = columnsSlider.find('.scroll-pane');
        const sliderItems = columnsSlider.find('.table-slider__col');

        scrollPane.width(sliderItems.length * sliderItems.innerWidth());

        scrollPane.on('touchstart touchmove', event => {
            event.stopImmediatePropagation();
        });
    }

    if (hasMobileTableSlider) {
        const headingsSlider = mobileTableSlider.find('.table-slider__headings');
        const statsSlider = mobileTableSlider.find('.table-slider__stats');

        headingsSlider.slick({
            dots: false,
            arrows: false,
            infinite: false,
            speed: 500,
            slidesToShow: 3,
            variableWidth: true,
            centerMode: true,
            centerPadding: '0',
            asNavFor: '.table-slider__stats'
        });

        headingsSlider.on('afterChange', (slick, currentSlide, nextSlideIndex) => {
            if (mobileTableSlider.hasClass('table-slider--degree-comparison-mobile--with-sections')) {
                const $tableSections = $('.table-slider__headings-sections-wrapper');

                const nextSectionParent = $(
                    $('.table-slider__headings').find('.table-slider__item')[nextSlideIndex]
                ).attr('data-parent-section');

                if (nextSectionParent !== undefined) {
                    $tableSections.addClass('table-slider__headings-sections-wrapper--enabled');
                    $tableSections.find('p').removeClass('js-active');
                    $tableSections.find(`#${nextSectionParent}`).addClass('js-active');
                    $('.table-slider__headings').addClass('table-slider__headings--no-border-top');
                } else {
                    $tableSections.find('p').removeClass('js-active');
                    $tableSections.removeClass('table-slider__headings-sections-wrapper--enabled');
                    $('.table-slider__headings').removeClass('table-slider__headings--no-border-top');
                }
            }
        });

        headingsSlider.find('.table-slider__item').click(function() {
            headingsSlider.slick('slickGoTo', $(this).index());
        });

        statsSlider.slick({
            dots: false,
            arrows: false,
            infinite: false,
            speed: 500
        });
    }

    window.comparisonTablesReady = true;
})();

// Modal sections toggle
(() => {
    const buttons = $('.modal-section-toggle button');
    if (buttons.length) {
        buttons.click(event => {
            buttons.each((index, element) => {
                $(element).removeClass('js-active');
                $(`#${$(element).attr('data-toggle')}`).hide();
                console.log($(`#${$(element).attr('data-toggle')}`));
            });
            $(event.target).addClass('js-active');
            $(`#${$(event.target).attr('data-toggle')}`).show();
        });
    }
})();
