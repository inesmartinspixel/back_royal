/* eslint-disable func-names */
import { TimelineMax, Power2 } from 'FrontRoyalGsap';

(() => {
    const $videos = $('[data-video-url]');
    // NOTE: we hard-code it to desktop below, since we decided we preferred the desktop experience
    // on mobile as well. We could always uncomment those lines if we wanted to move back to the
    // modal pop-up experience on mobile.
    let isDesktop;
    let $modal;
    let $modalCloseButton;
    let $modalVideo;
    let initComplete = false;

    if (!$videos.length) {
        return;
    }

    // Get scroll Y position
    function scrollY() {
        return $(window).scrollTop();
    }

    function isBodyFreezed() {
        return $('body').hasClass('scroll-freeze');
    }

    // Freeze page scroll when menu is open
    function freezeScroll(posY, scrollTop) {
        if (isBodyFreezed()) {
            $('body').css({
                top: -posY
            });
        } else {
            const currentPos = parseInt(scrollTop.replace('-', '').replace('px', ''), 10);
            window.scrollTo(0, currentPos);
        }
    }

    function removeFreezeScroll() {
        const posY = scrollY();
        const scrollTop = $('body').css('top') || 0;

        $('html, body').removeClass('scroll-freeze');
        freezeScroll(posY, scrollTop);
    }

    // Prevent page scroll when body has class freezed
    $('body').on('touchmove', event => {
        if (isBodyFreezed()) {
            event.preventDefault();
        }
    });

    const openModalTL = new TimelineMax({
        paused: true,
        onStart() {
            const posY = scrollY();
            const scrollTop = $('body').css('top') || 0;

            $('html, body').addClass('scroll-freeze');

            freezeScroll(posY, scrollTop);
        },
        onComplete() {
            // noop
        }
    });

    function openModal() {
        $modalVideo = $modal.find('iframe');

        openModalTL.clear();

        openModalTL
            .set($modal, {
                opacity: 0
            })
            .set($modalCloseButton, {
                opacity: 0,
                scale: 0.9
            })
            .set($modalVideo, {
                opacity: 0
            })
            .to($modal, 0.2, {
                display: 'block',
                opacity: 1,
                ease: Power2.easeInOut
            })
            .to($modalCloseButton, 0.2, {
                opacity: 1,
                scale: 1,
                ease: Power2.easeInOut
            })
            .to($modalVideo, 0.1, {
                opacity: 1,
                ease: Power2.easeIn
            });
    }

    function init() {
        if (initComplete) {
            return;
        }

        // HACK: hardcoded to desktop
        isDesktop = true; // $(window).innerWidth() > 1200;

        // mobile video modal
        $('body').append('<div class="video-modal"><span class="close-btn"></span>');

        $modal = $('.video-modal');
        $modalCloseButton = $modal.find('.close-btn');

        $videos.on('click', function() {
            const videoURL = $(this).attr('data-video-url');
            const videoID = videoURL.match(
                /(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/
            );
            const videoHeight = ($(window).innerWidth() * 9) / 16;
            const videoHTML = `<iframe style="max-height: ${videoHeight}px" src="https://www.youtube.com/embed/${videoID[1]}?rel=0&amp;enablejsapi=1&amp;showinfo=0&amp;autoplay=1&amp;playsinline=1&amp;modestbranding=0" frameBorder="0" allowfullscreen></iframe>`;

            if (videoID) {
                if (isDesktop) {
                    $(this).append(videoHTML);
                } else {
                    $modal.append(videoHTML);
                    openModal();
                    openModalTL.play();
                }
            }
        });

        $modalCloseButton.on('click', function() {
            openModalTL.reverse();
            $(this)
                .parent()
                .find('iframe')
                .remove();

            removeFreezeScroll();
        });

        initComplete = true;
    }

    function removeVideo() {
        const modalHasVideo = $modal.find('iframe').length;
        removeFreezeScroll();

        if (isDesktop) {
            $videos.find('iframe').remove();
        } else if (modalHasVideo) {
            $modal.find('iframe').remove();
        }
    }

    function updateVideo() {
        if (!isDesktop) {
            const videoHeight = ($(window).innerWidth() * 9) / 16;
            $('.video-modal iframe').css({
                'max-height': videoHeight
            });
        }
    }

    $(window).on('content-tabs-change', () => {
        $('iframe').each((index, iframe) => {
            const src = $(iframe).attr('src');
            if (src && src.includes('youtube')) {
                iframe.contentWindow.postMessage('{"event":"command","func":"stopVideo","args":""}', '*');
            }
        });
    });

    $(window).on('load', init);

    // HACK: on the dynamic landing page, where we lazy load this code,
    // the window load event has already been triggered here, so we call init right away
    // See See https://trello.com/c/B82ynZ6V
    if (window.authInitVideoModal) {
        init();
    }

    $(window).on('resize', () => {
        // HACK: hardcoded to desktop
        const hasChangedDevice = false; // isDesktop !== $(window).innerWidth() > 1200;

        if (hasChangedDevice) {
            removeVideo();
            isDesktop = !isDesktop;
        } else {
            updateVideo();
        }
    });
})();
