import 'ng-token-auth';

import 'Injector/angularModule';
import Auid from 'Auid';
import 'DateHelper/angularModule';
import { generateGuid } from 'guid';
import BuildConfig from 'BuildConfig';
import ClientStorage from 'ClientStorage';
import 'Onetrust/angularModule';   
import 'Segmentio/angularModule';  
import 'EventLogger/angularModule';
import 'angular-moment';

/*-----------------------------------------------------------------------------------*/
/*  Angular App for Event Logging
/*-----------------------------------------------------------------------------------*/


(angular => {

    // create the application module
    const app = angular.module('OuterScreens', [
        'segmentio',
        'ng-token-auth',
        'EventLogger',
        'Iguana',
        'Iguana.Adapters.RestfulIdStyle',
        'DateHelper',
        'onetrustCookieHelper',
        'Injector',
        'angularMoment'
    ])

        .config(['$injector', '$authProvider', 'IguanaProvider', '$compileProvider',
            ($injector, $authProvider, IguanaProvider, $compileProvider) => {

                $authProvider.configure({

                    // every API request originating from the ENDPOINT_ROOT
                    apiUrl: `${window.ENDPOINT_ROOT}/api`,

                    // defer validation for explicit validation once auth listeners are setup
                    validateOnPageLoad: false,

                    // required for cordova hybrid
                    storage: window.Modernizr && window.Modernizr.localstorage ? 'localStorage' : 'cookies', // we need to fall back to cookies in Safari private mode

                    // rely on auth events to perform redirect as necessary
                    confirmationSuccessUrl: `${window.location.origin}/`,
                    passwordResetSuccessUrl: `${window.location.origin}/settings/profile`

                });

                const uri = `${window.ENDPOINT_ROOT}/api`;
                IguanaProvider.setAdapter('Iguana.Adapters.RestfulIdStyle');
                IguanaProvider.setBaseUrl(uri);

                // by default, time requests out after 30 seconds
                IguanaProvider.setDefaultRequestOptions({
                    'timeout': 30 * 1000
                });

                // allow for sanitization of javascript, etc
                $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|mailto|blob|data|javascript*):/);

                // Set locale for moment (we only support English on marketing pages)
                $injector.get('moment').updateLocale('en');

            }
        ]);

    app.controller('OuterScreensCtrl', ['$injector', function($injector) {
        const $window = $injector.get('$window');
        const EventLogger = $injector.get('EventLogger');
        const segmentio = $injector.get('segmentio');
        const injector = $injector.get('injector');

        this.continueApplicationInMarketing = ClientStorage.getItem('continueApplicationInMarketing') === 'true';

        this.dateHelper = $injector.get('dateHelper');

        this.logClick = identifier => {
            EventLogger.log('click', {
                label: `marketing:${identifier}`
            }, {
                    segmentioType: 'marketing:click',
                    segmentioLabel: identifier
                });
        };

        this.loggedIn = $window.hasAuthHeaders;

        if (!this.loggedIn) {
            Auid.ensure(injector);
        }


        const auid = Auid.get(injector);
        if (auid) {
            segmentio.identify(auid, undefined, {
                integrations: {
                    'Customer.io': false
                }
            });
        }
    }]);

    app.run(['$injector',
        $injector => {
            const segmentio = $injector.get('segmentio');
            const $http = $injector.get('$http');
            const $window = $injector.get('$window');
            const $q = $injector.get('$q');
            const EventLogger = $injector.get('EventLogger');
            const injector = $injector.get('injector');
            const frontRoyalStore = injector.get('frontRoyalStore', { optional: true });
            const offlineModeManager = injector.get('offlineModeManager', { optional: true });

            // log the page_load
            EventLogger.logStartEvent();
            EventLogger.trackLocationChanges();

            // remove buffering because
            // - onload handler is unreliable
            // - user switch pages more often on marketing pages
            // - we don't log lots of events from marketing pages
            //
            // Do this after logging the page_load event and the
            //  pageview event so at least those two get bundled
            EventLogger.sendEventsImmediately();


            // either use pre-loaded config, or load config as necessary
            let configPromise;
            if (window.preloadedConfig) {
                configPromise = $q.when(window.preloadedConfig);
            } else {
                configPromise = $http.get('/api/config.json').then(response => {
                    if (response && response.data && response.data.contents && response.data.contents.config && response.data.contents.config[0]) {
                        return response.data.contents.config[0];
                    }
                    return $q.reject('invalid config response');
                });
            }

            // get appropriate config value and setup segment
            configPromise.then(config => {
                segmentio.load(config.segmentio_key);
            });
        }
    ]);


})(angular);