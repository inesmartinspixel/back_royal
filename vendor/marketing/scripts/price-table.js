import { TweenMax } from 'FrontRoyalGsap';

/*
 *  Employers Pricing Table
 */
(function() {
  var pricingTable = $('.pricing-table');
  var pricingTableHeader = $('.pricing-table__header');
  var pricingTableHeaderItems = $('.pricing-table__header-block');

  function centerBlockItem(element) {
    var scrollPosition = pricingTableHeader.scrollLeft();
    var elementPosition = element.position().left;
    var distanceToStart = scrollPosition + elementPosition;
    var elementHalfWidth = element.width() / 2;
    var containerHalfPosition = pricingTableHeader.width() / 2;
    var pricingTableLeftOffset = parseInt(pricingTableHeader.css('padding-left')) / 2;
    var elementCurrentHalfPosition = distanceToStart + elementHalfWidth;
    var scrollingDistance = elementCurrentHalfPosition - containerHalfPosition - pricingTableLeftOffset;

    pricingTableHeader.animate({
      scrollLeft: scrollingDistance
    }, 300);

    if (element.hasClass('active-table')) return;
    pricingTableHeaderItems.removeClass('active-table');
    element.addClass('active-table');
  }

  function displayItemTable(element) {
    var dataTableBody = $('.pricing-table__body');
    var dataItem = element.attr('data-item');
    var dataTable = $('[data-table="' + dataItem + '"]');
    var dataTableHeight = dataTable.height();
    var allDataTables = $('.pricing-table__body-block');

    if (dataTable.hasClass('active-table')) return;
    allDataTables.removeClass('active-table');

    TweenMax.set(dataTableBody, {
      opacity: 0,
      height: dataTableHeight
    })
    TweenMax.set(allDataTables, {
      display: 'none',
    })
    TweenMax.to(dataTable, .1, {
      display: 'block',
      className: "+=active-table"
    })
    TweenMax.to(dataTableBody, .2, {
      height: 'auto',
    })
    TweenMax.to(dataTableBody, .3, {
      opacity: 1,
    });
  }

  pricingTableHeaderItems.on('click', function() {
    var isDesktop = $(window).innerWidth() >= 992;
    var clickedItem = $(this);

    if (isDesktop) return false;
    centerBlockItem(clickedItem);
    displayItemTable(clickedItem);
  });
})();