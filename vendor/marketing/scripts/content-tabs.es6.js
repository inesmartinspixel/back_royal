class ContentTabs {
  constructor({element, onLinksClick}) {
      this.contentTabs = $(element)
      this.panels = $('.content-tabs-panel');
      this.wrapper = this.contentTabs.find('.content-tabs__wrapper');
      this.scroller = this.contentTabs.find('.scroller');
      this.links = this.contentTabs.find('a');;
      this.links.click(this.linksClick.bind(this))
      this.onLinksClick = onLinksClick
      this.lastActive = null;

      this.toggleLinks(window.location.pathname, 0);
      this.togglePanels(window.location.pathname);
  }
  toggleLinks(targetURL, targetIndex) {
      this.links.each((index, element) => {
          $(element).removeClass('active');
          if ($(element).attr('href') === targetURL || $(element).attr('data-index') === targetURL) {
              $(element).addClass('active');
              this.centerActiveAnchor($(element));
          }
      });

      this.contentTabs.removeClass('first-element-selected');
      this.contentTabs.removeClass('last-element-selected');
      if (targetIndex === 0) {
          this.contentTabs.addClass('first-element-selected');
      } else if (targetIndex === this.links.length - 1) {
          this.contentTabs.addClass('last-element-selected');
      }
  }
  togglePanels (targetURL) {
      this.panels.hide();
      this.panels.each((index, element) => {
          if ($(element).attr('data-content-url') === targetURL || $(element).attr('data-index') === targetURL) {
              $(element).show();
          }
      });
  }
  setScrollerWidth() {
      let totalWidth = 0;
      this.links.each((index, element) => {
          totalWidth += $(element).innerWidth() + 5; // inline negative space
      });

      this.scroller.width(totalWidth);
  }
  centerActiveAnchor(active) {
      const elem = active[0];
      if (this.lastActive !== elem) {
          const currentLeft = active.offset().left - this.scroller.offset().left;
          const remainingWidth = $(window).width() - active.width();
          const scrollAmount = currentLeft - (remainingWidth / 2);

          this.wrapper.animate({
              scrollLeft: scrollAmount
          }, 400);

          this.lastActive = elem;
      }
  }
  linksClick(event, external = false) {
      event.preventDefault();
      const target = $(event.target);
      const index = target.index()

      const targetURL = target.attr('href');
      const targetTitle = target.attr('data-content-title');

      this.toggleLinks(targetURL, index)
      this.togglePanels(targetURL);
      this.centerActiveAnchor($(target));
      history.pushState(null, targetTitle, targetURL)

      if(!external) {
          this.onLinksClick(event)
      }
  }
}
(() => {
  allContentTabs = [];

  updateAllContentTabs = (event, originIndex) => {
      allContentTabs.forEach((object, index) => {
          if (index !== originIndex) {
              object.linksClick(event, true)
          }
      })
  }

  $('.js-content-tabs').each((index, element) => {
      allContentTabs.push(new ContentTabs({
          element,
          onLinksClick(event) {
              updateAllContentTabs(event, index)
          }
      }))
  })
})();