'use strict';

(function() {
  var recruitInfoElements = $('.message-tooltip a');

  $('.message-tooltip__content').each(function() {
    var tooltipElement = $(this);
    var tooltipItems = tooltipElement.find('li').length;
    if (tooltipItems > 6) {
      $(this).addClass('two-columns');
    }
  });

  function modalOrTooltip() {
    recruitInfoElements.on('click', function() {
      if ($(window).width() < 768 || ($(window).width() <= 1024 && $('html').hasClass('touchevents'))) {
        var elementModal = $(this).attr('data-target');
        $(elementModal).modal('toggle');
      }
    });

    var timer = '';
    var delay = 100;
    var hoverableElements = $('.message-tooltip a');
    recruitInfoElements.mouseover(function() {
      var element = $(this);
      if ($(window).width() >= 768 && $('html').hasClass('no-touchevents')) {
        clearTimeout(timer);
        hoverableElements.removeClass('show-tooltip');
        element.addClass('show-tooltip');
      }
    }).mouseout(function() {
      timer = setTimeout(function() {
        hoverableElements.removeClass('show-tooltip');
      }, delay);
    });
  }

  modalOrTooltip();
})();