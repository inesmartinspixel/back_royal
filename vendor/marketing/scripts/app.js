/* eslint-disable consistent-return */
/* eslint-disable no-restricted-globals */
/* eslint-disable func-names */
(() => {
    function detectIE() {
        const ua = window.navigator.userAgent;

        const msie = ua.indexOf('MSIE ');
        if (msie > 0) {
            return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
        }

        const trident = ua.indexOf('Trident/');
        if (trident > 0) {
            const rv = ua.indexOf('rv:');
            return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
        }

        const edge = ua.indexOf('Edge/');
        if (edge > 0) {
            return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
        }

        return false;
    }

    const isFirefox = navigator.userAgent.toLowerCase().includes('firefox');
    const isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
    const isIE = detectIE();

    if (isSafari) {
        $('body').addClass('safari');
    }

    if (isFirefox) {
        $('body').addClass('firefox');
    }

    if (isIE) {
        $('body').addClass('ie');
    }

    // Detects Scroll on Hash Links to animate scrolling
    if ($('body').hasClass('home')) {
        $('a[href*="#"]:not([href="#"])').click(function () {
            if (
                location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') &&
                location.hostname === this.hostname
            ) {
                let target = $(this.hash);
                target = target.length ? target : $(`[name=${this.hash.slice(1)}]`);
                if (target.length) {
                    $('html,body').animate(
                        {
                            scrollTop: target.offset().top,
                        },
                        750,
                    );
                    return false;
                }
            }
        });
    }
})();
