'use strict';

const _createClass = (() => {
    function defineProperties(target, props) {
        for (const descriptor of props) {
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true;
            if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor);
        }
    }
    return (Constructor, protoProps, staticProps) => {
        if (protoProps) defineProperties(Constructor.prototype, protoProps);
        if (staticProps) defineProperties(Constructor, staticProps);
        return Constructor;
    };
})();

function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
        throw new TypeError("Cannot call a class as a function");
    }
}

const ContentTabs = (() => {
    function ContentTabs(_ref) {
        const element = _ref.element;
        const onLinksClick = _ref.onLinksClick;

        _classCallCheck(this, ContentTabs);

        this.contentTabs = $(element);
        this.panels = $('.content-tabs-panel');
        this.wrapper = this.contentTabs.find('.content-tabs__wrapper');
        this.scroller = this.contentTabs.find('.scroller');
        this.links = this.contentTabs.find('a');
        this.links.click(this.linksClick.bind(this));
        this.onLinksClick = onLinksClick;
        this.lastActive = null;

        // handle trailing slashes
        let targetURL = window.location.pathname;
        targetURL = targetURL.replace(/\/$/, '');

        this.toggleLinks(targetURL, 0);
        this.togglePanels(targetURL, false);
    }

    _createClass(ContentTabs, [{
        key: 'toggleLinks',
        value: function toggleLinks(targetURL, targetIndex) {
            const _this = this;

            this.links.each((index, element) => {
                $(element).removeClass('active');
                if ($(element).attr('href') === targetURL || $(element).attr('data-index') === targetURL) {
                    $(element).addClass('active');
                    _this.centerActiveAnchor($(element));
                }
            });

            this.contentTabs.removeClass('first-element-selected');
            this.contentTabs.removeClass('last-element-selected');
            if (targetIndex === 0) {
                this.contentTabs.addClass('first-element-selected');
            } else if (targetIndex === this.links.length - 1) {
                this.contentTabs.addClass('last-element-selected');
            }
        }
    }, {
        key: 'togglePanels',
        value: function togglePanels(targetURL, scrollToTop) {
            this.panels.hide();
            this.panels.each((index, element) => {
                if ($(element).attr('data-content-url') === targetURL || $(element).attr('data-index') === targetURL) {
                    $(element).show();

                    if (scrollToTop) {
                        $('html, body').scrollTop(0);
                    }
                }
            });
        }
    }, {
        key: 'setScrollerWidth',
        value: function setScrollerWidth() {
            let totalWidth = 0;
            this.links.each((index, element) => {
                totalWidth += $(element).innerWidth() + 5; // inline negative space
            });

            this.scroller.width(totalWidth);
        }
    }, {
        key: 'centerActiveAnchor',
        value: function centerActiveAnchor(active) {
            const elem = active[0];
            if (this.lastActive !== elem) {
                const currentLeft = active.offset().left - this.scroller.offset().left;
                const remainingWidth = $(window).width() - active.width();
                const scrollAmount = currentLeft - (remainingWidth / 2) + active.width() / 3;

                this.wrapper.animate({
                    scrollLeft: scrollAmount
                }, 400);

                this.lastActive = elem;
            }
        }
    }, {
        key: 'linksClick',
        value: function linksClick(event) {
            const external = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

            event.preventDefault();
            const target = $(event.target);
            const index = target.index();

            let targetURL = target.attr('href');

            // handle trailing slashes
            targetURL = targetURL.replace(/\/$/, '');

            this.toggleLinks(targetURL, index);
            this.togglePanels(targetURL, true);
            this.centerActiveAnchor($(target));

            if (!external) {
                $(window).trigger('content-tabs-change');
                this.onLinksClick(event);
            }
        }
    }]);

    return ContentTabs;
})();

(() => {
    const allContentTabs = [];

    const updateAllContentTabs = function updateAllContentTabs(event, originIndex) {
        allContentTabs.forEach((object, index) => {
            if (index !== originIndex) {
                object.linksClick(event, true);
            }
        });
    };

    $('.js-content-tabs').each((index, element) => {
        allContentTabs.push(new ContentTabs({
            element,
            onLinksClick: function onLinksClick(event) {
                updateAllContentTabs(event, index);
            }
        }));
    });
})();