class AddSenderHiringRelationshipRole < ActiveRecord::Migration[5.0]
    def up
        sql = "
            alter table events add column sender_hiring_relationship_role character varying(21) ENCODE LZO;
        "

        RedshiftHelper.sanitize_encodes(sql)

        ActiveRecord::Base.connection.execute(sql)
    end

    def down
        sql = "
            alter table events drop column sender_hiring_relationship_role;
        "
        execute(sql)
    end
end