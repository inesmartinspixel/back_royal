class CreateNewEventsSchema < ActiveRecord::Migration[5.0]

    def up
        create_events_table("events", "compound sortkey(created_at)")

        # setup NOW() / GETDATE() parity for mimicking Redshift if local
        if ActiveRecord::Base.connection_config[:store_type] != 'redshift'
            execute "CREATE OR REPLACE FUNCTION getdate()
                     RETURNS timestamp with time zone
                     VOLATILE AS $$
                        SELECT NOW()
                     $$ LANGUAGE SQL;"
        end

        # to populate these tables after a dump:
        # INSERT INTO events_cutt (SELECT * FROM events);

    end

    def down
        ActiveRecord::Base.connection.execute "DROP TABLE events"
    end

    def create_events_table(tablename, sortkey_def = nil)
        sql = "CREATE TABLE #{tablename} (
            id text NOT NULL ENCODE LZO,
            user_id text ENCODE LZO,
            event_type character varying(255) ENCODE LZO,
            created_at timestamp without time zone NOT NULL ENCODE AZ64,
            updated_at timestamp without time zone ENCODE AZ64,
            estimated_time timestamp without time zone ENCODE AZ64,
            client_reported_time timestamp without time zone ENCODE AZ64,
            hit_server_at timestamp without time zone ENCODE AZ64,
            total_buffered_seconds double precision ENCODE RUNLENGTH,
            #{columns}
        )"

        if ActiveRecord::Base.connection_config[:store_type] == 'redshift'
            sql += " #{sortkey_def.nil? ? "" : sortkey_def};"
        else
            sql.gsub!(/ENCODE [^,)]*/, "").strip!
        end
        ActiveRecord::Base.connection.execute(sql)
    end

    def columns
        %Q~
        peformance_to_eval_async float ENCODE BYTEDICT,
        peformance_to_timeout float ENCODE BYTEDICT,
        available_answer_ids character varying(938) ENCODE LZO, -- camelcased in original
        candidate_ids character varying(2048) ENCODE LZO,
        candidate_status character varying(29) ENCODE LZO,
        challenge_responses character varying(1094) ENCODE LZO, -- camelcased in original
        client_local_time character varying(36) ENCODE LZO,
        client_utc_time character varying(36) ENCODE LZO,
        event_errors character varying(1361) ENCODE LZO, -- errors in original, but that clashes with active record
        feedback_reasons character varying(80) ENCODE LZO, -- camelcased in original
        hiring_manager_status character varying(32) ENCODE LZO,
        modals character varying(1646) ENCODE LZO,
        orig_modal_keys character varying(56) ENCODE LZO, -- camelcased in original
        participant_ids character varying(60) ENCODE LZO,
        progress_options_to_filter_by character varying(62) ENCODE LZO,
        recent_formats character varying(14345) ENCODE LZO, -- camelcased in original
        recipient_ids character varying(60) ENCODE LZO,
        result_daily_lesson_config_ids character varying(10) ENCODE LZO,
        result_stream_ids character varying(5793) ENCODE LZO,
        topics_to_filter_by character varying(1436) ENCODE LZO,
        card_changed boolean ENCODE RUNLENGTH,
        card_data_saved boolean ENCODE RUNLENGTH,
        cordova boolean ENCODE RUNLENGTH,
        demo_mode boolean ENCODE RUNLENGTH, -- camelcased in original
        editor_mode boolean ENCODE RUNLENGTH, -- camelcased in original
        free_trial_set_to_cancel boolean ENCODE RUNLENGTH,
        lesson_complete boolean ENCODE RUNLENGTH,
        lesson_stream_complete boolean ENCODE RUNLENGTH,
        paid_subscription_set_to_cancel boolean ENCODE RUNLENGTH,
        partially_correct boolean ENCODE RUNLENGTH, -- camelcased in original
        preview_mode boolean ENCODE RUNLENGTH, -- camelcased in original
        set_to_cancel boolean ENCODE RUNLENGTH,
        should_retry boolean ENCODE RUNLENGTH,
        use_default_card boolean ENCODE RUNLENGTH,
        candidate_count integer ENCODE AZ64,
        chapter_index integer ENCODE AZ64,
        client_version integer ENCODE AZ64,
        conversation_id integer ENCODE AZ64,
        created integer ENCODE AZ64,
        duration_idle float ENCODE BYTEDICT,
        frame_index integer ENCODE AZ64,
        notification_id integer ENCODE AZ64,
        pending_webhooks integer ENCODE AZ64,
        receipt_id integer ENCODE AZ64,
        score float ENCODE BYTEDICT,
        status character varying(384) ENCODE LZO,
        total_frames integer ENCODE AZ64,
        buffered_time float ENCODE RAW,
        client_offset_from_utc float ENCODE RUNLENGTH,
        client_utc_timestamp float ENCODE RAW,
        duration_active float ENCODE RAW,
        duration_total float ENCODE RAW,
        next_period_amount float ENCODE BYTEDICT,
        revenue float ENCODE RAW,
        total float ENCODE BYTEDICT,
        value float ENCODE RAW,
        destination_params character varying(2048) ENCODE LZO,
        error character varying(225) ENCODE LZO,
        error_debug character varying(50913) ENCODE LZO,
        params character varying(2048) ENCODE LZO,
        performance character varying(56) ENCODE LZO,
        policy_config character varying(263) ENCODE LZO,
        request_config character varying(477) ENCODE LZO,
        score_metadata character varying(57) ENCODE LZO,
        selection_details character varying(1134) ENCODE LZO,
        server_error character varying(633) ENCODE LZO,
        since_activated character varying(111) ENCODE LZO,
        since_request_queued character varying(141) ENCODE LZO,
        since_request_sent character varying(141) ENCODE LZO,
        source_params character varying(2048) ENCODE LZO,
        text_details character varying(17709) ENCODE LZO, -- camelcased in original
        lesson_passed boolean ENCODE RUNLENGTH,
        user_subscription_type character varying(96) ENCODE LZO,
        action character varying(128) ENCODE LZO,
        api_version character varying(15) ENCODE LZO,
        browser_major character varying(36) ENCODE LZO,
        browser_name character varying(56) ENCODE LZO,
        browser_version character varying(36) ENCODE LZO,
        build_commit character varying(60) ENCODE LZO,
        build_number character varying(6) ENCODE LZO,
        build_timestamp character varying(48) ENCODE LZO,
        campaign character varying(48) ENCODE LZO,
        candidate_account_id character varying(53) ENCODE LZO,
        candidate_id character varying(54) ENCODE LZO,
        cause character varying(1455) ENCODE LZO,
        caused_by_event character varying(24) ENCODE LZO,
        challenge_id character varying(54) ENCODE LZO,
        challenge_type character varying(42) ENCODE LZO,
        chapter_label character varying(72) ENCODE LZO,
        chapter_title character varying(63) ENCODE LZO,
        client character varying(32) ENCODE LZO,
        cohort_id character varying(54) ENCODE LZO,
        component_id character varying(54) ENCODE LZO, -- camelcased in original
        content character varying(512) ENCODE LZO,
        currency character varying(5) ENCODE LZO,
        destination_directive character varying(72) ENCODE LZO,
        destination_path character varying(2048) ENCODE LZO,
        device_model character varying(96) ENCODE LZO,
        device_type character varying(96) ENCODE LZO,
        device_vendor character varying(96) ENCODE LZO,
        directive character varying(72) ENCODE LZO,
        editor_template character varying(44) ENCODE LZO,
        email character varying(192) ENCODE LZO,
        feedback_additional_thoughts character varying(1224) ENCODE LZO, -- camelcased in original
        formatted_free_trial_end_at character varying(27) ENCODE LZO,
        frame_id character varying(54) ENCODE LZO,
        frame_play_id character varying(54) ENCODE LZO,
        -- frame_text character varying(2048) ENCODE LZO,
        hiring_manager_account_id character varying(50) ENCODE LZO,
        hiring_manager_id character varying(54) ENCODE LZO,
        hiring_relationship_id character varying(54) ENCODE LZO,
        hiring_relationship_role character varying(21) ENCODE LZO,
        image_component_id character varying(48) ENCODE LZO,
        ip_address character varying(59) ENCODE LZO,
        label character varying(512) ENCODE LZO,
        launched_lesson_id character varying(54) ENCODE LZO,
        launched_stream_id character varying(54) ENCODE LZO,
        lesson_id character varying(54) ENCODE LZO,
        lesson_play_id character varying(54) ENCODE LZO,
        lesson_stream_id character varying(54) ENCODE LZO,
        lesson_stream_locale_pack_id character varying(54) ENCODE LZO,
        lesson_stream_title character varying(256) ENCODE LZO,
        lesson_stream_version_id character varying(54) ENCODE LZO,
        lesson_title character varying(256) ENCODE LZO,
        lesson_version_id character varying(54) ENCODE LZO,
        link character varying(2048) ENCODE LZO,
        logged_in_user_id character varying(54) ENCODE LZO,
        message character varying(65535) ENCODE LZO,
        object character varying(8) ENCODE LZO,
        os_name character varying(64) ENCODE LZO,
        os_version character varying(36) ENCODE LZO,
        page_load_id character varying(54) ENCODE LZO,
        path character varying(2048) ENCODE LZO, -- duplicate of url?
        plan_id character varying(128) ENCODE LZO,
        plan_name character varying(32) ENCODE LZO,
        playlist_id character varying(54) ENCODE LZO, -- camelcased in original
        provider character varying(20) ENCODE LZO,
        request character varying(27) ENCODE LZO,
        search_attempt_id character varying(54) ENCODE LZO,
        search_text character varying(237) ENCODE LZO,
        section character varying(30) ENCODE LZO,
        sender_avatar_url character varying(2048) ENCODE LZO,
        sender_company_logo_url character varying(2048) ENCODE LZO,
        sender_company_name character varying(192) ENCODE LZO,
        sender_id character varying(54) ENCODE LZO,
        sender_name character varying(36) ENCODE LZO,
        service character varying(12) ENCODE LZO,
        sign_up_code character varying(24) ENCODE LZO,
        source_directive character varying(72) ENCODE LZO,
        source_path character varying(2048) ENCODE LZO, -- duplicate of path?
        src character varying(1536) ENCODE LZO,
        stream_locale_pack_id character varying(54) ENCODE LZO, -- camelcased in original
        subscription_id character varying(54) ENCODE LZO,
        subscription_status character varying(32) ENCODE LZO,
        summary_id character varying(54) ENCODE LZO,
        text_being_processed character varying(1581) ENCODE LZO, -- camelcased in original
        type character varying(128) ENCODE LZO,
        url character varying(2048) ENCODE LZO, -- duplicate of path?
        user_agent character varying(480) ENCODE LZO,
        user_answer_to_display character varying(128) ENCODE LZO, -- camelcased in original
        utm_campaign character varying(72) ENCODE LZO,
        utm_content character varying(72) ENCODE LZO,
        utm_medium character varying(72) ENCODE LZO,
        utm_source character varying(72) ENCODE LZO,
        verified_by character varying(54) ENCODE LZO,
        viewed_stream_dashboard_id character varying(54) ENCODE LZO,
        aborted boolean ENCODE RUNLENGTH,
        card_added boolean ENCODE RUNLENGTH,
        feedback_is_positive boolean ENCODE RUNLENGTH, -- camelcased in original
        has_free_trial boolean ENCODE RUNLENGTH,
        has_some_results boolean ENCODE RUNLENGTH,
        livemode boolean ENCODE RUNLENGTH,
        new_subscription_created boolean ENCODE RUNLENGTH,
        notify_candidate_of_new_request boolean ENCODE RUNLENGTH,
        notify_hiring_manager_of_new_match boolean ENCODE RUNLENGTH,
        result boolean ENCODE RAW,
        server_event boolean ENCODE RUNLENGTH,
        stripe_event boolean ENCODE RUNLENGTH,
        candidate_name character varying(128) ENCODE LZO
        ~
    end
end
