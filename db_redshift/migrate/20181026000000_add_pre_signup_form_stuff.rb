class AddPreSignupFormStuff < ActiveRecord::Migration[5.0]

    def up
        sql = "
            alter table events add column education_level character varying(53) ENCODE LZO;
            alter table events add column area_of_study character varying(53) ENCODE LZO;
            alter table events add column work_experience character varying(15) ENCODE LZO;
            alter table events add column role character varying(53) ENCODE LZO;
        "

        RedshiftHelper.sanitize_encodes(sql)

        ActiveRecord::Base.connection.execute(sql)
    end

    def down
        sql = "
            alter table events drop column education_level;
            alter table events drop column area_of_study;
            alter table events drop column work_experience;
            alter table events drop column role;
        "
        execute(sql)
    end
end