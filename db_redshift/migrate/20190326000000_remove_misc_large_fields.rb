class RemoveMiscLargeFields < ActiveRecord::Migration[5.0]

    def up

        sql = "
            alter table events drop column error_debug;
            alter table events drop column recent_formats;
            alter table events drop column text_details;
        "

        execute(sql)
    end

    def down
        sql = "
            alter table events add column error_debug character varying(50913) ENCODE LZO;
            alter table events add column recent_formats character varying(14345) ENCODE LZO;
            alter table events add column text_details character varying(17709) ENCODE LZO;
        "

        RedshiftHelper.sanitize_encodes(sql)

        ActiveRecord::Base.connection.execute(sql)

    end
end