class AddJustAcceptedRelationship < ActiveRecord::Migration[5.0]
    def up
        sql = "
            alter table events add column just_accepted_relationship boolean ENCODE RUNLENGTH;
        "

        RedshiftHelper.sanitize_encodes(sql)

        ActiveRecord::Base.connection.execute(sql)
    end

    def down
        sql = "
            alter table events drop column just_accepted_relationship;
        "
        execute(sql)
    end
end