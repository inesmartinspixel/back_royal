class AddStripeColumns < ActiveRecord::Migration[5.0]

    def up

        sql = "
            alter table events add column captured boolean ENCODE RUNLENGTH;
            alter table events add column charge_id character varying(53) ENCODE LZO;
            alter table events add column failure_code character varying(53) ENCODE LZO;
            alter table events add column outcome character varying(2048) ENCODE LZO;
        "

        RedshiftHelper.sanitize_encodes(sql)

        ActiveRecord::Base.connection.execute(sql)
    end

    def down
        sql = "
            alter table events drop column outcome;
            alter table events drop column failure_code;
            alter table events drop column charge_id;
            alter table events drop column captured;
        "
        execute(sql)
    end
end