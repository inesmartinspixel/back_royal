class AddSendRetargetedEmail < ActiveRecord::Migration[5.0]
    def up
        sql = "
            alter table events add column send_retargeted_email_for_program_type character varying(64) ENCODE LZO;
        "

        RedshiftHelper.sanitize_encodes(sql)

        ActiveRecord::Base.connection.execute(sql)
    end

    def down
        sql = "
            alter table events drop column send_retargeted_email_for_program_type;
        "
        execute(sql)
    end
end