class AddSuggestedProgramType < ActiveRecord::Migration[5.0]
    def up
        sql = "
            alter table events add column suggested_program_type character varying(128) ENCODE LZO;
        "

        RedshiftHelper.sanitize_encodes(sql)

        ActiveRecord::Base.connection.execute(sql)
    end

    def down
        sql = "
            alter table events drop column suggested_program_type;
        "
        execute(sql)
    end
end