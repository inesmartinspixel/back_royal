class AddScholarshipLevelName < ActiveRecord::Migration[5.0]

    def up
        sql = "
            alter table events add column scholarship_level_name character varying(16) ENCODE LZO;
        "

        RedshiftHelper.sanitize_encodes(sql)

        ActiveRecord::Base.connection.execute(sql)
    end

    def down
        sql = "
            alter table events drop column scholarship_level_name;
        "
        execute(sql)
    end
end