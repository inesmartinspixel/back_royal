class AddEnrolled < ActiveRecord::Migration[5.0]
    def up
        sql = "
            alter table events add column enrolled boolean ENCODE RUNLENGTH;
        "

        RedshiftHelper.sanitize_encodes(sql)

        ActiveRecord::Base.connection.execute(sql)
    end

    def down
        execute('alter table events drop column enrolled')
    end
end