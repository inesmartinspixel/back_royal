class AddStuffForGetMap < ActiveRecord::Migration[5.0]

    def up
        sql = "
            alter table events add column attempts integer ENCODE AZ64;
            alter table events add column get_map_id character varying(53) ENCODE LZO;
        "

        RedshiftHelper.sanitize_encodes(sql)

        ActiveRecord::Base.connection.execute(sql)
    end

    def down
        sql = "
            alter table events drop column attempts;
            alter table events drop column get_map_id;
        "
        execute(sql)
    end
end