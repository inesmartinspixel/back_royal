class AddStudentNetworkFilters < ActiveRecord::Migration[5.0]

    def up
        sql = "
            alter table events add column filters_places character varying(512) ENCODE LZO;
            alter table events add column filters_keyword_search character varying(256) ENCODE LZO;
            alter table events add column filters_student_network_looking_for character varying(256) ENCODE LZO;
            alter table events add column filters_student_network_interests character varying(256) ENCODE LZO;
            alter table events add column filters_industries character varying(256) ENCODE LZO;
            alter table events add column filters_alumni boolean ENCODE RUNLENGTH;
            alter table events add column filters_program_type character varying(128) ENCODE LZO;
        "

        RedshiftHelper.sanitize_encodes(sql)

        ActiveRecord::Base.connection.execute(sql)
    end

    def down
        sql = "
            alter table events drop column filters_places;
            alter table events drop column filters_keyword_search;
            alter table events drop column filters_student_network_looking_for;
            alter table events drop column filters_student_network_interests;
            alter table events drop column filters_industries;
            alter table events drop column filters_alumni;
            alter table events drop column filters_program_type;
        "
        execute(sql)
    end
end