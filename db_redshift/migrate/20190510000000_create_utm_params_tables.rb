class CreateUtmParamsTables < ActiveRecord::Migration[5.0]

    def up

        # This table will hold any anonymous_user_id that eventually
        # logged in as an existing user.  We generally want to ignore these
        # in reports analyzing the performance of marketing pages. We don't
        # bother storing the user_id they logged in as because it's not really
        # needed.
        create_table "CREATE TABLE user_aliases (
            alias_id text NOT NULL ENCODE LZO
        )
        distkey(alias_id)
        compound sortkey(alias_id)
        "

        create_table "CREATE TABLE user_utm_params (
            user_id text NOT NULL ENCODE LZO,
            created_at timestamp without time zone NOT NULL ENCODE AZ64,
            utm_campaign character varying(72) ENCODE LZO,
            utm_content character varying(72) ENCODE LZO,
            utm_medium character varying(72) ENCODE LZO,
            utm_source character varying(72) ENCODE LZO
        )
        compound sortkey(created_at)
        "

    end

    def create_table(sql)
        if ActiveRecord::Base.connection_config[:store_type] != 'redshift'
            sql.gsub!(/ENCODE [^,)]*/, "")&.strip!
            sql.gsub!(/compound[^\n]*/, "")&.strip!
            sql.gsub!(/distkey[^\n]*/, "")&.strip!
        end
        ActiveRecord::Base.connection.execute(sql)
    end

    def down
        ActiveRecord::Base.connection.execute "DROP TABLE user_utm_params"
        ActiveRecord::Base.connection.execute "DROP TABLE user_aliases"
    end
end
