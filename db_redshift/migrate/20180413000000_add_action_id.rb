class AddActionId < ActiveRecord::Migration[5.0]

    def up
        sql = "
            alter table events add column action_id character varying(36) ENCODE LZO;
        "

        RedshiftHelper.sanitize_encodes(sql)

        ActiveRecord::Base.connection.execute(sql)
    end

    def down
        sql = "
            alter table events drop column action_id;
        "
        execute(sql)
    end
end