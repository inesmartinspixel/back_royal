class AddJustUsedDeferralLink < ActiveRecord::Migration[5.0]
    def up
        sql = "
            alter table events add column just_used_deferral_link boolean ENCODE RUNLENGTH;
        "
        RedshiftHelper.sanitize_encodes(sql)
        execute(sql)
    end

    def down
        execute("
            alter table events drop column just_used_deferral_link;
        ")
    end
end
