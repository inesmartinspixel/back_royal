class AddFoundWithSearchId < ActiveRecord::Migration[5.0]
    def up
        sql = "
            alter table events add column found_with_search_id character varying(53) ENCODE LZO;
        "

        RedshiftHelper.sanitize_encodes(sql)

        ActiveRecord::Base.connection.execute(sql)
    end

    def down
        execute('alter table events drop column found_with_search_id')
    end
end