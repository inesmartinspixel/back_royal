class AddOpenPositionId < ActiveRecord::Migration[5.0]

    # Move to other list:
    # ["hiring_manager_closed_info",
    #  "exercises",
    #  "exercise",
    #  "request",
    #  "suggested_cohort",
    #  "transferred_courses",
    #  "midterm_complete"]

    # Backfill:
    #  "open_position_id",

    def up
        sql = "
            alter table events add column open_position_id character varying(36) ENCODE LZO;
        "

        RedshiftHelper.sanitize_encodes(sql)

        ActiveRecord::Base.connection.execute(sql)
    end

    def down
        sql = "
            alter table events drop column open_position_id;
        "
        execute(sql)
    end
end