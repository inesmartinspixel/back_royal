class RemoveCandidateId < ActiveRecord::Migration[5.0]
    def up
        ActiveRecord::Base.connection.execute("alter table events drop column candidate_account_id;")
        ActiveRecord::Base.connection.execute("alter table events drop column hiring_manager_account_id;")
    end

    def down
        sql = "
            alter table events add column candidate_account_id character varying(2048) ENCODE LZO;
            alter table events add column hiring_manager_account_id character varying(2048) ENCODE LZO;
        "

        RedshiftHelper.sanitize_encodes(sql)

        execute(sql)
    end
end