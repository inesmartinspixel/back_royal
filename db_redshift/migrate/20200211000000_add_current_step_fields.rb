class AddCurrentStepFields < ActiveRecord::Migration[5.0]
    def up
        sql = "
            alter table events add column current_step_index integer ENCODE AZ64;
            alter table events add column current_step_title character varying(20) ENCODE LZO;
        "
        RedshiftHelper.sanitize_encodes(sql)
        execute(sql)
    end

    def down
        execute("
            alter table events drop column current_step_index;
            alter table events drop column current_step_title;
        ")
    end
end
