class AddMoreZrColumns < ActiveRecord::Migration[5.0]

    def up
        sql = "
            alter table events add column external_position_title character varying(256) ENCODE LZO;
            alter table events add column external_position_industry character varying(256) ENCODE LZO;
            alter table events add column external_position_location character varying(256) ENCODE LZO;
            alter table events add column external_position_company_name character varying(256) ENCODE LZO;
        "

        RedshiftHelper.sanitize_encodes(sql)

        execute(sql)
    end

    def down
        ActiveRecord::Base.connection.execute("alter table events drop column external_position_title;")
        ActiveRecord::Base.connection.execute("alter table events drop column external_position_industry;")
        ActiveRecord::Base.connection.execute("alter table events drop column external_position_location;")
        ActiveRecord::Base.connection.execute("alter table events drop column external_position_company_name;")
    end

end
