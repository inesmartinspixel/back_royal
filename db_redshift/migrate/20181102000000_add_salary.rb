class AddSalary < ActiveRecord::Migration[5.0]

    def up
        sql = "
            alter table events add column salary character varying(53) ENCODE LZO;
        "

        RedshiftHelper.sanitize_encodes(sql)

        ActiveRecord::Base.connection.execute(sql)
    end

    def down
        sql = "
            alter table events drop column salary;
        "
        execute(sql)
    end
end