class AddSessionColumns < ActiveRecord::Migration[4.2]
    def up
        execute('alter table events add column lesson_player_session_id character varying(256)')
        execute('alter table events add column lesson_editor_session_id character varying(256)')
        execute('alter table events add column client_session_id character varying(256)')
    end

    def down
        execute('alter table events drop column lesson_player_session_id')
        execute('alter table events drop column lesson_editor_session_id')
        execute('alter table events drop column client_session_id')
    end
end