class AddProgramType < ActiveRecord::Migration[5.0]
    def up
        sql = "
            alter table events add column program_type character varying(128) ENCODE LZO;
        "

        RedshiftHelper.sanitize_encodes(sql)

        ActiveRecord::Base.connection.execute(sql)
    end

    def down
        execute('alter table events drop column program_type')
    end
end