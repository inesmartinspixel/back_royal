class AddHasScrolled < ActiveRecord::Migration[5.0]
    def up
        sql = "
            alter table events add column has_scrolled boolean ENCODE RUNLENGTH;
        "
        RedshiftHelper.sanitize_encodes(sql)
        execute(sql)
    end

    def down
        execute("
            alter table events drop column has_scrolled;
        ")
    end
end
