class AddSelectedByPolicyColumn < ActiveRecord::Migration[4.2]
    def up
        can_encode = ActiveRecord::Base.connection_config[:store_type] == 'redshift'
        execute('ALTER TABLE events ADD COLUMN selected_by_policy character varying(64)'  + (can_encode ? ' ENCODE LZO' : ''))
    end

    def down
        execute('ALTER TABLE events DROP COLUMN selected_by_policy')
    end
end