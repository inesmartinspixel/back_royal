class AddDumpedAt < ActiveRecord::Migration[5.0]

    def up

        sql = "
            alter table events add column dumped_at timestamp without time zone ENCODE AZ64;
        "
        RedshiftHelper.sanitize_encodes(sql)

        ActiveRecord::Base.connection.execute(sql)
    end

    def down
        sql = "
            alter table events drop column dumped_at;
        "
        execute(sql)
    end
end