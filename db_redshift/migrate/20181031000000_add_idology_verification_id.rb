class AddIdologyVerificationId < ActiveRecord::Migration[5.0]

    def up
        sql = "
            alter table events add column idology_verification_id character varying(53) ENCODE LZO;
            alter table events add column verified boolean ENCODE RUNLENGTH;
        "

        RedshiftHelper.sanitize_encodes(sql)

        ActiveRecord::Base.connection.execute(sql)
    end

    def down
        sql = "
            alter table events drop column verified;
            alter table events drop column idology_verification_id;
        "
        execute(sql)
    end
end