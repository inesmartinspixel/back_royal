class AddCountryCode < ActiveRecord::Migration[5.0]

    def up
        sql = "
            alter table events add column cf_country_code character varying(3) ENCODE LZO;
        "

        RedshiftHelper.sanitize_encodes(sql)

        execute(sql)
    end

    def down
        ActiveRecord::Base.connection.execute("alter table events drop column cf_country_code;")
    end

end
