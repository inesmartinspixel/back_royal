class AddAdditionalSignableDocFields < ActiveRecord::Migration[5.0]
    def up
        sql = "
            alter table events add column cohort_application_id character varying(36) ENCODE LZO;
            alter table events add column follow_up_attempt boolean ENCODE RUNLENGTH;
        "
        RedshiftHelper.sanitize_encodes(sql)
        execute(sql)
    end

    def down
        execute("
            alter table events drop column follow_up_attempt;
            alter table events drop column cohort_application_id;
        ")
    end
end
