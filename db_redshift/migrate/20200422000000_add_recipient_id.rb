class AddRecipientId < ActiveRecord::Migration[5.0]
    def up
        sql = "
            alter table events add column recipient_id character varying(36) ENCODE LZO;
        "
        RedshiftHelper.sanitize_encodes(sql)
        execute(sql)
    end

    def down
        execute("
            alter table events drop column recipient_id;
        ")
    end
end
