class RemoveCause < ActiveRecord::Migration[4.2]
    def up
        execute('ALTER TABLE events DROP COLUMN cause')
    end

    def down
        execute('ALTER TABLE events ADD COLUMN cause character varying(1455)')
    end
end