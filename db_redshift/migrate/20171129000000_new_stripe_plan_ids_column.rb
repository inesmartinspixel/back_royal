class NewStripePlanIdsColumn < ActiveRecord::Migration[5.0]
    def up
        sql = "
            alter table events add column stripe_plan_ids character varying(1024) ENCODE LZO;
        "

        RedshiftHelper.sanitize_encodes(sql)

        ActiveRecord::Base.connection.execute(sql)
    end

    def down
        sql = "
            alter table events drop column stripe_plan_ids;
        "
        execute(sql)
    end
end