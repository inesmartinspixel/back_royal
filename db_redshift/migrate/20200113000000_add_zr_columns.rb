class AddZrColumns < ActiveRecord::Migration[5.0]

    def up
        sql = "
            alter table events add column external_position_id character varying(256) ENCODE LZO;
            alter table events add column external_position_url character varying(2048) ENCODE LZO;
        "

        RedshiftHelper.sanitize_encodes(sql)

        execute(sql)
    end

    def down
        ActiveRecord::Base.connection.execute("alter table events drop column external_position_id;")
        ActiveRecord::Base.connection.execute("alter table events drop column external_position_url;")
    end

end
