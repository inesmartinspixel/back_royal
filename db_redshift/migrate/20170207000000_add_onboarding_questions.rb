class AddOnboardingQuestions < ActiveRecord::Migration[4.2]
    def up
        can_encode = ActiveRecord::Base.connection_config[:store_type] == 'redshift'
        execute('ALTER TABLE events ADD COLUMN onboarding_questions character varying(2048)'  + (can_encode ? ' ENCODE LZO' : ''))
    end

    def down
        execute('ALTER TABLE events DROP COLUMN onboarding_questions')
    end
end