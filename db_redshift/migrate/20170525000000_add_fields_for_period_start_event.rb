class AddFieldsForPeriodStartEvent < ActiveRecord::Migration[5.0]
    def up

        sql = "
            alter table events add column period_index integer ENCODE AZ64;
            alter table events add column required_courses_completed_actual integer ENCODE AZ64;
            alter table events add column required_courses_completed_target integer ENCODE AZ64;
            alter table events add column required_courses_entire_schedule integer ENCODE AZ64;
        "

        RedshiftHelper.sanitize_encodes(sql)

        ActiveRecord::Base.connection.execute(sql)
    end

    def down
        execute('alter table events drop column period_index')
        execute('alter table events drop column required_courses_completed_actual')
        execute('alter table events drop column required_courses_completed_target')
        execute('alter table events drop column required_courses_entire_schedule')
    end
end