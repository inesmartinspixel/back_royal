# `gl` stands for "good lead". See https://trello.com/c/sqglpXD2
class AddGl < ActiveRecord::Migration[5.0]
    def up
        sql = "
            alter table events add column gl boolean ENCODE RUNLENGTH;
        "
        RedshiftHelper.sanitize_encodes(sql)
        execute(sql)
    end

    def down
        execute("
            alter table events drop column gl;
        ")
    end
end
