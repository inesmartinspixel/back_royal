class AddStripeFields < ActiveRecord::Migration[5.0]
    def up
        sql = "
            alter table events add column stripe_plan_id character varying(128) ENCODE LZO;
            alter table events add column stripe_coupon_id character varying(128) ENCODE LZO;
            alter table events add column stripe_coupon_percent_off integer ENCODE AZ64;
            alter table events add column stripe_coupon_amount_off integer ENCODE AZ64;
        "

        RedshiftHelper.sanitize_encodes(sql)

        ActiveRecord::Base.connection.execute(sql)
    end

    def down
        sql = "
            alter table events drop column stripe_plan_id;
            alter table events drop column stripe_coupon_id;
            alter table events drop column stripe_coupon_percent_off;
            alter table events drop column stripe_coupon_amount_off;
        "
        execute(sql)
    end
end