class RemoveStripeRequestField < ActiveRecord::Migration[5.0]
    def up
        ActiveRecord::Base.connection.execute("alter table events drop column request;")
    end

    def down
        sql = "
            alter table events add column request character varying(27) ENCODE LZO;
        "

        RedshiftHelper.sanitize_encodes(sql)

        execute(sql)
    end
end