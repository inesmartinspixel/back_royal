class AddTimeToColumnsToEvents < ActiveRecord::Migration[4.2]
    def up

        can_encode = ActiveRecord::Base.connection_config[:store_type] == 'redshift'

        execute('ALTER TABLE events ADD COLUMN time_to_activate float' + (can_encode ? ' ENCODE BYTEDICT' : ''))
        execute('ALTER TABLE events ADD COLUMN time_to_finish float'   + (can_encode ? ' ENCODE BYTEDICT' : ''))
    end

    def down
        execute('ALTER TABLE events DROP COLUMN time_to_activate')
        execute('ALTER TABLE events DROP COLUMN time_to_finish')
    end
end