class NewRecipientIdsColumn < ActiveRecord::Migration[5.0]
    def up
        sql = "
            alter table events add column recipient_ids_new character varying(1024) ENCODE LZO;
        "

        RedshiftHelper.sanitize_encodes(sql)

        ActiveRecord::Base.connection.execute(sql)
    end

    def down
        sql = "
            alter table events drop column recipient_ids_new;
        "
        execute(sql)
    end
end