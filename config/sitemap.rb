# SEE ALSO: https://github.com/kjvarga/sitemap_generator

# We write sitemaps for each domain, and then sitemap_controller.rb
# dynamically decides which one to return based on the domain associated
# with the current request.

# This file is tested in spec/lib/sitemap_spec.rb

SitemapHelper.domains.each do |domain|

    SitemapGenerator::Sitemap.create(
        public_path: SitemapHelper.public_path,
        default_host: SitemapHelper.host(domain),
        sitemaps_path: SitemapHelper.sitemaps_path(domain),
        verbose: !Rails.env.test?,

        # turn off compression in test mode so we
        # can assert on the contents
        compress: SitemapHelper.compress?
    ) do

        ############################
        ## Marketing Routes
        ############################


        # This is a list of select paths that should be included in the
        # sitemap.  This code is dynamic such that, when generating the smart.ly
        # sitemap, paths in this list that are not available on smart.ly will be
        # left out, and when generating the quantic.edu sitemap, paths that
        # are not available on quantic will be left out.

        # Some notes:
        #
        # 1. In order for a path to work here, it has to be defined with one
        #       of our custom helpers, like `get_quantic` or `get_smartly`. Routes
        #       that should appear an all domains must be defined with
        #       `get_with_sitemap_support` in order to work here.
        #
        # 2. This is a list of strings that are paths.  Things will only work if the path
        #       is the same as a route defined in one of our route files. If you
        #       need to put a path in here that is different from the associated
        #       route (due to dynamic parts of the route), you can enter a hash
        #       in this list instead of a string, like:
        #
        #       {path: '/some_path/with_dynamic_part', route: '/some_path/:page'}
        #
        # 3. We have many paths where the route is defined as `/some_path/(:topic)`
        #       and we only wanted to include the root path in the sitemap, so
        #       we decided to special-case that one.  So, for example, the path
        #       `/mba` is included below and works even though the
        #       relevant route is `/mba/(:topic)`
        #
        # 4. If you're adding something special here and want to make sure it works,
        #       the most efficient thing is probably to use sitemap_spec.rb and write a
        #       test for it first.

        [
            '/sign-in',
            '/library',

            '/efficacy',
            '/mission',

            '/press',
            '/about',
            '/reviews',

            '/tuition-assistance/students',
            '/tuition-reimbursement/companies',

            '/candidates/how-it-works',
            '/candidates/join-career-network',
            '/programs',
            '/programs/method',
            '/candidates/signup',

            '/mba',
            '/executive-mba',
            '/the-free-certificates/fundamentals-of-business',

            '/the-executive-certificates',
            '/the-executive-certificates/data-analytics',
            '/the-executive-certificates/business-strategy',
            '/the-executive-certificates/essentials-of-management',
            '/the-executive-certificates/marketing-pricing-strategy',
            '/the-executive-certificates/financial-managerial-accounting',

            '/the-free-certificates',
            '/open-courses',

            '/employers/recruit-our-candidates',
            '/employers/signup',

            '/corporate-training',
            '/prices/employers',
            '/prices/students',

            '/quantic-name-change',
            '/accreditation',

            '/student-experience'
    ]   .each do |entry|
            if entry.is_a?(String)
                path = entry
                route = entry
            else
                path = entry[:path]
                route = entry[:route]
            end
            add(path) if SitemapHelper.should_include_in_sitemap?(route, domain)
        end

        # This is a special case.  These paths are already public, but we
        # don't want it in the sitemap until after quantic is live
        %w(
            /our-programs
            /vision
            /platforms
        ).each do |path|
            if SitemapHelper.registered_routes[path].matches?(OpenStruct.new(params: {}, domain: domain))
                add(path)
            end
        end



        ############################
        ## MBA Courses
        ############################

        if SitemapHelper.should_write_course_sitemap?(domain)

            # get all promoted EMBA + MBA courses
            courses = ActiveSupport::JSON.decode(Lesson::Stream::ToJsonFromApiParams.new({
                        fields: ['updated_at', 'entity_metadata', 'coming_soon'],
                        filters: {
                            published: true,
                            view_as: "cohort:PROMOTED_DEGREE"
                        }
                    }).json)

            # assume a lack of canonical_url means we don't want to actually index it
            courses.select{ |c| !c['coming_soon'] && c['entity_metadata'] && c['entity_metadata']['canonical_url'] }.each do |course|
                add course['entity_metadata']['canonical_url'], :lastmod => Time.at(course['updated_at'])
            end

        end

    end
end
