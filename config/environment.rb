# Load the rails application
require File.expand_path('../application', __FILE__)

# Initialize the rails application
Rails.application.initialize!

# Rails 5 deprecation on Hash -> Parameters
require 'back_royal/unsafe_with_indifferent_access'
