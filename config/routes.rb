BackRoyal::Application.routes.draw do

    extend MetadataRoutes
    extend LegacyRoutes

    extend ApiRoutes
    extend WebhookRoutes
    extend OauthRoutes

    extend SharedRoutes
    # Note: order matters in routing. SmartlyRoutes will only match if the hostname is smart.ly post-switchover
    # Quantic routes will *redirect* to Quantic if the host is not quantic
    extend SmartlyRoutes # https://smart.ly/*
    extend QuanticRoutes # https://quantic.edu/*

    # This must be the last entry in routes.rb
    extend GlobalFallbackRoutes

end
