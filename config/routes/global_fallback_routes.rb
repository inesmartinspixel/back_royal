module GlobalFallbackRoutes
    def self.extended(router)
        router.instance_exec do

            ####################################################
            # Global Fallback
            ####################################################

            # Catch everything not explicitly mounted as a route or servable
            # directly via Nginx, since those would not have even hit application
            # server if requested
            match "*path" => "home#index", via: [:get, :post],
                constraints: lambda { |request|
                    !(request.path =~ /\.\w+$/) # anything not ending in a file extension
                }


            ####################################################
            # 404 Handling Fallthrough
            ####################################################

            match "*path" => "marketing#four_o_four", via: :all,
                constraints: lambda { |request|
                    request.path =~ /\.\w+$/ # anything ending in a file extension
                }

        end
    end
end