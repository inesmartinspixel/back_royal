module MetadataRoutes
    def self.extended(router)
        router.instance_exec do

            ####################################################
            # Routes just for the metadata controller
            ####################################################

            get "home/index"
            get "home/lesson_index"
            get "home/course_context"
        end
    end
end