module WebhookRoutes
    def self.extended(router)
        router.instance_exec do

            ####################################################
            # IDology Pages
            ####################################################
            get "/id_verification/success", to: "idology#success"
            get "/id_verification/failure", to: "idology#failure"
            get "/id_verification/scan/:id", to: "idology#scan"

            ####################################################
            # Stripe Event Webhooks
            ####################################################

            mount StripeEvent::Engine, at: '/payment/hooks'

            ####################################################
            # IDology Event Webhooks
            ####################################################

            post "api/idology/hook", to: "api/idology#hook"
            get "api/idology/ping", to: "api/idology#ping"
            post "api/idology/request_idology_link", to: "api/idology#request_idology_link"

            ####################################################
            # Signable Documents / SignNow Event Webhooks
            ####################################################

            post "api/sign_now/hook", to: "api/sign_now#hook"

            # for downloading pdfs
            get "/api/signable_documents/show_file/:id", to: "api/signable_documents#show_file"
            post "/api/signable_documents", to: "api/signable_documents#create"
        end
    end
end