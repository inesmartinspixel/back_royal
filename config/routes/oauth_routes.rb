module OauthRoutes
    def self.extended(router)
        router.instance_exec do

            ####################################################
            # Authentication
            ####################################################

            # this seems wrong.  See https://github.com/lynndylanhurley/devise_token_auth/issues/262
            get "/omniauth/failure", to: 'devise_token_auth/custom_omniauth_callbacks#omniauth_failure'

            # by default, omniauth callbacks are expected to be get requests, but saml uses
            # a post, apparently (FIXME: file devise token auth issue) In actuality, we only
            # need to override the post, since this is just a saml issue.  But it is confusing
            # to have these going to different places, so we do the get as well
            post "/omniauth/:provider/callback(.:format)", to: 'devise_token_auth/custom_omniauth_callbacks#redirect_callbacks'
            get "/omniauth/:provider/callback(.:format)", to: 'devise_token_auth/custom_omniauth_callbacks#redirect_callbacks'


            devise_scope :user do
                get "/native_oauth/facebook/callback(.:format)", to: "native_oauth_callbacks#facebook"
                get "/native_oauth/google_oauth2/callback(.:format)", to: "native_oauth_callbacks#google_oauth2"
                get "/native_oauth/apple/callback(.:format)", to: "native_oauth_callbacks#apple"
            end

            ####################################################
            # LinkedIn OAuth
            ####################################################
            get "/linkedin/auth", to: "linkedin#auth"
            get "/linkedin/popup_callback", to: "linkedin#popup_callback"

            ####################################################
            # Slack OAuth
            ####################################################
            get "/slack/auth", to: "slack_oauth#auth"
            get "/slack/auth_callback", to: "slack_oauth#auth_callback"
        end
    end
end