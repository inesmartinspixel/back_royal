require 'http_helpers_extensions'

module SharedRoutes
    def self.extended(router)
        router.instance_exec do

            # These routes are all available on both Quantic and Smartly and won't redirect

            ####################################################
            # Redirects for custom sign-in pages (e.g.: /georgetownmsb)
            ####################################################

            AppConfig.join_config.each do |key, value|
                if key == 'default'
                    next
                end
                if value['institutional_demo']
                    get_with_sitemap_support "/#{key}", to: redirect(path: "/#{key}/demo")
                else
                    if value['locale'].present?
                        get_with_sitemap_support "/#{key}", to: redirect(path: "/#{key}/join?force_locale=#{value['locale']}")
                    else
                        get_with_sitemap_support "/#{key}", to: redirect(path: "/#{key}/join")
                    end
                end
            end

            ####################################################
            # App Routes, i.e.: Lessons, Courses, Settings, Login, etc.
            ####################################################

            get_with_sitemap_support "/lesson/:title/show/:lesson_id", to: "home#lesson_index"
            get_with_sitemap_support "/lesson/:title/:lesson_id", to: "home#lesson_index"
            get_with_sitemap_support "/lesson/:lesson_id", to: "home#lesson_index" # alternate non-Latin route
            get_with_sitemap_support "/course/:stream_id", to: "home#course_context" # alternate non-Latin route
            get_with_sitemap_support "/course/:stream_id/completed", to: "home#course_context"
            get_with_sitemap_support "/course/:stream_id/certificate", to: "home#course_context"
            get_with_sitemap_support "/course/:title/:stream_id", to: "home#course_context"
            get_with_sitemap_support "/course/:title/show/:stream_id", to: "home#course_context"
            get_with_sitemap_support "/course/:stream_id/chapter/:chapter_index/lesson/:lesson_id/show", to: "home#course_context"
            get_with_sitemap_support "/schedule/:cohort_id", to: "cohort_schedule#index" # eventually we will get rid of this and just let this be a regular front-royal route

            get_with_sitemap_support "library", to: "home#library"
            get_with_sitemap_support "sign-in", to: "home#sign-in"
            get_with_sitemap_support "forgot-password", to: "home#forgot-password"
            get_with_sitemap_support "settings", to: "home#settings"

            ####################################################
            # Generic Routes
            ####################################################

            get_with_sitemap_support "health", to: "health#index"
            get_with_sitemap_support "/404", to: "marketing#four_o_four"
            get_with_sitemap_support "/help(/*subpath)", to: redirect { |path_params, req| "https://#{AppConfig.faq_domain}/#{path_params[:subpath]}" }
            get_with_sitemap_support "/robots.:format", to: "robots#robots"
            get_with_sitemap_support "/sitemap.xml.gz", to: "sitemap#sitemap"
            get_with_sitemap_support "/favicon.ico", to: redirect { |path_params, req| "/favicons/#{[AppConfig.quantic_domain, AppConfig.smartly_domain].include?(req.host) ? req.host : AppConfig.smartly_domain}/favicon.ico" }

            ####################################################
            # Email Unsubscribe
            ####################################################

            get_with_sitemap_support "unsubscribe/index"
            get_with_sitemap_support "/unsubscribe", to: "unsubscribe#index"

            ####################################################
            # Terms and Conditions
            ####################################################

            get_with_sitemap_support "/terms", to: "marketing#terms"
            get_with_sitemap_support "/privacy", to: "marketing#privacy"
            get_with_sitemap_support "/cookies", to: "marketing#cookies"
            get_with_sitemap_support "/impressum", to: "marketing#impressum"

        end
    end
end