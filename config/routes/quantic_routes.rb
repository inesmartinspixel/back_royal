require 'http_helpers_extensions'

module QuanticRoutes

    # These URLs also serve as the URLs we redirect EMBA users to for the /interview route.
    # See program_type_config.rb and https://trello.com/c/xx3g7wUC.
    INVITE_TO_INTERVIEW_URL_ASIA = 'https://calendly.com/quantic-interviews/15min-a'
    INVITE_TO_INTERVIEW_URL_FALLBACK = 'https://calendly.com/quantic-interviews/15min'

    # As part of https://trello.com/c/xx3g7wUC, we started sending MBA users to different Calendly URLs.
    # See also program_type_config.rb.
    INVITE_TO_INTERVIEW_URL_MBA = 'https://calendly.com/quantic-interviews/quantic-mba-interview'
    INVITE_TO_INTERVIEW_URL_ASIA_MBA = 'https://calendly.com/quantic-interviews/quantic-mba-interview-a'

    def self.extended(router)
        router.instance_exec do

            # All of these routes are only intended for use on https://quantic.edu

            ####################################################
            # One-off Special pages
            ####################################################

            get_quantic "/blue-ocean-strategy", to: "marketing#signup_candidates", defaults: { target: '/course/learn-blue-ocean-strategy/87b85177-fdf6-4cf8-a29a-c44b0ef77530' }

            ####################################################
            # Marketing Routes specific to Quantic
            ####################################################

            get_quantic "/", to: "marketing#quantic_index"
            get_quantic "/mission", to: "marketing#mission"
            get_quantic "/employers/recruit-our-candidates", to: "marketing#employers"

            get_quantic "/quantic-name-change", to: "marketing#quantic_name_change"
            get_quantic "/accreditation", to: "marketing#accreditation"

            get_quantic "/programs",  to: "marketing#learn_with_smartly"
            get_quantic "/programs/courses", to: redirect('/programs')
            get_quantic "/programs/method", to: "marketing#learn_with_smartly"

            get_quantic "/candidates/learn-with-smartly/courses", to: redirect('/program')
            get_quantic "/candidates/learn-with-smartly/method", to: redirect('/programs/method')
            get_quantic "/candidates/learn-with-smartly", to: redirect('/programs')
            get_quantic "/learn", to: redirect('/programs')
            get_quantic "/learn/:name", to: redirect('/programs')

            get_quantic "/student-experience", to: "marketing#student_experience"

            # This route was active, but unusable; disabling for now
            #get "/reset-password", to: "marketing#reset-password"
            get_quantic "/efficacy", to: "marketing#efficacy"
            get_quantic "/press", to: "marketing#press"
            get_quantic "/about", to: "marketing#about"
            get_quantic "/why-people-love-us", to: redirect('/reviews')
            get_quantic "/reviews", to: "marketing#why_people_love_us"

            get_quantic "/cn/1/home", to: "marketing#cn_landing"
            get_quantic "/cn/1/referral", to: redirect('/cn/1/home')
            get_quantic "/cn/1/:school", to: "marketing#school_landing"

            get_quantic "/start(/:program(/:header_text(/:background(/:form(/:testimonials_or_videos(/:bottom_section(/:form_question_set(/:experiment_id(/:global_network_section(/:nps_section(/:alumni_section(/:infographic_section(/:comparison_chart_section)))))))))))))", to:"marketing#dynamic_landing_page"
            get_quantic "/g/start(/:program(/:header_text(/:background(/:form(/:testimonials_or_videos(/:bottom_section(/:form_question_set(/:experiment_id(/:global_network_section(/:nps_section(/:alumni_section(/:infographic_section(/:comparison_chart_section)))))))))))))", to:"marketing#dynamic_landing_page"

            # Paid landing pages
            # /e/w/m/m/t/s/m/h/s/s/s/s/s
            plp_defaults = { program: 'e', header_text: 'w', background: 'm', form: 'm', testimonials_or_videos: 't', bottom_section: 's', form_question_set: 'm', experiment_id: 'h', global_network_section: 's', nps_section: 's', alumni_section: 's', infographic_section: 's', comparison_chart_section: 's' }
            get_quantic "/offer", to:"marketing#dynamic_landing_page", defaults: plp_defaults
            get_quantic "/offer/:audience", to:"marketing#dynamic_landing_page", defaults: plp_defaults
            get_quantic "/invite", to:"marketing#dynamic_landing_page", defaults: plp_defaults
            get_quantic "/invite/:audience", to:"marketing#dynamic_landing_page", defaults: plp_defaults

            # ModernMBA (tv ad) landing page variant
            # /h/f/r/m/t/s/m/h/h/s/s/h/h
            modern_mba_defaults = { program: 'h', header_text: 'f', background: 'r', form: 'm', testimonials_or_videos: 't', bottom_section: 's', form_question_set: 'm', experiment_id: 'h', global_network_section: 'h', nps_section: 's', alumni_section: 's', infographic_section: 'h', comparison_chart_section: 'h' }
            get_quantic "/ModernMBA", to:"marketing#dynamic_landing_page", defaults: modern_mba_defaults
            get_quantic "/ModernMba", to: redirect('/ModernMBA')
            get_quantic "/modernMBA", to: redirect('/ModernMBA')
            get_quantic "/Modernmba", to: redirect('/ModernMBA')
            get_quantic "/modernmba", to: redirect('/ModernMBA')

            get_quantic "/emba/1/:school", to: "marketing#learn_with_smartly_landing"

            get_quantic "/peer-recommendations/:user_id", to: "marketing#peer_recommendations"

            get_quantic "/interview-guide", to: "marketing#interview_guide"
            get_quantic "/meet-our-admissions-counselors", to: "marketing#meet_our_interviewers"

            get_quantic "/tuition-reimbursement/companies", to: "marketing#tuition_reimbursement_companies"
            get_quantic "/tuition-reimbursement/students", to: redirect('/tuition-assistance/students')
            get_quantic "/tuition-assistance/students", to: "marketing#tuition_reimbursement_students"

            get_quantic "/candidates/how-it-works", to: "marketing#candidates-how-it-works"
            get_quantic "/candidates/join-career-network", to: "marketing#candidates_join_career_network"

            get_quantic "/mba/1/:school", to: "marketing#mba_landing"
            get_quantic "/mba/(:topic)", to: "marketing#the_free_mba"
            get_quantic "/the-free-mba", to: redirect('/mba')

            get_quantic "/emba", to: redirect('/executive-mba')
            get_quantic "/the-executive-mba/intl", to: redirect('/executive-mba')
            get_quantic "/the-executive-mba/intl/(:topic)", to: redirect('/executive-mba')
            get_quantic "/the-executive-mba/(:topic)", to: redirect('/executive-mba')
            get_quantic "/executive-mba/(:topic)", to: "marketing#execmba"

            get_quantic "/open-courses", to: "marketing#open_courses"

            get_quantic "/state-licensing", to: "marketing#state-licensing"

            get_quantic "/school/:name", to: redirect('/start/e/n/b/s/v/n')

            get_quantic "/prices/students", to: "marketing#pricing_students"

            get_quantic "/candidates/signup", to: "marketing#signup_candidates"

            get_quantic "/policies", to: redirect('/regulatory-information')
            get_quantic "/regulatory-information", to: "marketing#policies"

            ####################################################
            # Aliased Links
            ####################################################

            get_quantic "/consumer-disclosure", to: redirect('https://uploads.smart.ly/accreditation/Quantic%20Consumer%20Information%20Disclosure.pdf')
            get_quantic "/school-catalog", to: redirect('https://uploads.smart.ly/accreditation/2020%20Quantic%20School%20Catalog.pdf')

            get_quantic '/interview/expired', to: "marketing#interview_expired"
            get_quantic '/interview(/:email(/:name))', to: redirect { |params, request|
                QuanticRoutes.calendly_interview_url(query_string: request.query_string)
            }
            # We primarily use the /interivew route for scheduling interviews,
            # but we still use this /interview-a route in some HelpScout responses.
            get_quantic '/interview-a(/:email(/:name))', to: redirect { |params, request|
                QuanticRoutes.calendly_interview_url(query_string: request.query_string, force_asia_url: true)
            }

            ####################################################
            # Miscellaneous
            ####################################################

            get_quantic "/contact-form", to: redirect('https://smartly.typeform.com/to/sa0ks7')
            get_quantic "/blog" => redirect("http://blog.smart.ly/")
            get_quantic "/efficacy-form", to: redirect('https://uploads.smart.ly/downloads/efficacy/QUANTICvsB-SCHOOLS_Study.pdf')
            get_quantic "/efficacy-2016-download", to: redirect('https://uploads.smart.ly/downloads/efficacy/QUANTICvsB-SCHOOLS_Study.pdf')
            get_quantic "/efficacy-2017-download", to: redirect('https://uploads.smart.ly/downloads/efficacy/QUANTICvsMOOCS.pdf')

            # The routes below are for external links from within the app, mostly on the student dashboard
            get_quantic "/links/library/calendly", to: redirect('https://calendly.com/quantic-library')
            get_quantic "/links/library/resume-consult", to: redirect('https://calendly.com/resume-consultants/quantic-resume-consult')
            get_quantic "/links/library/research-101", to: redirect('https://uploads.smart.ly/libraryservices/Research%20101%20%28Smartly%20Library%20Guides%29.pdf')
            get_quantic "/links/library/build-your-base", to: redirect('https://uploads.smart.ly/libraryservices/Step-by-Step%20Research%20%28Smartly%20Library%20Guides%29.pdf')
            get_quantic "/links/library/resource-cheat-sheet", to: redirect('https://uploads.smart.ly/libraryservices/Research%20Resource%20Cheat%20Sheet%20%28Smartly%20Library%20Guides%29.pdf')
            get_quantic "/links/library/citation-cheat-sheet", to: redirect('https://drive.google.com/open?id=1mqRYa91xrrtJUSktWtDWm357BuCeSpij')
            get_quantic "/links/library/public-library-resources", to: redirect('https://docs.google.com/spreadsheets/d/1fKSbgx5T6HfjvSRhZhuv6sVs3JwqJTmkIRocwpJnaNo/edit?usp=sharing')
            get_quantic "/links/diploma/frames", to: redirect('https://www.diplomaframe.com/qnti')

            # SignNow resiliency / forwarding endpoint
            get_quantic SignableDocument::ENROLLMENT_AGREEMENT_SIGN_ROUTE, to: "api/cohort_applications#sign"

        end
    end

    def self.calendly_interview_url(query_string:, force_asia_url: false)
        parsed_query = Rack::Utils.parse_query(query_string)

        email = parsed_query['email']
        timezone = parsed_query['tz']
        ignored_timezones_in_asia = ['Asia/Calcutta', 'Asia/Dubai', ' Asia/Kolkata']
        included_timezones_for_asia_url = [
            'Australia/Adelaide',
            'Australia/Brisbane',
            'Australia/Broken_Hill',
            'Australia/Currie',
            'Australia/Darwin',
            'Australia/Eucla',
            'Australia/Hobart',
            'Australia/Lindeman',
            'Australia/Lord_Howe',
            'Antarctica/Macquarie',
            'Australia/Melbourne',
            'Australia/Perth',
            'Australia/Sydney',
            'Pacific/Auckland',
            'Pacific/Chatham'
        ]

        # We send users in Asian timezones to a specific Calendly interview URL, with the exception
        # of a select few Asian timezones listed in the ignored_timezones_in_asia array. Additionally,
        # we also want to send users to this Asia-specific Calendly interview URL if their timezone
        # is one of a select few timezones outside of Asia (included_timezones_for_asia_url). I'm not
        # entirely sure why, but Allison could probably answer that question.
        timezone_in_asia = force_asia_url || (
            timezone && (
                timezone.include?('Asia') && !ignored_timezones_in_asia.include?(timezone) ||
                included_timezones_for_asia_url.include?(timezone)
            )
        )

        endpoint = timezone_in_asia ? QuanticRoutes::INVITE_TO_INTERVIEW_URL_ASIA : QuanticRoutes::INVITE_TO_INTERVIEW_URL_FALLBACK

        # Try to find the user from the email query param and dynamically determine
        # what Calendly interview URL they should be sent to based on their program.
        # See https://trello.com/c/xx3g7wUC
        if email
            user = User.find_by_email(email)

            # if the user has an application but it's not invited to interview, don't forward them to calendly
            # I'm cautious about keeping legacy links working that might not have provided the latest query params
            # FIXME: Eventually, we might want to do this redirect if a valid email is not provided, too. Maybe after a few months.
            if user&.last_application&.invite_to_interview_link_valid?
                endpoint = user.last_application.program_type_config.calendly_interview_url(timezone_in_asia)
            else
                endpoint = "/interview/expired"
            end
        end

        url = "#{endpoint}?#{parsed_query.to_query}"
        url
    end
end
