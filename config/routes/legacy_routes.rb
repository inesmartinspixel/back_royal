require 'http_helpers_extensions'

module LegacyRoutes
    def self.extended(router)
        router.instance_exec do

            get_with_sitemap_support "/login", to: redirect('/sign-in')

            get_with_sitemap_support "/stream/:stream_id/show", to: redirect('/course/%{stream_id}')

            get_with_sitemap_support "/index.html", to: redirect('/')

            get_with_sitemap_support "/difference", to: redirect('/efficacy')
            get_with_sitemap_support "/meet-our-interviewers", to: redirect('/meet-our-admissions-counselors')

            get_with_sitemap_support "/employers/how-it-works", to: redirect("/employers/recruit-our-candidates")
            get_with_sitemap_support "/prices", to: redirect('/prices/employers')
            get_with_sitemap_support "/hiring/signup", to: redirect('/employers/signup')

            get_with_sitemap_support "/signup", to: redirect('/employers/signup?utm_source=card&utm_medium=offline&utm_campaign=hireconf')
            get_with_sitemap_support "join", to: redirect('/candidates/signup')
            get_with_sitemap_support "join/account", to: redirect('/candidates/signup')
            get_with_sitemap_support "join/subscription", to: redirect('/candidates/signup')
            get_with_sitemap_support "/candidates/register", to: redirect('/candidates/signup')

            # Old website redirects (Pre-2017)
            get_with_sitemap_support "/network", to: redirect('/candidates/join-career-network')
            get_with_sitemap_support "/career-network", to: redirect('/candidates/join-career-network')
            get_with_sitemap_support "/training", to: redirect('/corporate-training')
            get_with_sitemap_support "/hiring", to: redirect('/employers/recruit-our-candidates')
            get_with_sitemap_support "/hiring/landing/:name", to: redirect("/employers/recruit-our-candidates/landing/%{name}")
            get_with_sitemap_support "/hiring/profile/:career_profile_id", to: redirect("/hiring/browse-candidates?tab=featured&id=%{career_profile_id}")
            get_with_sitemap_support "/execmba", to: redirect('/executive-mba')
            get_with_sitemap_support "/execmba/intl", to: redirect('/executive-mba')
            get_with_sitemap_support "/companies-why", to: redirect('/employers/recruit-our-candidates')
            get_with_sitemap_support "/student-why", to: redirect('/candidates/how-it-works')
            get_with_sitemap_support "/universities", to: redirect('/corporate-training') # is this right?
            get_with_sitemap_support "/method", to: redirect('/candidates/how-it-works')

            # request from Spanish vendors
            get_with_sitemap_support "/espanol", to: redirect('/sign-in?force_locale=es')
            get_with_sitemap_support "/español", to: redirect('/sign-in?force_locale=es')

            # pre-employer improvement routes
            get_with_sitemap_support "/why", to: redirect('/student-why')
            get_with_sitemap_support "/class", to: redirect('/student-why')

            # marketing campaigns
            get_with_sitemap_support "/clo-exec-mba", to: redirect('/execmba')

            # special redirects for app stores
            get_with_sitemap_support "/smartly-ios", to: "marketing#smartly_ios"
            get_with_sitemap_support "/smartly-android", to: "marketing#smartly_android"

            get_with_sitemap_support "/miyamiya-ios", to: "marketing#miyamiya_ios"
            get_with_sitemap_support "/miyamiya-android", to: "marketing#miyamiya_android"

            # old marketing routes (before we got the MBA license)
            get_with_sitemap_support "/companies/recruit", to: redirect('/hiring/signup')
            get_with_sitemap_support "/companies", to: redirect('/training') # "marketing#companies"
            get_with_sitemap_support "/courses", to: redirect('/mba') # "marketing#courses"

            # old "business IQ" marketing routes
            get_with_sitemap_support "/organizations", to: redirect('/companies')

            # Aliases for landing pages (used to track marketing channels)
            get_with_sitemap_support "/pre-mba", to: redirect('/')  # "marketing#mba"  this was premba; didn't work at the end; might resurrect it for Pre-MBA season...
            get_with_sitemap_support "/trk1", to: redirect('/')     # "marketing#trk1" this was a freemium CPA test run
            get_with_sitemap_support "/trk1/join", to: redirect('/candidates/signup')

            # Routing legacy Blue Ocean Strategy routes to root
            get_with_sitemap_support "/blue-ocean-strategy-demo", to: redirect('/')
        end
    end
end