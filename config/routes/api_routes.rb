module ApiRoutes
    def self.extended(router)
        router.instance_exec do

            ####################################################
            # Authentication
            ####################################################

            # expose devise via token over json api.
            # view responsibility is up to client applications
            mount_devise_token_auth_for 'User', at: 'api/auth', controllers: {
                passwords:          'devise_token_auth/custom_passwords',
                registrations:      'devise_token_auth/custom_registrations',
                sessions:           'devise_token_auth/custom_sessions',
                omniauth_callbacks: 'devise_token_auth/custom_omniauth_callbacks',
            }


            ####################################################
            # HelpScout JSON API Proxy
            ####################################################

            post "/feedback/new", to: "help_scout_proxy#proxy"


            ####################################################
            # Protected JSON API
            ####################################################

            post "/api/users/log_in_as", to: "api/users#log_in_as"
            get "/api/users/:id/show_transcript_data", to: "api/users#show_transcript_data"
            put "/api/users/batch_update", to: "api/users#batch_update"
            put "/api/users/batch_update_grades", to: "api/users#batch_update_grades"
            put "/api/users/batch_update_cohort_enrollment_status", to: "api/users#batch_update_cohort_enrollment_status"

            post "/api/assets", to: "api/assets#create"

            post "/api/auto_suggest_options", to: "api/auto_suggest_options#create"
            put "/api/auto_suggest_options", to: "api/auto_suggest_options#update"

            # S3 APIs
            ['s3_identification_assets', 's3_transcript_assets', 's3_english_language_proficiency_documents'].each do |s3_resource|
                post "/api/#{s3_resource}", to: "api/#{s3_resource}#create"
                delete "/api/#{s3_resource}/:id", to: "api/#{s3_resource}#destroy"
                get "/api/#{s3_resource}/:id/file", to: "api/#{s3_resource}#show_file"
            end

            post "/api/users/:id/upload_avatar", to: 'api/users#upload_avatar'
            post "/api/users/:id/upload_resume", to: 'api/users#upload_resume'

            # For endpoints where filters can get really long, we send index calls as a post
            post "/api/career_profiles/index", to: "api/career_profiles#index"
            post "/api/users/index", to: "api/users#index"
            post "/api/lesson_streams/index", to: "api/stream#index"
            post "/api/lesson_streams/get_outdated", to: "api/stream#get_outdated"
            post "/api/open_positions/index", to: "api/open_positions#index"

            post "/api/auto_suggest_options/upload_icon", to: "api/auto_suggest_options#upload_icon"

            post "/api/cohorts/upload_project_document", to: "api/cohorts#upload_project_document"
            post "/api/cohorts/upload_exercise_document", to: "api/cohorts#upload_exercise_document"

            # We saw an issue where uBlock Origin was blocking our ping updates because of the
            # word "ping". Once we changed the route to "status" they were no longer blocked.
            get "/api/status/simple", to: "api/status#simple"

            # The batch edit users feature needs a POST endpoint to avoid Http parse errors from being thrown.
            # Using a GET request and passing in too many ids/emails will exceed the query string character limit,
            # but by mapping a POST request to the index action we can bypass this issue by using POST data
            # instead of query parameters.
            post "/api/users/batch_edit_users_index", to: "api/users#index"

            # This endpoint is used on the hiring manager details page to batch create hiring relationships
            # between a hiring manager and candidates whose career profiles are in a career profile list.
            post "/api/hiring_relationships/batch_create", to: "api/hiring_relationships#batch_create"

            # This endpoint is used on the open position admin page to batch accept unreviewed interests and save interest priority order
            put "/api/candidate_position_interests/batch_update", to: "api/candidate_position_interests#batch_update"

            # This endpoint is used on the cohorts admin page to update cohort_slack_room_id on applications
            put "/api/cohort_applications/batch_update", to: "api/cohort_applications#batch_update"

            # This endpoint is used in the user editor
            get "/api/cohort_applications/refund_details", to: "api/cohort_applications#refund_details"

            # This endpoint is used on the open position admin page to manually trigger an email to the hiring manager
            # about outstanding and approved candidates for the :id position
            get "/api/open_positions/send_curation_email/:id", to: "api/open_positions#send_curation_email"

            # This endpoint is used for updating payment details in Stripe
            put "/api/subscriptions/modify_payment_details", to: "api/subscriptions#modify_payment_details"


            ####################################################
            # Content APIs
            ####################################################

            content_api = Proc.new do |model, controller|
                namespace :api do
                    # match options call for creates and updates:
                    #  i.e. match "api/lessons', to: 'lesson#options', :constraints => {:method => 'OPTIONS'}
                    get "#{model.table_name}", to: "#{controller}#options", :constraints => {:method => 'OPTIONS'}

                    # match options call for deletes:
                    #  i.e. match "api/lessons/:lesson_guid', to: 'lesson#options', :constraints => {:method => 'OPTIONS'}
                    get "#{model.table_name}/:id", to: "#{controller}#options", :constraints => {:method => 'OPTIONS'}

                    # i.e. get "api/lessons", to: 'lesson#index'
                    get "#{model.table_name}", to: "#{controller}#index"

                    # i.e. get "api/lessons/:lesson_guid", to: 'lesson#show'
                    get "#{model.table_name}/:id", to: "#{controller}#show"

                    # i.e. post "api/lessons", to: 'lesson#create'
                    post "#{model.table_name}", to: "#{controller}#create"

                    # i.e. put "api/lessons", to: 'lesson#update'
                    put "#{model.table_name}", to: "#{controller}#update"

                    # i.e. delete "api/lessons/:lesson_guid", to: 'lesson#destroy'
                    delete "#{model.table_name}/:id", to: "#{controller}#destroy"

                    # i.e. post "api/lessons/:lesson_guid/images", to: 'lesson#images'
                    post "#{model.table_name}/:id/images", to: "#{controller}#images"
                end
            end

            # api methods other than content API
            namespace :api do

                # this must be defined before resources :reports
                get "reports/filter_options", to: "reports#filter_options"

                #resources :users
                resources :coupons
                resources :reports
                resources :groups
                resources :institutions
                resources :cohorts
                resources :learner_projects
                resources :cohort_promotions
                resources :content_topics
                resources :cohort_applications
                resources :career_profiles
                resources :career_profile_lists
                resources :hiring_applications
                resources :auto_suggest_options
                resources :hiring_relationships
                resources :conversations
                resources :zapier_triggers
                resources :subscriptions
                resources :student_dashboards
                resources :curriculum_templates
                resources :open_positions
                resources :hiring_teams
                resources :hiring_team_invites
                resources :candidate_position_interests
                resources :student_network_maps
                resources :student_network_messages
                resources :user_timelines
                resources :project_progress_timelines
                resources :persisted_timeline_events
                resources :billing_transactions
                resources :student_network_events
                resources :deferral_links

                # imitate content_api for iguana consistency
                put "groups", to: "groups#update"
                put "institutions", to: "institutions#update"
                put "cohorts", to: "cohorts#update"
                put "learner_projects", to: "learner_projects#update"
                put "cohort_applications", to: "cohort_applications#update"
                delete "cohort_applications", to: "cohort_applications#destroy"
                put "cohort_promotions", to: "cohort_promotions#update"
                put "subscriptions", to: "subscriptions#update"
                put "career_profiles", to: "career_profiles#update"
                put "hiring_applications", to: "hiring_applications#update"
                put "hiring_relationships", to: "hiring_relationships#update"
                put "hiring_teams", to: "hiring_teams#update"
                put "open_positions", to: "open_positions#update"
                put "conversations", to: "conversations#update"
                put "career_profile_lists", to: "career_profile_lists#update"
                put "curriculum_templates", to: "curriculum_templates#update"
                put "candidate_position_interests", to: "candidate_position_interests#update"
                put "billing_transactions", to: "billing_transactions#update"

                post "content_topics", to: "content_topics#create"
                put "content_topics", to: "content_topics#update"
                delete "content_topics", to: "content_topics#destroy"
                delete "career_profile_lists", to: "career_profile_lists#destroy"

                delete "lesson_streams_progress", to: "lesson_streams_progress#destroy"

                get "/share_career_profile", to: "career_profiles#share" # default stuff messes things up if we do career_profiles/share
                get "career_profile_lists", to: "career_profile_lists#index"

                put "student_network_events", to: "student_network_events#update"

                get "deferral_links/:id", to: "deferral_links#show"
                post "deferral_links", to: "deferral_links#create"
                put "deferral_links", to: "deferral_links#update"
            end

            content_api.call Lesson, "lesson"
            content_api.call Lesson::Stream, "stream"
            content_api.call Lesson::StreamProgress, "stream_progress"
            content_api.call LessonProgress, "lesson_progress"
            content_api.call User, "users"
            content_api.call Playlist, "playlists"
            # todo: tags goes here? rename content_api to iguana_api ?
            content_api.call GlobalMetadata, "global_metadata"
            content_api.call EntityMetadata, "entity_metadata"
            # content_api.call Institution, "institution"
            # todo: why doesnt content_api work?\

            # Events (event_bundle pseudo-model that only handles create api calls)
            namespace :api do
                post "event_bundles", to: "event_bundle#create"
            end

            # Progress clearing
            delete "/api/destroy_all_progress", to: 'api/stream_progress#destroy_all_progress'

            # Application configuration
            get "/api/config",  to: 'api/config#index'

        end
    end
end