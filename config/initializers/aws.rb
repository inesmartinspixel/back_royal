if Rails.env.development? && ENV['AWS_ROLE_ARN_WEBSERVER']

    # Some async webpack output interferes with prompting below
    sleep 6 if ENV['WEBPACK_LAUNCHED_RAILS'] == 'true'

    # Get MFA information
    sts_client = Aws::STS::Client.new
    mfa_serial = sts_client.get_caller_identity.arn.gsub('user','mfa')
    iam_username = mfa_serial.split('/').last

    # Get the token from STDIN
    STDOUT.print "\nInput AWS MFA token for #{iam_username}: "
    STDOUT.flush # without this flush, webpack will not display the content
    token = STDIN.gets.strip

    # Assume the role
    creds = Aws::AssumeRoleCredentials.new(
        client: sts_client,
        role_arn: ENV['AWS_ROLE_ARN_WEBSERVER'],
        role_session_name: "#{ENV['USER'] || SecureRandom.uuid}-#{Time.now.to_i}", # do not use to_timestamp here, because it may not exist yet
        serial_number: mfa_serial,
        token_code: token,
        duration_seconds: 60 * 60 * 4
    )
    Aws.config.update(credentials: creds)


elsif Rails.env.production?

    # see also: https://github.com/aws/aws-sdk-ruby/issues/1301
    # see also: https://docs.aws.amazon.com/sdk-for-ruby/v3/api/Aws/InstanceProfileCredentials.html
    creds = Aws::InstanceProfileCredentials.new(retries: 10)
    Aws.config.update(credentials: creds)

end