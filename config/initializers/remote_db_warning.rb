host = ActiveRecord::Base.connection_config[:host]
if Rails.env.development? && host != 'localhost'
    puts ""
    puts ""
    puts "**********************************************"
    puts "**"
    puts "**"
    puts "You are connecting to a remote db in dev mode (#{host})"
    puts "**"
    puts "**"
    puts "**********************************************"
    puts ""
    puts ""
end