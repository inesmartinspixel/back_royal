module I18n

    def self.provider(provider, locale = nil)
        I18n.t(:"provider_#{provider}", default: provider.titleize, locale: locale)
    end

    LOCALE_NAME_EN = "English"
    LOCALE_NAME_ES = "Spanish"
    LOCALE_NAME_IT = "Italian"
    LOCALE_NAME_ZH = "Chinese"
    LOCALE_NAME_AR = "Arabic"
    LOCALE_NAME_SW = "Swahili"

    # using constants is nice here because it forces a good
    # error with an unexpected locale
    def self.locale_name(locale)
        const_name = :"LOCALE_NAME_#{locale.upcase}"
        const_get(const_name)
    end

    def self.is_locale?(locale)
        const_name = :"LOCALE_NAME_#{locale.upcase}"
        const_defined?(const_name)
    end

end