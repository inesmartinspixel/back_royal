if File.exists?(File.join(Dir.home, '.pryrc'))
    require 'binding_of_caller'

    module Kernel
        define_method('debugger') do
            Pry.start(binding.of_caller(1))
        end
    end
end