REDSHIFT_CONNECTION_CONFIGS = YAML::load(ERB.new(File.read(Rails.root.join("config","database_redshift.yml"))).result)
REDSHIFT_DB_CONFIG = REDSHIFT_CONNECTION_CONFIGS[Rails.env]
REDSHIFT_PROD_READONLY_DB_CONFIG = REDSHIFT_CONNECTION_CONFIGS['redshift_production_readonly']

unless REDSHIFT_DB_CONFIG.key?("store_type")
    raise "redshift config must define store_type"
end
