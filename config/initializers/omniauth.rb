require "omniauth-google-oauth2"
require "omniauth-facebook"
require "omniauth-apple"

module OmniAuth
    module Strategies
        class AppleQuantic < OmniAuth::Strategies::Apple
            option :name, 'apple_quantic'
        end
        class AppleSmartly < OmniAuth::Strategies::Apple
            option :name, 'apple_smartly'
        end
    end
end


# NOTE: This file configures OmniAuth middleware, but Devise also expects to do so.
# In order for this to prevent issues (see also: https://github.com/intridea/omniauth-oauth2/issues/58)
# plug-and-chug providers are configured in the devise.rb initializer
# (see also: https://github.com/zquestz/omniauth-google-oauth2#devise)

OMNIAUTH_SAML_PROVIDERS = Set.new

Rails.application.config.middleware.use OmniAuth::Builder do


    #################################
    # OmniAuth Local Debugging
    #################################

    # # uncomment this if you want to test error handling in dev mode
    # # also see  should_raise_exceptions? in custom_omniauth_callbacks_controller.rb
    configure do |config|
        config.failure_raise_out_environments = []
    end


    #################################
    # OmniAuth Failure Handling
    #################################

    on_failure do |env|

        begin
            exception = env['omniauth.error']
            expected_exception = false

            # This means that the user pressed no when asked if
            # we could have their e-mail address or account information
            query = env['rack.request.query_hash']

            # we know that these scenarios look like the following, so be as specific as possible ...
            expected_google_error = env['omniauth.strategy'].is_a?(OmniAuth::Strategies::GoogleOauth2) && query['error'] == 'access_denied'

            expected_facebook_error = env['omniauth.strategy'].is_a?(OmniAuth::Strategies::Facebook) &&
                        query['error'] == 'access_denied' &&
                        query['error_description'] == 'Permissions error' &&
                        query['error_reason'] == 'user_denied'

            expected_expired_error = (
                exception.message == 'Code was invalid or expired.' ||
                (exception.message.respond_to?(:match) && exception.message.match(/csrf/i))
            )


            # this will be passed along to the custom_omniauth_callbacks_controller#omniauth_failure,
            # which will show a nice message to the user
            if expected_expired_error
                env['omniauth.error.type'] = "invalid_or_expired_token"
                expected_exception = true
            elsif expected_google_error or expected_facebook_error
                env['omniauth.error.type'] = "user_denied"
                expected_exception = true
            end

            Raven.capture_exception(exception, {extra: {'rack.request.query_hash' => query}}) unless expected_exception

            # if this error happened during a saml login, log the saml response
            if env['omniauth.error.strategy'].is_a?(OmniAuth::Strategies::SAML)
                begin
                    # log the error to s3 so we can easily associated it with the response
                    LogToS3.log("omniauth-debugging", "Exception caught in omniauth.rb: #{exception.message.slice(0, 40)}", exception.inspect)

                    # convert the encoded SAMLResponse param into plain xml
                    response_xml = OneLogin::RubySaml::Response.new(env['omniauth.error.strategy'].request.params['SAMLResponse']).response
                    LogToS3.log("omniauth-debugging", "saml-response-that-led-to-error", response_xml)
                rescue Exception => err
                    # no reason to think we'll hit an error, but if we do it's
                    # nice to have some explanation
                    LogToS3.log("omniauth-debugging", "error-retrieving-saml-response", err.inspect)
                end
            end

        # We don't expect any errors above, but even if there
        # is one, we still want to go ahead to the failure
        # endpoint.  So catch all errors here.
        rescue Exception => err
            Raven.capture_exception(err)
        end

        # do the default behavior
        OmniAuth::FailureEndpoint.call(env)

    end


    #################################
    # OmniAuth Cert Handling
    #################################


    def format_cert(env_key)
        return nil unless val = ENV[env_key]
        return val if val.match("\n")

        # When we copy a cert into AWS, line breaks become spaces.
        # Convert them back, but be careful of the spaces in the first
        # and last lines
        val.gsub('BEGIN CERTIFICATE', 'BEGIN--SPACE--CERTIFICATE')
            .gsub('END CERTIFICATE', 'END--SPACE--CERTIFICATE')
            .gsub(' ', "\n")
            .gsub('--SPACE--', ' ')
    end


    #################################
    # SAML Supported Providers
    #################################

    # To test SAML login on local or staging,
    # * login to onelogin using the Onelogin-ori@pedago.com entry on 1password.
    # * Click "Company: Everything" to see all apps
    # * Make sure you are not logged in
    # * For local, click the "Smartly Localhost" app
    # * For staging, click the "Smartly Staging" app

    # We allow clock drift because we got the following error from an inframark login in July 2019: https://sentry.io/organizations/pedago/issues/1132269612/?project=1491374&referrer=slack
    # According to https://github.com/onelogin/ruby-saml, we should fix this by allowing some clock drift

    OMNIAUTH_SAML_PROVIDERS << 'onelogin'
    provider :saml, {
        :name                               => "onelogin",

        # issuer is also called "Audience", and may or may not be needed
        :issuer                             => "smartly",
        :idp_sso_target_url                 => ENV['SAML_ONELOGIN_TARGET_URL'],
        :idp_cert_fingerprint               => ENV['SAML_ONELOGIN_CERT_FINGERPRINT'],
        :allowed_clock_drift                => 2.seconds
    }

    OMNIAUTH_SAML_PROVIDERS << 'inframark'
    provider :saml, {
        :name                               => "inframark",

        # issuer is also called "Audience", and may or may not be needed
        :issuer                             => ENV['SAML_INFRAMARK_IDP_ISSUER'],
        :idp_sso_target_url                 => ENV['SAML_INFRAMARK_TARGET_URL'],
        :idp_cert                           => format_cert('SAML_INFRAMARK_CERTIFICATE'),
        :allowed_clock_drift                => 2.seconds
    }

    OMNIAUTH_SAML_PROVIDERS << 'jll'
    provider :saml, {
        :name                               => "jll",

        # issuer is also called "Audience", and may or may not be needed
        :issuer                             => ENV['SAML_JLL_IDP_ISSUER'],
        :idp_sso_target_url                 => ENV['SAML_JLL_TARGET_URL'],
        :idp_cert                           => format_cert('SAML_JLL_CERTIFICATE'),
        # no idea if setting the assertion_consumer_service_url will help, but we
        # suddenly started getting this error: https://sentry.io/pedago/production-rails/issues/297968075/
        # and it looked like maybe this would do it.
        :assertion_consumer_service_url     => 'https://jll.smart.ly/omniauth/jll/callback',
        :allowed_clock_drift                => 2.seconds
    }
end

# Monkey-patch Omniauth which can't reliably detect whether we are HTTPS during Cloudflare negotiation
module OmniAuth
    module Strategy
    protected
        def ssl?
            request.env['HTTPS'] == 'on' ||
            request.env['HTTP_X_FORWARDED_SSL'] == 'on' ||
            request.env['HTTP_X_FORWARDED_SCHEME'] == 'https' ||
            (request.env['HTTP_X_FORWARDED_PROTO'] && request.env['HTTP_X_FORWARDED_PROTO'].split(',')[0] == 'https') ||
            request.env['rack.url_scheme'] == 'https' ||
            Rails.env.production? # mattl: new addition - force SSL for production contexts
        end
    end
end
