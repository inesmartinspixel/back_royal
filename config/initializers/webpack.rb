Rails.configuration.webpack[:manifest_file] = WebpackHelper.get_parsed_manifest_file_for_klass('module')
Rails.configuration.webpack[:manifest_asset_file] = WebpackHelper.get_parsed_manifest_file_for_klass('asset')
Rails.configuration.webpack[:manifest_locale_file] = WebpackHelper.get_parsed_manifest_file_for_klass('locale')