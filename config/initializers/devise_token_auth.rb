DeviseTokenAuth.setup do |config|

	# By default the authorization headers will change after each request. The
	# client is responsible for keeping track of the changing tokens. Change
	# this to false to prevent the Authorization header from changing after
	# each request.
	config.change_headers_on_each_request = false

    # We have logic on the client that unsets the current user when ng-token-auth
    # detects that the user's tokens have expired. If a user's token expires at an
    # inoportune time (taking an exam, editing a lesson, etc.), this could result in
    # a bad user experience. We need to retain this client-side logic so we can revoke
    # tokens on the server and trust the client does the right thing. Increasing the
    # lifespan of the tokens seems to be the best and most flexible solution.
    # See also: https://trello.com/c/IHlGBNXu
    config.token_lifespan = 10.years

	# Sometimes it's necessary to make several requests to the API at the same
	# time. In this case, each request in the batch will need to share the same
	# auth token. This setting determines how far apart the requests can be while
	# still using the same auth token.
	# config.batch_request_buffer_throttle = 5.seconds

	# This route will be the prefix for all oauth2 redirect callbacks. For
	# example, using the default '/omniauth', the github oauth2 provider will
	# redirect successful authentications to '/omniauth/github/callback'
	config.omniauth_prefix = "/omniauth"

	# By defult sending current password is not needed for the password update.
	# Uncomment to enforce current_password param to be checked before all
	# attribute updates. Set it to :password if you want it to be checked only if
	# password is updated.
	# config.check_current_password_before_update = :attributes

	# By default, only Bearer Token authentication is implemented out of the box.
	# If, however, you wish to integrate with legacy Devise authentication, you can
	# do so by enabling this flag. NOTE: This feature is highly experimental!
	# enable_standard_devise_support = false


    ###############################
    # WHITELISTING SUPPORT
    # see also: https://github.com/lynndylanhurley/devise_token_auth#configuration-cont
    # see also: https://github.com/lynndylanhurley/devise_token_auth/issues/696
    ###############################

    # Construct a list of all viable redirect paths (see also: `routes.rb` / `routes.js`)
    #
    # omniauth always redirects to /home
    # /settings/profile* is needed for forgot my password links to work
    #
    # any other page that uses omniauth needs to be included here for failure situations
    valid_redirect_paths = %w(
        /*
    )

    # Construct a list of all viable host roots
    valid_hosts = []
    if Rails.env.production?
        valid_hosts = %w(
            https://smart.ly
            https://staging.smart.ly
            https://quantic.mba
            https://staging.quantic.mba
            https://quantic.edu
            https://staging.quantic.edu
            file://*
            http://localhost:8080
        )
    else
        require 'socket'

        # Be sure to account for ports using wildcards
        valid_hosts << "http://localhost:*"
        valid_hosts << "*.ngrok.io*"            # ngrok
        valid_hosts << "http://10.0.2.2:*"      # VirtualBox and Android
        valid_hosts << "http://10.0.3.2:*"      # Genymotion
        valid_hosts << "file://*"               # Cordova
        valid_hosts << "http://localhost:8080"  # Cordova WKWebView
        valid_hosts << "http://docker.for.mac.host.internal:8800"  # Puppeteer

        # Add local IP addresses
        addr_infos = Socket.ip_address_list
        addr_infos.each do |addr_info|
            if (addr_info.ip_address =~ /^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$/)
                valid_hosts << "http://#{addr_info.ip_address}:*"
            end
        end
    end

    # Configure valid whitelist combinations
    whitelist = []
    valid_hosts.each do |valid_host|
        valid_redirect_paths.each do |valid_path|
            whitelist << valid_host + valid_path
        end
    end
	config.redirect_whitelist = whitelist

end