class ValidatesTruthOf < ActiveModel::EachValidator

    def validate_each(record, attribute, value)
        if !value
            record.errors.add(attribute, options[:message] || "must be true")
        end
    end

end

class ValidatesNotNil < ActiveModel::EachValidator

    def validate_each(record, attribute, value)
        if value.nil?
            record.errors.add(attribute, options[:message] || "must not be nil")
        end
    end

end

class ActiveRecord::Base

    def self.truncate
        self.connection.execute("TRUNCATE #{self.table_name}")
    end

    def self.validates_truth_of(meth, options = {})
        validates_with ValidatesTruthOf, _merge_attributes([meth, options])
    end

    def self.validates_not_nil(meth, options = {})
        validates_with ValidatesNotNil, _merge_attributes([meth, options])
    end

    def self.has_many_with_validate_false(*args)

        if !args.last.is_a?(Hash)
            args << {}
        end

        if !args.last.key?(:validate)
            args.last[:validate] = false
        end

        # FIXME: what if there are no options?

        self.has_many_without_validate_false(*args)
    end
    class << self
        alias_method :has_many_without_validate_false,  :has_many
        alias_method :has_many, :has_many_with_validate_false
    end

    def self.has_and_belongs_to_many_with_validate_false(*args)
        if !args.last.is_a?(Hash)
            args << {}
        end

        if !args.last.key?(:validate)
            args.last[:validate] = false
        end

        self.has_and_belongs_to_many_without_validate_false(*args)
    end
    class << self
        alias_method :has_and_belongs_to_many_without_validate_false,  :has_and_belongs_to_many
        alias_method :has_and_belongs_to_many, :has_and_belongs_to_many_with_validate_false
    end

    def self.with_statement_timeout(ms, &block)
        ActiveRecord::Base.connection.execute("set statement_timeout=#{ms};")
        yield
    ensure
         ActiveRecord::Base.connection.execute("set statement_timeout=#{ENV['RDS_STATEMENT_TIMEOUT'] || 0};")
    end

    def self.execute_count_estimate(sql)
        quoted = connection.quote(sql)
        connection.execute("SELECT count_estimate(#{quoted}) as count_estimate").to_a[0]['count_estimate']
    end

end

class ActiveRecord::Relation

    def count_estimate
        quoted = ActiveRecord::Base.connection.quote(self.to_sql)
        connection.execute("SELECT count_estimate(#{quoted}) as count_estimate").to_a[0]['count_estimate']
    end

end


ActiveRecord::Base.allow_unsafe_raw_sql = :disabled

