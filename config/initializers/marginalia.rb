Marginalia.application_name = "BR"

Marginalia::Comment.components = [:application, :controller, :action, :job, :line]

Marginalia::Comment.prepend_comment = true
