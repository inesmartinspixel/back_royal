require 'stripe_extensions' # stripe extensions

# Private key used for Stripe connection
unless ENV['STRIPE_SECRET_KEY'].nil?
     Stripe.api_key = ENV['STRIPE_SECRET_KEY']
end

# Prevent unauthoried access to webhooks
# see also: https://github.com/integrallis/stripe_event#securing-your-webhook-endpoint
# see also: https://stripe.com/docs/webhooks#signatures
unless ENV['STRIPE_WEBHOOK_SECRET'].nil?
    StripeEvent.signing_secret = ENV['STRIPE_WEBHOOK_SECRET']
end

# Periodically update this value
# see also: https://stripe.com/docs/upgrades?since=2020-03-02#api-changelog
Stripe.api_version = "2020-03-02"

# Retry Stripe requests automatically and immediately on network failures so that we
# don't have to retry at a later time, and so that Sentry noise is reduced. According
# to the docs the retries are safe and idempotent.
# see also: https://github.com/stripe/stripe-ruby#configuring-automatic-retries
Stripe.max_network_retries = 2

# StripeEvent configuration
StripeEvent.configure do |events|
    events.all do |event|
        customer_id = StripeEventHandler.get_customer_info(event)[:customer_id]

        # Serialize the Stripe::Event for passing to the delayed_job
        serialized_event = Marshal::dump(event)

        if Rails.env.development? && ENV['FORCE_ASYNC_STRIPE'] != 'true'
            # In development it will be useful to continue synchronously handling Stripe
            # events for testing and debugging purposes.
            StripeEventHandler.new(event)
        else
            # Handle Stripe events asynchronously with a job.
            # For why we started doing this, see https://trello.com/c/Nn0EYWst
            # Stripe mentions this is okay in the docs, see https://stripe.com/docs/webhooks/best-practices#acknowledge-events-immediately
            HandleStripeEventJob.perform_later_with_delay(customer_id, event.id, serialized_event)
        end
    end
end