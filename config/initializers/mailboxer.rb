# Explicit `require` calls produce an Uninitialized Constant error when trying to access Mailboxer::Notification in development mode.
# see also: https://stackoverflow.com/questions/58866709/how-can-i-extend-gem-class-in-rails-6-zeitwerk-without-breaking-code-reloading
require 'mailboxer/conversation'
require 'mailboxer/message'
require 'mailboxer/notification'
require 'mailboxer/receipt'


Mailboxer.setup do |config|

  #Configures if your application uses or not email sending for Notifications and Messages
  config.uses_emails = false

  #Configures the default from for emails sent for Messages and Notifications
  config.default_from = "no-reply@mailboxer.com"

  #Configures the methods needed by mailboxer
  config.email_method = :mailboxer_email
  config.name_method = :email # FIXME: this is probably not the right thing to use

  #Configures if you use or not a search engine and which one you are using
  #Supported engines: [:solr,:sphinx]
  config.search_enabled = false
  config.search_engine = :solr

  #Configures maximum length of the message subject and body
  config.subject_max_length = 255
  config.body_max_length = 32000
end

module Mailboxer

    class DisallowedError < RuntimeError; end

    # It is possible for any entity, not just a User, to send messages
    # (though we currently only allow users to send them).  Rather than send
    # ruby classnames down to the client, we map them here.
    # (Note: we have to use the classname here rather than then class, otherwise
    # things will go haywire in devmode when ruby reloads classes)
    def self.recipient_types
        @mailboxer_recipient_types ||= {
            'user' => 'User'
        }
    end

    def self.recipient_type(key)
      self.recipient_types.fetch(key).constantize
    end

    def self.key_for_recipient_type(klass)
      self.recipient_types.key(klass.name)
    end
end

class Mailboxer::Conversation
    include Mailboxer::ConversationExtensions
end

class Mailboxer::Message
    include Mailboxer::MessageExtensions
end

class Mailboxer::Notification
    include Mailboxer::NotificationExtensions
end

class Mailboxer::Receipt
    include Mailboxer::ReceiptExtensions
end