require 'slack'


module Slack

    # Client Overrides

    module Web
        class Client

            def unsafe_send(channel, text, attachments = nil)
                Slack.client.chat_postMessage(channel: channel, text: text, as_user: true, parse: 'full', mrkdwn: true, attachments: attachments)
            end

            def safe_send(channel, text, attachments = nil)
                self.send(channel: channel, text: text, as_user: true, parse: 'full', mrkdwn: true, attachments: attachments)
            rescue Exception => err
                if Rails.env == 'production'
                    Raven.capture_exception(err)
                else
                    raise err
                end
            end

        end

        class MockClient
            def method_missing(*args)
            end
        end
    end

    # Convenience

    def self.client
        unless defined? @client
            @client = ENV['SLACK_TOKEN'] ? Slack::Web::Client.new : Slack::Web::MockClient.new
        end
        @client
    end

    # Does not swallow errors. Used in delayed_job contexts.
    def self.send(channel, text, attachments = nil)
        self.client.unsafe_send(channel, text, attachments)
    end

    # Swallows but logs errors. For use in controllers, etc.
    def self.safe_send(channel, text, attachments = nil)
        self.client.safe_send(channel, text, attachments)
    end

    def self.cohort_payments_channel
        ENV['SLACK_COHORT_PAYMENTS_CHANNEL'] || '#payments'
    end

    def self.hiring_payments_channel
        ENV['SLACK_HIRING_PAYMENTS_CHANNEL'] || '#payments-hiring'
    end

    def self.signups_channel
        ENV['SLACK_SIGNUPS_CHANNEL'] || '#sign-ups'
    end

    def self.talent_notify_channel
        ENV['SLACK_TALENT_NOTIFY_CHANNEL'] || '#talent-notify'
    end

    def self.marketing_notify_channel
        ENV['SLACK_MARKETING_NOTIFY_CHANNEL'] || '#marketing-notify'
    end

    def self.peer_review_notify_channel
        ENV['SLACK_PEER_REVIEW_NOTIFY_CHANNEL'] || '#peer-review-notify'
    end

    def self.engineering_alerts_channel
        ENV['SLACK_ENGINEERING_ALERTS_CHANNEL'] || '#engineering-alerts'
    end

    def self.mba_emba_admin_channel
        ENV['SLACK_MBA_EMBA_ADMIN_CHANNEL'] || '#mba_emba_admin'
    end

end


Slack.configure do |config|
  config.token = ENV['SLACK_TOKEN']
end