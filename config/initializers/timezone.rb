# Timezone library configuration to support timezone lookups using
# latitude and longitude values via the Google Timezone API.
# See https://github.com/panthomakos/timezone#lookup-configuration-with-google.
Timezone::Lookup.config(:google) do |c|
    # During this initial configuration, Timezone will instantiate a lookup instance,
    # but this raises a 'Timezone::Error::InvalidConfig: missing api key' error in CI.
    # According to https://github.com/panthomakos/timezone/issues/58 you can choose
    # to stub the api_key instead.
    c.api_key = ENV['GOOGLE_API_KEY'] || 'stub'
end