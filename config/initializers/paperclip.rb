Paperclip::Attachment.default_options.merge!({
    :storage => :s3,
    :s3_protocol => 'https',
    :s3_region => ENV['AWS_REGION'],
    :url => ':s3_alias_url',
    :use_timestamp => false
})

Paperclip.options[:content_type_mappings] = {
    :jfif => 'image/jpeg'
}


# BD: Monkeypatched from Paperclip 6.0.0 to force-feed `nice` priorities for `convert`
# (and other ImageMagick CLI invocations) to reduce CPU strain during resizes
module Paperclip
  module Helpers

    def run(cmd, arguments = "", interpolation_values = {}, local_options = {})

      cmd = "nice -n 10 #{cmd}" # BD: lower priority a bit (only change!)

      command_path = options[:command_path]
      terrapin_path_array = Terrapin::CommandLine.path.try(:split, Terrapin::OS.path_separator)
      Terrapin::CommandLine.path = [terrapin_path_array, command_path].flatten.compact.uniq
      if logging? && (options[:log_command] || local_options[:log_command])
        local_options = local_options.merge(:logger => logger)
      end
      Terrapin::CommandLine.new(cmd, arguments, local_options).run(interpolation_values)
    end
  end
end
