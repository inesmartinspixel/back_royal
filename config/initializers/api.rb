default_config = YAML.load_file("#{::Rails.root}/config/api.defaults.yml")[::Rails.env] || {}

overrides_file = "#{::Rails.root}/config/api.yml"
overrides = File.exist?(overrides_file) ? YAML.load_file(overrides_file)[::Rails.env] : {}

API_CONFIG = default_config.deep_merge(overrides)