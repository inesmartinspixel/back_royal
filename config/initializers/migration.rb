require File.expand_path("../../../db/migrate/migrate_helper", __FILE__)

module MigrationLogging

    def exec_migration(*args)
        begin
          super(*args)
        rescue Exception => err
            Raven.capture_exception(err)
            raise err
        end
    end

end
ActiveRecord::Migration.send(:prepend, MigrationLogging)
ActiveRecord::Migration.send(:include, MigrateHelper)