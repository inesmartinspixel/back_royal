require 'raven'
require 'raven/integrations/rack'

Raven.configure do |config|

    # configure logging endpoint
    if !ENV['SENTRY_DSN'].nil?
        config.dsn = ENV['SENTRY_DSN']
    else
        config.logger = Logger.new('/dev/null')
    end

    # provide release information if applicable
    if Rails.env.production?
        config.release = 'APPLICATION_BUILD_NUMBER_CI_STAMPED'
    end

    # determine environment for log segmentation
    sentry_environment = 'Local Development'
    if ENV['APP_ENV_NAME']&.match?(/staging/i)
        sentry_environment = 'Staging'
    elsif ENV['APP_ENV_NAME']&.match?(/production/i)
        sentry_environment = 'Production'
    end
    config.current_environment = "#{sentry_environment} - Server"

    config.excluded_exceptions = []
end

# This makes it so you can set `raven_options` on an exception
# and they will get to raven
module SupportRavenOptions
    def capture_type(exception, env, options = {})
        if exception&.respond_to?(:raven_options)
            options.deep_merge!(exception.raven_options || {})
        end
        super(exception, env, options)
    end
end

class Raven::Rack
    class << self

        prepend(SupportRavenOptions)

        # need to re-alias these in order for prepend to catch them too
        alias capture_message capture_type
        alias capture_exception capture_type
    end
end

class Exception
    attr_accessor :raven_options
end

module Raven

    def self.capture_in_production(err_type, raven_options={}, &block)

        begin
            yield
        rescue err_type => err
            if ENV['SENTRY_DSN']
                Raven.capture_exception(err, raven_options)
            else
                raise err
            end
        end
    end

end