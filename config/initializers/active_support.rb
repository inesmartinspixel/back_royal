ActiveSupport::JSON # make sure it is loaded

module ActiveSupport::JSON

    module InvalidUtf8Support
        def decode(json)
            super
        rescue JSON::ParserError => err
            # We have some code in the client that logs events for validation results
            # on user input challenges. Since we cannot limit user input for a variety
            # of reasons, we do not sanitize or alter this input. Because of this, we
            # have seem some errors caused by invalid UTF8 characters.
            #
            # NOTE: as of now, we only ever want to alter the behavior of ActiveSupport::JSON.decode
            #  for the specific case of invalid escaped characters in event bundles. Altering this
            #  behavior elsewhere could have unknown side effects, so we should have a discussion
            #  before we extend this elsewhere.
            #
            # See also: https://trello.com/c/QNygCnYj
            # See also: https://sentry.io/organizations/pedago/issues/940550288/?project=18175&query=is%3Aunresolved&statsPeriod=14d&utc=false
            if err.message.match(/invalid escaped character/) && json.match(/event_type/)
                super(json.gsub(/\\u.{,4}/, "\\uFFFD")) # \uFFFD is the unicode "replacement character"
            else
                raise
            end
        end
    end

    class << self
        prepend InvalidUtf8Support
    end

end