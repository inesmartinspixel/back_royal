# I do not understand why this is necessary. Adding jobs to auto-load paths does not seem to help
%w(ensure_3x_photo_sizes refresh_shared_sitemaps daily_marketing_update).each do |scheduled_job|
    require Rails.root.join("app/jobs/#{scheduled_job}")
end

Delayed::Worker.destroy_failed_jobs = false

Delayed::Worker.sleep_delay = ENV['DJ_SLEEP_DELAY_SECONDS'] ? ENV['DJ_SLEEP_DELAY_SECONDS'].to_i : 5
Delayed::Worker.max_attempts = ENV['DJ_MAX_ATTEMPTS'] ? ENV['DJ_MAX_ATTEMPTS'].to_i : 3
Delayed::Worker.max_run_time = ENV['DJ_MAX_RUN_MINUTES'] ? ENV['DJ_MAX_RUN_MINUTES'].to_f.minutes : 15.minutes
Delayed::Worker.read_ahead = ENV['DJ_READ_AHEAD'] ? ENV['DJ_READ_AHEAD'].to_i : 10

Delayed::Worker.logger = Logger.new(File.join(Rails.root, 'log', 'dj.log'))

module Delayed
    LOW_PRIORITY = 99
end

Delayed::Worker.queue_attributes = {
    # See log_to_customer_io_job.rb for more info.
    log_to_customerio: { priority: LogToCustomerIoJob::DEFAULT_PRIORITY },

    # Since a backfill or something like AdmissionRoundMidpointJob can queue
    # thousands of log_to_segment or identify_user jobs, we drop the priority on
    # these.  This prevents a big batch like this from blocking out all other jobs
    log_to_segment: { priority: LogToSegmentIoJob::DEFAULT_PRIORITY },
    identify_user: { priority: IdentifyUserJob::DEFAULT_PRIORITY }, # regarding IdentifyUserJob::DEFAULT_PRIORITY, see identify_user_job.rb
    get_idology_result: { priority: 1 },
    request_idology_link: { priority: 1 },

    # When a user registers on mobile, an identify_user job and a reconcile_mobile_device
    # job will be enqueued in succession. Since we can only log and identify devices for
    # identified users, we want to prioritize the identify_user job over the
    # reconcile_mobile_device job.
    reconcile_mobile_device: { priority: 2 },

    # This decision job will trigger sync_with_airtable jobs, so make sure it sits higher in priority than them.
    perform_admissions_decision: { priority: 2 },

    # Likewise for batch acceptance, batch rejectance, or backfills
    sync_with_airtable: { priority: SyncWithAirtableJob::DEFAULT_PRIORITY },
    copy_avatar: { priority: 3 },
    update_user_timezone: { priority: 3 },
    cohort__update_final_scores: { priority: 3 },
    rebuild_career_profile_fulltext: { priority: 3 },
    ensure_enrollment_agreement: { priority: 3 },
    update_cohort_status_changes: { priority: 3 },

    send_recommended_positions_email: { priority: 4 },
    delete_associated_user_data: { priority: 4 },
    delete_from_back_royal: { priority: 4 },
    delete_from_redshift: { priority: 4 },
    delete_from_airtable: { priority: 4 },
    delete_from_segment: { priority: 4 },
    delete_from_customer_io: { priority: 4 },
    ensure_stripe_customer: { priority: 4 },
    track_push_notification: { priority: 4 }
}

class Delayed::ForceRetry < RuntimeError
    attr_accessor :max_attempts

    def initialize(message, options = {max_attempts: 3})
        # FIXME: max_attempts can be gutted now. We never actually got it working, and
        # we figured out how to accomplish the same thing using retry_on in the job class.
        self.max_attempts = options[:max_attempts]
        super(message)
    end

end

class ActiveJob::Base

    def error(job, exception)
        Raven.capture_exception(exception)
    end

end


# Log the time to complete each job to cloudwatch
class CloudwatchPlugin < Delayed::Plugin

  callbacks do |lifecycle|

    lifecycle.before(:enqueue) do |job|

        klass = job.payload_object.job_data['job_class'].constantize
        if klass.respond_to?(:get_identifier)
            job.identifier = klass.get_identifier(*job.payload_object.job_data['arguments'])
        end
    end


    lifecycle.around(:invoke_job) do |job, *args, &block|
      begin
        start = Time.now

        begin
            # Forward the call to the next callback in the callback chain
            block.call(job, *args)
        rescue ActiveJob::DeserializationError => err
            raise err unless err.message.include?("Couldn't find User")
        end

        elapsed = Time.now - start

        # log jobs that take longer than 60 seconds
        if elapsed > 60

            metric_name = "Seconds to complete job in '#{job.queue}'"
            environment = ENV['APP_ENV_NAME']

            Cloudwatch.put_metric_data(
                ns_suffix: 'DelayedJob',
                metric_data: [{
                    metric_name: metric_name,
                    value: elapsed,
                    unit: 'Seconds'
                }])

            result = Cloudwatch.describe_alarms_for_metric(metric_name: metric_name, ns_suffix: 'DelayedJob')

            if result && result.metric_alarms.empty?

                Cloudwatch.put_metric_alarm(
                    ns_suffix: 'DelayedJob',
                    alarm_name: "#{environment} - Long time to complete job in '#{job.queue}'",
                    comparison_operator: 'GreaterThanThreshold',
                    evaluation_periods: 1,
                    metric_name: metric_name,
                    period: 1.day, # keep the alarm red for a whole day once it triggers
                    statistic: 'Maximum',
                    threshold: 0.7*Delayed::Worker.max_run_time,
                    alarm_actions: Cloudwatch.default_alarm_actions
                )
            end
        end
      end
    end
  end
end

Delayed::Worker.plugins << CloudwatchPlugin