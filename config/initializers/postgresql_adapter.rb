require 'active_record/connection_adapters/postgresql_adapter'

module SilenceIrrelevantOidWarning

    def warn(*args)
        msg = args[0]

        # career_profiles#location is a field of type geography(Point,4326).
        # The standard postgresql adapter does not know about geography.  We
        # could switch to the postgis adapter, but we don't actually need to
        # deal with this column at all in rails, and it seems a bit risky, so
        # I'm just silencing this warning
        unless msg.match(/unknown OID \d+: failed to recognize type of 'location'. It will be treated as String./)
            super
        end
    end

end
ActiveRecord::ConnectionAdapters::PostgreSQLAdapter.send(:prepend, SilenceIrrelevantOidWarning)