# by default disable segment logging in test env
unless ENV['SEGMENTIO_WRITE_KEY'].nil?
    Analytics = SimpleSegment::Client.new({
        write_key: ENV['SEGMENTIO_WRITE_KEY'],
        on_error: Proc.new { |status, message, e|
            # There may be extra information in the message, but the message can also be really log which will cause issues
            # when capturing with Raven. If in the future we think we need more information logged then we can do so accordingly here.
            # For now just raise the exception and let our global Raven handle it.
            # Raven.capture_exception(RuntimeError.new("Segment error: #{status}: #{message}"))

            raise e
        }
    })
end