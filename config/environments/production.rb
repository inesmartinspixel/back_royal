Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # Code is not reloaded between requests.
  config.cache_classes = true

  # Eager load code on boot. This eager loads most of Rails and
  # your application in memory, allowing both threaded web servers
  # and those relying on copy on write to perform better.
  # Rake tasks automatically ignore this option for performance.
  config.eager_load = true

  # Disable serving static files from the `/public` folder by default since
  # Apache or NGINX already handles this.
  # config.public_file_server.enabled = ENV['RAILS_SERVE_STATIC_FILES'].present?

  # Set to true because we have some locally served generated asset paths. If removed, will get errors like:
  # ActionController::RoutingError (No route matches [GET] "/assets/images/generated/icons-s7b908132a3.png"):
  config.public_file_server.enabled = true


  config.consider_all_requests_local       = false
  config.action_controller.perform_caching = true

  # ensure that we allow exceptions to bubble through to Raven (should be default)
  config.action_dispatch.show_exceptions = false

  # do not allow consumers to signup without a code yet
  config.require_sign_up_code = true

  # Specifies the header that your server uses for sending files
  # config.action_dispatch.x_sendfile_header = "X-Sendfile" # for apache
  # config.action_dispatch.x_sendfile_header = 'X-Accel-Redirect' # for nginx

  # Force all access to the app over SSL, use Strict-Transport-Security, and use secure cookies.
  # config.force_ssl = true

  # See everything in the log (default is :info)
  # config.log_level = :debug

  # Prepend all log lines with the following tags.
  # config.log_tags = [ :subdomain, :uuid ]

  # Use a different logger for distributed setups.
  # config.logger = ActiveSupport::TaggedLogging.new(SyslogLogger.new)

  # Enable serving of images, stylesheets, and JavaScripts from an asset server.
  # config.action_controller.asset_host = 'http://assets.example.com'

  # Enable threaded mode
  # config.threadsafe!

  # Enable locale fallbacks for I18n (makes lookups for any locale fall back to
  # the I18n.default_locale when a translation cannot be found).
  # NOTE: used to allow `true` but no longer: https://github.com/svenfuchs/i18n/releases/tag/v1.1.0
  config.i18n.fallbacks = [I18n.default_locale]

  # Send deprecation notices to registered listeners
  config.active_support.deprecation = :notify

  # Use default logging formatter so that PID and timestamp are not suppressed.
  config.log_formatter = ::Logger::Formatter.new

  # the build boxes don't like to dump sql.  Something about postgres versions? See comment in application.rb.
  config.active_record.schema_format = :ruby

  # support subdomains for saml login, like jll.smart.ly
  config.use_saml_subdomains = true

  # Redis caching support
  config.cache_store = :redis_cache_store, { driver: :hiredis, url: "redis://#{ENV['REDIS_HOST']}:#{ENV['REDIS_PORT']}" }


end
