Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # required to make SAML work on local
  config.action_controller.forgery_protection_origin_check = false

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Do not eager load code on boot.
  config.eager_load = false

  # Show full error reports and disable caching.
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  # in dev mode, saml logins use a query param, like localhost:3001/sign-in?subdomain=jll
  config.use_saml_subdomains = false

  # allow for consumers to sign up without a code in dev
  config.require_sign_up_code = false

  # Restrict the Rails logger max size
  config.logger = ActiveSupport::Logger.new("log/#{Rails.env}.log", 1, 256 * 1024 * 1024)

  # Print deprecation notices to the Rails logger
  config.active_support.deprecation = :log

  # Only use best-standards-support built into browsers
  config.action_dispatch.best_standards_support = :builtin

  # Disable caching in development mode -- http://guides.rubyonrails.org/caching_with_rails.html#activesupport-cache-nullstore
  # (We stopped disabling this around 2018/04/11)
  # config.cache_store = :null_store

  config.active_record.verbose_query_logs = true
  # allow any domain name
  config.hosts.clear

end