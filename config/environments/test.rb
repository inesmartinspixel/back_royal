Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # The test environment is used exclusively to run your application's
  # test suite. You never need to work with it otherwise. Remember that
  # your test database is "scratch space" for the test suite and is wiped
  # and recreated between test runs. Don't rely on the data there!
  config.cache_classes = true

  # Do not eager load code on boot. This avoids loading your whole application
  # just for the purpose of running a single test. If you are using a tool that
  # preloads Rails for running tests, you may have to set it to true.
  config.eager_load = false

  # Configure static file server for tests with Cache-Control for performance.
  config.public_file_server.enabled   = true
  config.public_file_server.headers = { 'Cache-Control' => 'public, max-age=3600' }

  # allow for consumers to sign up without a code in test
  config.require_sign_up_code = false

  # Show full error reports and disable caching
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  # Raise exceptions instead of rendering exception templates.
  config.action_dispatch.show_exceptions = false

  # Disable request forgery protection in test environment.
  config.action_controller.allow_forgery_protection = false

  # Randomize the order test cases are executed.
  config.active_support.test_order = :random

  # Print deprecation notices to the stderr.
  config.active_support.deprecation = :stderr

  config.cache_store = :memory_store

  # support subdomains for saml login, like jll.smart.ly
  config.use_saml_subdomains = true

  if PuppetDbConfig.enabled?
    config.paths['config/database'] = [PuppetDbConfig.datapase_config_path]
  end

  # Restrict the Rails logger max size
  config.logger = ActiveSupport::Logger.new("log/#{Rails.env}.log", 1, 256 * 1024 * 1024)

  # used when running `npm run testserver`
  if ENV['ENABLE_LOGGING']
    config.action_controller.logger = Logger.new(STDOUT)
    config.active_record.logger = Logger.new(STDOUT)
  end

end