const webpack = require('webpack');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const path = require('path');

const FixStyleOnlyEntriesPlugin = require('webpack-fix-style-only-entries');
const ManifestPlugin = require('webpack-manifest-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const StatsPlugin = require('stats-webpack-plugin');

const webpackAssets = require('./webpack/webpack.assets');
const webpackManifests = require('./webpack/webpack.manifests');
const webpackLocales = require('./webpack/webpack.locales');

//---------------------------
// Dynamic Config / Export
//---------------------------

module.exports = (env, argv) => {
    //---------------------------
    // Build flags
    //---------------------------

    const isDevServer = !!(env && env.devServer);
    const isCordovaBuild = !!(env && env.cordova);

    const criticalLocale = env && env.criticalLocale ? env.criticalLocale : 'en';
    const frontRoyalLocalesRegex = new RegExp(`.*/tmp/(?!.*${criticalLocale}).*.json$`, 'i');
    const modulesLocalesRegex = new RegExp(`.*/modules/.*/locales/.*${criticalLocale}.json$`);

    //---------------------------
    // Resolving modules
    //---------------------------

    const resolve = {
        descriptionFiles: ['package.json'],
        modules: ['node_modules', path.resolve(__dirname, './vendor/front-royal/modules')],
        extensions: ['.js', '.jsx'],
        alias: {
            assets: path.resolve(__dirname, './public/assets'),
            tmp: path.resolve(__dirname, './tmp'),
            images: path.resolve(__dirname, './tmp/images'),
            vectors: path.resolve(__dirname, './tmp/vectors'),
            styles: path.resolve(__dirname, './vendor/front-royal/styles'),
            commonFonts: path.resolve(__dirname, './vendor/common/fonts'),
            nodeModules: path.resolve(__dirname, './node_modules'),
            common: path.resolve(__dirname, './vendor/common'),
            frontRoyal: path.resolve(__dirname, './vendor/front-royal'),
            marketing: path.resolve(__dirname, './vendor/marketing'),
            modules: path.resolve(__dirname, './vendor/front-royal/modules'),
            Hammer: 'hammerjs', // we should probably fork angular-gestures to require 'hammerjs' instead of 'Hammer'
        },
    };

    //---------------------------
    // Entry points (chunking)
    //---------------------------

    // NOTE: Each entry point results in a separate JS bundle (common.js, front-royal.js, etc.)
    // in public/assets/scripts, as well as a corresponding CSS bundle (if the file imports a *.scss file)
    // in public/assets/styles. In our context, each entry point is a "dependency file", which is responsible
    // for pulling in everything we need for said bundle. We also make use of webpack's SplitChunksPlugin (see `optimization`
    // config), so any third-party vendors code required by an entry point and any code required by multiple entry points
    // will be separated into their own bundles. This means the resulting bundles created by webpack could have names
    // like front-royal.js, vendors~front-royal.js, vendors~admin~front-royal.js, admin~front-royal.js, vendors~admin.js,
    // and admin.js, for example.
    //
    // We also specify some .scss entry points here. These entry points result in a CSS bundle, as well as a
    // _needless_ javscript bundle that we go in and blow away during the build. Unfortunately, Webpack doesn't
    // give us a great way to point to a .scss file and say "I want that compiled as a standalone CSS bundle".
    // We generally include .scss files in our JS entry point, which will result in ONE corresponding CSS bundle.
    // In some instances, that doesn't really work with our project structure at the moment, and including those
    // here seems to be the best solution.

    // Entry points shared between web and cordova applications
    const baseEntryPoints = {
        common: path.resolve(__dirname, `./vendor/common/deps.js`),
        certificates: path.resolve(__dirname, './vendor/front-royal/modules/Certificates/entryPoint.js'),
        'front-royal': path.resolve(
            __dirname,
            `./vendor/front-royal/deps${argv.mode === 'development' ? '.dev' : ''}.js`,
        ),
        'one-trust': path.resolve(__dirname, './vendor/front-royal/modules/Onetrust/static/oneTrust/entryPoint.js'),
        'one-trust-quantic': path.resolve(
            __dirname,
            './vendor/front-royal/modules/Onetrust/static/oneTrust-quantic/entryPoint.js',
        ),

        // CSS-only entrypoints
        assessment: path.resolve(__dirname, './vendor/front-royal/styles/main/assessment.scss'),
        'font-families': path.resolve(__dirname, './vendor/common/styles/font-families.scss'),
        'arabic-font-families': path.resolve(__dirname, './vendor/common/styles/arabic-font-families.scss'),
    };

    // Additional entry points used only in web application
    const webEntryPoints = {
        admin: path.resolve(__dirname, './vendor/front-royal/modules/Admin/angularModule/index.js'),
        editor: path.resolve(__dirname, './vendor/front-royal/modules/Editor/angularModule/index.js'),
        marketing: path.resolve(__dirname, './vendor/marketing/deps.js'),
        'inline-head-scripts': path.resolve(
            __dirname,
            './vendor/front-royal/modules/InlineHeadScripts/inline-head-scripts.js',
        ),

        // This is inlined at the top of the page
        'dynamic-landing-page': path.resolve(
            __dirname,
            './vendor/front-royal/modules/DynamicLandingPage/dynamic-landing-page.deps.js',
        ),

        // 'auth-forms-app': path.resolve(__dirname, './vendor/front-royal/modules/AuthFormsApp/entryPoint.js'),
        'marketing-assets': path.resolve(__dirname, './vendor/marketing/deps.assets.js'),
        'script-injector': path.resolve(__dirname, './vendor/front-royal/modules/ScriptInjector/entryPoint.js'),
        reports: path.resolve(__dirname, './vendor/front-royal/modules/Reports/angularModule/index.js'),

        // CSS-only entrypoints
        translatable_export: path.resolve(__dirname, './vendor/front-royal/styles/main/translatable_export.scss'),
        'dynamic-landing-page-styles': path.resolve(__dirname, './vendor/marketing/styles/dynamic_landing_page.scss'),
    };

    const entryPoints = isCordovaBuild
        ? baseEntryPoints
        : {
              ...baseEntryPoints,
              ...webEntryPoints,
          };

    //---------------------------
    // Bundle output
    //---------------------------

    const outputs = {
        filename: isDevServer ? 'scripts/[name].js' : 'scripts/[name]-[contenthash:8].js',
        path: path.resolve(__dirname, 'public/assets'),
        publicPath: '/',
    };

    //---------------------------
    // Loaders
    //---------------------------

    function getFileLoaderConfig() {
        return [
            {
                loader: 'file-loader',
                options: {
                    esModule: false, // https://github.com/webpack-contrib/file-loader/pull/340
                    emitFile: true,
                    name(file) {
                        // Get rid of the filename and leading directories
                        let _path = '';

                        if (file.match(/^.*?public\/assets\//)) {
                            _path = file
                                .split(/[^/]+$/)[0]
                                .split(/^.*?public\/assets\//)
                                .pop();
                        } else if (file.match(/^.*?vendor\/(common|front-royal|marketing)\//)) {
                            _path = file
                                .split(/[^/]+$/)[0]
                                .split(/^.*?vendor\/(common|front-royal|marketing)\//)
                                .pop();
                        } else if (file.match(/^.*?tmp\//)) {
                            _path = file
                                .split(/[^/]+$/)[0]
                                .split(/^.*?tmp\//)
                                .pop();
                            _path = file.match(/.*?locales-/) ? `locales/${_path}` : _path;
                        }

                        return _path + (isDevServer ? '[name].[ext]' : '[name]-[hash:8].[ext]');
                    },
                    publicPath: '/assets/',
                },
            },
        ];
    }

    function getStyleLoaderConfigsForStyleKlass(klass) {
        const sourceMap = !isDevServer;

        const loaderConfigs = [
            {
                loader: MiniCssExtractPlugin.loader,
                options: {
                    hmr: !!isDevServer,
                    sourceMap,
                },
            },
            {
                loader: 'css-loader',
                options: {
                    sourceMap,
                },
            },
            {
                loader: 'resolve-url-loader',
            },
            {
                loader: 'postcss-loader',
                options: {
                    ident: 'postcss',
                    parser: 'postcss-scss',
                    plugins:
                        argv.mode === 'development'
                            ? [autoprefixer()]
                            : [
                                  autoprefixer(),
                                  cssnano({
                                      discardComments: {
                                          removeAll: true,
                                      },
                                      discardDuplicates: true,
                                      discardEmpty: true,
                                      discardOverriden: true,
                                  }),
                              ],
                },
            },
        ];

        if (klass === 'sass') {
            loaderConfigs.push({
                loader: 'sass-loader',
                options: {
                    sassOptions: {
                        includePaths: [
                            path.resolve(__dirname, './vendor/front-royal/styles'),
                            path.resolve(__dirname, './node_modules/'),
                        ],
                        sourceMapContents: false,
                    },
                    sourceMap,
                },
            });
        }

        return loaderConfigs;
    }

    const modules = {
        rules: [
            //---------------------------
            // Globally Shimmed Modules
            //---------------------------

            {
                test: path.resolve(__dirname, './node_modules/jquery'),
                use: [
                    {
                        loader: 'expose-loader',
                        options: '$',
                    },
                    {
                        loader: 'expose-loader',
                        options: 'jQuery',
                    },
                    {
                        loader: 'expose-loader',
                        options: 'jquery',
                    },
                ],
            },
            {
                test: path.resolve(__dirname, './node_modules/underscore'),
                use: [
                    {
                        loader: 'expose-loader',
                        options: '_',
                    },
                ],
            },
            // Used in some erb templates
            {
                test: require.resolve('ua-parser-js'),
                use: [
                    {
                        loader: 'expose-loader',
                        options: 'UAParser',
                    },
                ],
            },
            // Used in angular-selectize
            {
                test: require.resolve('selectize'),
                use: [
                    {
                        loader: 'expose-loader',
                        options: 'Selectize',
                    },
                ],
            },
            {
                test: path.resolve(__dirname, './node_modules/plotly.js/dist/plotly.min.js'),
                use: [
                    {
                        loader: 'expose-loader',
                        options: 'Plotly',
                    },
                ],
            },
            {
                test: path.resolve(__dirname, './vendor/front-royal/modules/Certificates/scripts/pdfkit-0.7.1.js'),
                use: [
                    {
                        loader: 'expose-loader',
                        options: 'PDFDocument',
                    },
                ],
            },
            {
                test: path.resolve(
                    __dirname,
                    './vendor/front-royal/modules/Certificates/scripts/blob-stream-v0.1.3.js',
                ),
                use: [
                    {
                        loader: 'expose-loader',
                        options: 'blobStream',
                    },
                ],
            },
            // Used in some Pixelmatters marketing code
            {
                test: path.resolve(__dirname, './vendor/marketing/scripts/sm-form.js'),
                use: [
                    {
                        loader: 'expose-loader',
                        options: 'smForm',
                    },
                ],
            },

            //-------------------------------
            // END Globally Shimmed Modules
            //-------------------------------

            {
                parser: {
                    amd: false,
                },
                include: /node_modules\/lodash/,
            },
            {
                test: /\.jsx?$/,
                use: [
                    {
                        loader: 'babel-loader',
                    },
                ],
                exclude: /node_modules/,
            },
            {
                test: /\.jsx?$/,
                loader: 'string-replace-loader',
                options: {
                    multiple: [
                        {
                            // FIXME: We think this was added to fix an issue in Safari regarding
                            // strict mode, probably the one mentioned for version 9.1 in https://caniuse.com/#feat=const
                            // So I'd bet that the issue is no longer relevant. And, we have now run a lebab 'no-strict'
                            // pass on our code to stop it from triggering eslint errors. But, just to be on the safe
                            // side I'm leaving it for now.
                            // See https://trello.com/c/8xBmDe4l
                            // See https://pedagollc.slack.com/archives/C04NSHLEW/p1537542148000100
                            search: 'use strict',
                            replace: 'xxx strict',
                        },
                        {
                            // Everywhere in our code base, we only require the developer to `import` english
                            // locales. We rely on an environment variable passed into Webpack to determine
                            // if we need ot build with a different locale. If we find that we do, we replace
                            // occurences of `en` with the correct locale where appropriate.
                            search: 'window.Smartly.locales.(.*).en(.*)en.json(.*)$',
                            replace: (match, p1, p2, p3) =>
                                `window.Smartly.locales.${p1}.${criticalLocale}${p2}${criticalLocale}.json${p3}`,
                            flags: 'gm',
                        },
                        {
                            // After doing the above, we need to replace the original references to the critical
                            // locale with `en` so english locales are built.
                            // We need the escape characters here, so the parentheses in the require statement
                            // aren't treated as a capture group.
                            // eslint-disable-next-line no-useless-escape
                            search: `^require\\('(.*)${criticalLocale}.json'\\);$`,
                            replace: (match, p1) => `require('${p1}en.json');`,
                            flags: 'gm',
                        },
                        {
                            // We also need to rewrite any statements that import json files.
                            search: `^import(.*)en.json';$`,
                            replace: (match, p1) => `import${p1}${criticalLocale}.json';`,
                            flags: 'gm',
                        },
                    ],
                },
            },
            {
                // Misc assets such as images, audio files, and fonts
                test: /\.(png|jpg|gif|svg|mp3|ttf|eot|eot\?#iefix|otf|woff(2)?)$/i,
                use: getFileLoaderConfig(),
            },
            {
                // Locale .json amalgamations
                type: 'javascript/auto', // See also: https://github.com/webpack/webpack/issues/6586
                test: frontRoyalLocalesRegex, // Critical locales are bundled into the JS amalagamations
                use: getFileLoaderConfig(),
                exclude: /node_modules/,
            },
            {
                test: /\.css$/,
                use: getStyleLoaderConfigsForStyleKlass('css'),
            },
            {
                test: /\.(sa|sc)ss$/,
                use: getStyleLoaderConfigsForStyleKlass('sass'),
            },
            {
                test: /vendor\/front-royal\/modules\/.*\/angularModule\/.*.html$/,
                // Right to left order - templateLoader gets it first.
                use: [{ loader: 'html-loader' }, { loader: path.resolve('./webpack/ngTemplateLoader.js') }],
            },
            {
                test: modulesLocalesRegex,
                type: 'javascript/auto',
                use: [
                    {
                        loader: path.resolve('./webpack/localeLoader.js'),
                        options: {
                            criticalLocale,
                            isDevServer,
                        },
                    },
                ],
            },
        ],
    };

    //---------------------------
    // Plugins
    //---------------------------

    const plugins = [
        // Inline plugin
        {
            apply: compiler => {
                // When compilation is done, build manifests
                compiler.hooks.done.tap('ManifestGenerationPlugin', () => {
                    webpackLocales.copyAndStampModulesLocales(isDevServer);
                    webpackManifests.buildManifests(isDevServer);
                });
            },
        },

        new MiniCssExtractPlugin({
            filename: isDevServer ? 'styles/[name].css' : 'styles/[name]-[contenthash:8].css',
        }),

        // If Webpack encounters any variables corresponding to the key in these key/value pairs,
        // it will automatically include the corresponding npm package in the modules that need it.
        // This is useful, and necessary, for code that relies on globals (i.e. `$`) where we may or
        // may not be loading the associated package before the code that relies on it. This also means
        // we never have to import these packages manually, anywhere.
        new webpack.ProvidePlugin({
            ScrollReveal: 'scrollreveal',
            React: 'react',
            diff_match_patch: 'diff-match-patch',
        }),

        // Do not output JS chunks for .scss entry points
        new FixStyleOnlyEntriesPlugin(),

        new ManifestPlugin({
            fileName: path.resolve(__dirname, './config/webpack_manifest.json'),
            filter: file => file.path && file.path.match(/\.(js|css)$/),
            publicPath: '/assets/',
            writeToFileEmit: true,
        }),

        // FIXME: Figure out a way to load a subset of the locales?
        // Prevent moment.js from including every single locale file
        new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),

        new StatsPlugin('webpack_stats.json', {
            chunkModules: true,
            exclude: [/(node_modules|tmp)/],
        }),

        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify(argv.mode),
            },
            'window.bundledLocale': JSON.stringify(criticalLocale),
        }),
    ];

    //---------------------------
    // webpackAssets Init
    //---------------------------

    webpackAssets.initialize(isDevServer, isCordovaBuild);

    //---------------------------
    // Exported Configs
    //---------------------------

    const config = {
        devtool: 'cheap-eval-source-map', // overwritten in ./webpack/webpack.production.config.js
        entry: entryPoints,
        module: modules,
        output: outputs,
        stats: {
            colors: true,
            hash: false,
            version: false,
            timings: false,
            assets: false,
            chunks: false,
            modules: false,
            reasons: false,
            children: false,
            source: false,
            errors: true,
            errorDetails: true,
            warnings: true,
            publicPath: false,
        },
        optimization: {
            // This splitChunks optimization is responsible for preventing the duplication of code
            // in our application bundles. See https://webpack.js.org/plugins/split-chunks-plugin/.
            splitChunks: {
                chunks: 'all',
                maxInitialRequests: 10,
                maxAsyncRequests: 10,
                cacheGroups: {
                    // Authentication, Careers, and Lessons angularModules are dependencies for other
                    // modules distributed across various entrypoints. In production mode, Webpack often
                    // decides to _not_ chunk these modules, and instead duplicates them across
                    // entrypoints. While there may be some wisdom in relying on Webpack's defaults,
                    // we think we know better than them and don't want to duplicate this code.
                    authentication: {
                        test: /.*Authentication\/angularModule\//,
                        enforce: true,
                    },
                    careers: {
                        test: /.*Careers\/angularModule\//,
                        enforce: true,
                    },
                    lessons: {
                        test: /.*Lessons\/angularModule\//,
                        enforce: true,
                    },
                },
            },
            // By default, each entry point in the webpack config will include some boilerplate runtime
            // code needed by webpack to resolve modules. Setting this runtimeChunk optimization setting
            // to 'single' creates a single file containing the boilerplate runtime code to be shared
            // between all of the generated chunks. This is important because otherwise imported modules
            // are initialized for each runtime chunk separately, which is a common problem for webpack
            // configs with multiple entry points.
            // See https://webpack.js.org/configuration/optimization/#optimizationruntimechunk.
            runtimeChunk: 'single',
        },
        plugins,
        resolve,
    };

    return config;
};
