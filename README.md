Back Royal
===========


Back Royal is the code that backs https://quantic.edu.  It is written in Rails, Angular and Node.

Basic Setup
-----------

*NOTE: These steps are required to get a barebones marketing stack setup. See below for advanced development setup.*

1. Setup Bash as your default shell

    `chsh -s /bin/bash`

1. Install homebrew at [http://brew.sh/](http://brew.sh/)

    Add sbin to your path: `echo $"export PATH=\$PATH:/usr/local/sbin" >> ~/.bash_profile`

    Re-source your bash_profile: `source ~/.bash_profile`

1. Install git-lfs, OpenSSL, CoreUtils, rename, libyaml GPG2, wget, magic-wormhole, and GNU-sed

    `brew install git-lfs openssl coreutils rename libyaml gnupg wget magic-wormhole gnu-sed`

1. Clone the back_royal repo:

    `mkdir -p ~/Development/Pedago/back_royal`

    `git lfs clone git@bitbucket.org:pedago-ondemand/back_royal.git ~/Development/Pedago/back_royal`

1. Add the following to `~/.bash_profile` on your dev machine:

    `alias br='cd ~/Development/Pedago/back_royal/'`

    `test -f ~/Development/Pedago/back_royal/bash/back-royal.bash && source ~/Development/Pedago/back_royal/bash/back-royal.bash`

1. Re-source your bash_profile: `source ~/.bash_profile`

1. Initialize git-lfs and pull within `back_royal`

    `br`

    `git lfs install --local`

    `git lfs pull`

1. Install Node

    1. Install whatever version of Node.js is defined in the project's `package.json` from [the Node distro repro](https://nodejs.org/dist/)

1. Open up write access node_modules

    `sudo chown -R $USER /usr/local/lib/node_modules`

    `sudo chown -R $USER /usr/local/bin`

    `sudo chown -R $USER /usr/local/share`

1. (Optional) Install redis-cli

    `brew tap ringohub/redis-cli`

    `brew update && brew doctor`

    `brew install redis-cli`

1. Install PostgreSQL

    1. Install postgres.app v2.3.5 from [http://postgresapp.com](http://postgresapp.com)
    1. Click the button in the bottom left to open the left preferences pane
    1. Click initialize to start a new server running Version 12
    1. Add Postgres.app to your path: `echo $"export PATH=\$PATH:/Applications/Postgres.app/Contents/Versions/latest/bin" >> ~/.bash_profile`
    1. Re-source your bash_profile: `source ~/.bash_profile`

1. Install RVM with appropriate certs and Ruby version

    `curl -sSL https://rvm.io/mpapis.asc | gpg --import -`

    `curl -sSL https://rvm.io/pkuczynski.asc | gpg --import -`

    `\curl -sSL https://get.rvm.io | bash -s stable`

    `rvm install 2.6.6 --disable-binary --with-openssl-dir=$(brew --prefix openssl)`

    `rvm osx-ssl-certs update all`

1. Add RVM to your path:

    `echo $"[[ -s '\$HOME/.rvm/scripts/rvm' ]] && source '\$HOME/.rvm/scripts/rvm'" >> ~/.bash_profile`

    `echo $"export PATH=\$PATH:$HOME/.rvm/bin" >> ~/.bash_profile`

    `source ~/.bash_profile`

1. Setup `application.yml`

    1. `cp config/application.yml.sample config/application.yml` and update values as necessary when encountering issues
    1. Alternatively, talk to another developer to get pre-populated values (contains secrets) securely.

1. Copy database.yml

    `cp config/database.sample.yml config/database.yml`

    `cp config/database_redshift.sample.yml config/database_redshift.yml`

1. Setup Pry as Ruby IRB and debugger

    `cp .irbrc ~`

    `cp .pryrc ~`

1. Install Gem and Node dependencies

    `gem install bundler`

    `bundle install`

    Obtain an `.npmrc` file for FortAwesome NPM repository access (see "To maintain Font Awesome Pro" below)

    `npm ci` and then run `git status` and delete any new directories added inside of node_modules

1. Install git hooks:

    `cp git_hooks/* .git/hooks/`

1. Get a local database up and running

    1. To use a seed DB, set `USE_SEED_DB: 'true'` in your `application.yml` config and run `rake db:setup; rake db:migrate; rake db:refresh_materialized_views`

    1. Otherwise, for an anonymized dump, see instructions instructions [on the wiki](https://pedago.atlassian.net/wiki/spaces/TECH/pages/10354691/Syncing+your+local+database+to+production)

1. Setup the test databases by running `rtd`

1. (Optional) Verify specs

    `rspec`

    `npm run test:all`

1. Start up a webserver (Rails will be backgrounded)

    `rails s & npm run devserver`

1. Now you should have smartly running in a browser.  Rejoice!



Advanced Setup
--------------

*Note: These steps are required to have a complete development environment setup, sufficient to connect to remote databases and perform various application features*

1. Install ImageMagick (This can take a while. Necessary once you want to upload an image in the app/editor)

    `brew install imagemagick`

1. Install fonts on your system that are needed by the certificate generation script

    `mkdir -p ~/.config/ImageMagick`

    `cp lib/font_config/type.xml ~/.config/ImageMagick/`

    `perl -pi -e 's/\/usr\/share\/fonts\/opentype\//\/Users\/'$USER'\/Library\/Fonts\//g' ~/.config/ImageMagick/type.xml`

1. Obtain a `~/.pgpass` file from another developer

    `chmod 600 ~/.pgpass`

1. Copy over [Redshift ACM Root CA](https://docs.aws.amazon.com/redshift/latest/mgmt/connecting-transitioning-to-acm-certs.html#connecting-transitioning-to-acm-other-ssl-types)

    1. `mkdir -p ~/.postgresql/; code ~/.postgresql/root.crt`
    1. copy the `content` block in `.ebextensions/20_redshift_certs.config` over to that file (removing the whitespace on the left)

1. Create a dev Redshift store

    **NOTE** The staging-events database is rebuilt once per week, on Sundays. This results in
    the dropping of any personal databases created by the below steps.

    1. `tunnel-redshift-staging`
    1. `setupreddev`

1. Setup Ngrok

    1. Talk to your manager about getting setup on the ngrok account

1. Settup AWS Console account

    1. Request AWS access setup from a Sr. Engineer
    1. You'll obtain an IAM username, a temporary password, and a QR code for MFA setup
    1. Ensure you setup a *personal* 1Password entry for these credentials
    1. Load the [AWS Console for pedago-dev](https://https://pedago-dev.signin.aws.amazon.com/) and reset your password
    1. Open your browser Javascript console and run the snippet found in `config/aws/roles/cookies/cookie.aws.developer`, replacing the `userName` value with your IAM user. This will populate the assumable role dropdown options for access convenience.

1. Install AWS CLI

    1. Run `brew install awscli`
    1. Request AWS access key credentials from a Sr. Engineer
    1. Run `aws configure` and supply your AWS Access Key / Secret and default region of `us-east-1`
    1. Run `cp config/aws/roles/cli/config.aws.developer ~/.aws/config`
    1. Update `~/.aws/config`, replacing the username prompts with your IAM username value

1. Install Docker (used for CI + screenshot diffs)

    1. Install the latest version of docker from the [Docker Store](https://store.docker.com/editions/community/docker-ce-desktop-mac). You will be prompted to create or use an existing DockerID account.
    1. Launch Docker and follow the install dialogs to get the `docker` CLI in your path.
    1. Navigate to Preferences > Advanced in the Docker toolbar menu. Max CPU / memory resources and increase swap to something like 4G.
    1. Run `docker login` (or login via the toolbar menu option). You will be prompted for your DockerID credentials.
    1. Run `docker pull booleanbetrayal/circleci-trusty-ruby-node-python-awscli-psql-chrome:1.0.21` to download our CI image (also used for screenshot diffs).
    1. Feel free to terminate Docker if you do not need to work with any screenshot diffs or debug CI images. These are currently the only components requiring Docker.

1. Setup PGAntomizer (for anonymization debugging)

    1. `git clone https://github.com/booleanbetrayal/pgantomizer.git`
    1. `cd pgantomizer`
    1. `git reset --hard; git checkout master; git branch -D misc-customizations; git pull; git checkout misc-customizations;`
    1. `cat files.txt | xargs sudo rm -f; sudo rm -rf .egg build dist; sudo python3 setup.py install --record files.txt`
    1. Re-run the last 2 commands whenever you want to pull / apply new changes to the existing `pgantomizer` installation

1. [Zapier setup](zapier/README.md)

1. [Mobile App Setup](HYBRID.md)

1. Speak with a Sr. Engineer about obtaining Bastion tunnel access. Be sure to run `ssh smartly-bastion` after setup to accept the key!


To Test
-------

### Rails

`rspec` build fixtures and run all ruby specs

`sfb=1 rspec`run all ruby specs without building fixtures first (you generally want to run this if you are repeatedly running specs)

`f=1 rspec` run all ruby specs defined with fdescribe or fit

`rake test:build_fixtures` build fixtures but do not run specs

`sfb=1 rspec ./spec/path/to/some_spec.rb` run all the specs defined in a particular file

`sfb=1 rspec ./spec/path/to/some_spec.rb:32` run the spec defined in line 32 of a particular file


### Front Royal (Angular app)

NOTE: Be sure you are running `npm run devserver` if you want changes to HTML files to be detected on watch update. This is due to the fact that we currently build out templates outside of the Webpack / Jest loader pipeline.

`npm run test` # run all javascript tests with watches
`npm run test:once` # run all javascript tests and exit

See also `jest/config.modules.json` for Jest configuration and `jest/setup.modules.js` for project dependencies.


### Puppeteer Specs (Current-Gen Screenshot Tests)

The majority of screenshot creation and validation will need to be handled in a Docker container, since
environmental differences between OS / architecture combinations (ie - OSX and our CI environment) yields
subtle differences in text rendering / image rasterization / etc. As a result, all the primary `puppet`
commands are executed against our CI container, but we retain `debug*` commands for local debugging purposes.

For more details on the `puppet` aliases and workflow, see the [Screenshots README](spec/puppeteer/README.md)



To Run local webserver
------

`rails s` will startup the rails server
`npm run devserver` will perform a build and setup watch targets for compilation and livereload

Note: Feel free to run them both by backgrounding Rails and launching Webpack via: `rails s & npm run devserver`


To test production redshift data with your local database
------
1. Change your `database_redshift.yml` file to use `redshift_dev_on_staging` for the `development` profile
1. Log in to redshift on staging: `psqlredst`
1. Create a database (if you don't have one already) suffixed with your **dev machine user's name**. In my case: `create database red_royal_theblang;`
1. Run `rake redshift:db:migrate` to create the necessary tables in your staging red_royal database
1. Dump events you that will need from `production` to your staging red_royal events database by using `rake redshift:copy_events_to_dev`. Be sure to pass a `w` argument as the where clause in order to only get a subset of the events (otherwise it might take a long, long time to copy).
1. Run `rake db:refresh_materialized_views` to fill your local database's views with the event data from your staging red_royal events database

NOTE: If you find yourself running into this error:

```
26.236605 to refresh public.user_can_see_playlist_locale_packs
rake aborted!
ActiveRecord::StatementInvalid: PG::FeatureNotSupported: ERROR:  Specified types or functions (one per INFO message) not supported on Redshift tables.
CONTEXT:  Error occurred on dblink connection named "unnamed": could not execute query.
: refresh materialized view concurrently etl.user_lesson_progress_records
```

Then be sure that your local database's materialized views were not created with the Postgres `now()` function (perhaps while you were still on `redshift_dev_on_local` or something), which is not supported in Redshift. I wanted to highlight this because the way it fails is so very cryptic. See `now_meth` variable.


To test payment notifications
------

In our Stripe account, configure in the “Webhooks” settings section to point a Test webhook to your dynamic-IP's `/payment/hooks` endpoint.

Monitor the rails logs of that environment's back_royal instance.

Create a subscription in through the UI, then cancel this subscription from Stripe. Stripe should send a webhook notification to back_royal notifying our app of the change.


To configure staging / production
-------
Most of these environment variables can be set / accessed through the Elastic Beanstalk application configuration panel. We are hitting a 4k limit though and have since started offloading any problematic-length Rails-specific environment variables to a secure S3 bucket (eb-application-configs). Changes to these configurations will require an application re-deploy to take effect.


To test SAML integration
--------
SAML is kind of like oAuth, except for institutions.  We use it to allow our institutional customers to login
to smart.ly with their single sign-on systems.

To test this, we use okta, which is a single-sign-on system for which we have an account.  You should
have an account on okta set up using your @pedago.com email.  You will create a password for yourself.

There are 3 smartly applications in okta.  "Smartly Local" and "Smartly Staging" can be used for testing
those instances.  "Smartly" was intended to become JLL's actual prod application, but they ended up creating
their own app within their internal instance, which is actually better.  We could decide to make Smartly an open app in Okta's public app
network at some point if we want.  I think okta manually configured it to only be available for JLL, though,
so we would probably have to contact them and get them to undo it or create a new one or something.

If we ever have another institution that wants to get setup the way JLL is, by creating an
okta app on their internal instance, there are instructions in a pdf in the doc/ directory
of the back_royal app.  There is also a .png file with a logo that will be needed in order
to follow those instructions.

To login to a Smartly instance from okta:

1. login to okta (https://dev-900860.oktapreview.com/app/UserHome) with username=nate+passwordisPassword@pedago.com and password=Password1
1. go to "Home" if you are not there already
1. Click on the "Smartly Local" or "Smartly Staging" link (see below for setting up your local to work with okta)

To setup local instance to accept logins from okta:

1. login to okta (https://dev-900860.oktapreview.com/app/UserHome) with your account, which should have admin rights
1. Click 'Admin' in the top right
1. Click 'Applications'
1. Click 'Smartly Local'
1. Click 'Sign on'
1. Click 'View Setup instructions'
1. In your application.yml, set the following properties:
  * `SAML_JLL_TARGET_URL`: value of 'Identity Provider Single Sign-On URL:' on that page
  *` SAML_JLL_IDP_ISSUER`: 'smartly'
  * `SAML_JLL_CERTIFICATE`: value of 'X.509 Certificate:' on that page, following the format example in `application.yml.sample`

Note: we cannot currently test JLL's actual production login system because we no longer have an
active user.


To test Google Analytics' Optimize plugin
--------
We've integrated the Google Analytics (GA) Optimize plugin on the marketing pages for A/B testing via the GA destination settings in Segment.io.
If you'd like to learn more about GAs' Optmize plugin, see [this webpage](https://support.google.com/optimize/answer/6197440?hl=en&ref_topic=6314903)
or you're unfamiliar with A/B testing, refer to [this webpage](https://support.google.com/optimize/answer/6211930?hl=en) for a general overview.

GA provides [detailed instructions](https://support.google.com/optimize/answer/6262084#optimize-ga-plugin) on how to add the Optimize plugin to your
site in several different ways; however, Segment provides a single configuration setting in the GA destination that will accomplish the same thing.
Since we've already setup the GA destination in Segment for other purposes, we've chosen to delegate the responsibility of inegrating the plugin
onto our site to Segment.io. As a caveat of adding Optimize via Segment, Optimize won't initialize until after Segment has been loaded onto our site
and any experiments created in Optimize won't run until the Optimize plugin has been fully loaded. This loading pattern creates a gap of time between
when our site has initially loaded and when the GA Optimize plugin has initialized, and when it does initialize, the page may flicker because the experiments
have loaded. This is a known issue with the Optimize plugin and Google has [detailed instructions](https://support.google.com/optimize/answer/6262084#page-hiding)
on how to prevent the page from flickering, which requires you to include a javascript snippet that will hide the page contents until the Optimize plugin
has been initialized or until a set timeout has expired, whichever comes first. However, since we're only using the Optimize plugin on a handful of marketing
pages, we've opted to only include the page hiding snippet on pages that need it. This is configured via the `GOOGLE_OPTIMIZE_ACTIVE_ROUTES` environment
variable (see `application.yml.sample` for more info). If you'd like to understand more about the page hiding javascript snippet, go [here](https://developers.google.com/optimize/devguides/pagehiding).

To test your Optimize experiments locally:

1. Ensure the *Optimize Container ID* Google Analytics destination setting in Segment.io is set.

    1. Navigate to the [Google Analytics destination settings page](https://app.segment.com/pedago/destinations/google-analytics/sources/local-development/configuration) in Segment.io and check if the *Optimize Container ID* setting is set. If the setting is already configured, continue to the next step, otherwise proceed with the instructions in this step.
    1. Navigate to the [Google Optimize dashboard for the Local Development Container](https://optimize.google.com/optimize/home/#/accounts/4178634089/containers/9979215).
    1. Copy the *Container ID* for the *Local Development Container*.
    1. Navigate back to the [Google Analytics destination settings page](https://app.segment.com/pedago/destinations/google-analytics/sources/local-development/configuration).
    1. Set the *Optimize Container ID* destination setting to the *Container ID* value you copied from the *Local Development Container*.

1. Set the `GOOGLE_OPTIMIZE_CONTAINER_ID` environment variable in your `application.yml` to the *Optimize Container ID* destination setting from the [Google Analytics destination settings page](https://app.segment.com/pedago/destinations/google-analytics/sources/local-development/configuration).
1. Create an experiment.

    1. Navigate to the [Google Optimize dashboard for the Local Development Container](https://optimize.google.com/optimize/home/#/accounts/4178634089/containers/9979215)
    1. Create an experiment (see the [Google Optimize docs](https://support.google.com/optimize/answer/6211930?hl=en) for more information on how to create an experiment) and ensure that the **editor page** URL is set to your personal ngrok URL.

1. Include the **editor page** in the `GOOGLE_OPTIMIZE_ACTIVE_ROUTES` environment variable in your `application.yml` (see `application.yml.sample` for more info on configuring this environment variable).
1. Fire up ngrok, ensuring that the public domain matches the domain you set for the **editor page** when you created the experiment. **NOTE:** You need to use ngrok because Optimize will only recognize localhost has a valid URL.
1. Navigate to the URL you set as the **editor page** when you created the experiment and verify that you see your experiment. **NOTE:** Your session data will be sent to Google Analytics, but there's at least a couple hour delay for the results to show up in the Google Optimize dashboard.


Notes on single-page-application integration
-------


See note above in "To Run" about running `npm run devserver` during development.

# .html files that are referenced by angular directives or routes
  - Included in amalgamated / minified Javascript source.

# .js files in front-royal
  - Processed by Webpack (see also: webpack.config.js) (included in: build / build-dev)
  - Non-spec files amalgamated and minified by Terser **without mangle**
  - Sourcemaps are generated and copied into target public/assets along with reference source

# .scss files in vendor/front-royal
  - Processed by Webpack (see also: webpack.config.js)

# all static assets
  - Processed by `npm run build` task (intended for distribution. only included in build)


Localization Notes
-----

We store locale files at the component level. This is handy for development, but tricky for sending locales to translators. To make this a bit easier, we have two rake tasks that allow batch export and import of the locales, along with some handy tools for creating scratch translations via the Google Translate API. See the instructions below.

## Backfill Translations ##

When adding new locale entries, you can backfill locale entries with this task:

```
#!sh

rake translate:backfill target=es [simulate=false] [force=false] [verbose=false]
```

It's helpful to run it the first time with simulate=true and verbse=true to make sure it's going to do what you expect.

## Export Script ##

To export all of the locales in your project to a single CSV, do the following:

```
#!sh

rake translate:export_consolidated_file target=es [verbose=false]
```

```target``` is optional; it defaults to "es". Changing the target just changes the header in the empty column of the spreadsheet that the translators would be expected to fill in.

This script also attempts to grab comments that occur after each line of the JSON and includes them in a column, to provide an easy way to leave comments for translators.

## Import Script ##

The second script is intended to import a filled-out version of this file.

```
#!sh

rake translate:import_consolidated_file file=/file/path/locales.csv` target=es [force=false] [translate=false] [verbose=false] [simulate=false]
```

```target``` specifies the locale you are importing translations to, and ```file``` specifies the location of the CSV to import. Both are required arguments.

The script also includes several optional arguments:

* ```simulate=true``` don't write any files, and don't spend money by actually invoking Google Translate API
* ```translate=true``` use google translate API to translate any blank entries
* ```force=true``` overwrite existing locale entries, instead of skipping them if they already are in the target locale file. If this isn't enabled, only blank or new locale entries will be updated/added.
* ```verbose=true``` output info for every locale entry and file

There are some handy combinations you might want to try:

### Preview What the Script Will Import ###

```
#!sh

rake translate:import_consolidated_file file=/file/path/locales.csv` target=es verbose=true simulate=true
```

### Force Overwrite Temp Translations with Real Translations  ###

The standard workflow we'll use after getting translations back. This will overwrite all

```
#!sh

rake translate:import_consolidated_file file=/file/path/locales.csv` target=es force=true
```

### Batch Google-Translate a Just-Exported CSV ###

If you have a CSV you've just exported that has all blanks in the target locale column, you can immediately feed it back to this script and use Google Translate to back-fill missing values before writing to disk.

Note: I've enabled simulate here so this example won't actually invoke the API or save the files.

```
#!sh

rake translate:import_consolidated_file file=/file/path/locales.csv` target=es force=true translate=true verbose=true simulate=true
```

### Move and delete keys from existing translation files ###

If you want move the files for all languages from one location to another:
```
#!sh

# Remove entries for 'connect' from `vendor/*/components/careers/locales/candidate_action_buttons-*.json`
f=hiring_featured_candidates t=vendor/front-royal/components/careers/locales/candidate_action_buttons rake translate:move_translation_files
```

If you have a key that you want to remove from all languages of a particular file:
```
#!sh

# Remove entries for 'connect' from `vendor/*/components/careers/locales/candidate_action_buttons-*.json`
k=careers.candidate_action_buttons.connect rake translate:remove_key
```

If you have a key that you want to move from all languages of one file to all languages for a different file:
```
#!sh

# Move entries for 'like' from `vendor/*/components/careers/locales/hiring_featured_candidates-*.json` to `vendor/front-royal/components/careers/locales/candidate_action_buttons-*.json`
k=careers.hiring_featured_candidates.like t=vendor/front-royal/components/careers/locales/candidate_action_buttons rake translate:move_key
```

To maintain Font Awesome Pro
--------
Font Awesome Pro has been installed via npm. In order to update, follow these steps:

1. Log in to fontawesome.com using login info from 1Password in the Engineering vault

1. On account page, find "Pro npm Package Token"

1. Create a `.npmrc` file in `/back_royal` with the following (secret token is saved as the "Pro npm Package Token" field in the FontAwesome entry in 1Password):

    `@fortawesome:registry=https://npm.fontawesome.com/`

    `//npm.fontawesome.com/:_authToken=OUR_SECRET_TOKEN`

1. Run update command in Terminal

1. **IMPORTANT!!!** `.npmrc` is `.gitignore`d, do not un-ignore this file. Additionally, notify developers if this key changes.
