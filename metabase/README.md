Upgrade Notes
=============

1. Be sure to add the following `.ebextensions` files into the archived zip, along with existing configs, prior to upload. After generating / updating the zip, be sure to prune it of garbage: `zip -d filename.zip __MACOSX/\*`
2. Be sure to name the EB version name / description to indicate that it is a customized installation.