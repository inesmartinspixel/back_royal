###############################################
##  Install memory monitoring for auto-scale
##  see also: http://aws.amazon.com/code/8720044071969977
##  see also: http://docs.aws.amazon.com/cli/latest/reference/cloudwatch/put-metric-alarm.html
###############################################

container_commands:

  00_install_perl_dependencies:
    test: test ! -f /opt/elasticbeanstalk/support/.mem-monitor-setup-complete
    command: yum install -y perl-DateTime perl-Sys-Syslog perl-LWP-Protocol-https perl-Switch perl-URI perl-Bundle-LWP

  01_download:
    test: test ! -f /opt/elasticbeanstalk/support/.mem-monitor-setup-complete
    command: wget http://aws-cloudwatch.s3.amazonaws.com/downloads/CloudWatchMonitoringScripts-1.2.0.zip

  02_extract:
    test: test ! -f /opt/elasticbeanstalk/support/.mem-monitor-setup-complete
    command: unzip CloudWatchMonitoringScripts-1.2.0.zip

  03_rmzip:
    test: test ! -f /opt/elasticbeanstalk/support/.mem-monitor-setup-complete
    command: rm CloudWatchMonitoringScripts-1.2.0.zip

  04_cdinto:
    test: test ! -f /opt/elasticbeanstalk/support/.mem-monitor-setup-complete
    command: mv aws-scripts-mon/ /home/ec2-user

  05_cron:
    test: test ! -f /opt/elasticbeanstalk/support/.mem-monitor-setup-complete
    command: crontab -l | grep -q 'mon-put-instance-data.pl' || crontab -l | { cat; echo '* * * * * /home/ec2-user/aws-scripts-mon/mon-put-instance-data.pl --mem-util --mem-used --mem-avail --from-cron'; } | crontab -

  06_setup_mem_alarm:
    test: test ! -f /opt/elasticbeanstalk/support/.mem-monitor-setup-complete
    command: "INSTANCE_ID=`/opt/aws/bin/ec2-metadata -i | sed 's/^.*: //'` AWS_DEFAULT_REGION=us-east-1 sh -c 'aws cloudwatch put-metric-alarm --alarm-name \"$APP_ENV_NAME - Low Available Memory ($INSTANCE_ID)\" --alarm-description \"Alarm when memory is < 300\" --metric-name MemoryAvailable --namespace System/Linux --statistic Average --period 300 --threshold 300 --comparison-operator LessThanThreshold --alarm-actions $SNS_NAME --unit Megabytes --evaluation-periods 1 --dimensions --dimensions Name=InstanceId,Value=$INSTANCE_ID'"

  07_setup_credits_alarm:
    test: test ! -f /opt/elasticbeanstalk/support/.mem-monitor-setup-complete
    command: "INSTANCE_ID=`/opt/aws/bin/ec2-metadata -i | sed 's/^.*: //'` AWS_DEFAULT_REGION=us-east-1 sh -c 'aws cloudwatch put-metric-alarm --alarm-name \"$APP_ENV_NAME - Low CPU Credits ($INSTANCE_ID)\" --alarm-description \"Alarm when T2 CPU Credits is < 10\" --metric-name CPUCreditBalance --namespace AWS/EC2 --statistic Average --period 300 --threshold 10 --comparison-operator LessThanThreshold --alarm-actions $SNS_NAME --unit Count --evaluation-periods 1 --dimensions --dimensions Name=InstanceId,Value=$INSTANCE_ID'"


  08_write_installation_completion_flag:
    cwd: /opt/elasticbeanstalk/support
    command: touch .mem-monitor-setup-complete