#################################################################################################################
## Nginx Custom Configuration
## We are modifying max body sizes / buffers and adding support for an S3 proxy for serving previously prepared
## assets, along with special backlog `listen` directive setup to match our sysctl settings.
#################################################################################################################

files:
    "/opt/elasticbeanstalk/support/conf/webapp_healthd.conf":
        mode: "000755"
        owner: root
        group: root
        content: |

           upstream my_app {
             server unix:///var/run/puma/my_app.sock;
           }

           # allow 100M uploads - restricted in client
           client_max_body_size 100M;

           # We were running into 502 errors when logging in with Facebook.
           # see also - https://www.ruby-forum.com/topic/4422529
           proxy_buffers 8 512k;
           proxy_buffer_size 2024k;
           proxy_busy_buffers_size 2024k;
           proxy_read_timeout 3000;

           log_format healthd '$msec"$uri"'
                           '$status"$request_time"$upstream_response_time"'
                           '$http_x_forwarded_for';

           server {
             listen 80 backlog=50000;
             server_name _ localhost; # need to listen to localhost for worker tier

             if ($time_iso8601 ~ "^(\d{4})-(\d{2})-(\d{2})T(\d{2})") {
               set $year $1;
               set $month $2;
               set $day $3;
               set $hour $4;
             }

             access_log  /var/log/nginx/access.log  main;
             access_log /var/log/nginx/healthd/application.log.$year-$month-$day-$hour healthd;

             large_client_header_buffers 4 32k;
             client_body_buffer_size     32k;
             client_header_buffer_size   8k;

             location / {
               limit_except GET HEAD POST PUT OPTIONS DELETE { deny all; } # smartly customization
               proxy_pass http://my_app; # match the name of upstream directive which is defined above
               proxy_set_header Host $host;
               proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
             }

             location /assets {
               alias /var/app/current/public/assets;
               gzip_static on;
               gzip on;
               expires max;
               add_header Cache-Control public;
             }

             location /public {
               alias /var/app/current/public;
               gzip_static on;
               gzip on;
               expires max;
               add_header Cache-Control public;
             }


             # Resolver appear to only work for proxy_pass directives with variable interpolation
             # see comment on: https://trac.nginx.org/nginx/ticket/723
             set $proxypass_target    http://s3.amazonaws.com;

             location ~ ^/assets/(scripts|styles|locales|fonts) {

                 # We've seen Nginx make failing IPv6 requests, so disable specifying Cloudflare's DNS
                 resolver 1.1.1.1 ipv6=off;

                 # Rewrite rule to capture URI pattern
                 rewrite ^/assets/(scripts|styles|locales|fonts)/(.*)$ /uploads.smart.ly/deployment_assets/$1/$2 break;

                 # Limit to GET
                 limit_except GET {
                   deny all;
                 }

                 # Proxy passthrough
                 proxy_pass              $proxypass_target;

                 # Header modification
                 proxy_http_version     1.1;
                 proxy_set_header       Connection "";
                 proxy_hide_header      x-amz-id-2;
                 proxy_hide_header      x-amz-request-id;
                 proxy_hide_header      x-amz-expiration;
                 proxy_hide_header      Set-Cookie;
                 proxy_ignore_headers   Set-Cookie;

                 # Error handling
                 proxy_intercept_errors on;

                 # AWS S3 sends 403s on unavailable objects
                 error_page 403 =404 /;

                 # Downstream cache strategy
                 expires max;
             }
           }



    "/etc/nginx/conf.d/maintenance.conf.BAK" :
        mode: "000755"
        owner: root
        group: root
        content: |
            server {

                large_client_header_buffers 4 32k;
                client_body_buffer_size     32k;
                client_header_buffer_size   8k;

                location / {
                    return 503;
                }

                error_page 503 @maintenance;
                location @maintenance {
                        rewrite ^(.*)$ /maintenance.html break;
                }

                location = /health {
                    return 200;
                    access_log off;
                }
            }



    "/etc/sysctl.conf":
        mode: "000644"
        owner: root
        group: root
        content: |

            # AWS LINUX DEFAULTS
            net.ipv4.ip_forward = 0
            net.ipv4.conf.default.rp_filter = 1
            net.ipv4.conf.default.accept_source_route = 0
            kernel.sysrq = 0
            kernel.core_uses_pid = 1
            net.ipv4.tcp_syncookies = 1
            kernel.msgmnb = 65536
            kernel.msgmax = 65536
            kernel.shmmax = 68719476736
            kernel.shmall = 4294967296

            # OUR TUNING
            # See also: http://www.queryadmin.com/1654/tuning-linux-kernel-tcp-parameters-sysctl/
            fs.file-max = 150000
            kernel.pid_max = 4194303
            net.core.netdev_max_backlog = 3240000
            net.core.somaxconn = 50000
            net.ipv4.tcp_max_tw_buckets = 1440000
            net.ipv4.ip_local_port_range = 1024 65535
            net.ipv4.tcp_window_scaling = 1
            net.ipv4.tcp_max_syn_backlog = 3240000


commands:
  00_reload_sysctl_conf:
    command: sysctl -p
  01_update_gzip_options:
    test: test ! -f /opt/elasticbeanstalk/support/.post-nginx-config-complete
    cwd: /etc/nginx
    command: perl -pi -e 's/(http {)/$1\n    gzip             on;\n    gzip_comp_level  9;\n    gzip_types       application\/json text\/css image\/gif image\/jpeg application\/javascript application\/x-javascript;/' nginx.conf
  99_write_post_nginx_config_complete:
    cwd: /opt/elasticbeanstalk/support
    command: touch .post-nginx-config-complete


container_commands:
  00_copy_maintenance:
    command: cp /var/app/ondeck/public/maintenance.html /usr/share/nginx/html/
  01_copy_health_conf_to_standard:
    command: cp /opt/elasticbeanstalk/support/conf/webapp_healthd.conf /opt/elasticbeanstalk/support/conf/webapp.conf
  02_set_maintenance:
    cwd: /etc/nginx/conf.d
    command: if [ "$SHOW_MAINTENANCE_PAGE" = "true" ]; then cp maintenance.conf.BAK maintenance.conf; else rm -f maintenance.conf; fi
  03_restart_nginx:
    command: service nginx restart




