#!/bin/bash

function eb-ssh {

    # profile validation
    local profile=$1
    if [ -z "$profile" ]; then
        echo "You must specify a valid profile as defined in '~/.aws/config'."
        kill -INT $$
    fi

    # environment validation
    local environment=$2
    if [ -z "$environment" ]; then
        echo "You must specify an environment."
        kill -INT $$
    fi

    # instance number parsing / validation
    local instance_num
    for WORD; do
        case $WORD in
            -n?)
                instance_num=${WORD:2:1}
                shift ;;
            -n)
                if [[ ${4:0:1} != "-" && ${4:0:1} != "" ]]; then
                    instance_num=$4
                    shift 2
                else
                    echo "Instance number param provided but without value."
                    kill -INT $$
                fi ;;
            -n*)
                instance_num=${WORD:3}
                shift ;;
            -*)
                echo "Unrecognized argument"
                kill -INT $$
            ;;
        esac
    done
    if ! [ $instance_num -eq $instance_num 2>/dev/null ]; then
        echo "You must provide an integer value for instance number."
        kill -INT $$
    fi


    # grabs a list of instance InstanceId and PublicIpAddress for the environment
    local connect_info=$(aws ec2 describe-instances --profile ${profile} --filters "Name=tag:elasticbeanstalk:environment-name,Values=${environment}" "Name=instance-state-name,Values=running" --query 'Reservations[].Instances[].[InstanceId,PrivateIpAddress]' --output text)
    if [ -z "$connect_info" ]; then
        echo "No Environment found for EnvironmentName = '${environment}'."
        kill -INT $$
    fi

    # if no instance_num was provided, provide the options
    local instance_ids=($(echo "$connect_info" | cut -f1))
    if [ -z "$instance_num" ]; then
        echo -e "\nSelect an instance to ssh into"
        i=0
        for instance_id in "${instance_ids[@]}"; do
            ((i++))
            echo "${i}) ${instance_ids[i-1]}"
        done

        local finished
        while [ ! ${finished} ]; do
            printf "(default is 1): "
            read input

            if [ ${#input} -eq 0 ]; then
                instance_num=1
                finished=true
            elif [ $input -eq $input 2>/dev/null ] && (( $input >=1 && $input <= ${#instance_ids[@]} )); then
                instance_num=$input
                finished=true
            else
                echo "Sorry, that is not a valid choice. Please choose a number between 1 and ${#instance_ids[@]}."
            fi
        done
    else
        if ! (( $instance_num >= 1 && $instance_num <= ${#instance_ids[@]} )); then
            echo "Invalid instance number (${instance_num}) for environment with ${#instance_ids[@]} instances."
            kill -INT $$
        fi
    fi

    # gather IPs for selection
    local ips=($(echo "$connect_info" | cut -f2))
    local ip_to_connect=${ips[$instance_num-1]}
    local ssh_command="ssh -A ec2-user@${ip_to_connect} -o \"proxycommand ssh -W %h:%p smartly-bastion\""
    echo "Running: ${ssh_command}"
    eval $ssh_command
}