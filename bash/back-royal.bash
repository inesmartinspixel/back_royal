#!/bin/bash

# Prerequisites:
#
#   1. Go to "Keychain Access -> Login -> Passwords -> +" and add an entry for each key with:
#      Keychain Item Name = 'smartly-android-upload-store' or 'smartly-android-upload-key'
#      Account Name = your username on your machine (i.e. the result of whoami)
#      Password = you can get this from brent, if you actually needed, or leave this value blank
#
#   2. Install ngrok <https://ngrok.com/download> and set `$NGROK_SUBDOMAIN` in your `~/.bash_profile`



##########################
# General Environment    #
##########################

export SMARTLY_ANDROID_STORE="`security find-generic-password -w -s smartly-android-upload-store`"
export SMARTLY_ANDROID_KEY="`security find-generic-password -w -s smartly-android-upload-key`"
export PGHOST=localhost
PATH=$PATH:/Applications/Postgres.app/Contents/Versions/latest/bin:./node_modules/.bin

# editor setup
export VISUAL=code
export EDITOR=code
export GIT_EDITOR="code -n -w"



##########################
# ngrok Tunneling        #
##########################

if [ -n "$NGROK_SUBDOMAIN" ]; then
    alias ngrok-smartly='ngrok http -subdomain=$NGROK_SUBDOMAIN 3000'
    alias smartly-ngrok='ngrok-smartly'
else
    echo "Please supply an export value for NGROK_SUBDOMAIN in your ~/.bash_profile to enable the 'ngrok-smartly' command!"
fi



##########################
# DB Templating          #
##########################

alias copy2to1='dropdb back_royal_development; createdb -T back_royal_development2 back_royal_development;'
alias copy1to2='dropdb back_royal_development2; createdb -T back_royal_development back_royal_development2;'
alias copy3to2='dropdb back_royal_development2; createdb -T back_royal_development3 back_royal_development2;'
alias copy3to1='dropdb back_royal_development; createdb -T back_royal_development3 back_royal_development;'
alias copy1to3='dropdb back_royal_development3; createdb -T back_royal_development back_royal_development3;'

alias tcopy2to1='dropdb back_royal_test; createdb -T back_royal_test2 back_royal_test;'
alias tcopy1to2='dropdb back_royal_test2; createdb -T back_royal_test back_royal_test2;'
alias tcopy3to2='dropdb back_royal_test2; createdb -T back_royal_test3 back_royal_test2;'
alias tcopy3to1='dropdb back_royal_test; createdb -T back_royal_test3 back_royal_test;'
alias tcopy1to3='dropdb back_royal_test3; createdb -T back_royal_test back_royal_test3;'


##########################
# DB State               #
##########################

function rdb {
    echo "Restoring to back_royal_development2"
    rake db:rebuild_db_from_source target="localhost" restore_only="true"
    echo "Restored into back_royal_development2"
    echo "Copy back_royal_development2 -> back_royal_development? Close connections and ENTER to continue / ^C to quit ..."
    read
    copy2to1
    echo "Copy back_royal_development1 -> back_royal_development3? Close connections and ENTER to continue / ^C to quit ..."
    copy1to3
}

function rtd {
    rake test:redshift:setup

    rake test:setup
    rake test:puppet:setup

    ENABLE_CLOUDWATCH_LOGGING=false rake test:build_fixtures
    ENABLE_CLOUDWATCH_LOGGING=false rake test:puppet:build_fixtures
}

function migrate_all {
    rake db:migrate
    RAILS_ENV=test rake db:migrate
    USE_PUPPET_DB=true RAILS_ENV=test rake db:migrate
}

function rollback_all {
    rake db:rollback
    RAILS_ENV=test rake db:rollback
    USE_PUPPET_DB=true RAILS_ENV=test rake db:rollback
}

##########################
# DB Tunneling           #
##########################

export SMARTLY_STAGING_RDS_HOST=smartly-staging.cgueubjoqruk.us-east-1.rds.amazonaws.com
export SMARTLY_STAGING_REDSHIFT_HOST=smartly-staging-events.c3rcoqja5qnq.us-east-1.redshift.amazonaws.com
export SMARTLY_PRODUCTION_WRITE_RDS_HOST=smartly-production.c1w22gxpuyvn.us-east-1.rds.amazonaws.com
export SMARTLY_PRODUCTION_READ_RDS_HOST=smartly-production-readonly.c1w22gxpuyvn.us-east-1.rds.amazonaws.com
export SMARTLY_PRODUCTION_REDSHIFT_HOST=smartly-production-events.chqfvmhbfufa.us-east-1.redshift.amazonaws.com
export SMARTLY_STAGING_REDIS_HOST=smartly-staging-cache.ifylnu.0001.use1.cache.amazonaws.com
export SMARTLY_PRODUCTION_REDIS_HOST=smartly-production-cache.3voo6y.0001.use1.cache.amazonaws.com
export SMARTLY_RDS_PORT=5432
export SMARTLY_REDSHIFT_PORT=5439
export SMARTLY_REDIS_PORT=6379

function db-tunnel {
    local existing_tunnel=`ps ax | grep -v grep | grep $CURRENT_TUNNEL`
    if [ -n "$existing_tunnel" ]; then
        echo "Tunnel already exists. NOTE: This command may fail if the existing tunnel is closed unexpectedly!"
        "$@"
    else
        ssh -N -L $CURRENT_TUNNEL smartly-bastion &
        local pid=$!
        echo "Establishing backgrounded tunnel ..."
        sleep 4
        "$@"
        echo "Closing tunnel $pid"
        kill $pid
    fi
}

# Useful for DataGrip or other application access, while still cleaning up after itself.
alias tunnel-rds-production-write="CURRENT_TUNNEL=${SMARTLY_RDS_PORT}1:${SMARTLY_PRODUCTION_WRITE_RDS_HOST}:${SMARTLY_RDS_PORT} db-tunnel read -p 'Press ENTER to close tunnel!'"
alias tunnel-rds-production-read="CURRENT_TUNNEL=${SMARTLY_RDS_PORT}3:${SMARTLY_PRODUCTION_READ_RDS_HOST}:${SMARTLY_RDS_PORT} db-tunnel read -p 'Press ENTER to close tunnel!'"
alias tunnel-rds-staging="CURRENT_TUNNEL=${SMARTLY_RDS_PORT}2:${SMARTLY_STAGING_RDS_HOST}:${SMARTLY_RDS_PORT} db-tunnel read -p 'Press ENTER to close tunnel!'"
alias tunnel-redshift-production="CURRENT_TUNNEL=${SMARTLY_REDSHIFT_PORT}1:${SMARTLY_PRODUCTION_REDSHIFT_HOST}:${SMARTLY_REDSHIFT_PORT} db-tunnel read -p 'Press ENTER to close tunnel!'"
alias tunnel-redshift-staging="CURRENT_TUNNEL=${SMARTLY_REDSHIFT_PORT}2:${SMARTLY_STAGING_REDSHIFT_HOST}:${SMARTLY_REDSHIFT_PORT} db-tunnel read -p 'Press ENTER to close tunnel!'"
alias tunnel-redis-production="CURRENT_TUNNEL=${SMARTLY_REDIS_PORT}1:${SMARTLY_PRODUCTION_REDIS_HOST}:${SMARTLY_REDIS_PORT} db-tunnel read -p 'Press ENTER to close tunnel!'"
alias tunnel-redis-staging="CURRENT_TUNNEL=${SMARTLY_REDIS_PORT}2:${SMARTLY_STAGING_REDIS_HOST}:${SMARTLY_REDIS_PORT} db-tunnel read -p 'Press ENTER to close tunnel!'"
alias tunnel-list="sudo lsof -i -n | egrep '\<ssh\>'"


##########################
# psql Aliases           #
##########################

psqld() {
    psql back_royal_development
}

psqlst() {
    CURRENT_TUNNEL=${SMARTLY_RDS_PORT}2:${SMARTLY_STAGING_RDS_HOST}:${SMARTLY_RDS_PORT} db-tunnel psql -h localhost -p ${SMARTLY_RDS_PORT}2 -U ebroot back_royal
}

psqlredst() {
    CURRENT_TUNNEL=${SMARTLY_REDSHIFT_PORT}2:${SMARTLY_STAGING_REDSHIFT_HOST}:${SMARTLY_REDSHIFT_PORT} db-tunnel psql -h localhost -p ${SMARTLY_REDSHIFT_PORT}2 -U cluster_admin red_royal
}

psqlreddev() {
    CURRENT_TUNNEL=${SMARTLY_REDSHIFT_PORT}2:${SMARTLY_STAGING_REDSHIFT_HOST}:${SMARTLY_REDSHIFT_PORT} db-tunnel psql -h localhost -p ${SMARTLY_REDSHIFT_PORT}2 -U cluster_admin red_royal_$USER
}

redis-clist() {
    CURRENT_TUNNEL=${SMARTLY_REDIS_PORT}2:${SMARTLY_STAGING_REDIS_HOST}:${SMARTLY_REDIS_PORT} db-tunnel redis-cli -h localhost -p ${SMARTLY_REDIS_PORT}2
}

redis-cliprod() {
    CURRENT_TUNNEL=${SMARTLY_REDIS_PORT}1:${SMARTLY_PRODUCTION_REDIS_HOST}:${SMARTLY_REDIS_PORT} db-tunnel redis-cli -h localhost -p ${SMARTLY_REDIS_PORT}1
}

setupreddev() {
    CURRENT_TUNNEL=${SMARTLY_REDSHIFT_PORT}2:${SMARTLY_STAGING_REDSHIFT_HOST}:${SMARTLY_REDSHIFT_PORT} db-tunnel psql -c"create database red_royal_$USER" -h localhost -p ${SMARTLY_REDSHIFT_PORT}2 -U cluster_admin red_royal ||  true
    CURRENT_TUNNEL=${SMARTLY_REDSHIFT_PORT}2:${SMARTLY_STAGING_REDSHIFT_HOST}:${SMARTLY_REDSHIFT_PORT} db-tunnel psql -c"CREATE TABLE if not exists schema_migrations (version character varying(255) not null);" -h localhost -p ${SMARTLY_REDSHIFT_PORT}2 -U cluster_admin red_royal_$USER
    CURRENT_TUNNEL=${SMARTLY_REDSHIFT_PORT}2:${SMARTLY_STAGING_REDSHIFT_HOST}:${SMARTLY_REDSHIFT_PORT} db-tunnel psql -c"CREATE TABLE if not exists public.ar_internal_metadata (key character varying NOT NULL, value character varying, created_at timestamp without time zone NOT NULL, updated_at timestamp without time zone NOT NULL);" -h localhost -p ${SMARTLY_REDSHIFT_PORT}2 -U cluster_admin red_royal_$USER
    rake redshift:db:migrate;
}

##########################
# Rollouts               #
##########################

roll() {
    rake rollout:document_rollouts
}

roll-slack() {
    rake rollout:document_rollouts announce=true
}



##########################
# Screenshot Container   #
##########################

puppet() {

    case "$1" in
        update)
            br; ./spec/puppeteer/puppet_master.sh --u $2
            ;;
        watch)
            br; ./spec/puppeteer/puppet_master.sh --watch $2
            ;;
        kill)
            docker kill screenshot-container
            ;;
        pattern)
            if [ -z "$2" ]; then
                echo "You must provide a pattern parameter when running in pattern mode!"
            else
                br; ./spec/puppeteer/puppet_master.sh $2
            fi
            ;;
        debug)
            br; ./spec/puppeteer/puppet_master.sh --debug $2
            ;;
        debug-watch)
            br; ./spec/puppeteer/puppet_master.sh --debug-watch $2
            ;;
        "") br; ./spec/puppeteer/puppet_master.sh
            ;;
        *)
            echo "Unknown option '$1'. USAGE: puppet [update|watch|debug|debug-watch|kill|pattern <PATTERN>]"
            ;;
    esac
}


##########################
# Rogue Processes        #
##########################

rubykill() {
    lsof -i :3000 | grep ruby | awk '{print $2 }' | xargs kill -9
}

alias killpuma='pkill -f puma'



##########################
# Git Aliases            #
##########################

alias giterdun='git checkout master; git pull -p; gitprune;'
alias gitprune='git branch --merged | grep -v "\*" | xargs -n 1 git branch -d'
alias gitprune-hard='git fetch -p && for branch in `git branch -vv | grep ": gone]" | awk '"'"'{print $1}'"'"'`; do git branch -D $branch; done'
alias gitbranchinfo=$'git for-each-ref --sort=committerdate refs/heads/ --format=\'%(HEAD) %(color:yellow)%(refname:short)%(color:reset) - %(color:red)%(objectname:short)%(color:reset) - %(contents:subject) - %(authorname) (%(color:green)%(committerdate:relative)%(color:reset))\''


##########################
# Spring Aliases         #
##########################

alias rspec="bin/rspec"
alias rake="bin/rake"
alias rails="bin/rails"

##########################
# Fancy Prompt           #
##########################

function parse_git_branch {
    ref=$(git symbolic-ref HEAD 2> /dev/null) || return
    echo " (${ref#refs/heads/}) "
}

function buildPrompt {
    local LIGHT_GREY="\[\033[0;37m\]"
    local LIGHT_CYAN="\[\033[1;36m\]"
    local WHITE="\[\033[1;37m\]"
    local YELLOW="\[\033[1;33m\]"
    local YELLOW="\[\033[1;33m\]"
    local NO_COLOR="\[\033[0;0m\]"

    case $TERM in
        xterm*|rxvt*)
            TITLEBAR='\[\033]0;\u@\h:\w\007\]'
            ;;
        *)
            TITLEBAR=""
            ;;
    esac

    PS1="${TITLEBAR}\
$YELLOW[$WHITE\u$LIGHT_GREY@$WHITE\h$LIGHT_GREY:$WHITE\w$YELLOW]\
$LIGHT_CYAN\$(parse_git_branch)\
$WHITE\$$NO_COLOR "

    PS2='> '
    PS4='+ '
}

# change prompt
buildPrompt

# setup colors for dark terminals
export CLICOLOR=1
export LSCOLORS=GxFxCxDxBxegedabagaced



##########################
# RVM Setup              #
##########################

[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*




##########################
# Git Rewriting          #
##########################

# See also, POST BFG steps:
# git clone git@bitbucket.org:pedago-ondemand/back_royal.git
# cp -R back_royal.OLD/config/* back_royal/config
# cp -R back_royal.OLD/.git/hooks/* back_royal/.git/hooks
# cp back_royal.OLD/hybrid/smartly-android-upload.keystore back_royal/hybrid/smartly-android-upload.keystore
# rm -rf back_royal.OLD
# cd back_royal
# npm ci
function bfg() { java -jar ~/bfg-1.13.0.jar "$@" ;}
function bfg_br() {
    echo "See also: https://rtyley.github.io/bfg-repo-cleaner/"
    echo "You should also consider aborting and creating a placeholder commit for any BFG files being added to HEAD commit"
    echo "Press ENTER to continue ..."
    read
    git clone --mirror git@bitbucket.org:pedago-ondemand/back_royal.git
    cp -R back_royal.git back_royal.git.BAK
    bfg --delete-folders "{cache,node_modules,bower_components,__image_snapshots__,platforms,store_assets}" back_royal.git
    bfg --convert-to-git-lfs "*.{png,jpg,gif,mov,sketch,jpeg,pdf,mp3,gem,zip,gz,tar}" --no-blob-protection back_royal.git
    cd back_royal.git
    git reflog expire --expire=now --all && git gc --prune=now --aggressive
    git lfs install
    echo "Now you should git push after manually verifying. Be sure to validate LFS init / settings in .gitattributes!"
}




##########################
# Android SDK Setup      #
##########################

# This assumes you've installed Android Studio
export ANDROID_HOME=$HOME/Library/Android/sdk
export ANDROID_SDK_ROOT=$ANDROID_HOME
export JAVA_HOME=/Applications/Android\ Studio.app/Contents/jre/jdk/Contents/Home

PATH=$PATH:$ANDROID_HOME/emulator
PATH=$PATH:$ANDROID_HOME/platform-tools
PATH=$PATH:$ANDROID_HOME/tools
PATH=${JAVA_HOME}/bin:$PATH
export PATH


##########################
# Additional Sources     #
##########################

SOURCE_INFO="$( cd "$( dirname "${BASH_SOURCE[0]}"  )" && pwd )"
SCRIPT_DIR=$(echo "$SOURCE_INFO" | head -n 1)

# git-completion
source $SCRIPT_DIR/git-completion.bash

# eb-ssh
source $SCRIPT_DIR/eb-ssh.bash