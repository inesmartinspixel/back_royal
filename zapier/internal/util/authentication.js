const constants = require('./constants');

const getSessionKey = (z, bundle) => {
    const promise = z.request({
        method: 'POST',
        url: `${constants.getHost(z)}/api/auth/sign_in`,
        body: {
            email: bundle.authData.username,
            password: bundle.authData.password,
            provider: 'email'
        }
    });

    return promise.then(response => {
        if (response.status === 401) {
            throw new Error('The username/password you supplied is invalid');
        }
        return {
            accessToken: response.getHeader('access-token'),
            tokenType: response.getHeader('token-type'),
            clientId: response.getHeader('client'),
            expiry: response.getHeader('expiry'),
            uid: response.getHeader('uid')
        };
    });
};

module.exports = {
    type: 'session',
    fields: [{
        key: 'username',
        label: 'Email Address',
        required: true,
        type: 'string',
        helpText: 'Your Smartly login email address.'

    }, {
        key: 'password',
        label: 'Password',
        required: true,
        type: 'password',
        helpText: 'Your Smartly login password.'
    }],
    test: z => {
        const promise = z.request({
            url: `${constants.getHost(z)}/api/hiring_applications.json`,
            params: {
                limit: 1
            }
        });
        return promise.then(response => {
            // not sure we can actually get here, due to the afterResponse handler catching this for us?
            if (response.status === 401) {
                throw new z.errors.RefreshAuthError('Session key needs refreshing.');
            } else {
                return response;
            }
        });
    },
    sessionConfig: {
        perform: getSessionKey
    }
};