const includeSessionHeaders = (request, z, bundle) => {
    if (bundle.authData) {
        request.headers = request.headers || {};
        request.headers['access-token'] = bundle.authData.accessToken;
        request.headers['token-type'] = bundle.authData.tokenType;
        request.headers.client = bundle.authData.clientId;
        request.headers.expiry = bundle.authData.expiry;
        request.headers.uid = bundle.authData.uid;
    }
    return request;
};

// If we get a response and it is a 401, we can raise a special error telling Zapier to retry this after another exchange.
const sessionRefreshIf401 = (response, z) => {
    if (response.status === 401) {
        throw new z.errors.RefreshAuthError('Session key needs refreshing.');
    }
    return response;
};

module.exports = {
    includeSessionHeaders: includeSessionHeaders,
    sessionRefreshIf401: sessionRefreshIf401
};