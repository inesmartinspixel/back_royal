const should = require('should');
const zapier = require('zapier-platform-core');
const App = require('../index');
const appTester = zapier.createAppTester(App);

describe('smartly app', () => {

    // hack: save the authData from the first spec that tests login to test the following specs
    let authData = {};

    process.env.SMARTLY_HOST = 'http://localhost:8800';

    beforeEach(done => {
        const bundle = {
            authData: {
                username: 'admin@pedago.com',
                password: 'password'
            }
        };

        appTester(App.authentication.sessionConfig.perform, bundle)
            .then(newAuthData => {
                should.exist(newAuthData.accessToken);
                should.exist(newAuthData.tokenType);
                should.exist(newAuthData.clientId);
                should.exist(newAuthData.expiry);
                should.exist(newAuthData.uid);

                // save the valid authData for use in later specs
                authData = newAuthData;
                done();
            })
            .catch(done);
    });

    it('has a successful exchange for username/password', () => {
        // this test is a beforeEach now so all the other specs work
    });

    it('fails authentication test if user is not admin', done => {
        const bundle = {
            authData: {
                username: 'mba_user@pedago.com',
                password: 'password'
            }
        };

        appTester(App.authentication.sessionConfig.perform, bundle)
            .then(newAuthData => {
                // save the valid authData for use in later specs
                bundle.authData = newAuthData;
            }, () => {
                throw new Error('Should have succeeded logging in with non-admin user...');
            })
            .then(() => appTester(App.authentication.test, bundle))
            .then(() => {
                // shouldn't get here!
                throw new Error('We should have caught an exception...');
            })
            .catch(error => {
                done(error.name === 'RefreshAuthError' ? null : error);
            });
    });

    it('succeeds authentication test if authData is provided', done => {
        const bundle = {
            authData
        };

        appTester(App.authentication.test, bundle)
            .then(() => {
                // it worked!
                done();
            })
            .catch(done);
    });

    it('fails authentication test if authData is not provided', done => {
        const bundle = {};

        appTester(App.authentication.test, bundle)
            .then(() => {
                // shouldn't get here!
                throw new Error('We should have caught an exception.');
            })
            .catch(error => {
                error.name.should.eql('RefreshAuthError');
                done();
            });
    });

    it('polls for new hiring manager applications correctly', done => {
        const bundle = {
            authData
        };

        appTester(App.resources.hiring_manager_application.list.operation.perform, bundle)
            .then(results => {
                // check that it returned some valid looking results
                should(results.length).above(1);
                const firstResult = results[0];
                should.exist(firstResult);
                should.exist(firstResult.company_name);
                should.exist(firstResult.job_title);
                should.exist(firstResult.email);
                done();
            })
            .catch(done);
    });

    it('polls for new cohort applications correctly', done => {
        const bundle = {
            authData
        };

        appTester(App.resources.new_cohort_application.list.operation.perform, bundle)
            .then(results => {
                // check that it returned some valid looking results
                should(results.length).aboveOrEqual(1);
                const firstResult = results[0];
                should.exist(firstResult);
                should.exist(firstResult.cohort_name);
                should.exist(firstResult.applied_at);
                should.exist(firstResult.name);
                should.exist(firstResult.id);
                done();
            })
            .catch(done);
    });

    it('polls for new or updated cohort applications correctly', done => {
        const bundle = {
            authData
        };

        appTester(App.resources.new_or_updated_cohort_application.list.operation.perform, bundle)
            .then(results => {
                // check that it returned some valid looking results
                should(results.length).aboveOrEqual(1);
                const firstResult = results[0];
                should.exist(firstResult);
                should.exist(firstResult.cohort_name);
                should.exist(firstResult.applied_at);
                should.exist(firstResult.name);
                should.equal(firstResult.id, firstResult.application_id + firstResult.updated_at);
                done();
            })
            .catch(done);
    });
});