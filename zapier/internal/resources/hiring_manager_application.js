const constants = require('../util/constants');

const listHiringManagerApplications = z => {
    const responsePromise = z.request({
        url: `${constants.getHost(z)}/api/hiring_applications.json`,
        params: {
            zapier: true,
            sort: 'created_at',
            direction: 'desc',
            limit: 10 // no need to get em all
        }
    });
    return responsePromise
        .then(response => {
            let body = JSON.parse(response.content);
            return body && body.contents && body.contents.hiring_applications;
        });
};

module.exports = {
    key: 'hiring_manager_application',
    noun: 'HiringManagerApplication',
    list: {
        display: {
            label: 'New Hiring Manager Application',
            description: 'Triggers on a new new Hiring Manager Application.'
        },
        operation: {
            perform: listHiringManagerApplications
        }
    }
};