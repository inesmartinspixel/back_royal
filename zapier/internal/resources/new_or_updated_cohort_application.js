const constants = require('../util/constants');

const listCohortApplications = z => {
    const responsePromise = z.request({
        url: `${constants.getHost(z)}/api/cohort_applications.json`,
        params: {
            zapier: true,
            include_updates: true
        }
    });
    return responsePromise
        .then(response => {
            let body = JSON.parse(response.content);
            var applications = (body && body.contents && body.contents.cohort_applications) || [];
            applications.forEach(application => {
                var id = application.id;
                application.application_id = id;

                // make sure zapier triggers on every update
                application.id = id + application.updated_at;
            });

            return applications;
        });
};

module.exports = {
    key: 'new_or_updated_cohort_application',
    noun: 'CohortApplication',
    list: {
        display: {
            label: 'New or Updated Cohort Application',
            description: 'Triggers on a new or updated Cohort Application.'
        },
        operation: {
            perform: listCohortApplications
        }
    }
};