const constants = require('../util/constants');

const listCohortApplications = z => {
    const responsePromise = z.request({
        url: `${constants.getHost(z)}/api/cohort_applications.json`,
        params: {
            zapier: true,
            include_updates: false,
            sort: 'created_at',
            direction: 'desc',
            limit: 100
        }
    });
    return responsePromise
        .then(response => {
            let body = JSON.parse(response.content);
            return body && body.contents && body.contents.cohort_applications;
        });
};

module.exports = {
    key: 'new_cohort_application',
    noun: 'CohortApplication',
    list: {
        display: {
            label: 'New Cohort Application',
            description: 'Triggers on a new Cohort Application.'
        },
        operation: {
            perform: listCohortApplications
        }
    }
};