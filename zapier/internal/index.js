const version = require('./package.json').version;
const zapier = require('zapier-platform-core');
const authentication = require('./util/authentication');
const session = require('./util/session.js');
const HiringManagerApplicationResource = require('./resources/hiring_manager_application');
const NewCohortApplicationResource = require('./resources/new_cohort_application');
const NewOrUpdatedCohortApplicationResource = require('./resources/new_or_updated_cohort_application');

const App = {
    version: version,
    platformVersion: zapier.version,
    authentication: authentication,
    beforeRequest: [
        session.includeSessionHeaders
    ],
    afterResponse: [
        session.sessionRefreshIf401
    ],
    resources: {
        [HiringManagerApplicationResource.key]: HiringManagerApplicationResource,
        [NewCohortApplicationResource.key]: NewCohortApplicationResource,
        [NewOrUpdatedCohortApplicationResource.key]: NewOrUpdatedCohortApplicationResource,
    },
};

module.exports = App;