
Setup

1. Install the Zapier platform CLI: `npm install -g zapier-platform-cli`
1. Log in using the shared Zapier account (marketing@pedago.com): `zapier login`
1. Install dependencies:
    1. `br; cd zapier/internal; npm ci`
    1. `br; cd zapier/institutional; npm ci`


Testing the zapier sub-projects

1. Run the casper server (the specs use this server): `br; npm run casperserver`
1. `br`
1. `npm run testserver`
1. `br; cd zapier/internal`
1. `zapier test`
1. `br; cd zapier/institutional`
1. `zapier test`
1. (See zapier/README.md for info on running a development version of one of the apps)


Updating a zapier app

1. Make changes
1. `br`
1. `npm run testserver`
1. `br; cd zapier/internal`
1. `zapier test`
1. Update the version number in `package.json`
1. `zapier push`

Setting up a development zap

1. `br`
1. `cd zapier/internal`
1. Look in `package.json` to determine the current version (See above for how to make a new version)
1. `zapier env {{VERSION}} SMARTLY_HOST https://you.ngrok.io`
1. now any zap set up for "Test Smartly Internal Triggers {{VERSION}}" will hit your local

Updating the version in production

1. `br`
1. `zapier env {{VERSION}} --remove SMARTLY_HOST`
1. change the first step of existing zaps to use the new version