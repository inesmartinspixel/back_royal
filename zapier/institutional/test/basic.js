const should = require('should');
const zapier = require('zapier-platform-core');
const App = require('../index');
const appTester = zapier.createAppTester(App);

describe('smartly app', () => {

    // hack: save the authData from the first spec that tests login to test the following specs
    let authData = {};

    process.env.SMARTLY_HOST = 'http://localhost:8800';

    it('has a successful auth for an institutional admin', done => {
        const bundle = {
            authData: {
                username: 'institutional_admin@pedago.com',
                password: 'password'
            }
        };

        appTester(App.authentication.sessionConfig.perform, bundle)
            .then(newAuthData => {
                should.exist(newAuthData.accessToken);
                should.exist(newAuthData.tokenType);
                should.exist(newAuthData.clientId);
                should.exist(newAuthData.expiry);
                should.exist(newAuthData.uid);
                should.exist(newAuthData.institution_id);

                // save the valid authData for use in later specs
                authData = newAuthData;
                done();
            })
            .catch(done);
    });

    it('fails authentication if user is not an institutional admin', done => {
        const bundle = {
            authData: {
                username: 'mba_user@pedago.com',
                password: 'password'
            }
        };

        appTester(App.authentication.sessionConfig.perform, bundle)
            .then(() => {
                throw new Error('Shouldn\'t have succeeded logging in with non-admin user...');
            })
            .catch(error => {
                done(error.name === 'Error' ? null : error);
            });
    });

    it('succeeds authentication test if authData is provided', done => {
        const bundle = {
            authData: authData
        };

        appTester(App.authentication.test, bundle)
            .then(() => {
                // it worked!
                done();
            })
            .catch(done);
    });

    it('fails authentication test if authData is not provided', done => {
        const bundle = {};

        appTester(App.authentication.test, bundle)
            .then(() => {
                // shouldn't get here!
                throw new Error('We should have caught an exception.');
            })
            .catch(error => {
                error.name.should.eql('RefreshAuthError');
                done();
            });
    });

    it('polls for new course completions correctly', done => {
        const bundle = {
            authData: authData
        };

        appTester(App.resources.course_completion.list.operation.perform, bundle)
            .then(results => {
                // check that it returned some valid looking results
                should(results.length).above(1);
                const firstResult = results[0];
                should.exist(firstResult);
                should.exist(firstResult.user_name);
                should.exist(firstResult.email);
                should.exist(firstResult.phone);
                should.exist(firstResult.english_course_title);
                should.exist(firstResult.started_at);
                should.exist(firstResult.completed_at);
                done();
            })
            .catch(done);
    });
});