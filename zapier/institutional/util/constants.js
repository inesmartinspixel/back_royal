const zapier = require('zapier-platform-core');

zapier.tools.env.inject();

module.exports = {
    getHost: () => {
        const host = process.env.SMARTLY_HOST || 'https://quantic.edu';
        // Zapier has had some bugs in ENV var handling; handy code to log the status of ENV vars
        // let smartly_keys = Object.keys(process.env).filter((key) => key.indexOf('SMARTLY_') === 0).map((key) => {
        //     return {
        //         [key]: process.env[key]
        //     };
        // });
        // z.console.log('GOT HOST / env:', host, '/', smartly_keys);
        return host;
    }
};
