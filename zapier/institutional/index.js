const version = require('./package.json').version;
const zapier = require('zapier-platform-core');
const authentication = require('./util/authentication');
const session = require('./util/session.js');
const CourseCompletionResource = require('./resources/course_completion');

const App = {
    version: version,
    platformVersion: zapier.version,
    authentication: authentication,
    beforeRequest: [
        session.includeSessionHeaders
    ],
    afterResponse: [
        session.sessionRefreshIf401
    ],
    resources: {
        [CourseCompletionResource.key]: CourseCompletionResource
    },
};

module.exports = App;