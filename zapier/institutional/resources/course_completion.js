const constants = require('../util/constants');

const listCourseCompletions = (z, bundle) => {
    const responsePromise = z.request({
        url: `${constants.getHost(z)}/api/lesson_streams_progress.json`,
        params: {
            institution_id: bundle.authData.institution_id,
            zapier: true,
            sort: 'completed_at',
            direction: 'desc',
            limit: 100 // no need to get em all
        }
    });
    return responsePromise
        .then(response => {
            let body = JSON.parse(response.content);
            return body && body.contents && body.contents.lesson_streams_progress;
        });
};

module.exports = {
    key: 'course_completion',
    noun: 'Course Completion',
    list: {
        display: {
            label: 'Learner Course Completion',
            description: 'Triggers on a learner completing a Course.'
        },
        operation: {
            perform: listCourseCompletions
        }
    }
};