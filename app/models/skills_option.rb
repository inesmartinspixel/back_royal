# == Schema Information
#
# Table name: skills_options
#
#  id         :uuid             not null, primary key
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  text       :text             not null
#  locale     :text             default("en"), not null
#  suggest    :boolean          default(FALSE)
#

class SkillsOption < ActiveRecord::Base

    include AutoSuggestOptionMixin

    self.table_name = "skills_options"
    has_and_belongs_to_many :career_profiles

    def type
        'skill'
    end

end
