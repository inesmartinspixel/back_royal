# == Schema Information
#
# Table name: cohorts
#
#  id                                      :uuid             not null, primary key
#  created_at                              :datetime         not null
#  updated_at                              :datetime         not null
#  start_date                              :datetime         not null
#  end_date                                :datetime         not null
#  was_published                           :boolean          default(FALSE)
#  name                                    :text             not null
#  specialization_playlist_pack_ids        :uuid             default([]), is an Array
#  periods                                 :json             not null, is an Array
#  program_type                            :text             default("mba"), not null
#  num_required_specializations            :integer          default(0), not null
#  title                                   :text             not null
#  description                             :text
#  admission_rounds                        :json             is an Array
#  registration_deadline_days_offset       :integer          default(0)
#  external_schedule_url                   :text
#  program_guide_url                       :text
#  enrollment_deadline_days_offset         :integer          default(0), not null
#  stripe_plans                            :json             is an Array
#  scholarship_levels                      :json             is an Array
#  internal_notes                          :text
#  project_submission_email                :string
#  early_registration_deadline_days_offset :integer          default(0)
#  id_verification_periods                 :json             is an Array
#  graduation_days_offset_from_end         :integer
#  diploma_generation_days_offset_from_end :integer
#  learner_project_ids                     :uuid             default([]), not null, is an Array
#  enrollment_agreement_template_id        :text
#  playlist_collections                    :json             not null, is an Array
#  isolated_network                        :boolean          default(FALSE), not null
#

require 'back_royal/time'

class Cohort < ApplicationRecord

    # FIXME: See https://trello.com/c/Jz1ubPBe.
    self.ignored_columns = %w(required_playlist_pack_ids)

    include Groupable
    include ContentPublisher::Publishable
    include Cohort::Schedulable
    include Cohort::CurriculumWithProjects
    include CrudFromHashHelper

    ACCEPTANCE_DATE_OFFSET = -5.days

    default_scope -> {order(:created_at)}

    # when associations are inherited by Cohort::Version, they
    # will use a different primary_key unless we set it explicity
    use_id_and_cohort_id = {:primary_key => 'id', :foreign_key => 'cohort_id'}

    has_many :cohort_applications, use_id_and_cohort_id.merge(:class_name => 'CohortApplication', :dependent => :destroy)
    has_one :cohort_promotion, use_id_and_cohort_id
    has_many :users, :through => :cohort_applications
    has_many :accepted_users, -> {where(cohort_applications: {status: 'accepted'}) }, :through => :cohort_applications, :source => :user
    has_many :pre_accepted_users, -> {where(cohort_applications: {status: 'pre_accepted'}) }, :through => :cohort_applications, :source => :user
    has_many :expelled_users, -> {where(cohort_applications: {status: 'expelled'}) }, :through => :cohort_applications, :source => :user

    has_many :published_cohort_periods, use_id_and_cohort_id
    has_many :slack_rooms, use_id_and_cohort_id.merge(:class_name => 'CohortSlackRoom', :dependent => :destroy)
    has_one :slack_room_assignment, use_id_and_cohort_id

    validates_presence_of :name
    validates_presence_of :title
    validates_presence_of :start_date
    validate :validate_supports_schedule
    validate :validate_supports_payments

    # This is still the case in the schedule editor UI for cohort-level projects, but can
    # easily be removed now that we have transitioned to supporting multiple projects
    validates_size_of :learner_project_ids, :maximum => 1, :message => "can have at most one entry"

    validates_inclusion_of :isolated_network, :in => [false],
        :unless => Proc.new { self.program_type_config.supports_isolated_network? },
        :message => "must be false for cohorts that do not support isolated network flag"


    set_callback :publish_change, :around, :verify_setup_slack_channels_jobs

    set_callback :publish_change, :before, :raise_if_unpublished_playlist_is_required

    # this can't happen until after save, so you'd better be saving in a transaction
    set_callback :publish_change, :after, :after_publish_change

    # by moving CohortRelationExtensions into a module in lib, we prevent
    # it from being auto-reloaded and avoid a reload error
    # in devmode.  Probably means that changes to this will require
    # an app restart.
    #
    # The error we're trying to avoid is
    # A copy of Cohort::ActiveRecord_Relation has been removed from the module tree but is still active!
    class ActiveRecord_Relation
        include Cohort::CohortRelationExtensions
    end

    def self.cached_published(cohort_id)
        SafeCache.fetch([cohort_id, ContentViewsRefresh.debounced_value].join('/')) do
            published_cohort = Cohort.find(cohort_id).published_version
            # This delegates to cohort.rb via version.rb; invoking this ensures we
            # cache the associated cohort with this published version
            published_cohort.published_at if !published_cohort.nil?
            published_cohort
        end
    end

    def self.promoted_cohort(program_type)
        return nil unless program_type_config = ProgramTypeConfig[program_type]
        if program_type_config.supports_manual_promotion?
            return Cohort.joins(:cohort_promotion).where(program_type: program_type).first&.published_version
        else
            promoted_admission_round = AdmissionRound.promoted(program_type)
            return promoted_admission_round && promoted_admission_round.cohort.published_version
        end
    end

    def self.create_from_hash!(user, hash, meta = {})
        RetriableTransaction.transaction do
            instance = self.new
            instance.save_from_hash(user, hash, meta)
        end
    end

    def self.update_from_hash!(user, hash, meta = {})
        RetriableTransaction.transaction do
            hash, meta, instance = prepare_update_from_hash(hash, meta)
            instance.save_from_hash(user, hash, meta)
        end
    end

    def self.emba_offset
        # first pair was MBA5 / EMBA1, so use an offset of four to line them up
        return 4
    end

    def self.get_course_entries_event_array(period, user)
        return period['stream_entries'].map { |stream_entry|
            Lesson::Stream.event_attributes(stream_entry['locale_pack_id'], user.locale).merge({
                required: stream_entry['required'],
                caption: stream_entry['caption'],
            })
        }
    end

    def foundations_updated_at
        playlist_updated_at = Playlist.where(locale_pack_id: self.required_playlist_pack_ids + self.specialization_playlist_pack_ids).pluck('updated_at').max
        playlist_updated_at ? [self.updated_at, playlist_updated_at].max.to_timestamp : self.updated_at.to_timestamp
    end

    def save_from_hash(user, hash, meta = {})

        orig_group_ids = self.access_groups.pluck(:id)
        self.merge_hash(hash)

        # Force this thing to save even if there are no changes. The whole
        # publishing system is a little confusing if you publish something without
        # saving and triggering a new version to get created. (We used to
        # call updated_at_will_change! here, but that either never worked or
        # it stopped working at some point.)
        self.updated_at = Time.now

        self.save_from_hash_with_publish_support(user, meta)

        # NOTE: we call after_access_group_update here, but we don't guarantee
        # this gets run on saves made outside of update_from_hash!  We've been
        # getting away with this for a long long time though, so hopefully
        # we will continue to. (See similar issue in is_content_item.rb)
        self.after_access_group_update(orig_group_ids)

        self
    end

    def event_attributes(timezone)
        {
            cohort_id: self.attributes['id'],
            cohort_title: self.title,
            program_type: self.program_type,
            is_paid_cert: self.program_type_config.is_paid_cert?,
            stripe_plan_ids: self.stripe_plan_ids,
            program_guide_url: self.program_guide_url,
            cohort_decision_date: self.decision_date.to_timestamp,
            cohort_start_date: self.start_date.to_timestamp,
            cohort_application_deadline: self.application_deadline&.relative_to_threshold(timezone: timezone).to_timestamp,
            cohort_registration_deadline: self.registration_deadline&.relative_to_threshold(timezone: timezone).to_timestamp,
            cohort_early_registration_deadline: self.early_registration_deadline&.relative_to_threshold(timezone: timezone).to_timestamp
        }
    end

    def event_attributes_for_period(period)
        hash = {
            cohort_id: self.attributes['id'],
            program_type: self.program_type,
            period_index: index_for_period(period),
            period_style: period['style'],
            period_title: period['title'].blank? ? "Week #{index_for_period(period)}" : period['title'],
            period_description: period['description'],
            period_start: self.start_time_for_period(period).to_timestamp,
            period_end: self.end_time_for_period(period).to_timestamp
        }

        if ['exam', 'review'].include?(period['style'])
            hash = hash.merge({
                period_exam_style: period['exam_style']
            })
        elsif period['style'] === 'project'
            hash = hash.merge({
                period_project_style: period['project_style'],
                period_learner_projects: LearnerProject.find_cached_by_id(period['learner_project_ids']).as_json,
            })
        end

        return hash
    end

    # Returns the dynamically generated, formatted project submission email or nil if no project_submission_email exists.
    # NOTE: This logic should match Cohort#getFormattedProjectSubmissionEmail in cohort.js. Currently, only the logic
    # for dynamically generating the formatted project submission email for program types that support cohort level projects
    # is implemented. The logic for generating the project submission email for program types that support period level
    # projects should be implemented when that need arises.
    def formatted_project_submission_email
        if self.project_submission_email
            email_components = self.project_submission_email.split('@')
            compact_cohort_name = self.name.gsub(/\s/, '')

            if self.program_type_config.supports_cohort_level_projects?
                return email_components[0] + '+' + compact_cohort_name + '@' + email_components[1]
            end
        end

        return nil
    end

    def index_for_period(period)
        periods.index(period) + 1
    end

    # Gets the next exam period relative to the period index that's passed in as an argument.
    # NOTE: the index argument should be 1-based.
    def next_exam_period(index)
        exam_period_index = self.periods[index..self.periods.size].find_index { |period| period["style"] == "exam" }
        exam_period_index ? self.periods[index + exam_period_index] : nil
    end

    def merge_hash(hash)
        %w(name title description specialization_playlist_pack_ids periods program_type
            num_required_specializations admission_rounds id_verification_periods
            registration_deadline_days_offset early_registration_deadline_days_offset
            enrollment_deadline_days_offset graduation_days_offset_from_end
            external_schedule_url program_guide_url stripe_plans scholarship_levels
            internal_notes project_submission_email diploma_generation_days_offset_from_end
            learner_project_ids enrollment_agreement_template_id playlist_collections
            isolated_network
        ).each do |key|

            if hash.key?(key)
                val =  hash[key]
                val = nil if val.is_a?(String) && val.blank?
                self[key] = val
            end

        end

        %w(start_date end_date application_deadline).each do |key|
            if hash.key?(key)
                self[key] = Time.at(hash[key])
            end
        end

        # then update associated group groups
        groups = hash["groups"]
        if groups
            self.access_groups = AccessGroup.where(id: groups.map { |g| g['id'] } )
        end

        if hash.key?('slack_rooms')
            # we expect ids for new rooms to be set client-side
            ids = hash['slack_rooms'].map { |e| e['id'] }

            # destroy any rooms that have been removed from the list
            self.slack_rooms.select { |r| !ids.include?(r.id) }.each(&:destroy)

            hash['slack_rooms'].each do |slack_room_hash|
                slack_room = CohortSlackRoom.find_by_id(slack_room_hash['id']) || CohortSlackRoom.new(id: slack_room_hash['id'])
                slack_room_hash.slice('title', 'admin_token', 'url', 'cohort_id').each do |key, value|
                    slack_room.send(:"#{key}=", value)
                end
                slack_room.save!
            end
            self.slack_rooms.reload
        end
    end

    def playlist_pack_ids
        return self.required_playlist_pack_ids + self.specialization_playlist_pack_ids
    end

    # unused, but useful so we should leave it here
    def get_required_playlist_locale_packs
        Playlist::LocalePack.where(id: self.required_playlist_pack_ids)
    end

    def get_foundations_playlist_locale_pack
        return self.required_playlist_pack_ids[0] && Playlist::LocalePack.find(self.required_playlist_pack_ids[0])
    end

    def get_first_concentration_playlist_locale_pack
        return self.required_playlist_pack_ids[1] && Playlist::LocalePack.find(self.required_playlist_pack_ids[1])
    end

    def get_required_lesson_stream_locale_pack_ids_by_playlist(limit, locale = nil)
        self.required_playlist_pack_ids.slice(0, limit).map do |playlist_locale_pack_id|
            Playlist.stream_locale_pack_ids_for_playlist(playlist_locale_pack_id, locale)
        end
    end

    def get_foundations_lesson_stream_locale_pack_ids(locale)
        get_required_lesson_stream_locale_pack_ids_by_playlist(1, locale)[0]
    end

    def get_first_concentration_lesson_stream_locale_pack_ids(locale)
        get_required_lesson_stream_locale_pack_ids_by_playlist(2, locale)[1]
    end

    def get_stream_locale_pack_ids
        # NOTE: this will rely on the published version of this cohort
        PublishedCohortStreamLocalePack.where(cohort_id: self.attributes['id']).pluck(:stream_locale_pack_id)
    end

    def get_required_stream_locale_pack_ids
        # NOTE: this will rely on the published version of this cohort
        PublishedCohortStreamLocalePack.where(cohort_id: self.attributes['id'], required: true).pluck(:stream_locale_pack_id)
    end

    def get_required_stream_count
        PublishedCohortStreamLocalePack.where(cohort_id: self.attributes['id'], required: true).count
    end

    def get_optional_stream_locale_pack_ids
        # NOTE: this will rely on the published version of this cohort
        PublishedCohortStreamLocalePack.where(cohort_id: self.attributes['id'], required: false).pluck(:stream_locale_pack_id)
    end

    def get_specialization_stream_locale_pack_ids
        # NOTE: this will rely on the published version of this cohort
        PublishedCohortStreamLocalePack.where(cohort_id: self.attributes['id'], specialization: true).pluck(:stream_locale_pack_id)
    end

    def get_required_lesson_locale_pack_ids
        # NOTE: this will rely on the published version of this cohort
        ActiveRecord::Base.connection.execute(%Q~
            select lesson_locale_pack_id from published_cohort_lesson_locale_packs
            where cohort_id = '#{self.attributes['id']}'
                and required=true
        ~).to_a.map { |row| row['lesson_locale_pack_id'] }
    end

    def get_all_lesson_locale_pack_ids
        # NOTE: this will rely on the published version of this cohort
        ActiveRecord::Base.connection.execute(%Q~
            select lesson_locale_pack_id from published_cohort_lesson_locale_packs
            where cohort_id = '#{self.attributes['id']}'
        ~).to_a.map { |row| row['lesson_locale_pack_id'] }
    end

    def enrollment_deadline
        start_date.add_dst_aware_offset(enrollment_deadline_days_offset.days)
    end

    # We arrived at two weeks based on the following thought process (copied from Slack):
    # 1) Don't charge them too soon / charge them at a similar time to when many normal,
    #   non-deferred people would be charged again
    #
    #   Note that typically users would have first registered about two weeks before the
    #   start of the program, then would be charged four weeks later, so by waiting two weeks
    #   we somewhat line up deferred users with most normal users.
    #
    # 2) Don't charge them too late, i.e.: before the enrollment deadline, so if the charge
    #   hits their card and it reminds them they can't continue and therefore need to defer again,
    #   we can defer them and it will still be prior to the enrollment deadline
    #   (and therefore not affect graduation rates yet)
    def trial_end_date
        start_date + 2.weeks
    end

    def acceptance_date
        start_date.add_dst_aware_offset(Cohort::ACCEPTANCE_DATE_OFFSET)
    end

    def on_period_start_job_details
        details = {}
        periods.each_with_index do |period, i|
            # since periods are 1-indexed in published_cohort_periods, we 1-index them here as well
            # also, add 30 minutes to queue to stagger with auto-expulsion job
            # see https://trello.com/c/yF4v2OPw
            details[start_time_for_period(period) + 30.minutes] = [i+1]
        end
        details
    end

    def on_period_action_job_details
        details = {}

        # We use compare_by_identify in case of multiple actions happening at the same time (since we use the date as the hash)
        details.compare_by_identity # https://stackoverflow.com/a/18796095/1747491

        periods.each_with_index do |period, i|
            if period["actions"]
                period["actions"].each do |action|
                    # since periods are 1-indexed in published_cohort_periods, we 1-index in the job argument as well
                    details[self.relative_time_from_end_of_period(action["days_offset_from_end"], period)] = [i+1, action["id"]]
                end
            end
        end
        return details
    end

    def on_period_exercise_job_details
        details = {}

        # We use compare_by_identify in case of multiple actions happening at the same time (since we use the date as the hash)
        details.compare_by_identity # https://stackoverflow.com/a/18796095/1747491

        periods.each_with_index do |period, i|
            period["exercises"]&.each do |exercise|
                # since periods are 1-indexed in published_cohort_periods, we 1-index in the job argument as well
                details[self.relative_time_from_end_of_period(exercise["hours_offset_from_end"], period, "hours")] = [i+1, exercise["id"]]
            end
        end
        return details
    end

    def admission_round_midpoint_job_details
        # this communication is based on the schedule of mba cohorts
        return [] unless self.program_type == 'mba'
        midpoints = []
        AdmissionRound.where(cohort_id: self.id).each do |admission_round|
            if admission_round.midpoint && admission_round.midpoint > Time.now
                midpoints << admission_round.midpoint
            end
        end
        midpoints
    end

    def admission_round_ends_job_details(offset)
        # this communication is based on the schedule of mba cohorts
        return [] unless self.program_type == 'mba'
        dates = []
        AdmissionRound.where(cohort_id: self.id).each do |admission_round|
            date = admission_round.application_deadline.add_dst_aware_offset(offset)
            if date > Time.now
                dates << date
            end
        end
        dates
    end

    def admission_round_ends_soon_job_details
        # Early AM deadlines communicated as preceding day to user,
        # so we need to add 1 day for desired queue
        # see https://trello.com/c/metOn0Wg
        admission_round_ends_job_details(-6.days)
    end

    def admission_round_ends_tomorrow_job_details
        # Early AM deadlines communicated as preceding day to user,
        # so we need to add 1 day for desired queue
        # see https://trello.com/c/metOn0Wg
        admission_round_ends_job_details(-2.days)
    end

    def decision_day_deferral_reminder_job_details
        return nil unless self.program_type_config.supports_deferral?

        last_admission_round = AdmissionRound.where(cohort_id: self.id).reorder(:index).last

        # The decision_date on admission rounds isn't calculated using an offset like
        # registration_deadline and early_registration_deadline and we're not passing
        # an offset to add_dst_aware_offset, so we could just use decision_date without
        # also using add_dst_aware_offset here, but in remembrance of the mountain goat
        # in Hoodwinked, we are prepared should these conditions ever change.
        last_admission_round&.decision_date&.add_dst_aware_offset(0.days)
    end

    def ensure_slack_room_assignment_job

        # We don't need to re-run this if the cohort has already started. In that
        # case people have already been assigned to slack rooms. However, we also don't want
        # to destroy an existing slack_room_assignment, because that's necessary to be able
        # to display locations in the admin UI. Therefore, we short circuit early in this method,
        # prior to actually removing the "out of date" assignment.
        return if self.start_date < Time.now

        # When we are batch saving a bunch of applications, we do not need to do this over and over.
        # If we have used preloading, then all those applications should share a reference to a single
        # cohort instance, and this will work. See CohortApplicationController#batch_update
        if @last_time_ensured_slack_room_assignment_job && @last_time_ensured_slack_room_assignment_job + 1.minute > Time.now
            return
        end
        @last_time_ensured_slack_room_assignment_job = Time.now

        # if there is one, the fact that we're calling this method means it is out of date.
        # We don't want someone using it in the admin to assign slack rooms, so we
        # delete it right away. If someone tries to use the admin before the job has run
        # to create a new one, they will get a message saying they have to wait.
        self.slack_room_assignment&.destroy

        # we don't need to do this if there are no slack rooms
        return unless self.slack_rooms.any?

        verify_cohort_jobs(
            # We wait a minute to debounce in case we're saving a bunch of applications
            # at once.
            # We don't assign slack rooms until after the registration deadline,
            # so no need to do this now
            [
                1.minute.from_now,
                self.registration_deadline
            ].compact.max,
            Cohort::SlackRoomAssignmentJob,

            #Since the job re-runs itself if anything changes while it's running,
            # we can safely use skip_if_job_running
            {skip_if_job_running: true}
        )
    end

    def registration_deadline
        supports_registration_deadline? ? timestamp_relative_to_start_date(registration_deadline_days_offset) : nil
    end

    def early_registration_deadline
        supports_early_registration_deadline? ? timestamp_relative_to_start_date(early_registration_deadline_days_offset) : nil
    end

    def timestamp_relative_to_start_date(offset)
        return offset.present? ? start_date.add_dst_aware_offset(offset.days) : nil
    end

    def graduation_date
        (supports_schedule? && !graduation_days_offset_from_end.nil?) ? end_date.add_dst_aware_offset(graduation_days_offset_from_end.days) : nil
    end

    def with_users_for_action(period, action)
        users_for_action = accepted_users

        if ["expulsion", "expulsion_warning"].include?(action["type"])
            # Throw out users with skip_period_expulsion set to true
            users_for_action = users_for_action.where.not(cohort_applications: {
                skip_period_expulsion: true
            })

            # Throw out users who have graduated already
            users_for_action = users_for_action.where.not(cohort_applications: {
                graduation_status: 'graduated'
            })
        end

        if action["rule"] == "requirements_not_met"
            users_for_action.each do |user|
                yield user if !user.has_met_requirements_for_period?(self, period)
            end
        end
    end

    def jobs_for_job_klass(job_klass)
        Delayed::Job.where(queue: job_klass.queue_name)
                    .where("handler ILIKE '\%arguments\%#{self.attributes['id']}\%'")
    end

    def verify_cohort_jobs(_datetimes, job_klass, opts = {})
        skip_if_job_running = opts[:skip_if_job_running] || false

        # remove any jobs that are queued for the future and have not yet run
        jobs = jobs_for_job_klass(job_klass)
        jobs.where(locked_at: nil, failed_at: nil).destroy_all

        return if _datetimes.nil? || !was_published

        if skip_if_job_running
            return if jobs.where.not(locked_at: nil).any?
        end

        # datetimes can be either
        # 1. a single time object
        # 2. an array of time objects
        # 3. a hash whose keys are time objects and whose values are
        #    arrays of extra arguments to be passed to the job after the
        #    cohort_id argument

        if _datetimes.is_a?(Time) || _datetimes.is_a?(DateTime)
            datetimes = {_datetimes => []}
        elsif _datetimes.is_a?(Array)
            datetimes = {}
            _datetimes.each do |datetime|
                datetimes[datetime] = []
            end
        else
            datetimes = _datetimes
        end

        datetimes.each do |datetime, args|

            # If the time is in the past, then assume it has been run already and do
            # not create a new one
            next if datetime < Time.now

            # we thought that we would have to worry about checking for jobs that
            # were locked or failed here, but for any of those to exist already,
            # datetime would have to already be in the past, and we would have
            # already returned, so seems unnecessary.

            args = args.clone.unshift(self.attributes['id'])
            job_klass.set(wait_until: datetime).perform_later(*args)
        end
    end

    def unpublished_required_playlist_pack_ids
        self.required_playlist_pack_ids - Playlist.all_published.pluck(:locale_pack_id)
    end

    def unpublished_required_playlists
        Playlist.where({
            locale_pack_id: unpublished_required_playlist_pack_ids,
            locale: 'en'
        })
    end

    def raise_if_unpublished_playlist_is_required
        if self.was_published
            unpublished_required_playlists.each do |playlist|
                errors.add(:cohort, "cannot be published when the required playlist '#{playlist.title}' has not been published")
            end

            unless errors.empty?
                raise ActiveRecord::RecordInvalid.new(self)
            end
        end
    end

    def after_publish_change
        # Check if we need to raise an error
        if cohort_promotion.present? && !has_published_version?
            errors.add(:promoted, "cannot be true for an unpublished cohort")
        end
        unless errors.empty?
            raise ActiveRecord::RecordInvalid.new(self)
        end

        self.verify_cohort_jobs(on_period_action_job_details, Cohort::OnPeriodActionJob)
        self.verify_cohort_jobs(on_period_exercise_job_details, Cohort::OnPeriodExerciseJob)
        self.verify_cohort_jobs(on_period_start_job_details, Cohort::OnPeriodStartJob)
        self.verify_cohort_jobs(admission_round_ends_soon_job_details, Cohort::AdmissionRoundEndsSoonJob)
        self.verify_cohort_jobs(admission_round_ends_tomorrow_job_details, Cohort::AdmissionRoundEndsTomorrowJob)
        self.verify_cohort_jobs(decision_day_deferral_reminder_job_details, Cohort::DecisionDayDeferralReminderJob)
        self.verify_cohort_jobs(enrollment_deadline, Cohort::UpdateCohortStatusChangesAtEnrollmentDeadlineJob)
        verify_admissions_round_midpoint_jobs_for_all_later_rounds
        ensure_enrollment_agreements_if_necessary

        Cohort::UpdateFinalScoresJob.perform_later(self.id)
    end

    def verify_setup_slack_channels_jobs(&block)
        old_version = self.published_version
        yield
        self.published_versions.reload
        new_version = self.published_version

        if new_version && old_version&.slack_channels_that_receive_automated_messages&.sort != new_version.slack_channels_that_receive_automated_messages.sort
            self.slack_rooms.each(&:verify_setup_slack_channels_job)
        end
    end

    def verify_admissions_round_midpoint_jobs_for_all_later_rounds
        verify_admissions_round_midpoint_jobs

        # since changing this cohort may have changed the start time
        # for subsequent admission rounds, we need to verify those events for all
        # cohorts with rounds starting after today
        AdmissionRound.where("application_deadline > ? ", Time.now)
            .where.not(cohort_id: self.attributes['id'])
            .distinct.pluck('cohort_id')
            .each do |cohort_id|
                cohort = Cohort.find(cohort_id)
                cohort.verify_admissions_round_midpoint_jobs
        end
    end

    def verify_admissions_round_midpoint_jobs
        jobs_for_job_klass(Cohort::AdmissionRoundMidpointJob).destroy_all
        if was_published
            self.verify_cohort_jobs(self.admission_round_midpoint_job_details, Cohort::AdmissionRoundMidpointJob)
        end
    end

    def ensure_enrollment_agreements_if_necessary
        return unless self.requires_enrollment_agreement? && self.enrollment_agreement_template_id.present?

        self.verify_cohort_jobs(decision_date, Cohort::DecisionDayEnrollmentAgreementsJob)
    end

    def supports_auto_expulsion?
        program_type == 'mba'
    end

    def supports_document_upload?
        ['mba', 'emba'].include?(self.program_type)
    end

    def supports_peer_recommendations?
        ['mba', 'emba'].include?(self.program_type)
    end

    def stripe_plan_ids
        self.stripe_plans && self.stripe_plans.map { |plan| plan['id'] }
    end

    def as_json(opts = {})
        opts = opts.with_indifferent_access

        basic_json = {
            id: self.attributes['id'],
            name: self.name,
            program_type: self.program_type
        }.stringify_keys

        return basic_json if opts[:fields] == ['BASIC_FIELDS']

        if opts[:fields] == ['UPDATE_FIELDS']
            # the slack room assignment can be unset by an updated if slack rooms are saved,
            # so send down the new list to the client
            association(:slack_room_assignment).reload

            # slack_rooms are included in the fields array so that we have access to
            # each slack room's created_at timestamp which is needed for UI purposes.
            fields = [
                'updated_at',
                'created_at',
                'published_at',
                'slack_room_assignment',
                'diff_from_published_version',
                'slack_rooms'
            ]
        else
            fields = opts[:fields]
        end

        cohort_json = basic_json.merge({
            title: self.title,
            description: self.description,
            specialization_playlist_pack_ids: self.specialization_playlist_pack_ids,
            periods: self.periods_as_json({fields: opts[:fields].dup}),
            start_date: self.start_date.to_timestamp,
            end_date: self.end_date.to_timestamp,
            num_required_specializations: self.num_required_specializations,
            admission_rounds: admission_rounds,
            id_verification_periods: id_verification_periods,
            registration_deadline_days_offset: registration_deadline_days_offset,
            early_registration_deadline_days_offset: early_registration_deadline_days_offset,
            enrollment_deadline_days_offset: enrollment_deadline_days_offset,
            graduation_days_offset_from_end: graduation_days_offset_from_end,
            external_schedule_url: external_schedule_url,
            stripe_plans: self.stripe_plans,
            scholarship_levels: self.scholarship_levels,
            program_guide_url: self.program_guide_url,
            diploma_generation_days_offset_from_end: self.diploma_generation_days_offset_from_end,
            current_period_index: self.current_period_index,
            project_submission_email: self.project_submission_email,
            playlist_collections: self.playlist_collections,
            isolated_network: self.isolated_network,
            learner_project_ids: learner_project_ids,
            learner_projects: self.cohort_level_learner_projects, # cached at rails layer
            trial_end_date: self.trial_end_date.to_timestamp
        }).stringify_keys

        # we don't always need these
        cohort_json['updated_at'] = self.updated_at.to_timestamp if self.has_attribute?('updated_at')
        cohort_json['created_at'] = self.created_at.to_timestamp if self.has_attribute?('created_at')

        except = opts[:except] || []

        ['published_at', 'groups', 'slack_room_assignment',
            'diff_from_published_version', 'slack_room_assignment', 'slack_rooms',
            'internal_notes', 'enrollment_agreement_template_id'].each do |f|
            if fields&.include?('ADMIN_FIELDS') || fields&.include?(f)
                v = self.send(f)
                if v.respond_to?(:strftime)
                    cohort_json[f] = v.to_timestamp
                elsif v.is_a?(Array)
                    cohort_json[f] = v.map(&:as_json)
                else
                    cohort_json[f] = v.as_json
                end
            end
        end

        if fields&.include?('ADMIN_FIELDS')
            fields.delete('ADMIN_FIELDS')
        else
            # we only need these in the client UI, so we save some time by not loading it in the admin case

            # To determine locking status, we only need to know the first stream in the first cohort,
            # but this used to be based on the whole Foundations playlist, so we still send the whole thing down instead.
            # See https://trello.com/c/k4UnLPgP
            if opts['user_id'].present?

                # be defensive against the user not existing in case this is a new user who is not
                # yet saved (which will happen if there is a validation error when registering)
                user = User.find_by_id(opts['user_id'])
                locale = user ? user.locale : I18n.default_locale
                cohort_json['foundations_lesson_stream_locale_pack_ids'] = get_foundations_lesson_stream_locale_pack_ids(locale.to_s)

                # Since this is only used on the student dashboard and requires
                # a query I moved setting it into this user_id option block
                cohort_json['foundations_playlist_locale_pack'] = get_foundations_playlist_locale_pack.as_json
            end
        end

        cohort_json = cohort_json.slice(*fields) if fields.present?
        cohort_json = cohort_json.except(*except) if except.present?

        cohort_json
    end

    def current_period_index
        now = Time.zone.now.utc

        # If we are currently before the start_date just return -1
        if now < self.start_date
            return -1
        end

        rolling_date = self.start_date
        self.periods.each_with_index do |period, i|
            end_date = rolling_date.add_dst_aware_offset((period['days_offset'] + period['days']).days)

            # If we are currently on or after the period start_date minus the days_offset (I figure if the user is in that gap we
            # should just consider them in the upcoming period), and before the period end_date
            if rolling_date <= now && now < end_date
                return i
            end

            rolling_date = end_date
        end

        # Otherwise we are currently after the end_date so just return nil
        return nil
    end

    def next_exam_info(period_or_index)
        index = period_or_index.is_a?(Integer) ? period_or_index - 1 : periods.index(period_or_index)
        subsequent_periods = self.periods.slice(index+1, self.periods.size) || []
        exam_period = subsequent_periods.detect { |p| p['style'] == 'exam' }
        return nil unless exam_period
        required_course_entry = exam_period['stream_entries'].detect { |e| e['required'] }
        return nil unless required_course_entry
        {
            locale_pack_id: required_course_entry['locale_pack_id'],
            start_time: self.start_time_for_period(exam_period)
        }
    end

    def start_time_for_period(period_or_index)

        # if an index is passed in here, it should be 1-indexed.  This is so that
        # we can follow the way that published_cohort_periods defines period 0 as
        # all of the periods before the start date
        index = period_or_index.is_a?(Integer) ? period_or_index - 1 : periods.index(period_or_index)

        if index < 0
            raise "start_time_for_period takes a 1-indexed period_index as it's argument"
        end

        time = start_date
        i = 0
        while i <= index
            period = periods[i]
            time = time.add_dst_aware_offset(period['days_offset'].days)

            if i < index
                time = time.add_dst_aware_offset(period['days'].days)
            end

            i += 1
        end

        time
    end

    def end_time_for_period(period_or_index)
        # if an index is passed in here, it should be 1-indexed.  This is so that
        # we can follow the way that published_cohort_periods defines period 0 as
        # all of the periods before the start date
        index = period_or_index.is_a?(Integer) ? period_or_index - 1 : periods.index(period_or_index)

        if index < 0
            raise "start_time_for_period takes a 1-indexed period_index as it's argument"
        end

        time = start_date
        i = 0
        while i <= index
            period = periods[i]
            time = time.add_dst_aware_offset((period['days_offset'] + period['days']).days)
            i += 1
        end

        time
    end

    def relative_time_from_end_of_period(offset, period_or_index, unit = "days")
        end_time = self.end_time_for_period(period_or_index)

        if unit == "days"
            return end_time.add_dst_aware_offset(offset.days)
        elsif unit == "hours"
            return end_time.add_dst_aware_offset(offset.hours)
        end
    end

    def period_for_required_stream_locale_pack_id(locale_pack_id)
        periods.detect do |period|
            period['stream_entries'].detect { |entry| entry['locale_pack_id'] == locale_pack_id && entry['required'] }
        end
    end

    def requires_stream_locale_pack_id?(locale_pack_id)
        ActiveRecord::Base.count_by_sql("
            SELECT count(*)
            FROM published_cohort_stream_locale_packs
            WHERE published_cohort_stream_locale_packs.cohort_id = '#{self.attributes['id']}'
                  AND published_cohort_stream_locale_packs.required = TRUE
                  AND published_cohort_stream_locale_packs.stream_locale_pack_id = '#{locale_pack_id}';
        ") > 0
    end

    def midterm_exam_locale_pack_id

        # only mba has a midterm, and since we have no real definition of 'midterm'
        # in our data model, it is safer to be really strict about this
        raise "Only for mba cohorts" unless program_type == 'mba'

        midterm_periods = self.periods.select { |p| p['style'] == 'exam' && p['exam_style'] == 'intermediate' }
        if midterm_periods.size != 1
            unless Rails.env.test? || self.name == 'MBA1'
                err = RuntimeError.new("Unexpected number of midterm exam periods")
                err.raven_options = {
                    cohort_id: self.id,
                    cohort_name: self.name,
                    midtem_period_count: midterm_periods.size
                }
                raise err
            end
            return
        end

        stream_entries = midterm_periods[0]['stream_entries'].select { |e| e['required'] }
        raise "Expecting exactly one required stream" unless stream_entries.size == 1
        stream_entries[0]['locale_pack_id']
    end

    def final_exam_locale_pack_id

        # only mba has a final, and since we have no real definition of 'final'
        # in our data model, it is safer to be really strict about this
        raise "Only for mba cohorts" unless program_type == 'mba'

        final_periods = self.periods.select { |p| p['style'] == 'exam' && p['exam_style'] == 'final' }
        if final_periods.size != 1
            unless Rails.env.test?
                err = RuntimeError.new("Unexpected number of final exam periods")
                err.raven_options = {
                    cohort_id: self.id,
                    cohort_name: self.name,
                    final_period_count: final_periods.size
                }
                raise err
            end
            return
        end

        stream_entries = final_periods[0]['stream_entries'].select { |e| e['required'] }
        raise "Expecting exactly one required stream" unless stream_entries.size == 1
        stream_entries[0]['locale_pack_id']
    end

    def expected_percent_complete(period_index)

        record = ActiveRecord::Base.connection.execute(%Q~
            select
                -- count of required streams up to the requested
                -- period
                coalesce((
                    select
                        cumulative_required_stream_pack_ids_count
                    from
                        published_cohort_periods
                    where
                        cohort_id='#{self.attributes['id']}'
                        and index = '#{period_index}'
                ), 0)::float

                -- divided by
                /

                -- count of all required streams
                (
                    select
                        count(*)
                    from published_cohort_stream_locale_packs
                    where required=true
                    and cohort_id='#{self.attributes['id']}'
                ) as expected_percent_complete
        ~).to_a.first

        record && record['expected_percent_complete']
    end

    def expected_stream_lpids_complete(period_index)
        expected_stream_lpids_complete = ActiveRecord::Base.connection.execute(%Q~
            select
                unnest(cumulative_required_stream_pack_ids) as expected_stream_lpids_complete
                from published_cohort_periods
                    where
                        cohort_id='#{self.attributes['id']}'
                        and index = '#{period_index}'
        ~).to_a.map { |row| row['expected_stream_lpids_complete'] }

        return expected_stream_lpids_complete || []
    end

    def decision_date
        unless defined?(@decision_date)
            @decision_date = SafeCache.fetch(cache_key_for_attr("decision_date"), expires_in: 1.week) do
                AdmissionRound.where(cohort_id: self.attributes['id']).maximum('decision_date')
            end
        end
        @decision_date
    end

    def application_deadline
        unless defined?(@application_deadline)
            @application_deadline = SafeCache.fetch(cache_key_for_attr("application_deadline"), expires_in: 1.week) do
                AdmissionRound.where(cohort_id: self.attributes['id']).maximum('application_deadline')
            end
        end
        @application_deadline
    end

    def sister_cohort
        unless defined? @sister_cohort
            if self.isolated_network
                return nil
            elsif program_type == 'mba'
                sister_program_type = 'emba'
            elsif program_type == 'emba'
                sister_program_type = 'mba'
            else
                raise "sister_cohort is only defined for mba and emba cohorts"
            end

            sister_admission_rounds = AdmissionRound.joins(:cohort)
                                        .where(cohorts: {program_type: sister_program_type, isolated_network: false})
                                        .where(application_deadline: application_deadline)

            # In the wild, there will only be one sister admission round, but we've
            # run into intermittently failing spec, presumably because there was an
            # admission round ending at the same time that wasn't the last round for a cohort
            sister_admission_round = sister_admission_rounds.includes(:cohort)
                                        .detect { |admission_round| admission_round.published_cohort.application_deadline == application_deadline }

            @sister_cohort = sister_admission_round && sister_admission_round.published_cohort
        end
        @sister_cohort
    end

    def validate_supports_schedule
        if !supports_schedule? && !periods.empty?
            errors.add(:periods, "must be empty if cohort does not support schedule")
        end
    end

    def validate_supports_payments
        if supports_payments? && (stripe_plans.nil? || stripe_plans.empty?)
            errors.add(:stripe_plans, "must be provided if cohort supports payments")
        end
    end

    # This is used by the MdpPredictor to determine where each period fits in the schedule
    def milestone_indexes

        if program_type == 'mba' && name != 'MBA1'
            periods = published_cohort_periods.sort_by(&:period_start)

            exam_periods = periods.select { |p| p['style'] == 'exam' }
            midterm_index = exam_periods[0].index
            final_exam_index = exam_periods[1].index

            review_periods = periods.select { |p| p['style'] == 'review' }
            # MBA2 had no midterm review period.  All later cohorts have 2 review periods
            raise "expecting one or two review periods" unless [1,2].include?(review_periods.size)
            midterm_review_index = review_periods.size == 2 ? review_periods[0].index : nil
            final_review_index = review_periods.size == 2 ? review_periods[1].index : review_periods[0].index

            enrollment_deadline_index = periods.detect { |p| p.period_end > enrollment_deadline }.index

            return {
                enrollment_deadline: enrollment_deadline_index,
                halfway_to_midterm: enrollment_deadline_index + ((midterm_review_index || midterm_index) - enrollment_deadline_index)/2,
                midterm_review: midterm_review_index,
                midterm: midterm_index,
                halfway_to_final: midterm_index + (final_review_index - midterm_index)/2,
                final_review: final_review_index,
                final_exam: final_exam_index
            }
        elsif program_type == 'emba'
            periods = published_cohort_periods.sort_by(&:period_start)

            exam_periods = periods.select { |p| p['style'] == 'exam' }
            raise "We may want to update this if we change the number of exams" unless exam_periods.size == 8

            enrollment_deadline_index = periods.detect { |p| p.period_end > enrollment_deadline }.index

            return {
                enrollment_deadline: enrollment_deadline_index,
                exam_1: exam_periods[0].index,
                exam_2: exam_periods[1].index,
                exam_4: exam_periods[3].index,
                exam_6: exam_periods[5].index,
                exam_8: exam_periods[7].index
            }
        else
            raise NotImplementedError.new("milestone_indexes not defined for this cohort")
        end
    end

    def get_last_period_before_time(time)
        published_cohort_periods.sort_by(&:period_start).reverse.detect { |period| period.period_end <= time }
    end

    def mba1?
        name == 'MBA1'
    end

    def ensure_stripe_products_and_plans

        return unless supports_payments?

        product_id = program_type_config.program_type
        product = begin
            Stripe::Product.retrieve(product_id)
        rescue Stripe::InvalidRequestError => err
            raise err unless err.message.match(/No such product/)
        end

        statement_descriptor = program_type_config.stripe_statement_descriptor

        if product
            product.name = program_type_config.program_title
            product.statement_descriptor = statement_descriptor
            product.save
        else
            Stripe::Product.create({
                id: program_type_config.program_type,
                name: program_type_config.program_title,
                statement_descriptor: statement_descriptor,
                type: 'service'
            })
        end

        program_type_config.stripe_plan_config.each do |plan_id, config|

            plan_id = plan_id.to_s
            plan = begin
                Stripe::Plan.retrieve(plan_id)
            rescue Stripe::InvalidRequestError => err
                raise err unless err.message.match(/No such plan/)
            end

            if plan

                # only the metadata and name can be updated
                plan.metadata = config[:metadata]
                plan.nickname = config[:nickname]
                plan.save

                plan.product = product_id
                plan.save

            else
                Stripe::Plan.create(config.merge({
                    id: plan_id,
                    product: product_id
                }))
            end

        end

    end

    def requires_id_upload?
        self.supports_document_upload? && !self.id_verification_periods&.any?
    end

    def active_verification_period
        id_verification_periods&.sort_by { |p| -p['start_date_days_offset'] }&.detect do |verification_period|
            start_date + verification_period['start_date_days_offset'].days < Time.now
        end
    end

    def index_for_active_verification_period
        raise "No active_verification_period" unless self.active_verification_period
        id_verification_periods.index(active_verification_period)
    end

    def verification_periods_before_active_one
        active_verification_period_offset = (active_verification_period && active_verification_period['start_date_days_offset'])
        return [] if active_verification_period_offset.nil?

        id_verification_periods.select do |verification_period|
            verification_period['start_date_days_offset'] < active_verification_period_offset
        end
    end

    def non_user_facing_attrs
        ['internal_notes', 'updated_at']
    end

    def user_facing_change_since_last_published?
        edited_since_last_published? &&
        (diff_from_published_version.keys - non_user_facing_attrs).any?
    end

    def diff_from_published_version(only_user_facing = false)
        return nil unless _published_version = published_version
        diff = {}
        self.attributes.each do |key, val|
            next if ['was_published'].include?(key)
            next if only_user_facing && non_user_facing_attrs.include?(key)

            if key == 'periods'
                self.periods.each_with_index do |period, i|
                    add_to_diff_from_published_version(diff, self, "period_#{i+1}", period, _published_version.periods[i])
                end
            else
                add_to_diff_from_published_version(diff, self, key, val, _published_version[key])
            end
        end
        diff.any? ? diff : nil
    end

    def cohort_level_learner_projects
        learner_project_ids.present? ? LearnerProject.find_cached_by_id(self.learner_project_ids).as_json : []
    end

    def periods_as_json(opts = {})
        periods.map do |period|
            period.as_json.merge(
                'learner_projects' => LearnerProject.find_cached_by_id(period['learner_project_ids']).as_json({fields: opts[:fields].dup}),
            )
        end
    end

    # see comment in CohortSlackRoom#ensure_accepted_users_have_slack_rooms
    def ensure_accepted_users_have_slack_rooms
        return unless self.slack_rooms.any?
        slack_room = self.slack_rooms.first
        cohort_applications.where(status: 'accepted', cohort_slack_room_id: nil).each { |ca| ca.update(cohort_slack_room_id: slack_room.id )}
    end

    # Our cohorts can be configured to send automated messages to users in their respective Slack room.
    # This method simply returns an array containing the names of Slack channels that receive these messages.
    def slack_channels_that_receive_automated_messages
        # Messages are currently only configured for periods with exercises
        self.periods.pluck('exercises').flatten.compact.pluck('channel').uniq.map { |channel| channel.gsub('#', '') } # remove leading #
    end

    private def add_to_diff_from_published_version(diff, cohort, key, val, current)
        return if val.blank? && current.blank?
        val = val.to_s
        current = current.to_s
        return if val == current
        return if val.blank? && current.blank?
        diff[key] = [current, val]
    end

    private def cache_key_for_attr(attr)
        "#{attr}/#{self.attributes['id']}/#{self.published_at.to_timestamp}"
    end

end
