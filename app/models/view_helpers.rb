module ViewHelpers

    def self.migrate(migration, version_number)

        # We need to deal with the possibility of migrations being run out of order (i.e.
        # if a branch that was created earlier ends up rolling out later).  So, for each
        # view, we aim for the maximum of the new version we're targeting and all of the
        # versions that have already been run
        versions_that_should_be_run = (versions_that_have_been_run + [version_number]).uniq.sort

        view_to_version_map = get_views_to_rebuild(versions_that_should_be_run) do |klass, versions_of_this_view_that_have_been_run, versions_of_this_view_that_should_be_run|
            # any view that has a version that SHOULD be run but HAS NOT been needs to be migrated
            (versions_of_this_view_that_should_be_run - versions_of_this_view_that_have_been_run).any?
        end
        rebuild_views(migration, view_to_version_map) do
            yield if block_given?
        end

        # we generally only run RefreshMaterializedInternalReportsViews once per day, but if something
        # has changed then these views might now be unpopulated, so we should run it again soon.  It probably
        # doesn't matter, but ideally we want a server with new code to pick the job up, so we wait 20 minutes
        # which most likely means that the rollout is complete
        RefreshMaterializedInternalReportsViews.perform_later_with_debounce(Time.now + 20.minutes)
    end

    def self.rollback(migration, version_number)

        # We need to deal with the possibility of migrations being run out of order (i.e.
        # if a branch that was created earlier ends up rolling out later).  So, for each
        # view, we aim for the maximum of the new version we're targeting and all of the
        # versions that have already been run
        versions_that_should_be_run = (versions_that_have_been_run - [version_number]).uniq.sort

        view_to_version_map = get_views_to_rebuild(versions_that_should_be_run) do |klass, versions_of_this_view_that_have_been_run, versions_of_this_view_that_should_be_run|
            # any view that has a version that HAS been run but SHOULD NOT have been
            # needs to rolled back
            (versions_of_this_view_that_have_been_run - versions_of_this_view_that_should_be_run).any?
        end
        rebuild_views(migration, view_to_version_map) do
            yield if block_given?
        end

    end

    def self.remove_view_temporarily_while(migration, klass, &block)
        view_to_version_map = get_views_to_rebuild(versions_that_have_been_run) do |_klass, ignore1, ignore2|
            _klass == klass
        end
        rebuild_views(migration, view_to_version_map, &block)
    end

    def self.versions_that_have_been_run
        ActiveRecord::Base.connection.execute('select version from schema_migrations order by version').to_a.map { |r| r['version'] }.map(&:to_i)
    end

    # NOTE: this method should no longer be used publicly.  Now
    # use migrate or rollback.  The problem with this method is
    # that if migrations get run out of order it can lead
    # to things getting messed up.  It is probably safe if you make
    # sure that it isn't run after migrations with a later
    # version number that also use ViewHelpers.
    def self.set_versions(migration, version_number)

        if versions_that_have_been_run.max >= 20170728155738
            raise "set_versions is deprecated. Use migrate and rollback"
        end

        definitions = definitions = []

        in_dependency_order.each do |klass|
            definitions << {klass => klass.max_version_before(version_number)}
        end

        # drop all the views from the bottom up
        definitions.reverse.each do |entry|
            klass = entry.keys.first
            klass.new(migration).drop
        end

        # create all the views from the top down
        definitions.each do |entry|
            klass = entry.keys.first
            meth = entry.values.first

            # any views that have no versions at or before this
            # number will not be created
            klass.new(migration).send(meth) if meth
        end
    end

    def self.ensure_all_populated
        in_dependency_order.each do |view|
            # We need to just handle the "not populated" error and ignore
            # if deprecated, rather than just skipping altogether, because in
            # our test:setup task, where we load a sql structure from
            # the last time we archived migrations, then populate the materialized
            # views, then actually run the migrations, we need the deprecated
            # views to still be populated to prevent an error when creating the
            # test db.
            begin
                view.ensure_populated
            rescue PG::ObjectNotInPrerequisiteState => e
                raise unless view.deprecated?
            end
        end
    end

    private
    def self.get_views_to_rebuild(versions_that_should_be_run, &block)
        view_to_version_map = view_to_version_map = []

        # once we've found any view that needs to be changed, we
        # will have to rebuild every view AFTER that one in the list,
        # since they may be dependent on it
        found_first_changed_view = false
        in_dependency_order.each do |klass|

            versions_of_this_view_that_should_be_run = versions_that_should_be_run & klass.version_numbers
            versions_of_this_view_that_have_been_run = versions_that_have_been_run & klass.version_numbers
            view_needs_rebuild = found_first_changed_view || yield(klass, versions_of_this_view_that_have_been_run, versions_of_this_view_that_should_be_run)
            found_first_changed_view = true if view_needs_rebuild

            if view_needs_rebuild
                version_number = (versions_that_should_be_run & klass.version_numbers).max
                version_meth = version_number ? :"version_#{version_number}" : nil
                view_to_version_map << {klass => version_meth}
            end
        end

        view_to_version_map
    end

    private
    def self.rebuild_views(migration, view_to_version_map, &block)
        # drop all the views from the bottom up
        view_to_version_map.reverse.each do |entry|
            klass = entry.keys.first
            klass.new(migration).drop
        end

        # do something while the views have all been dropped
        yield if block_given?

        # create all the views from the top down
        view_to_version_map.each do |entry|

            klass = entry.keys.first
            version_meth = entry.values.first

            # any views that have no versions at or before this
            # number will not be created
            klass.new(migration).send(version_meth) if version_meth
        end
    end

    def self.in_dependency_order
        @in_dependency_order ||= [
            # daily marketing update
            ViewHelpers::Acquisitions, # this one is refreshed by the daily marketing update

            # content views
            ViewHelpers::PublishedStreams,
            ViewHelpers::PublishedPlaylists,
            ViewHelpers::PublishedLessons,
            ViewHelpers::PublishedCohortAdmissionRounds,
            ViewHelpers::PublishedCohorts,
            ViewHelpers::PublishedStreamLessons,
            ViewHelpers::PublishedPlaylistStreams,
            ViewHelpers::PublishedPlaylistLessons,
            ViewHelpers::PublishedStreamLocalePacks,
            ViewHelpers::PublishedPlaylistLocalePacks,
            ViewHelpers::PublishedLessonLocalePacks,
            ViewHelpers::PublishedStreamLessonLocalePacks,
            ViewHelpers::PublishedCohortPeriods,
            ViewHelpers::PublishedCohortPeriodsStreamLocalePacks,
            ViewHelpers::CohortPlaylistLocalePacks, # deprecated in favor of PublishedCohortPlaylistLocalePacks
            ViewHelpers::PublishedCohortPlaylistLocalePacks,
            ViewHelpers::HackedAccessGroupsCohorts, # deprecated now that we use the schedule
            ViewHelpers::CohortStreamLocalePacks, # deprecated in favor of PublishedCohortStreamLocalePacks
            ViewHelpers::PublishedCohortStreamLocalePacks,
            ViewHelpers::CohortLessonLocalePacks, # deprecated in favor of PublishedCohortLessonLocalePacks
            ViewHelpers::PublishedCohortLessonLocalePacks,
            ViewHelpers::InstitutionPlaylistLocalePacks,
            ViewHelpers::InstitutionStreamLocalePacks,
            ViewHelpers::InstitutionLessonLocalePacks,
            ViewHelpers::CohortContentDetails, # deprecated in favor of PublishedCohortContentDetails
            ViewHelpers::PublishedCohortContentDetails, # removed

            # other critical (non-materialized) views
            ViewHelpers::ExternalUsers,
            ViewHelpers::UsersRelevantCohorts,
            ViewHelpers::YearsOfExperience, # deprecated and folded into CareerProfileSearchHelpers
            ViewHelpers::ApplicationsThatProvideStudentNetworkInclusion, # deprecated in favor of student_network_inclusions table
            ViewHelpers::StudentNetworkMembers, # deprecated in favor of student_network_inclusions table

            # progress views
            ViewHelpers::UserCanSeePlaylistLocalePacks, # non-materialized
            ViewHelpers::UserLessonProgressRecords, # no longer a view.  Now a table that is regularly updated
            ViewHelpers::PlaylistProgress,
            ViewHelpers::LessonActivityByCalendarDateRecords,
            ViewHelpers::UserProgressRecords,

            # internal reports views
            ViewHelpers::CohortApplicationsPlus,
            ViewHelpers::CohortUserLessonProgressRecords, # non-materialized
            ViewHelpers::CohortStatusChanges,
            ViewHelpers::CohortUserWeeks, # deprecated in favor of CohortUserPeriods
            ViewHelpers::CohortUserPeriods,
            ViewHelpers::CohortUserProgressRecords,
            ViewHelpers::ProgramEnrollmentProgressRecords,
            ViewHelpers::CohortConversionRates,
            ViewHelpers::GraduationKpis, # non-materialized
            ViewHelpers::Payments,
            ViewHelpers::CustomerBillingAccounts,
            ViewHelpers::CompletedAndExpectedPayments,

            # deprecated views
            ViewHelpers::CareerProfileSearchHelpers, # no longer a view.  Now a table that is regularly updated

        ]
    end

    def self.klass_for_view_name(view_name)
        view_name_without_schema = view_name.split(".").last
        "#{self.name}::#{view_name_without_schema.camelize}".constantize
    end

end