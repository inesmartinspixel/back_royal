# == Schema Information
#
# Table name: s3_assets
#
#  id                :uuid             not null, primary key
#  created_at        :datetime
#  updated_at        :datetime
#  file_file_name    :string(255)
#  file_content_type :string(255)
#  file_file_size    :integer
#  file_updated_at   :datetime
#  directory         :string(255)
#  styles            :text
#  file_fingerprint  :string(255)
#  dimensions        :json
#  formats           :json
#  source_url        :text
#

# I haven't tested any of this because I don't know how to mock out the file stuff
# create one of these by defining:
# * "file" - either an url or an uploaded file
# * "directory" - the name of the directory in s3.  File will be put into "BUCKET/DIRECTORY/..."
# * "styles" - for images, a hash of formatting directions.  See https://github.com/thoughtbot/paperclip/wiki/Thumbnail-Generation
class S3Asset < ApplicationRecord

    default_scope -> {order(:created_at)}

    #attr_accessible :file, :directory, :styles

    Paperclip.interpolates(:directory) do |attachment, style|
        attachment.instance.formatted_directory
    end

    has_attached_file :file,
        :styles => Proc.new { |attachment|
            attachment.instance.decoded_styles
        },
        :path => ":directory/:fingerprint/:style/:fingerprint.:extension",
        :processors => [:bulk],
        :s3_credentials => {
            :bucket => ENV['AWS_UPLOADS_BUCKET']
        },
        :s3_host_alias => ENV['AWS_UPLOADS_BUCKET']

    after_post_process -> { Paperclip::BulkQueue.process(file) }

    validates_attachment_content_type :file, :content_type => %w(
        text/plain
        text/x-python
        image/jpeg
        image/jpg
        image/pjpeg
        image/pipeg
        image/png
        image/svg+xml
        image/gif
        application/pdf
        application/json
        application/msword
        application/zip
        application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
        application/vnd.openxmlformats-officedocument.presentationml.presentation
        application/vnd.openxmlformats-officedocument.wordprocessingml.document) # http://stackoverflow.com/a/4212908/1747491

    validates_attachment_size :file, less_than: 3.megabytes

    before_save :set_formats

    # we need to make sure that the styles attribute is set before the file
    # attribute.  Otherwise paperclip will go ahead ans save the file to s3
    # without using the styles we want it to.
    def self.new(attrs = {})
        file = attrs.delete('file') || attrs.delete(:file)

        file_wrapper = nil
        unless (Rails.env == "test" && file.is_a?(String))
            file_wrapper = Paperclip.io_adapters.for(file, { hash_digest: Digest::SHA256 })

            # svgs do not need multiple sizes, so remove any styles request
            # that was passed into #new
            if file_wrapper.content_type == 'image/svg+xml'
                attrs['styles'] = attrs[:styles] = '{}'
            end
        end

        instance = super(attrs)
        instance.file = file_wrapper

        # add dimensions support for image-based content_types
        instance.add_dimensions(file_wrapper)

        instance
    end

    def formatted_directory
        # remove leading and trailing slashes
        self.directory.gsub(/\/$/, "").gsub(/^\//, "")
    end

    def set_formats
        hash = {}
        decoded_styles.each do |key, val|
            hash[key] = { url: self.file.url(key.to_sym) }
            if key.match(/\d+x\d+/)
                hash[key].merge!({
                    width: key.to_s.split("x")[0].to_i,
                    height: key.to_s.split("x")[1].to_i,
                })
            end
        end
        hash['original'] = {url: self.file.url}
        self.formats = hash
    end

    # I tried to convert this field to json now that we're on rails 4,
    # but we still have the issue of paperclip not support string
    # keys (https://github.com/thoughtbot/paperclip/issues/1027), so
    # we would still need this method.  That being the case, it didn't
    # seem worth to downtime
    def decoded_styles
        ActiveSupport::JSON.decode(self.styles || "{}").symbolize_keys
    end

    def add_dimensions(file)

        # If Content-Type is an SVG
        if file.content_type == 'image/svg+xml'

            # read from new file stream to prevent inadvertent closing of paperclip-expected stream
            svg_content =  File.read(file.path)
            xml_doc  = Nokogiri::XML(svg_content)

            self.dimensions = {
                width: xml_doc.children[0].get_attribute('width').to_f,
                height: xml_doc.children[0].get_attribute('height').to_f
            }

        # If Content-Type is any other image
        elsif %w(image/jpeg image/jpg image/png image/gif).include? file.content_type
            geometry = Paperclip::Geometry.from_file(file)

            self.dimensions = {
                width: geometry.width,
                height: geometry.height
            }
        end

        self
    end

    def as_json(options = {})
        {
            id: id,
            file_file_name: file_file_name,
            file_fingerprint: file_fingerprint,
            formats: formats ? formats.stringify_keys : {},
            dimensions: dimensions,
            file_updated_at: file_updated_at
        }
    end

end
