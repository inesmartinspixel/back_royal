module Mailboxer::ConversationExtensions

    extend ActiveSupport::Concern

    included do
        prepend(Prepend)

        has_many :descending_messages, -> { reorder('created_at desc') }, :dependent => :destroy, :class_name => "Mailboxer::Message"
        has_one :hiring_relationship, autosave: false

        belongs_to :candidate_position_interest, optional: true

        attr_accessor :new_messages
    end

    module ClassMethods

        def includes_needed_for_json_eager_loading
            [
                {:descending_messages => {:receipts => :receiver}},
                :candidate_position_interest
            ]
        end

        # NOTE: this will actually update an existing conversation
        # if there already is one between these two people
        #
        # We include the hiring relationship as an argument here because
        # we need to check the saved_changes on it in order to build the
        # payload for some message-related events
        #
        # In the wild (at least right now), we always expect a hiring_relationship,
        # but it's maybe more future-proof and prevents having to change a million specs
        # if we leave it as optional.
        def create_from_hash!(hash, sender, hiring_relationship = nil)
            instance = nil
            # Automatically retries in the event of ActiveRecord::PreparedStatementCacheExpired
            RetriableTransaction.transaction do
                hash = hash.unsafe_with_indifferent_access

                messages = hash['messages']
                unless messages&.size == 1
                    raise "Can only start a conversation with a single message"
                end

                message_hash = messages.first
                validate_message_sender(message_hash, sender)

                recipients = get_recipients_from_hash(hash['recipients']) - [sender]

                if recipients.size != 1
                    # we have not thought about how to ensure no duplicate conversations
                    # with more than 2 participants
                    raise NotImplementedError.new("We only support a single recipient")
                end

                # check to see if there is already a conversation. If so,
                # switch to using update_from_hash!
                if conversation = get_conversation_for_sender_and_recipient(sender, recipients.first)
                    conversation.hiring_relationship = hiring_relationship if hiring_relationship
                    hash = conversation.as_json(user_id: sender.id)
                    hash['messages'] << message_hash
                    return update_from_hash!(hash, sender)
                else
                    if message_hash['body'].blank?
                        raise "Message body should not be blank"
                    end

                    receipt = sender.send_message(recipients, message_hash['body'], hash['subject'])
                    receipt.message.update_attribute(:metadata, message_hash['metadata'])
                    instance = receipt.conversation
                    instance.update_attribute(:candidate_position_interest_id, hash['candidate_position_interest_id']) if hash.key?('candidate_position_interest_id')
                    instance.new_messages = [receipt.message]
                    instance.hiring_relationship = hiring_relationship if hiring_relationship
                    instance.log_create_events
                    log_events_for_new_messages(sender, [receipt.notification], hiring_relationship)
                end
            end

            instance
        end

        def update_from_hash!(hash, sender, hiring_relationship = nil)
            instance = nil
            # Automatically retries in the event of ActiveRecord::PreparedStatementCacheExpired
            RetriableTransaction.transaction do
                hash = hash.unsafe_with_indifferent_access
                id = hash.fetch(:id)
                instance = find_by_id(id)

                if instance.nil?
                    raise "No instance found"
                end

                instance.new_messages = instance.merge_hash(hash, sender)
                instance.hiring_relationship = hiring_relationship if hiring_relationship
                instance.candidate_position_interest_id = hash['candidate_position_interest_id'] if hash.key?('candidate_position_interest_id')

                log_events_for_new_messages(sender, instance.new_messages, hiring_relationship)
                instance.save!
            end

            instance

        end

        def create_or_update_from_hash!(hash, sender, hiring_relationship = nil)
            hash = hash.unsafe_with_indifferent_access
            id = hash[:id]
            if id.nil?
                create_from_hash!(hash, sender, hiring_relationship)
            else
                update_from_hash!(hash, sender, hiring_relationship)
            end
        end

        def log_events_for_new_messages(sender, messages, hiring_relationship)

            # These need to
            # be returned because they need to be logged to segment
            # so that we can identify users who have never sent a message
            message_sent_events = messages.map { |message| message.log_create_events(hiring_relationship) }.flatten

            # NOTE: These are message_received events.  They need to be sent to customer.io
            # as well in order
            # to support "someone sent you a message" email notifications.
            # These events must be logged from here, rather than from an
            # after_create in receipt_extensions ...
            #
            # 1. ... so that we can make sure that all the participants
            #       are set and we can get a reference to the hiring_relationship
            message_received_events = messages.map do |message|
                # there is a receipt for each recipient created, as well as one
                # for the sender.  Skip the one from the sender.
                message.receipts.reject { |r| r.receiver_id == sender.id }.map { |receipt|
                    receipt.log_create_events(hiring_relationship)
                }.flatten
            end.flatten.compact

            (message_sent_events + message_received_events).each(&:log_to_external_systems)
        end

        def validate_message_sender(message_hash, allowed_sender)
            klass = Mailboxer.recipient_type(message_hash['sender_type'])
            sender = klass.find(message_hash['sender_id'])
            unless allowed_sender == sender
                raise Mailboxer::DisallowedError.new("Attempting to send message from a disallowed sender.")
            end
        end

        def get_recipients_from_hash(recipient_hashes)
            recipient_hashes.map do |recipient_hash|
                klass = Mailboxer.recipient_type(recipient_hash['type'])
                klass.find(recipient_hash['id'])
            end
        end

        def get_conversation_for_sender_and_recipient(sender, recipient)
            # https://github.com/mailboxer/mailboxer/issues/95
            Mailboxer::Conversation.participant(sender)
                .where('mailboxer_conversations.id in (?)', Mailboxer::Conversation.participant(recipient).collect(&:id)).first
        end

    end

    # we have to specially handle places where we want to override the
    # original method
    module Prepend

        # performance improvement related to preloading.
        # See https://github.com/mailboxer/mailboxer/issues/246 and #recipients in notification_extensions.rb
        def original_message
            @original_message ||= descending_messages.last
        end
    end

    def hiring_relationship_role(user_id)
        if hiring_relationship.nil?
            return nil
        elsif hiring_relationship.hiring_manager_id == user_id
            return 'hiring_manager'
        elsif hiring_relationship.candidate_id == user_id
            return 'candidate'
        end

        hiring_team_id =  User.where(id: user_id).pluck('hiring_team_id').first
        if hiring_team_id && hiring_team_id == hiring_relationship.hiring_manager.hiring_team_id
            return 'hiring_teammate'
        else
            raise RuntimeError.new('Cannot determine role')
        end
    end

    def log_create_events
        participant_ids.each do |user_id|
            Event.create_server_event!(
                SecureRandom.uuid,
                user_id,
                "mailboxer:conversation_created",
                {
                    conversation_id: id,
                    hiring_relationship_id: hiring_relationship && hiring_relationship.id,
                    participant_ids: participant_ids - [user_id],
                    hiring_relationship_role: hiring_relationship_role(user_id),
                    just_accepted_relationship: hiring_relationship && hiring_relationship.just_accepted?
                }
            )
        end
    end

    # Hmmm.  I'm not sure what would happen here if not all recipients where intances
    # of User.  But, for now at least, they are
    def participant_ids
       @participant_ids ||= participants.pluck('id')
    end

    def as_json(options = {})
        if options[:format_for_update_response]
            return {
                "id" => id,
                "updated_at" => updated_at.to_timestamp
            }
        end

        {
            "id" => id,
            "created_at" => created_at.to_timestamp,
            "updated_at" => updated_at.to_timestamp,
            "subject" => subject,
            "recipients" => recipients.map { |recipient|
                {
                    "type" => Mailboxer.key_for_recipient_type(recipient.class),
                    "id" => recipient.id,
                    "display_name" => recipient.is_a?(User) ? recipient.name : nil,
                    "avatar_url" => recipient.is_a?(User) ? recipient.avatar_url_with_fallback : nil,
                }
            },
            "messages" => descending_messages.map { |message| message.as_json(options) },
            "candidate_position_interest_id" => self.candidate_position_interest_id,

            # Rather than sending down the entire candidate_position_interest, just send down
            # the cover_letter attached to the candidate_position_interest since that's all
            # that's needed in the client for now.
            "cover_letter" => self.candidate_position_interest&.cover_letter
        }
    end

    def merge_hash(hash, allowed_sender)

        if !self.participants.include?(allowed_sender)
            # this adds receipts for the new user and for
            # all the existing messages
            self.add_participant(allowed_sender)
            self.receipts.where(receiver_id: allowed_sender.id).update_all(
                mailbox_type: 'inbox', # I don't know why Mailboxer doesn't set this
                is_read: true # Assume all messages are read initially
            )

            # this is a stupid hack, but it is needed to get the list
            # of participants to refresh
            self.instance_variable_set(:@original_message, nil)
        end

        new_messages = []
        (hash['messages'] || []).map do |message_hash|
            if message_hash['id'].nil?
                self.class.validate_message_sender(message_hash, allowed_sender)
                receipt = allowed_sender.reply_to_conversation(self, message_hash['body'])
                receipt.message.update_attribute(:metadata, message_hash['metadata'])
                new_messages << receipt.message
            end

            raise "deprecated syntax" if message_hash['receipt'] && message_hash['receipts'].nil?

            if message_hash['receipts']
                receipt_hash = message_hash['receipts'].detect { |r| r['receiver_id'] == allowed_sender.id}
            end
            receipt_id = receipt_hash && receipt_hash['id']
            # When creating a new message, there will be a receipt_hash with no id, since the
            # client had to create it in order to have a valid object in the UI.  We do not support
            # any updates to this initial receipt. The message must be saved first, and then the receipt
            # updated later. (We can change this if we need to at some point)
            if receipt_id
                receipt = Mailboxer::Receipt.find_by_id(receipt_id)
                if receipt.nil?
                    raise "Receipt not found"
                end
                if receipt.receiver_id != allowed_sender.id
                    raise Mailboxer::DisallowedError.new("Attempting to update receipt for another user")
                end

                if receipt.is_read? != receipt_hash['is_read']
                    receipt_hash['is_read'] ? receipt.mark_as_read : receipt.mark_as_unread
                end

                if receipt.trashed? != receipt_hash['trashed']
                    receipt_hash['trashed'] ? receipt.move_to_trash : receipt.untrash
                end

                if receipt.mailbox_type != receipt_hash['mailbox_type']
                    receipt.send(:"move_to_#{receipt_hash['mailbox_type']}")
                end

            end

        end

        new_messages
    end

    def new_message
        if new_messages && new_messages.size > 1
            err = RuntimeError.new("new_message called when new_messages.size > 1")
            Rails.env.development? ? raise(err) : Raven.capture_exception(err)
        end

        new_messages && new_messages.last
    end

    def last_message_at
        descending_messages.first.updated_at
    end

    def contains_message_from_user?(user_id)
        !!self.messages.detect { |message| message.sender_id == user_id }
    end

end