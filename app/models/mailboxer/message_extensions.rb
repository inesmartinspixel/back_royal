module Mailboxer::MessageExtensions

    extend ActiveSupport::Concern

    def as_json(options = {})
        hash = {
            "id" => id,
            "sender_id" => sender_id,
            "sender_type" => sender_type,
            "body" => body,
            "metadata" => metadata,
            "updated_at" => updated_at.to_timestamp,
            "created_at" => created_at.to_timestamp
        }

        hash['receipts'] = self.receipts.map do |receipt|
            receipt.attributes.slice(*%w(
                id
                receiver_id
                is_read
                trashed
                deleted
                mailbox_type
            ))
        end


        hash
    end

end