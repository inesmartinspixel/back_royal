module Mailboxer::NotificationExtensions
    extend ActiveSupport::Concern

    included do |klass|
        prepend(Prepend)
        delegate :hiring_relationship, :to => :conversation
    end

    # we have to specially handle places where we want to override the
    # original method
    module Prepend

        # performance improvement related to preloading.
        # See https://github.com/mailboxer/mailboxer/issues/246 and #original_message in conversation_extensions.rb
        def recipients
            return Array.wrap(@recipients) unless @recipients.blank?
            if receipts.loaded?
                @recipients = receipts.map { |receipt| receipt.receiver }
            else
                @recipients = receipts.includes(:receiver).map { |receipt| receipt.receiver }
            end
        end
    end

    def should_log_events?
        # this is always true (for now)
        sender_type == 'User'
    end

    # in this case, the user who logs the event is the sender
    def hiring_relationship_role
       conversation.hiring_relationship_role(sender_id)
    end

    # see note in ConversationExtensions#create_from_hash about how
    # hiring_relationship is currently always expected
    def log_create_events(hiring_relationship)
        if should_log_events?
            Event.create_server_event!(
                SecureRandom.uuid,
                sender_id,
                "mailboxer:message_sent",
                log_info.merge(
                    just_accepted_relationship: hiring_relationship && hiring_relationship.just_accepted?,
                    hiring_manager_name: hiring_relationship && hiring_relationship.hiring_manager.preferred_name,
                    hiring_manager_company_name: hiring_relationship && hiring_relationship.hiring_manager.company_name,
                    candidate_name: hiring_relationship && hiring_relationship.candidate.preferred_name
                )
            )
        end
    end

    def log_info
        {
            notification_id: id,
            recipient_ids: recipients.pluck('id').sort - [sender_id],
            hiring_relationship_id: hiring_relationship && hiring_relationship.id,
            hiring_relationship_role: hiring_relationship_role
        }
    end
end