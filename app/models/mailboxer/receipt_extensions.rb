module Mailboxer::ReceiptExtensions

    extend ActiveSupport::Concern

    included do
        delegate :hiring_relationship, :to => :conversation
        delegate :sender, :to => :notification
        prepend(Prepend)
    end

    module Prepend

        def mark_as_read
            # only log events for messages from other people.  It's not interesting
            # if this message is from myself
            log_marked_as_read_event if receiver_id != notification.sender_id
            super
        end
    end

    def should_log_events?
        # this is always true (for now)
        receiver_type == 'User'
    end

    # see note in ConversationExtensions#create_from_hash about how
    # hiring_relationship is currently always expected
    def log_create_events(hiring_relationship = nil)
        if should_log_events?
            Event.create_server_event!(
                SecureRandom.uuid,
                receiver_id,
                "mailboxer:message_received",
                log_info.merge({
                    message: message_body,
                    just_accepted_relationship: hiring_relationship && hiring_relationship.just_accepted?
                })
            )
        end
    end

    def log_marked_as_read_event
        if should_log_events?
            Event.create_server_event!(
                SecureRandom.uuid,
                receiver_id,
                "mailboxer:message_marked_as_read",
                log_info
            )
        end
    end

    def message_body
        message && message.body
    end

    def log_info
        {
            receipt_id: id,
            sender_id: sender.id,
            hiring_relationship_id: hiring_relationship && hiring_relationship.id,
            hiring_relationship_role: conversation && conversation.hiring_relationship_role(receiver_id),
            sender_hiring_relationship_role: conversation && conversation.hiring_relationship_role(sender.id),
            sender_avatar_url: sender.avatar_url_with_fallback,
            sender_name: sender.preferred_name,
            sender_company_logo_url: sender.company_logo_url,
            sender_company_name: sender.company_name,
            hiring_manager_name: hiring_relationship && hiring_relationship.hiring_manager.preferred_name,
            hiring_manager_company_name: hiring_relationship && hiring_relationship.hiring_manager.company_name,
            candidate_name: hiring_relationship && hiring_relationship.candidate.preferred_name
            # we are not currently using is_delivered and delivery_method (I think),
            # but might want to add info about that in the future.
            #
            # Not tracking trashed and deleted because that stuff only happens
            # at the conversation level in our UI
        }
    end

end