module CrudFromHashHelper

    extend ActiveSupport::Concern
    class OldVersion < RuntimeError; end
    class NoDateStampProvided < RuntimeError; end

    module ClassMethods

        def prepare_update_from_hash(hash, meta)
            hash = hash.unsafe_with_indifferent_access
            meta ||= {}
            meta = meta.unsafe_with_indifferent_access

            instance = find_by_id(hash[:id])

            if instance.nil?
                raise ActiveRecord::RecordNotFound.new("Couldn't find #{self.name} with id=#{hash[:id]}")
            end

            check_updated_at_in_update_from_hash(instance, hash)

            [hash, meta, instance]
        end

        def check_updated_at_in_update_from_hash(instance, hash)
            updated_at = hash.delete('updated_at') # we are now using this instead of version for version checking

            # we have the client pass in a last_updated_at date so that we can tell whether
            # someone has updated out from under them
            unless updated_at
                raise NoDateStampProvided.new("No updated_at provided in #{hash.inspect}")
            end

            if instance.updated_at.to_timestamp > updated_at.to_timestamp
                raise OldVersion.new("Cannot save an instance that is not the latest version, probably someone else has saved while you were editing. Please refresh and try again. ")
            end

        end
    end

end