# == Schema Information
#
# Table name: deferral_links
#
#  id                         :uuid             not null, primary key
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  user_id                    :uuid             not null
#  expires_at                 :datetime         not null
#  used                       :boolean          default(FALSE), not null
#  from_cohort_application_id :uuid             not null
#  to_cohort_application_id   :uuid
#

class DeferralLink < ApplicationRecord

    belongs_to :user
    belongs_to :from_cohort_application, class_name: 'CohortApplication'
    belongs_to :to_cohort_application, class_name: 'CohortApplication', optional: true

    validates_presence_of :user_id
    validates_presence_of :expires_at

    validate :validate_no_existing_active_link, on: [:create, :update]
    validate :validate_approriate_status, on: [:create, :update]

    def self.includes_needed_for_json_eager_loading
        [:from_cohort_application, :to_cohort_application, {:user => [:cohort_applications]}]
    end

    def active?
        # If the link is:
        #   - Not used
        #   - Not expired
        #   - The application that triggered it is still one of valid_deferral_statuses
        #   - The application that triggered it is still the user's last_application
        #       - (Prevents a link from being active if the application is manually deferred or something)
        !self.used? && self.expires_at > Time.now && 
            CohortApplication.valid_deferral_statuses.include?(from_cohort_application.status) &&
            self.from_cohort_application_id == self.user.last_application.id
    end

    def upcoming_cohorts
        # Get cohorts that start after now, and up to a year after the start_date of 
        # the cohort that generated the link
        @upcoming_cohorts ||= Cohort.all_published
            .where(program_type: self.from_cohort_application.program_type)
            .where('cohorts.start_date > ?', Time.now.utc)
            .where('cohorts.start_date > ?', self.from_cohort_application.cohort.start_date)
            .where('cohorts.start_date <= ?', self.from_cohort_application.cohort.start_date + 1.year)
            .to_a

        @upcoming_cohorts
    end

    def as_json(options = {})
        fields = options[:fields]
        json = super(options)

        if fields&.include?('active')
            json = json.merge({'active' => self.active?})
        end

        if fields&.include?('from_cohort_application')
            json = json.merge({'from_cohort_application' => self.from_cohort_application.as_json})
        end

        json
    end

    private def validate_no_existing_active_link
        if DeferralLink.includes(*DeferralLink.includes_needed_for_json_eager_loading)
            .where.not(id: self.id)
            .where(user_id: self.user_id)
            .any? { |l| l.active? }
            errors.add(:user, 'already has an active deferral link')
        end
    end

    private def validate_approriate_status
        if self.from_cohort_application.present? &&
            !CohortApplication.valid_deferral_statuses.include?(self.from_cohort_application.status)
            errors.add(:user, 'does not have an appropriate application status to create a deferral link')
        end
    end
end
