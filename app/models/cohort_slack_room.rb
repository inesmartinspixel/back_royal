# == Schema Information
#
# Table name: cohort_slack_rooms
#
#  id                    :uuid             not null, primary key
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  cohort_id             :uuid             not null
#  title                 :text             not null
#  url                   :text             not null
#  admin_token           :text
#  bot_user_access_token :text
#

class CohortSlackRoom < ApplicationRecord

    belongs_to :cohort, :primary_key => 'id', :foreign_key => 'cohort_id'

    validates_presence_of :cohort_id
    validates_presence_of :title
    validates_presence_of :url

    before_save :ensure_slack_room_assignment_job

    after_create :ensure_accepted_users_have_slack_rooms
    after_save :verify_setup_slack_channels_job, if: Proc.new { bot_user_access_token && saved_change_to_bot_user_access_token? }

    def ensure_slack_room_assignment_job
        cohort.ensure_slack_room_assignment_job
    end

    def ensure_accepted_users_have_slack_rooms
        # see https://trello.com/b/GYcsNzAb
        #
        # Dealing with this issue
        # 1. cohort is created with no slack rooms
        # 2. student is accepted into the cohort, with no slack room assigned
        # 3. slack room is added to the cohort
        # 4. Now the student is invalid, because they are accepted but they have no slack room
        #
        # Tested in cohort_spec.rb: "ensure slack rooms for accepted users"
        cohort.ensure_accepted_users_have_slack_rooms
    end

    def verify_setup_slack_channels_job
        Delayed::Job.where(
            queue: Cohort::SetupSlackChannelsJob.queue_name,
            identifier: self.id,
            locked_at: nil,
            failed_at: nil
        ).destroy_all

        Cohort::SetupSlackChannelsJob.perform_later(self.id) if self.bot_user_access_token
    end
end
