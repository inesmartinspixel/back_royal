
class CertificateGenerator

    # Colors
    COLOR_WHITE="#FFFFFF"

    # Font Config - hopefully doesn't need to change
    FONT_BOLD="ProximaNovaSoftBold"
    FONT_MEDIUM="ProximaNovaSoftMedium"
    FONT_REGULAR="ProximaNovaSoftRegular"
    INTERNATIONAL_FALLBACK="ArialUnicode"
    FONTSIZE_SMALL_LABELS="13"
    FONTSIZE_LARGE_LABELS="56"

    # Dimensions
    CERTIFICATE_DIMENSIONS="1496x980"
    USERNAME_DIMENSIONS="1234x72"
    COURSENAME_DIMENSIONS="1258x112"
    LESSONS_DIMENSIONS="136x32"
    LESSONTOTAL_DIMENSIONS="140x114"

    # Positioning - should only change if certificate changes dimensions
    USERNAME_POSITION="+132+764"
    COURSENAME_POSITION="+120+554"
    DATE_POSITION="+136+154"
    LESSONS_POSITION="+1240+154"
    YEAR_POSITION="+134+182"
    LESSONTOTAL_POSITION="+1232+182"

    # usage: CertificateGenerator.generate_image(user, {
    #    user_name: "JACK\nBLACK",
    #    course_name: "THE SCHOOL OF ROCK",
    #    date: "05 DEC",
    #    year: "2014",
    #    lessons_label: "LESSONS",
    #    lesson_total: 6,
    # })
    def self.generate_image(user, opts)

        if opts[:exam]
            certificate_base_image = user.is_quantic_user? ?
                "vendor/images/_minified/certificate_without_data_turquoise_quantic.png" : "vendor/images/_minified/certificate_without_data_turquoise.png"
            color_sub_text="#158F8F"
            color_primary_text="#106E6E"
        else
            certificate_base_image = user.is_quantic_user? ?
                "vendor/images/_minified/certificate_without_data_gold_quantic.png" : "vendor/images/_minified/certificate_without_data_gold.png"
            color_sub_text="#B78800"
            color_primary_text="#896600"
        end

        asset_output_path = "/tmp/output_certificate_#{rand}.png"

        # todo: eventually use rmagick api for this instead of shelling out to imagemagick?
        system("convert",
            "-respect-parenthesis",
            "-density", "144",
            "-background", "transparent",
            "(", "#{certificate_base_image}", ")",
            "(", "-fill", COLOR_WHITE,
                 "-font", opts[:user_name].ascii_only? ? FONT_MEDIUM : INTERNATIONAL_FALLBACK,
                 "-size", USERNAME_DIMENSIONS,
                 "-gravity", "center",
                 "caption:#{opts[:user_name].upcase}",
                 "-repage", USERNAME_POSITION,
            ")",
            "(", "-fill", color_primary_text,
                 "-font", opts[:course_name].ascii_only? ? FONT_BOLD : INTERNATIONAL_FALLBACK,
                 "-size", COURSENAME_DIMENSIONS,
                 "-gravity", "center",
                 "caption:#{opts[:course_name].upcase}",
                 "-repage", COURSENAME_POSITION,
            ")",
            "(", "-fill", color_sub_text,
                 "-font", opts[:date].ascii_only? ? FONT_MEDIUM : INTERNATIONAL_FALLBACK,
                 "-pointsize", FONTSIZE_SMALL_LABELS,
                 "label:#{opts[:date]}",
                 "-repage", DATE_POSITION,
            ")",
            "(", "-fill", color_sub_text,
                 "-font", opts[:lessons_label].ascii_only? ? FONT_MEDIUM : INTERNATIONAL_FALLBACK,
                 "-pointsize", FONTSIZE_SMALL_LABELS,
                 "-size", LESSONS_DIMENSIONS,
                 "-gravity", "center",
                 "label:#{opts[:lessons_label]}",
                 "-repage", LESSONS_POSITION, ")",
            "(", "-fill", color_sub_text,
                 "-font",  FONT_BOLD,
                 "-pointsize", FONTSIZE_LARGE_LABELS,
                 "label:#{opts[:year]}",
                "-repage", YEAR_POSITION,
            ")",
            "(", "-fill", color_sub_text,
                 "-font", FONT_BOLD,
                 "-pointsize", FONTSIZE_LARGE_LABELS,
                 "-size", LESSONTOTAL_DIMENSIONS,
                 "-gravity", "center",
                 "label:#{opts[:lesson_total].to_s.rjust(2, "0")}",
                 "-repage", LESSONTOTAL_POSITION,
            ")",
            "-layers", "flatten",
            asset_output_path)

        # todo: check status code and handle failure cases
        return asset_output_path

    end

    def self.image_formats
        {
            "2100x825"  => '2100x825>',
            "1400x550"  => '1400x550>',
            "700x275"   => '700x275>'
        }
    end

    def self.generate_asset(user, opts)
        raise "mock me in specs" if Rails.env == 'test'
        asset_path = CertificateGenerator.generate_image(user, opts)

        new_certificate = S3Asset.new({
            :file => File.open(asset_path),
            :directory => "images/",
            :styles => image_formats.to_json
        })

        new_certificate.save!

        new_certificate
    ensure
        # cleanup temporary image file after upload to s3
        File.delete(asset_path) unless asset_path.nil?
    end

end
