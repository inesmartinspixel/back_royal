# == Schema Information
#
# Table name: events
#
#  id                                     :string(256)      not null, primary key
#  user_id                                :string(256)      not null
#  event_type                             :string(255)      not null
#  created_at                             :datetime         not null
#  updated_at                             :datetime         not null
#  estimated_time                         :datetime         not null
#  client_reported_time                   :datetime         not null
#  hit_server_at                          :datetime         not null
#  total_buffered_seconds                 :float            not null
#  peformance_to_eval_async               :float            not null
#  peformance_to_timeout                  :float            not null
#  available_answer_ids                   :string(938)      not null
#  candidate_ids                          :string(2048)     not null
#  candidate_status                       :string(29)       not null
#  challenge_responses                    :string(1094)     not null
#  client_local_time                      :string(36)       not null
#  client_utc_time                        :string(36)       not null
#  event_errors                           :string(1361)     not null
#  feedback_reasons                       :string(80)       not null
#  hiring_manager_status                  :string(32)       not null
#  modals                                 :string(1646)     not null
#  orig_modal_keys                        :string(56)       not null
#  participant_ids                        :string(60)       not null
#  progress_options_to_filter_by          :string(62)       not null
#  recipient_ids                          :string(60)       not null
#  result_daily_lesson_config_ids         :string(10)       not null
#  result_stream_ids                      :string(5793)     not null
#  topics_to_filter_by                    :string(1436)     not null
#  card_changed                           :boolean          not null
#  card_data_saved                        :boolean          not null
#  cordova                                :boolean          not null
#  demo_mode                              :boolean          not null
#  editor_mode                            :boolean          not null
#  free_trial_set_to_cancel               :boolean          not null
#  lesson_complete                        :boolean          not null
#  lesson_stream_complete                 :boolean          not null
#  paid_subscription_set_to_cancel        :boolean          not null
#  partially_correct                      :boolean          not null
#  preview_mode                           :boolean          not null
#  set_to_cancel                          :boolean          not null
#  should_retry                           :boolean          not null
#  use_default_card                       :boolean          not null
#  candidate_count                        :integer          not null
#  chapter_index                          :integer          not null
#  client_version                         :integer          not null
#  conversation_id                        :integer          not null
#  created                                :integer          not null
#  duration_idle                          :float            not null
#  frame_index                            :integer          not null
#  notification_id                        :integer          not null
#  pending_webhooks                       :integer          not null
#  receipt_id                             :integer          not null
#  score                                  :float            not null
#  status                                 :string(384)      not null
#  total_frames                           :integer          not null
#  buffered_time                          :float            not null
#  client_offset_from_utc                 :float            not null
#  client_utc_timestamp                   :float            not null
#  duration_active                        :float            not null
#  duration_total                         :float            not null
#  next_period_amount                     :float            not null
#  revenue                                :float            not null
#  total                                  :float            not null
#  value                                  :float            not null
#  destination_params                     :string(2048)     not null
#  error                                  :string(225)      not null
#  params                                 :string(2048)     not null
#  performance                            :string(56)       not null
#  policy_config                          :string(263)      not null
#  request_config                         :string(477)      not null
#  score_metadata                         :string(57)       not null
#  selection_details                      :string(1134)     not null
#  server_error                           :string(633)      not null
#  since_activated                        :string(111)      not null
#  since_request_queued                   :string(141)      not null
#  since_request_sent                     :string(141)      not null
#  source_params                          :string(2048)     not null
#  lesson_passed                          :boolean          not null
#  user_subscription_type                 :string(96)       not null
#  action                                 :string(128)      not null
#  api_version                            :string(15)       not null
#  browser_major                          :string(36)       not null
#  browser_name                           :string(56)       not null
#  browser_version                        :string(36)       not null
#  build_commit                           :string(60)       not null
#  build_number                           :string(6)        not null
#  build_timestamp                        :string(48)       not null
#  campaign                               :string(48)       not null
#  candidate_id                           :string(54)       not null
#  caused_by_event                        :string(24)       not null
#  challenge_id                           :string(54)       not null
#  challenge_type                         :string(42)       not null
#  chapter_label                          :string(72)       not null
#  chapter_title                          :string(63)       not null
#  client                                 :string(32)       not null
#  cohort_id                              :string(54)       not null
#  component_id                           :string(54)       not null
#  content                                :string(512)      not null
#  currency                               :string(5)        not null
#  destination_directive                  :string(72)       not null
#  destination_path                       :string(2048)     not null
#  device_model                           :string(96)       not null
#  device_type                            :string(96)       not null
#  device_vendor                          :string(96)       not null
#  directive                              :string(72)       not null
#  editor_template                        :string(44)       not null
#  email                                  :string(192)      not null
#  feedback_additional_thoughts           :string(1224)     not null
#  formatted_free_trial_end_at            :string(27)       not null
#  frame_id                               :string(54)       not null
#  frame_play_id                          :string(54)       not null
#  hiring_manager_id                      :string(54)       not null
#  hiring_relationship_id                 :string(54)       not null
#  hiring_relationship_role               :string(21)       not null
#  image_component_id                     :string(48)       not null
#  ip_address                             :string(59)       not null
#  label                                  :string(512)      not null
#  launched_lesson_id                     :string(54)       not null
#  launched_stream_id                     :string(54)       not null
#  lesson_id                              :string(54)       not null
#  lesson_play_id                         :string(54)       not null
#  lesson_stream_id                       :string(54)       not null
#  lesson_stream_locale_pack_id           :string(54)       not null
#  lesson_stream_title                    :string(256)      not null
#  lesson_stream_version_id               :string(54)       not null
#  lesson_title                           :string(256)      not null
#  lesson_version_id                      :string(54)       not null
#  link                                   :string(2048)     not null
#  logged_in_user_id                      :string(54)       not null
#  message                                :string(65535)    not null
#  object                                 :string(8)        not null
#  os_name                                :string(64)       not null
#  os_version                             :string(36)       not null
#  page_load_id                           :string(54)       not null
#  path                                   :string(2048)     not null
#  plan_id                                :string(128)      not null
#  plan_name                              :string(32)       not null
#  playlist_id                            :string(54)       not null
#  provider                               :string(20)       not null
#  search_attempt_id                      :string(54)       not null
#  search_text                            :string(237)      not null
#  section                                :string(30)       not null
#  sender_avatar_url                      :string(2048)     not null
#  sender_company_logo_url                :string(2048)     not null
#  sender_company_name                    :string(192)      not null
#  sender_id                              :string(54)       not null
#  sender_name                            :string(36)       not null
#  service                                :string(12)       not null
#  sign_up_code                           :string(24)       not null
#  source_directive                       :string(72)       not null
#  source_path                            :string(2048)     not null
#  src                                    :string(1536)     not null
#  stream_locale_pack_id                  :string(54)       not null
#  subscription_id                        :string(54)       not null
#  subscription_status                    :string(32)       not null
#  summary_id                             :string(54)       not null
#  text_being_processed                   :string(1581)     not null
#  type                                   :string(128)      not null
#  url                                    :string(2048)     not null
#  user_agent                             :string(480)      not null
#  user_answer_to_display                 :string(128)      not null
#  utm_campaign                           :string(72)       not null
#  utm_content                            :string(72)       not null
#  utm_medium                             :string(72)       not null
#  utm_source                             :string(72)       not null
#  verified_by                            :string(54)       not null
#  viewed_stream_dashboard_id             :string(54)       not null
#  aborted                                :boolean          not null
#  card_added                             :boolean          not null
#  feedback_is_positive                   :boolean          not null
#  has_free_trial                         :boolean          not null
#  has_some_results                       :boolean          not null
#  livemode                               :boolean          not null
#  new_subscription_created               :boolean          not null
#  notify_candidate_of_new_request        :boolean          not null
#  notify_hiring_manager_of_new_match     :boolean          not null
#  result                                 :boolean          not null
#  server_event                           :boolean          not null
#  stripe_event                           :boolean          not null
#  candidate_name                         :string(128)      not null
#  time_to_activate                       :float            not null
#  time_to_finish                         :float            not null
#  selected_by_policy                     :string(64)       not null
#  onboarding_questions                   :string(2048)     not null
#  lesson_player_session_id               :string(256)      not null
#  lesson_editor_session_id               :string(256)      not null
#  client_session_id                      :string(256)      not null
#  program_type                           :string(128)      not null
#  period_index                           :integer          not null
#  required_courses_completed_actual      :integer          not null
#  required_courses_completed_target      :integer          not null
#  required_courses_entire_schedule       :integer          not null
#  enrolled                               :boolean          not null
#  found_with_search_id                   :string(53)       not null
#  stripe_plan_id                         :string(128)      not null
#  stripe_coupon_id                       :string(128)      not null
#  stripe_coupon_percent_off              :integer          not null
#  stripe_coupon_amount_off               :integer          not null
#  just_accepted_relationship             :boolean          not null
#  sender_hiring_relationship_role        :string(21)       not null
#  inviter_id                             :string(53)       not null
#  recipient_ids_new                      :string(1024)     not null
#  stripe_plan_ids                        :string(1024)     not null
#  send_retargeted_email_for_program_type :string(64)       not null
#  suggested_program_type                 :string(128)      not null
#  open_position_id                       :string(36)       not null
#  action_id                              :string(36)       not null
#  scholarship_level_name                 :string(16)       not null
#  attempts                               :integer          not null
#  get_map_id                             :string(53)       not null
#  filters_places                         :string(512)      not null
#  filters_keyword_search                 :string(256)      not null
#  filters_student_network_looking_for    :string(256)      not null
#  filters_student_network_interests      :string(256)      not null
#  filters_industries                     :string(256)      not null
#  filters_alumni                         :boolean          not null
#  filters_program_type                   :string(128)      not null
#  filters_cohort_id                      :string(53)       not null
#  education_level                        :string(53)       not null
#  area_of_study                          :string(53)       not null
#  work_experience                        :string(15)       not null
#  role                                   :string(53)       not null
#  idology_verification_id                :string(53)       not null
#  verified                               :boolean          not null
#  salary                                 :string(53)       not null
#  captured                               :boolean          not null
#  charge_id                              :string(53)       not null
#  failure_code                           :string(53)       not null
#  outcome                                :string(2048)     not null
#  dumped_at                              :datetime         not null
#  signable_document_id                   :string(36)       not null
#  external_position_id                   :string(256)      not null
#  external_position_url                  :string(2048)     not null
#  external_position_title                :string(256)      not null
#  external_position_industry             :string(256)      not null
#  external_position_location             :string(256)      not null
#  external_position_company_name         :string(256)      not null
#  cf_country_code                        :string(3)        not null
#  current_step_index                     :integer          not null
#  current_step_title                     :string(20)       not null
#  has_scrolled                           :boolean          not null
#  just_used_deferral_link                :boolean          not null
#  gl                                     :boolean          not null
#  recipient_id                           :string(36)       not null
#  cohort_application_id                  :string(36)       not null
#  follow_up_attempt                      :boolean          not null
#

class RedshiftEvent < ApplicationRecord
    self.table_name = 'events'
    self.primary_key = 'id'

    establish_connection REDSHIFT_DB_CONFIG

    def self.sensitive_values
        RedshiftEvent.connection_config.map { |k,v| ["redshift_event_#{k}", v] }.to_h.
        merge(Event.connection_config.map { |k,v| ["event_#{k}", v] }.to_h)
    end


    def self.sanitized_error_message(err)
        sanitized = "#{err.message}"
        self.sensitive_values.each do |key, val|
            sanitized.gsub!(val.to_s, "#{key.upcase}_HIDDEN") unless val.to_s.blank?
        end
        sanitized
    end


    private

        # Redshift events don't have indexes on the id column, which causes simple UPDATE statements to perform
        # a full table scan, resulting in a very slow update operation. To speed up the UPDATE operation we also
        # supply the event_type and user_id to the UPDATE statement. Unlike the id column, the event_type and
        # user_id columns act as sort keys when querying Redshift, resulting in faster database operations.
        # NOTE: Overridden from persistence.rb in activerecord gem.
        #
        # So rather than:
        #
        #       UPDATE events SET foo = 'foo' WHERE events.id = 'some_id';
        #
        # The statement becomes optimized as:
        #
        #       UPDATE events SET foo = 'bar' WHERE events.event_type = 'some_event_type' AND events.user_id = 'some_user_id' AND events.id = 'some_id';
        #
        def _update_row(attribute_names, attempted_action = "update")
            self.class._update_record(
                attributes_with_values(attribute_names),
                {
                    "event_type" => self.respond_to?(:event_type) && self.event_type || nil,
                    "user_id" => self.respond_to?(:user_id) && self.user_id || nil,
                    self.class.primary_key => id_in_database
                }.delete_if { |key, value| value.nil? }
            )
        end
end
