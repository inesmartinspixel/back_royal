module HiringTeam::SubscriptionConcern

    extend ActiveSupport::Concern

    included do
        # this is tested in ensure_stripe_customer_current spec in subscription_spec.rb
        after_save :ensure_stripe_customer_current, if: :saved_change_to_owner_id?
    end

    module ClassMethods

        def stripe_statement_descriptor
            'Smartly'
        end

        # this is used in ensure_stripe_products_and_plans and in generating stripe_plans
        def stripe_product_config
            @stripe_product_config ||= {
                hiring_unlimited: {
                    name: 'Smartly Hiring (unlimited)',
                    metadata: {
                        available_for_hiring_plan: HiringTeam::HIRING_PLAN_LEGACY
                    },
                    plans: {
                        hiring_unlimited_300: {
                            nickname: 'Hiring Unlimited $300',
                            amount: 300 * 100,
                            billing_scheme: "per_unit",
                            currency: "usd",
                            interval: "month",
                            interval_count: 1,
                            usage_type: 'licensed'
                        }
                    }
                },

                hiring_per_match: {
                    name: 'Smartly Hiring',
                    unit_label: 'match',
                    metadata: {
                        available_for_hiring_plan: HiringTeam::HIRING_PLAN_LEGACY
                    },
                    plans: {
                        hiring_per_match_20: {
                            nickname: 'Hiring Per Match $20',
                            amount: 20 * 100,
                            billing_scheme: "per_unit",
                            currency: "usd",
                            interval: "month",
                            interval_count: 1,
                            usage_type: 'metered'
                        }
                    }
                },

                hiring_unlimited_w_sourcing: {
                    name: 'Smartly Talent',
                    metadata: {
                        available_for_hiring_plan: HiringTeam::HIRING_PLAN_UNLIMITED_WITH_SOURCING
                    },
                    plans: {
                        hiring_unlimited_w_sourcing_249: {
                            nickname: 'Hiring Unlimited w/ Sourcing',
                            amount: 249 * 100,
                            billing_scheme: "per_unit",
                            currency: "usd",
                            interval: "month",
                            interval_count: 1,
                            usage_type: 'licensed',
                            trial_period_days: 14
                        }
                    }
                },

                hiring_pay_per_post: {
                    name: 'Smartly Talent (Job Post)',
                    metadata: {
                        available_for_hiring_plan: HiringTeam::HIRING_PLAN_PAY_PER_POST
                    },
                    plans: {
                        hiring_pay_per_post_89: {
                            nickname: 'Hiring Per Post $89',
                            amount: 89 * 100,
                            billing_scheme: "per_unit",
                            currency: "usd",
                            interval: "month",
                            interval_count: 1,
                            usage_type: 'licensed'
                        }
                    }
                }
            }
        end

        # this is sent down in the json in HiringTeam.  It's maybe a little
        # backwards to flip the order here and shove the products into each plan,
        # but it's similar to what we're already doing for cohort plans, so maybe
        # that's a good thing?
        def stripe_plans
            unless defined? @stripe_plans
                @stripe_plans = stripe_product_config.map do |product_id, product_config|
                    product_config[:plans].map do |plan_id, plan_config|
                        entry = plan_config.clone
                        entry[:id] = plan_id.to_s
                        entry[:product_id] = product_id.to_s
                        product_config.each do |key, val|
                            next if key == :plans
                            entry[:"product_#{key}"] = val
                        end
                        entry
                    end
                end.flatten
            end
            @stripe_plans
        end

        def stripe_plan(stripe_plan_id)
            stripe_plans.detect { |plan| plan[:id] == stripe_plan_id}
        end

        def stripe_plans_for_hiring_plan(hiring_plan)
            @stripe_plans_for_hiring_plan_cache ||= {}
            @stripe_plans_for_hiring_plan_cache[hiring_plan] ||= stripe_plans.select { |plan| plan[:product_metadata][:available_for_hiring_plan] == hiring_plan }
        end

        def stripe_plans_to_send_to_client(hiring_plan)
            unless defined? @plans_to_show_to_legacy_users
                @plans_to_show_to_legacy_users = stripe_plans_for_hiring_plan(HiringTeam::HIRING_PLAN_LEGACY)

                # even though we have a plan available for HIRING_PLAN_PAY_PER_POST and
                # a separate plan available for HIRING_PLAN_UNLIMITED_WITH_SOURCING, we send both
                # of those down to the client, since users might need to see prices for each
                @plans_to_show_to_non_legacy_users = self.stripe_plans - @plans_to_show_to_legacy_users
            end
            hiring_plan == HiringTeam::HIRING_PLAN_LEGACY ? @plans_to_show_to_legacy_users : @plans_to_show_to_non_legacy_users
        end

        # see also hiring_team_checkout_helper#_subscribe
        def is_pay_per_post_plan?(stripe_plan_id)
            stripe_plans_for_hiring_plan(HiringTeam::HIRING_PLAN_PAY_PER_POST).map { |plan| plan[:id] }.include?(stripe_plan_id)
        end

        def is_unlimited_with_sourcing_plan?(stripe_plan_id)
            stripe_plans_for_hiring_plan(HiringTeam::HIRING_PLAN_UNLIMITED_WITH_SOURCING).map { |plan| plan[:id] }.include?(stripe_plan_id)
        end

        def ensure_product_and_plans(product_id)
            product_config = self.stripe_product_config[product_id]
            product_id = product_id.to_s
            product = begin
                Stripe::Product.retrieve(product_id)
            rescue Stripe::InvalidRequestError => err
                raise err unless err.message.match(/No such product/)
            end

            statement_descriptor = self.stripe_statement_descriptor

            if product
                product.name = product_config[:name]
                product.metadata = product_config[:metadata]
                product.statement_descriptor = statement_descriptor
                product.unit_label = product_config[:unit_label]
                product.save
            else
                product = Stripe::Product.create({
                    id: product_id,
                    name: product_config[:name],
                    statement_descriptor: statement_descriptor,
                    type: 'service',
                    unit_label: product_config[:unit_label],
                    metadata: product_config[:metadata]
                })
            end

            plans = product_config[:plans].map do |plan_id, config|

                plan_id = plan_id.to_s
                plan = begin
                    Stripe::Plan.retrieve(plan_id)
                rescue Stripe::InvalidRequestError => err
                    raise err unless err.message.match(/No such plan/)
                end

                if plan

                    # only certain things can be updated
                    plan.metadata = config[:metadata]
                    plan.nickname = config[:nickname]
                    plan.trial_period_days = config[:trial_period_days] || 0
                    plan.save

                    plan.product = product_id
                    plan.save

                else
                    plan Stripe::Plan.create(config.merge({
                        id: plan_id,
                        product: product_id
                    }))
                end

                plan

            end

            [product, plans]
        end

        def ensure_stripe_products_and_plans
            self.stripe_product_config.each_key do |product_id|
                ensure_product_and_plans(product_id)
            end
        end
    end # end ClassMethods

    def subscription_slack_channel
        Slack.hiring_payments_channel
    end

    def subscription_identifier_for_slack(subscription, stripe_subscription)
        "#{self.title}"
    end

    def record_invoice_payment(subscription, invoice)
        # we used to log hiring_subscription:payment_succeeded here, but decided to fold all that
        # information into the regular `stripe.invoice.payment_succeeded` event
    end

    def increment_usage(subscription, timestamp)

        raise "Attempting to increment usage on a non-metered subscription!" unless subscription.metered?

        # Grab the sole subscription item
        subscription_item_id = subscription.subscription_item&.id
        raise "No associated Stripe::SubscriptionItem could be found for HiringTeam!" unless subscription_item_id

        # Create a UsageRecord
        usage_record = Stripe::SubscriptionItem.create_usage_record(
            subscription_item_id,
            {
                quantity: 1,
                timestamp: timestamp,
                action: 'increment'
            }
        )

        usage_record
    end

    def after_subscription_create(subscription)
        Event.create_server_event!(
            SecureRandom.uuid,
            self.owner_id,
            'hiring_subscription:created',
            {
                plan_id: subscription.stripe_plan_id,
                metered_plan: subscription.metered?
            }
        ).log_to_external_systems
    end

    def pay_per_post_subscriptions
        self.subscriptions.select { |sub| HiringTeam.is_pay_per_post_plan?(sub.stripe_plan_id) }
    end

    # this is tested partially in the subscriptions_controller_spec
    # and partially in the subscription_concern_spec
    def handle_create_subscription_request(attrs, meta, stripe_plan_id)

        meta = meta || {}
        trial_end_ts = nil
        subscription_metadata = {}
        previous_hiring_plan = self.hiring_plan

        # Update the hiring_plan in our database first
        if HiringTeam.is_pay_per_post_plan?(stripe_plan_id)
            if meta[:open_position_id]
                open_position = OpenPosition.find(meta[:open_position_id])
                raise "Expected open position to be connected to hiring team for subscription" unless open_position.hiring_team == self

                # We add the open_position_id to the metadata, but nothing in code actually relies on this.  It
                # is just there so that it will be available in the stripe dashboard or for debugging or whatever.
                # The thing that actually attaches a subscription to a position is the subscription_id on the position.
                subscription_metadata[:open_position_id] = meta[:open_position_id]

                # There are references to hiring_plan in the rest of this method, so we need to make sure
                # that we set the hiring_plan first, so the rest of this method works as expected.
                self.update!(hiring_plan: HiringTeam::HIRING_PLAN_PAY_PER_POST) if self.hiring_plan.nil?
            else
                raise RuntimeError.new("Must provide an open_position when creating a subscription for a pay_per_post plan")
            end
        elsif HiringTeam.is_unlimited_with_sourcing_plan?(stripe_plan_id)
            if self.hiring_plan.nil? || self.hiring_plan == HiringTeam::HIRING_PLAN_PAY_PER_POST
                self.update!(hiring_plan: HiringTeam::HIRING_PLAN_UNLIMITED_WITH_SOURCING)
            elsif self.hiring_plan == HiringTeam::HIRING_PLAN_UNLIMITED_WITH_SOURCING
                # noop
            else
                raise RuntimeError.new("Cannot switch to unlimited")
            end
        end

        # Then check if this stripe_plan is not available for this hiring_plan
        available_for_hiring_plan = HiringTeam.stripe_plan(stripe_plan_id)[:product_metadata][:available_for_hiring_plan]
        if available_for_hiring_plan != self.hiring_plan
            exception = RuntimeError.new("Cannot create subscription for stripe_plan that is unavailable for team's hiring_plan")
            exception.raven_options = {
                extra: {
                    current_hiring_plan: self.hiring_plan,
                    stripe_plan_available_for_hiring_plan: available_for_hiring_plan,
                    stripe_plan_id: stripe_plan_id
                }
            }
            raise exception
        end

        # Lastly make the Stripe changes
        # NOTE: we currently do not support any form of automated proration when changing
        # from an unlimited -> metered plan, because this could easily be gamed.
        if self.hiring_plan == HiringTeam::HIRING_PLAN_LEGACY && meta.key?(:cancel_current_subscription) && meta[:cancel_current_subscription] == true

            subscription = self.primary_subscription
            raise "Attempting to cancel a non-existent current subscription!" unless subscription
            raise "Attempting to cancel a subscription when while setting the new plan to the same as the existing one!" if subscription.stripe_plan_id == stripe_plan_id

            # If migrating from metered (ie - to licensed), then pad with a short free trial phase.
            # This allows any sort of charge failures to be deferred and have the UI briefly
            # reflect a proper subscription state before invoking expected past_due handling.
            if subscription.metered?
                trial_end_ts = (Time.now + 2.minutes).to_timestamp
            end

            subscription.cancel_and_destroy(reason: "Changing plan to #{stripe_plan_id}")

            self.subscriptions.reload
            self.reset_stripe_customer
        elsif previous_hiring_plan == HiringTeam::HIRING_PLAN_PAY_PER_POST &&
            self.hiring_plan == HiringTeam::HIRING_PLAN_UNLIMITED_WITH_SOURCING
            self.cancel_pay_per_post_subscriptions_before_switching_to_unlimited
        end

        subscription = Subscription.create_for_owner(
            owner: self,
            stripe_plan_id: stripe_plan_id,
            trial_end_ts: trial_end_ts,
            metadata: subscription_metadata)

        if open_position
            open_position.subscription = subscription
            open_position.update!(
                featured: true,
                archived: false
                # auto_expiration_date is also being set to nil in reconcile_auto_expiration_date
            )
        end

        subscription
    end

    def raise_if_duplicate_subscription!(metadata)

        open_position_id = metadata[:open_position_id]

        if open_position_id
            raise_if_duplicate_pay_per_post_subscription!(open_position_id)
        else
            raise_if_duplicate_primary_subscription!
        end

    end

    def potential_primary_subscriptions
        # `primary_subscription` means the one global subscription for this team.  It does not refer
        # to subscriptions for an open_position, of which there can be many.
        subscriptions - pay_per_post_subscriptions
    end

    def open_position_for_subscription(subscription)
        # It is also possible to call subscription.open_position, but see
        # comment in cancel_pay_per_post_subscriptions_before_switching_to_unlimited
        # about keeping references consistent.
        self.open_positions.detect { |open_position| open_position.subscription_id == subscription.id }
    end

    # When a subscription ends, the associated open position should be archived
    def before_subscription_destroy(subscription)
        open_position = open_position_for_subscription(subscription)
        if open_position
            open_position.subscription = nil

            # If we canceled a pay_per_post subscription because the user switched to an
            # unlimited plan, then we don't want to archive the position.  If the subscription
            # ended for any other reason, we do.
            if open_position.should_have_auto_expiration_date? && 
                (!subscription.stripe_subscription || subscription.stripe_subscription.metadata['cancellation_reason'] != HiringTeam::CANCELLATION_REASON_SWITCHING_TO_UNLIMITED)

                open_position.archived = true
                open_position.just_auto_expired = true

                # set the auto_expiration_date to now to record when this position was auto-archived for
                # the purposes of the UI
                open_position.auto_expiration_date = Time.now
            end
            open_position.save!
        end

        # Set back to null if canceling a sourcing subscription
        if self.hiring_plan == HiringTeam::HIRING_PLAN_UNLIMITED_WITH_SOURCING && subscription.id == self.primary_subscription&.id
            self.hiring_plan = nil
            self.save!
        end
    end

    def has_full_access?
        if using_pay_per_post_hiring_plan?
            true
        else

            # check that we have an primary_subscription
            _primary_subscription = self.primary_subscription

            # 1. If there is no payment requied for the team, then it is considered good.
            # 2. If there is a subscription and it's not past due, then it's good as well.
            !subscription_required || !!(_primary_subscription && !_primary_subscription.past_due)
        end
    end

    def using_pay_per_post_hiring_plan?
        self.hiring_plan == HiringTeam::HIRING_PLAN_PAY_PER_POST
    end

    def stripe_plans_to_send_to_client
        HiringTeam.stripe_plans_to_send_to_client(self.hiring_plan)
    end

    def cancel_pay_per_post_subscriptions_before_switching_to_unlimited
        self.pay_per_post_subscriptions.each do |subscription|

            # We need to prevent race conditions.  If the stripe subscription is
            # already destroyed, the we assume another process has run this code.
            return unless subscription.stripe_subscription

            open_position = self.open_position_for_subscription(subscription)
            open_position.auto_expiration_date ||= (Time.now + 60.days).beginning_of_day if open_position.featured? && !open_position.archived?

            # Make sure that subscription does not have to reload the
            # owner.  This is important because we need self.open_position_for_subscription to
            # return the instance of the position we just modified when it is called inside of
            # before_subscription_destroy
            subscription.hiring_team = self
            subscription.cancel_and_destroy(reason: HiringTeam::CANCELLATION_REASON_SWITCHING_TO_UNLIMITED, prorate: true)
            open_position.save!
        end
    end

    def automatically_cancel_subscriptions_when_no_subscription_required
        if self.changes[:subscription_required] != [true, false]
            return
        end

        switching_from_pay_per_post = self.hiring_plan_changed? && self.changes[:hiring_plan][0] == HiringTeam::HIRING_PLAN_PAY_PER_POST
        if switching_from_pay_per_post
            self.cancel_pay_per_post_subscriptions_before_switching_to_unlimited
        else
            self.subscriptions.each { |sub| sub.cancel_and_destroy(reason: "changing to subscription not required")}
        end

    end

    private
    def raise_if_duplicate_pay_per_post_subscription!(open_position_id)

        if stripe_customer.subscriptions.data.reject { |sub| sub.status == 'incomplete' || sub.metadata[:open_position_id] != open_position_id }.size > 0
            raise(Subscription::CannotCreateDuplicateSubscription.new("There is already a subscription in stripe for open_position #{open_position_id.inspect}"))
        end
    end
end
