# == Schema Information
#
# Table name: career_profiles
#
#  id                                                   :uuid             not null, primary key
#  created_at                                           :datetime
#  updated_at                                           :datetime
#  user_id                                              :uuid
#  score_on_gmat                                        :text
#  score_on_sat                                         :text
#  score_on_act                                         :text
#  short_answers                                        :json             not null
#  willing_to_relocate                                  :boolean          default(TRUE), not null
#  open_to_remote_work                                  :boolean          default(TRUE), not null
#  authorized_to_work_in_us                             :text
#  top_mba_subjects                                     :text             default([]), not null, is an Array
#  top_personal_descriptors                             :text             default([]), not null, is an Array
#  top_motivations                                      :text             default([]), not null, is an Array
#  top_workplace_strengths                              :text             default([]), not null, is an Array
#  bio                                                  :text
#  personal_fact                                        :text
#  preferred_company_culture_descriptors                :text             default([]), not null, is an Array
#  li_profile_url                                       :text
#  resume_id                                            :uuid
#  personal_website_url                                 :text
#  blog_url                                             :text
#  tw_profile_url                                       :text
#  fb_profile_url                                       :text
#  job_sectors_of_interest                              :text             default([]), not null, is an Array
#  employment_types_of_interest                         :text             default([]), not null, is an Array
#  company_sizes_of_interest                            :text             default([]), not null, is an Array
#  primary_areas_of_interest                            :text             default([]), not null, is an Array
#  place_id                                             :text
#  place_details                                        :json             not null
#  last_calculated_complete_percentage                  :float
#  sat_max_score                                        :text             default("1600"), not null
#  score_on_gre_verbal                                  :text
#  score_on_gre_quantitative                            :text
#  score_on_gre_analytical                              :text
#  short_answer_greatest_achievement                    :text
#  primary_reason_for_applying                          :text
#  do_not_create_relationships                          :boolean          default(FALSE), not null
#  salary_range                                         :text
#  short_answer_leadership_challenge                    :text
#  interested_in_joining_new_company                    :text
#  locations_of_interest                                :text             default(["\"none\""]), not null, is an Array
#  profile_feedback                                     :text
#  feedback_last_sent_at                                :datetime
#  location                                             :geography(Point,
#  github_profile_url                                   :text
#  long_term_goal                                       :text
#  last_confirmed_at_by_student                         :datetime
#  last_confirmed_at_by_internal                        :datetime
#  last_updated_at_by_student                           :datetime
#  last_confirmed_internally_by                         :string
#  short_answer_scholarship                             :string
#  consider_early_decision                              :boolean
#  student_network_looking_for                          :text             default([]), not null, is an Array
#  native_english_speaker                               :boolean
#  earned_accredited_degree_in_english                  :boolean
#  survey_years_full_time_experience                    :text
#  survey_most_recent_role_description                  :text
#  survey_highest_level_completed_education_description :text
#  short_answer_why_pursuing                            :text
#  short_answer_use_skills_to_advance                   :text
#  short_answer_ask_professional_advice                 :text
#  has_no_formal_education                              :boolean          default(FALSE), not null
#  sufficient_english_work_experience                   :boolean
#  english_work_experience_description                  :text
#

# See hiring_application.rb for a mapping of columns in the hiring application
# to associated columns in the career profile

class CareerProfile < ApplicationRecord

    include Skylight::Helpers
    include HasFulltextSupport
    include CareerProfile::JsonHelper
    include TimezoneMixin

    class ActiveRecord::Relation
        # NOTE: This should be in accordance to the career_profile_active? method and is
        # also duplicated in ViewHelpers::YearsOfExperience and users_controller.rb.
        def active
            self.joins(:user).editable_and_complete.where(
                interested_in_joining_new_company: self.active_interest_levels
            )
        end

        def editable_and_complete
            # NOTE: this won't work unless you join against users
            self.where(
                users: {can_edit_career_profile: true},
                last_calculated_complete_percentage: 100
            )
        end

        def get_geo_cluster_json(zoom, marker_width)

            clustering_radius, min_cluster_size = Location.get_clustering_radius_and_min_cluster_size(zoom, marker_width)

            ctes = []

            step1query = self.select(:id)
            step_1_sql = step1query.to_sql

            # wrap in a CTE
            ctes << "WITH step_1 AS MATERIALIZED (#{step_1_sql})"

            # --------------------------
            # Step 2: Select location and cluster_id
            # This could be done as part of step 1, but it turns out to be faster to get
            # a list of ids first and then generate the cluster_ids.  Apparently if we do
            # the filtering for profiles and generating of cluster_ids in one step then postgres
            # wastes time generating cluster_ids for profiles that are going to get filtered out
            # --------------------------
            step_2_sql = "
                select
                    id
                    , location
                    , #{Location.select_cluster_id_sql_string('location', clustering_radius, min_cluster_size)}
                from career_profiles
                where career_profiles.id in (select id from step_1)"
            ctes << "step_2 AS MATERIALIZED (#{step_2_sql})"

            # --------------------------
            # Step 2: Group by the clusters
            # --------------------------

            step_3_sql = Location.select_and_group_by_cluster_id_sql_string(clustering_radius, 'location', 'cluster_id', 'id', 'step_2')
            ctes << "step_3 AS MATERIALIZED (#{step_3_sql})"

            # --------------------------
            # Step 3: Convert rows to GeoJSON Features
            # --------------------------

            step_4_sql = Location.select_as_geojson_features_sql_string('cluster_id', 'cluster_center', 'step_3')
            ctes << "step_4 AS MATERIALIZED (#{step_4_sql})"

            # --------------------------
            # Step 4: Aggregate Features into a FeatureCollection
            # --------------------------

            select_sql = Location.select_as_geojson_feature_collection_sql_string('feature', 'step_4')

            # Final SQL call to return GeoJSON
            query_json = "#{ctes.join(",\n")}#{select_sql}"

            result = ActiveRecord::Base.connection.execute(query_json)
            json = result.to_a[0]["json"] || "[]"
            json
        end
    end

    belongs_to :user
    belongs_to :resume, class_name: 'S3Asset', dependent: :destroy, optional: true
    belongs_to :student_network_inclusion, :primary_key => :user_id, :foreign_key => :user_id, optional: true

    # having this here instead of just through user can speed up queries since
    # we don't need the extra join
    has_many :cohort_applications, primary_key: :user_id, foreign_key: :user_id

    # autosave is true becasue merge_hash may update the ruby objects for the related experiences.
    # When save! is called, we want any changes to those experiences to be persisted in the db
    has_many :education_experiences, class_name: 'CareerProfile::EducationExperience', foreign_key: :career_profile_id, primary_key: :id, validate: 'false', dependent: :destroy, autosave: true, inverse_of: :career_profile

    has_many :work_experiences, class_name: 'CareerProfile::WorkExperience', foreign_key: :career_profile_id, primary_key: :id, validate: 'false', dependent: :destroy, autosave: true
    has_one :featured_work_experience, -> { where(featured: true) }, class_name: 'CareerProfile::WorkExperience', foreign_key: :career_profile_id, primary_key: :id, validate: 'false', dependent: :destroy

    # skills auto-suggest (ordered)
    has_many :skills_joins, class_name: 'CareerProfile::CareerProfilesSkillsOptionsJoin', dependent: :destroy
    has_many :skills, class_name: 'SkillsOption', through: :skills_joins, source: :skills_option, dependent: :destroy

    # awards and interests auto-suggest (ordered)
    has_many :awards_and_interests_joins, class_name: 'CareerProfile::CareerProfilesAwardsAndInterestsOptionsJoin', dependent: :destroy
    has_many :awards_and_interests, class_name: 'CareerProfile::AwardsAndInterestsOption', through: :awards_and_interests_joins, source: :awards_and_interests_option,  dependent: :destroy

    # student network interests auto-suggest (ordered)
    has_many :student_network_interests_joins, class_name: 'CareerProfile::CareerProfilesStudentNetworkInterestsOptionsJoin', dependent: :destroy
    has_many :student_network_interests, class_name: 'StudentNetworkInterestsOption', through: :student_network_interests_joins, source: :student_network_interests_option, dependent: :destroy

    # peer recommendations
    has_many :peer_recommendations, class_name: 'PeerRecommendation', dependent: :destroy

    has_many :candidate_position_interests, primary_key: :user_id, foreign_key: :candidate_id, validate: false

    has_one :career_profile_search_helper, class_name: 'CareerProfile::SearchHelper'

    before_destroy :disassociate_test_resume

    # Re-identify the user with segment when a change is made to a profile
    after_commit :identify_user, on: [:create, :update]

    # Tell Customer.io to trigger an email to send to the user with feedback on their profile
    after_save :send_feedback_email
    after_save :log_peer_recommendation_request_events

    after_save :reject_candidate_position_interests_not_reviewed_by_hiring_manager, if: :just_deactivated?

    after_save :update_in_airtable

    validate :validate_inactive_if_cannot_edit
    validate :validate_appropriate_do_not_create_relationships_change
    validate :only_one_featured_work_experience
    validate :validate_score_on_sat

    def self.active
        self.all.active
    end

    def self.editable_and_complete
        self.all.editable_and_complete
    end

    def self.new_company_interest_levels
        return %w(very_interested interested neutral not_interested)
    end

    def self.active_interest_levels
        return @active_interest_levels ||= self.new_company_interest_levels.reject { |level| inactive_interest_levels.include?(level) }
    end

    def self.inactive_interest_levels
        ['not_interested']
    end

    def self.create_from_hash!(hash)
        transaction do
            instance = initialize_from_hash(hash)
            instance.save!
            instance
        end
    end

    def self.initialize_from_hash(hash)
        hash = hash.unsafe_with_indifferent_access
        instance = new
        instance.user_id = hash['user_id']
        instance.merge_hash(hash)
        instance
    end

    def self.update_from_hash!(hash, meta = {})
        # Automatically retries in the event of ActiveRecord::PreparedStatementCacheExpired
        RetriableTransaction.transaction do
            hash = hash.unsafe_with_indifferent_access
            instance = find_by_id(hash[:id])

            if instance.nil?
                raise ActiveRecord::RecordNotFound.new("Couldn't find #{self.name} with id=#{hash[:id]}")
            end

            instance.merge_hash(hash, meta)

            # force updated_at to change regardless of other changes
            instance.updated_at = Time.now

            instance.save!
            instance
        end

    end

    def self.includes_needed_for_json_eager_loading_student_network
        [
            {:work_experiences => [:professional_organization]},
            {:education_experiences => CareerProfile::EducationExperience.includes_needed_for_json_eager_loading},
            {:user => [:avatar, :cohort_applications]},
            :student_network_interests
        ]
    end

    def self.includes_needed_for_json_eager_loading
        [
            {:work_experiences => [:professional_organization]},
            {:education_experiences => CareerProfile::EducationExperience.includes_needed_for_json_eager_loading},
            :skills,
            :awards_and_interests,
            {:user => [:avatar, :cohort_applications]},
            :resume,
            :peer_recommendations,
            :student_network_interests
        ]
    end

    def merge_hash(hash, meta = {})
        meta ||= {}
        hash = hash.unsafe_with_indifferent_access

        basic_profile_keys = %w(place_id place_details do_not_create_relationships)

        education_keys = %w(survey_highest_level_completed_education_description native_english_speaker earned_accredited_degree_in_english has_no_formal_education
            sufficient_english_work_experience english_work_experience_description)

        work_keys = %w(survey_years_full_time_experience survey_most_recent_role_description)

        test_score_keys = %w(score_on_gmat score_on_sat score_on_act score_on_gre_verbal score_on_gre_quantitative
                                score_on_gre_analytical sat_max_score)

        application_questions_keys = %w(short_answer_greatest_achievement short_answer_leadership_challenge short_answer_ask_professional_advice
                                        primary_reason_for_applying short_answer_scholarship short_answer_why_pursuing short_answer_use_skills_to_advance)

        job_preferences_keys = %w(willing_to_relocate open_to_remote_work authorized_to_work_in_us
                                employment_types_of_interest company_sizes_of_interest salary_range
                                interested_in_joining_new_company locations_of_interest)

        more_about_you_keys = %w(top_mba_subjects top_personal_descriptors top_motivations top_workplace_strengths awards_and_interests)
        skills_keys = %w(skills)
        student_network_keys = %w(student_network_interests student_network_looking_for)
        personal_bio_keys = %w(bio long_term_goal personal_fact)
        employer_preferences_keys = %w(job_sectors_of_interest primary_areas_of_interest preferred_company_culture_descriptors)
        resume_and_links_keys = %w(li_profile_url resume_id personal_website_url
                                    blog_url tw_profile_url fb_profile_url github_profile_url)
        profile_feedback_keys = %w(profile_feedback)
        tracking_timestamp_keys = %w(last_updated_at_by_student)
        submit_application_keys = %w(consider_early_decision)

        (
            ["last_calculated_complete_percentage"] +
            basic_profile_keys + education_keys + work_keys + test_score_keys + job_preferences_keys +
            more_about_you_keys + skills_keys + student_network_keys +
            personal_bio_keys + employer_preferences_keys + resume_and_links_keys +
            application_questions_keys + profile_feedback_keys + tracking_timestamp_keys +
            submit_application_keys
        ).each do |key|

            if hash.key?(key)
                val = hash[key]
                val = nil if val.is_a?(String) && val.blank?

                # special handling of skills, awards_and_interests, and student network interests mapping
                option_class = {
                    "skills" => SkillsOption,
                    "awards_and_interests" => CareerProfile::AwardsAndInterestsOption,
                    "student_network_interests" => StudentNetworkInterestsOption
                }[key]
                if option_class and val.is_a?(Array)

                    # loop through all the options
                    self.send(:"#{key}_joins=", []) # reset the association collection
                    self.send(:"#{key}=", []) # reset the association collection

                    val.each_with_index do |option_vals, index|
                        # find_or_initialize an option. Since this is a has_many :through association, rails will
                        # auto-create the join record, and acts_as_list will automatically set the position on the join record
                        # http://stackoverflow.com/questions/26651811/rails-activerecord-has-many-through-association-on-unsaved-objects
                        option = option_class.find_or_initialize_by(option_vals.slice(:locale, :text))
                        self.send(key.to_sym) << option
                    end
                else

                    # normalize URLs with a leading prefix
                    if val && key.ends_with?('_url') && !(val[/^http:\/\//] || val[/^https:\/\//])
                        val = "https://#{val}"
                    end

                    if (key == 'last_calculated_complete_percentage' && self.last_calculated_complete_percentage == 100 && self.last_calculated_complete_percentage > val)
                        # We don't really expect this to happen, but there are two cases that we are
                        # looking for.
                        #
                        # 1. Using two browsers, a user can override changes on a career profile and cause this to happen.
                        #       We should eventually prevent this and show a user-facing error, but the thought for now
                        #       is that the scale and impact of the bug means it's not worth writing code against yet.
                        #       See https://trello.com/c/4Lir2sIi
                        # 2. More than once, we have introduced new bugs that make it possible for the last_calculated_complete_percentage
                        #       to go down, even though we don't want it to.
                        #       See, for example, https://trello.com/c/KUSkdYg5
                        begin
                            # raise the error to get a stacktrace
                            raise "Decreasing last_calculated_complete_percentage on a career_profile"
                        rescue Exception => err
                            Raven.capture_exception(err,
                                :extra => {
                                    'user_id' => self.user_id,
                                    'orig_last_calculated_complete_percentage' => self.last_calculated_complete_percentage,
                                    'new_last_calculated_complete_percentage' => val
                                }
                            )
                        end
                    end

                    self.send(:"#{key}=", val)
                end

            end
        end

        self.update_associations_from_hash!(hash, meta)

        now = Time.now
        # We only update feedback_last_sent_at if send_feedback_email is included in the meta hash
        if meta.key?(:send_feedback_email) && meta[:send_feedback_email]
            self.feedback_last_sent_at = now
        end

        if meta.key?(:confirmed_by_internal) && meta[:confirmed_by_internal]
            self.last_confirmed_at_by_internal = now
            if !meta.key?(:confirmed_internally_by)
                raise "Must specify internal user who confirmed career profile if confirmed_by_internal"
            end
            self.last_confirmed_internally_by = meta[:confirmed_internally_by]
        else
            if meta.key?(:confirmed_by_student) && meta[:confirmed_by_student]
                self.last_confirmed_at_by_student = now
            end
        end

        # Ensure that we flip locations_of_interest back to "none" if the user becomes unwilling to relocate. We
        # should not be allowing this scenario on the frontend, and are migrating current users in this scenario, but we
        # need to do this for backwards compatability and should probably just leave it in going forward.
        # See https://trello.com/c/U6TQqTmq
        if hash["willing_to_relocate"] == false
            self["locations_of_interest"] = ["none"]
        end

        # There was a bug in older versions of the client that could allow a user to
        # input ['none', 'new_york', etc.] as their locations_of_interest. This caused
        # a UI issue in the front-end where we assumed the user was not interested
        # in other locations, because the first index was 'none'.
        #
        # ['none'] => valid, ['none', 'new_york'] => invalid
        #
        # See https://trello.com/c/BkaBTnnI
        if hash["locations_of_interest"]&.include?('none') && hash["locations_of_interest"].count > 1
            self["locations_of_interest"] = hash["locations_of_interest"].reject{|e| e == 'none'}
        end

        self
    end

    def update_associations_from_hash!(hash, meta = {})
        hash = hash.unsafe_with_indifferent_access

        update_association(hash, PeerRecommendation, %w(email))

        update_association(hash, CareerProfile::WorkExperience,
                %w(professional_organization job_title start_date end_date responsibilities featured industry role employment_type))

        update_association(hash, CareerProfile::EducationExperience,
                %w(educational_organization graduation_year degree major minor gpa gpa_description degree_program will_not_complete)) do
                # We need to recalculate transcripts_verified on the associated user when an
                # admin updates this association.
                # See also: https://trello.com/c/aFE73g7L
                if meta[:is_admin] && self.user.should_have_transcripts_auto_verified?
                    self.user.update!(transcripts_verified: true)
                end
            end
    end

    def update_association(hash, klass, props, &block)
        key = klass.name.demodulize.underscore.pluralize

        if (hash[key]) # e.g. if the hash has the key 'work_experiences'

            list = self.send(key.to_sym)
            existing_options = {}

            new_items = hash[key].map do |item_hash|

                # Unlike work and education experiences, we ensure that peer recommendation
                # records are unique by the 'email' key
                unique_key = key == 'peer_recommendations' ? 'email' : 'id'

                # item is an instance of the klass param, e.g. WorkExperience
                item = item_hash[unique_key] && list.detect { |_item| _item.send(:"#{unique_key}") == item_hash[unique_key] }
                if item.nil?
                    item = list.build({id: SecureRandom.uuid})
                elsif item.career_profile_id && item.career_profile_id != self.id
                    raise "Cannot update #{klass.name.demodulize.underscore.humanize(capitalize: false)} for a different career profile"
                end

                # props is a list of keys that we should update on the item
                props.each do |prop|

                    if item_hash.key?(prop)
                        val = item_hash[prop]
                        val = nil if val.is_a?(String) && val.blank?

                        # Special handling of specific timestamps for work and education experiences.
                        # The client will send up the dates as a timestamp in milliseconds UTC
                        if (prop == 'start_date' || prop == 'end_date') && val.is_a?(Integer)
                            val = Time.at(val / 1000.0).utc.to_date # convert milliseconds sent from client to a date object in UTC
                        end

                        # special handling for organizational mapping
                        option_class = {
                            "professional_organization" => ProfessionalOrganizationOption,
                            "educational_organization" => CareerProfile::EducationalOrganizationOption
                        }[prop]

                        if option_class

                            # find or create (non-suggestable) entry and associate with underlying experience item
                            # ensuring that duplicate option entries aren't inserted twice (unique error)
                            existing_options[val[:text]] ||= option_class.find_or_initialize_by(val.slice(:locale, :text))

                            item.send(:"#{prop}=", existing_options[val[:text]])
                        else
                            item[prop] = val
                        end

                    end
                end

                item
            end

            # reset the association with new_items
            self.send(:"#{key}=", new_items)

            yield if block_given?
        end
    end

    def as_json(options = {})
        view = options[:view]

        # FIXME: once this has been out in the wild for a bit and we're
        # confident that we're never calling this without a view explicitly
        # passed in we can remove this
        if view.nil? && Rails.env.production?
            begin
                raise "career profile called with no view"
            rescue Exception => err
                Raven.capture_exception(err)
                view = 'career_profiles'
            end
        end

        options[:fields] ||= []

        # these methods are al defined in CareerProfile::JsonHelper
        if view == 'student_network_profiles'
            as_json_for_student_network(options)
        elsif view == 'career_profiles'
            as_json_for_career_network(options)
        elsif view == 'editable'
            as_json_for_edit(options)
        else
            raise ArgumentError.new("Unexpected view #{view}")
        end
    end

    # We've modified how we determine if a career profile is active or inactive. If a profile is active, the user must be
    # able to edit their career profile, their profile must be complete, and they must have some kind of active interest
    # level in joining a new company (see the CareerProfile.active_interest_levels method above). NOTE: This should be
    # in accordance with the CareerProfile.active method above and is also duplicated in ViewHelpers::YearsOfExperience.
    def career_profile_active?
        self.career_profile_editable_and_complete? && self.class.active_interest_levels.include?(self.interested_in_joining_new_company)
    end

    # NOTE: This should be in accordance with any default filters in StudentNetworkFiltersMixin (i.e. filter_for_access_to_network)
    # Any changes here will need to be reflected there
    def student_network_active?
        self.user.pref_student_network_privacy != 'hidden' &&
        self.user.has_application_that_provides_student_network_inclusion? &&
        self.place_details.present? # NOTE: in sql, this filter is on location, but location is set by a sql trigger and reliant on place_details
    end

    def career_profile_editable_and_complete?
        self.user.can_edit_career_profile &&
        self.last_calculated_complete_percentage == 100
    end

    # determines if the career profile was just deactivated as a result of the most recent save
    def just_deactivated?
        # the career profile is considered to be "just deactivated" if interested_in_joining_new_company
        # was updated from an active interest level to an inactive interest level
        if self.saved_change_to_interested_in_joining_new_company? &&
            self.class.active_interest_levels.include?(self.attribute_before_last_save(:interested_in_joining_new_company)) &&
            self.class.inactive_interest_levels.include?(self.interested_in_joining_new_company)
            true
        else
            false
        end
    end

    def deep_link_url
        # NOTE: the path and query params of this URL should correspond
        # with the CareerProfile#deepLinkUrl on the client
        domain = Rails.env.development? ? 'http://localhost:3001' : 'https://smart.ly'

        "#{domain}/hiring/browse-candidates?tab=featured&id=#{self.id}"
    end

    # Determines if the career profile can be viewed by the hiring_manager for deep linking purposes.
    # NOTE: THis method makes an implicit assumption that a hiring manager should have a hiring application.
    def viewable_by_hiring_manager_through_deep_link?(hiring_manager)
        return false if hiring_manager.hiring_application.nil?
        # first, check if the candidate is available_for_relationships with this hiring manager and if not,
        # then fallback to checking if the hiring manager's team has any saved or accepted hiring relatinoships
        # with this candidate. If either of these conditions are true, then the career profile should be visible
        # to this hiring manager.
        User.available_for_relationships(hiring_manager.professional_organization_option_id).pluck(:id).include?(self.user_id) ||
        HiringRelationship.where(
            candidate_id: self.user_id,
            hiring_manager_id: hiring_manager.hiring_team&.hiring_manager_ids || hiring_manager.id,
            hiring_manager_status: HiringRelationship.viewable_hiring_manager_statuses
        ).any?
    end

    def reject_candidate_position_interests_not_reviewed_by_hiring_manager
        self.candidate_position_interests.where.not(hiring_manager_status: CandidatePositionInterest.reviewed_hiring_manager_statuses).each do |interest|
            interest.update!(candidate_status: 'rejected')
        end
    end

    def get_pdf_asset(file)
        S3Asset.new({
            :file => file,
            :directory => "careers/linkedin_profiles/"
        })
    end

    def identify_user
        self.user.identify
    end

    def update_in_airtable
        self.user.update_in_airtable
    end

    # For each associated peer recommendation record where 'contacted' is false,
    # 'contacted' gets set to true, a cohort:peer_recommendation_request server
    # event gets logged to Segment and Customer.io, and a LogToSlack delayed job
    # gets enqueued, all of which takes place inside of a transaction.
    def log_peer_recommendation_request_events
        # ensure that we only perform the following logic if the user has a pending
        # cohort application for a cohort that actually supports peer recommendations
        if self.user.pending_application && self.user.pending_application.published_cohort.supports_peer_recommendations?
            RetriableTransaction.transaction do
                # restrict to just peer_recommendations that have not been contacted
                self.peer_recommendations.where(contacted: false).each do |peer_recommendation|
                    peer_recommendation.update_attribute(:contacted, true)

                    Event.create_server_event!(
                        SecureRandom.uuid,
                        self.user_id,
                        'cohort:peer_recommendation_request',
                        {
                            applicant_name: self.user.name,
                            peer_recommender_email: peer_recommendation.email,
                            peer_recommendation_form_url: "https://smart.ly/peer-recommendations/#{self.user_id}?from=#{peer_recommendation.email}",
                            program_type: self.user.program_type
                        }
                    ).log_to_external_systems

                    LogToSlack.perform_later(
                        Slack.peer_review_notify_channel,
                        "*New Peer Recommendation Sent*",
                        [
                            {
                                pretext: "From: #{self.user.email}, To: #{peer_recommendation.email}"
                            }
                        ]
                    )
                end
            end
        end
    end

    # Creates a server event and tells Customer.io to send an email to the user,
    # alerting them of info on their career profile that we'd like them to modify.
    def send_feedback_email
        if self.saved_change_to_feedback_last_sent_at? && !self.feedback_last_sent_at.nil?
            Event.create_server_event!(
                SecureRandom.uuid,
                self.user_id,
                'career_profile:send_feedback',
                {
                    cohort_title: self.user.relevant_cohort.title,
                    profile_feedback: self.profile_feedback
            }).log_to_external_systems
        end
    end

    def validate_inactive_if_cannot_edit
        if self.career_profile_active? && (!self.user || !self.user.can_edit_career_profile?)
            errors.add(:career_profile, "can only be activated if the user's can_edit_career_profile flag is true")
        end
    end

    def validate_appropriate_do_not_create_relationships_change
        # we only need concern ourselves with 'do_not_create_relationships' changes from false to true and the validation should fail if
        # the user is in a CareerProfileList
        if do_not_create_relationships_changed? && do_not_create_relationships_changed?(from: false, to: true) && CareerProfileList.pluck(:career_profile_ids).flatten.include?(id)
            errors.add(:do_not_create_relationships, "can only be true if candidate is not in a CareerProfileList")
        end
    end

    def only_one_featured_work_experience
        work_experiences = self.work_experiences
        if !work_experiences.empty?
            featured_work_experiences = work_experiences.select { |work_experience| work_experience.featured? }
            if featured_work_experiences.length > 1
                errors.add(:career_profile, "can only have one featured work experience")
            end
        end
    end

    # NOTE: This should mirror the client-side validation.
    def validate_score_on_sat
        if score_on_sat && (/\D+/ =~ score_on_sat || score_on_sat.to_i < 400 || score_on_sat.to_i > sat_max_score.to_i)
            errors.add(:score_on_sat)
        end
    end

    def backfill_place(query_string)
        client = GooglePlaces::Client.new(ENV["GOOGLE_API_KEY"])
        places = client.spots_by_query(query_string)

        if places.nil? || places.size == 0
            puts "#{user.id} could not be backfilled with query string '#{query_string}'"
            return false
        end

        self.place_id = places.first.place_id
        self.place_details = Location.place_details_for_place_id(self.place_id)
        puts "#{user.id} location backfilled with '#{full_place.formatted_address}' for query string '#{query_string}'"

        return true
    end

    def should_update_fulltext?
        !!(
            saved_changes[:city_name] ||
            saved_changes[:personal_fact] ||
            saved_changes[:skills] ||
            saved_changes[:awards_and_interests] ||
            saved_changes[:top_personal_descriptors] ||
            saved_changes[:top_workplace_strengths] ||
            saved_changes[:job_sectors_of_interest] ||
            saved_changes[:student_network_interests] ||
            saved_changes[:student_network_looking_for]
        )
    end

    def update_fulltext
        RebuildCareerProfileFulltextJob.perform_later_with_debounce(self.id)
    end

    def indicates_user_should_upload_transcripts?
        self.education_experiences_indicating_transcript_required.any?
    end

    # A user's career profile should indicate that the user needs to upload
    # english language proficiency documents if the user is not a native english
    # speaker, has not earned an accredited degree in english, and does not
    # have sufficient english work experience.
    #
    # Duplicated in career_profile.js (indicatesUserShouldUploadEnglishLanguageProficiencyDocuments definition)
    def indicates_user_should_upload_english_language_proficiency_documents?
        # The native_english_speaker and earned_accredited_degree_in_english columns
        # are nullable in the database, so we need to explicitly check if these values
        # are set to false. Likewise, the sufficient_english_work_experience column is
        # nullable, so we need to explicitly check if this value is not true in order
        # to treat NULL the same as false.
        return self.native_english_speaker == false && self.earned_accredited_degree_in_english == false && self.sufficient_english_work_experience != true
    end

    def disassociate_test_resume

        # see also `career_profile_dev_helper.js`
        return unless Rails.env.development? && resume_id == '369d4249-4094-4904-939b-18637b0e5c31'

        # don't allow fulltext to rebuild on this change, since it was most likely already deleted
        CareerProfile.skip_callback(:save, :after, :update_fulltext)
        update_attribute(:resume_id, nil)
    end

    def city_name
        Location.city_name(place_details)
    end

    def city_name_and_state_abbr
        Location.city_name_and_state_abbr(place_details)
    end

    def country_name
        Location.country_name(place_details)
    end

    def real_locations_of_interest
        locations_of_interest.reject { |i| ['none', 'flexible'].include?(i) }
    end

    def real_primary_areas_of_interest
        primary_areas_of_interest.without('other')
    end

    # See also career_profile.js
    def missing_transcripts?
        self.education_experiences_indicating_transcript_required
            .select { |e| !e.transcript_uploaded_or_waived? }
            .size > 0
    end

    # See also career_profile.js
    def education_experiences_indicating_transcript_required
        self.education_experiences.to_a
            .select { |e| e.transcript_required? }
    end
end
