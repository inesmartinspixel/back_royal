module SignNow

    class ApiRequestError < RuntimeError
        attr_accessor :response

        def initialize(response)
            self.response = response

            if response['error']
                message = response['error']
            elsif response['errors']
                message = response['errors'].map { |e| e['message'] }.compact.join(', ')
            elsif response.respond_to?(:keys) && response.keys.size == 1 && response.keys[0].to_i > 0
                status_code = response.keys[0].to_i
                message = "#{status_code}: #{response.values[0]}"
            elsif response.respond_to?(:code)
                message = "Error code #{response.code}"
            else
                message = response.to_s 
            end
            super(message)
        end

        def raven_options
            {
                extra: {
                    response_code: response.respond_to?(:code) ? response.code : nil,
                    response_body: response.respond_to?(:body) ? response.body.slice(0, 1000) : nil
                }
            }
        end

    end

    def self.encoded_client_credentials
        ENV['SIGN_NOW_ENCODED_CLIENT_CREDENTIALS']
    end

    def self.sender_credentials
        OpenStruct.new({
            email: ENV['SIGN_NOW_SENDER_USERNAME'],
            password: ENV['SIGN_NOW_SENDER_PASSWORD']
        })
    end

    def self.signer_credentials
        OpenStruct.new({
            email: ENV['SIGN_NOW_SIGNER_USERNAME'],
            password: ENV['SIGN_NOW_SIGNER_PASSWORD']
        })
    end

    def self.sender_access_token
        @sender_access_token ||= self.generate_access_token(
            self.sender_credentials.email,
            self.sender_credentials.password
        )['access_token']
    end

    def self.generate_access_token(username, password, scope = nil)

        attrs = {
            username: username,
            password: password,
            grant_type: 'password'
        }

        if scope.present?
            attrs[:scope] = scope
        end

        response = SignNow.post(
            "/oauth2/token",
            body: attrs.to_query,
            headers: {
                "Authorization": "Basic #{SignNow.encoded_client_credentials}"
            }
        )

        response
    end

    def self.root_url
        "https://api.signnow.com"
    end

    def self.root_signing_link_url
        "https://signnow.com"
    end

    def self.subscribe_to_event(event, url)
        self.post(
            "/event_subscription",
            body: {
                event: event,
                callback_url: url
            }.to_json,
            headers: {
                "Content-Type": "application/json"
            }
        )
    end

    def self.unsubscribe_from_event(event, url)
        list_subscriptions.each do |subscription|
            if subscription['callback_url'] == url && subscription['event'] == event
                self.delete("/event_subscription/#{subscription['id']}")
            end
        end
    end

    def self.list_subscriptions
        self.get("/event_subscription")['subscriptions']
    end

    def self.post(path, options = {}, &block)
        request(:post, path, options, &block)
    end

    def self.delete(path, options = {}, &block)
        request(:delete, path, options, &block)
    end

    def self.get(path, options = {}, &block)
        request(:get, path, options, &block)
    end

    def self.put(path, options = {}, &block)
        request(:put, path, options, &block)
    end

    def self.request(meth, path, options, retry_count = 0, &block)

        raise "Do not make SignNow requests in test mode" if Rails.env.test?

        url = "#{self.root_url}#{path}"

        # Unless we're making the request to get the access token,
        # we want to add the access token we have in here.
        unless path == "/oauth2/token"
            options = {
                headers: {
                    Authorization: "Bearer #{SignNow.sender_access_token}"
                }
            }.deep_merge(options)
        end

        response = HTTParty.send(meth, url, options, &block)

        if response.ok?
            response

        # We don't know if this will really help anything, but maybe
        # if the problem was with a token then generating a new one
        # will help?  Since our servers never run for 30 days without
        # restarting, we should never have a token expire anyway.
        elsif response['error'] && response['error'].match(/token/) && retry_count == 0
            @sender_access_token  = nil
            request(meth, path, options, retry_count+1, &block)
        else
            raise ApiRequestError.new(response)
        end
    end
end