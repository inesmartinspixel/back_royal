# == Schema Information
#
# Table name: career_profile_lists
#
#  id                 :uuid             not null, primary key
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  name               :text             not null
#  career_profile_ids :uuid             default([]), not null, is an Array
#  role_descriptors   :text             default([]), not null, is an Array
#

class CareerProfileList < ApplicationRecord


    def as_json(options = {})

        fields = options[:fields]
        filters = options[:filters] || {}

        career_profiles = nil

        if fields && fields.include?("career_profiles")
            # FIXME: this could be further optimized
            career_profiles_records = CareerProfile.includes(*CareerProfile.includes_needed_for_json_eager_loading).where(id: self.career_profile_ids)

            # we show non-anonymized profiles even to users who wouldn't normally
            # be allowed to access them if they are viewing profiles in a CareerProfileList
            career_profiles_json = career_profiles_records.as_json(options.merge(view: 'career_profiles', anonymize: false))
            career_profiles = sort_career_profiles(career_profiles_json)

            if filters.key?(:limit)
                career_profiles = career_profiles.slice(0, filters[:limit])
            end
        end

        super.merge(
            created_at: created_at.to_timestamp,
            updated_at: updated_at.to_timestamp,
            career_profiles: career_profiles
        )
    end

    # Sorts the career profiles to match the order of the ids in
    # the CareerProfileList instance's career_profile_ids array.
    def sort_career_profiles(career_profiles)
        map = {}.with_indifferent_access
        sorted_career_profiles = []

        # index the career profiles by their id
        career_profiles.each do |career_profile|
            map[career_profile["id"]] = career_profile
        end

        # create a new array of the career profiles based on the order of the ids
        # in the career profile list's career_profile_ids
        career_profile_ids.each_with_index do |id, index|
            sorted_career_profiles[index] = map[id]
        end

        sorted_career_profiles
    end

end
