# == Schema Information
#
# Table name: peer_recommendations
#
#  id                :uuid             not null, primary key
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  career_profile_id :uuid             not null
#  email             :text             not null
#  contacted         :boolean          default(FALSE), not null
#

class PeerRecommendation < ActiveRecord::Base

    # FIXME: Drop airtable_record_id after this ignored change rolls
    self.ignored_columns = %w(airtable_record_id)

    belongs_to :career_profile

    # need to include options hash param to support Zapier
    def as_json(options = {})
        hash = {
            id: self.id,
            created_at: self.created_at.to_timestamp,
            updated_at: self.updated_at.to_timestamp,
            career_profile_id: self.career_profile_id,
            email: self.email,
            contacted: self.contacted
        }.stringify_keys

        hash
    end

end
