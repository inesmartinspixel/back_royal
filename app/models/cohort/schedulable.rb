module Cohort::Schedulable
    extend ActiveSupport::Concern

    included do
        include ActiveSupport::Callbacks

        has_many :elective_lesson_stream_locale_packs, :through => :access_groups, :source => :lesson_stream_locale_packs
        validate :validate_required_stream_not_included_twice_in_schedule
        validate :validate_stream_not_included_twice_in_period
        validate :validate_stream_entries
        validate :validate_additional_specialization_playlists_to_complete_equals_specialization_playlist_pack_ids
        validate :validate_no_overlap_between_specialization_and_required_playlists
        validate :validate_number_of_playlist_collections
        validate :validate_no_duplicate_playlist_collection_titles
        validate :validate_no_duplicate_required_playlist_pack_ids_in_playlist_collections
        validate :validate_stripe_plans_and_scholarships
        validate :validate_exercises

        delegate :stays_locked_until_acceptance?,
            :is_paid_cert?,
            :user_can_change_status?,
            :user_can_create_application?,
            :user_can_destroy_application?,
            :supports_reject_with_conversion_to_emba?,
            :supports_graduation_on_complete?,
            :supports_manual_promotion?,
            :supports_reapply?,
            :log_foundations_completed_events?,
            :supports_signup?,
            :supports_scholarship_levels?,
            :supports_recurring_payments?,
            :supports_payments?,
            :supports_schedule?,
            :supports_only_one_time_payment?,
            :supports_registration_deadline?,
            :supports_early_registration_deadline?,
            :supports_invite_to_reapply?,
            :coupon_prefix,
            :valid_coupon_id?,
            :supports_slack_rooms?,
            :requires_enrollment_agreement?,
            :supports_multiple_playlist_collections?,
            :to => :program_type_config
    end

    def program_type_config
        Cohort::ProgramTypeConfig[program_type]
    end

    def institution
        Cohort::ProgramTypeConfig[program_type].institution
    end

    def streams_for_all_locales
        Lesson::Stream.where(locale_pack_id: self.lesson_stream_locale_packs.map(&:id))
    end

    def english_streams
        streams_for_all_locales.where(locale: 'en')
    end

    def lesson_stream_locale_packs
        self.required_lesson_stream_locale_packs + self.elective_lesson_stream_locale_packs
    end

    def required_lesson_stream_locale_packs
        return @required_lesson_stream_locale_packs unless @required_lesson_stream_locale_packs.nil?

        stream_locale_packs = Lesson::Stream::LocalePack.find_by_sql("
            SELECT * FROM lesson_stream_locale_packs
            WHERE
                id IN (
                    SELECT (unnest(stream_entries)->>'locale_pack_id')::uuid
                    FROM playlists
                    WHERE playlists.locale_pack_id IN (
                        SELECT unnest(playlist_pack_ids)
                        FROM #{self.class.name.underscore.pluralize}
                        WHERE #{self.class.name.underscore.pluralize}.id = '#{self.id}'
                    )
                )
        ")

        # http://cha1tanya.com/2013/10/26/preload-associations-with-find-by-sql.html
        ActiveRecord::Associations::Preloader.new.preload(stream_locale_packs, :content_items)

        @required_lesson_stream_locale_packs = stream_locale_packs
        stream_locale_packs
    end

    # Overrides default behavior for required_playlist_pack_ids so we can derive
    # the required playlist locale pack ids for the cohort from playlist_collections
    # rather than the deprecated required_playlist_pack_ids database column.
    def required_playlist_pack_ids
        self.required_playlist_pack_ids_from_playlist_collections
    end

    # FIXME: Once the required_playlist_pack_ids columns have been removed from the database,
    # we can remove this overridden setter method. See https://trello.com/c/Jz1ubPBe.
    def required_playlist_pack_ids=(*args)
        raise "The required_playlist_pack_ids column has been deprecated. Update the required_playlist_pack_ids from playlist_collections instead." unless Rails.env.production?
    end

    def required_playlist_pack_ids_from_playlist_collections
        self.playlist_collections.inject([]) { |memo, playlist_collection| memo + playlist_collection['required_playlist_pack_ids'] }
    end

    def validate_required_stream_not_included_twice_in_schedule
        all_required_stream_pack_ids = (self.periods.map { |period| period["stream_entries"] } || []).flatten.compact.map { |entry| entry['required'] ? entry['locale_pack_id'] : nil }.compact

        all_required_stream_pack_ids.group_by { |id| id }.each do |id, list|
            if list.size > 1
                if stream = Lesson::Stream.where(locale: 'en', locale_pack_id: id).first
                    errors.add(:periods, "includes the stream #{stream.title.inspect} more than once")
                else
                    errors.add(:periods, "includes the stream locale pack id #{id.inspect} more than once")
                end
            end
        end
    end

    def validate_stream_not_included_twice_in_period
        periods.each_with_index do |period, i|
            next unless period['stream_entries']
            stream_pack_ids = period['stream_entries'].map { |entry| entry['locale_pack_id'] }
            if stream_pack_ids.size > stream_pack_ids.uniq.size
                errors.add(:"period_#{i+1}", "has the same stream included twice")
            end
        end
    end

    def validate_stream_entries
        periods.each_with_index do |period, i|
            next unless period['stream_entries']
            period['stream_entries'].each_with_index do |entry, j|
                if entry['locale_pack_id'].blank?
                    errors.add(:"period_#{i+1}_stream_entry_#{j+1}", "has no locale_pack_id")
                end
            end
        end
    end

    def validate_additional_specialization_playlists_to_complete_equals_specialization_playlist_pack_ids

        return unless self.periods.present?

        additional_specialization_playlists_complete = self.periods.pluck("additional_specialization_playlists_complete").compact.inject(0, :+)

        if additional_specialization_playlists_complete > self.specialization_playlist_pack_ids.size
            errors.add(:periods, "requires #{additional_specialization_playlists_complete} specialization playlists but there are only #{self.specialization_playlist_pack_ids.size}")
        end
    end

    def validate_no_overlap_between_specialization_and_required_playlists
        if (required_playlist_pack_ids & specialization_playlist_pack_ids).any?
            errors.add(:specialization_playlist_pack_ids, "must not overlap with required playlist pack ids")
        end
    end

    def validate_number_of_playlist_collections
        errors.add(:playlist_collections, "must only contain one playlist collection") if !self.supports_multiple_playlist_collections? && self.playlist_collections.length > 1
    end

    def validate_no_duplicate_playlist_collection_titles
        if self.playlist_collections.length > 1
            playlist_collection_titles = self.playlist_collections.map { |playlist_collection| playlist_collection['title'] }
            errors.add(:playlist_collections, "must not have duplicate titles") if playlist_collection_titles.length != playlist_collection_titles.uniq.length
        end
    end

    def validate_no_duplicate_required_playlist_pack_ids_in_playlist_collections
        _required_playlist_pack_ids = self.required_playlist_pack_ids_from_playlist_collections
        errors.add(:playlist_collections, "must not have duplicate required playlist pack ids") if _required_playlist_pack_ids.length != _required_playlist_pack_ids.uniq.length
    end

    def validate_stripe_plans_and_scholarships

        return unless stripe_plans.present? || scholarship_levels.present?

        if !stripe_plans && scholarship_levels
            errors.add(:scholarship_levels, "cannot be set without stripe_plans")

        elsif stripe_plans && supports_scholarship_levels? && !scholarship_levels
            errors.add(:stripe_plans, "cannot be set without scholarship_levels")

        else
            stripe_plan_ids = []
            visited_frequencies = {}

            # plan config (id, frequency uniqueness)
            stripe_plans.each do |plan|
                stripe_plan_ids << plan["id"]
                if visited_frequencies[plan["frequency"]]
                    errors.add(:stripe_plans, "can only have 1 type of billing frequency plan per template")
                end
                visited_frequencies[plan["frequency"]] = true
            end

            # scholarship levels (name, plan matching)
            scholarship_levels&.each do |level|
                errors.add(:scholarship_levels, "must be named") unless level["name"].present?
                ["standard", "early"].each do |registration_period|
                    level_plans = level[registration_period]&.keys
                    if !(stripe_plan_ids - level_plans | level_plans - stripe_plan_ids).empty?
                        errors.add(:scholarship_levels, "must supply a coupon value for each plan type and no others")
                    end
                end

            end

        end

    end

    def validate_exercises
        periods.each_with_index do |period, i|
            next unless period['exercises'].present?

            period['exercises'].each_with_index do |exercise, j|
                if exercise['channel'].nil?
                    errors.add(:"period_#{i+1}_exercise_#{j+1}", "must have a channel")
                end
                if exercise['hours_offset_from_end'].nil?
                    errors.add(:"period_#{i+1}_exercise_#{j+1}", "must have a hours_offset_from_end")
                end

                overrides = exercise['slack_room_overrides'] || {}
                has_document_override = !!(overrides.detect { |slack_room_id, entry| entry['document'].present? })
                has_message_override = !!(overrides.detect { |slack_room_id, entry| entry['message'].present? })

                if !has_message_override && exercise['message'].blank?
                    errors.add(:"period_#{i+1}_exercise_#{j+1}", "must have a message")
                end

                _slack_rooms = self.respond_to?(:slack_rooms) ? self.slack_rooms : []

                # If sending distinct messages or documents, each slack room must have one
                _slack_rooms.each do |slack_room|
                    entry = overrides[slack_room.id] || {}

                    if has_message_override && entry['message'].blank?
                        errors.add(:"period_#{i+1}_exercise_#{j+1}", "must have a message for #{slack_room&.title.inspect}")
                    end

                    if has_document_override && entry['document'].blank?
                        errors.add(:"period_#{i+1}_exercise_#{j+1}", "must have a document for #{slack_room&.title.inspect}")
                    end
                end

                overrides.keys.each do |slack_room_id|
                    if !_slack_rooms.map(&:id).include?(slack_room_id)
                        errors.add(:"period_#{i+1}_exercise_#{j+1}", "must not have overrides for a slack room that does not exist")
                    end
                end

            end
        end
    end
end