class Cohort::ProgramTypeConfig

    def self.[](program_type)
        program_type = program_type && program_type.to_sym

        @instances ||= {}

        if !@instances.key?(program_type)
            begin
                klass = self.const_get("#{program_type.to_s.camelize}")
            rescue NameError
                # when, for example, determining the promoted_cohort for a fallback_program_type,
                # demo or instituional could make it all the way into here
                klass = nil
            end
            @instances[program_type] = klass && klass.new
        end

        @instances[program_type]
    end

    def self.program_types
        @program_types ||= Cohort::ProgramTypeConfig::Base.subclasses.map { |klass| klass.program_type }.to_set
    end

    def self.configs
        @configs ||= program_types.map { |program_type| self[program_type] }.to_set
    end

    def self.signup_program_types
        @signup_program_types ||= configs.select(&:supports_signup?).map(&:program_type)
    end

    def self.degree_program_types
        @degree_program_types ||= configs
            .select { |c| c.is_a?(Cohort::ProgramTypeConfig::DegreeProgram) }
            .map(&:program_type)
    end

    class Base

        delegate :program_type, :to => :class

        def self.program_type
            name.split('::').last.underscore
        end

        # In the future if we convert program_type_config to a programs table
        # then this will instead be represented by join records between the institution
        # and the programs that it supports.
        def institution
            Institution.quantic
        end

        def stays_locked_until_acceptance?
            false
        end

        def user_can_change_status?(current_status, new_status)
            change = [current_status, new_status]
            return change == [nil, 'pending'] || (supports_reapply? && change == ['rejected', 'pending'])
        end

        def user_can_create_application?(user)
            true
        end

        def user_can_destroy_application?(status)
            false
        end

        def supports_reject_with_conversion_to_emba?
            false
        end

        def supports_graduation_on_complete?
            false
        end

        def supports_manual_promotion?
            false
        end

        # people can only be deferred in cohorts that have multiple cohorts
        # for the same program type (which is pretty much the opposite of
        # supports_manual_promotion?)
        def supports_deferral?
            !supports_manual_promotion?
        end

        def supports_reapply?
            supports_manual_promotion?
        end

        def supports_business_foundations_playlist?
            false
        end

        def log_foundations_completed_events?(application_status)
            false
        end

        def supports_signup?
            false
        end

        # should a user with this program_type be considered a consumer user?
        # for now at least, this is true for all program types defined here
        def consumer_user?
            true
        end

        def supports_only_one_time_payment?
            false
        end

        def supports_recurring_payments?
            false
        end

        def supports_payments?
            supports_only_one_time_payment? || supports_recurring_payments?
        end

        def coupon_prefix
            nil
        end

        def valid_coupon_id?(coupon_id)
            coupon_prefix.nil? ? true : coupon_id.starts_with?(coupon_prefix)
        end

        def supports_scholarship_levels?
            false
        end

        def supports_schedule?
            false
        end

        def supports_registration_deadline?
            supports_payments? && supports_schedule?
        end

        def supports_early_registration_deadline?
            supports_registration_deadline?
        end

        def supports_invite_to_reapply?
            supports_payments? && supports_admission_rounds?
        end

        def supports_admission_rounds?
            !supports_manual_promotion?
        end

        def supports_accept_on_registration?
            false
        end

        def supports_student_network_access?
            true
        end

        def provides_full_student_network_access?
            false
        end

        def supports_delayed_career_network_access_on_acceptance?
            false
        end

        def supports_career_network_access_on_acceptance?
            false
        end

        def supports_career_network_access_on_airtable_decision?
            false
        end

        def supports_auto_active_playlist_selection_on_acceptance?
            false
        end

        # IMPORTANT: Avoid using this in application logic.
        # We often times create campaigns in Customer.io that are for specific
        # program types, so it may be reasonable to include this in event payloads,
        # but do NOT use this in application logic.
        def is_paid_cert?
            false
        end

        def supports_cohort_level_projects?
            false
        end

        def supports_checking_in_with_inactive_user?
            false
        end

        def fixed_program_title
            nil
        end

        def program_title
            if @program_title.nil?

                # mba and emba have fixed program titles.  Others don't
                @program_title = fixed_program_title ||
                    (Cohort.where(program_type: self.program_type).all_published.first.published_version.title)

                raise "No program title" unless @program_title

            end

            @program_title
        end

        def supports_specializations?
            false
        end

        def supports_slack_rooms?
            false
        end

        def supports_partial_tuition_refund?
            false
        end

        def supports_final_score?
            false
        end

        def requires_english_language_proficiency?
            false
        end

        def requires_enrollment_agreement?
            false
        end

        def supports_multiple_playlist_collections?
            false
        end

        def grading_weights
            # Default, overwritten for EMBA and MBA
            {
                avg_test_score: 0.7,
                average_assessment_score_best: 0.3
            }
        end

        def supports_airtable_sync?
            true
        end

        def supports_isolated_network?
            false
        end

        def calendly_interview_url(asia = false)
            asia ? QuanticRoutes::INVITE_TO_INTERVIEW_URL_ASIA : QuanticRoutes::INVITE_TO_INTERVIEW_URL_FALLBACK
        end
    end

    ################################### Modules

    module ManualPromotion
        def supports_manual_promotion?
            true
        end
    end

    module AdmissionRounds
        def supports_manual_promotion?
            false
        end
    end

    module SupportsSignup
        def supports_signup?
            true
        end
    end

    module Certificate
        include ManualPromotion

        def institution
            Institution.smartly
        end

        def stays_locked_until_acceptance?
            true
        end

        def supports_career_network_access_on_airtable_decision?
            true
        end
    end

    module ProvidesFullStudentNetworkAccess
        def provides_full_student_network_access?
            supports_student_network_access?
        end
    end

    module SupportsSlackRooms
        def supports_slack_rooms?
            true
        end
    end

    module PaidCertificate

        # See Cohort::ProgramTypeConfig::Base#is_paid_cert? method comment
        def is_paid_cert?
            true
        end

        def user_can_change_status?(current_status, new_status)
            ['pre_accepted', 'accepted'].include?(new_status) || super
        end

        def user_can_create_application?(user)
            user.can_purchase_paid_certs?
        end

        def user_can_destroy_application?(status)
            status == 'pre_accepted'
        end

        def supports_only_one_time_payment?
            true
        end

        def coupon_prefix
            "PC"
        end

        def supports_accept_on_registration?
            true
        end

        def supports_cohort_level_projects?
            true
        end

        def supports_checking_in_with_inactive_user?
            true
        end

        def supports_career_network_access_on_airtable_decision?
            true
        end

        # Unlike the EMBA, the paid cert programs are Smartly programs, so the statement
        # descriptor for the Stripe products for these programs should reflect as such.
        def stripe_statement_descriptor
            'Smartly'
        end

        # NOTE: This task is designed to create the prerequisite data on the Stripe environments.
        # It is not designed to change the value of the `stripe_plans` in the Cohort or Template itself.
        def stripe_plan_config
            {
                :"#{program_type}_once_1500" => {
                    nickname: 'Single Payment',
                    amount: 1500 * 100,
                    billing_scheme: "per_unit",
                    currency: "usd",
                    interval: "year",
                    interval_count: 1,
                    metadata: {
                        total_num_required_payments: "1"
                    }
                }
            }
        end

    end

    module DegreeProgram
        include ProvidesFullStudentNetworkAccess

        def log_foundations_completed_events?(application_status)
            ['pending'].include?(application_status)
        end

        def supports_business_foundations_playlist?
            true
        end

        def supports_delayed_career_network_access_on_acceptance?
            true
        end

        def supports_auto_active_playlist_selection_on_acceptance?
            true
        end

        # NOTE: both EMBA and MBA support specializations, but only
        # EMBA makes use of them. This may change in the future.
        def supports_specializations?
            true
        end

        def supports_final_score?
            true
        end

        def requires_english_language_proficiency?
            true
        end

        def requires_enrollment_agreement?
            true
        end
    end

    ################################### Program types

    class Emba < Base
        include DegreeProgram
        include SupportsSignup
        include SupportsSlackRooms

        # Not presently referenced in the EMBA case, but set to match client-side expectations
        def coupon_prefix
            "discount_"
        end

        def supports_scholarship_levels?
            true
        end

        def supports_recurring_payments?
            true
        end

        def supports_schedule?
            true
        end

        def fixed_program_title
            "Quantic EMBA"
        end

        def stripe_statement_descriptor
            "Quantic"
        end

        # NOTE: This task is designed to create the prerequisite data on the Stripe environments.
        # It is not designed to change the value of the `stripe_plans` in the Cohort or Template itself.
        def stripe_plan_config
            {
                emba_800: { # Legacy
                    nickname: 'Monthly Plan',
                    amount: 800 * 100,
                    billing_scheme: "per_unit",
                    currency: "usd",
                    interval: 'month',
                    interval_count: 1,
                    metadata: {
                        sort_key: 1,
                        total_num_required_stripe_payments: "12"
                    }
                },
                emba_875: {
                    nickname: 'Monthly Plan',
                    amount: 875 * 100,
                    billing_scheme: "per_unit",
                    currency: "usd",
                    interval: 'month',
                    interval_count: 1,
                    metadata: {
                        sort_key: 1,
                        total_num_required_stripe_payments: "12"
                    }
                },
                emba_bi_4800: { # Legacy
                    nickname: 'Bi-Annual Plan',
                    amount: 4800 * 100,
                    billing_scheme: "per_unit",
                    currency: "usd",
                    interval: "month",
                    interval_count: 6,
                    metadata: {
                        sort_key: "2",
                        total_num_required_payments: "2"
                    }
                },
                emba_once_9600: {
                    nickname: 'Single Payment',
                    amount: 9600 * 100,
                    billing_scheme: "per_unit",
                    currency: "usd",
                    interval: "year",
                    interval_count: 1,
                    metadata: {
                        sort_key: "3",
                        total_num_required_payments: "1"
                    }
                }
            }
        end

        def supports_partial_tuition_refund?
            true
        end

        # EMBA18+
        # See CohortApplication::FinalScoreHelper for handling of legacy cohorts
        def grading_weights
            {
                avg_test_score: 0.6,
                average_assessment_score_best: 0.1,
                project_score: 0.3
            }
        end

        def supports_isolated_network?
            true
        end
    end

    class Mba < Base
        include DegreeProgram
        include SupportsSignup
        include SupportsSlackRooms

        def supports_schedule?
            true
        end

        def fixed_program_title
            "Quantic MBA"
        end

        # MBA22+
        # See CohortApplication::FinalScoreHelper for handling of legacy cohorts
        def grading_weights
            {
                avg_test_score: 0.7,
                average_assessment_score_best: 0.1,
                project_score: 0.2
            }
        end

        # See https://trello.com/c/xx3g7wUC.
        def calendly_interview_url(asia = false)
            asia ? QuanticRoutes::INVITE_TO_INTERVIEW_URL_ASIA_MBA : QuanticRoutes::INVITE_TO_INTERVIEW_URL_MBA
        end
    end

    class CareerNetworkOnly < Base
        include ManualPromotion
        include SupportsSignup

        def institution
            Institution.smartly
        end

        def supports_career_network_access_on_acceptance?
            true
        end
    end

    # NOTE: The jordanian_math program type is new and we haven't quite flushed out all
    # of its configuration details yet. So, for now, we're just going to give it a minimal
    # config and then we'll come back later and properly configure it.
    class JordanianMath < Base
        include ManualPromotion

        def institution
            Institution.miya_miya
        end

        def supports_multiple_playlist_collections?
            true
        end

        def supports_student_network_access?
            false
        end

        def supports_airtable_sync?
            false
        end

        def consumer_user?
            false
        end
    end

    class TheBusinessCertificate < Base
        include Certificate
        include SupportsSignup

        def supports_reject_with_conversion_to_emba?
            true
        end

        def supports_graduation_on_complete?
            true
        end

        def log_foundations_completed_events?(application_status)
            ['pending', 'pre_accepted'].include?(application_status)
        end

        def supports_business_foundations_playlist?
            true
        end

        def supports_final_score?
            true
        end
    end

    class PaidCertDataAnalytics < Base
        include Certificate
        include PaidCertificate
    end

    class PaidCertCompetitiveStrategy < Base
        include Certificate
        include PaidCertificate
    end

    class PaidCertEntrepreneurialStrategy < Base
        include Certificate
        include PaidCertificate
    end

    class PaidCertEssentialsOfManagement < Base
        include Certificate
        include PaidCertificate
    end

    class PaidCertFinancialAccounting < Base
        include Certificate
        include PaidCertificate
    end

    class PaidCertMarketingBasics < Base
        include Certificate
        include PaidCertificate
    end

    class PaidCertOperationsManagement < Base
        include Certificate
        include PaidCertificate
    end

    if Rails.env.test?

        # there are some fixtures that use 'test' as the program_type
        class Test < Base; end

    end

end