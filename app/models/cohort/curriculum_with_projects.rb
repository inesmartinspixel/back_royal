module Cohort::CurriculumWithProjects
    extend ActiveSupport::Concern

    included do |target|
        target.validate :validate_project_periods
        target.validate :validate_no_duplicate_requirement_identifiers
    end

    def all_learner_project_ids
        (learner_project_ids || []) + periods.map { |p| p['learner_project_ids'] }.flatten.compact.uniq
    end

    def learner_projects
        LearnerProject.find_cached_by_id(all_learner_project_ids)
    end

    def validate_project_periods
        periods&.each_with_index do |period, index|
            next unless period['style'] == 'project'
            if period['project_style'].nil?
                errors.add("period_#{index+1}", "must have a project style")
            end
        end
    end

    def validate_no_duplicate_requirement_identifiers
        learner_projects.group_by(&:requirement_identifier).each do |requirement_identifier, projects|
            if projects.size > 1
                errors.add(:learner_projects, "include two different projects for #{requirement_identifier.inspect}")
            end
        end
    end
end