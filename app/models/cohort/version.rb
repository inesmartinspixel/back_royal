# == Schema Information
#
# Table name: cohorts_versions
#
#  version_id                              :uuid             not null, primary key
#  operation                               :string(1)        not null
#  version_created_at                      :datetime         not null
#  id                                      :uuid             not null
#  created_at                              :datetime         not null
#  updated_at                              :datetime         not null
#  start_date                              :datetime         not null
#  end_date                                :datetime
#  was_published                           :boolean          default(FALSE)
#  name                                    :text
#  specialization_playlist_pack_ids        :uuid             default([]), is an Array
#  periods                                 :json             is an Array
#  program_type                            :text
#  num_required_specializations            :integer          default(0), not null
#  title                                   :text
#  description                             :text
#  admission_rounds                        :json             is an Array
#  registration_deadline_days_offset       :integer
#  external_schedule_url                   :text
#  program_guide_url                       :text
#  enrollment_deadline_days_offset         :integer
#  stripe_plans                            :json             is an Array
#  scholarship_levels                      :json             is an Array
#  internal_notes                          :text
#  project_submission_email                :string
#  early_registration_deadline_days_offset :integer
#  id_verification_periods                 :json             is an Array
#  graduation_days_offset_from_end         :integer
#  diploma_generation_days_offset_from_end :integer
#  learner_project_ids                     :uuid             is an Array
#  version_editor_id                       :uuid
#  version_editor_name                     :text
#  enrollment_agreement_template_id        :text
#  playlist_collections                    :json             is an Array
#  isolated_network                        :boolean
#

class Cohort::Version < Cohort

    include(ContentItemVersionMixin)

    self.primary_key = 'version_id'
    self.table_name = 'cohorts_versions'

    belongs_to :content_publisher, :class_name => 'ContentPublisher', :primary_key => :cohort_version_id, :foreign_key => :version_id
    belongs_to :cohort, :foreign_key => 'id', :primary_key => 'id'

    # See cohort.rb#cached_published where we explicitly preload the associated cohort
    # so it will be cached along with the published cohort
    delegate :published_at, :published_version_id, :to => :cohort

    def old_version
        @old_version || false
    end

    def old_version=(val)
        @old_version = !!val
    end

    def from_versions_table?
        true
    end

end
