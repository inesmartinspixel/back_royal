# The MdpSuccessPredictor uses a Markov Decision Process to predict the chance of success
# for a particular user or the graduation rate for a cohort.  Unlike most MDP's, there are
# no actions here, since we are just trying to predict values, not trying to decide what
# to do.
#
# The prime advantage of the MDP approach over a simple regression is that it can learn
# faster from new data.
#
# The simple regression approach works like this:  Look at a particular place in the
# curriculum, like halfway to the midterm.  For all users from completed cohorts who have
# either failed or graduated, create a vector with the features required_lessons_behind and
# previously_deferred, graduated.  Run a logit regression to predict graduated based on the
# two independent variables.
#
# This works just fine, but we cannot include users until well
# after a cohort is finished, since many users will defer and we won't know until later
# whether or not they will graduate. The MDP, by contrast,  can learn from cohorts as they are going on.
#
# A great place to learn about MDPs is Chapter 9: Planning Under Uncertainty in
# Udacity's Intro to AI course: https://classroom.udacity.com/courses/cs271
class Cohort

    class MdpSuccessPredictor
        attr_reader :similar_period_groups, :cohorts, :similar_periods_group_for_period,
                    :period_ends, :periods_by_period_end, :cohorts_by_id,
                    :user_states, :periods_by_cohort_and_index, :mdp_states, :results,
                    :program_type

        def self.everything_up_to_date_in_db?(program_type)
            now = get_now
            data_persisted_up_to = CohortPeriodGraduationRatePrediction
                                        .joins(:cohort)
                                        .where(cohorts: {program_type: program_type})
                                        .maximum(:period_end)
            return false if data_persisted_up_to.nil?
            last_period_end = PublishedCohortPeriod.where("period_end < ?", now).maximum(:period_end)
            last_period_end < data_persisted_up_to
        end

        def self.get_now
            # On prod, this could be Time.now, but in local development
            # with a dump that might not be totally up-to-date, this is helpful
            LessonProgress.maximum(:created_at)
        end

        def self.run_and_persist_if_necessary(program_type = nil)
            if program_type.nil?
                return [
                    self.run_and_persist_if_necessary('mba'),
                    self.run_and_persist_if_necessary('emba')
                ].any?
            end
            return false if self.everything_up_to_date_in_db?(program_type)
            self.new(program_type).run_and_persist
            true
        end

        def self.clear_data(program_type)
            CohortPeriodGraduationRatePrediction
                .joins(:cohort)
                .where(cohorts: {program_type: program_type}).delete_all
            UserChanceOfGraduatingRecord
                .where(program_type: program_type)
                .delete_all
        end

        def initialize(program_type)
            @program_type = program_type
            @mdp_states = {}
            load_data
            create_similar_period_groups
            @results = []
        end

        # This can be helpful sometimes in development, but isn't
        # used in production at all
        def run_and_write_to_csv
            determine_results
            write_results_to_csv
            nil
        end

        def run_and_persist
            old_logger = ActiveRecord::Base.logger
            determine_results
            ActiveRecord::Base.logger = nil
            result = write_results_to_db
            re_identify_users(result[:user_ids_to_identify])
            nil
        ensure
            ActiveRecord::Base.logger = old_logger
        end

        def determine_results
            step_through_each_period_and_write_result_rows
            write_final_result_rows
        end

        def step_through_each_period_and_write_result_rows
            # Starting from the beginning of time, step through each
            # time at which at least one period ended
            period_ends.each_with_index do |period_end, i|

                # For each user state, step through each cohort_status_change and
                # cohort_user_period up to period_end, updating mdp_states as users pass in and
                # out of different mdp_states.
                errored_user_states = []
                user_states.map do |user_state|
                    begin
                        user_state.fast_forward(period_end)
                    rescue Exception => err
                        Raven.capture_in_production(RuntimeError) do
                            raise
                        end
                        errored_user_states << user_state
                    end
                end
                errored_user_states.each do |user_state|
                    user_states.delete(user_state)
                end

                # This is the MDP magic.  We back-propagate values throughout
                # the various states in the MDP until all of the values converge.
                # In each iteration, if we find at least one state that changes
                # value, then we do another iteration.  Once we have an iteration where
                # nothing changes, we stop.
                found_diff = true
                j = 0
                start = Time.now
                while found_diff && Time.now - start < 5.seconds
                    found_diff = false
                    diffs = []
                    self.mdp_states.values.each_with_index do |mdp_state, k|
                        diff = mdp_state.recalculate_value
                        diffs << diff
                        if diff > 1e-10
                            found_diff = true
                        end
                    end
                    # puts "#{j} #{diffs.inject(0.0) { |sum, v| sum + v }.to_f / diffs.size}"
                    j += 1
                end
                if found_diff
                    raise "Did not converge after 5 seconds"
                end

                # Now that all of the user_states are updated up to period_end
                # and the values on all of the mdp_states have been trained on
                # the current state of all of the users, we can make predictions
                # for each cohort at this point in time.  Each user is in some particular
                # mdp_state, and the value of that state indicates that user's chance
                # of graduating.  By aggregating these, we can figure a predicted chance
                # of success for each cohort at this point in time.
                periods_by_period_end[period_end].each do |period|
                    attrs = {
                        index: period.index,
                        normalized_index: period.normalized_index,
                        period_end: period.period_end,
                        similar_periods: similar_periods_group_for_period[period].periods.select { |p| p.cohort.start_date < period.cohort.start_date }.sort_by { |p| p.cohort.start_date }
                    }
                    write_result_row_for_cohort(period.cohort, attrs)

                end

                puts "#{(100*i.to_f/period_ends.size).round}% complete" if i % 1 == 0
            end
        end

        def write_final_result_rows
            # Write one more result for the final state of each period.  This
            # matters because users continue to graduate or fail after the end
            # of the last period in a cohort.  This last record shows us
            # where that cohort is now.
            cohorts.each do |cohort|
                next if cohort.name == 'MBA1'
                next if cohort.enrollment_deadline > now
                write_result_row_for_cohort(cohort, {
                    index: -1, # set it to something so we can have a unique index
                    normalized_index: 'final_result',
                    period_end: now
                })
            end
        end

        def write_result_row_for_cohort(cohort, attrs)
            # Find all of the users from this cohort and determine which mdp_state they are currently in
            # The compact is almost always unnecessary, but if there is a user who was added to this
            # cohort after the enrollment deadline who had never been previously accepted into a cohort,
            # then they would not yet have an mdp_state.  The only place I've seen this is with a test
            # user on staging.
            user_states_for_cohort = user_states.select { |u| u.original_cohort_id == cohort['id'] }
            mdp_states = user_states_for_cohort.map(&:mdp_state).compact

            # average the value of each mdp_state to get the expected graduation rate for the cohort
            expected_graduation_rate = mdp_states.inject(0.0) { |sum, mdp_state| sum + mdp_state.value }.to_f / mdp_states.size

            # write a new row
            new_row = {
                cohort_id: cohort['id'],
                cohort_name: cohort.name,
                chance_of_success: expected_graduation_rate,
                cohort_end: cohort.end_date,
                num_enrolled: mdp_states.size,
                num_graduated: mdp_states.count { |s| s.status == 'graduated' },
                num_failed: mdp_states.count { |s| s.status == 'failed' },
                num_active: mdp_states.count { |s| s.status == 'enrolled' || s.status == 'deferred' }
            }.merge({
                index: attrs[:index],
                normalized_index: attrs[:normalized_index],
                period_end: attrs[:period_end],
                cohorts_with_similar_periods_count: attrs[:similar_periods]&.map(&:cohort)&.uniq&.size,
                similar_periods_count: attrs[:similar_periods]&.size,
                similar_periods: attrs[:similar_periods]&.map(&:inspect)
            })

            if new_row[:normalized_index] == 'final_result'
                current_position = nil

                # find each existing row for this same cohort
                results.reverse.each do |result_row|
                    if result_row[:cohort_name] == cohort.name
                        current_position = result_row[:normalized_index] if current_position.nil?

                        # add the final value and the error to each previous row
                        result_row[:final_result] = expected_graduation_rate
                        result_row[:error] = result_row[:chance_of_success] - expected_graduation_rate

                        result_row[:current_position] = current_position
                    end
                end

                new_row[:current_position] = current_position
            end


            results << new_row
        end

        def write_results_to_csv
            Dir.mktmpdir do |tmpdir|
                filepath = Rails.root.join('tmp', 'cohort_graduation_predictions.mdp.csv')

                # write training data to local file
                CSV.open(filepath, "wb", {
                    write_headers: true,
                    headers: results.map(&:keys).flatten.uniq.sort
                }) do |csv|
                    results.each do |result|
                        csv << result
                    end
                end

                puts "Data written to #{filepath}"
            end
        end

        def write_results_to_db
            user_ids_to_identify = []

            RetriableTransaction.transaction do
                original_chances_of_graduating = UserChanceOfGraduatingRecord.all.index_by(&:user_id)

                # we could probably make this a bit faster by not replacing every
                # record, but there are some updates even to existing records (i.e. error),
                # so it's simpler this way, and plenty fast for now
                MdpSuccessPredictor.clear_data(program_type)

                puts "writing #{CohortPeriodGraduationRatePrediction.table_name}"
                results.each do |result|
                    CohortPeriodGraduationRatePrediction.find_or_create_by(cohort_id: result[:cohort_id], index: result[:index]) do |record|
                        result.each do |k, v|
                            record[k] = v
                        end
                    end
                end


                puts "writing #{UserChanceOfGraduatingRecord.table_name}"
                user_states.each do |user_state|
                    next if user_state.chance_of_graduating.nil?
                    UserChanceOfGraduatingRecord.find_or_create_by(user_id: user_state.user_id, program_type: user_state.program_type) do |record|
                        record.chance_of_graduating = user_state.chance_of_graduating
                        original_chance = original_chances_of_graduating[user_state.user_id]&.chance_of_graduating
                        if original_chance.nil? || (record.chance_of_graduating - original_chance).abs > 0.001
                            user_ids_to_identify << user_state.user_id
                        end
                    end
                end

            end

            {
                user_ids_to_identify: user_ids_to_identify
            }
        end

        def re_identify_users(user_ids)
            puts "identifying #{user_ids.size} users"

            # FIXME: we could save some time here if we didn't load up all of the columns.
            # We probably just need the user_id, but the IdentifyUserJob is asking for
            # a whole user and it's hard to know exactly how it is being serialized in there
            User.where(id: user_ids).find_each(&:identify)
        end

        def get_mdp_state(period, status, lessons_behind_bucket, previously_deferred)

            similar_periods_group = self.similar_periods_group_for_period[period]

            # if the user has graduated or failed, we don't care about anything
            if ['graduated', 'failed'].include?(status)
                lessons_behind_bucket = nil
                previously_deferred = nil
                similar_periods_group = nil
            end

            features = [similar_periods_group, status, lessons_behind_bucket, previously_deferred]
            unless self.mdp_states[features]
                self.mdp_states[features] = MdpState.new(*features)
            end
            self.mdp_states[features]
        end

        def create_similar_period_groups
            # Go through each relevant period and group them into similar_period_groups.
            # See similar_period_group.rb for more information.  There will be one
            # group for each unique combination of [curriculum_position, required_lessons_bucket],
            # where curriculum_position is something like 'first_half_before_midterm' (meaning that
            # we are in the first half of the part of the curriculum that comes before the midterm)
            # and required_lessons_bucket is something like 200 (meaning that around 200 lessons were
            # required up to and including this period)
            similar_period_groups = {}
            @similar_periods_group_for_period = {}

            cohorts.reject(&:mba1?).map(&:published_cohort_periods).flatten.each do |period|
                features = [period.cohort.program_type, period.curriculum_position, period.required_lessons_bucket]
                if similar_period_groups[features].nil?
                    similar_period_groups[features] = SimilarPeriodGroup.new(*features)
                end
                similar_period_groups[features] << period
                @similar_periods_group_for_period[period] = similar_period_groups[features]
            end

            @similar_period_groups = similar_period_groups.values
        end

        def now
            @now ||= MdpSuccessPredictor.get_now
        end


        # This method pulls things out of the database and organizes them
        # into maps and things.
        def load_data
            load_cohorts
            load_periods
            create_user_states
        end

        def load_cohorts
            # get all of the published cohorts
            @cohorts = Cohort.all_published
                        .where(program_type: self.program_type)
                        .reorder(:start_date) # needed for similar_periods
                        .includes(:published_versions).
                        map(&:published_version).to_a

            # create a map keyed off of the id of each cohort
            @cohorts_by_id = self.cohorts.index_by { |c| c['id']}

            # preload some things
            ActiveRecord::Associations::Preloader.new.preload(cohorts, [
                :published_versions, # even though these are published versions, we also preload these because associated objects will reference cohort.published_version
                :published_cohort_periods,
            ])
            cohorts.each do |cohort|
                cohort.published_cohort_periods.each do |period|
                    period.cohort = cohort
                end
            end
        end

        def load_periods
            # create a map of periods, keyed off of cohort_id and index
            @periods_by_cohort_and_index = cohorts.map(&:published_cohort_periods).flatten.index_by do |period|
                [period.cohort_id, period.index]
            end

            # group periods by when they end, so we can easily grab, for example,
            # all periods ending on 2017/03/07
            @periods_by_period_end = cohorts.reject(&:mba1?).map(&:published_cohort_periods)
                                        .flatten
                                        .reject(&:before_enrollment_deadline?)
                                        .reject { |p| p.period_end > now }
                                        .group_by(&:period_end)

            self.periods_by_period_end.each do |period_end, periods|
                @periods_by_period_end[period_end] = periods.sort_by { |period| period.cohort.start_date }
            end

            # get a list of all the unique times at which a period ends
            @period_ends = self.periods_by_period_end.keys.sort
        end

        def create_user_states

            # Matt Schenck apparently tests all sorts of things with his own
            # user in prod, so that guy's history is crazy.  He's been in every
            # cohort and switched back and forth between statuses in an unreasoable
            # way
            matt_shenck_user = 'a039b7f4-4f4b-4ec9-8239-b453ede5cef8'

            # create a UserState object for each user who has ever enrolled in a cohort (see user_state.rb for more info)
            @user_states = []
            mba1 = cohorts.detect(&:mba1?)
            ProgramEnrollmentProgressRecord
                .where(program_type: program_type)
                .where.not(original_cohort_id: mba1 && mba1['id'])
                .where.not(user_id: matt_shenck_user)
                .each do |enrollment_record|

                    user_states << UserState.new(self, enrollment_record)
                end

            # A record in the cohort_user_periods view keeps track of a user's progress at the
            # end of a particular period.  Load up all of the cohort_user_periods for the users
            # we are interested in.  Set the published_cohort_period on each one using the
            # published_cohort_period objects we've already loaded up (basically doing some manual preloading here)
            cohort_user_periods = CohortUserPeriod.where(user_id: user_states.map(&:user_id)).each do |cohort_user_period|
                cohort_user_period.published_cohort_period = periods_by_cohort_and_index[[cohort_user_period.cohort_id, cohort_user_period.index]]
            end.group_by(&:user_id)

            # A record in the cohort_status_changes view keeps track of any time a user's cohort
            # status changed.  Load up all of the cohort_status_changes for users we are interested
            # in.
            cohort_status_changes = CohortStatusChange.where(user_id: user_states.map(&:user_id)).group_by(&:user_id)

            user_states.each do |user_state|
                user_id = user_state.user_id
                _cohort_status_changes = cohort_status_changes[user_id] || []

                # Store the cohort_user_periods for each user on the user_state for that user
                user_state.cohort_user_periods = cohort_user_periods[user_id].index_by(&:published_cohort_period)

                # Store the cohort_status_changes for each user on the user_state for that user.
                # Also sort the records and set the cohort on each one (more manual preloading)
                user_state.cohort_status_changes = _cohort_status_changes.select { |cohort_status_change|
                    cohort_status_change.cohort = cohorts_by_id[cohort_status_change.cohort_id]
                    cohort_status_change.cohort && cohort_status_change.cohort.program_type == user_state.program_type
                }.sort { |a, b|
                    if a.from_time == b.from_time
                        a.cohort.start_date <=> b.cohort.start_date
                    else
                        a.from_time <=> b.from_time
                    end
                }
            end
        end

    end

end