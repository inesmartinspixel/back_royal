# An instance of this class represents one state that a user can be in in our
# MDP view of the world.  A state is defined by a few features:
#
# 1. similar_period_group (i.e., any period from any cohort that is
#       in the first half of the curriculum before the midterm and that
#       has around 120 required lessons)
# 2. status (graduated, did_not_graduate, enrolled, deferred)
# 3. lessons_behind_bucket (i.e. -25, indicating that the user is 1-25 lessons behind)
# 4. previously_deferred (false if the user is still in the cohort ze first enrolled in.  true if ze
#       has moved into a later cohort)
class Cohort::MdpSuccessPredictor::MdpState

    attr_accessor :similar_period_group, :status, :lessons_behind_bucket, :previously_deferred, :subsequent_states,
                    :score, :value, :users_who_passed_through_state

    Dummy = Struct.new(:value) do
    end

    def inspect
        [
            self.class.name,
            similar_period_group&.identifier,
            "#{status}/#{lessons_behind_bucket}/#{previously_deferred ? 'previously_deferred' : ''}"
        ].compact.join(' ')
    end

    def initialize(similar_period_group, status, lessons_behind_bucket, previously_deferred)
        @similar_period_group = similar_period_group
        @status = status
        @lessons_behind_bucket = lessons_behind_bucket
        @previously_deferred = previously_deferred

        # We keep a list of the MdpStates that users passed into after being in
        # this state.  This list starts off with a few dummy states in it (see below).
        # It never has more than 200 states in it, once more than 200 users have passed
        # through this state, we start to forget the old ones.
        @subsequent_states = []
        @users_who_passed_through_state = Set.new # just used for debugging

        # initial value does not matter.  It will change the first time
        # that the MdpSuccessPredictor iterates through the states to determine
        # values.  The only difference is how many iterations it takes to converge.
        @value = 0.5

        # Someone who is deferred now should always be considered previously_deferred.  Just
        # adding a check here to make sure that is always the case, and that a mistake
        # somehere else didn't cause that to get passed in.
        raise "Unexpected" if status == 'deferred' && !previously_deferred

        # For failed and graduated, we know the values (1 and 0).  For any
        # other state, we need to set some reasonable initial value before
        # any users have passed through this state.  We do this by running
        # regressions on states kind of like this one.  Once we have an initial
        # value, we pretend that some users have passed through this state with that
        # value.  That way, the value doesn't go all the way down to 0 or all the way
        # up to 1 when the first person passes through this state.
        if !['failed', 'graduated'].include?(status)
            raise "need an initial_value" unless initial_value
            dummy_state = Dummy.new(initial_value)
            2.times do
                subsequent_states << dummy_state
            end
        end

    end

    def recalculate_value
        old_value = @value

        # to save time, hardcode the values for the graduated and failed states
        if self.status == 'graduated'
            self.value = 1
        elsif self.status == 'failed'
            self.value = 0
        else

            # subsequent_state_values is a list (with duplicates) of the states that
            # users who passed through this state entered.  The value of this state is
            # the average value of all those subsequent states.  This is set up a little
            # different from how you might normally see MDP values defined as the sum of
            # subsequent states multiplied by probabilities, but mathematically it is the same.
            subsequent_state_values = self.subsequent_states.map(&:value)
            self.value =  subsequent_state_values.inject(0.0) { |sum, v| sum + v }.to_f / subsequent_state_values.size
        end

        (old_value - self.value).abs
    end

    # Whenever a user passes from this state into another,
    # this method is called
    def user_passed_into_state(subsequent_state)
        subsequent_states << subsequent_state
        while subsequent_states.size > 200
            subsequent_states.shift
        end
        nil
    end

    def initial_value
        if self.similar_period_group.program_type == 'emba'
            initial_value_emba
        else
            initial_value_mba
        end
    end

    # just used for debugging
    def actual_result(cohort = nil, format = :float)
        enrollment_records = users_who_passed_through_state.map(&:enrollment_record).select { |r| cohort.nil? || r.original_cohort_id == cohort['id']}
        results = enrollment_records.map do |r|
            {
                'graduated' => 1,
                'did_not_graduate' => 0
            }[r.final_status]
        end
        active_count = results.size - results.compact.size
        completed_results = results.compact
        grad_rate = completed_results.size > 0 ? completed_results.inject(0.0) { |sum, v| sum + v }.to_f / completed_results.size : 0
        format == :string ? "#{(100*grad_rate).round}% (#{active_count} of #{results.size} incomplete" : grad_rate
    end

    def initial_value_emba
        # We generate these initial values by doing the following
        # - look at all users from completed cohorts who are
        #   marked as graduated/did_not_graduate
        # - group their cohort_user_periods by the next exam and
        #   the lessons_behind_bucket (i.e. 100 lessons behind in the
        #   weeks before exam_4)
        # - take the average graduation rate from each group
        #
        # There is a discussion at the top of mdp_success_predictor.rb about why
        # we can't use this strategy overall to predict success.  For EMBA, it might
        # actually work since the curriculum has changed less than mba has.  But you're
        # still ignoring deferred people until their status changes.
        #
        # The query for generating these values is saved at `r/mdp_success_predictor/emba_initial_values.sql`
            defaults = {
                'exam_1' => {
                    0 =>0.92,
                    -25 =>0.44,
                    -50 =>0.15,
                    -75 =>0.32,
                    -100 =>0.03
                },
                'exam_2' => {
                    0 =>0.95,
                    -25 =>0.77,
                    -50 =>0.19,
                    -75 =>0.15,
                    -100 =>0.03
                },
                'exam_3' => {
                    0 =>0.95,
                    -25 =>0.86,
                    -50 =>0.23,
                    -75 =>0.1,
                    -100 =>0.04
                },
                'exam_4' => {
                    0 =>0.97,
                    -25 =>0.92,
                    -50 =>0.71,
                    -75 =>0.2,
                    -100 =>0.07
                },
                'exam_5' => {
                    0 =>0.97,
                    -25 =>0.90,
                    -50 =>0.93,
                    -75 =>0.5,
                    -100 =>0.08,
                    -150 =>0.01
                },
                'exam_6' => {
                    0 =>0.97,
                    -25 =>0.97,
                    -50 =>0.78,
                    -75 =>0.67,
                    -100 =>0.12,
                    -150 =>0.01
                },
                'exam_7' => {
                    0 =>0.97,
                    -25 =>0.71,
                    -50 =>0.75,
                    -75 =>0.5,
                    -100 =>0.14,
                    -150 =>0.01
                },
                'exam_8' => {
                    0 =>0.98,
                    -25 =>0.57,
                    -50 =>0.75,
                    -75 =>0.5,
                    -100 =>0.17,
                    -150 =>0.01
                }
            }
            defaults['enrollment_deadline'] = defaults['exam_1']
            defaults['after_last_exam'] = defaults['after_cohort_end'] = defaults['exam_8']


            defaults_for_position = nil
            defaults.each do |exam_key, _defaults|
                if similar_period_group.curriculum_position.match(exam_key)
                    defaults_for_position = _defaults
                end
            end
            raise "No defaults for position" unless defaults_for_position
            defaults_for_position.keys.sort.each do |bucket|
                if bucket >= lessons_behind_bucket
                    return defaults_for_position[bucket]
                end
            end
    end

    def initial_value_mba
        # These coefficients come from running regressions
        # on all previous data from particular curriculum_positions.
        # See the comment at the top of mdp_success_predictor.rb for
        # why we can't just use this approach for all of our predicting.
        #
        # The coefficients have been updated with date up through MBA9
        #
        # See /r/mdp_success_predictor/results_by_position_and_behind.R to see how these models were generated
        coefficients = {
            enrollment_deadline: {
                intercept: 1.20376,
                previously_deferred: -1.62482,
                lessons_behind: 0.02662
            },
            before_enrollment_deadline: {
                intercept: 1.34687,
                previously_deferred: -1.74860,
                lessons_behind:  0.03791
            },
            first_half_before_midterm: {
                intercept: 2.102541,
                previously_deferred: -1.431517,
                lessons_behind: 0.034961
            },
            halfway_to_midterm: {
                intercept: 2.110968,
                previously_deferred: -1.416850,
                lessons_behind: 0.032973
            },
            second_half_before_midterm: {
                intercept:  2.790001,
                previously_deferred: -1.340784,
                lessons_behind: 0.030508
            },
            midterm_review: {
                intercept: 2.473807,
                previously_deferred: -1.175511,
                lessons_behind: 0.032013
            },
            midterm: {
                intercept: 2.245983,
                previously_deferred: -1.005277,
                lessons_behind:0.036149
            },
            first_half_before_final: {
                intercept: 3.570414,
                previously_deferred: -0.467461,
                lessons_behind: 0.030505
            },
            halfway_to_final: {
                intercept: 3.629781,
                previously_deferred: -0.371990,
                lessons_behind:  0.029225
            },
            second_half_before_final: {
                intercept: 4.578350,
                previously_deferred:-0.102047,
                lessons_behind: 0.025472
            },
            final_review: {
                intercept: 4.035880,
                previously_deferred: 0.228864,
                lessons_behind:  0.024840
            },
            final_exam: {
                intercept: 3.596106,
                previously_deferred: 1.059629,
                lessons_behind: 0.026618
            },

            # duplicate of final_exam
            after_cohort_end: {
                intercept: 3.596106,
                previously_deferred: 1.059629,
                lessons_behind: 0.026618
            }
        }.fetch(similar_period_group.curriculum_position.to_sym)

        t = coefficients[:intercept] +
             coefficients[:previously_deferred] * (previously_deferred ? 1 : 0) +
             coefficients[:lessons_behind] * lessons_behind_bucket

        v = 1/(1+Math.exp(-t))

        # puts "#{(100*v).round}% Initial value for #{similar_period_group.curriculum_position}/#{previously_deferred}/#{lessons_behind_bucket}"
        v
    end

end