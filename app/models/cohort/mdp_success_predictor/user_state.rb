# An instance of this class represents the state of a user as it changes over time.  It is responsible
# for determining when the user transitions from one mdp state to another.
class Cohort::MdpSuccessPredictor::UserState

    attr_accessor :cohort_status_changes,  :enrollment_record, :original_cohort, :time, :mdb_success_predictor,
                    :previously_deferred, :cohort_status_change_index, :status, :last_enrolled_cohort, :score,
                    :cohort_user_periods, :mdp_state

    delegate :user_id, :original_cohort_id, :program_type, :to => :enrollment_record

    class CannotFastForward < RuntimeError

        def initialize(user_state, cohort_status_change, message)
            super(message)
            user = User.find(user_state.user_id)
            self.raven_options = {
                extra: {
                    cohort_status_change: cohort_status_change.as_json
                },
                user: {
                    id: user_state.user_id,
                    email: user.email
                }
            }
        end

    end

    def initialize(mdb_success_predictor, enrollment_record)
        self.mdb_success_predictor = mdb_success_predictor
        self.enrollment_record = enrollment_record
        self.original_cohort = mdb_success_predictor.cohorts_by_id.fetch(enrollment_record.original_cohort_id)
        self.time = Time.at(0)
        self.cohort_status_change_index = 0
        self.status = nil
        self.previously_deferred = false
        self.last_enrolled_cohort = nil
    end

    def chance_of_graduating
        mdp_state&.value
    end

    # This method starts from the state of a user at a particular time and
    # then steps throuch cohort_status_changes and cohort_user_periods to determine
    # how the user transitioned between mdp states up until the time passed in.
    #
    # When this method is complete it leaves the instance in the state of the
    # user at the time passed in
    def fast_forward(time)
        previous_status = @status

        # you can enter a user's id here in order to activate a bunch of console logs below
        debug_user_id = nil
        @time = time

        # once someone has graduated, we don't need to know anything else.
        # That's not the case with expelled, etc., as they could enter a
        # later cohort
        return if @status == 'graduated'


        puts "------ #{time}" if user_id == debug_user_id

        # step through cohort status changes until we are up to the time that was passed in
        while cohort_status_changes[@cohort_status_change_index] && cohort_status_changes[@cohort_status_change_index].from_time < time
            cohort_status_change = cohort_status_changes[@cohort_status_change_index]
            self.cohort_status_change_index += 1
            raw_status = cohort_status_change.status

            puts [cohort_status_change.cohort.name, cohort_status_change.from_time, cohort_status_change.until_time, cohort_status_change.cohort_id, previous_status, raw_status].inspect if user_id == debug_user_id

            # keep track of which cohort was most recently marked as enrolled (i.e., the last
            # cohort they were enrolled in)
            if raw_status == 'enrolled'
                # sanity check.  Make sure that the first cohort we see them enroll in
                # matches the enrollment_record
                if last_enrolled_cohort.nil? && cohort_status_change.cohort_id != original_cohort_id
                    raise CannotFastForward.new(self, cohort_status_change, "Initial enrollment is for an unexpected cohort. #{cohort_status_change.cohort_id.inspect} is not #{original_cohort_id.inspect}")
                end
                self.last_enrolled_cohort = cohort_status_change.cohort
            end

            # we are not interested in anything that happens before they first enroll
            next unless last_enrolled_cohort

            # once we have enrolled in a later cohort, we don't care about any status changes
            # for a previous cohort.
            next if cohort_status_change.cohort.start_date < last_enrolled_cohort.start_date

            # Most of the time, users are marked as deferred when they get deferred.  However,
            # if a user shows up in another cohort after being enrolled, then we count them as also
            # deferred.  This happens every now and then when we expel a user and then later let
            # them back in to another cohort.
            if raw_status == 'deferred' || cohort_status_change.cohort_id != original_cohort_id
                self.previously_deferred = true
            end

            # If a user is marked as expelled, failed, or rejected, then we assume they
            # are not going to graduate (even though it is possible that they will)
            if ['expelled', 'failed', 'rejected'].include?(raw_status)
                new_status = 'failed'

            # If a user is deferred and then is placed in a new cohort and expelled
            # before the enrollment deadline, we assume they are not going to graduate
            elsif ['deferred', 'failed'].include?(previous_status) && raw_status == 'did_not_enroll'
                new_status = 'failed'

            # If a user is deferred and then is put into a new cohort that has not yet started,
            # we count them as still deferred until they actually enroll in the new cohort
            elsif ['deferred', 'failed'].include?(previous_status) && ['pending_acceptance', 'pending_enrollment'].include?(raw_status)
                new_status = 'deferred'
            elsif ['graduated', 'enrolled', 'deferred'].include?(raw_status)
                new_status = raw_status
            elsif raw_status == 'pending_enrollment'
                self.previously_deferred = true
                new_status = raw_status
            else
                raise CannotFastForward.new(self, cohort_status_change, "status_change did not match any of the expected states")
            end

            previous_status = new_status

            # Now the only available values for new_status are:
            # enrolled, failed, graduated, deferred
        end
        return unless last_enrolled_cohort

        if new_status && (new_status != status)
            status_changing = true
            self.status = new_status
        end

        # If they might be changing mdp states, then record the transition to the new state.
        # They can change mdp states either because the status changed or because they were enrolled
        # before, they are still enrolled, and they are now at a different place in the curriculum and
        # a different number of lessons behind.
        if (status == 'enrolled' || status_changing)

            period = last_enrolled_cohort.get_last_period_before_time(time)
            cohort_user_period = self.cohort_user_periods[period]
            lessons_behind_bucket = cohort_user_period.lessons_behind_bucket

            puts "entering state #{[period, status, lessons_behind_bucket, previously_deferred].inspect}"  if user_id == debug_user_id
            new_mdp_state = mdb_success_predictor.get_mdp_state(period, status, lessons_behind_bucket, previously_deferred)

            # EMBA3 was an odd cohort because our communications did not go out as
            # normal, so we're not training on it.
            if self.mdp_state && new_mdp_state != self.mdp_state &&
                self.original_cohort.name != 'EMBA3'

                    self.mdp_state.user_passed_into_state(new_mdp_state)
            end
            self.mdp_state = new_mdp_state
            new_mdp_state.users_who_passed_through_state << self
        end

    end

end