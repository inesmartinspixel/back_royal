# This class models a group of cohort periods that are all "similar".
#
# "similar" means that it is reasonable to assume that, if we look
# at two different periods in this group, users in those two periods
# with similar progress would be expected to have similar graduation rates.
#
# For example, period 16 of MBA8 and period 16 of MBA9 are "similar" because
# as user who is 20 lessons behind in the one has the same chance of graduating
# as a user who is 20 lessons behind in the other.  period 4 of MBA8 and period 20
# of MBA9 are not similar because being 20 lessons behind in the one means something
# very different from being 20 lessons behind in the other.
#
# Specifically, two periods will be in the same group if they have the same
# `curriculum_position` (i.e. they both show up in the first half of the part
# of the curriculum that comes before the midterm) and if they have roughly the
# same number of required lessons.

class Cohort::MdpSuccessPredictor::SimilarPeriodGroup
    attr_reader :periods, :curriculum_position, :required_lessons_bucket, :program_type

    delegate :size, :to => :periods

    def initialize(program_type, curriculum_position, required_lessons_bucket)
        @program_type = program_type
        @curriculum_position = curriculum_position
        @required_lessons_bucket = required_lessons_bucket
        @periods = []
    end

    def inspect
        "#{self.class.name}: #{identifier}"
    end

    def identifier
        "(#{program_type}) #{curriculum_position}/#{required_lessons_bucket} required lessons"
    end

    def <<(period)
        periods << period
    end
end