# == Schema Information
#
# Table name: cohort_slack_room_assignments
#
#  id                     :uuid             not null, primary key
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  cohort_id              :uuid             not null
#  slack_room_assignments :json             not null
#  location_assignments   :json             not null
#

class Cohort::SlackRoomAssignment < ApplicationRecord

end
