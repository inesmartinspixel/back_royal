# == Schema Information
#
# Table name: billing_transactions
#
#  id                      :uuid             not null, primary key
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  transaction_time        :datetime         not null
#  transaction_type        :text             not null
#  provider                :text
#  provider_transaction_id :text
#  amount                  :float            not null
#  amount_refunded         :float            default(0.0), not null
#  refunded                :boolean          default(FALSE), not null
#  currency                :text             not null
#  description             :text
#  metadata                :json
#  stripe_livemode         :boolean
#

class BillingTransaction < ApplicationRecord
    OwnsPayments.require_klasses_that_own_payments

    has_many :refunds, autosave: true

    PROVIDER_PAYPAL = 'Paypal - Q4XWQ' # Q4XWQ is the beginning of our account #
    PROVIDER_STRIPE = 'stripe - smart.ly'
    PROVIDER_SVB = 'SVB - 38322' # SILICON VALLEY BANK.  38322 is the beginning of our checking acct #


    TRANSACTION_TYPE_PAYMENT = 'payment'
    TRANSACTION_TYPE_SURCHARGE = 'surcharge'
    TRANSACTION_TYPE_CREDIT = 'credit'

    TYPES_WITH_POSITIVE_AMOUNTS = [TRANSACTION_TYPE_PAYMENT, TRANSACTION_TYPE_CREDIT]
    TYPES_WITH_NEGATIVE_AMOUNTS  = [TRANSACTION_TYPE_SURCHARGE]
    TRANSACTION_TYPES = TYPES_WITH_POSITIVE_AMOUNTS + TYPES_WITH_NEGATIVE_AMOUNTS

    # we plan to support invoice in the future, to record when people owe us money for things
    # TRANSACTION_TYPE_INVOICE = 'invoice'

    PROVIDERS_EDITABLE_BY_ADMINS = [PROVIDER_PAYPAL, PROVIDER_SVB]

    validates_inclusion_of :provider, :in => [PROVIDER_STRIPE, PROVIDER_SVB, PROVIDER_PAYPAL], :allow_nil => true
    validates_inclusion_of :transaction_type, :in => [TRANSACTION_TYPE_PAYMENT, TRANSACTION_TYPE_SURCHARGE, TRANSACTION_TYPE_CREDIT]

    validates_absence_of :provider, :unless => :is_payment?
    validates_presence_of :provider, :if => :is_payment?
    validates_absence_of :provider_transaction_id, :unless => :is_payment?
    validates_presence_of :provider_transaction_id, :if => :is_payment?
    validates_inclusion_of :amount_refunded, :in => [0], :unless => :is_payment?, :message => 'must be 0'
    validates_inclusion_of :refunded, :in => [false], :unless => :is_payment?

    validate :validate_net_amount_non_negative
    validate :validate_expected_sign_on_amount

    before_save :set_default_transaction_time
    before_destroy :raise_if_uneditable_provider
    after_commit :handle_billing_locked
    after_commit :warn_on_incorrect_amount_refunded

    def self.find_or_create_by_stripe_charge(charge)
        # see comment near all_for_owner_id
        owner = User.find_by_id(charge.customer) || HiringTeam.find_by_id(charge.customer)

        if owner
            record = owner.billing_transactions.where({provider_transaction_id: charge.id}).first_or_initialize
        else
            if charge.livemode
                Raven.capture_exception("Could not find owner while saving stripe billing_transaction", {
                    level: 'warning',
                    extra: {
                        charge: charge.as_json
                    }
                })
            end
            record = BillingTransaction.where({provider_transaction_id: charge.id}).first_or_initialize
        end

        record.update!(
            transaction_type: 'payment',
            transaction_time: Time.at(charge.created),
            provider: BillingTransaction::PROVIDER_STRIPE,
            provider_transaction_id: charge.id,
            amount: charge.amount ? charge.amount / 100.0 : nil,
            currency: charge.currency,
            description: charge.description,
            metadata: charge.metadata,
            stripe_livemode: charge.livemode
        )
        record
    end

    def self.all_for_owner_id(owner_id)
        # I don't like the fact that we're hardcoding the classes that could
        # possibly own a billing_transaction. Seems like somehow this should be setup dynamically
        # when OwnsPayments is included in something.  But I also hate having side effects
        # to including a module.  It's odd in specs if you include the module in a test
        # class just for a specific suite.  So, leaving it this way for now at least.
        self.left_outer_joins(:user)
            .left_outer_joins(:hiring_team)
            .where("hiring_teams.id = ? or users.id = ?", owner_id, owner_id)
    end

    def self.create_from_hash!(owner, hash)
        RetriableTransaction.transaction do
            instance = initialize_from_hash(hash)
            instance.set_refunds_from_hash(hash['refunds']) # do this after the instance has been saved and an id assigned
            owner.billing_transactions << instance # this will save the instance
            instance
        end
    end

    def self.initialize_from_hash(hash)
        hash = hash.unsafe_with_indifferent_access
        instance = new
        instance.merge_hash(hash)
        instance
    end

    def self.update_from_hash!(hash)
        hash = hash.unsafe_with_indifferent_access
        instance = find_by_id(hash[:id])

        if instance.nil?
            raise ActiveRecord::RecordNotFound.new("Couldn't find #{self.name} with id=#{hash[:id]}")
        end

        instance.raise_if_uneditable_provider
        instance.merge_hash(hash)

        # force updated_at to change regardless of other changes
        instance.updated_at = Time.now
        instance.set_refunds_from_hash(hash['refunds']) # since we do this after the save when creating, we'll do it after the save here too
        instance.save!
        instance
    end

    def merge_hash(hash)

        hash = hash.unsafe_with_indifferent_access

        (
            %w(provider provider_transaction_id amount
                amount_refunded description currency
                transaction_type
            )
        ).each do |key|
            if hash.key?(key)
                val = hash[key]
                val = nil if val.is_a?(String) && val.blank?

                if key == 'provider' && val && !PROVIDERS_EDITABLE_BY_ADMINS.include?(val)
                    raise "Uneditable provider"
                end

                self.send(:"#{key}=", val)
            end
        end

        (
            %w(transaction_time)
        ).each do |key|
            if hash.key?(key)
                val = Time.at(hash[key])
                self.send(:"#{key}=", val)
            end
    end

        self
    end

    def set_refunds_from_hash(refund_hashes)
        return if refund_hashes.nil?
        existing_refunds = {}

        incoming_ids = refund_hashes.map { |h| h['id'] }.to_set
        self.refunds.each do |refund|
            if incoming_ids.include?(refund.id)
                existing_refunds[refund.id] = refund
            else
                refund.destroy
            end
        end

        self.refunds = refund_hashes.map do |refund_hash|

            # When new refunds are created in the client, they are assigned an id.
            # So if we don't find the refund in the databse, assume we need to create it
            id = refund_hash['id']
            refund = existing_refunds[id] || Refund.new(id: id)

            attrs = {
                amount: refund_hash['amount'],
                refund_time: Time.at(refund_hash['refund_time']),
                provider_transaction_id: refund_hash['provider_transaction_id'],
                provider: self.provider
            }
            refund.billing_transaction = self
            refund.assign_attributes(attrs)
            refund
        end
        self.calculate_amount_refunded
    end

    # Note that amount_refunded is a denormalized piece of information
    # that we almost gutted until we realized that a number of reports
    # use it, so dynamically calculating it in SQL would have a performance cost.
    def calculate_amount_refunded
        self.amount_refunded = sum_of_refunds

        # maybe it's silly that we bothered to implement this, but just doing things how
        # stripe does it, and it could be convenient
        if self.amount.present? && self.amount > 0 && self.amount_refunded.present?
            self.refunded = self.amount > 0 && self.amount == self.amount_refunded
        else
            self.refunded = false
        end
    end

    def sum_of_refunds
        refunds.map(&:amount).reduce(0) { |sum, amount| sum + amount }
    end

    # We don't do this in before_save / before_destroy because making
    # sure everything gets updated in the right order is too hard.  So
    # we explicity update amount_refunded in the right places, and then
    # just check before any commit that we actually did it.
    # We log to sentry rather than erroring because it's probably easier
    # to go back and clean things up than it would be to not save things
    # altogether (if this is happening in a stripe hook)
    def warn_on_incorrect_amount_refunded
        if self.sum_of_refunds != self.amount_refunded
            Raven.capture_exception("amount_refunded different from expected value", {
                extra: {
                    billing_transaction_id: self.id,
                    expected_value: sum_of_refunds,
                    actual_value: self.amount_refunded
                }
            })
        end
    end

    def as_json(options = {})
        attributes.slice(*%w(id provider provider_transaction_id amount amount_refunded refunded currency description metadata transaction_type stripe_livemode)).merge({
            created_at: created_at.to_timestamp,
            updated_at: updated_at.to_timestamp,
            transaction_time: transaction_time.to_timestamp,
            refunds: refunds.sort_by(&:refund_time).map(&:as_json)
        }.stringify_keys)
    end

    def set_default_transaction_time
        self.transaction_time ||= Time.now
    end

    def raise_if_uneditable_provider
        return unless is_payment?

        if !PROVIDERS_EDITABLE_BY_ADMINS.include?(self.provider)
            raise "Uneditable provider"
        end
    end

    def is_payment?
        transaction_type === TRANSACTION_TYPE_PAYMENT
    end

    def validate_net_amount_non_negative
        if amount_refunded > 0 && amount - amount_refunded < 0
            errors.add(:amount, "cannot be less than amount_refunded")
        end
    end

    def validate_expected_sign_on_amount
        if TYPES_WITH_NEGATIVE_AMOUNTS.include?(transaction_type)
            if amount > 0
                errors.add(:amount, "cannot be positive for this transaction type")
            end
        else
            if amount < 0
                errors.add(:amount, "cannot be negative for this transaction type")
            end
        end

    end

    def handle_billing_locked
        # Certain users are moved into billing locked (i.e. locked_due_to_past_due_payment) if they promised payment outside of the product,
        # but haven't provided said payment by the end of the payment grace period they've been given. For these users, after they've provided
        # sufficient payment, we manually create billing transaction records for them, at which point we want to move them out of billing locked
        # so that they have access to content again. Users who are locked_due_to_past_due_payment in the normal flow because of late payments
        # via Stripe are handled elsewhere (see #handle_past_due_changes in subscription_concern.rb).
        if self.user&.last_application&.has_paid_in_full?
            EditorTracking.transaction("#{self.class.name}##{__method__}") do
                self.user.last_application.update!(locked_due_to_past_due_payment: false)
            end
        end
    end
end
