# This is a wrapper around the Rails.cache API, meant to be used
# instead of directly calling Rails.cache.
# This wrapper is designed to mirror the behavior of Rails.cache
# as much as possible.
# see https://github.com/rails/rails/blob/master/activesupport/lib/active_support/cache.rb
class SafeCache

    def self.fetch(name, options = nil, &block)
        # If a block was passed, we need to be able to execute said block
        # in rescue_from_errors. However, we cannot execute the block
        # immediately, as there may be side effects.
        #
        # In the event of a cache miss, the return value of the block
        # will be written to the cache under the given key, and that
        # return value will be returned from Rails.cache.fetch
        #
        # In the event of any of the exceptions rescued below, we still
        # want to return the appropriate value.
        rescue_from_errors(block) do
            Rails.cache.fetch(name, options) do
                if block_given?
                    yield
                end
            end
        end
    end

    def self.delete(name, options = nil)
        rescue_from_errors do
            Rails.cache.delete(name, options)
        end
    end

    def self.clear(options = nil)
        rescue_from_errors([]) do
            Rails.cache.clear(options)
        end
    end

    def self.read(*args)
        rescue_from_errors(nil) do
            Rails.cache.read(*args)
        end
    end

    def self.write(*args)
        rescue_from_errors(nil) do
            Rails.cache.write(*args)
        end
    end

    # This method is intended to rescue from uncommon OS exceptions
    # encountered when attempting to fetch, delete, or clear from
    # the cache via Rails.cache
    #
    # return_value_on_fail is needed to properly emulate
    # Rails.cache functionality. In the event of a cache miss,
    # the following values are returned from Rails.cache:
    #   fetch: nil
    #   delete: nil
    #   clear: [] (Seems like this is consistent with the behavior of file_store but not memory_store)
    #   read: nil
    #   write: nil
    def self.rescue_from_errors(return_value_on_fail = nil)
        if block_given?
            begin
                yield
            rescue SystemCallError => e
                raise e if (!e.class.name.match(/^Errno::(EACCES|ENOENT)/))

                # Puma can't read/write to cache location.
                # Return the appropriate value.
                return return_value_on_fail&.respond_to?(:call) ? return_value_on_fail.call : return_value_on_fail
            end
        else
            # noop
        end
    end

end