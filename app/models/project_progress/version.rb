# == Schema Information
#
# Table name: project_progress_versions
#
#  version_id             :uuid             not null, primary key
#  operation              :string(1)        not null
#  version_created_at     :datetime         not null
#  id                     :uuid
#  created_at             :datetime
#  updated_at             :datetime
#  user_id                :uuid
#  requirement_identifier :text
#  score                  :float
#  waived                 :boolean
#  marked_as_passed       :boolean
#  status                 :text
#  id_verified            :boolean
#  version_editor_id      :uuid
#  version_editor_name    :text
#

class ProjectProgress::Version < ProjectProgress
    include VersionMixin

    self.primary_key = 'version_id'
    self.table_name = 'project_progress_versions'

    belongs_to :project_progress, :foreign_key => 'id', :primary_key => 'id'
end
