class AutoSuggestOption::ToJsonFromApiParams

    class UnauthorizedError < RuntimeError; end

    include ToJsonFromApiParamsHelpers::ToJsonFromApiParamsMixin

    include AutoSuggestOptionMixin

    def initialize(params, is_admin = false)

        params = do_basic_params_prep(params)

        # org options have additional meta
        is_organization_option = false

        # support multiple models
        self.base_klass = get_klass_from_type(params[:filters][:type])

        if [ProfessionalOrganizationOption, CareerProfile::EducationalOrganizationOption].include?(self.base_klass)
            is_organization_option = true
        end

        @query_params = params.slice(
            :limit
        ).with_indifferent_access

        set_filters({
            type: /.*/,
            in_locale_or_en: :locale,
            search_text: /.*/,
            id: :array_of_uuids,
            suggested: :bool
        })

        fields = set_fields({
            available_fields: %w|
                id created_at updated_at type text locale suggest source_id image_url
            |
        })

        @query_builder = CrazyQueryBuilder.new(self.base_table_name)
        @selects = @query_builder.selects
        @joins = @query_builder.joins
        @wheres = @query_builder.wheres
        @withs = @query_builder.withs
        @orders = @query_builder.orders
        @query_builder.limit = @limit = @query_params[:limit]

        set_default_selects

        if is_organization_option
            set_organization_option_selects
        end

        if filters.key?(:search_text)
            # FIXME: https://trello.com/c/y6IM4obh
            # raise "Must provide minimum required characters for search_text." unless filters[:search_text].length >= 3
            filter_by_search_text
        end

        # If suggested is not set and the user is not an admin then throw an error
        if (!filters.key?(:suggested) || !filters[:suggested]) && !is_admin
            raise "Only suggested options are currently supported for non-admins."

        # Else if suggested is set then do the filtering
        elsif filters.key?(:suggested) || filters[:suggested]
            filter_only_suggested
        end

        handle_locale_or_en_filter

        @selects.slice!(*fields)

        if @selects.empty?
            raise "No selects provided. params: #{@query_params.inspect}"
        end
    end

    private

    def set_default_selects
        table = self.base_table_name
        @selects['id'] = "#{table}.id"
        @selects['created_at'] = "#{table}.created_at"
        @selects['updated_at'] = "#{table}.updated_at"
        @selects['type'] = "'#{filters[:type]}'"
        @selects['text'] = "#{table}.text"
        @selects['suggest'] = "#{table}.suggest"
        @selects['locale'] = "#{table}.locale"
        @orders << "CHAR_LENGTH(#{table}.text) ASC"
    end

    def set_organization_option_selects
        table = self.base_table_name
        @selects['source_id'] = "#{table}.source_id"
        @selects['image_url'] = "#{table}.image_url"
    end

    def handle_locale_or_en_filter
        return unless filters.key?(:in_locale_or_en)
        locale = filters[:in_locale_or_en] || "en"
        @wheres << "#{self.base_table_name}.locale = '#{locale}'"
    end

    def filter_by_search_text
        # show only the options that contain the search text
        filtered = escape_for_like_query(filters[:search_text])
        @wheres << "#{self.base_table_name}.text ILIKE '%#{filtered}%'"
    end

    def filter_only_suggested
        # show only the options that are marked as suggested
        @wheres << "#{self.base_table_name}.suggest = true"
    end

    def execute
        result = ActiveRecord::Base.connection.execute(@query_builder.to_json_sql)
        json = result.to_a[0]["json"] || "[]"
        json
    end

end
