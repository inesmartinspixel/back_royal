# == Schema Information
#
# Table name: s3_transcript_assets
#
#  id                      :uuid             not null, primary key
#  created_at              :datetime
#  updated_at              :datetime
#  file_file_name          :string
#  file_content_type       :string
#  file_file_size          :integer
#  file_updated_at         :datetime
#  file_fingerprint        :string
#  user_id                 :uuid
#  education_experience_id :uuid
#  transcript_type         :string
#  persisted_path          :string
#

class S3TranscriptAsset < ActiveRecord::Base

    include PrivateDocumentMixin

    belongs_to :education_experience, class_name: 'CareerProfile::EducationExperience', optional: true

    default_scope -> {order(:created_at)}

    after_create do
        self.user&.identify
    end

    after_destroy do
        self.user&.identify
    end

    validates_attachment_content_type :file, :content_type => %w(
        application/pdf
        application/msword
        application/vnd.openxmlformats-officedocument.wordprocessingml.document
        image/jpg
        image/jpeg
        image/pjpeg
        image/pipeg
        image/png
    )
    validates_attachment_size :file, less_than: 10.megabytes
    validates_inclusion_of :transcript_type, in: ['official', 'unofficial', nil]

    def user
        # Note that transcripts were associated with users until https://trello.com/c/ErENgFKR
        self.education_experience&.career_profile&.user || User.find_by_id(self.user_id)
    end

    def as_json(options = {})
        hash = {
            :id => self.id,
            :file_file_name => self.file_file_name,
            :file_updated_at => self.file_updated_at.to_timestamp,
            :file_content_type => self.file_content_type,
            :file_file_size => self.file_file_size,
            :transcript_type => self.transcript_type
        }

        return hash
    end
end
