# == Schema Information
#
# Table name: published_cohort_periods_stream_locale_packs
#
#  created_at     :datetime
#  cohort_id      :uuid
#  index          :integer
#  locale_pack_id :uuid
#  title          :text
#  required       :boolean
#  optional       :boolean
#

class PublishedCohortPeriodsStreamLocalePack < ApplicationRecord
    include IsDerivedContentTable
    has_many :lesson_streams_progresses, class_name: 'Lesson::StreamProgress', primary_key: :locale_pack_id, foreign_key: :locale_pack_id

    update_when_attr_change_is_published(Lesson::Stream, :title, identifier: :locale_pack_id)
    update_when_attr_change_is_published(Lesson::Stream, :locale_pack_id, identifier: :locale_pack_id)
    update_when_attr_change_is_published(Cohort, :periods, :comparison => :hashdiff)

    def self.rebuild
        cohort_ids = PublishedCohort.all.pluck(:id)
        return if Rails.env.test? && cohort_ids.empty?

        RetriableTransaction.transaction do
            self.truncate
            write_new_records(:cohort_id, cohort_ids)
        end
    end

    def create_or_update
        raise "Cannot use ActiveRecord create or update methods"
    end

    # The records in here are not actually dependent on changes to the cohort
    # directly, but instead dependent on published_cohort_periods.  However,
    # we think it's better to batch all the changes when a cohort is first
    # published (or when multiple periods are updated), so we trigger off
    # a change to the cohort and rewrite all the records.  This means
    # we're doing some unnecessary work when just one period has changed,
    # but that's a tradeoff we're comfortable with.
    def self.update_on_content_change(klass:, identifiers:)
        if klass == Cohort
            write_new_records(:cohort_id, identifiers)
        elsif klass == Lesson::Stream
            write_new_records(:locale_pack_id, identifiers)
        end
    end

    # FIXME: once we get rid of the temp table, add `private_class_method`
    def self.select_new_records_sql(column, identifiers)
        cohort_ids = nil
        stream_locale_pack_ids = nil
        if column == :cohort_id
            cohort_ids = identifiers
        elsif column == :locale_pack_id
            stream_locale_pack_ids = identifiers
        end

        %Q~
            select
                now() as created_at
                , published_cohort_periods.cohort_id
                , published_cohort_periods.index
                , published_stream_locale_packs.locale_pack_id
                , published_stream_locale_packs.title
                , published_stream_locale_packs.locale_pack_id = ANY(published_cohort_periods.required_stream_pack_ids) as required
                , published_stream_locale_packs.locale_pack_id = ANY(published_cohort_periods.optional_stream_pack_ids) as optional
            from published_cohort_periods
            join published_stream_locale_packs
                ON #{SqlIdList.get_in_clause("published_stream_locale_packs.locale_pack_id", stream_locale_pack_ids)}
                AND published_stream_locale_packs.locale_pack_id = ANY(published_cohort_periods.required_stream_pack_ids || published_cohort_periods.optional_stream_pack_ids)
            WHERE #{SqlIdList.get_in_clause("published_cohort_periods.cohort_id", cohort_ids)}
            order by cohort_id, index asc, published_stream_locale_packs.locale_pack_id = ANY(published_cohort_periods.required_stream_pack_ids) desc
        ~
    end
end
