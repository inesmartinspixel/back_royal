# == Schema Information
#
# Table name: playlist_progress
#
#  user_id        :uuid
#  locale_pack_id :uuid
#  started        :boolean
#  completed      :boolean
#

class PlaylistProgress < ApplicationRecord
    self.table_name = 'playlist_progress'
end
