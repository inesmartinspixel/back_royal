# == Schema Information
#
# Table name: cohort_period_graduation_rate_predictions
#
#  id                                 :uuid             not null, primary key
#  created_at                         :datetime         not null
#  updated_at                         :datetime         not null
#  chance_of_success                  :float            not null
#  cohort_end                         :datetime         not null
#  cohort_id                          :uuid             not null
#  cohort_name                        :text             not null
#  cohorts_with_similar_periods_count :integer
#  current_position                   :text
#  error                              :float
#  final_result                       :float
#  index                              :integer
#  normalized_index                   :text             not null
#  num_active                         :integer          not null
#  num_enrolled                       :integer          not null
#  num_failed                         :integer          not null
#  num_graduated                      :integer          not null
#  period_end                         :datetime
#  similar_periods                    :json
#  similar_periods_count              :integer
#

class CohortPeriodGraduationRatePrediction < ApplicationRecord

    belongs_to :cohort

end
