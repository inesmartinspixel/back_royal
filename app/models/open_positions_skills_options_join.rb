# == Schema Information
#
# Table name: open_positions_skills_options
#
#  id               :uuid             not null, primary key
#  open_position_id :uuid             not null
#  skills_option_id :uuid             not null
#  position         :integer          not null
#

class OpenPositionsSkillsOptionsJoin < ActiveRecord::Base
    self.table_name = 'open_positions_skills_options'
    default_scope -> {order(:position)}
    belongs_to :open_position
    belongs_to :skills_option, class_name: 'SkillsOption'
    acts_as_list :scope => :open_position
end
