
module Event::PutEventsInFirehose

    class << self

        # see comment above handled_properties
        class ReferencedKeyDetector
            attr_reader:referenced_keys

            def initialize
                @referenced_keys = Set.new
            end

            def [](key)
                self.referenced_keys << key
                nil
            end

        end

        # This is a bit crazy. We want to know all of the properties in `payload` that
        # are read here, so that we can warn if an event is logged with a property that
        # we haven't considered.  In order to do this, we create a special class that
        # records any time someone calls `instance['key']` on it.
        def handled_properties
            unless defined? @handled_properties
                referenced_key_detector = ReferencedKeyDetector.new
                map_payload({'payload' => referenced_key_detector})
                @handled_properties = referenced_key_detector.referenced_keys.to_a
            end
            @handled_properties
        end

        def delivery_stream_name
            @delivery_stream_name ||= ENV['KINESIS_FIREHOSE_DELIVERY_STREAM_NAME']
        end

        def is_enabled?
            !!delivery_stream_name
        end


        def raven_options_for_response(event_response, event)
            {
                level: 'warning',
                extra: {
                    error_code: event_response.error_code,
                    event_id: event['id']
                },
                fingerprint: ['Failed to send events to firehose', event_response.error_message]
            }
        end

        def put_and_queue_failures(events, source=nil)
            begin
                response = put(events, source)
            rescue Exception => err
                Raven.capture_exception(err)

                # For performance reasons, we don't persist certain events to RDS. This means
                # that if the initial `put` to Firehose fails, we've lost that data since the
                # `PutFailedEventsInFirehoseJob` won't have an event in RDS to retry.
                # We are OK with a certain loss threshold for these particular events.
                failed_events_to_retry = events.reject { |event| Event::CLIENT_SIDE_EVENTS_NOT_PERSISTED_TO_RDS.include?(event['event_type']) }
                failed_events_to_retry.each do |event|
                    PutFailedEventsInFirehoseJob.perform_later(event['id'])
                end

                # FIXME: once we've worked out the discrepencies in redshift, we probably
                # want to keep this logging around, but do it for all events, not just
                # client-side
                if source == :client_side_events
                    Cloudwatch.put_metric_data(
                        ns_suffix: 'Kinesis Firehose',
                        metric_data: [{
                            metric_name: 'Failed events queued to be put in the firehose later',
                            value: failed_events_to_retry.size,
                            unit: 'Count'
                        },{
                            metric_name: 'Failed firehose put requests queued for later',
                            value: 1,
                            unit: 'Count'
                        }])
                end

                return false
            end

            # if put aborted, we have no response to check for errors
            return true if response.nil?

            has_error = false
            response.request_responses.each_with_index do |event_response, i|
                event = events[i]

                if event_response.error_code && !Event::CLIENT_SIDE_EVENTS_NOT_PERSISTED_TO_RDS.include?(event['event_type'])
                    has_error = true

                    # We want to queue each failed event separately so we can retry them separately.
                    # It would be confusing if 3 events failed, we queued them all up in a single job,
                    # and then when the job ran, 1 succeeded and the other two failed.  How would we
                    # retry?  Creating one job for each failed event avoids that issue.
                    PutFailedEventsInFirehoseJob.perform_later(event['id'])

                    Cloudwatch.put_metric_data(
                        ns_suffix: 'Kinesis Firehose',
                        metric_data: [{
                            metric_name: 'Failed events queued to be put in the firehose later',
                            value: 1,
                            unit: 'Count'
                        }])

                    # Some of these will be things we can ignore, but we may want to see what is
                    # going on if there are failures here
                    Raven.capture_exception(("Firehose error: #{event_response.error_message}"), raven_options_for_response(event_response, event))
                end
            end

            # return true if all succeeded. false otherwise
            return !has_error
        end

        def put(events, source = nil)
            return unless is_enabled?
            return if events.empty?
            serialized = events.map { |e| serialize_event(e) }

            # https://docs.aws.amazon.com/sdk-for-ruby/v3/api/Aws/Firehose/Client.html#put_record_batch-instance_method
            firehose = Aws::Firehose::Client.new

            response = firehose.put_record_batch(
                delivery_stream_name: delivery_stream_name,
                records: serialized.map { |e| {data: e}}
            )

            response
        end

        private
        def limit_string(str, bytes)
            if str.nil?
                nil
            elsif str.is_a?(String)
                str.mb_chars.limit(bytes).to_s
            else
                str.to_json.mb_chars.limit(bytes).to_s
            end
        end

        private
        def map_payload(event)

            payload = event['payload'] || {}
            transformed_payload = {
                peformance_to_eval_async: payload['performance'] && payload['performance']['toEvalAsync'],
                peformance_to_timeout: payload['performance'] && payload['performance']['toTimeout'],
                aborted: payload['aborted'],
                card_added: payload['card_added'],
                feedback_is_positive: payload['feedbackIsPositive'],
                has_free_trial: payload['has_free_trial'],
                has_some_results: payload['has_some_results'],
                livemode: payload['livemode'],
                new_subscription_created: payload['new_subscription_created'],
                notify_candidate_of_new_request: payload['notify_candidate_of_new_request'],
                notify_hiring_manager_of_new_match: payload['notify_hiring_manager_of_new_match'],
                result: payload['result'],
                server_event: payload['server_event'],
                stripe_event: payload['stripe_event'],
                message: limit_string(payload['message'], 65535),
                result_stream_ids: limit_string(payload['result_stream_ids'], 1024),
                candidate_ids: limit_string(payload['candidate_ids'], 2048),
                modals: limit_string(payload['modals'], 328),
                text_being_processed: payload['textBeingProcessed'],
                src: payload['src'],
                topics_to_filter_by: payload['topics_to_filter_by'],
                event_errors: limit_string(payload['errors'], 1361),
                feedback_additional_thoughts: limit_string(payload['feedbackAdditionalThoughts'], 1224),
                selection_details: limit_string(payload['selection_details'], 1134),
                challenge_responses: limit_string(payload['challengeResponses'], 1094),
                available_answer_ids: payload['availableAnswerIds'],
                content: limit_string(payload['content'], 500),
                server_error: limit_string(payload['server_error'], 633),
                user_agent: payload['user_agent'],
                request_config: payload['request_config'],
                source_params: limit_string(payload['source_params'], 2048),
                source_path: limit_string(payload['source_path'], 2048),
                status: payload['status'],
                params: limit_string(payload['params'], 2048),
                destination_path: limit_string(payload['destination_path'], 2048),
                path: limit_string(payload['path'], 2048),
                policy_config: payload['policy_config'],
                destination_params: limit_string(payload['destination_params'], 2048),
                search_text: limit_string(payload['search_text'], 237),
                error: payload['error'],
                url: limit_string(payload['url'], 2048),
                email: payload['email'],
                sender_company_name: payload['sender_company_name'],
                sender_avatar_url: limit_string(payload['sender_avatar_url'], 2048),
                link: limit_string(payload['link'], 2048),
                since_request_queued: payload['since_request_queued'],
                since_request_sent: payload['since_request_sent'],
                since_activated: payload['since_activated'],
                sender_company_logo_url: limit_string(payload['sender_company_logo_url'], 2048),
                user_subscription_type: payload['user_subscription_type'],
                device_model: payload['device_model'],
                device_type: payload['device_type'],
                device_vendor: payload['device_vendor'],
                lesson_title: payload['lesson_title'],
                feedback_reasons: payload['feedbackReasons'],
                chapter_label: payload['chapter_label'],
                utm_campaign: limit_string(payload['utm_campaign'], 72),
                chapter_title: payload['chapter_title'],
                progress_options_to_filter_by: payload['progress_options_to_filter_by'],
                participant_ids: payload['participant_ids'],
                recipient_ids_new: payload['recipient_ids'],
                build_commit: payload['build_commit'],
                ip_address: payload['ip_address'],
                cf_country_code: payload['cf_country_code'],
                score_metadata: payload['score_metadata'],
                performance: payload['performance'],
                lesson_stream_title: payload['lesson_stream_title'],
                candidate_id: payload['candidate_id'],
                challenge_id: payload['challenge_id'],
                cohort_id: payload['cohort_id'],
                component_id: payload['componentId'],
                frame_id: payload['frame_id'],
                frame_play_id: payload['frame_play_id'],
                hiring_manager_id: payload['hiring_manager_id'],
                hiring_relationship_id: payload['hiring_relationship_id'],
                label: limit_string(payload['label'], 512),
                launched_lesson_id: payload['launched_lesson_id'],
                launched_stream_id: payload['launched_stream_id'],
                lesson_id: payload['lesson_id'],
                lesson_play_id: payload['lesson_play_id'],
                lesson_stream_id: payload['lesson_stream_id'],
                lesson_stream_locale_pack_id: payload['lesson_stream_locale_pack_id'],
                lesson_stream_version_id: payload['lesson_stream_version_id'],
                lesson_version_id: payload['lesson_version_id'],
                logged_in_user_id: payload['logged_in_user_id'],
                page_load_id: payload['page_load_id'],
                playlist_id: payload['playlistId'],
                search_attempt_id: payload['search_attempt_id'],
                sender_id: payload['sender_id'],
                stream_locale_pack_id: payload['streamLocalePackId'] || payload['stream_locale_pack_id'],
                subscription_id: payload['subscription_id'],
                summary_id: payload['summary_id'],
                verified_by: payload['verified_by'],
                viewed_stream_dashboard_id: payload['viewed_stream_dashboard_id'],
                utm_content: limit_string(payload['utm_content'], 72),
                campaign: payload['campaign'],
                image_component_id: payload['image_component_id'],
                user_answer_to_display: limit_string(payload['userAnswerToDisplay'], 128),
                orig_modal_keys: limit_string(payload['origModalKeys'], 56),
                editor_template: payload['editor_template'],
                challenge_type: payload['challenge_type'],
                utm_source: limit_string(payload['utm_source'], 72),
                client_local_time: payload['client_local_time'],
                client_utc_time: payload['client_utc_time'],
                build_timestamp: payload['build_timestamp'],
                sender_name: payload['sender_name'],
                destination_directive: payload['destination_directive'],
                hiring_manager_status: payload['hiring_manager_status'],
                open_position_id: payload['open_position_id'],
                plan_name: payload['plan_name'],
                section: payload['section'],
                candidate_status: payload['candidate_status'],
                formatted_free_trial_end_at: payload['formatted_free_trial_end_at'],
                buffered_time: payload['buffered_time'],
                value: payload['value'],
                source_directive: payload['source_directive'],
                client_utc_timestamp: payload['client_utc_timestamp'],
                caused_by_event: payload['caused_by_event'],
                sign_up_code: payload['sign_up_code'],
                type: payload['type'],
                hiring_relationship_role: payload['hiring_relationship_role'],
                sender_hiring_relationship_role: payload['sender_hiring_relationship_role'],
                provider: payload['provider'],
                browser_version: payload['browser_version'],
                plan_id: payload['plan_id'],
                utm_medium: limit_string(payload['utm_medium'], 72),
                directive: payload['directive'],
                created: payload['created'],
                duration_idle: payload['duration_idle'],
                duration_total: payload['duration_total'],
                api_version: payload['api_version'],
                duration_active: payload['duration_active'],
                service: payload['service'],
                subscription_status: payload['subscription_status'],
                total: payload['total'],
                os_version: payload['os_version'],
                action: payload['action'],
                browser_name: payload['browser_name'],
                os_name: payload['os_name'],
                card_changed: payload['card_changed'],
                card_data_saved: payload['card_data_saved'],
                cordova: payload['cordova'],
                demo_mode: payload['demoMode'],
                editor_mode: payload['editorMode'],
                free_trial_set_to_cancel: payload['free_trial_set_to_cancel'],
                lesson_complete: payload['lesson_complete'],
                lesson_stream_complete: payload['lesson_stream_complete'],
                paid_subscription_set_to_cancel: payload['paid_subscription_set_to_cancel'],
                partially_correct: payload['partiallyCorrect'],
                preview_mode: payload['previewMode'],
                set_to_cancel: payload['set_to_cancel'],
                should_retry: payload['should_retry'],
                use_default_card: payload['use_default_card'],
                client_offset_from_utc: payload['client_offset_from_utc'],
                next_period_amount: payload['next_period_amount'],
                revenue: payload['revenue'],
                lesson_passed: payload['lesson_passed'],
                object: payload['object'],
                client_version: payload['client_version'],
                build_number: payload['build_number'],
                client: payload['client'],
                currency: payload['currency'],
                result_daily_lesson_config_ids: payload['result_daily_lesson_config_ids'],
                candidate_count: payload['candidate_count'],
                conversation_id: payload['conversation_id'],
                frame_index: payload['frame_index'],
                notification_id: payload['notification_id'],
                receipt_id: payload['receipt_id'],
                total_frames: payload['total_frames'],
                browser_major: payload['browser_major'],
                chapter_index: payload['chapter_index'],
                pending_webhooks: payload['pending_webhooks'],
                score: payload['score'],
                onboarding_questions: payload['onboarding_questions'],
                candidate_name: limit_string(payload['candidate_name'], 128),
                time_to_finish: payload['time_to_finish'] && payload['time_to_finish']['duration_total'],
                time_to_activate: payload['time_to_activate'] && payload['time_to_activate']['duration_total'],
                selected_by_policy: payload['selected_by_policy'],
                lesson_player_session_id: payload['lesson_player_session_id'],
                lesson_editor_session_id: payload['lesson_editor_session_id'],
                client_session_id: payload['client_session_id'],
                program_type: payload['program_type'],
                period_index: payload['period_index'],
                required_courses_completed_actual: payload['required_courses_completed_actual'],
                required_courses_completed_target: payload['required_courses_completed_target'],
                required_courses_entire_schedule: payload['required_courses_entire_schedule'],
                enrolled: payload['enrolled'],
                found_with_search_id: payload['found_with_search_id'],
                stripe_plan_ids: payload['stripe_plan_ids'],
                stripe_coupon_id: payload['stripe_coupon_id'],
                stripe_coupon_percent_off: payload['stripe_coupon_percent_off'],
                stripe_coupon_amount_off: payload['stripe_coupon_amount_off'],
                scholarship_level_name: limit_string(payload['scholarship_level_name'], 16),
                just_accepted_relationship: payload['just_accepted_relationship'],
                inviter_id: payload['inviter_id'],
                send_retargeted_email_for_program_type: payload['send_retargeted_email_for_program_type'],
                suggested_program_type: payload['suggested_program_type'],
                action_id: payload['action_id'],
                attempts: payload['attempts'],
                get_map_id: payload['get_map_id'],
                filters_places: limit_string(payload['filters_places'], 512),
                filters_keyword_search: limit_string(payload['filters_keyword_search'], 256),
                filters_student_network_looking_for: limit_string(payload['filters_student_network_looking_for'], 256),
                filters_student_network_interests: limit_string(payload['filters_student_network_interests'], 256),
                filters_industries: limit_string(payload['filters_industries'], 256),
                filters_alumni: payload['filters_alumni'],
                filters_program_type: payload['filters_program_type'],
                filters_cohort_id: payload['filters_cohort_id'],
                idology_verification_id: payload['idology_verification_id'],
                verified: payload['verified'],
                education_level: payload['survey_highest_level_completed_education_description'],
                area_of_study: payload['area_of_study'],
                work_experience: limit_string(payload['survey_years_full_time_experience'], 15),
                role: limit_string(payload['survey_most_recent_role_description'], 53),
                salary: limit_string(payload['salary'], 53),
                outcome: limit_string(payload['outcome'], 2048),
                failure_code: payload['failure_code'],
                charge_id: payload['charge_id'],
                captured: payload['captured'],
                signable_document_id: payload['signable_document_id'],
                external_position_id: payload['external_position_id'],
                external_position_url: payload['external_position_url'],
                external_position_title: payload['external_position_title'],
                external_position_industry: payload['external_position_industry'],
                external_position_location: payload['external_position_location'],
                external_position_company_name: payload['external_position_company_name'],
                current_step_index: payload['current_step_index'],
                current_step_title: payload['current_step_title'],
                has_scrolled: payload['has_scrolled'],
                just_used_deferral_link: payload['just_used_deferral_link'],
                gl: payload['gl'], # "good lead" (see https://trello.com/c/sqglpXD2)
                recipient_id: payload['recipient_id'],
                cohort_application_id: payload['cohort_application_id'],
                follow_up_attempt: payload['follow_up_attempt']
            }

            # An actual event payload hash with legitimate data can be passed in as the payload here
            # OR a ReferencedKeyDetector object. A ReferencedKeyDetector object is different from an
            # actual event payload in that the ReferencedKeyDetector object doesn't contain legitimate
            # event data and is simply used as a way of recording what event attributes we're already
            # able to handle. Since the ReferencedKeyDetector object doesn't actually contain any data,
            # it's unnecessary for us to call auto_truncate_payload. See comment above handled_properties
            # and https://trello.com/c/HblQ3Umr.
            payload.is_a?(ReferencedKeyDetector) ? transformed_payload : auto_truncate_payload(transformed_payload, event)
        end

        private
        def auto_truncate_payload(transformed_payload, event)

            # We don't need to attempt to load the (possibly unconnected) RedshiftEvent connection
            # to pull column info if we're not actually going to put these in the stream
            return transformed_payload unless is_enabled?

            # Loop through each column in the redshift events table.
            # Since this requires a call to redshift to list the column names,
            # it takes a bit of time the very first time it is called on a new
            # process.  After that, it should be cached and fast.
            auto_truncated_columns = []
            columns_hash = RedshiftEvent.columns_hash
            columns_hash.each do |key, column|
                key = key.to_sym

                # skip any column that is not varchar and determine
                # the max length for columns that are
                next unless column.sql_type.starts_with?('character varying')
                maxlength = column.sql_type.match(/\d+/)[0].to_i

                # get the value of the column and determine the serialized
                # string that we need to check for being too long
                val = transformed_payload[key]
                string_to_check = if val.nil?
                    ""
                elsif val.is_a?(String)
                    val
                else
                    val.to_json
                end

                # if the serialized string is too long, truncate it
                # and save it in auto_truncated_columns so we can
                # send warnings later
                if string_to_check.bytesize > maxlength
                    transformed_payload[key] = limit_string(val, maxlength)

                    auto_truncated_columns << key.to_s

                end
            end

            # send warnings about any columns that were truncated and
            # store the full event in S3 so we can recover it if need
            # be
            if auto_truncated_columns.any?
                concatenated_keys = auto_truncated_columns.sort.join(',').slice(0, 40)

                path = 'auto-truncated-events'
                LogToS3.log(path, "#{event['id']} - #{concatenated_keys}", event.to_json)
                Raven.capture_exception('Auto-truncated event before sending to Kinesis Firehose', {
                    level: 'warning',
                    extra: {
                        event_id: event['id'],
                        auto_truncated_columns: auto_truncated_columns,
                        original_event_stored_at: "https://s3.console.aws.amazon.com/s3/buckets/#{LogToS3.bucket_name}/#{path}/?region=#{LogToS3.region}&tab=overview"
                    }
                })
            end

            transformed_payload
        end

        private
        def map_columns(e)
            # * the `compact` at the end here removes any keys with null values.  This just limits
            #   the size of the payload that we send through the firehose, since we don't have to explicitly
            #   tell redshift to set all of those columns to null
            # * All of these timestamps need to be formatted in a way that redshift can read to that
            #   redshift can create timestamp columns.  In the setup for the delivery stream in AWS,
            #   we add an option to the redshift COPY command to tell it how our times will be formatted.
            {
                id: e['id'],
                user_id: e['user_id'],
                event_type: e['event_type'],
                created_at: e['created_at']&.utc&.strftime('%Y-%m-%d %H:%M:%S.%N'),
                updated_at: e['updated_at']&.utc&.strftime('%Y-%m-%d %H:%M:%S.%N'),
                estimated_time: e['estimated_time']&.utc&.strftime('%Y-%m-%d %H:%M:%S.%N'),
                client_reported_time: e['client_reported_time']&.utc&.strftime('%Y-%m-%d %H:%M:%S.%N'),
                hit_server_at: e['hit_server_at']&.utc&.strftime('%Y-%m-%d %H:%M:%S.%N'),
                total_buffered_seconds: e['total_buffered_seconds'],
                dumped_at: Time.now&.utc&.strftime('%Y-%m-%d %H:%M:%S.%N'),
            }.merge(map_payload(e)).compact
        end

        private
        def serialize_event(event)
            map_columns(event).to_json
        end


    end

end