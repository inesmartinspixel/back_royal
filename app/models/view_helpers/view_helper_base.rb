basename = File.basename(__FILE__)

class ViewHelpers::ViewHelperBase

    attr_reader :migration

    delegate :execute, :add_index, :to => :migration
    delegate :view_name, :to => :class

    def self.schema
        "public"
    end

    def self.view_name
        [
            self.schema,
            view_name_without_schema
        ].join('.')
    end

    def self.view_name_without_schema
        self.name.split('::').last.underscore
    end

    def self.version_numbers
        self.instance_methods.grep(/^version_/).map { |meth|
            i = meth.to_s.gsub("version_", "").to_i
        }.compact
    end

    def self.max_version_before(max_version_number)
        version_number = self.version_numbers.select { |i| i <= max_version_number }.max
        :"version_#{version_number}" if version_number
    end

    def self.populated?
        result = get_populated_result

        # When restoring our local development DBs from CDB dumps, we often
        # find ourselves in a situation where one/many views have not yet
        # been created. This is likely due to a race condition in our
        # dump/import workflow.
        if !result && Rails.env.development?
            puts "Creating #{self.view_name} because it does not yet exist..."
            self.new(ActiveRecord::Migration).current
            result = get_populated_result
        end
        raise "#{view_name_without_schema} does not exist" unless result

        populated = result['relispopulated']
    end

    def self.created?
        result = get_populated_result
        !!result
    end

    def self.get_populated_result
        ActiveRecord::Base.connection.execute("
            SELECT relispopulated
            FROM pg_class
                JOIN pg_catalog.pg_namespace ON pg_namespace.oid=pg_class.relnamespace
            WHERE
                relname = '#{view_name_without_schema}'
                AND pg_namespace.nspname = '#{schema}'
            ;").to_a[0]
    end

    def self.refresh_concurrently
        start = Time.now
        # concurrently can only be used on populated views
        concurrently = self.populated? ? 'concurrently' : ''

        puts "Starting to refresh #{view_name}" if Rails.env.development?
        ActiveRecord::Base.with_statement_timeout(0) do
            Cloudwatch.put_timing_metric_data(metric_name: "refresh #{view_name_without_schema}", min_time: 5.seconds) do
                ActiveRecord::Base.connection.execute("refresh materialized view #{concurrently} #{view_name} ")
            end
        end
        puts " - #{Time.now - start} to refresh #{view_name}" if Rails.env.development? && Time.now - start > 1.second
    end

    def self.ensure_populated
        # We check first if the view has even been created because we run this
        # in rake test:setup after running db:structure:load.  At that point,
        # there might be views in the code that have not yet been put into the database.
        # But we have to populate them before running migrations.
        if created? && !populated?
            refresh_concurrently
        end
    end

    def self.deprecated?
        @deprecated == true
    end

    def self.deprecated=(val)
        @deprecated = val
    end

    def initialize(migration)
        @migration = migration
    end

    def drop
        # first try to drop a materialized view with this name. If none
        # exists, then drop a view with this name
        drop_with_type("materialized view") || drop_with_type("view")
    end

    def drop_with_type(type)
        # First try to drop the view in the current schema.  If none exists,
        # then see if it used to exist in a different schema
        drop_view(type, view_name)
    end

    def drop_view(type, view_name)
        savepoint = "before_drop_#{view_name.gsub('.', '')}"

        execute("SAVEPOINT #{savepoint};")
        execute("drop #{type} #{view_name}")
        true
    rescue ActiveRecord::StatementInvalid => err
        if err.message.match(/PG::UndefinedTable/) || err.message.match(/PG::WrongObjectType/)
            execute("ROLLBACK TO SAVEPOINT #{savepoint}")
            false
        else
            raise err
        end
    end

    def replace_with(version_name)
        drop
        send(version_name)
    end

end