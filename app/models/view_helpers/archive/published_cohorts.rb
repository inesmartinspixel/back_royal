class ViewHelpers::PublishedCohorts < ViewHelpers::ViewHelperBase
    # -*- SkipSchemaAnnotations

    # Materialized CTEs
    def version_20200625152000
        current
    end

    def version_20200309215223
        execute %Q~
            create view published_cohorts as
            WITH published_cohort_playlist_collections AS (
                SELECT
                    cohorts_versions.id                             AS cohort_id
                    , CASE WHEN playlist_collection IS NULL THEN '{}'::JSON
                        ELSE playlist_collection
                    END AS playlist_collection
                FROM cohorts_versions
                    JOIN content_publishers
                        ON content_publishers.cohort_version_id = cohorts_versions.version_id
                    -- This LEFT JOIN LATERAL against the UNNESTed playlist_collections array ensures that a
                    -- row is returned for every published cohort even if its playlist_collections array is
                    -- empty. Practically, this isn't a problem, but it's helpful in our specs.
                    LEFT JOIN LATERAL UNNEST(cohorts_versions.playlist_collections) playlist_collection ON TRUE
            ),
            published_cohort_playlist_collection_required_playlist_pack_ids AS (
                SELECT
                    published_cohort_playlist_collections.cohort_id
                    , ARRAY(SELECT required_playlist_pack_id FROM JSON_ARRAY_ELEMENTS_TEXT(playlist_collection->'required_playlist_pack_ids') required_playlist_pack_id) AS required_playlist_pack_ids
                FROM published_cohort_playlist_collections
            ),
            published_cohort_required_playlist_pack_ids AS (
                SELECT
                    published_cohort_playlist_collection_required_playlist_pack_ids.cohort_id
                    , ARRAY_CAT_AGG(published_cohort_playlist_collection_required_playlist_pack_ids.required_playlist_pack_ids)::uuid[] AS required_playlist_pack_ids
                FROM published_cohort_playlist_collection_required_playlist_pack_ids
                GROUP BY published_cohort_playlist_collection_required_playlist_pack_ids.cohort_id
            )
            select
                cohorts_versions.id
                , case
                    -- FIXME: once we've removed unnecessary cohort_promotions (https://trello.com/c/xzMZHN17/2068-chore-cleanup-after-cohort-dates-promotion-refactor)
                    -- we can stop hardcoding here which program types pay attention to cohort promotions, and just
                    -- say if either there is a cohort promotion or if there is a promoted admission round, then
                    -- the cohort is promoted
                    when cohorts_versions.program_type in ('emba', 'mba')
                        then promoted_admission_rounds.cohort_id is not null
                    ELSE
                        cohort_promotions.cohort_id is not null
                    end as promoted
                , cohorts_versions.name
                , cohorts_versions.program_type
                , published_cohort_required_playlist_pack_ids.required_playlist_pack_ids
                , cohorts_versions.specialization_playlist_pack_ids
                , cohorts_versions.start_date
                , cohorts_versions.end_date
                , cohorts_versions.start_date + interval '3 years' as grad_deadline
                , CASE
                    WHEN enrollment_deadline_days_offset IS NULL
                    THEN cohorts_versions.start_date
                    -- add_dst_aware_offset is a custom function we've added in a migration
                    ELSE add_dst_aware_offset(cohorts_versions.start_date, cohorts_versions.enrollment_deadline_days_offset, 'days')
                    END AS enrollment_deadline
                , add_dst_aware_offset(cohorts_versions.end_date, cohorts_versions.graduation_days_offset_from_end, 'days') AS graduation_date
                , add_dst_aware_offset(cohorts_versions.start_date, cohorts_versions.early_registration_deadline_days_offset, 'days') AS early_registration_deadline
                , cohorts_versions.version_id
            from cohorts_versions
                JOIN published_cohort_required_playlist_pack_ids
                    ON published_cohort_required_playlist_pack_ids.cohort_id = cohorts_versions.id
                join content_publishers
                    on content_publishers.cohort_version_id = cohorts_versions.version_id
                left join cohort_promotions
                    on cohort_promotions.cohort_id = cohorts_versions.id
                left join published_cohort_admission_rounds promoted_admission_rounds
                    on promoted_admission_rounds.cohort_id = cohorts_versions.id
                        and promoted_admission_rounds.promoted = true
        ~
    end

    def version_20190409135750
        execute %Q~
            create view published_cohorts as
            WITH published_cohort_playlist_collections AS (
                SELECT
                    cohorts_versions.id                             AS cohort_id
                    , CASE WHEN playlist_collection IS NULL THEN '{}'::JSON
                        ELSE playlist_collection
                    END AS playlist_collection
                FROM cohorts_versions
                    JOIN content_publishers
                        ON content_publishers.cohort_version_id = cohorts_versions.version_id
                    -- This LEFT JOIN LATERAL against the UNNESTed playlist_collections array ensures that a
                    -- row is returned for every published cohort even if its playlist_collections array is
                    -- empty. Practically, this isn't a problem, but it's helpful in our specs.
                    LEFT JOIN LATERAL UNNEST(cohorts_versions.playlist_collections) playlist_collection ON TRUE
            ),
            published_cohort_playlist_collection_required_playlist_pack_ids AS (
                SELECT
                    published_cohort_playlist_collections.cohort_id
                    , ARRAY(SELECT required_playlist_pack_id FROM JSON_ARRAY_ELEMENTS_TEXT(playlist_collection->'required_playlist_pack_ids') required_playlist_pack_id) AS required_playlist_pack_ids
                FROM published_cohort_playlist_collections
            ),
            published_cohort_required_playlist_pack_ids AS (
                SELECT
                    published_cohort_playlist_collection_required_playlist_pack_ids.cohort_id
                    , ARRAY_CAT_AGG(published_cohort_playlist_collection_required_playlist_pack_ids.required_playlist_pack_ids)::uuid[] AS required_playlist_pack_ids
                FROM published_cohort_playlist_collection_required_playlist_pack_ids
                GROUP BY published_cohort_playlist_collection_required_playlist_pack_ids.cohort_id
            )
            select
                cohorts_versions.id
                , case
                    -- FIXME: once we've removed unnecessary cohort_promotions (https://trello.com/c/xzMZHN17/2068-chore-cleanup-after-cohort-dates-promotion-refactor)
                    -- we can stop hardcoding here which program types pay attention to cohort promotions, and just
                    -- say if either there is a cohort promotion or if there is a promoted admission round, then
                    -- the cohort is promoted
                    when cohorts_versions.program_type in ('emba', 'mba')
                        then promoted_admission_rounds.cohort_id is not null
                    ELSE
                        cohort_promotions.cohort_id is not null
                    end as promoted
                , cohorts_versions.name
                , cohorts_versions.program_type
                , published_cohort_required_playlist_pack_ids.required_playlist_pack_ids
                , cohorts_versions.specialization_playlist_pack_ids
                , cohorts_versions.start_date
                , cohorts_versions.end_date
                , cohorts_versions.start_date + interval '3 years' as grad_deadline
                , CASE
                    WHEN enrollment_deadline_days_offset IS NULL
                    THEN cohorts_versions.start_date
                    -- add_dst_aware_offset is a custom function we've added in a migration
                    ELSE add_dst_aware_offset(cohorts_versions.start_date, cohorts_versions.enrollment_deadline_days_offset, 'days')
                    END AS enrollment_deadline
                , add_dst_aware_offset(cohorts_versions.end_date, cohorts_versions.graduation_days_offset_from_end, 'days') AS graduation_date
                , add_dst_aware_offset(cohorts_versions.start_date, cohorts_versions.early_registration_deadline_days_offset, 'days') AS early_registration_deadline
                , cohorts_versions.version_id
            from cohorts_versions
                JOIN published_cohort_required_playlist_pack_ids
                    ON published_cohort_required_playlist_pack_ids.cohort_id = cohorts_versions.id
                join content_publishers
                    on content_publishers.cohort_version_id = cohorts_versions.version_id
                left join cohort_promotions
                    on cohort_promotions.cohort_id = cohorts_versions.id
                left join published_cohort_admission_rounds promoted_admission_rounds
                    on promoted_admission_rounds.cohort_id = cohorts_versions.id
                        and promoted_admission_rounds.promoted = true
        ~
    end

    # use new offset column for graduation date
    def version_20181015125551
        execute %Q~
            create view published_cohorts as
            select
                cohorts_versions.id
                , case
                    -- FIXME: once we've removed unnecessary cohort_promotions (https://trello.com/c/xzMZHN17/2068-chore-cleanup-after-cohort-dates-promotion-refactor)
                    -- we can stop hardcoding here which program types pay attention to cohort promotions, and just
                    -- say if either there is a cohort promotion or if there is a promoted admission round, then
                    -- the cohort is promoted
                    when cohorts_versions.program_type in ('emba', 'mba')
                        then promoted_admission_rounds.cohort_id is not null
                    ELSE
                        cohort_promotions.cohort_id is not null
                    end as promoted
                , cohorts_versions.name
                , cohorts_versions.program_type
                , cohorts_versions.required_playlist_pack_ids
                , cohorts_versions.specialization_playlist_pack_ids
                , cohorts_versions.start_date
                , cohorts_versions.end_date
                , cohorts_versions.start_date + interval '3 years' as grad_deadline
                , CASE
                    WHEN enrollment_deadline_days_offset IS NULL
                    THEN cohorts_versions.start_date
                    -- add_dst_aware_offset is a custom function we've added in a migration
                    ELSE add_dst_aware_offset(cohorts_versions.start_date, cohorts_versions.enrollment_deadline_days_offset, 'days')
                    END AS enrollment_deadline
                , add_dst_aware_offset(cohorts_versions.end_date, cohorts_versions.graduation_days_offset_from_end, 'days') AS graduation_date
                , cohorts_versions.version_id
            from cohorts_versions
                join content_publishers on content_publishers.cohort_version_id = cohorts_versions.version_id
                left join cohort_promotions
                    on cohort_promotions.cohort_id = cohorts_versions.id
                left join published_cohort_admission_rounds promoted_admission_rounds
                    on promoted_admission_rounds.cohort_id = cohorts_versions.id
                        and promoted_admission_rounds.promoted = true
        ~
    end

    # add graduation_date
    def version_20181008182254
        execute %Q~
            create view published_cohorts as
            select
                cohorts_versions.id
                , case
                    -- FIXME: once we've removed unnecessary cohort_promotions (https://trello.com/c/xzMZHN17/2068-chore-cleanup-after-cohort-dates-promotion-refactor)
                    -- we can stop hardcoding here which program types pay attention to cohort promotions, and just
                    -- say if either there is a cohort promotion or if there is a promoted admission round, then
                    -- the cohort is promoted
                    when cohorts_versions.program_type in ('emba', 'mba')
                        then promoted_admission_rounds.cohort_id is not null
                    ELSE
                        cohort_promotions.cohort_id is not null
                    end as promoted
                , cohorts_versions.name
                , cohorts_versions.program_type
                , cohorts_versions.required_playlist_pack_ids
                , cohorts_versions.specialization_playlist_pack_ids
                , cohorts_versions.start_date
                , cohorts_versions.end_date
                , cohorts_versions.start_date + interval '3 years' as grad_deadline
                , CASE
                    WHEN enrollment_deadline_days_offset IS NULL
                    THEN cohorts_versions.start_date
                    -- add_dst_aware_offset is a custom function we've added in a migration
                    ELSE add_dst_aware_offset(cohorts_versions.start_date, cohorts_versions.enrollment_deadline_days_offset, 'days')
                    END AS enrollment_deadline
                , add_dst_aware_offset(cohorts_versions.start_date, cohorts_versions.graduation_date_days_offset, 'days') AS graduation_date
                , cohorts_versions.version_id
            from cohorts_versions
                join content_publishers on content_publishers.cohort_version_id = cohorts_versions.version_id
                left join cohort_promotions
                    on cohort_promotions.cohort_id = cohorts_versions.id
                left join published_cohort_admission_rounds promoted_admission_rounds
                    on promoted_admission_rounds.cohort_id = cohorts_versions.id
                        and promoted_admission_rounds.promoted = true
        ~
    end

    # update to graduation deadlines.  Set the deadline to 3 years.
    def version_20180801123727
        execute %Q~
            create view published_cohorts as
            select
                cohorts_versions.id
                , case
                    -- FIXME: once we've removed unnecessary cohort_promotions (https://trello.com/c/xzMZHN17/2068-chore-cleanup-after-cohort-dates-promotion-refactor)
                    -- we can stop hardcoding here which program types pay attention to cohort promotions, and just
                    -- say if either there is a cohort promotion or if there is a promoted admission round, then
                    -- the cohort is promoted
                    when cohorts_versions.program_type in ('emba', 'mba')
                        then promoted_admission_rounds.cohort_id is not null
                    ELSE
                        cohort_promotions.cohort_id is not null
                    end as promoted
                , cohorts_versions.name
                , cohorts_versions.program_type
                , cohorts_versions.required_playlist_pack_ids
                , cohorts_versions.specialization_playlist_pack_ids
                , cohorts_versions.start_date
                , cohorts_versions.end_date
                , cohorts_versions.start_date + interval '3 years' as grad_deadline
                , CASE
                    WHEN enrollment_deadline_days_offset IS NULL
                    THEN cohorts_versions.start_date
                    -- add_dst_aware_offset is a custom function we've added in a migration
                    ELSE add_dst_aware_offset(cohorts_versions.start_date, cohorts_versions.enrollment_deadline_days_offset, 'days')
                    END AS enrollment_deadline
                , cohorts_versions.version_id
            from cohorts_versions
                join content_publishers on content_publishers.cohort_version_id = cohorts_versions.version_id
                left join cohort_promotions
                    on cohort_promotions.cohort_id = cohorts_versions.id
                left join published_cohort_admission_rounds promoted_admission_rounds
                    on promoted_admission_rounds.cohort_id = cohorts_versions.id
                        and promoted_admission_rounds.promoted = true
        ~
    end

    # graduation deadlines
    def version_20180723070738
        execute %Q~
            create view published_cohorts as
            select
                cohorts_versions.id
                , case
                    -- FIXME: once we've removed unnecessary cohort_promotions (https://trello.com/c/xzMZHN17/2068-chore-cleanup-after-cohort-dates-promotion-refactor)
                    -- we can stop hardcoding here which program types pay attention to cohort promotions, and just
                    -- say if either there is a cohort promotion or if there is a promoted admission round, then
                    -- the cohort is promoted
                    when cohorts_versions.program_type in ('emba', 'mba')
                        then promoted_admission_rounds.cohort_id is not null
                    ELSE
                        cohort_promotions.cohort_id is not null
                    end as promoted
                , cohorts_versions.name
                , cohorts_versions.program_type
                , cohorts_versions.required_playlist_pack_ids
                , cohorts_versions.specialization_playlist_pack_ids
                , cohorts_versions.start_date
                , cohorts_versions.end_date
                , (cohorts_versions.start_date + ((cohorts_versions.end_date - cohorts_versions.start_date) * 1.5)) as grad_deadline_150_perc
                , case when cohorts_versions.program_type = 'mba'
                            then (cohorts_versions.start_date + ((interval '9 months') * 1.5))
                         when cohorts_versions.program_type = 'emba'
                            then (cohorts_versions.start_date + ((interval '12 months') * 1.5))
                    end as grad_deadline_9_12
                , CASE
                    WHEN enrollment_deadline_days_offset IS NULL
                    THEN cohorts_versions.start_date
                    -- add_dst_aware_offset is a custom function we've added in a migration
                    ELSE add_dst_aware_offset(cohorts_versions.start_date, cohorts_versions.enrollment_deadline_days_offset, 'days')
                    END AS enrollment_deadline
                , cohorts_versions.version_id
            from cohorts_versions
                join content_publishers on content_publishers.cohort_version_id = cohorts_versions.version_id
                left join cohort_promotions
                    on cohort_promotions.cohort_id = cohorts_versions.id
                left join published_cohort_admission_rounds promoted_admission_rounds
                    on promoted_admission_rounds.cohort_id = cohorts_versions.id
                        and promoted_admission_rounds.promoted = true
        ~
    end

    # dst aware enrollment_deadline
    def version_20180423140732
        execute %Q~
            create view published_cohorts as
            select
                cohorts_versions.id
                , case
                    -- FIXME: once we've removed unnecessary cohort_promotions (https://trello.com/c/xzMZHN17/2068-chore-cleanup-after-cohort-dates-promotion-refactor)
                    -- we can stop hardcoding here which program types pay attention to cohort promotions, and just
                    -- say if either there is a cohort promotion or if there is a promoted admission round, then
                    -- the cohort is promoted
                    when cohorts_versions.program_type in ('emba', 'mba')
                        then promoted_admission_rounds.cohort_id is not null
                    ELSE
                        cohort_promotions.cohort_id is not null
                    end as promoted
                , cohorts_versions.name
                , cohorts_versions.program_type
                , cohorts_versions.required_playlist_pack_ids
                , cohorts_versions.specialization_playlist_pack_ids
                , cohorts_versions.start_date
                , cohorts_versions.end_date
                , CASE
                    WHEN enrollment_deadline_days_offset IS NULL
                    THEN cohorts_versions.start_date
                    -- add_dst_aware_offset is a custom function we've added in a migration
                    ELSE add_dst_aware_offset(cohorts_versions.start_date, cohorts_versions.enrollment_deadline_days_offset, 'days')
                    END AS enrollment_deadline
                , cohorts_versions.version_id
            from cohorts_versions
                join content_publishers on content_publishers.cohort_version_id = cohorts_versions.version_id
                left join cohort_promotions
                    on cohort_promotions.cohort_id = cohorts_versions.id
                left join published_cohort_admission_rounds promoted_admission_rounds
                    on promoted_admission_rounds.cohort_id = cohorts_versions.id
                        and promoted_admission_rounds.promoted = true
        ~
    end

    # remove warning_date and expulsion_date.
    # add enrollment_deadline
    def version_20170908153230
        execute %Q~
            create view published_cohorts as
            select
                cohorts_versions.id
                , case
                    -- FIXME: once we've removed unnecessary cohort_promotions (https://trello.com/c/xzMZHN17/2068-chore-cleanup-after-cohort-dates-promotion-refactor)
                    -- we can stop hardcoding here which program types pay attention to cohort promotions, and just
                    -- say if either there is a cohort promotion or if there is a promoted admission round, then
                    -- the cohort is promoted
                    when cohorts_versions.program_type in ('emba', 'mba')
                        then promoted_admission_rounds.cohort_id is not null
                    ELSE
                        cohort_promotions.cohort_id is not null
                    end as promoted
                , cohorts_versions.name
                , cohorts_versions.program_type
                , cohorts_versions.required_playlist_pack_ids
                , cohorts_versions.specialization_playlist_pack_ids
                , cohorts_versions.start_date
                , cohorts_versions.end_date
                , CASE
                    WHEN enrollment_deadline_days_offset IS NULL
                    THEN cohorts_versions.start_date
                    ELSE cohorts_versions.start_date + (cohorts_versions.enrollment_deadline_days_offset || ' days')::interval
                    END AS enrollment_deadline
                , cohorts_versions.version_id
            from cohorts_versions
                join content_publishers on content_publishers.cohort_version_id = cohorts_versions.version_id
                left join cohort_promotions
                    on cohort_promotions.cohort_id = cohorts_versions.id
                left join published_cohort_admission_rounds promoted_admission_rounds
                    on promoted_admission_rounds.cohort_id = cohorts_versions.id
                        and promoted_admission_rounds.promoted = true
        ~
    end

    def version_20170515203652
        execute %Q~
            create view published_cohorts as
            select
                cohorts_versions.id
                , case
                    -- FIXME: once we've removed unnecessary cohort_promotions (https://trello.com/c/xzMZHN17/2068-chore-cleanup-after-cohort-dates-promotion-refactor)
                    -- we can stop hardcoding here which program types pay attention to cohort promotions, and just
                    -- say if either there is a cohort promotion or if there is a promoted admission round, then
                    -- the cohort is promoted
                    when cohorts_versions.program_type in ('emba', 'mba')
                        then promoted_admission_rounds.cohort_id is not null
                    ELSE
                        cohort_promotions.cohort_id is not null
                    end as promoted
                , cohorts_versions.name
                , cohorts_versions.program_type
                , cohorts_versions.required_playlist_pack_ids
                , cohorts_versions.specialization_playlist_pack_ids
                , cohorts_versions.start_date
                , cohorts_versions.end_date
                , CASE
                    WHEN enrollment_warning_days_offset IS NULL
                    THEN NULL
                    ELSE cohorts_versions.start_date + (cohorts_versions.enrollment_warning_days_offset || ' days')::interval
                    END AS warning_date
                , cohorts_versions.start_date + (cohorts_versions.enrollment_expulsion_days_offset || ' days')::interval AS expulsion_date
                , cohorts_versions.version_id
            from cohorts_versions
                join content_publishers on content_publishers.cohort_version_id = cohorts_versions.version_id
                left join cohort_promotions
                    on cohort_promotions.cohort_id = cohorts_versions.id
                left join published_cohort_admission_rounds promoted_admission_rounds
                    on promoted_admission_rounds.cohort_id = cohorts_versions.id
                        and promoted_admission_rounds.promoted = true
        ~
    end

    # convert to view with new promoted defnition
    def version_20170426142452
        execute %Q~
            create view published_cohorts as
            select
                cohorts_versions.id
                , case
                    -- FIXME: once we've removed unnecessary cohort_promotions (https://trello.com/c/xzMZHN17/2068-chore-cleanup-after-cohort-dates-promotion-refactor)
                    -- we can stop hardcoding here which program types pay attention to cohort promotions, and just
                    -- say if either there is a cohort promotion or if there is a promoted admission round, then
                    -- the cohort is promoted
                    when cohorts_versions.program_type in ('emba', 'mba')
                        then promoted_admission_rounds.cohort_id is not null
                    ELSE
                        cohort_promotions.cohort_id is not null
                    end as promoted
                , cohorts_versions.name
                , cohorts_versions.program_type
                , cohorts_versions.required_playlist_pack_ids
                , cohorts_versions.specialization_playlist_pack_ids
                , cohorts_versions.start_date
                , cohorts_versions.end_date
                , cohorts_versions.version_id
            from cohorts_versions
                join content_publishers on content_publishers.cohort_version_id = cohorts_versions.version_id
                left join cohort_promotions
                    on cohort_promotions.cohort_id = cohorts_versions.id
                left join published_cohort_admission_rounds promoted_admission_rounds
                    on promoted_admission_rounds.cohort_id = cohorts_versions.id
                        and promoted_admission_rounds.promoted = true
        ~
    end

    # moving promoted status to a separate table
    def version_20170327195041
        execute %Q~
            create materialized view published_cohorts as
            select
                cohorts_versions.id
                , cohort_promotions.cohort_id is not null as promoted
                , cohorts_versions.name
                , cohorts_versions.program_type
                , cohorts_versions.required_playlist_pack_ids
                , cohorts_versions.specialization_playlist_pack_ids
                , cohorts_versions.start_date
                , cohorts_versions.end_date
                , cohorts_versions.version_id
            from cohorts_versions
                join content_publishers on content_publishers.cohort_version_id = cohorts_versions.version_id
                left join cohort_promotions
                    on cohort_promotions.cohort_id = cohorts_versions.id
        ~

        add_index :published_cohorts, :id, :unique => true

        # so long as promoted cohorts is always changed within a transacation where
        # this view gets updated, it should be impossible to promote 2 cohorts of the
        # same program_type at once
        add_index :published_cohorts, [:program_type, :promoted], :where => "promoted = TRUE", :unique => true
    end

    # take away "with no data" so that this always
    # available immediately after migration completes
    #
    # replace playlist_pack_ids with required_playlist_pack_ids and optional_playlist_pack_ids
    def version_20170310204902
        execute %Q~
            create materialized view published_cohorts as
            select
                cohorts_versions.id
                , cohorts.promoted -- this comes from cohorts table
                , cohorts_versions.name
                , cohorts_versions.required_playlist_pack_ids
                , cohorts_versions.specialization_playlist_pack_ids
                , cohorts_versions.start_date
                , cohorts_versions.end_date
                , cohorts_versions.version_id
            from cohorts
                join cohorts_versions on cohorts_versions.id = cohorts.id
                join content_publishers on content_publishers.cohort_version_id = cohorts_versions.version_id
        ~

        add_index :published_cohorts, :id, :unique => true
    end

    def version_20170306001915
        execute %Q~
            create materialized view published_cohorts as
            select
                cohorts_versions.id
                , cohorts.promoted -- this comes from cohorts table
                , cohorts_versions.name
                , cohorts_versions.playlist_pack_ids
                , cohorts_versions.start_date
                , cohorts_versions.end_date
            from cohorts
                join cohorts_versions on cohorts_versions.id = cohorts.id
                join content_publishers on content_publishers.cohort_version_id = cohorts_versions.version_id
            with no data
        ~

        add_index :published_cohorts, :id, :unique => true
    end

    def version_20170220200244
        execute %Q~
            create materialized view published_cohorts as
            select
                cohorts_versions.id
                , cohorts.promoted -- this comes from cohorts table
                , cohorts_versions.name_locales
                , cohorts_versions.playlist_pack_ids
                , cohorts_versions.start_date
                , cohorts_versions.end_date
            from cohorts
                join cohorts_versions on cohorts_versions.id = cohorts.id
                join content_publishers on content_publishers.cohort_version_id = cohorts_versions.version_id
            with no data
        ~

        add_index :published_cohorts, :id, :unique => true
    end

end