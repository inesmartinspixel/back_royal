class ViewHelpers::PublishedLessons < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    def version_20170324205906
        current
    end

    def version_20170220200244
        execute %Q~
            create materialized view published_lessons as
            select
                lessons.id,
                lessons.locale_pack_id,
                lessons_versions.title,
                lessons_versions.version_id,
                lessons_versions.locale,
                lessons_versions.assessment,
                lessons_versions.test
            from lessons
                join lessons_versions on lessons.id = lessons_versions.id
                join content_publishers on content_publishers.lesson_version_id = lessons_versions.version_id
            with no data
        ~

        add_index :published_lessons, :id, :unique => :true
        add_index :published_lessons, :locale_pack_id
    end

end