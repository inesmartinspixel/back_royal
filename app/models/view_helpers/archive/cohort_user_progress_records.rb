class ViewHelpers::CohortUserProgressRecords < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    # Materialized CTEs
    def version_20200625152000
        current
    end

    # remove dependency on published_cohort_content_details
    def version_20200529191133
        execute %Q|
            create materialized view cohort_user_progress_records as
            with current_status as (
                select
                    cohort_applications_plus.cohort_id
                    , cohort_applications_plus.user_id
                    , cohort_applications_plus.cohort_name
                    , published_cohorts.program_type
                    , published_cohorts.start_date
                    , cohort_status_changes.status
                from cohort_applications_plus
                    join published_cohorts
                        on published_cohorts.id = cohort_applications_plus.cohort_id
                    join cohort_status_changes on
                        cohort_status_changes.cohort_id = cohort_applications_plus.cohort_id
                        and cohort_status_changes.user_id = cohort_applications_plus.user_id
                        and cohort_status_changes.until_time = '01-01-2099'
                where
                    cohort_applications_plus.was_accepted=true
                    and published_cohorts.program_type != 'career_network_only'
            )

            -- For emba (or any cohort with required specializations), we only count the test
            -- scores from the n best specializations, where n is the num_required_specializations,
            -- This ensures that people are not penalized for doing extra ones that are more difficult
            --
            -- First step is to grab the average test score from each specialization.  It is currently
            -- the case that each specialization only has one test stream with one test lesson, but this
            -- query does not assume that to be the case.  If there is more than one, it averages the
            -- scores.  (See note below near avg_test_score about how we average lesson scores to get the
            -- total score)
            --
            -- See e61f9411-21ba-44cd-a4e4-eb44ea30499e for an example of a user who did extra specializations
            , specialization_test_scores as (
                SELECT
                    current_status.user_id
                    , current_status.cohort_id
                    , playlists.playlist_title
                    , lessons.stream_title
                    , lessons.stream_locale_pack_id
                    , cohorts_versions.num_required_specializations
                    , array_agg(lessons.lesson_title) lessons
                    , array_agg(lessons.lesson_locale_pack_id) lesson_locale_pack_ids
                    , avg(lesson_progress.official_test_score) avg_test_score
                from current_status
                    join published_cohorts
                        on published_cohorts.id = current_status.cohort_id
                    join cohorts_versions
                        on published_cohorts.version_id = cohorts_versions.version_id
                        and cohorts_versions.num_required_specializations > 0
                    join published_cohort_playlist_locale_packs playlists
                        on playlists.cohort_id = current_status.cohort_id
                        and playlists.specialization = true
                    join published_playlist_streams
                        on published_playlist_streams.playlist_locale_pack_id = playlists.playlist_locale_pack_id
                    join published_stream_lesson_locale_packs lessons
                        on lessons.stream_locale_pack_id = published_playlist_streams.stream_locale_pack_id
                        and lessons.test = true
                    join lesson_streams_progress
                        on lesson_streams_progress.locale_pack_id = lessons.stream_locale_pack_id
                        and lesson_streams_progress.user_id = current_status.user_id
                        and lesson_streams_progress.completed_at is not null
                    join user_lesson_progress_records lesson_progress
                        on lesson_progress.locale_pack_id = lessons.lesson_locale_pack_id
                        and lesson_progress.user_id = current_status.user_id
                group by

                    current_status.user_id
                    , current_status.cohort_id
                    , playlists.playlist_title
                    , lessons.stream_title
                    , lessons.stream_locale_pack_id
                    , cohorts_versions.num_required_specializations
            )

            -- Assign each specialization a rank, ordering them by best avg_test_score
            -- for each user.  So the best score for a user will have a 1, then next
            -- will have a 2, etc.
            , ranked_specialization_test_scores as (
                SELECT
                    specialization_test_scores.*
                    , rank() OVER (
                        PARTITION BY cohort_id, user_id
                        ORDER BY avg_test_score DESC
                    )
                FROM
                    specialization_test_scores
                order by
                    specialization_test_scores.cohort_id
                    , specialization_test_scores.user_id
                    , rank
            )

            -- We only count lessons from the top n specializations, where n
            -- is the num_required_specializations for the cohort.  The unnesting
            -- here handles the case where one of the specialization has multiple
            -- lessons (though this currently is never the case, see note above).
            -- Since there could be multiple test lessons in a specialization, we
            -- might count more than num_required_specializations lessons.
            , test_scores_to_count as (
                -- specialization tests
                SELECT
                    cohort_id
                    , user_id
                    , avg_test_score as official_test_score
                    , unnest(lesson_locale_pack_ids) lesson_locale_pack_id
                FROM
                    ranked_specialization_test_scores
                WHERE
                    rank <= num_required_specializations

                UNION

                -- required tests
                SELECT
                    lesson_progress.cohort_id
                    , lesson_progress.user_id
                    , lesson_progress.official_test_score
                    , lesson_progress.lesson_locale_pack_id
                from cohort_applications_plus
                    left join cohort_user_lesson_progress_records lesson_progress on
                        cohort_applications_plus.cohort_id = lesson_progress.cohort_id
                        and cohort_applications_plus.user_id = lesson_progress.user_id
                where
                    lesson_progress.test
                    and lesson_progress.required

            )

            -- We count all test scores from required lessons as well as test scores
            -- from specializations (see notes above about which ones we count).  Note that
            -- we are averaging all the test lessons.  This means that an exam with more lessons
            -- gets weighted more than an exam with fewer.  This is helpful in MBA where the final
            -- has more lessons and should have more weight.  It is also fine in EMBA, where specializations
            -- each have one lesson each.
            , test_scores as (
                SELECT
                    cohort_id
                    , user_id
                    , avg(official_test_score) as avg_test_score
                FROM
                    test_scores_to_count
                group by
                    cohort_id
                    , user_id
            )

            , lesson_counts as (
                select
                    lesson_progress.cohort_id
                    , lesson_progress.user_id
                    , sum(case
                            when foundations = true and completed_at is not null then 1
                            else 0
                            end
                        ) as foundations_lessons_complete
                    , sum(case
                            when required = true and completed_at is not null then 1
                            else 0
                            end
                        ) as required_lessons_complete
                    , sum(case
                            when test = true and completed_at is not null then 1
                            else 0
                            end
                        ) as test_lessons_complete
                    , sum(case
                            when elective = true and completed_at is not null then 1
                            else 0
                            end
                        ) as elective_lessons_complete
                    , avg(case when assessment then average_assessment_score_first else null end) as average_assessment_score_first
                    , avg(case when assessment then average_assessment_score_best else null end) as average_assessment_score_best
                    , min(case when assessment then average_assessment_score_best else null end) as min_assessment_score_best
                from cohort_applications_plus
                    left join cohort_user_lesson_progress_records lesson_progress on
                        cohort_applications_plus.cohort_id = lesson_progress.cohort_id
                        and cohort_applications_plus.user_id = lesson_progress.user_id
                where
                    cohort_applications_plus.was_accepted=true
                group by
                    lesson_progress.cohort_id
                    , lesson_progress.user_id
            )
            , stream_counts as (
                select
                    cohort_applications_plus.cohort_id
                    , cohort_applications_plus.user_id
                    , sum(case
                            when foundations = true and completed_at is not null then 1
                            else 0
                            end
                        ) as foundations_streams_complete
                    , sum(case
                            when required = true and completed_at is not null then 1
                            else 0
                            end
                        ) as required_streams_complete
                    , sum(case
                            when exam = true and completed_at is not null then 1
                            else 0
                            end
                        ) as exam_streams_complete
                    , sum(case
                            when elective = true and completed_at is not null then 1
                            else 0
                            end
                        ) as elective_streams_complete
                from cohort_applications_plus
                    join published_cohort_stream_locale_packs
                        on published_cohort_stream_locale_packs.cohort_id = cohort_applications_plus.cohort_id
                    left join lesson_streams_progress
                        on published_cohort_stream_locale_packs.stream_locale_pack_id = lesson_streams_progress.locale_pack_id
                        and cohort_applications_plus.user_id = lesson_streams_progress.user_id
                where
                    cohort_applications_plus.was_accepted=true
                group by
                    cohort_applications_plus.cohort_id
                    , cohort_applications_plus.user_id
            )
            , playlist_counts as (
                select
                    cohort_applications_plus.cohort_id
                    , cohort_applications_plus.user_id
                    , sum(case
                            when required = true and completed = true then 1
                            else 0
                            end
                        ) as required_playlists_complete
                    , sum(case
                            when specialization = true and completed = true then 1
                            else 0
                            end
                        ) as specialization_playlists_complete
                from cohort_applications_plus
                    join published_cohort_playlist_locale_packs
                        on published_cohort_playlist_locale_packs.cohort_id = cohort_applications_plus.cohort_id
                    left join playlist_progress
                        on published_cohort_playlist_locale_packs.playlist_locale_pack_id = playlist_progress.locale_pack_id
                        and cohort_applications_plus.user_id = playlist_progress.user_id
                where
                    cohort_applications_plus.was_accepted=true
                group by
                    cohort_applications_plus.cohort_id
                    , cohort_applications_plus.user_id
            )
            , with_counts as (
                select
                    current_status.cohort_name
                    , current_status.cohort_id
                    , current_status.program_type
                    , current_status.start_date
                    , current_status.user_id
                    , current_status.status

                    , stream_counts.required_streams_complete
                    , stream_counts.foundations_streams_complete
                    , stream_counts.exam_streams_complete
                    , stream_counts.elective_streams_complete

                    , playlist_counts.required_playlists_complete
                    , playlist_counts.specialization_playlists_complete

                    , lesson_counts.required_lessons_complete
                    , lesson_counts.foundations_lessons_complete
                    , lesson_counts.test_lessons_complete
                    , lesson_counts.elective_lessons_complete
                    , lesson_counts.average_assessment_score_first
                    , lesson_counts.average_assessment_score_best
                    , lesson_counts.min_assessment_score_best
                    , test_scores.avg_test_score

                from current_status
                    join lesson_counts
                        on current_status.cohort_id = lesson_counts.cohort_id
                        and current_status.user_id = lesson_counts.user_id
                    join stream_counts
                        on current_status.cohort_id = stream_counts.cohort_id
                        and current_status.user_id = stream_counts.user_id
                    join playlist_counts
                        on current_status.cohort_id = playlist_counts.cohort_id
                        and current_status.user_id = playlist_counts.user_id
                    join test_scores
                        on current_status.cohort_id = test_scores.cohort_id
                        and current_status.user_id = test_scores.user_id


            )
            , lesson_info as (
                select
                    cohort_id
                    , count(distinct lesson_locale_pack_id) filter (where published_cohort_lesson_locale_packs.required = true) as required_lesson_count
                    , count(distinct lesson_locale_pack_id) filter (where published_cohort_lesson_locale_packs.elective = true) as elective_lesson_count
                from
                    published_cohort_lesson_locale_packs
                group by
                    cohort_id
            )
            , stream_info as (
                select
                    cohort_id
                    , count(distinct stream_locale_pack_id) filter (where published_cohort_stream_locale_packs.required = true) as required_stream_count
                    , count(distinct stream_locale_pack_id) filter (where published_cohort_stream_locale_packs.elective = true) as elective_stream_count
                from
                    published_cohort_stream_locale_packs
                group by
                    cohort_id
            )
            , with_averages as (
                select
                    with_counts.*

                    -- no entry in the table means you did not participate at all, and get a score of 0
                    , coalesce(user_participation_scores.score, 0) as participation_score
                    , coalesce(cohort_applications_plus.project_score, 0) as project_score

                    , cohort_applications_plus.final_score
                    , cohort_applications_plus.meets_graduation_requirements
                    , required_streams_complete::float / stream_info.required_stream_count as required_streams_perc_complete
                    , required_lessons_complete::float / lesson_info.required_lesson_count as required_lessons_perc_complete
                    , case when lesson_info.elective_lesson_count > 0
                        then elective_lessons_complete::float / lesson_info.elective_lesson_count
                        else null
                        end as elective_lessons_perc_complete
                    , case when stream_info.elective_stream_count > 0
                        then elective_streams_complete::float / stream_info.elective_stream_count
                        else null
                        end as elective_streams_perc_complete
                from with_counts
                    join stream_info
                        on with_counts.cohort_id = stream_info.cohort_id
                    join lesson_info
                        on with_counts.cohort_id = lesson_info.cohort_id
                    left join user_participation_scores
                        on user_participation_scores.user_id = with_counts.user_id
                    left join cohort_applications_plus
                        on cohort_applications_plus.cohort_id = with_counts.cohort_id
                        and cohort_applications_plus.user_id = with_counts.user_id
            )
            select * from with_averages
            with no data
        |

        add_index :cohort_user_progress_records, [:cohort_id, :user_id], :unique => true
    end

    # pull meets_graduation_requirements from cohort application
    def version_20190107195906
        execute %Q|
            create materialized view cohort_user_progress_records as
            with current_status as (
                select
                    cohort_applications_plus.cohort_id
                    , cohort_applications_plus.user_id
                    , cohort_applications_plus.cohort_name
                    , published_cohorts.program_type
                    , published_cohorts.start_date
                    , cohort_status_changes.status
                from cohort_applications_plus
                    join published_cohorts
                        on published_cohorts.id = cohort_applications_plus.cohort_id
                    join cohort_status_changes on
                        cohort_status_changes.cohort_id = cohort_applications_plus.cohort_id
                        and cohort_status_changes.user_id = cohort_applications_plus.user_id
                        and cohort_status_changes.until_time = '01-01-2099'
                where
                    cohort_applications_plus.was_accepted=true
                    and published_cohorts.program_type != 'career_network_only'
            )

            -- For emba (or any cohort with required specializations), we only count the test
            -- scores from the n best specializations, where n is the num_required_specializations,
            -- This ensures that people are not penalized for doing extra ones that are more difficult
            --
            -- First step is to grab the average test score from each specialization.  It is currently
            -- the case that each specialization only has one test stream with one test lesson, but this
            -- query does not assume that to be the case.  If there is more than one, it averages the
            -- scores.  (See note below near avg_test_score about how we average lesson scores to get the
            -- total score)
            --
            -- See e61f9411-21ba-44cd-a4e4-eb44ea30499e for an example of a user who did extra specializations
            , specialization_test_scores as (
                SELECT
                    current_status.user_id
                    , current_status.cohort_id
                    , playlists.playlist_title
                    , lessons.stream_title
                    , lessons.stream_locale_pack_id
                    , cohorts_versions.num_required_specializations
                    , array_agg(lessons.lesson_title) lessons
                    , array_agg(lessons.lesson_locale_pack_id) lesson_locale_pack_ids
                    , avg(lesson_progress.official_test_score) avg_test_score
                from current_status
                    join published_cohorts
                        on published_cohorts.id = current_status.cohort_id
                    join cohorts_versions
                        on published_cohorts.version_id = cohorts_versions.version_id
                        and cohorts_versions.num_required_specializations > 0
                    join published_cohort_playlist_locale_packs playlists
                        on playlists.cohort_id = current_status.cohort_id
                        and playlists.specialization = true
                    join published_playlist_streams
                        on published_playlist_streams.playlist_locale_pack_id = playlists.playlist_locale_pack_id
                    join published_stream_lesson_locale_packs lessons
                        on lessons.stream_locale_pack_id = published_playlist_streams.stream_locale_pack_id
                        and lessons.test = true
                    join lesson_streams_progress
                        on lesson_streams_progress.locale_pack_id = lessons.stream_locale_pack_id
                        and lesson_streams_progress.user_id = current_status.user_id
                        and lesson_streams_progress.completed_at is not null
                    join user_lesson_progress_records lesson_progress
                        on lesson_progress.locale_pack_id = lessons.lesson_locale_pack_id
                        and lesson_progress.user_id = current_status.user_id
                group by

                    current_status.user_id
                    , current_status.cohort_id
                    , playlists.playlist_title
                    , lessons.stream_title
                    , lessons.stream_locale_pack_id
                    , cohorts_versions.num_required_specializations
            )

            -- Assign each specialization a rank, ordering them by best avg_test_score
            -- for each user.  So the best score for a user will have a 1, then next
            -- will have a 2, etc.
            , ranked_specialization_test_scores as (
                SELECT
                    specialization_test_scores.*
                    , rank() OVER (
                        PARTITION BY cohort_id, user_id
                        ORDER BY avg_test_score DESC
                    )
                FROM
                    specialization_test_scores
                order by
                    specialization_test_scores.cohort_id
                    , specialization_test_scores.user_id
                    , rank
            )

            -- We only count lessons from the top n specializations, where n
            -- is the num_required_specializations for the cohort.  The unnesting
            -- here handles the case where one of the specialization has multiple
            -- lessons (though this currently is never the case, see note above).
            -- Since there could be multiple test lessons in a specialization, we
            -- might count more than num_required_specializations lessons.
            , test_scores_to_count as (
                -- specialization tests
                SELECT
                    cohort_id
                    , user_id
                    , avg_test_score as official_test_score
                    , unnest(lesson_locale_pack_ids) lesson_locale_pack_id
                FROM
                    ranked_specialization_test_scores
                WHERE
                    rank <= num_required_specializations

                UNION

                -- required tests
                SELECT
                    lesson_progress.cohort_id
                    , lesson_progress.user_id
                    , lesson_progress.official_test_score
                    , lesson_progress.lesson_locale_pack_id
                from cohort_applications_plus
                    left join cohort_user_lesson_progress_records lesson_progress on
                        cohort_applications_plus.cohort_id = lesson_progress.cohort_id
                        and cohort_applications_plus.user_id = lesson_progress.user_id
                where
                    lesson_progress.test
                    and lesson_progress.required

            )

            -- We count all test scores from required lessons as well as test scores
            -- from specializations (see notes above about which ones we count).  Note that
            -- we are averaging all the test lessons.  This means that an exam with more lessons
            -- gets weighted more than an exam with fewer.  This is helpful in MBA where the final
            -- has more lessons and should have more weight.  It is also fine in EMBA, where specializations
            -- each have one lesson each.
            , test_scores as (
                SELECT
                    cohort_id
                    , user_id
                    , avg(official_test_score) as avg_test_score
                FROM
                    test_scores_to_count
                group by
                    cohort_id
                    , user_id
            )

            , lesson_counts as (
                select
                    lesson_progress.cohort_id
                    , lesson_progress.user_id
                    , sum(case
                            when foundations = true and completed_at is not null then 1
                            else 0
                            end
                        ) as foundations_lessons_complete
                    , sum(case
                            when required = true and completed_at is not null then 1
                            else 0
                            end
                        ) as required_lessons_complete
                    , sum(case
                            when test = true and completed_at is not null then 1
                            else 0
                            end
                        ) as test_lessons_complete
                    , sum(case
                            when elective = true and completed_at is not null then 1
                            else 0
                            end
                        ) as elective_lessons_complete
                    , avg(case when assessment then average_assessment_score_first else null end) as average_assessment_score_first
                    , avg(case when assessment then average_assessment_score_best else null end) as average_assessment_score_best
                    , min(case when assessment then average_assessment_score_best else null end) as min_assessment_score_best
                from cohort_applications_plus
                    left join cohort_user_lesson_progress_records lesson_progress on
                        cohort_applications_plus.cohort_id = lesson_progress.cohort_id
                        and cohort_applications_plus.user_id = lesson_progress.user_id
                where
                    cohort_applications_plus.was_accepted=true
                group by
                    lesson_progress.cohort_id
                    , lesson_progress.user_id
            )
            , stream_counts as (
                select
                    cohort_applications_plus.cohort_id
                    , cohort_applications_plus.user_id
                    , sum(case
                            when foundations = true and completed_at is not null then 1
                            else 0
                            end
                        ) as foundations_streams_complete
                    , sum(case
                            when required = true and completed_at is not null then 1
                            else 0
                            end
                        ) as required_streams_complete
                    , sum(case
                            when exam = true and completed_at is not null then 1
                            else 0
                            end
                        ) as exam_streams_complete
                    , sum(case
                            when elective = true and completed_at is not null then 1
                            else 0
                            end
                        ) as elective_streams_complete
                from cohort_applications_plus
                    join published_cohort_stream_locale_packs
                        on published_cohort_stream_locale_packs.cohort_id = cohort_applications_plus.cohort_id
                    left join lesson_streams_progress
                        on published_cohort_stream_locale_packs.stream_locale_pack_id = lesson_streams_progress.locale_pack_id
                        and cohort_applications_plus.user_id = lesson_streams_progress.user_id
                where
                    cohort_applications_plus.was_accepted=true
                group by
                    cohort_applications_plus.cohort_id
                    , cohort_applications_plus.user_id
            )
            , playlist_counts as (
                select
                    cohort_applications_plus.cohort_id
                    , cohort_applications_plus.user_id
                    , sum(case
                            when required = true and completed = true then 1
                            else 0
                            end
                        ) as required_playlists_complete
                    , sum(case
                            when specialization = true and completed = true then 1
                            else 0
                            end
                        ) as specialization_playlists_complete
                from cohort_applications_plus
                    join published_cohort_playlist_locale_packs
                        on published_cohort_playlist_locale_packs.cohort_id = cohort_applications_plus.cohort_id
                    left join playlist_progress
                        on published_cohort_playlist_locale_packs.playlist_locale_pack_id = playlist_progress.locale_pack_id
                        and cohort_applications_plus.user_id = playlist_progress.user_id
                where
                    cohort_applications_plus.was_accepted=true
                group by
                    cohort_applications_plus.cohort_id
                    , cohort_applications_plus.user_id
            )
            , with_counts as (
                select
                    current_status.cohort_name
                    , current_status.cohort_id
                    , current_status.program_type
                    , current_status.start_date
                    , current_status.user_id
                    , current_status.status

                    , stream_counts.required_streams_complete
                    , stream_counts.foundations_streams_complete
                    , stream_counts.exam_streams_complete
                    , stream_counts.elective_streams_complete

                    , playlist_counts.required_playlists_complete
                    , playlist_counts.specialization_playlists_complete

                    , lesson_counts.required_lessons_complete
                    , lesson_counts.foundations_lessons_complete
                    , lesson_counts.test_lessons_complete
                    , lesson_counts.elective_lessons_complete
                    , lesson_counts.average_assessment_score_first
                    , lesson_counts.average_assessment_score_best
                    , lesson_counts.min_assessment_score_best
                    , test_scores.avg_test_score

                from current_status
                    join lesson_counts
                        on current_status.cohort_id = lesson_counts.cohort_id
                        and current_status.user_id = lesson_counts.user_id
                    join stream_counts
                        on current_status.cohort_id = stream_counts.cohort_id
                        and current_status.user_id = stream_counts.user_id
                    join playlist_counts
                        on current_status.cohort_id = playlist_counts.cohort_id
                        and current_status.user_id = playlist_counts.user_id
                    join test_scores
                        on current_status.cohort_id = test_scores.cohort_id
                        and current_status.user_id = test_scores.user_id


            )
            , with_averages as (
                select
                    with_counts.*

                    -- no entry in the table means you did not participate at all, and get a score of 0
                    , coalesce(user_participation_scores.score, 0) as participation_score
                    , coalesce(cohort_applications_plus.project_score, 0) as project_score

                    , cohort_applications_plus.final_score
                    , cohort_applications_plus.meets_graduation_requirements
                    , required_streams_complete::float / cohort_content_details.required_stream_count as required_streams_perc_complete
                    , required_lessons_complete::float / cohort_content_details.required_lesson_count as required_lessons_perc_complete
                    , case when cohort_content_details.elective_lesson_count > 0
                        then elective_lessons_complete::float / cohort_content_details.elective_lesson_count
                        else null
                        end as elective_lessons_perc_complete
                    , case when cohort_content_details.elective_stream_count > 0
                        then elective_streams_complete::float / cohort_content_details.elective_stream_count
                        else null
                        end as elective_streams_perc_complete
                from with_counts
                    join published_cohort_content_details cohort_content_details
                        on with_counts.cohort_id = cohort_content_details.cohort_id
                    left join user_participation_scores
                        on user_participation_scores.user_id = with_counts.user_id
                    left join cohort_applications_plus
                        on cohort_applications_plus.cohort_id = with_counts.cohort_id
                        and cohort_applications_plus.user_id = with_counts.user_id
            )
            select * from with_averages
            with no data
        |

        add_index :cohort_user_progress_records, [:cohort_id, :user_id], :unique => true
    end

    # pull project score from cohort application
    def version_20181212213605
        execute %Q|
            create materialized view cohort_user_progress_records as
            with current_status as (
                select
                    cohort_applications_plus.cohort_id
                    , cohort_applications_plus.user_id
                    , cohort_applications_plus.cohort_name
                    , published_cohorts.program_type
                    , published_cohorts.start_date
                    , cohort_status_changes.status
                from cohort_applications_plus
                    join published_cohorts
                        on published_cohorts.id = cohort_applications_plus.cohort_id
                    join cohort_status_changes on
                        cohort_status_changes.cohort_id = cohort_applications_plus.cohort_id
                        and cohort_status_changes.user_id = cohort_applications_plus.user_id
                        and cohort_status_changes.until_time = '01-01-2099'
                where
                    cohort_applications_plus.was_accepted=true
                    and published_cohorts.program_type != 'career_network_only'
            )

            -- For emba (or any cohort with required specializations), we only count the test
            -- scores from the n best specializations, where n is the num_required_specializations,
            -- This ensures that people are not penalized for doing extra ones that are more difficult
            --
            -- First step is to grab the average test score from each specialization.  It is currently
            -- the case that each specialization only has one test stream with one test lesson, but this
            -- query does not assume that to be the case.  If there is more than one, it averages the
            -- scores.  (See note below near avg_test_score about how we average lesson scores to get the
            -- total score)
            --
            -- See e61f9411-21ba-44cd-a4e4-eb44ea30499e for an example of a user who did extra specializations
            , specialization_test_scores as (
                SELECT
                    current_status.user_id
                    , current_status.cohort_id
                    , playlists.playlist_title
                    , lessons.stream_title
                    , lessons.stream_locale_pack_id
                    , cohorts_versions.num_required_specializations
                    , array_agg(lessons.lesson_title) lessons
                    , array_agg(lessons.lesson_locale_pack_id) lesson_locale_pack_ids
                    , avg(lesson_progress.official_test_score) avg_test_score
                from current_status
                    join published_cohorts
                        on published_cohorts.id = current_status.cohort_id
                    join cohorts_versions
                        on published_cohorts.version_id = cohorts_versions.version_id
                        and cohorts_versions.num_required_specializations > 0
                    join published_cohort_playlist_locale_packs playlists
                        on playlists.cohort_id = current_status.cohort_id
                        and playlists.specialization = true
                    join published_playlist_streams
                        on published_playlist_streams.playlist_locale_pack_id = playlists.playlist_locale_pack_id
                    join published_stream_lesson_locale_packs lessons
                        on lessons.stream_locale_pack_id = published_playlist_streams.stream_locale_pack_id
                        and lessons.test = true
                    join lesson_streams_progress
                        on lesson_streams_progress.locale_pack_id = lessons.stream_locale_pack_id
                        and lesson_streams_progress.user_id = current_status.user_id
                        and lesson_streams_progress.completed_at is not null
                    join user_lesson_progress_records lesson_progress
                        on lesson_progress.locale_pack_id = lessons.lesson_locale_pack_id
                        and lesson_progress.user_id = current_status.user_id
                group by

                    current_status.user_id
                    , current_status.cohort_id
                    , playlists.playlist_title
                    , lessons.stream_title
                    , lessons.stream_locale_pack_id
                    , cohorts_versions.num_required_specializations
            )

            -- Assign each specialization a rank, ordering them by best avg_test_score
            -- for each user.  So the best score for a user will have a 1, then next
            -- will have a 2, etc.
            , ranked_specialization_test_scores as (
                SELECT
                    specialization_test_scores.*
                    , rank() OVER (
                        PARTITION BY cohort_id, user_id
                        ORDER BY avg_test_score DESC
                    )
                FROM
                    specialization_test_scores
                order by
                    specialization_test_scores.cohort_id
                    , specialization_test_scores.user_id
                    , rank
            )

            -- We only count lessons from the top n specializations, where n
            -- is the num_required_specializations for the cohort.  The unnesting
            -- here handles the case where one of the specialization has multiple
            -- lessons (though this currently is never the case, see note above).
            -- Since there could be multiple test lessons in a specialization, we
            -- might count more than num_required_specializations lessons.
            , test_scores_to_count as (
                -- specialization tests
                SELECT
                    cohort_id
                    , user_id
                    , avg_test_score as official_test_score
                    , unnest(lesson_locale_pack_ids) lesson_locale_pack_id
                FROM
                    ranked_specialization_test_scores
                WHERE
                    rank <= num_required_specializations

                UNION

                -- required tests
                SELECT
                    lesson_progress.cohort_id
                    , lesson_progress.user_id
                    , lesson_progress.official_test_score
                    , lesson_progress.lesson_locale_pack_id
                from cohort_applications_plus
                    left join cohort_user_lesson_progress_records lesson_progress on
                        cohort_applications_plus.cohort_id = lesson_progress.cohort_id
                        and cohort_applications_plus.user_id = lesson_progress.user_id
                where
                    lesson_progress.test
                    and lesson_progress.required

            )

            -- We count all test scores from required lessons as well as test scores
            -- from specializations (see notes above about which ones we count).  Note that
            -- we are averaging all the test lessons.  This means that an exam with more lessons
            -- gets weighted more than an exam with fewer.  This is helpful in MBA where the final
            -- has more lessons and should have more weight.  It is also fine in EMBA, where specializations
            -- each have one lesson each.
            , test_scores as (
                SELECT
                    cohort_id
                    , user_id
                    , avg(official_test_score) as avg_test_score
                FROM
                    test_scores_to_count
                group by
                    cohort_id
                    , user_id
            )

            , lesson_counts as (
                select
                    lesson_progress.cohort_id
                    , lesson_progress.user_id
                    , sum(case
                            when foundations = true and completed_at is not null then 1
                            else 0
                            end
                        ) as foundations_lessons_complete
                    , sum(case
                            when required = true and completed_at is not null then 1
                            else 0
                            end
                        ) as required_lessons_complete
                    , sum(case
                            when test = true and completed_at is not null then 1
                            else 0
                            end
                        ) as test_lessons_complete
                    , sum(case
                            when elective = true and completed_at is not null then 1
                            else 0
                            end
                        ) as elective_lessons_complete
                    , avg(case when assessment then average_assessment_score_first else null end) as average_assessment_score_first
                    , avg(case when assessment then average_assessment_score_best else null end) as average_assessment_score_best
                    , min(case when assessment then average_assessment_score_best else null end) as min_assessment_score_best
                from cohort_applications_plus
                    left join cohort_user_lesson_progress_records lesson_progress on
                        cohort_applications_plus.cohort_id = lesson_progress.cohort_id
                        and cohort_applications_plus.user_id = lesson_progress.user_id
                where
                    cohort_applications_plus.was_accepted=true
                group by
                    lesson_progress.cohort_id
                    , lesson_progress.user_id
            )
            , stream_counts as (
                select
                    cohort_applications_plus.cohort_id
                    , cohort_applications_plus.user_id
                    , sum(case
                            when foundations = true and completed_at is not null then 1
                            else 0
                            end
                        ) as foundations_streams_complete
                    , sum(case
                            when required = true and completed_at is not null then 1
                            else 0
                            end
                        ) as required_streams_complete
                    , sum(case
                            when exam = true and completed_at is not null then 1
                            else 0
                            end
                        ) as exam_streams_complete
                    , sum(case
                            when elective = true and completed_at is not null then 1
                            else 0
                            end
                        ) as elective_streams_complete
                from cohort_applications_plus
                    join published_cohort_stream_locale_packs
                        on published_cohort_stream_locale_packs.cohort_id = cohort_applications_plus.cohort_id
                    left join lesson_streams_progress
                        on published_cohort_stream_locale_packs.stream_locale_pack_id = lesson_streams_progress.locale_pack_id
                        and cohort_applications_plus.user_id = lesson_streams_progress.user_id
                where
                    cohort_applications_plus.was_accepted=true
                group by
                    cohort_applications_plus.cohort_id
                    , cohort_applications_plus.user_id
            )
            , playlist_counts as (
                select
                    cohort_applications_plus.cohort_id
                    , cohort_applications_plus.user_id
                    , sum(case
                            when required = true and completed = true then 1
                            else 0
                            end
                        ) as required_playlists_complete
                    , sum(case
                            when specialization = true and completed = true then 1
                            else 0
                            end
                        ) as specialization_playlists_complete
                from cohort_applications_plus
                    join published_cohort_playlist_locale_packs
                        on published_cohort_playlist_locale_packs.cohort_id = cohort_applications_plus.cohort_id
                    left join playlist_progress
                        on published_cohort_playlist_locale_packs.playlist_locale_pack_id = playlist_progress.locale_pack_id
                        and cohort_applications_plus.user_id = playlist_progress.user_id
                where
                    cohort_applications_plus.was_accepted=true
                group by
                    cohort_applications_plus.cohort_id
                    , cohort_applications_plus.user_id
            )
            , with_counts as (
                select
                    current_status.cohort_name
                    , current_status.cohort_id
                    , current_status.program_type
                    , current_status.start_date
                    , current_status.user_id
                    , current_status.status

                    , stream_counts.required_streams_complete
                    , stream_counts.foundations_streams_complete
                    , stream_counts.exam_streams_complete
                    , stream_counts.elective_streams_complete

                    , playlist_counts.required_playlists_complete
                    , playlist_counts.specialization_playlists_complete

                    , lesson_counts.required_lessons_complete
                    , lesson_counts.foundations_lessons_complete
                    , lesson_counts.test_lessons_complete
                    , lesson_counts.elective_lessons_complete
                    , lesson_counts.average_assessment_score_first
                    , lesson_counts.average_assessment_score_best
                    , lesson_counts.min_assessment_score_best
                    , test_scores.avg_test_score

                from current_status
                    join lesson_counts
                        on current_status.cohort_id = lesson_counts.cohort_id
                        and current_status.user_id = lesson_counts.user_id
                    join stream_counts
                        on current_status.cohort_id = stream_counts.cohort_id
                        and current_status.user_id = stream_counts.user_id
                    join playlist_counts
                        on current_status.cohort_id = playlist_counts.cohort_id
                        and current_status.user_id = playlist_counts.user_id
                    join test_scores
                        on current_status.cohort_id = test_scores.cohort_id
                        and current_status.user_id = test_scores.user_id


            )
            , with_averages as (
                select
                    with_counts.*

                    -- no entry in the table means you did not participate at all, and get a score of 0
                    , coalesce(user_participation_scores.score, 0) as participation_score
                    , coalesce(cohort_applications_plus.project_score, 0) as project_score

                    , cohort_applications_plus.final_score
                    , required_streams_complete::float / cohort_content_details.required_stream_count as required_streams_perc_complete
                    , required_lessons_complete::float / cohort_content_details.required_lesson_count as required_lessons_perc_complete
                    , case when cohort_content_details.elective_lesson_count > 0
                        then elective_lessons_complete::float / cohort_content_details.elective_lesson_count
                        else null
                        end as elective_lessons_perc_complete
                    , case when cohort_content_details.elective_stream_count > 0
                        then elective_streams_complete::float / cohort_content_details.elective_stream_count
                        else null
                        end as elective_streams_perc_complete
                from with_counts
                    join published_cohort_content_details cohort_content_details
                        on with_counts.cohort_id = cohort_content_details.cohort_id
                    left join user_participation_scores
                        on user_participation_scores.user_id = with_counts.user_id
                    left join cohort_applications_plus
                        on cohort_applications_plus.cohort_id = with_counts.cohort_id
                          and cohort_applications_plus.user_id = with_counts.user_id
            )

            , with_grad_requirements as (
                select
                    with_averages.*
                    , case
                        -- No projects before MBA8 (and specializations are irrelevant to mba anyway)
                        when with_averages.program_type = 'mba' and with_averages.start_date < (select start_date from published_cohorts where name='MBA8')
                            then final_score > 0.7
                                    and required_streams_perc_complete = 1

                        -- For all emba cohorts, and mba starting with MBA8, we require a
                        -- project_score of 1, meaning that all projects are complete (irrelevant
                        -- of the score we reported to the student on each project)
                        --
                        -- `num_required_specializations` is always 0 for mba cohorts, so this
                        -- piece of the requirement is only relevant for emba.
                        when with_averages.program_type in ('mba', 'emba')
                            then
                                final_score > 0.7 -- see also StreamProgress#mark_application_completed where we auto-graduate if the score is > 0.7
                                    and required_streams_perc_complete = 1
                                    and specialization_playlists_complete >= cohorts_versions.num_required_specializations
                                    and project_score = 1
                        when with_averages.program_type  = 'the_business_certificate'
                            then
                                final_score > 0.7 -- see also StreamProgress#mark_application_completed where we auto-graduate if the score is > 0.7
                                    and required_streams_perc_complete = 1
                        when is_paid_cert_program_type(with_averages.program_type)
                            then
                                min_assessment_score_best > 0.8
                                and required_streams_perc_complete = 1
                                and project_score >= 3

                    end as meets_graduation_requirements
                from with_averages
                    join published_cohorts on with_averages.cohort_id = published_cohorts.id
                    join cohorts_versions on published_cohorts.version_id = cohorts_versions.version_id
            )
            select * from with_grad_requirements
            with no data
        |

        add_index :cohort_user_progress_records, [:cohort_id, :user_id], :unique => true
    end

    # remove final_score calculation since it is now on the cohort_application
    def version_20181211145713
        execute %Q|
            create materialized view cohort_user_progress_records as
            with current_status as (
                select
                    cohort_applications_plus.cohort_id
                    , cohort_applications_plus.user_id
                    , cohort_applications_plus.cohort_name
                    , published_cohorts.program_type
                    , published_cohorts.start_date
                    , cohort_status_changes.status
                from cohort_applications_plus
                    join published_cohorts
                        on published_cohorts.id = cohort_applications_plus.cohort_id
                    join cohort_status_changes on
                        cohort_status_changes.cohort_id = cohort_applications_plus.cohort_id
                        and cohort_status_changes.user_id = cohort_applications_plus.user_id
                        and cohort_status_changes.until_time = '01-01-2099'
                where
                    cohort_applications_plus.was_accepted=true
                    and published_cohorts.program_type != 'career_network_only'
            )

            -- For emba (or any cohort with required specializations), we only count the test
            -- scores from the n best specializations, where n is the num_required_specializations,
            -- This ensures that people are not penalized for doing extra ones that are more difficult
            --
            -- First step is to grab the average test score from each specialization.  It is currently
            -- the case that each specialization only has one test stream with one test lesson, but this
            -- query does not assume that to be the case.  If there is more than one, it averages the
            -- scores.  (See note below near avg_test_score about how we average lesson scores to get the
            -- total score)
            --
            -- See e61f9411-21ba-44cd-a4e4-eb44ea30499e for an example of a user who did extra specializations
            , specialization_test_scores as (
                SELECT
                    current_status.user_id
                    , current_status.cohort_id
                    , playlists.playlist_title
                    , lessons.stream_title
                    , lessons.stream_locale_pack_id
                    , cohorts_versions.num_required_specializations
                    , array_agg(lessons.lesson_title) lessons
                    , array_agg(lessons.lesson_locale_pack_id) lesson_locale_pack_ids
                    , avg(lesson_progress.official_test_score) avg_test_score
                from current_status
                    join published_cohorts
                        on published_cohorts.id = current_status.cohort_id
                    join cohorts_versions
                        on published_cohorts.version_id = cohorts_versions.version_id
                        and cohorts_versions.num_required_specializations > 0
                    join published_cohort_playlist_locale_packs playlists
                        on playlists.cohort_id = current_status.cohort_id
                        and playlists.specialization = true
                    join published_playlist_streams
                        on published_playlist_streams.playlist_locale_pack_id = playlists.playlist_locale_pack_id
                    join published_stream_lesson_locale_packs lessons
                        on lessons.stream_locale_pack_id = published_playlist_streams.stream_locale_pack_id
                        and lessons.test = true
                    join lesson_streams_progress
                        on lesson_streams_progress.locale_pack_id = lessons.stream_locale_pack_id
                        and lesson_streams_progress.user_id = current_status.user_id
                        and lesson_streams_progress.completed_at is not null
                    join user_lesson_progress_records lesson_progress
                        on lesson_progress.locale_pack_id = lessons.lesson_locale_pack_id
                        and lesson_progress.user_id = current_status.user_id
                group by

                    current_status.user_id
                    , current_status.cohort_id
                    , playlists.playlist_title
                    , lessons.stream_title
                    , lessons.stream_locale_pack_id
                    , cohorts_versions.num_required_specializations
            )

            -- Assign each specialization a rank, ordering them by best avg_test_score
            -- for each user.  So the best score for a user will have a 1, then next
            -- will have a 2, etc.
            , ranked_specialization_test_scores as (
                SELECT
                    specialization_test_scores.*
                    , rank() OVER (
                        PARTITION BY cohort_id, user_id
                        ORDER BY avg_test_score DESC
                    )
                FROM
                    specialization_test_scores
                order by
                    specialization_test_scores.cohort_id
                    , specialization_test_scores.user_id
                    , rank
            )

            -- We only count lessons from the top n specializations, where n
            -- is the num_required_specializations for the cohort.  The unnesting
            -- here handles the case where one of the specialization has multiple
            -- lessons (though this currently is never the case, see note above).
            -- Since there could be multiple test lessons in a specialization, we
            -- might count more than num_required_specializations lessons.
            , test_scores_to_count as (
                -- specialization tests
                SELECT
                    cohort_id
                    , user_id
                    , avg_test_score as official_test_score
                    , unnest(lesson_locale_pack_ids) lesson_locale_pack_id
                FROM
                    ranked_specialization_test_scores
                WHERE
                    rank <= num_required_specializations

                UNION

                -- required tests
                SELECT
                    lesson_progress.cohort_id
                    , lesson_progress.user_id
                    , lesson_progress.official_test_score
                    , lesson_progress.lesson_locale_pack_id
                from cohort_applications_plus
                    left join cohort_user_lesson_progress_records lesson_progress on
                        cohort_applications_plus.cohort_id = lesson_progress.cohort_id
                        and cohort_applications_plus.user_id = lesson_progress.user_id
                where
                    lesson_progress.test
                    and lesson_progress.required

            )

            -- We count all test scores from required lessons as well as test scores
            -- from specializations (see notes above about which ones we count).  Note that
            -- we are averaging all the test lessons.  This means that an exam with more lessons
            -- gets weighted more than an exam with fewer.  This is helpful in MBA where the final
            -- has more lessons and should have more weight.  It is also fine in EMBA, where specializations
            -- each have one lesson each.
            , test_scores as (
                SELECT
                    cohort_id
                    , user_id
                    , avg(official_test_score) as avg_test_score
                FROM
                    test_scores_to_count
                group by
                    cohort_id
                    , user_id
            )

            , lesson_counts as (
                select
                    lesson_progress.cohort_id
                    , lesson_progress.user_id
                    , sum(case
                            when foundations = true and completed_at is not null then 1
                            else 0
                            end
                        ) as foundations_lessons_complete
                    , sum(case
                            when required = true and completed_at is not null then 1
                            else 0
                            end
                        ) as required_lessons_complete
                    , sum(case
                            when test = true and completed_at is not null then 1
                            else 0
                            end
                        ) as test_lessons_complete
                    , sum(case
                            when elective = true and completed_at is not null then 1
                            else 0
                            end
                        ) as elective_lessons_complete
                    , avg(case when assessment then average_assessment_score_first else null end) as average_assessment_score_first
                    , avg(case when assessment then average_assessment_score_best else null end) as average_assessment_score_best
                    , min(case when assessment then average_assessment_score_best else null end) as min_assessment_score_best
                from cohort_applications_plus
                    left join cohort_user_lesson_progress_records lesson_progress on
                        cohort_applications_plus.cohort_id = lesson_progress.cohort_id
                        and cohort_applications_plus.user_id = lesson_progress.user_id
                where
                    cohort_applications_plus.was_accepted=true
                group by
                    lesson_progress.cohort_id
                    , lesson_progress.user_id
            )
            , stream_counts as (
                select
                    cohort_applications_plus.cohort_id
                    , cohort_applications_plus.user_id
                    , sum(case
                            when foundations = true and completed_at is not null then 1
                            else 0
                            end
                        ) as foundations_streams_complete
                    , sum(case
                            when required = true and completed_at is not null then 1
                            else 0
                            end
                        ) as required_streams_complete
                    , sum(case
                            when exam = true and completed_at is not null then 1
                            else 0
                            end
                        ) as exam_streams_complete
                    , sum(case
                            when elective = true and completed_at is not null then 1
                            else 0
                            end
                        ) as elective_streams_complete
                from cohort_applications_plus
                    join published_cohort_stream_locale_packs
                        on published_cohort_stream_locale_packs.cohort_id = cohort_applications_plus.cohort_id
                    left join lesson_streams_progress
                        on published_cohort_stream_locale_packs.stream_locale_pack_id = lesson_streams_progress.locale_pack_id
                        and cohort_applications_plus.user_id = lesson_streams_progress.user_id
                where
                    cohort_applications_plus.was_accepted=true
                group by
                    cohort_applications_plus.cohort_id
                    , cohort_applications_plus.user_id
            )
            , playlist_counts as (
                select
                    cohort_applications_plus.cohort_id
                    , cohort_applications_plus.user_id
                    , sum(case
                            when required = true and completed = true then 1
                            else 0
                            end
                        ) as required_playlists_complete
                    , sum(case
                            when specialization = true and completed = true then 1
                            else 0
                            end
                        ) as specialization_playlists_complete
                from cohort_applications_plus
                    join published_cohort_playlist_locale_packs
                        on published_cohort_playlist_locale_packs.cohort_id = cohort_applications_plus.cohort_id
                    left join playlist_progress
                        on published_cohort_playlist_locale_packs.playlist_locale_pack_id = playlist_progress.locale_pack_id
                        and cohort_applications_plus.user_id = playlist_progress.user_id
                where
                    cohort_applications_plus.was_accepted=true
                group by
                    cohort_applications_plus.cohort_id
                    , cohort_applications_plus.user_id
            )
            , with_counts as (
                select
                    current_status.cohort_name
                    , current_status.cohort_id
                    , current_status.program_type
                    , current_status.start_date
                    , current_status.user_id
                    , current_status.status

                    , stream_counts.required_streams_complete
                    , stream_counts.foundations_streams_complete
                    , stream_counts.exam_streams_complete
                    , stream_counts.elective_streams_complete

                    , playlist_counts.required_playlists_complete
                    , playlist_counts.specialization_playlists_complete

                    , lesson_counts.required_lessons_complete
                    , lesson_counts.foundations_lessons_complete
                    , lesson_counts.test_lessons_complete
                    , lesson_counts.elective_lessons_complete
                    , lesson_counts.average_assessment_score_first
                    , lesson_counts.average_assessment_score_best
                    , lesson_counts.min_assessment_score_best
                    , test_scores.avg_test_score

                from current_status
                    join lesson_counts
                        on current_status.cohort_id = lesson_counts.cohort_id
                        and current_status.user_id = lesson_counts.user_id
                    join stream_counts
                        on current_status.cohort_id = stream_counts.cohort_id
                        and current_status.user_id = stream_counts.user_id
                    join playlist_counts
                        on current_status.cohort_id = playlist_counts.cohort_id
                        and current_status.user_id = playlist_counts.user_id
                    join test_scores
                        on current_status.cohort_id = test_scores.cohort_id
                        and current_status.user_id = test_scores.user_id


            )
            , with_averages as (
                select
                    with_counts.*

                    -- no entry in the table means you did not participate at all, and get a score of 0
                    , coalesce(user_participation_scores.score, 0) as participation_score
                    , coalesce(user_project_scores.score, 0) as project_score

                    , cohort_applications_plus.final_score
                    , required_streams_complete::float / cohort_content_details.required_stream_count as required_streams_perc_complete
                    , required_lessons_complete::float / cohort_content_details.required_lesson_count as required_lessons_perc_complete
                    , case when cohort_content_details.elective_lesson_count > 0
                        then elective_lessons_complete::float / cohort_content_details.elective_lesson_count
                        else null
                        end as elective_lessons_perc_complete
                    , case when cohort_content_details.elective_stream_count > 0
                        then elective_streams_complete::float / cohort_content_details.elective_stream_count
                        else null
                        end as elective_streams_perc_complete
                from with_counts
                    join published_cohort_content_details cohort_content_details
                        on with_counts.cohort_id = cohort_content_details.cohort_id
                    left join user_participation_scores
                        on user_participation_scores.user_id = with_counts.user_id
                    left join user_project_scores
                        on user_project_scores.user_id = with_counts.user_id
                    left join cohort_applications_plus
                        on cohort_applications_plus.cohort_id = with_counts.cohort_id
                          and cohort_applications_plus.user_id = with_counts.user_id
            )

            , with_grad_requirements as (
                select
                    with_averages.*
                    , case
                        -- No projects before MBA8 (and specializations are irrelevant to mba anyway)
                        when with_averages.program_type = 'mba' and with_averages.start_date < (select start_date from published_cohorts where name='MBA8')
                            then final_score > 0.7
                                    and required_streams_perc_complete = 1

                        -- For all emba cohorts, and mba starting with MBA8, we require a
                        -- project_score of 1, meaning that all projects are complete (irrelevant
                        -- of the score we reported to the student on each project)
                        --
                        -- `num_required_specializations` is always 0 for mba cohorts, so this
                        -- piece of the requirement is only relevant for emba.
                        when with_averages.program_type in ('mba', 'emba')
                            then
                                final_score > 0.7 -- see also StreamProgress#mark_application_completed where we auto-graduate if the score is > 0.7
                                    and required_streams_perc_complete = 1
                                    and specialization_playlists_complete >= cohorts_versions.num_required_specializations
                                    and project_score = 1
                        when with_averages.program_type  = 'the_business_certificate'
                            then
                                final_score > 0.7 -- see also StreamProgress#mark_application_completed where we auto-graduate if the score is > 0.7
                                    and required_streams_perc_complete = 1
                        when is_paid_cert_program_type(with_averages.program_type)
                            then
                                min_assessment_score_best > 0.8
                                and required_streams_perc_complete = 1
                                and project_score >= 3

                    end as meets_graduation_requirements
                from with_averages
                    join published_cohorts on with_averages.cohort_id = published_cohorts.id
                    join cohorts_versions on published_cohorts.version_id = cohorts_versions.version_id
            )
            select * from with_grad_requirements
            with no data
        |

        add_index :cohort_user_progress_records, [:cohort_id, :user_id], :unique => true
    end

    # update meets_graduation_requirements to handle project scores
    def version_20180807190214
        execute %Q|
            create materialized view cohort_user_progress_records as
            with current_status as (
                select
                    cohort_applications_plus.cohort_id
                    , cohort_applications_plus.user_id
                    , cohort_applications_plus.cohort_name
                    , published_cohorts.program_type
                    , published_cohorts.start_date
                    , cohort_status_changes.status
                from cohort_applications_plus
                    join published_cohorts
                        on published_cohorts.id = cohort_applications_plus.cohort_id
                    join cohort_status_changes on
                        cohort_status_changes.cohort_id = cohort_applications_plus.cohort_id
                        and cohort_status_changes.user_id = cohort_applications_plus.user_id
                        and cohort_status_changes.until_time = '01-01-2099'
                where
                    cohort_applications_plus.was_accepted=true
                    and published_cohorts.program_type != 'career_network_only'
            )

            -- For emba (or any cohort with required specializations), we only count the test
            -- scores from the n best specializations, where n is the num_required_specializations,
            -- This ensures that people are not penalized for doing extra ones that are more difficult
            --
            -- First step is to grab the average test score from each specialization.  It is currently
            -- the case that each specialization only has one test stream with one test lesson, but this
            -- query does not assume that to be the case.  If there is more than one, it averages the
            -- scores.  (See note below near avg_test_score about how we average lesson scores to get the
            -- total score)
            --
            -- See e61f9411-21ba-44cd-a4e4-eb44ea30499e for an example of a user who did extra specializations
            , specialization_test_scores as (
                SELECT
                    current_status.user_id
                    , current_status.cohort_id
                    , playlists.playlist_title
                    , lessons.stream_title
                    , lessons.stream_locale_pack_id
                    , cohorts_versions.num_required_specializations
                    , array_agg(lessons.lesson_title) lessons
                    , array_agg(lessons.lesson_locale_pack_id) lesson_locale_pack_ids
                    , avg(lesson_progress.official_test_score) avg_test_score
                from current_status
                    join published_cohorts
                        on published_cohorts.id = current_status.cohort_id
                    join cohorts_versions
                        on published_cohorts.version_id = cohorts_versions.version_id
                        and cohorts_versions.num_required_specializations > 0
                    join published_cohort_playlist_locale_packs playlists
                        on playlists.cohort_id = current_status.cohort_id
                        and playlists.specialization = true
                    join published_playlist_streams
                        on published_playlist_streams.playlist_locale_pack_id = playlists.playlist_locale_pack_id
                    join published_stream_lesson_locale_packs lessons
                        on lessons.stream_locale_pack_id = published_playlist_streams.stream_locale_pack_id
                        and lessons.test = true
                    join lesson_streams_progress
                        on lesson_streams_progress.locale_pack_id = lessons.stream_locale_pack_id
                        and lesson_streams_progress.user_id = current_status.user_id
                        and lesson_streams_progress.completed_at is not null
                    join user_lesson_progress_records lesson_progress
                        on lesson_progress.locale_pack_id = lessons.lesson_locale_pack_id
                        and lesson_progress.user_id = current_status.user_id
                group by

                    current_status.user_id
                    , current_status.cohort_id
                    , playlists.playlist_title
                    , lessons.stream_title
                    , lessons.stream_locale_pack_id
                    , cohorts_versions.num_required_specializations
            )

            -- Assign each specialization a rank, ordering them by best avg_test_score
            -- for each user.  So the best score for a user will have a 1, then next
            -- will have a 2, etc.
            , ranked_specialization_test_scores as (
                SELECT
                    specialization_test_scores.*
                    , rank() OVER (
                        PARTITION BY cohort_id, user_id
                        ORDER BY avg_test_score DESC
                    )
                FROM
                    specialization_test_scores
                order by
                    specialization_test_scores.cohort_id
                    , specialization_test_scores.user_id
                    , rank
            )

            -- We only count lessons from the top n specializations, where n
            -- is the num_required_specializations for the cohort.  The unnesting
            -- here handles the case where one of the specialization has multiple
            -- lessons (though this currently is never the case, see note above).
            -- Since there could be multiple test lessons in a specialization, we
            -- might count more than num_required_specializations lessons.
            , test_scores_to_count as (
                -- specialization tests
                SELECT
                    cohort_id
                    , user_id
                    , avg_test_score as official_test_score
                    , unnest(lesson_locale_pack_ids) lesson_locale_pack_id
                FROM
                    ranked_specialization_test_scores
                WHERE
                    rank <= num_required_specializations

                UNION

                -- required tests
                SELECT
                    lesson_progress.cohort_id
                    , lesson_progress.user_id
                    , lesson_progress.official_test_score
                    , lesson_progress.lesson_locale_pack_id
                from cohort_applications_plus
                    left join cohort_user_lesson_progress_records lesson_progress on
                        cohort_applications_plus.cohort_id = lesson_progress.cohort_id
                        and cohort_applications_plus.user_id = lesson_progress.user_id
                where
                    lesson_progress.test
                    and lesson_progress.required

            )

            -- We count all test scores from required lessons as well as test scores
            -- from specializations (see notes above about which ones we count).  Note that
            -- we are averaging all the test lessons.  This means that an exam with more lessons
            -- gets weighted more than an exam with fewer.  This is helpful in MBA where the final
            -- has more lessons and should have more weight.  It is also fine in EMBA, where specializations
            -- each have one lesson each.
            , test_scores as (
                SELECT
                    cohort_id
                    , user_id
                    , avg(official_test_score) as avg_test_score
                FROM
                    test_scores_to_count
                group by
                    cohort_id
                    , user_id
            )

            , lesson_counts as (
                select
                    lesson_progress.cohort_id
                    , lesson_progress.user_id
                    , sum(case
                            when foundations = true and completed_at is not null then 1
                            else 0
                            end
                        ) as foundations_lessons_complete
                    , sum(case
                            when required = true and completed_at is not null then 1
                            else 0
                            end
                        ) as required_lessons_complete
                    , sum(case
                            when test = true and completed_at is not null then 1
                            else 0
                            end
                        ) as test_lessons_complete
                    , sum(case
                            when elective = true and completed_at is not null then 1
                            else 0
                            end
                        ) as elective_lessons_complete
                    , avg(case when assessment then average_assessment_score_first else null end) as average_assessment_score_first
                    , avg(case when assessment then average_assessment_score_best else null end) as average_assessment_score_best
                    , min(case when assessment then average_assessment_score_best else null end) as min_assessment_score_best
                from cohort_applications_plus
                    left join cohort_user_lesson_progress_records lesson_progress on
                        cohort_applications_plus.cohort_id = lesson_progress.cohort_id
                        and cohort_applications_plus.user_id = lesson_progress.user_id
                where
                    cohort_applications_plus.was_accepted=true
                group by
                    lesson_progress.cohort_id
                    , lesson_progress.user_id
            )
            , stream_counts as (
                select
                    cohort_applications_plus.cohort_id
                    , cohort_applications_plus.user_id
                    , sum(case
                            when foundations = true and completed_at is not null then 1
                            else 0
                            end
                        ) as foundations_streams_complete
                    , sum(case
                            when required = true and completed_at is not null then 1
                            else 0
                            end
                        ) as required_streams_complete
                    , sum(case
                            when exam = true and completed_at is not null then 1
                            else 0
                            end
                        ) as exam_streams_complete
                    , sum(case
                            when elective = true and completed_at is not null then 1
                            else 0
                            end
                        ) as elective_streams_complete
                from cohort_applications_plus
                    join published_cohort_stream_locale_packs
                        on published_cohort_stream_locale_packs.cohort_id = cohort_applications_plus.cohort_id
                    left join lesson_streams_progress
                        on published_cohort_stream_locale_packs.stream_locale_pack_id = lesson_streams_progress.locale_pack_id
                        and cohort_applications_plus.user_id = lesson_streams_progress.user_id
                where
                    cohort_applications_plus.was_accepted=true
                group by
                    cohort_applications_plus.cohort_id
                    , cohort_applications_plus.user_id
            )
            , playlist_counts as (
                select
                    cohort_applications_plus.cohort_id
                    , cohort_applications_plus.user_id
                    , sum(case
                            when required = true and completed = true then 1
                            else 0
                            end
                        ) as required_playlists_complete
                    , sum(case
                            when specialization = true and completed = true then 1
                            else 0
                            end
                        ) as specialization_playlists_complete
                from cohort_applications_plus
                    join published_cohort_playlist_locale_packs
                        on published_cohort_playlist_locale_packs.cohort_id = cohort_applications_plus.cohort_id
                    left join playlist_progress
                        on published_cohort_playlist_locale_packs.playlist_locale_pack_id = playlist_progress.locale_pack_id
                        and cohort_applications_plus.user_id = playlist_progress.user_id
                where
                    cohort_applications_plus.was_accepted=true
                group by
                    cohort_applications_plus.cohort_id
                    , cohort_applications_plus.user_id
            )
            , with_counts as (
                select
                    current_status.cohort_name
                    , current_status.cohort_id
                    , current_status.program_type
                    , current_status.start_date
                    , current_status.user_id
                    , current_status.status

                    , stream_counts.required_streams_complete
                    , stream_counts.foundations_streams_complete
                    , stream_counts.exam_streams_complete
                    , stream_counts.elective_streams_complete

                    , playlist_counts.required_playlists_complete
                    , playlist_counts.specialization_playlists_complete

                    , lesson_counts.required_lessons_complete
                    , lesson_counts.foundations_lessons_complete
                    , lesson_counts.test_lessons_complete
                    , lesson_counts.elective_lessons_complete
                    , lesson_counts.average_assessment_score_first
                    , lesson_counts.average_assessment_score_best
                    , lesson_counts.min_assessment_score_best
                    , test_scores.avg_test_score

                from current_status
                    join lesson_counts
                        on current_status.cohort_id = lesson_counts.cohort_id
                        and current_status.user_id = lesson_counts.user_id
                    join stream_counts
                        on current_status.cohort_id = stream_counts.cohort_id
                        and current_status.user_id = stream_counts.user_id
                    join playlist_counts
                        on current_status.cohort_id = playlist_counts.cohort_id
                        and current_status.user_id = playlist_counts.user_id
                    join test_scores
                        on current_status.cohort_id = test_scores.cohort_id
                        and current_status.user_id = test_scores.user_id


            )
            , with_averages as (
                select
                    with_counts.*

                    -- no entry in the table means you did not participate at all, and get a score of 0
                    , coalesce(user_participation_scores.score, 0) as participation_score
                    , coalesce(user_project_scores.score, 0) as project_score

                    , coalesce(cohort_applications_plus.final_score,
                          case
                            when with_counts.cohort_name='MBA1'
                                then (
                                    0.7*(case when avg_test_score is null then 0 else avg_test_score end)
                                    + 0.3*(case when average_assessment_score_best is null then 0 else average_assessment_score_best end)
                                )
                            when with_counts.program_type = 'mba' and with_counts.start_date < (select start_date from published_cohorts where name='MBA8')
                                then (
                                    0.7*(case when avg_test_score is null then 0 else avg_test_score end)
                                    + 0.2*(case when average_assessment_score_best is null then 0 else average_assessment_score_best end)
                                    + 0.1*coalesce(user_participation_scores.score, 0)
                                )
                            when with_counts.program_type = 'mba'
                                then (
                                    0.7*(case when avg_test_score is null then 0 else avg_test_score end)
                                    + 0.2*(case when average_assessment_score_best is null then 0 else average_assessment_score_best end)
                                    + 0.1*coalesce(user_project_scores.score, 0)
                                )
                            when with_counts.program_type = 'emba' and with_counts.start_date < (select start_date from published_cohorts where name='EMBA4')
                                then (
                                    0.6*(case when avg_test_score is null then 0 else avg_test_score end)
                                    + 0.2*(case when average_assessment_score_best is null then 0 else average_assessment_score_best end)
                                    + 0.1*coalesce(user_project_scores.score, 0)
                                    + 0.1*coalesce(user_participation_scores.score, 0)
                                )
                            when with_counts.program_type = 'emba'
                                then (
                                    0.6*(case when avg_test_score is null then 0 else avg_test_score end)
                                    + 0.2*(case when average_assessment_score_best is null then 0 else average_assessment_score_best end)
                                    + 0.2*coalesce(user_project_scores.score, 0)
                                )
                            when is_paid_cert_program_type(with_counts.program_type)
                                -- there are min requirements defined in meets_graduation_requirements below,
                                -- but there is no definition of a score for these
                                then null
                            end
                      ) as final_score
                    , required_streams_complete::float / cohort_content_details.required_stream_count as required_streams_perc_complete
                    , required_lessons_complete::float / cohort_content_details.required_lesson_count as required_lessons_perc_complete
                    , case when cohort_content_details.elective_lesson_count > 0
                        then elective_lessons_complete::float / cohort_content_details.elective_lesson_count
                        else null
                        end as elective_lessons_perc_complete
                    , case when cohort_content_details.elective_stream_count > 0
                        then elective_streams_complete::float / cohort_content_details.elective_stream_count
                        else null
                        end as elective_streams_perc_complete
                from with_counts
                    join published_cohort_content_details cohort_content_details
                        on with_counts.cohort_id = cohort_content_details.cohort_id
                    left join user_participation_scores
                        on user_participation_scores.user_id = with_counts.user_id
                    left join user_project_scores
                        on user_project_scores.user_id = with_counts.user_id
                    left join cohort_applications_plus
                        on cohort_applications_plus.cohort_id = with_counts.cohort_id
                          and cohort_applications_plus.user_id = with_counts.user_id
            )

            , with_grad_requirements as (
                select
                    with_averages.*
                    , case
                        -- No projects before MBA8 (and specializations are irrelevant to mba anyway)
                        when with_averages.program_type = 'mba' and with_averages.start_date < (select start_date from published_cohorts where name='MBA8')
                            then final_score > 0.7
                                    and required_streams_perc_complete = 1

                        -- For all emba cohorts, and mba starting with MBA8, we require a
                        -- project_score of 1, meaning that all projects are complete (irrelevant
                        -- of the score we reported to the student on each project)
                        --
                        -- `num_required_specializations` is always 0 for mba cohorts, so this
                        -- piece of the requirement is only relevant for emba.
                        when with_averages.program_type in ('mba', 'emba')
                            then
                                final_score > 0.7 -- see also StreamProgress#mark_application_completed where we auto-graduate if the score is > 0.7
                                    and required_streams_perc_complete = 1
                                    and specialization_playlists_complete >= cohorts_versions.num_required_specializations
                                    and project_score = 1
                        when with_averages.program_type  = 'the_business_certificate'
                            then
                                final_score > 0.7 -- see also StreamProgress#mark_application_completed where we auto-graduate if the score is > 0.7
                                    and required_streams_perc_complete = 1
                        when is_paid_cert_program_type(with_averages.program_type)
                            then
                                min_assessment_score_best > 0.8
                                and required_streams_perc_complete = 1
                                and project_score >= 3

                    end as meets_graduation_requirements
                from with_averages
                    join published_cohorts on with_averages.cohort_id = published_cohorts.id
                    join cohorts_versions on published_cohorts.version_id = cohorts_versions.version_id
            )
            select * from with_grad_requirements
            with no data
        |

        add_index :cohort_user_progress_records, [:cohort_id, :user_id], :unique => true
    end

    # fix to lesson counts and
    # change project scores to a 0-1 scale
    def version_20180313150205
        execute %Q|
            create materialized view cohort_user_progress_records as
            with current_status as (
                select
                    cohort_applications_plus.cohort_id
                    , cohort_applications_plus.user_id
                    , cohort_applications_plus.cohort_name
                    , published_cohorts.program_type
                    , published_cohorts.start_date
                    , cohort_status_changes.status
                from cohort_applications_plus
                    join published_cohorts
                        on published_cohorts.id = cohort_applications_plus.cohort_id
                    join cohort_status_changes on
                        cohort_status_changes.cohort_id = cohort_applications_plus.cohort_id
                        and cohort_status_changes.user_id = cohort_applications_plus.user_id
                        and cohort_status_changes.until_time = '01-01-2099'
                where
                    cohort_applications_plus.was_accepted=true
                    and published_cohorts.program_type != 'career_network_only'
            )

            -- For emba (or any cohort with required specializations), we only count the test
            -- scores from the n best specializations, where n is the num_required_specializations,
            -- This ensures that people are not penalized for doing extra ones that are more difficult
            --
            -- First step is to grab the average test score from each specialization.  It is currently
            -- the case that each specialization only has one test stream with one test lesson, but this
            -- query does not assume that to be the case.  If there is more than one, it averages the
            -- scores.  (See note below near avg_test_score about how we average lesson scores to get the
            -- total score)
            --
            -- See e61f9411-21ba-44cd-a4e4-eb44ea30499e for an example of a user who did extra specializations
            , specialization_test_scores as (
                SELECT
                    current_status.user_id
                    , current_status.cohort_id
                    , playlists.playlist_title
                    , lessons.stream_title
                    , lessons.stream_locale_pack_id
                    , cohorts_versions.num_required_specializations
                    , array_agg(lessons.lesson_title) lessons
                    , array_agg(lessons.lesson_locale_pack_id) lesson_locale_pack_ids
                    , avg(lesson_progress.official_test_score) avg_test_score
                from current_status
                    join published_cohorts
                        on published_cohorts.id = current_status.cohort_id
                    join cohorts_versions
                        on published_cohorts.version_id = cohorts_versions.version_id
                        and cohorts_versions.num_required_specializations > 0
                    join published_cohort_playlist_locale_packs playlists
                        on playlists.cohort_id = current_status.cohort_id
                        and playlists.specialization = true
                    join published_playlist_streams
                        on published_playlist_streams.playlist_locale_pack_id = playlists.playlist_locale_pack_id
                    join published_stream_lesson_locale_packs lessons
                        on lessons.stream_locale_pack_id = published_playlist_streams.stream_locale_pack_id
                        and lessons.test = true
                    join lesson_streams_progress
                        on lesson_streams_progress.locale_pack_id = lessons.stream_locale_pack_id
                        and lesson_streams_progress.user_id = current_status.user_id
                        and lesson_streams_progress.completed_at is not null
                    join user_lesson_progress_records lesson_progress
                        on lesson_progress.locale_pack_id = lessons.lesson_locale_pack_id
                        and lesson_progress.user_id = current_status.user_id
                group by

                    current_status.user_id
                    , current_status.cohort_id
                    , playlists.playlist_title
                    , lessons.stream_title
                    , lessons.stream_locale_pack_id
                    , cohorts_versions.num_required_specializations
            )

            -- Assign each specialization a rank, ordering them by best avg_test_score
            -- for each user.  So the best score for a user will have a 1, then next
            -- will have a 2, etc.
            , ranked_specialization_test_scores as (
                SELECT
                    specialization_test_scores.*
                    , rank() OVER (
                        PARTITION BY cohort_id, user_id
                        ORDER BY avg_test_score DESC
                    )
                FROM
                    specialization_test_scores
                order by
                    specialization_test_scores.cohort_id
                    , specialization_test_scores.user_id
                    , rank
            )

            -- We only count lessons from the top n specializations, where n
            -- is the num_required_specializations for the cohort.  The unnesting
            -- here handles the case where one of the specialization has multiple
            -- lessons (though this currently is never the case, see note above).
            -- Since there could be multiple test lessons in a specialization, we
            -- might count more than num_required_specializations lessons.
            , test_scores_to_count as (
                -- specialization tests
                SELECT
                    cohort_id
                    , user_id
                    , avg_test_score as official_test_score
                    , unnest(lesson_locale_pack_ids) lesson_locale_pack_id
                FROM
                    ranked_specialization_test_scores
                WHERE
                    rank <= num_required_specializations

                UNION

                -- required tests
                SELECT
                    lesson_progress.cohort_id
                    , lesson_progress.user_id
                    , lesson_progress.official_test_score
                    , lesson_progress.lesson_locale_pack_id
                from cohort_applications_plus
                    left join cohort_user_lesson_progress_records lesson_progress on
                        cohort_applications_plus.cohort_id = lesson_progress.cohort_id
                        and cohort_applications_plus.user_id = lesson_progress.user_id
                where
                    lesson_progress.test
                    and lesson_progress.required

            )

            -- We count all test scores from required lessons as well as test scores
            -- from specializations (see notes above about which ones we count).  Note that
            -- we are averaging all the test lessons.  This means that an exam with more lessons
            -- gets weighted more than an exam with fewer.  This is helpful in MBA where the final
            -- has more lessons and should have more weight.  It is also fine in EMBA, where specializations
            -- each have one lesson each.
            , test_scores as (
                SELECT
                    cohort_id
                    , user_id
                    , avg(official_test_score) as avg_test_score
                FROM
                    test_scores_to_count
                group by
                    cohort_id
                    , user_id
            )

            , lesson_counts as (
                select
                    lesson_progress.cohort_id
                    , lesson_progress.user_id
                    , sum(case
                            when foundations = true and completed_at is not null then 1
                            else 0
                            end
                        ) as foundations_lessons_complete
                    , sum(case
                            when required = true and completed_at is not null then 1
                            else 0
                            end
                        ) as required_lessons_complete
                    , sum(case
                            when test = true and completed_at is not null then 1
                            else 0
                            end
                        ) as test_lessons_complete
                    , sum(case
                            when elective = true and completed_at is not null then 1
                            else 0
                            end
                        ) as elective_lessons_complete
                    , avg(case when assessment then average_assessment_score_first else null end) as average_assessment_score_first
                    , avg(case when assessment then average_assessment_score_best else null end) as average_assessment_score_best
                    , min(case when assessment then average_assessment_score_best else null end) as min_assessment_score_best
                from cohort_applications_plus
                    left join cohort_user_lesson_progress_records lesson_progress on
                        cohort_applications_plus.cohort_id = lesson_progress.cohort_id
                        and cohort_applications_plus.user_id = lesson_progress.user_id
                where
                    cohort_applications_plus.was_accepted=true
                group by
                    lesson_progress.cohort_id
                    , lesson_progress.user_id
            )
            , stream_counts as (
                select
                    cohort_applications_plus.cohort_id
                    , cohort_applications_plus.user_id
                    , sum(case
                            when foundations = true and completed_at is not null then 1
                            else 0
                            end
                        ) as foundations_streams_complete
                    , sum(case
                            when required = true and completed_at is not null then 1
                            else 0
                            end
                        ) as required_streams_complete
                    , sum(case
                            when exam = true and completed_at is not null then 1
                            else 0
                            end
                        ) as exam_streams_complete
                    , sum(case
                            when elective = true and completed_at is not null then 1
                            else 0
                            end
                        ) as elective_streams_complete
                from cohort_applications_plus
                    join published_cohort_stream_locale_packs
                        on published_cohort_stream_locale_packs.cohort_id = cohort_applications_plus.cohort_id
                    left join lesson_streams_progress
                        on published_cohort_stream_locale_packs.stream_locale_pack_id = lesson_streams_progress.locale_pack_id
                        and cohort_applications_plus.user_id = lesson_streams_progress.user_id
                where
                    cohort_applications_plus.was_accepted=true
                group by
                    cohort_applications_plus.cohort_id
                    , cohort_applications_plus.user_id
            )
            , playlist_counts as (
                select
                    cohort_applications_plus.cohort_id
                    , cohort_applications_plus.user_id
                    , sum(case
                            when required = true and completed = true then 1
                            else 0
                            end
                        ) as required_playlists_complete
                    , sum(case
                            when specialization = true and completed = true then 1
                            else 0
                            end
                        ) as specialization_playlists_complete
                from cohort_applications_plus
                    join published_cohort_playlist_locale_packs
                        on published_cohort_playlist_locale_packs.cohort_id = cohort_applications_plus.cohort_id
                    left join playlist_progress
                        on published_cohort_playlist_locale_packs.playlist_locale_pack_id = playlist_progress.locale_pack_id
                        and cohort_applications_plus.user_id = playlist_progress.user_id
                where
                    cohort_applications_plus.was_accepted=true
                group by
                    cohort_applications_plus.cohort_id
                    , cohort_applications_plus.user_id
            )
            , with_counts as (
                select
                    current_status.cohort_name
                    , current_status.cohort_id
                    , current_status.program_type
                    , current_status.start_date
                    , current_status.user_id
                    , current_status.status

                    , stream_counts.required_streams_complete
                    , stream_counts.foundations_streams_complete
                    , stream_counts.exam_streams_complete
                    , stream_counts.elective_streams_complete

                    , playlist_counts.required_playlists_complete
                    , playlist_counts.specialization_playlists_complete

                    , lesson_counts.required_lessons_complete
                    , lesson_counts.foundations_lessons_complete
                    , lesson_counts.test_lessons_complete
                    , lesson_counts.elective_lessons_complete
                    , lesson_counts.average_assessment_score_first
                    , lesson_counts.average_assessment_score_best
                    , lesson_counts.min_assessment_score_best
                    , test_scores.avg_test_score

                from current_status
                    join lesson_counts
                        on current_status.cohort_id = lesson_counts.cohort_id
                        and current_status.user_id = lesson_counts.user_id
                    join stream_counts
                        on current_status.cohort_id = stream_counts.cohort_id
                        and current_status.user_id = stream_counts.user_id
                    join playlist_counts
                        on current_status.cohort_id = playlist_counts.cohort_id
                        and current_status.user_id = playlist_counts.user_id
                    join test_scores
                        on current_status.cohort_id = test_scores.cohort_id
                        and current_status.user_id = test_scores.user_id


            )
            , with_averages as (
                select
                    with_counts.*

                    -- no entry in the table means you did not participate at all, and get a score of 0
                    , coalesce(user_participation_scores.score, 0) as participation_score
                    , coalesce(user_project_scores.score, 0) as project_score

                    , coalesce(cohort_applications_plus.final_score,
                          case
                            when with_counts.cohort_name='MBA1'
                                then (
                                    0.7*(case when avg_test_score is null then 0 else avg_test_score end)
                                    + 0.3*(case when average_assessment_score_best is null then 0 else average_assessment_score_best end)
                                )
                            when with_counts.program_type = 'mba' and with_counts.start_date < (select start_date from published_cohorts where name='MBA8')
                                then (
                                    0.7*(case when avg_test_score is null then 0 else avg_test_score end)
                                    + 0.2*(case when average_assessment_score_best is null then 0 else average_assessment_score_best end)
                                    + 0.1*coalesce(user_participation_scores.score, 0)
                                )
                            when with_counts.program_type = 'mba'
                                then (
                                    0.7*(case when avg_test_score is null then 0 else avg_test_score end)
                                    + 0.2*(case when average_assessment_score_best is null then 0 else average_assessment_score_best end)
                                    + 0.1*coalesce(user_project_scores.score, 0)
                                )
                            when with_counts.program_type = 'emba' and with_counts.start_date < (select start_date from published_cohorts where name='EMBA4')
                                then (
                                    0.6*(case when avg_test_score is null then 0 else avg_test_score end)
                                    + 0.2*(case when average_assessment_score_best is null then 0 else average_assessment_score_best end)
                                    + 0.1*coalesce(user_project_scores.score, 0)
                                    + 0.1*coalesce(user_participation_scores.score, 0)
                                )
                            when with_counts.program_type = 'emba'
                                then (
                                    0.6*(case when avg_test_score is null then 0 else avg_test_score end)
                                    + 0.2*(case when average_assessment_score_best is null then 0 else average_assessment_score_best end)
                                    + 0.2*coalesce(user_project_scores.score, 0)
                                )
                            when is_paid_cert_program_type(with_counts.program_type)
                                -- there are min requirements defined in meets_graduation_requirements below,
                                -- but there is no definition of a score for these
                                then null
                            end
                      ) as final_score
                    , required_streams_complete::float / cohort_content_details.required_stream_count as required_streams_perc_complete
                    , required_lessons_complete::float / cohort_content_details.required_lesson_count as required_lessons_perc_complete
                    , case when cohort_content_details.elective_lesson_count > 0
                        then elective_lessons_complete::float / cohort_content_details.elective_lesson_count
                        else null
                        end as elective_lessons_perc_complete
                    , case when cohort_content_details.elective_stream_count > 0
                        then elective_streams_complete::float / cohort_content_details.elective_stream_count
                        else null
                        end as elective_streams_perc_complete
                from with_counts
                    join published_cohort_content_details cohort_content_details
                        on with_counts.cohort_id = cohort_content_details.cohort_id
                    left join user_participation_scores
                        on user_participation_scores.user_id = with_counts.user_id
                    left join user_project_scores
                        on user_project_scores.user_id = with_counts.user_id
                    left join cohort_applications_plus
                        on cohort_applications_plus.cohort_id = with_counts.cohort_id
                          and cohort_applications_plus.user_id = with_counts.user_id
            )

            , with_grad_requirements as (
                select
                    with_averages.*
                    , case
                        when with_averages.program_type in ('mba', 'emba', 'the_business_certificate')
                            then
                                final_score > 0.7 -- see also StreamProgress#mark_application_completed where we auto-graduate if the score is > 0.7
                                    and required_streams_perc_complete = 1
                                    and specialization_playlists_complete >= cohorts_versions.num_required_specializations
                        when is_paid_cert_program_type(with_averages.program_type)
                            then
                                min_assessment_score_best > 0.8
                                and required_streams_perc_complete = 1
                                and project_score >= 3

                    end as meets_graduation_requirements
                from with_averages
                    join published_cohorts on with_averages.cohort_id = published_cohorts.id
                    join cohorts_versions on published_cohorts.version_id = cohorts_versions.version_id
            )
            select * from with_grad_requirements
            with no data
        |

        add_index :cohort_user_progress_records, [:cohort_id, :user_id], :unique => true
    end

    # add new final score calculations for mba, emba, and certs
    def version_20180301211924
        execute %Q|
            create materialized view cohort_user_progress_records as
            with current_status as (
                select
                    cohort_applications_plus.cohort_id
                    , cohort_applications_plus.user_id
                    , cohort_applications_plus.cohort_name
                    , published_cohorts.program_type
                    , published_cohorts.start_date
                    , cohort_status_changes.status
                from cohort_applications_plus
                    join published_cohorts
                        on published_cohorts.id = cohort_applications_plus.cohort_id
                    join cohort_status_changes on
                        cohort_status_changes.cohort_id = cohort_applications_plus.cohort_id
                        and cohort_status_changes.user_id = cohort_applications_plus.user_id
                        and cohort_status_changes.until_time = '01-01-2099'
                where
                    cohort_applications_plus.was_accepted=true
                    and published_cohorts.program_type != 'career_network_only'
            )

            -- For emba (or any cohort with required specializations), we only count the test
            -- scores from the n best specializations, where n is the num_required_specializations,
            -- This ensures that people are not penalized for doing extra ones that are more difficult
            --
            -- First step is to grab the average test score from each specialization.  It is currently
            -- the case that each specialization only has one test stream with one test lesson, but this
            -- query does not assume that to be the case.  If there is more than one, it averages the
            -- scores.  (See note below near avg_test_score about how we average lesson scores to get the
            -- total score)
            --
            -- See e61f9411-21ba-44cd-a4e4-eb44ea30499e for an example of a user who did extra specializations
            , specialization_test_scores as (
                SELECT
                    current_status.user_id
                    , current_status.cohort_id
                    , playlists.playlist_title
                    , lessons.stream_title
                    , lessons.stream_locale_pack_id
                    , cohorts_versions.num_required_specializations
                    , array_agg(lessons.lesson_title) lessons
                    , array_agg(lessons.lesson_locale_pack_id) lesson_locale_pack_ids
                    , avg(lesson_progress.official_test_score) avg_test_score
                from current_status
                    join published_cohorts
                        on published_cohorts.id = current_status.cohort_id
                    join cohorts_versions
                        on published_cohorts.version_id = cohorts_versions.version_id
                        and cohorts_versions.num_required_specializations > 0
                    join published_cohort_playlist_locale_packs playlists
                        on playlists.cohort_id = current_status.cohort_id
                        and playlists.specialization = true
                    join published_playlist_streams
                        on published_playlist_streams.playlist_locale_pack_id = playlists.playlist_locale_pack_id
                    join published_stream_lesson_locale_packs lessons
                        on lessons.stream_locale_pack_id = published_playlist_streams.stream_locale_pack_id
                        and lessons.test = true
                    join lesson_streams_progress
                        on lesson_streams_progress.locale_pack_id = lessons.stream_locale_pack_id
                        and lesson_streams_progress.user_id = current_status.user_id
                        and lesson_streams_progress.completed_at is not null
                    join user_lesson_progress_records lesson_progress
                        on lesson_progress.locale_pack_id = lessons.lesson_locale_pack_id
                        and lesson_progress.user_id = current_status.user_id
                group by

                    current_status.user_id
                    , current_status.cohort_id
                    , playlists.playlist_title
                    , lessons.stream_title
                    , lessons.stream_locale_pack_id
                    , cohorts_versions.num_required_specializations
            )

            -- Assign each specialization a rank, ordering them by best avg_test_score
            -- for each user.  So the best score for a user will have a 1, then next
            -- will have a 2, etc.
            , ranked_specialization_test_scores as (
                SELECT
                    specialization_test_scores.*
                    , rank() OVER (
                        PARTITION BY cohort_id, user_id
                        ORDER BY avg_test_score DESC
                    )
                FROM
                    specialization_test_scores
                order by
                    specialization_test_scores.cohort_id
                    , specialization_test_scores.user_id
                    , rank
            )

            -- We only count lessons from the top n specializations, where n
            -- is the num_required_specializations for the cohort.  The unnesting
            -- here handles the case where one of the specialization has multiple
            -- lessons (though this currently is never the case, see note above).
            -- Since there could be multiple test lessons in a specialization, we
            -- might count more than num_required_specializations lessons.
            , specialization_test_scores_to_count as (
                SELECT
                    cohort_id
                    , user_id
                    , avg_test_score
                    , rank <= num_required_specializations as should_count
                    , unnest(lesson_locale_pack_ids) lesson_locale_pack_id
                FROM
                    ranked_specialization_test_scores
            )

            , lesson_counts as (
                select
                    lesson_progress.cohort_id
                    , lesson_progress.user_id
                    , sum(case
                            when foundations = true and completed_at is not null then 1
                            else 0
                            end
                        ) as foundations_lessons_complete
                    , sum(case
                            when required = true and completed_at is not null then 1
                            else 0
                            end
                        ) as required_lessons_complete
                    , sum(case
                            when test = true and completed_at is not null then 1
                            else 0
                            end
                        ) as test_lessons_complete
                    , sum(case
                            when elective = true and completed_at is not null then 1
                            else 0
                            end
                        ) as elective_lessons_complete
                    , avg(case when assessment then average_assessment_score_first else null end) as average_assessment_score_first
                    , avg(case when assessment then average_assessment_score_best else null end) as average_assessment_score_best
                    , min(case when assessment then average_assessment_score_best else null end) as min_assessment_score_best

                    -- We count all test scores from required lessons as well as test scores
                    -- from specializations (see notes above about which ones we count).  Note that
                    -- we are averaging all the test lessons.  This means that an exam with more lessons
                    -- gets weighted more than an exam with fewer.  This is helpful in MBA where the final
                    -- has more lessons and should have more weight.  It is also fine in EMBA, where specializations
                    -- each have one lesson each.
                    , avg(
                        case
                            when lesson_progress.test and lesson_progress.required then lesson_progress.official_test_score
                            when
                                specialization_test_scores_to_count.lesson_locale_pack_id is not null
                                and specialization_test_scores_to_count.should_count = TRUE
                                then lesson_progress.official_test_score
                            ELSE
                                NULL
                            end

                    ) as avg_test_score
                from cohort_applications_plus
                    left join cohort_user_lesson_progress_records lesson_progress on
                        cohort_applications_plus.cohort_id = lesson_progress.cohort_id
                        and cohort_applications_plus.user_id = lesson_progress.user_id
                    left join specialization_test_scores_to_count on
                        cohort_applications_plus.cohort_id = specialization_test_scores_to_count.cohort_id
                        and cohort_applications_plus.user_id = specialization_test_scores_to_count.user_id
                where
                    cohort_applications_plus.was_accepted=true
                group by
                    lesson_progress.cohort_id
                    , lesson_progress.user_id
            )
            , stream_counts as (
                select
                    cohort_applications_plus.cohort_id
                    , cohort_applications_plus.user_id
                    , sum(case
                            when foundations = true and completed_at is not null then 1
                            else 0
                            end
                        ) as foundations_streams_complete
                    , sum(case
                            when required = true and completed_at is not null then 1
                            else 0
                            end
                        ) as required_streams_complete
                    , sum(case
                            when exam = true and completed_at is not null then 1
                            else 0
                            end
                        ) as exam_streams_complete
                    , sum(case
                            when elective = true and completed_at is not null then 1
                            else 0
                            end
                        ) as elective_streams_complete
                from cohort_applications_plus
                    join published_cohort_stream_locale_packs
                        on published_cohort_stream_locale_packs.cohort_id = cohort_applications_plus.cohort_id
                    left join lesson_streams_progress
                        on published_cohort_stream_locale_packs.stream_locale_pack_id = lesson_streams_progress.locale_pack_id
                        and cohort_applications_plus.user_id = lesson_streams_progress.user_id
                where
                    cohort_applications_plus.was_accepted=true
                group by
                    cohort_applications_plus.cohort_id
                    , cohort_applications_plus.user_id
            )
            , playlist_counts as (
                select
                    cohort_applications_plus.cohort_id
                    , cohort_applications_plus.user_id
                    , sum(case
                            when required = true and completed = true then 1
                            else 0
                            end
                        ) as required_playlists_complete
                    , sum(case
                            when specialization = true and completed = true then 1
                            else 0
                            end
                        ) as specialization_playlists_complete
                from cohort_applications_plus
                    join published_cohort_playlist_locale_packs
                        on published_cohort_playlist_locale_packs.cohort_id = cohort_applications_plus.cohort_id
                    left join playlist_progress
                        on published_cohort_playlist_locale_packs.playlist_locale_pack_id = playlist_progress.locale_pack_id
                        and cohort_applications_plus.user_id = playlist_progress.user_id
                where
                    cohort_applications_plus.was_accepted=true
                group by
                    cohort_applications_plus.cohort_id
                    , cohort_applications_plus.user_id
            )
            , with_counts as (
                select
                    current_status.cohort_name
                    , current_status.cohort_id
                    , current_status.program_type
                    , current_status.start_date
                    , current_status.user_id
                    , current_status.status

                    , stream_counts.required_streams_complete
                    , stream_counts.foundations_streams_complete
                    , stream_counts.exam_streams_complete
                    , stream_counts.elective_streams_complete

                    , playlist_counts.required_playlists_complete
                    , playlist_counts.specialization_playlists_complete

                    , lesson_counts.required_lessons_complete
                    , lesson_counts.foundations_lessons_complete
                    , lesson_counts.test_lessons_complete
                    , lesson_counts.elective_lessons_complete
                    , lesson_counts.average_assessment_score_first
                    , lesson_counts.average_assessment_score_best
                    , lesson_counts.min_assessment_score_best
                    , lesson_counts.avg_test_score

                from current_status
                    join lesson_counts
                        on current_status.cohort_id = lesson_counts.cohort_id
                        and current_status.user_id = lesson_counts.user_id
                    join stream_counts
                        on current_status.cohort_id = stream_counts.cohort_id
                        and current_status.user_id = stream_counts.user_id
                    join playlist_counts
                        on current_status.cohort_id = playlist_counts.cohort_id
                        and current_status.user_id = playlist_counts.user_id

            )
            , with_averages as (
                select
                    with_counts.*

                    -- no entry in the table means you did not participate at all, and get a score of 0
                    , coalesce(user_participation_scores.score, 0) as participation_score
                    , coalesce(user_project_scores.score, 0) as project_score

                    , coalesce(cohort_applications_plus.final_score,
                          case
                            when with_counts.cohort_name='MBA1'
                                then (
                                    0.7*(case when avg_test_score is null then 0 else avg_test_score end)
                                    + 0.3*(case when average_assessment_score_best is null then 0 else average_assessment_score_best end)
                                )
                            when with_counts.program_type = 'mba' and with_counts.start_date < (select start_date from published_cohorts where name='MBA8')
                                then (
                                    0.7*(case when avg_test_score is null then 0 else avg_test_score end)
                                    + 0.2*(case when average_assessment_score_best is null then 0 else average_assessment_score_best end)
                                    + 0.1*coalesce(user_participation_scores.score, 0)
                                )
                            when with_counts.program_type = 'mba'
                                then (
                                    0.7*(case when avg_test_score is null then 0 else avg_test_score end)
                                    + 0.2*(case when average_assessment_score_best is null then 0 else average_assessment_score_best end)
                                    + 0.1*coalesce(user_project_scores.score/5.0, 0)
                                )
                            when with_counts.program_type = 'emba' and with_counts.start_date < (select start_date from published_cohorts where name='EMBA4')
                                then (
                                    0.6*(case when avg_test_score is null then 0 else avg_test_score end)
                                    + 0.2*(case when average_assessment_score_best is null then 0 else average_assessment_score_best end)
                                    + 0.1*coalesce(user_project_scores.score/5.0, 0)
                                    + 0.1*coalesce(user_participation_scores.score, 0)
                                )
                            when with_counts.program_type = 'emba'
                                then (
                                    0.6*(case when avg_test_score is null then 0 else avg_test_score end)
                                    + 0.2*(case when average_assessment_score_best is null then 0 else average_assessment_score_best end)
                                    + 0.2*coalesce(user_project_scores.score/5.0, 0)
                                )
                            when is_paid_cert_program_type(with_counts.program_type)
                                -- there are min requirements defined in meets_graduation_requirements below,
                                -- but there is no definition of a score for these
                                then null
                            end
                      ) as final_score
                    , required_streams_complete::float / cohort_content_details.required_stream_count as required_streams_perc_complete
                    , required_lessons_complete::float / cohort_content_details.required_lesson_count as required_lessons_perc_complete
                    , case when cohort_content_details.elective_lesson_count > 0
                        then elective_lessons_complete::float / cohort_content_details.elective_lesson_count
                        else null
                        end as elective_lessons_perc_complete
                    , case when cohort_content_details.elective_stream_count > 0
                        then elective_streams_complete::float / cohort_content_details.elective_stream_count
                        else null
                        end as elective_streams_perc_complete
                from with_counts
                    join published_cohort_content_details cohort_content_details
                        on with_counts.cohort_id = cohort_content_details.cohort_id
                    left join user_participation_scores
                        on user_participation_scores.user_id = with_counts.user_id
                    left join user_project_scores
                        on user_project_scores.user_id = with_counts.user_id
                    left join cohort_applications_plus
                        on cohort_applications_plus.cohort_id = with_counts.cohort_id
                          and cohort_applications_plus.user_id = with_counts.user_id
            )

            , with_grad_requirements as (
                select
                    with_averages.*
                    , case
                        when with_averages.program_type in ('mba', 'emba', 'the_business_certificate')
                            then
                                final_score > 0.7 -- see also CohortApplication@handle_cohort_events where we auto-graduate if the score is > 0.7
                                    and required_streams_perc_complete = 1
                                    and specialization_playlists_complete >= cohorts_versions.num_required_specializations
                        when is_paid_cert_program_type(with_averages.program_type)
                            then
                                min_assessment_score_best > 0.8
                                and required_streams_perc_complete = 1
                                and project_score >= 3

                    end as meets_graduation_requirements
                from with_averages
                    join published_cohorts on with_averages.cohort_id = published_cohorts.id
                    join cohorts_versions on published_cohorts.version_id = cohorts_versions.version_id
            )
            select * from with_grad_requirements
            with no data
        |

        add_index :cohort_user_progress_records, [:cohort_id, :user_id], :unique => true
    end

    # fix a bug in the last version that was causing
    # meets_graduation_requirements to not be set for
    # cohorts with no required specializations
    def version_20170727183320
        execute %Q|
            create materialized view cohort_user_progress_records as
            with current_status as (
                select
                    cohort_applications_plus.cohort_id
                    , cohort_applications_plus.user_id
                    , cohort_applications_plus.cohort_name
                    , cohort_status_changes.status
                from cohort_applications_plus
                    join cohort_status_changes on
                        cohort_status_changes.cohort_id = cohort_applications_plus.cohort_id
                        and cohort_status_changes.user_id = cohort_applications_plus.user_id
                        and cohort_status_changes.until_time = '01-01-2099'
                where
                    cohort_applications_plus.was_accepted=true
            ), lesson_counts as (
                select
                    cohort_user_lesson_progress_records.cohort_id
                    , cohort_user_lesson_progress_records.user_id
                    , sum(case
                            when foundations = true and completed_at is not null then 1
                            else 0
                            end
                        ) as foundations_lessons_complete
                    , sum(case
                            when required = true and completed_at is not null then 1
                            else 0
                            end
                        ) as required_lessons_complete
                    , sum(case
                            when test = true and completed_at is not null then 1
                            else 0
                            end
                        ) as test_lessons_complete
                    , sum(case
                            when elective = true and completed_at is not null then 1
                            else 0
                            end
                        ) as elective_lessons_complete
                    , avg(case when assessment then average_assessment_score_first else null end) as average_assessment_score_first
                    , avg(case when assessment then average_assessment_score_best else null end) as average_assessment_score_best
                    , avg(official_test_score) as avg_test_score
                from cohort_applications_plus
                    left join cohort_user_lesson_progress_records on
                        cohort_applications_plus.cohort_id = cohort_user_lesson_progress_records.cohort_id
                        and cohort_applications_plus.user_id = cohort_user_lesson_progress_records.user_id
                where
                    cohort_applications_plus.was_accepted=true
                group by
                    cohort_user_lesson_progress_records.cohort_id
                    , cohort_user_lesson_progress_records.user_id
            )
            , stream_counts as (
                select
                    cohort_applications_plus.cohort_id
                    , cohort_applications_plus.user_id
                    , sum(case
                            when foundations = true and completed_at is not null then 1
                            else 0
                            end
                        ) as foundations_streams_complete
                    , sum(case
                            when required = true and completed_at is not null then 1
                            else 0
                            end
                        ) as required_streams_complete
                    , sum(case
                            when exam = true and completed_at is not null then 1
                            else 0
                            end
                        ) as exam_streams_complete
                    , sum(case
                            when elective = true and completed_at is not null then 1
                            else 0
                            end
                        ) as elective_streams_complete
                from cohort_applications_plus
                    join published_cohort_stream_locale_packs
                        on published_cohort_stream_locale_packs.cohort_id = cohort_applications_plus.cohort_id
                    left join lesson_streams_progress
                        on published_cohort_stream_locale_packs.stream_locale_pack_id = lesson_streams_progress.locale_pack_id
                        and cohort_applications_plus.user_id = lesson_streams_progress.user_id
                where
                    cohort_applications_plus.was_accepted=true
                group by
                    cohort_applications_plus.cohort_id
                    , cohort_applications_plus.user_id
            )
            , playlist_counts as (
                select
                    cohort_applications_plus.cohort_id
                    , cohort_applications_plus.user_id
                    , sum(case
                            when required = true and completed = true then 1
                            else 0
                            end
                        ) as required_playlists_complete
                    , sum(case
                            when specialization = true and completed = true then 1
                            else 0
                            end
                        ) as specialization_playlists_complete
                from cohort_applications_plus
                    join published_cohort_playlist_locale_packs
                        on published_cohort_playlist_locale_packs.cohort_id = cohort_applications_plus.cohort_id
                    left join playlist_progress
                        on published_cohort_playlist_locale_packs.playlist_locale_pack_id = playlist_progress.locale_pack_id
                        and cohort_applications_plus.user_id = playlist_progress.user_id
                where
                    cohort_applications_plus.was_accepted=true
                group by
                    cohort_applications_plus.cohort_id
                    , cohort_applications_plus.user_id
            )
            , with_counts as (
                select
                    current_status.cohort_name
                    , current_status.cohort_id
                    , current_status.user_id
                    , current_status.status

                    , stream_counts.required_streams_complete
                    , stream_counts.foundations_streams_complete
                    , stream_counts.exam_streams_complete
                    , stream_counts.elective_streams_complete

                    , playlist_counts.required_playlists_complete
                    , playlist_counts.specialization_playlists_complete

                    , lesson_counts.required_lessons_complete
                    , lesson_counts.foundations_lessons_complete
                    , lesson_counts.test_lessons_complete
                    , lesson_counts.elective_lessons_complete
                    , lesson_counts.average_assessment_score_first
                    , lesson_counts.average_assessment_score_best
                    , lesson_counts.avg_test_score

                from current_status
                    join lesson_counts
                        on current_status.cohort_id = lesson_counts.cohort_id
                        and current_status.user_id = lesson_counts.user_id
                    join stream_counts
                        on current_status.cohort_id = stream_counts.cohort_id
                        and current_status.user_id = stream_counts.user_id
                    join playlist_counts
                        on current_status.cohort_id = playlist_counts.cohort_id
                        and current_status.user_id = playlist_counts.user_id

            )
            , with_averages as (
                select
                    with_counts.*

                    -- no entry in the table means you did not participate at all, and get a score of 0
                    , coalesce(user_participation_scores.score, 0) as participation_score
                    , coalesce(cohort_applications_plus.final_score,
                          case
                            when with_counts.cohort_name='MBA1'
                                then (
                                    0.7*(case when avg_test_score is null then 0 else avg_test_score end)
                                    + 0.3*(case when average_assessment_score_best is null then 0 else average_assessment_score_best end)
                                )
                            else
                                (
                                    0.7*(case when avg_test_score is null then 0 else avg_test_score end)
                                    + 0.2*(case when average_assessment_score_best is null then 0 else average_assessment_score_best end)
                                    + 0.1*coalesce(user_participation_scores.score, 0)
                                )
                            end
                      ) as final_score
                    , required_streams_complete::float / cohort_content_details.required_stream_count as required_streams_perc_complete
                    , required_lessons_complete::float / cohort_content_details.required_lesson_count as required_lessons_perc_complete
                    , case when cohort_content_details.elective_lesson_count > 0
                        then elective_lessons_complete::float / cohort_content_details.elective_lesson_count
                        else null
                        end as elective_lessons_perc_complete
                    , case when cohort_content_details.elective_stream_count > 0
                        then elective_streams_complete::float / cohort_content_details.elective_stream_count
                        else null
                        end as elective_streams_perc_complete
                from with_counts
                    join published_cohort_content_details cohort_content_details
                        on with_counts.cohort_id = cohort_content_details.cohort_id
                    left join user_participation_scores
                        on user_participation_scores.user_id = with_counts.user_id
                    left join cohort_applications_plus
                        on cohort_applications_plus.cohort_id = with_counts.cohort_id
                          and cohort_applications_plus.user_id = with_counts.user_id
            )
            , with_grad_requirements as (
                select
                    with_averages.*
                    , final_score > 0.7
                        and required_streams_perc_complete = 1
                        and specialization_playlists_complete >= cohorts_versions.num_required_specializations
                    as meets_graduation_requirements
                from with_averages
                    join published_cohorts on with_averages.cohort_id = published_cohorts.id
                    join cohorts_versions on published_cohorts.version_id = cohorts_versions.version_id
            )
            select * from with_grad_requirements
            with no data
        |

        add_index :cohort_user_progress_records, [:cohort_id, :user_id], :unique => true
    end

    # * add info about specialization playlists
    # * add info about percent of streams complete (not lessons)
    # * remove weekly info
    def version_20170726074016
        execute %Q|
            create materialized view cohort_user_progress_records as
            with current_status as (
                select
                    cohort_applications_plus.cohort_id
                    , cohort_applications_plus.user_id
                    , cohort_applications_plus.cohort_name
                    , cohort_status_changes.status
                from cohort_applications_plus
                    join cohort_status_changes on
                        cohort_status_changes.cohort_id = cohort_applications_plus.cohort_id
                        and cohort_status_changes.user_id = cohort_applications_plus.user_id
                        and cohort_status_changes.until_time = '01-01-2099'
                where
                    cohort_applications_plus.was_accepted=true
            ), lesson_counts as (
                select
                    cohort_user_lesson_progress_records.cohort_id
                    , cohort_user_lesson_progress_records.user_id
                    , sum(case
                            when foundations = true and completed_at is not null then 1
                            when foundations = true and completed_at is null then 0
                            else null
                            end
                        ) as foundations_lessons_complete
                    , sum(case
                            when required = true and completed_at is not null then 1
                            when required = true and completed_at is null then 0
                            else null
                            end
                        ) as required_lessons_complete
                    , sum(case
                            when test = true and completed_at is not null then 1
                            when test = true and completed_at is null then 0
                            else null
                            end
                        ) as test_lessons_complete
                    , sum(case
                            when elective = true and completed_at is not null then 1
                            when elective = true and completed_at is null then 0
                            else null
                            end
                        ) as elective_lessons_complete
                    , avg(case when assessment then average_assessment_score_first else null end) as average_assessment_score_first
                    , avg(case when assessment then average_assessment_score_best else null end) as average_assessment_score_best
                    , avg(official_test_score) as avg_test_score
                from cohort_applications_plus
                    left join cohort_user_lesson_progress_records on
                        cohort_applications_plus.cohort_id = cohort_user_lesson_progress_records.cohort_id
                        and cohort_applications_plus.user_id = cohort_user_lesson_progress_records.user_id
                where
                    cohort_applications_plus.was_accepted=true
                group by
                    cohort_user_lesson_progress_records.cohort_id
                    , cohort_user_lesson_progress_records.user_id
            )
            , stream_counts as (
                select
                    cohort_applications_plus.cohort_id
                    , cohort_applications_plus.user_id
                    , sum(case
                            when foundations = true and completed_at is not null then 1
                            when foundations = true and completed_at is null then 0
                            else null
                            end
                        ) as foundations_streams_complete
                    , sum(case
                            when required = true and completed_at is not null then 1
                            when required = true and completed_at is null then 0
                            else null
                            end
                        ) as required_streams_complete
                    , sum(case
                            when exam = true and completed_at is not null then 1
                            when exam = true and completed_at is null then 0
                            else null
                            end
                        ) as exam_streams_complete
                    , sum(case
                            when elective = true and completed_at is not null then 1
                            when elective = true and completed_at is null then 0
                            else null
                            end
                        ) as elective_streams_complete
                from cohort_applications_plus
                    join published_cohort_stream_locale_packs
                        on published_cohort_stream_locale_packs.cohort_id = cohort_applications_plus.cohort_id
                    left join lesson_streams_progress
                        on published_cohort_stream_locale_packs.stream_locale_pack_id = lesson_streams_progress.locale_pack_id
                        and cohort_applications_plus.user_id = lesson_streams_progress.user_id
                where
                    cohort_applications_plus.was_accepted=true
                group by
                    cohort_applications_plus.cohort_id
                    , cohort_applications_plus.user_id
            )
            , playlist_counts as (
                select
                    cohort_applications_plus.cohort_id
                    , cohort_applications_plus.user_id
                    , sum(case
                            when required = true and completed = true then 1
                            when required = true and completed = false then 0
                            else null
                            end
                        ) as required_playlists_complete
                    , sum(case
                            when specialization = true and completed = true then 1
                            when specialization = true and completed = false then 0
                            else null
                            end
                        ) as specialization_playlists_complete
                from cohort_applications_plus
                    join published_cohort_playlist_locale_packs
                        on published_cohort_playlist_locale_packs.cohort_id = cohort_applications_plus.cohort_id
                    left join playlist_progress
                        on published_cohort_playlist_locale_packs.playlist_locale_pack_id = playlist_progress.locale_pack_id
                        and cohort_applications_plus.user_id = playlist_progress.user_id
                where
                    cohort_applications_plus.was_accepted=true
                group by
                    cohort_applications_plus.cohort_id
                    , cohort_applications_plus.user_id
            )
            , with_counts as (
                select
                    current_status.cohort_name
                    , current_status.cohort_id
                    , current_status.user_id
                    , current_status.status

                    , stream_counts.required_streams_complete
                    , stream_counts.foundations_streams_complete
                    , stream_counts.exam_streams_complete
                    , stream_counts.elective_streams_complete

                    , playlist_counts.required_playlists_complete
                    , playlist_counts.specialization_playlists_complete

                    , lesson_counts.required_lessons_complete
                    , lesson_counts.foundations_lessons_complete
                    , lesson_counts.test_lessons_complete
                    , lesson_counts.elective_lessons_complete
                    , lesson_counts.average_assessment_score_first
                    , lesson_counts.average_assessment_score_best
                    , lesson_counts.avg_test_score

                from current_status
                    join lesson_counts
                        on current_status.cohort_id = lesson_counts.cohort_id
                        and current_status.user_id = lesson_counts.user_id
                    join stream_counts
                        on current_status.cohort_id = stream_counts.cohort_id
                        and current_status.user_id = stream_counts.user_id
                    join playlist_counts
                        on current_status.cohort_id = playlist_counts.cohort_id
                        and current_status.user_id = playlist_counts.user_id

            )
            , with_averages as (
                select
                    with_counts.*

                    -- no entry in the table means you did not participate at all, and get a score of 0
                    , coalesce(user_participation_scores.score, 0) as participation_score
                    , coalesce(cohort_applications_plus.final_score,
                          case
                            when with_counts.cohort_name='MBA1'
                                then (
                                    0.7*(case when avg_test_score is null then 0 else avg_test_score end)
                                    + 0.3*(case when average_assessment_score_best is null then 0 else average_assessment_score_best end)
                                )
                            else
                                (
                                    0.7*(case when avg_test_score is null then 0 else avg_test_score end)
                                    + 0.2*(case when average_assessment_score_best is null then 0 else average_assessment_score_best end)
                                    + 0.1*coalesce(user_participation_scores.score, 0)
                                )
                            end
                      ) as final_score
                    , required_streams_complete::float / cohort_content_details.required_stream_count as required_streams_perc_complete
                    , required_lessons_complete::float / cohort_content_details.required_lesson_count as required_lessons_perc_complete
                    , elective_lessons_complete::float / cohort_content_details.elective_lesson_count as elective_lessons_perc_complete
                    , elective_streams_complete::float / cohort_content_details.elective_stream_count as elective_streams_perc_complete
                from with_counts
                    join published_cohort_content_details cohort_content_details
                        on with_counts.cohort_id = cohort_content_details.cohort_id
                    left join user_participation_scores
                        on user_participation_scores.user_id = with_counts.user_id
                    left join cohort_applications_plus
                        on cohort_applications_plus.cohort_id = with_counts.cohort_id
                          and cohort_applications_plus.user_id = with_counts.user_id
            )
            , with_grad_requirements as (
                select
                    with_averages.*
                    , final_score > 0.7
                        and required_streams_perc_complete = 1
                        and specialization_playlists_complete >= cohorts_versions.num_required_specializations
                    as meets_graduation_requirements
                from with_averages
                    join published_cohorts on with_averages.cohort_id = published_cohorts.id
                    join cohorts_versions on published_cohorts.version_id = cohorts_versions.version_id
            )
            select * from with_grad_requirements
            with no data
        |

        add_index :cohort_user_progress_records, [:cohort_id, :user_id], :unique => true
    end

    # pay attention to the final_score on the cohort application
    # when there is one
    def version_20170703170913
        execute %Q|
            create materialized view cohort_user_progress_records as
            with with_counts as (
                select
                    cohort_user_lesson_progress_records.cohort_id
                    , cohort_user_lesson_progress_records.cohort_name
                    , cohort_user_lesson_progress_records.user_id
                    , cohort_status_changes.status
                    , sum(case
                            when foundations = true and completed_at is not null then 1
                            when foundations = true and completed_at is null then 0
                            else null
                            end
                        ) as foundations_lessons_complete
                    , sum(case
                            when required = true and completed_at is not null then 1
                            when required = true and completed_at is null then 0
                            else null
                            end
                        ) as curriculum_lessons_complete
                    , sum(case
                            when test = true and completed_at is not null then 1
                            when test = true and completed_at is null then 0
                            else null
                            end
                        ) as test_lessons_complete
                    , sum(case
                            when elective = true and completed_at is not null then 1
                            when elective = true and completed_at is null then 0
                            else null
                            end
                        ) as elective_lessons_complete
                    , avg(case when assessment then average_assessment_score_first else null end) as average_assessment_score_first
                    , avg(case when assessment then average_assessment_score_best else null end) as average_assessment_score_best
                    , avg(official_test_score) as avg_test_score
                from cohort_applications_plus
                    join cohort_status_changes on
                        cohort_status_changes.cohort_id = cohort_applications_plus.cohort_id
                        and cohort_status_changes.user_id = cohort_applications_plus.user_id
                        and cohort_status_changes.until_time = '01-01-2099'
                    left join cohort_user_lesson_progress_records on
                        cohort_applications_plus.cohort_id = cohort_user_lesson_progress_records.cohort_id
                        and cohort_applications_plus.user_id = cohort_user_lesson_progress_records.user_id
                where
                    cohort_applications_plus.was_accepted=true
                group by
                    cohort_user_lesson_progress_records.cohort_id
                    , cohort_user_lesson_progress_records.cohort_name
                    , cohort_user_lesson_progress_records.user_id
                    , cohort_status_changes.status
            )
            , with_averages as (
                select
                    with_counts.*

                    -- no entry in the table means you did not participate at all, and get a score of 0
                    , coalesce(user_participation_scores.score, 0) as participation_score
                    , coalesce(cohort_applications_plus.final_score,
                          case
                            when with_counts.cohort_name='MBA1'
                                then (
                                    0.7*(case when avg_test_score is null then 0 else avg_test_score end)
                                    + 0.3*(case when average_assessment_score_best is null then 0 else average_assessment_score_best end)
                                )
                            else
                                (
                                    0.7*(case when avg_test_score is null then 0 else avg_test_score end)
                                    + 0.2*(case when average_assessment_score_best is null then 0 else average_assessment_score_best end)
                                    + 0.1*coalesce(user_participation_scores.score, 0)
                                )
                            end
                      ) as final_score
                    , foundations_lessons_complete::float / cohort_content_details.foundations_lesson_count as foundations_perc_complete
                    , curriculum_lessons_complete::float / cohort_content_details.required_lesson_count as curriculum_perc_complete
                    , test_lessons_complete::float / cohort_content_details.test_lesson_count as test_perc_complete
                    , elective_lessons_complete::float / cohort_content_details.elective_lesson_count as elective_perc_complete
                from with_counts
                    join published_cohort_content_details cohort_content_details
                        on with_counts.cohort_id = cohort_content_details.cohort_id
                    left join user_participation_scores
                        on user_participation_scores.user_id = with_counts.user_id
                    left join cohort_applications_plus
                        on cohort_applications_plus.cohort_id = with_counts.cohort_id
                          and cohort_applications_plus.user_id = with_counts.user_id
            )
            , with_grad_requirements as (
                select
                    with_averages.*
                    , final_score > 0.7 and curriculum_perc_complete = 1 as meets_graduation_requirements
                from with_averages
            )
            , cohort_applications_with_weeks as (
                SELECT
                    cohort_application_id
                    , week0
                    , week1
                    , week2
                    , week3
                    , week4
                    , week5
                    , week6
                    , week7
                    , week8
                    , week9
                    , week10
                    , week11
                    , week12
                    , week13
                    , week14
                    , week15
                    , week16
                    , week17
                    , week18
                    , week19
                    , week20
                    , week21
                    , week22
                    , week23
                    , week24
                    , week25
                    , week26
                FROM crosstab(
                  $$
                    with all_week_indexes as (
                        SELECT
                            week_index
                        FROM
                            generate_series(0, 26) AS week_index
                    )
                    select
                        cohort_applications_plus.id
                        , all_week_indexes.week_index
                        , coalesce(lessons_completed_in_week, 0) as lessons_completed_in_week
                    from cohort_applications_plus
                        cross join all_week_indexes
                        left join cohort_user_weeks
                            on cohort_applications_plus.user_id = cohort_user_weeks.user_id
                            and cohort_applications_plus.cohort_id = cohort_user_weeks.cohort_id
                            and all_week_indexes.week_index = cohort_user_weeks.week_index
                    where
                        cohort_applications_plus.was_accepted=true
                    order by
                        cohort_applications_plus.id
                        , all_week_indexes.week_index
                  $$
                  , 'VALUES (0),(1),(2),(3),(4),(5),(6),(7),(8),(9),(10),(11),(12),(13),(14),(15),(16),(17),(18),(19),(20),(21),(22),(23),(24),(25),(26)'
                  ) AS t (
                    cohort_application_id uuid
                    , week0 int
                    , week1 int
                    , week2 int
                    , week3 int
                    , week4 int
                    , week5 int
                    , week6 int
                    , week7 int
                    , week8 int
                    , week9 int
                    , week10 int
                    , week11 int
                    , week12 int
                    , week13 int
                    , week14 int
                    , week15 int
                    , week16 int
                    , week17 int
                    , week18 int
                    , week19 int
                    , week20 int
                    , week21 int
                    , week22 int
                    , week23 int
                    , week24 int
                    , week25 int
                    , week26 int
                  )
            )
            , flattened_weeks as (
                select
                    cohort_id
                    , user_id
                    , cohort_applications_with_weeks.*
                from cohort_applications_with_weeks
                    join cohort_applications_plus
                        on cohort_applications_with_weeks.cohort_application_id = cohort_applications_plus.id
            )
            , with_weeks as (
                select
                    with_grad_requirements.*
                    , coalesce(flattened_weeks.week0, 0) week0
                    , coalesce(flattened_weeks.week1, 0) week1
                    , coalesce(flattened_weeks.week2, 0) week2
                    , coalesce(flattened_weeks.week3, 0) week3
                    , coalesce(flattened_weeks.week4, 0) week4
                    , coalesce(flattened_weeks.week5, 0) week5
                    , coalesce(flattened_weeks.week6, 0) week6
                    , coalesce(flattened_weeks.week7, 0) week7
                    , coalesce(flattened_weeks.week8, 0) week8
                    , coalesce(flattened_weeks.week9, 0) week9
                    , coalesce(flattened_weeks.week10, 0) week10
                    , coalesce(flattened_weeks.week11, 0) week11
                    , coalesce(flattened_weeks.week12, 0) week12
                    , coalesce(flattened_weeks.week13, 0) week13
                    , coalesce(flattened_weeks.week14, 0) week14
                    , coalesce(flattened_weeks.week15, 0) week15
                    , coalesce(flattened_weeks.week16, 0) week16
                    , coalesce(flattened_weeks.week17, 0) week17
                    , coalesce(flattened_weeks.week18, 0) week18
                    , coalesce(flattened_weeks.week19, 0) week19
                    , coalesce(flattened_weeks.week20, 0) week20
                    , coalesce(flattened_weeks.week21, 0) week21
                    , coalesce(flattened_weeks.week22, 0) week22
                    , coalesce(flattened_weeks.week23, 0) week23
                    , coalesce(flattened_weeks.week24, 0) week24
                    , coalesce(flattened_weeks.week25, 0) week25
                    , coalesce(flattened_weeks.week26, 0) week26
                from with_grad_requirements
                    left join flattened_weeks on
                        with_grad_requirements.user_id = flattened_weeks.user_id
                        and with_grad_requirements.cohort_id = flattened_weeks.cohort_id
            )
            select * from with_weeks
            with no data
        |

        add_index :cohort_user_progress_records, [:cohort_id, :user_id], :unique => true
    end

    # use official_test_score to calculate avg_test_score
    def version_20170526143117
        execute %Q|
            create materialized view cohort_user_progress_records as
            with with_counts as (
                select
                    cohort_user_lesson_progress_records.cohort_id
                    , cohort_user_lesson_progress_records.cohort_name
                    , cohort_user_lesson_progress_records.user_id
                    , cohort_status_changes.status
                    , sum(case
                            when foundations = true and completed_at is not null then 1
                            when foundations = true and completed_at is null then 0
                            else null
                            end
                        ) as foundations_lessons_complete
                    , sum(case
                            when required = true and completed_at is not null then 1
                            when required = true and completed_at is null then 0
                            else null
                            end
                        ) as curriculum_lessons_complete
                    , sum(case
                            when test = true and completed_at is not null then 1
                            when test = true and completed_at is null then 0
                            else null
                            end
                        ) as test_lessons_complete
                    , sum(case
                            when elective = true and completed_at is not null then 1
                            when elective = true and completed_at is null then 0
                            else null
                            end
                        ) as elective_lessons_complete
                    , avg(case when assessment then average_assessment_score_first else null end) as average_assessment_score_first
                    , avg(case when assessment then average_assessment_score_best else null end) as average_assessment_score_best
                    , avg(official_test_score) as avg_test_score
                from cohort_applications_plus
                    join cohort_status_changes on
                        cohort_status_changes.cohort_id = cohort_applications_plus.cohort_id
                        and cohort_status_changes.user_id = cohort_applications_plus.user_id
                        and cohort_status_changes.until_time = '01-01-2099'
                    left join cohort_user_lesson_progress_records on
                        cohort_applications_plus.cohort_id = cohort_user_lesson_progress_records.cohort_id
                        and cohort_applications_plus.user_id = cohort_user_lesson_progress_records.user_id
                where
                    cohort_applications_plus.was_accepted=true
                group by
                    cohort_user_lesson_progress_records.cohort_id
                    , cohort_user_lesson_progress_records.cohort_name
                    , cohort_user_lesson_progress_records.user_id
                    , cohort_status_changes.status
            )
            , with_averages as (
                select
                    with_counts.*

                    -- no entry in the table means you did not participate at all, and get a score of 0
                    , coalesce(user_participation_scores.score, 0) as participation_score
                    , case
                        when with_counts.cohort_name='MBA1'
                            then (
                                0.7*(case when avg_test_score is null then 0 else avg_test_score end)
                                + 0.3*(case when average_assessment_score_best is null then 0 else average_assessment_score_best end)
                            )
                        else
                            (
                                0.7*(case when avg_test_score is null then 0 else avg_test_score end)
                                + 0.2*(case when average_assessment_score_best is null then 0 else average_assessment_score_best end)
                                + 0.1*coalesce(user_participation_scores.score, 0)
                            )
                        end as final_score
                    , foundations_lessons_complete::float / cohort_content_details.foundations_lesson_count as foundations_perc_complete
                    , curriculum_lessons_complete::float / cohort_content_details.required_lesson_count as curriculum_perc_complete
                    , test_lessons_complete::float / cohort_content_details.test_lesson_count as test_perc_complete
                    , elective_lessons_complete::float / cohort_content_details.elective_lesson_count as elective_perc_complete
                from with_counts
                    join published_cohort_content_details cohort_content_details
                        on with_counts.cohort_id = cohort_content_details.cohort_id
                    left join user_participation_scores
                        on user_participation_scores.user_id = with_counts.user_id
            )
            , with_grad_requirements as (
                select
                    with_averages.*
                    , final_score > 0.7 and curriculum_perc_complete = 1 as meets_graduation_requirements
                from with_averages
            )
            , cohort_applications_with_weeks as (
                SELECT
                    cohort_application_id
                    , week0
                    , week1
                    , week2
                    , week3
                    , week4
                    , week5
                    , week6
                    , week7
                    , week8
                    , week9
                    , week10
                    , week11
                    , week12
                    , week13
                    , week14
                    , week15
                    , week16
                    , week17
                    , week18
                    , week19
                    , week20
                    , week21
                    , week22
                    , week23
                    , week24
                    , week25
                    , week26
                FROM crosstab(
                  $$
                    with all_week_indexes as (
                        SELECT
                            week_index
                        FROM
                            generate_series(0, 26) AS week_index
                    )
                    select
                        cohort_applications_plus.id
                        , all_week_indexes.week_index
                        , coalesce(lessons_completed_in_week, 0) as lessons_completed_in_week
                    from cohort_applications_plus
                        cross join all_week_indexes
                        left join cohort_user_weeks
                            on cohort_applications_plus.user_id = cohort_user_weeks.user_id
                            and cohort_applications_plus.cohort_id = cohort_user_weeks.cohort_id
                            and all_week_indexes.week_index = cohort_user_weeks.week_index
                    where
                        cohort_applications_plus.was_accepted=true
                    order by
                        cohort_applications_plus.id
                        , all_week_indexes.week_index
                  $$
                  , 'VALUES (0),(1),(2),(3),(4),(5),(6),(7),(8),(9),(10),(11),(12),(13),(14),(15),(16),(17),(18),(19),(20),(21),(22),(23),(24),(25),(26)'
                  ) AS t (
                    cohort_application_id uuid
                    , week0 int
                    , week1 int
                    , week2 int
                    , week3 int
                    , week4 int
                    , week5 int
                    , week6 int
                    , week7 int
                    , week8 int
                    , week9 int
                    , week10 int
                    , week11 int
                    , week12 int
                    , week13 int
                    , week14 int
                    , week15 int
                    , week16 int
                    , week17 int
                    , week18 int
                    , week19 int
                    , week20 int
                    , week21 int
                    , week22 int
                    , week23 int
                    , week24 int
                    , week25 int
                    , week26 int
                  )
            )
            , flattened_weeks as (
                select
                    cohort_id
                    , user_id
                    , cohort_applications_with_weeks.*
                from cohort_applications_with_weeks
                    join cohort_applications_plus
                        on cohort_applications_with_weeks.cohort_application_id = cohort_applications_plus.id
            )
            , with_weeks as (
                select
                    with_grad_requirements.*
                    , coalesce(flattened_weeks.week0, 0) week0
                    , coalesce(flattened_weeks.week1, 0) week1
                    , coalesce(flattened_weeks.week2, 0) week2
                    , coalesce(flattened_weeks.week3, 0) week3
                    , coalesce(flattened_weeks.week4, 0) week4
                    , coalesce(flattened_weeks.week5, 0) week5
                    , coalesce(flattened_weeks.week6, 0) week6
                    , coalesce(flattened_weeks.week7, 0) week7
                    , coalesce(flattened_weeks.week8, 0) week8
                    , coalesce(flattened_weeks.week9, 0) week9
                    , coalesce(flattened_weeks.week10, 0) week10
                    , coalesce(flattened_weeks.week11, 0) week11
                    , coalesce(flattened_weeks.week12, 0) week12
                    , coalesce(flattened_weeks.week13, 0) week13
                    , coalesce(flattened_weeks.week14, 0) week14
                    , coalesce(flattened_weeks.week15, 0) week15
                    , coalesce(flattened_weeks.week16, 0) week16
                    , coalesce(flattened_weeks.week17, 0) week17
                    , coalesce(flattened_weeks.week18, 0) week18
                    , coalesce(flattened_weeks.week19, 0) week19
                    , coalesce(flattened_weeks.week20, 0) week20
                    , coalesce(flattened_weeks.week21, 0) week21
                    , coalesce(flattened_weeks.week22, 0) week22
                    , coalesce(flattened_weeks.week23, 0) week23
                    , coalesce(flattened_weeks.week24, 0) week24
                    , coalesce(flattened_weeks.week25, 0) week25
                    , coalesce(flattened_weeks.week26, 0) week26
                from with_grad_requirements
                    left join flattened_weeks on
                        with_grad_requirements.user_id = flattened_weeks.user_id
                        and with_grad_requirements.cohort_id = flattened_weeks.cohort_id
            )
            select * from with_weeks
            with no data
        |

        add_index :cohort_user_progress_records, [:cohort_id, :user_id], :unique => true
    end

    # fix weekly data
    def version_20170323141852
        execute %Q|
            create materialized view cohort_user_progress_records as
            with with_counts as (
                select
                    cohort_user_lesson_progress_records.cohort_id
                    , cohort_user_lesson_progress_records.cohort_name
                    , cohort_user_lesson_progress_records.user_id
                    , cohort_status_changes.status
                    , sum(case
                            when foundations = true and completed_at is not null then 1
                            when foundations = true and completed_at is null then 0
                            else null
                            end
                        ) as foundations_lessons_complete
                    , sum(case
                            when required = true and completed_at is not null then 1
                            when required = true and completed_at is null then 0
                            else null
                            end
                        ) as curriculum_lessons_complete
                    , sum(case
                            when test = true and completed_at is not null then 1
                            when test = true and completed_at is null then 0
                            else null
                            end
                        ) as test_lessons_complete
                    , sum(case
                            when elective = true and completed_at is not null then 1
                            when elective = true and completed_at is null then 0
                            else null
                            end
                        ) as elective_lessons_complete
                    , avg(case when assessment then average_assessment_score_first else null end) as average_assessment_score_first
                    , avg(case when assessment then average_assessment_score_best else null end) as average_assessment_score_best
                    , avg(case when test then average_assessment_score_first else null end) as avg_test_score
                from cohort_applications_plus
                    join cohort_status_changes on
                        cohort_status_changes.cohort_id = cohort_applications_plus.cohort_id
                        and cohort_status_changes.user_id = cohort_applications_plus.user_id
                        and cohort_status_changes.until_time = '01-01-2099'
                    left join cohort_user_lesson_progress_records on
                        cohort_applications_plus.cohort_id = cohort_user_lesson_progress_records.cohort_id
                        and cohort_applications_plus.user_id = cohort_user_lesson_progress_records.user_id
                where
                    cohort_applications_plus.was_accepted=true
                group by
                    cohort_user_lesson_progress_records.cohort_id
                    , cohort_user_lesson_progress_records.cohort_name
                    , cohort_user_lesson_progress_records.user_id
                    , cohort_status_changes.status
            )
            , with_averages as (
                select
                    with_counts.*

                    -- no entry in the table means you did not participate at all, and get a score of 0
                    , coalesce(user_participation_scores.score, 0) as participation_score
                    , case
                        when with_counts.cohort_name='MBA1'
                            then (
                                0.7*(case when avg_test_score is null then 0 else avg_test_score end)
                                + 0.3*(case when average_assessment_score_best is null then 0 else average_assessment_score_best end)
                            )
                        else
                            (
                                0.7*(case when avg_test_score is null then 0 else avg_test_score end)
                                + 0.2*(case when average_assessment_score_best is null then 0 else average_assessment_score_best end)
                                + 0.1*coalesce(user_participation_scores.score, 0)
                            )
                        end as final_score
                    , foundations_lessons_complete::float / cohort_content_details.foundations_lesson_count as foundations_perc_complete
                    , curriculum_lessons_complete::float / cohort_content_details.required_lesson_count as curriculum_perc_complete
                    , test_lessons_complete::float / cohort_content_details.test_lesson_count as test_perc_complete
                    , elective_lessons_complete::float / cohort_content_details.elective_lesson_count as elective_perc_complete
                from with_counts
                    join published_cohort_content_details cohort_content_details
                        on with_counts.cohort_id = cohort_content_details.cohort_id
                    left join user_participation_scores
                        on user_participation_scores.user_id = with_counts.user_id
            )
            , with_grad_requirements as (
                select
                    with_averages.*
                    , final_score > 0.7 and curriculum_perc_complete = 1 as meets_graduation_requirements
                from with_averages
            )
            , cohort_applications_with_weeks as (
                SELECT
                    cohort_application_id
                    , week0
                    , week1
                    , week2
                    , week3
                    , week4
                    , week5
                    , week6
                    , week7
                    , week8
                    , week9
                    , week10
                    , week11
                    , week12
                    , week13
                    , week14
                    , week15
                    , week16
                    , week17
                    , week18
                    , week19
                    , week20
                    , week21
                    , week22
                    , week23
                    , week24
                    , week25
                    , week26
                FROM crosstab(
                  $$
                    with all_week_indexes as (
                        SELECT
                            week_index
                        FROM
                            generate_series(0, 26) AS week_index
                    )
                    select
                        cohort_applications_plus.id
                        , all_week_indexes.week_index
                        , coalesce(lessons_completed_in_week, 0) as lessons_completed_in_week
                    from cohort_applications_plus
                        cross join all_week_indexes
                        left join cohort_user_weeks
                            on cohort_applications_plus.user_id = cohort_user_weeks.user_id
                            and cohort_applications_plus.cohort_id = cohort_user_weeks.cohort_id
                            and all_week_indexes.week_index = cohort_user_weeks.week_index
                    where
                        cohort_applications_plus.was_accepted=true
                    order by
                        cohort_applications_plus.id
                        , all_week_indexes.week_index
                  $$
                  , 'VALUES (0),(1),(2),(3),(4),(5),(6),(7),(8),(9),(10),(11),(12),(13),(14),(15),(16),(17),(18),(19),(20),(21),(22),(23),(24),(25),(26)'
                  ) AS t (
                    cohort_application_id uuid
                    , week0 int
                    , week1 int
                    , week2 int
                    , week3 int
                    , week4 int
                    , week5 int
                    , week6 int
                    , week7 int
                    , week8 int
                    , week9 int
                    , week10 int
                    , week11 int
                    , week12 int
                    , week13 int
                    , week14 int
                    , week15 int
                    , week16 int
                    , week17 int
                    , week18 int
                    , week19 int
                    , week20 int
                    , week21 int
                    , week22 int
                    , week23 int
                    , week24 int
                    , week25 int
                    , week26 int
                  )
            )
            , flattened_weeks as (
                select
                    cohort_id
                    , user_id
                    , cohort_applications_with_weeks.*
                from cohort_applications_with_weeks
                    join cohort_applications_plus
                        on cohort_applications_with_weeks.cohort_application_id = cohort_applications_plus.id
            )
            , with_weeks as (
                select
                    with_grad_requirements.*
                    , coalesce(flattened_weeks.week0, 0) week0
                    , coalesce(flattened_weeks.week1, 0) week1
                    , coalesce(flattened_weeks.week2, 0) week2
                    , coalesce(flattened_weeks.week3, 0) week3
                    , coalesce(flattened_weeks.week4, 0) week4
                    , coalesce(flattened_weeks.week5, 0) week5
                    , coalesce(flattened_weeks.week6, 0) week6
                    , coalesce(flattened_weeks.week7, 0) week7
                    , coalesce(flattened_weeks.week8, 0) week8
                    , coalesce(flattened_weeks.week9, 0) week9
                    , coalesce(flattened_weeks.week10, 0) week10
                    , coalesce(flattened_weeks.week11, 0) week11
                    , coalesce(flattened_weeks.week12, 0) week12
                    , coalesce(flattened_weeks.week13, 0) week13
                    , coalesce(flattened_weeks.week14, 0) week14
                    , coalesce(flattened_weeks.week15, 0) week15
                    , coalesce(flattened_weeks.week16, 0) week16
                    , coalesce(flattened_weeks.week17, 0) week17
                    , coalesce(flattened_weeks.week18, 0) week18
                    , coalesce(flattened_weeks.week19, 0) week19
                    , coalesce(flattened_weeks.week20, 0) week20
                    , coalesce(flattened_weeks.week21, 0) week21
                    , coalesce(flattened_weeks.week22, 0) week22
                    , coalesce(flattened_weeks.week23, 0) week23
                    , coalesce(flattened_weeks.week24, 0) week24
                    , coalesce(flattened_weeks.week25, 0) week25
                    , coalesce(flattened_weeks.week26, 0) week26
                from with_grad_requirements
                    left join flattened_weeks on
                        with_grad_requirements.user_id = flattened_weeks.user_id
                        and with_grad_requirements.cohort_id = flattened_weeks.cohort_id
            )
            select * from with_weeks
            with no data
        |

        add_index :cohort_user_progress_records, [:cohort_id, :user_id], :unique => true
    end

    # user published_cohort_content_details instead of cohort_content_details
    #
    # change in_curriculum to required
    def version_20170310204902
        execute %Q|

            create materialized view cohort_user_progress_records as
            with with_counts as (
                select
                    cohort_user_lesson_progress_records.cohort_id
                    , cohort_user_lesson_progress_records.cohort_name
                    , cohort_user_lesson_progress_records.user_id
                    , cohort_status_changes.status
                    , sum(case
                            when foundations = true and completed_at is not null then 1
                            when foundations = true and completed_at is null then 0
                            else null
                            end
                        ) as foundations_lessons_complete
                    , sum(case
                            when required = true and completed_at is not null then 1
                            when required = true and completed_at is null then 0
                            else null
                            end
                        ) as curriculum_lessons_complete
                    , sum(case
                            when test = true and completed_at is not null then 1
                            when test = true and completed_at is null then 0
                            else null
                            end
                        ) as test_lessons_complete
                    , sum(case
                            when elective = true and completed_at is not null then 1
                            when elective = true and completed_at is null then 0
                            else null
                            end
                        ) as elective_lessons_complete
                    , avg(case when assessment then average_assessment_score_first else null end) as average_assessment_score_first
                    , avg(case when assessment then average_assessment_score_best else null end) as average_assessment_score_best
                    , avg(case when test then average_assessment_score_first else null end) as avg_test_score
                from cohort_applications_plus
                    join cohort_status_changes on
                        cohort_status_changes.cohort_id = cohort_applications_plus.cohort_id
                        and cohort_status_changes.user_id = cohort_applications_plus.user_id
                        and cohort_status_changes.until_time = '01-01-2099'
                    left join cohort_user_lesson_progress_records on
                        cohort_applications_plus.cohort_id = cohort_user_lesson_progress_records.cohort_id
                        and cohort_applications_plus.user_id = cohort_user_lesson_progress_records.user_id
                where
                    cohort_applications_plus.was_accepted=true
                group by
                    cohort_user_lesson_progress_records.cohort_id
                    , cohort_user_lesson_progress_records.cohort_name
                    , cohort_user_lesson_progress_records.user_id
                    , cohort_status_changes.status
            )
            , with_averages as (
                select
                    with_counts.*

                    -- no entry in the table means you did not participate at all, and get a score of 0
                    , coalesce(user_participation_scores.score, 0) as participation_score
                    , case
                        when with_counts.cohort_name='MBA1'
                            then (
                                0.7*(case when avg_test_score is null then 0 else avg_test_score end)
                                + 0.3*(case when average_assessment_score_best is null then 0 else average_assessment_score_best end)
                            )
                        else
                            (
                                0.7*(case when avg_test_score is null then 0 else avg_test_score end)
                                + 0.2*(case when average_assessment_score_best is null then 0 else average_assessment_score_best end)
                                + 0.1*coalesce(user_participation_scores.score, 0)
                            )
                        end as final_score
                    , foundations_lessons_complete::float / cohort_content_details.foundations_lesson_count as foundations_perc_complete
                    , curriculum_lessons_complete::float / cohort_content_details.required_lesson_count as curriculum_perc_complete
                    , test_lessons_complete::float / cohort_content_details.test_lesson_count as test_perc_complete
                    , elective_lessons_complete::float / cohort_content_details.elective_lesson_count as elective_perc_complete
                from with_counts
                    join published_cohort_content_details cohort_content_details
                        on with_counts.cohort_id = cohort_content_details.cohort_id
                    left join user_participation_scores
                        on user_participation_scores.user_id = with_counts.user_id
            )
            , with_grad_requirements as (
                select
                    with_averages.*
                    , final_score > 0.7 and curriculum_perc_complete = 1 as meets_graduation_requirements
                from with_averages
            )
            , flattened_weeks as (
                SELECT
                    cohort_id
                    , user_id

                    -- I don't understand why the summing and grouping is necessary.  But the
                    -- crosstab sometimes returns an extra row with 0s in every column (maybe when
                    -- as user is in multiple cohortS?). This gets around that
                    , sum(week0) week0
                    , sum(week1) week1
                    , sum(week2) week2
                    , sum(week3) week3
                    , sum(week4) week4
                    , sum(week5) week5
                    , sum(week6) week6
                    , sum(week7) week7
                    , sum(week8) week8
                    , sum(week9) week9
                    , sum(week10) week10
                    , sum(week11) week11
                    , sum(week12) week12
                    , sum(week13) week13
                    , sum(week14) week14
                    , sum(week15) week15
                    , sum(week16) week16
                    , sum(week17) week17
                    , sum(week18) week18
                    , sum(week19) week19
                    , sum(week20) week20
                    , sum(week21) week21
                    , sum(week22) week22
                    , sum(week23) week23
                    , sum(week24) week24
                    , sum(week25) week25
                    , sum(week26) week26
                FROM crosstab(
                  $$SELECT cohort_id, user_id, rn, lessons_completed_in_week
                     FROM  (
                        select
                            cohort_user_weeks.cohort_id
                            , cohort_user_weeks.user_id
                            , lessons_completed_in_week
                            , row_number() OVER (PARTITION BY cohort_user_weeks.cohort_id, cohort_user_weeks.user_id
                                           ORDER BY cohort_user_weeks.week_index ASC NULLS LAST) AS rn
                        from cohort_user_weeks
                        order by
                            cohort_user_weeks.cohort_id
                            , cohort_user_weeks.user_id
                            , week_index
                        ) sub
                     ORDER  BY user_id
                  $$
                  , 'VALUES (1),(2),(3),(4),(5),(6),(7),(8),(9),(10),(11),(12),(13),(14),(15),(16),(17),(18),(19),(20),(21),(22),(23),(24),(25),(26),(27)'
                  ) AS t (
                    cohort_id uuid
                    , user_id uuid
                    , week0 int
                    , week1 int
                    , week2 int
                    , week3 int
                    , week4 int
                    , week5 int
                    , week6 int
                    , week7 int
                    , week8 int
                    , week9 int
                    , week10 int
                    , week11 int
                    , week12 int
                    , week13 int
                    , week14 int
                    , week15 int
                    , week16 int
                    , week17 int
                    , week18 int
                    , week19 int
                    , week20 int
                    , week21 int
                    , week22 int
                    , week23 int
                    , week24 int
                    , week25 int
                    , week26 int
                  )
                  group by
                    cohort_id
                    , user_id
            )
            , with_weeks as (
                select
                    with_grad_requirements.*
                    , coalesce(flattened_weeks.week0, 0) week0
                    , coalesce(flattened_weeks.week1, 0) week1
                    , coalesce(flattened_weeks.week2, 0) week2
                    , coalesce(flattened_weeks.week3, 0) week3
                    , coalesce(flattened_weeks.week4, 0) week4
                    , coalesce(flattened_weeks.week5, 0) week5
                    , coalesce(flattened_weeks.week6, 0) week6
                    , coalesce(flattened_weeks.week7, 0) week7
                    , coalesce(flattened_weeks.week8, 0) week8
                    , coalesce(flattened_weeks.week9, 0) week9
                    , coalesce(flattened_weeks.week10, 0) week10
                    , coalesce(flattened_weeks.week11, 0) week11
                    , coalesce(flattened_weeks.week12, 0) week12
                    , coalesce(flattened_weeks.week13, 0) week13
                    , coalesce(flattened_weeks.week14, 0) week14
                    , coalesce(flattened_weeks.week15, 0) week15
                    , coalesce(flattened_weeks.week16, 0) week16
                    , coalesce(flattened_weeks.week17, 0) week17
                    , coalesce(flattened_weeks.week18, 0) week18
                    , coalesce(flattened_weeks.week19, 0) week19
                    , coalesce(flattened_weeks.week20, 0) week20
                    , coalesce(flattened_weeks.week21, 0) week21
                    , coalesce(flattened_weeks.week22, 0) week22
                    , coalesce(flattened_weeks.week23, 0) week23
                    , coalesce(flattened_weeks.week24, 0) week24
                    , coalesce(flattened_weeks.week25, 0) week25
                    , coalesce(flattened_weeks.week26, 0) week26
                from with_grad_requirements
                    left join flattened_weeks on
                        with_grad_requirements.user_id = flattened_weeks.user_id
                        and with_grad_requirements.cohort_id = flattened_weeks.cohort_id
            )
            select * from with_weeks
            with no data
        |

        add_index :cohort_user_progress_records, [:cohort_id, :user_id], :unique => true
    end

    # add participation scores
    def version_20170227200154
        execute %Q|

            create materialized view cohort_user_progress_records as
            with with_counts as (
                select
                    cohort_user_lesson_progress_records.cohort_id
                    , cohort_user_lesson_progress_records.cohort_name
                    , cohort_user_lesson_progress_records.user_id
                    , cohort_status_changes.status
                    , sum(case
                            when foundations = true and completed_at is not null then 1
                            when foundations = true and completed_at is null then 0
                            else null
                            end
                        ) as foundations_lessons_complete
                    , sum(case
                            when in_curriculum = true and completed_at is not null then 1
                            when in_curriculum = true and completed_at is null then 0
                            else null
                            end
                        ) as curriculum_lessons_complete
                    , sum(case
                            when test = true and completed_at is not null then 1
                            when test = true and completed_at is null then 0
                            else null
                            end
                        ) as test_lessons_complete
                    , sum(case
                            when elective = true and completed_at is not null then 1
                            when elective = true and completed_at is null then 0
                            else null
                            end
                        ) as elective_lessons_complete
                    , avg(case when assessment then average_assessment_score_first else null end) as average_assessment_score_first
                    , avg(case when assessment then average_assessment_score_best else null end) as average_assessment_score_best
                    , avg(case when test then average_assessment_score_first else null end) as avg_test_score
                from cohort_applications_plus
                    join cohort_status_changes on
                        cohort_status_changes.cohort_id = cohort_applications_plus.cohort_id
                        and cohort_status_changes.user_id = cohort_applications_plus.user_id
                        and cohort_status_changes.until_time = '01-01-2099'
                    left join cohort_user_lesson_progress_records on
                        cohort_applications_plus.cohort_id = cohort_user_lesson_progress_records.cohort_id
                        and cohort_applications_plus.user_id = cohort_user_lesson_progress_records.user_id
                where
                    cohort_applications_plus.was_accepted=true
                group by
                    cohort_user_lesson_progress_records.cohort_id
                    , cohort_user_lesson_progress_records.cohort_name
                    , cohort_user_lesson_progress_records.user_id
                    , cohort_status_changes.status
            )
            , with_averages as (
                select
                    with_counts.*

                    -- no entry in the table means you did not participate at all, and get a score of 0
                    , coalesce(user_participation_scores.score, 0) as participation_score
                    , case
                        when with_counts.cohort_name='MBA1'
                            then (
                                0.7*(case when avg_test_score is null then 0 else avg_test_score end)
                                + 0.3*(case when average_assessment_score_best is null then 0 else average_assessment_score_best end)
                            )
                        else
                            (
                                0.7*(case when avg_test_score is null then 0 else avg_test_score end)
                                + 0.2*(case when average_assessment_score_best is null then 0 else average_assessment_score_best end)
                                + 0.1*coalesce(user_participation_scores.score, 0)
                            )
                        end as final_score
                    , foundations_lessons_complete::float / cohort_content_details.foundations_lesson_count as foundations_perc_complete
                    , curriculum_lessons_complete::float / cohort_content_details.in_curriculum_lesson_count as curriculum_perc_complete
                    , test_lessons_complete::float / cohort_content_details.test_lesson_count as test_perc_complete
                    , elective_lessons_complete::float / cohort_content_details.elective_lesson_count as elective_perc_complete
                from with_counts
                    join cohort_content_details on with_counts.cohort_id = cohort_content_details.cohort_id
                    left join user_participation_scores
                        on user_participation_scores.user_id = with_counts.user_id
            )
            , with_grad_requirements as (
                select
                    with_averages.*
                    , final_score > 0.7 and curriculum_perc_complete = 1 as meets_graduation_requirements
                from with_averages
            )
            , flattened_weeks as (
                SELECT
                    cohort_id
                    , user_id

                    -- I don't understand why the summing and grouping is necessary.  But the
                    -- crosstab sometimes returns an extra row with 0s in every column (maybe when
                    -- as user is in multiple cohortS?). This gets around that
                    , sum(week0) week0
                    , sum(week1) week1
                    , sum(week2) week2
                    , sum(week3) week3
                    , sum(week4) week4
                    , sum(week5) week5
                    , sum(week6) week6
                    , sum(week7) week7
                    , sum(week8) week8
                    , sum(week9) week9
                    , sum(week10) week10
                    , sum(week11) week11
                    , sum(week12) week12
                    , sum(week13) week13
                    , sum(week14) week14
                    , sum(week15) week15
                    , sum(week16) week16
                    , sum(week17) week17
                    , sum(week18) week18
                    , sum(week19) week19
                    , sum(week20) week20
                    , sum(week21) week21
                    , sum(week22) week22
                    , sum(week23) week23
                    , sum(week24) week24
                    , sum(week25) week25
                    , sum(week26) week26
                FROM crosstab(
                  $$SELECT cohort_id, user_id, rn, lessons_completed_in_week
                     FROM  (
                        select
                            cohort_user_weeks.cohort_id
                            , cohort_user_weeks.user_id
                            , lessons_completed_in_week
                            , row_number() OVER (PARTITION BY cohort_user_weeks.cohort_id, cohort_user_weeks.user_id
                                           ORDER BY cohort_user_weeks.week_index ASC NULLS LAST) AS rn
                        from cohort_user_weeks
                        order by
                            cohort_user_weeks.cohort_id
                            , cohort_user_weeks.user_id
                            , week_index
                        ) sub
                     ORDER  BY user_id
                  $$
                  , 'VALUES (1),(2),(3),(4),(5),(6),(7),(8),(9),(10),(11),(12),(13),(14),(15),(16),(17),(18),(19),(20),(21),(22),(23),(24),(25),(26),(27)'
                  ) AS t (
                    cohort_id uuid
                    , user_id uuid
                    , week0 int
                    , week1 int
                    , week2 int
                    , week3 int
                    , week4 int
                    , week5 int
                    , week6 int
                    , week7 int
                    , week8 int
                    , week9 int
                    , week10 int
                    , week11 int
                    , week12 int
                    , week13 int
                    , week14 int
                    , week15 int
                    , week16 int
                    , week17 int
                    , week18 int
                    , week19 int
                    , week20 int
                    , week21 int
                    , week22 int
                    , week23 int
                    , week24 int
                    , week25 int
                    , week26 int
                  )
                  group by
                    cohort_id
                    , user_id
            )
            , with_weeks as (
                select
                    with_grad_requirements.*
                    , coalesce(flattened_weeks.week0, 0) week0
                    , coalesce(flattened_weeks.week1, 0) week1
                    , coalesce(flattened_weeks.week2, 0) week2
                    , coalesce(flattened_weeks.week3, 0) week3
                    , coalesce(flattened_weeks.week4, 0) week4
                    , coalesce(flattened_weeks.week5, 0) week5
                    , coalesce(flattened_weeks.week6, 0) week6
                    , coalesce(flattened_weeks.week7, 0) week7
                    , coalesce(flattened_weeks.week8, 0) week8
                    , coalesce(flattened_weeks.week9, 0) week9
                    , coalesce(flattened_weeks.week10, 0) week10
                    , coalesce(flattened_weeks.week11, 0) week11
                    , coalesce(flattened_weeks.week12, 0) week12
                    , coalesce(flattened_weeks.week13, 0) week13
                    , coalesce(flattened_weeks.week14, 0) week14
                    , coalesce(flattened_weeks.week15, 0) week15
                    , coalesce(flattened_weeks.week16, 0) week16
                    , coalesce(flattened_weeks.week17, 0) week17
                    , coalesce(flattened_weeks.week18, 0) week18
                    , coalesce(flattened_weeks.week19, 0) week19
                    , coalesce(flattened_weeks.week20, 0) week20
                    , coalesce(flattened_weeks.week21, 0) week21
                    , coalesce(flattened_weeks.week22, 0) week22
                    , coalesce(flattened_weeks.week23, 0) week23
                    , coalesce(flattened_weeks.week24, 0) week24
                    , coalesce(flattened_weeks.week25, 0) week25
                    , coalesce(flattened_weeks.week26, 0) week26
                from with_grad_requirements
                    left join flattened_weeks on
                        with_grad_requirements.user_id = flattened_weeks.user_id
                        and with_grad_requirements.cohort_id = flattened_weeks.cohort_id
            )
            select * from with_weeks
            with no data
        |

        add_index :cohort_user_progress_records, [:cohort_id, :user_id], :unique => true
    end

    def version_20170221140313
        execute %Q|
            create materialized view cohort_user_progress_records as
            with with_counts as (
                select
                    cohort_user_lesson_progress_records.cohort_id
                    , cohort_user_lesson_progress_records.cohort_name
                    , cohort_user_lesson_progress_records.user_id
                    , cohort_status_changes.status
                    , sum(case
                            when foundations = true and completed_at is not null then 1
                            when foundations = true and completed_at is null then 0
                            else null
                            end
                        ) as foundations_lessons_complete
                    , sum(case
                            when in_curriculum = true and completed_at is not null then 1
                            when in_curriculum = true and completed_at is null then 0
                            else null
                            end
                        ) as curriculum_lessons_complete
                    , sum(case
                            when test = true and completed_at is not null then 1
                            when test = true and completed_at is null then 0
                            else null
                            end
                        ) as test_lessons_complete
                    , sum(case
                            when elective = true and completed_at is not null then 1
                            when elective = true and completed_at is null then 0
                            else null
                            end
                        ) as elective_lessons_complete
                    , avg(case when assessment then average_assessment_score_first else null end) as average_assessment_score_first
                    , avg(case when assessment then average_assessment_score_best else null end) as average_assessment_score_best
                    , avg(case when test then average_assessment_score_first else null end) as avg_test_score
                from cohort_applications_plus
                    join cohort_status_changes on
                        cohort_status_changes.cohort_id = cohort_applications_plus.cohort_id
                        and cohort_status_changes.user_id = cohort_applications_plus.user_id
                        and cohort_status_changes.until_time = '01-01-2099'
                    left join cohort_user_lesson_progress_records on
                        cohort_applications_plus.cohort_id = cohort_user_lesson_progress_records.cohort_id
                        and cohort_applications_plus.user_id = cohort_user_lesson_progress_records.user_id
                where
                    cohort_applications_plus.was_accepted=true
                group by
                    cohort_user_lesson_progress_records.cohort_id
                    , cohort_user_lesson_progress_records.cohort_name
                    , cohort_user_lesson_progress_records.user_id
                    , cohort_status_changes.status
            )
            , with_averages as (
                select
                    with_counts.*
                    , case
                        when with_counts.cohort_name='MBA1'
                            then (
                                0.7*(case when avg_test_score is null then 0 else avg_test_score end)
                                + 0.3*(case when average_assessment_score_best is null then 0 else average_assessment_score_best end)
                            )
                        else
                            (
                                0.7*(case when avg_test_score is null then 0 else avg_test_score end)
                                + 0.2*(case when average_assessment_score_best is null then 0 else average_assessment_score_best end)
                                + 0.1*(0) -- placeholder for participation score
                            )
                        end as final_score
                    , foundations_lessons_complete::float / cohort_content_details.foundations_lesson_count as foundations_perc_complete
                    , curriculum_lessons_complete::float / cohort_content_details.in_curriculum_lesson_count as curriculum_perc_complete
                    , test_lessons_complete::float / cohort_content_details.test_lesson_count as test_perc_complete
                    , elective_lessons_complete::float / cohort_content_details.elective_lesson_count as elective_perc_complete
                from with_counts
                    join cohort_content_details on with_counts.cohort_id = cohort_content_details.cohort_id
            )
            , with_grad_requirements as (
                select
                    with_averages.*
                    , final_score > 0.7 and curriculum_perc_complete = 1 as meets_graduation_requirements
                from with_averages
            )
            , flattened_weeks as (
                SELECT
                    cohort_id
                    , user_id

                    -- I don't understand why the summing and grouping is necessary.  But the
                    -- crosstab sometimes returns an extra row with 0s in every column (maybe when
                    -- as user is in multiple cohortS?). This gets around that
                    , sum(week0) week0
                    , sum(week1) week1
                    , sum(week2) week2
                    , sum(week3) week3
                    , sum(week4) week4
                    , sum(week5) week5
                    , sum(week6) week6
                    , sum(week7) week7
                    , sum(week8) week8
                    , sum(week9) week9
                    , sum(week10) week10
                    , sum(week11) week11
                    , sum(week12) week12
                    , sum(week13) week13
                    , sum(week14) week14
                    , sum(week15) week15
                    , sum(week16) week16
                    , sum(week17) week17
                    , sum(week18) week18
                    , sum(week19) week19
                    , sum(week20) week20
                    , sum(week21) week21
                    , sum(week22) week22
                    , sum(week23) week23
                    , sum(week24) week24
                    , sum(week25) week25
                    , sum(week26) week26
                FROM crosstab(
                  $$SELECT cohort_id, user_id, rn, lessons_completed_in_week
                     FROM  (
                        select
                            cohort_user_weeks.cohort_id
                            , cohort_user_weeks.user_id
                            , lessons_completed_in_week
                            , row_number() OVER (PARTITION BY cohort_user_weeks.cohort_id, cohort_user_weeks.user_id
                                           ORDER BY cohort_user_weeks.week_index ASC NULLS LAST) AS rn
                        from cohort_user_weeks
                        order by
                            cohort_user_weeks.cohort_id
                            , cohort_user_weeks.user_id
                            , week_index
                        ) sub
                     ORDER  BY user_id
                  $$
                  , 'VALUES (1),(2),(3),(4),(5),(6),(7),(8),(9),(10),(11),(12),(13),(14),(15),(16),(17),(18),(19),(20),(21),(22),(23),(24),(25),(26),(27)'
                  ) AS t (
                    cohort_id uuid
                    , user_id uuid
                    , week0 int
                    , week1 int
                    , week2 int
                    , week3 int
                    , week4 int
                    , week5 int
                    , week6 int
                    , week7 int
                    , week8 int
                    , week9 int
                    , week10 int
                    , week11 int
                    , week12 int
                    , week13 int
                    , week14 int
                    , week15 int
                    , week16 int
                    , week17 int
                    , week18 int
                    , week19 int
                    , week20 int
                    , week21 int
                    , week22 int
                    , week23 int
                    , week24 int
                    , week25 int
                    , week26 int
                  )
                  group by
                    cohort_id
                    , user_id
            )
            , with_weeks as (
                select
                    with_grad_requirements.*
                    , coalesce(flattened_weeks.week0, 0) week0
                    , coalesce(flattened_weeks.week1, 0) week1
                    , coalesce(flattened_weeks.week2, 0) week2
                    , coalesce(flattened_weeks.week3, 0) week3
                    , coalesce(flattened_weeks.week4, 0) week4
                    , coalesce(flattened_weeks.week5, 0) week5
                    , coalesce(flattened_weeks.week6, 0) week6
                    , coalesce(flattened_weeks.week7, 0) week7
                    , coalesce(flattened_weeks.week8, 0) week8
                    , coalesce(flattened_weeks.week9, 0) week9
                    , coalesce(flattened_weeks.week10, 0) week10
                    , coalesce(flattened_weeks.week11, 0) week11
                    , coalesce(flattened_weeks.week12, 0) week12
                    , coalesce(flattened_weeks.week13, 0) week13
                    , coalesce(flattened_weeks.week14, 0) week14
                    , coalesce(flattened_weeks.week15, 0) week15
                    , coalesce(flattened_weeks.week16, 0) week16
                    , coalesce(flattened_weeks.week17, 0) week17
                    , coalesce(flattened_weeks.week18, 0) week18
                    , coalesce(flattened_weeks.week19, 0) week19
                    , coalesce(flattened_weeks.week20, 0) week20
                    , coalesce(flattened_weeks.week21, 0) week21
                    , coalesce(flattened_weeks.week22, 0) week22
                    , coalesce(flattened_weeks.week23, 0) week23
                    , coalesce(flattened_weeks.week24, 0) week24
                    , coalesce(flattened_weeks.week25, 0) week25
                    , coalesce(flattened_weeks.week26, 0) week26
                from with_grad_requirements
                    left join flattened_weeks on
                        with_grad_requirements.user_id = flattened_weeks.user_id
                        and with_grad_requirements.cohort_id = flattened_weeks.cohort_id
            )
            select * from with_weeks
            with no data
        |

        add_index :cohort_user_progress_records, [:cohort_id, :user_id], :unique => true
    end

end