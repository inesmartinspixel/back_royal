class ViewHelpers::PublishedCohortPeriods < ViewHelpers::ViewHelperBase
    # -*- SkipSchemaAnnotations

    def version_20200609141113
        # do nothing. converted to table
    end

    # add learner_project_ids
    def version_20181203030126
        execute %Q~
            create materialized view published_cohort_periods as
            WITH period_definitions AS (
                SELECT
                      published_cohorts.id        AS cohort_id
                    , published_cohorts.name
                    , cohorts_versions.start_date AS cohort_start_date
                    , periods.definition
                    , periods.index
                FROM
                    published_cohorts
                    JOIN cohorts_versions ON cohorts_versions.version_id = published_cohorts.version_id
                    , unnest(cohorts_versions.periods) WITH ORDINALITY periods(definition, index)
            )
            , with_period_details AS (
                SELECT
                    cohort_id
                    , name
                    , index
                    , definition ->> 'title'                                                          AS title
                    , definition ->> 'style'                                                          AS style
                    , (definition ->> 'days') :: INTEGER                                              AS days
                    , (definition ->> 'days_offset') :: INTEGER                                       AS days_offset
                    , (definition ->>
                        'additional_specialization_playlists_to_complete' ) :: INTEGER                AS additional_specialization_playlists_to_complete
                    , ARRAY(SELECT elem :: JSON
                            FROM json_array_elements((definition -> 'stream_entries')) elem) AS stream_entries
                    , ARRAY(SELECT elem :: JSON
                            FROM json_array_elements((definition -> 'exercises')) elem) AS exercises
                    , ARRAY(SELECT elem :: JSON
                            FROM json_array_elements((definition -> 'actions')) elem) AS actions
                    , ARRAY(SELECT elem :: uuid
                            FROM json_array_elements_text((definition -> 'learner_project_ids')) elem) AS learner_project_ids
                    --, ARRAY(json_array_elements_text((definition -> 'learner_project_ids'))) as learner_project_ids
                    , cohort_start_date
                FROM period_definitions

                UNION all
                    SELECT
                        published_cohorts.id as cohort_id
                        , published_cohorts.name
                        , 0 as index
                        , 'Period 0' as title
                        , null as style
                        , null as days
                        , null as days_offset
                        , null as additional_specialization_playlists_to_complete
                        , null as stream_entries
                        , null as exercises
                        , null as actions
                        , Array[]::uuid[] as learner_project_ids
                        , start_date as cohort_start_date
                FROM published_cohorts
            )
            , time_info AS (
                SELECT
                    this_periods.cohort_id
                    , this_periods.index
                    , coalesce(sum(previous_periods.days + previous_periods.days_offset), 0) +
                      this_periods.days_offset offset_from_cohort_start
                FROM
                    with_period_details this_periods
                    LEFT JOIN with_period_details previous_periods
                        ON this_periods.cohort_id = previous_periods.cohort_id
                           AND this_periods.index > previous_periods.index
                GROUP BY
                    this_periods.cohort_id
                    , this_periods.index
                    , this_periods.days_offset
            )
            , with_time_info AS (
                SELECT
                    with_period_details.*

                    -- for period, start before the beginning of pedago
                    , case when with_period_details.index = 0 then '2000/01/01'
                        -- add_dst_aware_offset is a custom function we've added in a migration
                        else add_dst_aware_offset(cohort_start_date, offset_from_cohort_start, 'days')
                      end AS period_start

                    -- for period 0, end at the start of period 1
                    , case when with_period_details.index = 0
                        then (
                            select
                                -- add_dst_aware_offset is a custom function we've added in a migration
                                add_dst_aware_offset(with_period_details.cohort_start_date, first_periods.offset_from_cohort_start, 'days')
                            from time_info first_periods
                            where
                                first_periods.cohort_id = with_period_details.cohort_id
                                and first_periods.index = 1
                            )
                        -- add_dst_aware_offset is a custom function we've added in a migration
                        ELSE add_dst_aware_offset(cohort_start_date, offset_from_cohort_start + days, 'days')
                        end AS period_end
                FROM
                    with_period_details
                    JOIN time_info
                        ON with_period_details.cohort_id = time_info.cohort_id
                           AND with_period_details.index = time_info.index
            )

            -- For each cohort, create an extra period for each week in between
            -- the end date and the graduation date.
            , extra_periods as (
                select
                    published_cohorts.id as cohort_id
                    , published_cohorts.name as name
                    , (select max(index) from with_time_info where with_time_info.cohort_id = published_cohorts.id) + 1 + extra_week_index as index
                    , 'After Cohort End ' || (1+extra_week_index)::text as title
                    , 'after_cohort_end' as style
                    , 7 as days
                    , 0 as days_offset
                    , 0 as additional_specialization_playlists_to_complete
                    , null::json[] as stream_entries
                    , null::json[] as exercises
                    , null::json[] as actions
                    , null::uuid[] as learner_project_ids
                    , published_cohorts.start_date as cohort_start_date
                    , published_cohorts.end_date + (extra_week_index || ' weeks')::interval as period_start
                    , published_cohorts.end_date + ((extra_week_index+1) || ' weeks')::interval as period_end
                from published_cohorts
                    join cohorts_versions
                        on cohorts_versions.version_id = published_cohorts.version_id

                    -- Join each row with the indexes 0..12.  12 is arbitrarily
                    -- selected since it is probably more than the number of weeks we would
                    -- ever have after the end date and before the graduation date
                    cross join (
                        SELECT
                            extra_week_index
                        FROM
                            generate_series(0, 12) AS extra_week_index
                    ) extra_weeks
                where

                    -- since 12 weeks is probably too many weeks, we filter out any that starts after the
                    -- graduation date
                    published_cohorts.end_date + (extra_week_index || ' weeks')::interval < published_cohorts.graduation_date
            )
            , with_extra_periods as (
                select * from with_time_info
                union all
                select * from extra_periods
            )
            , with_stream_entries AS (
                SELECT
                    cohort_id
                    , index
                    , period_start
                    , period_end
                    , stream_entries.stream_entry
                    , stream_entries.stream_entry_index
                FROM with_extra_periods
                    , unnest(stream_entries) WITH ORDINALITY stream_entries(stream_entry, stream_entry_index)
            )
            , stream_pack_ids AS (
                SELECT
                    cohort_id
                    , index
                    , array_remove(
                          array_agg(
                              CASE
                              WHEN (stream_entry ->> 'required') :: BOOLEAN = TRUE
                                  THEN (stream_entry ->> 'locale_pack_id') :: UUID
                              ELSE NULL
                              END
                          )
                          , NULL
                      ) AS required_stream_pack_ids
                    , array_remove(
                          array_agg(
                              CASE
                              WHEN (stream_entry ->> 'required') :: BOOLEAN = FALSE
                                  THEN (stream_entry ->> 'locale_pack_id') :: UUID
                              ELSE NULL
                              END
                          )
                          , NULL
                      ) AS optional_stream_pack_ids
                FROM with_stream_entries
                GROUP BY cohort_id, index
            )
            , with_pack_ids AS (
                SELECT
                    with_extra_periods.*
                    , stream_pack_ids.required_stream_pack_ids
                    , stream_pack_ids.optional_stream_pack_ids
                    , array(
                          SELECT lesson_locale_pack_id
                          FROM
                              published_stream_lesson_locale_packs
                          WHERE stream_locale_pack_id = ANY (required_stream_pack_ids)
                      ) required_lesson_pack_ids
                FROM with_extra_periods
                    left JOIN stream_pack_ids
                        ON with_extra_periods.cohort_id = stream_pack_ids.cohort_id
                           AND with_extra_periods.index = stream_pack_ids.index
            )
            , expected_specializations_complete_as_integer as (
                SELECT
                    with_pack_ids.cohort_id
                    , with_pack_ids.index
                    , with_pack_ids.style
                    , with_pack_ids.additional_specialization_playlists_to_complete
                    , sum(previous_periods.additional_specialization_playlists_to_complete) expected_specializations_complete
                from with_pack_ids
                    join with_pack_ids previous_periods
                        on with_pack_ids.cohort_id = previous_periods.cohort_id
                        and with_pack_ids.index >= previous_periods.index
                group by
                    with_pack_ids.cohort_id
                    , with_pack_ids.index
                    , with_pack_ids.style
                    , with_pack_ids.additional_specialization_playlists_to_complete
            )
            , expected_specializations_complete_as_float as (
                SELECT
                    this_period.cohort_id
                    , this_period.index

                    -- This is a complicated calculation.  We are trying to get a float for each period
                    -- indicating how many specializations should be complete.  For example, if there are
                    -- 2 specialization periods in a row, and the second one has additional_specialization_playlists_to_complete=1,
                    -- then the first one should require 0.5 specializations, and the second one should require 1

                    -- take the total number of specializations that should be totally complete
                    , this_period.expected_specializations_complete +

                        -- figure out how many other specialization periods exist since the last one that has
                        -- additional_specialization_playlists_to_complete > 0 and the next one.  Come up with
                        -- a number between 0 and 1 by dividing the number of such specialization periods before
                        -- this one (inclusive) by the total number of such periods.
                        case when count(other_specialization_periods.index) = 0 or this_period.additional_specialization_playlists_to_complete > 0
                            then 0
                            else
                                count(case when
                                        other_specialization_periods.expected_specializations_complete = this_period.expected_specializations_complete
                                        and other_specialization_periods.index <= this_period.index
                                        and other_specialization_periods.additional_specialization_playlists_to_complete = 0
                                    then true else null end)::float
                                /
                                (count(case when
                                        other_specialization_periods.expected_specializations_complete = this_period.expected_specializations_complete
                                        and other_specialization_periods.additional_specialization_playlists_to_complete = 0
                                    then true else null end)::float + 1)
                            end
                    as expected_specializations_complete
                FROM
                    expected_specializations_complete_as_integer this_period
                    left join expected_specializations_complete_as_integer other_specialization_periods
                        on this_period.cohort_id = other_specialization_periods.cohort_id
                        and other_specialization_periods.style = 'specialization'
                group by
                    this_period.cohort_id
                    , this_period.index
                    , this_period.expected_specializations_complete
                    , this_period.additional_specialization_playlists_to_complete
            )
            , expected_counts AS (
                SELECT
                    periods.cohort_id
                    , periods.index
                    , array_sort_unique(
                          array_cat_agg(previous_periods.required_stream_pack_ids))     AS cumulative_required_stream_pack_ids
                    , array_sort_unique(array_cat_agg(
                                            previous_periods.required_lesson_pack_ids)) AS cumulative_required_lesson_pack_ids
                    , expected_specializations_complete_as_float.expected_specializations_complete
                FROM
                    with_pack_ids periods
                    JOIN with_pack_ids previous_periods
                        ON periods.cohort_id = previous_periods.cohort_id
                           AND periods.index >= previous_periods.index
                    join expected_specializations_complete_as_float
                        on periods.cohort_id = expected_specializations_complete_as_float.cohort_id
                        and periods.index = expected_specializations_complete_as_float.index
                    join published_cohorts
                        on periods.cohort_id = published_cohorts.id

                    -- num_required_specializations is not in published_cohorts,
                    -- so we need to join against the versions
                    join cohorts_versions
                        on published_cohorts.version_id = cohorts_versions.version_id
                GROUP BY
                    periods.cohort_id
                    , periods.index
                    , expected_specializations_complete_as_float.expected_specializations_complete
                    , num_required_specializations
            )
            , with_expected_counts_and_time_info AS (
                SELECT
                    with_pack_ids.*
                    , expected_counts.cumulative_required_stream_pack_ids
                    , expected_counts.cumulative_required_lesson_pack_ids
                    , array_length(expected_counts.cumulative_required_stream_pack_ids,
                                   1) AS cumulative_required_stream_pack_ids_count
                    , array_length(expected_counts.cumulative_required_lesson_pack_ids,
                                   1) AS cumulative_required_lesson_pack_ids_count
                    , expected_counts.expected_specializations_complete
                FROM
                    with_pack_ids
                    JOIN expected_counts
                        ON expected_counts.index = with_pack_ids.index
                           AND expected_counts.cohort_id = with_pack_ids.cohort_id
            )
            SELECT *
            FROM with_expected_counts_and_time_info
            ORDER BY cohort_id, index
        ~

        add_index :published_cohort_periods, [:cohort_id, :index], :unique => true, :name => :coh_id_index_on_publ_coh_periods
    end

    # add extra periods
    def version_20181008182254
        execute %Q~
            create materialized view published_cohort_periods as
            WITH period_definitions AS (
                SELECT
                      published_cohorts.id        AS cohort_id
                    , published_cohorts.name
                    , cohorts_versions.start_date AS cohort_start_date
                    , periods.definition
                    , periods.index
                FROM
                    published_cohorts
                    JOIN cohorts_versions ON cohorts_versions.version_id = published_cohorts.version_id
                    , unnest(cohorts_versions.periods) WITH ORDINALITY periods(definition, index)
            )
            , with_period_details AS (
                SELECT
                    cohort_id
                    , name
                    , index
                    , definition ->> 'title'                                                          AS title
                    , definition ->> 'style'                                                          AS style
                    , (definition ->> 'days') :: INTEGER                                              AS days
                    , (definition ->> 'days_offset') :: INTEGER                                       AS days_offset
                    , (definition ->>
                        'additional_specialization_playlists_to_complete' ) :: INTEGER                AS additional_specialization_playlists_to_complete
                    , ARRAY(SELECT elem :: JSON
                            FROM json_array_elements((definition ->> 'stream_entries') :: JSON) elem) AS stream_entries
                    , ARRAY(SELECT elem :: JSON
                            FROM json_array_elements((definition ->> 'exercises') :: JSON) elem) AS exercises
                    , ARRAY(SELECT elem :: JSON
                            FROM json_array_elements((definition ->> 'actions') :: JSON) elem) AS actions
                    , cohort_start_date
                FROM period_definitions

                UNION all
                    SELECT
                        published_cohorts.id as cohort_id
                        , published_cohorts.name
                        , 0 as index
                        , 'Period 0' as title
                        , null as style
                        , null as days
                        , null as days_offset
                        , null as additional_specialization_playlists_to_complete
                        , null as stream_entries
                        , null as exercises
                        , null as actions
                        , start_date as cohort_start_date
                FROM published_cohorts
            )
            , time_info AS (
                SELECT
                    this_periods.cohort_id
                    , this_periods.index
                    , coalesce(sum(previous_periods.days + previous_periods.days_offset), 0) +
                      this_periods.days_offset offset_from_cohort_start
                FROM
                    with_period_details this_periods
                    LEFT JOIN with_period_details previous_periods
                        ON this_periods.cohort_id = previous_periods.cohort_id
                           AND this_periods.index > previous_periods.index
                GROUP BY
                    this_periods.cohort_id
                    , this_periods.index
                    , this_periods.days_offset
            )
            , with_time_info AS (
                SELECT
                    with_period_details.*

                    -- for period, start before the beginning of pedago
                    , case when with_period_details.index = 0 then '2000/01/01'
                        -- add_dst_aware_offset is a custom function we've added in a migration
                        else add_dst_aware_offset(cohort_start_date, offset_from_cohort_start, 'days')
                      end AS period_start

                    -- for period 0, end at the start of period 1
                    , case when with_period_details.index = 0
                        then (
                            select
                                -- add_dst_aware_offset is a custom function we've added in a migration
                                add_dst_aware_offset(with_period_details.cohort_start_date, first_periods.offset_from_cohort_start, 'days')
                            from time_info first_periods
                            where
                                first_periods.cohort_id = with_period_details.cohort_id
                                and first_periods.index = 1
                            )
                        -- add_dst_aware_offset is a custom function we've added in a migration
                        ELSE add_dst_aware_offset(cohort_start_date, offset_from_cohort_start + days, 'days')
                        end AS period_end
                FROM
                    with_period_details
                    JOIN time_info
                        ON with_period_details.cohort_id = time_info.cohort_id
                           AND with_period_details.index = time_info.index
            )

            -- For each cohort, create an extra period for each week in between
            -- the end date and the graduation date.
            , extra_periods as (
                select
                    published_cohorts.id as cohort_id
                    , published_cohorts.name as name
                    , (select max(index) from with_time_info where with_time_info.cohort_id = published_cohorts.id) + 1 + extra_week_index as index
                    , 'After Cohort End ' || (1+extra_week_index)::text as title
                    , 'after_cohort_end' as style
                    , 7 as days
                    , 0 as days_offset
                    , 0 as additional_specialization_playlists_to_complete
                    , null::json[] as stream_entries
                    , null::json[] as exercises
                    , null::json[] as actions
                    , published_cohorts.start_date as cohort_start_date
                    , published_cohorts.end_date + (extra_week_index || ' weeks')::interval as period_start
                    , published_cohorts.end_date + ((extra_week_index+1) || ' weeks')::interval as period_end
                from published_cohorts
                    join cohorts_versions
                        on cohorts_versions.version_id = published_cohorts.version_id

                    -- Join each row with the indexes 0..12.  12 is arbitrarily
                    -- selected since it is probably more than the number of weeks we would
                    -- ever have after the end date and before the graduation date
                    cross join (
                        SELECT
                            extra_week_index
                        FROM
                            generate_series(0, 12) AS extra_week_index
                    ) extra_weeks
                where

                    -- since 12 weeks is probably too many weeks, we filter out any that starts after the
                    -- graduation date
                    published_cohorts.end_date + (extra_week_index || ' weeks')::interval < published_cohorts.graduation_date
            )
            , with_extra_periods as (
                select * from with_time_info
                union all
                select * from extra_periods
            )
            , with_stream_entries AS (
                SELECT
                    cohort_id
                    , index
                    , period_start
                    , period_end
                    , stream_entries.stream_entry
                    , stream_entries.stream_entry_index
                FROM with_extra_periods
                    , unnest(stream_entries) WITH ORDINALITY stream_entries(stream_entry, stream_entry_index)
            )
            , stream_pack_ids AS (
                SELECT
                    cohort_id
                    , index
                    , array_remove(
                          array_agg(
                              CASE
                              WHEN (stream_entry ->> 'required') :: BOOLEAN = TRUE
                                  THEN (stream_entry ->> 'locale_pack_id') :: UUID
                              ELSE NULL
                              END
                          )
                          , NULL
                      ) AS required_stream_pack_ids
                    , array_remove(
                          array_agg(
                              CASE
                              WHEN (stream_entry ->> 'required') :: BOOLEAN = FALSE
                                  THEN (stream_entry ->> 'locale_pack_id') :: UUID
                              ELSE NULL
                              END
                          )
                          , NULL
                      ) AS optional_stream_pack_ids
                FROM with_stream_entries
                GROUP BY cohort_id, index
            )
            , with_pack_ids AS (
                SELECT
                    with_extra_periods.*
                    , stream_pack_ids.required_stream_pack_ids
                    , stream_pack_ids.optional_stream_pack_ids
                    , array(
                          SELECT lesson_locale_pack_id
                          FROM
                              published_stream_lesson_locale_packs
                          WHERE stream_locale_pack_id = ANY (required_stream_pack_ids)
                      ) required_lesson_pack_ids
                FROM with_extra_periods
                    left JOIN stream_pack_ids
                        ON with_extra_periods.cohort_id = stream_pack_ids.cohort_id
                           AND with_extra_periods.index = stream_pack_ids.index
            )
            , expected_specializations_complete_as_integer as (
                SELECT
                    with_pack_ids.cohort_id
                    , with_pack_ids.index
                    , with_pack_ids.style
                    , with_pack_ids.additional_specialization_playlists_to_complete
                    , sum(previous_periods.additional_specialization_playlists_to_complete) expected_specializations_complete
                from with_pack_ids
                    join with_pack_ids previous_periods
                        on with_pack_ids.cohort_id = previous_periods.cohort_id
                        and with_pack_ids.index >= previous_periods.index
                group by
                    with_pack_ids.cohort_id
                    , with_pack_ids.index
                    , with_pack_ids.style
                    , with_pack_ids.additional_specialization_playlists_to_complete
            )
            , expected_specializations_complete_as_float as (
                SELECT
                    this_period.cohort_id
                    , this_period.index

                    -- This is a complicated calculation.  We are trying to get a float for each period
                    -- indicating how many specializations should be complete.  For example, if there are
                    -- 2 specialization periods in a row, and the second one has additional_specialization_playlists_to_complete=1,
                    -- then the first one should require 0.5 specializations, and the second one should require 1

                    -- take the total number of specializations that should be totally complete
                    , this_period.expected_specializations_complete +

                        -- figure out how many other specialization periods exist since the last one that has
                        -- additional_specialization_playlists_to_complete > 0 and the next one.  Come up with
                        -- a number between 0 and 1 by dividing the number of such specialization periods before
                        -- this one (inclusive) by the total number of such periods.
                        case when count(other_specialization_periods.index) = 0 or this_period.additional_specialization_playlists_to_complete > 0
                            then 0
                            else
                                count(case when
                                        other_specialization_periods.expected_specializations_complete = this_period.expected_specializations_complete
                                        and other_specialization_periods.index <= this_period.index
                                        and other_specialization_periods.additional_specialization_playlists_to_complete = 0
                                    then true else null end)::float
                                /
                                (count(case when
                                        other_specialization_periods.expected_specializations_complete = this_period.expected_specializations_complete
                                        and other_specialization_periods.additional_specialization_playlists_to_complete = 0
                                    then true else null end)::float + 1)
                            end
                    as expected_specializations_complete
                FROM
                    expected_specializations_complete_as_integer this_period
                    left join expected_specializations_complete_as_integer other_specialization_periods
                        on this_period.cohort_id = other_specialization_periods.cohort_id
                        and other_specialization_periods.style = 'specialization'
                group by
                    this_period.cohort_id
                    , this_period.index
                    , this_period.expected_specializations_complete
                    , this_period.additional_specialization_playlists_to_complete
            )
            , expected_counts AS (
                SELECT
                    periods.cohort_id
                    , periods.index
                    , array_sort_unique(
                          array_cat_agg(previous_periods.required_stream_pack_ids))     AS cumulative_required_stream_pack_ids
                    , array_sort_unique(array_cat_agg(
                                            previous_periods.required_lesson_pack_ids)) AS cumulative_required_lesson_pack_ids
                    , expected_specializations_complete_as_float.expected_specializations_complete
                FROM
                    with_pack_ids periods
                    JOIN with_pack_ids previous_periods
                        ON periods.cohort_id = previous_periods.cohort_id
                           AND periods.index >= previous_periods.index
                    join expected_specializations_complete_as_float
                        on periods.cohort_id = expected_specializations_complete_as_float.cohort_id
                        and periods.index = expected_specializations_complete_as_float.index
                    join published_cohorts
                        on periods.cohort_id = published_cohorts.id

                    -- num_required_specializations is not in published_cohorts,
                    -- so we need to join against the versions
                    join cohorts_versions
                        on published_cohorts.version_id = cohorts_versions.version_id
                GROUP BY
                    periods.cohort_id
                    , periods.index
                    , expected_specializations_complete_as_float.expected_specializations_complete
                    , num_required_specializations
            )
            , with_expected_counts_and_time_info AS (
                SELECT
                    with_pack_ids.*
                    , expected_counts.cumulative_required_stream_pack_ids
                    , expected_counts.cumulative_required_lesson_pack_ids
                    , array_length(expected_counts.cumulative_required_stream_pack_ids,
                                   1) AS cumulative_required_stream_pack_ids_count
                    , array_length(expected_counts.cumulative_required_lesson_pack_ids,
                                   1) AS cumulative_required_lesson_pack_ids_count
                    , expected_counts.expected_specializations_complete
                FROM
                    with_pack_ids
                    JOIN expected_counts
                        ON expected_counts.index = with_pack_ids.index
                           AND expected_counts.cohort_id = with_pack_ids.cohort_id
            )
            SELECT *
            FROM with_expected_counts_and_time_info
            ORDER BY cohort_id, index
        ~

        add_index :published_cohort_periods, [:cohort_id, :index], :unique => true, :name => :coh_id_index_on_publ_coh_periods
    end

    # added specialization_expected_perc_complete
    def version_20180911132527
        execute %Q~
            create materialized view published_cohort_periods as
            WITH period_definitions AS (
                SELECT
                      published_cohorts.id        AS cohort_id
                    , published_cohorts.name
                    , cohorts_versions.start_date AS cohort_start_date
                    , periods.definition
                    , periods.index
                FROM
                    published_cohorts
                    JOIN cohorts_versions ON cohorts_versions.version_id = published_cohorts.version_id
                    , unnest(cohorts_versions.periods) WITH ORDINALITY periods(definition, index)
            )
            , with_period_details AS (
                SELECT
                    cohort_id
                    , name
                    , index
                    , definition ->> 'title'                                                          AS title
                    , definition ->> 'style'                                                          AS style
                    , (definition ->> 'days') :: INTEGER                                              AS days
                    , (definition ->> 'days_offset') :: INTEGER                                       AS days_offset
                    , (definition ->>
                        'additional_specialization_playlists_to_complete' ) :: INTEGER                AS additional_specialization_playlists_to_complete
                    , ARRAY(SELECT elem :: JSON
                            FROM json_array_elements((definition ->> 'stream_entries') :: JSON) elem) AS stream_entries
                    , ARRAY(SELECT elem :: JSON
                            FROM json_array_elements((definition ->> 'exercises') :: JSON) elem) AS exercises
                    , ARRAY(SELECT elem :: JSON
                            FROM json_array_elements((definition ->> 'actions') :: JSON) elem) AS actions
                    , cohort_start_date
                FROM period_definitions

                UNION all
                    SELECT
                        published_cohorts.id as cohort_id
                        , published_cohorts.name
                        , 0 as index
                        , 'Period 0' as title
                        , null as style
                        , null as days
                        , null as days_offset
                        , null as additional_specialization_playlists_to_complete
                        , null as stream_entries
                        , null as exercises
                        , null as actions
                        , start_date as cohort_start_date
                FROM published_cohorts
            )
            , with_stream_entries AS (
                SELECT
                    cohort_id
                    , index
                    , stream_entries.stream_entry
                    , stream_entries.stream_entry_index
                FROM with_period_details
                    , unnest(stream_entries) WITH ORDINALITY stream_entries(stream_entry, stream_entry_index)
            )
            , stream_pack_ids AS (
                SELECT
                    cohort_id
                    , index
                    , array_remove(
                          array_agg(
                              CASE
                              WHEN (stream_entry ->> 'required') :: BOOLEAN = TRUE
                                  THEN (stream_entry ->> 'locale_pack_id') :: UUID
                              ELSE NULL
                              END
                          )
                          , NULL
                      ) AS required_stream_pack_ids
                    , array_remove(
                          array_agg(
                              CASE
                              WHEN (stream_entry ->> 'required') :: BOOLEAN = FALSE
                                  THEN (stream_entry ->> 'locale_pack_id') :: UUID
                              ELSE NULL
                              END
                          )
                          , NULL
                      ) AS optional_stream_pack_ids
                FROM with_stream_entries
                GROUP BY cohort_id, index
            )
            , with_pack_ids AS (
                SELECT
                    with_period_details.*
                    , stream_pack_ids.required_stream_pack_ids
                    , stream_pack_ids.optional_stream_pack_ids
                    , array(
                          SELECT lesson_locale_pack_id
                          FROM
                              published_stream_lesson_locale_packs
                          WHERE stream_locale_pack_id = ANY (required_stream_pack_ids)
                      ) required_lesson_pack_ids
                FROM with_period_details
                    left JOIN stream_pack_ids
                        ON with_period_details.cohort_id = stream_pack_ids.cohort_id
                           AND with_period_details.index = stream_pack_ids.index
            )
            , expected_specializations_complete_as_integer as (
                SELECT
                    with_period_details.cohort_id
                    , with_period_details.index
                    , with_period_details.style
                    , with_period_details.additional_specialization_playlists_to_complete
                    , sum(previous_periods.additional_specialization_playlists_to_complete) expected_specializations_complete
                from with_period_details
                    join with_period_details previous_periods
                        on with_period_details.cohort_id = previous_periods.cohort_id
                        and with_period_details.index >= previous_periods.index
                group by
                    with_period_details.cohort_id
                    , with_period_details.index
                    , with_period_details.style
                    , with_period_details.additional_specialization_playlists_to_complete
            )
            , expected_specializations_complete_as_float as (
                SELECT
                    this_period.cohort_id
                    , this_period.index

                    -- This is a complicated calculation.  We are trying to get a float for each period
                    -- indicating how many specializations should be complete.  For example, if there are
                    -- 2 specialization periods in a row, and the second one has additional_specialization_playlists_to_complete=1,
                    -- then the first one should require 0.5 specializations, and the second one should require 1

                    -- take the total number of specializations that should be totally complete
                    , this_period.expected_specializations_complete +

                        -- figure out how many other specialization periods exist since the last one that has
                        -- additional_specialization_playlists_to_complete > 0 and the next one.  Come up with
                        -- a number between 0 and 1 by dividing the number of such specialization periods before
                        -- this one (inclusive) by the total number of such periods.
                        case when count(other_specialization_periods.index) = 0 or this_period.additional_specialization_playlists_to_complete > 0
                            then 0
                            else
                                count(case when
                                        other_specialization_periods.expected_specializations_complete = this_period.expected_specializations_complete
                                        and other_specialization_periods.index <= this_period.index
                                        and other_specialization_periods.additional_specialization_playlists_to_complete = 0
                                    then true else null end)::float
                                /
                                (count(case when
                                        other_specialization_periods.expected_specializations_complete = this_period.expected_specializations_complete
                                        and other_specialization_periods.additional_specialization_playlists_to_complete = 0
                                    then true else null end)::float + 1)
                            end
                    as expected_specializations_complete
                FROM
                    expected_specializations_complete_as_integer this_period
                    left join expected_specializations_complete_as_integer other_specialization_periods
                        on this_period.cohort_id = other_specialization_periods.cohort_id
                        and other_specialization_periods.style = 'specialization'
                group by
                    this_period.cohort_id
                    , this_period.index
                    , this_period.expected_specializations_complete
                    , this_period.additional_specialization_playlists_to_complete
            )
            , expected_counts AS (
                SELECT
                    periods.cohort_id
                    , periods.index
                    , array_sort_unique(
                          array_cat_agg(previous_periods.required_stream_pack_ids))     AS cumulative_required_stream_pack_ids
                    , array_sort_unique(array_cat_agg(
                                            previous_periods.required_lesson_pack_ids)) AS cumulative_required_lesson_pack_ids
                    , expected_specializations_complete_as_float.expected_specializations_complete
                FROM
                    with_pack_ids periods
                    JOIN with_pack_ids previous_periods
                        ON periods.cohort_id = previous_periods.cohort_id
                           AND periods.index >= previous_periods.index
                    join expected_specializations_complete_as_float
                        on periods.cohort_id = expected_specializations_complete_as_float.cohort_id
                        and periods.index = expected_specializations_complete_as_float.index
                    join published_cohorts
                        on periods.cohort_id = published_cohorts.id

                    -- num_required_specializations is not in published_cohorts,
                    -- so we need to join against the versions
                    join cohorts_versions
                        on published_cohorts.version_id = cohorts_versions.version_id
                GROUP BY
                    periods.cohort_id
                    , periods.index
                    , expected_specializations_complete_as_float.expected_specializations_complete
                    , num_required_specializations
            )
            , time_info AS (
                SELECT
                    this_periods.cohort_id
                    , this_periods.index
                    , coalesce(sum(previous_periods.days + previous_periods.days_offset), 0) +
                      this_periods.days_offset offset_from_cohort_start
                FROM
                    with_period_details this_periods
                    LEFT JOIN with_period_details previous_periods
                        ON this_periods.cohort_id = previous_periods.cohort_id
                           AND this_periods.index > previous_periods.index
                GROUP BY
                    this_periods.cohort_id
                    , this_periods.index
                    , this_periods.days_offset
            )
            , with_time_info AS (
                SELECT
                    with_pack_ids.*

                    -- for period, start before the beginning of pedago
                    , case when with_pack_ids.index = 0 then '2000/01/01'
                        -- add_dst_aware_offset is a custom function we've added in a migration
                        else add_dst_aware_offset(cohort_start_date, offset_from_cohort_start, 'days')
                      end AS period_start

                    -- for period 0, end at the start of period 1
                    , case when with_pack_ids.index = 0
                        then (
                            select
                                -- add_dst_aware_offset is a custom function we've added in a migration
                                add_dst_aware_offset(with_pack_ids.cohort_start_date, first_periods.offset_from_cohort_start, 'days')
                            from time_info first_periods
                            where
                                first_periods.cohort_id = with_pack_ids.cohort_id
                                and first_periods.index = 1
                            )
                        -- add_dst_aware_offset is a custom function we've added in a migration
                        ELSE add_dst_aware_offset(cohort_start_date, offset_from_cohort_start + days, 'days')
                        end AS period_end
                FROM
                    with_pack_ids
                    JOIN time_info
                        ON with_pack_ids.cohort_id = time_info.cohort_id
                           AND with_pack_ids.index = time_info.index
            )
            , with_expected_counts_and_time_info AS (
                SELECT
                    with_time_info.*
                    , expected_counts.cumulative_required_stream_pack_ids
                    , expected_counts.cumulative_required_lesson_pack_ids
                    , array_length(expected_counts.cumulative_required_stream_pack_ids,
                                   1) AS cumulative_required_stream_pack_ids_count
                    , array_length(expected_counts.cumulative_required_lesson_pack_ids,
                                   1) AS cumulative_required_lesson_pack_ids_count
                    , expected_counts.expected_specializations_complete
                FROM
                    with_time_info
                    JOIN expected_counts
                        ON expected_counts.index = with_time_info.index
                           AND expected_counts.cohort_id = with_time_info.cohort_id
                    JOIN time_info
                        ON time_info.index = with_time_info.index
                           AND time_info.cohort_id = with_time_info.cohort_id
            )
            SELECT *
            FROM with_expected_counts_and_time_info
            ORDER BY cohort_id, index
        ~

        add_index :published_cohort_periods, [:cohort_id, :index], :unique => true, :name => :coh_id_index_on_publ_coh_periods
    end

    # added period exercises and actions
    def version_20180728183452
        execute %Q~
            create materialized view published_cohort_periods as
            WITH period_definitions AS (
                SELECT
                      published_cohorts.id        AS cohort_id
                    , published_cohorts.name
                    , cohorts_versions.start_date AS cohort_start_date
                    , periods.definition
                    , periods.index
                FROM
                    published_cohorts
                    JOIN cohorts_versions ON cohorts_versions.version_id = published_cohorts.version_id
                    , unnest(cohorts_versions.periods) WITH ORDINALITY periods(definition, index)
            )
                , with_period_details AS (
                SELECT
                    cohort_id
                    , name
                    , index
                    , definition ->> 'title'                                                          AS title
                    , definition ->> 'style'                                                          AS style
                    , (definition ->> 'days') :: INTEGER                                              AS days
                    , (definition ->> 'days_offset') :: INTEGER                                       AS days_offset
                    , definition ->>
                      'additional_specialization_playlists_to_complete'                               AS additional_specialization_playlists_to_complete
                    , ARRAY(SELECT elem :: JSON
                            FROM json_array_elements((definition ->> 'stream_entries') :: JSON) elem) AS stream_entries
                    , ARRAY(SELECT elem :: JSON
                            FROM json_array_elements((definition ->> 'exercises') :: JSON) elem) AS exercises
                    , ARRAY(SELECT elem :: JSON
                            FROM json_array_elements((definition ->> 'actions') :: JSON) elem) AS actions
                    , cohort_start_date
                FROM period_definitions

                UNION all
                    SELECT
                        published_cohorts.id as cohort_id
                        , published_cohorts.name
                        , 0 as index
                        , 'Period 0' as title
                        , null as style
                        , null as days
                        , null as days_offset
                        , null as  additional_specialization_playlists_to_complete
                        , null as stream_entries
                        , null as exercises
                        , null as actions
                        , start_date as cohort_start_date
                FROM published_cohorts
            )
                , with_stream_entries AS (
                SELECT
                    cohort_id
                    , index
                    , stream_entries.stream_entry
                    , stream_entries.stream_entry_index
                FROM with_period_details
                    , unnest(stream_entries) WITH ORDINALITY stream_entries(stream_entry, stream_entry_index)
            )
                , stream_pack_ids AS (
                SELECT
                    cohort_id
                    , index
                    , array_remove(
                          array_agg(
                              CASE
                              WHEN (stream_entry ->> 'required') :: BOOLEAN = TRUE
                                  THEN (stream_entry ->> 'locale_pack_id') :: UUID
                              ELSE NULL
                              END
                          )
                          , NULL
                      ) AS required_stream_pack_ids
                    , array_remove(
                          array_agg(
                              CASE
                              WHEN (stream_entry ->> 'required') :: BOOLEAN = FALSE
                                  THEN (stream_entry ->> 'locale_pack_id') :: UUID
                              ELSE NULL
                              END
                          )
                          , NULL
                      ) AS optional_stream_pack_ids
                FROM with_stream_entries
                GROUP BY cohort_id, index
            )
                , with_pack_ids AS (
                SELECT
                    with_period_details.*
                    , stream_pack_ids.required_stream_pack_ids
                    , stream_pack_ids.optional_stream_pack_ids
                    , array(
                          SELECT lesson_locale_pack_id
                          FROM
                              published_stream_lesson_locale_packs
                          WHERE stream_locale_pack_id = ANY (required_stream_pack_ids)
                      ) required_lesson_pack_ids
                FROM with_period_details
                    left JOIN stream_pack_ids
                        ON with_period_details.cohort_id = stream_pack_ids.cohort_id
                           AND with_period_details.index = stream_pack_ids.index
            )
                , expected_counts AS (
                SELECT
                    periods.cohort_id
                    , periods.index
                    , array_sort_unique(
                          array_cat_agg(previous_periods.required_stream_pack_ids))     AS cumulative_required_stream_pack_ids
                    , array_sort_unique(array_cat_agg(
                                            previous_periods.required_lesson_pack_ids)) AS cumulative_required_lesson_pack_ids
                FROM
                    with_pack_ids periods
                    JOIN with_pack_ids previous_periods
                        ON periods.cohort_id = previous_periods.cohort_id
                           AND periods.index >= previous_periods.index
                GROUP BY
                    periods.cohort_id
                    , periods.index
            )

                , time_info AS (
                SELECT
                    this_periods.cohort_id
                    , this_periods.index
                    , coalesce(sum(previous_periods.days + previous_periods.days_offset), 0) +
                      this_periods.days_offset offset_from_cohort_start
                FROM
                    with_period_details this_periods
                    LEFT JOIN with_period_details previous_periods
                        ON this_periods.cohort_id = previous_periods.cohort_id
                           AND this_periods.index > previous_periods.index
                GROUP BY
                    this_periods.cohort_id
                    , this_periods.index
                    , this_periods.days_offset
            )
                , with_time_info AS (
                SELECT
                    with_pack_ids.*

                    -- for period, start before the beginning of pedago
                    , case when with_pack_ids.index = 0 then '2000/01/01'
                        -- add_dst_aware_offset is a custom function we've added in a migration
                        else add_dst_aware_offset(cohort_start_date, offset_from_cohort_start, 'days')
                      end AS period_start

                    -- for period 0, end at the start of period 1
                    , case when with_pack_ids.index = 0
                        then (
                            select
                                -- add_dst_aware_offset is a custom function we've added in a migration
                                add_dst_aware_offset(with_pack_ids.cohort_start_date, first_periods.offset_from_cohort_start, 'days')
                            from time_info first_periods
                            where
                                first_periods.cohort_id = with_pack_ids.cohort_id
                                and first_periods.index = 1
                            )
                        -- add_dst_aware_offset is a custom function we've added in a migration
                        ELSE add_dst_aware_offset(cohort_start_date, offset_from_cohort_start + days, 'days')
                        end AS period_end
                FROM
                    with_pack_ids
                    JOIN time_info
                        ON with_pack_ids.cohort_id = time_info.cohort_id
                           AND with_pack_ids.index = time_info.index
            )
                , with_expected_counts_and_time_info AS (
                SELECT
                    with_time_info.*
                    , expected_counts.cumulative_required_stream_pack_ids
                    , expected_counts.cumulative_required_lesson_pack_ids
                    , array_length(expected_counts.cumulative_required_stream_pack_ids,
                                   1) AS cumulative_required_stream_pack_ids_count
                    , array_length(expected_counts.cumulative_required_lesson_pack_ids,
                                   1) AS cumulative_required_lesson_pack_ids_count
                FROM
                    with_time_info
                    JOIN expected_counts
                        ON expected_counts.index = with_time_info.index
                           AND expected_counts.cohort_id = with_time_info.cohort_id
                    JOIN time_info
                        ON time_info.index = with_time_info.index
                           AND time_info.cohort_id = with_time_info.cohort_id
            )
            SELECT *
            FROM with_expected_counts_and_time_info
            ORDER BY cohort_id, index
        ~

        add_index :published_cohort_periods, [:cohort_id, :index], :unique => true, :name => :coh_id_index_on_publ_coh_periods
    end

    # dst aware enrollment_deadline
    def version_20180423140732
        execute %Q~
            create materialized view published_cohort_periods as
            WITH period_definitions AS (
                SELECT
                      published_cohorts.id        AS cohort_id
                    , published_cohorts.name
                    , cohorts_versions.start_date AS cohort_start_date
                    , periods.definition
                    , periods.index
                FROM
                    published_cohorts
                    JOIN cohorts_versions ON cohorts_versions.version_id = published_cohorts.version_id
                    , unnest(cohorts_versions.periods) WITH ORDINALITY periods(definition, index)
            )
                , with_period_details AS (
                SELECT
                    cohort_id
                    , name
                    , index
                    , definition ->> 'title'                                                          AS title
                    , definition ->> 'style'                                                          AS style
                    , (definition ->> 'days') :: INTEGER                                              AS days
                    , (definition ->> 'days_offset') :: INTEGER                                       AS days_offset
                    , definition ->>
                      'additional_specialization_playlists_to_complete'                               AS additional_specialization_playlists_to_complete
                    , ARRAY(SELECT elem :: JSON
                            FROM json_array_elements((definition ->> 'stream_entries') :: JSON) elem) AS stream_entries
                    , cohort_start_date
                FROM period_definitions

                UNION all
                    SELECT
                        published_cohorts.id as cohort_id
                        , published_cohorts.name
                        , 0 as index
                        , 'Period 0' as title
                        , null as style
                        , null as days
                        , null as days_offset
                        , null as  additional_specialization_playlists_to_complete
                        , null as stream_entries
                        , start_date as cohort_start_date
                FROM published_cohorts
            )
                , with_stream_entries AS (
                SELECT
                    cohort_id
                    , index
                    , stream_entries.stream_entry
                    , stream_entries.stream_entry_index
                FROM with_period_details
                    , unnest(stream_entries) WITH ORDINALITY stream_entries(stream_entry, stream_entry_index)
            )
                , stream_pack_ids AS (
                SELECT
                    cohort_id
                    , index
                    , array_remove(
                          array_agg(
                              CASE
                              WHEN (stream_entry ->> 'required') :: BOOLEAN = TRUE
                                  THEN (stream_entry ->> 'locale_pack_id') :: UUID
                              ELSE NULL
                              END
                          )
                          , NULL
                      ) AS required_stream_pack_ids
                    , array_remove(
                          array_agg(
                              CASE
                              WHEN (stream_entry ->> 'required') :: BOOLEAN = FALSE
                                  THEN (stream_entry ->> 'locale_pack_id') :: UUID
                              ELSE NULL
                              END
                          )
                          , NULL
                      ) AS optional_stream_pack_ids
                FROM with_stream_entries
                GROUP BY cohort_id, index
            )
                , with_pack_ids AS (
                SELECT
                    with_period_details.*
                    , stream_pack_ids.required_stream_pack_ids
                    , stream_pack_ids.optional_stream_pack_ids
                    , array(
                          SELECT lesson_locale_pack_id
                          FROM
                              published_stream_lesson_locale_packs
                          WHERE stream_locale_pack_id = ANY (required_stream_pack_ids)
                      ) required_lesson_pack_ids
                FROM with_period_details
                    left JOIN stream_pack_ids
                        ON with_period_details.cohort_id = stream_pack_ids.cohort_id
                           AND with_period_details.index = stream_pack_ids.index
            )
                , expected_counts AS (
                SELECT
                    periods.cohort_id
                    , periods.index
                    , array_sort_unique(
                          array_cat_agg(previous_periods.required_stream_pack_ids))     AS cumulative_required_stream_pack_ids
                    , array_sort_unique(array_cat_agg(
                                            previous_periods.required_lesson_pack_ids)) AS cumulative_required_lesson_pack_ids
                FROM
                    with_pack_ids periods
                    JOIN with_pack_ids previous_periods
                        ON periods.cohort_id = previous_periods.cohort_id
                           AND periods.index >= previous_periods.index
                GROUP BY
                    periods.cohort_id
                    , periods.index
            )

                , time_info AS (
                SELECT
                    this_periods.cohort_id
                    , this_periods.index
                    , coalesce(sum(previous_periods.days + previous_periods.days_offset), 0) +
                      this_periods.days_offset offset_from_cohort_start
                FROM
                    with_period_details this_periods
                    LEFT JOIN with_period_details previous_periods
                        ON this_periods.cohort_id = previous_periods.cohort_id
                           AND this_periods.index > previous_periods.index
                GROUP BY
                    this_periods.cohort_id
                    , this_periods.index
                    , this_periods.days_offset
            )
                , with_time_info AS (
                SELECT
                    with_pack_ids.*

                    -- for period, start before the beginning of pedago
                    , case when with_pack_ids.index = 0 then '2000/01/01'
                        -- add_dst_aware_offset is a custom function we've added in a migration
                        else add_dst_aware_offset(cohort_start_date, offset_from_cohort_start, 'days')
                      end AS period_start

                    -- for period 0, end at the start of period 1
                    , case when with_pack_ids.index = 0
                        then (
                            select
                                -- add_dst_aware_offset is a custom function we've added in a migration
                                add_dst_aware_offset(with_pack_ids.cohort_start_date, first_periods.offset_from_cohort_start, 'days')
                            from time_info first_periods
                            where
                                first_periods.cohort_id = with_pack_ids.cohort_id
                                and first_periods.index = 1
                            )
                        -- add_dst_aware_offset is a custom function we've added in a migration
                        ELSE add_dst_aware_offset(cohort_start_date, offset_from_cohort_start + days, 'days')
                        end AS period_end
                FROM
                    with_pack_ids
                    JOIN time_info
                        ON with_pack_ids.cohort_id = time_info.cohort_id
                           AND with_pack_ids.index = time_info.index
            )
                , with_expected_counts_and_time_info AS (
                SELECT
                    with_time_info.*
                    , expected_counts.cumulative_required_stream_pack_ids
                    , expected_counts.cumulative_required_lesson_pack_ids
                    , array_length(expected_counts.cumulative_required_stream_pack_ids,
                                   1) AS cumulative_required_stream_pack_ids_count
                    , array_length(expected_counts.cumulative_required_lesson_pack_ids,
                                   1) AS cumulative_required_lesson_pack_ids_count
                FROM
                    with_time_info
                    JOIN expected_counts
                        ON expected_counts.index = with_time_info.index
                           AND expected_counts.cohort_id = with_time_info.cohort_id
                    JOIN time_info
                        ON time_info.index = with_time_info.index
                           AND time_info.cohort_id = with_time_info.cohort_id
            )
            SELECT *
            FROM with_expected_counts_and_time_info
            ORDER BY cohort_id, index
        ~
    end

    def version_20170414174516
        execute %Q~
            create materialized view published_cohort_periods as
            WITH period_definitions AS (
                SELECT
                      published_cohorts.id        AS cohort_id
                    , published_cohorts.name
                    , cohorts_versions.start_date AS cohort_start_date
                    , periods.definition
                    , periods.index
                FROM
                    published_cohorts
                    JOIN cohorts_versions ON cohorts_versions.version_id = published_cohorts.version_id
                    , unnest(cohorts_versions.periods) WITH ORDINALITY periods(definition, index)
            )
                , with_period_details AS (
                SELECT
                    cohort_id
                    , name
                    , index
                    , definition ->> 'title'                                                          AS title
                    , definition ->> 'style'                                                          AS style
                    , (definition ->> 'days') :: INTEGER                                              AS days
                    , (definition ->> 'days_offset') :: INTEGER                                       AS days_offset
                    , definition ->>
                      'additional_specialization_playlists_to_complete'                               AS additional_specialization_playlists_to_complete
                    , ARRAY(SELECT elem :: JSON
                            FROM json_array_elements((definition ->> 'stream_entries') :: JSON) elem) AS stream_entries
                    , cohort_start_date
                FROM period_definitions

                UNION all
                    SELECT
                        published_cohorts.id as cohort_id
                        , published_cohorts.name
                        , 0 as index
                        , 'Period 0' as title
                        , null as style
                        , null as days
                        , null as days_offset
                        , null as  additional_specialization_playlists_to_complete
                        , null as stream_entries
                        , start_date as cohort_start_date
                FROM published_cohorts
            )
                , with_stream_entries AS (
                SELECT
                    cohort_id
                    , index
                    , stream_entries.stream_entry
                    , stream_entries.stream_entry_index
                FROM with_period_details
                    , unnest(stream_entries) WITH ORDINALITY stream_entries(stream_entry, stream_entry_index)
            )
                , stream_pack_ids AS (
                SELECT
                    cohort_id
                    , index
                    , array_remove(
                          array_agg(
                              CASE
                              WHEN (stream_entry ->> 'required') :: BOOLEAN = TRUE
                                  THEN (stream_entry ->> 'locale_pack_id') :: UUID
                              ELSE NULL
                              END
                          )
                          , NULL
                      ) AS required_stream_pack_ids
                    , array_remove(
                          array_agg(
                              CASE
                              WHEN (stream_entry ->> 'required') :: BOOLEAN = FALSE
                                  THEN (stream_entry ->> 'locale_pack_id') :: UUID
                              ELSE NULL
                              END
                          )
                          , NULL
                      ) AS optional_stream_pack_ids
                FROM with_stream_entries
                GROUP BY cohort_id, index
            )
                , with_pack_ids AS (
                SELECT
                    with_period_details.*
                    , stream_pack_ids.required_stream_pack_ids
                    , stream_pack_ids.optional_stream_pack_ids
                    , array(
                          SELECT lesson_locale_pack_id
                          FROM
                              published_stream_lesson_locale_packs
                          WHERE stream_locale_pack_id = ANY (required_stream_pack_ids)
                      ) required_lesson_pack_ids
                FROM with_period_details
                    left JOIN stream_pack_ids
                        ON with_period_details.cohort_id = stream_pack_ids.cohort_id
                           AND with_period_details.index = stream_pack_ids.index
            )
                , expected_counts AS (
                SELECT
                    periods.cohort_id
                    , periods.index
                    , array_sort_unique(
                          array_cat_agg(previous_periods.required_stream_pack_ids))     AS cumulative_required_stream_pack_ids
                    , array_sort_unique(array_cat_agg(
                                            previous_periods.required_lesson_pack_ids)) AS cumulative_required_lesson_pack_ids
                FROM
                    with_pack_ids periods
                    JOIN with_pack_ids previous_periods
                        ON periods.cohort_id = previous_periods.cohort_id
                           AND periods.index >= previous_periods.index
                GROUP BY
                    periods.cohort_id
                    , periods.index
            )

                , time_info AS (
                SELECT
                    this_periods.cohort_id
                    , this_periods.index
                    , coalesce(sum(previous_periods.days + previous_periods.days_offset), 0) +
                      this_periods.days_offset offset_from_cohort_start
                FROM
                    with_period_details this_periods
                    LEFT JOIN with_period_details previous_periods
                        ON this_periods.cohort_id = previous_periods.cohort_id
                           AND this_periods.index > previous_periods.index
                GROUP BY
                    this_periods.cohort_id
                    , this_periods.index
                    , this_periods.days_offset
            )
                , with_time_info AS (
                SELECT
                    with_pack_ids.*

                    -- for period, start before the beginning of pedago
                    , case when with_pack_ids.index = 0 then '2000/01/01'
                        else cohort_start_date + (offset_from_cohort_start :: TEXT || ' days') :: INTERVAL
                      end AS period_start

                    -- for period 0, end at the start of period 1
                    , case when with_pack_ids.index = 0
                        then (
                            select
                                with_pack_ids.cohort_start_date + (first_periods.offset_from_cohort_start :: TEXT || ' days') :: INTERVAL
                            from time_info first_periods
                            where
                                first_periods.cohort_id = with_pack_ids.cohort_id
                                and first_periods.index = 1
                            )
                        ELSE cohort_start_date + ((offset_from_cohort_start + days) :: TEXT || ' days') :: INTERVAL
                        end AS period_end
                FROM
                    with_pack_ids
                    JOIN time_info
                        ON with_pack_ids.cohort_id = time_info.cohort_id
                           AND with_pack_ids.index = time_info.index
            )
                , with_expected_counts_and_time_info AS (
                SELECT
                    with_time_info.*
                    , expected_counts.cumulative_required_stream_pack_ids
                    , expected_counts.cumulative_required_lesson_pack_ids
                    , array_length(expected_counts.cumulative_required_stream_pack_ids,
                                   1) AS cumulative_required_stream_pack_ids_count
                    , array_length(expected_counts.cumulative_required_lesson_pack_ids,
                                   1) AS cumulative_required_lesson_pack_ids_count
                FROM
                    with_time_info
                    JOIN expected_counts
                        ON expected_counts.index = with_time_info.index
                           AND expected_counts.cohort_id = with_time_info.cohort_id
                    JOIN time_info
                        ON time_info.index = with_time_info.index
                           AND time_info.cohort_id = with_time_info.cohort_id
            )
            SELECT *
            FROM with_expected_counts_and_time_info
            ORDER BY cohort_id, index
        ~

        add_index :published_cohort_periods, [:cohort_id, :index], :unique => true, :name => :coh_id_index_on_publ_coh_periods
    end

    # Make necessary changes to support the single stream_entries field (rather than separate required and optional)
    def version_20170411201741
        execute %Q~
            create materialized view published_cohort_periods as
            with period_definitions as (
                select
                    published_cohorts.id as cohort_id
                    , published_cohorts.name
                    , cohorts_versions.start_date as cohort_start_date
                    , periods.definition
                    , periods.index
                from
                    published_cohorts
                join cohorts_versions on cohorts_versions.version_id = published_cohorts.version_id
                    , unnest(cohorts_versions.periods) WITH ORDINALITY periods(definition, index)
            )
            , step_2 as (
                select
                    cohort_id
                    , name
                    , index
                    , definition->>'title' as title
                    , definition->>'style' as style
                    , (definition->>'days')::integer as days
                    , (definition->>'days_offset')::integer as days_offset
                    , definition->>'additional_specialization_playlists_to_complete' as additional_specialization_playlists_to_complete
                    , ARRAY(SELECT elem::json
                            FROM json_array_elements((definition->>'stream_entries')::json) elem) AS stream_entries
                    , cohort_start_date
                from period_definitions
            )
            , step_3 as (
                select
                    cohort_id,
                    index,
                    stream_entries.stream_entry,
                    stream_entries.stream_entry_index
                from step_2
                    , unnest(stream_entries) WITH ORDINALITY stream_entries(stream_entry, stream_entry_index)
            )
            , step_4 as (
                select
                    cohort_id
                    , index
                    , array_remove(
                        array_agg(
                            case
                                when (stream_entry->>'required')::boolean = true then (stream_entry->>'locale_pack_id')::uuid
                                else null
                            end
                        )
                        , null
                    ) as required_stream_pack_ids
                    , array_remove(
                        array_agg(
                            case
                                when (stream_entry->>'required')::boolean = false then (stream_entry->>'locale_pack_id')::uuid
                                else null
                            end
                        )
                        , null
                    ) as optional_stream_pack_ids
                from step_3
                    group by cohort_id, index
            )
            , step_5 as (
                select
                    step_2.*
                    , step_4.required_stream_pack_ids
                    , step_4.optional_stream_pack_ids
                from step_2
                    join step_4
                        on step_2.cohort_id = step_4.cohort_id
                        and step_2.index = step_4.index
             )
             select * from step_5 order by cohort_id, index;
        ~

        add_index :published_cohort_periods, [:cohort_id, :index], :unique => true, :name => :coh_id_index_on_publ_coh_periods
    end

    def version_20170404153806
        execute %Q~
            create materialized view published_cohort_periods as
            with period_definitions as (
                select
                    published_cohorts.id as cohort_id
                    , published_cohorts.name
                    , cohorts_versions.start_date as cohort_start_date
                    , periods.definition
                    , periods.index
                from
                    published_cohorts
                        join cohorts_versions on cohorts_versions.version_id = published_cohorts.version_id
                    , unnest(cohorts_versions.periods) WITH ORDINALITY periods(definition, index)
            )
            , step_2 as (
                select
                    cohort_id
                    , name
                    , index
                    , definition->>'title' as title
                    , definition->>'style' as style
                    , (definition->>'days')::integer as days
                    , (definition->>'days_offset')::integer as days_offset
                    , definition->>'additional_specialization_playlists_to_complete' as additional_specialization_playlists_to_complete
                    , ARRAY(SELECT trim(elem::text, '"')::uuid
                                 FROM   json_array_elements((definition->>'required_stream_pack_ids')::json) elem) AS required_stream_pack_ids
                    , ARRAY(SELECT trim(elem::text, '"')::uuid
                                 FROM   json_array_elements((definition->>'optional_stream_pack_ids')::json) elem) AS optional_stream_pack_ids
                    , cohort_start_date
                from period_definitions
            )
            , time_info as (
                select
                    this_periods.cohort_id
                    , this_periods.index
                    , coalesce(sum(previous_periods.days + previous_periods.days_offset), 0) + this_periods.days_offset offset_from_cohort_start
                from
                    step_2 this_periods
                    left join step_2 previous_periods
                        on this_periods.cohort_id = previous_periods.cohort_id
                        and this_periods.index > previous_periods.index
                group by
                    this_periods.cohort_id
                    , this_periods.index
                    , this_periods.days_offset
            )
            , step_3 as (
                select
                    step_2.*
                    , cohort_start_date + (offset_from_cohort_start::text || ' days')::interval as period_start
                    , cohort_start_date + ((offset_from_cohort_start + days)::text || ' days')::interval as period_end
                from
                    step_2
                    join time_info
                        on step_2.cohort_id = time_info.cohort_id
                        and step_2.index = time_info.index
            )
            select * from step_3 order by cohort_id, index;

        ~

        add_index :published_cohort_periods, [:cohort_id, :index], :unique => true, :name => :coh_id_index_on_publ_coh_periods
    end

end