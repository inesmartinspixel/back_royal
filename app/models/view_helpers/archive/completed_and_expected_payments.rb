class ViewHelpers::CompletedAndExpectedPayments < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    # Materialized CTEs
    def version_20200625152000
        current
    end

    # handle users with multiple subscriptions (edge case
    # that does not happen in the wild)
    def version_20191002105749
        execute %Q~
            create materialized view completed_and_expected_payments as

            with charge_events as (
                SELECT *
                FROM dblink('red_royal', $REDSHIFT$

                    select
                        user_id
                        , id
                        , 'stripe - smart.ly'
                        , estimated_time
                        , event_type
                        , revenue
                    from events
                    where event_type in ('stripe.invoice.payment_success', 'stripe.charge.succeeded', 'stripe.charge.refunded', 'stripe.charge.failed')
                    and created_at > '2017/01/01'
            $REDSHIFT$) AS charge_events (
                    user_id UUID
                    , id UUID
                    , provider TEXT
                    , estimated_time TIMESTAMP
                    , event_type TEXT
                    , revenue FLOAT
                )

                union

                -- For stripe charges, we are relying on events from redshift so that we can assign refunds to the
                -- month of the refund, rather than the month of the charge.  For non-stripe charges, we COULD do that
                -- by looking at versions, but there has never been a non-stripe charge refunded, so not bothering
                -- for now.  Just assuming that refunds happened in the year of the associated charge.
                select
                    user_id
                    , payments.id
                    , payments.provider
                    , transaction_time as estimated_time
                    , 'stripe.charge.succeeded' as event_type
                    , amount - amount_refunded as revenue
                from payments
                where provider != 'stripe - smart.ly'
            )

            -- This three step process to get all registered applications is overly complex,
            -- but it is fast.  It's fast because we first look for applications where we can
            -- tell just from the application itself that it was registered.  Then, we only have
            -- to join other, possibly registered, applications against the versions table.
            , accepted_or_registered_applications as (
                select
                    cohort_applications_plus.id,
                    cohort_applications_plus.user_id,
                    cohorts.program_type,
                    cohort_applications_plus.was_accepted
                from cohort_applications_plus
                    join cohorts on cohorts.id = cohort_id
                    join cohort_applications on cohort_applications_plus.id = cohort_applications.cohort_id
                where
                    cohorts.program_type='emba'
                    and (was_accepted=true or cohort_applications.registered=true)
            )
            , applications_that_were_registered_at_some_point as (
                select
                    distinct cohort_applications.id,
                        cohort_applications.user_id,
                        cohorts.program_type,
                        false as was_accepted -- if this was true, they would have been included in accepted_or_registered_applications
                from cohort_applications
                    join cohorts on cohorts.id = cohort_id
                    join cohort_applications_versions versions
                        on versions.id = cohort_applications.id
                where
                    cohort_applications.id not in (select id from accepted_or_registered_applications)
                    and cohorts.program_type='emba'
                    and versions.registered

                    -- ignore people who were eventually marked as rejected
                    and cohort_applications.status in ('accepted', 'pre_accepted', 'deferred', 'expelled')
            )
            , all_applications_that_ever_registered as (
                select
                    apps.id
                    , apps.user_id
                    , apps.program_type
                    , program_enrollment_progress_records.final_status as current_status
                from (
                    select id, user_id, program_type from applications_that_were_registered_at_some_point
                    union
                    select id, user_id, program_type from accepted_or_registered_applications
                ) apps join external_users
                    on external_users.id = apps.user_id
                left join program_enrollment_progress_records
                    on program_enrollment_progress_records.user_id = apps.user_id
                    and program_enrollment_progress_records.program_type = apps.program_type
            )

            -- in the wild there is only ever one subscription for a
            -- cohort user.  But this is not enforced in the DB, and on staging
            -- I've seen duplicates.
            , active_subscriptions as (
                select
                    u.user_id
                    , max(current_period_end) as current_period_end
                from subscriptions
                    join subscriptions_users u on subscriptions.id = u.subscription_id
                    join all_applications_that_ever_registered apps on apps.user_id = u.user_id
                group by u.user_id
            )

            -- For this report, we are counting each user in the cohort they registered for.
            , all_users_who_ever_registered as (
                SELECT
                    all_applications_that_ever_registered.user_id
                    , (array_agg(cohort_applications.cohort_id order by cohort_applications.applied_at))[1] as original_cohort_id
                    , (array_agg(net_required_payment(cohort_applications) order by cohort_applications.applied_at))[1] as original_expected_revenue
                    , current_cohort.version_id as current_cohort_version_id
                    , current_application.stripe_plan_id
                    , current_application.total_num_required_stripe_payments
                    , current_application.num_charged_payments
                    , current_application.num_refunded_payments
                    , current_application.registered as currently_registered
                    , all_applications_that_ever_registered.current_status
                    , (array_agg(current_application.scholarship_level))[1] scholarship_level -- cannot group by json, so aggregating
                    , active_subscriptions.current_period_end - stripe_plan_interval((array_agg(current_application))[1])::interval as last_period_end
                from all_applications_that_ever_registered
                    join cohort_applications
                        on all_applications_that_ever_registered.id = cohort_applications.id
                    left join cohort_applications current_application
                        on current_application.user_id = all_applications_that_ever_registered.user_id
                        and current_application.status in ('accepted', 'pre_accepted')
                        and current_application.registered = true
                    left join published_cohorts
                        on published_cohorts.id = current_application.cohort_id
                    left join cohorts_versions current_cohort
                        on current_cohort.version_id = published_cohorts.version_id
                    left join active_subscriptions
                        on active_subscriptions.user_id = all_applications_that_ever_registered.user_id
                group BY
                    all_applications_that_ever_registered.user_id
                    , current_cohort.version_id
                    , current_application.stripe_plan_id
                    , current_application.total_num_required_stripe_payments
                    , current_application.num_charged_payments
                    , current_application.num_refunded_payments
                    , current_application.registered
                    , current_application.status
                    , all_applications_that_ever_registered.current_status
                    , active_subscriptions.current_period_end
                order by all_applications_that_ever_registered.user_id asc
            )

            , completed_charges as (
                select
                    id
                    , user_id
                    , estimated_time as charge_time
                    , event_type
                    , revenue
                    , provider
                from
                charge_events
                where event_type != 'stripe.charge.failed'
            )
            , last_charges as (
                SELECT
                    user_id
                    , max(completed_charges.charge_time) as last_charge_at
                FROM
                    completed_charges
                where completed_charges.revenue > 0
                    and event_type='stripe.charge.succeeded'
                group by user_id
            )
            , pending_failed_charges as (
                select
                    failed_charges.user_id
                    , max(failed_charges.estimated_time) as failed_at

                    -- stripe is configured to retry the payment after 3 days and then again
                    -- 1 day after that.  So, after a failed payment, we expect it to be paid
                    -- in 4 days.  If it is not, then we consider the user delinquent and do
                    -- not expect any more payments from them.  (At that point the application
                    -- should be marked as locked_due_to_past_due_payment)
                    , last_period_end + interval '4 days' as expected_failed_payment_by
                    , now() - last_period_end > interval '4 days' as delinquent
                from
                    charge_events failed_charges
                    join all_users_who_ever_registered registered_users
                        using(user_id)
                    left join last_charges
                        using(user_id)
                where failed_charges.event_type = 'stripe.charge.failed'
                group by
                    failed_charges.user_id
                    , last_charges.last_charge_at
                    , last_period_end
                having
                    last_charges.last_charge_at is null
                    or max(failed_charges.estimated_time) > last_charges.last_charge_at
            )

            -- Starting from the last_period_end, expect a payment for
            -- each remaining period.  In addition, if the last payment
            -- failed, and it has been less then 4 days since the last period's
            -- end, then expect that failed payment to get paid (this is the only
            -- case where we will have a record for index=0)
            , upcoming_charges as (
                SELECT
                    registered_users.user_id
                    , registered_users.total_num_required_stripe_payments
                    , registered_users.num_charged_payments
                    , registered_users.num_refunded_payments
                    , index
                    , last_charge_at
                    , ca.stripe_plan_id
                    , payment_amount_per_interval(ca) as revenue
                    , case
                        when  pending_failed_charges.expected_failed_payment_by is not null and index = 0
                            then 'expected_late_payment'
                        ELSE
                            'expected_payment'
                    end event_type
                    , case
                        when pending_failed_charges.expected_failed_payment_by is not null and index = 0
                            then pending_failed_charges.expected_failed_payment_by
                        ELSE
                            last_period_end + index*stripe_plan_interval(ca)::interval
                    end charge_time
                from all_users_who_ever_registered registered_users
                    cross join generate_series(0, 100) as index
                    join last_charges
                        on last_charges.user_id = registered_users.user_id
                    join cohort_applications ca
                        on ca.user_id = registered_users.user_id
                        and ca.registered = true
                        and ca.status in ('accepted', 'pre_accepted')
                    left join pending_failed_charges
                        on pending_failed_charges.user_id = registered_users.user_id
                where
                    registered_users.currently_registered = true

                    -- if a user is delinquent (failed payment more than 4 days ago),
                    -- we do not expect any more payments from them
                    and pending_failed_charges.delinquent is distinct from true

                    and case
                        -- When the last payment failed for the user, we expect a payment for the
                        -- last period.  Then, we expect there to be as many remaining periods as
                        -- there are remaining payments, minus 1 since we're still expecting a payment
                        -- for the last period
                        when pending_failed_charges.expected_failed_payment_by is not null
                            then index between 0 and (registered_users.total_num_required_stripe_payments - registered_users.num_charged_payments + registered_users.num_refunded_payments) - 1

                        -- In the normal case, when the last payment was not failed, then we
                        -- expect as many remaining periods as there are remaining payments
                        else
                            index between 1 and (registered_users.total_num_required_stripe_payments - registered_users.num_charged_payments + registered_users.num_refunded_payments)
                    end

                order by
                    registered_users.user_id
                    , index
            )
            , all_charges_1 as (
                SELECT
                    id::text as id
                    , user_id
                    , charge_time
                    , event_type
                    , revenue
                    , revenue as collected_revenue
                    , 0 as expected_revenue
                    , true as completed
                    , provider
                from completed_charges

                UNION

                SELECT
                    user_id::text  || '-'|| charge_time::text as id
                    , user_id
                    , charge_time
                    , event_type
                    , revenue
                    , 0 as collected_revenue
                    , revenue as expected_revenue
                    , false as completed
                    , null as provider
                from upcoming_charges
            )

            , all_charges_2 as (
                select
                    original_cohorts.name cohort
                    , date_trunc('month', charge_time) as charge_month
                    , all_charges_1.*
                    , all_users_who_ever_registered.original_expected_revenue
                    , all_users_who_ever_registered.current_status
                from all_charges_1
                join all_users_who_ever_registered on
                    all_users_who_ever_registered.user_id = all_charges_1.user_id
                join cohorts original_cohorts
                    on original_cohorts.id = all_users_who_ever_registered.original_cohort_id
            )

            select users.email, users.name, all_charges_2.*
            from all_charges_2
            join users on users.id = all_charges_2.user_id
            order by charge_time, email

            with no data

        ~

        add_index :completed_and_expected_payments, :id, unique: true # unique index needed for concurrent refresh
        add_index :completed_and_expected_payments, :user_id
        add_index :completed_and_expected_payments, :cohort
        add_index :completed_and_expected_payments, :charge_time
    end

    # Fix what happens when someone has a failed payment
    def version_20191001184345
        execute %Q~
            create materialized view completed_and_expected_payments as

           with charge_events as (
                SELECT *
                FROM dblink('red_royal', $REDSHIFT$

                    select
                        user_id
                        , id
                        , 'stripe - smart.ly'
                        , estimated_time
                        , event_type
                        , revenue
                    from events
                    where event_type in ('stripe.invoice.payment_success', 'stripe.charge.succeeded', 'stripe.charge.refunded', 'stripe.charge.failed')
                    and created_at > '2017/01/01'
            $REDSHIFT$) AS charge_events (
                    user_id UUID
                    , id UUID
                    , provider TEXT
                    , estimated_time TIMESTAMP
                    , event_type TEXT
                    , revenue FLOAT
                )

                union

                -- For stripe charges, we are relying on events from redshift so that we can assign refunds to the
                -- month of the refund, rather than the month of the charge.  For non-stripe charges, we COULD do that
                -- by looking at versions, but there has never been a non-stripe charge refunded, so not bothering
                -- for now.  Just assuming that refunds happened in the year of the associated charge.
                select
                    user_id
                    , payments.id
                    , payments.provider
                    , transaction_time as estimated_time
                    , 'stripe.charge.succeeded' as event_type
                    , amount - amount_refunded as revenue
                from payments
                where provider != 'stripe - smart.ly'
            )

            -- This three step process to get all registered applications is overly complex,
            -- but it is fast.  It's fast because we first look for applications where we can
            -- tell just from the application itself that it was registered.  Then, we only have
            -- to join other, possibly registered, applications against the versions table.
            , accepted_or_registered_applications as (
                select
                    cohort_applications_plus.id,
                    cohort_applications_plus.user_id,
                    cohorts.program_type,
                    cohort_applications_plus.was_accepted
                from cohort_applications_plus
                    join cohorts on cohorts.id = cohort_id
                    join cohort_applications on cohort_applications_plus.id = cohort_applications.cohort_id
                where
                    cohorts.program_type='emba'
                    and (was_accepted=true or cohort_applications.registered=true)
            )
            , applications_that_were_registered_at_some_point as (
                select
                    distinct cohort_applications.id,
                        cohort_applications.user_id,
                        cohorts.program_type,
                        false as was_accepted -- if this was true, they would have been included in accepted_or_registered_applications
                from cohort_applications
                    join cohorts on cohorts.id = cohort_id
                    join cohort_applications_versions versions
                        on versions.id = cohort_applications.id
                where
                    cohort_applications.id not in (select id from accepted_or_registered_applications)
                    and cohorts.program_type='emba'
                    and versions.registered

                    -- ignore people who were eventually marked as rejected
                    and cohort_applications.status in ('accepted', 'pre_accepted', 'deferred', 'expelled')
            )
            , all_applications_that_ever_registered as (
                select
                    apps.id
                    , apps.user_id
                    , apps.program_type
                    , program_enrollment_progress_records.final_status as current_status
                from (
                    select id, user_id, program_type from applications_that_were_registered_at_some_point
                    union
                    select id, user_id, program_type from accepted_or_registered_applications
                ) apps join external_users
                    on external_users.id = apps.user_id
                left join program_enrollment_progress_records
                    on program_enrollment_progress_records.user_id = apps.user_id
                    and program_enrollment_progress_records.program_type = apps.program_type
            )

            -- For this report, we are counting each user in the cohort they registered for.
            , all_users_who_ever_registered as (
                SELECT
                    all_applications_that_ever_registered.user_id
                    , (array_agg(cohort_applications.cohort_id order by cohort_applications.applied_at))[1] as original_cohort_id
                    , (array_agg(net_required_payment(cohort_applications) order by cohort_applications.applied_at))[1] as original_expected_revenue
                    , current_cohort.version_id as current_cohort_version_id
                    , current_application.stripe_plan_id
                    , current_application.total_num_required_stripe_payments
                    , current_application.num_charged_payments
                    , current_application.num_refunded_payments
                    , current_application.registered as currently_registered
                    , all_applications_that_ever_registered.current_status
                    , (array_agg(current_application.scholarship_level))[1] scholarship_level -- cannot group by json, so aggregating
                    , subscriptions.current_period_end - stripe_plan_interval((array_agg(current_application))[1])::interval as last_period_end
                from all_applications_that_ever_registered
                    join cohort_applications
                        on all_applications_that_ever_registered.id = cohort_applications.id
                    left join cohort_applications current_application
                        on current_application.user_id = all_applications_that_ever_registered.user_id
                        and current_application.status in ('accepted', 'pre_accepted')
                        and current_application.registered = true
                    left join published_cohorts
                        on published_cohorts.id = current_application.cohort_id
                    left join cohorts_versions current_cohort
                        on current_cohort.version_id = published_cohorts.version_id
                    left join subscriptions_users
                        on subscriptions_users.user_id = current_application.user_id
                    left join subscriptions
                        on subscriptions_users.subscription_id = subscriptions.id
                group BY
                    all_applications_that_ever_registered.user_id
                    , current_cohort.version_id
                    , current_application.stripe_plan_id
                    , current_application.total_num_required_stripe_payments
                    , current_application.num_charged_payments
                    , current_application.num_refunded_payments
                    , current_application.registered
                    , current_application.status
                    , all_applications_that_ever_registered.current_status
                    , subscriptions.current_period_end
                order by all_applications_that_ever_registered.user_id asc
            )

            , completed_charges as (
                select
                    id
                    , user_id
                    , estimated_time as charge_time
                    , event_type
                    , revenue
                    , provider
                from
                charge_events
                where event_type != 'stripe.charge.failed'
            )
            , last_charges as (
                SELECT
                    user_id
                    , max(completed_charges.charge_time) as last_charge_at
                FROM
                    completed_charges
                where completed_charges.revenue > 0
                    and event_type='stripe.charge.succeeded'
                group by user_id
            )
            , pending_failed_charges as (
                select
                    failed_charges.user_id
                    , max(failed_charges.estimated_time) as failed_at

                    -- stripe is configured to retry the payment after 3 days and then again
                    -- 1 day after that.  So, after a failed payment, we expect it to be paid
                    -- in 4 days.  If it is not, then we consider the user delinquent and do
                    -- not expect any more payments from them.  (At that point the application
                    -- should be marked as locked_due_to_past_due_payment)
                    , last_period_end + interval '4 days' as expected_failed_payment_by
                    , now() - last_period_end > interval '4 days' as delinquent
                from
                    charge_events failed_charges
                    join all_users_who_ever_registered registered_users
                        using(user_id)
                    left join last_charges
                        using(user_id)
                where failed_charges.event_type = 'stripe.charge.failed'
                group by
                    failed_charges.user_id
                    , last_charges.last_charge_at
                    , last_period_end
                having
                    last_charges.last_charge_at is null
                    or max(failed_charges.estimated_time) > last_charges.last_charge_at
            )

            -- Starting from the last_period_end, expect a payment for
            -- each remaining period.  In addition, if the last payment
            -- failed, and it has been less then 4 days since the last period's
            -- end, then expect that failed payment to get paid (this is the only
            -- case where we will have a record for index=0)
            , upcoming_charges as (
                SELECT
                    registered_users.user_id
                    , registered_users.total_num_required_stripe_payments
                    , registered_users.num_charged_payments
                    , registered_users.num_refunded_payments
                    , index
                    , last_charge_at
                    , ca.stripe_plan_id
                    , payment_amount_per_interval(ca) as revenue
                    , case
                        when  pending_failed_charges.expected_failed_payment_by is not null and index = 0
                            then 'expected_late_payment'
                        ELSE
                            'expected_payment'
                    end event_type
                    , case
                        when pending_failed_charges.expected_failed_payment_by is not null and index = 0
                            then pending_failed_charges.expected_failed_payment_by
                        ELSE
                            last_period_end + index*stripe_plan_interval(ca)::interval
                    end charge_time
                from all_users_who_ever_registered registered_users
                    cross join generate_series(0, 100) as index
                    join last_charges
                        on last_charges.user_id = registered_users.user_id
                    join cohort_applications ca
                        on ca.user_id = registered_users.user_id
                        and ca.registered = true
                        and ca.status in ('accepted', 'pre_accepted')
                    left join pending_failed_charges
                        on pending_failed_charges.user_id = registered_users.user_id
                where
                    registered_users.currently_registered = true

                    -- if a user is delinquent (failed payment more than 4 days ago),
                    -- we do not expect any more payments from them
                    and pending_failed_charges.delinquent is distinct from true

                    and case
                        -- When the last payment failed for the user, we expect a payment for the
                        -- last period.  Then, we expect there to be as many remaining periods as
                        -- there are remaining payments, minus 1 since we're still expecting a payment
                        -- for the last period
                        when pending_failed_charges.expected_failed_payment_by is not null
                            then index between 0 and (registered_users.total_num_required_stripe_payments - registered_users.num_charged_payments + registered_users.num_refunded_payments) - 1

                        -- In the normal case, when the last payment was not failed, then we
                        -- expect as many remaining periods as there are remaining payments
                        else
                            index between 1 and (registered_users.total_num_required_stripe_payments - registered_users.num_charged_payments + registered_users.num_refunded_payments)
                    end

                order by
                    registered_users.user_id
                    , index
            )
            , all_charges_1 as (
                SELECT
                    id::text as id
                    , user_id
                    , charge_time
                    , event_type
                    , revenue
                    , revenue as collected_revenue
                    , 0 as expected_revenue
                    , true as completed
                    , provider
                from completed_charges

                UNION

                SELECT
                    user_id::text  || '-'|| charge_time::text as id
                    , user_id
                    , charge_time
                    , event_type
                    , revenue
                    , 0 as collected_revenue
                    , revenue as expected_revenue
                    , false as completed
                    , null as provider
                from upcoming_charges
            )

            , all_charges_2 as (
                select
                    original_cohorts.name cohort
                    , date_trunc('month', charge_time) as charge_month
                    , all_charges_1.*
                    , all_users_who_ever_registered.original_expected_revenue
                    , all_users_who_ever_registered.current_status
                from all_charges_1
                join all_users_who_ever_registered on
                    all_users_who_ever_registered.user_id = all_charges_1.user_id
                join cohorts original_cohorts
                    on original_cohorts.id = all_users_who_ever_registered.original_cohort_id
            )

            select users.email, users.name, all_charges_2.*
            from all_charges_2
            join users on users.id = all_charges_2.user_id
            order by charge_time, email

            with no data

        ~

        add_index :completed_and_expected_payments, :id, unique: true # unique index needed for concurrent refresh
        add_index :completed_and_expected_payments, :user_id
        add_index :completed_and_expected_payments, :cohort
        add_index :completed_and_expected_payments, :charge_time
    end

    def version_20190410151654
        execute %Q~
            create materialized view completed_and_expected_payments as

            with charge_events as (
                SELECT *
                FROM dblink('red_royal', $REDSHIFT$

                    select
                        user_id
                        , id
                        , 'stripe - smart.ly'
                        , estimated_time
                        , event_type
                        , revenue
                    from events
                    where event_type in ('stripe.invoice.payment_success', 'stripe.charge.succeeded', 'stripe.charge.refunded')
                    and estimated_time > '2017/01/01'
            $REDSHIFT$) AS charge_events (
                    user_id UUID
                    , id UUID
                    , provider TEXT
                    , estimated_time TIMESTAMP
                    , event_type TEXT
                    , revenue FLOAT
                )

                union

                -- For stripe charges, we are relying on events from redshift so that we can assign refunds to the
                -- month of the refund, rather than the month of the charge.  For non-stripe charges, we COULD do that
                -- by looking at versions, but there has never been a non-stripe charge refunded, so not bothering
                -- for now.  Just assuming that refunds happened in the year of the associated charge.
                select
                    user_id
                    , payments.id
                    , payments.provider
                    , transaction_time as estimated_time
                    , 'stripe.charge.succeeded' as event_type
                    , amount - amount_refunded as revenue
                from payments
                where provider != 'stripe - smart.ly'
            )

            -- This three step process to get all registered applications is overly complex,
            -- but it is fast.  It's fast because we first look for applications where we can
            -- tell just from the application itself that it was registered.  Then, we only have
            -- to join other, possibly registered, applications against the versions table.
            , accepted_or_registered_applications as (
                select
                    cohort_applications_plus.id,
                    cohort_applications_plus.user_id,
                    cohorts.program_type,
                    cohort_applications_plus.was_accepted
                from cohort_applications_plus
                    join cohorts on cohorts.id = cohort_id
                    join cohort_applications on cohort_applications_plus.id = cohort_applications.cohort_id
                where
                    cohorts.program_type='emba'
                    and (was_accepted=true or cohort_applications.registered=true)
            )
            , applications_that_were_registered_at_some_point as (
                select
                    distinct cohort_applications.id,
                        cohort_applications.user_id,
                        cohorts.program_type,
                        false as was_accepted -- if this was true, they would have been included in accepted_or_registered_applications
                from cohort_applications
                    join cohorts on cohorts.id = cohort_id
                    join cohort_applications_versions versions
                        on versions.id = cohort_applications.id
                where
                    cohort_applications.id not in (select id from accepted_or_registered_applications)
                    and cohorts.program_type='emba'
                    and versions.registered

                    -- ignore people who were eventually marked as rejected
                    and cohort_applications.status in ('accepted', 'pre_accepted', 'deferred', 'expelled')
            )
            , all_applications_that_ever_registered as (
                select
                    apps.id
                    , apps.user_id
                    , apps.program_type
                    , program_enrollment_progress_records.final_status as current_status
                from (
                    select id, user_id, program_type from applications_that_were_registered_at_some_point
                    union
                    select id, user_id, program_type from accepted_or_registered_applications
                ) apps join external_users
                    on external_users.id = apps.user_id
                left join program_enrollment_progress_records
                    on program_enrollment_progress_records.user_id = apps.user_id
                    and program_enrollment_progress_records.program_type = apps.program_type
            )

            -- For this report, we are counting each user in the cohort they registered for.
            , all_users_who_ever_registered as (
                SELECT
                    all_applications_that_ever_registered.user_id
                    , (array_agg(cohort_applications.cohort_id order by cohort_applications.applied_at))[1] as original_cohort_id
                    , (array_agg(net_required_payment(cohort_applications) order by cohort_applications.applied_at))[1] as original_expected_revenue
                    , current_cohort.version_id as current_cohort_version_id
                    , current_application.stripe_plan_id
                    , current_application.total_num_required_stripe_payments
                    , current_application.num_charged_payments
                    , current_application.num_refunded_payments
                    , current_application.registered as currently_registered
                    , all_applications_that_ever_registered.current_status
                    , (array_agg(current_application.scholarship_level))[1] scholarship_level -- cannot group by json, so aggregating
                from all_applications_that_ever_registered
                    join cohort_applications
                        on all_applications_that_ever_registered.id = cohort_applications.id
                    left join cohort_applications current_application
                        on current_application.user_id = all_applications_that_ever_registered.user_id
                        and current_application.status in ('accepted', 'pre_accepted')
                        and current_application.registered = true
                    left join published_cohorts
                        on published_cohorts.id = current_application.cohort_id
                    left join cohorts_versions current_cohort
                        on current_cohort.version_id = published_cohorts.version_id
                group BY
                    all_applications_that_ever_registered.user_id
                    , current_cohort.version_id
                    , current_application.stripe_plan_id
                    , current_application.total_num_required_stripe_payments
                    , current_application.num_charged_payments
                    , current_application.num_refunded_payments
                    , current_application.registered
                    , current_application.status
                    , all_applications_that_ever_registered.current_status
                order by all_applications_that_ever_registered.user_id asc
            )

            , completed_charges as (
                select
                    id
                    , user_id
                    , estimated_time as charge_time
                    , event_type
                    , revenue
                    , provider
                from
                charge_events
            )
            , last_charges as (
                SELECT
                    user_id
                    , max(completed_charges.charge_time) as last_charge_at
                FROM
                    completed_charges
                where completed_charges.revenue > 0 -- ignore refunds
                group by user_id
            )
            , upcoming_charges as (
                SELECT
                    registered_users.user_id
                    , registered_users.total_num_required_stripe_payments
                    , registered_users.num_charged_payments
                    , registered_users.num_refunded_payments
                    , index
                    , last_charge_at
                    , ca.stripe_plan_id
                    , payment_amount_per_interval(ca) as revenue
                    , case
                        when last_charge_at + index*stripe_plan_interval(ca)::interval < now()
                            then 'late_payment'
                        ELSE
                            'expected_payment'
                    end event_type
                    , case
                        when last_charge_at + index*stripe_plan_interval(ca)::interval < now()
                            then now() + interval '1 day'
                        ELSE
                            last_charge_at + index*stripe_plan_interval(ca)::interval
                    end charge_time
                from all_users_who_ever_registered registered_users
                    cross join generate_series(1, 100) as index
                    join last_charges
                        on last_charges.user_id = registered_users.user_id
                    join cohort_applications ca
                        on ca.user_id = registered_users.user_id
                        and ca.registered = true
                        and ca.status in ('accepted', 'pre_accepted')
                where
                    registered_users.currently_registered = true
                    and index <= (registered_users.total_num_required_stripe_payments - registered_users.num_charged_payments + registered_users.num_refunded_payments)

                order by
                    registered_users.user_id
                    , index
            )
            , all_charges_1 as (
                SELECT
                    id::text as id
                    , user_id
                    , charge_time
                    , event_type
                    , revenue
                    , revenue as collected_revenue
                    , 0 as expected_revenue
                    , true as completed
                    , provider
                from completed_charges

                UNION

                SELECT
                    user_id::text  || '-'|| charge_time::text as id
                    , user_id
                    , charge_time
                    , event_type
                    , revenue
                    , 0 as collected_revenue
                    , revenue as expected_revenue
                    , false as completed
                    , null as provider
                from upcoming_charges
            )

            , all_charges_2 as (
                select
                    original_cohorts.name cohort
                    , date_trunc('month', charge_time) as charge_month
                    , all_charges_1.*
                    , all_users_who_ever_registered.original_expected_revenue
                    , all_users_who_ever_registered.current_status
                from all_charges_1
                join all_users_who_ever_registered on
                    all_users_who_ever_registered.user_id = all_charges_1.user_id
                join cohorts original_cohorts
                    on original_cohorts.id = all_users_who_ever_registered.original_cohort_id
            )

            select users.email, users.name, all_charges_2.*
            from all_charges_2
            join users on users.id = all_charges_2.user_id
            order by charge_time, email

            with no data

        ~

        add_index :completed_and_expected_payments, :id, unique: true # unique index needed for concurrent refresh
        add_index :completed_and_expected_payments, :user_id
        add_index :completed_and_expected_payments, :cohort
        add_index :completed_and_expected_payments, :charge_time
    end

end