class ViewHelpers::CustomerBillingAccounts < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    # Materialized CTEs
    def version_20200625152000
        current
    end

    def version_20190212135529
        execute %Q~
            create view customer_billing_accounts as
            with current_applications as (
                select
                    user_id
                    , (array_agg(cohort_applications.id order by applied_at desc))[1] as cohort_application_id
                from cohort_applications
                    join cohorts on cohorts.id = cohort_id
                where program_type ='emba'
                    and cohort_applications.status in ('deferred', 'accepted', 'expelled', 'pre_accepted')
                group by user_id
            )
            , users_with_required_tuition as (
                select
                    current_applications.user_id
                    , net_required_payment(cohort_applications) total_invoiced_amount
                from current_applications
                    join cohort_applications
                        on cohort_applications.id = current_applications.cohort_application_id
                where net_required_payment(cohort_applications) > 0
            )
            , with_summed_transactions as (
                select
                    users_with_required_tuition.*
                    , sum(case when transaction_type='payment' then amount - amount_refunded else null end) as payments
                    , sum(case when transaction_type='credit' then amount else null end) as credits
                    , sum(case when transaction_type='surcharge' then amount else null end) as surcharges
                    , sum(amount-amount_refunded) as total_credit
                from users_with_required_tuition
                    join billing_transactions_users
                        using (user_id)
                    join billing_transactions
                        on billing_transactions_users.billing_transaction_id = billing_transactions.id
                group by
                    users_with_required_tuition.user_id
                    , users_with_required_tuition.total_invoiced_amount
            )
            select
                with_summed_transactions.*
                , total_credit - total_invoiced_amount as account_balance
            from with_summed_transactions
            order by total_credit - total_invoiced_amount desc
        ~
    end

end