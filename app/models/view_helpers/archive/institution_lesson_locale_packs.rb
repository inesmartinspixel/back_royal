class ViewHelpers::InstitutionLessonLocalePacks < ViewHelpers::ViewHelperBase
    # -*- SkipSchemaAnnotations

    def version_20200624011101
        # do nothing. removed
    end

    def version_20170220200244
        execute %Q~
            create materialized view institution_lesson_locale_packs as
            select
                institution_stream_locale_packs.*
                , published_stream_lesson_locale_packs.test
                , published_stream_lesson_locale_packs.assessment
                , published_stream_lesson_locale_packs.lesson_title
                , published_stream_lesson_locale_packs.lesson_locale_pack_id
                , published_stream_lesson_locale_packs.lesson_locales
            from institution_stream_locale_packs
                join published_stream_lesson_locale_packs on published_stream_lesson_locale_packs.stream_locale_pack_id = institution_stream_locale_packs.stream_locale_pack_id
            with no data
        ~

        add_index :institution_lesson_locale_packs, [:institution_id, :stream_locale_pack_id, :lesson_locale_pack_id], :name => :inst_and_lesson_lp_on_inst_lesson_lps, :unique => true
        add_index :institution_lesson_locale_packs, :lesson_locale_pack_id
    end

end