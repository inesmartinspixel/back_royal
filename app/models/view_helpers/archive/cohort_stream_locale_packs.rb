class ViewHelpers::CohortStreamLocalePacks < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    def version_20170310204902
        # do nothing.  this view has been renamed published_cohort_stream_locale_packs after version 20170310204902
    end

    def version_20170306001915
        execute %Q~
            create materialized view cohort_stream_locale_packs as
            select
                cohorts.id as cohort_id
                , cohorts.name as cohort_name
                , max(case when cohort_playlist_locale_packs.foundations then 1 else 0 end) = 1 as foundations
                , sum(case when cohort_playlist_locale_packs.playlist_locale_pack_id is not null then 1 else 0 end) > 0 as in_curriculum
                , case when exam = true then false
                    else sum(case when cohort_playlist_locale_packs.playlist_locale_pack_id is not null and exam = false then 1 else 0 end) = 0
                    end as elective
                , published_stream_locale_packs.exam as exam

                , published_stream_locale_packs.title as stream_title
                , published_stream_locale_packs.locale_pack_id as stream_locale_pack_id
                , published_stream_locale_packs.locales as stream_locales

                , count(distinct cohort_playlist_locale_packs.playlist_locale_pack_id) as playlist_count
            from
                cohorts
                join hacked_access_groups_cohorts on hacked_access_groups_cohorts.cohort_id = cohorts.id
                join access_groups_lesson_stream_locale_packs on access_groups_lesson_stream_locale_packs.access_group_id = hacked_access_groups_cohorts.access_group_id
                join published_stream_locale_packs on published_stream_locale_packs.locale_pack_id = access_groups_lesson_stream_locale_packs.locale_pack_id
                left join published_playlist_streams on published_playlist_streams.stream_locale_pack_id = published_stream_locale_packs.locale_pack_id
                left join cohort_playlist_locale_packs on
                    cohorts.id = cohort_playlist_locale_packs.cohort_id
                    and published_playlist_streams.playlist_locale_pack_id = cohort_playlist_locale_packs.playlist_locale_pack_id

            group by
                cohorts.id
                , cohorts.name
                , published_stream_locale_packs.title
                , published_stream_locale_packs.locale_pack_id
                , published_stream_locale_packs.locales
                , published_stream_locale_packs.exam
            with no data
        ~

        add_index :cohort_stream_locale_packs, [:cohort_id, :stream_locale_pack_id], :name => :cohort_and_str_lp_on_cohort_str_lps, :unique => true
        add_index :cohort_stream_locale_packs, :stream_locale_pack_id
    end

    def version_20170220200244
        execute %Q~
            create materialized view cohort_stream_locale_packs as
            select
                cohorts.id as cohort_id
                , cohorts.name_locales->>'en' as cohort_name
                , max(case when cohort_playlist_locale_packs.foundations then 1 else 0 end) = 1 as foundations
                , sum(case when cohort_playlist_locale_packs.playlist_locale_pack_id is not null then 1 else 0 end) > 0 as in_curriculum
                , case when exam = true then false
                    else sum(case when cohort_playlist_locale_packs.playlist_locale_pack_id is not null and exam = false then 1 else 0 end) = 0
                    end as elective
                , published_stream_locale_packs.exam as exam

                , published_stream_locale_packs.title as stream_title
                , published_stream_locale_packs.locale_pack_id as stream_locale_pack_id
                , published_stream_locale_packs.locales as stream_locales

                , count(distinct cohort_playlist_locale_packs.playlist_locale_pack_id) as playlist_count
            from
                cohorts
                join hacked_access_groups_cohorts on hacked_access_groups_cohorts.cohort_id = cohorts.id
                join access_groups_lesson_stream_locale_packs on access_groups_lesson_stream_locale_packs.access_group_id = hacked_access_groups_cohorts.access_group_id
                join published_stream_locale_packs on published_stream_locale_packs.locale_pack_id = access_groups_lesson_stream_locale_packs.locale_pack_id
                left join published_playlist_streams on published_playlist_streams.stream_locale_pack_id = published_stream_locale_packs.locale_pack_id
                left join cohort_playlist_locale_packs on
                    cohorts.id = cohort_playlist_locale_packs.cohort_id
                    and published_playlist_streams.playlist_locale_pack_id = cohort_playlist_locale_packs.playlist_locale_pack_id

            group by
                cohorts.id
                , cohorts.name_locales->>'en'
                , published_stream_locale_packs.title
                , published_stream_locale_packs.locale_pack_id
                , published_stream_locale_packs.locales
                , published_stream_locale_packs.exam
            with no data
        ~

        add_index :cohort_stream_locale_packs, [:cohort_id, :stream_locale_pack_id], :name => :cohort_and_str_lp_on_cohort_str_lps, :unique => true
        add_index :cohort_stream_locale_packs, :stream_locale_pack_id
    end

end