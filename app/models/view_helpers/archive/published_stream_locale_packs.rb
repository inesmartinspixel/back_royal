class ViewHelpers::PublishedStreamLocalePacks < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    def version_20170310204902
        current
    end

    def version_20170220200244
        execute %Q~
            create materialized view published_stream_locale_packs as
            select
                published_streams.locale_pack_id
                , lesson_streams.title -- we use the working english title in case it is not published in english
                , string_agg(published_streams.locale, ',') as locales
                , max(case when published_streams.exam then 1 else 0 end) = 1 as exam
            from published_streams
                join lesson_streams on lesson_streams.locale_pack_id = published_streams.locale_pack_id and lesson_streams.locale='en'
            group by
                published_streams.locale_pack_id
                , lesson_streams.title
            with no data
        ~

        add_index :published_stream_locale_packs, :locale_pack_id, :unique => true
    end

end