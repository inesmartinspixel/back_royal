class ViewHelpers::PublishedStreamLessonLocalePacks < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    def version_20170324205906
        current
    end

    def version_20170220200244
        execute %Q~
            create materialized view published_stream_lesson_locale_packs as
            select
                distinct published_stream_locale_packs.title as stream_title
                , published_lesson_locale_packs.title as lesson_title
                , published_lesson_locale_packs.test as test
                , published_lesson_locale_packs.assessment as assessment
                , published_stream_locale_packs.locale_pack_id as stream_locale_pack_id
                , published_lesson_locale_packs.locale_pack_id as lesson_locale_pack_id
                , published_stream_locale_packs.locales as stream_locales
                , published_lesson_locale_packs.locales as lesson_locales
            from
                published_stream_lessons
                join published_stream_locale_packs on published_stream_locale_packs.locale_pack_id = published_stream_lessons.stream_locale_pack_id
                join published_lesson_locale_packs on published_lesson_locale_packs.locale_pack_id = published_stream_lessons.lesson_locale_pack_id
            with no data
        ~

        add_index :published_stream_lesson_locale_packs, [:stream_locale_pack_id, :lesson_locale_pack_id], :name => :stream_lp_on_pub_str_lesson_lp, :unique => true
        add_index :published_stream_lesson_locale_packs, :lesson_locale_pack_id, :name => :lesson_lp_on_pub_str_lesson_lp
    end

end