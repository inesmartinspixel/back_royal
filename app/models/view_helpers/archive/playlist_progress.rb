# == Schema Information
#
# Table name: playlist_progress
#
#  user_id        :uuid
#  locale_pack_id :uuid
#  started        :boolean
#  completed      :boolean
#

class ViewHelpers::PlaylistProgress < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    def version_20180629200606
        current
    end

    def version_20170302202710
        execute %Q~
            create materialized view playlist_progress as

            select
                lesson_streams_progress.user_id
                , playlist_locale_pack_id as locale_pack_id
                , true as started
                , count(distinct case when lesson_streams_progress.completed_at is not null then lesson_streams_progress.locale_pack_id else null end) = published_playlists.stream_count as completed
            from
                lesson_streams_progress
                join published_playlist_streams
                    on published_playlist_streams.stream_locale_pack_id = lesson_streams_progress.locale_pack_id
                join published_playlists
                    on published_playlists.locale_pack_id = published_playlist_streams.playlist_locale_pack_id
                join user_can_see_playlist_locale_packs
                    on published_playlists.locale_pack_id = user_can_see_playlist_locale_packs.locale_pack_id
                    and lesson_streams_progress.user_id = user_can_see_playlist_locale_packs.user_id
            group by
                lesson_streams_progress.user_id
                , playlist_locale_pack_id
                , published_playlists.stream_count
            with no data
        ~

        add_index :playlist_progress, [:user_id, :locale_pack_id], :unique => true
    end

    def version_20170221140313
        execute %Q~
            create materialized view playlist_progress as

            select
                lesson_streams_progress.user_id
                , playlist_locale_pack_id as locale_pack_id
                , true as started
                , count(distinct case when lesson_streams_progress.completed_at is not null then lesson_streams_progress.locale_pack_id else null end) = array_length(stream_entries, 1) as completed
            from
                lesson_streams_progress
                join published_playlist_streams
                    on published_playlist_streams.stream_locale_pack_id = lesson_streams_progress.locale_pack_id
                join published_playlists
                    on published_playlists.locale_pack_id = published_playlist_streams.playlist_locale_pack_id
                join user_can_see_playlist_locale_packs
                    on published_playlists.locale_pack_id = user_can_see_playlist_locale_packs.locale_pack_id
                    and lesson_streams_progress.user_id = user_can_see_playlist_locale_packs.user_id
            group by
                lesson_streams_progress.user_id
                , playlist_locale_pack_id
                , array_length(stream_entries, 1)
            with no data
        ~

        add_index :playlist_progress, [:user_id, :locale_pack_id], :unique => true
    end

end
