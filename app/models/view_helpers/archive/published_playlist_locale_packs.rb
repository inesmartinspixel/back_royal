class ViewHelpers::PublishedPlaylistLocalePacks < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    def version_20170310204902
        current
    end

    def version_20170220200244
        execute %Q~
            create materialized view published_playlist_locale_packs as
            select
                published_playlists.locale_pack_id
                , playlists.title -- we use the working english title in case it is not published in english
                , string_agg(published_playlists.locale, ',') as locales
            from published_playlists
                join playlists on playlists.locale_pack_id = published_playlists.locale_pack_id and playlists.locale='en'
            group by
                published_playlists.locale_pack_id
                , playlists.title
            with no data
        ~
        add_index :published_playlist_locale_packs, :locale_pack_id, :unique => true
    end

end