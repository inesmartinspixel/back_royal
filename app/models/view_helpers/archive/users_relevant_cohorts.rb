class ViewHelpers::UsersRelevantCohorts < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    # This version adds pre_accepted to the list of applicable statuses
    def version_20171011175022
        current
    end

    def version_20170601135311
        execute %Q~
            create view users_relevant_cohorts as

            select
                distinct users.id as user_id
                , coalesce(pending_or_accepted_cohorts.id, promoted_cohorts.id) as cohort_id
            from
                users

                -- join against cohorts for which this user has a pending or accepted application
                left join
                    cohort_applications on cohort_applications.user_id = users.id
                    and cohort_applications.status in ('accepted', 'pending')
                left join published_cohorts pending_or_accepted_cohorts
                    on pending_or_accepted_cohorts.id = cohort_applications.cohort_id

                -- and, if there are no pending or accepted ones, join against promoted
                -- cohorts where the program_type matches the user's fallback_program_type
                left join published_cohorts promoted_cohorts
                    on pending_or_accepted_cohorts.id is null
                        and promoted_cohorts.promoted = true
                        and users.fallback_program_type = promoted_cohorts.program_type

        ~
    end
end