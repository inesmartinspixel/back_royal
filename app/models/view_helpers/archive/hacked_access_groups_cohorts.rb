class ViewHelpers::HackedAccessGroupsCohorts < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    def version_20170310204902
        # do nothing.  We don't need this anymore
    end

    def version_20170306001915
        execute %Q~
            create materialized view hacked_access_groups_cohorts as
            select
                access_group_id,
                cohort_id
            from access_groups_cohorts
            union
            select
                access_groups.id access_group_id
                , cohorts.id cohort_id
            from cohorts
                join access_groups
                    on cohorts.name || '_EXAM' = access_groups.name
            with no data
        ~

        add_index :hacked_access_groups_cohorts, [:access_group_id, :cohort_id], :unique => true, :name => :group_and_cohort_on_hacked_groups
        add_index :hacked_access_groups_cohorts, :cohort_id
    end

    def version_20170220200244
        execute %Q~
            create materialized view hacked_access_groups_cohorts as
            select
                access_group_id,
                cohort_id
            from access_groups_cohorts
            union
            select
                access_groups.id access_group_id
                , cohorts.id cohort_id
            from cohorts
                join access_groups
                    on (cohorts.name_locales->>'en')::text || '_EXAM' = access_groups.name
            with no data
        ~

        add_index :hacked_access_groups_cohorts, [:access_group_id, :cohort_id], :unique => true, :name => :group_and_cohort_on_hacked_groups
        add_index :hacked_access_groups_cohorts, :cohort_id
    end

end