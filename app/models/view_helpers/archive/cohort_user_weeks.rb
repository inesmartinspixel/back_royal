class ViewHelpers::CohortUserWeeks < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    def version_20180111150157
        # do nothing.  This has been replaced with cohort_user_periods
    end

    def version_20170323141852
        execute %Q~
            create materialized view cohort_user_weeks as
            with lesson_progress_records as (
                select
                    user_id
                    , cohort_id
                    , completed_at
                    , start_date
                    , case
                        when completed_at < cohorts.start_date then 0
                        else
                            ceil((extract(epoch from completed_at) - extract(epoch from cohorts.start_date)) / extract(epoch from interval '1 week'))
                        end as completed_in_week
                from
                    cohort_user_lesson_progress_records
                    join cohorts on cohort_user_lesson_progress_records.cohort_id = cohorts.id
                where
                    completed_at is not null
                    and required=true
            )
            -- the active_weeks step and the weeks_1 step could be combined, but this
            -- is a lot faster.  For some reason switching the join with lesson_progress_records
            -- to a left join really slows things down
            , active_weeks as (
                select
                    cohort_applications_plus.user_id
                    , cohort_applications_plus.cohort_id
                    , week_index
                    , count(*) as lessons_completed_in_week
                from
                    cohort_applications_plus
                    cross join (
                        SELECT
                            week_index
                        FROM
                            generate_series(0, 26) AS week_index
                    ) user_weeks
                    join lesson_progress_records
                        on cohort_applications_plus.cohort_id = lesson_progress_records.cohort_id
                        and cohort_applications_plus.user_id = lesson_progress_records.user_id
                        and week_index = lesson_progress_records.completed_in_week
                group by
                    cohort_applications_plus.user_id
                    , cohort_applications_plus.cohort_id
                    , week_index
            )
            , weeks_1 as (
                select
                    cohort_applications_plus.user_id
                    , cohort_applications_plus.cohort_id
                    , cohort_applications_plus.cohort_name
                    , week_index
                    , case when week_index = 0 then '2000/01/01'::timestamp else cohorts.start_date + ((week_index-1) || ' weeks')::interval end as week_start
                    , cohorts.start_date + (week_index || ' weeks')::interval as week_end
                    , case
                        when week_index >= duration_weeks then required_lesson_count
                        else week_index*cohort_content_details.expected_pace
                        end as expected_curriculum_lessons_complete
                    , case
                        when week_index >= duration_weeks then required_lesson_count
                        else cohort_content_details.foundations_lesson_count + week_index*cohort_content_details.expected_pace_fp
                        end as expected_curriculum_lessons_complete_fp
                from
                    cohort_applications_plus
                    cross join (
                        SELECT
                            week_index
                        FROM
                            generate_series(0, 26) AS week_index
                    ) user_weeks
                    join cohorts
                        on cohorts.id = cohort_applications_plus.cohort_id
                    join published_cohort_content_details cohort_content_details
                        on cohort_content_details.cohort_id = cohorts.id
                where
                    cohort_applications_plus.was_accepted = true
                group by
                    cohort_applications_plus.user_id
                    , cohort_applications_plus.cohort_id
                    , cohort_applications_plus.cohort_name
                    , cohorts.start_date
                    , week_index
                    , cohort_content_details.expected_pace_fp
                    , cohort_content_details.expected_pace
                    , cohort_content_details.foundations_lesson_count
                    , cohort_content_details.duration_weeks
                    , cohort_content_details.required_lesson_count
                having
                    cohorts.start_date + (week_index || ' weeks')::interval < now()
            )
            , weeks_2 as (
                select
                    weeks_1.*
                    , cohort_status_changes.status as status_at_week_end
                    , coalesce(active_weeks.lessons_completed_in_week, 0) as lessons_completed_in_week

                from weeks_1
                    join cohort_status_changes
                        on cohort_status_changes.cohort_id = weeks_1.cohort_id
                        and cohort_status_changes.user_id = weeks_1.user_id
                        and cohort_status_changes.from_time <= weeks_1.week_end
                        and cohort_status_changes.until_time > weeks_1.week_end
                    left join active_weeks
                        on weeks_1.user_id = active_weeks.user_id
                        and weeks_1.cohort_id = active_weeks.cohort_id
                        and weeks_1.week_index = active_weeks.week_index
            )
            , weeks_3 as (
                select
                    this_week.*
                    , sum(prev_weeks.lessons_completed_in_week) lessons_completed_by_end_of_week
                from
                    weeks_2 this_week
                        join weeks_2 prev_weeks
                            on this_week.user_id = prev_weeks.user_id
                            and this_week.cohort_id = prev_weeks.cohort_id
                            and this_week.week_index >= prev_weeks.week_index
                group by
                    this_week.user_id
                    , this_week.cohort_name
                    , this_week.cohort_id
                    , this_week.week_index
                    , this_week.week_start
                    , this_week.week_end
                    , this_week.status_at_week_end
                    , this_week.lessons_completed_in_week
                    , this_week.expected_curriculum_lessons_complete
                    , this_week.expected_curriculum_lessons_complete_fp

            )
            select * from weeks_3 order by user_id, week_index
            with no data
        ~

        add_index :cohort_user_weeks, [:cohort_id, :user_id, :week_index], :unique => true

    end

    # use published_cohort_content_details instead of cohort_content_details
    #
    # change in_curriculum to required
    def version_20170310204902
        execute %Q~
            create materialized view cohort_user_weeks as
            with lesson_progress_records as (
                select
                    user_id
                    , cohort_id
                    , completed_at
                    , start_date
                    , case
                        when completed_at < cohorts.start_date then 0
                        else
                            ceil((extract(epoch from completed_at) - extract(epoch from cohorts.start_date)) / extract(epoch from interval '1 week'))
                        end as completed_in_week
                from
                    cohort_user_lesson_progress_records
                    join cohorts on cohort_user_lesson_progress_records.cohort_id = cohorts.id
                where
                    completed_at is not null
                    and required=true
            )
            -- the active_weeks step and the weeks_1 step could be combined, but this
            -- is a lot faster.  For some reason switching the join with lesson_progress_records
            -- to a left join really slows things down
            , active_weeks as (
                select
                    cohort_applications_plus.user_id
                    , cohort_applications_plus.cohort_id
                    , week_index
                    , count(*) as lessons_completed_in_week
                from
                    cohort_applications_plus
                    cross join (
                        SELECT
                            week_index
                        FROM
                            generate_series(0, 26) AS week_index
                    ) user_weeks
                    join lesson_progress_records
                        on cohort_applications_plus.cohort_id = lesson_progress_records.cohort_id
                        and cohort_applications_plus.user_id = lesson_progress_records.user_id
                        and week_index = lesson_progress_records.completed_in_week
                group by
                    cohort_applications_plus.user_id
                    , cohort_applications_plus.cohort_id
                    , week_index
            )
            , weeks_1 as (
                select
                    cohort_applications_plus.user_id
                    , cohort_applications_plus.cohort_id
                    , cohort_applications_plus.cohort_name
                    , week_index
                    , case when week_index = 0 then '2000/01/01'::timestamp else cohorts.start_date + (week_index || ' weeks')::interval end as week_start
                    , cohorts.start_date + ((week_index) || ' weeks')::interval as week_end
                    , week_index*cohort_content_details.expected_pace as expected_curriculum_lessons_complete
                    , cohort_content_details.foundations_lesson_count + week_index*cohort_content_details.expected_pace_fp as expected_curriculum_lessons_complete_fp
                from
                    cohort_applications_plus
                    cross join (
                        SELECT
                            week_index
                        FROM
                            generate_series(0, 26) AS week_index
                    ) user_weeks
                    join cohorts
                        on cohorts.id = cohort_applications_plus.cohort_id
                    join published_cohort_content_details cohort_content_details
                        on cohort_content_details.cohort_id = cohorts.id
                where
                    cohort_applications_plus.was_accepted = true
                group by
                    cohort_applications_plus.user_id
                    , cohort_applications_plus.cohort_id
                    , cohort_applications_plus.cohort_name
                    , cohorts.start_date
                    , week_index
                    , cohort_content_details.expected_pace_fp
                    , cohort_content_details.expected_pace
                    , cohort_content_details.foundations_lesson_count
                having
                    cohorts.start_date + (week_index || ' weeks')::interval < now()
            )
            , weeks_2 as (
                select
                    weeks_1.*
                    , cohort_status_changes.status as status_at_week_end
                    , coalesce(active_weeks.lessons_completed_in_week, 0) as lessons_completed_in_week

                from weeks_1
                    join cohort_status_changes
                        on cohort_status_changes.cohort_id = weeks_1.cohort_id
                        and cohort_status_changes.user_id = weeks_1.user_id
                        and cohort_status_changes.from_time <= weeks_1.week_end
                        and cohort_status_changes.until_time > weeks_1.week_end
                    left join active_weeks
                        on weeks_1.user_id = active_weeks.user_id
                        and weeks_1.cohort_id = active_weeks.cohort_id
                        and weeks_1.week_index = active_weeks.week_index
            )
            , weeks_3 as (
                select
                    this_week.*
                    , sum(prev_weeks.lessons_completed_in_week) lessons_completed_by_end_of_week
                from
                    weeks_2 this_week
                        join weeks_2 prev_weeks
                            on this_week.user_id = prev_weeks.user_id
                            and this_week.cohort_id = prev_weeks.cohort_id
                            and this_week.week_index >= prev_weeks.week_index
                group by
                    this_week.user_id
                    , this_week.cohort_name
                    , this_week.cohort_id
                    , this_week.week_index
                    , this_week.week_start
                    , this_week.week_end
                    , this_week.status_at_week_end
                    , this_week.lessons_completed_in_week
                    , this_week.expected_curriculum_lessons_complete
                    , this_week.expected_curriculum_lessons_complete_fp

            )
            select * from weeks_3 order by user_id, week_index
            with no data
        ~

        add_index :cohort_user_weeks, [:cohort_id, :user_id, :week_index], :unique => true

    end

    # 1. week 0 should end at the start_date, not 1 week later
    # 2. change a '<' to a '<=' so we don't end up filtering out all the weeks that have enrollment deadlines
    def version_20170303185136
        execute %Q~
            create materialized view cohort_user_weeks as
            with lesson_progress_records as (
                select
                    user_id
                    , cohort_id
                    , completed_at
                    , start_date
                    , case
                        when completed_at < cohorts.start_date then 0
                        else
                            ceil((extract(epoch from completed_at) - extract(epoch from cohorts.start_date)) / extract(epoch from interval '1 week'))
                        end as completed_in_week
                from
                    cohort_user_lesson_progress_records
                    join cohorts on cohort_user_lesson_progress_records.cohort_id = cohorts.id
                where
                    completed_at is not null
                    and in_curriculum=true
            )
            -- the active_weeks step and the weeks_1 step could be combined, but this
            -- is a lot faster.  For some reason switching the join with lesson_progress_records
            -- to a left join really slows things down
            , active_weeks as (
                select
                    cohort_applications_plus.user_id
                    , cohort_applications_plus.cohort_id
                    , week_index
                    , count(*) as lessons_completed_in_week
                from
                    cohort_applications_plus
                    cross join (
                        SELECT
                            week_index
                        FROM
                            generate_series(0, 26) AS week_index
                    ) user_weeks
                    join lesson_progress_records
                        on cohort_applications_plus.cohort_id = lesson_progress_records.cohort_id
                        and cohort_applications_plus.user_id = lesson_progress_records.user_id
                        and week_index = lesson_progress_records.completed_in_week
                group by
                    cohort_applications_plus.user_id
                    , cohort_applications_plus.cohort_id
                    , week_index
            )
            , weeks_1 as (
                select
                    cohort_applications_plus.user_id
                    , cohort_applications_plus.cohort_id
                    , cohort_applications_plus.cohort_name
                    , week_index
                    , case when week_index = 0 then '2000/01/01'::timestamp else cohorts.start_date + (week_index || ' weeks')::interval end as week_start
                    , cohorts.start_date + ((week_index) || ' weeks')::interval as week_end
                    , week_index*cohort_content_details.expected_pace as expected_curriculum_lessons_complete
                    , cohort_content_details.foundations_lesson_count + week_index*cohort_content_details.expected_pace_fp as expected_curriculum_lessons_complete_fp
                from
                    cohort_applications_plus
                    cross join (
                        SELECT
                            week_index
                        FROM
                            generate_series(0, 26) AS week_index
                    ) user_weeks
                    join cohorts
                        on cohorts.id = cohort_applications_plus.cohort_id
                    join cohort_content_details
                        on cohort_content_details.cohort_id = cohorts.id
                where
                    cohort_applications_plus.was_accepted = true
                group by
                    cohort_applications_plus.user_id
                    , cohort_applications_plus.cohort_id
                    , cohort_applications_plus.cohort_name
                    , cohorts.start_date
                    , week_index
                    , cohort_content_details.expected_pace_fp
                    , cohort_content_details.expected_pace
                    , cohort_content_details.foundations_lesson_count
                having
                    cohorts.start_date + (week_index || ' weeks')::interval < now()
            )
            , weeks_2 as (
                select
                    weeks_1.*
                    , cohort_status_changes.status as status_at_week_end
                    , coalesce(active_weeks.lessons_completed_in_week, 0) as lessons_completed_in_week

                from weeks_1
                    join cohort_status_changes
                        on cohort_status_changes.cohort_id = weeks_1.cohort_id
                        and cohort_status_changes.user_id = weeks_1.user_id
                        and cohort_status_changes.from_time <= weeks_1.week_end
                        and cohort_status_changes.until_time > weeks_1.week_end
                    left join active_weeks
                        on weeks_1.user_id = active_weeks.user_id
                        and weeks_1.cohort_id = active_weeks.cohort_id
                        and weeks_1.week_index = active_weeks.week_index
            )
            , weeks_3 as (
                select
                    this_week.*
                    , sum(prev_weeks.lessons_completed_in_week) lessons_completed_by_end_of_week
                from
                    weeks_2 this_week
                        join weeks_2 prev_weeks
                            on this_week.user_id = prev_weeks.user_id
                            and this_week.cohort_id = prev_weeks.cohort_id
                            and this_week.week_index >= prev_weeks.week_index
                group by
                    this_week.user_id
                    , this_week.cohort_name
                    , this_week.cohort_id
                    , this_week.week_index
                    , this_week.week_start
                    , this_week.week_end
                    , this_week.status_at_week_end
                    , this_week.lessons_completed_in_week
                    , this_week.expected_curriculum_lessons_complete
                    , this_week.expected_curriculum_lessons_complete_fp

            )
            select * from weeks_3 order by user_id, week_index
            with no data
        ~

        add_index :cohort_user_weeks, [:cohort_id, :user_id, :week_index], :unique => true

    end

    def version_20170221140313
        execute %Q~
            create materialized view cohort_user_weeks as
            with lesson_progress_records as (
                select
                    user_id
                    , cohort_id
                    , completed_at
                    , start_date
                    , case
                        when completed_at < cohorts.start_date then 0
                        else
                            ceil((extract(epoch from completed_at) - extract(epoch from cohorts.start_date)) / extract(epoch from interval '1 week'))
                        end as completed_in_week
                from
                    cohort_user_lesson_progress_records
                    join cohorts on cohort_user_lesson_progress_records.cohort_id = cohorts.id
                where
                    completed_at is not null
                    and in_curriculum=true
            )
            -- the active_weeks step and the weeks_1 step could be combined, but this
            -- is a lot faster.  For some reason switching the join with lesson_progress_records
            -- to a left join really slows things down
            , active_weeks as (
                select
                    cohort_applications_plus.user_id
                    , cohort_applications_plus.cohort_id
                    , week_index
                    , count(*) as lessons_completed_in_week
                from
                    cohort_applications_plus
                    cross join (
                        SELECT
                            week_index
                        FROM
                            generate_series(0, 26) AS week_index
                    ) user_weeks
                    join lesson_progress_records
                        on cohort_applications_plus.cohort_id = lesson_progress_records.cohort_id
                        and cohort_applications_plus.user_id = lesson_progress_records.user_id
                        and week_index = lesson_progress_records.completed_in_week
                group by
                    cohort_applications_plus.user_id
                    , cohort_applications_plus.cohort_id
                    , week_index
            )
            , weeks_1 as (
                select
                    cohort_applications_plus.user_id
                    , cohort_applications_plus.cohort_id
                    , cohort_applications_plus.cohort_name
                    , week_index
                    , case when week_index = 0 then '2000/01/01'::timestamp else cohorts.start_date + (week_index || ' weeks')::interval end as week_start
                    , cohorts.start_date + ((week_index+1) || ' weeks')::interval as week_end
                    , week_index*cohort_content_details.expected_pace as expected_curriculum_lessons_complete
                    , cohort_content_details.foundations_lesson_count + week_index*cohort_content_details.expected_pace_fp as expected_curriculum_lessons_complete_fp
                from
                    cohort_applications_plus
                    cross join (
                        SELECT
                            week_index
                        FROM
                            generate_series(0, 26) AS week_index
                    ) user_weeks
                    join cohorts
                        on cohorts.id = cohort_applications_plus.cohort_id
                    join cohort_content_details
                        on cohort_content_details.cohort_id = cohorts.id
                where
                    cohort_applications_plus.was_accepted = true
                group by
                    cohort_applications_plus.user_id
                    , cohort_applications_plus.cohort_id
                    , cohort_applications_plus.cohort_name
                    , cohorts.start_date
                    , week_index
                    , cohort_content_details.expected_pace_fp
                    , cohort_content_details.expected_pace
                    , cohort_content_details.foundations_lesson_count
                having
                    cohorts.start_date + (week_index || ' weeks')::interval < now()
            )
            , weeks_2 as (
                select
                    weeks_1.*
                    , cohort_status_changes.status as status_at_week_end
                    , coalesce(active_weeks.lessons_completed_in_week, 0) as lessons_completed_in_week

                from weeks_1
                    join cohort_status_changes
                        on cohort_status_changes.cohort_id = weeks_1.cohort_id
                        and cohort_status_changes.user_id = weeks_1.user_id
                        and cohort_status_changes.from_time < weeks_1.week_end
                        and cohort_status_changes.until_time > weeks_1.week_end
                    left join active_weeks
                        on weeks_1.user_id = active_weeks.user_id
                        and weeks_1.cohort_id = active_weeks.cohort_id
                        and weeks_1.week_index = active_weeks.week_index
            )
            , weeks_3 as (
                select
                    this_week.*
                    , sum(prev_weeks.lessons_completed_in_week) lessons_completed_by_end_of_week
                from
                    weeks_2 this_week
                        join weeks_2 prev_weeks
                            on this_week.user_id = prev_weeks.user_id
                            and this_week.cohort_id = prev_weeks.cohort_id
                            and this_week.week_index >= prev_weeks.week_index
                group by
                    this_week.user_id
                    , this_week.cohort_name
                    , this_week.cohort_id
                    , this_week.week_index
                    , this_week.week_start
                    , this_week.week_end
                    , this_week.status_at_week_end
                    , this_week.lessons_completed_in_week
                    , this_week.expected_curriculum_lessons_complete
                    , this_week.expected_curriculum_lessons_complete_fp

            )
            select * from weeks_3 order by user_id, week_index
            with no data
        ~

        add_index :cohort_user_weeks, [:cohort_id, :user_id, :week_index], :unique => true

    end

end