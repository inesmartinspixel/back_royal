class ViewHelpers::ProgramEnrollmentProgressRecords < ViewHelpers::ViewHelperBase
    # -*- SkipSchemaAnnotations

    # Materialized CTEs
    def version_20200625152000
        current
    end

    # - replace consider_graduated_by_deadline with could_graduate_by_deadline
    # - fix issue where the final cohort could be determined wrong
    #       if a user was deferred into a previous cohort
    # - fix issue where graduated_at could be taken from a cohort with a
    #       different program type
    # - ensure that graduated_at is null unless the user is currently
    #       marked as graduated
    def version_20200204160313
        execute %Q~
            create materialized view program_enrollment_progress_records as

            -- Determine which cohort each user first enrolled in.
            --
            -- For the calculations that go into graduation rates, we want to assign each
            -- user to the first cohort they enrolled in, regardless of which cohort they
            -- ended up graduating from after deferring.  So if a user enrolled in MBA1 and
            -- then deferred into MBA3, ze is included in MBA1 for the purposes of calculating
            -- graduation rates.
            --
            -- In the rare cases where a user switched program types, we handle it this way (see original_enrollments_2):
            -- * They are ignored completely in the program type they switched out of
            -- * Their first enrollment is considered to be the first one in their new program type
            --
            with original_enrollments_1 as (
                SELECT
                    user_id
                    , published_cohorts.program_type
                    , min(from_time) as enrolled_at
                    , (array_agg(cohort_id order by from_time))[1] as original_cohort_id
                FROM
                    cohort_status_changes
                    join published_cohorts
                        on published_cohorts.id = cohort_status_changes.cohort_id
                where cohort_status_changes.status = 'enrolled'
                group by user_id, published_cohorts.program_type
            )

            -- remove cases where the user switched to a different program type
            , original_enrollments_2 as (
                SELECT
                    first_enrollment.*
                    , later_enrollment.program_type as pt2
                    , later_enrollment.enrolled_at e2
                FROM
                    original_enrollments_1 first_enrollment
                    left join original_enrollments_1 later_enrollment
                        on first_enrollment.user_id = later_enrollment.user_id
                        and first_enrollment.enrolled_at < later_enrollment.enrolled_at

                -- only keep the last enrollment for each use. Throw away
                -- any where they enrolled in another program type later
                where later_enrollment.program_type is null
            )

            -- Figure out when people graduated (There is also a graduated_at
            -- field on cohort_applications, but is not always set historically,
            -- so we cannot rely on it)
            , graduations as (
                SELECT
                    original_enrollments_2.user_id
                     , original_enrollments_2.program_type
                    , min(cohort_status_changes.from_time) as graduated_at
                from cohort_status_changes
                    join original_enrollments_2
                        on original_enrollments_2.user_id = cohort_status_changes.user_id
                    join cohorts
                        on cohorts.id = cohort_status_changes.cohort_id
                        and cohorts.program_type = original_enrollments_2.program_type
                where status = 'graduated'
                group by original_enrollments_2.user_id
                    , original_enrollments_2.program_type
            )

            -- For each enrolled user, find the last cohort they were in and their
            -- status in that cohort
            , final_statuses_1 as (
                SELECT
                    original_enrollments.user_id
                    , original_enrollments.original_cohort_id
                    , original_enrollments.program_type

                    , count(*) as cohort_count
                    , (array_agg(cohort_user_progress_records.cohort_id order by cohort_applications.applied_at desc))[1] as final_cohort_id
                    , (array_agg(cohort_user_progress_records.status order by cohort_applications.applied_at desc))[1] final_status

                from original_enrollments_2 original_enrollments

                    -- join against ALL of the users' cohort_user_progress_records for the program_type (could have > 1)
                    join cohort_user_progress_records
                        on cohort_user_progress_records.user_id = original_enrollments.user_id
                    join cohort_applications
                        on cohort_applications.user_id = original_enrollments.user_id
                        and cohort_applications.cohort_id = cohort_user_progress_records.cohort_id
                    join published_cohorts
                        on cohort_user_progress_records.cohort_id = published_cohorts.id
                where
                    published_cohorts.program_type = original_enrollments.program_type
                group by
                    original_enrollments.user_id
                    , original_enrollments.original_cohort_id
                    , original_enrollments.program_type
            )
            , final_statuses_2 as (
                select
                    final_statuses_1.*

                     -- We had one case of a user being marked as graduated and then
                     -- unmarked.  Apparently just a mistake.  1032a95a-4309-45cd-b38b-82cf4168a0d1
                     -- So we need to be defensive against that
                    , case when final_status='graduated'
                        then graduated_at
                        else null
                        end as graduated_at
                from final_statuses_1
                    left join graduations
                        using (user_id, program_type)
            )

            -- do some transformations on the statuses to get cases where someone should
            -- be marked as "did_not_graduate"
            , final_statuses_3 as (
                SELECT
                    final_statuses_2.user_id
                    , final_statuses_2.program_type
                    , final_statuses_2.original_cohort_id
                    , final_statuses_2.final_cohort_id
                    , original_cohort_id != final_cohort_id OR final_status='deferred' as deferred_out_of_original_cohort
                    , case

                        -- 3 years matches the grad deadline defined in published_cohrots, which is our
                        -- official limit for how long you have to graduate
                        when final_status = 'deferred' and original_cohorts.start_date < now() - interval '3 years' then 'did_not_graduate'

                        -- since we are only considering learners who enrolled at some point, if they did_not_enroll in their
                        -- last cohort, then we consider them failed
                        when final_status in ('expelled', 'failed', 'did_not_enroll', 'rejected') then 'did_not_graduate'
                        else final_status
                        end as final_status
                    , final_statuses_2.graduated_at
                    , case
                        when final_statuses_2.graduated_at is not null then final_statuses_2.graduated_at < original_cohorts.grad_deadline
                        when final_statuses_2.graduated_at is null and original_cohorts.grad_deadline < now() then false
                        when final_status in ('expelled', 'failed', 'did_not_enroll', 'rejected') then false
                        else true
                        end as could_graduate_by_deadline
                    , final_statuses_2.graduated_at < original_cohorts.grad_deadline as did_graduate_by_deadline
                from final_statuses_2
                    join published_cohorts original_cohorts
                        on original_cohorts.id = final_statuses_2.original_cohort_id
            )
            select * from final_statuses_3
            with no data
        ~

        add_index :program_enrollment_progress_records, :user_id, :unique => true, :name => :user_id_on_enrollment_records

    end

    # update to graduation deadlines.  Set the deadline to 3 years.
    def version_20180801123727
        execute %Q~
            create materialized view program_enrollment_progress_records as

            -- Determine which cohort each user first enrolled in.
            --
            -- For the calculations that go into graduation rates, we want to assign each
            -- user to the first cohort they enrolled in, regardless of which cohort they
            -- ended up graduating from after deferring.  So if a user enrolled in MBA1 and
            -- then deferred into MBA3, ze is included in MBA1 for the purposes of calculating
            -- graduation rates.
            --
            -- In the rare cases where a user switched program types, we handle it this way (see original_enrollments_2):
            -- * They are ignored completely in the program type they switched out of
            -- * Their first enrollment is considered to be the first one in their new program type
            --
            with original_enrollments_1 as (
                SELECT
                    user_id
                    , published_cohorts.program_type
                    , min(from_time) as enrolled_at
                    , (array_agg(cohort_id order by from_time))[1] as original_cohort_id
                FROM
                    cohort_status_changes
                    join published_cohorts
                        on published_cohorts.id = cohort_status_changes.cohort_id
                where cohort_status_changes.status = 'enrolled'
                group by user_id, published_cohorts.program_type
            )

            -- remove cases where the user switched to a different program type
            , original_enrollments_2 as (
                SELECT
                    first_enrollment.*
                    , later_enrollment.program_type as pt2
                    , later_enrollment.enrolled_at e2
                FROM
                    original_enrollments_1 first_enrollment
                    left join original_enrollments_1 later_enrollment
                        on first_enrollment.user_id = later_enrollment.user_id
                        and first_enrollment.enrolled_at < later_enrollment.enrolled_at

                -- only keep the last enrollment for each use. Throw away
                -- any where they enrolled in another program type later
                where later_enrollment.program_type is null
            )

            -- Figure out when people graduated (There is also a graduated_at
            -- field on cohort_applications, but is not always set historically,
            -- so we cannot rely on it)
            , graduations as (
                SELECT
                    original_enrollments_2.user_id
                    , min(cohort_status_changes.from_time) as graduated_at
                from cohort_status_changes
                join original_enrollments_2
                    on original_enrollments_2.user_id = cohort_status_changes.user_id
                where status = 'graduated'
                group by original_enrollments_2.user_id
            )

            -- For each enrolled user, find the last cohort they were in and their
            -- status in that cohort
            , final_statuses_1 as (
                SELECT
                    original_enrollments.user_id
                    , original_enrollments.original_cohort_id
                    , original_enrollments.program_type

                    , count(*) as cohort_count
                    , (array_agg(cohort_user_progress_records.cohort_id order by published_cohorts.start_date desc))[1] as final_cohort_id
                    , (array_agg(cohort_user_progress_records.status order by published_cohorts.start_date desc))[1] final_status

                from original_enrollments_2 original_enrollments

                    -- join against ALL of the users' cohort_user_progress_records for the program_type (could have > 1)
                    join cohort_user_progress_records
                        on cohort_user_progress_records.user_id = original_enrollments.user_id
                    join published_cohorts
                        on cohort_user_progress_records.cohort_id = published_cohorts.id
                where
                    published_cohorts.program_type = original_enrollments.program_type
                group by
                    original_enrollments.user_id
                    , original_enrollments.original_cohort_id
                    , original_enrollments.program_type
            )

            -- do some transformations on the statuses to get cases where someone should
            -- be marked as "did_not_graduate"
            , final_statuses_2 as (
                SELECT
                    final_statuses_1.user_id
                    , final_statuses_1.program_type
                    , final_statuses_1.original_cohort_id
                    , final_statuses_1.final_cohort_id
                    , original_cohort_id != final_cohort_id OR final_status='deferred' as deferred_out_of_original_cohort
                    , case

                        -- 3 years matches the grad deadline defined in published_cohrots, which is our
                        -- official limit for how long you have to graduate
                        when final_status = 'deferred' and original_cohorts.start_date < now() - interval '3 years' then 'did_not_graduate'

                        -- since we are only considering learners who enrolled at some point, if they did_not_enroll in their
                        -- last cohort, then we consider them failed
                        when final_status in ('expelled', 'failed', 'did_not_enroll', 'rejected') then 'did_not_graduate'
                        else final_status
                        end as final_status
                    , graduations.graduated_at
                    , case
                        when graduations.graduated_at is not null then graduations.graduated_at < original_cohorts.grad_deadline
                        when final_status in ('expelled', 'failed', 'did_not_enroll', 'rejected') then false
                        else true
                        end as consider_graduated_by_deadline
                from final_statuses_1
                    join published_cohorts original_cohorts
                        on original_cohorts.id = final_statuses_1.original_cohort_id
                    left join graduations
                        on graduations.user_id = final_statuses_1.user_id
            )
            select * from final_statuses_2
            with no data
        ~

        add_index :program_enrollment_progress_records, :user_id, :unique => true, :name => :user_id_on_enrollment_records
    end

    def version_20180723070738
        execute %Q~
            create materialized view program_enrollment_progress_records as

            -- Determine which cohort each user first enrolled in.
            --
            -- For the calculations that go into graduation rates, we want to assign each
            -- user to the first cohort they enrolled in, regardless of which cohort they
            -- ended up graduating from after deferring.  So if a user enrolled in MBA1 and
            -- then deferred into MBA3, ze is included in MBA1 for the purposes of calculating
            -- graduation rates.
            --
            -- In the rare cases where a user switched program types, we handle it this way (see original_enrollments_2):
            -- * They are ignored completely in the program type they switched out of
            -- * Their first enrollment is considered to be the first one in their new program type
            --
            with original_enrollments_1 as (
                SELECT
                    user_id
                    , published_cohorts.program_type
                    , min(from_time) as enrolled_at
                    , (array_agg(cohort_id order by from_time))[1] as original_cohort_id
                FROM
                    cohort_status_changes
                    join published_cohorts
                        on published_cohorts.id = cohort_status_changes.cohort_id
                where cohort_status_changes.status = 'enrolled'
                group by user_id, published_cohorts.program_type
            )

            -- remove cases where the user switched to a different program type
            , original_enrollments_2 as (
                SELECT
                    first_enrollment.*
                    , later_enrollment.program_type as pt2
                    , later_enrollment.enrolled_at e2
                FROM
                    original_enrollments_1 first_enrollment
                    left join original_enrollments_1 later_enrollment
                        on first_enrollment.user_id = later_enrollment.user_id
                        and first_enrollment.enrolled_at < later_enrollment.enrolled_at

                -- only keep the last enrollment for each use. Throw away
                -- any where they enrolled in another program type later
                where later_enrollment.program_type is null
            )

            -- Figure out when people graduated (There is also a graduated_at
            -- field on cohort_applications, but is not always set historically,
            -- so we cannot rely on it)
            , graduations as (
                SELECT
                    original_enrollments_2.user_id
                    , min(cohort_status_changes.from_time) as graduated_at
                from cohort_status_changes
                join original_enrollments_2
                    on original_enrollments_2.user_id = cohort_status_changes.user_id
                where status = 'graduated'
                group by original_enrollments_2.user_id
            )

            -- For each enrolled user, find the last cohort they were in and their
            -- status in that cohort
            , final_statuses_1 as (
                SELECT
                    original_enrollments.user_id
                    , original_enrollments.original_cohort_id
                    , original_enrollments.program_type

                    , count(*) as cohort_count
                    , (array_agg(cohort_user_progress_records.cohort_id order by published_cohorts.start_date desc))[1] as final_cohort_id
                    , (array_agg(cohort_user_progress_records.status order by published_cohorts.start_date desc))[1] final_status

                from original_enrollments_2 original_enrollments

                    -- join against ALL of the users' cohort_user_progress_records for the program_type (could have > 1)
                    join cohort_user_progress_records
                        on cohort_user_progress_records.user_id = original_enrollments.user_id
                    join published_cohorts
                        on cohort_user_progress_records.cohort_id = published_cohorts.id
                where
                    published_cohorts.program_type = original_enrollments.program_type
                group by
                    original_enrollments.user_id
                    , original_enrollments.original_cohort_id
                    , original_enrollments.program_type
            )

            -- do some transformations on the statuses to get cases where someone should
            -- be marked as "did_not_graduate"
            , final_statuses_2 as (
                SELECT
                    final_statuses_1.user_id
                    , final_statuses_1.program_type
                    , final_statuses_1.original_cohort_id
                    , final_statuses_1.final_cohort_id
                    , original_cohort_id != final_cohort_id OR final_status='deferred' as deferred_out_of_original_cohort
                    , case

                        -- I don't have a great explanation for why this is two years, but it came from discussion with Alexie
                        when final_status = 'deferred' and original_cohorts.start_date < now() - interval '2 years' then 'did_not_graduate'

                        -- since we are only considering learners who enrolled at some point, if they did_not_enroll in their
                        -- last cohort, then we consider them failed
                        when final_status in ('expelled', 'failed', 'did_not_enroll', 'rejected') then 'did_not_graduate'
                        else final_status
                        end as final_status
                    , graduations.graduated_at
                    , case when original_cohorts.grad_deadline_150_perc < now()
                        then coalesce(graduations.graduated_at < original_cohorts.grad_deadline_150_perc, false)
                        else null end as graduated_by_deadline_150_perc
                    , case when original_cohorts.grad_deadline_9_12 < now()
                        then coalesce(graduations.graduated_at < original_cohorts.grad_deadline_9_12, false)
                        else null end as graduated_by_deadline_9_12
                from final_statuses_1
                    join published_cohorts original_cohorts
                        on original_cohorts.id = final_statuses_1.original_cohort_id
                    left join graduations
                        on graduations.user_id = final_statuses_1.user_id
            )
            select * from final_statuses_2
            with no data
        ~

        add_index :program_enrollment_progress_records, :user_id, :unique => true, :name => :user_id_on_enrollment_records

    end

    def version_20180220203724
        execute %Q~
            create materialized view program_enrollment_progress_records as

            -- Determine which cohort each user first enrolled in.
            --
            -- For the calculations that go into graduation rates, we want to assign each
            -- user to the first cohort they enrolled in, regardless of which cohort they
            -- ended up graduating from after deferring.  So if a user enrolled in MBA1 and
            -- then deferred into MBA3, ze is included in MBA1 for the purposes of calculating
            -- graduation rates.
            --
            -- In the rare cases where a user switched program types, we handle it this way (see original_enrollments_2):
            -- * They are ignored completely in the program type they switched out of
            -- * Their first enrollment is considered to be the first one in their new program type
            --
            with original_enrollments_1 as (
                SELECT
                    user_id
                    , published_cohorts.program_type
                    , min(from_time) as enrolled_at
                    , (array_agg(cohort_id order by from_time))[1] as original_cohort_id
                FROM
                    cohort_status_changes
                    join published_cohorts
                        on published_cohorts.id = cohort_status_changes.cohort_id
                where cohort_status_changes.status = 'enrolled'
                group by user_id, published_cohorts.program_type
            )

            -- remove cases where the user switched to a different program type
            , original_enrollments_2 as (
                SELECT
                    first_enrollment.*
                    , later_enrollment.program_type as pt2
                    , later_enrollment.enrolled_at e2
                FROM
                    original_enrollments_1 first_enrollment
                    left join original_enrollments_1 later_enrollment
                        on first_enrollment.user_id = later_enrollment.user_id
                        and first_enrollment.enrolled_at < later_enrollment.enrolled_at

                -- only keep the last enrollment for each use. Throw away
                -- any where they enrolled in another program type later
                where later_enrollment.program_type is null
            )

            -- For each enrolled user, find the last cohort they were in and their
            -- status in that cohort
            , final_statuses_1 as (
                SELECT
                    original_enrollments.user_id
                    , original_enrollments.original_cohort_id
                    , original_enrollments.program_type

                    , count(*) as cohort_count
                    , (array_agg(cohort_user_progress_records.cohort_id order by published_cohorts.start_date desc))[1] as final_cohort_id
                    , (array_agg(cohort_user_progress_records.status order by published_cohorts.start_date desc))[1] final_status

                from original_enrollments_2 original_enrollments

                    -- join against ALL of the users' cohort_user_progress_records for the program_type (could have > 1)
                    join cohort_user_progress_records
                        on cohort_user_progress_records.user_id = original_enrollments.user_id
                    join published_cohorts
                        on cohort_user_progress_records.cohort_id = published_cohorts.id
                where
                    published_cohorts.program_type = original_enrollments.program_type
                group by
                    original_enrollments.user_id
                    , original_enrollments.original_cohort_id
                    , original_enrollments.program_type
            )

            -- do some transformations on the statuses to get cases where someone should
            -- be marked as "did_not_graduate"
            , final_statuses_2 as (
                SELECT
                    final_statuses_1.user_id
                    , final_statuses_1.program_type
                    , final_statuses_1.original_cohort_id
                    , final_statuses_1.final_cohort_id
                    , original_cohort_id != final_cohort_id OR final_status='deferred' as deferred_out_of_original_cohort
                    , case

                        -- I don't have a great explanation for why this is two years, but it came from discussion with Alexie
                        when final_status = 'deferred' and original_cohorts.start_date < now() - interval '2 years' then 'did_not_graduate'

                        -- since we are only considering learners who enrolled at some point, if they did_not_enroll in their
                        -- last cohort, then we consider them failed
                        when final_status in ('expelled', 'failed', 'did_not_enroll', 'rejected') then 'did_not_graduate'
                        else final_status
                        end as final_status
                from final_statuses_1
                    join published_cohorts original_cohorts
                        on original_cohorts.id = final_statuses_1.original_cohort_id
            )
            select * from final_statuses_2
            with no data
        ~

        add_index :program_enrollment_progress_records, :user_id, :unique => true, :name => :user_id_on_enrollment_records

    end
end