class ViewHelpers::ExternalUsers < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    # This version simply modifies the implementation of the external_users view. By modifying
    # the implementation to use EXISTS subquery expressions, queries that utilize the external_users
    # view saw a significant decrease in execution time. Something to note, however, is that while
    # queries that use the external_users view may see a performance increase, the actual implementation
    # of this version is marginally less performant than the previous version when run in isolation.
    def version_20170918143723
        current
    end

    # This version actually does not change anything from the
    # last version.  It is here because this view
    # didn't really always look like this.  We have the
    # old version there to support `rtd` and this version here
    # to make sure that existing databases get changed
    # to the new definition with just an id
    def version_20170807200654
        execute %Q~
            CREATE VIEW external_users AS
            SELECT users.id
            FROM users
                JOIN users_roles ON users_roles.user_id = users.id
                JOIN roles ON users_roles.role_id = roles.id
            WHERE
                roles.name = 'learner'
                AND email NOT LIKE '%pedago.com%'
                AND email NOT LIKE '%smart.ly%'
                AND email NOT LIKE '%workaround.com%';
        ~
    end

    def version_20150604084346
        execute %Q~
            CREATE VIEW external_users AS
            SELECT users.id
            FROM users
                JOIN users_roles ON users_roles.user_id = users.id
                JOIN roles ON users_roles.role_id = roles.id
            WHERE
                roles.name = 'learner'
                AND email NOT LIKE '%pedago.com%'
                AND email NOT LIKE '%smart.ly%'
                AND email NOT LIKE '%workaround.com%';
        ~
    end

    # this view actually used to look like this,
    # with all of the users columns
    # but that was back before ViewHelpers, and other
    # migrations break if we leave it like this in old versions

    #             CREATE VIEW external_users AS
    #             SELECT users.*
    #             FROM users
    #                 JOIN users_roles ON users_roles.user_id = users.id
    #                 JOIN roles ON users_roles.role_id = roles.id
    #             WHERE
    #                 roles.name = 'learner'
    #                 AND email NOT LIKE '%pedago.com%'
    #                 AND email NOT LIKE '%smart.ly%'
    #                 AND email NOT LIKE '%workaround.com%';
end