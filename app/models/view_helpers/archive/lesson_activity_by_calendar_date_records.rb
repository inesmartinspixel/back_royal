class ViewHelpers::LessonActivityByCalendarDateRecords < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    # move from etl schema to public
    def version_20180130030914
        current
    end

    def version_20170403171258
        execute %Q~
            create materialized view etl.lesson_activity_by_calendar_date_records as

            select
                *
            from
                dblink('red_royal',
                    $REDSHIFT$
                        with unload_events as (
                            select

                            user_id

                            --  https://trello.com/c/RgurwSyg/1875-bug-fix-last-seen-column-showing-dates-in-the-future
                            , case
                                when events.estimated_time > getdate() then events.created_at
                                else events.estimated_time
                                end as estimated_time
                            , case when events.duration_total > 120 then 120 else events.duration_total end as duration_total
                            from
                                events
                            where events.event_type in ('lesson:frame:unload')
                        )
                        select
                            user_id
                            , date_trunc('day', estimated_time) as date
                            , sum(duration_total) as total_lesson_seconds
                        from unload_events
                        group by
                            user_id
                            , date_trunc('day', estimated_time)
                    $REDSHIFT$
                ) AS (
                    user_id uuid
                    , date timestamp
                    , total_lesson_seconds float
                )

            with no data
        ~

        add_index :"etl.lesson_activity_by_calendar_date_records", [:date, :user_id], :unique => true, :name => :user_and_date_on_act_by_cal_date_rec

    end

end