class ViewHelpers::PublishedPlaylists < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    def version_20170310204902
        current
    end

    # add in stream_count
    def version_20170302202710
        execute %Q~
            create materialized view published_playlists as
            select
                playlists.id,
                playlists.locale_pack_id,
                playlists_versions.title,
                playlists_versions.version_id,
                playlists_versions.locale,
                playlists_versions.stream_entries,
                array_length(playlists_versions.stream_entries, 1) as stream_count
            from playlists
                join playlists_versions on playlists.id = playlists_versions.id
                join content_publishers on content_publishers.playlist_version_id = playlists_versions.version_id
            with no data

        ~

        add_index :published_playlists, :id, :unique => :true
        add_index :published_playlists, :locale_pack_id
    end

    def version_20170220200244
        execute %Q~
            create materialized view published_playlists as
            select
                playlists.id,
                playlists.locale_pack_id,
                playlists_versions.title,
                playlists_versions.version_id,
                playlists_versions.locale,
                playlists_versions.stream_entries
            from playlists
                join playlists_versions on playlists.id = playlists_versions.id
                join content_publishers on content_publishers.playlist_version_id = playlists_versions.version_id
            with no data

        ~

        add_index :published_playlists, :id, :unique => :true
        add_index :published_playlists, :locale_pack_id
    end

end