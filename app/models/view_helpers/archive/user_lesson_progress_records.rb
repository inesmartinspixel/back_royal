class ViewHelpers::UserLessonProgressRecords < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    # remove in favor of a table generated in write_user_lesson_progress_records
    def version_20170918153357
        current
    end

    def version_20170526143117
        execute %Q~
            create materialized view etl.user_lesson_progress_records as
            with step_1 as (
                select
                    user_lesson_event_aggregates.user_id
                    , lessons.locale_pack_id

                    -- use the time sfrom lesson_progress to ensure we always have agreement with user's UI. Newer code
                    -- should ensure agreements between events and lesson_progress records, but there are some
                    -- issues in older code
                    , lesson_progress.started_at
                    , lesson_progress.completed_at
                    , lesson_progress.best_score as best_score_from_lesson_progress

                    , sum(total_lesson_time) as total_lesson_time
                    , max(last_lesson_activity_time) as last_lesson_activity_time
                    , sum(total_lesson_time_on_desktop) as total_lesson_time_on_desktop
                    , sum(total_lesson_time_on_mobile_app) as total_lesson_time_on_mobile_app
                    , sum(total_lesson_time_on_mobile_web) as total_lesson_time_on_mobile_web
                    , sum(total_lesson_time_on_unknown) as total_lesson_time_on_unknown
                    , min(completed_on_client_type) as completed_on_client_type     -- there should only ever be one anyway, but take the min in the off chance there are more
                    , max(last_lesson_reset_at) as last_lesson_reset_at
                    , sum(lesson_reset_count) as lesson_reset_count
                from
                    dblink('red_royal',
                        $REDSHIFT$
                            with lesson_events as (
                                select
                                    events.user_id
                                    , events.lesson_id

                                    --  https://trello.com/c/RgurwSyg/1875-bug-fix-last-seen-column-showing-dates-in-the-future
                                    , case
                                        when events.estimated_time > getdate() then events.created_at
                                        else events.estimated_time
                                        end as estimated_time
                                    , case when events.duration_total > 120 then 120 else events.duration_total end as duration_total
                                    , events.event_type
                                    , events.score
                                    , case
                                        when page_loads.client in ('ios', 'android')
                                            then 'mobile_app'
                                        when page_loads.os_name in ('Windows Phone', 'iOS', 'blackberry', 'Android', 'Tizen')
                                            then 'mobile_web'
                                        when page_loads.os_name in ('Windows', 'Mac OS', 'Chromium OS', 'Ubuntu')
                                            then 'desktop'
                                        else
                                            'unknown'
                                        end as client_type
                                from
                                events
                                    left join events page_loads
                                        on events.page_load_id = page_loads.page_load_id
                                            and page_loads.event_type='page_load:load'
                                            and page_loads.user_id = events.user_id -- doesn't change result, but should help query planner given our sort keys

                                where events.event_type in ('lesson:frame:unload', 'lesson:start', 'lesson:complete', 'lesson:reset')
                                order by
                                    event_type
                                    , estimated_time -- ordering by estimated_time allows us to find the first assessment score
                            )
                            select
                                user_id
                                , lesson_id
                                , sum(duration_total) as total_lesson_time
                                , min(estimated_time) as started_at
                                , min(case when event_type='lesson:complete' then estimated_time else null end) as completed_at
                                , max(estimated_time) as last_lesson_activity_time
                                , sum(case
                                    when client_type='mobile_app' and duration_total is not null
                                        then duration_total
                                    else 0
                                    end) as total_lesson_time_on_mobile_app
                                , sum(case
                                    when client_type='mobile_web' and duration_total is not null
                                        then duration_total
                                    else 0
                                    end) as total_lesson_time_on_mobile_web
                                , sum(case
                                    when client_type='desktop' and duration_total is not null
                                        then duration_total
                                    else 0
                                    end) as total_lesson_time_on_desktop
                                , sum(case
                                    when client_type='unknown' and duration_total is not null
                                        then duration_total
                                    else 0
                                    end) as total_lesson_time_on_unknown
                                , min(case when event_type='lesson:complete' then client_type else null end) as completed_on_client_type
                                , max(case when event_type='lesson:reset' then estimated_time else null end) as lesson_lesson_reset_at
                                , count(case when event_type='lesson:reset' then true else null end) as lesson_reset_count
                            from lesson_events
                            group by
                                user_id
                                , lesson_id
                        $REDSHIFT$) AS user_lesson_event_aggregates (
                            user_id uuid
                            , lesson_id uuid
                            , total_lesson_time float
                            , started_at timestamp
                            , completed_at timestamp
                            , last_lesson_activity_time timestamp
                            , total_lesson_time_on_mobile_app float
                            , total_lesson_time_on_mobile_web float
                            , total_lesson_time_on_desktop float
                            , total_lesson_time_on_unknown float
                            , completed_on_client_type text
                            , last_lesson_reset_at timestamp
                            , lesson_reset_count int
                        )
                    join lessons
                        on lessons.id = user_lesson_event_aggregates.lesson_id
                    join lesson_progress
                        on lesson_progress.locale_pack_id = lessons.locale_pack_id
                            and lesson_progress.user_id = user_lesson_event_aggregates.user_id
                group by
                    user_lesson_event_aggregates.user_id
                    , lessons.locale_pack_id
                    , lesson_progress.completed_at
                    , lesson_progress.started_at
                    , lesson_progress.best_score
            )
            , lesson_finishes as (
                select * from
                dblink('red_royal', $REDSHIFT$
                    select
                        events.user_id
                        --  https://trello.com/c/RgurwSyg/1875-bug-fix-last-seen-column-showing-dates-in-the-future
                        , case
                            when events.estimated_time > getdate() then events.created_at
                            else events.estimated_time
                            end as estimated_time
                        , events.lesson_id
                        , events.score
                    from
                        events
                    where event_type='lesson:finish'
                $REDSHIFT$) AS lesson_finishes (
                            user_id uuid
                            , estimated_time timestamp
                            , lesson_id uuid
                            , score float
                        )
            )
            , lesson_finish_info as (
                select
                    lesson_finishes.user_id
                    , lessons.locale_pack_id
                    , sum(case when assessment then 1 else 0 end) > 0 as assessment
                    , sum(case when test then 1 else 0 end) > 0 as test
                    , count(*) as lesson_finish_count
                    , case when sum(case when assessment or test then 1 else 0 end) > 0 then
                        case when old_assessment_scores.average_assessment_score_first is not null then
                            old_assessment_scores.average_assessment_score_first
                        else
                            (array_remove(array_agg(lesson_finishes.score order by estimated_time asc), null))[1]
                        end
                      else null
                      end as average_assessment_score_first
                    , old_assessment_scores.average_assessment_score_last best_score_from_old_assessment_scores
                from lesson_finishes
                join published_lessons lessons
                    on lessons.id = lesson_finishes.lesson_id
                left join etl.old_assessment_scores
                        on lessons.locale_pack_id = old_assessment_scores.locale_pack_id
                        and lesson_finishes.user_id = old_assessment_scores.user_id
                group by
                    lesson_finishes.user_id
                    , lessons.locale_pack_id
                    , old_assessment_scores.average_assessment_score_first
                    , old_assessment_scores.average_assessment_score_last
            )
            , step_2 as (
                select
                    step_1.*
                    , lesson_finish_info.test
                    , lesson_finish_info.assessment
                    , lesson_finish_info.lesson_finish_count
                    , lesson_finish_info.average_assessment_score_first
                    , case when assessment or test then
                        case when best_score_from_lesson_progress is null or best_score_from_old_assessment_scores > best_score_from_lesson_progress then
                            best_score_from_old_assessment_scores
                        else
                            best_score_from_lesson_progress
                        end
                      else
                        null
                      end as average_assessment_score_best
                from step_1
                    left join lesson_finish_info
                    on step_1.user_id = lesson_finish_info.user_id
                        and step_1.locale_pack_id = lesson_finish_info.locale_pack_id
            )
            , step_3 as (
                select
                    step_2.*

                    -- We alias average_assessment_score_best as official_test_score just to indicate
                    -- that this is the score we should be using when calculating test scores to report
                    -- to users, final scores for determining graduation, etc.
                    -- Probably, we would prefer to use the first test score, rather than the best, as the
                    -- official test score, but there are two reasons we use the best score
                    -- instead.
                    --
                    -- 1. (It's easier right now)  We need the official test score to be available in real-time to users, and
                    --      right now we already have the best_score in lesson_progress.  By
                    --      using best score both in reports and in real-time, we make sure there are
                    --      no edge cases where we show the user one score but then use a different score
                    --      in our reporting or analysis.  Nothing would
                    --      have prevented us from storing the first_score in lesson_progress and then
                    --      using that in both place, we just
                    --      never did so now it would be extra work to go back and add it.

                    -- 2. (It makes no difference) Except where we have other bugs, the first score and the best score should be
                    --      the same for test lessons, since users are not allowed to re-take them.  As of
                    --      2017/05/29 there was one instance in our db where a user had managed to re-take a
                    --      test lesson.  Using best score instead of first score means that, when we mess up
                    --      and let a user re-take a test lesson, we then will give them credit for the better
                    --      score when we would probably prefer to keep the first score. Big deal.
                    , case when test then average_assessment_score_best else null end as official_test_score
                from
                    step_2
            )
            select
                *
            from step_3
            with no data
        ~

        add_index 'etl.user_lesson_progress_records', [:user_id, :locale_pack_id], :unique => :true, :name => :user_and_lp_on_ul_prog_rec
    end

    # change the max time allowed in a frame from 60 seconds to 120 seconds
    def version_20170403171258
        execute %Q~
            create materialized view etl.user_lesson_progress_records as
            with step_1 as (
                select
                    user_lesson_event_aggregates.user_id
                    , lessons.locale_pack_id

                    -- use the time sfrom lesson_progress to ensure we always have agreement with user's UI. Newer code
                    -- should ensure agreements between events and lesson_progress records, but there are some
                    -- issues in older code
                    , lesson_progress.started_at
                    , lesson_progress.completed_at

                    , sum(total_lesson_time) as total_lesson_time
                    , max(last_lesson_activity_time) as last_lesson_activity_time
                    , sum(total_lesson_time_on_desktop) as total_lesson_time_on_desktop
                    , sum(total_lesson_time_on_mobile_app) as total_lesson_time_on_mobile_app
                    , sum(total_lesson_time_on_mobile_web) as total_lesson_time_on_mobile_web
                    , sum(total_lesson_time_on_unknown) as total_lesson_time_on_unknown
                    , min(completed_on_client_type) as completed_on_client_type     -- there should only ever be one anyway, but take the min in the off chance there are more
                    , max(last_lesson_reset_at) as last_lesson_reset_at
                    , sum(lesson_reset_count) as lesson_reset_count
                from
                    dblink('red_royal',
                        $REDSHIFT$
                            with lesson_events as (
                                select
                                    events.user_id
                                    , events.lesson_id

                                    --  https://trello.com/c/RgurwSyg/1875-bug-fix-last-seen-column-showing-dates-in-the-future
                                    , case
                                        when events.estimated_time > getdate() then events.created_at
                                        else events.estimated_time
                                        end as estimated_time
                                    , case when events.duration_total > 120 then 120 else events.duration_total end as duration_total
                                    , events.event_type
                                    , events.score
                                    , case
                                        when page_loads.client in ('ios', 'android')
                                            then 'mobile_app'
                                        when page_loads.os_name in ('Windows Phone', 'iOS', 'blackberry', 'Android', 'Tizen')
                                            then 'mobile_web'
                                        when page_loads.os_name in ('Windows', 'Mac OS', 'Chromium OS', 'Ubuntu')
                                            then 'desktop'
                                        else
                                            'unknown'
                                        end as client_type
                                from
                                events
                                    left join events page_loads
                                        on events.page_load_id = page_loads.page_load_id
                                            and page_loads.event_type='page_load:load'
                                            and page_loads.user_id = events.user_id -- doesn't change result, but should help query planner given our sort keys

                                where events.event_type in ('lesson:frame:unload', 'lesson:start', 'lesson:complete', 'lesson:reset')
                                order by
                                    event_type
                                    , estimated_time -- ordering by estimated_time allows us to find the first assessment score
                            )
                            select
                                user_id
                                , lesson_id
                                , sum(duration_total) as total_lesson_time
                                , min(estimated_time) as started_at
                                , min(case when event_type='lesson:complete' then estimated_time else null end) as completed_at
                                , max(estimated_time) as last_lesson_activity_time
                                , sum(case
                                    when client_type='mobile_app' and duration_total is not null
                                        then duration_total
                                    else 0
                                    end) as total_lesson_time_on_mobile_app
                                , sum(case
                                    when client_type='mobile_web' and duration_total is not null
                                        then duration_total
                                    else 0
                                    end) as total_lesson_time_on_mobile_web
                                , sum(case
                                    when client_type='desktop' and duration_total is not null
                                        then duration_total
                                    else 0
                                    end) as total_lesson_time_on_desktop
                                , sum(case
                                    when client_type='unknown' and duration_total is not null
                                        then duration_total
                                    else 0
                                    end) as total_lesson_time_on_unknown
                                , min(case when event_type='lesson:complete' then client_type else null end) as completed_on_client_type
                                , max(case when event_type='lesson:reset' then estimated_time else null end) as lesson_lesson_reset_at
                                , count(case when event_type='lesson:reset' then true else null end) as lesson_reset_count
                            from lesson_events
                            group by
                                user_id
                                , lesson_id
                        $REDSHIFT$) AS user_lesson_event_aggregates (
                            user_id uuid
                            , lesson_id uuid
                            , total_lesson_time float
                            , started_at timestamp
                            , completed_at timestamp
                            , last_lesson_activity_time timestamp
                            , total_lesson_time_on_mobile_app float
                            , total_lesson_time_on_mobile_web float
                            , total_lesson_time_on_desktop float
                            , total_lesson_time_on_unknown float
                            , completed_on_client_type text
                            , last_lesson_reset_at timestamp
                            , lesson_reset_count int
                        )
                    join lessons
                        on lessons.id = user_lesson_event_aggregates.lesson_id
                    join lesson_progress
                        on lesson_progress.locale_pack_id = lessons.locale_pack_id
                            and lesson_progress.user_id = user_lesson_event_aggregates.user_id
                group by
                    user_lesson_event_aggregates.user_id
                    , lessons.locale_pack_id
                    , lesson_progress.completed_at
                    , lesson_progress.started_at
            )
            , lesson_finishes as (
                select * from
                dblink('red_royal', $REDSHIFT$
                    select
                        events.user_id
                        --  https://trello.com/c/RgurwSyg/1875-bug-fix-last-seen-column-showing-dates-in-the-future
                        , case
                            when events.estimated_time > getdate() then events.created_at
                            else events.estimated_time
                            end as estimated_time
                        , events.lesson_id
                        , events.score
                    from
                        events
                    where event_type='lesson:finish'
                $REDSHIFT$) AS lesson_finishes (
                            user_id uuid
                            , estimated_time timestamp
                            , lesson_id uuid
                            , score float
                        )
            )
            , lesson_finish_info as (
                select
                    lesson_finishes.user_id
                    , lessons.locale_pack_id
                    , count(*) as lesson_finish_count
                    , case when sum(case when assessment or test then 1 else 0 end) > 0 then
                        case when old_assessment_scores.average_assessment_score_first is not null then
                            old_assessment_scores.average_assessment_score_first
                        else
                            (array_remove(array_agg(score order by estimated_time asc), null))[1]
                        end
                      else null
                      end as average_assessment_score_first
                    , case when sum(case when assessment or test then 1 else 0 end) > 0 then
                       case when old_assessment_scores.average_assessment_score_last is not null then
                            old_assessment_scores.average_assessment_score_last
                        else
                            max(score)
                        end
                      else null
                      end as average_assessment_score_best
                from lesson_finishes
                join published_lessons lessons
                    on lessons.id = lesson_finishes.lesson_id
                left join etl.old_assessment_scores
                        on lessons.locale_pack_id = old_assessment_scores.locale_pack_id
                        and lesson_finishes.user_id = old_assessment_scores.user_id
                group by
                    lesson_finishes.user_id
                    , lessons.locale_pack_id
                    , old_assessment_scores.average_assessment_score_first
                    , old_assessment_scores.average_assessment_score_last
            )
            , step_2 as (
                select
                    step_1.*
                    , lesson_finish_info.lesson_finish_count
                    , lesson_finish_info.average_assessment_score_first
                    , lesson_finish_info.average_assessment_score_best
                    , lesson_finish_info.average_assessment_score_best as average_assessment_score_last -- support old clients.  This can be removed soon
                from step_1
                    left join lesson_finish_info
                    on step_1.user_id = lesson_finish_info.user_id
                        and step_1.locale_pack_id = lesson_finish_info.locale_pack_id
            )
            select
                *
            from step_2
            with no data
        ~

        add_index 'etl.user_lesson_progress_records', [:user_id, :locale_pack_id], :unique => :true, :name => :user_and_lp_on_ul_prog_rec
    end

    # dynamically decide whether to use now() or getdate() based on connection
    def version_20170302180457
        execute %Q~
            create materialized view etl.user_lesson_progress_records as
            with step_1 as (
                select
                    user_lesson_event_aggregates.user_id
                    , lessons.locale_pack_id

                    -- use the time sfrom lesson_progress to ensure we always have agreement with user's UI. Newer code
                    -- should ensure agreements between events and lesson_progress records, but there are some
                    -- issues in older code
                    , lesson_progress.started_at
                    , lesson_progress.completed_at

                    , sum(total_lesson_time) as total_lesson_time
                    , max(last_lesson_activity_time) as last_lesson_activity_time
                    , sum(total_lesson_time_on_desktop) as total_lesson_time_on_desktop
                    , sum(total_lesson_time_on_mobile_app) as total_lesson_time_on_mobile_app
                    , sum(total_lesson_time_on_mobile_web) as total_lesson_time_on_mobile_web
                    , sum(total_lesson_time_on_unknown) as total_lesson_time_on_unknown
                    , min(completed_on_client_type) as completed_on_client_type     -- there should only ever be one anyway, but take the min in the off chance there are more
                    , max(last_lesson_reset_at) as last_lesson_reset_at
                    , sum(lesson_reset_count) as lesson_reset_count
                from
                    dblink('red_royal',
                        $REDSHIFT$
                            with lesson_events as (
                                select
                                    events.user_id
                                    , events.lesson_id

                                    --  https://trello.com/c/RgurwSyg/1875-bug-fix-last-seen-column-showing-dates-in-the-future
                                    , case
                                        when events.estimated_time > getdate() then events.created_at
                                        else events.estimated_time
                                        end as estimated_time
                                    , case when events.duration_total > 60 then 60 else events.duration_total end as duration_total
                                    , events.event_type
                                    , events.score
                                    , case
                                        when page_loads.client in ('ios', 'android')
                                            then 'mobile_app'
                                        when page_loads.os_name in ('Windows Phone', 'iOS', 'blackberry', 'Android', 'Tizen')
                                            then 'mobile_web'
                                        when page_loads.os_name in ('Windows', 'Mac OS', 'Chromium OS', 'Ubuntu')
                                            then 'desktop'
                                        else
                                            'unknown'
                                        end as client_type
                                from
                                events
                                    left join events page_loads
                                        on events.page_load_id = page_loads.page_load_id
                                            and page_loads.event_type='page_load:load'
                                            and page_loads.user_id = events.user_id -- doesn't change result, but should help query planner given our sort keys

                                where events.event_type in ('lesson:frame:unload', 'lesson:start', 'lesson:complete', 'lesson:reset')
                                order by
                                    event_type
                                    , estimated_time -- ordering by estimated_time allows us to find the first assessment score
                            )
                            select
                                user_id
                                , lesson_id
                                , sum(duration_total) as total_lesson_time
                                , min(estimated_time) as started_at
                                , min(case when event_type='lesson:complete' then estimated_time else null end) as completed_at
                                , max(estimated_time) as last_lesson_activity_time
                                , sum(case
                                    when client_type='mobile_app' and duration_total is not null
                                        then duration_total
                                    else 0
                                    end) as total_lesson_time_on_mobile_app
                                , sum(case
                                    when client_type='mobile_web' and duration_total is not null
                                        then duration_total
                                    else 0
                                    end) as total_lesson_time_on_mobile_web
                                , sum(case
                                    when client_type='desktop' and duration_total is not null
                                        then duration_total
                                    else 0
                                    end) as total_lesson_time_on_desktop
                                , sum(case
                                    when client_type='unknown' and duration_total is not null
                                        then duration_total
                                    else 0
                                    end) as total_lesson_time_on_unknown
                                , min(case when event_type='lesson:complete' then client_type else null end) as completed_on_client_type
                                , max(case when event_type='lesson:reset' then estimated_time else null end) as lesson_lesson_reset_at
                                , count(case when event_type='lesson:reset' then true else null end) as lesson_reset_count
                            from lesson_events
                            group by
                                user_id
                                , lesson_id
                        $REDSHIFT$) AS user_lesson_event_aggregates (
                            user_id uuid
                            , lesson_id uuid
                            , total_lesson_time float
                            , started_at timestamp
                            , completed_at timestamp
                            , last_lesson_activity_time timestamp
                            , total_lesson_time_on_mobile_app float
                            , total_lesson_time_on_mobile_web float
                            , total_lesson_time_on_desktop float
                            , total_lesson_time_on_unknown float
                            , completed_on_client_type text
                            , last_lesson_reset_at timestamp
                            , lesson_reset_count int
                        )
                    join lessons
                        on lessons.id = user_lesson_event_aggregates.lesson_id
                    join lesson_progress
                        on lesson_progress.locale_pack_id = lessons.locale_pack_id
                            and lesson_progress.user_id = user_lesson_event_aggregates.user_id
                group by
                    user_lesson_event_aggregates.user_id
                    , lessons.locale_pack_id
                    , lesson_progress.completed_at
                    , lesson_progress.started_at
            )
            , lesson_finishes as (
                select * from
                dblink('red_royal', $REDSHIFT$
                    select
                        events.user_id
                        --  https://trello.com/c/RgurwSyg/1875-bug-fix-last-seen-column-showing-dates-in-the-future
                        , case
                            when events.estimated_time > getdate() then events.created_at
                            else events.estimated_time
                            end as estimated_time
                        , events.lesson_id
                        , events.score
                    from
                        events
                    where event_type='lesson:finish'
                $REDSHIFT$) AS lesson_finishes (
                            user_id uuid
                            , estimated_time timestamp
                            , lesson_id uuid
                            , score float
                        )
            )
            , lesson_finish_info as (
                select
                    lesson_finishes.user_id
                    , lessons.locale_pack_id
                    , count(*) as lesson_finish_count
                    , case when sum(case when assessment or test then 1 else 0 end) > 0 then
                        case when old_assessment_scores.average_assessment_score_first is not null then
                            old_assessment_scores.average_assessment_score_first
                        else
                            (array_remove(array_agg(score order by estimated_time asc), null))[1]
                        end
                      else null
                      end as average_assessment_score_first
                    , case when sum(case when assessment or test then 1 else 0 end) > 0 then
                       case when old_assessment_scores.average_assessment_score_last is not null then
                            old_assessment_scores.average_assessment_score_last
                        else
                            max(score)
                        end
                      else null
                      end as average_assessment_score_best
                from lesson_finishes
                join published_lessons lessons
                    on lessons.id = lesson_finishes.lesson_id
                left join etl.old_assessment_scores
                        on lessons.locale_pack_id = old_assessment_scores.locale_pack_id
                        and lesson_finishes.user_id = old_assessment_scores.user_id
                group by
                    lesson_finishes.user_id
                    , lessons.locale_pack_id
                    , old_assessment_scores.average_assessment_score_first
                    , old_assessment_scores.average_assessment_score_last
            )
            , step_2 as (
                select
                    step_1.*
                    , lesson_finish_info.lesson_finish_count
                    , lesson_finish_info.average_assessment_score_first
                    , lesson_finish_info.average_assessment_score_best
                    , lesson_finish_info.average_assessment_score_best as average_assessment_score_last -- support old clients.  This can be removed soon
                from step_1
                    left join lesson_finish_info
                    on step_1.user_id = lesson_finish_info.user_id
                        and step_1.locale_pack_id = lesson_finish_info.locale_pack_id
            )
            select
                *
            from step_2
            with no data
        ~

        add_index 'etl.user_lesson_progress_records', [:user_id, :locale_pack_id], :unique => :true, :name => :user_and_lp_on_ul_prog_rec
    end

    # do not show any times in the future: https://trello.com/c/RgurwSyg/1875-bug-fix-last-seen-column-showing-dates-in-the-future
    def version_20170302155419
        execute %Q~
            create materialized view etl.user_lesson_progress_records as
            with step_1 as (
                select
                    user_lesson_event_aggregates.user_id
                    , lessons.locale_pack_id

                    -- use the time sfrom lesson_progress to ensure we always have agreement with user's UI. Newer code
                    -- should ensure agreements between events and lesson_progress records, but there are some
                    -- issues in older code
                    , lesson_progress.started_at
                    , lesson_progress.completed_at

                    , sum(total_lesson_time) as total_lesson_time
                    , max(last_lesson_activity_time) as last_lesson_activity_time
                    , sum(total_lesson_time_on_desktop) as total_lesson_time_on_desktop
                    , sum(total_lesson_time_on_mobile_app) as total_lesson_time_on_mobile_app
                    , sum(total_lesson_time_on_mobile_web) as total_lesson_time_on_mobile_web
                    , sum(total_lesson_time_on_unknown) as total_lesson_time_on_unknown
                    , min(completed_on_client_type) as completed_on_client_type     -- there should only ever be one anyway, but take the min in the off chance there are more
                    , max(last_lesson_reset_at) as last_lesson_reset_at
                    , sum(lesson_reset_count) as lesson_reset_count
                from
                    dblink('red_royal',
                        $REDSHIFT$
                            with lesson_events as (
                                select
                                    events.user_id
                                    , events.lesson_id

                                    --  https://trello.com/c/RgurwSyg/1875-bug-fix-last-seen-column-showing-dates-in-the-future
                                    , case
                                        when events.estimated_time > getdate() then events.created_at
                                        else events.estimated_time
                                        end as estimated_time
                                    , case when events.duration_total > 60 then 60 else events.duration_total end as duration_total
                                    , events.event_type
                                    , events.score
                                    , case
                                        when page_loads.client in ('ios', 'android')
                                            then 'mobile_app'
                                        when page_loads.os_name in ('Windows Phone', 'iOS', 'blackberry', 'Android', 'Tizen')
                                            then 'mobile_web'
                                        when page_loads.os_name in ('Windows', 'Mac OS', 'Chromium OS', 'Ubuntu')
                                            then 'desktop'
                                        else
                                            'unknown'
                                        end as client_type
                                from
                                events
                                    left join events page_loads
                                        on events.page_load_id = page_loads.page_load_id
                                            and page_loads.event_type='page_load:load'
                                            and page_loads.user_id = events.user_id -- doesn't change result, but should help query planner given our sort keys

                                where events.event_type in ('lesson:frame:unload', 'lesson:start', 'lesson:complete', 'lesson:reset')
                                order by
                                    event_type
                                    , estimated_time -- ordering by estimated_time allows us to find the first assessment score
                            )
                            select
                                user_id
                                , lesson_id
                                , sum(duration_total) as total_lesson_time
                                , min(estimated_time) as started_at
                                , min(case when event_type='lesson:complete' then estimated_time else null end) as completed_at
                                , max(estimated_time) as last_lesson_activity_time
                                , sum(case
                                    when client_type='mobile_app' and duration_total is not null
                                        then duration_total
                                    else 0
                                    end) as total_lesson_time_on_mobile_app
                                , sum(case
                                    when client_type='mobile_web' and duration_total is not null
                                        then duration_total
                                    else 0
                                    end) as total_lesson_time_on_mobile_web
                                , sum(case
                                    when client_type='desktop' and duration_total is not null
                                        then duration_total
                                    else 0
                                    end) as total_lesson_time_on_desktop
                                , sum(case
                                    when client_type='unknown' and duration_total is not null
                                        then duration_total
                                    else 0
                                    end) as total_lesson_time_on_unknown
                                , min(case when event_type='lesson:complete' then client_type else null end) as completed_on_client_type
                                , max(case when event_type='lesson:reset' then estimated_time else null end) as lesson_lesson_reset_at
                                , count(case when event_type='lesson:reset' then true else null end) as lesson_reset_count
                            from lesson_events
                            group by
                                user_id
                                , lesson_id
                        $REDSHIFT$) AS user_lesson_event_aggregates (
                            user_id uuid
                            , lesson_id uuid
                            , total_lesson_time float
                            , started_at timestamp
                            , completed_at timestamp
                            , last_lesson_activity_time timestamp
                            , total_lesson_time_on_mobile_app float
                            , total_lesson_time_on_mobile_web float
                            , total_lesson_time_on_desktop float
                            , total_lesson_time_on_unknown float
                            , completed_on_client_type text
                            , last_lesson_reset_at timestamp
                            , lesson_reset_count int
                        )
                    join lessons
                        on lessons.id = user_lesson_event_aggregates.lesson_id
                    join lesson_progress
                        on lesson_progress.locale_pack_id = lessons.locale_pack_id
                            and lesson_progress.user_id = user_lesson_event_aggregates.user_id
                group by
                    user_lesson_event_aggregates.user_id
                    , lessons.locale_pack_id
                    , lesson_progress.completed_at
                    , lesson_progress.started_at
            )
            , lesson_finishes as (
                select * from
                dblink('red_royal', $REDSHIFT$
                    select
                        events.user_id
                        --  https://trello.com/c/RgurwSyg/1875-bug-fix-last-seen-column-showing-dates-in-the-future
                        , case
                            when events.estimated_time > getdate() then events.created_at
                            else events.estimated_time
                            end as estimated_time
                        , events.lesson_id
                        , events.score
                    from
                        events
                    where event_type='lesson:finish'
                $REDSHIFT$) AS lesson_finishes (
                            user_id uuid
                            , estimated_time timestamp
                            , lesson_id uuid
                            , score float
                        )
            )
            , lesson_finish_info as (
                select
                    lesson_finishes.user_id
                    , lessons.locale_pack_id
                    , count(*) as lesson_finish_count
                    , case when sum(case when assessment or test then 1 else 0 end) > 0 then
                        case when old_assessment_scores.average_assessment_score_first is not null then
                            old_assessment_scores.average_assessment_score_first
                        else
                            (array_remove(array_agg(score order by estimated_time asc), null))[1]
                        end
                      else null
                      end as average_assessment_score_first
                    , case when sum(case when assessment or test then 1 else 0 end) > 0 then
                       case when old_assessment_scores.average_assessment_score_last is not null then
                            old_assessment_scores.average_assessment_score_last
                        else
                            max(score)
                        end
                      else null
                      end as average_assessment_score_best
                from lesson_finishes
                join published_lessons lessons
                    on lessons.id = lesson_finishes.lesson_id
                left join etl.old_assessment_scores
                        on lessons.locale_pack_id = old_assessment_scores.locale_pack_id
                        and lesson_finishes.user_id = old_assessment_scores.user_id
                group by
                    lesson_finishes.user_id
                    , lessons.locale_pack_id
                    , old_assessment_scores.average_assessment_score_first
                    , old_assessment_scores.average_assessment_score_last
            )
            , step_2 as (
                select
                    step_1.*
                    , lesson_finish_info.lesson_finish_count
                    , lesson_finish_info.average_assessment_score_first
                    , lesson_finish_info.average_assessment_score_best
                    , lesson_finish_info.average_assessment_score_best as average_assessment_score_last -- support old clients.  This can be removed soon
                from step_1
                    left join lesson_finish_info
                    on step_1.user_id = lesson_finish_info.user_id
                        and step_1.locale_pack_id = lesson_finish_info.locale_pack_id
            )
            select
                *
            from step_2
            with no data
        ~

        add_index 'etl.user_lesson_progress_records', [:user_id, :locale_pack_id], :unique => :true, :name => :user_and_lp_on_ul_prog_rec
    end

    # switch from a left join to an inner join with lesson_progress, to
    # ensure that we don't have lessons included with no started_at or completed_at
    # that are not visible in the UI
    def version_20170301211232
        execute %Q~
            create materialized view etl.user_lesson_progress_records as
            with step_1 as (
                select
                    user_lesson_event_aggregates.user_id
                    , lessons.locale_pack_id

                    -- use the time sfrom lesson_progress to ensure we always have agreement with user's UI. Newer code
                    -- should ensure agreements between events and lesson_progress records, but there are some
                    -- issues in older code
                    , lesson_progress.started_at
                    , lesson_progress.completed_at

                    , sum(total_lesson_time) as total_lesson_time
                    , max(last_lesson_activity_time) as last_lesson_activity_time
                    , sum(total_lesson_time_on_desktop) as total_lesson_time_on_desktop
                    , sum(total_lesson_time_on_mobile_app) as total_lesson_time_on_mobile_app
                    , sum(total_lesson_time_on_mobile_web) as total_lesson_time_on_mobile_web
                    , sum(total_lesson_time_on_unknown) as total_lesson_time_on_unknown
                    , min(completed_on_client_type) as completed_on_client_type     -- there should only ever be one anyway, but take the min in the off chance there are more
                    , max(last_lesson_reset_at) as last_lesson_reset_at
                    , sum(lesson_reset_count) as lesson_reset_count
                from
                    dblink('red_royal',
                        $REDSHIFT$
                            with lesson_events as (
                                select
                                    events.user_id
                                    , events.lesson_id
                                    , events.estimated_time
                                    , case when events.duration_total > 60 then 60 else events.duration_total end as duration_total
                                    , events.event_type
                                    , events.score
                                    , case
                                        when page_loads.client in ('ios', 'android')
                                            then 'mobile_app'
                                        when page_loads.os_name in ('Windows Phone', 'iOS', 'blackberry', 'Android', 'Tizen')
                                            then 'mobile_web'
                                        when page_loads.os_name in ('Windows', 'Mac OS', 'Chromium OS', 'Ubuntu')
                                            then 'desktop'
                                        else
                                            'unknown'
                                        end as client_type
                                from
                                events
                                    left join events page_loads
                                        on events.page_load_id = page_loads.page_load_id
                                            and page_loads.event_type='page_load:load'
                                            and page_loads.user_id = events.user_id -- doesn't change result, but should help query planner given our sort keys

                                where events.event_type in ('lesson:frame:unload', 'lesson:start', 'lesson:complete', 'lesson:reset')
                                order by
                                    event_type
                                    , estimated_time -- ordering by estimated_time allows us to find the first assessment score
                            )
                            select
                                user_id
                                , lesson_id
                                , sum(duration_total) as total_lesson_time
                                , min(estimated_time) as started_at
                                , min(case when event_type='lesson:complete' then estimated_time else null end) as completed_at
                                , max(estimated_time) as last_lesson_activity_time
                                , sum(case
                                    when client_type='mobile_app' and duration_total is not null
                                        then duration_total
                                    else 0
                                    end) as total_lesson_time_on_mobile_app
                                , sum(case
                                    when client_type='mobile_web' and duration_total is not null
                                        then duration_total
                                    else 0
                                    end) as total_lesson_time_on_mobile_web
                                , sum(case
                                    when client_type='desktop' and duration_total is not null
                                        then duration_total
                                    else 0
                                    end) as total_lesson_time_on_desktop
                                , sum(case
                                    when client_type='unknown' and duration_total is not null
                                        then duration_total
                                    else 0
                                    end) as total_lesson_time_on_unknown
                                , min(case when event_type='lesson:complete' then client_type else null end) as completed_on_client_type
                                , max(case when event_type='lesson:reset' then estimated_time else null end) as lesson_lesson_reset_at
                                , count(case when event_type='lesson:reset' then true else null end) as lesson_reset_count
                            from lesson_events
                            group by
                                user_id
                                , lesson_id
                        $REDSHIFT$) AS user_lesson_event_aggregates (
                            user_id uuid
                            , lesson_id uuid
                            , total_lesson_time float
                            , started_at timestamp
                            , completed_at timestamp
                            , last_lesson_activity_time timestamp
                            , total_lesson_time_on_mobile_app float
                            , total_lesson_time_on_mobile_web float
                            , total_lesson_time_on_desktop float
                            , total_lesson_time_on_unknown float
                            , completed_on_client_type text
                            , last_lesson_reset_at timestamp
                            , lesson_reset_count int
                        )
                    join lessons
                        on lessons.id = user_lesson_event_aggregates.lesson_id
                    join lesson_progress
                        on lesson_progress.locale_pack_id = lessons.locale_pack_id
                            and lesson_progress.user_id = user_lesson_event_aggregates.user_id
                group by
                    user_lesson_event_aggregates.user_id
                    , lessons.locale_pack_id
                    , lesson_progress.completed_at
                    , lesson_progress.started_at
            )
            , lesson_finishes as (
                select * from
                dblink('red_royal', $REDSHIFT$
                    select
                        events.user_id
                        , events.estimated_time
                        , events.lesson_id
                        , events.score
                    from
                        events
                    where event_type='lesson:finish'
                $REDSHIFT$) AS lesson_finishes (
                            user_id uuid
                            , estimated_time timestamp
                            , lesson_id uuid
                            , score float
                        )
            )
            , lesson_finish_info as (
                select
                    lesson_finishes.user_id
                    , lessons.locale_pack_id
                    , count(*) as lesson_finish_count
                    , case when sum(case when assessment or test then 1 else 0 end) > 0 then
                        case when old_assessment_scores.average_assessment_score_first is not null then
                            old_assessment_scores.average_assessment_score_first
                        else
                            (array_remove(array_agg(score order by estimated_time asc), null))[1]
                        end
                      else null
                      end as average_assessment_score_first
                    , case when sum(case when assessment or test then 1 else 0 end) > 0 then
                       case when old_assessment_scores.average_assessment_score_last is not null then
                            old_assessment_scores.average_assessment_score_last
                        else
                            max(score)
                        end
                      else null
                      end as average_assessment_score_best
                from lesson_finishes
                join published_lessons lessons
                    on lessons.id = lesson_finishes.lesson_id
                left join etl.old_assessment_scores
                        on lessons.locale_pack_id = old_assessment_scores.locale_pack_id
                        and lesson_finishes.user_id = old_assessment_scores.user_id
                group by
                    lesson_finishes.user_id
                    , lessons.locale_pack_id
                    , old_assessment_scores.average_assessment_score_first
                    , old_assessment_scores.average_assessment_score_last
            )
            , step_2 as (
                select
                    step_1.*
                    , lesson_finish_info.lesson_finish_count
                    , lesson_finish_info.average_assessment_score_first
                    , lesson_finish_info.average_assessment_score_best
                    , lesson_finish_info.average_assessment_score_best as average_assessment_score_last -- support old clients.  This can be removed soon
                from step_1
                    left join lesson_finish_info
                    on step_1.user_id = lesson_finish_info.user_id
                        and step_1.locale_pack_id = lesson_finish_info.locale_pack_id
            )
            select
                *
            from step_2
            with no data
        ~

        add_index 'etl.user_lesson_progress_records', [:user_id, :locale_pack_id], :unique => :true, :name => :user_and_lp_on_ul_prog_rec
    end

    def version_20170221140313
        execute %Q~
            create materialized view etl.user_lesson_progress_records as
            with step_1 as (
                select
                    user_lesson_event_aggregates.user_id
                    , lessons.locale_pack_id

                    -- use the time sfrom lesson_progress to ensure we always have agreement with user's UI. Newer code
                    -- should ensure agreements between events and lesson_progress records, but there are some
                    -- issues in older code
                    , lesson_progress.started_at
                    , lesson_progress.completed_at

                    , sum(total_lesson_time) as total_lesson_time
                    , max(last_lesson_activity_time) as last_lesson_activity_time
                    , sum(total_lesson_time_on_desktop) as total_lesson_time_on_desktop
                    , sum(total_lesson_time_on_mobile_app) as total_lesson_time_on_mobile_app
                    , sum(total_lesson_time_on_mobile_web) as total_lesson_time_on_mobile_web
                    , sum(total_lesson_time_on_unknown) as total_lesson_time_on_unknown
                    , min(completed_on_client_type) as completed_on_client_type     -- there should only ever be one anyway, but take the min in the off chance there are more
                    , max(last_lesson_reset_at) as last_lesson_reset_at
                    , sum(lesson_reset_count) as lesson_reset_count
                from
                    dblink('red_royal',
                        $REDSHIFT$
                            with lesson_events as (
                                select
                                    events.user_id
                                    , events.lesson_id
                                    , events.estimated_time
                                    , case when events.duration_total > 60 then 60 else events.duration_total end as duration_total
                                    , events.event_type
                                    , events.score
                                    , case
                                        when page_loads.client in ('ios', 'android')
                                            then 'mobile_app'
                                        when page_loads.os_name in ('Windows Phone', 'iOS', 'blackberry', 'Android', 'Tizen')
                                            then 'mobile_web'
                                        when page_loads.os_name in ('Windows', 'Mac OS', 'Chromium OS', 'Ubuntu')
                                            then 'desktop'
                                        else
                                            'unknown'
                                        end as client_type
                                from
                                events
                                    left join events page_loads
                                        on events.page_load_id = page_loads.page_load_id
                                            and page_loads.event_type='page_load:load'
                                            and page_loads.user_id = events.user_id -- doesn't change result, but should help query planner given our sort keys

                                where events.event_type in ('lesson:frame:unload', 'lesson:start', 'lesson:complete', 'lesson:reset')
                                order by
                                    event_type
                                    , estimated_time -- ordering by estimated_time allows us to find the first assessment score
                            )
                            select
                                user_id
                                , lesson_id
                                , sum(duration_total) as total_lesson_time
                                , min(estimated_time) as started_at
                                , min(case when event_type='lesson:complete' then estimated_time else null end) as completed_at
                                , max(estimated_time) as last_lesson_activity_time
                                , sum(case
                                    when client_type='mobile_app' and duration_total is not null
                                        then duration_total
                                    else 0
                                    end) as total_lesson_time_on_mobile_app
                                , sum(case
                                    when client_type='mobile_web' and duration_total is not null
                                        then duration_total
                                    else 0
                                    end) as total_lesson_time_on_mobile_web
                                , sum(case
                                    when client_type='desktop' and duration_total is not null
                                        then duration_total
                                    else 0
                                    end) as total_lesson_time_on_desktop
                                , sum(case
                                    when client_type='unknown' and duration_total is not null
                                        then duration_total
                                    else 0
                                    end) as total_lesson_time_on_unknown
                                , min(case when event_type='lesson:complete' then client_type else null end) as completed_on_client_type
                                , max(case when event_type='lesson:reset' then estimated_time else null end) as lesson_lesson_reset_at
                                , count(case when event_type='lesson:reset' then true else null end) as lesson_reset_count
                            from lesson_events
                            group by
                                user_id
                                , lesson_id
                        $REDSHIFT$) AS user_lesson_event_aggregates (
                            user_id uuid
                            , lesson_id uuid
                            , total_lesson_time float
                            , started_at timestamp
                            , completed_at timestamp
                            , last_lesson_activity_time timestamp
                            , total_lesson_time_on_mobile_app float
                            , total_lesson_time_on_mobile_web float
                            , total_lesson_time_on_desktop float
                            , total_lesson_time_on_unknown float
                            , completed_on_client_type text
                            , last_lesson_reset_at timestamp
                            , lesson_reset_count int
                        )
                    join lessons
                        on lessons.id = user_lesson_event_aggregates.lesson_id
                    left join lesson_progress
                        on lesson_progress.locale_pack_id = lessons.locale_pack_id
                            and lesson_progress.user_id = user_lesson_event_aggregates.user_id
                group by
                    user_lesson_event_aggregates.user_id
                    , lessons.locale_pack_id
                    , lesson_progress.completed_at
                    , lesson_progress.started_at
            )
            , lesson_finishes as (
                select * from
                dblink('red_royal', $REDSHIFT$
                    select
                        events.user_id
                        , events.estimated_time
                        , events.lesson_id
                        , events.score
                    from
                        events
                    where event_type='lesson:finish'
                $REDSHIFT$) AS lesson_finishes (
                            user_id uuid
                            , estimated_time timestamp
                            , lesson_id uuid
                            , score float
                        )
            )
            , lesson_finish_info as (
                select
                    lesson_finishes.user_id
                    , lessons.locale_pack_id
                    , count(*) as lesson_finish_count
                    , case when sum(case when assessment or test then 1 else 0 end) > 0 then
                        case when old_assessment_scores.average_assessment_score_first is not null then
                            old_assessment_scores.average_assessment_score_first
                        else
                            (array_remove(array_agg(score order by estimated_time asc), null))[1]
                        end
                      else null
                      end as average_assessment_score_first
                    , case when sum(case when assessment or test then 1 else 0 end) > 0 then
                       case when old_assessment_scores.average_assessment_score_last is not null then
                            old_assessment_scores.average_assessment_score_last
                        else
                            max(score)
                        end
                      else null
                      end as average_assessment_score_best
                from lesson_finishes
                join published_lessons lessons
                    on lessons.id = lesson_finishes.lesson_id
                left join etl.old_assessment_scores
                        on lessons.locale_pack_id = old_assessment_scores.locale_pack_id
                        and lesson_finishes.user_id = old_assessment_scores.user_id
                group by
                    lesson_finishes.user_id
                    , lessons.locale_pack_id
                    , old_assessment_scores.average_assessment_score_first
                    , old_assessment_scores.average_assessment_score_last
            )
            , step_2 as (
                select
                    step_1.*
                    , lesson_finish_info.lesson_finish_count
                    , lesson_finish_info.average_assessment_score_first
                    , lesson_finish_info.average_assessment_score_best
                    , lesson_finish_info.average_assessment_score_best as average_assessment_score_last -- support old clients.  This can be removed soon
                from step_1
                    left join lesson_finish_info
                    on step_1.user_id = lesson_finish_info.user_id
                        and step_1.locale_pack_id = lesson_finish_info.locale_pack_id
            )
            select
                *
            from step_2
            with no data
        ~

        add_index 'etl.user_lesson_progress_records', [:user_id, :locale_pack_id], :unique => :true, :name => :user_and_lp_on_ul_prog_rec
    end

end