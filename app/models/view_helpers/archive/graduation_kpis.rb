class ViewHelpers::GraduationKpis < ViewHelpers::ViewHelperBase
    # -*- SkipSchemaAnnotations

    # Materialized CTEs
    def version_20200625152000
        current
    end

    def version_20200204160313
        execute %Q~
            create view graduation_kpis as
            with has_graduation_expectations as (
                SELECT
                    original_cohort_id cohort_id
                    , cohorts.name
                    , count(user_chance_of_graduating_records.user_id) > 0 as has_graduation_expectations
                from
                    program_enrollment_progress_records
                    join cohorts on cohorts.id = original_cohort_id
                    left join user_chance_of_graduating_records using (user_id)
                group by
                    original_cohort_id
                    , cohorts.name
            )
            , cohort_results_1 as (
                SELECT
                    cohort_conversion_rates.program_type as program_type
                    , cohorts.name as name
                    , num_first_enrolled as enrolled
                    , num_did_graduate_by_deadline as graduated_by_deadline
                    , num_could_graduate_by_deadline as could_graduate_by_deadline
                    , num_graduated as graduated
                    , num_did_not_graduate as failed
                    , num_currently_deferred + num_currently_enrolled_in_orig_cohort + num_currently_enrolled_in_later_cohort as could_still_graduate
                    , case
                        when has_graduation_expectations then expected_grad_rate*num_first_enrolled
                        else null
                        end as num_expected_to_graduate
                    , case
                        when has_graduation_expectations then num_first_enrolled
                        else null
                        end as expected_rate_denominator
                    , num_graduated > 0 as completed
                    , cohorts.start_date
                    , add_dst_aware_offset(c.end_date, c.graduation_days_offset_from_end, 'days') as graduation_date
                    , '3' || cohort_conversion_rates.program_type || cohorts.start_date::text as sort_key
                FROM cohort_conversion_rates
                    join published_cohorts cohorts on cohorts.id = cohort_conversion_rates.cohort_id
                    join cohorts c on c.id = cohorts.id
                    join has_graduation_expectations on has_graduation_expectations.cohort_id = cohorts.id
                WHERE  cohort_conversion_rates.program_type in ('mba', 'emba')
                    AND cohorts.enrollment_deadline < now()
                ORDER by program_type, cohorts.start_date
            )
            , cohort_results as (
                select *
                    , graduated - graduated_by_deadline as graduated_after_deadline
                    , could_graduate_by_deadline - graduated_by_deadline as could_still_graduate_by_deadline
                    , could_still_graduate - (could_graduate_by_deadline - graduated_by_deadline) as could_still_graduate_after_deadline
                from cohort_results_1
            )
            , yearly_results as (
                select
                    program_type
                    , program_type || '/' || years.year::text as name
                    , null::timestamp as start_date
                    , null::timestamp as graduation_date
                    , (array_agg(name order by start_date asc))[1] || '-' || (array_agg(name order by start_date desc))[1] as cohorts

                    , sum(enrolled) as enrolled
                    , sum(failed) as failed
                    , sum(graduated_by_deadline) as graduated_by_deadline
                    , sum(graduated_after_deadline) as graduated_after_deadline
                    , sum(could_still_graduate_by_deadline) as could_still_graduate_by_deadline
                    , sum(could_still_graduate_after_deadline) as could_still_graduate_after_deadline
                    , sum(could_graduate_by_deadline) as could_graduate_by_deadline

                    -- There are no expected grad rates for MBA1.  Because of this,
                    -- the expected grad rate (calculated for MBA2-MBA3) ends up being
                    -- higher than the max_rate.  This is just confusing, so null
                    -- out the expected rate.  Almost all of these people are done
                    -- anyway, so we don't really need the expected rate
                    , case when year=2016 and program_type='mba'
                        then null
                        else sum(num_expected_to_graduate)
                    end num_expected_to_graduate
                    , sum(expected_rate_denominator) as expected_rate_denominator
                    , '0' || program_type || (3000-years.year)::text as sort_key
                    , true as show_min_max
                from
                    cohort_results
                    join (
                        SELECT
                            year
                        FROM
                            generate_series(2016, date_part('year', now())::int) AS year
                    ) years on years.year = date_part('year', cohort_results.start_date)::int
                group by
                    program_type
                    , years.year
            )
            , completed_cohort_results as (
                select
                    program_type
                    , program_type || '/completed' as name
                    , null::timestamp as start_date
                    , null::timestamp as graduation_date
                    , 'up to ' || (array_agg(name order by start_date desc))[1] as cohorts

                    , sum(enrolled) as enrolled
                    , sum(failed) as failed
                    , sum(graduated_by_deadline) as graduated_by_deadline
                    , sum(graduated_after_deadline) as graduated_after_deadline
                    , sum(could_still_graduate_by_deadline) as could_still_graduate_by_deadline
                    , sum(could_still_graduate_after_deadline) as could_still_graduate_after_deadline
                    , sum(could_graduate_by_deadline) as could_graduate_by_deadline

                    , sum(num_expected_to_graduate) num_expected_to_graduate
                    , sum(expected_rate_denominator) as expected_rate_denominator
                    , '1' || program_type as sort_key
                    , true as show_min_max
                from cohort_results
                where completed = true
                group by program_type
                order by program_type
            )
            , all_cohort_results as (
                select
                    program_type
                    , program_type || '/all' as name
                    , null::timestamp as start_date
                    , null::timestamp as graduation_date
                    , 'up to ' || (array_agg(name order by start_date desc))[1] as cohorts

                    , sum(enrolled) as enrolled
                    , sum(failed) as failed
                    , sum(graduated_by_deadline) as graduated_by_deadline
                    , sum(graduated_after_deadline) as graduated_after_deadline
                    , sum(could_still_graduate_by_deadline) as could_still_graduate_by_deadline
                    , sum(could_still_graduate_after_deadline) as could_still_graduate_after_deadline
                    , sum(could_graduate_by_deadline) as could_graduate_by_deadline

                    , sum(num_expected_to_graduate) num_expected_to_graduate
                    , sum(expected_rate_denominator) as expected_rate_denominator
                    , '2' || program_type as sort_key
                    , false as show_min_max
                from cohort_results
                group by program_type
                order by program_type
            )
            , combined as (
                SELECT *
                    , 'completed_cohorts' as type
                FROM completed_cohort_results
                UNION
                SELECT *
                    , 'all_cohorts' as type
                FROM all_cohort_results
                UNION
                SELECT *
                    , 'yearly' as type
                FROM yearly_results
                UNION
                SELECT
                    program_type
                    , name
                    , start_date
                    , graduation_date
                    , null::text as cohorts

                    , enrolled
                    , failed
                    , graduated_by_deadline
                    , graduated_after_deadline
                    , could_still_graduate_by_deadline
                    , could_still_graduate_after_deadline
                    , could_graduate_by_deadline

                    , num_expected_to_graduate
                    , expected_rate_denominator
                    , sort_key
                    , true as show_min_max
                    , 'cohort' as type
                FROM cohort_results

                ORDER BY sort_key
            )
            select
                name
                , start_date
                , graduation_date
                , cohorts
                , round(100*num_expected_to_graduate::float/expected_rate_denominator) as expected_rate
                , round(100*could_graduate_by_deadline::float/enrolled) as official_grad_rate
                , case when show_min_max then round(100*graduated_by_deadline::float / enrolled) else null end as min_rate
                , case when show_min_max then round(100*could_graduate_by_deadline::float/enrolled) else null end as max_rate
                , enrolled
                , failed
                , graduated_by_deadline
                , could_still_graduate_by_deadline
                , graduated_after_deadline
                , could_still_graduate_after_deadline
                , round(num_expected_to_graduate) num_expected_to_graduate -- this is calculated above by multiplying a rate with an integer, so round it
                , type
            from combined;
        ~

    end

end