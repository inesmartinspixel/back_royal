class ViewHelpers::CohortPlaylistLocalePacks < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    def version_20170310204902
        # do nothing. This view has been renamed to published_cohort_playlist_locale_packs
    end

    def version_20170306001915
        execute %Q|
            create materialized view cohort_playlist_locale_packs as
            with cohort_playlist_locale_packs_1 as (
                select
                    cohorts.id as cohort_id,
                    cohorts.name as cohort_name,
                    unnest(playlist_pack_ids) as playlist_locale_pack_id
                from published_cohorts cohorts
            )
            , cohort_playlist_locale_packs_2 as (
                select
                    cohort_playlist_locale_packs_1.cohort_id,
                    cohort_playlist_locale_packs_1.cohort_name,
                    published_playlist_locale_packs.locale_pack_id = (select playlist_pack_ids[1] from cohorts where id = cohort_id) as foundations,
                    published_playlist_locale_packs.title as playlist_title,
                    published_playlist_locale_packs.locales as playlist_locales,
                    published_playlist_locale_packs.locale_pack_id as playlist_locale_pack_id
                from cohort_playlist_locale_packs_1
                    join published_playlist_locale_packs on published_playlist_locale_packs.locale_pack_id = cohort_playlist_locale_packs_1.playlist_locale_pack_id
            )

            select * from cohort_playlist_locale_packs_2
            with no data
        |

        add_index :cohort_playlist_locale_packs, [:cohort_id, :playlist_locale_pack_id], :unique => true, :name => :cohort_and_play_lp_on_coh_play_lps
        add_index :cohort_playlist_locale_packs, :playlist_locale_pack_id
    end

    def version_20170220200244
        execute %Q|
            create materialized view cohort_playlist_locale_packs as
            with cohort_playlist_locale_packs_1 as (
                select
                    cohorts.id as cohort_id,
                    cohorts.name_locales->>'en' as cohort_name,
                    unnest(playlist_pack_ids) as playlist_locale_pack_id
                from published_cohorts cohorts
            )
            , cohort_playlist_locale_packs_2 as (
                select
                    cohort_playlist_locale_packs_1.cohort_id,
                    cohort_playlist_locale_packs_1.cohort_name,
                    published_playlist_locale_packs.locale_pack_id = (select playlist_pack_ids[1] from cohorts where id = cohort_id) as foundations,
                    published_playlist_locale_packs.title as playlist_title,
                    published_playlist_locale_packs.locales as playlist_locales,
                    published_playlist_locale_packs.locale_pack_id as playlist_locale_pack_id
                from cohort_playlist_locale_packs_1
                    join published_playlist_locale_packs on published_playlist_locale_packs.locale_pack_id = cohort_playlist_locale_packs_1.playlist_locale_pack_id
            )

            select * from cohort_playlist_locale_packs_2
            with no data
        |

        add_index :cohort_playlist_locale_packs, [:cohort_id, :playlist_locale_pack_id], :unique => true, :name => :cohort_and_play_lp_on_coh_play_lps
        add_index :cohort_playlist_locale_packs, :playlist_locale_pack_id
    end

end