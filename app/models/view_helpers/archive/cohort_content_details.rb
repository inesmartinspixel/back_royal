class ViewHelpers::CohortContentDetails < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    def version_20170310204902
        # do nothing.  renamed to published_cohort_content_details
    end

     # add enrollment deadine
    def version_20170228160118
        execute %Q~
            create materialized view cohort_content_details as
            with step_1 as (
                select
                    cohort_lesson_locale_packs.cohort_id
                    , cohort_lesson_locale_packs.cohort_name
                    , array_length(cohorts.playlist_pack_ids, 1) as playlist_count
                    , count(distinct case when cohort_stream_locale_packs.foundations then cohort_stream_locale_packs.stream_locale_pack_id else null end) as foundations_stream_count
                    , count(distinct case when cohort_stream_locale_packs.in_curriculum then cohort_stream_locale_packs.stream_locale_pack_id else null end) as in_curriculum_stream_count
                    , count(distinct case when cohort_stream_locale_packs.exam then cohort_stream_locale_packs.stream_locale_pack_id else null end) as exam_stream_count
                    , count(distinct case when cohort_stream_locale_packs.elective then cohort_stream_locale_packs.stream_locale_pack_id else null end) as elective_stream_count
                    , count(distinct case when cohort_lesson_locale_packs.foundations then cohort_lesson_locale_packs.lesson_locale_pack_id else null end) as foundations_lesson_count
                    , count(distinct case when cohort_lesson_locale_packs.in_curriculum then cohort_lesson_locale_packs.lesson_locale_pack_id else null end) as in_curriculum_lesson_count
                    , count(distinct case when cohort_lesson_locale_packs.test then cohort_lesson_locale_packs.lesson_locale_pack_id else null end) as test_lesson_count
                    , count(distinct case when cohort_lesson_locale_packs.elective then cohort_lesson_locale_packs.lesson_locale_pack_id else null end) as elective_lesson_count

                    -- duration weeks is the number of weeks from the start to end - 1 week for the final exam
                    , floor(EXTRACT(epoch FROM (cohorts.end_date - cohorts.start_date)) / extract(epoch from interval '7 days')) - 1 duration_weeks
                    , case
                        -- MBA1 and MBA2 use 5 weeks
                        when cohorts.id in ('8f9f77d1-069b-4ab4-a8b0-28d9d3ab29f2', '1789b5f0-0436-4e65-b521-3c19f63a71db') then cohorts.start_date + interval '5 weeks'
                        else cohorts.start_date + interval '3 weeks'
                        end as enrollment_deadline
                from
                    cohort_lesson_locale_packs
                    join cohort_stream_locale_packs
                        on cohort_lesson_locale_packs.stream_locale_pack_id = cohort_stream_locale_packs.stream_locale_pack_id
                    join published_cohorts cohorts
                        on cohorts.id = cohort_lesson_locale_packs.cohort_id
                group by
                    cohort_lesson_locale_packs.cohort_id
                    , cohort_lesson_locale_packs.cohort_name
                    , cohorts.playlist_pack_ids
                    , cohorts.end_date
                    , cohorts.start_date
                    , cohorts.id
            )
            select
                step_1.*
                , in_curriculum_lesson_count / duration_weeks as expected_pace
                , (in_curriculum_lesson_count - foundations_lesson_count) / duration_weeks as expected_pace_fp
            from step_1
            with no data
        ~

        add_index :cohort_content_details, :cohort_id, :unique => true
    end

    def version_20170220200244
        execute %Q~
            create materialized view cohort_content_details as
            with step_1 as (
                select
                    cohort_lesson_locale_packs.cohort_id
                    , cohort_lesson_locale_packs.cohort_name
                    , array_length(cohorts.playlist_pack_ids, 1) as playlist_count
                    , count(distinct case when cohort_stream_locale_packs.foundations then cohort_stream_locale_packs.stream_locale_pack_id else null end) as foundations_stream_count
                    , count(distinct case when cohort_stream_locale_packs.in_curriculum then cohort_stream_locale_packs.stream_locale_pack_id else null end) as in_curriculum_stream_count
                    , count(distinct case when cohort_stream_locale_packs.exam then cohort_stream_locale_packs.stream_locale_pack_id else null end) as exam_stream_count
                    , count(distinct case when cohort_stream_locale_packs.elective then cohort_stream_locale_packs.stream_locale_pack_id else null end) as elective_stream_count
                    , count(distinct case when cohort_lesson_locale_packs.foundations then cohort_lesson_locale_packs.lesson_locale_pack_id else null end) as foundations_lesson_count
                    , count(distinct case when cohort_lesson_locale_packs.in_curriculum then cohort_lesson_locale_packs.lesson_locale_pack_id else null end) as in_curriculum_lesson_count
                    , count(distinct case when cohort_lesson_locale_packs.test then cohort_lesson_locale_packs.lesson_locale_pack_id else null end) as test_lesson_count
                    , count(distinct case when cohort_lesson_locale_packs.elective then cohort_lesson_locale_packs.lesson_locale_pack_id else null end) as elective_lesson_count

                    -- duration weeks is the number of weeks from the start to end - 1 week for the final exam
                    , floor(EXTRACT(epoch FROM (cohorts.end_date - cohorts.start_date)) / extract(epoch from interval '7 days')) - 1 duration_weeks
                from
                    cohort_lesson_locale_packs
                    join cohort_stream_locale_packs
                        on cohort_lesson_locale_packs.stream_locale_pack_id = cohort_stream_locale_packs.stream_locale_pack_id
                    join cohorts
                        on cohorts.id = cohort_lesson_locale_packs.cohort_id
                group by
                    cohort_lesson_locale_packs.cohort_id
                    , cohort_lesson_locale_packs.cohort_name
                    , cohorts.playlist_pack_ids
                    , cohorts.end_date
                    , cohorts.start_date
            )
            select
                step_1.*
                , in_curriculum_lesson_count / duration_weeks as expected_pace
                , (in_curriculum_lesson_count - foundations_lesson_count) / duration_weeks as expected_pace_fp
            from step_1
            with no data
        ~

        add_index :cohort_content_details, :cohort_id, :unique => true
    end

end