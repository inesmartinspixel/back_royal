class ViewHelpers::UserCanSeePlaylistLocalePacks < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    # stop materilalizing the view to try to save cpu, since this is a lot
    # of data to write but probably not a difficult join to run at runtime
    def version_20170918194659
        current
    end

    def version_20170601135311

        execute %Q~
            create materialized view user_can_see_playlist_locale_packs as

            -- playlists from cohorts
            select
                distinct users.id as user_id
                , unnest(relevant_cohorts.required_playlist_pack_ids || relevant_cohorts.specialization_playlist_pack_ids) as locale_pack_id
            from
                users
                    join users_relevant_cohorts
                        on users_relevant_cohorts.user_id = users.id
                    join published_cohorts relevant_cohorts
                        on relevant_cohorts.id = users_relevant_cohorts.cohort_id

            union

            -- playlists from institutions
            select
                distinct institutions_users.user_id
                , unnest(institutions.playlist_pack_ids) as locale_pack_id
            from institutions_users
                join institutions on institutions.id = institutions_users.institution_id

            with no data
        ~

        add_index :user_can_see_playlist_locale_packs, [:user_id, :locale_pack_id], :unique => true, :name => :user_and_lp_on_user_can_see_play
    end

    # switch from users.program_type to users.default_program_type
    def version_20170405194031

        execute %Q~
            create materialized view user_can_see_playlist_locale_packs as

            -- playlists from cohorts
            select
                distinct users.id as user_id
                , unnest(case
                            when pending_or_accepted_cohorts.id is not null then
                                pending_or_accepted_cohorts.required_playlist_pack_ids || pending_or_accepted_cohorts.specialization_playlist_pack_ids
                            else
                                promoted_cohorts.required_playlist_pack_ids || promoted_cohorts.specialization_playlist_pack_ids
                            end) as locale_pack_id
            from
                users

                -- join against cohorts for which this user has a pending or accepted application
                left join
                    cohort_applications on cohort_applications.user_id = users.id
                left join published_cohorts pending_or_accepted_cohorts
                    on pending_or_accepted_cohorts.id = cohort_applications.cohort_id
                        and cohort_applications.status in ('accepted', 'pending')

                -- and, if there are no pending or accepted ones, join against promoted
                -- cohorts where the program_type matches the user's fallback_program_type
                left join published_cohorts promoted_cohorts
                    on pending_or_accepted_cohorts.id is null
                        and promoted_cohorts.promoted = true
                        and users.fallback_program_type = promoted_cohorts.program_type

            union

            -- playlists from institutions
            select
                distinct institutions_users.user_id
                , unnest(institutions.playlist_pack_ids) as locale_pack_id
            from institutions_users
                join institutions on institutions.id = institutions_users.institution_id

            with no data
        ~

        add_index :user_can_see_playlist_locale_packs, [:user_id, :locale_pack_id], :unique => true, :name => :user_and_lp_on_user_can_see_play
    end

    # program_type on users instead of mba_enabled
    def version_20170329132726

        execute %Q~
            create materialized view user_can_see_playlist_locale_packs as

            -- playlists from cohorts
            select
                distinct users.id as user_id
                , unnest(case
                            when accepted_into_cohorts.id is not null then
                                accepted_into_cohorts.required_playlist_pack_ids || accepted_into_cohorts.specialization_playlist_pack_ids
                            else
                                promoted_cohorts.required_playlist_pack_ids || promoted_cohorts.specialization_playlist_pack_ids
                            end) as locale_pack_id
            from
                users
                left join
                    cohort_applications on cohort_applications.user_id = users.id
                left join published_cohorts accepted_into_cohorts
                    on accepted_into_cohorts.id = cohort_applications.cohort_id
                        and cohort_applications.status = 'accepted'
                left join published_cohorts promoted_cohorts
                    on accepted_into_cohorts.id is null
                        and promoted_cohorts.promoted = true
                        and users.program_type = promoted_cohorts.program_type

            union

            -- playlists from institutions
            select
                distinct institutions_users.user_id
                , unnest(institutions.playlist_pack_ids) as locale_pack_id
            from institutions_users
                join institutions on institutions.id = institutions_users.institution_id

            with no data
        ~

        add_index :user_can_see_playlist_locale_packs, [:user_id, :locale_pack_id], :unique => true, :name => :user_and_lp_on_user_can_see_play

    end

    # update for required and specialization playlists
    def version_20170310204902

        execute %Q~
            create materialized view user_can_see_playlist_locale_packs as

            -- playlists from cohorts
            select
                distinct users.id as user_id
                , unnest(case
                            when accepted_into_cohorts.id is not null then
                                accepted_into_cohorts.required_playlist_pack_ids || accepted_into_cohorts.specialization_playlist_pack_ids
                            else
                                promoted_cohorts.required_playlist_pack_ids || promoted_cohorts.specialization_playlist_pack_ids
                            end) as locale_pack_id
            from
                users
                left join
                    cohort_applications on cohort_applications.user_id = users.id
                left join published_cohorts accepted_into_cohorts
                    on accepted_into_cohorts.id = cohort_applications.cohort_id
                        and cohort_applications.status = 'accepted'
                left join published_cohorts promoted_cohorts
                    on accepted_into_cohorts.id is null
                        and promoted_cohorts.promoted = true
                        and users.mba_enabled

            union

            -- playlists from institutions
            select
                distinct institutions_users.user_id
                , unnest(institutions.playlist_pack_ids) as locale_pack_id
            from institutions_users
                join institutions on institutions.id = institutions_users.institution_id

            with no data
        ~

        add_index :user_can_see_playlist_locale_packs, [:user_id, :locale_pack_id], :unique => true, :name => :user_and_lp_on_user_can_see_play
    end

    def version_20170221140312

        execute %Q|
            create materialized view user_can_see_playlist_locale_packs as

            -- playlists from cohorts
            select
                distinct users.id as user_id
                , unnest(case
                            when accepted_into_cohorts.id is not null then accepted_into_cohorts.playlist_pack_ids
                            else promoted_cohorts.playlist_pack_ids
                            end) as locale_pack_id
            from
                users
                left join
                    cohort_applications on cohort_applications.user_id = users.id
                left join published_cohorts accepted_into_cohorts
                    on accepted_into_cohorts.id = cohort_applications.cohort_id
                        and cohort_applications.status = 'accepted'
                left join published_cohorts promoted_cohorts
                    on accepted_into_cohorts.id is null
                        and promoted_cohorts.promoted = true
                        and users.mba_enabled

            union

            -- playlists from institutions
            select
                distinct institutions_users.user_id
                , unnest(institutions.playlist_pack_ids) as locale_pack_id
            from institutions_users
                join institutions on institutions.id = institutions_users.institution_id

            with no data
        |

        add_index :user_can_see_playlist_locale_packs, [:user_id, :locale_pack_id], :unique => true, :name => :user_and_lp_on_user_can_see_play
    end

end