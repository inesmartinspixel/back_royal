class ViewHelpers::CohortUserPeriods < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    # converted to table
    def version_20180911132527
    end

    # add cumulative_required_up_to_now_lessons_completed
    def version_20180202021428
        execute %Q~
            create materialized view cohort_user_periods as
            WITH lesson_progress_records AS (
                SELECT
                    user_id
                    , cohort_user_lesson_progress_records.cohort_id
                    , cohort_user_lesson_progress_records.cohort_name
                    , completed_at
                    , completed_in_periods.index AS completed_in_period_index
                    , lesson_locale_pack_id
                FROM
                    cohort_user_lesson_progress_records
                    JOIN published_cohort_periods completed_in_periods
                        ON cohort_user_lesson_progress_records.cohort_id = completed_in_periods.cohort_id
                           AND completed_at BETWEEN completed_in_periods.period_start AND completed_in_periods.period_end
                WHERE
                    completed_at IS NOT NULL
                    AND required = TRUE
            )
            , with_lesson_info AS (
                SELECT
                    cohort_applications_plus.user_id
                    , cohort_applications_plus.cohort_id
                    , cohort_applications_plus.cohort_name
                    , published_cohort_periods.index
                    , count(DISTINCT lesson_progress_records.lesson_locale_pack_id) AS required_lessons_completed_in_period
                    , published_cohort_periods.period_start
                    , published_cohort_periods.period_end

                    , array_agg(distinct lesson_progress_records.lesson_locale_pack_id) as required_lesson_pack_ids_completed_in_period
                FROM
                    cohort_applications_plus
                    JOIN published_cohort_periods
                        ON cohort_applications_plus.cohort_id = published_cohort_periods.cohort_id
                    LEFT JOIN lesson_progress_records
                        ON published_cohort_periods.cohort_id = lesson_progress_records.cohort_id
                           AND cohort_applications_plus.user_id = lesson_progress_records.user_id
                           AND published_cohort_periods.index = lesson_progress_records.completed_in_period_index
                WHERE
                    was_accepted = TRUE
                    AND published_cohort_periods.period_end < NOW()
                GROUP BY
                    cohort_applications_plus.user_id
                    , cohort_applications_plus.cohort_id
                    , cohort_applications_plus.cohort_name
                    , published_cohort_periods.index
                    , published_cohort_periods.period_start
                    , published_cohort_periods.period_end
            )
            , with_status_info AS (
                SELECT
                    with_lesson_info.*
                    , coalesce(cohort_status_changes.status, 'not_yet_applied') AS status_at_period_end

                FROM with_lesson_info
                    LEFT JOIN cohort_status_changes
                        ON cohort_status_changes.cohort_id = with_lesson_info.cohort_id
                           AND cohort_status_changes.user_id = with_lesson_info.user_id
                           AND cohort_status_changes.from_time <= with_lesson_info.period_end
                           AND cohort_status_changes.until_time > with_lesson_info.period_end
            )
            -- we want to know how many lessons the user completed in the last
            -- 4 standard periods.  This block figures out the index of the period
            -- that was 4 standard periods ago
            , last_4_standard_periods as (
                SELECT
                    cohort_id
                    , index
                    , style
                    , (select min(index) from (
                        SELECT prev_standard_periods.index
                            FROM published_cohort_periods prev_standard_periods
                            WHERE prev_standard_periods.cohort_id = this_period.cohort_id
                                  AND prev_standard_periods.index <= this_period.index
                                  AND prev_standard_periods.style = 'standard'
                            ORDER BY index DESC
                            LIMIT 4
                    ) a) as four_standard_periods_ago_index
                FROM published_cohort_periods this_period
            )
            , cumululative_data AS (
                SELECT
                    user_periods.user_id
                    , user_periods.cohort_id
                    , user_periods.index
                    , sum(prev_periods.required_lessons_completed_in_period) AS cumulative_required_lessons_completed
                    , sum(
                          case when prev_periods.index >= last_4_standard_periods.four_standard_periods_ago_index
                                then prev_periods.required_lessons_completed_in_period
                                else 0 end
                      ) as cumulative_required_lessons_completed_in_last_4_periods

                    , coalesce(
                        array_length(
                            -- array_intersect is a custom function we've added in a migration
                            array_intersect(
                                published_cohort_periods.cumulative_required_lesson_pack_ids,
                                -- array_cat is a custom aggregate we've added in a migration
                                array_cat(DISTINCT prev_periods.required_lesson_pack_ids_completed_in_period)
                            ),
                            1
                        ),
                        0
                      ) as cumulative_required_up_to_now_lessons_completed

                FROM with_status_info user_periods
                    join published_cohort_periods
                        on published_cohort_periods.cohort_id = user_periods.cohort_id
                        and published_cohort_periods.index = user_periods.index
                    join last_4_standard_periods
                        on last_4_standard_periods.cohort_id = user_periods.cohort_id
                        and last_4_standard_periods.index = user_periods.index
                    LEFT JOIN with_status_info prev_periods
                        ON user_periods.user_id = prev_periods.user_id
                           AND user_periods.cohort_id = prev_periods.cohort_id
                           AND user_periods.index >= prev_periods.index
                GROUP BY
                    user_periods.user_id
                    , user_periods.cohort_id
                    , user_periods.index
                    , published_cohort_periods.cumulative_required_lesson_pack_ids
            )
            , with_cumulative_data AS (
                SELECT
                    with_status_info.*
                    , cumululative_data.cumulative_required_lessons_completed
                    , cumululative_data.cumulative_required_up_to_now_lessons_completed
                    , cumululative_data.cumulative_required_lessons_completed_in_last_4_periods
                FROM with_status_info
                    JOIN cumululative_data
                        ON with_status_info.user_id = cumululative_data.user_id
                           AND with_status_info.cohort_id = cumululative_data.cohort_id
                           AND with_status_info.index = cumululative_data.index
            )
            SELECT *
            FROM with_cumulative_data
            ORDER BY cohort_id, user_id, index
            WITH NO DATA
        ~

        add_index :cohort_user_periods, [:cohort_id, :user_id, :index], :unique => true, :name => :coh_user__index_on_coh_user_periods
    end

    def version_20170421201751
        execute %Q~
            create materialized view cohort_user_periods as
            WITH lesson_progress_records AS (
                SELECT
                    user_id
                    , cohort_user_lesson_progress_records.cohort_id
                    , cohort_user_lesson_progress_records.cohort_name
                    , completed_at
                    , completed_in_periods.index AS completed_in_period_index
                    , lesson_locale_pack_id
                FROM
                    cohort_user_lesson_progress_records
                    JOIN published_cohort_periods completed_in_periods
                        ON cohort_user_lesson_progress_records.cohort_id = completed_in_periods.cohort_id
                           AND completed_at BETWEEN completed_in_periods.period_start AND completed_in_periods.period_end
                WHERE
                    completed_at IS NOT NULL
                    AND required = TRUE
            )
                , with_lesson_info AS (
                SELECT
                    cohort_applications_plus.user_id
                    , cohort_applications_plus.cohort_id
                    , cohort_applications_plus.cohort_name
                    , published_cohort_periods.index
                    , count(DISTINCT lesson_progress_records.lesson_locale_pack_id) AS required_lessons_completed_in_period
                    , published_cohort_periods.period_start
                    , published_cohort_periods.period_end
                FROM
                    cohort_applications_plus
                    JOIN published_cohort_periods
                        ON cohort_applications_plus.cohort_id = published_cohort_periods.cohort_id
                    LEFT JOIN lesson_progress_records
                        ON published_cohort_periods.cohort_id = lesson_progress_records.cohort_id
                           AND cohort_applications_plus.user_id = lesson_progress_records.user_id
                           AND published_cohort_periods.index = lesson_progress_records.completed_in_period_index
                WHERE
                    was_accepted = TRUE
                    AND published_cohort_periods.period_end < NOW()
                GROUP BY
                    cohort_applications_plus.user_id
                    , cohort_applications_plus.cohort_id
                    , cohort_applications_plus.cohort_name
                    , published_cohort_periods.index
                    , published_cohort_periods.period_start
                    , published_cohort_periods.period_end
            )
                , with_status_info AS (
                SELECT
                    with_lesson_info.*
                    , coalesce(cohort_status_changes.status, 'not_yet_applied') AS status_at_period_end

                FROM with_lesson_info
                    LEFT JOIN cohort_status_changes
                        ON cohort_status_changes.cohort_id = with_lesson_info.cohort_id
                           AND cohort_status_changes.user_id = with_lesson_info.user_id
                           AND cohort_status_changes.from_time <= with_lesson_info.period_end
                           AND cohort_status_changes.until_time > with_lesson_info.period_end
            )
                , cumululative_data AS (
                SELECT
                    user_periods.user_id
                    , user_periods.cohort_id
                    , user_periods.index
                    , sum(prev_periods.required_lessons_completed_in_period) AS cumulative_required_lessons_completed
                FROM with_status_info user_periods
                    LEFT JOIN with_status_info prev_periods
                        ON user_periods.user_id = prev_periods.user_id
                           AND user_periods.cohort_id = prev_periods.cohort_id
                           AND user_periods.index >= prev_periods.index
                GROUP BY
                    user_periods.user_id
                    , user_periods.cohort_id
                    , user_periods.index
            )
                , with_cumulative_data AS (
                SELECT
                    with_status_info.*
                    , cumululative_data.cumulative_required_lessons_completed
                FROM with_status_info
                    JOIN cumululative_data
                        ON with_status_info.user_id = cumululative_data.user_id
                           AND with_status_info.cohort_id = cumululative_data.cohort_id
                           AND with_status_info.index = cumululative_data.index
            )
            SELECT *
            FROM with_cumulative_data
            ORDER BY cohort_id, user_id, index
            WITH NO DATA
        ~

        add_index :cohort_user_periods, [:cohort_id, :user_id, :index], :unique => true, :name => :coh_user__index_on_coh_user_periods
    end

    def version_20170414174516
        execute %Q~
            create materialized view cohort_user_periods as
            WITH lesson_progress_records AS (
                SELECT
                    user_id
                    , cohort_user_lesson_progress_records.cohort_id
                    , cohort_user_lesson_progress_records.cohort_name
                    , completed_at
                    , completed_in_periods.index AS completed_in_period_index
                    , lesson_locale_pack_id
                FROM
                    cohort_user_lesson_progress_records
                    JOIN published_cohort_periods completed_in_periods
                        ON cohort_user_lesson_progress_records.cohort_id = completed_in_periods.cohort_id
                           AND completed_at BETWEEN completed_in_periods.period_start AND completed_in_periods.period_end
                WHERE
                    completed_at IS NOT NULL
                    AND required = TRUE
            )
                , with_lesson_info AS (
                SELECT
                    cohort_applications_plus.user_id
                    , cohort_applications_plus.cohort_id
                    , cohort_applications_plus.cohort_name
                    , published_cohort_periods.index
                    , count(DISTINCT lesson_progress_records.lesson_locale_pack_id) AS required_lessons_completed_in_period
                    , published_cohort_periods.period_start
                    , published_cohort_periods.period_end
                FROM
                    cohort_applications_plus
                    JOIN published_cohort_periods
                        ON cohort_applications_plus.cohort_id = published_cohort_periods.cohort_id
                    LEFT JOIN lesson_progress_records
                        ON published_cohort_periods.cohort_id = lesson_progress_records.cohort_id
                           AND cohort_applications_plus.user_id = lesson_progress_records.user_id
                           AND published_cohort_periods.index = lesson_progress_records.completed_in_period_index
                WHERE
                    was_accepted = TRUE
                GROUP BY
                    cohort_applications_plus.user_id
                    , cohort_applications_plus.cohort_id
                    , cohort_applications_plus.cohort_name
                    , published_cohort_periods.index
                    , published_cohort_periods.period_start
                    , published_cohort_periods.period_end
            )
                , with_status_info AS (
                SELECT
                    with_lesson_info.*
                    , coalesce(cohort_status_changes.status, 'not_yet_applied') AS status_at_period_end

                FROM with_lesson_info
                    LEFT JOIN cohort_status_changes
                        ON cohort_status_changes.cohort_id = with_lesson_info.cohort_id
                           AND cohort_status_changes.user_id = with_lesson_info.user_id
                           AND cohort_status_changes.from_time <= with_lesson_info.period_end
                           AND cohort_status_changes.until_time > with_lesson_info.period_end
            )
                , cumululative_data AS (
                SELECT
                    user_periods.user_id
                    , user_periods.cohort_id
                    , user_periods.index
                    , sum(prev_periods.required_lessons_completed_in_period) AS cumulative_required_lessons_completed
                FROM with_status_info user_periods
                    LEFT JOIN with_status_info prev_periods
                        ON user_periods.user_id = prev_periods.user_id
                           AND user_periods.cohort_id = prev_periods.cohort_id
                           AND user_periods.index >= prev_periods.index
                GROUP BY
                    user_periods.user_id
                    , user_periods.cohort_id
                    , user_periods.index
            )
                , with_cumulative_data AS (
                SELECT
                    with_status_info.*
                    , cumululative_data.cumulative_required_lessons_completed
                FROM with_status_info
                    JOIN cumululative_data
                        ON with_status_info.user_id = cumululative_data.user_id
                           AND with_status_info.cohort_id = cumululative_data.cohort_id
                           AND with_status_info.index = cumululative_data.index
            )
            SELECT *
            FROM with_cumulative_data
            ORDER BY cohort_id, user_id, index
            WITH NO DATA
        ~

        add_index :cohort_user_periods, [:cohort_id, :user_id, :index], :unique => true, :name => :coh_user__index_on_coh_user_periods
    end

end