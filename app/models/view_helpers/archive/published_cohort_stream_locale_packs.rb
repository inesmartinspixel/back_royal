class ViewHelpers::PublishedCohortStreamLocalePacks < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    def version_20200609141113
        # do nothing. converted to table
    end

    def version_20170424161351
        execute %Q~
            create materialized view published_cohort_stream_locale_packs as
            WITH streams_from_groups AS (
                SELECT
                    DISTINCT
                      cohorts.id     AS cohort_id
                    , locale_pack_id AS stream_locale_pack_id
                    , 0              AS required
                    , 0              AS specialization
                    , 0              AS foundations
                FROM
                    published_cohorts cohorts
                    LEFT JOIN access_groups_cohorts ON access_groups_cohorts.cohort_id = cohorts.id
                    LEFT JOIN access_groups_lesson_stream_locale_packs
                        ON access_groups_lesson_stream_locale_packs.access_group_id = access_groups_cohorts.access_group_id
            )
                , streams_from_playlists AS (
                SELECT
                    DISTINCT
                      cohorts.id                                       AS cohort_id
                    , published_playlist_streams.stream_locale_pack_id AS stream_locale_pack_id
                    , CASE WHEN published_cohort_playlist_locale_packs.required
                    THEN 1
                      ELSE 0 END                                       AS required
                    , CASE WHEN published_cohort_playlist_locale_packs.specialization
                    THEN 1
                      ELSE 0 END                                       AS specialization
                    , CASE WHEN published_cohort_playlist_locale_packs.foundations
                    THEN 1
                      ELSE 0 END                                       AS foundations
                FROM
                    published_cohorts cohorts
                    JOIN published_cohort_playlist_locale_packs
                        ON cohorts.id = published_cohort_playlist_locale_packs.cohort_id
                    JOIN published_playlist_streams
                        ON published_playlist_streams.playlist_locale_pack_id =
                           published_cohort_playlist_locale_packs.playlist_locale_pack_id
            )
                , required_streams_from_schedule AS (
                SELECT
                    DISTINCT
                    cohort_id
                    , unnest(required_stream_pack_ids) stream_locale_pack_id
                    , 1 AS                             required
                    , 0 AS                             specialization
                    , 0 AS                             foundations
                FROM
                    published_cohort_periods
            )
                , optional_streams_from_schedule AS (
                SELECT
                    DISTINCT
                    cohort_id
                    , unnest(optional_stream_pack_ids) stream_locale_pack_id
                    , 0 AS                             required
                    , 0 AS                             specialization
                    , 0 AS                             foundations
                FROM
                    published_cohort_periods

            )
                , all_stream_locale_pack_ids AS (
                SELECT *
                FROM streams_from_groups
                UNION SELECT *
                      FROM streams_from_playlists
                UNION SELECT *
                      FROM required_streams_from_schedule
                UNION SELECT *
                      FROM optional_streams_from_schedule
            )
                , published_cohort_stream_locale_packs AS (
                SELECT
                      cohorts.id                                                                     AS cohort_id
                    , cohorts.name                                                                   AS cohort_name
                    , CASE WHEN sum(all_stream_locale_pack_ids.foundations) > 0
                    THEN TRUE
                      ELSE FALSE END                                                                 AS foundations
                    , CASE WHEN sum(all_stream_locale_pack_ids.required) > 0
                    THEN TRUE
                      ELSE FALSE END                                                                 AS required
                    , CASE WHEN sum(all_stream_locale_pack_ids.specialization) > 0
                    THEN TRUE
                      ELSE FALSE END                                                                 AS specialization
                    , CASE WHEN sum(all_stream_locale_pack_ids.required) + sum(all_stream_locale_pack_ids.specialization) = 0
                    THEN TRUE
                      ELSE FALSE END                                                                 AS elective
                    , published_stream_locale_packs.exam                                             AS exam
                    , published_stream_locale_packs.title                                            AS stream_title
                    , published_stream_locale_packs.locale_pack_id                                   AS stream_locale_pack_id
                    , published_stream_locale_packs.locales                                          AS stream_locales
                    , count(DISTINCT published_cohort_playlist_locale_packs.playlist_locale_pack_id) AS playlist_count
                FROM
                    published_cohorts cohorts
                    JOIN all_stream_locale_pack_ids
                        ON cohorts.id = all_stream_locale_pack_ids.cohort_id
                    JOIN published_stream_locale_packs
                        ON published_stream_locale_packs.locale_pack_id = all_stream_locale_pack_ids.stream_locale_pack_id
                    LEFT JOIN published_playlist_streams
                        ON published_playlist_streams.stream_locale_pack_id = all_stream_locale_pack_ids.stream_locale_pack_id
                    LEFT JOIN published_cohort_playlist_locale_packs ON
                                                                         cohorts.id =
                                                                         published_cohort_playlist_locale_packs.cohort_id
                                                                         AND published_playlist_streams.playlist_locale_pack_id =
                                                                             published_cohort_playlist_locale_packs.playlist_locale_pack_id
                GROUP BY
                    cohorts.id
                    , cohorts.name
                    , published_stream_locale_packs.title
                    , published_stream_locale_packs.locale_pack_id
                    , published_stream_locale_packs.locales
                    , published_stream_locale_packs.exam
            )
            SELECT *
            FROM published_cohort_stream_locale_packs
        ~

        add_index :published_cohort_stream_locale_packs, [:cohort_id, :stream_locale_pack_id], :name => :cohort_and_str_lp_on_cohort_str_lps, :unique => true
        add_index :published_cohort_stream_locale_packs, :stream_locale_pack_id, :name => :stream_lp_on_cohort_stream_packs
    end


    # this used to be called cohort_stream_locale_packs before version 20170310204902
    #
    # take away "with no data" so that this always
    # available immediately after migration completes
    #
    # use the schedule, required_playlist_pack_ids, and specialization_playlist_pack_ids to determine the streams
    #
    # change in_curriculum to required
    #
    # rename to published_cohort_stream_locale_packs
    def version_20170310204902
        execute %Q~
            create materialized view published_cohort_stream_locale_packs as
            with streams_from_groups as (
                select
                    distinct cohorts.id as cohort_id
                    , locale_pack_id as stream_locale_pack_id
                    , 0 as required
                    , 0 as specialization
                    , 0 as foundations
                from
                    published_cohorts cohorts
                    left join access_groups_cohorts on access_groups_cohorts.cohort_id = cohorts.id
                    left join access_groups_lesson_stream_locale_packs on access_groups_lesson_stream_locale_packs.access_group_id = access_groups_cohorts.access_group_id
            )
            , streams_from_playlists as (
                select
                    distinct cohorts.id as cohort_id
                    , published_playlist_streams.stream_locale_pack_id as stream_locale_pack_id
                    , case when published_cohort_playlist_locale_packs.required then 1 else 0 end as required
                    , case when published_cohort_playlist_locale_packs.specialization then 1 else 0 end as specialization
                    , case when published_cohort_playlist_locale_packs.foundations then 1 else 0 end as foundations
                from
                    published_cohorts cohorts
                    join published_cohort_playlist_locale_packs
                        on cohorts.id = published_cohort_playlist_locale_packs.cohort_id
                    join published_playlist_streams
                        on published_playlist_streams.playlist_locale_pack_id = published_cohort_playlist_locale_packs.playlist_locale_pack_id
            )
            , required_streams_from_schedule as (
                select
                    distinct cohorts.id as cohort_id
                    , trim(
                        json_array_elements((unnest(periods)->>'required_stream_pack_ids')::json)::text,
                        '\"'
                    )::uuid stream_locale_pack_id
                    , 1 as required
                    , 0 as specialization
                    , 0 as foundations
                from
                    published_cohorts cohorts
                    join cohorts_versions
                        on cohorts.version_id = cohorts_versions.version_id

            )
            , optional_streams_from_schedule as (
                select
                    distinct cohorts.id as cohort_id
                    , trim(
                        json_array_elements((unnest(periods)->>'optional_stream_pack_ids')::json)::text,
                        '\"'
                    )::uuid stream_locale_pack_id
                    , 0 as required
                    , 0 as specialization
                    , 0 as foundations
                from
                    published_cohorts cohorts
                    join cohorts_versions
                        on cohorts.version_id = cohorts_versions.version_id

            )
            , all_stream_locale_pack_ids as (
                select * from streams_from_groups
                union select * from streams_from_playlists
                union select * from required_streams_from_schedule
                union select * from optional_streams_from_schedule
            )
            , published_cohort_stream_locale_packs as (
                select
                    cohorts.id as cohort_id
                    , cohorts.name as cohort_name
                    , case when sum(all_stream_locale_pack_ids.foundations) > 0 then true else false end as foundations
                    , case when sum(all_stream_locale_pack_ids.required) > 0 then true else false end as required
                    , case when sum(all_stream_locale_pack_ids.specialization) > 0 then true else false end as specialization
                    , case when sum(all_stream_locale_pack_ids.required) + sum(all_stream_locale_pack_ids.specialization) = 0 then true else false end as elective
                    , published_stream_locale_packs.exam as exam
                    , published_stream_locale_packs.title as stream_title
                    , published_stream_locale_packs.locale_pack_id as stream_locale_pack_id
                    , published_stream_locale_packs.locales as stream_locales
                    , count(distinct published_cohort_playlist_locale_packs.playlist_locale_pack_id) as playlist_count
                from
                    published_cohorts cohorts
                    join all_stream_locale_pack_ids
                        on cohorts.id = all_stream_locale_pack_ids.cohort_id
                    join published_stream_locale_packs
                        on published_stream_locale_packs.locale_pack_id = all_stream_locale_pack_ids.stream_locale_pack_id
                    left join published_playlist_streams
                        on published_playlist_streams.stream_locale_pack_id = all_stream_locale_pack_ids.stream_locale_pack_id
                    left join published_cohort_playlist_locale_packs on
                        cohorts.id = published_cohort_playlist_locale_packs.cohort_id
                        and published_playlist_streams.playlist_locale_pack_id = published_cohort_playlist_locale_packs.playlist_locale_pack_id
                group by
                    cohorts.id
                    , cohorts.name
                    , published_stream_locale_packs.title
                    , published_stream_locale_packs.locale_pack_id
                    , published_stream_locale_packs.locales
                    , published_stream_locale_packs.exam
            )
            select * from published_cohort_stream_locale_packs
        ~

        add_index :published_cohort_stream_locale_packs, [:cohort_id, :stream_locale_pack_id], :name => :cohort_and_str_lp_on_cohort_str_lps, :unique => true
        add_index :published_cohort_stream_locale_packs, :stream_locale_pack_id, :name => :stream_lp_on_cohort_stream_packs
    end

end