class ViewHelpers::PublishedCohortContentDetails < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    def version_20200529191133
        # do nothing. removed
    end

    # pull enrollment deadline directly from published_cohorts
    def version_20170908153230
        execute %Q~
            create materialized view published_cohort_content_details as
            -- FIXME: split into one query for lesson info and one for stream info
            with lesson_info as (
                select
                    cohort_lesson_locale_packs.cohort_id
                    , cohort_lesson_locale_packs.cohort_name
                    , array_length(cohorts.required_playlist_pack_ids, 1) as required_playlist_count
                    , array_length(cohorts.specialization_playlist_pack_ids, 1) as specialization_playlist_count
                    , count(distinct case when cohort_lesson_locale_packs.foundations then cohort_lesson_locale_packs.lesson_locale_pack_id else null end) as foundations_lesson_count
                    , count(distinct case when cohort_lesson_locale_packs.required then cohort_lesson_locale_packs.lesson_locale_pack_id else null end) as required_lesson_count
                    , count(distinct case when cohort_lesson_locale_packs.test then cohort_lesson_locale_packs.lesson_locale_pack_id else null end) as test_lesson_count
                    , count(distinct case when cohort_lesson_locale_packs.elective then cohort_lesson_locale_packs.lesson_locale_pack_id else null end) as elective_lesson_count

                    -- duration weeks is the number of weeks from the start to end - 1 week for the final exam
                    , floor(EXTRACT(epoch FROM (cohorts.end_date - cohorts.start_date)) / extract(epoch from interval '7 days')) - 1 duration_weeks
                    , cohorts.enrollment_deadline
                from
                    published_cohort_lesson_locale_packs cohort_lesson_locale_packs
                    join published_cohorts cohorts
                        on cohorts.id = cohort_lesson_locale_packs.cohort_id
                group by
                    cohort_lesson_locale_packs.cohort_id
                    , cohort_lesson_locale_packs.cohort_name
                    , cohorts.required_playlist_pack_ids
                    , cohorts.specialization_playlist_pack_ids
                    , cohorts.end_date
                    , cohorts.start_date
                    , cohorts.id
                    , cohorts.enrollment_deadline
            )
            , stream_info as (
                select
                    cohort_stream_locale_packs.cohort_id
                    , count(distinct case when cohort_stream_locale_packs.foundations then cohort_stream_locale_packs.stream_locale_pack_id else null end) as foundations_stream_count
                    , count(distinct case when cohort_stream_locale_packs.required then cohort_stream_locale_packs.stream_locale_pack_id else null end) as required_stream_count
                    , count(distinct case when cohort_stream_locale_packs.exam then cohort_stream_locale_packs.stream_locale_pack_id else null end) as exam_stream_count
                    , count(distinct case when cohort_stream_locale_packs.elective then cohort_stream_locale_packs.stream_locale_pack_id else null end) as elective_stream_count
                from
                    published_cohort_stream_locale_packs cohort_stream_locale_packs
                    join published_cohorts cohorts
                        on cohorts.id = cohort_stream_locale_packs.cohort_id
                group by
                    cohort_stream_locale_packs.cohort_id
            )
            select
                lesson_info.*
                , stream_info.foundations_stream_count
                , stream_info.required_stream_count
                , stream_info.exam_stream_count
                , stream_info.elective_stream_count
                , case
                    when duration_weeks > 0 then
                        required_lesson_count / duration_weeks
                    else
                        null
                    end as expected_pace
                , case
                    when duration_weeks > 0 then
                        (required_lesson_count - foundations_lesson_count) / duration_weeks
                    else
                        null
                    end as expected_pace_fp
            from lesson_info
                join stream_info
                    on lesson_info.cohort_id = stream_info.cohort_id
        ~

        add_index :published_cohort_content_details, :cohort_id, :unique => true
    end


    def version_20170525165348
        execute %Q~
            create materialized view published_cohort_content_details as
            -- FIXME: split into one query for lesson info and one for stream info
            with lesson_info as (
                select
                    cohort_lesson_locale_packs.cohort_id
                    , cohort_lesson_locale_packs.cohort_name
                    , array_length(cohorts.required_playlist_pack_ids, 1) as required_playlist_count
                    , array_length(cohorts.specialization_playlist_pack_ids, 1) as specialization_playlist_count
                    , count(distinct case when cohort_lesson_locale_packs.foundations then cohort_lesson_locale_packs.lesson_locale_pack_id else null end) as foundations_lesson_count
                    , count(distinct case when cohort_lesson_locale_packs.required then cohort_lesson_locale_packs.lesson_locale_pack_id else null end) as required_lesson_count
                    , count(distinct case when cohort_lesson_locale_packs.test then cohort_lesson_locale_packs.lesson_locale_pack_id else null end) as test_lesson_count
                    , count(distinct case when cohort_lesson_locale_packs.elective then cohort_lesson_locale_packs.lesson_locale_pack_id else null end) as elective_lesson_count

                    -- duration weeks is the number of weeks from the start to end - 1 week for the final exam
                    , floor(EXTRACT(epoch FROM (cohorts.end_date - cohorts.start_date)) / extract(epoch from interval '7 days')) - 1 duration_weeks
                    , CASE
                        WHEN cohorts.expulsion_date IS NULL
                            THEN cohorts.start_date -- if we do not auto-expel, then all users are enrolled
                        ELSE cohorts.expulsion_date + INTERVAL '1 day' -- add 1 day to give the job time to run
                        END AS enrollment_deadline
                from
                    published_cohort_lesson_locale_packs cohort_lesson_locale_packs
                    join published_cohorts cohorts
                        on cohorts.id = cohort_lesson_locale_packs.cohort_id
                group by
                    cohort_lesson_locale_packs.cohort_id
                    , cohort_lesson_locale_packs.cohort_name
                    , cohorts.required_playlist_pack_ids
                    , cohorts.specialization_playlist_pack_ids
                    , cohorts.end_date
                    , cohorts.start_date
                    , cohorts.id
                    , cohorts.expulsion_date
            )
            , stream_info as (
                select
                    cohort_stream_locale_packs.cohort_id
                    , count(distinct case when cohort_stream_locale_packs.foundations then cohort_stream_locale_packs.stream_locale_pack_id else null end) as foundations_stream_count
                    , count(distinct case when cohort_stream_locale_packs.required then cohort_stream_locale_packs.stream_locale_pack_id else null end) as required_stream_count
                    , count(distinct case when cohort_stream_locale_packs.exam then cohort_stream_locale_packs.stream_locale_pack_id else null end) as exam_stream_count
                    , count(distinct case when cohort_stream_locale_packs.elective then cohort_stream_locale_packs.stream_locale_pack_id else null end) as elective_stream_count
                from
                    published_cohort_stream_locale_packs cohort_stream_locale_packs
                    join published_cohorts cohorts
                        on cohorts.id = cohort_stream_locale_packs.cohort_id
                group by
                    cohort_stream_locale_packs.cohort_id
            )
            select
                lesson_info.*
                , stream_info.foundations_stream_count
                , stream_info.required_stream_count
                , stream_info.exam_stream_count
                , stream_info.elective_stream_count
                , case
                    when duration_weeks > 0 then
                        required_lesson_count / duration_weeks
                    else
                        null
                    end as expected_pace
                , case
                    when duration_weeks > 0 then
                        (required_lesson_count - foundations_lesson_count) / duration_weeks
                    else
                        null
                    end as expected_pace_fp
            from lesson_info
                join stream_info
                    on lesson_info.cohort_id = stream_info.cohort_id
        ~

        add_index :published_cohort_content_details, :cohort_id, :unique => true
    end

    # Be resilient to a duration_weeks of zero
    def version_20170411201741
        execute %Q~
            create materialized view published_cohort_content_details as
            -- FIXME: split into one query for lesson info and one for stream info
            with lesson_info as (
                select
                    cohort_lesson_locale_packs.cohort_id
                    , cohort_lesson_locale_packs.cohort_name
                    , array_length(cohorts.required_playlist_pack_ids, 1) as required_playlist_count
                    , array_length(cohorts.specialization_playlist_pack_ids, 1) as specialization_playlist_count
                    , count(distinct case when cohort_lesson_locale_packs.foundations then cohort_lesson_locale_packs.lesson_locale_pack_id else null end) as foundations_lesson_count
                    , count(distinct case when cohort_lesson_locale_packs.required then cohort_lesson_locale_packs.lesson_locale_pack_id else null end) as required_lesson_count
                    , count(distinct case when cohort_lesson_locale_packs.test then cohort_lesson_locale_packs.lesson_locale_pack_id else null end) as test_lesson_count
                    , count(distinct case when cohort_lesson_locale_packs.elective then cohort_lesson_locale_packs.lesson_locale_pack_id else null end) as elective_lesson_count

                    -- duration weeks is the number of weeks from the start to end - 1 week for the final exam
                    , floor(EXTRACT(epoch FROM (cohorts.end_date - cohorts.start_date)) / extract(epoch from interval '7 days')) - 1 duration_weeks
                    , case
                        -- MBA1 and MBA2 use 5 weeks
                        when cohorts.id in ('8f9f77d1-069b-4ab4-a8b0-28d9d3ab29f2', '1789b5f0-0436-4e65-b521-3c19f63a71db') then cohorts.start_date + interval '5 weeks'

                        -- MBA5's expulsions were done a few hours late
                        when cohorts.id in ('2843c7bb-b94e-4496-b7e3-a407c332e511') then cohorts.start_date + interval '3 weeks' + interval '1 day'
                        else cohorts.start_date + interval '3 weeks'
                        end as enrollment_deadline
                from
                    published_cohort_lesson_locale_packs cohort_lesson_locale_packs
                    join published_cohorts cohorts
                        on cohorts.id = cohort_lesson_locale_packs.cohort_id
                group by
                    cohort_lesson_locale_packs.cohort_id
                    , cohort_lesson_locale_packs.cohort_name
                    , cohorts.required_playlist_pack_ids
                    , cohorts.specialization_playlist_pack_ids
                    , cohorts.end_date
                    , cohorts.start_date
                    , cohorts.id
            )
            , stream_info as (
                select
                    cohort_stream_locale_packs.cohort_id
                    , count(distinct case when cohort_stream_locale_packs.foundations then cohort_stream_locale_packs.stream_locale_pack_id else null end) as foundations_stream_count
                    , count(distinct case when cohort_stream_locale_packs.required then cohort_stream_locale_packs.stream_locale_pack_id else null end) as required_stream_count
                    , count(distinct case when cohort_stream_locale_packs.exam then cohort_stream_locale_packs.stream_locale_pack_id else null end) as exam_stream_count
                    , count(distinct case when cohort_stream_locale_packs.elective then cohort_stream_locale_packs.stream_locale_pack_id else null end) as elective_stream_count
                from
                    published_cohort_stream_locale_packs cohort_stream_locale_packs
                    join published_cohorts cohorts
                        on cohorts.id = cohort_stream_locale_packs.cohort_id
                group by
                    cohort_stream_locale_packs.cohort_id
            )
            select
                lesson_info.*
                , stream_info.foundations_stream_count
                , stream_info.required_stream_count
                , stream_info.exam_stream_count
                , stream_info.elective_stream_count
                , case
                    when duration_weeks > 0 then
                        required_lesson_count / duration_weeks
                    else
                        null
                    end as expected_pace
                , case
                    when duration_weeks > 0 then
                        (required_lesson_count - foundations_lesson_count) / duration_weeks
                    else
                        null
                    end as expected_pace_fp
            from lesson_info
                join stream_info
                    on lesson_info.cohort_id = stream_info.cohort_id
            with no data
        ~

        add_index :published_cohort_content_details, :cohort_id, :unique => true
    end

    # this version makes no changes.  It only exists because we had an
    # issue in our ViewHelpers that caused prod to get messed up
    def version_20170330133007

        execute %Q~
            create materialized view published_cohort_content_details as
            -- FIXME: split into one query for lesson info and one for stream info
            with lesson_info as (
                select
                    cohort_lesson_locale_packs.cohort_id
                    , cohort_lesson_locale_packs.cohort_name
                    , array_length(cohorts.required_playlist_pack_ids, 1) as required_playlist_count
                    , array_length(cohorts.specialization_playlist_pack_ids, 1) as specialization_playlist_count
                    , count(distinct case when cohort_lesson_locale_packs.foundations then cohort_lesson_locale_packs.lesson_locale_pack_id else null end) as foundations_lesson_count
                    , count(distinct case when cohort_lesson_locale_packs.required then cohort_lesson_locale_packs.lesson_locale_pack_id else null end) as required_lesson_count
                    , count(distinct case when cohort_lesson_locale_packs.test then cohort_lesson_locale_packs.lesson_locale_pack_id else null end) as test_lesson_count
                    , count(distinct case when cohort_lesson_locale_packs.elective then cohort_lesson_locale_packs.lesson_locale_pack_id else null end) as elective_lesson_count

                    -- duration weeks is the number of weeks from the start to end - 1 week for the final exam
                    , floor(EXTRACT(epoch FROM (cohorts.end_date - cohorts.start_date)) / extract(epoch from interval '7 days')) - 1 duration_weeks
                    , case
                        -- MBA1 and MBA2 use 5 weeks
                        when cohorts.id in ('8f9f77d1-069b-4ab4-a8b0-28d9d3ab29f2', '1789b5f0-0436-4e65-b521-3c19f63a71db') then cohorts.start_date + interval '5 weeks'

                        -- MBA5's expulsions were done a few hours late
                        when cohorts.id in ('2843c7bb-b94e-4496-b7e3-a407c332e511') then cohorts.start_date + interval '3 weeks' + interval '1 day'
                        else cohorts.start_date + interval '3 weeks'
                        end as enrollment_deadline
                from
                    published_cohort_lesson_locale_packs cohort_lesson_locale_packs
                    join published_cohorts cohorts
                        on cohorts.id = cohort_lesson_locale_packs.cohort_id
                group by
                    cohort_lesson_locale_packs.cohort_id
                    , cohort_lesson_locale_packs.cohort_name
                    , cohorts.required_playlist_pack_ids
                    , cohorts.specialization_playlist_pack_ids
                    , cohorts.end_date
                    , cohorts.start_date
                    , cohorts.id
            )
            , stream_info as (
                select
                    cohort_stream_locale_packs.cohort_id
                    , count(distinct case when cohort_stream_locale_packs.foundations then cohort_stream_locale_packs.stream_locale_pack_id else null end) as foundations_stream_count
                    , count(distinct case when cohort_stream_locale_packs.required then cohort_stream_locale_packs.stream_locale_pack_id else null end) as required_stream_count
                    , count(distinct case when cohort_stream_locale_packs.exam then cohort_stream_locale_packs.stream_locale_pack_id else null end) as exam_stream_count
                    , count(distinct case when cohort_stream_locale_packs.elective then cohort_stream_locale_packs.stream_locale_pack_id else null end) as elective_stream_count
                from
                    published_cohort_stream_locale_packs cohort_stream_locale_packs
                    join published_cohorts cohorts
                        on cohorts.id = cohort_stream_locale_packs.cohort_id
                group by
                    cohort_stream_locale_packs.cohort_id
            )
            select
                lesson_info.*
                , stream_info.foundations_stream_count
                , stream_info.required_stream_count
                , stream_info.exam_stream_count
                , stream_info.elective_stream_count
                , required_lesson_count / duration_weeks as expected_pace
                , (required_lesson_count - foundations_lesson_count) / duration_weeks as expected_pace_fp
            from lesson_info
                join stream_info
                    on lesson_info.cohort_id = stream_info.cohort_id
            with no data
        ~

        add_index :published_cohort_content_details, :cohort_id, :unique => true

    end

    # change mba5 enrollment deadline to make up for the fact
    # that expulsions were done late
    def version_20170327184151
        version_20170330133007
    end

    # compared with the last cohort_content_details version, we
    #
    # split lesson and stream info into separate queries to
    #  support the case where a lesson is in 2 streams (real issue
    #  in tests.  theoretical issue in real content)
    #
    # replace playlist_count with required_playlist_count and specialization_playlist_count
    #
    # reference published_ tables that have been renamed
    def version_20170310204902
        execute %Q~
            create materialized view published_cohort_content_details as
            -- FIXME: split into one query for lesson info and one for stream info
            with lesson_info as (
                select
                    cohort_lesson_locale_packs.cohort_id
                    , cohort_lesson_locale_packs.cohort_name
                    , array_length(cohorts.required_playlist_pack_ids, 1) as required_playlist_count
                    , array_length(cohorts.specialization_playlist_pack_ids, 1) as specialization_playlist_count
                    , count(distinct case when cohort_lesson_locale_packs.foundations then cohort_lesson_locale_packs.lesson_locale_pack_id else null end) as foundations_lesson_count
                    , count(distinct case when cohort_lesson_locale_packs.required then cohort_lesson_locale_packs.lesson_locale_pack_id else null end) as required_lesson_count
                    , count(distinct case when cohort_lesson_locale_packs.test then cohort_lesson_locale_packs.lesson_locale_pack_id else null end) as test_lesson_count
                    , count(distinct case when cohort_lesson_locale_packs.elective then cohort_lesson_locale_packs.lesson_locale_pack_id else null end) as elective_lesson_count

                    -- duration weeks is the number of weeks from the start to end - 1 week for the final exam
                    , floor(EXTRACT(epoch FROM (cohorts.end_date - cohorts.start_date)) / extract(epoch from interval '7 days')) - 1 duration_weeks
                    , case
                        -- MBA1 and MBA2 use 5 weeks
                        when cohorts.id in ('8f9f77d1-069b-4ab4-a8b0-28d9d3ab29f2', '1789b5f0-0436-4e65-b521-3c19f63a71db') then cohorts.start_date + interval '5 weeks'
                        else cohorts.start_date + interval '3 weeks'
                        end as enrollment_deadline
                from
                    published_cohort_lesson_locale_packs cohort_lesson_locale_packs
                    join published_cohorts cohorts
                        on cohorts.id = cohort_lesson_locale_packs.cohort_id
                group by
                    cohort_lesson_locale_packs.cohort_id
                    , cohort_lesson_locale_packs.cohort_name
                    , cohorts.required_playlist_pack_ids
                    , cohorts.specialization_playlist_pack_ids
                    , cohorts.end_date
                    , cohorts.start_date
                    , cohorts.id
            )
            , stream_info as (
                select
                    cohort_stream_locale_packs.cohort_id
                    , count(distinct case when cohort_stream_locale_packs.foundations then cohort_stream_locale_packs.stream_locale_pack_id else null end) as foundations_stream_count
                    , count(distinct case when cohort_stream_locale_packs.required then cohort_stream_locale_packs.stream_locale_pack_id else null end) as required_stream_count
                    , count(distinct case when cohort_stream_locale_packs.exam then cohort_stream_locale_packs.stream_locale_pack_id else null end) as exam_stream_count
                    , count(distinct case when cohort_stream_locale_packs.elective then cohort_stream_locale_packs.stream_locale_pack_id else null end) as elective_stream_count
                from
                    published_cohort_stream_locale_packs cohort_stream_locale_packs
                    join published_cohorts cohorts
                        on cohorts.id = cohort_stream_locale_packs.cohort_id
                group by
                    cohort_stream_locale_packs.cohort_id
            )
            select
                lesson_info.*
                , stream_info.foundations_stream_count
                , stream_info.required_stream_count
                , stream_info.exam_stream_count
                , stream_info.elective_stream_count
                , required_lesson_count / duration_weeks as expected_pace
                , (required_lesson_count - foundations_lesson_count) / duration_weeks as expected_pace_fp
            from lesson_info
                join stream_info
                    on lesson_info.cohort_id = stream_info.cohort_id
            with no data
        ~

        add_index :published_cohort_content_details, :cohort_id, :unique => true
    end

end