class ViewHelpers::PublishedCohortPlaylistLocalePacks < ViewHelpers::ViewHelperBase
    # -*- SkipSchemaAnnotations

    def version_20200609141113
        # do nothing. converted to table
    end

    # Ensure that the 'foundations' column looks at the published_cohorts required_playlist_pack_ids
    # rather than the cohorts table since the required_playlist_pack_ids column on the cohorts table
    # will soon be dropped. See https://trello.com/c/jpdCS5KO.
    def version_20190409135750
        execute %Q|
            create materialized view published_cohort_playlist_locale_packs as
            with required_cohort_playlist_locale_packs as (
                select
                    cohorts.id as cohort_id,
                    cohorts.name as cohort_name,
                    true as required,
                    false as specialization,
                    unnest(required_playlist_pack_ids) as playlist_locale_pack_id
                from published_cohorts cohorts
            )
            , specialization_cohort_playlist_locale_packs as (
                select
                    cohorts.id as cohort_id,
                    cohorts.name as cohort_name,
                    false as required,
                    true as specialization,
                    unnest(specialization_playlist_pack_ids) as playlist_locale_pack_id
                from published_cohorts cohorts
            )
            , cohort_playlist_locale_packs_1 as (
                select * from required_cohort_playlist_locale_packs
                union
                select * from specialization_cohort_playlist_locale_packs
            )
            , cohort_playlist_locale_packs_2 as (
                select
                    cohort_playlist_locale_packs_1.cohort_id,
                    cohort_playlist_locale_packs_1.cohort_name,
                    cohort_playlist_locale_packs_1.required,
                    cohort_playlist_locale_packs_1.specialization,
                    published_playlist_locale_packs.locale_pack_id = (select required_playlist_pack_ids[1] from published_cohorts where id = cohort_id) as foundations,
                    published_playlist_locale_packs.title as playlist_title,
                    published_playlist_locale_packs.locales as playlist_locales,
                    published_playlist_locale_packs.locale_pack_id as playlist_locale_pack_id
                from cohort_playlist_locale_packs_1
                    join published_playlist_locale_packs on published_playlist_locale_packs.locale_pack_id = cohort_playlist_locale_packs_1.playlist_locale_pack_id
            )

            select * from cohort_playlist_locale_packs_2
        |

        add_index :published_cohort_playlist_locale_packs, [:cohort_id, :playlist_locale_pack_id], :unique => true, :name => :cohort_and_play_lp_on_coh_play_lps
        add_index :published_cohort_playlist_locale_packs, :playlist_locale_pack_id, :name => :playlist_lp_on_cohort_playlist_packs
    end

    def version_20170310204902
        execute %Q|
            create materialized view published_cohort_playlist_locale_packs as
            with required_cohort_playlist_locale_packs as (
                select
                    cohorts.id as cohort_id,
                    cohorts.name as cohort_name,
                    true as required,
                    false as specialization,
                    unnest(required_playlist_pack_ids) as playlist_locale_pack_id
                from published_cohorts cohorts
            )
            , specialization_cohort_playlist_locale_packs as (
                select
                    cohorts.id as cohort_id,
                    cohorts.name as cohort_name,
                    false as required,
                    true as specialization,
                    unnest(specialization_playlist_pack_ids) as playlist_locale_pack_id
                from published_cohorts cohorts
            )
            , cohort_playlist_locale_packs_1 as (
                select * from required_cohort_playlist_locale_packs
                union
                select * from specialization_cohort_playlist_locale_packs
            )
            , cohort_playlist_locale_packs_2 as (
                select
                    cohort_playlist_locale_packs_1.cohort_id,
                    cohort_playlist_locale_packs_1.cohort_name,
                    cohort_playlist_locale_packs_1.required,
                    cohort_playlist_locale_packs_1.specialization,
                    published_playlist_locale_packs.locale_pack_id = (select required_playlist_pack_ids[1] from cohorts where id = cohort_id) as foundations,
                    published_playlist_locale_packs.title as playlist_title,
                    published_playlist_locale_packs.locales as playlist_locales,
                    published_playlist_locale_packs.locale_pack_id as playlist_locale_pack_id
                from cohort_playlist_locale_packs_1
                    join published_playlist_locale_packs on published_playlist_locale_packs.locale_pack_id = cohort_playlist_locale_packs_1.playlist_locale_pack_id
            )

            select * from cohort_playlist_locale_packs_2
        |

        add_index :published_cohort_playlist_locale_packs, [:cohort_id, :playlist_locale_pack_id], :unique => true, :name => :cohort_and_play_lp_on_coh_play_lps
        add_index :published_cohort_playlist_locale_packs, :playlist_locale_pack_id, :name => :playlist_lp_on_cohort_playlist_packs
    end
end