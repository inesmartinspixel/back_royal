class ViewHelpers::CareerProfileSearchHelpers < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    def version_20180910205309
        # do nothing.  converted to table
    end

    # add student_network_interests
    def version_20180418143340

        # FIXME: when we change this view (or any view higher up than it in the list),
        # it locks up career profile queries for 30 seconds or so.  We could prevent
        # this if we used `WITH NO DATA`, but then we would have to handle what happens when
        # a user tries to use this view and gets an unpopulated error (like we do with reports)
        execute %Q~
            create materialized view career_profile_search_helpers as

            WITH aggregated_education_experience_data as (
                SELECT
                    career_profiles.id as career_profile_id

                    -- use string_agg to make an indexed ilike query easy
                    , string_agg(DISTINCT educational_organization_options.text, ' ' ) as education_experience_school_names

                    , (min(graduation_year) || '/01/01')::timestamp first_graduation_date

                    -- used in calculating years_of_experience
                    , array_remove(
                          array_agg(CASE WHEN degree_program THEN (graduation_year || '/01/01') :: TIMESTAMP ELSE NULL END ORDER BY graduation_year)
                          , NULL
                      ) as graduation_dates

                    -- in_school
                    , count(
                            case when
                                degree_program
                                and (
                                    -- if it is before june, than a graduation_year of this year counts as in_school
                                    (date_part('month', now()) < 6 AND graduation_year = date_part('year', now()))
                                    -- a graduation_year after this year always counts as in_school
                                    OR (graduation_year > date_part('year', now()))
                                )
                            then true
                            else null end
                        ) > 0 as in_school
                FROM
                    career_profiles
                    left join education_experiences on career_profiles.id = education_experiences.career_profile_id
                        left join educational_organization_options on education_experiences.educational_organization_option_id = educational_organization_options.id
                where career_profiles.updated_at > career_profiles.created_at -- for performance, ignore the 1/3 of profiles that were never changed after initially created
                group by
                    career_profiles.id
            )
            , aggregated_hiring_relationship_data as (
                SELECT
                    career_profiles.id as career_profile_id

                    , count(accepted_hiring_relationships.id) as accepted_by_hiring_managers_count
                FROM
                    career_profiles
                    left join hiring_relationships accepted_hiring_relationships
                        on accepted_hiring_relationships.candidate_id = career_profiles.user_id
                        and accepted_hiring_relationships.hiring_manager_status = 'accepted'
                where career_profiles.updated_at > career_profiles.created_at -- for performance, ignore the 1/3 of profiles that were never changed after initially created
                group by
                    career_profiles.id
            )
            , aggregated_work_experience_data as (
                SELECT
                    career_profiles.id as career_profile_id

                    -- work experience roles and industries
                    , array_agg(DISTINCT work_experiences.role) as work_experience_roles
                    , array_remove(
                            array_agg(case when work_experiences.end_date is null then work_experiences.role else null end)
                            , null
                        ) as current_work_experience_roles
                    , array_agg(DISTINCT work_experiences.industry) as work_experience_industries
                    , array_remove(
                            array_agg(case when work_experiences.end_date is null then work_experiences.industry else null end)
                            , null
                        ) as current_work_experience_industries

                    -- use string_agg to make an indexed ilike query easy
                    , string_agg(DISTINCT professional_organization_options.text, ' ' ) as work_experience_company_names

                    -- stuff for calculating years_of_experience
                    -- when calculating years_of_experience we ignore any
                    -- work experiences from before the first graduation date, assuming
                    -- that those are not really real jobs
                    , min(
                        case
                            when first_graduation_date is null or work_experiences.start_date > first_graduation_date
                            then work_experiences.start_date
                            else null
                        end
                      ) as earliest_work_start_date_after_first_graduation

                    , (
                        -- The rules for deciding when we start counting for years_of_experience
                        -- are a little crazy.  We want to
                        -- ignore any jobs before the user had ever graduated, since those are just
                        -- internships.  Then, we start counting from the last graduation date
                        -- before the user started working (in case they left out some less
                        -- important jobs they did earlier on).  Put another way:
                        --
                        -- 1. ignore any wor experiences before your first graduation
                        -- 2. if you have no graduations, then count from first work experience
                        -- 3. if you have no work experiences, the start date does not matter, years_of_experience will be 0
                        -- 4  otherwise, start counting from the last graduation before your first work experience (ignoring ones we assume are internships)
                        --
                        -- (There are specs for this in CareerProfileSearchHelpers detailing all the different cases that led to this rule)
                        select
                            max(_graduation_dates)
                        from unnest(graduation_dates) _graduation_dates
                        where _graduation_dates < min(case when work_experiences.start_date > graduation_dates[1] then work_experiences.start_date else null end)
                      ) as last_graduation_before_first_work_experience
                    , max(work_experiences.end_date) latest_work_end_date
                    , count(case when work_experiences.id is not null and work_experiences.end_date is null then true else null end) > 0 as has_current_work_experience
                FROM
                    career_profiles
                    join aggregated_education_experience_data
                        on career_profiles.id = aggregated_education_experience_data.career_profile_id
                    left join work_experiences ON career_profiles.id = work_experiences.career_profile_id
                        left join professional_organization_options on work_experiences.professional_organization_option_id = professional_organization_options.id
                where career_profiles.updated_at > career_profiles.created_at -- for performance, ignore the 1/3 of profiles that were never changed after initially created
                group by
                    career_profiles.id
                    , graduation_dates
            )
            , aggregated_skills_data as (
                SELECT
                    career_profiles.id as career_profile_id

                    -- skills
                    , array_agg(distinct skills_options.text) as skills_texts
                FROM
                    career_profiles
                    left join career_profiles_skills_options on career_profiles.id = career_profiles_skills_options.career_profile_id
                        left join skills_options skills_options ON career_profiles_skills_options.skills_option_id = skills_options.id
                where career_profiles.updated_at > career_profiles.created_at -- for performance, ignore the 1/3 of profiles that were never changed after initially created
                group by
                    career_profiles.id
            )
            , aggregated_student_network_interests_data as (
                SELECT
                    career_profiles.id as career_profile_id

                    -- student_network_interests
                    , array_agg(distinct student_network_interests_options.text) as student_network_interests_texts
                FROM
                    career_profiles
                    left join career_profiles_student_network_interests_options on career_profiles.id = career_profiles_student_network_interests_options.career_profile_id
                        left join student_network_interests_options student_network_interests_options ON career_profiles_student_network_interests_options.student_network_interests_option_id = student_network_interests_options.id
                where career_profiles.updated_at > career_profiles.created_at -- for performance, ignore the 1/3 of profiles that were never changed after initially created
                group by
                    career_profiles.id
            )
            , career_profiles_with_aggregated_data as (
                SELECT
                    aggregated_work_experience_data.career_profile_id

                    -- work experience roles and industries
                    , work_experience_roles
                    , work_experience_industries

                    -- use string_agg to make an indexed ilike query easy
                    , work_experience_company_names
                    , education_experience_school_names

                    , current_work_experience_roles
                    , current_work_experience_industries

                    -- stuff for calculating years_experience
                    , earliest_work_start_date_after_first_graduation
                    , latest_work_end_date
                    , has_current_work_experience
                    , first_graduation_date
                    , last_graduation_before_first_work_experience

                    -- in_school
                    , in_school

                    -- skills
                    , skills_texts

                    -- student_network_interests
                    , student_network_interests_texts

                    -- hiring_relationships
                    , accepted_by_hiring_managers_count
                FROM
                    aggregated_work_experience_data
                    join aggregated_education_experience_data on aggregated_education_experience_data.career_profile_id = aggregated_work_experience_data.career_profile_id
                    join aggregated_skills_data on aggregated_skills_data.career_profile_id = aggregated_work_experience_data.career_profile_id
                    join aggregated_student_network_interests_data on aggregated_student_network_interests_data.career_profile_id = aggregated_work_experience_data.career_profile_id
                    join aggregated_hiring_relationship_data on aggregated_hiring_relationship_data.career_profile_id = aggregated_work_experience_data.career_profile_id
            )

            -- add in the dates that we need to calculate the years of experience
            , with_experience_start_and_end_dates as (
                select
                    career_profiles_with_aggregated_data.*

                    -- since the last_graduation_date can be after now(), use now() to
                    -- prevent a negative result for years_of_experience
                    , least(earliest_work_start_date_after_first_graduation, last_graduation_before_first_work_experience) as earliest_experience_start
                    , case
                        when has_current_work_experience then now()
                        else latest_work_end_date
                        end as latest_experience_end
                from career_profiles_with_aggregated_data
            )

            -- subtract the start from the end to get the years
            select
                with_experience_start_and_end_dates.*

                -- default prioritization
                ,   (
                        -- candidates in the US and Canada come first
                        1000000 *
                        case
                            when place_details #>> '{country,short}' IN ('US', 'CA')
                            then 1 else 0 end
                      +
                        -- Candidate most interested in a job come first
                        100000 *
                        case
                            when interested_in_joining_new_company='very_interested' then 9
                            when interested_in_joining_new_company='interested' then 8
                            when interested_in_joining_new_company='neutral' then 7
                            else 6
                        end
                      +
                        -- Candidates that are marked as do_not_create_relationships come last
                        10000 *
                        case
                            when do_not_create_relationships=false
                            then 1 else 0
                        end
                      +
                        -- Candidates that have been accepted by more hiring managers come first,
                        -- since apparently they are good candidates
                        case
                            when accepted_by_hiring_managers_count is not null
                            then accepted_by_hiring_managers_count else 0
                        end
                      +
                        -- we need something unique across records to ensure consistent
                        -- sorting for pagination.  created_at desc makes it more likely
                        -- for new users to get attention
                        (extract(epoch from created_at)::float / 100000000000)
                    ) as default_prioritization

                , case
                    when earliest_experience_start is null or latest_experience_end is null or latest_experience_end < earliest_experience_start
                        then 0
                    else
                      EXTRACT(epoch FROM latest_experience_end - earliest_experience_start) / EXTRACT(epoch FROM interval '1 year')
                    end
                 as years_of_experience
            from with_experience_start_and_end_dates
                join career_profiles on career_profiles.id = career_profile_id

            -- This cannot be done 'with no data' because the browse page relies on it and we do not have any handling for the
            -- situation where this is not populated
        ~

        add_index :career_profile_search_helpers, :career_profile_id, :unique => true
        add_index :career_profile_search_helpers, :work_experience_roles, using: :gin, :name => :wexp_roles_on_cp_search_helpers
        add_index :career_profile_search_helpers, :work_experience_industries, using: :gin, :name => :wexp_industries_on_cp_search_helpers
        add_index :career_profile_search_helpers, [:years_of_experience, :default_prioritization], order: {years_of_experience: :desc, default_prioritization: :desc}, :name => :years_exp_on_cp_search_helpers
        add_index :career_profile_search_helpers, :skills_texts, using: :gin, :name => :skills_texts_on_cp_search_helpers
        add_index :career_profile_search_helpers, :student_network_interests_texts, using: :gin, :name => :student_network_interests_texts_on_cp_search_helpers

        # sometimes we order by both in_school and default_prioritization, sometimes just by default_prioritization, so
        # it is good to have both indexes
        add_index :career_profile_search_helpers, [:in_school, :default_prioritization], order: {default_prioritization: :desc}, :name => :in_school_and_default_prioritization_on_cp_search_helpers
        add_index :career_profile_search_helpers, :default_prioritization, :name => :default_prioritization_on_cp_search_helpers

        execute "CREATE INDEX wexp_company_names_on_cp_search_helpers ON career_profile_search_helpers USING gin (work_experience_company_names gin_trgm_ops)"
        execute "CREATE INDEX edexp_school_names_on_cp_search_helpers ON career_profile_search_helpers USING gin (education_experience_school_names gin_trgm_ops)"

    end

    def version_20180111202937
        execute %Q~
            create materialized view career_profile_search_helpers as

            WITH aggregated_education_experience_data as (
                SELECT
                    career_profiles.id as career_profile_id

                    -- use string_agg to make an indexed ilike query easy
                    , string_agg(DISTINCT educational_organization_options.text, ' ' ) as education_experience_school_names

                    , (min(graduation_year) || '/01/01')::timestamp first_graduation_date

                    -- used in calculating years_of_experience
                    , array_remove(
                          array_agg(CASE WHEN degree_program THEN (graduation_year || '/01/01') :: TIMESTAMP ELSE NULL END ORDER BY graduation_year)
                          , NULL
                      ) as graduation_dates

                    -- in_school
                    , count(
                            case when
                                degree_program
                                and (
                                    -- if it is before june, than a graduation_year of this year counts as in_school
                                    (date_part('month', now()) < 6 AND graduation_year = date_part('year', now()))
                                    -- a graduation_year after this year always counts as in_school
                                    OR (graduation_year > date_part('year', now()))
                                )
                            then true
                            else null end
                        ) > 0 as in_school
                FROM
                    career_profiles
                    left join education_experiences on career_profiles.id = education_experiences.career_profile_id
                        left join educational_organization_options on education_experiences.educational_organization_option_id = educational_organization_options.id
                where career_profiles.updated_at > career_profiles.created_at -- for performance, ignore the 1/3 of profiles that were never changed after initially created
                group by
                    career_profiles.id
            )
            , aggregated_hiring_relationship_data as (
                SELECT
                    career_profiles.id as career_profile_id

                    , count(accepted_hiring_relationships.id) as accepted_by_hiring_managers_count
                FROM
                    career_profiles
                    left join hiring_relationships accepted_hiring_relationships
                        on accepted_hiring_relationships.candidate_id = career_profiles.user_id
                        and accepted_hiring_relationships.hiring_manager_status = 'accepted'
                where career_profiles.updated_at > career_profiles.created_at -- for performance, ignore the 1/3 of profiles that were never changed after initially created
                group by
                    career_profiles.id
            )
            , aggregated_work_experience_data as (
                SELECT
                    career_profiles.id as career_profile_id

                    -- work experience roles and industries
                    , array_agg(DISTINCT work_experiences.role) as work_experience_roles
                    , array_remove(
                            array_agg(case when work_experiences.end_date is null then work_experiences.role else null end)
                            , null
                        ) as current_work_experience_roles
                    , array_agg(DISTINCT work_experiences.industry) as work_experience_industries
                    , array_remove(
                            array_agg(case when work_experiences.end_date is null then work_experiences.industry else null end)
                            , null
                        ) as current_work_experience_industries

                    -- use string_agg to make an indexed ilike query easy
                    , string_agg(DISTINCT professional_organization_options.text, ' ' ) as work_experience_company_names

                    -- stuff for calculating years_of_experience
                    -- when calculating years_of_experience we ignore any
                    -- work experiences from before the first graduation date, assuming
                    -- that those are not really real jobs
                    , min(
                        case
                            when first_graduation_date is null or work_experiences.start_date > first_graduation_date
                            then work_experiences.start_date
                            else null
                        end
                      ) as earliest_work_start_date_after_first_graduation

                    , (
                        -- The rules for deciding when we start counting for years_of_experience
                        -- are a little crazy.  We want to
                        -- ignore any jobs before the user had ever graduated, since those are just
                        -- internships.  Then, we start counting from the last graduation date
                        -- before the user started working (in case they left out some less
                        -- important jobs they did earlier on).  Put another way:
                        --
                        -- 1. ignore any wor experiences before your first graduation
                        -- 2. if you have no graduations, then count from first work experience
                        -- 3. if you have no work experiences, the start date does not matter, years_of_experience will be 0
                        -- 4  otherwise, start counting from the last graduation before your first work experience (ignoring ones we assume are internships)
                        --
                        -- (There are specs for this in CareerProfileSearchHelpers detailing all the different cases that led to this rule)
                        select
                            max(_graduation_dates)
                        from unnest(graduation_dates) _graduation_dates
                        where _graduation_dates < min(case when work_experiences.start_date > graduation_dates[1] then work_experiences.start_date else null end)
                      ) as last_graduation_before_first_work_experience
                    , max(work_experiences.end_date) latest_work_end_date
                    , count(case when work_experiences.id is not null and work_experiences.end_date is null then true else null end) > 0 as has_current_work_experience
                FROM
                    career_profiles
                    join aggregated_education_experience_data
                        on career_profiles.id = aggregated_education_experience_data.career_profile_id
                    left join work_experiences ON career_profiles.id = work_experiences.career_profile_id
                        left join professional_organization_options on work_experiences.professional_organization_option_id = professional_organization_options.id
                where career_profiles.updated_at > career_profiles.created_at -- for performance, ignore the 1/3 of profiles that were never changed after initially created
                group by
                    career_profiles.id
                    , graduation_dates
            )
            , aggregated_skills_data as (
                SELECT
                    career_profiles.id as career_profile_id

                    -- skills
                    , array_agg(distinct skills_options.text) as skills_texts
                FROM
                    career_profiles
                    left join career_profiles_skills_options on career_profiles.id = career_profiles_skills_options.career_profile_id
                        left join skills_options skills_options ON career_profiles_skills_options.skills_option_id = skills_options.id
                where career_profiles.updated_at > career_profiles.created_at -- for performance, ignore the 1/3 of profiles that were never changed after initially created
                group by
                    career_profiles.id
            )
            , career_profiles_with_aggregated_data as (
                SELECT
                    aggregated_work_experience_data.career_profile_id

                    -- work experience roles and industries
                    , work_experience_roles
                    , work_experience_industries

                    -- use string_agg to make an indexed ilike query easy
                    , work_experience_company_names
                    , education_experience_school_names

                    , current_work_experience_roles
                    , current_work_experience_industries

                    -- stuff for calculating years_experience
                    , earliest_work_start_date_after_first_graduation
                    , latest_work_end_date
                    , has_current_work_experience
                    , first_graduation_date
                    , last_graduation_before_first_work_experience

                    -- in_school
                    , in_school

                    -- skills
                    , skills_texts

                    -- hiring_relationships
                    , accepted_by_hiring_managers_count
                FROM
                    aggregated_work_experience_data
                    join aggregated_education_experience_data on aggregated_education_experience_data.career_profile_id = aggregated_work_experience_data.career_profile_id
                    join aggregated_skills_data on aggregated_skills_data.career_profile_id = aggregated_work_experience_data.career_profile_id
                    join aggregated_hiring_relationship_data on aggregated_hiring_relationship_data.career_profile_id = aggregated_work_experience_data.career_profile_id
            )

            -- add in the dates that we need to calculate the years of experience
            , with_experience_start_and_end_dates as (
                select
                    career_profiles_with_aggregated_data.*

                    -- since the last_graduation_date can be after now(), use now() to
                    -- prevent a negative result for years_of_experience
                    , least(earliest_work_start_date_after_first_graduation, last_graduation_before_first_work_experience) as earliest_experience_start
                    , case
                        when has_current_work_experience then now()
                        else latest_work_end_date
                        end as latest_experience_end
                from career_profiles_with_aggregated_data
            )

            -- subtract the start from the end to get the years
            select
                with_experience_start_and_end_dates.*

                -- default prioritization
                ,   (
                        -- candidates in the US and Canada come first
                        1000000 *
                        case
                            when place_details #>> '{country,short}' IN ('US', 'CA')
                            then 1 else 0 end
                      +
                        -- Candidate most interested in a job come first
                        100000 *
                        case
                            when interested_in_joining_new_company='very_interested' then 9
                            when interested_in_joining_new_company='interested' then 8
                            when interested_in_joining_new_company='neutral' then 7
                            else 6
                        end
                      +
                        -- Candidates that are marked as do_not_create_relationships come last
                        10000 *
                        case
                            when do_not_create_relationships=false
                            then 1 else 0
                        end
                      +
                        -- Candidates that have been accepted by more hiring managers come first,
                        -- since apparently they are good candidates
                        case
                            when accepted_by_hiring_managers_count is not null
                            then accepted_by_hiring_managers_count else 0
                        end
                      +
                        -- we need something unique across records to ensure consistent
                        -- sorting for pagination.  created_at desc makes it more likely
                        -- for new users to get attention
                        (extract(epoch from created_at)::float / 100000000000)
                    ) as default_prioritization

                , case
                    when earliest_experience_start is null or latest_experience_end is null or latest_experience_end < earliest_experience_start
                        then 0
                    else
                      EXTRACT(epoch FROM latest_experience_end - earliest_experience_start) / EXTRACT(epoch FROM interval '1 year')
                    end
                 as years_of_experience
            from with_experience_start_and_end_dates
                join career_profiles on career_profiles.id = career_profile_id

            -- This cannot be done 'with no data' because the browse page relies on it and we do not have any handling for the
            -- situation where this is not populated
        ~

        add_index :career_profile_search_helpers, :career_profile_id, :unique => true
        add_index :career_profile_search_helpers, :work_experience_roles, using: :gin, :name => :wexp_roles_on_cp_search_helpers
        add_index :career_profile_search_helpers, :work_experience_industries, using: :gin, :name => :wexp_industries_on_cp_search_helpers
        add_index :career_profile_search_helpers, [:years_of_experience, :default_prioritization], order: {years_of_experience: :desc, default_prioritization: :desc}, :name => :years_exp_on_cp_search_helpers
        add_index :career_profile_search_helpers, :skills_texts, using: :gin, :name => :skills_texts_on_cp_search_helpers

        # sometimes we order by both in_school and default_prioritization, sometimes just by default_prioritization, so
        # it is good to have both indexes
        add_index :career_profile_search_helpers, [:in_school, :default_prioritization], order: {default_prioritization: :desc}, :name => :in_school_and_default_prioritization_on_cp_search_helpers
        add_index :career_profile_search_helpers, :default_prioritization, :name => :default_prioritization_on_cp_search_helpers

        execute "CREATE INDEX wexp_company_names_on_cp_search_helpers ON career_profile_search_helpers USING gin (work_experience_company_names gin_trgm_ops)"
        execute "CREATE INDEX edexp_school_names_on_cp_search_helpers ON career_profile_search_helpers USING gin (education_experience_school_names gin_trgm_ops)"
    end
end