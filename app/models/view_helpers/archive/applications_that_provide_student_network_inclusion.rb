class ViewHelpers::ApplicationsThatProvideStudentNetworkInclusion < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    def version_20190606193943
        # do nothing.  converted to table
    end

    def version_20181005141700
        execute %Q~
            CREATE VIEW applications_that_provide_student_network_inclusion AS
            with accepted_cohort_applications as (
                select cohort_applications.id
                    from cohort_applications
                        join cohorts on cohort_applications.cohort_id = cohorts.id
                    where status='accepted'
                        and cohorts.program_type in ('mba', 'emba')
                        and graduation_status != 'failed'
            )
            , active_deferred_applications as (
                SELECT
                    deferred_cohort_applications.user_id
                    , (array_agg(deferred_cohort_applications.id order by deferred_cohort_applications.applied_at desc))[1] as id
                from cohort_applications deferred_cohort_applications
                    join cohorts on deferred_cohort_applications.cohort_id = cohorts.id
                    left join cohort_applications subsequent_applications
                        on subsequent_applications.user_id = deferred_cohort_applications.user_id
                        and subsequent_applications.status not in ('deferred', 'pre_accepted')
                where deferred_cohort_applications.status='deferred'
                    and cohorts.program_type in ('mba', 'emba')
                group by deferred_cohort_applications.user_id
                HAVING

                    -- This user only gets access from zir deferrals if the last deferral
                    -- has no expelled, rejected, or accepted applications after it
                    count(subsequent_applications.id) = 0

                    OR
                    (
                        max(deferred_cohort_applications.applied_at)
                        >
                        max(subsequent_applications.applied_at)
                    )
            )
            SELECT
                cohort_applications.*
            from cohort_applications
            where id in (
                select id from accepted_cohort_applications
                UNION
                select id from active_deferred_applications
            )
        ~
    end
end