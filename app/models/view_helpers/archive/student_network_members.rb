class ViewHelpers::StudentNetworkMembers < ViewHelpers::ViewHelperBase
    # -*- SkipSchemaAnnotations

    def version_20190619181948
        # do nothing.  deprecated
    end

    def version_20190606193943
        execute %Q~
            CREATE VIEW student_network_members AS
            select
                student_network_inclusions.user_id
                , case
                    when location is null or pref_student_network_privacy = 'hidden'
                        then 'none'
                    else
                        pref_student_network_privacy
                end as visibility
                , true as has_application_that_provides_network_inclusion
                , pref_student_network_privacy
                , location is not null as has_location
            from student_network_inclusions
                join career_profiles on career_profiles.user_id = student_network_inclusions.user_id
        ~
    end

    def version_20181005141700
        execute %Q~
            CREATE VIEW student_network_members AS
            select
                applications_that_provide_student_network_inclusion.user_id
                , case
                    when location is null or pref_student_network_privacy = 'hidden'
                        then 'none'
                    else
                        pref_student_network_privacy
                end as visibility
                , true as has_application_that_provides_network_inclusion
                , pref_student_network_privacy
                , location is not null as has_location
            from applications_that_provide_student_network_inclusion
                join users on users.id = applications_that_provide_student_network_inclusion.user_id
                join career_profiles on career_profiles.user_id = users.id
        ~
    end
end