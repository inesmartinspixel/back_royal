class ViewHelpers::CohortUserLessonProgressRecords < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations


    # remove group by, which was doing nothing but making this query
    # super slow
    def version_20180301211924
        current
    end

    # user_lesson_progress_records no longer has the etl prefix.
    # stop materializing the view
    def version_20170918153357
        execute %Q|
            create view cohort_user_lesson_progress_records as
            select
                cohort_applications_plus.cohort_id
                , cohort_applications_plus.cohort_name
                , cohort_applications_plus.user_id as user_id
                , cohort_lesson_locale_packs.required
                , cohort_lesson_locale_packs.assessment
                , cohort_lesson_locale_packs.test
                , cohort_lesson_locale_packs.elective
                , cohort_lesson_locale_packs.foundations
                , cohort_lesson_locale_packs.lesson_title
                , cohort_lesson_locale_packs.lesson_locale_pack_id
                , lesson_progress.started_at
                , lesson_progress.completed_at
                , lesson_progress.total_lesson_time
                , lesson_progress.last_lesson_activity_time
                , lesson_progress.lesson_finish_count
                , lesson_progress.average_assessment_score_first
                , lesson_progress.average_assessment_score_best
                , lesson_progress.official_test_score
                , lesson_progress.total_lesson_time_on_desktop
                , lesson_progress.total_lesson_time_on_mobile_app
                , lesson_progress.total_lesson_time_on_mobile_web
                , lesson_progress.total_lesson_time_on_unknown
                , lesson_progress.completed_on_client_type
                , lesson_progress.lesson_reset_count
                , lesson_progress.last_lesson_reset_at
            from cohort_applications_plus
                join published_cohort_lesson_locale_packs cohort_lesson_locale_packs
                    on cohort_applications_plus.cohort_id = cohort_lesson_locale_packs.cohort_id
                left join user_lesson_progress_records lesson_progress
                    on lesson_progress.locale_pack_id = cohort_lesson_locale_packs.lesson_locale_pack_id
                        and lesson_progress.user_id = cohort_applications_plus.user_id
            where cohort_applications_plus.was_accepted=true
            group by
                cohort_applications_plus.cohort_id
                , cohort_applications_plus.cohort_name
                , cohort_applications_plus.user_id
                , cohort_lesson_locale_packs.required
                , cohort_lesson_locale_packs.assessment
                , cohort_lesson_locale_packs.test
                , cohort_lesson_locale_packs.elective
                , cohort_lesson_locale_packs.lesson_title
                , cohort_lesson_locale_packs.lesson_locale_pack_id
                , cohort_lesson_locale_packs.foundations
                , lesson_progress.started_at
                , lesson_progress.completed_at
                , lesson_progress.total_lesson_time
                , lesson_progress.last_lesson_activity_time
                , lesson_progress.lesson_finish_count
                , lesson_progress.average_assessment_score_first
                , lesson_progress.average_assessment_score_best
                , lesson_progress.official_test_score
                , lesson_progress.total_lesson_time_on_desktop
                , lesson_progress.total_lesson_time_on_mobile_app
                , lesson_progress.total_lesson_time_on_mobile_web
                , lesson_progress.total_lesson_time_on_unknown
                , lesson_progress.completed_on_client_type
                , lesson_progress.lesson_reset_count
                , lesson_progress.last_lesson_reset_at
        |

    end

    def version_20170526143117
        execute %Q|
            create materialized view cohort_user_lesson_progress_records as
            select
                cohort_applications_plus.cohort_id
                , cohort_applications_plus.cohort_name
                , cohort_applications_plus.user_id as user_id
                , cohort_lesson_locale_packs.required
                , cohort_lesson_locale_packs.assessment
                , cohort_lesson_locale_packs.test
                , cohort_lesson_locale_packs.elective
                , cohort_lesson_locale_packs.foundations
                , cohort_lesson_locale_packs.lesson_title
                , cohort_lesson_locale_packs.lesson_locale_pack_id
                , lesson_progress.started_at
                , lesson_progress.completed_at
                , lesson_progress.total_lesson_time
                , lesson_progress.last_lesson_activity_time
                , lesson_progress.lesson_finish_count
                , lesson_progress.average_assessment_score_first
                , lesson_progress.average_assessment_score_best
                , lesson_progress.official_test_score
                , lesson_progress.total_lesson_time_on_desktop
                , lesson_progress.total_lesson_time_on_mobile_app
                , lesson_progress.total_lesson_time_on_mobile_web
                , lesson_progress.total_lesson_time_on_unknown
                , lesson_progress.completed_on_client_type
                , lesson_progress.lesson_reset_count
                , lesson_progress.last_lesson_reset_at
            from cohort_applications_plus
                join published_cohort_lesson_locale_packs cohort_lesson_locale_packs
                    on cohort_applications_plus.cohort_id = cohort_lesson_locale_packs.cohort_id
                left join etl.user_lesson_progress_records lesson_progress
                    on lesson_progress.locale_pack_id = cohort_lesson_locale_packs.lesson_locale_pack_id
                        and lesson_progress.user_id = cohort_applications_plus.user_id
            where cohort_applications_plus.was_accepted=true
            group by
                cohort_applications_plus.cohort_id
                , cohort_applications_plus.cohort_name
                , cohort_applications_plus.user_id
                , cohort_lesson_locale_packs.required
                , cohort_lesson_locale_packs.assessment
                , cohort_lesson_locale_packs.test
                , cohort_lesson_locale_packs.elective
                , cohort_lesson_locale_packs.lesson_title
                , cohort_lesson_locale_packs.lesson_locale_pack_id
                , cohort_lesson_locale_packs.foundations
                , lesson_progress.started_at
                , lesson_progress.completed_at
                , lesson_progress.total_lesson_time
                , lesson_progress.last_lesson_activity_time
                , lesson_progress.lesson_finish_count
                , lesson_progress.average_assessment_score_first
                , lesson_progress.average_assessment_score_best
                , lesson_progress.official_test_score
                , lesson_progress.total_lesson_time_on_desktop
                , lesson_progress.total_lesson_time_on_mobile_app
                , lesson_progress.total_lesson_time_on_mobile_web
                , lesson_progress.total_lesson_time_on_unknown
                , lesson_progress.completed_on_client_type
                , lesson_progress.lesson_reset_count
                , lesson_progress.last_lesson_reset_at
            with no data
        |

        add_index :cohort_user_lesson_progress_records, [:cohort_id, :user_id, :lesson_locale_pack_id], :unique => true, :name => :coh_user_lp_on_coh_uspr
        add_index :cohort_user_lesson_progress_records, :user_id

    end

    # user published_cohort_lesson_locale_packs instead of cohort_lesson_locale_packs
    #
    # remove playlist_count, since it's no longer defined in cohort_user_lesson_progress_records
    #
    # change in_curriculum to required
    def version_20170310204902
        execute %Q|
            create materialized view cohort_user_lesson_progress_records as
            select
                cohort_applications_plus.cohort_id
                , cohort_applications_plus.cohort_name
                , cohort_applications_plus.user_id as user_id
                , cohort_lesson_locale_packs.required
                , cohort_lesson_locale_packs.assessment
                , cohort_lesson_locale_packs.test
                , cohort_lesson_locale_packs.elective
                , cohort_lesson_locale_packs.foundations
                , cohort_lesson_locale_packs.lesson_title
                , cohort_lesson_locale_packs.lesson_locale_pack_id
                , lesson_progress.started_at
                , lesson_progress.completed_at
                , lesson_progress.total_lesson_time
                , lesson_progress.last_lesson_activity_time
                , lesson_progress.lesson_finish_count
                , lesson_progress.average_assessment_score_first
                , lesson_progress.average_assessment_score_best
                , lesson_progress.total_lesson_time_on_desktop
                , lesson_progress.total_lesson_time_on_mobile_app
                , lesson_progress.total_lesson_time_on_mobile_web
                , lesson_progress.total_lesson_time_on_unknown
                , lesson_progress.completed_on_client_type
                , lesson_progress.lesson_reset_count
                , lesson_progress.last_lesson_reset_at
            from cohort_applications_plus
                join published_cohort_lesson_locale_packs cohort_lesson_locale_packs
                    on cohort_applications_plus.cohort_id = cohort_lesson_locale_packs.cohort_id
                left join etl.user_lesson_progress_records lesson_progress
                    on lesson_progress.locale_pack_id = cohort_lesson_locale_packs.lesson_locale_pack_id
                        and lesson_progress.user_id = cohort_applications_plus.user_id
            where cohort_applications_plus.was_accepted=true
            group by
                cohort_applications_plus.cohort_id
                , cohort_applications_plus.cohort_name
                , cohort_applications_plus.user_id
                , cohort_lesson_locale_packs.required
                , cohort_lesson_locale_packs.assessment
                , cohort_lesson_locale_packs.test
                , cohort_lesson_locale_packs.elective
                , cohort_lesson_locale_packs.lesson_title
                , cohort_lesson_locale_packs.lesson_locale_pack_id
                , cohort_lesson_locale_packs.foundations
                , lesson_progress.started_at
                , lesson_progress.completed_at
                , lesson_progress.total_lesson_time
                , lesson_progress.last_lesson_activity_time
                , lesson_progress.lesson_finish_count
                , lesson_progress.average_assessment_score_first
                , lesson_progress.average_assessment_score_best
                , lesson_progress.total_lesson_time_on_desktop
                , lesson_progress.total_lesson_time_on_mobile_app
                , lesson_progress.total_lesson_time_on_mobile_web
                , lesson_progress.total_lesson_time_on_unknown
                , lesson_progress.completed_on_client_type
                , lesson_progress.lesson_reset_count
                , lesson_progress.last_lesson_reset_at
            with no data
        |

        add_index :cohort_user_lesson_progress_records, [:cohort_id, :user_id, :lesson_locale_pack_id], :unique => true, :name => :coh_user_lp_on_coh_uspr
        add_index :cohort_user_lesson_progress_records, :user_id

    end

    def version_20170221140313
        execute %Q|
            create materialized view cohort_user_lesson_progress_records as
            select
                cohort_applications_plus.cohort_id
                , cohort_applications_plus.cohort_name
                , cohort_applications_plus.user_id as user_id
                , cohort_lesson_locale_packs.in_curriculum
                , cohort_lesson_locale_packs.assessment
                , cohort_lesson_locale_packs.test
                , cohort_lesson_locale_packs.elective
                , cohort_lesson_locale_packs.foundations
                , cohort_lesson_locale_packs.lesson_title
                , cohort_lesson_locale_packs.lesson_locale_pack_id
                , cohort_lesson_locale_packs.playlist_count
                , lesson_progress.started_at
                , lesson_progress.completed_at
                , lesson_progress.total_lesson_time
                , lesson_progress.last_lesson_activity_time
                , lesson_progress.lesson_finish_count
                , lesson_progress.average_assessment_score_first
                , lesson_progress.average_assessment_score_best
                , lesson_progress.total_lesson_time_on_desktop
                , lesson_progress.total_lesson_time_on_mobile_app
                , lesson_progress.total_lesson_time_on_mobile_web
                , lesson_progress.total_lesson_time_on_unknown
                , lesson_progress.completed_on_client_type
                , lesson_progress.lesson_reset_count
                , lesson_progress.last_lesson_reset_at
            from cohort_applications_plus
                join cohort_lesson_locale_packs
                    on cohort_applications_plus.cohort_id = cohort_lesson_locale_packs.cohort_id
                left join etl.user_lesson_progress_records lesson_progress
                    on lesson_progress.locale_pack_id = cohort_lesson_locale_packs.lesson_locale_pack_id
                        and lesson_progress.user_id = cohort_applications_plus.user_id
            where cohort_applications_plus.was_accepted=true
            group by
                cohort_applications_plus.cohort_id
                , cohort_applications_plus.cohort_name
                , cohort_applications_plus.user_id
                , cohort_lesson_locale_packs.in_curriculum
                , cohort_lesson_locale_packs.assessment
                , cohort_lesson_locale_packs.test
                , cohort_lesson_locale_packs.elective
                , cohort_lesson_locale_packs.lesson_title
                , cohort_lesson_locale_packs.lesson_locale_pack_id
                , cohort_lesson_locale_packs.foundations
                , cohort_lesson_locale_packs.playlist_count
                , lesson_progress.started_at
                , lesson_progress.completed_at
                , lesson_progress.total_lesson_time
                , lesson_progress.last_lesson_activity_time
                , lesson_progress.lesson_finish_count
                , lesson_progress.average_assessment_score_first
                , lesson_progress.average_assessment_score_best
                , lesson_progress.total_lesson_time_on_desktop
                , lesson_progress.total_lesson_time_on_mobile_app
                , lesson_progress.total_lesson_time_on_mobile_web
                , lesson_progress.total_lesson_time_on_unknown
                , lesson_progress.completed_on_client_type
                , lesson_progress.lesson_reset_count
                , lesson_progress.last_lesson_reset_at
            with no data
        |

        add_index :cohort_user_lesson_progress_records, [:cohort_id, :user_id, :lesson_locale_pack_id], :unique => true, :name => :coh_user_lp_on_coh_uspr
        add_index :cohort_user_lesson_progress_records, :user_id
    end

end