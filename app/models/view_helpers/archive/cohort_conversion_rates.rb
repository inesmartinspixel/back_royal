class ViewHelpers::CohortConversionRates < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    # Materialized CTEs
    def version_20200625152000
        current
    end

    # support replacement of consider_graduated_by_deadline with could_graduate_by_deadline
    # some tweaks to min and max grad rates
    def version_20200204160313
        execute %Q~
            create materialized view cohort_conversion_rates as

            -- count up things that refer to the applications for this cohort (i.e. not
            -- looking at the first cohort a user enrolled in).  These things are not used
            -- for graduation rate calculations, but for yield, etc.
            with counts_from_this_cohort as (
                SELECT
                    aps.cohort_id
                    , count(distinct aps.id) as num_applied
                    , count(case when aps.was_accepted then true else null end) as num_accepted
                    , count(case when cohort_user_progress_records.status in ('enrolled', 'graduated', 'failed', 'expelled') then true else null end) as num_still_enrolled
                    , count(case when cohort_user_progress_records.status in ('enrolled', 'pending_enrollment') then true else null end) as num_active
                    , count(case when cohort_user_progress_records.status = 'deferred' then true else null end) as num_deferred
                    , count(case when cohort_user_progress_records.meets_graduation_requirements then true else null end) as num_meets_grad_req

                from cohort_applications_plus aps
                    left join cohort_user_progress_records
                        on aps.user_id = cohort_user_progress_records.user_id
                        and aps.cohort_id = cohort_user_progress_records.cohort_id
                group by aps.cohort_id
            )

            , enrollment_records_with_chance_of_graduating as (
                select
                    enrollment_records.*

                    -- This is necessary because we do not define a chance_of_graduating
                    -- for all cohorts.  It's not set for MBA1 because we had to leave MBA1
                    -- out of the MDPPredictor stuff, since it is so unique.  It's not set
                    -- for EMBA because we haven't gotten around to it.  I haven't even thought
                    -- about how this stuff would work for program types with schedules or different
                    -- cohorts
                    , case
                        when could_graduate_by_deadline = false
                            then 0
                        when did_graduate_by_deadline
                            then 1
                        when chance_of_graduating is not NULL
                            then chance_of_graduating
                        else
                            null
                    end as chance_of_graduating
                from
                    program_enrollment_progress_records enrollment_records
                    left join user_chance_of_graduating_records
                        on user_chance_of_graduating_records.user_id = enrollment_records.user_id
                        and user_chance_of_graduating_records.program_type = enrollment_records.program_type
            )

            -- count up things that refer to the users who originally enrolled in
            -- a particular cohort.  These are things related to graduation rates.
            , counts_from_users_who_first_enrolled_in_this_cohort as (
                select
                    enrollment_records.original_cohort_id
                    , count(*) as num_first_enrolled
                    , count(case when enrollment_records.final_status = 'graduated' then true else null end) as num_graduated
                    , count(case when enrollment_records.did_graduate_by_deadline then true else null end) as num_did_graduate_by_deadline
                    , count(case when enrollment_records.could_graduate_by_deadline then true else null end) as num_could_graduate_by_deadline
                    , count(case when enrollment_records.final_status = 'did_not_graduate' then true else null end) as num_did_not_graduate
                    , count(case when enrollment_records.final_status = 'deferred' then true else null end) as num_currently_deferred
                    , count(case when enrollment_records.original_cohort_id = enrollment_records.final_cohort_id and enrollment_records.final_status in ('enrolled', 'pending_enrollment') then true else null end) as num_currently_enrolled_in_orig_cohort
                    , count(case when enrollment_records.original_cohort_id != enrollment_records.final_cohort_id and enrollment_records.final_status in ('enrolled', 'pending_enrollment') then true else null end) as num_currently_enrolled_in_later_cohort
                    , avg(chance_of_graduating) as expected_grad_rate
                    , avg(case
                        when did_graduate_by_deadline then 1
                        else 0 end
                    ) as min_grad_rate

                from enrollment_records_with_chance_of_graduating enrollment_records
                    join published_cohorts original_cohorts
                        on original_cohorts.id = enrollment_records.original_cohort_id
                group by
                    enrollment_records.original_cohort_id
            )

            -- put all the counts together and filter out irrelevant program types
            , step_1 as (
                select
                    counts_from_this_cohort.cohort_id
                    , published_cohorts.program_type
                    , published_cohorts.name as cohort_name

                    -- these counts refer to applications for this cohort
                    , counts_from_this_cohort.num_applied
                    , counts_from_this_cohort.num_accepted
                    , counts_from_this_cohort.num_still_enrolled
                    , counts_from_this_cohort.num_active
                    , counts_from_this_cohort.num_meets_grad_req
                    , counts_from_this_cohort.num_deferred


                    -- these counts refer to people who first enrolled in this cohort, and
                    -- indicate what their status is in the last cohort they were in
                    , counts_from_users_who_first_enrolled_in_this_cohort.num_first_enrolled
                    , counts_from_users_who_first_enrolled_in_this_cohort.num_graduated
                    , counts_from_users_who_first_enrolled_in_this_cohort.num_did_graduate_by_deadline
                    , counts_from_users_who_first_enrolled_in_this_cohort.num_could_graduate_by_deadline
                    , counts_from_users_who_first_enrolled_in_this_cohort.num_did_not_graduate
                    , counts_from_users_who_first_enrolled_in_this_cohort.num_currently_deferred
                    , counts_from_users_who_first_enrolled_in_this_cohort.num_currently_enrolled_in_orig_cohort
                    , counts_from_users_who_first_enrolled_in_this_cohort.num_currently_enrolled_in_later_cohort
                    , counts_from_users_who_first_enrolled_in_this_cohort.expected_grad_rate
                    , counts_from_users_who_first_enrolled_in_this_cohort.min_grad_rate

                from counts_from_this_cohort
                    join counts_from_users_who_first_enrolled_in_this_cohort
                        on counts_from_users_who_first_enrolled_in_this_cohort.original_cohort_id = counts_from_this_cohort.cohort_id
                    join published_cohorts
                        on published_cohorts.id = counts_from_this_cohort.cohort_id
                WHERE program_type in ('mba', 'emba')
                order by
                    published_cohorts.program_type
                    , published_cohorts.start_date
            )

            -- do some division to get some averages
            , step_2 as (
                select
                    step_1.cohort_id
                    , step_1.program_type
                    , step_1.cohort_name
                    , case when num_applied = 0
                        then null
                        else (num_accepted / num_applied::float)
                        end as acceptance_rate

                    -- it's a little odd that we refactored the graduation rate to
                    -- count deferred people into their first cohort without doing
                    -- something similar for yield, but it seemed less necessary there
                    , case when num_accepted - num_deferred = 0
                        then null
                        else num_still_enrolled / (num_accepted - num_deferred)::float
                        end as yield
                    , expected_grad_rate

                    -- max_grad_rate and official_grad_rate are the same
                    , num_could_graduate_by_deadline::float / num_first_enrolled as official_grad_rate
                    , num_could_graduate_by_deadline::float / num_first_enrolled as max_grad_rate

                    , min_grad_rate


                    , case when num_first_enrolled = 0
                        then null
                        else num_meets_grad_req / num_first_enrolled::float
                        end as meets_grad_req_rate

                    , step_1.num_applied
                    , step_1.num_accepted
                    , step_1.num_still_enrolled
                    , step_1.num_active
                    , step_1.num_deferred
                    , step_1.num_meets_grad_req

                    , step_1.num_first_enrolled
                    , step_1.num_graduated
                    , step_1.num_did_graduate_by_deadline
                    , step_1.num_could_graduate_by_deadline
                    , step_1.num_did_not_graduate
                    , step_1.num_currently_deferred
                    , step_1.num_currently_enrolled_in_orig_cohort
                    , step_1.num_currently_enrolled_in_later_cohort
                from step_1
            )
            select * from step_2
            with no data
        ~

        add_index :cohort_conversion_rates, :cohort_id, :unique => true

    end

    # update to graduation deadlines.  Set the deadline to 3 years.
    def version_20180801123727
        execute %Q~
            create materialized view cohort_conversion_rates as

            -- count up things that refer to the applications for this cohort (i.e. not
            -- looking at the first cohort a user enrolled in).  These things are not used
            -- for graduation rate calculations, but for yield, etc.
            with counts_from_this_cohort as (
                SELECT
                    aps.cohort_id
                    , count(distinct aps.id) as num_applied
                    , count(case when aps.was_accepted then true else null end) as num_accepted
                    , count(case when cohort_user_progress_records.status in ('enrolled', 'graduated', 'failed', 'expelled') then true else null end) as num_still_enrolled
                    , count(case when cohort_user_progress_records.status in ('enrolled', 'pending_enrollment') then true else null end) as num_active
                    , count(case when cohort_user_progress_records.status = 'deferred' then true else null end) as num_deferred
                    , count(case when cohort_user_progress_records.meets_graduation_requirements then true else null end) as num_meets_grad_req

                from cohort_applications_plus aps
                    left join cohort_user_progress_records
                        on aps.user_id = cohort_user_progress_records.user_id
                        and aps.cohort_id = cohort_user_progress_records.cohort_id
                group by aps.cohort_id
            )

            , enrollment_records_with_chance_of_graduating as (
                select
                    enrollment_records.*

                    -- This is necessary because we do not define a chance_of_graduating
                    -- for all cohorts.  It's not set for MBA1 because we had to leave MBA1
                    -- out of the MDPPredictor stuff, since it is so unique.  It's not set
                    -- for EMBA because we haven't gotten around to it.  I haven't even thought
                    -- about how this stuff would work for program types with schedules or different
                    -- cohorts
                    , case
                        when chance_of_graduating is not NULL
                            then chance_of_graduating
                        when final_status = 'graduated'
                            then 1
                        when final_status = 'did_not_graduate'
                            then 0
                        else
                            null
                    end as chance_of_graduating
                from
                    program_enrollment_progress_records enrollment_records
                    left join user_chance_of_graduating_records
                        on user_chance_of_graduating_records.user_id = enrollment_records.user_id
                        and user_chance_of_graduating_records.program_type = enrollment_records.program_type
            )

            -- count up things that refer to the users who originally enrolled in
            -- a particular cohort.  These are things related to graduation rates.
            , counts_from_users_who_first_enrolled_in_this_cohort as (
                select
                    enrollment_records.original_cohort_id
                    , count(*) as num_first_enrolled
                    , count(case when enrollment_records.final_status = 'graduated' then true else null end) as num_graduated
                    , count(case when enrollment_records.consider_graduated_by_deadline then true else null end) as num_graduated_by_deadline
                    , count(case when enrollment_records.final_status = 'did_not_graduate' then true else null end) as num_did_not_graduate
                    , count(case when enrollment_records.final_status = 'deferred' then true else null end) as num_currently_deferred
                    , count(case when enrollment_records.original_cohort_id = enrollment_records.final_cohort_id and enrollment_records.final_status in ('enrolled', 'pending_enrollment') then true else null end) as num_currently_enrolled_in_orig_cohort
                    , count(case when enrollment_records.original_cohort_id != enrollment_records.final_cohort_id and enrollment_records.final_status in ('enrolled', 'pending_enrollment') then true else null end) as num_currently_enrolled_in_later_cohort
                    , avg(chance_of_graduating) as expected_grad_rate
                    , avg(case
                        when chance_of_graduating is null then 1
                        when chance_of_graduating > 0 then 1
                        else 0 end
                    ) as max_grad_rate
                    , avg(case
                        when chance_of_graduating is null then 0
                        when chance_of_graduating < 1
                        then 0
                        else 1 end
                    ) as min_grad_rate

                from enrollment_records_with_chance_of_graduating enrollment_records
                    join published_cohorts original_cohorts
                        on original_cohorts.id = enrollment_records.original_cohort_id
                group by
                    enrollment_records.original_cohort_id
            )

            -- put all the counts together and filter out irrelevant program types
            , step_1 as (
                select
                    counts_from_this_cohort.cohort_id
                    , published_cohorts.program_type
                    , published_cohorts.name as cohort_name

                    -- these counts refer to applications for this cohort
                    , counts_from_this_cohort.num_applied
                    , counts_from_this_cohort.num_accepted
                    , counts_from_this_cohort.num_still_enrolled
                    , counts_from_this_cohort.num_active
                    , counts_from_this_cohort.num_meets_grad_req
                    , counts_from_this_cohort.num_deferred


                    -- these counts refer to people who first enrolled in this cohort, and
                    -- indicate what their status is in the last cohort they were in
                    , counts_from_users_who_first_enrolled_in_this_cohort.num_first_enrolled
                    , counts_from_users_who_first_enrolled_in_this_cohort.num_graduated
                    , counts_from_users_who_first_enrolled_in_this_cohort.num_graduated_by_deadline
                    , counts_from_users_who_first_enrolled_in_this_cohort.num_did_not_graduate
                    , counts_from_users_who_first_enrolled_in_this_cohort.num_currently_deferred
                    , counts_from_users_who_first_enrolled_in_this_cohort.num_currently_enrolled_in_orig_cohort
                    , counts_from_users_who_first_enrolled_in_this_cohort.num_currently_enrolled_in_later_cohort
                    , counts_from_users_who_first_enrolled_in_this_cohort.expected_grad_rate
                    , counts_from_users_who_first_enrolled_in_this_cohort.min_grad_rate
                    , counts_from_users_who_first_enrolled_in_this_cohort.max_grad_rate

                from counts_from_this_cohort
                    join counts_from_users_who_first_enrolled_in_this_cohort
                        on counts_from_users_who_first_enrolled_in_this_cohort.original_cohort_id = counts_from_this_cohort.cohort_id
                    join published_cohorts
                        on published_cohorts.id = counts_from_this_cohort.cohort_id
                WHERE program_type in ('mba', 'emba')
                order by
                    published_cohorts.program_type
                    , published_cohorts.start_date
            )

            -- do some division to get some averages
            , step_2 as (
                select
                    step_1.cohort_id
                    , step_1.program_type
                    , step_1.cohort_name
                    , case when num_applied = 0
                        then null
                        else (num_accepted / num_applied::float)
                        end as acceptance_rate

                    -- it's a little odd that we refactored the graduation rate to
                    -- count deferred people into their first cohort without doing
                    -- something similar for yield, but it seemed less necessary there
                    , case when num_accepted - num_deferred = 0
                        then null
                        else num_still_enrolled / (num_accepted - num_deferred)::float
                        end as yield
                    , expected_grad_rate
                    , num_graduated_by_deadline::float / num_first_enrolled as official_grad_rate

                    , min_grad_rate
                    , max_grad_rate


                    , case when num_first_enrolled = 0
                        then null
                        else num_meets_grad_req / num_first_enrolled::float
                        end as meets_grad_req_rate

                    , step_1.num_applied
                    , step_1.num_accepted
                    , step_1.num_still_enrolled
                    , step_1.num_active
                    , step_1.num_deferred
                    , step_1.num_meets_grad_req

                    , step_1.num_first_enrolled
                    , step_1.num_graduated
                    , step_1.num_graduated_by_deadline
                    , step_1.num_did_not_graduate
                    , step_1.num_currently_deferred
                    , step_1.num_currently_enrolled_in_orig_cohort
                    , step_1.num_currently_enrolled_in_later_cohort
                from step_1
            )
            select * from step_2
            with no data
        ~

        add_index :cohort_conversion_rates, :cohort_id, :unique => true

    end

    # graduation deadlines
    def version_20180723070738
        execute %Q~
            create materialized view cohort_conversion_rates as

            -- count up things that refer to the applications for this cohort (i.e. not
            -- looking at the first cohort a user enrolled in).  These things are not used
            -- for graduation rate calculations, but for yield, etc.
            with counts_from_this_cohort as (
                SELECT
                    aps.cohort_id
                    , count(distinct aps.id) as num_applied
                    , count(case when aps.was_accepted then true else null end) as num_accepted
                    , count(case when cohort_user_progress_records.status in ('enrolled', 'graduated', 'failed', 'expelled') then true else null end) as num_still_enrolled
                    , count(case when cohort_user_progress_records.status in ('enrolled', 'pending_enrollment') then true else null end) as num_active
                    , count(case when cohort_user_progress_records.status = 'deferred' then true else null end) as num_deferred
                    , count(case when cohort_user_progress_records.meets_graduation_requirements then true else null end) as num_meets_grad_req

                from cohort_applications_plus aps
                    left join cohort_user_progress_records
                        on aps.user_id = cohort_user_progress_records.user_id
                        and aps.cohort_id = cohort_user_progress_records.cohort_id
                group by aps.cohort_id
            )

            , enrollment_records_with_chance_of_graduating as (
                select
                    enrollment_records.*

                    -- This is necessary because we do not define a chance_of_graduating
                    -- for all cohorts.  It's not set for MBA1 because we had to leave MBA1
                    -- out of the MDPPredictor stuff, since it is so unique.  It's not set
                    -- for EMBA because we haven't gotten around to it.  I haven't even thought
                    -- about how this stuff would work for program types with schedules or different
                    -- cohorts
                    , case
                        when chance_of_graduating is not NULL
                            then chance_of_graduating
                        when final_status = 'graduated'
                            then 1
                        when final_status = 'did_not_graduate'
                            then 0
                        else
                            null
                    end as chance_of_graduating
                from
                    program_enrollment_progress_records enrollment_records
                    left join user_chance_of_graduating_records
                        on user_chance_of_graduating_records.user_id = enrollment_records.user_id
                        and user_chance_of_graduating_records.program_type = enrollment_records.program_type
            )

            -- count up things that refer to the users who originally enrolled in
            -- a particular cohort.  These are things related to graduation rates.
            , counts_from_users_who_first_enrolled_in_this_cohort as (
                select
                    enrollment_records.original_cohort_id
                    , count(*) as num_first_enrolled
                    , count(case when enrollment_records.final_status = 'graduated' then true else null end) as num_graduated
                    , case when original_cohorts.grad_deadline_150_perc < now()
                        then count(case when enrollment_records.graduated_by_deadline_150_perc then true else null end)
                        else NULL
                        end as num_graduated_by_deadline_150_perc
                    , case when original_cohorts.grad_deadline_9_12 < now()
                        then count(case when enrollment_records.graduated_by_deadline_9_12 then true else null end)
                        else NULL
                        end as num_graduated_by_deadline_9_12
                    , count(case when enrollment_records.final_status = 'did_not_graduate' then true else null end) as num_did_not_graduate
                    , count(case when enrollment_records.final_status = 'deferred' then true else null end) as num_currently_deferred
                    , count(case when enrollment_records.original_cohort_id = enrollment_records.final_cohort_id and enrollment_records.final_status in ('enrolled', 'pending_enrollment') then true else null end) as num_currently_enrolled_in_orig_cohort
                    , count(case when enrollment_records.original_cohort_id != enrollment_records.final_cohort_id and enrollment_records.final_status in ('enrolled', 'pending_enrollment') then true else null end) as num_currently_enrolled_in_later_cohort
                    , avg(chance_of_graduating) as expected_grad_rate
                    , avg(case
                        when chance_of_graduating is null then 1
                        when chance_of_graduating > 0 then 1
                        else 0 end
                    ) as max_grad_rate
                    , avg(case
                        when chance_of_graduating is null then 0
                        when chance_of_graduating < 1
                        then 0
                        else 1 end
                    ) as min_grad_rate

                from enrollment_records_with_chance_of_graduating enrollment_records
                    join published_cohorts original_cohorts
                        on original_cohorts.id = enrollment_records.original_cohort_id
                group by
                    enrollment_records.original_cohort_id
                    , original_cohorts.grad_deadline_150_perc
                    , original_cohorts.grad_deadline_9_12
            )

            -- put all the counts together and filter out irrelevant program types
            , step_1 as (
                select
                    counts_from_this_cohort.cohort_id
                    , published_cohorts.program_type
                    , published_cohorts.name as cohort_name

                    -- these counts refer to applications for this cohort
                    , counts_from_this_cohort.num_applied
                    , counts_from_this_cohort.num_accepted
                    , counts_from_this_cohort.num_still_enrolled
                    , counts_from_this_cohort.num_active
                    , counts_from_this_cohort.num_meets_grad_req
                    , counts_from_this_cohort.num_deferred


                    -- these counts refer to people who first enrolled in this cohort, and
                    -- indicate what their status is in the last cohort they were in
                    , counts_from_users_who_first_enrolled_in_this_cohort.num_first_enrolled
                    , counts_from_users_who_first_enrolled_in_this_cohort.num_graduated
                    , counts_from_users_who_first_enrolled_in_this_cohort.num_graduated_by_deadline_150_perc
                    , counts_from_users_who_first_enrolled_in_this_cohort.num_graduated_by_deadline_9_12
                    , counts_from_users_who_first_enrolled_in_this_cohort.num_did_not_graduate
                    , counts_from_users_who_first_enrolled_in_this_cohort.num_currently_deferred
                    , counts_from_users_who_first_enrolled_in_this_cohort.num_currently_enrolled_in_orig_cohort
                    , counts_from_users_who_first_enrolled_in_this_cohort.num_currently_enrolled_in_later_cohort
                    , counts_from_users_who_first_enrolled_in_this_cohort.expected_grad_rate
                    , counts_from_users_who_first_enrolled_in_this_cohort.min_grad_rate
                    , counts_from_users_who_first_enrolled_in_this_cohort.max_grad_rate

                from counts_from_this_cohort
                    join counts_from_users_who_first_enrolled_in_this_cohort
                        on counts_from_users_who_first_enrolled_in_this_cohort.original_cohort_id = counts_from_this_cohort.cohort_id
                    join published_cohorts
                        on published_cohorts.id = counts_from_this_cohort.cohort_id
                WHERE program_type in ('mba', 'emba')
                order by
                    published_cohorts.program_type
                    , published_cohorts.start_date
            )

            -- do some division to get some averages
            , step_2 as (
                select
                    step_1.cohort_id
                    , step_1.program_type
                    , step_1.cohort_name
                    , case when num_applied = 0
                        then null
                        else (num_accepted / num_applied::float)
                        end as acceptance_rate

                    -- it's a little odd that we refactored the graduation rate to
                    -- count deferred people into their first cohort without doing
                    -- something similar for yield, but it seemed less necessary there
                    , case when num_accepted - num_deferred = 0
                        then null
                        else num_still_enrolled / (num_accepted - num_deferred)::float
                        end as yield
                    , expected_grad_rate
                    , num_graduated_by_deadline_150_perc::float / num_first_enrolled as grad_rate_150_perc
                    , num_graduated_by_deadline_9_12::float / num_first_enrolled as grad_rate_9_12

                    , min_grad_rate
                    , max_grad_rate


                    , case when num_first_enrolled = 0
                        then null
                        else num_meets_grad_req / num_first_enrolled::float
                        end as meets_grad_req_rate

                    , step_1.num_applied
                    , step_1.num_accepted
                    , step_1.num_still_enrolled
                    , step_1.num_active
                    , step_1.num_deferred
                    , step_1.num_meets_grad_req

                    , step_1.num_first_enrolled
                    , step_1.num_graduated
                    , step_1.num_graduated_by_deadline_150_perc
                    , step_1.num_graduated_by_deadline_9_12
                    , step_1.num_did_not_graduate
                    , step_1.num_currently_deferred
                    , step_1.num_currently_enrolled_in_orig_cohort
                    , step_1.num_currently_enrolled_in_later_cohort
                from step_1
            )
            select * from step_2
            with no data
        ~

        add_index :cohort_conversion_rates, :cohort_id, :unique => true

    end

    # use expected graduation rates
    def version_20180504161347
        execute %Q~
            create materialized view cohort_conversion_rates as

            -- count up things that refer to the applications for this cohort (i.e. not
            -- looking at the first cohort a user enrolled in).  These things are not used
            -- for graduation rate calculations, but for yield, etc.
            with counts_from_this_cohort as (
                SELECT
                    aps.cohort_id
                    , count(distinct aps.id) as num_applied
                    , count(case when aps.was_accepted then true else null end) as num_accepted
                    , count(case when cohort_user_progress_records.status in ('enrolled', 'graduated', 'failed', 'expelled') then true else null end) as num_enrolled
                    , count(case when cohort_user_progress_records.status in ('enrolled', 'pending_enrollment') then true else null end) as num_active
                    , count(case when cohort_user_progress_records.status = 'deferred' then true else null end) as num_deferred
                    , count(case when cohort_user_progress_records.meets_graduation_requirements then true else null end) as num_meets_grad_req

                from cohort_applications_plus aps
                    left join cohort_user_progress_records
                        on aps.user_id = cohort_user_progress_records.user_id
                        and aps.cohort_id = cohort_user_progress_records.cohort_id
                group by aps.cohort_id
            )

            , enrollment_records_with_chance_of_graduating as (
                select
                    enrollment_records.*

                    -- This is necessary because we do not define a chance_of_graduating
                    -- for all cohorts.  It's not set for MBA1 because we had to leave MBA1
                    -- out of the MDPPredictor stuff, since it is so unique.  It's not set
                    -- for EMBA because we haven't gotten around to it.  I haven't even thought
                    -- about how this stuff would work for program types with schedules or different
                    -- cohorts
                    , case
                        when chance_of_graduating is not NULL
                            then chance_of_graduating
                        when final_status = 'graduated'
                            then 1
                        when final_status = 'did_not_graduate'
                            then 0
                        else
                            null
                    end as chance_of_graduating
                from
                    program_enrollment_progress_records enrollment_records
                    left join user_chance_of_graduating_records
                        on user_chance_of_graduating_records.user_id = enrollment_records.user_id
                        and user_chance_of_graduating_records.program_type = enrollment_records.program_type
            )

            -- count up things that refer to the users who originally enrolled in
            -- a particular cohort.  These are things related to graduation rates.
            , counts_from_users_who_first_enrolled_in_this_cohort as (
                select
                    enrollment_records.original_cohort_id
                    , count(*) as num_enrolled
                    , count(case when enrollment_records.final_status = 'graduated' then true else null end) as num_graduated
                    , count(case when enrollment_records.final_status = 'did_not_graduate' then true else null end) as num_did_not_graduate
                    , count(case when enrollment_records.final_status = 'deferred' then true else null end) as num_currently_deferred
                    , count(case when enrollment_records.original_cohort_id = enrollment_records.final_cohort_id and enrollment_records.final_status in ('enrolled', 'pending_enrollment') then true else null end) as num_currently_enrolled_in_orig_cohort
                    , count(case when enrollment_records.original_cohort_id != enrollment_records.final_cohort_id and enrollment_records.final_status in ('enrolled', 'pending_enrollment') then true else null end) as num_currently_enrolled_in_later_cohort
                    , avg(chance_of_graduating) as expected_grad_rate
                    , avg(case
                        when chance_of_graduating is null then 1
                        when chance_of_graduating > 0 then 1
                        else 0 end
                    ) as max_grad_rate
                    , avg(case
                        when chance_of_graduating is null then 0
                        when chance_of_graduating < 1
                        then 0
                        else 1 end
                    ) as min_grad_rate

                from enrollment_records_with_chance_of_graduating enrollment_records
                group by
                    enrollment_records.original_cohort_id
            )

            -- put all the counts together and filter out irrelevant program types
            , step_1 as (
                select
                    counts_from_this_cohort.cohort_id
                    , published_cohorts.program_type
                    , published_cohorts.name as cohort_name

                    -- these counts refer to applications for this cohort
                    , counts_from_this_cohort.num_applied
                    , counts_from_this_cohort.num_accepted
                    , counts_from_this_cohort.num_enrolled
                    , counts_from_this_cohort.num_active
                    , counts_from_this_cohort.num_meets_grad_req
                    , counts_from_this_cohort.num_deferred


                    -- these counts refer to people who first enrolled in this cohort, and
                    -- indicate what their status is in the last cohort they were in
                    , counts_from_users_who_first_enrolled_in_this_cohort.num_enrolled as num_first_enrolled
                    , counts_from_users_who_first_enrolled_in_this_cohort.num_graduated
                    , counts_from_users_who_first_enrolled_in_this_cohort.num_did_not_graduate
                    , counts_from_users_who_first_enrolled_in_this_cohort.num_currently_deferred
                    , counts_from_users_who_first_enrolled_in_this_cohort.num_currently_enrolled_in_orig_cohort
                    , counts_from_users_who_first_enrolled_in_this_cohort.num_currently_enrolled_in_later_cohort
                    , counts_from_users_who_first_enrolled_in_this_cohort.expected_grad_rate
                    , counts_from_users_who_first_enrolled_in_this_cohort.min_grad_rate
                    , counts_from_users_who_first_enrolled_in_this_cohort.max_grad_rate

                from counts_from_this_cohort
                    join counts_from_users_who_first_enrolled_in_this_cohort
                        on counts_from_users_who_first_enrolled_in_this_cohort.original_cohort_id = counts_from_this_cohort.cohort_id
                    join published_cohorts
                        on published_cohorts.id = counts_from_this_cohort.cohort_id
                WHERE program_type in ('mba', 'emba')
                order by
                    published_cohorts.program_type
                    , published_cohorts.start_date
            )

            -- do some division to get some averages
            , step_2 as (
                select
                    step_1.cohort_id
                    , step_1.program_type
                    , step_1.cohort_name
                    , case when num_applied = 0
                        then null
                        else (num_accepted / num_applied::float)
                        end as acceptance_rate

                    -- it's a little odd that we refactored the graduation rate to
                    -- count deferred people into their first cohort without doing
                    -- something similar for yield, but it seemed less necessary there
                    , case when num_accepted - num_deferred = 0
                        then null
                        else num_enrolled / (num_accepted - num_deferred)::float
                        end as yield
                    , expected_grad_rate
                    , min_grad_rate
                    , max_grad_rate


                    , case when num_enrolled = 0
                        then null
                        else num_meets_grad_req / num_enrolled::float
                        end as meets_grad_req_rate

                    , step_1.num_applied
                    , step_1.num_accepted
                    , step_1.num_enrolled
                    , step_1.num_active
                    , step_1.num_deferred
                    , step_1.num_meets_grad_req

                    , step_1.num_first_enrolled
                    , step_1.num_graduated
                    , step_1.num_did_not_graduate
                    , step_1.num_currently_deferred
                    , step_1.num_currently_enrolled_in_orig_cohort
                    , step_1.num_currently_enrolled_in_later_cohort
                from step_1
            )
            select * from step_2
            with no data
        ~

        add_index :cohort_conversion_rates, :cohort_id, :unique => true

    end

    # change the way we handle deferred users, assigning each user to the first
    # cohort they enrolled in for the purposes of graduation rates
    def version_20180220203724
        execute %Q~
            create materialized view cohort_conversion_rates as

            -- We want to get an expected graduation rate for deferred and enrolled people so we can
            -- make some kind of prediction on a cohort's exact graduation rate when some percentage
            -- of it's members are still enrolled or deferred.
            --
            -- It's hard
            -- to say who we should look at when determining the graduation rate for deferred
            -- people.  If we look at everyone who has either graduated or failed, we might get
            -- an underestimation, since you can fail faster than you can graduate.  Let's look at
            -- anyone who has failed or graduated if it has been 18 months after their first cohort's
            -- start (not scientific, but it's something)

            -- get deferred users
            with deferred_users as (
                SELECT
                    distinct user_id
                from cohort_status_changes
                where status='deferred'
            )

            -- figure out the counts for deferred users
            , deferred_averages_1 as (
                SELECT
                    program_enrollment_progress_records.program_type
                    , count(*) as total
                    , sum(case when final_status = 'graduated' then 1 else 0 end)::float as num_graduated
                    , sum(case when final_status = 'did_not_graduate' then 1 else 0 end)::float as num_did_not_graduate
                from program_enrollment_progress_records
                    join published_cohorts on published_cohorts.id = program_enrollment_progress_records.original_cohort_id
                    join deferred_users on deferred_users.user_id = program_enrollment_progress_records.user_id
                where
                    published_cohorts.start_date < now() - interval '18 months'
                group by program_enrollment_progress_records.program_type
            )

            -- divide to get averages for deferred users
            , deferred_averages_2 as (
                SELECT
                    deferred_averages_1.*
                    , case when num_graduated > 0 then num_graduated / (num_graduated + num_did_not_graduate) else null end as graduation_rate
                from deferred_averages_1
            )

            -- do the same thing for non-deferred users
            , non_deferred_averages_1 as (
                SELECT
                    program_enrollment_progress_records.program_type
                    , count(*) as total
                    , sum(case when final_status = 'graduated' then 1 else 0 end)::float as num_graduated
                    , sum(case when final_status = 'did_not_graduate' then 1 else 0 end)::float as num_did_not_graduate
                from program_enrollment_progress_records
                    join published_cohorts on published_cohorts.id = program_enrollment_progress_records.original_cohort_id
                    left join deferred_users on deferred_users.user_id = program_enrollment_progress_records.user_id
                where
                    deferred_users.user_id is null
                    -- for people who never deferred, it's enough to just wait until the cohort is done
                    -- (add a little buffer just to be sure)
                    and published_cohorts.end_date < now() - interval '4 weeks'
                group by program_enrollment_progress_records.program_type
            )
            , non_deferred_averages_2 as (
                SELECT
                    non_deferred_averages_1.*
                    , case when num_graduated > 0 then num_graduated / (num_graduated + num_did_not_graduate) else null end as graduation_rate
                from non_deferred_averages_1
            )

            -- count up things that refer to the applications for this cohort (i.e. not
            -- looking at the first cohort a user enrolled in).  These things are not used
            -- for graduation rate calculations, but for yield, etc.
            , counts_from_this_cohort as (
                SELECT
                    aps.cohort_id
                    , count(distinct aps.id) as num_applied
                    , count(case when aps.was_accepted then true else null end) as num_accepted
                    , count(case when cohort_user_progress_records.status in ('enrolled', 'graduated', 'failed', 'expelled') then true else null end) as num_enrolled
                    , count(case when cohort_user_progress_records.status in ('enrolled', 'pending_enrollment') then true else null end) as num_active
                    , count(case when cohort_user_progress_records.status = 'deferred' then true else null end) as num_deferred
                    , count(case when cohort_user_progress_records.meets_graduation_requirements then true else null end) as num_meets_grad_req

                from cohort_applications_plus aps
                    left join cohort_user_progress_records
                        on aps.user_id = cohort_user_progress_records.user_id
                        and aps.cohort_id = cohort_user_progress_records.cohort_id
                group by aps.cohort_id
            )

            -- count up things that refer to the users who originally enrolled in
            -- a particular cohort.  These are things related to graduation rates.
            , counts_from_users_who_first_enrolled_in_this_cohort as (
                select
                    enrollment_records.original_cohort_id
                    , count(*) as num_enrolled
                    , count(case when enrollment_records.final_status = 'graduated' then true else null end) as num_graduated
                    , count(case when enrollment_records.final_status = 'did_not_graduate' then true else null end) as num_did_not_graduate
                    , count(case when enrollment_records.final_status = 'deferred' then true else null end) as num_currently_deferred
                    , count(case when enrollment_records.original_cohort_id = enrollment_records.final_cohort_id and enrollment_records.final_status in ('enrolled', 'pending_enrollment') then true else null end) as num_currently_enrolled_in_orig_cohort
                    , count(case when enrollment_records.original_cohort_id != enrollment_records.final_cohort_id and enrollment_records.final_status in ('enrolled', 'pending_enrollment') then true else null end) as num_currently_enrolled_in_later_cohort

                from program_enrollment_progress_records enrollment_records
                group by
                    enrollment_records.original_cohort_id
            )

            -- put all the counts together and filter out irrelevant program types
            , step_1 as (
                select
                    counts_from_this_cohort.cohort_id
                    , published_cohorts.program_type
                    , published_cohorts.name as cohort_name

                    -- these counts refer to applications for this cohort
                    , counts_from_this_cohort.num_applied
                    , counts_from_this_cohort.num_accepted
                    , counts_from_this_cohort.num_enrolled
                    , counts_from_this_cohort.num_active
                    , counts_from_this_cohort.num_meets_grad_req
                    , counts_from_this_cohort.num_deferred


                    -- these counts refer to people who first enrolled in this cohort, and
                    -- indicate what their status is in the last cohort they were in
                    , counts_from_users_who_first_enrolled_in_this_cohort.num_enrolled as num_first_enrolled
                    , counts_from_users_who_first_enrolled_in_this_cohort.num_graduated
                    , counts_from_users_who_first_enrolled_in_this_cohort.num_did_not_graduate
                    , counts_from_users_who_first_enrolled_in_this_cohort.num_currently_deferred
                    , counts_from_users_who_first_enrolled_in_this_cohort.num_currently_enrolled_in_orig_cohort
                    , counts_from_users_who_first_enrolled_in_this_cohort.num_currently_enrolled_in_later_cohort

                from counts_from_this_cohort
                    join counts_from_users_who_first_enrolled_in_this_cohort
                        on counts_from_users_who_first_enrolled_in_this_cohort.original_cohort_id = counts_from_this_cohort.cohort_id
                    join published_cohorts
                        on published_cohorts.id = counts_from_this_cohort.cohort_id
                WHERE program_type in ('mba', 'emba')
                order by
                    published_cohorts.program_type
                    , published_cohorts.start_date
            )

            -- do some division to get some averages
            , step_2 as (
                select
                    step_1.cohort_id
                    , step_1.program_type
                    , step_1.cohort_name
                    , case when num_applied = 0
                        then null
                        else (num_accepted / num_applied::float)
                        end as acceptance_rate

                    -- it's a little odd that we refactored the graduation rate to
                    -- count deferred people into their first cohort without doing
                    -- something similar for yield, but it seemed less necessary there
                    , case when num_accepted - num_deferred = 0
                        then null
                        else num_enrolled / (num_accepted - num_deferred)::float
                        end as yield


                    -- the next three grad rates are very similar.  The difference is just how much
                    -- weighting we give to people who still could graduate.  For the expected_grad_rate,
                    -- we multiple by the overall averages we found above.  For the min, we assume none
                    -- of them will graduate, for the max we assume they all will.
                    , case when num_graduated = 0
                        then null
                        else
                            (
                                num_graduated +
                                (select graduation_rate from deferred_averages_2 where deferred_averages_2.program_type = step_1.program_type)*(num_currently_enrolled_in_later_cohort+num_currently_deferred) +
                                (select graduation_rate from non_deferred_averages_2 where non_deferred_averages_2.program_type = step_1.program_type)*(num_currently_enrolled_in_orig_cohort)
                            )
                            /
                            (
                                num_did_not_graduate+num_graduated
                                +num_currently_enrolled_in_later_cohort
                                +num_currently_enrolled_in_orig_cohort
                                +num_currently_deferred
                            )::float
                        end as expected_grad_rate

                    , case when num_did_not_graduate + num_graduated = 0
                        then null
                        else
                            (
                                num_graduated +
                                (0)*(num_currently_enrolled_in_later_cohort+num_currently_deferred) +
                                (0)*(num_currently_enrolled_in_orig_cohort)
                            )
                            /
                            (
                                num_did_not_graduate+num_graduated
                                +num_currently_enrolled_in_later_cohort
                                +num_currently_enrolled_in_orig_cohort
                                +num_currently_deferred
                            )::float
                        end as min_grad_rate

                    , case when num_did_not_graduate + num_graduated = 0
                        then null
                        else
                            (
                                num_graduated +
                                (1)*(num_currently_enrolled_in_later_cohort+num_currently_deferred) +
                                (1)*(num_currently_enrolled_in_orig_cohort)
                            )
                            /
                            (
                                num_did_not_graduate+num_graduated
                                +num_currently_enrolled_in_later_cohort
                                +num_currently_enrolled_in_orig_cohort
                                +num_currently_deferred
                            )::float
                        end as max_grad_rate

                    , case when num_enrolled = 0
                        then null
                        else num_meets_grad_req / num_enrolled::float
                        end as meets_grad_req_rate

                    , step_1.num_applied
                    , step_1.num_accepted
                    , step_1.num_enrolled
                    , step_1.num_active
                    , step_1.num_deferred
                    , step_1.num_meets_grad_req

                    , step_1.num_first_enrolled
                    , step_1.num_graduated
                    , step_1.num_did_not_graduate
                    , step_1.num_currently_deferred
                    , step_1.num_currently_enrolled_in_orig_cohort
                    , step_1.num_currently_enrolled_in_later_cohort

                    , (select graduation_rate from non_deferred_averages_2 where non_deferred_averages_2.program_type = step_1.program_type) expected_non_deferred_grad_rate
                    , (select graduation_rate from deferred_averages_2 where deferred_averages_2.program_type = step_1.program_type) expected_deferred_grad_rate
                from step_1
            )
            select * from step_2
            with no data
        ~

        add_index :cohort_conversion_rates, :cohort_id, :unique => true

    end

    def version_20170323141852
        execute %Q~
            create materialized view cohort_conversion_rates as
            with step_1 as (
                select
                    aps.cohort_id
                    , aps.cohort_name
                    , count(*) as num_applied
                    , count(case when aps.was_accepted = true then true else null end) as num_accepted

                    , count(case when
                        cohort_user_progress_records.status in ('enrolled', 'graduated', 'failed', 'expelled') then true
                        else null
                        end) as num_enrolled
                    , count(case when
                        cohort_user_progress_records.status in ('enrolled', 'pending_enrollment') then true
                        else null
                        end) as num_active
                    , count(case when cohort_user_progress_records.status = 'graduated' then true else null end) as num_graduated
                    , count(case when cohort_user_progress_records.status = 'deferred' then true else null end) as num_deferred
                    , count(case when cohort_user_progress_records.status = 'did_not_enroll' then true else null end) as num_did_not_enroll
                    , count(case when
                        cohort_user_progress_records.status in ('failed', 'enrolled', 'graduated')
                        and cohort_user_progress_records.meets_graduation_requirements = true
                        then true else null end) as num_meets_grad_req
                from cohort_applications_plus aps
                    left join cohort_user_progress_records
                        on aps.user_id = cohort_user_progress_records.user_id
                        and aps.cohort_id = cohort_user_progress_records.cohort_id
                group by
                    aps.cohort_id
                    , aps.cohort_name
                order by aps.cohort_name
            )
            , step_2 as (
                select
                    cohort_id
                    , cohort_name
                    , case when num_applied = 0
                        then null
                        else (num_accepted / num_applied::float)
                        end as acceptance_rate

                    , case when num_accepted - num_deferred = 0
                        then null
                        else num_enrolled / (num_accepted - num_deferred)::float
                        end as yield

                    , case when num_enrolled = 0
                        then null
                        else num_graduated / num_enrolled::float
                        end as graduation_rate
                    , case when num_enrolled = 0
                        then null
                        else num_meets_grad_req / num_enrolled::float
                        end as meets_grad_req_rate
                    , step_1.num_applied
                    , step_1.num_accepted
                    , step_1.num_enrolled
                    , step_1.num_active
                    , step_1.num_deferred
                    , step_1.num_did_not_enroll
                    , step_1.num_graduated
                    , step_1.num_meets_grad_req
                from step_1
            )
            select * from step_2
            with no data
        ~

        add_index :cohort_conversion_rates, :cohort_id, :unique => true

    end

    def version_20170221140313
        execute %Q~
            create materialized view cohort_conversion_rates as
            with step_1 as (
                select
                    aps.cohort_id
                    , aps.cohort_name
                    , count(*) as num_applied
                    , count(case when aps.was_accepted = true then true else null end) as num_accepted

                    , count(case when
                        cohort_user_progress_records.status in ('enrolled', 'graduated', 'failed', 'expelled') then true
                        else null
                        end) as num_enrolled
                    , count(case when cohort_user_progress_records.status = 'graduated' then true else null end) as num_graduated
                    , count(case when cohort_user_progress_records.status = 'deferred' then true else null end) as num_deferred
                    , count(case when cohort_user_progress_records.status = 'did_not_enroll' then true else null end) as num_did_not_enroll
                    , count(case when
                        cohort_user_progress_records.status in ('failed', 'enrolled', 'graduated')
                        and cohort_user_progress_records.meets_graduation_requirements = true
                        then true else null end) as num_meets_grad_req
                from cohort_applications_plus aps
                    left join cohort_user_progress_records
                        on aps.user_id = cohort_user_progress_records.user_id
                        and aps.cohort_id = cohort_user_progress_records.cohort_id
                group by
                    aps.cohort_id
                    , aps.cohort_name
                order by aps.cohort_name
            )
            , step_2 as (
                select
                    cohort_id
                    , cohort_name
                    , case when num_applied = 0
                        then null
                        else (num_accepted / num_applied::float)
                        end as acceptance_rate

                    , case when num_accepted - num_deferred = 0
                        then null
                        else num_enrolled / (num_accepted - num_deferred)::float
                        end as yield

                    , case when num_enrolled = 0
                        then null
                        else num_graduated / num_enrolled::float
                        end as graduation_rate
                    , case when num_enrolled = 0
                        then null
                        else num_meets_grad_req / num_enrolled::float
                        end as meets_grad_req_rate
                    , step_1.num_applied
                    , step_1.num_accepted
                    , step_1.num_enrolled
                    , step_1.num_deferred
                    , step_1.num_did_not_enroll
                    , step_1.num_graduated
                    , step_1.num_meets_grad_req
                from step_1
            )
            select * from step_2
            with no data
        ~

        add_index :cohort_conversion_rates, :cohort_id, :unique => true

    end

end