class ViewHelpers::PublishedCohortLessonLocalePacks < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    def version_20200609141113
        # do nothing. converted to table
    end

    def version_20170324205906
        execute %Q~
            create materialized view published_cohort_lesson_locale_packs as
            select
                published_cohort_stream_locale_packs.cohort_id
                , published_cohort_stream_locale_packs.cohort_name
                , sum(case when published_cohort_stream_locale_packs.foundations then 1 else 0 end) > 0 as foundations
                , sum(case when published_cohort_stream_locale_packs.required then 1 else 0 end) > 0 as required
                , sum(case when published_cohort_stream_locale_packs.specialization then 1 else 0 end) > 0 as specialization
                , sum(case when published_cohort_stream_locale_packs.required then 0 else 1 end) > 0 as elective
                , sum(case when published_cohort_stream_locale_packs.exam then 1 else 0 end) > 0 as exam
                , published_stream_lesson_locale_packs.test
                , published_stream_lesson_locale_packs.assessment
                , published_stream_lesson_locale_packs.lesson_title
                , published_stream_lesson_locale_packs.lesson_locale_pack_id
                , published_stream_lesson_locale_packs.lesson_locales
            from published_cohort_stream_locale_packs
                join published_stream_lesson_locale_packs
                    on published_stream_lesson_locale_packs.stream_locale_pack_id = published_cohort_stream_locale_packs.stream_locale_pack_id
            group by
                published_stream_lesson_locale_packs.test
                , published_stream_lesson_locale_packs.assessment
                , published_stream_lesson_locale_packs.lesson_title
                , published_stream_lesson_locale_packs.lesson_locale_pack_id
                , published_stream_lesson_locale_packs.lesson_locales
                , published_cohort_stream_locale_packs.cohort_id
                , published_cohort_stream_locale_packs.cohort_name
        ~

        add_index :published_cohort_lesson_locale_packs, [:cohort_id, :lesson_locale_pack_id], :name => :cohort_and_lesson_lp_on_coh_lesson_lps, :unique => true
        add_index :published_cohort_lesson_locale_packs, :lesson_locale_pack_id, :name => :lesson_lp_on_cohort_lesson_locale_packs
    end

    def version_20170310204902
        execute %Q~
            create materialized view published_cohort_lesson_locale_packs as
            select
                published_cohort_stream_locale_packs.cohort_id
                , published_cohort_stream_locale_packs.cohort_name
                , sum(case when published_cohort_stream_locale_packs.foundations then 1 else 0 end) > 0 as foundations
                , sum(case when published_cohort_stream_locale_packs.required then 1 else 0 end) > 0 as required
                , sum(case when published_cohort_stream_locale_packs.specialization then 1 else 0 end) > 0 as specialization
                , sum(case when published_cohort_stream_locale_packs.required then 0 else 1 end) > 0 as elective
                , sum(case when published_cohort_stream_locale_packs.exam then 1 else 0 end) > 0 as exam
                , published_stream_lesson_locale_packs.test
                , published_stream_lesson_locale_packs.assessment
                , published_stream_lesson_locale_packs.lesson_title
                , published_stream_lesson_locale_packs.lesson_locale_pack_id
                , published_stream_lesson_locale_packs.lesson_locales
            from published_cohort_stream_locale_packs
                join published_stream_lesson_locale_packs
                    on published_stream_lesson_locale_packs.stream_locale_pack_id = published_cohort_stream_locale_packs.stream_locale_pack_id
            group by
                published_stream_lesson_locale_packs.test
                , published_stream_lesson_locale_packs.assessment
                , published_stream_lesson_locale_packs.lesson_title
                , published_stream_lesson_locale_packs.lesson_locale_pack_id
                , published_stream_lesson_locale_packs.lesson_locales
                , published_cohort_stream_locale_packs.cohort_id
                , published_cohort_stream_locale_packs.cohort_name
            with no data
        ~

        add_index :published_cohort_lesson_locale_packs, [:cohort_id, :lesson_locale_pack_id], :name => :cohort_and_lesson_lp_on_coh_lesson_lps, :unique => true
        add_index :published_cohort_lesson_locale_packs, :lesson_locale_pack_id, :name => :lesson_lp_on_cohort_lesson_locale_packs
    end

end