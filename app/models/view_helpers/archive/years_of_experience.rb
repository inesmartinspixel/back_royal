class ViewHelpers::YearsOfExperience < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    # Get rid of the view.  The information here was folded into career_profile_search_helpers
    def version_20180117163345
    end

    # Exclude work experience prior to graduation date of earliest education experience.
    # Prevent students who list a bunch of activities as work experience during their undergrad
    # from having an extra year (or more) of "work experience" in browse queries.
    def version_20171013223600
        execute %Q~
            create view years_of_experience as
            with step_1 as (
                select
                    career_profiles.id as career_profile_id
                    , min(work_experiences.start_date) earliest_work_start_date
                    , max(work_experiences.end_date) latest_work_end_date
                    , count(case when work_experiences.id is not null and work_experiences.end_date is null then true else null end) > 0 as has_current_work_experience
                    , (min(graduation_year) || '/01/01')::timestamp first_graduation_date
                    , (max(graduation_year) || '/01/01')::timestamp last_graduation_date
                from
                    career_profiles
                        left join work_experiences
                            on career_profiles.id = work_experiences.career_profile_id
                        left join education_experiences
                            on career_profiles.id = education_experiences.career_profile_id
                        left join users
                            on career_profiles.user_id = users.id
                -- duplicating the logic for decideing if a career profile is active or not
                -- not strictly necessary but could possible help performance
                where can_edit_career_profile = true
                and last_calculated_complete_percentage = 100
                and interested_in_joining_new_company in ('very_interested', 'interested', 'neutral')
                group by
                    career_profiles.id
            )
            , step_2 as (
                select
                    step_1.*
                    , case
                        -- If a user's work experience is post first_graduation_date, use earliest_work_start_date when possible
                        -- Else if a user's work experience is pre first_graduation_date, use last_graduation_date when possible
                        -- Else (no work experience), use NULL
                        when earliest_work_start_date > first_graduation_date or earliest_work_start_date is null or first_graduation_date is null then case
                            -- If there's a graduation date, we need to compare to now(), because
                            -- latest_experience_end may use now(), resulting in a negative result.
                            when earliest_work_start_date > now() then NULL
                            -- This will be NULL if there is no work experience (expected)
                            else earliest_work_start_date
                        end -- inner case
                        -- Avoid negative results from future dates
                        when last_graduation_date > now() then now()
                        else last_graduation_date
                        end as earliest_experience_start
                    , case
                        -- If a user has current work experience, use now.
                        -- Else if a user's work experience is pre first_graduation_date, use now.
                        -- Else (post-graduation work experience, but no current work experience), use latest_work_end_date
                        when has_current_work_experience then now()
                        when first_graduation_date > latest_work_end_date then now()
                        -- Avoid negative results from future dates
                        when latest_work_end_date > now() and first_graduation_date is not null then now()
                        -- This will be NULL if there is no work experience (expected)
                        else latest_work_end_date
                        end as latest_experience_end
                from step_1
            )
            select
                step_2.*
                , EXTRACT(epoch FROM latest_experience_end - earliest_experience_start) / EXTRACT(epoch FROM interval '1 year') as years
            from step_2
        ~
    end

    # Change YearsOfExperience to no longer use career_profile_active, but instead use can_edit_career_profile,
    # last_calculated_complete_percentage, and interested_in_joining_new_company to determine if the profile is
    # considered active or inactive.
    def version_20170728155738
        execute %Q~
            create view years_of_experience as
            with step_1 as (
                select
                    career_profiles.id as career_profile_id
                    , min(work_experiences.start_date) earliest_work_start_date
                    , max(work_experiences.end_date) latest_work_end_date
                    , count(case when work_experiences.id is not null and work_experiences.end_date is null then true else null end) > 0 as has_current_work_experience
                    , (max(graduation_year) || '/01/01')::timestamp last_graduation_date
                from
                    career_profiles
                        left join work_experiences
                            on career_profiles.id = work_experiences.career_profile_id
                        left join education_experiences
                            on career_profiles.id = education_experiences.career_profile_id
                        left join users
                            on career_profiles.user_id = users.id
                -- duplicating the logic for decideing if a career profile is active or not
                -- not strictly necessary but could possible help performance
                where can_edit_career_profile = true
                and last_calculated_complete_percentage = 100
                and interested_in_joining_new_company in ('very_interested', 'interested', 'neutral')
                group by
                    career_profiles.id
            )
            , step_2 as (
                select
                    step_1.*
                    , least(earliest_work_start_date, last_graduation_date) as earliest_experience_start
                    , case
                        when has_current_work_experience then now()
                        else latest_work_end_date
                        end as latest_experience_end
                from step_1
            )
            select
                step_2.*
                , EXTRACT(epoch FROM latest_experience_end - earliest_experience_start) / EXTRACT(epoch FROM interval '1 year') as years
            from step_2
        ~
    end

    def version_20170626073944
        execute %Q~
            create view years_of_experience as
            with step_1 as (
                select
                    career_profiles.id as career_profile_id
                    , min(work_experiences.start_date) earliest_work_start_date
                    , max(work_experiences.end_date) latest_work_end_date
                    , count(case when work_experiences.id is not null and work_experiences.end_date is null then true else null end) > 0 as has_current_work_experience
                    , (max(graduation_year) || '/01/01')::timestamp last_graduation_date
                from
                    career_profiles
                        left join work_experiences
                            on career_profiles.id = work_experiences.career_profile_id
                        left join education_experiences
                            on career_profiles.id = education_experiences.career_profile_id
                where career_profile_active = true -- not strictly necessary but could possible help performance
                group by
                    career_profiles.id
            )
            , step_2 as (
                select
                    step_1.*
                    , least(earliest_work_start_date, last_graduation_date) as earliest_experience_start
                    , case
                        when has_current_work_experience then now()
                        else latest_work_end_date
                        end as latest_experience_end
                from step_1
            )
            select
                step_2.*
                , EXTRACT(epoch FROM latest_experience_end - earliest_experience_start) / EXTRACT(epoch FROM interval '1 year') as years
            from step_2
        ~
    end

    def version_20170608162605
        execute %Q~
            create view years_of_experience as
            with step_1 as (
                select
                    career_profiles.id as career_profile_id
                    , min(work_experiences.start_date) earliest_work_start_date
                    , max(work_experiences.end_date) latest_work_end_date
                    , count(case when work_experiences.id is not null and work_experiences.end_date is null then true else null end) > 0 as has_current_work_experience
                    , (max(graduation_year) || '/01/01')::timestamp last_graduation_date
                from
                    career_profiles
                        left join work_experiences
                            on career_profiles.id = work_experiences.career_profile_id
                        left join education_experiences
                            on career_profiles.id = education_experiences.career_profile_id
                where career_profile_active = true -- not strictly necessary but could possible help performance
                group by
                    career_profiles.id
            )
            , step_2 as (
                select
                    step_1.*
                    , least(earliest_work_start_date, last_graduation_date) as earliest_experience_start
                    , case
                        when has_current_work_experience then now()
                        else latest_work_end_date
                        end as latest_experience_end
                from step_1
            )
            select
                step_2.*
                , round(EXTRACT(epoch FROM latest_experience_end - earliest_experience_start) / EXTRACT(epoch FROM interval '1 year')) as years
            from step_2
        ~
    end

end