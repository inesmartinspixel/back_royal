class ViewHelpers::CohortLessonLocalePacks < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    def version_20170310204902
        # do nothing.  renamed to published_cohort_lesson_locale_packs
    end

    def version_20170220200244
        execute %Q~
            create materialized view cohort_lesson_locale_packs as
            select
                cohort_stream_locale_packs.*
                , published_stream_lesson_locale_packs.test
                , published_stream_lesson_locale_packs.assessment
                , published_stream_lesson_locale_packs.lesson_title
                , published_stream_lesson_locale_packs.lesson_locale_pack_id
                , published_stream_lesson_locale_packs.lesson_locales
            from cohort_stream_locale_packs
                join published_stream_lesson_locale_packs on published_stream_lesson_locale_packs.stream_locale_pack_id = cohort_stream_locale_packs.stream_locale_pack_id
            with no data
        ~

        add_index :cohort_lesson_locale_packs, [:cohort_id, :stream_locale_pack_id, :lesson_locale_pack_id], :name => :cohort_and_lesson_lp_on_coh_lesson_lps, :unique => true
        add_index :cohort_lesson_locale_packs, :lesson_locale_pack_id
    end

end