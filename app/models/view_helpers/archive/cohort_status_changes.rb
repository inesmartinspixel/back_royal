class ViewHelpers::CohortStatusChanges < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    def version_20191125221306
        current
    end

    # Handle multiple saves committed together with the same version_created_at
    # so that we don't cause duplicate PG errors
    def version_20180828220207
        execute %Q~
            create materialized view cohort_status_changes as
            with indexed_application_versions as (
                select
                    -- in order to be careful of the case where the cohort_id has changed on an application over time,
                    -- we grab the cohort_id from the current record in cohort_applications
                    cohort_applications.user_id
                    , cohort_applications.cohort_id

                    -- rarely, there can be multiple updates with the same version_created_at.  This happens if rails makes multiple
                    -- updates to the application and then commits them all at once.  In those cases, we want the status and graduation_status
                    -- from the last save made in rails. For example, this happened when issuing a refund.
                    -- See this user_id:cohort_id:from_time
                    --  414f8014-53e9-4a48-985e-8363f28841a9,6682fe74-03be-4174-9de7-abae016f6546,"2018-08-27 13:50:00.612373"
                    , (array_agg(cohort_applications_versions.status order by cohort_applications_versions.updated_at desc))[1] as status
                    , (array_agg(cohort_applications_versions.graduation_status order by cohort_applications_versions.updated_at desc))[1] as graduation_status
                    , cohort_applications_versions.version_created_at
                    , row_number() OVER (PARTITION BY cohort_applications_versions.user_id, cohort_applications_versions.id
                                ORDER BY cohort_applications_versions.version_created_at ASC NULLS LAST) AS version_index
                from cohort_applications
                    join cohort_applications_versions on cohort_applications_versions.id = cohort_applications.id
                group by
                    cohort_applications.user_id
                    , cohort_applications_versions.user_id
                    , cohort_applications.cohort_id
                    , cohort_applications_versions.version_created_at
                    , cohort_applications_versions.id
                order by
                    cohort_applications_versions.user_id
                    , cohort_applications_versions.version_created_at
            )
            -- Join each version with the version that came after it, and record the change
            , application_changes as (
                select
                    old_versions.user_id
                    , old_versions.cohort_id
                    , old_versions.version_created_at as from_time
                    , coalesce(new_versions.version_created_at, '2099-01-01') as until_time
                    , old_versions.status as cohort_application_status
                    , coalesce(old_versions.graduation_status, 'pending') as graduation_status
                    , count(previous_late_enrollments.user_id) > 0 as enrolled_late
                from
                    indexed_application_versions old_versions
                        join published_cohort_content_details cohort_content_details
                            on cohort_content_details.cohort_id = old_versions.cohort_id
                        left join indexed_application_versions new_versions
                            on old_versions.user_id = new_versions.user_id
                                and old_versions.cohort_id = new_versions.cohort_id
                                and old_versions.version_index = new_versions.version_index - 1

                        -- this is a special fix for people who were expelled at the enrollment
                        -- deadline but later marked as accepted again.  We need them to show
                        -- up as enrolled.  So we mark them as enrolled_late if they have any
                        -- versions that are marked as accepted after the deadline
                        left join indexed_application_versions previous_late_enrollments
                            on old_versions.user_id = previous_late_enrollments.user_id
                                and old_versions.cohort_id = previous_late_enrollments.cohort_id
                                and old_versions.version_index >= previous_late_enrollments.version_index
                                and previous_late_enrollments.status = 'accepted'
                                and previous_late_enrollments.version_created_at >= enrollment_deadline
                group by
                    old_versions.user_id
                    , old_versions.cohort_id
                    , old_versions.version_created_at
                    , coalesce(new_versions.version_created_at, '2099-01-01')
                    , old_versions.status
                    , coalesce(old_versions.graduation_status, 'pending')

            )
            -- Find places where past_enrollment_deadline changed
            , past_enrollment_deadline_changes as (
                select
                    cohort_applications.user_id
                    , cohort_applications.cohort_id
                    , cohort_content_details.enrollment_deadline as from_time
                    , '2099-01-01'::timestamp as until_time
                    , true as past_enrollment_deadline
                    , application_changes.cohort_application_status = 'accepted' as enrolled_at_deadline
                    , cohorts.name
                    , cohorts.start_date
                from cohort_applications
                    join published_cohort_content_details cohort_content_details
                        on cohort_content_details.cohort_id = cohort_applications.cohort_id
                    join published_cohorts cohorts on cohorts.id = cohort_applications.cohort_id
                    join application_changes on
                        application_changes.user_id = cohort_applications.user_id
                        and application_changes.cohort_id = cohort_applications.cohort_id
                        and application_changes.from_time <= cohort_content_details.enrollment_deadline
                        and application_changes.until_time > cohort_content_details.enrollment_deadline
                        and cohort_content_details.enrollment_deadline < now()
            )
            -- Find places where foundations_complete changed
            , foundations_complete_changes as (
                select
                    cohort_user_lesson_progress_records.user_id
                    , cohort_user_lesson_progress_records.cohort_id
                    , true as foundations_complete
                    , max(cohort_user_lesson_progress_records.completed_at) as from_time
                    , '2099-01-01'::timestamp as until_time
                from
                    cohort_user_lesson_progress_records
                    join published_cohort_content_details cohort_content_details
                        on cohort_user_lesson_progress_records.cohort_id = cohort_content_details.cohort_id
                where
                    true
                    and foundations = true
                    and completed_at is not null
                group by
                    cohort_user_lesson_progress_records.user_id
                    , cohort_user_lesson_progress_records.cohort_id
                    , foundations_lesson_count
                having
                    count(*) >= foundations_lesson_count
            )
            -- Find places where showed_up changed
            , showed_up_changes as (
                select
                    cohort_user_lesson_progress_records.user_id
                    , cohort_user_lesson_progress_records.cohort_id
                    , true as showed_up
                    , min(cohort_user_lesson_progress_records.completed_at) as from_time
                    , '2099-01-01'::timestamp as until_time
                from
                    cohort_user_lesson_progress_records
                    join cohort_applications_plus
                        on cohort_user_lesson_progress_records.cohort_id = cohort_applications_plus.cohort_id
                        and cohort_user_lesson_progress_records.user_id = cohort_applications_plus.user_id
                where
                    completed_at > cohort_applications_plus.accepted_at
                group by
                    cohort_user_lesson_progress_records.user_id
                    , cohort_user_lesson_progress_records.cohort_id
            )
            -- Find the set of unique times in which something changed
            -- for each application
            , change_times as (
                select
                    user_id
                    , cohort_id
                    , from_time as change_time
                from application_changes

                union all

                select
                    user_id
                    , cohort_id
                    , from_time as change_time
                from foundations_complete_changes

                union all

                select
                    user_id
                    , cohort_id
                    , from_time as change_time
                from showed_up_changes

                union all

                select
                    user_id
                    , cohort_id
                    , from_time as change_time
                from past_enrollment_deadline_changes

                order by change_time

            )
            -- Put the change times for each user in order with an index so we can
            -- join each version with the one after it
            , change_times_2 as (
                select
                    change_times.user_id
                    , change_times.cohort_id
                    , change_times.change_time
                    , row_number() OVER (PARTITION BY change_times.user_id, change_times.cohort_id
                                ORDER BY change_times.change_time ASC NULLS LAST) AS version_index
                from change_times
            )
            -- determine the status at each change_time
            , versions as (
                select
                    change_times.user_id
                    , change_times.cohort_id
                    , change_times.change_time as from_time
                    , coalesce(next_change_times.change_time, '2099-01-01') as until_time
                    , application_changes.cohort_application_status
                    , application_changes.graduation_status
                    , coalesce(foundations_complete_changes.foundations_complete, false) as foundations_complete
                    , coalesce(showed_up_changes.showed_up, false) as showed_up
                    , coalesce(past_enrollment_deadline_changes.past_enrollment_deadline, false) as past_enrollment_deadline
                    , coalesce(application_changes.enrolled_late or past_enrollment_deadline_changes.enrolled_at_deadline, false) as enrolled
                    , row_number() OVER (PARTITION BY change_times.user_id
                        ORDER BY change_times.change_time ASC NULLS LAST) AS version_index
                    , cohort_applications_plus.accepted_at
                    , application_changes.enrolled_late
                from
                    change_times_2 change_times
                        join cohort_applications_plus
                            on cohort_applications_plus.user_id = change_times.user_id
                            and cohort_applications_plus.cohort_id = change_times.cohort_id
                        left join application_changes
                            on application_changes.from_time <= change_times.change_time
                                and application_changes.until_time > change_times.change_time
                                and application_changes.user_id = change_times.user_id
                                and application_changes.cohort_id = change_times.cohort_id
                        left join foundations_complete_changes
                            on foundations_complete_changes.from_time <= change_times.change_time
                                and foundations_complete_changes.until_time > change_times.change_time
                                and foundations_complete_changes.user_id = change_times.user_id
                                and foundations_complete_changes.cohort_id = change_times.cohort_id
                        left join showed_up_changes
                            on showed_up_changes.from_time <= change_time
                                and showed_up_changes.until_time > change_time
                                and showed_up_changes.user_id = change_times.user_id
                                and showed_up_changes.cohort_id = change_times.cohort_id
                        left join past_enrollment_deadline_changes
                            on past_enrollment_deadline_changes.from_time <= change_time
                                and past_enrollment_deadline_changes.until_time > change_time
                                and past_enrollment_deadline_changes.user_id = change_times.user_id
                                and past_enrollment_deadline_changes.cohort_id = change_times.cohort_id
                        left join change_times_2 next_change_times
                            on next_change_times.user_id = change_times.user_id
                                and next_change_times.cohort_id = change_times.cohort_id
                                and next_change_times.version_index = change_times.version_index + 1
                where cohort_application_status is not null

            )
            , versions_2 as (
                select
                    user_id
                    , cohort_id
                    , from_time
                    , until_time
                    , case
                        when cohort_application_status in ('rejected', 'deferred') then cohort_application_status
                        when cohort_application_status = 'pending' then 'pending_acceptance'
                        when enrolled = true and cohort_application_status = 'expelled' then 'expelled'
                        when enrolled = false and cohort_application_status = 'expelled' then 'did_not_enroll'
                        when graduation_status in ('graduated', 'honors') then 'graduated'
                        when graduation_status = 'failed' then 'failed'
                        when enrolled = true then 'enrolled'
                        else 'pending_enrollment' end
                        as status
                    , versions.cohort_application_status
                    , versions.graduation_status
                    , versions.foundations_complete
                    , versions.showed_up
                    , versions.past_enrollment_deadline
                    , versions.accepted_at
                    , versions.enrolled
                    , versions.enrolled_late
                from versions
            )
            select * from versions_2
                order by cohort_id, user_id, from_time, until_time
            with no data
        ~

        add_index :cohort_status_changes, [:user_id, :cohort_id, :from_time, :until_time], :name => :user_cohort_time_on_coh_status_changes, :unique => true
    end

    def version_20170720133418
        execute %Q~
            create materialized view cohort_status_changes as
            with indexed_application_versions as (
                select
                    -- in order to be careful of the case where the cohort_id has changed on an application over time,
                    -- we grab the cohort_id from the current record in cohort_applications
                    cohort_applications.user_id
                    , cohort_applications.cohort_id
                    , cohort_applications_versions.status
                    , cohort_applications_versions.graduation_status
                    , cohort_applications_versions.version_created_at
                    , row_number() OVER (PARTITION BY cohort_applications_versions.user_id, cohort_applications_versions.id
                                ORDER BY cohort_applications_versions.version_created_at ASC NULLS LAST) AS version_index
                from cohort_applications
                    join cohort_applications_versions on cohort_applications_versions.id = cohort_applications.id
                order by
                 cohort_applications_versions.user_id
                 , cohort_applications_versions.version_created_at
            )
            -- Join each version with the version that came after it, and record the change
            , application_changes as (
                select
                    old_versions.user_id
                    , old_versions.cohort_id
                    , old_versions.version_created_at as from_time
                    , coalesce(new_versions.version_created_at, '2099-01-01') as until_time
                    , old_versions.status as cohort_application_status
                    , coalesce(old_versions.graduation_status, 'pending') as graduation_status
                    , count(previous_late_enrollments.user_id) > 0 as enrolled_late
                from
                    indexed_application_versions old_versions
                        join published_cohort_content_details cohort_content_details
                            on cohort_content_details.cohort_id = old_versions.cohort_id
                        left join indexed_application_versions new_versions
                            on old_versions.user_id = new_versions.user_id
                                and old_versions.cohort_id = new_versions.cohort_id
                                and old_versions.version_index = new_versions.version_index - 1

                        -- this is a special fix for people who were expelled at the enrollment
                        -- deadline but later marked as accepted again.  We need them to show
                        -- up as enrolled.  So we mark them as enrolled_late if they have any
                        -- versions that are marked as accepted after the deadline
                        left join indexed_application_versions previous_late_enrollments
                            on old_versions.user_id = previous_late_enrollments.user_id
                                and old_versions.cohort_id = previous_late_enrollments.cohort_id
                                and old_versions.version_index >= previous_late_enrollments.version_index
                                and previous_late_enrollments.status = 'accepted'
                                and previous_late_enrollments.version_created_at >= enrollment_deadline
                group by
                    old_versions.user_id
                    , old_versions.cohort_id
                    , old_versions.version_created_at
                    , coalesce(new_versions.version_created_at, '2099-01-01')
                    , old_versions.status
                    , coalesce(old_versions.graduation_status, 'pending')

            )
            -- Find places where past_enrollment_deadline changed
            , past_enrollment_deadline_changes as (
                select
                    cohort_applications.user_id
                    , cohort_applications.cohort_id
                    , cohort_content_details.enrollment_deadline as from_time
                    , '2099-01-01'::timestamp as until_time
                    , true as past_enrollment_deadline
                    , application_changes.cohort_application_status = 'accepted' as enrolled_at_deadline
                    , cohorts.name
                    , cohorts.start_date
                from cohort_applications
                    join published_cohort_content_details cohort_content_details
                        on cohort_content_details.cohort_id = cohort_applications.cohort_id
                    join published_cohorts cohorts on cohorts.id = cohort_applications.cohort_id
                    join application_changes on
                        application_changes.user_id = cohort_applications.user_id
                        and application_changes.cohort_id = cohort_applications.cohort_id
                        and application_changes.from_time <= cohort_content_details.enrollment_deadline
                        and application_changes.until_time > cohort_content_details.enrollment_deadline
                        and cohort_content_details.enrollment_deadline < now()
            )
            -- Find places where foundations_complete changed
            , foundations_complete_changes as (
                select
                    cohort_user_lesson_progress_records.user_id
                    , cohort_user_lesson_progress_records.cohort_id
                    , true as foundations_complete
                    , max(cohort_user_lesson_progress_records.completed_at) as from_time
                    , '2099-01-01'::timestamp as until_time
                from
                    cohort_user_lesson_progress_records
                    join published_cohort_content_details cohort_content_details
                        on cohort_user_lesson_progress_records.cohort_id = cohort_content_details.cohort_id
                where
                    true
                    and foundations = true
                    and completed_at is not null
                group by
                    cohort_user_lesson_progress_records.user_id
                    , cohort_user_lesson_progress_records.cohort_id
                    , foundations_lesson_count
                having
                    count(*) >= foundations_lesson_count
            )
            -- Find places where showed_up changed
            , showed_up_changes as (
                select
                    cohort_user_lesson_progress_records.user_id
                    , cohort_user_lesson_progress_records.cohort_id
                    , true as showed_up
                    , min(cohort_user_lesson_progress_records.completed_at) as from_time
                    , '2099-01-01'::timestamp as until_time
                from
                    cohort_user_lesson_progress_records
                    join cohort_applications_plus
                        on cohort_user_lesson_progress_records.cohort_id = cohort_applications_plus.cohort_id
                        and cohort_user_lesson_progress_records.user_id = cohort_applications_plus.user_id
                where
                    completed_at > cohort_applications_plus.accepted_at
                group by
                    cohort_user_lesson_progress_records.user_id
                    , cohort_user_lesson_progress_records.cohort_id
            )
            -- Find the set of unique times in which something changed
            -- for each application
            , change_times as (
                select
                    user_id
                    , cohort_id
                    , from_time as change_time
                from application_changes

                union all

                select
                    user_id
                    , cohort_id
                    , from_time as change_time
                from foundations_complete_changes

                union all

                select
                    user_id
                    , cohort_id
                    , from_time as change_time
                from showed_up_changes

                union all

                select
                    user_id
                    , cohort_id
                    , from_time as change_time
                from past_enrollment_deadline_changes

                order by change_time

            )
            -- Put the change times for each user in order with an index so we can
            -- join each version with the one after it
            , change_times_2 as (
                select
                    change_times.user_id
                    , change_times.cohort_id
                    , change_times.change_time
                    , row_number() OVER (PARTITION BY change_times.user_id, change_times.cohort_id
                                ORDER BY change_times.change_time ASC NULLS LAST) AS version_index
                from change_times
            )
            -- determine the status at each change_time
            , versions as (
                select
                    change_times.user_id
                    , change_times.cohort_id
                    , change_times.change_time as from_time
                    , coalesce(next_change_times.change_time, '2099-01-01') as until_time
                    , application_changes.cohort_application_status
                    , application_changes.graduation_status
                    , coalesce(foundations_complete_changes.foundations_complete, false) as foundations_complete
                    , coalesce(showed_up_changes.showed_up, false) as showed_up
                    , coalesce(past_enrollment_deadline_changes.past_enrollment_deadline, false) as past_enrollment_deadline
                    , coalesce(application_changes.enrolled_late or past_enrollment_deadline_changes.enrolled_at_deadline, false) as enrolled
                    , row_number() OVER (PARTITION BY change_times.user_id
                        ORDER BY change_times.change_time ASC NULLS LAST) AS version_index
                    , cohort_applications_plus.accepted_at
                    , application_changes.enrolled_late
                from
                    change_times_2 change_times
                        join cohort_applications_plus
                            on cohort_applications_plus.user_id = change_times.user_id
                            and cohort_applications_plus.cohort_id = change_times.cohort_id
                        left join application_changes
                            on application_changes.from_time <= change_times.change_time
                                and application_changes.until_time > change_times.change_time
                                and application_changes.user_id = change_times.user_id
                                and application_changes.cohort_id = change_times.cohort_id
                        left join foundations_complete_changes
                            on foundations_complete_changes.from_time <= change_times.change_time
                                and foundations_complete_changes.until_time > change_times.change_time
                                and foundations_complete_changes.user_id = change_times.user_id
                                and foundations_complete_changes.cohort_id = change_times.cohort_id
                        left join showed_up_changes
                            on showed_up_changes.from_time <= change_time
                                and showed_up_changes.until_time > change_time
                                and showed_up_changes.user_id = change_times.user_id
                                and showed_up_changes.cohort_id = change_times.cohort_id
                        left join past_enrollment_deadline_changes
                            on past_enrollment_deadline_changes.from_time <= change_time
                                and past_enrollment_deadline_changes.until_time > change_time
                                and past_enrollment_deadline_changes.user_id = change_times.user_id
                                and past_enrollment_deadline_changes.cohort_id = change_times.cohort_id
                        left join change_times_2 next_change_times
                            on next_change_times.user_id = change_times.user_id
                                and next_change_times.cohort_id = change_times.cohort_id
                                and next_change_times.version_index = change_times.version_index + 1
                where cohort_application_status is not null

            )
            , versions_2 as (
                select
                    user_id
                    , cohort_id
                    , from_time
                    , until_time
                    , case
                        when cohort_application_status in ('rejected', 'deferred') then cohort_application_status
                        when cohort_application_status = 'pending' then 'pending_acceptance'
                        when enrolled = true and cohort_application_status = 'expelled' then 'expelled'
                        when enrolled = false and cohort_application_status = 'expelled' then 'did_not_enroll'
                        when graduation_status in ('graduated', 'honors') then 'graduated'
                        when graduation_status = 'failed' then 'failed'
                        when enrolled = true then 'enrolled'
                        else 'pending_enrollment' end
                        as status
                    , versions.cohort_application_status
                    , versions.graduation_status
                    , versions.foundations_complete
                    , versions.showed_up
                    , versions.past_enrollment_deadline
                    , versions.accepted_at
                    , versions.enrolled
                    , versions.enrolled_late
                from versions
            )
            select * from versions_2
                order by cohort_id, user_id, from_time, until_time
            with no data
        ~

        add_index :cohort_status_changes, [:user_id, :cohort_id, :from_time, :until_time], :name => :user_cohort_time_on_coh_status_changes, :unique => true
    end

    # FIX an off-by-one error in the late_enrollment logic
    def version_20170531164523
        execute %Q~
            create materialized view cohort_status_changes as
            with indexed_application_versions as (
                select
                    -- in order to be careful of the case where the cohort_id has changed on an application over time,
                    -- we grab the cohort_id from the current record in cohort_applications
                    cohort_applications.user_id
                    , cohort_applications.cohort_id
                    , cohort_applications_versions.status
                    , cohort_applications_versions.graduation_status
                    , cohort_applications_versions.version_created_at
                    , row_number() OVER (PARTITION BY cohort_applications_versions.user_id, cohort_applications_versions.id
                                ORDER BY cohort_applications_versions.version_created_at ASC NULLS LAST) AS version_index
                from cohort_applications
                    join cohort_applications_versions on cohort_applications_versions.id = cohort_applications.id
                order by
                 cohort_applications_versions.user_id
                 , cohort_applications_versions.version_created_at
            )
            -- Join each version with the version that came after it, and record the change
            , application_changes as (
                select
                    old_versions.user_id
                    , old_versions.cohort_id
                    , old_versions.version_created_at as from_time
                    , coalesce(new_versions.version_created_at, '2099-01-01') as until_time
                    , old_versions.status as cohort_application_status
                    , coalesce(old_versions.graduation_status, 'pending') as graduation_status
                    , count(previous_late_enrollments.user_id) > 0 as enrolled_late
                from
                    indexed_application_versions old_versions
                        join published_cohort_content_details cohort_content_details
                            on cohort_content_details.cohort_id = old_versions.cohort_id
                        left join indexed_application_versions new_versions
                            on old_versions.user_id = new_versions.user_id
                                and old_versions.cohort_id = new_versions.cohort_id
                                and old_versions.version_index = new_versions.version_index - 1

                        -- this is a special fix for people who were expelled at the enrollment
                        -- deadline but later marked as accepted again.  We need them to show
                        -- up as enrolled.  So we mark them as enrolled_late if they have any
                        -- versions that are marked as accepted after the deadline
                        left join indexed_application_versions previous_late_enrollments
                            on old_versions.user_id = previous_late_enrollments.user_id
                                and old_versions.cohort_id = previous_late_enrollments.cohort_id
                                and old_versions.version_index >= previous_late_enrollments.version_index - 1
                                and previous_late_enrollments.status = 'accepted'
                                and previous_late_enrollments.version_created_at >= enrollment_deadline
                group by
                    old_versions.user_id
                    , old_versions.cohort_id
                    , old_versions.version_created_at
                    , coalesce(new_versions.version_created_at, '2099-01-01')
                    , old_versions.status
                    , coalesce(old_versions.graduation_status, 'pending')

            )
            -- Find places where past_enrollment_deadline changed
            , past_enrollment_deadline_changes as (
                select
                    cohort_applications.user_id
                    , cohort_applications.cohort_id
                    , cohort_content_details.enrollment_deadline as from_time
                    , '2099-01-01'::timestamp as until_time
                    , true as past_enrollment_deadline
                    , application_changes.cohort_application_status = 'accepted' as enrolled_at_deadline
                    , cohorts.name
                    , cohorts.start_date
                from cohort_applications
                    join published_cohort_content_details cohort_content_details
                        on cohort_content_details.cohort_id = cohort_applications.cohort_id
                    join published_cohorts cohorts on cohorts.id = cohort_applications.cohort_id
                    join application_changes on
                        application_changes.user_id = cohort_applications.user_id
                        and application_changes.cohort_id = cohort_applications.cohort_id
                        and application_changes.from_time <= cohort_content_details.enrollment_deadline
                        and application_changes.until_time > cohort_content_details.enrollment_deadline
                        and cohort_content_details.enrollment_deadline < now()
            )
            -- Find places where foundations_complete changed
            , foundations_complete_changes as (
                select
                    cohort_user_lesson_progress_records.user_id
                    , cohort_user_lesson_progress_records.cohort_id
                    , true as foundations_complete
                    , max(cohort_user_lesson_progress_records.completed_at) as from_time
                    , '2099-01-01'::timestamp as until_time
                from
                    cohort_user_lesson_progress_records
                    join published_cohort_content_details cohort_content_details
                        on cohort_user_lesson_progress_records.cohort_id = cohort_content_details.cohort_id
                where
                    true
                    and foundations = true
                    and completed_at is not null
                group by
                    cohort_user_lesson_progress_records.user_id
                    , cohort_user_lesson_progress_records.cohort_id
                    , foundations_lesson_count
                having
                    count(*) >= foundations_lesson_count
            )
            -- Find places where showed_up changed
            , showed_up_changes as (
                select
                    cohort_user_lesson_progress_records.user_id
                    , cohort_user_lesson_progress_records.cohort_id
                    , true as showed_up
                    , min(cohort_user_lesson_progress_records.completed_at) as from_time
                    , '2099-01-01'::timestamp as until_time
                from
                    cohort_user_lesson_progress_records
                    join cohort_applications_plus
                        on cohort_user_lesson_progress_records.cohort_id = cohort_applications_plus.cohort_id
                        and cohort_user_lesson_progress_records.user_id = cohort_applications_plus.user_id
                where
                    completed_at > cohort_applications_plus.accepted_at
                group by
                    cohort_user_lesson_progress_records.user_id
                    , cohort_user_lesson_progress_records.cohort_id
            )
            -- Find the set of unique times in which something changed
            -- for each application
            , change_times as (
                select
                    user_id
                    , cohort_id
                    , from_time as change_time
                from application_changes

                union all

                select
                    user_id
                    , cohort_id
                    , from_time as change_time
                from foundations_complete_changes

                union all

                select
                    user_id
                    , cohort_id
                    , from_time as change_time
                from showed_up_changes

                union all

                select
                    user_id
                    , cohort_id
                    , from_time as change_time
                from past_enrollment_deadline_changes

                order by change_time

            )
            -- Put the change times for each user in order with an index so we can
            -- join each version with the one after it
            , change_times_2 as (
                select
                    change_times.user_id
                    , change_times.cohort_id
                    , change_times.change_time
                    , row_number() OVER (PARTITION BY change_times.user_id, change_times.cohort_id
                                ORDER BY change_times.change_time ASC NULLS LAST) AS version_index
                from change_times
            )
            -- determine the status at each change_time
            , versions as (
                select
                    change_times.user_id
                    , change_times.cohort_id
                    , change_times.change_time as from_time
                    , coalesce(next_change_times.change_time, '2099-01-01') as until_time
                    , application_changes.cohort_application_status
                    , application_changes.graduation_status
                    , coalesce(foundations_complete_changes.foundations_complete, false) as foundations_complete
                    , coalesce(showed_up_changes.showed_up, false) as showed_up
                    , coalesce(past_enrollment_deadline_changes.past_enrollment_deadline, false) as past_enrollment_deadline
                    , coalesce(application_changes.enrolled_late or past_enrollment_deadline_changes.enrolled_at_deadline, false) as enrolled
                    , row_number() OVER (PARTITION BY change_times.user_id
                        ORDER BY change_times.change_time ASC NULLS LAST) AS version_index
                    , cohort_applications_plus.accepted_at
                    , application_changes.enrolled_late
                from
                    change_times_2 change_times
                        join cohort_applications_plus
                            on cohort_applications_plus.user_id = change_times.user_id
                            and cohort_applications_plus.cohort_id = change_times.cohort_id
                        left join application_changes
                            on application_changes.from_time <= change_times.change_time
                                and application_changes.until_time > change_times.change_time
                                and application_changes.user_id = change_times.user_id
                                and application_changes.cohort_id = change_times.cohort_id
                        left join foundations_complete_changes
                            on foundations_complete_changes.from_time <= change_times.change_time
                                and foundations_complete_changes.until_time > change_times.change_time
                                and foundations_complete_changes.user_id = change_times.user_id
                                and foundations_complete_changes.cohort_id = change_times.cohort_id
                        left join showed_up_changes
                            on showed_up_changes.from_time <= change_time
                                and showed_up_changes.until_time > change_time
                                and showed_up_changes.user_id = change_times.user_id
                                and showed_up_changes.cohort_id = change_times.cohort_id
                        left join past_enrollment_deadline_changes
                            on past_enrollment_deadline_changes.from_time <= change_time
                                and past_enrollment_deadline_changes.until_time > change_time
                                and past_enrollment_deadline_changes.user_id = change_times.user_id
                                and past_enrollment_deadline_changes.cohort_id = change_times.cohort_id
                        left join change_times_2 next_change_times
                            on next_change_times.user_id = change_times.user_id
                                and next_change_times.cohort_id = change_times.cohort_id
                                and next_change_times.version_index = change_times.version_index + 1
                where cohort_application_status is not null

            )
            , versions_2 as (
                select
                    user_id
                    , cohort_id
                    , from_time
                    , until_time
                    , case
                        when cohort_application_status in ('rejected', 'deferred') then cohort_application_status
                        when cohort_application_status = 'pending' then 'pending_acceptance'
                        when enrolled = true and cohort_application_status = 'expelled' then 'expelled'
                        when enrolled = false and cohort_application_status = 'expelled' then 'did_not_enroll'
                        when graduation_status in ('graduated', 'honors') then 'graduated'
                        when graduation_status = 'failed' then 'failed'
                        when enrolled = true then 'enrolled'
                        else 'pending_enrollment' end
                        as status
                    , versions.cohort_application_status
                    , versions.graduation_status
                    , versions.foundations_complete
                    , versions.showed_up
                    , versions.past_enrollment_deadline
                    , versions.accepted_at
                    , versions.enrolled
                    , versions.enrolled_late
                from versions
            )
            select * from versions_2
                order by cohort_id, user_id, from_time, until_time
            with no data
        ~

        add_index :cohort_status_changes, [:user_id, :cohort_id, :from_time, :until_time], :name => :user_cohort_time_on_coh_status_changes, :unique => true
    end

    # do not create records for changes in enrolled that happen in the future
    # (check cohort_content_details.enrollment_deadline < now())
    def version_20170323141852
        execute %Q~
            create materialized view cohort_status_changes as
            with indexed_application_versions as (
                select
                    -- in order to be careful of the case where the cohort_id has changed on an application over time,
                    -- we grab the cohort_id from the current record in cohort_applications
                    cohort_applications.user_id
                    , cohort_applications.cohort_id
                    , cohort_applications_versions.status
                    , cohort_applications_versions.graduation_status
                    , cohort_applications_versions.version_created_at
                    , row_number() OVER (PARTITION BY cohort_applications_versions.user_id, cohort_applications_versions.id
                                ORDER BY cohort_applications_versions.version_created_at ASC NULLS LAST) AS version_index
                from cohort_applications
                    join cohort_applications_versions on cohort_applications_versions.id = cohort_applications.id
                order by
                 cohort_applications_versions.user_id
                 , cohort_applications_versions.version_created_at
            )
            -- Join each version with the version that came after it, and record the change
            , application_changes as (
                select
                    old_versions.user_id
                    , old_versions.cohort_id
                    , old_versions.version_created_at as from_time
                    , coalesce(new_versions.version_created_at, '2099-01-01') as until_time
                    , old_versions.status as cohort_application_status
                    , coalesce(old_versions.graduation_status, 'pending') as graduation_status
                from
                    indexed_application_versions old_versions
                        left join indexed_application_versions new_versions
                            on old_versions.user_id = new_versions.user_id
                                and old_versions.cohort_id = new_versions.cohort_id
                                and old_versions.version_index = new_versions.version_index - 1

            )
            -- Find places where past_enrollment_deadline changed
            , past_enrollment_deadline_changes as (
                select
                    cohort_applications.user_id
                    , cohort_applications.cohort_id
                    , cohort_content_details.enrollment_deadline as from_time
                    , '2099-01-01'::timestamp as until_time
                    , application_changes.cohort_application_status = 'accepted' as enrolled
                    , cohorts.name
                    , cohorts.start_date
                from cohort_applications
                    join published_cohort_content_details cohort_content_details
                        on cohort_content_details.cohort_id = cohort_applications.cohort_id
                    join published_cohorts cohorts on cohorts.id = cohort_applications.cohort_id
                    join application_changes on
                        application_changes.user_id = cohort_applications.user_id
                        and application_changes.cohort_id = cohort_applications.cohort_id
                        and application_changes.from_time <= cohort_content_details.enrollment_deadline
                        and application_changes.until_time > cohort_content_details.enrollment_deadline
                        and cohort_content_details.enrollment_deadline < now()
            )
            -- Find places where foundations_complete changed
            , foundations_complete_changes as (
                select
                    cohort_user_lesson_progress_records.user_id
                    , cohort_user_lesson_progress_records.cohort_id
                    , true as foundations_complete
                    , max(cohort_user_lesson_progress_records.completed_at) as from_time
                    , '2099-01-01'::timestamp as until_time
                from
                    cohort_user_lesson_progress_records
                    join published_cohort_content_details cohort_content_details
                        on cohort_user_lesson_progress_records.cohort_id = cohort_content_details.cohort_id
                where
                    true
                    and foundations = true
                    and completed_at is not null
                group by
                    cohort_user_lesson_progress_records.user_id
                    , cohort_user_lesson_progress_records.cohort_id
                    , foundations_lesson_count
                having
                    count(*) >= foundations_lesson_count
            )
            -- Find places where showed_up changed
            , showed_up_changes as (
                select
                    cohort_user_lesson_progress_records.user_id
                    , cohort_user_lesson_progress_records.cohort_id
                    , true as showed_up
                    , min(cohort_user_lesson_progress_records.completed_at) as from_time
                    , '2099-01-01'::timestamp as until_time
                from
                    cohort_user_lesson_progress_records
                    join cohort_applications_plus
                        on cohort_user_lesson_progress_records.cohort_id = cohort_applications_plus.cohort_id
                        and cohort_user_lesson_progress_records.user_id = cohort_applications_plus.user_id
                where
                    completed_at > cohort_applications_plus.accepted_at
                group by
                    cohort_user_lesson_progress_records.user_id
                    , cohort_user_lesson_progress_records.cohort_id
            )
            -- Find the set of unique times in which something changed
            -- for each application
            , change_times as (
                select
                    user_id
                    , cohort_id
                    , from_time as change_time
                from application_changes

                union all

                select
                    user_id
                    , cohort_id
                    , from_time as change_time
                from foundations_complete_changes

                union all

                select
                    user_id
                    , cohort_id
                    , from_time as change_time
                from showed_up_changes

                union all

                select
                    user_id
                    , cohort_id
                    , from_time as change_time
                from past_enrollment_deadline_changes

                order by change_time
            )
            -- Put the change times for each user in order with an index so we can
            -- join each version with the one after it
            , change_times_2 as (
                select
                    change_times.user_id
                    , change_times.cohort_id
                    , change_times.change_time
                    , row_number() OVER (PARTITION BY change_times.user_id, change_times.cohort_id
                                ORDER BY change_times.change_time ASC NULLS LAST) AS version_index
                from change_times
            )
            -- determine the status at each change_time
            , versions as (
                select
                    change_times.user_id
                    , change_times.cohort_id
                    , change_times.change_time as from_time
                    , coalesce(next_change_times.change_time, '2099-01-01') as until_time
                    , application_changes.cohort_application_status
                    , application_changes.graduation_status
                    , coalesce(foundations_complete_changes.foundations_complete, false) as foundations_complete
                    , coalesce(showed_up_changes.showed_up, false) as showed_up
                    , coalesce(past_enrollment_deadline_changes.enrolled, false) as enrolled
                    , row_number() OVER (PARTITION BY change_times.user_id
                        ORDER BY change_times.change_time ASC NULLS LAST) AS version_index
                from
                    change_times_2 change_times
                        left join application_changes
                            on application_changes.from_time <= change_times.change_time
                                and application_changes.until_time > change_times.change_time
                                and application_changes.user_id = change_times.user_id
                                and application_changes.cohort_id = change_times.cohort_id
                        left join foundations_complete_changes
                            on foundations_complete_changes.from_time <= change_times.change_time
                                and foundations_complete_changes.until_time > change_times.change_time
                                and foundations_complete_changes.user_id = change_times.user_id
                                and foundations_complete_changes.cohort_id = change_times.cohort_id
                        left join showed_up_changes
                            on showed_up_changes.from_time <= change_time
                                and showed_up_changes.until_time > change_time
                                and showed_up_changes.user_id = change_times.user_id
                                and showed_up_changes.cohort_id = change_times.cohort_id
                        left join past_enrollment_deadline_changes
                            on past_enrollment_deadline_changes.from_time <= change_time
                                and past_enrollment_deadline_changes.until_time > change_time
                                and past_enrollment_deadline_changes.user_id = change_times.user_id
                                and past_enrollment_deadline_changes.cohort_id = change_times.cohort_id
                        left join change_times_2 next_change_times
                            on next_change_times.user_id = change_times.user_id
                                and next_change_times.cohort_id = change_times.cohort_id
                                and next_change_times.version_index = change_times.version_index + 1
                where cohort_application_status is not null

            )
            , versions_2 as (
                select
                    user_id
                    , cohort_id
                    , from_time
                    , until_time
                    , case
                        when cohort_application_status in ('rejected', 'deferred') then cohort_application_status
                        when cohort_application_status = 'pending' then 'pending_acceptance'
                        when enrolled = true and cohort_application_status = 'expelled' then 'expelled'
                        when enrolled = false and cohort_application_status = 'expelled' then 'did_not_enroll'
                        when graduation_status in ('graduated', 'honors') then 'graduated'
                        when graduation_status = 'failed' then 'failed'
                        when enrolled = true then 'enrolled'
                        else 'pending_enrollment' end
                        as status
                    , versions.cohort_application_status
                    , versions.graduation_status
                    , versions.foundations_complete
                    , versions.showed_up
                    , versions.enrolled
                from versions
            )
            select * from versions_2
            with no data
        ~

        add_index :cohort_status_changes, [:user_id, :cohort_id, :from_time, :until_time], :name => :user_cohort_time_on_coh_status_changes, :unique => true

    end

    # user published_cohort_content_details instead of cohort_content_details
    #
    # remove in_curriculum check for showed_up
    def version_20170310204902
        execute %Q~
            create materialized view cohort_status_changes as
            with indexed_application_versions as (
                select
                    -- in order to be careful of the case where the cohort_id has changed on an application over time,
                    -- we grab the cohort_id from the current record in cohort_applications
                    cohort_applications.user_id
                    , cohort_applications.cohort_id
                    , cohort_applications_versions.status
                    , cohort_applications_versions.graduation_status
                    , cohort_applications_versions.version_created_at
                    , row_number() OVER (PARTITION BY cohort_applications_versions.user_id, cohort_applications_versions.id
                                ORDER BY cohort_applications_versions.version_created_at ASC NULLS LAST) AS version_index
                from cohort_applications
                    join cohort_applications_versions on cohort_applications_versions.id = cohort_applications.id
                order by
                 cohort_applications_versions.user_id
                 , cohort_applications_versions.version_created_at
            )
            -- Join each version with the version that came after it, and record the change
            , application_changes as (
                select
                    old_versions.user_id
                    , old_versions.cohort_id
                    , old_versions.version_created_at as from_time
                    , coalesce(new_versions.version_created_at, '2099-01-01') as until_time
                    , old_versions.status as cohort_application_status
                    , coalesce(old_versions.graduation_status, 'pending') as graduation_status
                from
                    indexed_application_versions old_versions
                        left join indexed_application_versions new_versions
                            on old_versions.user_id = new_versions.user_id
                                and old_versions.cohort_id = new_versions.cohort_id
                                and old_versions.version_index = new_versions.version_index - 1

            )
            -- Find places where past_enrollment_deadline changed
            , past_enrollment_deadline_changes as (
                select
                    cohort_applications.user_id
                    , cohort_applications.cohort_id
                    , cohort_content_details.enrollment_deadline as from_time
                    , '2099-01-01'::timestamp as until_time
                    , application_changes.cohort_application_status = 'accepted' as enrolled
                    , cohorts.name
                    , cohorts.start_date
                from cohort_applications
                    join published_cohort_content_details cohort_content_details
                        on cohort_content_details.cohort_id = cohort_applications.cohort_id
                    join published_cohorts cohorts on cohorts.id = cohort_applications.cohort_id
                    join application_changes on
                        application_changes.user_id = cohort_applications.user_id
                        and application_changes.cohort_id = cohort_applications.cohort_id
                        and application_changes.from_time <= cohort_content_details.enrollment_deadline
                        and application_changes.until_time > cohort_content_details.enrollment_deadline
            )
            -- Find places where foundations_complete changed
            , foundations_complete_changes as (
                select
                    cohort_user_lesson_progress_records.user_id
                    , cohort_user_lesson_progress_records.cohort_id
                    , true as foundations_complete
                    , max(cohort_user_lesson_progress_records.completed_at) as from_time
                    , '2099-01-01'::timestamp as until_time
                from
                    cohort_user_lesson_progress_records
                    join published_cohort_content_details cohort_content_details
                        on cohort_user_lesson_progress_records.cohort_id = cohort_content_details.cohort_id
                where
                    true
                    and foundations = true
                    and completed_at is not null
                group by
                    cohort_user_lesson_progress_records.user_id
                    , cohort_user_lesson_progress_records.cohort_id
                    , foundations_lesson_count
                having
                    count(*) >= foundations_lesson_count
            )
            -- Find places where showed_up changed
            , showed_up_changes as (
                select
                    cohort_user_lesson_progress_records.user_id
                    , cohort_user_lesson_progress_records.cohort_id
                    , true as showed_up
                    , min(cohort_user_lesson_progress_records.completed_at) as from_time
                    , '2099-01-01'::timestamp as until_time
                from
                    cohort_user_lesson_progress_records
                    join cohort_applications_plus
                        on cohort_user_lesson_progress_records.cohort_id = cohort_applications_plus.cohort_id
                        and cohort_user_lesson_progress_records.user_id = cohort_applications_plus.user_id
                where
                    completed_at > cohort_applications_plus.accepted_at
                group by
                    cohort_user_lesson_progress_records.user_id
                    , cohort_user_lesson_progress_records.cohort_id
            )
            -- Find the set of unique times in which something changed
            -- for each application
            , change_times as (
                select
                    user_id
                    , cohort_id
                    , from_time as change_time
                from application_changes

                union all

                select
                    user_id
                    , cohort_id
                    , from_time as change_time
                from foundations_complete_changes

                union all

                select
                    user_id
                    , cohort_id
                    , from_time as change_time
                from showed_up_changes

                union all

                select
                    user_id
                    , cohort_id
                    , from_time as change_time
                from past_enrollment_deadline_changes

                order by change_time
            )
            -- Put the change times for each user in order with an index so we can
            -- join each version with the one after it
            , change_times_2 as (
                select
                    change_times.user_id
                    , change_times.cohort_id
                    , change_times.change_time
                    , row_number() OVER (PARTITION BY change_times.user_id, change_times.cohort_id
                                ORDER BY change_times.change_time ASC NULLS LAST) AS version_index
                from change_times
            )
            -- determine the status at each change_time
            , versions as (
                select
                    change_times.user_id
                    , change_times.cohort_id
                    , change_times.change_time as from_time
                    , coalesce(next_change_times.change_time, '2099-01-01') as until_time
                    , application_changes.cohort_application_status
                    , application_changes.graduation_status
                    , coalesce(foundations_complete_changes.foundations_complete, false) as foundations_complete
                    , coalesce(showed_up_changes.showed_up, false) as showed_up
                    , coalesce(past_enrollment_deadline_changes.enrolled, false) as enrolled
                    , row_number() OVER (PARTITION BY change_times.user_id
                        ORDER BY change_times.change_time ASC NULLS LAST) AS version_index
                from
                    change_times_2 change_times
                        left join application_changes
                            on application_changes.from_time <= change_times.change_time
                                and application_changes.until_time > change_times.change_time
                                and application_changes.user_id = change_times.user_id
                                and application_changes.cohort_id = change_times.cohort_id
                        left join foundations_complete_changes
                            on foundations_complete_changes.from_time <= change_times.change_time
                                and foundations_complete_changes.until_time > change_times.change_time
                                and foundations_complete_changes.user_id = change_times.user_id
                                and foundations_complete_changes.cohort_id = change_times.cohort_id
                        left join showed_up_changes
                            on showed_up_changes.from_time <= change_time
                                and showed_up_changes.until_time > change_time
                                and showed_up_changes.user_id = change_times.user_id
                                and showed_up_changes.cohort_id = change_times.cohort_id
                        left join past_enrollment_deadline_changes
                            on past_enrollment_deadline_changes.from_time <= change_time
                                and past_enrollment_deadline_changes.until_time > change_time
                                and past_enrollment_deadline_changes.user_id = change_times.user_id
                                and past_enrollment_deadline_changes.cohort_id = change_times.cohort_id
                        left join change_times_2 next_change_times
                            on next_change_times.user_id = change_times.user_id
                                and next_change_times.cohort_id = change_times.cohort_id
                                and next_change_times.version_index = change_times.version_index + 1
                where cohort_application_status is not null

            )
            , versions_2 as (
                select
                    user_id
                    , cohort_id
                    , from_time
                    , until_time
                    , case
                        when cohort_application_status in ('rejected', 'deferred') then cohort_application_status
                        when cohort_application_status = 'pending' then 'pending_acceptance'
                        when enrolled = true and cohort_application_status = 'expelled' then 'expelled'
                        when enrolled = false and cohort_application_status = 'expelled' then 'did_not_enroll'
                        when graduation_status in ('graduated', 'honors') then 'graduated'
                        when graduation_status = 'failed' then 'failed'
                        when enrolled = true then 'enrolled'
                        else 'pending_enrollment' end
                        as status
                    , versions.cohort_application_status
                    , versions.graduation_status
                    , versions.foundations_complete
                    , versions.showed_up
                    , versions.enrolled
                from versions
            )
            select * from versions_2
            with no data
        ~

        add_index :cohort_status_changes, [:user_id, :cohort_id, :from_time, :until_time], :name => :user_cohort_time_on_coh_status_changes, :unique => true

    end

    def version_20170306001915
        execute %Q~
            create materialized view cohort_status_changes as
            with indexed_application_versions as (
                select
                    -- in order to be careful of the case where the cohort_id has changed on an application over time,
                    -- we grab the cohort_id from the current record in cohort_applications
                    cohort_applications.user_id
                    , cohort_applications.cohort_id
                    , cohort_applications_versions.status
                    , cohort_applications_versions.graduation_status
                    , cohort_applications_versions.version_created_at
                    , row_number() OVER (PARTITION BY cohort_applications_versions.user_id, cohort_applications_versions.id
                                ORDER BY cohort_applications_versions.version_created_at ASC NULLS LAST) AS version_index
                from cohort_applications
                    join cohort_applications_versions on cohort_applications_versions.id = cohort_applications.id
                order by
                 cohort_applications_versions.user_id
                 , cohort_applications_versions.version_created_at
            )
            -- Join each version with the version that came after it, and record the change
            , application_changes as (
                select
                    old_versions.user_id
                    , old_versions.cohort_id
                    , old_versions.version_created_at as from_time
                    , coalesce(new_versions.version_created_at, '2099-01-01') as until_time
                    , old_versions.status as cohort_application_status
                    , coalesce(old_versions.graduation_status, 'pending') as graduation_status
                from
                    indexed_application_versions old_versions
                        left join indexed_application_versions new_versions
                            on old_versions.user_id = new_versions.user_id
                                and old_versions.cohort_id = new_versions.cohort_id
                                and old_versions.version_index = new_versions.version_index - 1

            )
            -- Find places where past_enrollment_deadline changed
            , past_enrollment_deadline_changes as (
                select
                    cohort_applications.user_id
                    , cohort_applications.cohort_id
                    , cohort_content_details.enrollment_deadline as from_time
                    , '2099-01-01'::timestamp as until_time
                    , application_changes.cohort_application_status = 'accepted' as enrolled
                    , cohorts.name
                    , cohorts.start_date
                from cohort_applications
                    join cohort_content_details on cohort_content_details.cohort_id = cohort_applications.cohort_id
                    join published_cohorts cohorts on cohorts.id = cohort_applications.cohort_id
                    join application_changes on
                        application_changes.user_id = cohort_applications.user_id
                        and application_changes.cohort_id = cohort_applications.cohort_id
                        and application_changes.from_time <= cohort_content_details.enrollment_deadline
                        and application_changes.until_time > cohort_content_details.enrollment_deadline
            )
            -- Find places where foundations_complete changed
            , foundations_complete_changes as (
                select
                    cohort_user_lesson_progress_records.user_id
                    , cohort_user_lesson_progress_records.cohort_id
                    , true as foundations_complete
                    , max(cohort_user_lesson_progress_records.completed_at) as from_time
                    , '2099-01-01'::timestamp as until_time
                from
                    cohort_user_lesson_progress_records
                    join cohort_content_details
                        on cohort_user_lesson_progress_records.cohort_id = cohort_content_details.cohort_id
                where
                    true
                    and foundations = true
                    and completed_at is not null
                group by
                    cohort_user_lesson_progress_records.user_id
                    , cohort_user_lesson_progress_records.cohort_id
                    , foundations_lesson_count
                having
                    count(*) >= foundations_lesson_count
            )
            -- Find places where showed_up changed
            , showed_up_changes as (
                select
                    cohort_user_lesson_progress_records.user_id
                    , cohort_user_lesson_progress_records.cohort_id
                    , true as showed_up
                    , min(cohort_user_lesson_progress_records.completed_at) as from_time
                    , '2099-01-01'::timestamp as until_time
                from
                    cohort_user_lesson_progress_records
                    join cohort_applications_plus
                        on cohort_user_lesson_progress_records.cohort_id = cohort_applications_plus.cohort_id
                        and cohort_user_lesson_progress_records.user_id = cohort_applications_plus.user_id
                where
                    true
                    and in_curriculum = true
                    and completed_at > cohort_applications_plus.accepted_at
                group by
                    cohort_user_lesson_progress_records.user_id
                    , cohort_user_lesson_progress_records.cohort_id
            )
            -- Find the set of unique times in which something changed
            -- for each application
            , change_times as (
                select
                    user_id
                    , cohort_id
                    , from_time as change_time
                from application_changes

                union all

                select
                    user_id
                    , cohort_id
                    , from_time as change_time
                from foundations_complete_changes

                union all

                select
                    user_id
                    , cohort_id
                    , from_time as change_time
                from showed_up_changes

                union all

                select
                    user_id
                    , cohort_id
                    , from_time as change_time
                from past_enrollment_deadline_changes

                order by change_time
            )
            -- Put the change times for each user in order with an index so we can
            -- join each version with the one after it
            , change_times_2 as (
                select
                    change_times.user_id
                    , change_times.cohort_id
                    , change_times.change_time
                    , row_number() OVER (PARTITION BY change_times.user_id, change_times.cohort_id
                                ORDER BY change_times.change_time ASC NULLS LAST) AS version_index
                from change_times
            )
            -- determine the status at each change_time
            , versions as (
                select
                    change_times.user_id
                    , change_times.cohort_id
                    , change_times.change_time as from_time
                    , coalesce(next_change_times.change_time, '2099-01-01') as until_time
                    , application_changes.cohort_application_status
                    , application_changes.graduation_status
                    , coalesce(foundations_complete_changes.foundations_complete, false) as foundations_complete
                    , coalesce(showed_up_changes.showed_up, false) as showed_up
                    , coalesce(past_enrollment_deadline_changes.enrolled, false) as enrolled
                    , row_number() OVER (PARTITION BY change_times.user_id
                        ORDER BY change_times.change_time ASC NULLS LAST) AS version_index
                from
                    change_times_2 change_times
                        left join application_changes
                            on application_changes.from_time <= change_times.change_time
                                and application_changes.until_time > change_times.change_time
                                and application_changes.user_id = change_times.user_id
                                and application_changes.cohort_id = change_times.cohort_id
                        left join foundations_complete_changes
                            on foundations_complete_changes.from_time <= change_times.change_time
                                and foundations_complete_changes.until_time > change_times.change_time
                                and foundations_complete_changes.user_id = change_times.user_id
                                and foundations_complete_changes.cohort_id = change_times.cohort_id
                        left join showed_up_changes
                            on showed_up_changes.from_time <= change_time
                                and showed_up_changes.until_time > change_time
                                and showed_up_changes.user_id = change_times.user_id
                                and showed_up_changes.cohort_id = change_times.cohort_id
                        left join past_enrollment_deadline_changes
                            on past_enrollment_deadline_changes.from_time <= change_time
                                and past_enrollment_deadline_changes.until_time > change_time
                                and past_enrollment_deadline_changes.user_id = change_times.user_id
                                and past_enrollment_deadline_changes.cohort_id = change_times.cohort_id
                        left join change_times_2 next_change_times
                            on next_change_times.user_id = change_times.user_id
                                and next_change_times.cohort_id = change_times.cohort_id
                                and next_change_times.version_index = change_times.version_index + 1
                where cohort_application_status is not null

            )
            , versions_2 as (
                select
                    user_id
                    , cohort_id
                    , from_time
                    , until_time
                    , case
                        when cohort_application_status in ('rejected', 'deferred') then cohort_application_status
                        when cohort_application_status = 'pending' then 'pending_acceptance'
                        when enrolled = true and cohort_application_status = 'expelled' then 'expelled'
                        when enrolled = false and cohort_application_status = 'expelled' then 'did_not_enroll'
                        when graduation_status in ('graduated', 'honors') then 'graduated'
                        when graduation_status = 'failed' then 'failed'
                        when enrolled = true then 'enrolled'
                        else 'pending_enrollment' end
                        as status
                    , versions.cohort_application_status
                    , versions.graduation_status
                    , versions.foundations_complete
                    , versions.showed_up
                    , versions.enrolled
                from versions
            )
            select * from versions_2
            with no data
        ~

        add_index :cohort_status_changes, [:user_id, :cohort_id, :from_time, :until_time], :name => :user_cohort_time_on_coh_status_changes, :unique => true

    end

    # add new enrolled definition based on enrollment deadline
    def version_20170228160118
        execute %Q~
            create materialized view cohort_status_changes as
            with indexed_application_versions as (
                select
                    -- in order to be careful of the case where the cohort_id has changed on an application over time,
                    -- we grab the cohort_id from the current record in cohort_applications
                    cohort_applications.user_id
                    , cohort_applications.cohort_id
                    , cohort_applications_versions.status
                    , cohort_applications_versions.graduation_status
                    , cohort_applications_versions.version_created_at
                    , row_number() OVER (PARTITION BY cohort_applications_versions.user_id, cohort_applications_versions.id
                                ORDER BY cohort_applications_versions.version_created_at ASC NULLS LAST) AS version_index
                from cohort_applications
                    join cohort_applications_versions on cohort_applications_versions.id = cohort_applications.id
                order by
                 cohort_applications_versions.user_id
                 , cohort_applications_versions.version_created_at
            )
            -- Join each version with the version that came after it, and record the change
            , application_changes as (
                select
                    old_versions.user_id
                    , old_versions.cohort_id
                    , old_versions.version_created_at as from_time
                    , coalesce(new_versions.version_created_at, '2099-01-01') as until_time
                    , old_versions.status as cohort_application_status
                    , coalesce(old_versions.graduation_status, 'pending') as graduation_status
                from
                    indexed_application_versions old_versions
                        left join indexed_application_versions new_versions
                            on old_versions.user_id = new_versions.user_id
                                and old_versions.cohort_id = new_versions.cohort_id
                                and old_versions.version_index = new_versions.version_index - 1

            )
            -- Find places where past_enrollment_deadline changed
            , past_enrollment_deadline_changes as (
                select
                    cohort_applications.user_id
                    , cohort_applications.cohort_id
                    , cohort_content_details.enrollment_deadline as from_time
                    , '2099-01-01'::timestamp as until_time
                    , application_changes.cohort_application_status = 'accepted' as enrolled
                    , cohorts.name_locales
                    , cohorts.start_date
                from cohort_applications
                    join cohort_content_details on cohort_content_details.cohort_id = cohort_applications.cohort_id
                    join published_cohorts cohorts on cohorts.id = cohort_applications.cohort_id
                    join application_changes on
                        application_changes.user_id = cohort_applications.user_id
                        and application_changes.cohort_id = cohort_applications.cohort_id
                        and application_changes.from_time <= cohort_content_details.enrollment_deadline
                        and application_changes.until_time > cohort_content_details.enrollment_deadline
            )
            -- Find places where foundations_complete changed
            , foundations_complete_changes as (
                select
                    cohort_user_lesson_progress_records.user_id
                    , cohort_user_lesson_progress_records.cohort_id
                    , true as foundations_complete
                    , max(cohort_user_lesson_progress_records.completed_at) as from_time
                    , '2099-01-01'::timestamp as until_time
                from
                    cohort_user_lesson_progress_records
                    join cohort_content_details
                        on cohort_user_lesson_progress_records.cohort_id = cohort_content_details.cohort_id
                where
                    true
                    and foundations = true
                    and completed_at is not null
                group by
                    cohort_user_lesson_progress_records.user_id
                    , cohort_user_lesson_progress_records.cohort_id
                    , foundations_lesson_count
                having
                    count(*) >= foundations_lesson_count
            )
            -- Find places where showed_up changed
            , showed_up_changes as (
                select
                    cohort_user_lesson_progress_records.user_id
                    , cohort_user_lesson_progress_records.cohort_id
                    , true as showed_up
                    , min(cohort_user_lesson_progress_records.completed_at) as from_time
                    , '2099-01-01'::timestamp as until_time
                from
                    cohort_user_lesson_progress_records
                    join cohort_applications_plus
                        on cohort_user_lesson_progress_records.cohort_id = cohort_applications_plus.cohort_id
                        and cohort_user_lesson_progress_records.user_id = cohort_applications_plus.user_id
                where
                    true
                    and in_curriculum = true
                    and completed_at > cohort_applications_plus.accepted_at
                group by
                    cohort_user_lesson_progress_records.user_id
                    , cohort_user_lesson_progress_records.cohort_id
            )
            -- Find the set of unique times in which something changed
            -- for each application
            , change_times as (
                select
                    user_id
                    , cohort_id
                    , from_time as change_time
                from application_changes

                union all

                select
                    user_id
                    , cohort_id
                    , from_time as change_time
                from foundations_complete_changes

                union all

                select
                    user_id
                    , cohort_id
                    , from_time as change_time
                from showed_up_changes

                union all

                select
                    user_id
                    , cohort_id
                    , from_time as change_time
                from past_enrollment_deadline_changes

                order by change_time
            )
            -- Put the change times for each user in order with an index so we can
            -- join each version with the one after it
            , change_times_2 as (
                select
                    change_times.user_id
                    , change_times.cohort_id
                    , change_times.change_time
                    , row_number() OVER (PARTITION BY change_times.user_id, change_times.cohort_id
                                ORDER BY change_times.change_time ASC NULLS LAST) AS version_index
                from change_times
            )
            -- determine the status at each change_time
            , versions as (
                select
                    change_times.user_id
                    , change_times.cohort_id
                    , change_times.change_time as from_time
                    , coalesce(next_change_times.change_time, '2099-01-01') as until_time
                    , application_changes.cohort_application_status
                    , application_changes.graduation_status
                    , coalesce(foundations_complete_changes.foundations_complete, false) as foundations_complete
                    , coalesce(showed_up_changes.showed_up, false) as showed_up
                    , coalesce(past_enrollment_deadline_changes.enrolled, false) as enrolled
                    , row_number() OVER (PARTITION BY change_times.user_id
                        ORDER BY change_times.change_time ASC NULLS LAST) AS version_index
                from
                    change_times_2 change_times
                        left join application_changes
                            on application_changes.from_time <= change_times.change_time
                                and application_changes.until_time > change_times.change_time
                                and application_changes.user_id = change_times.user_id
                                and application_changes.cohort_id = change_times.cohort_id
                        left join foundations_complete_changes
                            on foundations_complete_changes.from_time <= change_times.change_time
                                and foundations_complete_changes.until_time > change_times.change_time
                                and foundations_complete_changes.user_id = change_times.user_id
                                and foundations_complete_changes.cohort_id = change_times.cohort_id
                        left join showed_up_changes
                            on showed_up_changes.from_time <= change_time
                                and showed_up_changes.until_time > change_time
                                and showed_up_changes.user_id = change_times.user_id
                                and showed_up_changes.cohort_id = change_times.cohort_id
                        left join past_enrollment_deadline_changes
                            on past_enrollment_deadline_changes.from_time <= change_time
                                and past_enrollment_deadline_changes.until_time > change_time
                                and past_enrollment_deadline_changes.user_id = change_times.user_id
                                and past_enrollment_deadline_changes.cohort_id = change_times.cohort_id
                        left join change_times_2 next_change_times
                            on next_change_times.user_id = change_times.user_id
                                and next_change_times.cohort_id = change_times.cohort_id
                                and next_change_times.version_index = change_times.version_index + 1
                where cohort_application_status is not null

            )
            , versions_2 as (
                select
                    user_id
                    , cohort_id
                    , from_time
                    , until_time
                    , case
                        when cohort_application_status in ('rejected', 'deferred') then cohort_application_status
                        when cohort_application_status = 'pending' then 'pending_acceptance'
                        when enrolled = true and cohort_application_status = 'expelled' then 'expelled'
                        when enrolled = false and cohort_application_status = 'expelled' then 'did_not_enroll'
                        when graduation_status in ('graduated', 'honors') then 'graduated'
                        when graduation_status = 'failed' then 'failed'
                        when enrolled = true then 'enrolled'
                        else 'pending_enrollment' end
                        as status
                    , versions.cohort_application_status
                    , versions.graduation_status
                    , versions.foundations_complete
                    , versions.showed_up
                    , versions.enrolled
                from versions
            )
            select * from versions_2
            with no data
        ~

        add_index :cohort_status_changes, [:user_id, :cohort_id, :from_time, :until_time], :name => :user_cohort_time_on_coh_status_changes, :unique => true
    end

    def version_20170221140313
        execute %Q|
        create materialized view cohort_status_changes as
        with indexed_application_versions as (
                select
                    cohort_applications_versions.*
                    , row_number() OVER (PARTITION BY cohort_applications_versions.user_id, cohort_applications_versions.cohort_id
                                ORDER BY cohort_applications_versions.version_created_at ASC NULLS LAST) AS version_index
                from cohort_applications_versions
                order by
                 cohort_applications_versions.user_id
                 , cohort_applications_versions.version_created_at
            )
            , application_changes as (
                select
                    old_versions.user_id
                    , old_versions.cohort_id
                    , old_versions.version_created_at as from_time
                    , coalesce(new_versions.version_created_at, '2099-01-01') as until_time
                    , old_versions.status as cohort_application_status
                    , coalesce(old_versions.graduation_status, 'pending') as graduation_status
                from
                    indexed_application_versions old_versions
                        left join indexed_application_versions new_versions
                            on old_versions.user_id = new_versions.user_id
                                and old_versions.cohort_id = new_versions.cohort_id
                                and old_versions.version_index = new_versions.version_index - 1

            )
            , foundations_complete_changes as (
                select
                    cohort_user_lesson_progress_records.user_id
                    , cohort_user_lesson_progress_records.cohort_id
                    , true as foundations_complete
                    , max(cohort_user_lesson_progress_records.completed_at) as from_time
                    , '2099-01-01'::timestamp as until_time
                from
                    cohort_user_lesson_progress_records
                    join cohort_content_details
                        on cohort_user_lesson_progress_records.cohort_id = cohort_content_details.cohort_id
                where
                    true
                    and foundations = true
                    and completed_at is not null
                group by
                    cohort_user_lesson_progress_records.user_id
                    , cohort_user_lesson_progress_records.cohort_id
                    , foundations_lesson_count
                having
                    count(*) >= foundations_lesson_count
            )
            , showed_up_changes as (
                select
                    cohort_user_lesson_progress_records.user_id
                    , cohort_user_lesson_progress_records.cohort_id
                    , true as showed_up
                    , min(cohort_user_lesson_progress_records.completed_at) as from_time
                    , '2099-01-01'::timestamp as until_time
                from
                    cohort_user_lesson_progress_records
                    join cohort_applications_plus
                        on cohort_user_lesson_progress_records.cohort_id = cohort_applications_plus.cohort_id
                        and cohort_user_lesson_progress_records.user_id = cohort_applications_plus.user_id
                where
                    true
                    and in_curriculum = true
                    and completed_at > cohort_applications_plus.accepted_at
                group by
                    cohort_user_lesson_progress_records.user_id
                    , cohort_user_lesson_progress_records.cohort_id
            )
            , change_times as (
                select
                    user_id
                    , cohort_id
                    , from_time as change_time
                from application_changes

                union all

                select
                    user_id
                    , cohort_id
                    , from_time as change_time
                from foundations_complete_changes

                union all

                select
                    user_id
                    , cohort_id
                    , from_time as change_time
                from showed_up_changes

                order by change_time
            )
            , change_times_2 as (
                select
                    change_times.user_id
                    , change_times.cohort_id
                    , change_times.change_time
                    , row_number() OVER (PARTITION BY change_times.user_id, change_times.cohort_id
                                ORDER BY change_times.change_time ASC NULLS LAST) AS version_index
                from change_times
            )
            , versions as (
                select
                    change_times.user_id
                    , change_times.cohort_id
                    , change_times.change_time as from_time
                    , coalesce(next_change_times.change_time, '2099-01-01') as until_time
                    , application_changes.cohort_application_status
                    , application_changes.graduation_status
                    , coalesce(foundations_complete_changes.foundations_complete, false) as foundations_complete
                    , coalesce(showed_up_changes.showed_up, false) as showed_up
                    , coalesce(foundations_complete = true and showed_up = true, false) as enrolled
                    , row_number() OVER (PARTITION BY change_times.user_id
                        ORDER BY change_times.change_time ASC NULLS LAST) AS version_index
                from
                    change_times_2 change_times
                        left join application_changes
                            on application_changes.from_time <= change_times.change_time
                                and application_changes.until_time > change_times.change_time
                                and application_changes.user_id = change_times.user_id
                                and application_changes.cohort_id = change_times.cohort_id
                        left join foundations_complete_changes
                            on foundations_complete_changes.from_time <= change_times.change_time
                                and foundations_complete_changes.until_time > change_times.change_time
                                and foundations_complete_changes.user_id = change_times.user_id
                                and foundations_complete_changes.cohort_id = change_times.cohort_id
                        left join showed_up_changes
                            on showed_up_changes.from_time <= change_time
                                and showed_up_changes.until_time > change_time
                                and showed_up_changes.user_id = change_times.user_id
                                and showed_up_changes.cohort_id = change_times.cohort_id
                        left join change_times_2 next_change_times
                            on next_change_times.user_id = change_times.user_id
                                and next_change_times.cohort_id = change_times.cohort_id
                                and next_change_times.version_index = change_times.version_index + 1
                where cohort_application_status is not null

            )
            , versions_2 as (
                select
                    user_id
                    , cohort_id
                    , from_time
                    , until_time
                    , case
                        when cohort_application_status in ('rejected', 'deferred') then cohort_application_status
                        when cohort_application_status = 'pending' then 'pending_acceptance'
                        when enrolled = true and cohort_application_status = 'expelled' then 'expelled'
                        when enrolled = false and cohort_application_status = 'expelled' then 'did_not_enroll'
                        when graduation_status in ('graduated', 'honors') then 'graduated'
                        when graduation_status = 'failed' then 'failed'
                        when enrolled = true then 'enrolled'
                        else 'pending_enrollment' end
                        as status
                    , versions.cohort_application_status
                    , versions.graduation_status
                    , versions.foundations_complete
                    , versions.showed_up
                    , versions.enrolled
                from versions
            )
            select * from versions_2
            with no data
            |

        add_index :cohort_status_changes, [:user_id, :cohort_id, :from_time], :name => :user_cohort_time_on_coh_status_changes


    end

end