class ViewHelpers::PublishedPlaylistStreams < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    # Materialized CTEs
    def version_20200625152000
        current
    end

    def version_20170310204902
        execute %Q~
            create materialized view published_playlist_streams as
            with stream_locale_packs as (
                select
                    id as playlist_id
                    , locale_pack_id as playlist_locale_pack_id
                    , title as playlist_title
                    , version_id as playlist_version_id
                    , locale as playlist_locale
                    , (unnest(published_playlists.stream_entries)->>'locale_pack_id')::uuid as stream_locale_pack_id
                from published_playlists
            )
           select
               stream_locale_packs.*
               , case when locale_streams.title is not null then locale_streams.title else en_streams.title end as stream_title
               , case when locale_streams.id is not null then locale_streams.id else en_streams.id end as stream_id
               , case when locale_streams.version_id is not null then locale_streams.version_id else en_streams.version_id end as stream_version_id
               , case when locale_streams.locale is not null then locale_streams.locale else en_streams.locale end as stream_locale
           from stream_locale_packs
               left join published_streams as en_streams on
                   stream_locale_packs.stream_locale_pack_id = en_streams.locale_pack_id
                       and en_streams.locale = 'en'
               left join published_streams as locale_streams on
                   stream_locale_packs.stream_locale_pack_id = locale_streams.locale_pack_id
                       and locale_streams.locale = stream_locale_packs.playlist_locale
        ~

        add_index :published_playlist_streams, [:playlist_id, :stream_id], :unique => true
        add_index :published_playlist_streams, :stream_id
        add_index :published_playlist_streams, :playlist_locale_pack_id
        add_index :published_playlist_streams, :stream_locale_pack_id
    end

    def version_20170220200244
        execute %Q~
            create materialized view published_playlist_streams as
            with stream_locale_packs as (
                select
                    id as playlist_id
                    , locale_pack_id as playlist_locale_pack_id
                    , title as playlist_title
                    , version_id as playlist_version_id
                    , locale as playlist_locale
                    , (unnest(published_playlists.stream_entries)->>'locale_pack_id')::uuid as stream_locale_pack_id
                from published_playlists
            )
           select
               stream_locale_packs.*
               , case when locale_streams.title is not null then locale_streams.title else en_streams.title end as stream_title
               , case when locale_streams.id is not null then locale_streams.id else en_streams.id end as stream_id
               , case when locale_streams.version_id is not null then locale_streams.version_id else en_streams.version_id end as stream_version_id
               , case when locale_streams.locale is not null then locale_streams.locale else en_streams.locale end as stream_locale
           from stream_locale_packs
               left join published_streams as en_streams on
                   stream_locale_packs.stream_locale_pack_id = en_streams.locale_pack_id
                       and en_streams.locale = 'en'
               left join published_streams as locale_streams on
                   stream_locale_packs.stream_locale_pack_id = locale_streams.locale_pack_id
                       and locale_streams.locale = stream_locale_packs.playlist_locale
            with no data
        ~

        add_index :published_playlist_streams, [:playlist_id, :stream_id], :unique => true
        add_index :published_playlist_streams, :stream_id
        add_index :published_playlist_streams, :playlist_locale_pack_id
        add_index :published_playlist_streams, :stream_locale_pack_id
    end

end