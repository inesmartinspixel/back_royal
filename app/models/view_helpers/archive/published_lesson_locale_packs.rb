class ViewHelpers::PublishedLessonLocalePacks < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    def version_20170324205906
        current
    end

    def version_20170220200244
        execute %Q~
            create materialized view published_lesson_locale_packs as
            select
                published_lessons.locale_pack_id
                , lessons.title -- we use the working english title in case it is not published in english
                , string_agg(published_lessons.locale, ',') as locales
                , max(case when published_lessons.test then 1 else 0 end) = 1 as test
                , max(case when published_lessons.assessment then 1 else 0 end) = 1 as assessment
            from published_lessons
                join lessons on lessons.locale_pack_id = published_lessons.locale_pack_id and lessons.locale='en'
            group by
                published_lessons.locale_pack_id
                , lessons.title
            with no data
        ~
        add_index :published_lesson_locale_packs, :locale_pack_id, :unique => true
    end

end