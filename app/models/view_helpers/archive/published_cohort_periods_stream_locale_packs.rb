class ViewHelpers::PublishedCohortPeriodsStreamLocalePacks < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    def version_20200609141113
        # do nothing. converted to table
    end

    def version_20170404153806
        execute %Q~
            create materialized view published_cohort_periods_stream_locale_packs
            as (
                select
                    published_cohort_periods.cohort_id
                    , published_cohort_periods.index
                    , published_stream_locale_packs.locale_pack_id
                    , published_stream_locale_packs.title
                    , published_stream_locale_packs.locale_pack_id = ANY(published_cohort_periods.required_stream_pack_ids) as required
                    , published_stream_locale_packs.locale_pack_id = ANY(published_cohort_periods.optional_stream_pack_ids) as optional
                from published_cohort_periods
                join published_stream_locale_packs
                    on published_stream_locale_packs.locale_pack_id = ANY(published_cohort_periods.required_stream_pack_ids || published_cohort_periods.optional_stream_pack_ids)
                order by cohort_id, index asc, published_stream_locale_packs.locale_pack_id = ANY(published_cohort_periods.required_stream_pack_ids) desc
            )
        ~

        add_index :published_cohort_periods_stream_locale_packs, [:cohort_id, :index, :locale_pack_id], :unique => true, :name => :coh_id_index_on_publ_coh_per_str_loc_packs
    end
end