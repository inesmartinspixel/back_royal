class ViewHelpers::PublishedCohortAdmissionRounds < ViewHelpers::ViewHelperBase
    # -*- SkipSchemaAnnotations

    # Materialized CTEs
    def version_20200625152000
        current
    end

    def version_20190815141309
        execute %Q~
            create view published_cohort_admission_rounds as
            WITH admission_round_definitions AS (
                SELECT
                      published_cohorts.id        AS cohort_id
                    , published_cohorts.name
                    , published_cohorts.program_type
                    , published_cohorts.isolated_network
                    , cohorts_versions.start_date AS cohort_start_date
                    , admission_rounds.definition
                    , admission_rounds.index
                from cohorts_versions published_cohorts
                    join content_publishers on content_publishers.cohort_version_id = published_cohorts.version_id
                    JOIN cohorts_versions ON cohorts_versions.version_id = published_cohorts.version_id
                    , unnest(cohorts_versions.admission_rounds) WITH ORDINALITY admission_rounds(definition, index)
            )
                , with_admission_round_details AS (
                SELECT
                    cohort_id
                    , name
                    , index
                    , program_type
                    , isolated_network
                    -- add_dst_aware_offset is a custom function we've added in a migration
                    , add_dst_aware_offset(cohort_start_date, (definition ->> 'application_deadline_days_offset')::INTEGER, 'days') AS application_deadline
                    , add_dst_aware_offset(cohort_start_date, (definition ->> 'decision_date_days_offset')::INTEGER, 'days') AS decision_date
                    , cohort_start_date
                FROM admission_round_definitions
            )
            , program_types as (
                select distinct program_type from with_admission_round_details
            )
            , promoted_admission_rounds as (
                SELECT
                    program_type
                    , (SELECT row_to_json(prepared_groups) FROM
                        (
                            -- select the first admission round that has
                            -- a cohort_start_date and application_deadline in the future
                            -- sorted by cohort_start_date, application_deadline
                            SELECT
                                cohort_id
                                , index
                            FROM with_admission_round_details
                            where
                                with_admission_round_details.program_type = program_types.program_type
                                and cohort_start_date > now()
                                and application_deadline > now()

                                -- We have a not-null constraint on isolated_network for the cohorts
                                -- table, but do not enforce those contraints on _versions tables.
                                -- This column can be null for cohorts_versions, which published_cohorts
                                -- uses, for legacy cohorts that were backfilled when we added the column.
                                and COALESCE(isolated_network, FALSE) = FALSE
                            order by
                                cohort_start_date
                                ,  application_deadline
                            limit 1
                        ) prepared_groups
                    ) as admission_round

                FROM program_types
            )
                , with_promoted as (
                SELECT
                    with_admission_round_details.*
                    , promoted_admission_rounds.program_type is not null as promoted
                FROM with_admission_round_details
                    left join promoted_admission_rounds
                        on (promoted_admission_rounds.admission_round->>'cohort_id')::uuid = with_admission_round_details.cohort_id
                        and (promoted_admission_rounds.admission_round->>'index')::int  = with_admission_round_details.index
            )
            select * from with_promoted order by program_type, cohort_start_date, application_deadline
        ~

    end

    # dst aware enrollment_deadline
    def version_20180423140732
        execute %Q~
            create view published_cohort_admission_rounds as
            WITH admission_round_definitions AS (
                SELECT
                    published_cohorts.id        AS cohort_id
                    , published_cohorts.name
                    , published_cohorts.program_type
                    , cohorts_versions.start_date AS cohort_start_date
                    , admission_rounds.definition
                    , admission_rounds.index
                from cohorts_versions published_cohorts
                    join content_publishers on content_publishers.cohort_version_id = published_cohorts.version_id
                    JOIN cohorts_versions ON cohorts_versions.version_id = published_cohorts.version_id
                    , unnest(cohorts_versions.admission_rounds) WITH ORDINALITY admission_rounds(definition, index)
            )
                , with_admission_round_details AS (
                SELECT
                    cohort_id
                    , name
                    , index
                    , program_type
                    -- add_dst_aware_offset is a custom function we've added in a migration
                    , add_dst_aware_offset(cohort_start_date, (definition ->> 'application_deadline_days_offset')::INTEGER, 'days') AS application_deadline
                    , add_dst_aware_offset(cohort_start_date, (definition ->> 'decision_date_days_offset')::INTEGER, 'days') AS decision_date
                    , cohort_start_date
                FROM admission_round_definitions
            )
            , program_types as (
                select distinct program_type from with_admission_round_details
            )
            , promoted_admission_rounds as (
                SELECT
                    program_type
                    , (SELECT row_to_json(prepared_groups) FROM
                        (
                            -- select the first admission round that has
                            -- a cohort_start_date and application_deadline in the future
                            -- sorted by cohort_start_date, application_deadline
                            SELECT
                                cohort_id
                                , index
                            FROM with_admission_round_details
                            where
                                with_admission_round_details.program_type = program_types.program_type
                                and cohort_start_date > now()
                                and application_deadline > now()
                            order by
                                cohort_start_date
                                ,  application_deadline
                            limit 1
                        ) prepared_groups
                    ) as admission_round

                FROM program_types
            )
                , with_promoted as (
                SELECT
                    with_admission_round_details.*
                    , promoted_admission_rounds.program_type is not null as promoted
                FROM with_admission_round_details
                    left join promoted_admission_rounds
                        on (promoted_admission_rounds.admission_round->>'cohort_id')::uuid = with_admission_round_details.cohort_id
                        and (promoted_admission_rounds.admission_round->>'index')::int  = with_admission_round_details.index
            )
            select * from with_promoted order by program_type, cohort_start_date, application_deadline
        ~
    end

    def version_20170426142452
        execute %Q~
            create view published_cohort_admission_rounds as
            WITH admission_round_definitions AS (
                SELECT
                      published_cohorts.id        AS cohort_id
                    , published_cohorts.name
                    , published_cohorts.program_type
                    , cohorts_versions.start_date AS cohort_start_date
                    , admission_rounds.definition
                    , admission_rounds.index
                from cohorts_versions published_cohorts
                    join content_publishers on content_publishers.cohort_version_id = published_cohorts.version_id
                    JOIN cohorts_versions ON cohorts_versions.version_id = published_cohorts.version_id
                    , unnest(cohorts_versions.admission_rounds) WITH ORDINALITY admission_rounds(definition, index)
            )
                , with_admission_round_details AS (
                SELECT
                    cohort_id
                    , name
                    , index
                    , program_type
                    , cohort_start_date + (definition ->> 'application_deadline_days_offset'  || ' days')::interval   AS application_deadline
                    , cohort_start_date + (definition ->> 'decision_date_days_offset'  || ' days')::interval   AS decision_date
                    , cohort_start_date
                FROM admission_round_definitions
            )
            , program_types as (
                select distinct program_type from with_admission_round_details
            )
            , promoted_admission_rounds as (
                SELECT
                    program_type
                    , (SELECT row_to_json(prepared_groups) FROM
                        (
                            -- select the first admission round that has
                            -- a cohort_start_date and application_deadline in the future
                            -- sorted by cohort_start_date, application_deadline
                            SELECT
                                cohort_id
                                , index
                            FROM with_admission_round_details
                            where
                                with_admission_round_details.program_type = program_types.program_type
                                and cohort_start_date > now()
                                and application_deadline > now()
                            order by
                                cohort_start_date
                                ,  application_deadline
                            limit 1
                        ) prepared_groups
                    ) as admission_round

                FROM program_types
            )
                , with_promoted as (
                SELECT
                    with_admission_round_details.*
                    , promoted_admission_rounds.program_type is not null as promoted
                FROM with_admission_round_details
                    left join promoted_admission_rounds
                        on (promoted_admission_rounds.admission_round->>'cohort_id')::uuid = with_admission_round_details.cohort_id
                        and (promoted_admission_rounds.admission_round->>'index')::int  = with_admission_round_details.index
            )
            select * from with_promoted order by program_type, cohort_start_date, application_deadline
        ~
    end

end