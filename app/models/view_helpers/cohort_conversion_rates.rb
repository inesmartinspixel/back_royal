basename = File.basename(__FILE__)
require File.expand_path("./../archive/#{basename}", __FILE__)

class ViewHelpers::CohortConversionRates < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    def current
        execute %Q~
            create materialized view cohort_conversion_rates as

            -- count up things that refer to the applications for this cohort (i.e. not
            -- looking at the first cohort a user enrolled in).  These things are not used
            -- for graduation rate calculations, but for yield, etc.
            with counts_from_this_cohort AS MATERIALIZED (
                SELECT
                    aps.cohort_id
                    , count(distinct aps.id) as num_applied
                    , count(case when aps.was_accepted then true else null end) as num_accepted
                    , count(case when cohort_user_progress_records.status in ('enrolled', 'graduated', 'failed', 'expelled') then true else null end) as num_still_enrolled
                    , count(case when cohort_user_progress_records.status in ('enrolled', 'pending_enrollment') then true else null end) as num_active
                    , count(case when cohort_user_progress_records.status = 'deferred' then true else null end) as num_deferred
                    , count(case when cohort_user_progress_records.meets_graduation_requirements then true else null end) as num_meets_grad_req

                from cohort_applications_plus aps
                    left join cohort_user_progress_records
                        on aps.user_id = cohort_user_progress_records.user_id
                        and aps.cohort_id = cohort_user_progress_records.cohort_id
                group by aps.cohort_id
            )

            , enrollment_records_with_chance_of_graduating AS MATERIALIZED (
                select
                    enrollment_records.*

                    -- This is necessary because we do not define a chance_of_graduating
                    -- for all cohorts.  It's not set for MBA1 because we had to leave MBA1
                    -- out of the MDPPredictor stuff, since it is so unique.  It's not set
                    -- for EMBA because we haven't gotten around to it.  I haven't even thought
                    -- about how this stuff would work for program types with schedules or different
                    -- cohorts
                    , case
                        when could_graduate_by_deadline = false
                            then 0
                        when did_graduate_by_deadline
                            then 1
                        when chance_of_graduating is not NULL
                            then chance_of_graduating
                        else
                            null
                    end as chance_of_graduating
                from
                    program_enrollment_progress_records enrollment_records
                    left join user_chance_of_graduating_records
                        on user_chance_of_graduating_records.user_id = enrollment_records.user_id
                        and user_chance_of_graduating_records.program_type = enrollment_records.program_type
            )

            -- count up things that refer to the users who originally enrolled in
            -- a particular cohort.  These are things related to graduation rates.
            , counts_from_users_who_first_enrolled_in_this_cohort AS MATERIALIZED (
                select
                    enrollment_records.original_cohort_id
                    , count(*) as num_first_enrolled
                    , count(case when enrollment_records.final_status = 'graduated' then true else null end) as num_graduated
                    , count(case when enrollment_records.did_graduate_by_deadline then true else null end) as num_did_graduate_by_deadline
                    , count(case when enrollment_records.could_graduate_by_deadline then true else null end) as num_could_graduate_by_deadline
                    , count(case when enrollment_records.final_status = 'did_not_graduate' then true else null end) as num_did_not_graduate
                    , count(case when enrollment_records.final_status = 'deferred' then true else null end) as num_currently_deferred
                    , count(case when enrollment_records.original_cohort_id = enrollment_records.final_cohort_id and enrollment_records.final_status in ('enrolled', 'pending_enrollment') then true else null end) as num_currently_enrolled_in_orig_cohort
                    , count(case when enrollment_records.original_cohort_id != enrollment_records.final_cohort_id and enrollment_records.final_status in ('enrolled', 'pending_enrollment') then true else null end) as num_currently_enrolled_in_later_cohort
                    , avg(chance_of_graduating) as expected_grad_rate
                    , avg(case
                        when did_graduate_by_deadline then 1
                        else 0 end
                    ) as min_grad_rate

                from enrollment_records_with_chance_of_graduating enrollment_records
                    join published_cohorts original_cohorts
                        on original_cohorts.id = enrollment_records.original_cohort_id
                group by
                    enrollment_records.original_cohort_id
            )

            -- put all the counts together and filter out irrelevant program types
            , step_1 AS MATERIALIZED (
                select
                    counts_from_this_cohort.cohort_id
                    , published_cohorts.program_type
                    , published_cohorts.name as cohort_name

                    -- these counts refer to applications for this cohort
                    , counts_from_this_cohort.num_applied
                    , counts_from_this_cohort.num_accepted
                    , counts_from_this_cohort.num_still_enrolled
                    , counts_from_this_cohort.num_active
                    , counts_from_this_cohort.num_meets_grad_req
                    , counts_from_this_cohort.num_deferred


                    -- these counts refer to people who first enrolled in this cohort, and
                    -- indicate what their status is in the last cohort they were in
                    , counts_from_users_who_first_enrolled_in_this_cohort.num_first_enrolled
                    , counts_from_users_who_first_enrolled_in_this_cohort.num_graduated
                    , counts_from_users_who_first_enrolled_in_this_cohort.num_did_graduate_by_deadline
                    , counts_from_users_who_first_enrolled_in_this_cohort.num_could_graduate_by_deadline
                    , counts_from_users_who_first_enrolled_in_this_cohort.num_did_not_graduate
                    , counts_from_users_who_first_enrolled_in_this_cohort.num_currently_deferred
                    , counts_from_users_who_first_enrolled_in_this_cohort.num_currently_enrolled_in_orig_cohort
                    , counts_from_users_who_first_enrolled_in_this_cohort.num_currently_enrolled_in_later_cohort
                    , counts_from_users_who_first_enrolled_in_this_cohort.expected_grad_rate
                    , counts_from_users_who_first_enrolled_in_this_cohort.min_grad_rate

                from counts_from_this_cohort
                    join counts_from_users_who_first_enrolled_in_this_cohort
                        on counts_from_users_who_first_enrolled_in_this_cohort.original_cohort_id = counts_from_this_cohort.cohort_id
                    join published_cohorts
                        on published_cohorts.id = counts_from_this_cohort.cohort_id
                WHERE program_type in ('mba', 'emba')
                order by
                    published_cohorts.program_type
                    , published_cohorts.start_date
            )

            -- do some division to get some averages
            , step_2 AS MATERIALIZED (
                select
                    step_1.cohort_id
                    , step_1.program_type
                    , step_1.cohort_name
                    , case when num_applied = 0
                        then null
                        else (num_accepted / num_applied::float)
                        end as acceptance_rate

                    -- it's a little odd that we refactored the graduation rate to
                    -- count deferred people into their first cohort without doing
                    -- something similar for yield, but it seemed less necessary there
                    , case when num_accepted - num_deferred = 0
                        then null
                        else num_still_enrolled / (num_accepted - num_deferred)::float
                        end as yield
                    , expected_grad_rate

                    -- max_grad_rate and official_grad_rate are the same
                    , num_could_graduate_by_deadline::float / num_first_enrolled as official_grad_rate
                    , num_could_graduate_by_deadline::float / num_first_enrolled as max_grad_rate

                    , min_grad_rate


                    , case when num_first_enrolled = 0
                        then null
                        else num_meets_grad_req / num_first_enrolled::float
                        end as meets_grad_req_rate

                    , step_1.num_applied
                    , step_1.num_accepted
                    , step_1.num_still_enrolled
                    , step_1.num_active
                    , step_1.num_deferred
                    , step_1.num_meets_grad_req

                    , step_1.num_first_enrolled
                    , step_1.num_graduated
                    , step_1.num_did_graduate_by_deadline
                    , step_1.num_could_graduate_by_deadline
                    , step_1.num_did_not_graduate
                    , step_1.num_currently_deferred
                    , step_1.num_currently_enrolled_in_orig_cohort
                    , step_1.num_currently_enrolled_in_later_cohort
                from step_1
            )
            select * from step_2
            with no data
        ~

        add_index :cohort_conversion_rates, :cohort_id, :unique => true

    end
end