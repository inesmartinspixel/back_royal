basename = File.basename(__FILE__)
require File.expand_path("./../archive/#{basename}", __FILE__)

class ViewHelpers::ExternalUsers < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    # This is a view instead of a materialized view because this
    # stuff changes all the time

    def current
        execute %Q~
            CREATE VIEW external_users AS
            SELECT users.id
            FROM users
            WHERE EXISTS (
                    SELECT 1
                    FROM users_roles
                    WHERE users_roles.user_id = users.id
                        AND EXISTS (
                            SELECT 1
                            FROM roles
                            WHERE roles.id = users_roles.role_id
                                AND roles.name = 'learner'
                                AND users.email NOT LIKE '%pedago.com%'
                                AND users.email NOT LIKE '%smart.ly%'
                                AND users.email NOT LIKE '%workaround.com%'
                        )
                )
        ~
    end
end