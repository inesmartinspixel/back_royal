basename = File.basename(__FILE__)
require File.expand_path("./../archive/#{basename}", __FILE__)

class ViewHelpers::PublishedLessonLocalePacks < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    # returns one row for every published content item locale pack

    def current
        execute %Q~
            create materialized view published_lesson_locale_packs as
            select
                published_lessons.locale_pack_id
                , lessons.title -- we use the working english title in case it is not published in english
                , string_agg(published_lessons.locale, ',') as locales
                , max(case when published_lessons.test then 1 else 0 end) = 1 as test
                , max(case when published_lessons.assessment then 1 else 0 end) = 1 as assessment
            from published_lessons
                join lessons on lessons.locale_pack_id = published_lessons.locale_pack_id and lessons.locale='en'
            group by
                published_lessons.locale_pack_id
                , lessons.title
        ~
        add_index :published_lesson_locale_packs, :locale_pack_id, :unique => true
    end
end