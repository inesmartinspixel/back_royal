basename = File.basename(__FILE__)
require File.expand_path("./../archive/#{basename}", __FILE__)

class ViewHelpers::PublishedStreamLessons < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    # query that returns one row for every published lesson than is in a published stream,
    # with basic info about both

    def current
        execute %Q~
            create materialized view published_stream_lessons as
            with stream_lessons_1 AS MATERIALIZED (
                select published_streams.*,
                    trim(json_array_elements((json_array_elements(published_streams.chapters)->>'lesson_ids')::json)::text, '\"')::uuid lesson_id
                from published_streams
            )
            , stream_lessons_2 AS MATERIALIZED (
                select
                    lessons_versions.id || stream_lessons_1.id::text as id,
                    coalesce(lessons_versions.test, false) test,
                    coalesce(lessons_versions.assessment, false) assessment,
                    stream_lessons_1.title as stream_title,
                    stream_lessons_1.locale as stream_locale,
                    lessons_versions.title as lesson_title,
                    stream_lessons_1.locale_pack_id as stream_locale_pack_id,
                    stream_lessons_1.version_id as stream_version_id,
                    stream_lessons_1.id as stream_id,
                    published_lessons.locale_pack_id as lesson_locale_pack_id,
                    lessons_versions.version_id as lesson_version_id,
                    lessons_versions.locale as lesson_locale,
                    lessons_versions.id as lesson_id
                from stream_lessons_1
                    join published_lessons on stream_lessons_1.lesson_id = published_lessons.id
                    join lessons_versions on published_lessons.version_id = lessons_versions.version_id
            )
            select * from stream_lessons_2
        ~

        add_index :published_stream_lessons, [:stream_id, :lesson_id], :unique => true
        add_index :published_stream_lessons, :stream_id
        add_index :published_stream_lessons, :lesson_locale_pack_id
        add_index :published_stream_lessons, :stream_locale_pack_id
    end
end