basename = File.basename(__FILE__)
require File.expand_path("./../archive/#{basename}", __FILE__)

class ViewHelpers::CohortStreamLocalePacks < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    self.deprecated = true

    # do nothing.  this view has been renamed published_cohort_stream_locale_packs after version 20170310204902

end