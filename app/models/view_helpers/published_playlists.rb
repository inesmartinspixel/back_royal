basename = File.basename(__FILE__)
require File.expand_path("./../archive/#{basename}", __FILE__)

class ViewHelpers::PublishedPlaylists < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    # returns one row for every published content item

    def current
        execute %Q~
            create materialized view published_playlists as
            select
                playlists.id,
                playlists.locale_pack_id,
                playlists_versions.title,
                playlists_versions.version_id,
                playlists_versions.locale,
                playlists_versions.stream_entries,
                array_length(playlists_versions.stream_entries, 1) as stream_count
            from playlists
                join playlists_versions on playlists.id = playlists_versions.id
                join content_publishers on content_publishers.playlist_version_id = playlists_versions.version_id
        ~

        add_index :published_playlists, :id, :unique => :true
        add_index :published_playlists, :locale_pack_id
    end
end