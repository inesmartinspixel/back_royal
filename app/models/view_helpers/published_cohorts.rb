basename = File.basename(__FILE__)
require File.expand_path("./../archive/#{basename}", __FILE__)

class ViewHelpers::PublishedCohorts < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    # returns one row for every published content item

    # This is a view instead of a materialized view because the
    # value of promoted can change with the clock

    def current
        execute %Q~
            create view published_cohorts as
            WITH published_cohort_playlist_collections AS MATERIALIZED (
                SELECT
                    cohorts_versions.id                             AS cohort_id
                    , CASE WHEN playlist_collection IS NULL THEN '{}'::JSON
                        ELSE playlist_collection
                    END AS playlist_collection
                FROM cohorts_versions
                    JOIN content_publishers
                        ON content_publishers.cohort_version_id = cohorts_versions.version_id
                    -- This LEFT JOIN LATERAL against the UNNESTed playlist_collections array ensures that a
                    -- row is returned for every published cohort even if its playlist_collections array is
                    -- empty. Practically, this isn't a problem, but it's helpful in our specs.
                    LEFT JOIN LATERAL UNNEST(cohorts_versions.playlist_collections) playlist_collection ON TRUE
            ),
            published_cohort_playlist_collection_required_playlist_pack_ids AS MATERIALIZED (
                SELECT
                    published_cohort_playlist_collections.cohort_id
                    , ARRAY(SELECT required_playlist_pack_id FROM JSON_ARRAY_ELEMENTS_TEXT(playlist_collection->'required_playlist_pack_ids') required_playlist_pack_id) AS required_playlist_pack_ids
                FROM published_cohort_playlist_collections
            ),
            published_cohort_required_playlist_pack_ids AS MATERIALIZED (
                SELECT
                    published_cohort_playlist_collection_required_playlist_pack_ids.cohort_id
                    , ARRAY_CAT_AGG(published_cohort_playlist_collection_required_playlist_pack_ids.required_playlist_pack_ids)::uuid[] AS required_playlist_pack_ids
                FROM published_cohort_playlist_collection_required_playlist_pack_ids
                GROUP BY published_cohort_playlist_collection_required_playlist_pack_ids.cohort_id
            )
            select
                cohorts_versions.id
                , case
                    -- FIXME: once we've removed unnecessary cohort_promotions (https://trello.com/c/xzMZHN17/2068-chore-cleanup-after-cohort-dates-promotion-refactor)
                    -- we can stop hardcoding here which program types pay attention to cohort promotions, and just
                    -- say if either there is a cohort promotion or if there is a promoted admission round, then
                    -- the cohort is promoted
                    when cohorts_versions.program_type in ('emba', 'mba')
                        then promoted_admission_rounds.cohort_id is not null
                    ELSE
                        cohort_promotions.cohort_id is not null
                    end as promoted
                , cohorts_versions.name
                , cohorts_versions.program_type
                , published_cohort_required_playlist_pack_ids.required_playlist_pack_ids
                , cohorts_versions.specialization_playlist_pack_ids
                , cohorts_versions.start_date
                , cohorts_versions.end_date
                , cohorts_versions.start_date + interval '3 years' as grad_deadline
                , CASE
                    WHEN enrollment_deadline_days_offset IS NULL
                    THEN cohorts_versions.start_date
                    -- add_dst_aware_offset is a custom function we've added in a migration
                    ELSE add_dst_aware_offset(cohorts_versions.start_date, cohorts_versions.enrollment_deadline_days_offset, 'days')
                    END AS enrollment_deadline
                , add_dst_aware_offset(cohorts_versions.end_date, cohorts_versions.graduation_days_offset_from_end, 'days') AS graduation_date
                , add_dst_aware_offset(cohorts_versions.start_date, cohorts_versions.early_registration_deadline_days_offset, 'days') AS early_registration_deadline
                , cohorts_versions.version_id
            from cohorts_versions
                JOIN published_cohort_required_playlist_pack_ids
                    ON published_cohort_required_playlist_pack_ids.cohort_id = cohorts_versions.id
                join content_publishers
                    on content_publishers.cohort_version_id = cohorts_versions.version_id
                left join cohort_promotions
                    on cohort_promotions.cohort_id = cohorts_versions.id
                left join published_cohort_admission_rounds promoted_admission_rounds
                    on promoted_admission_rounds.cohort_id = cohorts_versions.id
                        and promoted_admission_rounds.promoted = true
        ~
    end
end