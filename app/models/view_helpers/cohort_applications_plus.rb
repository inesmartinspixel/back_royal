basename = File.basename(__FILE__)
require File.expand_path("./../archive/#{basename}", __FILE__)

class ViewHelpers::CohortApplicationsPlus < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    def current
        execute %Q~
            create materialized view cohort_applications_plus as

            -- first, combine all of the versions for a record into a single record
            with cohort_states AS MATERIALIZED (
                select
                    cohort_applications.id
                    , cohort_applications.user_id
                    , cohort_applications.cohort_id
                    , cohort_applications.status as current_status
                    , cohort_applications.graduation_status
                    , cohort_applications.final_score
                    , cohort_applications.project_score
                    , cohort_applications.meets_graduation_requirements
                    , cohorts.name as cohort_name
                    , cohort_applications.applied_at

                    -- We check the cohort_id because some applications were
                    -- marked as accepted in business cert and then switched to emba, which does not mean
                    -- that we should count them as accepted
                    , max(case when aps.cohort_id = cohort_applications.cohort_id and aps.status = 'accepted' then 1 else 0 end) = 1 as was_accepted
                    , max(case when aps.cohort_id = cohort_applications.cohort_id and aps.registered then 1 else 0 end) = 1 as was_registered
                    , min(case when aps.cohort_id = cohort_applications.cohort_id and aps.status = 'accepted' then version_created_at else null end) accepted_at
                    , min(case when aps.cohort_id = cohort_applications.cohort_id and aps.registered then version_created_at else null end) registered_at
                    , cohorts.start_date as cohort_start_date -- used to join with previous and later applications

                -- note that, if the cohort ever changed on an application, we treat it has
                -- though it has always been for the current cohort
                from cohort_applications
                    join cohorts on cohort_applications.cohort_id = cohorts.id
                    join cohort_applications_versions aps on cohort_applications.id = aps.id
                group by
                    cohort_applications.id,
                    cohort_applications.user_id,
                    cohort_applications.cohort_id,
                    cohorts.name,
                    cohorts.start_date,
                    cohort_applications.status,
                    cohort_applications.graduation_status,
                    cohort_applications.applied_at,
                    cohort_applications.final_score
            )

            -- then, for each record, look for records for the same user on different cohorts, and
            -- add some info about that
            , cohort_states_2 AS MATERIALIZED (
                select
                    cohort_states.id
                    , cohort_states.user_id
                    , cohort_states.cohort_id
                    , cohort_states.cohort_name
                    , cohort_states.current_status
                    , cohort_states.graduation_status
                    , cohort_states.applied_at
                    , cohort_states.accepted_at
                    , cohort_states.was_accepted
                    , cohort_states.was_registered
                    , cohort_states.registered_at
                    , cohort_states.final_score
                    , cohort_states.meets_graduation_requirements
                    , cohort_states.project_score
                    , string_agg(case when later_cohort_states.cohort_name is not null then later_cohort_states.cohort_name || ':' || later_cohort_states.current_status else null end, ',') as applied_to_later
                    , string_agg(case when previous_cohort_states.cohort_name is not null then previous_cohort_states.cohort_name || ':' || previous_cohort_states.current_status else null end, ',') as applied_to_previous
                from cohort_states
                    left join cohort_states as later_cohort_states
                        on cohort_states.user_id = later_cohort_states.user_id and later_cohort_states.cohort_start_date > cohort_states.cohort_start_date
                    left join cohort_states as previous_cohort_states
                        on cohort_states.user_id = previous_cohort_states.user_id and previous_cohort_states.cohort_start_date < cohort_states.cohort_start_date
                group by
                    cohort_states.id
                    , cohort_states.user_id
                    , cohort_states.cohort_id
                    , cohort_states.cohort_name
                    , cohort_states.current_status
                    , cohort_states.applied_at
                    , cohort_states.accepted_at
                    , cohort_states.final_score
                    , cohort_states.meets_graduation_requirements
                    , cohort_states.project_score
                    , cohort_states.was_accepted
                    , cohort_states.was_registered
                    , cohort_states.registered_at
                    , cohort_states.graduation_status
            )
            select * from cohort_states_2
            with no data
        ~

        add_index :cohort_applications_plus, [:cohort_id, :user_id], :unique => true, :name => :coh_and_user_on_coh_app_plus
        add_index :cohort_applications_plus, :user_id
        add_index :cohort_applications_plus, :was_accepted
        add_index :cohort_applications_plus, :was_registered
    end
end