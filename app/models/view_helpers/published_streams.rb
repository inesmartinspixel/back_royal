basename = File.basename(__FILE__)
require File.expand_path("./../archive/#{basename}", __FILE__)

class ViewHelpers::PublishedStreams < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    # returns one row for every published content item

    def current
        execute %Q~
            create materialized view published_streams as
            select
                lesson_streams.id,
                lesson_streams.locale_pack_id,
                lesson_streams_versions.title,
                lesson_streams_versions.version_id,
                lesson_streams_versions.chapters,
                lesson_streams_versions.locale,
                lesson_streams_versions.exam
            from lesson_streams
                join lesson_streams_versions on lesson_streams.id = lesson_streams_versions.id
                join content_publishers on content_publishers.lesson_stream_version_id = lesson_streams_versions.version_id
        ~

        add_index :published_streams, :id, :unique => :true
        add_index :published_streams, :locale_pack_id
    end
end