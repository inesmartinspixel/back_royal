basename = File.basename(__FILE__)
require File.expand_path("./../archive/#{basename}", __FILE__)

class ViewHelpers::UsersRelevantCohorts < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations


    # this is a view because it is used real-time in the app
    # but it relates to users so can change too much to materialize
    def current
        execute %Q~
            create view users_relevant_cohorts as

            select
                distinct users.id as user_id
                , coalesce(pending_or_accepted_cohorts.id, promoted_cohorts.id) as cohort_id
            from
                users

                -- join against cohorts for which this user has a pending or accepted application
                left join
                    cohort_applications on cohort_applications.user_id = users.id
                    and cohort_applications.status in ('pre_accepted', 'accepted', 'pending')
                left join published_cohorts pending_or_accepted_cohorts
                    on pending_or_accepted_cohorts.id = cohort_applications.cohort_id

                -- and, if there are no pending or accepted ones, join against promoted
                -- cohorts where the program_type matches the user's fallback_program_type
                left join published_cohorts promoted_cohorts
                    on pending_or_accepted_cohorts.id is null
                        and promoted_cohorts.promoted = true
                        and users.fallback_program_type = promoted_cohorts.program_type

        ~

    end
end