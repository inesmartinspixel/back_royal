basename = File.basename(__FILE__)
require File.expand_path("./../archive/#{basename}", __FILE__)

class ViewHelpers::UserLessonProgressRecords < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    self.deprecated = true

    def current
        # do nothing.  This has been replaced by a table created in
        # WriteUserLessonProgessRecords
    end
end