basename = File.basename(__FILE__)
require File.expand_path("./../archive/#{basename}", __FILE__)

class ViewHelpers::PlaylistProgress < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    def current
        execute %Q~
            create materialized view playlist_progress as

            select
                lesson_streams_progress.user_id
                , playlist_locale_pack_id as locale_pack_id
                , true as started
                , count(distinct case when lesson_streams_progress.completed_at is not null then lesson_streams_progress.locale_pack_id else null end) = published_playlists.stream_count as completed
            from
                lesson_streams_progress
                join published_playlist_streams
                    on published_playlist_streams.stream_locale_pack_id = lesson_streams_progress.locale_pack_id
                join published_playlists
                    on published_playlists.locale_pack_id = published_playlist_streams.playlist_locale_pack_id
                join user_can_see_playlist_locale_packs
                    on published_playlists.locale_pack_id = user_can_see_playlist_locale_packs.locale_pack_id
                    and lesson_streams_progress.user_id = user_can_see_playlist_locale_packs.user_id
                    -- note: next line ensures that if there are ever a different number of streams across languages, use the English version's stream count
                    -- this would mean that if a non-English version had *more* streams than the English version, it couldn't show up as complete in this view
                    -- but, this at least means the view won't break. we have generally said that playlists should contain the same number of streams in all
                    -- locales, but we hit this bug on staging when experimenting with playlist changes. best to avoid it breaking by adding this clause.
                    and published_playlists.locale='en'
            group by
                lesson_streams_progress.user_id
                , playlist_locale_pack_id
                , published_playlists.stream_count
            with no data
        ~

        add_index :playlist_progress, [:user_id, :locale_pack_id], :unique => true
    end
end