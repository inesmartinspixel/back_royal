basename = File.basename(__FILE__)
require File.expand_path("./../archive/#{basename}", __FILE__)

class ViewHelpers::Acquisitions < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    def current
        execute %Q~
            create materialized view acquisitions as

                select
                    *
                from dblink('red_royal', $REDSHIFT$

                        with ip_addresses_with_many_logins_in_one_day AS MATERIALIZED (
                            select
                                distinct ip_address
                            from
                                events
                            where
                                event_type='page_load:load'
                                and ip_address is not null
                            group by
                                ip_address
                                , date_trunc('day', estimated_time)
                            having
                               count(distinct user_id) > 5
                            order by ip_address
                        )
                        , ip_addresses_from_which_someone_registered AS MATERIALIZED (
                            select
                                distinct ip_addresses_with_many_logins_in_one_day.ip_address
                            from
                                ip_addresses_with_many_logins_in_one_day
                                join events page_load_events
                                    on page_load_events.ip_address = ip_addresses_with_many_logins_in_one_day.ip_address
                                    and page_load_events.event_type = 'page_load:load'
                                join events user_created_events
                                    on user_created_events.user_id = page_load_events.user_id
                                    and user_created_events.event_type = 'user:created'
                        )
                        , bot_ip_addresses AS MATERIALIZED (
                            select
                                ip_address
                            from
                                ip_addresses_with_many_logins_in_one_day
                            where
                                ip_address not in (select ip_address from ip_addresses_from_which_someone_registered)
                        )
                        , bot_users AS MATERIALIZED (
                            select
                                distinct user_id
                            from events
                                join bot_ip_addresses
                                    on bot_ip_addresses.ip_address = events.ip_address
                            where event_type='page_load:load'
                        )
                        , alias_users AS MATERIALIZED (
                            select
                                distinct user_id
                            from
                                events
                            where event_type  = 'anonymous_user:login'
                                and user_id != logged_in_user_id
                                and user_id not in (select distinct user_id from events where event_type='user:created')
                        )
                        , first_page_loads AS MATERIALIZED (
                            select
                                events.user_id
                                , min(estimated_time) acquired_at
                            from events
                            where event_type = 'page_load:load'
                                and user_id not in (select user_id from alias_users)
                                and user_id not in (select user_id from bot_users)
                            group by
                                events.user_id
                        )
                        , first_page_loads_with_cordova_flag AS MATERIALIZED (
                            select
                                first_page_loads.user_id
                                , acquired_at

                                -- in the wild, there will only be one event with this
                                -- estimated_time, but in tests there are multiple events with the
                                -- same estimated_time, so we need to guarantee uniqueness
                                , count(case when cordova then true else null end) > 0 acquired_on_cordova
                            from first_page_loads
                                join events
                                    on events.user_id = first_page_loads.user_id
                                    and events.event_type='page_load:load'
                                    and events.estimated_time = acquired_at
                            group by
                                first_page_loads.user_id
                                , acquired_at
                        )
                        , acquisitions AS MATERIALIZED (
                            select
                              first_page_loads_with_cordova_flag.user_id
                              , acquired_at
                              , acquired_on_cordova
                              , sum(case when event_type='user:created' then 1 else 0 end) > 0 as registered
                              , count(*) > 1 as did_not_bounce
                            from first_page_loads_with_cordova_flag
                                join events
                                    on first_page_loads_with_cordova_flag.user_id = events.user_id
                                    and events.event_type in ('user:created', 'page_load:load', 'click')
                            group by
                                first_page_loads_with_cordova_flag.user_id
                              , acquired_at
                              , acquired_on_cordova
                        )
                        select * from acquisitions

                    $REDSHIFT$) AS acquisitions (
                        user_id uuid
                        , acquired_at timestamp
                        , acquired_on_cordova boolean
                        , registered boolean
                        , did_not_bounce boolean
                    )
            with no data
        ~

        add_index :acquisitions, [:user_id], :unique => true
    end
end