basename = File.basename(__FILE__)
require File.expand_path("./../archive/#{basename}", __FILE__)

class ViewHelpers::PublishedLessons < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    # returns one row for every published content item

    def current
        execute %Q~
            create materialized view published_lessons as
            select
                lessons.id,
                lessons.locale_pack_id,
                lessons_versions.title,
                lessons_versions.version_id,
                lessons_versions.locale,
                lessons_versions.assessment,
                lessons_versions.test
            from lessons
                join lessons_versions on lessons.id = lessons_versions.id
                join content_publishers on content_publishers.lesson_version_id = lessons_versions.version_id
        ~

        add_index :published_lessons, :id, :unique => :true
        add_index :published_lessons, :locale_pack_id
    end
end