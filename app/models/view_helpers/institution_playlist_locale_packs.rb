basename = File.basename(__FILE__)
require File.expand_path("./../archive/#{basename}", __FILE__)

class ViewHelpers::InstitutionPlaylistLocalePacks < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    def current
        execute %Q|
            create materialized view institution_playlist_locale_packs as
            with step_1 AS MATERIALIZED (
                select
                    institutions.id as institution_id,
                    institutions.name as institution_name,
                    unnest(playlist_pack_ids) as playlist_locale_pack_id
                from institutions
            )
            select
                step_1.institution_id,
                step_1.institution_name,
                published_playlist_locale_packs.title as playlist_title,
                published_playlist_locale_packs.locales as playlist_locales,
                published_playlist_locale_packs.locale_pack_id as playlist_locale_pack_id
            from step_1
                join published_playlist_locale_packs on published_playlist_locale_packs.locale_pack_id = step_1.playlist_locale_pack_id
            group by
                step_1.institution_id,
                step_1.institution_name,
                published_playlist_locale_packs.title,
                published_playlist_locale_packs.locales,
                published_playlist_locale_packs.locale_pack_id
            order by institution_name, playlist_title
        |

        add_index :institution_playlist_locale_packs, [:institution_id, :playlist_locale_pack_id], :unique => true, :name => :inst_and_play_lp_on_inst_play_lps
        add_index :institution_playlist_locale_packs, :playlist_locale_pack_id, :name => :play_lps_on_inst_play_lps
    end
end