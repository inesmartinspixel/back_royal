basename = File.basename(__FILE__)
require File.expand_path("./../archive/#{basename}", __FILE__)

class ViewHelpers::CohortUserProgressRecords < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    def current
        execute %Q|
            create materialized view cohort_user_progress_records as
            with current_status AS MATERIALIZED (
                select
                    cohort_applications_plus.cohort_id
                    , cohort_applications_plus.user_id
                    , cohort_applications_plus.cohort_name
                    , published_cohorts.program_type
                    , published_cohorts.start_date
                    , cohort_status_changes.status
                from cohort_applications_plus
                    join published_cohorts
                        on published_cohorts.id = cohort_applications_plus.cohort_id
                    join cohort_status_changes on
                        cohort_status_changes.cohort_id = cohort_applications_plus.cohort_id
                        and cohort_status_changes.user_id = cohort_applications_plus.user_id
                        and cohort_status_changes.until_time = '01-01-2099'
                where
                    cohort_applications_plus.was_accepted=true
                    and published_cohorts.program_type != 'career_network_only'
            )

            -- For emba (or any cohort with required specializations), we only count the test
            -- scores from the n best specializations, where n is the num_required_specializations,
            -- This ensures that people are not penalized for doing extra ones that are more difficult
            --
            -- First step is to grab the average test score from each specialization.  It is currently
            -- the case that each specialization only has one test stream with one test lesson, but this
            -- query does not assume that to be the case.  If there is more than one, it averages the
            -- scores.  (See note below near avg_test_score about how we average lesson scores to get the
            -- total score)
            --
            -- See e61f9411-21ba-44cd-a4e4-eb44ea30499e for an example of a user who did extra specializations
            , specialization_test_scores AS MATERIALIZED (
                SELECT
                    current_status.user_id
                    , current_status.cohort_id
                    , playlists.playlist_title
                    , lessons.stream_title
                    , lessons.stream_locale_pack_id
                    , cohorts_versions.num_required_specializations
                    , array_agg(lessons.lesson_title) lessons
                    , array_agg(lessons.lesson_locale_pack_id) lesson_locale_pack_ids
                    , avg(lesson_progress.official_test_score) avg_test_score
                from current_status
                    join published_cohorts
                        on published_cohorts.id = current_status.cohort_id
                    join cohorts_versions
                        on published_cohorts.version_id = cohorts_versions.version_id
                        and cohorts_versions.num_required_specializations > 0
                    join published_cohort_playlist_locale_packs playlists
                        on playlists.cohort_id = current_status.cohort_id
                        and playlists.specialization = true
                    join published_playlist_streams
                        on published_playlist_streams.playlist_locale_pack_id = playlists.playlist_locale_pack_id
                    join published_stream_lesson_locale_packs lessons
                        on lessons.stream_locale_pack_id = published_playlist_streams.stream_locale_pack_id
                        and lessons.test = true
                    join lesson_streams_progress
                        on lesson_streams_progress.locale_pack_id = lessons.stream_locale_pack_id
                        and lesson_streams_progress.user_id = current_status.user_id
                        and lesson_streams_progress.completed_at is not null
                    join user_lesson_progress_records lesson_progress
                        on lesson_progress.locale_pack_id = lessons.lesson_locale_pack_id
                        and lesson_progress.user_id = current_status.user_id
                group by

                    current_status.user_id
                    , current_status.cohort_id
                    , playlists.playlist_title
                    , lessons.stream_title
                    , lessons.stream_locale_pack_id
                    , cohorts_versions.num_required_specializations
            )

            -- Assign each specialization a rank, ordering them by best avg_test_score
            -- for each user.  So the best score for a user will have a 1, then next
            -- will have a 2, etc.
            , ranked_specialization_test_scores AS MATERIALIZED (
                SELECT
                    specialization_test_scores.*
                    , rank() OVER (
                        PARTITION BY cohort_id, user_id
                        ORDER BY avg_test_score DESC
                    )
                FROM
                    specialization_test_scores
                order by
                    specialization_test_scores.cohort_id
                    , specialization_test_scores.user_id
                    , rank
            )

            -- We only count lessons from the top n specializations, where n
            -- is the num_required_specializations for the cohort.  The unnesting
            -- here handles the case where one of the specialization has multiple
            -- lessons (though this currently is never the case, see note above).
            -- Since there could be multiple test lessons in a specialization, we
            -- might count more than num_required_specializations lessons.
            , test_scores_to_count AS MATERIALIZED (
                -- specialization tests
                SELECT
                    cohort_id
                    , user_id
                    , avg_test_score as official_test_score
                    , unnest(lesson_locale_pack_ids) lesson_locale_pack_id
                FROM
                    ranked_specialization_test_scores
                WHERE
                    rank <= num_required_specializations

                UNION

                -- required tests
                SELECT
                    lesson_progress.cohort_id
                    , lesson_progress.user_id
                    , lesson_progress.official_test_score
                    , lesson_progress.lesson_locale_pack_id
                from cohort_applications_plus
                    left join cohort_user_lesson_progress_records lesson_progress on
                        cohort_applications_plus.cohort_id = lesson_progress.cohort_id
                        and cohort_applications_plus.user_id = lesson_progress.user_id
                where
                    lesson_progress.test
                    and lesson_progress.required

            )

            -- We count all test scores from required lessons as well as test scores
            -- from specializations (see notes above about which ones we count).  Note that
            -- we are averaging all the test lessons.  This means that an exam with more lessons
            -- gets weighted more than an exam with fewer.  This is helpful in MBA where the final
            -- has more lessons and should have more weight.  It is also fine in EMBA, where specializations
            -- each have one lesson each.
            , test_scores AS MATERIALIZED (
                SELECT
                    cohort_id
                    , user_id
                    , avg(official_test_score) as avg_test_score
                FROM
                    test_scores_to_count
                group by
                    cohort_id
                    , user_id
            )

            , lesson_counts AS MATERIALIZED (
                select
                    lesson_progress.cohort_id
                    , lesson_progress.user_id
                    , sum(case
                            when foundations = true and completed_at is not null then 1
                            else 0
                            end
                        ) as foundations_lessons_complete
                    , sum(case
                            when required = true and completed_at is not null then 1
                            else 0
                            end
                        ) as required_lessons_complete
                    , sum(case
                            when test = true and completed_at is not null then 1
                            else 0
                            end
                        ) as test_lessons_complete
                    , sum(case
                            when elective = true and completed_at is not null then 1
                            else 0
                            end
                        ) as elective_lessons_complete
                    , avg(case when assessment then average_assessment_score_first else null end) as average_assessment_score_first
                    , avg(case when assessment then average_assessment_score_best else null end) as average_assessment_score_best
                    , min(case when assessment then average_assessment_score_best else null end) as min_assessment_score_best
                from cohort_applications_plus
                    left join cohort_user_lesson_progress_records lesson_progress on
                        cohort_applications_plus.cohort_id = lesson_progress.cohort_id
                        and cohort_applications_plus.user_id = lesson_progress.user_id
                where
                    cohort_applications_plus.was_accepted=true
                group by
                    lesson_progress.cohort_id
                    , lesson_progress.user_id
            )
            , stream_counts AS MATERIALIZED (
                select
                    cohort_applications_plus.cohort_id
                    , cohort_applications_plus.user_id
                    , sum(case
                            when foundations = true and completed_at is not null then 1
                            else 0
                            end
                        ) as foundations_streams_complete
                    , sum(case
                            when required = true and completed_at is not null then 1
                            else 0
                            end
                        ) as required_streams_complete
                    , sum(case
                            when exam = true and completed_at is not null then 1
                            else 0
                            end
                        ) as exam_streams_complete
                    , sum(case
                            when elective = true and completed_at is not null then 1
                            else 0
                            end
                        ) as elective_streams_complete
                from cohort_applications_plus
                    join published_cohort_stream_locale_packs
                        on published_cohort_stream_locale_packs.cohort_id = cohort_applications_plus.cohort_id
                    left join lesson_streams_progress
                        on published_cohort_stream_locale_packs.stream_locale_pack_id = lesson_streams_progress.locale_pack_id
                        and cohort_applications_plus.user_id = lesson_streams_progress.user_id
                where
                    cohort_applications_plus.was_accepted=true
                group by
                    cohort_applications_plus.cohort_id
                    , cohort_applications_plus.user_id
            )
            , playlist_counts AS MATERIALIZED (
                select
                    cohort_applications_plus.cohort_id
                    , cohort_applications_plus.user_id
                    , sum(case
                            when required = true and completed = true then 1
                            else 0
                            end
                        ) as required_playlists_complete
                    , sum(case
                            when specialization = true and completed = true then 1
                            else 0
                            end
                        ) as specialization_playlists_complete
                from cohort_applications_plus
                    join published_cohort_playlist_locale_packs
                        on published_cohort_playlist_locale_packs.cohort_id = cohort_applications_plus.cohort_id
                    left join playlist_progress
                        on published_cohort_playlist_locale_packs.playlist_locale_pack_id = playlist_progress.locale_pack_id
                        and cohort_applications_plus.user_id = playlist_progress.user_id
                where
                    cohort_applications_plus.was_accepted=true
                group by
                    cohort_applications_plus.cohort_id
                    , cohort_applications_plus.user_id
            )
            , with_counts AS MATERIALIZED (
                select
                    current_status.cohort_name
                    , current_status.cohort_id
                    , current_status.program_type
                    , current_status.start_date
                    , current_status.user_id
                    , current_status.status

                    , stream_counts.required_streams_complete
                    , stream_counts.foundations_streams_complete
                    , stream_counts.exam_streams_complete
                    , stream_counts.elective_streams_complete

                    , playlist_counts.required_playlists_complete
                    , playlist_counts.specialization_playlists_complete

                    , lesson_counts.required_lessons_complete
                    , lesson_counts.foundations_lessons_complete
                    , lesson_counts.test_lessons_complete
                    , lesson_counts.elective_lessons_complete
                    , lesson_counts.average_assessment_score_first
                    , lesson_counts.average_assessment_score_best
                    , lesson_counts.min_assessment_score_best
                    , test_scores.avg_test_score

                from current_status
                    join lesson_counts
                        on current_status.cohort_id = lesson_counts.cohort_id
                        and current_status.user_id = lesson_counts.user_id
                    join stream_counts
                        on current_status.cohort_id = stream_counts.cohort_id
                        and current_status.user_id = stream_counts.user_id
                    join playlist_counts
                        on current_status.cohort_id = playlist_counts.cohort_id
                        and current_status.user_id = playlist_counts.user_id
                    join test_scores
                        on current_status.cohort_id = test_scores.cohort_id
                        and current_status.user_id = test_scores.user_id


            )
            , lesson_info AS MATERIALIZED (
                select
                    cohort_id
                    , count(distinct lesson_locale_pack_id) filter (where published_cohort_lesson_locale_packs.required = true) as required_lesson_count
                    , count(distinct lesson_locale_pack_id) filter (where published_cohort_lesson_locale_packs.elective = true) as elective_lesson_count
                from
                    published_cohort_lesson_locale_packs
                group by
                    cohort_id
            )
            , stream_info AS MATERIALIZED (
                select
                    cohort_id
                    , count(distinct stream_locale_pack_id) filter (where published_cohort_stream_locale_packs.required = true) as required_stream_count
                    , count(distinct stream_locale_pack_id) filter (where published_cohort_stream_locale_packs.elective = true) as elective_stream_count
                from
                    published_cohort_stream_locale_packs
                group by
                    cohort_id
            )
            , with_averages AS MATERIALIZED (
                select
                    with_counts.*

                    -- no entry in the table means you did not participate at all, and get a score of 0
                    , coalesce(user_participation_scores.score, 0) as participation_score
                    , coalesce(cohort_applications_plus.project_score, 0) as project_score

                    , cohort_applications_plus.final_score
                    , cohort_applications_plus.meets_graduation_requirements
                    , required_streams_complete::float / stream_info.required_stream_count as required_streams_perc_complete
                    , required_lessons_complete::float / lesson_info.required_lesson_count as required_lessons_perc_complete
                    , case when lesson_info.elective_lesson_count > 0
                        then elective_lessons_complete::float / lesson_info.elective_lesson_count
                        else null
                        end as elective_lessons_perc_complete
                    , case when stream_info.elective_stream_count > 0
                        then elective_streams_complete::float / stream_info.elective_stream_count
                        else null
                        end as elective_streams_perc_complete
                from with_counts
                    join stream_info
                        on with_counts.cohort_id = stream_info.cohort_id
                    join lesson_info
                        on with_counts.cohort_id = lesson_info.cohort_id
                    left join user_participation_scores
                        on user_participation_scores.user_id = with_counts.user_id
                    left join cohort_applications_plus
                        on cohort_applications_plus.cohort_id = with_counts.cohort_id
                        and cohort_applications_plus.user_id = with_counts.user_id
            )
            select * from with_averages
            with no data
        |

        add_index :cohort_user_progress_records, [:cohort_id, :user_id], :unique => true
    end
end