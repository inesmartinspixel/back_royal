basename = File.basename(__FILE__)
require File.expand_path("./../archive/#{basename}", __FILE__)

class ViewHelpers::PublishedStreamLessonLocalePacks < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    # This one is kind of tricky, and requires some fudging, because lessons are
    # connected to streams, lesson_locale_packs are not connected to stream_locale_packs.
    # That means that, theoretically, the english version of a stream can have a totally
    # different list of lessons than the spanish stream.  In practice, though, we rarely
    # if ever differ on that.  If we did, this table probably wouldn't make sense to even have
    #
    # As it is, it gives one record for each distinct, stream_locale_pack_id/lesson_locale_pack_id
    # in a published stream.  So, if there is an English stream and a Spanish stream that agree
    # on a list of 6 lessons, then there will be 6 rows in this table.  If, however, the two
    # streams disagreed on what the 6th lesson, should be, then you would get 7 records in this
    # table, 5 for the lessons that agree, 1 for the English-only lesson, and 1 for the Spanish-only
    # lesson

    def current
        execute %Q~
            create materialized view published_stream_lesson_locale_packs as
            select
                distinct published_stream_locale_packs.title as stream_title
                , published_lesson_locale_packs.title as lesson_title
                , published_lesson_locale_packs.test as test
                , published_lesson_locale_packs.assessment as assessment
                , published_stream_locale_packs.locale_pack_id as stream_locale_pack_id
                , published_lesson_locale_packs.locale_pack_id as lesson_locale_pack_id
                , published_stream_locale_packs.locales as stream_locales
                , published_lesson_locale_packs.locales as lesson_locales
            from
                published_stream_lessons
                join published_stream_locale_packs on published_stream_locale_packs.locale_pack_id = published_stream_lessons.stream_locale_pack_id
                join published_lesson_locale_packs on published_lesson_locale_packs.locale_pack_id = published_stream_lessons.lesson_locale_pack_id
        ~

        add_index :published_stream_lesson_locale_packs, [:stream_locale_pack_id, :lesson_locale_pack_id], :name => :stream_lp_on_pub_str_lesson_lp, :unique => true
        add_index :published_stream_lesson_locale_packs, :lesson_locale_pack_id, :name => :lesson_lp_on_pub_str_lesson_lp
    end
end