basename = File.basename(__FILE__)
require File.expand_path("./../archive/#{basename}", __FILE__)

class ViewHelpers::HackedAccessGroupsCohorts < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    self.deprecated = true

    # query that returns all of the groups for a cohort, plus ones with
    # the name "#{COHORT_NAME}_EXAM"

    # do nothing.  We don't need this anymore
end