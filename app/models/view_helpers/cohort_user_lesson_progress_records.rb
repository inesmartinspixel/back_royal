basename = File.basename(__FILE__)
require File.expand_path("./../archive/#{basename}", __FILE__)

class ViewHelpers::CohortUserLessonProgressRecords < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    # this used to be a materialzied view but since it has so much
    # data and so little logic, it seemed better to make it a
    # regular view.  Made this change at the same time that we
    # converted user_lesson_progress_records into a table to try
    # to prevent the spikes in cpu associated with refreshing progress views.

    def current
        execute %Q|
            create view cohort_user_lesson_progress_records as
            select
                cohort_applications_plus.cohort_id
                , cohort_applications_plus.cohort_name
                , cohort_applications_plus.user_id as user_id
                , cohort_lesson_locale_packs.required
                , cohort_lesson_locale_packs.assessment
                , cohort_lesson_locale_packs.test
                , cohort_lesson_locale_packs.elective
                , cohort_lesson_locale_packs.foundations
                , cohort_lesson_locale_packs.lesson_title
                , cohort_lesson_locale_packs.lesson_locale_pack_id
                , lesson_progress.started_at
                , lesson_progress.completed_at
                , lesson_progress.total_lesson_time
                , lesson_progress.last_lesson_activity_time
                , lesson_progress.lesson_finish_count
                , lesson_progress.average_assessment_score_first
                , lesson_progress.average_assessment_score_best
                , lesson_progress.official_test_score
                , lesson_progress.total_lesson_time_on_desktop
                , lesson_progress.total_lesson_time_on_mobile_app
                , lesson_progress.total_lesson_time_on_mobile_web
                , lesson_progress.total_lesson_time_on_unknown
                , lesson_progress.completed_on_client_type
                , lesson_progress.lesson_reset_count
                , lesson_progress.last_lesson_reset_at
            from cohort_applications_plus
                join published_cohort_lesson_locale_packs cohort_lesson_locale_packs
                    on cohort_applications_plus.cohort_id = cohort_lesson_locale_packs.cohort_id
                left join user_lesson_progress_records lesson_progress
                    on lesson_progress.locale_pack_id = cohort_lesson_locale_packs.lesson_locale_pack_id
                        and lesson_progress.user_id = cohort_applications_plus.user_id
            where cohort_applications_plus.was_accepted=true
        |

    end
end