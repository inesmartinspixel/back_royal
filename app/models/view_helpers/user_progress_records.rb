basename = File.basename(__FILE__)
require File.expand_path("./../archive/#{basename}", __FILE__)

class ViewHelpers::UserProgressRecords < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    def current
        execute %Q~
            create materialized view user_progress_records as

            with lesson_info AS MATERIALIZED (
                select
                    user_id
                    , count(*) as started_lesson_count
                    , count(case when completed_at is not null then true else null end) as completed_lesson_count
                    , array_agg(locale_pack_id) as started_lesson_locale_pack_ids
                    , array_remove(
                            array_agg(case when completed_at is not null then locale_pack_id else null end)
                            , null
                        ) as completed_lesson_locale_pack_ids
                    , sum(total_lesson_time) as total_lesson_time
                    , max(last_lesson_activity_time) as last_lesson_activity_time
                    , avg(average_assessment_score_first) as average_assessment_score_first
                    , avg(average_assessment_score_best) as average_assessment_score_best
                    , sum(total_lesson_time_on_desktop) as total_lesson_time_on_desktop
                    , sum(total_lesson_time_on_mobile_app) as total_lesson_time_on_mobile_app
                    , sum(total_lesson_time_on_mobile_web) as total_lesson_time_on_mobile_web
                    , sum(total_lesson_time_on_unknown) as total_lesson_time_on_unknown
                from
                    user_lesson_progress_records
                group by
                    user_id
            )
            , stream_info AS MATERIALIZED (
                select
                    user_id
                    , count(*) as started_stream_count
                    , count(case when completed_at is not null then true else null end) as completed_stream_count
                    , array_agg(locale_pack_id) as started_stream_locale_pack_ids
                    , array_remove(
                            array_agg(case when completed_at is not null then locale_pack_id else null end)
                            , null
                        ) as completed_stream_locale_pack_ids
                from
                    lesson_streams_progress
                group by user_id
            )
            , playlist_info AS MATERIALIZED (
                select
                    user_id
                    , count(*) as started_playlist_count
                    , count(case when completed then true else null end) as completed_playlist_count
                    , array_agg(locale_pack_id) as started_playlist_locale_pack_ids
                    , array_remove(
                            array_agg(case when completed then locale_pack_id else null end)
                            , null
                        ) as completed_playlist_locale_pack_ids
                from playlist_progress
                group by user_id
            )
            , user_info AS MATERIALIZED (
                select
                    lesson_info.*
                    , stream_info.started_stream_count
                    , stream_info.completed_stream_count
                    , stream_info.started_stream_locale_pack_ids
                    , stream_info.completed_stream_locale_pack_ids
                    , playlist_info.started_playlist_count
                    , playlist_info.completed_playlist_count
                    , playlist_info.started_playlist_locale_pack_ids
                    , playlist_info.completed_playlist_locale_pack_ids
                from
                    lesson_info
                    left join stream_info
                        on stream_info.user_id = lesson_info.user_id
                    left join playlist_info
                        on playlist_info.user_id = lesson_info.user_id

            )
            select * from user_info
            with no data
        ~

        add_index 'user_progress_records', [:user_id], :unique => true

    end
end