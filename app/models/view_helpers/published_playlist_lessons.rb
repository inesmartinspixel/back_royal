basename = File.basename(__FILE__)
require File.expand_path("./../archive/#{basename}", __FILE__)

class ViewHelpers::PublishedPlaylistLessons < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    # query that returns one row for every published lesson that is in a published
    # playlist, with basic info about both, as well as the stream that links them.
    #
    # This is only used in CohortUserPeriod.write_new_records, so we refresh it in
    # there.  If it was used more widely, we might want to convert it to table using
    # IsDerivedContentTable

    def current
        execute %Q|
            create materialized view published_playlist_lessons as
            select
                published_playlist_streams.*
                , published_stream_lessons.lesson_title
                , published_stream_lessons.lesson_locale_pack_id
                , published_stream_lessons.lesson_version_id
                , published_stream_lessons.lesson_id
                , published_stream_lessons.lesson_locale as lesson_locale
            from published_playlist_streams
                join published_stream_lessons on published_stream_lessons.stream_id = published_playlist_streams.stream_id
            with no data
        |

        add_index :published_playlist_lessons, [:playlist_id, :stream_id, :lesson_id], :name => :unique_on_published_playlist_lessons,  :unique => true
        add_index :published_playlist_lessons, :stream_id
        add_index :published_playlist_lessons, :playlist_locale_pack_id
        add_index :published_playlist_lessons, :stream_locale_pack_id
        add_index :published_playlist_lessons, :lesson_locale_pack_id
    end
end