basename = File.basename(__FILE__)
require File.expand_path("./../archive/#{basename}", __FILE__)

class ViewHelpers::Payments < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    def current
        execute %Q~
            create view payments as
            select
                billing_transactions.*
                , user_id
                , hiring_team_id
            from
                billing_transactions
                left join billing_transactions_users
                    on billing_transactions_users.billing_transaction_id = billing_transactions.id
                left join billing_transactions_hiring_teams
                    on billing_transactions_hiring_teams.billing_transaction_id = billing_transactions.id
            where transaction_type='payment'
        ~

    end

end