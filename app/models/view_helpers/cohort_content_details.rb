basename = File.basename(__FILE__)
require File.expand_path("./../archive/#{basename}", __FILE__)

class ViewHelpers::CohortContentDetails < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    self.deprecated = true

    # query that returns one row for each cohort, with information about the numbers
    # of playlists/streams/lessons of different types


    # do nothing.  renamed to published_cohort_content_details
end