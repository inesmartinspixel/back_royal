basename = File.basename(__FILE__)
require File.expand_path("./../archive/#{basename}", __FILE__)

class ViewHelpers::PublishedPlaylistLocalePacks < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    # returns one row for every published content item locale pack

    def current
        execute %Q~
            create materialized view published_playlist_locale_packs as
            select
                published_playlists.locale_pack_id
                , playlists.title -- we use the working english title in case it is not published in english
                , string_agg(published_playlists.locale, ',') as locales
            from published_playlists
                join playlists on playlists.locale_pack_id = published_playlists.locale_pack_id and playlists.locale='en'
            group by
                published_playlists.locale_pack_id
                , playlists.title
        ~
        add_index :published_playlist_locale_packs, :locale_pack_id, :unique => true
    end
end