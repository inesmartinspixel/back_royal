basename = File.basename(__FILE__)
require File.expand_path("./../archive/#{basename}", __FILE__)

class ViewHelpers::UserCanSeePlaylistLocalePacks < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    def current

        execute %Q~
            create view user_can_see_playlist_locale_packs as

            -- playlists from cohorts
            select
                distinct users.id as user_id
                , unnest(relevant_cohorts.required_playlist_pack_ids || relevant_cohorts.specialization_playlist_pack_ids) as locale_pack_id
            from
                users
                    join users_relevant_cohorts
                        on users_relevant_cohorts.user_id = users.id
                    join published_cohorts relevant_cohorts
                        on relevant_cohorts.id = users_relevant_cohorts.cohort_id

            union

            -- playlists from institutions
            select
                distinct institutions_users.user_id
                , unnest(institutions.playlist_pack_ids) as locale_pack_id
            from institutions_users
                join institutions on institutions.id = institutions_users.institution_id

        ~
    end
end