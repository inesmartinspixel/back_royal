basename = File.basename(__FILE__)
require File.expand_path("./../archive/#{basename}", __FILE__)

class ViewHelpers::InstitutionStreamLocalePacks < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    def current
        execute %Q~
            create materialized view institution_stream_locale_packs as
            with streams_from_groups AS MATERIALIZED (
                select
                    distinct institutions.id as institution_id
                    , locale_pack_id as stream_locale_pack_id
                from
                    institutions
                    left join access_groups_institutions on access_groups_institutions.institution_id = institutions.id
                    left join access_groups_lesson_stream_locale_packs on access_groups_lesson_stream_locale_packs.access_group_id = access_groups_institutions.access_group_id
            )
            , streams_from_playlists AS MATERIALIZED (
                select
                    distinct institutions.id as institution_id
                    , published_playlist_streams.stream_locale_pack_id as stream_locale_pack_id
                from
                    institutions
                    join institution_playlist_locale_packs
                        on institutions.id = institution_playlist_locale_packs.institution_id
                    join published_playlist_streams
                        on published_playlist_streams.playlist_locale_pack_id = institution_playlist_locale_packs.playlist_locale_pack_id
            )
            , all_stream_locale_pack_ids AS MATERIALIZED (
                select * from streams_from_groups
                union select * from streams_from_playlists
            )
            select
                institutions.id as institution_id
                , institutions.name as institution_name
                , streams_from_playlists is not null as in_playlist
                , published_stream_locale_packs.exam as exam

                , published_stream_locale_packs.title as stream_title
                , published_stream_locale_packs.locale_pack_id as stream_locale_pack_id
                , published_stream_locale_packs.locales as stream_locales
            from
                all_stream_locale_pack_ids
                join institutions
                    on institutions.id = all_stream_locale_pack_ids.institution_id
                join published_stream_locale_packs
                    on published_stream_locale_packs.locale_pack_id = all_stream_locale_pack_ids.stream_locale_pack_id
                left join published_playlist_streams
                    on published_playlist_streams.stream_locale_pack_id = published_stream_locale_packs.locale_pack_id
                left join streams_from_playlists on
                    institutions.id = streams_from_playlists.institution_id
                    and streams_from_playlists.stream_locale_pack_id = all_stream_locale_pack_ids.stream_locale_pack_id
            group by
                institutions.id
                , institutions.name
                , published_stream_locale_packs.title
                , published_stream_locale_packs.locale_pack_id
                , published_stream_locale_packs.locales
                , published_stream_locale_packs.exam
                , streams_from_playlists.*
        ~

        add_index :institution_stream_locale_packs, [:institution_id, :stream_locale_pack_id], :unique => true, :name => :inst_and_str_lp_on_inst_str_lps
        add_index :institution_stream_locale_packs, :stream_locale_pack_id, :name => :str_lp_on_inst_stream_lps
    end
end