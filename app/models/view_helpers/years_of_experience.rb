basename = File.basename(__FILE__)
require File.expand_path("./../archive/#{basename}", __FILE__)

class ViewHelpers::YearsOfExperience < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    self.deprecated = true

    # Do nothing.  The information here was folded into career_profile_search_helpers

end