basename = File.basename(__FILE__)
require File.expand_path("./../archive/#{basename}", __FILE__)

class ViewHelpers::CohortLessonLocalePacks < ViewHelpers::ViewHelperBase
	# -*- SkipSchemaAnnotations

    self.deprecated = true

    # query that returns one row for each lesson that is available to
    # members of a cohort, (see criteria above in cohort_stream)

    # do nothing.  renamed to published_cohort_lesson_locale_packs

end