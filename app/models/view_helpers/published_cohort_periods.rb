basename = File.basename(__FILE__)
require File.expand_path("./../archive/#{basename}", __FILE__)

class ViewHelpers::PublishedCohortPeriods < ViewHelpers::ViewHelperBase
    # -*- SkipSchemaAnnotations

    self.deprecated = true

    # do nothing.  converted to table

end