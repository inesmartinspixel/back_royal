# == Schema Information
#
# Table name: cohort_status_changes
#
#  user_id                   :uuid
#  cohort_id                 :uuid
#  from_time                 :datetime
#  until_time                :datetime
#  status                    :text
#  cohort_application_status :text
#  graduation_status         :text
#  id                        :uuid             not null, primary key
#  was_enrolled              :boolean          not null
#  cohort_application_id     :uuid             not null
#

class CohortStatusChange < ApplicationRecord
    attr_accessor :cohort # this should be an association, but that doesn't work well with how we want to set up SuccessPredictor

    # This code relies on the assumption that versions are never added in
    # with older version_created_at values after this code has run.  We can
    # be sure that this assumption holds true even if an application is saved by
    # different processes at the same time because this code is run in a
    # debounced job
    def self.update_cohort_status_changes_for_application(cohort_application_id)

        cohort_application = CohortApplication.select(:cohort_id, :user_id, :id).find_by_id(cohort_application_id)

        # If the cohort application has been destroyed, then cohort_status_changes will have been
        # deleted in an after_destroy callback on the application.  We don't need to do anything.
        if (cohort_application.nil?)
            return
        end

        last_cohort_status_change ||= CohortStatusChange
            .where(cohort_application_id: cohort_application_id)
            .reorder(:from_time)
            .last

        # this relies on the versions being ordered correctly
        # in the association
        unprocessed_versions = cohort_application.versions
            .select(:id, :version_id, :status, :graduation_status, :version_created_at, :updated_at, :user_id, :cohort_id)
            .where.not(status: 'pending')
            .where("version_created_at >= ?", last_cohort_status_change&.from_time || Time.at(0)).to_a

        enrollment_deadline = cohort_application.published_cohort.enrollment_deadline

        # triggers is an array of things that can create cohort_status_changes, ordered
        # consecutively.
        # There are two such things: a cohort application version or an enrollment deadline,
        # which is encoded in the array as the symbol :enrollment_deadline
        triggers = unprocessed_versions
        processed_up_to = last_cohort_status_change ? last_cohort_status_change.from_time : Time.at(0)
        if enrollment_deadline && enrollment_deadline > processed_up_to && enrollment_deadline < Time.now
            index = triggers.index { |version| version.version_created_at > enrollment_deadline }
            index = triggers.size unless index
            triggers.insert(index, :enrollment_deadline)
        end

        cohort_status_changes = [last_cohort_status_change].compact

        last_version = nil
        triggers.each do |trigger|

            # if the user is accepted at the enrollment deadline, then
            # the status changes to "enrolled"
            if trigger == :enrollment_deadline && last_version&.status == 'accepted'
                attrs = last_version.get_cohort_status_change_attrs_at_enrollment_deadline(cohort_status_changes.last)

            # if the user is not accepted at the enrollment deadline, then nothing
            # changes
            elsif trigger == :enrollment_deadline
                next

            # otherwise we're looking at a version, and we process it
            else
                version = trigger
                attrs = version.get_cohort_status_change_attrs(cohort_status_changes.last)
                last_version = version

                # not every version results in creation of a cohort_status_change
                # see CohortStatusChangeMixin#get_status_for_cohort_status_change
                next unless attrs.present?
            end

            last_cohort_status_change, new_cohort_status_change = append_cohort_status_change(attrs, cohort_status_changes.last)

            if new_cohort_status_change
                cohort_status_changes << new_cohort_status_change
            end
        end

        RetriableTransaction.transaction do
            cohort_status_changes.each(&:save!)
        end
    end

    def self.append_cohort_status_change(attrs, last_cohort_status_change)

        keys = [:graduation_status, :status, :cohort_application_status, :cohort_id]

        # if there are no changes, return
        if last_cohort_status_change&.attributes&.symbolize_keys&.slice(*keys) == attrs.slice(*keys)
            return [last_cohort_status_change, nil]
        end

        # If the from_time values are equal, it means we have two versions
        # with the same version_created_at, which in turn means that
        # they were saved in the same transaction.  We only want to
        # save a single cohort_status_change in that case, relying on the status of the
        # last version.  We know we can replace the existing record with the
        # attrs coming in because we are ordering the versions by [version_created_at, updated_at]
        # (see sort order when loading versions above, and explanation above <=>
        # in VersionMixin)
        if last_cohort_status_change&.from_time == attrs[:from_time]
            last_cohort_status_change&.assign_attributes(attrs)
            new_cohort_status_change = nil

        # if we are adding a new cohort_status_change, then we update the until_time
        # on the previous one to be the from_time on this one (which is the same as
        # the version_created_at)
        else

            if last_cohort_status_change

                # I saw this situation once when making changes in here.  I think that
                # it can't happen anymore now that we're loading the last_cohort_change
                # by last_cohort_status_change, but just to be sure
                # I'm leaving this here for now.  Probably fine to remove if we don't
                # hit it after a round of acceptances
                if last_cohort_status_change.until_time < attrs[:from_time]
                    raise "Unexpected until_time"
                end
                last_cohort_status_change.assign_attributes(until_time: attrs[:from_time])
            end
            new_cohort_status_change = CohortStatusChange.new(attrs)
        end

        return [last_cohort_status_change, new_cohort_status_change]
    end

end
