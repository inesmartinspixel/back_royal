# == Schema Information
#
# Table name: lessons
#
#  id                 :uuid             not null, primary key
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  content_json       :json
#  author_id          :uuid
#  title              :string(255)
#  description        :text             default([]), is an Array
#  modified_at        :datetime
#  archived           :boolean          default(FALSE), not null
#  pinned             :boolean          default(FALSE), not null
#  pinned_title       :string(255)
#  pinned_description :text
#  was_published      :boolean          default(FALSE), not null
#  last_editor_id     :uuid
#  seo_title          :string
#  seo_description    :string
#  seo_canonical_url  :text
#  seo_image_id       :uuid
#  lesson_type        :string
#  frame_count        :integer
#  key_terms          :string           default([]), not null, is an Array
#  entity_metadata_id :uuid
#  assessment         :boolean          default(FALSE)
#  unrestricted       :boolean          default(FALSE)
#  tag                :text
#  locale_pack_id     :uuid
#  locale             :text             default("en"), not null
#  duplicated_from_id :uuid
#  duplicated_to_id   :uuid
#  test               :boolean          default(FALSE), not null
#

class Lesson < ApplicationRecord
    include Skylight::Helpers
    include IsContentItem
    include HasFulltextSupport


    default_scope -> {order(:created_at)}

    resourcify # access controlled by roles set in app/models/ability.rb (todo: move to IsContentItem?)
    validates_presence_of :frame_count, if: :has_frames?
    validate :validate_pinned, if: :was_published?
    before_validation :reconcile_content_json
    after_validation :validate_content
    after_validation :validate_frame_count
    after_validation :validate_key_terms
    before_validation :do_work_if_archiving
    has_many :to_stream_joins, class_name: 'Lesson::ToStreamJoin', dependent: :destroy
    has_many :lesson_streams_with_just_titles, -> { select('id', 'title') }, through: :to_stream_joins, class_name: 'Lesson::Stream', source: :stream
    has_many :lesson_permission_roles, class_name: 'Role', foreign_key: :resource_id, dependent: :destroy
    belongs_to :entity_metadata
    belongs_to :locale_pack, class_name: Lesson::LocalePack.name, foreign_key: :locale_pack_id, optional: true

    # it sucks that we can't set this up through active_record, but there is no
    # belongs_to => through.  There is a has_one through, but seems silly to
    # change the data model around just for active record's sake.
    delegate :practice_locale_pack, to: :locale_pack

    def self.test_lesson_locale_pack_id?(locale_pack_id)
        test_and_assessment_locale_pack_ids[:test].include?(locale_pack_id)
    end

    def self.assessment_lesson_locale_pack_id?(locale_pack_id)
        test_and_assessment_locale_pack_ids[:assessment].include?(locale_pack_id)
    end

    private_class_method def self.test_and_assessment_locale_pack_ids
        content_views_refresh_updated_at = ContentViewsRefresh.debounced_value

        SafeCache.fetch("test_and_assessment_locale_pack_ids-#{content_views_refresh_updated_at}") do
            results = Lesson::Version.published_versions.where("test or assessment").pluck('locale_pack_id', 'test', 'assessment')
            test_ids = results.map { |row| row[1] ? row[0] : nil }.compact
            assessment_ids = results.map { |row| row[2] ? row[0] : nil }.compact
            {
                test: Set.new(test_ids),
                assessment: Set.new(assessment_ids)
            }
        end
    end

    def self.prepare_params_for_duplication(hash_for_original_item, hash_for_new_item, user, meta)
        hash_for_original_item.except('archived')
    end

    def self.progress_class
        LessonProgress
    end

    def self.published_titles_in_a_stream
        ActiveSupport::JSON.decode(Lesson::ToJsonFromApiParams.new({
            fields: [:title, :stream_titles],
            filters: {
                :published => true,
                :archived => false
            }
        }).json).reject {|l| l['stream_titles'].length <= 0 }.map {|l| l['title']}.sort
    end

    def self.all_published_and_in_a_stream
        # seems like neither the ordering from our has_many or the default_scope is applied when using joins.
        # here is an issue that is at least related, but it's old: https://rails.lighthouseapp.com/projects/8994/tickets/3610-has_many-through-associations-may-not-respect-default_scope-conditions
        Lesson::Stream.all_published.joins(:lessons).includes(:lessons).order("#{Lesson::ToStreamJoin.table_name}.position").map(&:lessons).flatten.uniq.select(&:has_published_version?)
    end

    # override is_content_item to do lesson-specific overrides
    def self.create_from_hash!(hash, meta = {})
        # Automatically retries in the event of ActiveRecord::PreparedStatementCacheExpired
        RetriableTransaction.transaction do
            if meta['is_practice_for_locale_pack_id']
                locale_pack = Lesson::LocalePack.find(meta['is_practice_for_locale_pack_id'])
                if locale_pack.practice_locale_pack.nil?
                    locale_pack.practice_locale_pack = Lesson::LocalePack.create!
                    locale_pack.save!
                end

                hash['locale_pack'] = locale_pack.practice_locale_pack.as_json
            end

            instance = super(hash, meta)

            # Additionally give the author editor role on this lesson
            instance.author.add_role(:editor, instance)

            instance
        end
    end

    def self.image_formats
        unless defined? @image_formats
            base_sizes = [
                # main image
                [700, 275],
                [510, 200],

                # inline
                [84, 33],

                # buttons
                [408, 160],
                [293, 115],

                # this or that
                [573, 225],
                [344, 135]
            ]

            formats = {}
            base_sizes.each do |base_size|
                [1,2,3].each do |x|
                    format = base_size.map { |n| n*x }.map(&:to_s).join('x')
                    formats[format] = "#{format}>"
                end

            end

            @image_formats = formats
        end
        @image_formats
    end

    module OldVersionSupport
        def update_from_hash!(user, hash, meta = {})
            hash = hash.unsafe_with_indifferent_access
            meta ||= {}
            meta = meta.unsafe_with_indifferent_access
            if meta[:update_version]
                instance = Lesson::Version.find_by_version_id(meta[:update_version])

                # this normally happens in is_content_item.rb::update_from_hash!, but
                # we're skipping that
                instance.title = hash['title'] if hash.key?('title')
                instance.merge_hash(hash)

                instance.commit_changes

                # have to reload so we get a saved version
                return Lesson::Version.find_by_version_id(meta[:update_version])
            else
                super(user, hash, meta)
            end
        end
    end

    class << self
        prepend OldVersionSupport
    end

    # merge in a hash with json versions of author, image, and lessons
    def merge_hash(hash, meta = {})
        hash = hash.clone.with_indifferent_access

        content_hash = hash.slice('frames', 'lesson_type')

        # If we don't have any content already, and neither
        # lesson_type nor frames is set, then don't create content.
        # This simplifies things in tests, since we don't have
        # to worry about creating valid content
        if content_hash.any? && (content_hash.values.any? || self.has_content?)

            # we want to do a merge here in case an incomplete object is being sent up from the client.  For example, a lesson
            # can be save from the list_lessons directive, but those lessons never loaded up frames.  So when they send a hash
            # up in the update call, that hash has no 'frames' key.  We don't want to blow away all the frames in that case, we
            # just want to leave that untouched.  So we merge
            self.content ||= {lesson_type: content_hash['lesson_type']}
            self.content.merge_hash(content_hash)
        end

        %w(lesson_type frame_count description key_terms assessment unrestricted tag test).each do |key|
            if hash.key?(key)
                val =  hash[key]
                val = nil if val.is_a?(String) && val.blank?
                self[key] = val
            end
        end
    end

    def content_json=(value)
        self.content = value
        write_attribute('content_json', value)
    end

    # overridden in Lesson::Version
    def old_version
        false
    end

    def validate_content
        if content && !content.valid?
            content.errors.each do |attr, err|
                errors.add(attr, err)
            end
        end

        if has_content? && content.lesson_type != self.lesson_type
            errors.add(:lesson_type, "must match content.lesson_type")
        end
    end

    def allow_unrestricted_access?
        return true if unrestricted?
        return true if self.id == ENV['DEMO_LESSON_ID']

        return false
    end

    def streams
        Lesson::ToStreamJoin.streams_for_lesson(self)
    end

    def published_streams
        Lesson::ToStreamJoin.streams_for_lesson(self).select(&:has_published_version?)
    end

    def published_and_in_a_stream?
        has_published_version? && published_streams.any?
    end

    def content
        if @content.nil? && !content_json.nil?
            self.content = self.content_json
        end
        @content
    end

    # this is only necessary for old versions, because there is no
    # Lesson::FrameList::Version that descends from Lesson::FrameList
    def content_klass
        if lesson_type == 'frame_list'
            Content::FrameList
        else
            Content
        end
    end

    def content=(attrs)
        if attrs.nil?
            @content = nil
        else
            @content = attrs.is_a?(Content) ? attrs : content_klass.load(ActiveSupport::HashWithIndifferentAccess.new(attrs))
            @content.lesson = self
        end
        @content
    end

    # if @content has not been created yet, then the content
    # cannot have changed.  This saves time
    def content_changed?
        @content && content.changed?
    end

    instrument_method
    def self.get_published_versions(lesson_ids)

        # run one query to get all the published lessons from content_publishers
        content_publishers = ContentPublisher.
            joins(:lesson).
            where('lessons.id in (?)', lesson_ids).
            includes('published_lesson_version')

        # turn the serialized published lessons back into objects
        lessons = {}
        content_publishers.each do |content_publisher|
            lesson = content_publisher.published_version
            lessons[lesson.attributes['id']] = lesson
        end

        lessons
    end

    instrument_method
    def as_json(options = {})
        raise "Cannot as_json an unsaved lesson" if changed?
        options = options.with_indifferent_access
        options[:filters] ||= {}
        raise ArgumentError.new('Cannot set published when as_jsonning a particular lesson instance') if options.key?(:published) || options[:filters].key?(:published)
        # If this is a lesson version, return the
        # json for this version
        if self.respond_to?(:version_id)
            options.deep_merge!({
                filters: {
                    version_id: self.version_id
                }
            })

        # If this is a Lesson, not a Lesson::Version,
        # return the working (most recent) version
        else
            options.deep_merge!({
                filters: {
                    id: self.id,
                    published: false
                }
            })
        end

        json = Lesson::ToJsonFromApiParams.new(options).json
        ActiveSupport::JSON.decode(json).first
    end

    def save_warnings
        content ? content.save_warnings : []
    end

    def image_formats
        self.class.image_formats
    end

    def get_image_asset(file)
        S3Asset.new({
            :file => file,
            :directory => "images/",

            # see also: https://github.com/thoughtbot/paperclip/wiki/Thumbnail-Generation
            # see also: image_model.js
            :styles => image_formats.to_json
        })
    end

    instrument_method
    def reconcile_content_json
        self.content_json = content.nil? ? nil : content.as_json
    end

    def remove_from_all_streams
        self.streams.compact.each do |stream|
            stream.remove_lesson!(self)
        end
    end

    def do_work_if_archiving
        if attribute_changed?(:archived) && archived?
            unpublish!
            remove_from_all_streams
        end
    end

    def get_version(version_id)
        version = versions.where(version_id: version_id).first
        return nil unless version
        version.old_version = (version.updated_at != self.updated_at)
        version
    end

    def has_content?
        # an empty hash should not count as content. For some
        # reason we cannot save a nil value into the database
        !self.content_json.nil?
    end

    def frame_list?
        self.content_json && self.content_json['lesson_type'] == 'frame_list'
    end

    def frames
        self.frame_list? ? self.content.frames : nil
    end

    def has_frames?
        !frames.nil?
    end

    def validate_frame_count
        has_err = false
        if has_frames? && (frame_count.nil? || frame_count != self.frames.size)
            has_err = true
        elsif !has_frames? && !frame_count.nil?
            has_err = true
        end

        if has_err
            expected_frame_count = self.frames ? self.frames.size : nil
            errors.add(:frame_count, "must match the number of frames frame_count(#{self.frame_count.inspect}) != frames.size(#{expected_frame_count.inspect})")
        end
    end

    def validate_key_terms
        dupes = key_terms.select{ |term| key_terms.count(term) > 1 }.uniq
        if dupes.any?
            errors.add(:key_terms, "must have no duplicates. (#{dupes.inspect}")
        end
    end

    def should_update_fulltext?
        !!(
            saved_changes[:title] ||
            saved_changes[:description] ||
            saved_changes[:content_json]
        )
    end

    instrument_method
    def update_fulltext

        # get and sanitize relevant strings
        title_text = self.class.sanitize_fulltext_sql(self.title)
        description_text = self.class.sanitize_fulltext_sql(self.description.join(' '))

        # iterate over frames
        if self.content_json && self.content_json['frames']

            # get content values
            content_text = self.content_json['frames'].
                map { |f| f['components'] }.flatten.compact.
                select { |c| c['component_type'] == 'ComponentizedFrame.Text' }.
                map { |c| c['text'] }.join(' ')
            content_text = self.class.sanitize_fulltext_sql(content_text)

            content_full_text = self.class.sanitize_fulltext_sql([title_text, description_text, content_text].join(' '))

            # get comment values
            comments_text = self.content_json['frames'].
                map { |f| f['author_comments'] }.flatten.compact.
                map { |c| c['text'] }.join(' ')
            comments_text = self.class.sanitize_fulltext_sql(comments_text)

        end

        # defaults
        content_text        ||= 'NULL'
        comments_text       ||= 'NULL'
        content_full_text   ||= 'NULL'

        # build out sql snippets
        content_sql = "setweight(to_tsvector('english', coalesce(#{title_text},'')), 'A') ||
                       setweight(to_tsvector('english', coalesce(#{description_text},'')), 'B') ||
                       setweight(to_tsvector('english', coalesce(#{content_text},'')), 'C')"

        comments_sql = "setweight(to_tsvector('english', coalesce(#{comments_text},'')), 'A')"

        # TODO: upsert would be nice here
        if ActiveRecord::Base.connection.execute("SELECT 1 FROM lesson_fulltext WHERE lesson_id='#{self.id}'").count == 0
            sql = "INSERT INTO lesson_fulltext (lesson_id, content_vector, comments_vector, content_text, comments_text) VALUES ('#{self.id}', #{content_sql}, #{comments_sql}, #{content_full_text}, #{comments_text})"
        else
            sql = "UPDATE lesson_fulltext SET content_vector = #{content_sql}, comments_vector = #{comments_sql}, content_text = #{content_full_text}, comments_text = #{comments_text} WHERE lesson_id = '#{self.id}'"
        end

        # update or insert the fulltext
        ActiveRecord::Base.connection.execute(sql)
    end


    def validate_pinned
        unless self.pinned?
            errors.add(:pinned, "must be true")
        end
    end

    def editor_link(site = 'https://smart.ly')
        "#{site}/editor/lesson/#{id}/edit"
    end

    def practice_lesson_id
        practice_locale_pack && practice_locale_pack.content_items.where(locale: locale).ids.first
    end

    def practice_lesson
        if practice_lesson_id.nil?
            return nil
        end

        unless defined?(@practice_lesson) && @practice_lesson.id == practice_lesson_id
            @practice_lesson = Lesson.find(practice_lesson_id)
        end

        @practice_lesson
    end

    def is_practice?
        self.locale_pack_id && Lesson::LocalePack.where(practice_locale_pack_id: self.locale_pack_id).exists?
    end
end
