require 'back_royal/object' # looks_like_uuid?

class Playlist::ToJsonFromApiParams

    include ToJsonFromApiParamsHelpers::ToJsonFromApiParamsMixin
    include ToJsonFromApiParamsHelpers::ContentItemMixin

    self.base_klass = Playlist
    attr_reader :user_id

    def initialize(params)

        params = do_basic_params_prep(params)

        @query_params = params.slice(
            :id,
            :user,
            :limit,
            :fields,
            :order
        ).with_indifferent_access

        set_filters({
            # Note: We do not support view_as like we do with other content items here because we realized
            # that we were only ever getting user-facing playlists through the student_dashboards_controller,
            # so we instead handle any view_as logic around playlists over there.

            id: :array_of_uuids,
            locale_pack_id: :array_of_uuids,
            version_id: :array_of_uuids,
            in_users_locale_or_en: :bool,
            in_locale_or_en: :locale,
            published: :bool,
            complete: :bool,
            locale: :locale,
            updated_since: :number,
            updated_before: :number
        })

        fields = set_fields({
            available_fields: %w|
                id version_id title description modified_at updated_at
                author image entity_metadata
                was_published published_at published_version_id
                stream_entries percent_complete courses_complete_count locale
                locale_pack tag
                related_cohort_names programs_included_in
            |,
            aliases: {
                UPDATE_FIELDS: ['updated_at', 'published_at', 'modified_at']

            }
        })

        @user = @query_params[:user]
        @user_id = @user.id unless !@user

        @query_builder = CrazyQueryBuilder.new('playlists_versions')
        @selects = @query_builder.selects
        @joins = @query_builder.joins
        @wheres = @query_builder.wheres
        @withs = @query_builder.withs
        @query_builder.limit = @limit = @query_params[:limit]

        handle_content_item_filters
        add_required_playlist_pack_ids_cte
        set_default_selects
        handle_content_item_selects

        if @user_id.nil?
            fields.delete('percent_complete')
            fields.delete('courses_complete_count')
        end

        if fields.include?('percent_complete')
            select_percent_complete
        end

        if fields.include?('courses_complete_count')
            select_courses_complete_count
        end

        if filters.key?(:complete)
            filter_by_complete
        end

        @selects.slice!(*fields)

        if @selects.empty?
            raise "No selects provided. params: #{@query_params.inspect}"
        end
    end

    # NOTE: Currently, this is the only place where we need to add these CTEs
    # to ensure that the required_playlist_pack_ids are properly collected. If
    # we ever need this logic elsewhere, we should make sure to DRY it up in a
    # mixin or something, so it can be resued consistently.
    private
    def add_required_playlist_pack_ids_cte
        @withs << "cohort_playlist_collections AS MATERIALIZED (
            SELECT
                cohorts.id AS cohort_id
                , CASE WHEN playlist_collection IS NULL THEN '{}'::JSON
                    ELSE playlist_collection
                END AS playlist_collection
            FROM cohorts
                -- This LEFT JOIN against the UNNESTed playlist_collections array ensures that a
                -- row is returned for every cohort even if its playlist_collections array is empty.
                -- Practically, this isn't a problem, but it's helpful in our specs.
                LEFT JOIN LATERAL UNNEST(cohorts.playlist_collections) playlist_collection ON TRUE
        )"
        @withs << "cohort_playlist_collection_required_playlist_pack_ids AS MATERIALIZED (
            SELECT
                cohort_playlist_collections.cohort_id
                , ARRAY(SELECT required_playlist_pack_id FROM JSON_ARRAY_ELEMENTS_TEXT(playlist_collection->'required_playlist_pack_ids') required_playlist_pack_id) AS required_playlist_pack_ids
            FROM cohort_playlist_collections
        )"
        @withs << "cohort_required_playlist_pack_ids AS MATERIALIZED (
            SELECT
                cohort_playlist_collection_required_playlist_pack_ids.cohort_id
                , ARRAY_CAT_AGG(cohort_playlist_collection_required_playlist_pack_ids.required_playlist_pack_ids)::uuid[] AS required_playlist_pack_ids
            FROM cohort_playlist_collection_required_playlist_pack_ids
            GROUP BY cohort_playlist_collection_required_playlist_pack_ids.cohort_id
        )"
    end

    private
    def set_default_selects
        @selects['id'] = 'playlists_versions.id'
        @selects['version_id'] = 'playlists_versions.version_id'
        @selects['title'] = 'playlists_versions.title'
        @selects['description'] = 'playlists_versions.description'
        @selects['modified_at'] = 'cast(EXTRACT(EPOCH FROM playlists_versions.modified_at) as int)'
        @selects['updated_at'] = 'cast(EXTRACT(EPOCH FROM playlists_versions.updated_at) as int)'
        @selects['stream_entries'] = 'playlists_versions.stream_entries'
        @selects['was_published'] = 'playlists_versions.was_published'
        @selects['locale'] = 'playlists_versions.locale'
        @selects['tag'] = 'playlists_versions.tag'

        select_author
        join_and_select_image
        join_and_select_entity_metadata
        select_related_cohort_names
        select_programs_included_in

    end

    private
    def select_related_cohort_names
        @selects['related_cohort_names'] = %q|
            (
            SELECT array_agg(name)
            FROM cohorts
                JOIN cohort_required_playlist_pack_ids
                    ON cohort_required_playlist_pack_ids.cohort_id = cohorts.id
            WHERE playlists.locale_pack_id = ANY(cohort_required_playlist_pack_ids.required_playlist_pack_ids)
                OR playlists.locale_pack_id = ANY(cohorts.specialization_playlist_pack_ids)
            )
        |
    end

    def select_programs_included_in
        @selects['programs_included_in'] = %q|
            (
                SELECT array_agg(published_cohorts.program_type)
                FROM published_cohort_playlist_locale_packs
                JOIN published_cohorts
                    ON published_cohorts.id = published_cohort_playlist_locale_packs.cohort_id
                    AND published_cohorts.promoted = true
                WHERE published_cohort_playlist_locale_packs.playlist_locale_pack_id = playlists.locale_pack_id
                GROUP BY playlist_locale_pack_id
            )
        |
    end

    def select_percent_complete
        @selects['percent_complete'] = "(
            select count(*)
            from lesson_streams_progress
            where completed_at is not null
                and user_id='#{@user_id}'
                and locale_pack_id in (select (unnest(playlists_versions.stream_entries)->>'locale_pack_id')::uuid)
        ) / array_length(playlists_versions.stream_entries, 1)::float"
    end

    def select_courses_complete_count
        @selects['courses_complete_count'] = "(
            select count(*)
            from lesson_streams_progress
            where completed_at is not null
                and user_id='#{@user_id}'
                and locale_pack_id in (select (unnest(playlists_versions.stream_entries)->>'locale_pack_id')::uuid)
        )"
    end

    def filter_by_complete
        raise ArgumentError.new("Can only filter by complete=false") unless filters[:complete] == false
        raise ArgumentError.new("Cannot filter by complete without a user_id") unless @user_id

        # filter out completed playlists
        @wheres << "
            exists(
                select streams_in_playlist.locale_pack_id
                from
                    (select (unnest(playlists_versions.stream_entries)->>'locale_pack_id')::uuid as locale_pack_id) as streams_in_playlist
                    left join lesson_streams_progress
                        on lesson_streams_progress.locale_pack_id = streams_in_playlist.locale_pack_id
                        and lesson_streams_progress.user_id='#{@user_id}'
                where lesson_streams_progress.completed_at is null
                limit 1
            )
        "
    end

    def execute
        result = ActiveRecord::Base.connection.execute(@query_builder.to_json_sql)
        json = result.to_a[0]["json"] || "[]"
        json
    end

end
