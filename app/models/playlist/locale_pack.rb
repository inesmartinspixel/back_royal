# == Schema Information
#
# Table name: playlist_locale_packs
#
#  id         :uuid             not null, primary key
#  created_at :datetime
#  updated_at :datetime
#

class Playlist::LocalePack < ApplicationRecord
    include LocalePackMixin
    include Groupable

    set_content_item_klass(Playlist)

    def published_versions_lesson_stream_locale_pack_ids
        content_items.map(&:published_version).compact.map(&:stream_locale_pack_ids).flatten.uniq
    end
end
