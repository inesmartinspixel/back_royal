# == Schema Information
#
# Table name: playlists_versions
#
#  version_id          :uuid             not null, primary key
#  operation           :string(1)        not null
#  version_created_at  :datetime         not null
#  id                  :uuid             not null
#  title               :text             not null
#  description         :text
#  stream_entries      :json             is an Array
#  image_id            :uuid
#  entity_metadata_id  :uuid
#  author_id           :uuid
#  last_editor_id      :uuid
#  modified_at         :datetime
#  was_published       :boolean          not null
#  created_at          :datetime
#  updated_at          :datetime
#  locale_pack_id      :uuid
#  locale              :text
#  duplicated_from_id  :uuid
#  duplicated_to_id    :uuid
#  tag                 :text
#  version_editor_id   :uuid
#  version_editor_name :text
#

class Playlist::Version < Playlist

    include(ContentItemVersionMixin)

    self.primary_key = 'version_id'
    self.table_name = 'playlists_versions'

    belongs_to :content_publisher, :class_name => 'ContentPublisher', :primary_key => :playlist_version_id, :foreign_key => :version_id
    belongs_to :playlist, :foreign_key => 'id', :primary_key => 'id' , :class_name => 'Playlist'
    delegate :published_at, :published_version_id, :to => :playlist

end
