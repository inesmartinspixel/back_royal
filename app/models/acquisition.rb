# == Schema Information
#
# Table name: acquisitions
#
#  user_id             :uuid             primary key
#  acquired_at         :datetime
#  acquired_on_cordova :boolean
#  registered          :boolean
#  did_not_bounce      :boolean
#

class Acquisition < ActiveRecord::Base

    self.primary_key = :user_id

    belongs_to :user
    has_one :user_progress_record, :foreign_key => :user_id, :primary_key => :user_id, :class_name => "Report::UserProgressRecord"


end
