class VersionMigrator

    def initialize(klass)
        @klass = klass
        @version_klass = @klass::Version
        version_table_name = @version_klass.table_name
        @updatable_version_klass = Class.new(ActiveRecord::Base) do
            self.table_name = version_table_name
            self.primary_key = "version_id"
        end

        @queries = {
            @klass => {
                list: @klass.where(""),
                count: @klass.where("")
            },

            @version_klass => {
                list: @version_klass.where("")
            },

            # see comment below around find_in_batches about why
            # we use the updatable_version_klass here
            @updatable_version_klass => {
                list: @updatable_version_klass.where(""),
                count: @updatable_version_klass.where("")
            }
        }
    end

    def method_missing(meth, *args, &block)
        if @queries[@klass][:list].respond_to?(meth)

            if meth != :select
                @queries[@klass][:count] = @queries[@klass][:count].send(meth, *args)
                @queries[@updatable_version_klass][:count] = @queries[@updatable_version_klass][:count].send(meth, *args)

            else

                # we need special handling for select because
                # 1. it messes up count queries
                # 2. we need to be smart about adding id, version_id
                @queries[@klass][:list] = @queries[@klass][:list].send(meth, *(args + ['id']))
                @queries[@version_klass][:list] = @queries[@version_klass][:list].send(meth, *(args + ['id', 'version_id', 'operation']))
                @queries[@updatable_version_klass][:list] = @queries[@updatable_version_klass][:list].send(meth, *(args + ['id', 'version_id', 'operation']))
            end
            return self
        end

        super
    end

    def each(&block)
        @klass.connection.execute("alter table #{@klass.table_name} disable trigger #{@klass.table_name}_versions")

        last_puts = Time.now
        puts "Counting #{@klass.table_name}"
        record_ids = @queries[@klass][:count].pluck('id')
        remaining_ids = record_ids.clone
        puts "Migrating #{record_ids.size} #{@klass.table_name}"
        i = 0

        while remaining_ids.any?
            @queries[@klass][:list].where(id: remaining_ids.slice!(0, 20)).each do |instance|
                if Time.now - last_puts > 3.seconds
                    puts "Migrated #{i} of #{record_ids.size} #{@klass.table_name}"
                    last_puts = Time.now
                end
                result = yield(instance)
                i = i + 1

                if result == :skip
                    next
                end
                instance.reconcile_content_json if instance.has_attribute?('content_json')

                new_values = instance.attributes.slice(*instance.changes.keys)
                instance.update_columns(new_values) unless new_values.blank?

            end
        end



        puts "Counting #{@version_klass.table_name}"
        version_ids = @queries[@updatable_version_klass][:count].pluck('version_id')
        remaining_version_ids = version_ids.clone
        puts "Migrating #{version_ids.size} #{@version_klass.table_name}"
        i = 0

        # it's a bit awkward, but find_in_batches does not work on our
        # version classes because of the id nonsense, so we have to use
        # the updatable class
        while remaining_version_ids.any?
            @queries[@updatable_version_klass][:list].where(version_id: remaining_version_ids.slice!(0, 20)).each do |updatable_instance|

                instance = @queries[@version_klass][:list].where(version_id: updatable_instance.version_id).first
                if Time.now - last_puts > 3.seconds
                    puts "Migrated #{i} of #{version_ids.size} #{@version_klass.table_name}"
                    last_puts = Time.now
                end
                i = i + 1

                next if instance.operation == 'D'

                result = yield(instance)
                if result == :skip
                    next
                end
                instance.reconcile_content_json if instance.has_attribute?('content_json')

                new_values = instance.attributes.slice(*instance.changes.keys)
                updatable_instance.update_columns(new_values) unless new_values.blank?
            end
        end

    ensure
        @klass.connection.execute("alter table #{@klass.table_name} enable trigger #{@klass.table_name}_versions")
    end

end
