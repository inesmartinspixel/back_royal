# == Schema Information
#
# Table name: learner_projects
#
#  id                     :uuid             not null, primary key
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  title                  :text             not null
#  requirement_identifier :text             not null
#  tag                    :text
#  internal_notes         :text
#  project_documents      :json             not null, is an Array
#  project_type           :text             default("standard"), not null
#

class LearnerProject < ApplicationRecord
    include CrudFromHashHelper

    validates_presence_of :title
    validates_presence_of :requirement_identifier

    CAPSTONE_PROJECT_TYPE = 'capstone'
    STANDARD_PROJECT_TYPE = 'standard'
    PRESENTATION_PROJECT_TYPE = 'presentation'
    def self.valid_project_types
        return [LearnerProject::CAPSTONE_PROJECT_TYPE, LearnerProject::STANDARD_PROJECT_TYPE, LearnerProject::PRESENTATION_PROJECT_TYPE]
    end
    validates :project_type, inclusion: { in: self.valid_project_types, message: "%{value} is not a valid project_type" }

    # EMBA/MBA current - duplicated on client model
    PASSING_SCORES_MAP = {
        LearnerProject::CAPSTONE_PROJECT_TYPE => 3,
        LearnerProject::STANDARD_PROJECT_TYPE => 2,
        LearnerProject::PRESENTATION_PROJECT_TYPE => 2
    }

    before_destroy :ensure_can_be_destroyed

    def self.create_from_hash!(hash, meta = {})
        RetriableTransaction.transaction do
            instance = self.new
            instance.merge_hash(hash)
            instance.save!
            instance
        end
    end

    def self.update_from_hash!(hash, meta = {})
        RetriableTransaction.transaction do
            hash, meta, instance = prepare_update_from_hash(hash, meta)
            instance.merge_hash(hash)
            instance.save!
            instance
        end
    end

    def self.all_by_id

        # We cache the max updated at with a very short expiry so that we don't have to hit
        # this repeatedly in cases where a single request is getting this over and over.  This is
        # a little bit dangerous.  For example, if we created a single request that updated projects and
        # then generated json for a cohort immediately, we might not bump the updated_at and get the
        # fresh list of projects. But I don't think this can be a real issue.  If you're adding a new project,
        # it's always gonna take more than 5 seconds for you to add that new project to a cohort.  If you're updating
        # a project, then having it be possible that the old version gets returned for 5 seconds seems safe.
        max_updated_at = SafeCache.fetch("LearnerProject/max_updated_at", expires_in: 5.seconds) do
            LearnerProject.maximum(:updated_at)
        end

        SafeCache.fetch("LearnerProject/all_by_id/#{max_updated_at}") do
            LearnerProject.all.index_by(&:id)
        end
    end

    def self.find_cached_by_id(id_or_ids)
        ids = Array(id_or_ids)

        ids.map { |id| self.all_by_id.fetch(id) }
    end

    def merge_hash(hash)
        %w(title internal_notes project_documents tag requirement_identifier project_type).each do |key|

            if hash.key?(key)
                val =  hash[key]
                val = nil if val.is_a?(String) && val.blank?
                self[key] = val
            end
        end
    end

    def as_json(options = {})
        options = options.with_indifferent_access
        fields = options[:fields]

        json = {
            id: id,
            title: title,
            tag: tag,
            project_documents: project_documents,
            created_at: created_at.to_timestamp,
            updated_at: updated_at.to_timestamp,
            requirement_identifier: requirement_identifier,
            project_type: project_type
        }.stringify_keys

        if fields&.include?('ADMIN_FIELDS')
            json['internal_notes'] = self.internal_notes
            json['related_cohorts'] = get_related_cohort_json(options[:cohorts], options[:curriculum_templates])
            fields.delete('ADMIN_FIELDS')
        end

        json
    end

    def get_related_cohort_json(cohorts, curriculum_templates)
        records = (cohorts || []) + (curriculum_templates || [])
        records.select { |record|
            published_version = record.respond_to?(:published_version) ? record.published_version : nil

            record.all_learner_project_ids.include?(self.id) ||
            published_version&.all_learner_project_ids&.include?(self.id)
        }.sort { |a, b|
                if a.program_type != b.program_type
                    a.program_type <=> b.program_type
                else
                    # descending by start date, with curriculum templates, that
                    # have no start_date, at the end
                    a_start_date = a.respond_to?(:start_date) ? a.start_date : Time.at(0)
                    b_start_date = b.respond_to?(:start_date) ? b.start_date : Time.at(0)
                    b_start_date <=> a_start_date
                end
            }
            .map { |record|
                {
                    id: record.id,
                    name: record.name,
                    type: record.is_a?(CurriculumTemplate) ? 'CurriculumTemplate' : 'Cohort'
                }.as_json
            }
    end

    def ensure_can_be_destroyed
        cannot_be_destroyed = false
        (Cohort.all + CurriculumTemplate.all).each do |record|
            published_version = record.respond_to?(:published_version) ? record.published_version : nil
            if (record.all_learner_project_ids + (published_version&.all_learner_project_ids || [])).include?(self.id)
                cannot_be_destroyed = true
                errors.add(:"#{self.title}", "cannot be destroyed because it is used in #{record.name.inspect}")
            end
        end
        raise ActiveRecord::RecordInvalid.new(self) if cannot_be_destroyed
    end

    # duplicated in learner_project.js, varies between EMBA and MBA
    def scoring_weight(program_type)
        # NOTE: If we ever decide to support scoring_weights in non-degree programs, we
        # need to give these scoring weights great consideration.
        if !['emba', 'mba'].include?(program_type)
            Raven.capture_in_production(RuntimeError) do
                raise RuntimeError.new("Trying to determine scoring weights for unsupported program #{program_type}")
            end
        end

        # Weights can vary depending on the presence of presentation projects.
        # For EMBA, weights are as follows:
        #  1. For cohorts with presentation projects:
        #    - presentation projects 10% (each), standard projects 20% (each), capstone project 40% -> [1, 1, 2, 2, 4]
        #  2. For cohorts without presentation projects:
        #    - standard projects 25% (each), capstone project 50% -> [2, 2, 4]
        #
        # For MBA, weights are as follows:
        #  1. For cohorts with presentation projects:
        #    - presentation projects 10% (each), standard projects 40% (each) -> [1, 1, 4, 4]
        #  2. For cohorts without presentation projects:
        #    - standard projects 50% (each) -> [4, 4]
        case project_type
        when LearnerProject::CAPSTONE_PROJECT_TYPE
            return 4
        when LearnerProject::STANDARD_PROJECT_TYPE
            return program_type == 'emba' ? 2 : 4
        when LearnerProject::PRESENTATION_PROJECT_TYPE
            return 1
        else
            Raven.capture_in_production(RuntimeError) do
                raise RuntimeError.new("Trying to determine scoring weights for unsupported project_type #{project_type}")
            end
        end
    end

    # duplicated in learner_project.js
    def passing_score(cohort)
        # Standard projects can have varying passing scores, dependent on the
        # cohort's program_type and start_date
        if self.project_type == LearnerProject::STANDARD_PROJECT_TYPE &&
            (cohort.program_type == 'emba' && cohort.start_date <= Time.parse('2019/07/01') ||
            cohort.program_type == 'mba' && cohort.start_date <= Time.parse('2019/07/29'))
            return 1
        end

        # use fetch here to catch us if we add a new project type and forget to add it
        # to the map
        return LearnerProject::PASSING_SCORES_MAP.fetch(self.project_type)
    end
end
