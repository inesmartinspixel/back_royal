# == Schema Information
#
# Table name: published_cohort_lesson_locale_packs
#
#  created_at            :datetime
#  cohort_id             :uuid
#  cohort_name           :text
#  foundations           :boolean
#  required              :boolean
#  specialization        :boolean
#  elective              :boolean
#  exam                  :boolean
#  test                  :boolean
#  assessment            :boolean
#  lesson_title          :text
#  lesson_locale_pack_id :uuid
#  lesson_locales        :text
#
class PublishedCohortLessonLocalePack < ApplicationRecord
    include IsDerivedContentTable

    update_when_the_role_of_a_stream_in_a_cohort_curriculum_changes
    update_when_attr_change_is_published(Cohort, :name)
    update_when_attr_change_is_published(Lesson::Stream, :chapters, comparison: :hashdiff)

    update_when_attr_change_is_published(Lesson, :title, identifier: :locale_pack_id)
    update_when_attr_change_is_published(Lesson, :assessment, identifier: :locale_pack_id)
    update_when_attr_change_is_published(Lesson, :locale, identifier: :locale_pack_id)
    update_when_attr_change_is_published(Lesson, :test, identifier: :locale_pack_id)
    update_when_attr_change_is_published(Lesson, :locale_pack_id, identifier: :locale_pack_id)

    update_after(PublishedCohortStreamLocalePack)


    def self.rebuild
        cohort_ids = Cohort.all_published.pluck(:id)
        return if Rails.env.test? && cohort_ids.empty?

        RetriableTransaction.transaction do
            self.truncate
            write_new_records(:cohort_id, cohort_ids)
        end
    end

    def create_or_update
        raise "Cannot use ActiveRecord create or update methods"
    end

    def self.update_on_content_change(klass:, identifiers: nil, version_pairs: nil)
        if klass == Playlist
            stream_locale_pack_ids = IsDerivedContentTable.get_changed_stream_lpids_on_playlist_change(version_pairs)
            write_new_records(:lesson_locale_pack_id, Lesson::Stream.published_lesson_locale_pack_ids(stream_locale_pack_ids))
        end

        if klass == Lesson::Stream
            lesson_ids = version_pairs.map do |pair|
                old_version, new_version = pair
                a, b = (old_version&.lesson_ids_from_chapters || []), (new_version&.lesson_ids_from_chapters || [])
                a - b | b - a
            end.flatten.uniq

            lesson_locale_pack_ids = Lesson.reorder(nil).where(id: lesson_ids).distinct.pluck(:locale_pack_id).compact
            write_new_records(:lesson_locale_pack_id, lesson_locale_pack_ids)
        end

        if klass == Lesson
            write_new_records(:lesson_locale_pack_id, identifiers)
        end

        if klass == Cohort
            write_new_records(:cohort_id, identifiers)
        end
    end

    private_class_method def self.select_new_records_sql(column, identifiers)
        if column == :cohort_id
            cohort_ids = identifiers
        elsif column == :lesson_locale_pack_id
            lesson_locale_pack_ids = identifiers
        end

        %Q~
            select
                now() as created_at
                , published_cohort_stream_locale_packs.cohort_id
                , published_cohort_stream_locale_packs.cohort_name
                , sum(case when published_cohort_stream_locale_packs.foundations then 1 else 0 end) > 0 as foundations
                , sum(case when published_cohort_stream_locale_packs.required then 1 else 0 end) > 0 as required
                , sum(case when published_cohort_stream_locale_packs.specialization then 1 else 0 end) > 0 as specialization

                -- `elective` is true for any lesson that is in an elective stream.  That means it's
                -- possible for a lesson to be both `required` and `elective` if it is in both
                -- a required stream and an elective stream.  There are no cases of this in the
                -- wild as of 06/01/2020
                , sum(case when published_cohort_stream_locale_packs.required then 0 else 1 end) > 0 as elective
                , sum(case when published_cohort_stream_locale_packs.exam then 1 else 0 end) > 0 as exam
                , published_stream_lesson_locale_packs.test
                , published_stream_lesson_locale_packs.assessment
                , published_stream_lesson_locale_packs.lesson_title
                , published_stream_lesson_locale_packs.lesson_locale_pack_id
                , published_stream_lesson_locale_packs.lesson_locales
            from published_cohort_stream_locale_packs
                join published_stream_lesson_locale_packs
                    on published_stream_lesson_locale_packs.stream_locale_pack_id = published_cohort_stream_locale_packs.stream_locale_pack_id
                    and #{SqlIdList.get_in_clause("lesson_locale_pack_id", lesson_locale_pack_ids)}
            where #{SqlIdList.get_in_clause("published_cohort_stream_locale_packs.cohort_id", cohort_ids)}
            group by
                published_stream_lesson_locale_packs.test
                , published_stream_lesson_locale_packs.assessment
                , published_stream_lesson_locale_packs.lesson_title
                , published_stream_lesson_locale_packs.lesson_locale_pack_id
                , published_stream_lesson_locale_packs.lesson_locales
                , published_cohort_stream_locale_packs.cohort_id
                , published_cohort_stream_locale_packs.cohort_name
        ~
    end
end
