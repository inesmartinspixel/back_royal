# == Schema Information
#
# Table name: student_network_events
#
#  id                                             :uuid             not null, primary key
#  created_at                                     :datetime         not null
#  updated_at                                     :datetime         not null
#  title                                          :text             not null
#  description                                    :text             not null
#  event_type                                     :text             not null
#  start_time                                     :datetime
#  end_time                                       :datetime
#  image_id                                       :uuid
#  external_rsvp_url                              :text
#  place_id                                       :text
#  place_id_anonymized                            :text
#  place_details                                  :json             not null
#  place_details_anonymized                       :json             not null
#  location_name                                  :text
#  published                                      :boolean          default(FALSE), not null
#  published_at                                   :datetime
#  internal_notes                                 :text
#  author_id                                      :uuid             not null
#  visible_to_current_degree_students             :boolean          default(TRUE), not null
#  visible_to_graduated_degree_students           :boolean          default(TRUE), not null
#  visible_to_non_degree_users                    :boolean          default(FALSE), not null
#  location                                       :geography(Point,
#  location_anonymized                            :geography(Point,
#  timezone                                       :text
#  visible_to_accepted_degree_students_in_cohorts :uuid             default([]), not null, is an Array
#  rsvp_status                                    :text             default("not_required"), not null
#  date_tbd                                       :boolean          default(FALSE), not null
#  date_tbd_description                           :text
#

class StudentNetworkEvent < ApplicationRecord

    include HasFulltextSupport
    include Skylight::Helpers

    belongs_to :author, class_name: 'User', primary_key: 'id', foreign_key:'author_id', optional: false
    belongs_to :image, class_name: 'S3Asset', dependent: :destroy, optional: true

    before_save :set_published_at
    before_save :set_place_details_anonymized

    validates_presence_of :title
    validates_presence_of :description
    validates_presence_of :event_type
    validates_presence_of :timezone, if: Proc.new { place_details.present? }

    validate :valid_date_config?


    EVENT_TYPE_CONFIGS = {
        'meetup' => {
            'recommend' => false
        },
        'book_club' => {
            'recommend' => false
        },
        'online_event' => {
            'recommend' => false
        },
        'admissions_event' => {
            'recommend' => false
        },
        'career_fair' => {
            'recommend' => false
        },
        'company_visit' => {
            'recommend' => false
        },
        'special_event' => {
            'recommend' => true
        },
        'conference' => {
            'recommend' => true
        }
    }
    validates_inclusion_of :event_type, :in => EVENT_TYPE_CONFIGS.keys

    def self.rsvp_statuses
        return %w(not_required required closed)
    end
    validates :rsvp_status, inclusion: { in: self.rsvp_statuses, message: "%{value} is not a valid rsvp status" }
    validates_presence_of :external_rsvp_url, :if => Proc.new { rsvp_status == 'required' }
    validates_absence_of :external_rsvp_url, :if => Proc.new { rsvp_status == 'not_required' }

    def self.create_from_hash!(hash)
        RetriableTransaction.transaction do
            instance = initialize_from_hash(hash)
            instance.save!
            instance
        end
    end

    def self.initialize_from_hash(hash)
        hash = hash.unsafe_with_indifferent_access
        instance = new
        instance.merge_hash(hash)
        instance.author_id = hash['author_id']
        instance
    end

    def self.update_from_hash!(hash)
        hash = hash.unsafe_with_indifferent_access
        instance = find_by_id(hash[:id])

        raise ActiveRecord::RecordNotFound.new("Couldn't find #{self.name} with id=#{hash[:id]}") if instance.nil?

        instance.merge_hash(hash)
        instance.updated_at = Time.now
        instance.save!
        instance
    end

    def self.includes_needed_for_json_eager_loading(options = {})
        options = options.with_indifferent_access
        includes = []

        if options[:fields]&.include?('ADMIN_FIELDS')
            includes << :author
        end

        includes
    end

    def merge_hash(hash)
        hash = hash.unsafe_with_indifferent_access

        %w(title description event_type
            image_id
            rsvp_status external_rsvp_url
            place_id place_details location_name timezone
            published internal_notes
            visible_to_current_degree_students visible_to_graduated_degree_students visible_to_accepted_degree_students_in_cohorts visible_to_non_degree_users
            date_tbd date_tbd_description
        ).each do |key|
            if hash.key?(key)
                val = hash[key]
                val = nil if val.is_a?(String) && val.blank?
                self.send(:"#{key}=", val)
            end
        end

        %w(start_time end_time).each do |key|
            if hash.key?(key)
                val = hash[key]
                self[key] = val.nil? ? nil : Time.at(val)
            end
        end

        self
    end

    def author_name
        author&.name
    end

    def as_json(opts = {})
        opts = opts.with_indifferent_access
        additional_data = {}

        keys = %w(
            id
            title
            event_type
            image_id
            rsvp_status
            timezone
        )

        if opts[:fields]&.include?('ADMIN_FIELDS')
            keys.concat(
                %w(
                    published
                    internal_notes
                    author_id
                    visible_to_current_degree_students
                    visible_to_graduated_degree_students
                    visible_to_accepted_degree_students_in_cohorts
                    visible_to_non_degree_users
                )
            )
            additional_data['author_name'] = author_name
        end

        if opts[:user_has_full_student_network_events_access]
            keys.concat(%w(
                description
                external_rsvp_url
                place_id
                place_details
                location
                location_name
            ))
        else
            keys.concat(%w(
                place_id_anonymized
                place_details_anonymized
                location_anonymized
            ))
        end

        additional_data.merge!({
            created_at: self.created_at.to_timestamp,
            updated_at: self.updated_at.to_timestamp,
            anonymized: !opts[:user_has_full_student_network_events_access],
            start_time: self.start_time&.to_timestamp,
            end_time: self.end_time&.to_timestamp,
            date_tbd: self.date_tbd,
            date_tbd_description: self.date_tbd_description,
            published_at: opts[:fields]&.include?('ADMIN_FIELDS') ? self.published_at.to_timestamp : nil,
            recommended: self.recommended?(opts[:user_place_details], opts[:user_is_alumnus]),
            image: self.image.as_json
        })

        json = self.attributes.slice(*keys).merge(additional_data).stringify_keys
        json
    end

    def event_type_config
        StudentNetworkEvent::EVENT_TYPE_CONFIGS[self.event_type]
    end

    def set_published_at
        self.published_at = Time.now if self.published_at.nil? && self.published
    end

    def anonymized_place_details_array
        # `locality` is generally the city, `administrative_area_level_1` the state/province,
        # `country` the country. This will give us something along the lines of
        # ["Washington", "District of Columbia", "United States"]
        details_array = %w(
            locality
            administrative_area_level_1
            country
        ).map do |key|
            self.place_details[key]['long'] unless self.place_details[key].nil?
        end.compact
        details_array
    end

    def set_place_details_anonymized
        if self.place_details.present? && will_save_change_to_place_details?
            # NOTE: this will be an API call out to Google that will block the saving of a
            # StudentNetworkEvent when `place_details` is set or changes. We decided that we
            # are OK with this for now. If it becomes a pain in the future, we could refactor
            # this to run in a delayed_job. However, we'd have to figure out how to display
            # "incomplete" records, without the anonymized information, on the client inbetween
            # record creation and when the job runs.
            place = Location.find_place_from_text(anonymized_place_details_array.join(', '))
            if place
                self.place_id_anonymized = place['place_id']
                self.place_details_anonymized = self.place_details.slice(*%w(
                    locality
                    administrative_area_level_1
                    country
                )).merge({
                    "lat"=>place['geometry']['location']['lat'],
                    "lng"=>place['geometry']['location']['lng']
                })
            end
        end
    end

    def should_update_fulltext?
        !!(
            saved_changes[:title] ||
            saved_changes[:description] ||
            saved_changes[:event_type] ||
            saved_changes[:location_name] ||
            saved_changes[:place_details] ||
            saved_changes[:visible_to_current_degree_students] ||
            saved_changes[:visible_to_graduated_degree_students] ||
            saved_changes[:visible_to_accepted_degree_students_in_cohorts]
        )
    end

    def valid_date_config?
        return if self.date_tbd
        errors.add(:start_time, "must be set if not date_tbd") unless self.start_time.present?
        errors.add(:end_time, "must be set if not date_tbd") unless self.end_time.present?
    end

    private def degree_program_types_visible_to
        visible_to = Cohort.where(id: visible_to_accepted_degree_students_in_cohorts).pluck(:program_type)

        if self.visible_to_current_degree_students || self.visible_to_graduated_degree_students
            visible_to += Cohort::ProgramTypeConfig.degree_program_types
        end

        visible_to.uniq
    end

    instrument_method
    def update_fulltext

        # get and sanitize relevant strings
        title_text = self.class.sanitize_fulltext_sql(self.title)
        description_text = self.class.sanitize_fulltext_sql(self.description)
        event_type_text = self.class.sanitize_fulltext_sql(self.event_type)
        location_name_text = self.class.sanitize_fulltext_sql(self.location_name) if self.location_name

        # See https://trello.com/c/878vw10z for why this is added to the tsvector
        degree_program_types_visible_to_text = self.class
            .sanitize_fulltext_sql(degree_program_types_visible_to.empty? ? nil : degree_program_types_visible_to.join(' '))

        if self.place_details.present?
            # full_keyword_vector includes: title, description, type, location_name, and formatted address (street address, city, state, zip, county)
            # anonymous_keyword_vector includes: title, type, and city, state, country
            full_place_details_text = self.class.sanitize_fulltext_sql(self.place_details['formatted_address'])
            anonymized_place_details_text = self.class.sanitize_fulltext_sql(anonymized_place_details_array.join(' '))
        end

        location_name_text ||= 'NULL'
        full_place_details_text ||= 'NULL'
        anonymized_place_details_text ||= 'NULL'

        # build out sql snippets
        full_keyword_sql = "setweight(to_tsvector('english', coalesce(#{title_text},'')), 'A') ||
                        setweight(to_tsvector('english', coalesce(#{description_text},'')), 'B') ||
                        setweight(to_tsvector('english', coalesce(#{event_type_text},'')), 'C') ||
                        setweight(to_tsvector('english', coalesce(#{location_name_text},'')), 'D') ||
                        setweight(to_tsvector('english', coalesce(#{full_place_details_text},'')), 'D') ||
                        setweight(to_tsvector('english', coalesce(#{degree_program_types_visible_to_text},'')), 'D')"
        full_keyword_text = self.class.sanitize_fulltext_sql([title_text, description_text, event_type_text, location_name_text, full_place_details_text].join(' '))

        anonymized_keyword_sql = "setweight(to_tsvector('english', coalesce(#{title_text},'')), 'A') ||
                        setweight(to_tsvector('english', coalesce(#{event_type_text},'')), 'B') ||
                        setweight(to_tsvector('english', coalesce(#{anonymized_place_details_text},'')), 'C')"
        anonymized_keyword_text = self.class.sanitize_fulltext_sql([title_text, event_type_text, anonymized_place_details_text].join(' '))

        if ActiveRecord::Base.connection.execute("SELECT 1 FROM student_network_event_fulltext WHERE student_network_event_id='#{self.id}'").count == 0
            sql = "
                INSERT INTO student_network_event_fulltext
                (
                    student_network_event_id,
                    full_keyword_vector,
                    full_keyword_text,
                    anonymous_keyword_vector,
                    anonymous_keyword_text,
                    created_at,
                    updated_at
                )
                VALUES
                (
                    '#{self.id}',
                    #{full_keyword_sql},
                    #{full_keyword_text},
                    #{anonymized_keyword_sql},
                    #{anonymized_keyword_text},
                    NOW(),
                    NOW()
                )"
        else
            sql = "
                UPDATE student_network_event_fulltext
                SET full_keyword_vector = #{full_keyword_sql},
                    full_keyword_text = #{full_keyword_text},
                    anonymous_keyword_vector = #{anonymized_keyword_sql},
                    anonymous_keyword_text = #{anonymized_keyword_text},
                    updated_at = NOW()
                WHERE student_network_event_id = '#{self.id}'"
        end

        # update or insert the fulltext
        ActiveRecord::Base.connection.execute(sql)
    end

    def recommended?(place_details = nil, user_is_alumnus = false)
        # An event should be recommended if:
        # It is important enough to show regardless of geographic relevancy, or
        # It is an online event with no location, or
        # It is geographically relevant to the user, or
        # It is for an alumnus, which are shown events outside their location.

        !!self.event_type_config['recommend'] ||
            self.location.nil? ||
            self.geographically_relevant_to_place_details_or_fallback_location?(place_details) ||
            user_is_alumnus
    end

    def geographically_relevant_to_place_details_or_fallback_location?(_place_details = nil)
        return false if self.place_details.empty?

        # If the passed-in place_details are nil, we should default to recommending
        # events geographically relevant to Washington, DC.
        location = _place_details && Location.from_place_details(_place_details.symbolize_keys) || Location.locations[:washington_dc]

        # We define geographic relevancy as being within 50 miles.
        Location.from_place_details(self.place_details.symbolize_keys).distance_between(location, {units: :mi}) <= 50
    end

end
