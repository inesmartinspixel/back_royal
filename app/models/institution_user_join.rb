# == Schema Information
#
# Table name: institutions_users
#
#  institution_id :uuid
#  user_id        :uuid
#  active         :boolean          default(FALSE), not null
#  id             :uuid             not null, primary key
#

class InstitutionUserJoin < ApplicationRecord
    self.table_name = 'institutions_users'

    belongs_to :user
    belongs_to :institution

    before_destroy :ensure_no_acceptable_application_for_user
    after_commit :identify_user, if: :should_identify_after_commit?
    
    def ensure_no_acceptable_application_for_user
        if self.user.cohort_applications.where(status: CohortApplication.acceptable_statuses).to_a
            .map(&:institution).pluck(:id).any?(self.institution_id)
            statuses_message = CohortApplication.acceptable_statuses.to_sentence(last_word_connector: ', or ')
            errors.add(:user, "has an application with status #{statuses_message} for this institution")
            raise ActiveRecord::RecordInvalid.new(self)
        end
    end

    def identify_user
        # FK constraint will ensure a user
        self.user.identify
    end

    private def should_identify_after_commit?
        !self.destroyed? || (self.destroyed? && self.destroyed_by_association&.active_record&.name != User.name)
    end
end
