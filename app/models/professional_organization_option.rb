# == Schema Information
#
# Table name: professional_organization_options
#
#  id         :uuid             not null, primary key
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  text       :text             not null
#  locale     :text             default("en"), not null
#  suggest    :boolean          default(FALSE)
#  source_id  :text
#  image_url  :text
#

class ProfessionalOrganizationOption < ActiveRecord::Base

    include AutoSuggestOptionMixin

    self.table_name = 'professional_organization_options'
    has_one :work_experience
    has_one :user

    def promote_suggested_for_all_locales
        I18n.available_locales.each do |locale|

            target_attrs = self.attributes
                .except('id', 'created_at', 'updated_at', 'suggest')
                .merge({ 'locale' => locale.to_s })

            organization = ProfessionalOrganizationOption.find_or_initialize_by(target_attrs)
            organization.suggest = true
            organization.save!
        end
        self.reload
    end

    def type
        'professional_organization'
    end

end
