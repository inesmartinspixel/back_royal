module InternalInstitutionMixin
    QUANTIC_ID = '85fec419-8dc5-45a5-afbd-0cc285a595b9'
    SMARTLY_ID = 'ebc4730d-19d8-4cb3-b2ca-e20308bec8e3'
    MIYA_MIYA_ID = 'a297c552-8cef-4c78-83b3-465c8711377c'

    QUANTIC_BRANDING = 'quantic'
    SMARTLY_BRANDING = 'smartly'

    def self.included(base)
        base.extend ClassMethods
    end

    def brand_name_short
        if self.id == QUANTIC_ID
            'Quantic'
        elsif self.id == MIYA_MIYA_ID
            'مية مية'
        else
            'Smartly'
        end
    end

    def brand_name_standard
        if self.id == QUANTIC_ID
            'Quantic'
        elsif self.id == MIYA_MIYA_ID
            'مية مية'
        else
            'Smartly Institute'
        end
    end

    def brand_name_long
        if self.id == QUANTIC_ID
            'Quantic School of Business and Technology'
        elsif self.id == MIYA_MIYA_ID
            'مية مية'
        else
            'Smartly Institute'
        end
    end

    module ClassMethods
        # Note: Some of our institutions are very unlikely to change after being set up
        # and are used in every request for a majority of users, so apply Rails-level
        # caching to them. If we need to expire them immediately then log into each
        # prod box and run, for example:
        #   `SafeCache.delete("institution/#{InternalInstitutionMixin::QUANTIC_ID}")`


        def valid_quantic_attrs
            {
                id: QUANTIC_ID,
                name: 'Quantic',
                sign_up_code: 'QUANTIC',
                external: false,
                domain: 'quantic.edu'
            }
        end

        def valid_smartly_attrs
            {
                id: SMARTLY_ID,
                name: 'Smartly',
                sign_up_code: 'SMARTLY',
                external: false,
                domain: 'smart.ly'
            }
        end

        def valid_miya_miya_attrs
            {
                id: MIYA_MIYA_ID,
                name: 'Miya Miya',
                sign_up_code: 'MIYAMIYA',
                external: false,
                domain: 'smart.ly'
            }
        end


        def quantic
            SafeCache.fetch("institution/#{QUANTIC_ID}", expires_in: 1.hour) do
                if ENV['USE_SEED_DB'] == 'true'
                    Institution.new(valid_quantic_attrs)
                else
                    Institution.find(QUANTIC_ID)
                end
            end
        end

        def smartly
            SafeCache.fetch("institution/#{SMARTLY_ID}", expires_in: 1.hour) do
                if ENV['USE_SEED_DB'] == 'true'
                    Institution.new(valid_smartly_attrs)
                else
                    Institution.find(SMARTLY_ID)
                end
            end
        end

        def miya_miya
            SafeCache.fetch("institution/#{MIYA_MIYA_ID}", expires_in: 1.hour) do
                if ENV['USE_SEED_DB'] == 'true'
                    Institution.new(valid_miya_miya_attrs)
                else
                    Institution.find(MIYA_MIYA_ID)
                end
            end
        end
    end
end