# == Schema Information
#
# Table name: published_cohort_playlist_locale_packs
#  created_at              :datetime
#  cohort_id               :uuid
#  cohort_name             :text
#  required                :boolean
#  specialization          :boolean
#  foundations             :boolean
#  playlist_title          :text
#  playlist_locales        :text
#  playlist_locale_pack_id :uuid
#
class PublishedCohortPlaylistLocalePack < ApplicationRecord
    include IsDerivedContentTable
    self.table_name = 'published_cohort_playlist_locale_packs'

    update_when_attr_change_is_published(Cohort, :name)
    update_when_attr_change_is_published(Cohort, :playlist_collections, comparison: :hashdiff)
    update_when_attr_change_is_published(Cohort, :specialization_playlist_pack_ids, comparison: :sort_agnostic_collection)
    update_when_attr_change_is_published(Playlist, :title, identifier: :locale_pack_id)
    update_when_attr_change_is_published(Playlist, :locale, identifier: :locale_pack_id)
    update_when_attr_change_is_published(Playlist, :locale_pack_id, identifier: :locale_pack_id)

    def create_or_update
        raise "Cannot use ActiveRecord create or update methods"
    end

    def self.update_on_content_change(klass:, identifiers:)
        if klass == Cohort
            write_new_records(:cohort_id, identifiers)
        elsif klass == Playlist
            write_new_records(:playlist_locale_pack_id, identifiers)
        end
    end

    def self.rebuild
        RefreshMaterializedContentViews.ensure_all_populated
        cohort_ids = PublishedCohort.all.pluck(:id)

        return if Rails.env.test? && cohort_ids.empty?

        RetriableTransaction.transaction do
            self.truncate
            write_new_records(:cohort_id, cohort_ids)
        end
    end

    private_class_method def self.select_new_records_sql(column, identifiers)
        cohort_ids = nil
        stream_locale_pack_ids = nil
        if column == :cohort_id
            cohort_ids = identifiers
        elsif column == :playlist_locale_pack_id
            playlist_locale_pack_ids = identifiers
        end

        %Q|
            with required_cohort_playlist_locale_packs AS MATERIALIZED (
                select
                    cohorts.id as cohort_id,
                    cohorts.name as cohort_name,
                    true as required,
                    false as specialization,
                    unnest(required_playlist_pack_ids) as playlist_locale_pack_id
                from published_cohorts cohorts
                WHERE #{SqlIdList.get_in_clause("cohorts.id", cohort_ids)}
            )
            , specialization_cohort_playlist_locale_packs AS MATERIALIZED (
                select
                    cohorts.id as cohort_id,
                    cohorts.name as cohort_name,
                    false as required,
                    true as specialization,
                    unnest(specialization_playlist_pack_ids) as playlist_locale_pack_id
                from published_cohorts cohorts
                WHERE #{SqlIdList.get_in_clause("cohorts.id", cohort_ids)}
            )
            , cohort_playlist_locale_packs_1 AS MATERIALIZED (
                select * from required_cohort_playlist_locale_packs
                WHERE #{SqlIdList.get_in_clause("playlist_locale_pack_id", playlist_locale_pack_ids)}

                union

                select * from specialization_cohort_playlist_locale_packs
                WHERE #{SqlIdList.get_in_clause("playlist_locale_pack_id", playlist_locale_pack_ids)}
            )
            , cohort_playlist_locale_packs_2 AS MATERIALIZED (
                select
                    cohort_playlist_locale_packs_1.cohort_id,
                    cohort_playlist_locale_packs_1.cohort_name,
                    cohort_playlist_locale_packs_1.required,
                    cohort_playlist_locale_packs_1.specialization,
                    published_playlist_locale_packs.locale_pack_id = (select required_playlist_pack_ids[1] from published_cohorts where id = cohort_id) as foundations,
                    published_playlist_locale_packs.title as playlist_title,
                    published_playlist_locale_packs.locales as playlist_locales,
                    published_playlist_locale_packs.locale_pack_id as playlist_locale_pack_id
                from cohort_playlist_locale_packs_1
                    join published_playlist_locale_packs on published_playlist_locale_packs.locale_pack_id = cohort_playlist_locale_packs_1.playlist_locale_pack_id
            )

            SELECT now() as created_at, * from cohort_playlist_locale_packs_2
            ORDER BY cohort_id, playlist_locale_pack_id
        |
    end
end
