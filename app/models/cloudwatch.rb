class Cloudwatch

    attr_reader :cw

    # NOTE: While we currently delegate to these methods, we might not necessarily IAM policy permissions for them
    # until they are actually required. Please verify that the permissions exist.
    class << self
        delegate :delete_alarms, :describe_alarm_history, :describe_alarms, :describe_alarms_for_metric,
            :disable_alarm_actions, :enable_alarm_actions, :get_metric_statistics, :list_metrics,
            :put_metric_alarm, :put_metric_data, :set_alarm_state,
            :put_timing_metric_data,
            :to => :client

        def default_alarm_actions
            ENV['SNS_NAME'] ? [ENV['SNS_NAME']] : []
        end

    end

    def self.client
        @client ||= self.new
    end

    def initialize
        unless Rails.env.test?
            @cw = Aws::CloudWatch::Client.new
        end
    end

    [:delete_alarms, :describe_alarm_history, :describe_alarms, :describe_alarms_for_metric, :disable_alarm_actions, :enable_alarm_actions, :get_metric_statistics, :list_metrics, :put_metric_alarm, :put_metric_data, :set_alarm_state].each do |meth|
        define_method meth do |opts = {}|
            Raven.capture do
                return unless ENV['ENABLE_CLOUDWATCH_LOGGING'] != 'false'
                return if Rails.env.test?
                app_name = ENV['APP_ENV_NAME'] || (Rails.env.development? ? 'Smartly (Dev)' : 'UNKNOWN')
                namespace = [app_name, opts.delete(:ns_suffix)].join (' ')
                cw.send(meth, opts.merge(namespace: namespace))
            end
        end
    end

    def put_timing_metric_data(metric_name:, ns_suffix: 'Performance', min_time: 0, &block)
        start = Time.now
        return_value = yield
        elapsed = Time.now - start

        if elapsed >= min_time
            self.put_metric_data(
                ns_suffix: ns_suffix,
                metric_data: [{
                    metric_name: metric_name,
                    value: elapsed,
                    unit: 'Seconds'
                }])
        end

        return return_value


    end



end