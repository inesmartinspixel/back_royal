class HiringTeamInvite

    # This method creates a new user with
    # 1. the email and name provided
    # 2. provider=hiring_team_invite
    # 3. hiring_team_id=inviter.hiring_team_id
    # 4. professional_organization_option_id=inviter.professional_organization_option_id
    # 5. an accepted hiring application with some stuff copied over from the
    #       inviter's application
    #
    # With the hiring_team_invite provider, they will be able to login
    #   with their email and any password.  Once they do, their password
    #   will be assigned to whatever they provided and they will become
    #   a regular provider=email user. See custom_sessions_controller
    #
    # The advantages of creating the user up-front are
    # 1. Our internal team can immediately start completing
    #       the new user's profile for them
    # 2. We can easily use customer.io to send this user the
    #       invite email, since we can identify them and log
    #       events for them
    # 3. We don't have to create a new table to store invites
    def self.invite!(params)
        RetriableTransaction.transaction do

            inviter = User.find((params[:inviter_id]))
            inviter.ensure_hiring_team!

            invitee = User.new(
                email: params[:invitee_email],
                sign_up_code: 'HIRING_TEAM_INVITE',
                provider: 'hiring_team_invite',
                uid: params[:invitee_email],
                name: params[:invitee_name],
                hiring_team_id: inviter.hiring_team_id,
                professional_organization_option_id: inviter.professional_organization_option_id
            )
            invitee.set_random_password

            if !invitee.valid? && invitee.errors[:email]&.include?('has already been taken')
                existing_record = User.find_by_email(params[:invitee_email])
                raise InviteeExists.new(existing_record)
            end

            invitee.save!

            application_params = {
                user_id: invitee.id,
                status: 'accepted'
            }

            %w(
                company_year place_id place_details company_employee_count
                company_annual_revenue industry funding
                company_sells_recruiting_services website_url
                team_mission role_descriptors where_descriptors
                position_descriptors salary_ranges
            ).each do |key|
                application_params[key] = inviter.hiring_application[key]
            end

            HiringApplication.create!(application_params)

            Event.create_server_event!(
                SecureRandom.uuid,
                invitee.id,
                'invited_to_hiring_team',
                {
                    inviter_name: inviter.name,
                    inviter_id: inviter.id
                }
            ).log_to_external_systems

            invitee

        end
    end

    class InviteeExists < RuntimeError
        attr_accessor :invitee

        def initialize(invitee)
            @invitee = invitee
            super("Invitee already has an account")
        end

        def details
            {
                error_type: 'invitee_exists',
                invitee_hiring_team_id: invitee.hiring_team_id,
                invitee_provider: invitee.provider
            }
        end

    end
end
