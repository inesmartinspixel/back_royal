# Make all transactions for all records automatically retriable
# in the event of cache failure
# ticket: https://trello.com/c/QHTNy5ux
# introduced here: https://github.com/rails/rails/commit/50c53340824de2a8000fd2d5551cbce2603dc34a
# discussion here: https://github.com/rails/rails/pull/22170
class RetriableTransaction < ApplicationRecord
    class << self
        # Retry automatically on ActiveRecord::PreparedStatementCacheExpired.
        #
        # Do not use this for transactions with side-effects unless it is acceptable
        # for these side-effects to occasionally happen twice
        def transaction(*args, &block)
            retried ||= false
            savepoint_id = "savepoint_#{SecureRandom.hex(10)}"
            super(*args) do
                begin
                    connection.execute("SAVEPOINT #{savepoint_id}")
                    yield
                rescue ActiveRecord::PreparedStatementCacheExpired
                    if retried
                        raise
                    else
                        connection.execute("ROLLBACK TO SAVEPOINT #{savepoint_id}")
                        connection.clear_cache!
                        retried = true
                        retry
                    end
                end
            end
        end
    end
end