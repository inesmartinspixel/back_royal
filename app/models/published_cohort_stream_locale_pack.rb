# == Schema Information
#
# Table name: published_cohort_stream_locale_packs
#
#  created_at            :datetime
#  cohort_id             :uuid
#  cohort_name           :text
#  foundations           :boolean
#  required              :boolean
#  specialization        :boolean
#  elective              :boolean
#  exam                  :boolean
#  stream_title          :text
#  stream_locale_pack_id :uuid
#  stream_locales        :text
#  playlist_count        :integer
#

# one row for each stream that is available
# to members of a cohort, either because
# - it is in one of the cohort's playlists
# - it is in one of the cohort's groups
# - it is in the schedule for the cohort
#
# Each stream will be marked as
# - required (is marked as required in the schedule or is in a required_playlist),
# - exam (exam = true),
# - specialization (in a specialization playlist)
# - and/or elective (not required)
class PublishedCohortStreamLocalePack < ApplicationRecord
    include IsDerivedContentTable

    update_when_the_role_of_a_stream_in_a_cohort_curriculum_changes
    update_when_attr_change_is_published(Cohort, :name)

    update_when_attr_change_is_published(Lesson::Stream, :title, identifier: :locale_pack_id)
    update_when_attr_change_is_published(Lesson::Stream, :locale, identifier: :locale_pack_id)

    update_after(PublishedCohortPlaylistLocalePack, PublishedCohortPeriodsStreamLocalePack)

    def self.rebuild
        cohort_ids = PublishedCohort.all.pluck(:id)
        return if Rails.env.test? && cohort_ids.empty?

        RetriableTransaction.transaction do
            self.truncate
            write_new_records(:cohort_id, cohort_ids)
        end
    end

    def create_or_update
        raise "Cannot use ActiveRecord create or update methods"
    end

    def self.update_on_content_change(klass:, identifiers: nil, version_pairs: nil)
        if klass == Playlist
            stream_locale_pack_ids = IsDerivedContentTable.get_changed_stream_lpids_on_playlist_change(version_pairs)
            write_new_records(:stream_locale_pack_id, stream_locale_pack_ids)
        end

        if klass == Lesson::Stream
            write_new_records(:stream_locale_pack_id, identifiers)
        end

        if klass == Cohort
            write_new_records(:cohort_id,  identifiers)
        end
    end

    private_class_method def self.select_new_records_sql(column, identifiers)
        cohort_ids = nil
        stream_locale_pack_ids = nil
        if column == :cohort_id
            cohort_ids = identifiers
        elsif column == :stream_locale_pack_id
            stream_locale_pack_ids = identifiers
        end

        %Q~
            WITH streams_from_groups AS MATERIALIZED (
                SELECT
                    DISTINCT
                      cohorts.id     AS cohort_id
                    , locale_pack_id AS stream_locale_pack_id
                    , 0              AS required
                    , 0              AS specialization
                    , 0              AS foundations
                FROM
                    published_cohorts cohorts
                    LEFT JOIN access_groups_cohorts ON access_groups_cohorts.cohort_id = cohorts.id
                    LEFT JOIN access_groups_lesson_stream_locale_packs
                        ON access_groups_lesson_stream_locale_packs.access_group_id = access_groups_cohorts.access_group_id
                WHERE #{SqlIdList.get_in_clause("cohorts.id", cohort_ids)}
                    AND #{SqlIdList.get_in_clause("access_groups_lesson_stream_locale_packs.locale_pack_id", stream_locale_pack_ids)}
            )
            , streams_from_playlists AS MATERIALIZED (
                SELECT
                    DISTINCT
                      cohorts.id                                       AS cohort_id
                    , published_playlist_streams.stream_locale_pack_id AS stream_locale_pack_id
                    , CASE WHEN published_cohort_playlist_locale_packs.required
                    THEN 1
                      ELSE 0 END                                       AS required
                    , CASE WHEN published_cohort_playlist_locale_packs.specialization
                    THEN 1
                      ELSE 0 END                                       AS specialization
                    , CASE WHEN published_cohort_playlist_locale_packs.foundations
                    THEN 1
                      ELSE 0 END                                       AS foundations
                FROM
                    published_cohorts cohorts
                    JOIN #{PublishedCohortPlaylistLocalePack.table_name} published_cohort_playlist_locale_packs
                        ON cohorts.id = published_cohort_playlist_locale_packs.cohort_id
                    JOIN published_playlist_streams
                        ON published_playlist_streams.playlist_locale_pack_id =
                           published_cohort_playlist_locale_packs.playlist_locale_pack_id
                        AND #{SqlIdList.get_in_clause("published_playlist_streams.stream_locale_pack_id", stream_locale_pack_ids)}
                WHERE #{SqlIdList.get_in_clause("cohorts.id", cohort_ids)}
            )
            , streams_from_schedule AS MATERIALIZED (
                SELECT
                    cohort_id
                    , locale_pack_id as stream_locale_pack_id
                    , case when required then 1 else 0 end as required
                    , 0 as specialization
                    , 0 as foundations
                FROM #{PublishedCohortPeriodsStreamLocalePack.table_name}
                WHERE
                    #{SqlIdList.get_in_clause("cohort_id", cohort_ids)}
                    AND #{SqlIdList.get_in_clause("locale_pack_id", stream_locale_pack_ids)}
            )
            , all_stream_locale_pack_ids AS MATERIALIZED (
                SELECT *
                FROM streams_from_groups
                UNION SELECT *
                      FROM streams_from_playlists
                UNION SELECT *
                      FROM streams_from_schedule
            )
            , published_cohort_stream_locale_packs AS MATERIALIZED (
                SELECT
                      cohorts.id                                                                     AS cohort_id
                    , cohorts.name                                                                   AS cohort_name
                    , CASE WHEN sum(all_stream_locale_pack_ids.foundations) > 0
                    THEN TRUE
                      ELSE FALSE END                                                                 AS foundations
                    , CASE WHEN sum(all_stream_locale_pack_ids.required) > 0
                    THEN TRUE
                      ELSE FALSE END                                                                 AS required
                    , CASE WHEN sum(all_stream_locale_pack_ids.specialization) > 0
                    THEN TRUE
                      ELSE FALSE END                                                                 AS specialization
                    , CASE WHEN sum(all_stream_locale_pack_ids.required) + sum(all_stream_locale_pack_ids.specialization) = 0
                    THEN TRUE
                      ELSE FALSE END                                                                 AS elective
                    , published_stream_locale_packs.exam                                             AS exam
                    , published_stream_locale_packs.title                                            AS stream_title
                    , published_stream_locale_packs.locale_pack_id                                   AS stream_locale_pack_id
                    , published_stream_locale_packs.locales                                          AS stream_locales
                    , count(DISTINCT published_cohort_playlist_locale_packs.playlist_locale_pack_id) AS playlist_count
                FROM
                    published_cohorts cohorts
                    JOIN all_stream_locale_pack_ids
                        ON cohorts.id = all_stream_locale_pack_ids.cohort_id
                    JOIN published_stream_locale_packs
                        ON published_stream_locale_packs.locale_pack_id = all_stream_locale_pack_ids.stream_locale_pack_id
                    LEFT JOIN published_playlist_streams
                        ON published_playlist_streams.stream_locale_pack_id = all_stream_locale_pack_ids.stream_locale_pack_id
                    LEFT JOIN #{PublishedCohortPlaylistLocalePack.table_name} published_cohort_playlist_locale_packs ON
                                                                         cohorts.id =
                                                                         published_cohort_playlist_locale_packs.cohort_id
                                                                         AND published_playlist_streams.playlist_locale_pack_id =
                                                                             published_cohort_playlist_locale_packs.playlist_locale_pack_id
                GROUP BY
                    cohorts.id
                    , cohorts.name
                    , published_stream_locale_packs.title
                    , published_stream_locale_packs.locale_pack_id
                    , published_stream_locale_packs.locales
                    , published_stream_locale_packs.exam
            )
            SELECT now() as created_at, *
            FROM published_cohort_stream_locale_packs
        ~
    end

end
