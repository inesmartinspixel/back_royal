module User::SubscriptionConcern

    extend ActiveSupport::Concern

    class InvalidCoupon < RuntimeError; end

    included do
        after_save :change_email_in_stripe, if: :saved_change_to_email?
    end

    # this is tested in the subscriptions_controller_spec
    def handle_create_subscription_request(attrs, meta, stripe_plan_id)
        meta = meta || {}
        application = last_application

        if !application.published_cohort.supports_payments?
            raise "Cannot create subscription on application (#{application['id']}) for non-billable Cohort (#{application.published_cohort['id']})!"
        end

        trial_end_ts = nil
        coupon_id = get_coupon_id_for_request(application, stripe_plan_id, meta[:coupon_id])
        # handle full scholarship or paid-out re-registation situation. Don't do any payment adjusment or subscription
        # creation, but ensure registered, total_num_required_stripe_payments, and stripe_plan_id are properly set
        if !application.required_to_provide_payment?

            application.total_num_required_stripe_payments = 0
            application.stripe_plan_id = stripe_plan_id # FIXME: maybe this is unnecessary?
            application.registered = true;

        # otherwise, collect billing info and create subscriptions as necessary, based on application
        else

            # Determines if the user has paid recently, and if so, pads with a trial_period if appropriate
            all_charges_refunded = application.num_charged_payments.to_i > 0 && application.num_charged_payments == application.num_refunded_payments
            if !all_charges_refunded && application.num_charged_payments.to_i > 0 # may be nil

                # get last and figure out next billing cycle
                last_charged_ts = self.last_charged_ts || 0

                frequency = application.get_plan_frequency(stripe_plan_id)
                if frequency == 'bi_annual'
                    interval = 6.months
                elsif frequency == "monthly"
                    interval = 1.month
                else
                    # should be impossible to have `once` frequency here
                    raise "Attempting to re-bill on an invalid plan interval #{stripe_plan_id}"
                end

                # if in the future, go ahead and set the trial until that period
                next_charge_at = Time.at(last_charged_ts) + interval
                if next_charge_at > Time.now
                    trial_end = next_charge_at
                end
                trial_end_ts = trial_end ? trial_end.to_timestamp : nil

            end
        end

        # Application / Subscription writes
        subscription = nil
        RetriableTransaction.transaction do

            if meta.key?(:cohort_application)
                application.shareable_with_classmates = meta[:cohort_application][:shareable_with_classmates]
            end

            # ensure we save the application so that it is valid prior to calling `Subscription.create_for_owner` which
            # further modifies the record
            application.save!

            # If payment is required, create a Stripe subscription and rely on hooks to create internal representation
            if application.required_to_provide_payment?
                subscription = Subscription.create_for_owner(
                    owner: self,
                    stripe_plan_id: stripe_plan_id,
                    coupon_id: coupon_id,
                    trial_end_ts: trial_end_ts)
            end
        end

        subscription
    end

    def get_coupon_id_for_request(application, stripe_plan_id, coupon_id)
        # Determine coupon criteria
        program_type_config = application.program_type_config
        if program_type_config.supports_scholarship_levels?

            # Get coupon based on scholarship and registration period
            coupon_id = application.applicable_coupon_info(stripe_plan_id)['id']
            if coupon_id == 'none'
                coupon_id = nil
            end
        elsif coupon_id

            # Handle basic prefix-based validation
            # NOTE: Providing a non-existent `coupon_id` will result in InvalidRequestError, which
            # we catch and render with a HTTP response code of 406 via `render_stripe_error`
            unless program_type_config.valid_coupon_id?(coupon_id)
                raise InvalidCoupon.new
            end
        end

        return coupon_id
    end

    def after_subscription_commit(subscription)
        self.identify
    end

    def before_subscription_destroy(subscription)
        stripe_plan_id = subscription.stripe_plan_id
        application = application_for_stripe_plan_id(stripe_plan_id)

        stripe_subscription = subscription.stripe_subscription
        if stripe_subscription && application
            stripe_subscription.metadata['relevant_cohort_id'] = application.cohort_id
            stripe_subscription.save
        end

        application.lock!

        # unregister if there are still required stripe payments left (this is important
        # if a subscription is destroyed through the stripe interface).  If a description is
        # destroyed through the admin UI, this registered change is also sent up from the client.
        # It would probably work fine if it weren't sent up from the client, but over time that's
        # how things came to be.
        application.registered = (application.total_num_required_stripe_payments == 0)
        application.save!
    end

    def handle_past_due_changes(subscription)
        cohort_application = cohort_application_for_subscription(subscription)

        if cohort_application.payment_past_due?
            # When a subscription becomes past_due, we give the user a grace period to provide us with
            # the required payment. Under the hood, configuring this payment grace period enqueues a
            # LockContentAccessDueToPastDuePayment delayed job that will in turn mark the application
            # as locked_due_to_past_due_payment, which locks the user's content access on the client.
            cohort_application.ensure_payment_grace_period
            cohort_application.save!
        else
            cohort_application.update!(locked_due_to_past_due_payment: false, payment_grace_period_end_at: nil)
        end
    end

    def record_invoice_payment(subscription, invoice)
        stripe_plan_id = subscription.stripe_plan_id
        application = application_for_stripe_plan_id(stripe_plan_id)
        application.lock!

        application.num_charged_payments = application.num_charged_payments.nil? ? 1 : application.num_charged_payments + 1
        application.num_refunded_payments ||= 0

        # if this is the very first payment for the user's application, log an event
        if application.num_charged_payments == 1 && application.num_refunded_payments == 0
            Event.create_server_event!(SecureRandom.uuid, self.id, 'cohort:first_payment_succeeded', {
                is_paid_cert: application.program_type_config.is_paid_cert?,
                cohort_title: application.published_cohort.title,
                learner_projects: application.program_type_config.supports_cohort_level_projects? ? application.published_cohort.cohort_level_learner_projects.as_json : [],
            }).log_to_external_systems
        end

        # still have payments remaining, but marked as registered
        if !all_payments_complete?(subscription)
            application.save!
            return
        end

        # set the cancellation reason for auto-pay completion
        subscription.stripe_subscription.metadata['cancellation_reason'] = 'Payment complete'
        subscription.stripe_subscription.save

        # all paid! time to mark the student as no longer requiring payment and clean out the subscription
        # see also: `CohortApplication::clear_subscription_on_zero_payments` `after_save` callback
        application.total_num_required_stripe_payments = 0
        application.save!
    end

    def record_refund(stripe_plan_id)
        application = application_for_stripe_plan_id(stripe_plan_id)
        application.lock!
        application.num_refunded_payments = application.num_refunded_payments.nil? ? 1 : application.num_refunded_payments + 1

        # If the corresponding user has made any payments outside of Stripe, we do not want to unregister, lock,
        # and reset `total_num_required_stripe_payments` on their cohort application.
        if !self.primary_subscription && !application.user.billing_transactions.where(transaction_type: 'payment').where.not(provider: BillingTransaction::PROVIDER_STRIPE).any?
            application.registered = false
            application.locked_due_to_past_due_payment = true
            application.total_num_required_stripe_payments = application.set_total_num_default_value(stripe_plan_id)

            LogToSlack.perform_later(subscription_slack_channel, nil,
                [
                    {
                        color: '#FF9300',
                        text: "*#{application.user.email}* has been *unregistered* and *locked_due_to_past_due_payment* after issuing refund. :warning:",
                        mrkdwn_in: ['text']
                    }
                ]
            )
        end

        application.save!

    end

    def all_payments_complete?(subscription)
        stripe_plan_id = subscription.stripe_plan_id
        application = application_for_stripe_plan_id(stripe_plan_id)
        application.total_num_successful_payments >= application.total_num_required_stripe_payments
    end

    def after_subscription_reconciled_with_stripe(subscription)

        stripe_plan_id = subscription.stripe_plan_id

        application = application_for_stripe_plan_id(stripe_plan_id)

        return if Rails.env.test? && !application

        set_total_num_default_value_if_changing_plans(application, stripe_plan_id)

        # if you have a subscription, then you are registered
        application.registered = true
        application.stripe_plan_id = stripe_plan_id

        application.save! if application.changed?
    end

    def application_for_stripe_plan_id(stripe_plan_id)
        application = application_for_relevant_cohort

        if !application || !application.stripe_plans || !application.stripe_plan_ids.include?(stripe_plan_id)
            application = self.cohort_applications.detect { |app| app.stripe_plan_ids&.include?(stripe_plan_id) }
        end
        application
    end

    def subscription_slack_channel
        Slack.cohort_payments_channel
    end

    def subscription_identifier_for_slack(subscription, stripe_subscription)
        if subscription
            identifier = application_for_stripe_plan_id(subscription.stripe_plan_id).published_cohort.name
        elsif stripe_subscription&.metadata && stripe_subscription.metadata['relevant_cohort_id']
            identifier = Cohort.find_by_id(stripe_subscription.metadata['relevant_cohort_id'])&.name
        else
            identifier = "UNKNOWN"
        end
        identifier
    end

    def raise_if_duplicate_subscription!(*args)
        # for users, all subscriptions are active subscriptions, so this is all
        # we have to do
        raise_if_duplicate_primary_subscription!
    end

    def potential_primary_subscriptions
        # Students can only have 1 subscription, so any subscription they have could be the primary_subscription
        # one.  Eventually we will hopefully move
        # subscriptions so they are associated with particular programs.  At that point, there will probably
        # no longer be an primary_subscription for users.
        #
        # Do not use `.first` here, since that would always make a query and result in a new copy of the subscription
        subscriptions
    end

    private
    def set_total_num_default_value_if_changing_plans(application, new_stripe_plan_id)

        # if the user is coming from a different plan because they are fully refunded, then reset # payments
        changing_plans = application.stripe_plan_id.present? && application.stripe_plan_id != new_stripe_plan_id
        all_charges_refunded = application.num_charged_payments.to_i > 0 && application.num_charged_payments == application.num_refunded_payments
        if changing_plans && !all_charges_refunded
            raise "Unable to change plans without being fully refunded!"
        end

        # Default Stripe plan total # required if not already set or changing plans
        if application.total_num_required_stripe_payments.nil? || changing_plans
            application.set_total_num_default_value(new_stripe_plan_id)
        end
    end

    private
    def cohort_application_for_subscription(subscription)
        application_for_stripe_plan_id(subscription.plan.id)
    end

    # this is tested in ensure_stripe_customer_current spec in subscription_spec.rb
    private
    def change_email_in_stripe
        ensure_stripe_customer_current

        if hiring_team&.owner == self
            hiring_team.owner = self # we need to make sure that the hiring team references this object so it knows about the changes
            hiring_team.ensure_stripe_customer_current
        end
    end

end
