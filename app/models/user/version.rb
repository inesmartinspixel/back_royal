# == Schema Information
#
# Table name: users_versions
#
#  version_id                                      :uuid             primary key
#  operation                                       :string(1)        not null
#  version_created_at                              :datetime         not null
#  id                                              :uuid             not null
#  email                                           :string(255)
#  sign_in_count                                   :integer
#  current_sign_in_at                              :datetime
#  last_sign_in_at                                 :datetime
#  current_sign_in_ip                              :string(255)
#  last_sign_in_ip                                 :string(255)
#  created_at                                      :datetime         not null
#  updated_at                                      :datetime         not null
#  failed_attempts                                 :integer
#  unlock_token                                    :string(255)
#  locked_at                                       :datetime
#  sign_up_code                                    :string(255)
#  provider                                        :string(255)
#  uid                                             :string(255)      not null
#  has_seen_welcome                                :boolean          default(FALSE)
#  notify_email_newsletter                         :boolean
#  school                                          :text
#  provider_user_info                              :json
#  free_trial_started                              :boolean
#  confirmed_profile_info                          :boolean
#  pref_decimal_delim                              :string
#  pref_locale                                     :string
#  active_playlist_locale_pack_id                  :uuid
#  avatar_url                                      :text
#  additional_details                              :json
#  mba_content_lockable                            :boolean          default(FALSE)
#  job_title                                       :text
#  phone                                           :text
#  country                                         :text
#  has_seen_accepted                               :boolean          default(FALSE)
#  name                                            :text
#  nickname                                        :text
#  phone_extension                                 :text
#  can_edit_career_profile                         :boolean
#  professional_organization_option_id             :uuid
#  pref_show_photos_names                          :boolean
#  identity_verified                               :boolean
#  pref_keyboard_shortcuts                         :boolean
#  sex                                             :text
#  ethnicity                                       :text
#  race                                            :text             is an Array
#  how_did_you_hear_about_us                       :text
#  anything_else_to_tell_us                        :text
#  birthdate                                       :date
#  address_line_1                                  :text
#  address_line_2                                  :text
#  city                                            :text
#  state                                           :text
#  zip                                             :text
#  last_seen_at                                    :datetime
#  fallback_program_type                           :string
#  program_type_confirmed                          :boolean
#  invite_code                                     :string
#  notify_hiring_updates                           :boolean
#  notify_hiring_spotlights                        :boolean
#  notify_hiring_content                           :boolean
#  notify_hiring_candidate_activity                :boolean
#  hiring_team_id                                  :uuid
#  referred_by_id                                  :uuid
#  has_seen_featured_positions_page                :boolean
#  pref_one_click_reach_out                        :boolean
#  has_seen_careers_welcome                        :boolean
#  can_purchase_paid_certs                         :boolean
#  has_seen_hide_candidate_confirm                 :boolean
#  pref_positions_candidate_list                   :boolean
#  skip_apply                                      :boolean
#  has_seen_student_network                        :boolean
#  pref_student_network_privacy                    :text
#  notify_hiring_spotlights_business               :boolean
#  notify_hiring_spotlights_technical              :boolean
#  has_drafted_open_position                       :boolean
#  deactivated                                     :boolean
#  avatar_id                                       :uuid
#  has_seen_first_position_review_modal            :boolean
#  timezone                                        :text
#  pref_allow_push_notifications                   :boolean
#  english_language_proficiency_comments           :text
#  english_language_proficiency_documents_type     :text
#  english_language_proficiency_documents_approved :boolean
#  transcripts_verified                            :boolean
#  has_logged_in                                   :boolean
#  has_seen_mba_submit_popup                       :boolean
#  has_seen_short_answer_warning                   :boolean
#  version_editor_id                               :uuid
#  version_editor_name                             :text
#  recommended_positions_seen_ids                  :uuid             is an Array
#  allow_password_change                           :boolean
#  notify_candidate_positions                      :boolean
#  notify_candidate_positions_recommended          :text
#  has_seen_hiring_tour                            :boolean
#  academic_hold                                   :boolean
#  experiment_ids                                  :string           is an Array
#  enable_front_royal_store                        :boolean
#  pref_sound_enabled                              :boolean
#  student_network_email                           :string(255)
#

class User::Version < User
    include VersionMixin

    self.primary_key = 'version_id'
    self.table_name = 'users_versions'

    belongs_to :user, :foreign_key => 'id', :primary_key => 'id'
end
