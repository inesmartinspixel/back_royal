class User::BatchUpdater

    attr_reader :user_ids, :options, :users, :errors, :current_user

    def initialize(current_user, user_ids, options)
        @current_user = current_user
        @user_ids = user_ids
        @options = options.unsafe_with_indifferent_access
        @errors = Hash.new { |hash, user_id| hash[user_id] = [] }
    end

    def exec
        existing_user_ids = User.where(id: user_ids).pluck('id')
        missing_user_ids = user_ids - existing_user_ids
        if missing_user_ids.any?
            raise RuntimeError.new("Cannot find all users")
        end

        query = User.where(id: user_ids)

        if updates_cohort_status?
            query = query.includes(:cohort_applications => :cohort)
        end

        if updates_graduation_status?
            query = query.includes(:cohort_applications => :cohort)
        end

        query.find_each(batch_size: 100) do |user|

            # We cannot set the transaction up in the controller here, since we want a new transaction
            # for each batch, so we had to push the editor tracking down to here.
            EditorTracking.transaction(self.current_user) do
                update_cohort_status(user) if updates_cohort_status?
                update_graduation_status(user) if updates_graduation_status?


                should_save_user = false

                if updates_can_edit_career_profile?
                    user.can_edit_career_profile = options['can_edit_career_profile']
                    should_save_user = true
                end

                begin
                    user.save! if should_save_user
                rescue ActiveRecord::RecordInvalid => e
                    errors[user.id] << e.message.to_s
                end
            end
        end

        if (query.size - errors.size) > 0
            # we need to ensure CohortApplicationsPlus view is rebuilt
            RefreshMaterializedInternalReportsViews.perform_later_with_debounce
        end

        self
    end

    private
    def updates_can_edit_career_profile?
        [true, false].include?(options['can_edit_career_profile'])
    end

    private
    def updates_cohort_status?
       cohort_id && cohort_status
    end

    private
    def updates_graduation_status?
        cohort_id && graduation_status
    end

    private
    def cohort_id
        options['cohort_id']
    end

    private
    def cohort
        unless defined? @cohort
            @cohort = cohort_id ? Cohort.find(cohort_id) : nil
        end
        @cohort
    end

    private
    def cohort_status
        if options['cohort_status'] == 'UNCHANGED'
            nil
        elsif options['cohort_status']
            options['cohort_status']
        end
    end

    private
    def graduation_status
        if options['graduation_status'] == 'UNCHANGED'
            nil
        elsif options['graduation_status']
            options['graduation_status']
        end
    end

    # Updates the cohort status on the existing application for the specified cohort. If no existing cohort
    # application exists, a new one will be created for the user; provided an appropriate status is given.
    # Should the new status update results in a semantically invalid update (e.g. expelling a user that hasn't
    # even been accepted into the program yet), an error associated with the provided user will be added to
    # the errors instance variable.
    private
    def update_cohort_status(user)
        existing_accepted_or_limbo_application = user.application_for_relevant_cohort
        existing_application_for_cohort = user.cohort_applications.detect { |app| app.cohort_id == cohort_id }
        existing_status = existing_application_for_cohort && existing_application_for_cohort.status
        new_status = cohort_status

        # Error handling for users with existing applications not for the specified cohort
        # that are 'accepted', 'pending', 'pre_accepted'.
        if existing_accepted_or_limbo_application && existing_accepted_or_limbo_application.cohort_id != cohort_id
            errors[user.id] << "#{user.account_id} has an existing #{existing_accepted_or_limbo_application.status} application for #{existing_accepted_or_limbo_application.published_cohort.name}"

        # Create a new cohort application for the user if no existing cohort application exists and an appropriate status is given.
        elsif existing_application_for_cohort.nil? && ['pending', 'accepted', 'pre_accepted'].include?(new_status)
            create_cohort_application(user)

        # Error handling for tyring to defer, reject, or expel a user when they have no existing cohort application.
        elsif existing_application_for_cohort.nil? && ['deferred', 'rejected', 'expelled'].include?(new_status)
            errors[user.id] << "#{user.account_id} does not have an application for cohort #{cohort.name}"

        # Error handling for trying to unexpel a user by setting their status to anything other than expelled.
        elsif existing_status == 'expelled' && new_status != 'expelled'
            errors[user.id] << "Cannot update status to #{new_status} because #{user.account_id} has been expelled."

        # Error handling for trying to expel a user that has not yet been accepted.
        elsif ['pending', 'rejected', 'pre_accepted'].include?(existing_status) && new_status == 'expelled'
            errors[user.id] << "Cannot expel #{user.account_id} from cohort #{cohort.name} because he/she is not yet accepted."

        # Error handling for trying to update the status from pre_accepted to anything but accepted, rejected, or itself.
        elsif existing_status == 'pre_accepted' && ['pending', 'deferred', 'expelled'].include?(new_status)
            errors[user.id] << "#{user.account_id} must have their status updated to accepted or rejected; not #{new_status}"

        # If we get to this point, we should have accounted for all possible errors, so we can now update the
        # existing cohort application status if it needs to be changed.
        elsif existing_status != new_status
            new_status = (new_status == 'rejected' ? existing_application_for_cohort.rejected_or_expelled : new_status)
            existing_application_for_cohort.status = new_status
            existing_application_for_cohort.set_manual_admin_decision_if_applicable
            begin
                existing_application_for_cohort.save!
            rescue ActiveRecord::RecordInvalid => e
                errors[user.id] << e.message.to_s
            end
        end
    end

    private
    def update_graduation_status(user)
        existing_application = user.cohort_applications.detect { |app| app.cohort_id == cohort_id }
        raise "No cohort_application found for user #{user.id}" unless existing_application.present?

        existing_application.graduation_status = graduation_status

        begin
            existing_application.save!
        rescue ActiveRecord::RecordInvalid => e
            errors[user.id] << e.message.to_s
        end
    end

    private
    def create_cohort_application(user)
        cohort_application = CohortApplication.new(
            cohort_id: cohort_id,
            status: cohort_status,
            applied_at: Time.now,

            # since the cohort_application might update the user, we need to
            # ensure it's the same user object that we're using
            user: user
        )

        cohort_application.set_manual_admin_decision_if_applicable

        user.cohort_applications << cohort_application

        begin
            user.save!
        rescue ActiveRecord::RecordInvalid => e
            errors[user.id] << e.message.to_s
        end
    end

end