class User::ProgressMerger

    attr_reader :from_user, :to_user

    def initialize(from_user, to_user)
        @from_user = from_user
        @to_user = to_user
        @to_user_progress = {}
    end

    def merge!
        # Automatically retries in the event of ActiveRecord::PreparedStatementCacheExpired
        RetriableTransaction.transaction do

            # This worked for https://trello.com/c/5UkdLAsA/1061-feat-one-off-rake-task-to-merge-a-troubling-user-for-georgetown
            # but is not a complete general solution for the following reasons:
            # 1. If the to_user has done some of the lessons in a stream and
            #    the from_user has done the rest, it will not be marked as
            #    complete
            # 2. events in segment are not transferred
            # 3. stream certificates are not regenerated with the to_user's name, etc.
            #    (probably never worth worrying about)


            # Here is a ticket for finishing this functionality.  Some of
            # the code is already started below for generalizing this

            # In this particular case, the to_user has started a stream that the from_user
            # has completed, so we delete that one first from the to_user.
            [LessonProgress, Lesson::StreamProgress].each do |klass|

                # delete any progress records that are completed on
                # the from_user but not on the to_user
                klass.connection.execute("
                    delete from #{klass.table_name}
                    where
                        user_id='#{to_user.id}'
                        and locale_pack_id in (
                            select locale_pack_id
                            from #{klass.table_name}
                            where user_id='#{from_user.id}'
                                and completed_at is not null
                        )
                        and completed_at is null
                ")

                # copy progress records over, skipping ones
                # that already exist on the to_user
                klass.connection.execute("
                    update #{klass.table_name}
                    set user_id='#{to_user.id}'
                    where
                        user_id='#{from_user.id}'
                        and locale_pack_id not in (
                            select locale_pack_id
                            from #{klass.table_name}
                            where user_id='#{to_user.id}'
                        )
                    ")
            end

            # copy events over
            Event.connection.execute("
                update events
                set user_id='#{to_user.id}'
                where user_id='#{from_user.id}'
            ")

            from_user.destroy!

            # transfer_progress(:lesson_progresses)
            # transfer_progress(:lesson_stream_progresses)
            # add_missing_stream_progress
            # transfer_events
        end
    end

    # def transfer_progress(meth)
    #     to_user_records = to_user_progress(meth)
    #     from_user_records = from_user.send(meth).index_by(&:locale_pack_id)

    #     records_to_transfer = []
    #         from_user_records.each do |locale_pack_id, from_user_record|
    #             to_user_record = to_user_records[locale_pack_id]
    #             if to_user_record.nil? || (to_user_record.completed_at.nil? && from_user_record.completed_at.present?)
    #                 records_to_transfer << from_user_record
    #             end
    #         end
    #     end

    #     records_to_transfer.each do |record|
    #         records_to_transfer.user_id = to_user.id
    #         records_to_transfer.save!
    #     end
    # end

    # def to_user_progress(meth)
    #     @to_user_progress[meth] ||= to_user.send(meth).index_by(&:locale_pack_id)
    # end

    # def add_missing_stream_progress
    #     streams = Lesson::Stream.ToJsonFromApiParams.new({
    #         user_id: to_user.id,
    #         filters: {
    #             in_users_groups: true,
    #             in_users_locale_or_en: true,
    #             published: true
    #         },
    #         fields: ['locale_pack', 'chapters', 'lessons'],
    #         lesson_fields: ['locale_pack']
    #     }).to_a

    #     streams.each do |stream|
    #         stream_progress = to_user_progress(:lesson_stream_progresses)[stream['locale_pack']['id']]

    #         # if there is already a completed stream progress, then we
    #         # don't need to do anything
    #         next if stream_progress && stream_progress.completed_at


    #         lesson_progresses_for_stream = lesson_locale_pack_ids.detect do |lesson_locale_pack_id|
    #             to_user_progress(:lesson_progresses)[lesson_locale_pack_id]
    #         end
    #         # if there is no lesson progress for this stream, then we don't need
    #         # to do anything
    #         next unless lesson_progresses_for_stream.any?

    #         started_at = lesson_progresses_for_stream.map(&:started_at).min
    #         completed_at = lesson_progresses_for_stream.map(&:completed_at).max

    #         # FIXME: I got to here and then stopped because I realized it was
    #         # not necessary for the one user we cared about

    #     end
    # end

end