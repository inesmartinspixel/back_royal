# A syncable record is one that we sync to Airtable and store a corresponding airtable_record_id for.
module Airtable::Syncable

    # Since the airtable_record_id changes when duplicating the Airtable base it will be
    # different in our staging and local environments, so let's fall back to a filter call
    # in those environments while still retaining the speed boost from a find call in production.
    # Also see https://trello.com/c/Qgo4XgHk regaring an edge case where a record could be
    # created on Airtable's side but error before we have a chance to sync the airtable_record_id
    # to our side.
    def find_by_id_with_fallback(database_record)
        # Airrecord expects a base_key and table_name in order for API
        # calls to Airtable to work.
        # If we're somehow calling into this method with a class that is
        # missing either, or both, of those attributes, we should catch that
        # and raise here.
        if !self.base_key.present? || !self.table_name.present?
            raise "find_by_id_with_fallback called without base_key or table_name"
        end

        # The `find_by_id` method will swallow any errors from Airtable, including errors about
        # not being able to find the Airtable base. Other parts of the app rely on the fallback
        # to the `all` method to raise an error in such cases, so it's important that this logic
        # not rescue any errors from the `all` method.
        return self.find_by_id(database_record.airtable_record_id) ||
            self.all(filter: "{#{self.database_id_column}} = '#{database_record[:id]}'").first # use id symbol because it could be a version
    end

    def find_or_create(database_record, editor_tracking_user)
        return self.find_by_id_with_fallback(database_record) || self.create(database_record, editor_tracking_user)
    end

    # This method will silently swallow ANY error from Airtable and return `nil` if an error occurs.
    # This means that if the Airtable base itself couldn't be found, then we return `nil`, which may
    # insinuate that we searched for the record in the base but couldn't find it, when really we
    # couldn't find the Airtable base to begin with. Unfortunately, a 404 error for failing to find
    # a specific record in the Airtable base looks exactly the same as a 404 error for failing to
    # find the Airtable base, so we can't rely on the error messages to handle each case appropriately.
    def find_by_id(airtable_record_id)
        return nil if airtable_record_id.nil?

        airtable_record = nil
        begin
            airtable_record = self.find(airtable_record_id)
        rescue Airrecord::Error
            return nil
        end

        airtable_record
    end

    def create(database_record, editor_tracking_user)
        airtable_record = self.new(self.get_airtable_hash(database_record))
        airtable_record.create

        # Avoid including the API request to create the Airtable record in the EditorTracking
        # transaction. Otherwise, we may end up with a long-running transaction if the request
        # takes a long time.
        EditorTracking.transaction(editor_tracking_user) do
            database_record.update_column(:airtable_record_id, airtable_record.id)
        end
        return airtable_record
    end

    def update(database_record)
        airtable_application = self.find_by_id_with_fallback(database_record)

        return nil if airtable_application.nil?

        airtable_hash = self.get_airtable_hash(database_record)
        airtable_hash.each do |k, v|
            airtable_application[k] = v
        end

        airtable_application.save
        return airtable_application
    end

    def destroy(database_record)
        return self.find_by_id_with_fallback(database_record)&.destroy
    end
end