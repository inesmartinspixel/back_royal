class Airtable::Company < Airrecord::Table

    self.base_key = ENV['AIRTABLE_BASE_KEY_LIVE']
    self.table_name = "Companies"

    def self.find_by_name(name)
        escaped_name = name.gsub("'","\\\\'") # things like Queen's College will break otherwise
        return self.all(filter: "{Name} = '#{escaped_name}'").first
    end
end