class Airtable::Application < Airrecord::Table
    extend Airtable::Syncable

    self.base_key = ENV['AIRTABLE_BASE_KEY_LIVE']
    self.table_name = 'Applications'

    def self.klass_for_base_key(airtable_base_key)

        if !airtable_base_key.start_with?('AIRTABLE_BASE_KEY_')
            raise "Unexpected airtable_base_key - #{airtable_base_key}"
        elsif !ENV[airtable_base_key].present? && !is_staging?
            raise "No ENV variable for - #{airtable_base_key}"
        end

        return self if airtable_base_key == 'AIRTABLE_BASE_KEY_LIVE'

        # each time a new airtable_base_key is passed into this method,
        # subclass Airtable::Application to create a new class that is pointing
        # at the requested base
        @archives ||= Hash.new do |hash, _airtable_base_key|
            archive_klass = hash[_airtable_base_key] = self.create_subclass
            archive_klass.base_key = ENV[_airtable_base_key]
            archive_klass.table_name = self.table_name
            archive_klass
        end
        @archives[airtable_base_key]
    end

    def self.create_subclass
        return Class.new(self)
    end

    def self.database_id_column
        return "Application ID"
    end

    def self.is_staging?
        ENV['APP_ENV_NAME']&.match?(/staging/i)
    end

    # Sometimes we hit situations where we need to fix the decision made in Airtable.
    #
    # For example:
    #   If the admissions_decision was set to nil by our system (e.g., when a promotable cohort
    #   application user reapplies) then we need to sync this with the "Decision" and "Dev - Synced Decision"
    #   columns in Airtable. See https://trello.com/c/UtftVAYt
    # Or
    #   If the admissions_decision was accidentally set to "Reject for No Interview" when really
    #   the interview had been conducted then we need to fix it in Airtable.
    #   See https://trello.com/c/8N32IZaX
    def self.fix_decision(cohort_application, new_decision)
        airtable_application = self.find_by_id_with_fallback(cohort_application)
        if airtable_application
            airtable_application["Decision"] = new_decision
            airtable_application["Dev - Synced Decision"] = new_decision
            airtable_application.save
        end
        return airtable_application
    end

    def self.get_airtable_hash(cohort_application)
        database_hash = cohort_application.as_json({:airtable => true})
        airtable_hash = {}

        # Map database hash fields to Airtable hash fields
        database_hash.each do |key, value|
            airtable_key = self.column_map[key]

            next unless airtable_key.present?

            # Handle booleans. This is a bit annoying, but Airtable API doesn't seem to like booleans. Could maybe use the
            # typecast option to keep from doing this.
            if !!value == value # https://stackoverflow.com/a/3033645/1747491
                value = value.to_s
            end

            if value.nil?
                airtable_hash[airtable_key] = nil
            # For education experiences, work experiences, and cities we set two columns for the name.
            # One to link them to the whitelist table for use in Airtable function and rollup columns, and
            # another to just display the value in plaintext for when it is not linked.
            elsif /education_\d_name/.match?(key)
                university = Airtable::University.find_by_name(value)
                airtable_hash[airtable_key] = university.present? ? [university.id] : nil
            elsif /work_\d_name/.match?(key)
                company = Airtable::Company.find_by_name(value)
                airtable_hash[airtable_key] = company.present? ? [company.id] : nil
            elsif key == "city"
                city = Airtable::City.find_by_name(value)
                airtable_hash[airtable_key] = city.present? ? [city.id] : nil
            else
                airtable_hash[airtable_key] = value
            end

            # Set the plaintext, unlinked "name" field for education, work, and city entries
            if /education_\d_name/.match?(key) || /work_\d_name/.match?(key) || key == "city"
                airtable_hash["#{airtable_key} name"] = value
            end
        end

        if cohort_application.admissions_decision == "manual_admin_decision"
            # If the admission_decision was made manually by an admin then we need to ensure that we set
            # the "Decision" column in Airtable (in addition to the "Dev - Synced Decision" column)
            airtable_hash["Decision"] = "manual_admin_decision"
        end

        return airtable_hash
    end

    private

    # Maps database fields to their corresponding column names in Airtable
    def self.column_map

        # NOTE: before adding cohort_slack_room_id to here, see note in cohort_application.rb: `after_update :update_in_airtable`

        return {
            "name" => "Name",
            "email" => "Email address",
            # Age is computed
            "cohort_name" => "Cohort Name",
            "program_type" => "Program Type",
            "city" => "City",
            "state" => "State",
            "country" => "Country",
            "phone" => "Phone number",
            "birthdate" => "Birthdate",
            "nickname" => "Nickname",
            "education_1_name" => "1 University/College",
            "education_1_graduation_year" => "1 Graduation year (or anticipated graduation year)",
            "education_1_degree" => "1 Degree (BA, BSc, MA, etc)",
            "education_1_major" => "1 Major (or anticipated Major)",
            "education_1_minor" => "1 Minor (optional)",
            "education_1_gpa" => "1 GPA",
            "education_1_gpa_description" => "1 GPA description",
            "education_1_degree_program" => "1 Degree program",
            "education_1_will_not_complete" => "1 Will Not Complete",
            "education_2_name" => "2 University/College",
            "education_2_graduation_year" => "2 Graduation year (or anticipated graduation year)",
            "education_2_degree" => "2 Degree (BA, BSc, MA, etc)",
            "education_2_major" => "2 Major (or anticipated Major)",
            "education_2_minor" => "2 Minor (optional)",
            "education_2_gpa" => "2 GPA",
            "education_2_gpa_description" => "2 GPA description",
            "education_2_degree_program" => "2 Degree program",
            "education_2_will_not_complete" => "2 Will Not Complete",
            "education_3_name" => "3 University/College",
            "education_3_graduation_year" => "3 Graduation year (or anticipated graduation year)",
            "education_3_degree" => "3 Degree (BA, BSc, MA, etc)",
            "education_3_major" => "3 Major (or anticipated Major)",
            "education_3_minor" => "3 Minor (optional)",
            "education_3_gpa" => "3 GPA",
            "education_3_gpa_description" => "3 GPA description",
            "education_3_degree_program" => "3 Degree program",
            "education_3_will_not_complete" => "3 Will Not Complete",
            "education_4_name" => "4 Institution",
            "education_4_graduation_year" => "4 Completion year (or anticipated completion year)",
            "education_4_major" => "4 Program Name",
            "education_4_degree_program" => "4 Degree program",
            "education_4_will_not_complete" => "4 Will Not Complete",
            "education_5_name" => "5 Institution",
            "education_5_graduation_year" => "5 Completion year (or anticipated completion year)",
            "education_5_major" => "5 Program Name",
            "education_5_degree_program" => "5 Degree program",
            "education_5_will_not_complete" => "5 Will Not Complete",
            "education_6_name" => "6 Institution",
            "education_6_graduation_year" => "6 Completion year (or anticipated completion year)",
            "education_6_major" => "6 Program Name",
            "education_6_degree_program" => "6 Degree program",
            "education_6_will_not_complete" => "6 Will Not Complete",
            "work_1_name" => "1 Company or organization",
            "work_1_job_title" => "1 What was your title at that job?",
            "work_1_industry" => "1 Industry",
            "work_1_role" => "1 Role",
            "work_1_responsibilities" => "1 Please also briefly describe your roles and responsibilities.",
            "work_1_length_string" => "1 How long were you at that company or organization?",
            "work_1_employment_type" => "1 Employment type",
            "work_2_name" => "2 Company or organization",
            "work_2_job_title" => "2 What was your title at that job?",
            "work_2_industry" => "2 Industry",
            "work_2_role" => "2 Role",
            "work_2_responsibilities" => "2 Please also briefly describe your roles and responsibilities.",
            "work_2_length_string" => "2 How long were you at that company or organization?",
            "work_2_employment_type" => "2 Employment type",
            "work_3_name" => "3 Company or organization",
            "work_3_job_title" => "3 What was your title at that job?",
            "work_3_industry" => "3 Industry",
            "work_3_role" => "3 Role",
            "work_3_responsibilities" => "3 Please also briefly describe your roles and responsibilities.",
            "work_3_length_string" => "3 How long were you at that company or organization?",
            "work_3_employment_type" => "3 Employment type",
            "work_4_name" => "4 Company or organization",
            "work_4_job_title" => "4 What was your title at that job?",
            "work_4_industry" => "4 Industry",
            "work_4_role" => "4 Role",
            "work_4_responsibilities" => "4 Please also briefly describe your roles and responsibilities.",
            "work_4_length_string" => "4 How long were you at that company or organization?",
            "work_4_employment_type" => "4 Employment type",
            "awards_and_interests" => "Awards & Interests",
            "score_on_gmat" => "GMAT score",
            "score_on_sat" => "SAT score",
            "sat_max_score" => "SAT max score",
            "score_on_act" => "ACT score",
            "score_on_gre_verbal" => "GRE verbal",
            "score_on_gre_quantitative" => "GRE quantitative",
            "score_on_gre_analytical" => "GRE analytical",
            "primary_reason_for_applying" => "What is the primary reason you are applying for a Smartly MBA?",
            "short_answer_greatest_achievement" => "What do you consider your greatest achievement so far? Why?",
            "short_answer_leadership_challenge" => "Describe a specific leadership or management challenge you faced. How did you approach and ultimately resolve it?",
            "short_answer_why_pursuing" => "Why MBA and Smartly",
            "short_answer_use_skills_to_advance" => "How They Plan to Use Skills",
            "short_answer_ask_professional_advice" => "Professional Advice - Who, What, Why",
            "has_no_formal_education" => "No Formal Degree",
            "li_profile_url" => "LinkedIn Profile (link)",
            "salary_range" => "Salary Range",
            "sex" => "Sex",
            "race" => "Race",
            "how_did_you_hear_about_us" => "How did you hear about Smartly?",
            "anything_else_to_tell_us" => "Is there anything else you would like to tell us?",
            "start_date" => "Start Date (UTC)",
            "submit_date" => "Submit Date",
            "id" => "Application ID",
            "user_id" => "User ID",

            "status" => "Application Status",
            "graduation_status" => "Graduation Status",
            "career_profile_active" => "Career Profile Active?",
            "willing_to_relocate" => "Willing to Relocate?",
            "open_to_remote_work" => "Open to Remote Work?",
            "authorized_to_work_in_us" => "Authorized to Work in the US?",
            "interested_in_joining_new_company" => "Interested in Joining New Company",
            "locations_of_interest" => "Locations of Interest",
            "employment_types_of_interest" => "Employment Types of Interest",
            "company_sizes_of_interest" => "Company Sizes of Interest",
            "primary_areas_of_interest" => "Primary Areas of Interest",
            "completed_lessons" => "Completed Lessons",
            "editor_url" => "Editor URL",
            "deep_link_url" => "Deep Link URL",
            "profile_feedback" => "Profile Feedback",
            "feedback_last_sent_at" => "Feedback Sent At",
            "admissions_decision" => "Dev - Synced Decision",
            "converted_to_emba" => "Successfully Converted to EMBA",
            "last_confirmed_at_by_student" => "Profile Last Confirmed By Candidate At",
            "last_updated_at_by_student" => "Profile Last Updated By Candidate At",
            "last_confirmed_at_by_internal" => "Profile Last Confirmed By Internal At",
            "short_answer_scholarship" => "Scholarship Circumstances",
            "registered" => "Registered?",
            "registered_date" => "Registered Date (UTC)",
            "invited_to_interview_date" => "Invited to Interview Date (UTC)",
            "sufficient_english_work_experience" => "Sufficient English Work Experience?",
            "english_work_experience_description" => "English Work Experience Description",
            "prior_applications_string" => "Prior Applications",
            "prior_applications_decisions_string" => "Prior Decisions",
            "deferred_and_good_standing" => "Deferred?",
            "resume_url" => "Resume URL",
            "place_details_formatted_address" => "Google Location",
            "survey_years_full_time_experience" => "Years of Experience",
            "survey_highest_level_completed_education_description" => "Education Level",
            "survey_most_recent_role_description" => "Position Level",
            "long_term_goal" => "Long Term Goal",
            "original_program_type" => "Original Program Type"

            # ... plus additional fields that are used in Airtable only
        }
    end

    # Maps database fields whose data originates from Airtable
    def self.from_airtable_column_map
        return {
            "rubric_round_1_tag" => "Round 1 Tag",
            "rubric_round_1_reason_mba_invite" => "Round 1 Reason - MBA Invite",
            "rubric_round_1_reason_mba_reject" => "Round 1 Reason - MBA Reject",
            "rubric_round_1_reason_emba_invite" => "Round 1 Reason - EMBA Invite",
            "rubric_round_1_reason_emba_reject" => "Round 1 Reason - EMBA Reject",
            "rubric_final_decision" => "Final Decision",
            "rubric_final_decision_reason" => "Final Decision Reason",
            "rubric_interviewer" => "Interviewer",
            "rubric_interview" => "Interview",
            "rubric_interview_scheduled" => "Interview Scheduled",
            "rubric_interview_recommendation" => "Interview Recommendation",
            "rubric_reason_for_declining" => "Reason for Declining",
            "rubric_likelihood_to_yield" => "Likelihood to Yield",
            "rubric_risks_to_yield" => "Risks to Yield",
            "rubric_interview_date" => "Interview Date",
            "rubric_motivation_score" => "Motivation Score",
            "rubric_contribution_score" => "Contribution Score",
            "rubric_insightfulness_score" => "Insightfulness Score"
        }
    end
end