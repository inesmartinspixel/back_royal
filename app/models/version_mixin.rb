module VersionMixin

    extend ActiveSupport::Concern

    included do
        updatable_klass = Class.new(ApplicationRecord)
        updatable_klass.send(:primary_key=,"version_id")
        updatable_klass.send(:table_name=, self.table_name)
        self.const_set(:Updatable, updatable_klass)
    end

    # by default, rails will map the 'id' method to the
    # primary_key field.  We do not want that, since we
    # have an id column, but it is not the primary field.
    def ==(other)
        return false unless self.class == other.class
        return true if self.object_id == other.object_id
        self.get_version_id == other.get_version_id
    end
    alias :eql? :==

    def hash
        version_id = get_version_id
        self.class.hash ^ version_id.hash
    end

    def get_version_id
        # this is way faster that attributes['id]
        val = @attributes['version_id'].value_before_type_cast
        raise "No version_id loaded" if val.nil?
        val
    end

    def get_id
        # this is way faster that attributes['id]
        val = @attributes['id'].value_before_type_cast
        raise "No id loaded" if val.nil?
        val
    end

    def id
        raise NotImplementedError.new("Due to ActiveRecord assumptions, we cannot make the id method safe to use.  It will always be ambiguouse.  Use attributes['id'] or version_id.")
    end

    def id=(val)
        raise NotImplementedError.new("Due to ActiveRecord assumptions, we cannot make the id method safe to use.  It will always be ambiguouse.  Use attributes['id'] or version_id.")
    end

    def [](val)
        val.to_s == 'id' ? self.attributes['id'] : super
    end

    def []=(key, val)
        key.to_s == 'id' ? self.attributes['id']=(val) : super
    end

     # We have cases in the db where we created versions without bumping the
    # updated_at, so in general we have to rely on the version_created_at.
    # However, we also have cases where two versions were created in a single
    # transaction, in which case they would have the same version_created_at.
    # so we rely on the updated_at for sorting.
    def <=>(other_version)
        if self.version_created_at == other_version.version_created_at
            self.updated_at <=> other_version.updated_at
        else
            self.version_created_at <=> other_version.version_created_at
        end
    end

    def commit_changes
        if has_attribute?('content_json')
            reconcile_content_json
            # force an update to happen even if active record thinks
            # that nothing has changed, since it does not check to see if
            # the CONTENTS of content_json has changed.  It only checks for
            # identity changes
            content_json_will_change!
        end

        updatable_instance = self.class.find_by_version_id(version_id)

        changes = self.attributes.slice(*self.changes.keys)
        updatable_instance.update_columns(changes) unless changes.blank?
        changes
    end

    # The purpose of this method is to easily tell whether or not an arbitrary attribute
    # on a given ::Version record was set to the column_default of the superclass
    # (since _versions tables don't have defaults) when the record was inserted.
    def attr_changed_to_column_default_on_insert?(attr)
        column_defaults = self.class&.superclass&.column_defaults
        return self.operation == 'I' && !column_defaults.blank? && self[attr] == column_defaults[attr.to_s]
    end

end