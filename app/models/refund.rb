# == Schema Information
#
# Table name: refunds
#
#  id                      :uuid             not null, primary key
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  billing_transaction_id  :uuid             not null
#  provider                :text             not null
#  provider_transaction_id :text             not null
#  refund_time             :datetime         not null
#  amount                  :float            not null
#

class Refund < ApplicationRecord

    belongs_to :billing_transaction

    # see comment near BillingTransaction#warn_on_incorrect_amount_refunded
    # for why this is a before_commit
    before_commit :warn_on_incorrect_amount_refunded

    def self.find_or_create_by_stripe_refund(stripe_refund)
        # see comment near all_for_owner_id
        billing_transaction = BillingTransaction.where(
            provider: BillingTransaction::PROVIDER_STRIPE,
            provider_transaction_id: stripe_refund.charge
        ).first

        if billing_transaction.nil?
            if Stripe::Charge.retrieve(stripe_refund.charge).livemode
                Raven.capture_exception("Could not find billing_transaction while saving stripe refund", {
                    level: 'warning',
                    extra: {
                        refund: stripe_refund.as_json
                    }
                })
            end
            return
        end

        RetriableTransaction.transaction do
            record = billing_transaction.refunds.where(provider_transaction_id: stripe_refund.id).first_or_initialize
            record.update!(
                refund_time: Time.at(stripe_refund.created),
                amount: stripe_refund.amount / 100.0,
                provider: BillingTransaction::PROVIDER_STRIPE,
                provider_transaction_id: stripe_refund.id,
            )
            record.billing_transaction.calculate_amount_refunded
            record.billing_transaction.save!
            record
        end
    end

    def as_json(options = {})
        attributes.slice(*%w(id provider provider_transaction_id amount billing_transaction_id)).merge({
            created_at: created_at.to_timestamp,
            updated_at: updated_at.to_timestamp,
            refund_time: refund_time.to_timestamp
        }.stringify_keys)
    end

    def warn_on_incorrect_amount_refunded
        billing_transaction.warn_on_incorrect_amount_refunded
    end
end
