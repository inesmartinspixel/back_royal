# An instance of this class wraps a GlobalMetadata record,
# allowing for any of the attribtues on global metadata
# to be overridden

class ActionMetadata

    GlobalMetadata.column_names.each do |column|

        meth = column.gsub('default_', '').to_sym

        define_method(meth) do
            if instance_variable_get(:"@#{meth}").present?
                instance_variable_get(:"@#{meth}")
            elsif @global_metadata[column].present?
                @global_metadata[column]
            else
                default_metadata[column]
            end
        end

        define_method(:"#{meth}=") do |val|
            instance_variable_set(:"@#{meth}", val)
        end

    end

    def image
        if defined? @image
            @image
        elsif @global_metadata.default_image
            @global_metadata.default_image
        else
            default_metadata.default_image
        end
    end

    def image=(val)
        @image = val
    end

    def image_url
        image && image.formats["original"]["url"]
    end

    def initialize(global_metadata)
        @global_metadata = global_metadata || GlobalMetadata.new
    end

    def default_metadata
        @default_metadata ||= GlobalMetadata.find_by_site_name('__defaults') || GlobalMetadata.new
    end

end
