# == Schema Information
#
# Table name: global_metadata
#
#  id                    :uuid             not null, primary key
#  created_at            :datetime
#  updated_at            :datetime
#  site_name             :string
#  default_title         :string
#  default_description   :string
#  default_image_id      :uuid
#  default_canonical_url :string
#

class GlobalMetadata < ApplicationRecord
    self.table_name = "global_metadata"

    belongs_to :default_image, :class_name => 'S3Asset', optional: true

    validates_presence_of :site_name
    validates_uniqueness_of :site_name
    before_destroy :abort_destroy_if_not_destroyable

    # Get all controller/action combinations in
    # marketing, home, and blue_ocean_strategy.  Any
    # of these can have a GlobalMetadata record
    def self.available_actions
        Rails.application.routes.routes
        .select { |route|
            route.defaults[:controller] && route.defaults[:controller].match(/(marketing|home|blue_ocean|landing_page|idology)/)
        }.map { |route|
            site_name_for_action(route.defaults)
        }.uniq
    end

    def self.site_name_for_action(opts)
        unless (controller = opts[:controller]) && (action = opts[:action])
            raise ArgumentError.new("Expecting controller and action in #{opts.inspect}")
        end
        "#{controller}##{action}"
    end

    def self.find_by_action(opts)
        name = site_name_for_action(opts)
        where(site_name: name).first
    end

    def self.create_from_hash!(hash, meta = {})
        hash = hash.unsafe_with_indifferent_access
        meta = (meta || {}).unsafe_with_indifferent_access
        instance = new

        # handle associations and things
        instance.merge_hash(hash, meta)

        if instance.save!
            instance
        else
            raise ActiveRecord::RecordNotSaved.new("#{self.name} could not be saved: #{instance.errors.full_messages}")
        end

        instance
    end

    def self.update_from_hash!(hash, meta = {})
        hash = hash.unsafe_with_indifferent_access
        meta = (meta || {}).unsafe_with_indifferent_access
        instance = find_by_id(hash[:id])

        if instance.nil?
            raise ActiveRecord::RecordNotFound.new("Couldn't find #{self.name} with id=#{hash[:id]}")
        end

        instance.merge_hash(hash, meta)
        instance.save!
        instance

    end

    def merge_hash(hash, meta = {})
        hash = hash.clone.with_indifferent_access

        %w(site_name default_title default_description default_canonical_url).each do |key|
            self[key] = hash[key] if hash.key?(key)
        end

        if hash.key?('default_image') && hash['default_image'] && hash['default_image']['id']
            self.default_image = S3Asset.find(hash['default_image']['id'])
        else
            self.default_image = nil
        end

    end

    def as_json(opts = {})
        super(opts).merge({
            updated_at: self.updated_at.to_timestamp,
            created_at: self.created_at.to_timestamp,
            default_image: self.default_image.as_json,
            destroyable: destroyable?
        })
    end

    def get_image_asset(file)
        S3Asset.new({
            :file => file,
            :directory => "images/",

            # see also: https://github.com/thoughtbot/paperclip/wiki/Thumbnail-Generation
            # see also: image_model.js
            :styles => {}.to_json
        })
    end

    def destroyable?
        site_name != "__defaults"
    end

    def abort_destroy_if_not_destroyable
        raise "Cannot destroy #{site_name}" unless destroyable?
    end

end
