# == Schema Information
#
# Table name: published_cohorts
#
#  id                               :uuid
#  promoted                         :boolean
#  name                             :text
#  program_type                     :text
#  required_playlist_pack_ids       :uuid             is an Array
#  specialization_playlist_pack_ids :uuid             is an Array
#  start_date                       :datetime
#  end_date                         :datetime
#  grad_deadline                    :datetime
#  enrollment_deadline              :datetime
#  graduation_date                  :datetime
#  early_registration_deadline      :datetime
#  version_id                       :uuid
#
class PublishedCohort < ApplicationRecord
end
