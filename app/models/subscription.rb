# == Schema Information
#
# Table name: subscriptions
#
#  id                     :uuid             not null, primary key
#  created_at             :datetime
#  updated_at             :datetime
#  stripe_plan_id         :text             not null
#  stripe_subscription_id :text             not null
#  past_due               :boolean
#  stripe_product_id      :text             not null
#  cancel_at_period_end   :boolean          default(FALSE), not null
#  current_period_end     :datetime         not null
#

=begin

    == Stripe subscriptions and back_royal subscriptions

        Subscriptions exist in both our database and in Stripe. The
        main reason that we made our own local copy is for performance
        and convennience.

        In the normal case, a subscription is created when the
        # Stripe Checkout endpoint action is called on the subscriptions
        controller. In that case, we create the subscription in
        stripe and then create a copy in our database.

        So, there should be a record in our database reflecting each
        subscription in Stripe.  Some cases in which this will
        not be true:
        * The subscription was created in the Stripe dashboard but
            the webhook has not hit our servers yet.
        * The subscription was created in the Stripe dashboard but
            the webhook repeatedly failed when sent to our servers,
            until Stripe gave up and stopped sending it.
        * A subscription was created in the Stripe interface for a
            user or hiring team that does not exist in our database.
        * The subscription has been deleted from our database, and there
            has been no subsequent subscription.updated webhook from
            stripe that would cause us to re-create it.

        There is a one-to-many relationship between users and subscriptions,
        even though we only ever really expect a user to have one subscription.
        We allow the one-to-many relationship because stripe does.  If a user
        ever ends up with 2 subscriptions, we will log a message to Raven,
        but the user's account will continue to work, with the "primary_subscription".

    == Plans and metadata

        There are a few special metadata parameters that can be saved
        to a plan that we will respect:

        * sort_key: if and only if this is set, then the plan will show up in the
            list of available plans in the UI. In that list, plans will be sorted
            by the sort key.


=end

class Subscription < ApplicationRecord

    class CannotCreateDuplicateSubscription < RuntimeError; end
    class IncompleteSubscription < RuntimeError; end

    OwnsPayments.require_klasses_that_own_payments

    before_destroy :trigger_before_destroy_on_owner
    before_destroy :delete_stripe_subscription

    after_create :trigger_after_create_on_owner, unless: :fake_subscription?
    after_save :trigger_handle_past_due_changes_on_owner, if: Proc.new { !fake_subscription? && saved_change_to_past_due? }
    after_commit :trigger_after_commit_on_owner, on: [:create, :update, :destroy]
    after_commit :destroy_if_canceled, on: [:create, :update, :destroy]

    # not all subscriptions are associated with an open_position, but some are
    has_one :open_position

    # see comment near ensure_current_period_end about why this
    # is only run in test mode
    if Rails.env.test?
        before_save :ensure_current_period_end
    end

    validates_presence_of :owner

    def self.find_owner_by_customer_id(customer_id)
        User.find_by_id(customer_id) || HiringTeam.find_by_id(customer_id)
    end

    def self.create_or_reconcile_from_stripe_subscription!(owner, stripe_subscription)

        # never allow us to persist cancelled subscriptions
        if stripe_subscription.status == 'canceled' || stripe_subscription.status.starts_with?('incomplete')
            return
        end

        # reload the stripe subscription if we do not have all of the
        # necessary fields expanded
        unless expected_fields_expanded?(stripe_subscription)
            stripe_subscription = self.find_stripe_subscription(owner, stripe_subscription.id)
        end

        # since this method is likely to be called from the stripe event handler
        # before the subscriptions_controller commits its transaction, we need
        # support for retrying when both processes try to save a new subscription
        # for the same stripe_subscription
        #
        # There is no spec for this, because it cannot really be tested with only one connection,
        # and even if we somehow used 2 connections, we would have to actually
        # commit, which we don't do in specs
        retry_on_duplicate_key_error do

            # We have to make sure we've reloaded the association on the user.
            # Otherwise, on a retry `owner.primary_subscription = subscription` will try to
            # create a new join for the existing subscription and we will hit a duplicate
            # key error.  See https://trello.com/c/9BqrDPPd
            subscription = nil
            owner.subscriptions.reload

            # check for an existing subscription
            subscription = owner.subscriptions.detect { |sub| sub.stripe_subscription_id == stripe_subscription.id }

            unless subscription.present?

                subscription = new({
                    stripe_subscription_id: stripe_subscription.id,
                    stripe_plan_id: stripe_subscription.plan.id,
                    stripe_product_id: stripe_subscription.plan.product.id
                })

                if owner.is_a?(User)
                    subscription.user = owner
                elsif owner.is_a?(HiringTeam)
                    subscription.hiring_team = owner
                else
                    raise "Invalid owner: #{owner.class.name}"
                end
            end

            subscription.stripe_subscription = stripe_subscription
            subscription.reconcile_with_stripe
            subscription.save!
            owner.subscriptions.reload
            subscription
        end
    end

    def self.find_stripe_subscription(owner, stripe_subscription_id)
        begin
            owner.ensure_stripe_customer.subscriptions.retrieve({:id => stripe_subscription_id, :expand => ['plan', 'plan.product'] })
        rescue Stripe::InvalidRequestError => err
            raise err unless (err.message.match(/No such subscription/) || err.message.match(/does not have a subscription with/))
        end
    end

    def self.expected_fields_expanded?(stripe_subscription)
        stripe_subscription.plan.product.id.is_a?(String)
    rescue NoMethodError
        nil
    end

    def self.create_for_owner(owner:, stripe_plan_id:, coupon_id: nil, trial_end_ts: nil, metadata: {})
        # fire create and return stripe subscription
        stripe_subscription = create_stripe_subscription(owner, stripe_plan_id, coupon_id, trial_end_ts, metadata)
        self.create_or_reconcile_from_stripe_subscription!(owner, stripe_subscription)
    end

    # This is private.  We always want to call create_for_owner in the wild
    # The tests for this are in create_for_owner
    private
    def self.create_stripe_subscription(owner, stripe_plan_id, coupon_id=nil, trial_end_ts=nil, metadata = {})
        raise "No stripe customer" unless stripe_customer = owner.stripe_customer

        owner.raise_if_duplicate_subscription!(metadata)

        stripe_subscription_params = {
            plan: stripe_plan_id,
            coupon: coupon_id,
            expand:  ['plan', 'plan.product']
        }

        if trial_end_ts.present?
            stripe_subscription_params[:trial_end] = trial_end_ts
        else
            stripe_subscription_params[:trial_from_plan] = true
        end

        # fire create and return stripe subscription
        subscription = stripe_customer.subscriptions.create(stripe_subscription_params.merge(metadata: metadata))

        if subscription.status == 'incomplete'
            # An incomplete Subscription means that there was a failure charging the card at creation. We will not
            # be treating these like active subscriptions in our system, and will be pruned in StripeEventHandler.
            # NOTE: This error message is user-facing.  This is what the user will see in the modal popup (untranslated). (UPDATE: This message
            # is not user-facing on new clients.  SubscriptionsController now sets a message_key in this case.)
            raise Subscription::IncompleteSubscription.new("There was an issue charging the provided card. Please contact your card provider or try another payment source.")
        end

        subscription
    end

    private
    def self.retry_on_duplicate_key_error(&block)
        RetriableTransaction.transaction do
            begin
                retries ||= 0
                self.connection.execute("SAVEPOINT before_create_or_reconcile_from_stripe_subscription")
                yield

            rescue ActiveRecord::RecordNotUnique
                if (retries += 1) < 3
                    self.connection.execute("ROLLBACK TO SAVEPOINT before_create_or_reconcile_from_stripe_subscription")
                    retry
                else
                    raise
                end
            end
        end
    end

    public
    def get_stripe_customer
        Stripe::Customer.retrieve(self.owner_id)
    end

    def owner
        # we check the associations so we don't have to do a query against
        # the users table if we already have an associated hiring_team loaded up
        # see hiring_teams_controller_spec.rbshould use eager loading"
        if association(:user).loaded? && user
            return user
        elsif association(:hiring_team).loaded? && hiring_team
            return hiring_team
        else
            user || hiring_team
        end
    end

    def owner_id
        owner&.id
    end

    def record_invoice_payment(invoice)
        owner.record_invoice_payment(self, invoice)
    end

    # this is only relevant in the cohort case, where after
    # completing a limited number of payments we want to destroy
    # the subscription
    def all_payments_complete?
        owner.all_payments_complete?(self)
    end

    def plan
        if !defined? @plan
            @plan = Stripe::Plan.retrieve({ :id => stripe_plan_id, :expand => ['product'] })
        end
        @plan
    end

    def metered?
        plan&.usage_type == "metered"
    end

    def stripe_subscription=(stripe_subscription)
        unless stripe_subscription.id == self.stripe_subscription_id
            raise "Unexpected stripe_subscription #{stripe_subscription.id.inspect} != #{self.stripe_subscription_id.inspect}"
        end
        unless Subscription.expected_fields_expanded?(stripe_subscription)
            raise "Invalid stripe_subscription.  Not all expected fields expanded."
        end
        @stripe_subscription = stripe_subscription
    end

    def stripe_subscription
        if !defined? @stripe_subscription
            @stripe_subscription = begin
                Subscription.find_stripe_subscription(owner, stripe_subscription_id) if stripe_subscription_id
            # guard against stripe returning nothing for the owner if they don't exist in stripe
            rescue NoMethodError => err
                raise err unless err.message.match(/undefined method.*subscriptions/)
            end

            # NOTE: Upon original writing, Stripe would not return a details for a canceled subscription, but now it does with
            # cancellation information attached. For consistency in our API we reset the subscription if it's been canceled
            @stripe_subscription = nil if @stripe_subscription && @stripe_subscription.status == 'canceled'
        end
        @stripe_subscription
    end

    def plan_metadata
        plan.respond_to?(:metadata) ? plan.metadata : {}
    end


    def status
        if stripe_subscription
            return stripe_subscription.status
        end
    end

    def charge_unpaid_invoices

        unpaid_invoices.sort_by(&:created).each do |invoice|
            invoice.pay

            # useful delay for testing failing invoice back-payments and changing cards (see also: `SubscriptionsController::create`)
            # sleep 10 unless Rails.env.production?
        end

        reset_stripe_subscription
        reconcile_with_stripe
        save!
    ensure
        reset_invoices # pretty sure that invoice.pay does not update the invoices themselves
    end

    def subscription_item
        stripe_subscription&.items&.first
    end

    # this will get called directly with any new subscription creation (via `create_or_reconcile_from_stripe_subscription`),
    # any direct `charge_open_invoice` calls, or through event hooks (see also: `StripeEventHandler` handling of
    # `'customer.subscription.created', 'customer.subscription.updated'` event types)
    def reconcile_with_stripe
        self.owner.ensure_stripe_customer

        self.stripe_plan_id = stripe_subscription.plan.id
        self.stripe_product_id = stripe_subscription.plan.product.id
        self.cancel_at_period_end = stripe_subscription.cancel_at_period_end
        self.current_period_end = Time.at(stripe_subscription.current_period_end)

        # Stripe does not mark people as past_due until all retries have been attempted.  We do
        # it once thay have an unpaid invoice.  A bit unfortunate that we're using the same property
        # but with a different meaning, but it is what it is.
        self.past_due = (self.status == 'past_due' || self.num_unpaid_invoices > 0)

        self.owner.after_subscription_reconciled_with_stripe(self)
    end

    def next_period_amount
        plan.amount - discount_amount
    end

    def discount
        if stripe_subscription && stripe_subscription.respond_to?(:discount)
            stripe_subscription.discount
        end
    end

    def coupon
        discount && discount.coupon
    end

    def coupon_id
        coupon && coupon.id
    end

    def discount_amount
        if coupon && coupon.amount_off
            coupon.amount_off
        elsif discount_percent
            plan.amount * (discount_percent/100.0)
        else
            0
        end
    end

    def discount_percent
        coupon && coupon.percent_off
    end

    def as_json(options = {})
        # NOTE: this is duplicated in UsersController near @select_subscriptions
        {
            id: self.id,
            stripe_plan_id: stripe_plan_id,
            user_id: owner.is_a?(User) && owner.id,
            hiring_team_id: owner.is_a?(HiringTeam) && owner.id,
            past_due: past_due,
            cancel_at_period_end: cancel_at_period_end,
            current_period_end: current_period_end.to_timestamp
        }.as_json
    end

    def num_unpaid_invoices
        unpaid_invoices&.size || 0
    end

    def update_cancel_at_period_end!(val)
        self.cancel_at_period_end = stripe_subscription.cancel_at_period_end = val
        stripe_subscription.save
        save!
    end

    def cancel_and_destroy(reason:, prorate: false, &block)
        if stripe_subscription
            stripe_subscription.metadata['cancellation_reason'] = reason unless reason.nil?
            stripe_subscription.save
        else
            Raven.capture_exception("No corresponding subscription found in Stripe!", {
                extra: {
                    subscription_id: self.id,
                    owner: self.owner&.id,
                    owner_type: self.owner&.class
                }
            })
        end

        if block_given?
            yield

            # see users_controller.  When updating cohort applications,
            # we may want to set a cancellation reason but then leave
            # the cohort application to actually cancel the subscription.
            begin
                reload
            rescue ActiveRecord::RecordNotFound => err
                return
            end
        end

        @prorate_on_destroy = prorate
        destroy
    end

    def increment_usage(timestamp)
        self.owner.increment_usage(self, timestamp)
    end

    private
    def reset_invoices
        remove_instance_variable :@invoices
    rescue NameError
    end

    private
    def trigger_handle_past_due_changes_on_owner
        self.owner.handle_past_due_changes(self)
    end

    private
    def reset_stripe_subscription
        remove_instance_variable :@stripe_subscription
    rescue NameError
    end

    private
    def reset_plan
        remove_instance_variable :@plan
    rescue NameError
    end

    private
    def delete_stripe_subscription
        return unless self.stripe_subscription

        # Make sure subscriptions are loaded before we delete the stripe subscription.  Otherwise
        # it is a race to determine whether the stripe event handler deletes the record from our
        # database before it is referenced in trigger_before_destroy_on_owner
        self.owner.subscriptions

        # Pull any pending amounts prior to cancellation (if metered)
        amount_due = (self.metered? && upcoming_invoice&.amount_due) || 0

        # Cancel the subscription
        stripe_subscription.delete(prorate: @prorate_on_destroy)

        # There is currently no way of forcing UsageRecord processing upon immediate deletion. Instead, we create a manual
        # invoice not attached to a Subscription and attempt to bill that, after cancelling the Subscription. If this fails
        # due to a CardError, we log notification admins, but otherwise allow the deletion to succeed. Admins can provide
        # customers with direct payment gateway links if retried payments continue to fail.
        # see also: https://stripe.com/docs/billing/subscriptions/metered-billing
        if amount_due > 0
            begin
                Stripe::InvoiceItem.create({ customer: owner_id, amount: upcoming_invoice&.amount_due, currency: "USD"})
                immediate_invoice = Stripe::Invoice.create({ customer: owner_id }) # NOTE: NOT attached to subscription
                immediate_invoice.pay
            rescue Stripe::CardError
                Raven.capture_message("Accrued usage invoicing on subscription cancellation failed. Invoice will be auto-retried, but should be monitored. Subscription will be cancelled.")
            end
        end

    end

    private
    def invoices
        @invoices ||= Stripe::Invoice.list(customer: owner_id).data.select do |inv|
            inv.subscription == self.stripe_subscription_id || # auto-billed
                inv&.lines&.data&.map(&:subscription).include?(self.stripe_subscription_id) # ad-hoc billed against subscription
        end
    end

    private
    def paid_invoices
        # filter out unpaid invoices any 0-total invoices
        invoices.select { |i| i.paid && i.total > 0 }
    end

    private
    def unpaid_invoices
        # filter out paid, non-attempted, closed, or any 0-total invoices
        invoices.select { |i| !i.paid && i.auto_advance && i.attempted && i.total > 0 }
    end

    private
    def open_invoices
        invoices.reject(&:closed)
    end

    private
    def upcoming_invoice
        @upcoming_invoice ||= Stripe::Invoice.upcoming(:customer => self.owner_id, subscription: self.stripe_subscription_id)
    end

    private
    def fake_subscription?
        # See fixture_builder.rb; we want to skip some hooks when creating a fake subscription
        self.stripe_subscription_id.starts_with?('fake') && Rails.env.test?
    end

    private
    def trigger_after_create_on_owner
        self.owner.after_subscription_create(self)
    end

    private
    def trigger_after_commit_on_owner
        self.owner.after_subscription_commit(self)
    end

    private
    def trigger_before_destroy_on_owner
        self.owner.before_subscription_destroy(self)
    end

    private
    def destroy_if_canceled
        # This code is to prevent a race condition caused by a delete
        # and create event being processed at the same time.  If the create
        # transaction commits after, then we could end up with a record in
        # the subscriptions table even if the subcription has been canceled
        # in stripe.
        #
        # So, we check our versions table for a version with operation='D'
        # and the same stripe_subscription_id as this record.  If such a version
        # exists, then we delete this record.
        #
        # (This is not ideal since we still end up re-creating a record for a moment
        # and destroying it.  But Ori and Nate considered a few other ways to implement
        # this and decided this was the most expedient.  It is also similar to what we
        # do in hiring_relationships)
        RetriableTransaction.transaction do
            ctes = %Q~
                with stripe_subscription_ids_to_delete AS MATERIALIZED (
                    select stripe_subscription_id from subscriptions_versions
                    where stripe_subscription_id='#{self.stripe_subscription_id}'
                    and operation = 'D'
                )
                , subscriptions_to_delete AS MATERIALIZED (
                    select id from subscriptions
                    where stripe_subscription_id in (select stripe_subscription_id from stripe_subscription_ids_to_delete)
                )
            ~

            ActiveRecord::Base.connection.execute(%Q~
                #{ctes}
                delete from subscriptions_users where subscription_id in (select id from subscriptions_to_delete);

                #{ctes}
                delete from subscriptions where id in (select id from subscriptions_to_delete);
            ~)
        end

    end

    # This methid is a test-mode-only convenience so we don't have to
    # set current_period_end explicitly every time
    # we manually create a subscription in fixtures or specs
    def ensure_current_period_end
        return unless Rails.env.test?

        self.current_period_end ||= Time.now + 1.month
    end

end
