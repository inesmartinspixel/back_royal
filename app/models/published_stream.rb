# == Schema Information
#
# Table name: published_streams
#
#  id             :uuid
#  locale_pack_id :uuid
#  title          :string
#  version_id     :uuid
#  chapters       :json
#  locale         :text
#  exam           :boolean
#

class PublishedStream < ApplicationRecord

    # NOTE: this business logic is duplicated in ToJsonFromApiParamsHelpers::StreamsAccessMixin#filter_by_user_can_see
    def self.for_user(user, columns)
        # Grab all published streams that a user has access to, either because
        # 1. Ze has a group connected to the stream
        # 2. Ze has a relevant cohort that includes the stream in its curriculum
        # 3. Ze is in an institution that includes this stream
        #
        # Return the locale that matches this user's pref_locale if possible. Otherwise English
        #
        # Implemenation:
        # Join against things to filter for locale packs the user has access to
        # Group by locale pack id
        # Within each group, sort by locale and grab the first

        user_id = user.id
        pref_locale = user.pref_locale
        relevant_cohort_id = user.relevant_cohort ? user.relevant_cohort['id'] : nil

        query =  joins("
                -- Courses available via groups
                LEFT JOIN (
                        access_groups_lesson_stream_locale_packs
                        JOIN access_groups_users
                            ON access_groups_users.access_group_id = access_groups_lesson_stream_locale_packs.access_group_id
                            AND access_groups_users.user_id = '#{user_id}'
                    )
                    ON access_groups_lesson_stream_locale_packs.locale_pack_id = published_streams.locale_pack_id

                -- Courses available via cohorts
                left join published_cohort_stream_locale_packs
                    on published_cohort_stream_locale_packs.stream_locale_pack_id = published_streams.locale_pack_id
                    and published_cohort_stream_locale_packs.cohort_id = '#{relevant_cohort_id || SecureRandom.uuid}'

                -- Courses available via institutions
                left join (
                        institution_stream_locale_packs
                        join institutions_users
                            ON institutions_users.user_id = '#{user_id}'
                            AND institution_stream_locale_packs.institution_id = institutions_users.institution_id
                    )
                    on institution_stream_locale_packs.stream_locale_pack_id = published_streams.locale_pack_id
            ")
            .where("
                    -- Courses available via groups
                    access_groups_users.user_id is not null

                    -- Courses available via cohorts
                    or published_cohort_stream_locale_packs.cohort_id is not null

                    -- Courses available via institutions
                    or institution_stream_locale_packs.institution_id is not null
            ")
            .where("locale in ('#{pref_locale}', 'en')")
            .group("published_streams.locale_pack_id")


        columns.each do |col|
            query = query.select("(array_agg(published_streams.#{col} order by case when locale = '#{pref_locale}' then 0 else 1 end))[1] as #{col}")
        end

        query
    end

    def self.for_group_name(access_group_name, columns)

        query =  joins("
                JOIN access_groups_lesson_stream_locale_packs
                    ON access_groups_lesson_stream_locale_packs.locale_pack_id = published_streams.locale_pack_id
                JOIN access_groups
                    ON access_groups.id = access_groups_lesson_stream_locale_packs.access_group_id
                    AND access_groups.name = '#{access_group_name}'
            ")
            .where(locale: 'en') # we hardcode en, which is fine for now everywhere we use this
            .select(columns)

        query
    end

    def self.for_cohort(cohort_id, columns)

        query =  joins("
                join published_cohort_stream_locale_packs
                    on published_cohort_stream_locale_packs.stream_locale_pack_id = published_streams.locale_pack_id
                    and published_cohort_stream_locale_packs.cohort_id = '#{cohort_id}'
            ")
            .where(locale: 'en') # we hardcode en, which is fine for now everywhere we use this
            .select(columns)

        query
    end

    def self.for_institution(institution_id, columns)

        query =  joins("
                join institution_stream_locale_packs
                    on institution_stream_locale_packs.stream_locale_pack_id = published_streams.locale_pack_id
                    and institution_stream_locale_packs.institution_id = '#{institution_id}'
            ")
            .where(locale: 'en') # we hardcode en, which is fine for now everywhere we use this
            .select(columns)

        query
    end

end
