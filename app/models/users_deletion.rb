# == Schema Information
#
# Table name: users_deletions
#
#  id                           :uuid             not null, primary key
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#  user_id                      :uuid             not null
#  psuedo_anonymized_user_email :text             not null
#  admin_id                     :uuid
#  admin_name                   :text
#

class UsersDeletion < ApplicationRecord

end
