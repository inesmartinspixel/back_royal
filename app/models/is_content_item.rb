require 'back_royal/object' # looks_like_uuid?

# Included in models that represent content, and does the following:
#  adds an author
#  adds create_from_hash! and update_from_hash! methods

# Usage
#  include this module in a model
#  add a merge_hash instance method
module IsContentItem
    extend ActiveSupport::Concern

    included do
        include ContentPublisher::Publishable
        include CrudFromHashHelper
        belongs_to :author, :class_name => 'User', optional: true
        belongs_to :last_editor, :class_name => 'User', optional: true

        self.after_save :prune_old_versions
        self.after_commit :refresh_internal_content_titles_if_necessary

        after_destroy :destroy_empty_locale_pack

        validates_presence_of :author
        validates_presence_of :title
    end

    module ClassMethods

        # it would be better to just pass the author as an argument
        # here, but I didn't want to mess with all the tests
        def create_from_hash!(hash, meta = {})
            # Automatically retries in the event of ActiveRecord::PreparedStatementCacheExpired
            RetriableTransaction.transaction do
                hash = hash.unsafe_with_indifferent_access
                instance = new(id: hash['id'] || SecureRandom.uuid)

                %w(title locale).each do |key|
                    if hash.key?(key)
                        val =  hash[key]
                        val = nil if val.is_a?(String) && val.blank?
                        instance[key] = val
                    end
                end

                user = User.find(hash['author']['id'])
                instance.author = instance.last_editor = user
                instance.title = hash['title']

                # handle associations and things
                instance.merge_hash(hash, meta)
                instance.update_timestamps

                # create entity metadata with default values
                metadata = EntityMetadata.create!({
                    title: instance.title
                })
                instance.entity_metadata_id = metadata.id

                handle_locale_pack(instance, user, hash)
                handle_pinning(instance, meta)

                instance.save!

                instance
            end
        end

        def update_from_hash!(user, hash, meta = {})
            # Automatically retries in the event of ActiveRecord::PreparedStatementCacheExpired
            RetriableTransaction.transaction do
                hash, meta, instance = prepare_update_from_hash(hash, meta)

                new_archived_value = hash.delete('archived')

                instance.last_editor = user

                was_english = instance.locale == 'en'
                %w(title locale).each do |key|
                    if hash.key?(key)
                        val =  hash[key]
                        val = nil if val.is_a?(String) && val.blank?
                        instance[key] = val
                    end
                end

                instance.merge_hash(hash, meta)
                instance.update_timestamps

                # archived is only defined on lessons
                instance.archived = new_archived_value unless new_archived_value.nil?

                # access groups only exist on streams. We need to check if they are
                # changing so we can know whether to refresh materialized views
                if instance.supports_access_groups?
                    orig_group_ids = instance.access_groups.pluck(:id)
                end

                existing_locale_pack = handle_locale_pack(instance, user, hash, was_english)
                handle_pinning(instance, meta)

                force_unpublish = (new_archived_value == true && instance.has_published_version?)
                instance.save_from_hash_with_publish_support(user, meta, force_unpublish)

                # this has to be done after the main item is saved to avoid
                # foreign key constraint violations
                existing_locale_pack.destroy if existing_locale_pack && existing_locale_pack.content_items.empty?

                # NOTE: we call after_access_group_update here, but we don't guarantee
                # this gets run on saves made outside of update_from_hash!  We've been
                # getting away with this for a long long time though, so hopefully
                # we will continue to. (See similar issue in cohort.rb)
                if instance.supports_access_groups?
                    instance.after_access_group_update(orig_group_ids)
                end

                instance
            end
        end

        def duplicate!(user, hash_for_new_item, meta)
            # Automatically retries in the event of ActiveRecord::PreparedStatementCacheExpired
            RetriableTransaction.transaction do

                # frames is edited on the client, so we have to pass it through
                hash_for_new_item = hash_for_new_item.unsafe_with_indifferent_access.slice('title', 'locale', 'frames')
                meta = meta.unsafe_with_indifferent_access

                hash_for_new_item['id'] = SecureRandom.uuid
                original = update_before_duplicate!(user, hash_for_new_item, meta)

                # we start from the as_json of the original, without
                # the locale_pack, which has special handling
                hash_for_original_item = original.as_json.except('locale_pack')

                # we pass the params through prepare_params_for_duplication,
                # which implement class-specific stuff
                hash_for_original_item = prepare_params_for_duplication(hash_for_original_item, hash_for_new_item, user, meta)

                # we override the params from the original with the
                # hash that was passed in, and we set the author
                hash_for_new_item = hash_for_original_item
                    .merge(hash_for_new_item)
                    .merge({'author' => user})

                create_meta = {
                    should_pin: true,
                    pinned_title: "Duplicated from '#{original.title}'",
                    duplicated_from_id: original.id
                }.with_indifferent_access

                if meta[:in_same_locale_pack]
                    locale_pack_json = original.locale_pack.as_json

                    # if the locale pack already has an item for this locale,
                    # remove it (the client should have given the editor a warning about this)
                    locale_pack_json['content_items'].delete_if { |item|
                        item['locale'] == hash_for_new_item['locale']
                    }
                    locale_pack_json['content_items'] << {
                        'id' => hash_for_new_item['id']
                    }
                    hash_for_new_item['locale_pack'] = locale_pack_json
                    create_meta[:pinned_title] = "Duplicated from #{I18n.locale_name(original.locale)}"
                end

                create_from_hash!(hash_for_new_item, create_meta)
            end
        end

        private def update_before_duplicate!(user, new_item_hash, meta)

            unless new_item_title = new_item_hash['title']
                raise ArgumentError.new("title not set")
            end
            unless new_item_id = new_item_hash['id']
                raise ArgumentError.new("new_item_id not set")
            end
            unless duplicate_from_id = meta[:duplicate_from_id]
                raise ArgumentError.new("duplicate_from_id not set")
            end
            unless duplicate_from_updated_at = meta[:duplicate_from_updated_at]
                raise ArgumentError.new("duplicate_from_updated_at not set")
            end
            instance = find(duplicate_from_id)

            update_hash = {
                id: instance.id,
                updated_at: duplicate_from_updated_at
            }.with_indifferent_access
            update_meta = {
                should_pin: true,
                pinned_title: "Duplicated to '#{new_item_title}'",
                duplicated_to_id: new_item_id
            }.with_indifferent_access

            if meta[:in_same_locale_pack]
                unless instance.locale_pack
                    update_hash[:locale_pack] = self::LocalePack.new(id: SecureRandom.uuid).as_json.with_indifferent_access
                    update_hash[:locale_pack][:content_items] << {id: instance.id}.with_indifferent_access
                end
                update_meta[:pinned_title] = "Duplicated to #{I18n.locale_name(new_item_hash['locale'])}"
            end

            update_from_hash!(user, update_hash, update_meta)

        end

        private def handle_locale_pack(instance, user, hash, was_english = false)
            if hash.key?('locale_pack')
                locale_pack_id = hash['locale_pack'] &&  hash['locale_pack']['id']
                raise "Disallowed locale_pack_id #{locale_pack_id.inspect}" unless locale_pack_id.nil? || locale_pack_id.looks_like_uuid?

                existing_locale_pack = instance.locale_pack

                if locale_pack_id.nil? && existing_locale_pack
                    instance.locale_pack_id = nil

                    # we now remove all OTHER items from the locale pack.  This
                    # item is being handled here, so we skip it by passing it as
                    # the first argument. We only do this when removing the locale pack
                    # from an English item.  Non-english items can be removed from a locale
                    # pack without destroying the locale pack.  Note that if we are removing an English
                    # item from a locale pack, then we are also changing it's locale.  We cannot
                    # have English items with no locale pack.
                    remove_all_translations_but([instance.id], existing_locale_pack, user) if was_english

                elsif locale_pack_id != instance.locale_pack_id
                    locale_pack = self::LocalePack.find_by_id(locale_pack_id) || instance.create_locale_pack(locale_pack_id)
                    instance.locale_pack = locale_pack
                end

                # the editor UI only lets us make changes to the locale pack
                # from English items, so we only have to support it for those.
                if instance.locale_pack && instance.locale == 'en'
                    update_locale_pack(instance, user, hash)
                end

            end

            # english items mush have locale packs
            instance.ensure_locale_pack if instance.locale == "en"

            existing_locale_pack
        end

        private def update_locale_pack(instance, user, hash)
            locale_pack = instance.locale_pack

            # streams have access groups.  Update those
            if locale_pack.respond_to?(:access_groups=) && (groups = hash['locale_pack']['groups'])
                new_groups = AccessGroup.where(id: groups.map { |g| g['id'] } )
                locale_pack.access_groups = new_groups
            end

            if locale_pack.respond_to?(:content_topics=) && (content_topics = hash['locale_pack']['content_topics'])
                locale_pack.content_topics = ContentTopic.where(id: content_topics.map { |ct| ct['id']} )
            end

            locale_pack.save!

            # remove this locale pack from any item that is not
            # in the list passed in
            new_content_item_ids = hash['locale_pack']['content_items'].map { |i| i['id'] }
            remove_all_translations_but(new_content_item_ids, locale_pack, user)

            # add this locale pack to items that do not yet have it
            self.where(id: new_content_item_ids)
                .where("locale_pack_id is null OR locale_pack_id != '#{locale_pack.id}'")
                .where.not(id: instance.id)
                .each do |content_item|
                    content_item.class.update_from_hash!(user, {
                        'id' => content_item.id,
                        'updated_at' => content_item.updated_at,
                        'locale_pack' => locale_pack.as_json
                    }, {})
            end
        end

        private def remove_all_translations_but(ids_to_keep, locale_pack, user)

            locale_pack.content_items
                .where.not(id: ids_to_keep)
                .each do |content_item|
                    content_item.class.update_from_hash!(user, {
                        'id' => content_item.id,
                        'updated_at' => content_item.updated_at,
                        'locale_pack' => nil
                    }, {})
            end

        end

        private def handle_pinning(instance, meta)

            # this was poorly done.  lessons and lesson_streams both have the columns
            # for pinning, but streams have never actually been pinned when published.
            # so we cannot prune_old_versions now.  Playlists do not have those
            # columns at all.
            #
            # Long story short, only lessons support pinning
            return unless instance.is_a?(Lesson)
            should_publish = meta["should_publish"] || false
            should_pin = meta["should_pin"] || should_publish || false

            if should_pin
                instance.pinned = true
                instance.pinned_title = meta['pinned_title'].blank? ? "Pin from #{Time.now.strftime('%D')}" : meta['pinned_title']
                instance.pinned_description = meta['pinned_description']
                instance.duplicated_from_id = meta['duplicated_from_id']
                instance.duplicated_to_id = meta['duplicated_to_id']
            else
                instance.pinned = false
                instance.pinned_title = nil
                instance.pinned_description = nil
                instance.duplicated_from_id = nil
                instance.duplicated_to_id = nil
            end
        end
    end

    # Note: this should only be called when a human updates the lesson, not
    # when a migration or something does
    def update_timestamps
        self.modified_at = Time.now
    end

    # overridden in Lesson::Version and Lesson::Stream::Version
    def old_version
        false
    end

    def prune_old_versions
        # if this version is pinned when it is saved, then
        # delete older, unpinned versions.
        if respond_to?(:pinned?) && pinned?
            self.versions.
                where(pinned: false).
                where('updated_at < ?', updated_at).
                delete_all
        end
    end

    def destroy_empty_locale_pack
        if locale_pack && locale_pack.content_items == [self]
            locale_pack.destroy
        end
    end

    def refresh_internal_content_titles_if_necessary
        if (previous_changes.keys & ['locale', 'title', 'locale_pack_id']).any?
            Report::RefreshInternalContentTitles.perform_later
        end
    end

end
