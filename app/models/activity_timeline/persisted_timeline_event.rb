# == Schema Information
#
# Table name: persisted_timeline_events
#
#  id             :uuid             not null, primary key
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  time           :datetime         not null
#  event          :text             not null
#  secondary_sort :float
#  text           :text
#  subtext        :text
#  editor_id      :uuid
#  editor_name    :text
#  labels         :text             default([]), not null, is an Array
#  category       :text             not null
#

# See the comment at the top of derived_timeline_event about the two different kinds
# of events that can be included in a timeline
class ActivityTimeline::PersistedTimelineEvent < ApplicationRecord
    self.table_name = "persisted_timeline_events"

    # PersistedTimelineEvents can be associated with some object.  For example, if a
    # PersistedTimelineEvent is associated with a user, then it would be in the timeline
    # for that user.  We can set up join tables with any records that have timelines that
    # need PersistedTimelineEvents.  This structure is similar to that for Subscriptions, which
    # can be associated with either a user or a hiring_team.
    has_one :persisted_timeline_event_user_join, class_name: 'ActivityTimeline::PersistedTimelineEventUserJoin', :dependent => :destroy
    has_one :user, :through => :persisted_timeline_event_user_join

    validates_inclusion_of :event, :in => ['note', 'performed_action']

    def self.create_for_user!(user, params)
        create_from_hash!(params, {
            user: user
        })
    end

    def self.create_from_hash!(params, meta)
        RetriableTransaction.transaction do
            instance = create!(
                id: params[:id] || SecureRandom.uuid,
                time: Time.at(params[:time]),
                event: params[:event],
                labels: params[:labels] || [],
                text: params[:text],
                subtext: params[:subtext],
                editor_id: params[:editor_id],
                editor_name: params[:editor_name],
                category: params[:category]
            )

            # Associate the event with whichever object has the timeline
            # on which this event should appear
            if meta[:user] || meta[:object] == 'user'
                user = meta[:user] || User.find(meta[:object_id])
                user.persisted_timeline_events << instance
            else
                raise ArgumentError.new("Unexpected object #{meta[:object].inspect}")
            end

            instance
        end
    end

    def as_json(options = {})
        {
            id: self.id,
            category: self.category,
            event: self.event,
            time: self.time.to_timestamp,
            text: self.text,
            subtext: self.subtext,
            editor_name: self.editor_name,
            editor_id: self.editor_id,
            labels: self.labels
        }
    end
end
