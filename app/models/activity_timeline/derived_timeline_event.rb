# At the highest level, a timeline has two different kinds of events.
#  * DerivedTimelineEvents are generated at runtime when generating the json for a timeline,
#    generally by looking through versions tables in the DB
#  * PersistedTimelineEvents are stored in a table in the database and pulled out of there
#    when generating the json for a timeline
class ActivityTimeline::DerivedTimelineEvent
    include ActiveModel::Validations

    attr_accessor :category, :event, :time, :labels, :editor_name, :editor_id, :changes, :secondary_sort, :text

    validates_presence_of :category, :event, :time

    def initialize(attrs)
        [:category, :event, :time, :labels, :editor_name, :editor_id, :changes, :secondary_sort, :text].each do |attr|
            self.send(:"#{attr}=", attrs[attr])
        end

        self.labels ||= []
    end

    def as_json(options = {})
        {
            category: self.category,
            event: self.event,

            # keep the millescond granularity, just in case
            time: self.time.to_f,
            secondary_sort: self.secondary_sort,
            labels: self.labels,
            editor_name: self.editor_name,
            editor_id: self.editor_id,
            changes: self.changes,
            text: self.text
        }.as_json
    end
end