class ActivityTimeline::UserTimeline < ActivityTimeline
    attr_accessor :user_id, :cohort_applications_versions, :users_versions,
        :career_profiles_versions, :user_id_verifications, :failed_idology_verifications, :project_progress_versions,
        :enrollment_agreements

    # before generating json for the events in a timeline, we want
    # to preload everything we'll need
    def self.preload(instances)
        instance_map = instances.index_by(&:user_id)

        preload_cohort_applications_versions(instance_map)
        preload_users_versions(instance_map)
        preload_career_profiles_versions(instance_map)
        preload_id_verifications(instance_map)
        preload_project_progress_versions(instance_map)
        preload_enrollment_agreements(instance_map)
        after_preloaded(instances)
    end

    private
    def self.preload_cohort_applications_versions(instance_map)

        all_cohort_applications_versions = []
        CohortApplication::Version
            .where(user_id: instance_map.keys)
            .reorder(:user_id, 'id', :version_created_at, :updated_at)
            .select('id', :version_id,  :version_created_at, :updated_at, :operation, :version_editor_id, :version_editor_name, :user_id, :status, :cohort_id, :admissions_decision, :registered, :graduation_status, :locked_due_to_past_due_payment)
            .group_by(&:user_id)
            .each do |user_id, cohort_applications_versions|
                all_cohort_applications_versions += cohort_applications_versions
                instance_map[user_id].cohort_applications_versions = cohort_applications_versions
            end

        ActiveRecord::Associations::Preloader.new.preload(all_cohort_applications_versions, {
            :cohort => []
        })

        # When looking at cohort_applications_versions records, we cannot guarantee that a
        # given cohort referenced  by `cohort_id` was not deleted at some point after the
        # creation of said records. If we detect that a cohort has been deleted, we should
        # attempt to find an associated record in the cohorts_versions table.
        all_cohort_applications_versions.each do |application|
            application.cohort ||= Cohort::Version
                .where(id: application.cohort_id)
                .reorder("version_created_at desc")
                .first
        end

        instance_map.values.each { |i|
            i.cohort_applications_versions ||= []
        }
    end

    private
    def self.preload_users_versions(instance_map)

        User::Version
            .where(id: instance_map.keys)
            .reorder('id', :version_created_at, :updated_at)
            .select('id', :version_id, :version_created_at, :updated_at, :operation, :version_editor_id, :version_editor_name, :pref_student_network_privacy, :can_edit_career_profile, :identity_verified, :transcripts_verified, :english_language_proficiency_documents_approved)
            .group_by { |v| v.attributes['id'] }
            .each do |user_id, users_versions|
                instance_map[user_id].users_versions = users_versions
            end

        instance_map.values.each { |i|
            i.users_versions ||= []
        }

    end

    private
    def self.preload_career_profiles_versions(instance_map)

        CareerProfile::Version
            .where(user_id: instance_map.keys)
            .reorder('id', :version_created_at, :updated_at)
            .select('id', :version_id, :version_created_at, :updated_at, :operation, :version_editor_id, :version_editor_name, :user_id, :interested_in_joining_new_company)
            .group_by(&:user_id)
            .each do |user_id, career_profiles_versions|
                instance_map[user_id].career_profiles_versions = career_profiles_versions
            end

        instance_map.values.each { |i|
            i.career_profiles_versions ||= []
        }

    end

    private
    def self.preload_id_verifications(instance_map)
        # We show both IDology verifications and manual admin verifications. Both of
        # these methods of verification create a user_id_verification.
        UserIdVerification.where(user_id: instance_map.keys)
            .reorder('id', :created_at)
            .select('id', :created_at, :user_id, :verified_at, :verification_method, :verifier_id, :verifier_name)
            .group_by(&:user_id)
            .each do |user_id, user_id_verifications|
                instance_map[user_id].user_id_verifications = user_id_verifications
            end

        instance_map.values.each { |i|
            i.user_id_verifications ||= []
        }

        # We also show failed IDology verifications
        IdologyVerification.where(user_id: instance_map.keys, verified: false)
            .reorder('id', :created_at)
            .select('id', :created_at, :user_id)
            .group_by(&:user_id)
            .each do |user_id, failed_idology_verifications|
                instance_map[user_id].failed_idology_verifications = failed_idology_verifications
            end

        instance_map.values.each { |i|
            i.failed_idology_verifications ||= []
        }
    end

    private
    def self.preload_enrollment_agreements(instance_map)

        SignableDocument.where(user_id: instance_map.keys, document_type: SignableDocument::TYPE_ENROLLMENT_AGREEMENT)
            .where.not(signed_at: nil)
            .reorder('id', :signed_at)
            .select('id', :signed_at, :user_id)
            .group_by(&:user_id)
            .each do |user_id, enrollment_agreements|
                instance_map[user_id].enrollment_agreements = enrollment_agreements
            end
    end

    private
    def self.preload_project_progress_versions(instance_map)

        ProjectProgress::Version
            .where(user_id: instance_map.keys)
            .reorder('id', :version_created_at, :updated_at)
            .select('id', :version_id, :version_created_at, :updated_at, :operation, :version_editor_id, :version_editor_name,
                :user_id, :requirement_identifier,
                :score, :waived, :marked_as_passed,
                :status, :id_verified)
            .group_by(&:user_id)
            .each do |user_id, project_progress_versions|
                instance_map[user_id].project_progress_versions = project_progress_versions
            end

        instance_map.values.each { |i|
            i.project_progress_versions ||= []
        }

    end

    public
    def initialize(user_id)
        self.user_id = user_id
    end

    def user
        unless defined?(@user)
            @user = User.find(self.user_id)
        end
        @user
    end

    def get_events
        cohort_application_events +
        enrollment_modification_events +
        student_network_modification_events +
        career_profile_modification_events +
        id_verification_events +
        enrollment_agreement_events +
        project_progress_modification_events +
        self.user.persisted_timeline_events.to_a
    end

    private
    def cohort_application_events
        events = []
        cohort_applications_versions.group_by{ |v| v.attributes['id'] }.each do |id, versions|
            previous_version = nil
            versions.each do |version|

                if version.operation == 'I'
                    events << ActivityTimeline::DerivedTimelineEvent.new(
                        category: 'cohort_application',
                        time: version.version_created_at,
                        secondary_sort: version.updated_at.to_f,
                        editor_id: version.version_editor_id,
                        editor_name: version.version_editor_name,
                        event: 'creation',
                        labels: [version&.cohort&.name || 'COHORT DELETED', version.status]
                    )
                end

                if version.operation == 'D'
                    events << ActivityTimeline::DerivedTimelineEvent.new(
                        category: 'cohort_application',
                        event: 'deletion',
                        time: version.version_created_at,
                        editor_id: version.version_editor_id,
                        editor_name: version.version_editor_name,
                        secondary_sort: 0, # the updated_at does not get updated on deletion
                        labels: [previous_version&.cohort&.name || 'COHORT DELETED', previous_version&.status],
                    )
                end

                previous_version = version
            end
        end

        events += get_change_events({
            category: 'cohort_application',
            versions: cohort_applications_versions,
            attrs: [
                {:cohort => Proc.new { cohort&.name || 'COHORT DELETED' } },
                :status,
                :admissions_decision,
                :graduation_status,
                :registered,
                :locked_due_to_past_due_payment
            ],
            labels: Proc.new { |version|
                [version&.cohort&.name || 'COHORT DELETED', version.status]
            }
        })

        events
    end

    private
    def enrollment_modification_events
        events = []
        events += get_change_events(
            category: 'enrollment',
            versions: users_versions || [],
            attrs: [{identity_verified: :identity_verified}, {transcripts_verified: :transcripts_verified}, {english_proficiency_approved: :english_language_proficiency_documents_approved}])
        events
    end

    private
    def student_network_modification_events
        get_change_events(
            category: 'student_network',
            versions: users_versions,
            attrs: [{privacy: :pref_student_network_privacy}]
        )
    end

    private
    def career_profile_modification_events
        events = []
        events += get_change_events(
            category: 'career_profile',
            versions: career_profiles_versions,
            attrs: [{status: :interested_in_joining_new_company}]
        )
        events += get_change_events(
            category: 'career_profile',
            versions: users_versions,
            attrs: [{has_career_network_access: :can_edit_career_profile}]
        )
        events
    end

    private
    def id_verification_events
        events = []
        user_id_verifications.each do |verification|
            event = ActivityTimeline::DerivedTimelineEvent.new(
                category: 'enrollment',
                time: verification.verified_at,
                secondary_sort: verification.created_at.to_f,
                event: 'performed_action'
            )

            if verification.verification_method == 'verified_by_idology'
                event.text = 'ID Verified'
                event.editor_name = 'IDology'
            elsif verification.verification_method == 'verified_by_admin'
                event.text = 'ID Verified'
                event.editor_id = verification.verifier_id
                event.editor_name = verification.verifier_name
            elsif verification.verification_method == 'waived_due_to_late_enrollment'
                event.text = 'ID Verification Waived'
            else
                raise "See user_id_verification verification_method validation"
            end

            events << event
        end

        failed_idology_verifications.each do |failed_verification|
            events << ActivityTimeline::DerivedTimelineEvent.new(
                category: 'enrollment',
                time: failed_verification.created_at,
                secondary_sort: failed_verification.created_at.to_f,
                event: 'performed_action',
                text: 'ID Verification Failed',
                editor_name: 'IDology'
            )

        end
        events
    end

    private
    def enrollment_agreement_events
        (enrollment_agreements || []).map do |enrollment_agreement|
            ActivityTimeline::DerivedTimelineEvent.new(
                category: 'enrollment',
                time: enrollment_agreement.signed_at,
                event: 'performed_action',
                text: 'Signed enrollment agreement'
            )
        end
    end

    private
    def project_progress_modification_events
        events = []
        events += get_change_events(
            category: 'project',
            versions: project_progress_versions,
            attrs: [
                {score: :score},
                {waived: :waived},
                {marked_as_passed: :marked_as_passed},
                {status: :status},
                {id_verified: :id_verified}
            ],
            # ProjectProgress records can be created with any number of attrs
            # that we want to track changes for. Instead of special-casing
            # inserts into the _versions table and creating `creation` events,
            # get_change_events can handle it for us.
            operations: ['U', 'I'],
            labels: Proc.new { |version|
                [version.requirement_identifier.humanize]
            }
        )
        events
    end

end