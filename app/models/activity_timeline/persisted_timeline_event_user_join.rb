# == Schema Information
#
# Table name: persisted_timeline_events_users
#
#  id                          :uuid             not null, primary key
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#  persisted_timeline_event_id :uuid
#  user_id                     :uuid
#

class ActivityTimeline::PersistedTimelineEventUserJoin < ApplicationRecord

    self.table_name = 'persisted_timeline_events_users'

    belongs_to :persisted_timeline_event, class_name: "ActivityTimeline::PersistedTimelineEvent", dependent: :destroy
    belongs_to :user

end
