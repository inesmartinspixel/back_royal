class ActivityTimeline::ProjectProgressTimeline < ActivityTimeline
    attr_accessor :project_progress_id, :project_progress_versions

    # before generating json for the events in a timeline, we want
    # to preload everything we'll need
    def self.preload(instances)
        instance_map = instances.index_by(&:project_progress_id)

        preload_project_progress_versions(instance_map)
        after_preloaded(instances)
    end

    private
    def self.preload_project_progress_versions(instance_map)

        ProjectProgress::Version
            .where(id: instance_map.keys)
            .reorder('id', :version_created_at, :updated_at)
            .select('id', :version_id, :version_created_at, :updated_at, :operation, :version_editor_id, :version_editor_name,
                :user_id, :requirement_identifier,
                :score, :waived, :marked_as_passed,
                :status, :id_verified)
            .group_by { |version| version['id'] }
            .each do |id, project_progress_versions|
                instance_map[id].project_progress_versions = project_progress_versions
            end

        instance_map.values.each { |i|
            i.project_progress_versions ||= []
        }

    end

    public
    def initialize(project_progress_id)
        self.project_progress_id = project_progress_id
    end

    def get_events
        project_progress_modification_events
    end

    private
    def project_progress_modification_events
        events = []
        events += get_change_events(
            category: 'project',
            versions: project_progress_versions,
            attrs: [
                {score: :score},
                {waived: :waived},
                {marked_as_passed: :marked_as_passed},
                {status: :status},
                {id_verified: :id_verified}
            ],
            # ProjectProgress records can be created with any number of attrs
            # that we want to track changes for. Instead of special-casing
            # inserts into the _versions table and creating `creation` events,
            # get_change_events can handle it for us.
            operations: ['U', 'I'],
            labels: Proc.new { |version|
                [version.requirement_identifier.humanize]
            }
        )
        events
    end

end