require 'geocoder'

class Location
    attr_reader :lat, :lng

    extend Location::PlaceFormattingMixin

    def self.locations
        #I found coordinates by googling "{{CITY}} lat long"
        # NOTE: If you add anything to this list, see the client-side careers_module.js and the "Locations of Interest" Airtable column as well
        @locations ||= {
            san_francisco: new('37.7749° N, 122.4194° W'),
            new_york: new('40.7128° N, 74.0059° W'),
            seattle: new('47.6062° N, 122.3321° W'),
            denver: new('39.7392° N, 104.9903° W'),
            boston: new('42.3601° N, 71.0589° W'),
            los_angeles: new('34.0522° N, 118.2437° W'),
            san_diego: new('32.7157° N, 117.1611° W'),
            washington_dc: new('38.9072° N, 77.0369° W'),
            chicago: new('41.8781° N, 87.6298° W'),
            austin: new('30.2672° N, 97.7431° W'),
            atlanta: new('33.7490° N, 84.3880° W'),
            dallas: new('32.7767° N, 96.7970° W'),
            london: new('51.5074° N, 0.1278° W'),
            raleigh_durham: new('35.8801° N, 78.7880° W'),
            miami: new('25.7617° N, 80.1918° W'),
            houston: new('29.7604° N, 95.3698° W'),
            seoul: new('37.5665° N, 126.9780° E'),
            singapore: new('1.3521° N, 103.8198° E'),
            dubai: new('25.2048° N, 55.2708° E'),
            paris: new('48.8566° N, 2.3522° E'),
            berlin: new('52.5200° N, 13.4050° E'),
            hong_kong: new('22.3964° N, 114.1095° E'),
            beijing: new('39.9042° N, 116.4074° E'),
            shanghai: new('31.2304° N, 121.4737° E'),
            tokyo: new('35.6895° N, 139.6917° E'),
        }
    end

    def self.key?(key)
        self.locations.key?(key.to_sym)
    end

    # We expect either a hash or ActionController::Parameters to be
    # passed into this method. If you pass ActionController::Parameters,
    # it gets converted to an unsafe hash. If you pass a hash, you should
    # symbolize the keys first.
    def self.lat_lng_from_hashy_thing(place_details)
        # we shouldn't have to worry about any sort of
        # sql injection here since we use to_f below
        if place_details.respond_to?(:to_unsafe_h)
            place_details = place_details.to_unsafe_h
        end

        [place_details[:lat].to_f, place_details[:lng].to_f]
    end

    def self.from_place_details(place_details)
        new(*lat_lng_from_hashy_thing(place_details))
    end

    def self.location_keys_close_to(place_details, threshold = 100, options = {units: :km})
        target_location = Location.from_place_details(place_details)
        close_locations = []
        self.locations.each do |key, location|
            if target_location.distance_between(location, options) < threshold
                close_locations << key
            end
        end
        close_locations
    end

    def self.at_location_sql_string(key_or_place_details, column = 'location', kilometers = 100)
        location = nil
        if key_or_place_details.is_a?(String) || key_or_place_details.is_a?(Symbol)
            location = Location.locations[key_or_place_details.to_sym]

            if location.nil?
                err = RuntimeError.new("Location not found")
                err.raven_options = {
                    extra: {
                        location: key_or_place_details
                    }
                }
                raise err
            end
        elsif key_or_place_details.is_a?(Hash) && key_or_place_details.present?
            location = Location.from_place_details(key_or_place_details.symbolize_keys)
        elsif key_or_place_details.is_a?(ActionController::Parameters) && key_or_place_details.present?
            location = Location.from_place_details(key_or_place_details)
        end

        if location.present?
            point = "ST_GeographyFromText('POINT(#{location.lng} #{location.lat})')"
        else
            point = "null"
        end

        # find everything with a location that is within 100km of the
        # target location.  This query will use a gin index on the location
        # column.  See career_profiles for an example. Note that the following condition would return
        # the same results, but it would not be able to use the index: `ST_Distance(location, #{point}) < 100000`
       "ST_DWithin(#{column}, #{point}, #{1000*kilometers})"
    end

    def self.within_geometry_sql_string(column = 'location', geometry)
        "ST_Contains('#{geometry}'::geometry, #{self.transform_geography_to_geometry(column)})"
    end

    def self.within_viewports(query, column, viewports, cast = '')
        # viewports are formatted as: [xmin, ymin, xmax,ymax]

        # we found that in order to take advantage of indexes and make things
        # fast, we have to create the envelopes in a cte rather than
        # inside of the where statement
        envelopes_sql = viewports.map do |viewport|
            "select ST_MakeEnvelope(#{viewport.join(', ')}) as envelope"
        end.join("\nunion\n")

        query = query.with(envelopes: envelopes_sql)
                    .joins("join envelopes on envelopes.envelope && #{column}#{cast}")
    end

    def self.transform_geography_to_geometry(column = 'location', srid = 3857)
        # We transform the geometry into Web Mercator coordinate space (SRID: 3857) to allow distance measuring in meters
        "ST_Transform(#{column}::geometry, 3857)"
    end

    def self.select_cluster_id_sql_string(column = 'location', epsilon = 8000, min_points = 1, table_with_location_column = 'career_profiles')
        # From http://postgis.net/docs/ST_ClusterDBSCAN.html
        #
        # An input geometry will be added to a cluster if it is either:
        # - A "core" geometry, that is within eps distance (Cartesian) of at least minpoints input geometries (including itself) or
        # - A "border" geometry, that is within eps distance of a core geometry.
        #
        # We set minPoints=1 here so that all points are labeled with a cluster
        # We transform the geometry into Web Mercator coordinate space (SRID: 3857) to allow distance measuring in meters
        "ST_ClusterDBSCAN(#{self.transform_geography_to_geometry(column)}, eps := #{epsilon}, minPoints := #{min_points}) OVER(order by #{table_with_location_column}.id) as cluster_id"
    end

    # NOTE: we explicitly cast to ::text both `cluster_hull` and `cluster_radius` to support PostGIS 2x -> 3x compatability and simple data structure
    def self.select_and_group_by_cluster_id_sql_string(cluster_radius, location_column = 'location', cluster_column = 'cluster_id', profile_id_column = 'id', table_with_cluster_id = 'labeled_with_cluster_id')
        "select
            count(*) as count
            , #{cluster_column}
            , ST_Centroid(ST_Collect(#{location_column}::geometry)) as cluster_center
            , ST_Buffer(ST_ConvexHull(ST_Collect(#{self.transform_geography_to_geometry(location_column)})), 1)::text as cluster_hull
            , #{cluster_radius}::text as cluster_radius
        from #{table_with_cluster_id}
        group by #{cluster_column}"
    end

    def self.select_as_geojson_features_sql_string(cluster_column = 'cluster_id', geometry_column = 'cluster_center', table_of_clustered_geometry = 'grouped_by_cluster_id')
        "select cluster_id
            , jsonb_build_object(
                'type',       'Feature',
                'id',         #{cluster_column},
                'geometry',   ST_AsGeoJSON(#{geometry_column})::jsonb,
                'properties', to_jsonb(row) - '#{geometry_column}' - '#{cluster_column}'
            ) as feature
        from (select * from #{table_of_clustered_geometry}) row"
    end

    def self.select_as_geojson_feature_collection_sql_string(feature_column = 'feature', table_of_feature_rows = 'geo_features')
        "select
            jsonb_build_object(
                'type',     'FeatureCollection',
                'features', jsonb_agg(#{feature_column})
            )::json as json
        from #{table_of_feature_rows}"
    end

    # ground resolution indicates the distance in meters on the ground that is represented by a single pixel in the map.
    def self.ground_resolution_by_zoom(zoom)
        # Explanation:
        #
        # * Google maps tiles are 256 pixels wide at zoom level 0
        # * at zoom level 0, the entire world fits into 1 256 px square tile (web mercator projection)
        # * at zoom level 1, the entire world fits into 4 256 px square tiles
        #
        # ...and so on
        #
        # Given the user's zoom level, we can compute an approximate distance, in meters, for a single pixel on the map.
        # Because we only need it to be approximate, we assume a spherical earth.

        # the number of pixels in a google map tile (at zoom level 0, this tile has the whole world in it)
        base_tile_width_pixels = 256

        # number of pixels required to render the world given a zoom level
        # at zoom=1, it takes two tiles, and continues to double at each level
        whole_world_width_pixels = base_tile_width_pixels * (2 ** zoom)

        # rough estimate of earth's radius in meters at latitude 0 if earth was a perfect sphere.
        earth_radius = 6378.137 * 1000

        # circumfrence of earth (maps to the width of the flattened square projection of the world)
        earth_circumfrence = 2 * Math::PI * earth_radius

        # meters per pixel at the equator, assuming the earth is a sphere
        earth_circumfrence / whole_world_width_pixels
    end

    def self.get_clustering_radius_and_min_cluster_size(zoom, marker_width)
        # The zoom level to stop clustering at
        min_zoom_level = 10

        # For highest zoom levels, consider stations 8000 meters apart as the same.
        if zoom >= min_zoom_level
            clustering_radius = 8000
        else
            ground_resolution = self.ground_resolution_by_zoom(zoom)

            # multiply ground resolution (meters / pixel) by marker width (pixels) to get cluster radius in meters
            clustering_radius = ground_resolution * marker_width
        end

        return clustering_radius, 1
    end

    def initialize(lat, lng = nil)
        # lat can also be a string like '35.6895° N, 139.6917° E'
        if lat.is_a?(String)
            parsed = lat.match(/([\d\.]+)[^NSEW]+([NSEW])[^\d]+([\d\.]+)[^NSEW]+([NSEW])/).to_a.slice(1, 4)
            raise "Invalid string #{lat}" unless parsed.size == 4
            [1,3].each do |i|
                dir = parsed[i]
                if dir == 'N'
                    lat = parsed[i-1].to_f
                elsif dir == 'S'
                    lat = -parsed[i-1].to_f
                elsif dir == 'E'
                    lng = parsed[i-1].to_f
                elsif dir == 'W'
                    lng = -parsed[i-1].to_f
                end
            end
        end

        raise ArgumentError.new("Expecting lat and long") unless lat.is_a?(Numeric) && lng.is_a?(Numeric)
        @lat = lat
        @lng = lng
    end

    def distance_between(location, options = {units: :km})
        # We can pass locations in here because we have defined to_coordinates
        Geocoder::Calculations.distance_between(self, location, options)
    end

    # compatibility with Geocoder
    def to_coordinates
        [lat, lng]
    end

    def self.place_details_for_place_id(place_id)
        client = GooglePlaces::Client.new(ENV["GOOGLE_API_KEY"])
        full_place = client.spot(place_id)

        if full_place.lat.nil? || full_place.lng.nil?
            puts "#{user.id} lat/lng could not be backfilled with query string '#{query_string}'"
            return false
        end

        # Build out place_details object with information we want to cache in our database.
        # Note: Mirrors logic in location_autocomplete_dir.js
        place_details = {}
        full_place.address_components.each do |address_component|
            place_details[address_component["types"][0]] = {
                "short": address_component["short_name"],
                "long": address_component["long_name"]
            }
        end
        place_details["formatted_address"] = full_place.formatted_address
        place_details["adr_address"] = full_place.json_result_object["adr_address"]
        place_details["utc_offset"] = full_place.utc_offset_minutes || full_place.utc_offset
        place_details["lat"] = full_place.lat
        place_details["lng"] = full_place.lng
        place_details["name"] = full_place.name # We started collecting this with student network events
        place_details
    end

    def self.find_place_from_text(text)
        # See also: https://developers.google.com/places/web-service/search
        google_response = HTTParty.get('https://maps.googleapis.com/maps/api/place/findplacefromtext/json', query: {
            key: ENV["GOOGLE_API_KEY"],
            input: "#{text}",
            inputtype: 'textquery',
            fields: 'place_id,geometry'
        }).parsed_response
        google_response['candidates']&.first # The first result is generally the most relevant
    end

    def place_details
        unless defined? @place_details
            client = GooglePlaces::Client.new(ENV["GOOGLE_API_KEY"])
            places = client.spots(lat, lng)

            if places.nil? || places.size == 0
                puts "#{user.id} could not be backfilled with query string '#{query_string}'"
                return false
            end

            @place_details = Location.place_details_for_place_id(places.first.place_id)
        end
        @place_details
    end

end