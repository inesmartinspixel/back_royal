module AutoSuggestOptionMixin
    extend ActiveSupport::Concern

    module ClassMethods

        def update_all_locales_from_hash!(hash)
            # Automatically retries in the event of ActiveRecord::PreparedStatementCacheExpired
            RetriableTransaction.transaction do
                hash = hash.unsafe_with_indifferent_access
                meta = (meta || {}).with_indifferent_access
                instance = find_by_id(hash[:id])

                if instance.nil?
                    raise ActiveRecord::RecordNotFound.new("Couldn't find #{self.name} with id=#{hash[:id]}")
                end

                instances = self.where(text: instance.text)
                instances.each do |instance|
                    self.update_from_hash!(hash.merge(id: instance.id))
                end
            end
        end

        def create_from_hash!(instance, hash)
            # Automatically retries in the event of ActiveRecord::PreparedStatementCacheExpired
            RetriableTransaction.transaction do
                hash = hash.unsafe_with_indifferent_access
                meta = (meta || {}).with_indifferent_access

                instance.create_hash(hash)
                instance.save!
                instance
            end
        end

        def update_from_hash!(hash)
            # Automatically retries in the event of ActiveRecord::PreparedStatementCacheExpired
            RetriableTransaction.transaction do
                hash = hash.unsafe_with_indifferent_access
                meta = (meta || {}).with_indifferent_access
                instance = find_by_id(hash[:id])

                if instance.nil?
                    raise ActiveRecord::RecordNotFound.new("Couldn't find #{self.name} with id=#{hash[:id]}")
                end

                instance.merge_hash(hash)
                instance.save!
                instance
            end
        end
    end

    def create_hash(hash)
        hash = hash.unsafe_with_indifferent_access

        %w(text suggest image_url source_id locale).each do |key|
            if hash.key?(key)
                val = hash[key]
                val = nil if val.is_a?(String) && val.blank?
                self[key] = val
            end
        end

        self
    end

    def merge_hash(hash)
        hash = hash.unsafe_with_indifferent_access

        basic_keys = %w(text suggest)

        (
            basic_keys
        ).each do |key|
            if hash.key?(key)
                val = hash[key]
                val = nil if val.is_a?(String) && val.blank?
                self[key] = val
            end
        end

        self
    end

    included do
    end

    def as_json(options = {})
        hash = {
            'id' => self.id,
            'created_at' => self.created_at,
            'updated_at' => self.updated_at,
            'type' => self.type,
            'text' => self.text,
            'locale' => self.locale,
            'suggest' => self.suggest
        }

        hash['source_id'] = self.source_id if has_attribute?(:source_id)
        hash['image_url'] = self.image_url if has_attribute?(:image_url)

        hash
    end

    def get_klass_from_type(type)
        return case type
            when 'skills'
                SkillsOption
            when 'student_network_interests'
                StudentNetworkInterestsOption
            when 'awards_and_interests'
                CareerProfile::AwardsAndInterestsOption
            when 'professional_organization'
                ProfessionalOrganizationOption
            when 'educational_organization'
                CareerProfile::EducationalOrganizationOption
        end
    end

end