# == Schema Information
#
# Table name: idology_verifications
#
#  id                   :uuid             not null, primary key
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  user_id              :uuid             not null
#  idology_id_number    :text             not null
#  scan_capture_url     :text             not null
#  response_received_at :datetime
#  verified             :boolean
#  response             :json
#  email                :text
#  phone                :text
#

require 'net/http'
require 'uri'
require 'openssl'



class IdologyVerification < ApplicationRecord
    has_one :user_id_verification
    belongs_to :user

    after_create :log_to_external_systems

    # When a user clicks to say "I want to verify", we can use
    # this method to get a url from IDology that we can give to
    # the user.  We store that url and the associated id number
    # in an IdologyVerification record so that when we get information
    # related to that id_number from IDology, we know which user
    # it applies to
    def self.fetch_idology_capture_token(params)
        response_data = make_idology_request("scan-capture-token.svc")

        # We expect user_id and either email or phone to be included in the params
        create!(params.merge(
            idology_id_number: response_data['id_number'],
            scan_capture_url: response_data['scan_capture_url']
        ))
    end

    # Make a request to any of the api endpoints that IDology supports
    def self.make_idology_request(endpoint, query_params = {})
        uri = URI.parse("https://web.idologylive.com/api/#{endpoint}")
        request = Net::HTTP::Post.new(uri)
        request.set_form_data({
            "password" => ENV['IDOLOGY_PASSWORD'],
            "username" => ENV['IDOLOGY_USERNAME'],
        }.merge(query_params))

        req_options = {
            use_ssl: true,
            verify_mode: OpenSSL::SSL::VERIFY_NONE,
        }

        response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
            http.request(request)
        end

        begin
            response_body = response.body
            response_data = Hash.from_xml(response_body)['response']
            fingerprint = error_message = response_data['error']
        rescue REXML::ParseException, NoMethodError => err
            # treat malformed XML responses as an internal error and allow the response to be processed
            # if response_body is empty, NoMethodError will be thrown; treat the same way
            response_data = {"capture_decision" => {"key" => "capture.internal.error"} }
            fingerprint = error_message = nil
        end

        if error_message
            err = RuntimeError.new("Error response from IDology: #{error_message.inspect}")
            err.raven_options = {
                extra: {
                    response: response_data,
                    endpoint: endpoint,
                    query_params: query_params
                },
                fingerprint: ['Idology.make_idology_request', fingerprint]
            }
            raise err
        end

        response_data

    end

    def log_to_external_systems
        Event.create_server_event!(
            SecureRandom.uuid,
            self.user_id,
            'idology:scan_capture_url_created',
            {
                scan_capture_url: scan_capture_url,
                idology_verification_id: self.id,
                email: email,
                phone: phone
            }
        ).log_to_external_systems
    end

    # We can use this method to fetch the full results from idology and
    # decide whether or not to mark our record as verified.  If the record
    # is verified, then we create an instance of UserIdVerification
    def fetch_result_and_record_verification
        # if we already have a result, abort
        return unless verified.nil?

        response_data = fetch_result

        RetriableTransaction.transaction do

            capture_decision_key = begin
                response_data['capture_decision']['key']
            rescue Exception => e
                nil
            end

            # if there is no capture_decision_key, it means the user has
            # not yet tried to verify
            return if capture_decision_key.nil?

            self.response = response_data
            self.response_received_at = Time.now

            self.verified = false
            if capture_decision_key == 'result.scan.capture.id.approved'
                self.verified = true
            end

            if !['capture.internal.error', 'result.scan.capture.id.not.approved', 'result.scan.capture.id.approved'].include?(capture_decision_key)

                # This is expected, but we do not have a good idea right now
                # what sort of things we might see here.  Fingerprinting in
                # this was should let us get a notification about each possible
                # response and to ignore them as needed
                Raven.capture_exception("Unexpected capture_decision_key",
                    level: 'warning',
                    extra: {
                        user_id: self.user_id,
                        idology_id_number: self.idology_id_number,
                        response: self.response
                    },
                    fingerprint: ['Unexpected capture_decision_key', capture_decision_key]
                )

                raise "Unexpected capture_decision_key: #{capture_decision_key.inspect}" if Rails.env.development?

                self.verified = false
            end

            self.save!

            if self.verified?
                UserIdVerification.record_verification!(self.user, {
                    verification_method: 'verified_by_idology',
                    idology_verification_id: self.id
                })
            end
        end
    end

    # We cannot trust idology to always hit us with the callback to tell us when verification
    # results are available because they have no retry logic.  So, if a user is sitting on the
    # verification modal, we want to check every now and then for results.  We don't need to
    # bother if
    #
    # 1. we already got a response (`verified?`)
    # 2. this instance was just created (it's gonna take them a while to get through the process
    #    and even if they do it super-fast, we should most always get the hook)
    def ensure_job_if_necessary
        age = Time.now - self.created_at
        if self.verified.nil? && age > 30.seconds
            GetIdologyResultJob.ensure_job(self.idology_id_number, 30.seconds)
        end
    end

    def as_json
        self.attributes.slice('idology_id_number', 'scan_capture_url', 'verified', 'email', 'phone').merge({
            created_at: created_at.to_timestamp,
            updated_at: updated_at.to_timestamp,
            response_received_at: response_received_at.to_timestamp

        })
    end

    private
    def fetch_result
        response_data = self.class.make_idology_request("scan-capture-poll.svc", {
            queryId: self.idology_id_number
        })
    end

    #curl -k --data-urlencode "username=$USERNAME" --data-urlencode "password=$PASSWORD" --data-urlencode "queryId=$QUERYID" https://web.idologylive.com/api/scan-capture-poll.svc
end
