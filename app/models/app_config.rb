class AppConfig

    def self.all(params = {})

        # client_version is only passed in when this is called from
        # the API.  When it is called from within the inline config,
        # we always get the up-to-date config
        client_version = params.with_indifferent_access['client_version'] || Float::INFINITY

        hash = current(client_version)
        _deprecated = deprecated

        # go backwards through deprecated keys as long as
        # the client's version is earlier than the version
        # in which the key was deprecated
        _deprecated.keys.sort.reverse.each do |version|
            config = _deprecated[version]
            if client_version < version
                hash.merge!(config)
            end
        end

        hash
    end

    def self.current(client_version = nil)

        current_config = {
            'segmentio_key' => ENV['SEGMENTIO_WRITE_KEY'],
            'segmentio_disabled' => ENV['DISABLE_EVENT_LOGGING'],
            'sentry_dsn' => ENV['SENTRY_DSN'],
            'stripe_publishable_key' => ENV['STRIPE_PUBLISHABLE_KEY'],
            'stripe_image' => ENV['STRIPE_IMAGE'] || 'https://uploads.smart.ly/downloads/stripe/smart.ly/favicon-192x192.png', # FIXME: https://trello.com/c/X5DWN7Xk
            'stripe_image_quantic' => ENV['STRIPE_IMAGE_QUANTIC'] || 'https://uploads.smart.ly/downloads/stripe/quantic.edu/favicon-192x192.png',
            'google_api_key' => ENV['GOOGLE_API_KEY'],
            'google_places_api_key' => ENV['GOOGLE_API_KEY'], # FIXME: https://trello.com/c/b4eU2odj
            'linked_in_api_key' => ENV['LINKED_IN_API_KEY'],
            'http_queue_events_enabled' => ENV['HTTP_QUEUE_EVENTS_ENABLED'],
            'disable_3x_image_support' => ENV['DISABLE_3X_IMAGE_SUPPORT'] ,
            'disable_auto_events_for' => disable_auto_events_for,
            'join_config' => join_config,
            'force_idp_initiated_login_urls': force_idp_initiated_login_urls,
            'demo_lesson_id': ENV['DEMO_LESSON_ID'],
            'default_playlist_id': ENV['DEFAULT_PLAYLIST_ID'],
            'reporting_maintenance': reporting_maintenance,
            'jmu_playlist_id': ENV['JMU_PLAYLIST_ID'] || ENV['DEFAULT_PLAYLIST_ID'],
            'math101a_playlist_id': ENV['MATH101A_PLAYLIST_ID'] || ENV['DEFAULT_PLAYLIST_ID'],
            'math101b_playlist_id': ENV['MATH101B_PLAYLIST_ID'] || ENV['DEFAULT_PLAYLIST_ID'],
            'free_mba_groups': ENV['FREE_MBA_GROUPS'],
            'mba_application_survey_url': ENV['MBA_APPLICATION_SURVEY_URL'],
            'skip_cohort_application_form': (Rails.env.development? && ENV['SHOW_COHORT_APPLICATION_FORM_IN_DEV_MODE'] != 'true') ? 'true' : 'false',
            'alternate_emba_us_banner_message': ENV['ALTERNATE_EMBA_US_BANNER_MESSAGE'],
            'google_oauth_id': ENV['OAUTH_GOOGLE_KEY'],
            'recaptcha_site_key': ENV['RECAPTCHA_SITE_KEY'],
            'app_env_name': ENV['APP_ENV_NAME'],
            'quantic_domain': quantic_domain,
            'smartly_domain': smartly_domain,
            'domain': domain,
            'social_name': social_name,
            'social_hashtag': social_hashtag,
            'statista_url': ENV['STATISTA_URL'],
            'forward_to_correct_domain_in_devmode': ENV['FORWARD_TO_CORRECT_DOMAIN_IN_DEVMODE'],
            'activate_ziprecruiter_search': ENV['ACTIVATE_ZIPRECRUITER_SEARCH'],
            'ziprecruiter_api_key_us': ENV['ZIPRECRUITER_API_KEY_US'],
            'enable_offline_mode': ENV['ENABLE_OFFLINE_MODE'],
            'ziprecruiter_api_key_de': ENV['ZIPRECRUITER_API_KEY_DE'],
            'ziprecruiter_api_key_ca': ENV['ZIPRECRUITER_API_KEY_CA'],
            'ziprecruiter_api_key_gb': ENV['ZIPRECRUITER_API_KEY_GB'],
            'ziprecruiter_search_url': ENV['ZIPRECRUITER_SEARCH_URL'],
            'is_alternative_staging_environment': is_alternative_staging_environment
        }.with_indifferent_access

        begin
            current_config['default_page_metadata'] = GlobalMetadata.find_by_site_name('__defaults').as_json
        rescue Exception => ex
            if !Rails.env.test?
                Raven.capture_exception(ex)
            end
        end

        current_config
    end

    def self.force_idp_initiated_login_urls
        if Rails.env.test?
            # see vendor/front-royal/components/authentication/spec/casper/saml_casper_spec.js
            {
                'test' => 'http://example.com'
            }
        elsif ENV['FORCE_IDP_INITIATED_LOGIN_URLS']
            ActiveSupport::JSON.decode(ENV['FORCE_IDP_INITIATED_LOGIN_URLS'])
        else
            {}
        end
    end

    def self.join_config_for_sign_up_code(signup_code)
      join_config.values.detect do |entry|
        entry['signup_code'] == (signup_code)
      end
    end

    def self.join_config
        default = {
            "requires_email" => true,
        }

        institution_default = default.merge({
            "auto_select_first_playlist" => true
        })

        # The server configs here define the sign-up-code (possible
        # we could have pushed this to the client) and which fields
        # are required on the record (this definitely needs to be
        # here)
        #
        # routes.rb also uses this list to decide which urls need
        # to be forwarded, although that could probably be pushed to
        # the client as well.
        #
        # The client will add more info to these configs.  We wanted
        # to keep the stuff that just the client needs in the client
        # in order to not worry about client versioning issues and
        # backwards compatability bs.  See authentication/scripts/join_config.js
        return {

            # consumers
            'default' => default.merge({
                "signup_code" => "FREEMBA"
            }),

            # special users
            'blended' => default.merge({
                "signup_code" => "BLENDED",
                "auto_favorite_all_courses" => true
            }),
            'transformafrica' => default.merge({
                "signup_code" => "TRANSFORMAFRICA",
                "auto_favorite_all_courses" => true
            }),

            # institutions
            'opgt16' => institution_default.merge({
                "signup_code" => "OPMANGEORGETOWN"
            }),
            'georgetownmsb' => institution_default.merge({
                "signup_code" => "GEORGETOWNMSB",
                "email_pattern_domain" => "georgetown.edu"
            }),
            'reboot' => institution_default.merge({
                "signup_code" => "REBOOT"
            }),
            'asugsv' => institution_default.merge({
                "signup_code" => "ASUGSV"
            }),
            'wwf' => institution_default.merge({
                "signup_code" => "WWF"
            }),
            'jmu' => institution_default.merge({
                "signup_code" => "JMU"
            }),
            'emiratesdb' => institution_default.merge({
                "signup_code" => "EMIRATESDB"
            }),
            'math101a' => institution_default.merge({
                "signup_code" => "EMUMATH-A",
                "email_pattern_domain" => "emu.edu"
            }),
            'math101b' => institution_default.merge({
                "signup_code" => "EMUMATH-B",
                "email_pattern_domain" => "emu.edu"
            }),
            'gwstatistics' => institution_default.merge({
                "signup_code" => "GWMBASTATISTICS"
            }),
            'nyu' => institution_default.merge({
                "signup_code" => "NYU",
                "email_pattern_domain" => "stern.nyu.edu"
            }),
            'iebusiness' => institution_default.merge({
                "signup_code" => "IEBUSINESS",
                "email_pattern" => /^.+@(faculty\.|student\.)?ie\.edu$/,
                "email_pattern_error_message" => "must be your IE email address"
            }),
            'dubaipolice' => institution_default.merge({
                "signup_code" => "DUBAIPOLICE"
            }),
            'research-study17' => institution_default.merge({
                "signup_code" => "EFFICACY-STAT"
            }),
            'intuit' => institution_default.merge({
                "signup_code" => "INTUIT_PILOT"
            }),
            'advancedplacement' => institution_default.merge({
                "signup_code" => "ADVANCED_PLACEMENT",
                "auto_select_first_playlist" => false
            }),
            'inframark' => institution_default.merge({
                "signup_code" => "inframark",
                "email_pattern_domain" => "inframark.com",
                "auto_select_first_playlist" => false
            }),
            'georgetownmim' => institution_default.merge({
                "signup_code" => "GEORGETOWNMIM",
                "email_pattern_domain" => "georgetown.edu"
            }),
            'miyamiya' => institution_default.merge({
                "signup_code" => "MIYAMIYA",
                "locale" => 'ar'
            }),


            # institutional demos

            # default_demo is used at the /demo route
            'default_demo' => institution_default.merge({
                "signup_code" => "FREEDEMO",
                "institutional_demo" => true
            }),
            'shrm' =>  institution_default.merge({
                "signup_code" => "SHRMDEMO",
                "institutional_demo" => true
            }),

            # hiring managers
            'hiring' => institution_default.merge({
                "signup_code" => "HIRING",
                "auto_favorite_all_courses" => true
            })



        }
    end

    def self.deprecated
        {
            42 => {
                "key" => "value"
            }
        }
    end

    def self.playlist_locale_pack_id(active_playlist_id_key)
        playlist_id = self.all[active_playlist_id_key]

        locale_pack_id = playlist_id &&
            (playlist = Playlist.find(playlist_id)) &&
            playlist.locale_pack &&
            playlist.locale_pack.id

        if locale_pack_id.nil?
            Raven.capture_exception(RuntimeError.new("No active_playlist_locale_pack_id from #{active_playlist_id_key.inspect}"))
        end

        locale_pack_id
    end

    def self.free_mba_groups
        (self.all[:free_mba_groups] || '').split(",").map(&:strip)
    end

    def self.disable_auto_events_for
        @disable_auto_events_for ||= (ENV['DISABLE_AUTO_EVENTS_FOR'].blank? ? [] : ActiveSupport::JSON.decode(ENV['DISABLE_AUTO_EVENTS_FOR']))
    end

    def self.reporting_maintenance
        ENV['REPORTING_MAINTENANCE']
    end

    def self.google_oauth_id
        self.all[:google_oauth_id]
    end

    def self.google_oauth_id_ios
        ENV['OAUTH_GOOGLE_KEY_IOS']
    end

    def self.google_oauth_id_android
        ENV['OAUTH_GOOGLE_KEY_ANDROID']
    end

    # Set automatically in deploy.sh for alternative environments
    def self.is_alternative_staging_environment
        !!ENV['ALTERNATIVE_STAGING_ENVIRONMENT'] ? 'true' : 'false'
    end

    ########################################################################################################################
    # FIXME: everything below here should really be dynamic based on instituion once https://trello.com/c/JQEV2To2 is done
    ########################################################################################################################


    def self.quantic_domain
        Institution.quantic.domain
    end

    def self.smartly_domain
        Institution.smartly.domain
    end

    def self.domain
        self.quantic_domain
    end

    def self.faq_domain
        "support.#{self.domain}"
    end

    def self.smartly_social_name
        'SmartlyHQ'
    end

    def self.quantic_social_name
        'QuanticSchool'
    end

    def self.social_name
        self.quantic_social_name
    end

    def self.twitter_id
        '2585893555'
    end

    def self.social_hashtag
        # Smartly = '#SmartlyMBA' or '#Smartly'
        '#Quantic'
    end

end