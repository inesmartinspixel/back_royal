# == Schema Information
#
# Table name: hiring_relationships
#
#  id                         :uuid             not null, primary key
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  hiring_manager_id          :uuid             not null
#  candidate_id               :uuid             not null
#  hiring_manager_status      :text             default("pending"), not null
#  candidate_status           :text             default("hidden"), not null
#  hiring_manager_priority    :float            default(100.0), not null
#  hiring_manager_closed      :boolean
#  candidate_closed           :boolean
#  open_position_id           :uuid
#  matched_at                 :datetime
#  hiring_manager_closed_info :json             not null
#  conversation_id            :integer
#  matched_by_hiring_manager  :boolean
#  stripe_usage_record_info   :json
#

class HiringRelationship < ActiveRecord::Base

    def self.candidate_statuses
        return %w(pending accepted rejected hidden)
    end

    def self.hiring_manager_statuses
        return %w(pending saved_for_later accepted rejected hidden)
    end

    def self.viewable_hiring_manager_statuses
        return %w(saved_for_later accepted rejected)
    end

    belongs_to :candidate, :class_name => 'User', optional: true
    belongs_to :hiring_manager, :class_name => 'User', optional: true
    belongs_to :conversation, :class_name => 'Mailboxer::Conversation', optional: true

    delegate :hiring_team, :to => :hiring_manager

    validates :candidate_status, inclusion: { in: self.candidate_statuses, message: "%{value} is not a valid candidate status" }
    validates :hiring_manager_status, inclusion: { in: self.hiring_manager_statuses, message: "%{value} is not a valid hiring manager status" }
    validate :no_invalid_status_changes

    before_save :set_matched_at

    before_save :handle_matched_by_hiring_manager

    # FIXME: https://trello.com/c/KacuP08X
    before_save :hide_teammates_relationships, if: :just_accepted_by_hiring_manager?

    # It is very unlikely, but it is possible that hide_teammates_relationships
    # could still let a conflict through in the case of concurrent updates.  Like this:
    # 1. Process 1 updates, checks for conflicts, finds none, but has not yet committed
    # 2. Process 2 updates, checks for conflicts, finds none, but has not yet committed
    # 3. Both processes commit, creating a conflict
    # This leads us to call hide_teammates_relationships before the save and after the commit. There are
    # however cases where the conflict cannot be resolved (if both sides have actually accepted) and we surface
    # an error to the user.
    after_commit :hide_teammates_relationships, if: :just_accepted_by_hiring_manager?

    # There is a brief window while the transaction is working that invalid candidate_position_interests
    # can be returned to a user since we do the cleanup after the transaction is committed. We decided
    # that this is acceptable. Originally we went down a path of creating database triggers to
    # prevent even the aforementioned window from occuring, but that got complicated and had
    # its own concurrency issues. While working on those concurrency issues we realized that Serializable
    # transactions can be used to ensure that a conflict never ever is put in the db (See https://trello.com/c/s6eC64aI).
    # But again, we decided that more work on this was not necessary since it is not a big deal for a hiring manager
    # to see an interest that would otherwise be hidden if they happen to access the database during
    # the brief update window.
    after_commit :hide_candidate_position_interests_on_accepted_or_rejected, if: :just_accepted_or_rejected_by_hiring_manager?

    # When connecting now with a candidate from Positions, the candidate position interest
    # is updated with the appropriate hiring_manager_status.
    # However, it is possible to invite a candidate to connect from Browse for a position
    # that candidate has previously expressed interest in. In this case, the invite will
    # be considered matched_by_hiring_manager and will result in an "instant match". We
    # want to update the associated candidate position interest to reflect this.
    after_commit :ensure_associated_candidate_position_interest_invited_on_accepted, if: :just_accepted_by_hiring_manager?

    # We want to handle certain audit trails with our paid users
    before_commit :after_match, if: :just_accepted_and_now_matched?

    after_create :log_creation_events
    after_update :log_update_events

    before_validation :update_candidate_status
    validates_absence_of :candidate_closed, :unless => :candidate_accepted?
    validates_absence_of :hiring_manager_closed, :unless => :hiring_manager_accepted?

    has_one :career_profile, :through => :candidate
    has_one :hiring_application, :through => :hiring_manager

    belongs_to :open_position, optional: true

    self.ignored_columns = %w(archived_after_accepted)

    attr_accessor :selection_details # used to log details about the creation of the relationship
    attr_accessor :new_message # used to log message details in the hiring_relationship:accepted events

    # see note in cohort.rb#ActiveRecord_Relation about why this has to be a separate module
    class ActiveRecord_Relation
        include HiringRelationship::HiringRelationshipRelationExtensions
    end
    include(HiringRelationshipRelationHelpers)

    def self.includes_needed_for_json_eager_loading
        {
            career_profile: CareerProfile.includes_needed_for_json_eager_loading,
            hiring_application: HiringApplication.includes_needed_for_json_eager_loading,
            conversation: Mailboxer::Conversation.includes_needed_for_json_eager_loading
        }
    end

    # FIXME: remove once the migration StrictifyConnectionsConstraint has
    # been run on production
    def self.resolve_existing_team_conflicts

        # this query find every candidate/team combination where
        # 1. someone on the team has accepted the candidate
        # 2. at least one other person on the team has the candidate in
        #     a non-hidden state
        query = %Q~
            SELECT
                candidates.name
                , candidates.id as candidate_id
                , hiring_managers.hiring_team_id
                , count(case when hiring_relationships.hiring_manager_status = 'accepted' then true else null end ) as accepted
                , count(*)
                , array_agg(hiring_manager_status order by hiring_manager_status)
            from hiring_relationships
                join users candidates on candidates.id = candidate_id
                join users hiring_managers on hiring_managers.id = hiring_manager_id
            where  hiring_managers.hiring_team_id is not null
                and hiring_manager_status != 'hidden'
            group BY
                candidates.name
                , candidates.id
                , hiring_managers.hiring_team_id
            having count(case when hiring_relationships.hiring_manager_status = 'accepted' then true else null end ) > 0
                and count(*) > 1
        ~

        result = self.connection.execute(query).to_a

        # for each candidate/team combination, hide all non-accepted relationships
        result.each do |record|
            HiringRelationship.joins("join users hiring_managers on hiring_managers.id = hiring_manager_id")
                .where(
                    candidate_id: record['candidate_id'],
                    "hiring_managers.hiring_team_id" => record['hiring_team_id']
                )
                .where.not(hiring_manager_status: 'accepted')
                .each do |relationship|
                    relationship.update_attribute(:hiring_manager_status, 'hidden')
                end
        end
    end

    def as_json(options = {})
        hash = {
            id: id,

            created_at: created_at.to_timestamp,
            updated_at: updated_at.to_timestamp,
            matched_at: matched_at.to_timestamp,
            candidate_status: candidate_status,
            hiring_manager_status: hiring_manager_status,
            candidate_closed: candidate_closed,
            hiring_manager_closed: hiring_manager_closed,
            open_position_id: open_position_id,
            hiring_manager_priority: hiring_manager_priority,
            matched_by_hiring_manager: matched_by_hiring_manager,

            # FIXME: all this stuff is available in career_profile or hiring_application.  Should
            # we remove it and just read it out of there?
            candidate_id: candidate.id,
            hiring_manager_id: hiring_manager.id,
            candidate_display_name: candidate.name,
            hiring_manager_display_name: hiring_manager.name,
            hiring_manager_email: hiring_manager.email
        }.stringify_keys

        if options[:is_admin]
            hash = hash.merge({
                "hiring_manager_closed_info" => hiring_manager_closed_info
            })
        end

        # since calling as_json on these things
        # can be somewhat expensive, we check the fields first to see if we
        # need them
        fields = hash.keys + ["career_profile", "hiring_application", "conversation"]
        if options[:fields]
            fields = options[:fields].map(&:to_s)
        end
        if options[:except]
            fields = fields - options[:except].map(&:to_s)
        end

        if fields.include?("career_profile")
            career_profile_options = options[:career_profile_options] || {}

            if !career_profile_options.key?(:anonymize)
                begin
                    raise "hiring_relationship as_json called without career_profile_options :: anonymize"
                rescue Exception => err
                    Raven.capture_exception(err)
                    career_profile_options[:anonymize] = false
                end
            end

            hash["career_profile"] = career_profile.as_json(career_profile_options.merge(view: 'career_profiles'))
        end
        if fields.include?("hiring_application")
            hash["hiring_application"] = hiring_application.as_json
        end
        if fields.include?("conversation")
            hash["conversation"] = conversation.as_json(options) # pass along the user_id
        end

        hash.slice!(*fields)

        return hash
    end

    def update_candidate_status
        # Unhide the relationship if the hiring manager's application is accepted and the hiring_manager_status has changed
        # to be accepted. Otherwise, ensure that candidate status remains hidden.
        if(self.hiring_application && self.hiring_application.status == "accepted")
            if changes['hiring_manager_status'] && changes['hiring_manager_status'][1] == 'accepted' && candidate_status == 'hidden'
                self.candidate_status = 'pending'
            end
        else
            self.candidate_status = 'hidden'
        end
    end

    def no_invalid_status_changes
        if changes['hiring_manager_status'] == ["accepted", "pending"]
            errors.add(:hiring_manager_status, "Cannot be changed from accepted to pending")
        end

        if changes['hiring_manager_status'] == ["accepted", "rejected"]
            errors.add(:hiring_manager_status, "Cannot be changed from accepted to rejected")
        end
    end

    def accepted?
        [:hiring_manager_status, :candidate_status].each do |key|
            current_status = send(key)
            if ['accepted', 'sent_message', 'hired'].exclude?(current_status)
                return false
            end
        end
        return true
    end

    def user_ids
        [hiring_manager_id, candidate_id]
    end

    def visible_to_candidate?
        if self.candidate_status != "hidden"
            return true
        end
        return false
    end

    def log_creation_events

        # For better querying in redshift, we pull out selected_by_policy and found_with_search_id
        if self.selection_details
            selection_details = self.selection_details.with_indifferent_access
            payload = {'selection_details' => self.selection_details}
            payload['selected_by_policy'] = selection_details['policy']
            payload['found_with_search_id'] = selection_details['found_with_search_id']
        else
            payload = {}
        end
        log_events "created", payload
    end

    def log_update_events

        just_closed_by_hiring_manager = (saved_change_to_hiring_manager_closed && hiring_manager_closed)
        just_closed_by_candidate = (saved_change_to_candidate_closed && candidate_closed)

        # It is not JUST closed if, for example, candidate_closed just changed from false to true
        # but hiring_manager_closed was already true.  In that case it was already closed, so we
        # don't want to log the 'closed' event now. (The UI doesn't actually give a way to close a
        # relationship that the other person has closed already, but it's still possible so just being
        # careful here)
        just_closed = (just_closed_by_candidate && !hiring_manager_closed) ||
                        (just_closed_by_hiring_manager && !candidate_closed) ||
                        (just_closed_by_hiring_manager && just_closed_by_candidate) # these would never both change at once, but nothing prevents it, so...
        if just_closed
            log_events("closed", {
                closed_by: just_closed_by_hiring_manager ? 'hiring_manager' : 'candidate',
                hiring_manager_closed_info: hiring_manager_closed_info,
                hiring_manager_company_name: hiring_manager.company_name,
                open_position_id: open_position_id
            }).each { |event| event.log_to_external_systems}

            # If the hiring manager closed the connection and left feedback then send a message to Slack
            if just_closed_by_hiring_manager && hiring_manager_closed_info && hiring_manager_closed_info["feedback"].present?
                slack_info = [
                    ["Candidate Name", candidate.name],
                    ["Candidate Email", candidate.email],
                    ["Reason for Closing", hiring_manager_closed_info["reasons"].join(", ")],
                    ["Feedback to Smartly", hiring_manager_closed_info["feedback"]],
                    ["Hiring Manager Editor URL", "https://smart.ly/admin/careers/hiring_applications?id=#{hiring_manager.hiring_application.id}"],
                    ["Candidate Editor URL", "https://smart.ly/admin/users/applicants?id=#{candidate.id}"]
                ]

                attachments = [
                    {
                        color: '#36A64F',
                        fields: slack_info.map { |info|  { title: info[0], value: info[1], short: true }  }
                    }
                ]

                LogToSlack.perform_later(Slack.talent_notify_channel, "#{hiring_manager.name} (#{hiring_manager.email}) left feedback for a closed connection", attachments)
            end
        end
    end

    def log_events(event_type, payload = {})
        [
            log_event(event_type, "hiring_manager", payload),
            log_event(event_type, "candidate", payload)
        ]
    end

    def log_event(event_type, role, payload = {})
        Event.create_server_event!(
            SecureRandom.uuid,
            send("#{role}_id"),
            "hiring_relationship:#{event_type}",
            log_info(role).merge(payload)
        )
    end

    def matching_employment_type_count
        (hiring_application.position_descriptors & career_profile.employment_types_of_interest).size
    end

    def matching_industries_count
        ([hiring_application.industry] & career_profile.job_sectors_of_interest).size
    end

    def matching_roles_count
        (hiring_application.role_descriptors & career_profile.primary_areas_of_interest).size
    end

    def location_state_match_descriptor
        if same_state?
            "same_location"
        elsif career_profile.willing_to_relocate? and career_profile.open_to_remote_work?
            'relocatable_and_remote'
        elsif career_profile.willing_to_relocate?
            'willing_to_relocate'
        elsif career_profile.open_to_remote_work?
            'open_to_remote_work'
        else
            'incompatible'
        end
    end

    def same_state?
        career_profile.place_details['administrative_area_level_1'] == hiring_application.place_details['administrative_area_level_1']
    end

    def hiring_manager_status_binary_flag
        if hiring_manager_status == 'accepted'
            1
        elsif hiring_manager_status == 'rejected'
            0
        else
            raise "Cannot determine hiring_manager_status_binary_flag for hiring_manager_status=#{hiring_manager_status.inspect}"
        end
    end

    def career_profile_id
        career_profile && career_profile.id
    end

    def candidate_accepted?
        candidate_status == 'accepted'
    end

    def hiring_manager_accepted?
        hiring_manager_status == 'accepted'
    end

    # FIXME: remove once migration has added conversation_id for all conversations
    def unassociated_conversation
        unless defined?(@unassociated_conversation)
            @unassociated_conversation = Mailboxer::Conversation.find_by_sql(%Q~
                with conversation_ids_for_messages_from_hiring_manager_to_candidate AS MATERIALIZED (
                    SELECT
                        distinct mailboxer_notifications.conversation_id
                    FROM mailboxer_notifications
                        join mailboxer_receipts on mailboxer_receipts.notification_id = mailboxer_notifications.id
                    WHERE
                        mailboxer_notifications.sender_id = '#{hiring_manager_id}'
                        and mailboxer_receipts.receiver_id = '#{candidate_id}'

                )
                , conversation_ids_for_messages_from_candidate_to_hiring_manager AS MATERIALIZED (
                    SELECT
                        distinct mailboxer_notifications.conversation_id
                    FROM mailboxer_notifications
                        join mailboxer_receipts on mailboxer_receipts.notification_id = mailboxer_notifications.id
                    WHERE
                        mailboxer_notifications.sender_id = '#{candidate_id}'
                        and mailboxer_receipts.receiver_id = '#{hiring_manager_id}'

                )
                select
                    distinct mailboxer_conversations.*
                FROM mailboxer_conversations
                WHERE id in (
                    select * from conversation_ids_for_messages_from_hiring_manager_to_candidate
                    union
                    select * from conversation_ids_for_messages_from_candidate_to_hiring_manager
                )
            ~).first
        end
        @unassociated_conversation
    end

    # since this object could be saved multiple times,
    # we can't rely on the saved_changes hash.  So, we store
    # whether the status was ever changed on this object
    def candidate_status=(val)
        if candidate_status != 'accepted' && val == 'accepted'
            @just_accepted = true
            @just_accepted_by_candidate = true
        end
        super
    end

    # since this object could be saved multiple times,
    # we can't rely on the saved_changes hash.  So, we store
    # whether the status was ever changed on this object
    def hiring_manager_status=(val)
        if hiring_manager_status != 'accepted' && val == 'accepted'
            @just_accepted = true
            @just_accepted_by_hiring_manager = true
        end

        if hiring_manager_status != 'rejected' && val == 'rejected'
            @just_rejected_by_hiring_manager = true
        end

        super
    end

    def just_accepted?
        @just_accepted || false
    end

    def just_accepted_by_candidate?
        @just_accepted_by_candidate || false
    end

    def just_accepted_by_hiring_manager?
        @just_accepted_by_hiring_manager || false
    end

    def just_accepted_or_rejected_by_hiring_manager?
        @just_accepted_by_hiring_manager || @just_rejected_by_hiring_manager || false
    end

    def just_accepted_and_now_matched?
        just_accepted? && matched?
    end

    def closed?
        hiring_manager_closed || candidate_closed
    end

    def matched?
        hiring_manager_status == 'accepted' && candidate_status == 'accepted'
    end

    def matched_not_closed?
        matched? && !closed?
    end

    def candidate_position_interest_for_any_related_open_positions?
        CandidatePositionInterest
            .joins(:open_position)
            .where(
                candidate_id: self.candidate_id,
                candidate_status: 'accepted',
                open_positions: {
                    hiring_manager_id: self.hiring_manager.hiring_team_members.pluck(:id) + [self.hiring_manager_id]
                }).count > 0
    end

    def handle_matched_by_hiring_manager
        # If the candidate has previously expressed interest in ANY OpenPosition created by
        # this hiring manager, this relationship should be considered matched_by_hiring_manager
        return unless self.hiring_manager_status == 'accepted' && candidate_position_interest_for_any_related_open_positions?
        self.candidate_status = 'accepted'
        self.matched_by_hiring_manager = true
    end

    # warning! this requires loading up the conversation, so it an be a slow
    # process if done on a lot of relationships
    def last_activity_at
        [conversation && conversation.last_message_at, updated_at].compact.max
    end

    def days_since_matched(time_zone = 'Eastern Time (US & Canada)')
        return nil unless matched_at
        (Time.now.in_time_zone(time_zone).beginning_of_day - matched_at.in_time_zone(time_zone).beginning_of_day).to_timestamp / 1.day
    end

    # we keep track of things that were updated by this so that we
    # can send them down to the client
    def side_effects
        @side_effects ||= {
            hiring_relationships: {},
            candidate_position_interests: {}
        }
    end

    def notify_participants_if_just_accepted
        if just_accepted_by_candidate?
            log_event("accepted", "hiring_manager").log_to_external_systems
        end

        if just_accepted_by_hiring_manager?
            log_event("accepted", "candidate").log_to_external_systems

            now = Time.now
            RemindCandidateOfPendingHiringRelationship.set(wait_until: now + 24.hours).perform_later(self.id, 24)
            RemindCandidateOfPendingHiringRelationship.set(wait_until: now + 72.hours).perform_later(self.id, 72)
        end
    end

    def ensure_associated_candidate_position_interest_invited_on_accepted
        if self.hiring_manager_status == 'accepted' && associated_candidate_position_interest
            associated_candidate_position_interest.update!(hiring_manager_status: "invited")
            side_effects[:candidate_position_interests][associated_candidate_position_interest.id] = associated_candidate_position_interest
        end
    end

    private
    def log_info(role)
        params = {
            hiring_relationship_id: self.id,
            hiring_relationship_role: role,
            hiring_manager_status: hiring_manager_status,
            candidate_status: candidate_status,
            hiring_manager_nickname: hiring_manager.preferred_name,
            candidate_nickname: candidate.preferred_name,
            new_message: new_message.as_json,
            matched_by_hiring_manager: matched_by_hiring_manager
        }

        if role == 'hiring_manager'
            params.merge!(
                candidate_id: candidate_id
            )
        else
            params.merge!(
                hiring_manager_id: hiring_manager_id,
                hiring_manager_company_name: hiring_manager.company_name
            )
        end

        params
    end

    private
    def set_matched_at
        if matched? && matched_at.nil?
            self.matched_at = Time.now
        elsif !matched?
            self.matched_at = nil
        end

    end

    def associated_candidate_position_interest
        unless defined? @associated_candidate_position_interest
            @associated_candidate_position_interest = CandidatePositionInterest.find_by({candidate_id: self.candidate_id, open_position_id: self.open_position_id})
        end
        @associated_candidate_position_interest
    end

    # FIXME: Super edge-case concurrency issue: If you call hide_teammates_relationships, and then a teammate
    # saves this person for later (and commits) before you save yours and commit, it's going to trigger
    # a multiple connections error and your update will not be saved.  This will be surfaced to
    # the user and it will say that the teammate has already accepted the connection.  What should
    # happen is that we should catch that error here, check if the the error can be resolved (it can
    # be if only one side is accepted and the other has a different status), and only error if it
    # cannot be resolved.
    def hide_teammates_relationships
        relationships_to_hide = hiring_manager
            .relationships_from_team
            .where.not(hiring_manager_id: hiring_manager.id)
            .where.not(hiring_manager_status: ['hidden', 'accepted'])
            .where(candidate_id: candidate_id)

        relationships_to_hide.each do |hiring_relationship|
            hiring_relationship.update_attribute(:hiring_manager_status, 'hidden')
            side_effects[:hiring_relationships][hiring_relationship.id] = hiring_relationship
        end
    end

    def hide_candidate_position_interests_on_accepted_or_rejected
        if ['accepted', 'rejected'].include?(self.hiring_manager_status)
            # if we are updating to accepted, we need to hide records for this hiring manager and all
            # teammates.  If we are updating to rejected, we just need to hide records for this
            # hiring manager
            hiring_manager_ids = hiring_manager_status == 'accepted' ? self.hiring_manager.hiring_team_members.pluck(:id) + [self.hiring_manager_id] : [self.hiring_manager_id]

            # Explanation of .where.not(hiring_manager_status: CandidatePositionInterest.hidden_or_rejected_hiring_manager_statuses):
            # If the hiring manager rejects a candidate, or accepts zer for another position, then we update
            # existing candidate position interests.  If those interests are in a rejected status already, then we leave them
            # as is.  They will stay in the "passed" list, showing the hiring manager that ze once passed this candidate
            # in the context of this particular position.  If those interests are in any other state, then we hide them,
            # since it is now impossible for the hiring manager to take any action on the candidate in the context of a different position.
            CandidatePositionInterest
                .joins(:open_position)
                .where(
                    candidate_id: self.candidate_id,
                    open_positions: {
                        hiring_manager_id: hiring_manager_ids
                    })
                .where.not(open_position_id: self.open_position_id)
                .where.not(hiring_manager_status: CandidatePositionInterest.hidden_or_rejected_hiring_manager_statuses)
                .each do |interest|
                    interest.hide_from_hiring_manager!
                    side_effects[:candidate_position_interests][interest.id] = interest
                 end
        end
    end

    def after_match
        if self.stripe_usage_record_info.nil? && hiring_team&.subscription_required && hiring_team&.primary_subscription
            IncrementMatchUsageJob.perform_later(self.id)
        end
    end

end
