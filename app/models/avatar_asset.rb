# == Schema Information
#
# Table name: avatar_assets
#
#  id                :uuid             not null, primary key
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  file_file_name    :string
#  file_content_type :string
#  file_file_size    :integer
#  file_updated_at   :datetime
#  file_fingerprint  :string
#  source_url        :text
#  url               :text
#

class AvatarAsset < ApplicationRecord

    class DisallowedScheme < RuntimeError; end

    has_one :user, foreign_key: :avatar_id

    has_attached_file :file,
        :path => :path,
        :processors => [:bulk],
        :s3_credentials => {
            :bucket => ENV['AWS_UPLOADS_BUCKET']
        },
        :s3_host_alias => ENV['AWS_UPLOADS_BUCKET']

    after_post_process -> { Paperclip::BulkQueue.process(file) }

    validates_attachment_content_type :file, :content_type => %w(
        image/jpeg
        image/jpg
        image/pjpeg
        image/pipeg
        image/png
        image/svg+xml
        image/gif
    )

    validates_attachment_size :file, less_than: 3.megabytes

    before_save :set_url

    def set_url
        self.url = self.file&.url
    end

    def fetch_file_from_url(url)
        self.source_url = url
        file = URI.parse(url)

        # not sure if this is necessary, but trying to prevent users from trying to
        # hit our file system or do something else shady
        raise DisallowedScheme.new("Disallowed scheme") unless ['http', 'https'].include?(file.scheme)
        self.file = file.open
    end

    def as_json(options = {})
        {
            id: id,
            file_file_name: file_file_name,
            file_updated_at: file_updated_at,
            file_content_type: file_content_type,
            file_file_size: file_file_size,
            url: self.url
        }
    end

    def path

        # If we have a url already, then use that to generate the path.  If we do not yet
        # have one, then generate one.
        if url.nil?
            self.id ||= SecureRandom.uuid
            self.created_at ||= Time.now
            self.updated_at ||= self.created_at
            "avatar_assets/#{self.id}/:fingerprint.#{self.created_at.to_timestamp}.:filename"
        else
            url.match(/avatar_assets.+/).to_s
        end


    end

end
