# == Schema Information
#
# Table name: hiring_teams
#
#  id                    :uuid             not null, primary key
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  title                 :text             not null
#  owner_id              :uuid
#  subscription_required :boolean          default(TRUE), not null
#  hiring_plan           :text
#  domain                :text             not null
#

class HiringTeam < ApplicationRecord
    include Subscription::SubscriptionConcern # we wrote this one
    include HiringTeam::SubscriptionConcern

    HIRING_PLAN_LEGACY='legacy'
    HIRING_PLAN_PAY_PER_POST='pay_per_post'
    HIRING_PLAN_UNLIMITED_WITH_SOURCING='unlimited_w_sourcing'

    CANCELLATION_REASON_SWITCHING_TO_UNLIMITED='switching to unlimited plan'

    # ensure we run validations on hiring managers when modified in the collection,
    # as well as identify when any hiring manager's association changes
    has_many :hiring_managers, :class_name => 'User',
        :before_remove => :validate_hiring_manager_removal,
        :after_remove => :identify_hiring_manager, :after_add => :identify_hiring_manager

    # this is used for performance in the hiring_index
    has_many :hiring_managers_with_just_ids, -> {select(:id, :hiring_team_id)}, :class_name => 'User'
    has_many :candidate_relationships_with_accepted_hiring_manager_status, :through => :hiring_managers

    has_many :open_positions, :through => :hiring_managers
    has_many :unarchived_open_positions, :through => :hiring_managers

    has_many :candidate_position_interests, :through => :open_positions

    belongs_to :owner, :class_name => 'User', optional: true

    before_destroy :nullify_hiring_managers
    validate :validate_owner_is_team_member

    delegate :email, :to => :owner, :allow_nil => true

    after_save :create_hiring_plan_changed_event

    validates_inclusion_of :hiring_plan, :in => [nil, HIRING_PLAN_LEGACY, HIRING_PLAN_PAY_PER_POST, HIRING_PLAN_UNLIMITED_WITH_SOURCING]

    before_create :ensure_domain

    validates_inclusion_of :subscription_required, :in => [true],
        :unless => Proc.new { [HIRING_PLAN_UNLIMITED_WITH_SOURCING, HIRING_PLAN_LEGACY].include?(hiring_plan)  },
        :message => "can only be false when using unlimited plan"

    # reject(&:destroyed?) is necessary because a subscription can be destroyed in update_from_hash
    validates_inclusion_of :subscription_required, :in => [true],
        :if => Proc.new { subscriptions.reject(&:destroyed?).size > 0 },
        :message => "can only be false when there is no subscription"

    # We can only turn off subscription_required when the hiring manager is accepted
    # Theoretically, there are two problems with this, but practically they should not be issues
    # 1. We should have a symmetrical validation in hiring_application, but we would never
    #       switch a hiring_manager back to pending after making the subscription_required=false
    # 2. We should validate all the hiring_managers, not just the owner.  But after the first team
    #       member, subsequent members are always added in the accepted state
    validates_inclusion_of :subscription_required, :in => [true],
        :if => Proc.new { owner&.hiring_application&.status == 'pending' },
        :message => "can only be false when the hiring manager is already accepted"

    def self.create_from_hash!(hash)
        transaction do
            begin
                instance = initialize_from_hash(hash)
                instance.save!
                instance.hiring_managers.map(&:save!)
                instance
            rescue ActiveRecord::StatementInvalid => err
                if err.message.match("Multiple connections to candidate")
                    instance.errors.add(:hiring_team, "has multiple connections to the same candidate. Please notify engineering to resolve the issue.")
                    raise ActiveRecord::RecordInvalid.new(instance)
                else
                    raise
                end
            end
        end
    end

    def self.initialize_from_hash(hash)
        hash = hash.unsafe_with_indifferent_access
        instance = new
        instance.merge_hash(hash)
        instance
    end

    def self.update_from_hash!(hash)
        # Automatically retries in the event of ActiveRecord::PreparedStatementCacheExpired
        RetriableTransaction.transaction do
            begin
                hash = hash.unsafe_with_indifferent_access
                instance = find_by_id(hash[:id])

                if instance.nil?
                    raise ActiveRecord::RecordNotFound.new("Couldn't find #{self.name} with id=#{hash[:id]}")
                end

                instance.merge_hash(hash)

                # force updated_at to change regardless of other changes
                instance.updated_at = Time.now
                instance.automatically_cancel_subscriptions_when_no_subscription_required
                instance.save!

                # The subscription may have been destroyed if setting subscription_required to false.  If so,
                # subscriptions need to be reloaded so we don't try to include the destroyed subscription
                # the json we send down after saving (there is a test for this)
                instance.subscriptions.reload
                instance

            rescue ActiveRecord::StatementInvalid
                instance.errors.add(:hiring_team, "has multiple connections to the same candidate. Please notify engineering to resolve the issue.")
                raise ActiveRecord::RecordInvalid.new(instance)
            end
        end

    end

    def self.includes_needed_for_json_eager_loading
        [
            :hiring_managers_with_just_ids,
            :subscriptions,
            :owner # so we can include the email in the json
        ]
    end

    def merge_hash(hash)
        hash = hash.unsafe_with_indifferent_access

        # if we're updating the `owner_id` and it's in the collection, disable the `User::validate_hiring_team_owner`
        # validation, since we know we're about to re-assign `owner_id` to another `hiring_manager`
        if self.owner_id != hash["owner_id"] && hash["hiring_manager_ids"]&.include?(hash["owner_id"])
            self.hiring_managers.each do |hiring_manager|
                hiring_manager.skip_hiring_team_owner_validation = true
            end
        end

        (
            %w(title owner_id subscription_required domain hiring_plan)
        ).each do |key|
            if hash.key?(key)
                val = hash[key]
                val = nil if val.is_a?(String) && val.blank?
                self.send(:"#{key}=", val)
            end
        end

        self.update_hiring_managers(hash)

        self
    end


    def update_hiring_managers(hash)

        if hash["hiring_manager_ids"]
            # For performance, create a hash map that indexes the new hiring managers by their id.
            hiring_managers = User.find(hash["hiring_manager_ids"])
            hiring_managers_map = hiring_managers.index_by(&:id)

            # Loop over the current hiring managers and if there's no related record in the hash
            # map set the hiring_team_id to nil and save the hiring manager record.
            self.hiring_managers.each do |hiring_manager|
                if !hiring_managers_map[hiring_manager.id]
                    hiring_manager.hiring_team = nil
                end
            end

            # this actually triggers association update, so performs a write
            # may be unexpected if used outside of an update call!
            self.hiring_managers = hiring_managers
        end
    end

    def as_json(opts = {})
        # this is just here to prevent `primary_subscription` from having to make
        # another database call in its as_json
        if self.primary_subscription
            self.primary_subscription.hiring_team = self
        end

        attrs = self.attributes.as_json.merge({
            "created_at" => self.created_at.to_timestamp,
            "updated_at" => self.updated_at.to_timestamp,
            "hiring_manager_ids" => self.hiring_managers_with_just_id_ids,
            "has_full_access" => self.has_full_access?,
            "subscriptions" => self.subscriptions.as_json,
            "subscription_required" => self.subscription_required,
            "stripe_plans" => self.stripe_plans_to_send_to_client,
            "email" => self.owner&.email
        })

        # This is for a show call. Avoid using it in an index call as loading the
        # users and respective associations will be slow.
        if opts[:fields]&.include?('hiring_managers')
            attrs['hiring_managers'] = self.hiring_managers.as_json
        end

        attrs
    end

    # Sets the hiring_team_id foreign key on all associated hiring managers to NULL.
    # We've implemented it this way to ensure that IdentifyUserJobs get enqueued.
    def nullify_hiring_managers
        self.owner = nil
        self.save! # this has to be saved first so that we can remove the owner from the team
        self.hiring_managers.each do |hiring_manager|
            hiring_manager.hiring_team = nil
            hiring_manager.save!
        end
    end

    def validate_owner_is_team_member
        if owner && owner.hiring_team_id != self.id
            errors.add(:owner, "must be a member of the team")
        end
    end

    # Removing a hiring manager from a team might make the user invalid (since accepted
    # hiring managers are required to be on teams).  So we check the users' validity
    # before removing them.
    def validate_hiring_manager_removal(hiring_manager)
        unless hiring_manager.valid?
            errors.add(:hiring_manager, "cannot be removed: #{hiring_manager.errors.full_messages.join(',')}")
            raise ActiveRecord::RecordInvalid.new(self)
        end
    end

    def identify_hiring_manager(hiring_manager)
        hiring_manager.identify
    end

    def should_anonymize_interest?(candidate_position_interest)
        return false if has_full_access?

        return !freemium_interest_ids.include?(candidate_position_interest.id)
    end

    def is_freemium_candidate?(candidate_id)
        freemium_candidate_ids.include?(candidate_id)
    end

    def freemium_interests
        ensure_freemium_interests_loaded
        @freemium_interests
    end

    def freemium_candidate_ids
        ensure_freemium_interests_loaded
        @freemium_candidate_ids
    end

    def freemium_interest_ids
        ensure_freemium_interests_loaded
        @freemium_interest_ids
    end

    def ensure_freemium_interests_loaded
        raise RuntimeError.new("Only valid if !has_full_access") if has_full_access?
        unless defined? @freemium_interests

            reviewed_statuses_str = CandidatePositionInterest.reviewed_hiring_manager_statuses.map { |status| "'#{status}'" }.join(',')

            sql = %Q~

                with prioritized_interests AS MATERIALIZED (
                    SELECT
                        open_position_id
                        , candidate_position_interests.id as id
                        , candidate_position_interests.candidate_id
                        , candidate_position_interests.created_at
                        , candidate_position_interests.hiring_manager_priority
                        , CASE
                              WHEN hiring_manager_status in (#{reviewed_statuses_str}) THEN 0
                              ELSE null
                          END AS reviewed_status_priority
                        , CASE
                              WHEN admin_status = 'outstanding' THEN 0
                              WHEN admin_status = 'reviewed' THEN 1
                              ELSE null
                          END AS admin_status_priority
                    FROM
                        candidate_position_interests
                        join open_positions on candidate_position_interests.open_position_id = open_positions.id
                        join users on open_positions.hiring_manager_id = users.id
                    where hiring_team_id = '#{self.id}'
                        and candidate_status='accepted'
                        and hiring_manager_status != 'hidden'
                )

                -- Then we group the interests by position, creating an array of the
                -- top 3 candidates according to the sort and a separate array of any candidate
                -- who has already been acted upon

                , grouped AS MATERIALIZED (
                    SELECT
                        open_position_id
                        , array_agg(
                            prioritized_interests.id
                            order by
                                reviewed_status_priority
                                , admin_status_priority
                                , hiring_manager_priority
                                , created_at
                          ) prioritized_interest_ids
                        , array_remove(array_agg(
                            case when reviewed_status_priority = 0 then
                                prioritized_interests.id
                            else null end
                        ), null) as reviewed_interest_ids
                    FROM
                        prioritized_interests
                    group by open_position_id
                )

                -- Then we limit to the top 3, plus any other ones who have already been acted upon
                -- (because of the ordering, any who have been acted upon will be in the top 3, so this
                -- only matters if more than 3 have already been acted upon, which can only happen if
                -- a team goes from in good standing to in bad standing)

                , limited AS MATERIALIZED (
                    SELECT
                        open_position_id
                        , prioritized_interest_ids[1:3] || reviewed_interest_ids as interest_ids
                    from grouped
                )

                -- unique the ids
                , uniq AS MATERIALIZED (
                    select distinct unnest(interest_ids) as interest_id from limited
                )

                -- add in the candidate_ids
                , with_candidate_ids AS MATERIALIZED (
                    SELECT
                        interest_id
                        , candidate_id
                    from uniq
                        join prioritized_interests
                            on prioritized_interests.id = interest_id
                )

                select * from with_candidate_ids
                ~

            @freemium_interests = self.class.connection.execute(sql).to_a
            @freemium_candidate_ids = Set.new(@freemium_interests.map { |row| row['candidate_id']})
            @freemium_interest_ids = Set.new(@freemium_interests.map { |row| row['interest_id']})
        end
    end

    def ensure_domain
        if self.domain.blank? && owner
            self.domain = owner.email_domain
        end
    end

    def create_hiring_plan_changed_event

        return unless self.owner
        return unless self.saved_change_to_hiring_plan?

        old_plan, new_plan = self.previous_changes[:hiring_plan]

        Event.create_server_event!(
            SecureRandom.uuid,
            self.owner_id,
            "hiring_team:hiring_plan_changed",
            {
                hiring_plan: new_plan,
                old_hiring_plan: old_plan
            }
        ).log_to_external_systems(nil, false)
    end

end
