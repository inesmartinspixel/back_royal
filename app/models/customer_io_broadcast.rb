module CustomerIoBroadcast

    class << self

        def trigger_broadcast(id)
            uri = URI.parse("https://api.customer.io/v1/api/campaigns/#{id}/triggers")

            session = Net::HTTP.new(uri.host, uri.port)
            session.use_ssl = true

            promoted_admission_rounds = AdmissionRound.promoted_admission_rounds

            request = Net::HTTP::Post.new(uri)
            request.basic_auth ENV['CUSTOMER_IO_SITE_ID'], ENV['CUSTOMER_IO_API_KEY']
            request.body = {}.to_json
            request.body = {

                # FIXME: timezone
                data: AdmissionRound.promoted_rounds_event_attributes('America/Mendoza', promoted_admission_rounds),
                ids: ['20b5efb2-e46f-47d1-af52-13f136856a6b']
            }.to_json

            session.start do |http|
                puts uri
                http.request(request)
            end
        end

    end

end