=begin

OwnsPayments should be included in any active record class that can
be associated with a subscriptions and payments.

As of 1/11/2019, User and HiringTeam include this module.

Including OwnsPayments in an active record class sets up joins
betwen that class and payment-related things.  As of 5/11/2019, those things
are subscriptions and billing_transactions.  So, when User includes OwnsPayments,
the following methods are create:

User#subscriptions
User#billing_transactions
HiringTeam#subscriptions
HiringTeam#billing_transactions

Subscription#user
BillingTransaction#user
Subscription#hiring_team
BillingTransaction#hiring_team


*********** Adding a new payment-related model ***********************************

If there is a new payment related model in the future (in addition to subscriptions and billing_transactions)

    1. Create a migration that sets up the appropriate join tables
            between each owner table (i.e. users, hiring_teams) and the new
            payment-related table.
    1. Add new calls below inside of `included do`
    1. Call `OwnsPayments.require_klasses_that_own_payments` inside of the new model. (For
        and example, see BillingTransaction)



*********** Adding the ability to own payments to a new model *********************

If there is a new payment owner in the future (in addition to users and subscriptions):

    1. Create a migration that sets up the appropriate join tables
        between each payment-related table (i.e. subscriptions, billing_transactions) and the new
        owner.  For an example, see the migration CreateBillingTransactions (but if you're copying
        and pasting it, pay attention to the stuff about foreign keys, as your needs
        may be different).
    1. include OwnsPayments in the new owner model
    1. Add the new owner to require_klasses_that_own_payments

=end
module OwnsPayments
    extend ActiveSupport::Concern

    included do
        setup_joins_on_owner(:subscriptions)
        setup_joins_on_owner(:billing_transactions)

        setup_joins_on_payment_related_thing(Subscription)
        setup_joins_on_payment_related_thing(BillingTransaction)
    end

    # It is unfortunate that this module needs to keep a list of
    # every class that includes it, but we're breaking the activesupport autoloading
    # by the way we're auto-generating classes, so without this some specs might
    # fail intermittently because certain things are not loaded correctly.
    # (require_dependency is like require, but it plays nice with autoreloading. To
    # see why we need this, change these to `require`, start a rails server, try hitting
    # BillingTransactionsController#index, then edit billing_transaction.rb and try indexing again.
    # You'll get an error about the associations missing.  See
    # https://guides.rubyonrails.org/autoloading_and_reloading_constants.html#constant-reloading)
    def self.require_klasses_that_own_payments
        require_dependency 'user'
        require_dependency 'hiring_team'
    end

    module ClassMethods

        def create_join_classes(join_with_plural)

            # join_with_plural is like `subscriptions` or `billing_transactions`
            this_table_name = self.table_name # users or hiring_teams
            join_with_singular = join_with_plural.to_s.singularize # subscription, billing_transaction
            this_singular = this_table_name.singularize # user or hiring_team
            class_name = "#{join_with_singular}_#{this_singular}_join".camelize # SubscriptionUserJoin, BillingTransactionHiringTeamJoin

            klass = Class.new(ApplicationRecord) do
                self.table_name = "#{join_with_plural}_#{this_table_name}" # subscriptions_users, billing_transactions_hiring_teams
                cattr_accessor :owner_meth, :join_with_singular

                belongs_to join_with_singular.to_sym # subscription, billing_transaction
                belongs_to this_singular.to_sym # user, hiring_team

                # ensure_stripe_customer_current must be run after create
                # to ensure the customer is updated in stripe in
                # the following case
                # 1. hiring team has a subscription
                # 2. subscription is deleted
                # 3. email is changed
                # 4. new subscription is created
                after_create :ensure_stripe_customer_current

                after_create :trigger_after_create_on_owner

                # returns the user, hiring_team, etc.
                def owner
                    send(self.class.owner_meth)
                end

                def ensure_stripe_customer_current
                    owner.ensure_stripe_customer_current
                end

                # see SubscriptionConcern#after_subscription_added for an example of this in use
                def trigger_after_create_on_owner
                    meth = :"after_#{join_with_singular}_added" # i.e. after_subscription_added
                    if owner.respond_to?(meth)
                        item = self.send(join_with_singular) # i.e. subscription, billing_transaction
                        owner.send(meth, item)
                    end
                end
            end

            klass.owner_meth = this_singular.to_sym # user or hiring_team
            klass.join_with_singular = join_with_singular.to_sym # subscription or billing_transaction

            Object.const_set(class_name.to_sym, klass)
        end

        def setup_joins_on_payment_related_thing(klass)

            # klass is like `Subscription` or `BillingTransaction`
            payment_thing_singular = klass.table_name.to_s.singularize # subscription, billing_transaction
            this_name_lower_singular = self.table_name.singularize # user, hiring_team

            join_name = "#{payment_thing_singular}_#{this_name_lower_singular}_join".to_sym # subscription_user_join
            klass.has_one join_name # BillingTransaction.has_one :subscription_user_join, :dependent => :destroy
            klass.has_one this_name_lower_singular.to_sym, :through => join_name # BillingTransaction.has_one :user

            klass.send(:before_destroy) do
                payment_related_thing = self

                # Before removing the join, cache the current value on the owner.
                # So, if I'm deleting a subscription, owner.subscriptions will consistently include
                # the deleted subscription if referenced after the deletion.  Without this line of
                # code here, the deleted subscription might be included or might not be, depending on
                # whether or not the `subscriptions` association was referenced before the destroy.  That
                # is a recipe for mysterious bugs.
                # For an example of where this causes issues, see subscriptions_controller_spec.rb#it "should work to destroy an unlimited subscription"
                # and run that spec with this commented out
                if payment_related_thing.respond_to?(:owner) && payment_related_thing.owner.present?
                    payment_related_thing.owner.send(payment_thing_singular.pluralize.to_sym).to_a # subscriptions, etc.
                end

                payment_related_thing.send(join_name)&.destroy
            end
        end

        def setup_joins_on_owner(join_with_plural)
            create_join_classes(join_with_plural)

            # join_with_plural is like `subscriptions` or `billing_transactions`
            join_with_singular = join_with_plural.to_s.singularize # subscription, billing_transaction
            this_name_lower_singular = self.table_name.singularize # user, hiring_team
            join_singular = :"#{join_with_singular}_#{this_name_lower_singular}_join" # subscription_user_join, billing_transaction_hiring_team_join
            join_plural = join_singular.to_s.pluralize.to_sym # subscription_user_joins, billing_transaction_hiring_team_joins
            join_class_name = join_singular.to_s.camelize # SubscriptionUserJoin, BillingTransactionHiringTeamJoin

            has_many join_plural # e.g. has_many :subscription_user_joins, has_many :billing_transaction_hiring_team_joins
            has_many join_with_plural, :through => join_plural # e.g. has_many :subscriptions, :through => :subscription_user_joins
        end

    end
end