# == Schema Information
#
# Table name: institutions
#
#  id                :uuid             not null, primary key
#  created_at        :datetime
#  updated_at        :datetime
#  name              :string(255)
#  sign_up_code      :string(255)
#  scorm_token       :uuid
#  playlist_pack_ids :uuid             default([]), is an Array
#  domain            :text
#  external          :boolean          default(TRUE), not null
#

class Institution < ApplicationRecord
    include InternalInstitutionMixin
    include Groupable
    include CrudFromHashHelper

    default_scope -> {order(:created_at)}

    has_many :user_joins, class_name: 'InstitutionUserJoin'
    has_many :users, through: :user_joins

    has_many :lesson_stream_locale_packs, :through => :access_groups
    has_many :playlist_locale_packs, :through => :access_groups
    has_many :lesson_streams, :through => :lesson_stream_locale_packs, :source => :content_items
    has_and_belongs_to_many :reports_viewers, :class_name => 'User', :join_table => :institutions_reports_viewers, :validate => false

    validates_uniqueness_of :sign_up_code

    def self.create_from_hash!(hash, meta = {})
        # Automatically retries in the event of ActiveRecord::PreparedStatementCacheExpired
        RetriableTransaction.transaction do
            instance = self.new
            instance.save_from_hash(hash, meta)
        end
    end

    def self.update_from_hash!(hash, meta = {})
        # Automatically retries in the event of ActiveRecord::PreparedStatementCacheExpired
        RetriableTransaction.transaction do
            hash, meta, instance = prepare_update_from_hash(hash, meta)
            instance.save_from_hash(hash, meta)
        end
    end

    def save_from_hash(hash, meta = {})
        orig_group_ids = self.access_groups.pluck(:id)
        orig_playlist_pack_ids = self.playlist_pack_ids
        orig_name = self.name

        self.merge_hash(hash, meta)
        self.save!

        if orig_group_ids.sort != self.access_groups.pluck(:id).sort ||
            orig_playlist_pack_ids.sort != self.playlist_pack_ids.sort ||
            orig_name != self.name
            RefreshMaterializedContentViews.refresh
        end

        self
    end

    def merge_hash(hash, meta = {})
        %w(name id sign_up_code scorm_token playlist_pack_ids domain external).each do |key|
            if hash.key?(key)
                val =  hash[key]
                val = nil if val.is_a?(String) && val.blank?
                self[key] = val
            end
        end

        # Update report viewers
        if hash.key?('reports_viewer_ids')
            self.reports_viewers = User.where(id: hash['reports_viewer_ids'])
            hash.delete('reports_viewer_ids')
        end

        # Update associated group groups
        groups = hash["groups"]
        if groups
            self.access_groups = AccessGroup.where(id: groups.map { |g| g['id'] } )
        end
    end

    def as_json(opts = {})
        fields = opts[:fields]

        institution_json = {
            id: self.id,
            name: self.name,
            scorm_token: self.scorm_token,
            groups: self.access_groups,
            playlist_pack_ids: self.playlist_pack_ids,
            domain: self.domain,
            external: self.external?,
            branding: self.branding
        }

        if self.reports_viewers.loaded?
            institution_json['reports_viewer_ids'] = self.reports_viewers.pluck('id')
        end

        if fields&.include?('users')
            institution_json['users'] = self.users.map do |user|
                {
                    id: user.id,
                    email: user.email,
                    name: user.name
                }
            end
        end

        # we don't always need these
        institution_json['sign_up_code'] = self.sign_up_code if self.has_attribute?('sign_up_code')
        institution_json['updated_at'] = self.updated_at.to_timestamp if self.has_attribute?('updated_at')
        institution_json['created_at'] = self.created_at.to_timestamp if self.has_attribute?('created_at')

        institution_json
    end

    def branding
        self.domain === self.class.quantic.domain ? self.class::QUANTIC_BRANDING : self.class::SMARTLY_BRANDING
    end

    def internal?
        !self.external?
    end
end
