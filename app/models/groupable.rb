# add this module to your model class to allow for the ability to manage
#  view access to via group membership
module Groupable
    extend ActiveSupport::Concern

    class NoSuchGroup < RuntimeError; end

    included do

        assoc_attrs = {}
        snake_cased = if self == Lesson::Stream
            "lesson_stream"
        elsif self == Lesson::Stream::LocalePack
            "lesson_stream_locale_pack"
        elsif self == Playlist::LocalePack
            "playlist_locale_pack"
        else
            self.name.underscore
        end

        foreign_key = self.include?(LocalePackMixin) ? :locale_pack_id : :"#{snake_cased}_id"

        assoc_attrs[:foreign_key] = foreign_key
        has_and_belongs_to_many :access_groups, assoc_attrs

        # FIXME: we may not really want to do this, revisit
        alias_attribute :groups, :access_groups

        AccessGroup.has_and_belongs_to_many :"#{snake_cased.pluralize}", :class_name => self.name, :association_foreign_key => foreign_key


    end

    def replace_groups(group_name_list)
        self.access_groups = AccessGroup.where("name in (?)", group_name_list)
    end

    def add_to_group(group_name)
        self.id ||= SecureRandom.uuid

        if access_group = AccessGroup.find_by_name(group_name)
            if !access_groups.include?(access_group)
                self.access_groups << access_group
                true
            else
                false
            end
        else
            raise(NoSuchGroup.new("No #{group_name} group"))
        end
    end

    def remove_from_group(group_name)
        if access_group = AccessGroup.find_by_name(group_name)
            self.access_groups.delete(access_group)
            true
        else
            false
        end
    end

    def in_group?(group_name)
        group_names.include?(group_name)
    end

    def group_names
        access_groups.map(&:name).sort
    end

    def validate_in_superviewer_group
        if !in_group?('SUPERVIEWER')
            errors.add(:access_groups, "must include SUPERVIEWER")
        end
    end

end
