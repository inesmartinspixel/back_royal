# == Schema Information
#
# Table name: user_id_verifications
#
#  id                           :uuid             not null, primary key
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#  cohort_id                    :uuid             not null
#  user_id                      :uuid             not null
#  id_verification_period_index :integer          not null
#  verified_at                  :datetime         not null
#  verification_method          :text             not null
#  idology_verification_id      :uuid
#  verifier_id                  :uuid
#  verifier_name                :text
#

class UserIdVerification < ApplicationRecord
    belongs_to :idology_verification, optional: true
    belongs_to :user

    validates_inclusion_of :verification_method, :in => %w(verified_by_admin verified_by_idology waived_due_to_late_enrollment)
    validates_presence_of :idology_verification_id, :if => :verified_by_idology?
    validates_presence_of :verifier_id, :verifier_name, :if => Proc.new{ verification_method == 'verified_by_admin' }

    after_create :log_created_event

    # Create a record for a particular user and attach it to the active
    # verification period on zir relevant cohort.  Supported `attrs` are
    #
    # - verification_method
    # - idology_verification_id
    def self.record_verification!(user, attrs = {})
        cohort = user.relevant_cohort

        base_attrs = {
            user_id: user.id,
            cohort_id: cohort['id'],
            id_verification_period_index: cohort.index_for_active_verification_period
        }

        existing_record = UserIdVerification.where(base_attrs).first

        # I guess it's possible for an idology verification to come in after a user
        # is already verified, so we will ignore those. In the normal case, though,
        # we don't really expect someone to be verified for a period they are
        # alreaby verified for, so let us know.
        if existing_record && attrs[:verification_method] != 'verified_by_idology'
            Raven.capture_exception("Trying to verify a user for a period, but they are already verified.", {
                extra: base_attrs.merge(attrs)
            })
        end

        if existing_record.nil?
            UserIdVerification.create!(base_attrs.merge({
                verified_at: Time.now,
                verification_method: attrs[:verification_method],
                idology_verification_id: attrs[:idology_verification_id],
                verifier_id: attrs[:verifier_id],
                verifier_name: attrs[:verifier_name]
            }))
        end
    end

    def log_created_event
        Event.create_server_event!(
            SecureRandom.uuid,
            user_id,
            'user_id_verification:created',
            {
                cohort_id: cohort_id,
                id_verification_period_index: id_verification_period_index,
                verification_method: verification_method
            }
        ).log_to_external_systems
    end

    def verified_by_idology?
        verification_method == 'verified_by_idology'
    end

    def as_json(options = {})
        {
            "id" => id,
            "updated_at" => updated_at.to_timestamp,
            "created_at" => created_at.to_timestamp,
            "cohort_id" => cohort_id,
            "user_id" => user_id,
            "id_verification_period_index" => id_verification_period_index,
            "verified_at" => verified_at.to_timestamp,
            "verification_method" => verification_method,
            "idology_verification_id" => idology_verification_id,
        }

    end
end
