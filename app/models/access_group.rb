# == Schema Information
#
# Table name: access_groups
#
#  id         :uuid             not null, primary key
#  name       :text             not null
#  created_at :datetime
#  updated_at :datetime
#


class AccessGroup < ApplicationRecord

    default_scope -> { order(:created_at) }

    scopify

    INDESTRUCTIBLE_ACCESS_GROUPS = Set.new([
        'OPEN COURSES',
        'EXTRA COURSES'
    ])

    validates_uniqueness_of :name

    before_destroy :ensure_can_be_destroyed
    around_destroy :update_derived_content_tables_on_destroy

    def self.create_from_hash!(hash)
        group = create!(name: hash['name'])

        if hash.key?('stream_locale_pack_ids')
            # NOTE: since you can only add the access group to streams here, and not
            # to cohorts or anything else, it is not possible that the result of
            # creating an access group is that a stream gets attached to something that
            # it was not attached to before.  For that reason, we do not need
            # to call make_derived_content_table_updates.  But, if we started allowing, for
            # example, updating the list of cohorts here as well, then we would need to do that.
            group.lesson_stream_locale_packs = Lesson::Stream::LocalePack.find(hash['stream_locale_pack_ids'])
        end

        group
    end

    def update_from_hash!(hash)
        self.update!(name: hash['name'])

        if hash.key?('stream_locale_pack_ids')
            orig_stream_locale_pack_ids = self.lesson_stream_locale_packs.map(&:id)
            self.lesson_stream_locale_packs = Lesson::Stream::LocalePack.find(hash['stream_locale_pack_ids'])
            new_stream_locale_pack_ids = self.lesson_stream_locale_packs.map(&:id)

            if orig_stream_locale_pack_ids.sort != new_stream_locale_pack_ids.sort
                make_derived_content_table_updates(orig_stream_locale_pack_ids)
            end
        end

        self
    end

    def as_json(options = {})

        hash = {
            "id" => id,
            "name" => name,
            "updated_at" => updated_at.to_timestamp,
            "created_at" => created_at.to_timestamp,
            "stream_locale_pack_ids" => lesson_stream_locale_packs.map(&:id) # this is faster than lesson_stream_locale_packs.ids, assuming we have done eager-loading with includes
        }

        hash

    end

    def ensure_can_be_destroyed
        if AccessGroup::INDESTRUCTIBLE_ACCESS_GROUPS.include?(self.name)
            errors.add(:"#{self.name}", "is indestructible. Refer to engineering for assistance.")
            raise ActiveRecord::RecordInvalid.new(self)
        end
    end

    def update_derived_content_tables_on_destroy(&block)

        orig_stream_locale_pack_ids = self.lesson_stream_locale_packs.map(&:id)
        yield
        make_derived_content_table_updates(orig_stream_locale_pack_ids)

    end

    def make_derived_content_table_updates(orig_stream_locale_pack_ids)
        RefreshMaterializedContentViews.refresh

        # Eventually, we may need to support other content items in
        # here.  For now, though, just checking streams is enough to
        # make sure that PublishedCohortStreamLocalePack gets
        # updated
        a = orig_stream_locale_pack_ids
        b = self.lesson_stream_locale_packs.reload.map(&:id)
        locale_pack_ids = a - b | b - a

        streams = Lesson::Stream.all_published.where(locale_pack_id: locale_pack_ids).includes(:published_versions)

        triggers = streams.map do |stream|
            stream.get_derived_content_table_update_triggers(

                # Since nothing is changing with the actual content
                # item, we use the current version as both the old and
                # the new.  Any rules that are looking for changes to
                # the content item will find none.
                # (See similar comment in Publishable#after_access_group_update)
                old_version: stream.published_version,
                new_version: stream.published_version,
                access_groups_did_change: true)
        end.flatten

        Lesson::Stream.make_derived_content_table_updates(triggers)
    end

end
