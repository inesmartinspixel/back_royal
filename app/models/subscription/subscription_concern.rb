require 'back_royal/string'

module Subscription::SubscriptionConcern

    extend ActiveSupport::Concern

    included do
        include OwnsPayments

        # We skip trying to ensure in Stripe from tests everywhere except for the
        # specific describe that tests the method
        attr_accessor :force_ensure_stripe_customer_current_in_test_mode

        after_destroy :delete_stripe_customer_if_stripe_on
    end

    # We check for duplicate subscriptions when creating a subscription in the app, but those checks
    # would not trigger if a subscription was created in the stripe dashboard and then reconciled here,
    # so we have this extra check as well.
    def after_subscription_added(subscription)
        if self.potential_primary_subscriptions.count > 1
            Raven.capture_exception(RuntimeError.new("Owner #{self.class.name}:#{self.id} has multiple primary subscriptions"))
        end
    end

    def create_stripe_customer
        self.id ||= SecureRandom.uuid
        begin
            @stripe_customer = Stripe::Customer.create(
                :id => self.id,
                :email => self.email
            )
        rescue Stripe::InvalidRequestError => err
            raise err unless err.message == "Customer already exists."
        end
        self.stripe_customer
    end

    def delete_stripe_customer
        # this means that we make a call to stripe every time we destroy
        # a user, but we hardly ever do that, so it should be fine.
        stripe_customer.delete if stripe_customer
    rescue RuntimeError => err
        raise err unless err.message == "Stripe customer #{self.id} has been deleted!"
    end

    def ensure_stripe_customer_current
        return if Rails.env.test? && !force_ensure_stripe_customer_current_in_test_mode

        # we don't want to unnecessarily hit stripe to find out if the user has a stripe
        # customer, so only do it if there is a subscription in our db
        if subscriptions.reload.any? && stripe_customer && stripe_customer.email != self.email
            stripe_customer.email = self.email
            stripe_customer.save
        end
    end

    def delete_stripe_customer_if_stripe_on
        return if Stripe.api_key.nil?
        delete_stripe_customer
    end


    def get_all_charges
        subscription_invoices = Stripe::Invoice.list(:customer => self.id, :expand => ['data.charge'])

        return unless subscription_invoices

        subscription_invoices.data.select(&:paid).map(&:charge).compact
    end

    # Determine number of payments toward the plan via Stripe's API
    # NOTE: The invoices do not reflect their refunded status and must be accessed through the corresponding charge object.
    def get_payment_details
        all_charges = get_all_charges

        return unless all_charges

        num_charged_payments = all_charges.length
        num_refunded_payments = num_charged_payments > 0 ? all_charges.select(&:refunded).size : 0 # refunded = refunded in full. no partials.

        return num_charged_payments, num_refunded_payments
    end

    def ensure_stripe_customer
        stripe_customer || create_stripe_customer
    end

    def stripe_customer
        if !defined? @stripe_customer
            self.id ||= SecureRandom.uuid
            @stripe_customer = begin
                Stripe::Customer.retrieve(self.id)
            rescue Stripe::InvalidRequestError => err
                raise err unless err.message.match(/No such customer/)
            end

            if @stripe_customer && @stripe_customer.respond_to?(:deleted) && @stripe_customer.deleted
                raise "Stripe customer #{self.id} has been deleted!"
            end
        end
        @stripe_customer
    end

    def reset_stripe_customer
        remove_instance_variable :@stripe_customer
        rescue NameError
    end

    def stripe_customer=(customer)
        raise "Unexpected customer #{customer.id} != #{id}" unless customer.id == self.id
        @stripe_customer = customer
    end

    def default_stripe_card
        if stripe_customer && stripe_customer.default_source
            card_for_id(stripe_customer.default_source)
        end
    end

    def card_for_id(id)
        return nil unless stripe_customer
        source = stripe_customer.sources.data.detect { |s| s['id'] == id }
        if source && source['object'] == 'card'
            return source
        end
    end

    # Question: Why are we setting the idempotency_key?
    # Answer: If a network error between front royal and back royal caused
    # a request with a source token to get retried, then Stripe will, by default,
    # give us an InvalidRequest error because tokens cannot be reused.  But if the
    # card is already in Stripe then we're happy to accept whatever response the
    # first request got.
    def update_stripe_card(token_id)
        ensure_stripe_customer
        source = stripe_customer.sources.create({source: token_id}, {idempotency_key: token_id})
        source.save
        stripe_customer.default_source = source.id
        stripe_customer.save
        source
    end

    def last_charged_ts
        last_charge = get_all_charges&.sort_by(&:created).reject(&:refunded).last
        last_charge && last_charge[:created]
    end

    # overriden in User::SubscriptionConcern
    def after_subscription_reconciled_with_stripe(subscription)
    end

    # overriden in HiringTeam::SubscriptionConcern
    def after_subscription_create(subscription)
    end

    # overriden in User::SubscriptionConcern
    def after_subscription_commit(subscription)
    end

    # overriden in User::SubscriptionConcern
    def before_subscription_destroy(subscription)
    end

    # overriden in User::SubscriptionConcern
    def handle_past_due_changes(subscription)
    end

    # overriden in User::SubscriptionConcern
    def all_payments_complete?(subscription)
        false
    end

    # overriden in User::SubscriptionConcern
    def record_refund(stripe_plan_id)
    end

    # overridden in both
    def potential_primary_subscriptions
        raise NotImplementedError.new("SubscriptionConcerns must define primary_subscription")
    end

    def primary_subscription
        subs = potential_primary_subscriptions
        # we only ever expect 1 subscription here, but there are other things
        # that will error if this is not the case, so just returning the most
        # recent one
        subs.sort_by(&:created_at).last
    end


    def raise_if_duplicate_primary_subscription!

        if stripe_customer.subscriptions.data.reject { |sub| sub.status == 'incomplete' }.size > 0
            raise(Subscription::CannotCreateDuplicateSubscription.new("There is already a subscription in stripe for #{self.class.name}=#{self.id}"))
        end

        if self.subscriptions.count > 0
            raise(Subscription::CannotCreateDuplicateSubscription.new("There is already a subscription in the database for #{self.class.name}=#{self.id}"))
        end

    end

end
