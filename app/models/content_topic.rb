# == Schema Information
#
# Table name: content_topics
#
#  id         :uuid             not null, primary key
#  created_at :datetime
#  updated_at :datetime
#  locales    :json
#

class ContentTopic < ApplicationRecord

    has_and_belongs_to_many :lesson_stream_locale_packs, :class_name => 'Lesson::Stream::LocalePack', :association_foreign_key => :locale_pack_id
    has_many :en_lesson_streams, -> { where(locale: 'en') }, :through => :lesson_stream_locale_packs, :source => :content_items
    before_destroy :ensure_not_used?

    def self.create_from_hash!(hash)
        create!(locales: hash['locales'])
    end

    def update_from_hash!(hash)
        self.update!(locales: hash['locales'])
        self
    end

    # called when the selectize on the edit stream page is
    # updates with just a string, so we have to copy it over
    # to English
    def self.update_topics(record, topic_hashes)
        topic_hashes = topic_hashes || []
        existing_topics = ContentTopic.where(id: topic_hashes.map { |h| h['id'] } )
        existing_topic_ids = existing_topics.map(&:id)

        # new topics should come in with client-generated ids in them
        record.locale_pack.content_topics = topic_hashes.map do |hash|
            id = hash['id']
            if existing_topic = existing_topics.detect { |topic| topic.id == id }
                existing_topic
            else
                ContentTopic.create!({
                    id: hash['id'],
                    locales: {
                        en: hash['locales']['en']
                    }
                })
            end
        end
    end

    def as_json(options = {})
        attrs = self.attributes.slice('id', 'locales').as_json

        if self.en_lesson_streams.loaded?
            attrs['stream_titles'] = self.en_lesson_streams.map(&:title)
        end

        attrs
    end

    def ensure_not_used?
        if self.lesson_stream_locale_packs.count > 0

            problematic = []
            self.lesson_stream_locale_packs.each do |locale_pack|
                locale_pack.content_items.each do |content_item|
                    if content_item.locale == 'en'
                        problematic.push(content_item.title)
                    end
                end
            end

            errors.add(:base, "Used in: #{problematic.join(' -- ')}")
        end

        unless errors.empty?
            raise ActiveRecord::RecordInvalid.new(self)
        end

        return errors.blank? # return false to not destroy the element, true to delete.
    end
end
