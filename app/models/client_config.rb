class ClientConfig

    # All this commented out stuff seems good, and there are
    # tests for it, but we're not using it yet, so I just commented
    # it out until we need it
    # class Configurer

    #     attr_accessor :min_version_number
    #     attr_reader :client_config_class

    #     def initialize(client_config_class)
    #         @client_config_class = client_config_class
    #     end

    #     def versions_above(version_number)
    #         self.min_version_number = version_number + 1
    #         self
    #     end

    #     def support(feature_id_or_class)
    #         feature_id = self.client_config_class.to_feature_id(feature_id_or_class)
    #         self.client_config_class.feature_config[feature_id][:min_version_number] = self.min_version_number
    #     end

    # end

    # class << self

    #     def feature_config
    #         unless defined? @feature_config
    #             @feature_config = Hash.new do |hash, feature_id|
    #                 hash[feature_id] = {
    #                     :min_version_number => nil
    #                 }
    #             end
    #         end
    #         @feature_config
    #     end

    #     def versions_above(version_number)
    #         Configurer.new(self).versions_above(version_number)
    #     end

    #     def to_feature_id(feature_id_or_class)
    #         if feature_id_or_class.is_a?(Class)
    #             feature_id = "class_#{feature_id_or_class.name}"
    #         else
    #             feature_id = feature_id_or_class
    #         end
    #         feature_id
    #     end

    # end

    # Component = Lesson::Content::FrameList::Frame::Componentized::Component

    # def self.inherited(subclass)

    #     [
    #         Component::Answer,
    #         Component::Answer::SelectableAnswer,
    #         Component::Answer::SelectableAnswerNavigator,
    #         Component::AnswerList,
    #         Component::AnswerMatcher,
    #         Component::AnswerMatcher::MatchesExpectedText,
    #         Component::AnswerMatcher::SimilarToSelectableAnswer,
    #         Component::Challenge,
    #         Component::Challenge::MultipleChoiceChallenge,
    #         Component::Challenge::UserInputChallenge,
    #         Component::ChallengeOverlayBlank,
    #         Component::ChallengeValidator,
    #         Component::Challenges,
    #         Component::ComponentList,
    #         Component::ComponentOverlay,
    #         Component::ContinueButton,
    #         Component::ContinueButton::AlwaysReadyContinueButton,
    #         Component::ContinueButton::ChallengesContinueButton,
    #         Component::FrameNavigator,
    #         Component::Image,
    #         Component::InteractiveCards,
    #         Component::Layout,
    #         Component::Layout::TextImageInteractive,
    #         Component::MatchingBoard,
    #         Component::MatchingChallengeButton,
    #         Component::MultipleChoiceMessage,
    #         Component::SpinningPrompt,
    #         Component::Text,
    #         Component::TilePrompt,
    #         Component::TilePromptBoard
    #     ].each do |klass|
    #         subclass.versions_above(0).support(klass)
    #     end
    # end



    attr_reader :version_number

    def initialize(version_number)
        @version_number = version_number
    end

    def min_allowed_version
        self.class::MIN_ALLOWED_VERSION
    end

    def min_suggested_version
        self.class::MIN_SUGGESTED_VERSION
    end

    def app_store_name
        self.class::APP_STORE_NAME
    end

    def app_store_name_miyamiya
        self.class::APP_STORE_NAME_MIYAMIYA
    end

    def deprecated?
        @version_number < min_allowed_version
    end

    # See message above about commented out stuff
    # def supports?(feature_id_or_class)
    #     feature_id = self.class.to_feature_id(feature_id_or_class)
    #     entry = self.class.feature_config[feature_id]
    #     if entry[:min_version_number].nil?
    #         raise UnknownFeatureException.new("Do not recognize feature_id #{feature_id.inspect}")
    #     end

    #     entry[:min_version_number] <= self.version_number
    # end

    # class UnknownFeatureException < RuntimeError

    # end

end
