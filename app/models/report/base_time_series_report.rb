class Report::BaseTimeSeriesReport < Report
    def time_series_data(locale)
        unless @time_series_data
            query = @config['klass']
            join_identifiers = Set.new

            query, join_identifiers = filter(query, join_identifiers)
            query = filter_by_time(query)
            query, join_identifiers, blank_values = group(query, join_identifiers)
            query = join(query, join_identifiers, locale)
            time_series_data = aggregate_and_count(query)
            @time_series_data = handle_json_keys(time_series_data, blank_values)
        end
        @time_series_data
    end

    def group(query, join_identifiers)
        blank_values = []
        unless @config['group_bys'].blank?
            group_by_columns = @config['group_bys'].map do |group_by|
                group_by_config = self.class.group_by_configs[group_by]
                raise RuntimeError.new("Unsupported group_by #{group_by.inspect}") unless group_by_config
                blank_values << group_by_config['blank_value']
                join_identifiers += (group_by_config['joins'] || [])
                group_by_config['column']
            end
            query = query.group(*group_by_columns)
        end

        [query, join_identifiers, blank_values]
    end

    def aggregate_and_count(query)
        aggregated_time_column = Arel.sql("(extract(epoch from date_trunc('#{@config['date_zoom_id']}', #{@config['time_column']})))::int")

        query = query
                .group(aggregated_time_column)
                .order(aggregated_time_column)

        if @config['count_column']
            return count_column(query)
        elsif @config['sum_column']
            return sum_column(query)
        else
            raise RuntimeError("Either count_column or sum_column must be defined. Got #{config.inspect}")
        end
    end

    # broken out because we'll likely want to override this in specific reports
    def count_column(query)
        return query.distinct.count(@config['count_column'])
    end

    # broken out because we'll likely want to override this in specific reports
    def sum_column(query)
        return query.sum(@config['sum_column'])
    end

    def handle_json_keys(time_series_data, blank_values)
        # ruby sets the keys to be arrays.  json will
        # not accept that.  Normally, to_json handles this
        # ok by calling to_s on the arrays, but if there is
        # a nil value for one of the groups that the string is
        # '[nil, 123456]'.  We need '[null, 123456]', so we
        # have to to_json all the keys.
        # this also affords us the opportunity to replace null
        # values with human readable strings from the config above
        with_json_keys = {}
        time_series_data.each do |key, value|
            # use the blank values from the group_bys to set human-readable null values
            if key && key.respond_to?(:length) && key.length > 0 && blank_values.length > 0
                key.each_with_index do |k, index|
                    if k.nil?
                        key[index] = blank_values[index] || I18n.t(:filter_empty_unknown)
                    end
                end
            end
            with_json_keys[key.to_json] = value
        end

        with_json_keys
    end

    def as_json(options)
        return {
            "time_series_data" => time_series_data(options[:locale])
        }
    end
end