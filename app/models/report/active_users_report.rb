class Report::ActiveUsersReport < Report::BaseTimeSeriesReport

    REPORT_CONFIG = {
        'klass' => Report::LessonActivityByCalendarDateRecord,
        'time_column' => 'date',            
        'count_column' => 'user_id'
    }

end