class Report::TimeOnTaskReport < Report::BaseTimeSeriesReport

    REPORT_CONFIG = {
        'klass' => Report::LessonActivityByCalendarDateRecord,
        'time_column' => 'date',            
        'sum_column' => 'total_lesson_seconds'
    }
    
    # convert to minutes
    def sum_column(query)
        return query.sum("#{@config['sum_column']} / 60")
    end
end