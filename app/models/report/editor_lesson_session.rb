# == Schema Information
#
# Table name: editor_lesson_sessions
#
#  id                 :uuid             not null, primary key
#  user_id            :uuid             not null
#  lesson_id          :uuid             not null
#  started_at         :datetime         not null
#  last_activity_time :datetime         not null
#  total_time         :float            not null
#  save_count         :integer          not null
#

class Report::EditorLessonSession < ApplicationRecord

    self.table_name = 'editor_lesson_sessions'

    belongs_to :user

    # see comment above write_new_records in user_lesson_progress_record.rb.  Most
    # of that all applies here
    def self.write_new_records
        DbLinker.setup_red_royal_dblink

        columns = %w(
            id
            user_id
            lesson_id
            started_at
            last_activity_time
            total_time
            save_count
        )

        set_clause = (columns - ['id', 'user_id', 'lesson_id']).map { |col|
            "#{col} = EXCLUDED.#{col}"
        }.join(',')

        sql = select_new_records_sql
        return if sql.nil? # this will be nil if there are no editors in the db

        ActiveRecord::Base.connection.execute %Q~

            insert into editor_lesson_sessions (
                #{columns.join(',')}
            )
                (#{select_new_records_sql})
            on conflict (id) do update
                set #{set_clause}
        ~
    end

    def self.select_new_records_sql

        # 2017/12/18 is the minimum because it is after we bumped the min version such that this
        # work was out on all clients: https://bitbucket.org/pedago-ondemand/back_royal/pull-requests/3776
        already_updated_to = Report::EditorLessonSession.maximum(:last_activity_time) || Time.parse('2017/12/18')
        already_updated_to_str = already_updated_to.utc.strftime('%Y-%m-%d %H:%M:%S.%N')

        # This should really be unnecessary, but because of login_as, it is possible to get
        # a lesson:save or lesson:editor:ping event for a non-editor.  These are confusing and
        # they lead to duplicate key errors since multiple users exist for the same session.
        editor_ids = User.joins(:roles).where(roles: {name: ['editor', 'admin', 'lesson_editor']}).distinct.pluck('id')
        editor_id_str = editor_ids.map { |id| "'#{id}'" }.join(',')

        return nil if editor_ids.empty?

        %Q~
            select
                *
            from
                dblink('red_royal',
                    $REDSHIFT$
                        -- First we just determine which sessions need to be updated
                        with recent_sessions AS (
                            select distinct
                                lesson_editor_session_id
                                , user_id
                            from events
                            where events.event_type in ('lesson:editor:ping')
                                and events.created_at > '#{already_updated_to_str}'
                                and user_id in (#{editor_id_str})
                        )

                        -- For each of those sessions, we recalculate all of the data for
                        -- the session
                        ,  sessions AS (
                            select
                                events.lesson_editor_session_id as id
                                , events.user_id
                                , events.lesson_id
                                , min(estimated_time) as started_at
                                , max(estimated_time) as last_activity_time
                                , extract('epoch' from max(estimated_time)) - extract('epoch' from min(estimated_time)) as total_time
                            from
                            events
                                join recent_sessions
                                    on recent_sessions.lesson_editor_session_id = events.lesson_editor_session_id
                                    and recent_sessions.user_id = events.user_id
                            where events.event_type in ('lesson:editor:ping')
                            group by
                                events.lesson_editor_session_id
                                , events.user_id
                                , events.lesson_id
                        )
                        , saves AS (
                            select
                                events.id
                                , events.user_id
                                , events.lesson_id
                                , events.estimated_time
                                , sessions.id as session_id
                            from
                            events
                                join sessions

                                    -- ideally, we should also ensure that the lesson:save event
                                    -- has one of the page_load_ids from the session.  Otherwise, if
                                    -- a user has two sessions going on at once in different browsers,
                                    -- each save will be counted twice.  Seems like an edge case though
                                    -- and not a big deal, especially since you can't really be saving
                                    -- in two browsers at once without getting conflict errors, so
                                    -- probably not worth worrying about.   We need to look for saves
                                    -- a bit after the last ping in case we didn't get another ping after the
                                    -- last save
                                    on sessions.user_id = events.user_id
                                    and sessions.lesson_id = events.lesson_id
                                    and events.estimated_time between sessions.started_at and sessions.last_activity_time + interval '2 minutes'
                            where events.event_type in ('lesson:save')
                        )
                        , with_save_counts AS (
                            SELECT
                                sessions.id,
                                -- It only happened once, and has since been fixed, but it was possible
                                -- for two users to be assigned the same
                                -- session id for the same lesson (when switching users on a single
                                -- browser).  In that case, we just assign all the activity to one
                                -- of the users.  See https://trello.com/c/aqZZZO3k and https://bitbucket.org/pedago-ondemand/back_royal/pull-requests/4398
                                min(sessions.user_id) user_id,
                                min(sessions.lesson_id) lesson_id,
                                min(sessions.started_at) started_at,
                                max(sessions.last_activity_time) last_activity_time,
                                extract('epoch' FROM max(sessions.last_activity_time)) - extract('epoch' FROM min(sessions.started_at)) AS total_time,
                                count(saves.id) save_count
                            FROM
                                sessions
                                LEFT JOIN saves
                                    ON saves.session_id = sessions.id
                            GROUP BY
                                sessions.id
                        )
                        select * from with_save_counts

                    $REDSHIFT$) AS user_lesson_event_aggregates (
                        id uuid
                        , user_id uuid
                        , lesson_id uuid
                        , started_at timestamp
                        , last_activity_time timestamp
                        , total_time float
                        , save_count int
                    )
        ~
    end

end
