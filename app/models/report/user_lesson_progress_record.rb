# == Schema Information
#
# Table name: user_lesson_progress_records
#
#  user_id                         :uuid             not null
#  locale_pack_id                  :uuid             not null
#  started_at                      :datetime         not null
#  completed_at                    :datetime
#  best_score_from_lesson_progress :float
#  total_lesson_time               :float
#  last_lesson_activity_time       :datetime         not null
#  total_lesson_time_on_desktop    :float
#  total_lesson_time_on_mobile_app :float
#  total_lesson_time_on_mobile_web :float
#  total_lesson_time_on_unknown    :float
#  completed_on_client_type        :text
#  last_lesson_reset_at            :datetime
#  lesson_reset_count              :integer
#  lesson_finish_count             :integer
#  average_assessment_score_first  :float
#  average_assessment_score_best   :float
#  official_test_score             :float
#

class Report::UserLessonProgressRecord < ApplicationRecord


    self.table_name = 'user_lesson_progress_records'
    belongs_to :user

    # Since this is a more expensive query than many of our other materialized views, we could not afford to
    # do it on all the data every 10 minutes.  Instead, we look for any user/lesson combo that has activity
    # since our last update and we update the row for that combo.
    #
    # Note that this only works if everything that can change a record also logs one of the events used
    # in the query.  For example, if we tried to
    # denormalize lesson titles in this view, this whole approach would fail because changes
    # to lesson titles would never be picked up.  Also, if we ever change lesson progress records
    # directly without any associated events, the changes would not get picked up here.
    #
    # NOTE: If we make changes to this query and need to update existing records, it should be safe
    # to simply truncate this table.  While this table is empty reports will automatically enter
    # maintenance mode.  The next time that refresh_materialized_progress views calls write_new_records,
    # it will write all records from the beginning of time.
    def self.write_new_records(should_log = nil)
        DbLinker.setup_red_royal_dblink
        should_log = true if should_log.nil? && Rails.env.development?

        interval = 1.month
        start = Time.now
        puts "writing records" if should_log

        already_updated_to = get_initial_already_updated_to
        update_to = already_updated_to + interval
        while already_updated_to < Time.now
            write_records_for_lessons_with_events_in_interval(start: already_updated_to, finish: update_to)

            if should_log
                puts "Processed up to #{update_to} in #{(Time.now - start).round} seconds"
            end

            already_updated_to = update_to
            update_to += interval
        end
    end

    # You can run this from rails console if you want to refresh all of the
    # user/lesson pairs that have events within some interval.  (For example, see https://trello.com/c/DZI9Zphk)
    def self.write_records_for_lessons_with_events_in_interval(start:, finish:)
        columns = %w(
            user_id
            locale_pack_id
            started_at
            completed_at
            best_score_from_lesson_progress
            total_lesson_time
            last_lesson_activity_time
            total_lesson_time_on_desktop
            total_lesson_time_on_mobile_app
            total_lesson_time_on_mobile_web
            total_lesson_time_on_unknown
            completed_on_client_type
            last_lesson_reset_at
            lesson_reset_count
            lesson_finish_count
            average_assessment_score_first
            average_assessment_score_best
            official_test_score
        )

        set_clause = (columns - ['user_id', 'locale_pack_id']).map { |col|
            "#{col} = EXCLUDED.#{col}"
        }.join(',')

        ActiveRecord::Base.connection.execute %Q~

            insert into user_lesson_progress_records (
                #{columns.join(',')}
            )
                (#{select_new_records_sql(start, finish)})
            on conflict (user_id, locale_pack_id) do update
                set #{set_clause}
        ~
    end

    # If we want to re-create all records from the beginning of time (after fixing a bug or something),
    # then disable refresh_materialized_progress job and run this from a rails console.
    def self.rewrite_all_records
        more_records = true

        puts "deleting records"
        while self.first
            result = self.connection.execute(%Q~
                with user_ids AS MATERIALIZED (select user_id from user_lesson_progress_records limit 1000)
                delete from user_lesson_progress_records where user_id in (select user_id from user_ids)
            ~)
        end

        self.write_new_records(true)
    end

    def self.get_initial_already_updated_to
        max_last_lesson_activity_time  = self.get_max_last_lesson_activity_time

        # Normal case.  Start from the max_last_lesson_activity_time
        if max_last_lesson_activity_time.present?
            return max_last_lesson_activity_time

        # If user_lesson_progress_records is empty, start from the first redshift event
        elsif min_event_created_at = RedshiftEvent.where("events.event_type in #{lesson_event_types}").minimum('created_at')
            return min_event_created_at - 1.second # make sure we start from a time before the first redshift event

        # if there are no records and no redshift events, then it doesn't matter anyway
        # since there is no data to process.  So start from now.  This is not realistic,
        # but can happen in specs.
        else
            # This is not realistic.  In the wild, there are some redshift events in the db
            return Time.now
        end
    end

    def self.get_max_last_lesson_activity_time
        Report::UserLessonProgressRecord.maximum(:last_lesson_activity_time)
    end

    def self.lesson_event_types
        "('lesson:frame:unload', 'lesson:start', 'lesson:complete', 'lesson:reset', 'lesson:finish', 'lesson:touched')"
    end

    # This method will rewrite all records for any user/lesson pairs that
    # have an event in the time window.
    #
    # For each pair, the query will go back and look at the full history for the
    # user on this lesson, rewriting the record from scratch (nothing is incremented).
    def self.select_new_records_sql(start, finish)
        start_str = (start || Time.at(0)).utc.strftime('%Y-%m-%d %H:%M:%S.%N')
        finish_str = finish.utc.strftime('%Y-%m-%d %H:%M:%S.%N')

        # the lesson:touched event is logged when we have manually changed something on a lesson.
        # See `rake user:change_lesson_score`
        recent_pairs_query = %Q~
                                select
                                    distinct events.user_id
                                    , events.lesson_id
                                from events
                                where events.event_type in #{lesson_event_types}
                                and events.created_at > '#{start_str}'
                                and events.created_at <= '#{finish_str}'
        ~

        %Q~
            with step_1 AS MATERIALIZED (
                select
                    user_lesson_event_aggregates.user_id
                    , lessons.locale_pack_id

                    -- use the times from lesson_progress to ensure we always have agreement with user's UI. Newer code
                    -- should ensure agreements between events and lesson_progress records, but there are some
                    -- issues in older code
                    , lesson_progress.started_at
                    , lesson_progress.completed_at
                    , lesson_progress.best_score as best_score_from_lesson_progress

                    , sum(case when assessment then 1 else 0 end) > 0 as assessment
                    , sum(case when test then 1 else 0 end) > 0 as test

                    , sum(total_lesson_time) as total_lesson_time
                    , max(last_lesson_activity_time) as last_lesson_activity_time
                    , sum(total_lesson_time_on_desktop) as total_lesson_time_on_desktop
                    , sum(total_lesson_time_on_mobile_app) as total_lesson_time_on_mobile_app
                    , sum(total_lesson_time_on_mobile_web) as total_lesson_time_on_mobile_web
                    , sum(total_lesson_time_on_unknown) as total_lesson_time_on_unknown
                    , min(completed_on_client_type) as completed_on_client_type     -- there should only ever be one anyway, but take the min in the off chance there are more
                    , max(last_lesson_reset_at) as last_lesson_reset_at
                    , sum(lesson_reset_count) as lesson_reset_count
                from
                    dblink('red_royal',
                        $REDSHIFT$
                            with recent_pairs AS (#{recent_pairs_query})
                            ,  lesson_events AS (
                                select
                                    events.user_id
                                    , events.lesson_id

                                    --  https://trello.com/c/RgurwSyg/1875-bug-fix-last-seen-column-showing-dates-in-the-future
                                    -- https://trello.com/c/h1oor5aA/1547-bug-userlessonprogressrecords-missing-data-from-lessonprogress
                                    , case
                                        when events.estimated_time > events.created_at then events.created_at
                                        else events.estimated_time
                                        end as estimated_time
                                    , case when events.duration_total > 120 then 120 else events.duration_total end as duration_total
                                    , events.event_type
                                    , events.score
                                    , case
                                        when page_loads.client in ('ios', 'android')
                                            then 'mobile_app'
                                        when page_loads.os_name in ('Windows Phone', 'iOS', 'blackberry', 'Android', 'Tizen') or page_loads.browser_name='Facebook'
                                            then 'mobile_web'
                                        when page_loads.os_name in ('Windows', 'Mac OS', 'Chromium OS', 'Ubuntu')
                                            then 'desktop'
                                        else
                                            'unknown'
                                        end as client_type
                                from
                                events
                                    left join events page_loads
                                        on events.page_load_id = page_loads.page_load_id
                                            and page_loads.event_type='page_load:load'
                                            and page_loads.user_id = events.user_id -- doesn't change result, but should help query planner given our sort keys
                                    join recent_pairs
                                        on recent_pairs.user_id = events.user_id
                                        and recent_pairs.lesson_id = events.lesson_id
                                where events.event_type in ('lesson:frame:unload', 'lesson:start', 'lesson:complete', 'lesson:reset', 'lesson:finish')
                                    and events.created_at <= '#{finish_str}'
                                order by
                                    event_type
                                    , estimated_time -- ordering by estimated_time allows us to find the first assessment score
                            )
                            select
                                user_id
                                , lesson_id
                                , sum(duration_total) as total_lesson_time
                                , max(estimated_time) as last_lesson_activity_time
                                , sum(case
                                    when client_type='mobile_app' and duration_total is not null
                                        then duration_total
                                    else 0
                                    end) as total_lesson_time_on_mobile_app
                                , sum(case
                                    when client_type='mobile_web' and duration_total is not null
                                        then duration_total
                                    else 0
                                    end) as total_lesson_time_on_mobile_web
                                , sum(case
                                    when client_type='desktop' and duration_total is not null
                                        then duration_total
                                    else 0
                                    end) as total_lesson_time_on_desktop
                                , sum(case
                                    when client_type='unknown' and duration_total is not null
                                        then duration_total
                                    else 0
                                    end) as total_lesson_time_on_unknown
                                , min(case when event_type='lesson:complete' then client_type else null end) as completed_on_client_type
                                , max(case when event_type='lesson:reset' then estimated_time else null end) as last_lesson_reset_at
                                , count(case when event_type='lesson:reset' then true else null end) as lesson_reset_count
                            from lesson_events
                            group by
                                user_id
                                , lesson_id
                        $REDSHIFT$) AS user_lesson_event_aggregates (
                            user_id uuid
                            , lesson_id uuid
                            , total_lesson_time float
                            , last_lesson_activity_time timestamp
                            , total_lesson_time_on_mobile_app float
                            , total_lesson_time_on_mobile_web float
                            , total_lesson_time_on_desktop float
                            , total_lesson_time_on_unknown float
                            , completed_on_client_type text
                            , last_lesson_reset_at timestamp
                            , lesson_reset_count int
                        )
                    join lessons
                        on lessons.id = user_lesson_event_aggregates.lesson_id
                    join lesson_progress
                        on lesson_progress.locale_pack_id = lessons.locale_pack_id
                            and lesson_progress.user_id = user_lesson_event_aggregates.user_id
                group by
                    user_lesson_event_aggregates.user_id
                    , lessons.locale_pack_id
                    , lesson_progress.completed_at
                    , lesson_progress.started_at
                    , lesson_progress.best_score
            )
            , lesson_finishes AS MATERIALIZED (
                select * from
                dblink('red_royal', $REDSHIFT$
                    with recent_pairs AS (#{recent_pairs_query})
                    select
                        events.user_id
                        --  https://trello.com/c/RgurwSyg/1875-bug-fix-last-seen-column-showing-dates-in-the-future
                        -- https://trello.com/c/h1oor5aA/1547-bug-userlessonprogressrecords-missing-data-from-lessonprogress
                        , case
                            when events.estimated_time > events.created_at then events.created_at
                            else events.estimated_time
                            end as estimated_time
                        , events.lesson_id
                        , events.score
                    from
                        events
                        join recent_pairs
                            on recent_pairs.user_id = events.user_id
                            and recent_pairs.lesson_id = events.lesson_id
                    where event_type='lesson:finish'
                        and events.created_at <= '#{finish_str}'
                $REDSHIFT$) AS lesson_finishes (
                            user_id uuid
                            , estimated_time timestamp
                            , lesson_id uuid
                            , score float
                        )
            )
            , lesson_finish_info AS MATERIALIZED (
                select
                    lesson_finishes.user_id
                    , lessons.locale_pack_id
                    , count(*) as lesson_finish_count
                    , case when sum(case when assessment or test then 1 else 0 end) > 0 then
                        case when old_assessment_scores.average_assessment_score_first is not null then
                            old_assessment_scores.average_assessment_score_first
                        else
                            (array_remove(array_agg(lesson_finishes.score order by estimated_time asc), null))[1]
                        end
                      else null
                      end as average_assessment_score_first
                    , old_assessment_scores.average_assessment_score_last best_score_from_old_assessment_scores
                from lesson_finishes
                join published_lessons lessons
                    on lessons.id = lesson_finishes.lesson_id
                left join old_assessment_scores
                        on lessons.locale_pack_id = old_assessment_scores.locale_pack_id
                        and lesson_finishes.user_id = old_assessment_scores.user_id
                group by
                    lesson_finishes.user_id
                    , lessons.locale_pack_id
                    , old_assessment_scores.average_assessment_score_first
                    , old_assessment_scores.average_assessment_score_last
            )
            , step_2 AS MATERIALIZED (
                select
                    step_1.*
                    , lesson_finish_info.lesson_finish_count
                    , lesson_finish_info.average_assessment_score_first
                    , case when assessment or test then
                        case when best_score_from_lesson_progress is null or best_score_from_old_assessment_scores > best_score_from_lesson_progress then
                            best_score_from_old_assessment_scores
                        else
                            best_score_from_lesson_progress
                        end
                      else
                        null
                      end as average_assessment_score_best
                from step_1
                    left join lesson_finish_info
                    on step_1.user_id = lesson_finish_info.user_id
                        and step_1.locale_pack_id = lesson_finish_info.locale_pack_id
            )
            , step_3 AS MATERIALIZED (
                select
                    step_2.user_id
                    , step_2.locale_pack_id
                    , step_2.started_at
                    , step_2.completed_at
                    , step_2.best_score_from_lesson_progress
                    , step_2.total_lesson_time
                    , step_2.last_lesson_activity_time
                    , step_2.total_lesson_time_on_desktop
                    , step_2.total_lesson_time_on_mobile_app
                    , step_2.total_lesson_time_on_mobile_web
                    , step_2.total_lesson_time_on_unknown
                    , step_2.completed_on_client_type
                    , step_2.last_lesson_reset_at
                    , step_2.lesson_reset_count
                    , step_2.lesson_finish_count
                    , step_2.average_assessment_score_first
                    , step_2.average_assessment_score_best

                    -- We alias average_assessment_score_best as official_test_score just to indicate
                    -- that this is the score we should be using when calculating test scores to report
                    -- to users, final scores for determining graduation, etc.
                    -- Probably, we would prefer to use the first test score, rather than the best, as the
                    -- official test score, but there are two reasons we use the best score
                    -- instead.
                    --
                    -- 1. (It's easier right now)  We need the official test score to be available in real-time to users, and
                    --      right now we already have the best_score in lesson_progress.  By
                    --      using best score both in reports and in real-time, we make sure there are
                    --      no edge cases where we show the user one score but then use a different score
                    --      in our reporting or analysis.  Nothing would
                    --      have prevented us from storing the first_score in lesson_progress and then
                    --      using that in both place, we just
                    --      never did so now it would be extra work to go back and add it.

                    -- 2. (It makes no difference) Except where we have other bugs, the first score and the best score should be
                    --      the same for test lessons, since users are not allowed to re-take them.  As of
                    --      2017/05/29 there was one instance in our db where a user had managed to re-take a
                    --      test lesson.  Using best score instead of first score means that, when we mess up
                    --      and let a user re-take a test lesson, we then will give them credit for the better
                    --      score when we would probably prefer to keep the first score. Big deal.
                    , case when test then average_assessment_score_best else null end as official_test_score
                from
                    step_2
            )
            select * from step_3
        ~
    end

    def challenge_scores_last_in_lesson_progress_format
        challenge_scores_in_lesson_progress_format(challenge_scores_last)
    end

    def challenge_scores_first_in_lesson_progress_format
        challenge_scores_in_lesson_progress_format(challenge_scores_first)
    end

    def challenge_scores_in_lesson_progress_format(challenge_scores)
        result = {}
        challenge_scores.each do |key, val|
            new_key = key.gsub('.', ',')
            result[new_key] = val
        end
        result
    end

end
