class Report::BaseTabularReport < Report

    def self.field_configs
        {
            'id' => {
                'column' => "#{self::REPORT_CONFIG['klass'].table_name}.id"
            },
            'user_id' => {
                'column' => "#{self::REPORT_CONFIG['klass'].table_name}.user_id"
            },

            'account_id' => {
                'column' => %q~
                    case
                        when users.provider = 'phone_no_password' then users.phone
                        when users.email is not null then users.email
                        else users.id::text
                    end
                ~,
                'joins' => ['users']
            },
            'user_name' => {
                'column' => %q~
                    (
                        initcap(case when users.name is null then '' else users.name end)
                    )
                ~,
                'joins' => ['users']
            },
            'registered_at' => {
                'column' => 'cast(EXTRACT(EPOCH FROM users.created_at) as int)',
                'joins' => ['users']
            }
        }
    end

    def tabular_data(locale)
        @tabular_data ||= {}
        unless @tabular_data[locale]
            query = @config['klass']
            join_identifiers = Set.new

            query, join_identifiers = filter(query, join_identifiers)
            columns, join_identifiers, group_bys = select(join_identifiers)
            query = query.reorder(nil) # for the users report, remove the default created_at ordering
            query = filter_by_time(query) if (@config['time_column'] && @config['date_range'])
            query = join(query, join_identifiers, locale)
            query = limit(query)
            query = query.group(*group_bys)
            @tabular_data[locale] = query.pluck(*columns.map { |col| Arel.sql(col) })
        end
        @tabular_data[locale]
    end

    def select(join_identifiers)
        columns = []
        group_bys = []

        i = 0
        @config['fields'].each do |key|
            field_config = self.class.field_configs[key]

            raise RuntimeError.new("Unsupported field #{key.inspect}") unless field_config

            columns << "#{field_config['column']} as column_#{i}"
            i += 1
            join_identifiers += field_config['joins'] if field_config['joins']
            group_bys << field_config['column'] unless field_config['aggregate']
        end
        [columns, join_identifiers, group_bys]
    end

    def limit(query)
        if @config['limit']
            query = query.limit(@config['limit'])
        end

        query
    end

    def as_json(options)
        return {
            "tabular_data" => tabular_data(options[:locale])
        }
    end

end