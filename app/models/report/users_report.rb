class Report::UsersReport < Report::BaseTabularReport

    REPORT_CONFIG = {
        'klass' => User,
        'time_column' => 'last_lesson_activity_time',
        'registered_column' => 'users.created_at'
    }

    def self.available_joins
        @available_joins ||= super.merge({

            'user_progress_records' => Proc.new { |query, locale|
                query.joins('LEFT JOIN user_progress_records ON users.id = user_progress_records.user_id')
            },

            # Override Report::available_joins' institutions join since User does not have a user association
            'institutions' => Proc.new { |query, locale|
                query.left_outer_joins(:institutions)
            },

            # Override Report::available_joins' institutions join since User does not have a user association
            'cohorts' => Proc.new { |query, locale|
                query.left_outer_joins(cohort_applications: :cohort)
            }

        })
    end

    def self.filter_configs
        @filter_configs ||= super.merge({
            "CourseStartedFilter" => {
                'type' => 'list',
                'column' => 'user_progress_records.started_stream_locale_pack_ids',
                'array_column' => true,
                'blank_value' => I18n.t(:filter_empty_courses_started)
            },
            "CourseCompletedFilter" => {
                'type' => 'list',
                'column' => 'user_progress_records.completed_stream_locale_pack_ids',
                'array_column' => true,
                'blank_value' => I18n.t(:filter_empty_courses_completed)
            },
            "PlaylistStartedFilter" => {
                'type' => 'list',
                'column' => 'user_progress_records.started_playlist_locale_pack_ids',
                'array_column' => true,
                'blank_value' => I18n.t(:filter_empty_playlists_started)
            },
            "PlaylistCompletedFilter" => {
                'type' => 'list',
                'column' => 'user_progress_records.completed_playlist_locale_pack_ids',
                'array_column' => true,
                'blank_value' => I18n.t(:filter_empty_playlists_completed)
            },
            "ApplicationRegisteredFilter" => {
                'type' => 'bool',
                'field' => 'registered_for_cohort'
            }
        })
    end

    def self.internal_get_filter_options_for_institution(institution, locale)
        lesson_stream_options = get_lesson_stream_options(locale, {
            locale_pack_id: institution.lesson_stream_locale_packs.ids
        })

        playlist_options = get_playlist_options(locale, {
            locale_pack_id: institution.playlist_locale_packs.ids
        })

        super.concat([
            {
                filter_type: 'CourseStartedFilter',
                options: lesson_stream_options + [{value: nil, text: self.filter_configs['CourseStartedFilter']['blank_value']}],
            },

            {
                filter_type: 'CourseCompletedFilter',
                options: lesson_stream_options + [{value: nil, text: self.filter_configs['CourseCompletedFilter']['blank_value']}],
            },

            {
                filter_type: 'PlaylistStartedFilter',
                options: playlist_options + [{value: nil, text: self.filter_configs['PlaylistStartedFilter']['blank_value']}]
            },

            {
                filter_type: 'PlaylistCompletedFilter',
                options: playlist_options + [{value: nil, text: self.filter_configs['PlaylistCompletedFilter']['blank_value']}]
            }
        ])
    end

    def self.internal_get_filter_options(locale)
        lesson_stream_options = get_lesson_stream_options(locale)
        playlist_options = get_playlist_options(locale)

        super.concat([
            {
                filter_type: 'CourseStartedFilter',
                options: lesson_stream_options + [{value: nil, text: self.filter_configs['CourseStartedFilter']['blank_value']}],
            },

            {
                filter_type: 'CourseCompletedFilter',
                options: lesson_stream_options + [{value: nil, text: self.filter_configs['CourseCompletedFilter']['blank_value']}],
            },

            {
                filter_type: 'PlaylistStartedFilter',
                options: playlist_options + [{value: nil, text: self.filter_configs['PlaylistStartedFilter']['blank_value']}]
            },

            {
                filter_type: 'PlaylistCompletedFilter',
                options: playlist_options + [{value: nil, text: self.filter_configs['PlaylistCompletedFilter']['blank_value']}]
            }
        ])
    end

    def self.get_lesson_stream_options(locale, filters = {})
        filters = {
            in_locale_or_en: locale,
            published: true
        }.merge(filters)

        streams = Lesson::Stream::ToJsonFromApiParams.new(
            filters: filters,
            fields: ['id', 'locale_pack', 'title']
        ).to_a

        streams.map { |stream|
            {
                value: stream['locale_pack']['id'],
                text: stream['title']
            }
        }
    end

    def self.get_playlist_options(locale, filters = {})
        filters = {
            in_locale_or_en: locale,
            published: true
        }.merge(filters)

        playlists = Playlist::ToJsonFromApiParams.new(
            filters: filters,
            fields: ['id', 'locale_pack', 'title']
        ).to_a

        playlists.map { |playlist|
            {
                value: playlist['locale_pack']['id'],
                text: playlist['title']
            }
        }
    end

    def self.field_configs

        user_progress_record_columns = {}

        # vanilla columns
        %w(
            average_assessment_score_first average_assessment_score_last
            lesson_finish_count average_assessment_score_best
        ).each do |name|
            user_progress_record_columns[name] = {
                'column' => name,
                'joins' => ['user_progress_records']
            }
        end

        # since there may not be an associated progress record,
        # we need to set default values for the missing columns

        # array columns
        %w(started_lesson_locale_pack_ids completed_lesson_locale_pack_ids
        started_stream_locale_pack_ids completed_stream_locale_pack_ids
        started_playlist_locale_pack_ids completed_playlist_locale_pack_ids
        ).each do |name|
            user_progress_record_columns[name] = {
                'column' => "case when #{name} is null then Array[]::uuid[] else #{name} end",
                'joins' => ['user_progress_records']
            }
        end

        # count columns
        %w(started_lesson_count completed_lesson_count started_stream_count completed_stream_count total_lesson_time
            started_playlist_count completed_playlist_count
        ).each do |name|
            user_progress_record_columns[name] = {
                'column' => "COALESCE(#{name}, 0)::integer",
                'joins' => ['user_progress_records']
            }
        end

        super.merge(user_progress_record_columns).merge({
            'user_id' => {
                # since the base table is users, we need to map it to
                # user_id to work with some of the other shared code
                'column' => 'users.id'
            },
            'registered_for_cohort' => {
                'column' => "(array_remove(array_agg(case when cohort_applications.status in ('pre_accepted', 'accepted') then cohort_applications.registered else null end), null))[1]",
                'aggregate' => true,
                'joins' => ['cohort_applications']
            },
            'last_lesson_activity_time' => {
                'column' => 'cast(EXTRACT(EPOCH FROM last_lesson_activity_time) as int)'
            }
        })
    end

end