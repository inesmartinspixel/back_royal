# == Schema Information
#
# Table name: published_content_titles
#
#  type             :text
#  locale_pack_id   :uuid
#  target_locale    :text
#  published_locale :text
#  published_title  :string
#

class Report::PublishedContentTitle < ApplicationRecord

    self.inheritance_column = nil

    self.table_name = 'published_content_titles'
end
