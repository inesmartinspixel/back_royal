class Report::PlayerLessonSessionsReport < Report::BaseTabularReport

    REPORT_CONFIG = {
        'klass' => Report::PlayerLessonSession,
        'time_column' => 'started_at',
        'registered_column' => 'users.created_at'
    }

    def self.filter_configs
        @filter_configs ||= super.merge({
            "CourseFilter" => {
                'type' => 'list',
                'joins' => ['published_stream_titles'],
                'column' => 'published_stream_titles.locale_pack_id'
            },
            "LessonLocaleFilter" => {
                'type' => 'list',
                'column' => 'lesson_locale'
            },
            "ClientTypeFilter" => {
                'type' => 'list',
                'column' => 'client_type'
            }
            # commenting out for now because it's ugly and brings up odd questions
            # see https://trello.com/c/kQp6zGuR/634-feat-support-lessons-filter-for-internal-users
            # "LessonFilter" => {
            #     'type' => 'list',
            #     'joins' => ['published_lesson_titles'],
            #     'column' => 'published_lesson_titles.locale_pack_id'
            # }
        })
    end

    def self.internal_get_filter_options_for_institution(institution, locale)
        super.concat(get_content_filter_options(locale, institution))
    end

    def self.internal_get_filter_options(locale)
        super.concat(get_content_filter_options(locale)).concat([
            {
                filter_type: 'LessonLocaleFilter',
                options: I18n.available_locales.map do |available_locale|
                    {
                        text: I18n.t(available_locale, :locale => locale),
                        value: available_locale.to_s
                    }
                end.sort_by { |e| e[:value]}
            },
            {
                filter_type: 'ClientTypeFilter',
                options: ['all', 'mobile_app', 'desktop', 'mobile_web', 'unknown'].map do |option|
                    {
                        text: I18n.t(option, :locale => locale),
                        value: option
                    }
                end.sort_by { |e| e[:value]}
            }
        ])
    end

    def self.get_content_filter_options(locale, institution = nil)
        [
            {
                filter_type: 'CourseFilter',
                options: get_course_title_options(locale, institution).map do |row|
                    {
                        text: row.published_title,
                        value: row.locale_pack_id
                    }
                end
            },

            # commenting out for now because it's ugly and brings up odd questions
            # see https://trello.com/c/kQp6zGuR/634-feat-support-lessons-filter-for-internal-users
            # {
            #     filter_type: 'LessonFilter',
            #     options: Report::PublishedContentTitle.where({
            #         target_locale: locale,
            #         type: 'Lesson'
            #     }).map do |row|
            #         {
            #             text: row.published_title,
            #             value: row.locale_pack_id
            #         }
            #     end
            # }
        ]
    end

    def self.get_course_title_options(locale, institution)
        query = Report::PublishedContentTitle.where({
            target_locale: locale,
            type: 'Lesson::Stream'
        })

        if (institution)
            query = query.where(locale_pack_id: institution.lesson_stream_locale_packs.ids)
        end

        query
    end

    # def self.get_lesson_stream_options(locale, filters = {})
    #     filters = {
    #         in_locale_or_en: locale,
    #         published: true
    #     }.merge(filters)

    #     streams = Lesson::Stream::ToJsonFromApiParams.new(
    #         filters: filters,
    #         fields: ['id', 'locale_pack', 'title']
    #     ).to_a

    #     streams.map { |stream|
    #         {
    #             value: stream['locale_pack']['id'],
    #             text: stream['title']
    #         }
    #     }
    # end

    def self.field_configs

        vanilla_columns = {}
        %w(
            total_lesson_time started_in_this_session completed_in_this_session
            lesson_id lesson_stream_id lesson_locale client_type
        ).each do |name|
            vanilla_columns[name] = {'column' => name}
        end

        super.merge(vanilla_columns).merge({
            'last_lesson_activity_time' => {
                'column' => 'cast(EXTRACT(EPOCH FROM last_lesson_activity_time) as int)',
            },
            'started_at' => {
                'column' => 'cast(EXTRACT(EPOCH FROM started_at) as int)',
            },
            'lesson_title' => {
                'type' => 'text',
                'column' => 'published_lesson_titles.published_title',
                'joins' => ['published_lesson_titles']
            },
            'lesson_stream_title' => {
                'type' => 'text',
                'column' => 'published_stream_titles.published_title',
                'joins' => ['published_stream_titles']
            }
        })
    end

end