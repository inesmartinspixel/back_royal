# == Schema Information
#
# Table name: lesson_activity_by_calendar_date_records
#
#  user_id              :uuid
#  date                 :datetime
#  total_lesson_seconds :float
#

class Report::LessonActivityByCalendarDateRecord < ApplicationRecord

    self.table_name = 'lesson_activity_by_calendar_date_records'
    belongs_to :user


end
