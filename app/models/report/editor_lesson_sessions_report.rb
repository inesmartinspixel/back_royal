class Report::EditorLessonSessionsReport < Report::BaseTabularReport

    REPORT_CONFIG = {
        'klass' => Report::EditorLessonSession,
        'time_column' => 'started_at'
    }

    def self.filter_configs
        @filter_configs ||= super.merge({
            "LessonLocaleFilter" => {
                'type' => 'list',
                'column' => 'internal_lesson_titles.locale',
                'joins' => ['internal_lesson_titles']
            },
            "LessonFilter" => {
                'type' => 'list',
                'column' => 'lesson_id'
            }
        })
    end

    def self.internal_get_filter_options(locale)
        super.concat([
            {
                filter_type: 'LessonLocaleFilter',
                options: I18n.available_locales.map do |available_locale|
                    {
                        text: I18n.t(available_locale, :locale => locale),
                        value: available_locale.to_s
                    }
                end.sort_by { |e| e[:value]}
            },
            {
                filter_type: 'LessonFilter',
                options: Report::InternalContentTitle.where(type: 'Lesson').map do |record|
                    {
                        text: record.internal_title + (record.locale == 'en' ? '' : ' ('+record.locale+')'),
                        value: record.id
                    }
                end
            }

        ])
    end

    def self.field_configs

        vanilla_columns = {}
        %w(
            total_time save_count
            lesson_id lesson_stream_id
        ).each do |name|
            vanilla_columns[name] = {'column' => name}
        end

        super.merge(vanilla_columns).merge({
            'last_activity_time' => {
                'column' => 'cast(EXTRACT(EPOCH FROM last_activity_time) as int)',
            },
            'started_at' => {
                'column' => 'cast(EXTRACT(EPOCH FROM started_at) as int)',
            },
            'lesson_title' => {
                'type' => 'text',
                'column' => 'internal_lesson_titles.internal_title',
                'joins' => ['internal_lesson_titles']
            },
            'lesson_locale' => {
                'type' => 'text',
                'column' => 'internal_lesson_titles.locale',
                'joins' => ['internal_lesson_titles']
            }
        })
    end

end