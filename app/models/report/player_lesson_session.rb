# == Schema Information
#
# Table name: player_lesson_sessions
#
#  id                           :uuid             not null, primary key
#  session_id                   :uuid             not null
#  user_id                      :uuid             not null
#  lesson_id                    :uuid             not null
#  lesson_stream_id             :uuid
#  started_at                   :datetime         not null
#  last_lesson_activity_time    :datetime         not null
#  total_lesson_time            :float            not null
#  started_in_this_session      :boolean          not null
#  completed_in_this_session    :boolean          not null
#  lesson_locale_pack_id        :uuid             not null
#  lesson_stream_locale_pack_id :uuid
#  lesson_locale                :text             not null
#  client_type                  :text             not null
#

class Report::PlayerLessonSession < ApplicationRecord


    self.table_name = 'player_lesson_sessions'

    belongs_to :user
    # see comment above write_new_records in user_lesson_progress_record.rb.  Most
    # of that all applies here
    def self.write_new_records
        DbLinker.setup_red_royal_dblink

        columns = %w(
            session_id
            user_id
            lesson_id
            lesson_stream_id
            started_at
            last_lesson_activity_time
            total_lesson_time
            started_in_this_session
            completed_in_this_session
            lesson_locale_pack_id
            lesson_stream_locale_pack_id
            lesson_locale
            client_type
        )


        # it would be nice if we were unique just on session_id, but see https://trello.com/c/eYxDiyUB
        ActiveRecord::Base.connection.execute %Q~

            insert into player_lesson_sessions (
                #{columns.join(',')}
            )
                (#{select_new_records_sql})
            on conflict (user_id, lesson_id, lesson_stream_id, session_id) do update set
                last_lesson_activity_time = EXCLUDED.last_lesson_activity_time,
                total_lesson_time = extract('epoch' from EXCLUDED.last_lesson_activity_time) - extract('epoch' from player_lesson_sessions.started_at),
                started_in_this_session = player_lesson_sessions.started_in_this_session OR EXCLUDED.started_in_this_session,
                completed_in_this_session = player_lesson_sessions.completed_in_this_session OR EXCLUDED.completed_in_this_session
        ~
    end

    def self.select_new_records_sql

        # after 2017-07-14 all lesson:frame:unload events have session ids
        already_updated_to = Report::PlayerLessonSession.maximum(:last_lesson_activity_time) || Time.parse('2017-07-14')
        already_updated_to_str = already_updated_to.utc.strftime('%Y-%m-%d %H:%M:%S.%N')

        %Q~
            with recent_sessions AS MATERIALIZED (
                SELECT *
                FROM
                    dblink('red_royal',

                           -- first we determine which sessions need to be updated and get
                           -- information about the started_at, last_lesson_activity_time, and client
                           $REDSHIFT$
                           WITH recent_sessions AS (
                               SELECT
                                   lesson_events.lesson_player_session_id,
                                   lesson_events.user_id,
                                   lesson_events.lesson_id,
                                   lesson_events.lesson_stream_id,
                                   split_part(
                                       #{string_agg('lesson_events.page_load_id', 'lesson_events.estimated_time')}
                                       , ','
                                       , 1) AS                       page_load_id,
                                   split_part(
                                       #{string_agg('page_loads.client', 'lesson_events.estimated_time')}
                                       , ','
                                       , 1) AS                       client,
                                   split_part(
                                       #{string_agg('page_loads.os_name', 'lesson_events.estimated_time')}
                                       , ','
                                       , 1) AS                       os_name,
                                   min(lesson_events.estimated_time) started_at,
                                   max(lesson_events.estimated_time) last_lesson_activity_time
                               FROM events lesson_events
                                   LEFT JOIN events page_loads
                                       ON page_loads.page_load_id = lesson_events.page_load_id
                                          AND page_loads.event_type = 'page_load:load'
                                          AND page_loads.estimated_time > '#{already_updated_to}'::timestamp - interval '1 months'
                                          -- We limit this to page loads in the last 1 month for performance reasons. It is
                                          -- possible but unlikely that the relevant page load was older than a month.  There is
                                          -- no way (far as I know) for us to tell redshift that each lesson_event will have only
                                          -- one page load and to tell it to start searching from now and go back in time to find it
                                          -- quickly.  Without the limit on estimated_time, redshift would have to look through every
                                          -- single page load for the user.  If we tell it only to look in the last month before the
                                          -- lesson event, however, then it can use the sort key on the table to limit what it looks at



                               WHERE lesson_events.event_type IN
                                     ('lesson:frame:unload', 'lesson:play', 'lesson:finish')
                                     AND lesson_events.created_at > '#{already_updated_to}'
                                     AND lesson_events.estimated_time < getdate()
                                     AND lesson_events.lesson_player_session_id is not null
                               GROUP BY
                                   lesson_events.lesson_player_session_id
                                   , lesson_events.user_id
                                   , lesson_events.lesson_id
                                   , lesson_events.lesson_stream_id
                           )

                           -- add in info about when the user started and completed this lesson. We have to do this
                           -- separately because the events do not have session ids in them
                           , with_starts_and_completes AS (
                               SELECT
                                   recent_sessions.lesson_player_session_id,
                                   recent_sessions.user_id,
                                   recent_sessions.lesson_id,
                                   recent_sessions.lesson_stream_id,
                                   recent_sessions.client,
                                   recent_sessions.os_name,
                                   recent_sessions.started_at,
                                   recent_sessions.last_lesson_activity_time,
                                   min(CASE
                                       WHEN event_type = 'lesson:start'
                                           THEN estimated_time
                                       ELSE NULL
                                       END) AS start_time,
                                   min(CASE
                                       WHEN event_type = 'lesson:complete'
                                           THEN estimated_time
                                       ELSE NULL
                                       END) AS complete_time
                               FROM recent_sessions
                                   LEFT JOIN events start_and_complete_events
                                       ON recent_sessions.user_id = start_and_complete_events.user_id
                                          AND recent_sessions.lesson_id = start_and_complete_events.lesson_id
                                          AND start_and_complete_events.event_type IN ('lesson:start', 'lesson:complete')
                               GROUP BY
                                   recent_sessions.lesson_player_session_id
                                   , recent_sessions.user_id
                                   , recent_sessions.lesson_id
                                   , recent_sessions.lesson_stream_id
                                   , recent_sessions.client
                                   , recent_sessions.os_name
                                   , recent_sessions.started_at
                                   , recent_sessions.last_lesson_activity_time
                           )
                           SELECT
                               lesson_player_session_id,
                               user_id,
                               lesson_id,
                               lesson_stream_id,
                               started_at,
                               last_lesson_activity_time,
                               start_time,
                               complete_time,
                               client,
                               os_name
                           FROM
                               with_starts_and_completes

                $REDSHIFT$) AS recent_sessions (
                    lesson_player_session_id UUID
                    , user_id UUID
                    , lesson_id UUID
                    , lesson_stream_id UUID
                    , started_at TIMESTAMP
                    , last_lesson_activity_time TIMESTAMP
                    , start_time TIMESTAMP
                    , complete_time TIMESTAMP
                    , client TEXT
                    , os_name TEXT
                )
            )
            SELECT
                lesson_player_session_id as session_id
                , user_id
                , lesson_id
                , lesson_stream_id
                , started_at
                , last_lesson_activity_time
                , extract('epoch' from last_lesson_activity_time) - extract('epoch' from started_at) as total_lesson_time
                , coalesce(start_time between started_at - interval '1 minute' and last_lesson_activity_time + interval '1 minute', false) as started_in_this_session
                , coalesce(complete_time between started_at - interval '1 minute' and last_lesson_activity_time + interval '1 minute', false) as completed_in_this_session
                , lessons.locale_pack_id as lesson_locale_pack_id
                , lesson_streams.locale_pack_id as lesson_stream_locale_pack_id
                , lessons.locale as lesson_locale
                , client_type(client, os_name) as client_type
            from recent_sessions
                join lessons on lessons.id = lesson_id
                left join lesson_streams on lesson_streams.id = lesson_stream_id
        ~
    end

    def self.string_agg(col, order)
        if RedshiftEvent.connection_config[:store_type] == "redshift"
            "listagg(DISTINCT #{col}) WITHIN GROUP (ORDER BY #{order})"
        else
            "string_agg(#{col}, ',' ORDER BY #{order})"
        end


    end
end
