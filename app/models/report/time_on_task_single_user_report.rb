class Report::TimeOnTaskSingleUserReport < Report::TimeOnTaskReport
    
    # as far as the server is concerned, this report is the same
    # as the TimeOnTaskReport.  In the client, however, there are 
    # different visible filters between the two, and the TimeOnTaskSingleUserReport
    # always has the user_id filter set

end