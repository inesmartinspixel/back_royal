# == Schema Information
#
# Table name: published_content_titles
#
#  type             :text
#  locale_pack_id   :uuid             primary key
#  target_locale    :text
#  published_locale :text
#  published_title  :string
#

class Report::JoinablePublishedContentTitle < ApplicationRecord
    self.inheritance_column = nil

    # this isn't really a primary key, but it works for our purposes since we're just interested in English titles where we're using this in StreamProgress
    # if we want to load up locales other than English, we need to add a real primary key (unique column) to this view.
    self.primary_key = :locale_pack_id

    default_scope -> { where(:published_locale => 'en') }

    self.table_name = 'published_content_titles'
end
