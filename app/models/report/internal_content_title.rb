# == Schema Information
#
# Table name: internal_content_titles
#
#  type           :text
#  id             :uuid
#  locale         :text
#  internal_title :text
#

class Report::InternalContentTitle < ApplicationRecord

    self.inheritance_column = nil

    self.table_name = 'internal_content_titles'
end
