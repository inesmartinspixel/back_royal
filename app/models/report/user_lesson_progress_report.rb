class Report::UserLessonProgressReport < Report::BaseTabularReport

    REPORT_CONFIG = {
        'klass' => Report::UserLessonProgressRecord
    }

    def self.field_configs

        vanilla_columns = {}
        %w(
            total_lesson_time locale_pack_id
            average_assessment_score_first
            lesson_finish_count average_assessment_score
            lesson_reset_count
        ).each do |name|
            vanilla_columns[name] = {'column' => name}
        end

        super.merge(vanilla_columns).merge({
            'last_lesson_activity_time' => {
                'column' => 'cast(EXTRACT(EPOCH FROM last_lesson_activity_time) as int)',
            },
            'started_at' => {
                'column' => 'cast(EXTRACT(EPOCH FROM started_at) as int)',
            },
            'completed_at' => {
                'column' => 'cast(EXTRACT(EPOCH FROM completed_at) as int)',
            },
            'last_lesson_reset_at' => {
                'column' => 'cast(EXTRACT(EPOCH FROM last_lesson_reset_at) as int)',
            },

            # assessment_finish_count is the same as lesson_finish_count, but only for assessment lessons
            'assessment_finish_count' => {
                'column' => 'CASE WHEN average_assessment_score_first IS NOT NULL THEN lesson_finish_count ELSE NULL END'
            },

            # only average_assessment_score_best if
            # - there is more than one finish
            # - (In the past we also checked) OR the two scores are different (It is only possible for those 2 values to be different
            #       when there is only one finish with old sessions where it was possible for users
            #       to go back and retry challenges within a session, before https://trello.com/c/zDkuVRYP/663-feat-threshold-for-completion-of-smartcase-lessons-and-don-t-allow-navigation-in-them)
            'average_assessment_score_best' => {
                'column' => 'CASE WHEN lesson_finish_count > 1 THEN average_assessment_score_best ELSE NULL END'
            }
        })
    end

end