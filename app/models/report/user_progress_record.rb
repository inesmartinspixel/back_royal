# == Schema Information
#
# Table name: user_progress_records
#
#  user_id                            :uuid
#  started_lesson_count               :bigint
#  completed_lesson_count             :bigint
#  started_lesson_locale_pack_ids     :uuid             is an Array
#  completed_lesson_locale_pack_ids   :uuid             is an Array
#  total_lesson_time                  :float
#  last_lesson_activity_time          :datetime
#  average_assessment_score_first     :float
#  average_assessment_score_best      :float
#  total_lesson_time_on_desktop       :float
#  total_lesson_time_on_mobile_app    :float
#  total_lesson_time_on_mobile_web    :float
#  total_lesson_time_on_unknown       :float
#  started_stream_count               :bigint
#  completed_stream_count             :bigint
#  started_stream_locale_pack_ids     :uuid             is an Array
#  completed_stream_locale_pack_ids   :uuid             is an Array
#  started_playlist_count             :bigint
#  completed_playlist_count           :bigint
#  started_playlist_locale_pack_ids   :uuid             is an Array
#  completed_playlist_locale_pack_ids :uuid             is an Array
#

class Report::UserProgressRecord < ApplicationRecord


    self.table_name = 'user_progress_records'
    belongs_to :user


end
