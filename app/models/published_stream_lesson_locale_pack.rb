# == Schema Information
#
# Table name: published_stream_lesson_locale_packs
#
#  stream_title          :string(255)
#  lesson_title          :string(255)
#  test                  :boolean
#  assessment            :boolean
#  stream_locale_pack_id :uuid
#  lesson_locale_pack_id :uuid
#  stream_locales        :text
#  lesson_locales        :text
#
class PublishedStreamLessonLocalePack < ApplicationRecord
end
