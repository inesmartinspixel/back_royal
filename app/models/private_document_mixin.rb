module PrivateDocumentMixin
    extend ActiveSupport::Concern

    included do

        has_attached_file :file,
            :path => :path,
            :processors => [:bulk],
            :s3_credentials => {
                :bucket => ENV['AWS_PRIVATE_USER_DOCUMENTS_BUCKET']
            },
            :s3_host_alias => "s3.amazonaws.com/#{ENV['AWS_PRIVATE_USER_DOCUMENTS_BUCKET']}",
            :s3_permissions => :private

        after_post_process -> { Paperclip::BulkQueue.process(file) }

        before_save :set_persisted_path
    end

    def set_persisted_path
        # in specs, sometimes there is no user, in which case
        # we cannot generate a path
        if Rails.env.test? && !user
            return
        end
        self.persisted_path = path if self.user
    end

    def path

        # If we have a url already, then use that to generate the path.  If we do not yet
        # have one, then generate one.
        if persisted_path.present?
            return persisted_path
        end

        # Since this is called from a before_save, these won't
        # be set if this is a new record.
        self.created_at ||= Time.now
        self.updated_at ||= self.created_at

        "#{self.user.id}/:fingerprint.#{self.created_at.to_timestamp}.:filename"
    end

end