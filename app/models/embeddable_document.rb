
=begin

We use this module to allow us to take the content_json hash
from a Lesson and instantiate a hierarchichal tree of frames
and components.  This is based on mongo mapper's API for doing
the same thing.

When this module is included in a class, the following class
methods are defined:

# set_type_field

    Defines the field that is used to determine the subclass that
    should be instantiated.  For example, the following code makes
    allows us to define two different components as the expected
    type (Note: in this case we had to do some special :

    class Lesson::Content
        include EmbeddableDocument
        set_type_field 'lesson_type'
    end

    Lesson::Content.load({component_type: 'frame_list'}).class
    => Lesson::Content::FrameList

    Lesson::Content.load({component_type: 'something_else'}).class
    => Lesson::Content::SomethingElse

# key(name, klass, options)

    Defines an allowed key for the class.  If you attempt to assign
    any other keys to an instance of the class, you will get a NoMethodError.
    If you try to assign a key of the wrong type, you will get an error

    class Component::Image
        include EmbeddableDocument
        key :label, String, :required => true
        key :image, Hash, :default => Proc.new { {} }
    end

# many(name, klass)

    Defines an key whose value will be an array of instances of the
    klass, which should also be an EmbeddableDocument

    class Componentized
        include EmbeddableDocument
        many :components, Component
    end

    frame = Componentized.load({
        components: [{}]
    })
    frame.components.class
    => Component

# embedded_in(name)

    The class that is passed into the `many` method can use this
    method to create an accessor for the parent:

    class Componentized
        include EmbeddableDocument
        many :components, Component
    end

    class Component
        include EmbeddableDocument
        embedded_in :frame
    end

    frame = Componentized.load({
        components: [{}]
    })
    frame.components[0].frame == frame
    => true



The following instance methods are available:

# as_json

    Includes values for each key that were defined on the
    class with the `key` method and that have been set on
    this particular instance

    class MyClass
        include EmbeddableDocument
        key :defined, String
        key :has_default, String, :default => 'default_value'
        key :undefined, String
    end

    MyClass.load({
        defined: 'some value'
    }).as_json
    => {'defined' => 'some value', 'has_default' => 'default_value'}

=end

module EmbeddableDocument
    extend ActiveSupport::Concern

    class Boolean; end

    class ManyCollection < Array
        attr_accessor :parent_document

        def <<(val)
            val.parent_document = self.parent_document
            super(val)
        end

        def delete(val)
            if val.parent_document == self.parent_document
                val.parent_document = nil
            end
            super(val)
        end

        def push(val)
            self << val
        end
    end

    included do |target|
        target.send(:include, ActiveModel::Model)
        target.send(:include, ActiveModel::Serialization)
        target.send(:attr_accessor, :attributes)
        target.send(:const_set, :Boolean, Boolean)
        target.send(:attr_accessor, :parent_document)
        key :id, String
    end

    module ClassMethods

        def load(attributes, parent_document = nil)
            if attributes.is_a?(self)
                return attributes
            elsif attributes.is_a?(Hash)
                instance = new(:called_from_load)
                instance.parent_document = parent_document
                instance.assign_attributes(attributes)
                return instance
            else
                raise ArgumentError.new("#{attributes.class} should be either a Hash or a #{self.name}")
            end
        end

        def key(*args)
            options_from_args = args.extract_options!
            name, type = args.shift.to_s, args.shift
            type ||= Object
            options = (options_from_args || {}).with_indifferent_access

            _keys[name] = options

            define_method(name.to_sym) do
                if !key_defined?(name) && key_has_default?(name)
                    return self.assign_default_value_for_key(name)
                end
                return instance_variable_get(:"@#{name}")
            end

            define_method("#{name}=".to_sym) do |val|
                if [true, false].include?(val) && type == Boolean
                    # this is ok, let it through
                elsif !val.nil? && !val.is_a?(type)
                    raise ArgumentError.new("Expecting a #{type.name} for key #{name} but got a #{val.class.name}")
                end
                if val && type == Hash
                    val = val.with_indifferent_access
                end
                return instance_variable_set(:"@#{name}", val)
            end

            if type == Boolean
                define_method("#{name}?".to_sym) do
                     self.send(name.to_sym)
                end
            end

            options = options.with_indifferent_access
            unexpected_options = options.keys.map(&:to_sym) - [:required, :default]
            if unexpected_options.any?
                raise "#{unexpected_options.inspect} options are not supported by EmbeddableDocument::key"
            end

            if options[:required]
                self.validates_presence_of name.to_sym
            end
        end

        def many(name, klass)

            _keys[name] = {
                :default => Proc.new { |doc|
                    list = ManyCollection.new
                    list.parent_document = doc
                    list
                }
            }

            unless klass.respond_to?(:load)
                raise ArgumentError.new("Invalid class #{klass}")
            end

            define_method(name.to_sym) do
                if !key_defined?(name) && key_has_default?(name)
                    return self.assign_default_value_for_key(name)
                end
                return instance_variable_get(:"@#{name}")
            end

            define_method(:"#{name}=") do |val|
                if val.nil?
                    list = nil
                else
                    if !key_defined?(name)
                        list = instance_variable_set(:"@#{name}", default_for_key(name))
                    else
                        list = self[name]
                    end

                    # remove all elements currently on the list, so
                    # that parent documents get unset
                    while list.first
                        list.delete(list.first)
                    end

                    # add each passed in element onto the list
                    val.each do |attrs|
                        list << klass.load(attrs, self)
                    end
                end
                self.instance_variable_set(:"@#{name}", list)
            end
        end

        def set_type_field(field)

            superclass = self

            self.key field, :required => true

            # set up the validation that disallows the saving of instances of the superclass
            self.validate :validate_is_subclass

            self.singleton_class.class_eval do

                define_method(:new) do |*args|
                    if args[0] == :called_from_load
                        super()
                    else
                        raise "classes that include EmbeddableDocument should be instantiate with ::load instead of ::new in order to be created with the appropriate class as specified in the json (i.e. in the lesson_type or frame_type field)."
                    end
                end

                # override new so that it checks the type field and instantiates the
                # appropriate subclass
                define_method(:load) do |attrs = {}, parent_document = nil|
                    if self == superclass
                        type = attrs[field.to_s] || attrs[field.to_sym] || ""

                        # if the type field is not set, or of the associated class
                        # cannot be found, then return an instance of the superclass.
                        # this cannot be saved however, as it will be invalid
                        begin
                            # FIXME: not sure if this is taking much time, but we could cache
                            # the result (component.rb overrides get_klass_for_type and might take
                            # more time)
                            klass = get_klass_for_type(type)
                        rescue NameError => e
                            return super(attrs)
                        end

                        klass.load(attrs, parent_document)
                    else
                        super(attrs, parent_document)
                    end
                end

                # this method can be overridden (see Componentized.Component)
                define_method(:get_klass_for_type) do |type|
                    const_get(type.camelize)
                end
            end

            define_method(:validate_is_subclass) do
                if self.class == superclass
                    self.errors.add(field.to_sym, "#{self[field].inspect} does not map to a known class")
                end
            end
        end

        def embedded_in(key)
            if @embedded_in
                raise "embedded_in already set to #{key}"
            end

            send(:define_method, key.to_sym) do
                @parent_document
            end
        end

        def key_has_default?(name)
            keys.fetch(name).key?(:default)
        end

        def keys
            if superclass.respond_to?(:keys)
                superclass.keys.merge(_keys)
            else
                _keys
            end
        end

        def key_names
            keys.keys
        end

        private
        def _keys
            unless defined?(@_keys)
                @_keys = {}.with_indifferent_access
            end
            @_keys
        end

    end # end ClassMethods


    # instance methods
    delegate :key_has_default?, :to => :class

    def as_json(options = {})
        attrs = {}
        self.class.key_names.each do |name|
            attrs[name] = self[name].as_json if key?(name)
        end
        attrs.deep_stringify_keys
    end

    def key?(name)
        assign_default_value_for_key_if_not_defined(name)
        key_defined?(name)
    end

    def [](key)
        return self.send(key.to_sym)
    end

    def []=(key, val)
        return self.send(:"#{key}=", val)
    end

    def assign_attributes(attributes)
        attributes.each do |key, val|
            self[key] = val
        end
    end

    def key_defined?(name)
        instance_variable_defined?(:"@#{name}")
    end

    def assign_default_value_for_key_if_not_defined(name)
        if key_defined?(name)
            self[name]
        elsif key_has_default?(name)
            assign_default_value_for_key(name)
        end
    end

    def assign_default_value_for_key(name)
        self[name] = default_for_key(name)
        return self[name]
    end

    def default_for_key(name)
        raise "No default for key #{name.inspect}" unless self.class.keys.fetch(name).key?(:default)
        default = self.class.keys[name][:default]
        val = default.is_a?(Proc) ? default.call(self) : default
        val
    end

end