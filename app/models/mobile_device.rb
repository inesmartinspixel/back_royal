# == Schema Information
#
# Table name: mobile_devices
#
#  id           :uuid             not null, primary key
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  user_id      :uuid             not null
#  device_token :text             not null
#  platform     :text             not null
#  last_used_at :datetime         not null
#

class MobileDevice < ApplicationRecord

    validates_presence_of :user_id
    validates_presence_of :device_token
    validates_presence_of :platform
    validates_presence_of :last_used_at

    before_save :verify_valid_platform

    def verify_valid_platform

        if !['android', 'ios'].include?(self.platform)
            Raven.capture_exception("Unexpected mobile_devices.platform", {
                level: 'warning',
                extra: {
                    time: Time.now.to_s,
                    id: self.id,
                    user_id: self.user_id,
                    device_token: self.device_token,
                    platform: self.platform,
                    last_used_at: self.last_used_at
                }
            })
        end

    end

end
