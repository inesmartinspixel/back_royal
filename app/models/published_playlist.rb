# == Schema Information
#
# Table name: published_playlists
#
#  id             :uuid
#  locale_pack_id :uuid
#  title          :text
#  version_id     :uuid
#  locale         :text
#  stream_entries :json             is an Array
#  stream_count   :integer
#

class PublishedPlaylist < ApplicationRecord

    def self.for_pref_locale(pref_locale, columns)

        query =  group("published_playlists.locale_pack_id")
                    .where("locale in ('#{pref_locale}', 'en')")

        columns.each do |col|
            query = query.select("(array_agg(published_playlists.#{col} order by case when locale = '#{pref_locale}' then 0 else 1 end))[1] as #{col}")
        end

        query
    end

end
