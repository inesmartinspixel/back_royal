# == Schema Information
#
# Table name: published_cohort_periods
#
#  created_at                                      :datetime
#  cohort_id                                       :uuid
#  name                                            :text
#  index                                           :integer
#  title                                           :text
#  style                                           :text
#  days                                            :integer
#  days_offset                                     :integer
#  additional_specialization_playlists_to_complete :integer
#  stream_entries                                  :json             is an Array
#  exercises                                       :json             is an Array
#  actions                                         :json             is an Array
#  learner_project_ids                             :uuid             is an Array
#  cohort_start_date                               :datetime
#  period_start                                    :datetime
#  period_end                                      :datetime
#  required_stream_pack_ids                        :uuid             is an Array
#  optional_stream_pack_ids                        :uuid             is an Array
#  required_lesson_pack_ids                        :uuid             is an Array
#  cumulative_required_stream_pack_ids             :uuid             is an Array
#  cumulative_required_lesson_pack_ids             :uuid             is an Array
#  cumulative_required_stream_pack_ids_count       :integer
#  cumulative_required_lesson_pack_ids_count       :integer
#  expected_specializations_complete               :float
#

class PublishedCohortPeriod < ApplicationRecord
    include IsDerivedContentTable

    update_when_attr_change_is_published(Cohort, :name)
    update_when_attr_change_is_published(Cohort, :start_date)
    update_when_attr_change_is_published(Cohort, :periods, :comparison => :hashdiff)

    default_scope -> { order(:cohort_id, :index) }

    # we need to specify the keys so that this works with Cohort::Version
    # as well as Cohort
    belongs_to :cohort, :primary_key => 'id', :foreign_key => 'cohort_id'

    class CurriculumPosition < String

        INDEXES = {
            before_enrollment_deadline: 0,
            enrollment_deadline: 1,
            after_cohort_end: 99,

            # mba
            first_half_before_midterm: 2,
            halfway_to_midterm: 3,
            second_half_before_midterm: 4,
            midterm_review: 5,
            midterm: 6,
            first_half_before_final: 7,
            halfway_to_final: 8,
            second_half_before_final: 9,
            final_review: 10,
            final_exam: 11,

            # emba
            before_exam_1: 2,
            exam_1: 3,
            before_exam_2: 4,
            exam_2: 5,
            before_exam_4: 6,
            exam_4: 7,
            before_exam_6: 8,
            exam_6: 9,
            before_exam_8: 10,
            exam_8: 11,
            after_last_exam: 12
        }

        def <=>(other)
            raise "Unknown curriculum position #{self.to_sym}" unless INDEXES.key?(self.to_sym)
            raise "Unknown curriculum position #{other.to_sym}" unless INDEXES.key?(other.to_sym)
            INDEXES[self.to_sym] <=> INDEXES[other.to_sym]
        end
    end

    REQUIRED_LESSONS_BUCKETS = 0.upto(50).to_a.map { |i| i*10 }

    def required_lessons_bucket

        unless defined?(@required_lessons_bucket)

            @required_lessons_bucket = REQUIRED_LESSONS_BUCKETS.detect do |i|
                (cumulative_required_lesson_pack_ids_count || 0) <= i
            end
        end
        @required_lessons_bucket
    end

    def inspect
        "#{name}.#{index}"
    end

    def normalized_index

        unless defined? @normalized_index

            periods = cohort.published_cohort_periods.sort_by(&:period_start)

            index_within_position = periods.select { |p| p.curriculum_position == self.curriculum_position }
                                        .sort_by(&:index)
                                        .index(self)

            @normalized_index = "#{curriculum_position} - #{index_within_position + 1}"

        end

        @normalized_index
    end

    def curriculum_position
        unless defined? @curriculum_position
            if cohort.program_type == 'mba'
                @curriculum_position = curriculum_position_mba
            elsif cohort.program_type == 'emba'
                @curriculum_position = curriculum_position_emba
            end
        end

        @curriculum_position

    end

    def before_enrollment_deadline?
        curriculum_position < CurriculumPosition.new('enrollment_deadline')
    end

    def self.rebuild
        cohort_ids = PublishedCohort.all.pluck(:id)
        return if Rails.env.test? && cohort_ids.empty?

        RetriableTransaction.transaction do
            self.truncate
            write_new_records(:cohort_id, cohort_ids)
        end
    end

    def create_or_update
        raise "Cannot use ActiveRecord create or update methods"
    end

    def self.update_on_content_change(klass:, identifiers: )
        if klass == Cohort
            write_new_records(:cohort_id, identifiers)
        end
    end

    private_class_method def self.select_new_records_sql(column, cohort_ids)
        if column != :cohort_id
            raise ArgumentError.new("must be cohort_id")
        end

        %Q~
            WITH period_definitions AS MATERIALIZED (
                SELECT
                    published_cohorts.id        AS cohort_id
                    , published_cohorts.name
                    , cohorts_versions.start_date AS cohort_start_date
                    , periods.definition
                    , periods.index
                FROM
                    published_cohorts
                    JOIN cohorts_versions
                        ON cohorts_versions.version_id = published_cohorts.version_id
                    , unnest(cohorts_versions.periods) WITH ORDINALITY periods(definition, index)
                    WHERE #{SqlIdList.get_in_clause("published_cohorts.id", cohort_ids)}
            )
            , with_period_details AS MATERIALIZED (
                SELECT
                    cohort_id
                    , name
                    , index
                    , definition ->> 'title'                                                          AS title
                    , definition ->> 'style'                                                          AS style
                    , (definition ->> 'days') :: INTEGER                                              AS days
                    , (definition ->> 'days_offset') :: INTEGER                                       AS days_offset
                    , (definition ->>
                        'additional_specialization_playlists_to_complete' ) :: INTEGER                AS additional_specialization_playlists_to_complete
                    , ARRAY(SELECT elem :: JSON
                            FROM json_array_elements((definition -> 'stream_entries')) elem) AS stream_entries
                    , ARRAY(SELECT elem :: JSON
                            FROM json_array_elements((definition -> 'exercises')) elem) AS exercises
                    , ARRAY(SELECT elem :: JSON
                            FROM json_array_elements((definition -> 'actions')) elem) AS actions
                    , ARRAY(SELECT elem :: uuid
                            FROM json_array_elements_text((definition -> 'learner_project_ids')) elem) AS learner_project_ids
                    --, ARRAY(json_array_elements_text((definition -> 'learner_project_ids'))) as learner_project_ids
                    , cohort_start_date
                FROM period_definitions

                UNION all
                    SELECT
                        published_cohorts.id as cohort_id
                        , published_cohorts.name
                        , 0 as index
                        , 'Period 0' as title
                        , null as style
                        , null as days
                        , null as days_offset
                        , null as additional_specialization_playlists_to_complete
                        , null as stream_entries
                        , null as exercises
                        , null as actions
                        , Array[]::uuid[] as learner_project_ids
                        , start_date as cohort_start_date
                FROM published_cohorts
                WHERE #{SqlIdList.get_in_clause("published_cohorts.id", cohort_ids)}
            )
            , time_info AS MATERIALIZED (
                SELECT
                    this_periods.cohort_id
                    , this_periods.index
                    , coalesce(sum(previous_periods.days + previous_periods.days_offset), 0) +
                    this_periods.days_offset offset_from_cohort_start
                FROM
                    with_period_details this_periods
                    LEFT JOIN with_period_details previous_periods
                        ON this_periods.cohort_id = previous_periods.cohort_id
                        AND this_periods.index > previous_periods.index
                GROUP BY
                    this_periods.cohort_id
                    , this_periods.index
                    , this_periods.days_offset
            )
            , with_time_info AS MATERIALIZED (
                SELECT
                    with_period_details.*

                    -- for period, start before the beginning of pedago
                    , case when with_period_details.index = 0 then '2000/01/01'
                        -- add_dst_aware_offset is a custom function we've added in a migration
                        else add_dst_aware_offset(cohort_start_date, offset_from_cohort_start, 'days')
                    end AS period_start

                    -- for period 0, end at the start of period 1
                    , case when with_period_details.index = 0
                        then (
                            select
                                -- add_dst_aware_offset is a custom function we've added in a migration
                                add_dst_aware_offset(with_period_details.cohort_start_date, first_periods.offset_from_cohort_start, 'days')
                            from time_info first_periods
                            where
                                first_periods.cohort_id = with_period_details.cohort_id
                                and first_periods.index = 1
                            )
                        -- add_dst_aware_offset is a custom function we've added in a migration
                        ELSE add_dst_aware_offset(cohort_start_date, offset_from_cohort_start + days, 'days')
                        end AS period_end
                FROM
                    with_period_details
                    JOIN time_info
                        ON with_period_details.cohort_id = time_info.cohort_id
                        AND with_period_details.index = time_info.index
            )

            -- For each cohort, create an extra period for each week in between
            -- the end date and the graduation date.
            , extra_periods AS MATERIALIZED (
                select
                    published_cohorts.id as cohort_id
                    , published_cohorts.name as name
                    , (select max(index) from with_time_info where with_time_info.cohort_id = published_cohorts.id) + 1 + extra_week_index as index
                    , 'After Cohort End ' || (1+extra_week_index)::text as title
                    , 'after_cohort_end' as style
                    , 7 as days
                    , 0 as days_offset
                    , 0 as additional_specialization_playlists_to_complete
                    , null::json[] as stream_entries
                    , null::json[] as exercises
                    , null::json[] as actions
                    , null::uuid[] as learner_project_ids
                    , published_cohorts.start_date as cohort_start_date
                    , published_cohorts.end_date + (extra_week_index || ' weeks')::interval as period_start
                    , published_cohorts.end_date + ((extra_week_index+1) || ' weeks')::interval as period_end
                from published_cohorts
                    join cohorts_versions
                        on cohorts_versions.version_id = published_cohorts.version_id

                    -- Join each row with the indexes 0..12.  12 is arbitrarily
                    -- selected since it is probably more than the number of weeks we would
                    -- ever have after the end date and before the graduation date
                    cross join (
                        SELECT
                            extra_week_index
                        FROM
                            generate_series(0, 12) AS extra_week_index
                    ) extra_weeks
                where

                    -- since 12 weeks is probably too many weeks, we filter out any that starts after the
                    -- graduation date
                    published_cohorts.end_date + (extra_week_index || ' weeks')::interval < published_cohorts.graduation_date
                    AND #{SqlIdList.get_in_clause("published_cohorts.id", cohort_ids)}
            )
            , with_extra_periods AS MATERIALIZED (
                select * from with_time_info
                union all
                select * from extra_periods
            )
            , with_stream_entries AS MATERIALIZED (
                SELECT
                    cohort_id
                    , index
                    , period_start
                    , period_end
                    , stream_entries.stream_entry
                    , stream_entries.stream_entry_index
                FROM with_extra_periods
                    , unnest(stream_entries) WITH ORDINALITY stream_entries(stream_entry, stream_entry_index)
            )
            , stream_pack_ids AS MATERIALIZED (
                SELECT
                    cohort_id
                    , index
                    , array_remove(
                        array_agg(
                            CASE
                            WHEN (stream_entry ->> 'required') :: BOOLEAN = TRUE
                                THEN (stream_entry ->> 'locale_pack_id') :: UUID
                            ELSE NULL
                            END
                        )
                        , NULL
                    ) AS required_stream_pack_ids
                    , array_remove(
                        array_agg(
                            CASE
                            WHEN (stream_entry ->> 'required') :: BOOLEAN = FALSE
                                THEN (stream_entry ->> 'locale_pack_id') :: UUID
                            ELSE NULL
                            END
                        )
                        , NULL
                    ) AS optional_stream_pack_ids
                FROM with_stream_entries
                GROUP BY cohort_id, index
            )
            , with_pack_ids AS MATERIALIZED (
                SELECT
                    with_extra_periods.*
                    , stream_pack_ids.required_stream_pack_ids
                    , stream_pack_ids.optional_stream_pack_ids
                    , array(
                        SELECT lesson_locale_pack_id
                        FROM
                            published_stream_lesson_locale_packs
                        WHERE stream_locale_pack_id = ANY (required_stream_pack_ids)
                    ) required_lesson_pack_ids
                FROM with_extra_periods
                    left JOIN stream_pack_ids
                        ON with_extra_periods.cohort_id = stream_pack_ids.cohort_id
                        AND with_extra_periods.index = stream_pack_ids.index
            )
            , expected_specializations_complete_as_integer AS MATERIALIZED (
                SELECT
                    with_pack_ids.cohort_id
                    , with_pack_ids.index
                    , with_pack_ids.style
                    , with_pack_ids.additional_specialization_playlists_to_complete
                    , sum(previous_periods.additional_specialization_playlists_to_complete) expected_specializations_complete
                from with_pack_ids
                    join with_pack_ids previous_periods
                        on with_pack_ids.cohort_id = previous_periods.cohort_id
                        and with_pack_ids.index >= previous_periods.index
                group by
                    with_pack_ids.cohort_id
                    , with_pack_ids.index
                    , with_pack_ids.style
                    , with_pack_ids.additional_specialization_playlists_to_complete
            )
            , expected_specializations_complete_as_float AS MATERIALIZED (
                SELECT
                    this_period.cohort_id
                    , this_period.index

                    -- This is a complicated calculation.  We are trying to get a float for each period
                    -- indicating how many specializations should be complete.  For example, if there are
                    -- 2 specialization periods in a row, and the second one has additional_specialization_playlists_to_complete=1,
                    -- then the first one should require 0.5 specializations, and the second one should require 1

                    -- take the total number of specializations that should be totally complete
                    , this_period.expected_specializations_complete +

                        -- figure out how many other specialization periods exist since the last one that has
                        -- additional_specialization_playlists_to_complete > 0 and the next one.  Come up with
                        -- a number between 0 and 1 by dividing the number of such specialization periods before
                        -- this one (inclusive) by the total number of such periods.
                        case when count(other_specialization_periods.index) = 0 or this_period.additional_specialization_playlists_to_complete > 0
                            then 0
                            else
                                count(case when
                                        other_specialization_periods.expected_specializations_complete = this_period.expected_specializations_complete
                                        and other_specialization_periods.index <= this_period.index
                                        and other_specialization_periods.additional_specialization_playlists_to_complete = 0
                                    then true else null end)::float
                                /
                                (count(case when
                                        other_specialization_periods.expected_specializations_complete = this_period.expected_specializations_complete
                                        and other_specialization_periods.additional_specialization_playlists_to_complete = 0
                                    then true else null end)::float + 1)
                            end
                    as expected_specializations_complete
                FROM
                    expected_specializations_complete_as_integer this_period
                    left join expected_specializations_complete_as_integer other_specialization_periods
                        on this_period.cohort_id = other_specialization_periods.cohort_id
                        and other_specialization_periods.style = 'specialization'
                group by
                    this_period.cohort_id
                    , this_period.index
                    , this_period.expected_specializations_complete
                    , this_period.additional_specialization_playlists_to_complete
            )
            , expected_counts AS MATERIALIZED (
                SELECT
                    periods.cohort_id
                    , periods.index
                    , array_sort_unique(
                        array_cat_agg(previous_periods.required_stream_pack_ids))     AS cumulative_required_stream_pack_ids
                    , array_sort_unique(array_cat_agg(
                                            previous_periods.required_lesson_pack_ids)) AS cumulative_required_lesson_pack_ids
                    , expected_specializations_complete_as_float.expected_specializations_complete
                FROM
                    with_pack_ids periods
                    JOIN with_pack_ids previous_periods
                        ON periods.cohort_id = previous_periods.cohort_id
                        AND periods.index >= previous_periods.index
                    join expected_specializations_complete_as_float
                        on periods.cohort_id = expected_specializations_complete_as_float.cohort_id
                        and periods.index = expected_specializations_complete_as_float.index
                    join published_cohorts
                        on periods.cohort_id = published_cohorts.id
                        AND #{SqlIdList.get_in_clause("published_cohorts.id", cohort_ids)}

                    -- num_required_specializations is not in published_cohorts,
                    -- so we need to join against the versions
                    join cohorts_versions
                        on published_cohorts.version_id = cohorts_versions.version_id
                GROUP BY
                    periods.cohort_id
                    , periods.index
                    , expected_specializations_complete_as_float.expected_specializations_complete
                    , num_required_specializations
            )
            , with_expected_counts_and_time_info AS MATERIALIZED (
                SELECT
                    with_pack_ids.*
                    , expected_counts.cumulative_required_stream_pack_ids
                    , expected_counts.cumulative_required_lesson_pack_ids
                    , array_length(expected_counts.cumulative_required_stream_pack_ids,
                                1) AS cumulative_required_stream_pack_ids_count
                    , array_length(expected_counts.cumulative_required_lesson_pack_ids,
                                1) AS cumulative_required_lesson_pack_ids_count
                    , expected_counts.expected_specializations_complete
                FROM
                    with_pack_ids
                    JOIN expected_counts
                        ON expected_counts.index = with_pack_ids.index
                        AND expected_counts.cohort_id = with_pack_ids.cohort_id
            )
            SELECT now() as created_at, *
            FROM with_expected_counts_and_time_info
            ORDER BY cohort_id, index
        ~
    end

    private def curriculum_position_mba
        position = cohort.milestone_indexes.invert[self.index]
        indexes = cohort.milestone_indexes

        if position
            str = position.to_s
        elsif self.period_end > cohort.end_date
            str = "after_cohort_end"
        elsif self.index < indexes[:enrollment_deadline]
            str = "before_enrollment_deadline"
        elsif self.index < indexes[:halfway_to_midterm]
            str = "first_half_before_midterm"
        elsif self.index < indexes[:midterm]
            str = "second_half_before_midterm"
        elsif self.index < indexes[:halfway_to_final]
            str = "first_half_before_final"
        else
            str = "second_half_before_final"
        end

        CurriculumPosition.new(str)
    end

    private def curriculum_position_emba
        position = cohort.milestone_indexes.invert[self.index]
        indexes = cohort.milestone_indexes

        if position
            str = position.to_s
        elsif self.index < indexes[:enrollment_deadline]
            str = "before_enrollment_deadline"
        elsif self.index < indexes[:exam_1]
            str = "before_exam_1"
        elsif self.index < indexes[:exam_2]
            str = "before_exam_2"
        elsif self.index < indexes[:exam_4]
            str = "before_exam_4"
        elsif self.index < indexes[:exam_6]
            str = "before_exam_6"
        elsif self.index < indexes[:exam_8]
            str = "before_exam_8"
        elsif self.period_end <= cohort.end_date
            str = "after_last_exam"
        else
            str = "after_cohort_end"
        end

        CurriculumPosition.new(str)
    end
end
