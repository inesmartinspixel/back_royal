# == Schema Information
#
# Table name: student_network_inclusions
#
#  id                           :uuid             not null, primary key
#  user_id                      :uuid             not null
#  cohort_application_id        :uuid             not null
#  cohort_id                    :uuid             not null
#  program_type                 :text             not null
#  pref_student_network_privacy :text             not null
#  graduation_status            :text             not null
#  status                       :text
#

class StudentNetworkInclusion < ApplicationRecord

    def alumnus?
        ['graduated', 'honors'].include?(self.graduation_status)
    end

    # See https://trello.com/c/W5MRKn72 and https://trello.com/c/rFSEI4h5
    def provides_access_to_student_network_events_visible_to_current_degree_students?
        # The student_network_inclusions table excludes users who have been 'accepted'
        # but have a graduation_status of 'failed', so we don't need to explicitly check
        # against 'failed' here.
        self.status == 'accepted' && self.graduation_status == 'pending'
    end

    # See https://trello.com/c/W5MRKn72 and https://trello.com/c/rFSEI4h5
    def provides_access_to_student_network_events_visible_to_graduated_degree_students?
        self.alumnus?
    end

    # See https://trello.com/c/W5MRKn72 and https://trello.com/c/rFSEI4h5
    def provides_access_to_student_network_events_visible_to_accepted_degree_students_in_cohorts?
        self.status == 'accepted'
    end

    # While actively deferred users with a student_network_inclusion record may have full access
    # to the student network map, we restrict their access to student network events since they
    # aren't currently 'accepted' to a degree program. Therefore, they should only see the student
    # network events that other non-degree users see.
    # See https://trello.com/c/W5MRKn72 and https://trello.com/c/rFSEI4h5
    def provides_access_to_student_network_events_visible_to_non_degree_users?
        # NOTE: users with `deferred` and `pre_accepted` applications are limited access users.
        # They should be able to see events marked visible to everyone.
        #
        # See ControllerMixins::StudentNetworkEventsFiltersMixin#filter_student_network_events_by_user_access
        # for a more detailed explanation.
        ['deferred', 'pre_accepted'].include?(self.status)
    end

    # EMBA users are given access to old conference events i.e. conference events
    # that have ended more than a week ago. See https://trello.com/c/ueGVLnTF.
    def provides_access_to_old_conference_events?
        self.program_type == 'emba'
    end
end
