module LocalePackMixin
    extend ActiveSupport::Concern

    module ClassMethods

        def set_content_item_klass(klass)
            has_many :content_items, :class_name => klass.name, :foreign_key => :locale_pack_id
            klass.send(:include, ::HasLocalePack)
        end

    end

    included do

    end

    def as_json(options = {})
        {
            'id' => self.id,
            'content_items' => content_items.map { |content_item|
                {
                    'id' => content_item.id,
                    'title' => content_item.title,
                    'locale' => content_item.locale
                }
            }
        }
    end

end