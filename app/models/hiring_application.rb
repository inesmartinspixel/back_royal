# == Schema Information
#
# Table name: hiring_applications
#
#  id                                  :uuid             not null, primary key
#  created_at                          :datetime         not null
#  updated_at                          :datetime         not null
#  applied_at                          :datetime         not null
#  accepted_at                         :datetime
#  user_id                             :uuid             not null
#  status                              :text             default("pending"), not null
#  job_role                            :text
#  company_year                        :integer
#  company_employee_count              :text
#  company_annual_revenue              :text
#  company_sells_recruiting_services   :boolean
#  place_id                            :text
#  place_details                       :json             not null
#  logo_url                            :text
#  website_url                         :text
#  hiring_for                          :text
#  team_name                           :text
#  team_mission                        :text
#  role_descriptors                    :text             default([]), is an Array
#  fun_fact                            :text
#  where_descriptors                   :text             default([]), is an Array
#  position_descriptors                :text             default([]), is an Array
#  funding                             :text
#  industry                            :text
#  last_calculated_complete_percentage :float
#  has_seen_welcome                    :boolean          default(FALSE), not null
#  salary_ranges                       :text             default([]), is an Array
#

###########################################################################################
=begin

 Hiring Application column          | Hiring Application text           | Career Profile column         | Career profile text                    | Options
 ----------------------------------------------------------------------------------------------------------------------------------------------------------
 role_descriptors                   | What roles are you hiring for?    | primary_areas_of_interest     | Choose up to 3 areas you're interested | CAREERS_AREA_KEYS
                                    |                                   |                               | in for future job roles, post-MBA      |

=end

class HiringApplication < ActiveRecord::Base

    include TimezoneMixin

    belongs_to :user

    # hardcoding this sort and limit so it's easier to include in includes_needed_for_json_eager_loading
    has_many :recent_career_profile_searches,
        -> { reorder('last_search_at DESC').limit(3) },
        :through => :user,
        :source => :career_profile_searches

    before_create :set_applied_at_to_now
    after_save :check_professional_organization
    after_save :update_hidden_relationships
    after_create :auto_create_hiring_team

    after_create :log_created_event
    after_save :log_status_event, if: :saved_change_to_status?
    after_save :close_hiring_relationships_if_changed_to_rejected

    # Re-identify the user with segment when a change is made to an application
    after_commit :identify_user, on: [:create, :update]

    before_validation :set_accepted_at_to_now_if_accepting
    before_validation :set_accepted_at_to_nil_if_not_accepted

    validates_presence_of :accepted_at, :if => :accepted?
    validate :validate_user_has_hiring_team, :if => :accepted?
    validates_absence_of :accepted_at, :unless => :accepted?

    COMPANY_KEYS = [:logo_url, :place_id, :place_details, :company_year, :company_employee_count, :company_annual_revenue, :website_url, :funding, :industry]
    TEAM_KEYS = [:hiring_for, :team_name, :team_mission, :role_descriptors]
    YOU_KEYS = [:job_role, :fun_fact]
    CANDIDATES_KEYS = [:where_descriptors, :position_descriptors, :salary_ranges]

    validates_presence_of :professional_organization, :on => :create
    delegate :professional_organization, :company_id, :company_name, :company_logo_url, :to => :user

    def self.update_from_hash!(hash)
        # Automatically retries in the event of ActiveRecord::PreparedStatementCacheExpired
        RetriableTransaction.transaction do
            hash = hash.unsafe_with_indifferent_access
            instance = find_by_id(hash[:id])

            if instance.nil?
                raise ActiveRecord::RecordNotFound.new("Couldn't find #{self.name} with id=#{hash[:id]}")
            end

            instance.merge_hash(hash)

            # force updated_at to change regardless of other changes
            instance.updated_at = Time.now

            instance.save!
            instance
        end
    end

    def self.includes_needed_for_json_eager_loading
        [
            :recent_career_profile_searches,
            {:user => [:avatar, :professional_organization]}
        ]
    end

    def merge_hash(hash)
        hash = hash.unsafe_with_indifferent_access

        (
            HiringApplication::COMPANY_KEYS +
            HiringApplication::TEAM_KEYS +
            HiringApplication::YOU_KEYS +
            HiringApplication::CANDIDATES_KEYS +
            ["status", "company_sells_recruiting_services", "last_calculated_complete_percentage", "has_seen_welcome"]
        ).each do |key|
            if hash.key?(key)
                val = hash[key]
                val = nil if val.is_a?(String) && val.blank?

                # normalize URLs with a leading prefix
                if val && key.to_s.ends_with?('_url') && !(val[/^http:\/\//] || val[/^https:\/\//])
                    val = "https://#{val}"
                end

                self[key] = val
            end
        end

        self
    end

    def set_applied_at_to_now
        self.applied_at = Time.now
    end

    def auto_create_hiring_team
        unless user.hiring_team.present? || HiringTeam.where(domain: user.email_domain).exists?
            user.hiring_team = HiringTeam.new(
                title: user.company_name,
                owner_id: user.id
            )
            user.save!
        end
    end

    def set_accepted_at_to_now_if_accepting
        if status == 'accepted' && accepted_at.nil?
            self.accepted_at = Time.now
        end
    end

    def set_accepted_at_to_nil_if_not_accepted
        if status != 'accepted'
            self.accepted_at = nil
        end
    end

    def accepted?
        accepted_at.present?
    end

    def validate_user_has_hiring_team
        if !self.user.hiring_team
            errors.add(:user, "hiring_team must be specified if accepted")
        end
    end

    def as_json(options = {})

        hash = {
            "id" => id,
            "applied_at" => applied_at.to_timestamp,
            "accepted_at" => accepted_at.to_timestamp,
            "updated_at" => updated_at.to_timestamp,
            "status" => status,
            "user_id" => user_id,
            "company_id" => company_id,
            "company_name" => company_name,
            "company_logo_url" => company_logo_url,
            "last_calculated_complete_percentage" => last_calculated_complete_percentage,
            "company_sells_recruiting_services" => company_sells_recruiting_services,
            "has_seen_welcome" => has_seen_welcome,
            "recent_career_profile_searches" => recent_career_profile_searches.as_json,

            # User fields
            "avatar_url" => user.avatar_url,
            "job_title" => user.job_title,
            "phone" => user.phone,
            "name" => user.name,
            "nickname" => user.nickname,
            "hiring_team_id" => self.user.hiring_team_id
        }

        %w(email phone name phone_extension job_title).each do |user_prop|
            hash[user_prop] = user.send(user_prop.to_sym)
        end

        form_keys = HiringApplication::COMPANY_KEYS +
            HiringApplication::TEAM_KEYS +
            HiringApplication::YOU_KEYS +
            HiringApplication::CANDIDATES_KEYS

        form_keys.each do |key|
            hash[key] = self[key]
        end

        hash.stringify_keys!

        if options[:zapier]
            hash['location'] = self.place_details ? self.place_details['formatted_address'] : ""
            ['applied_at', 'accepted_at'].each do |key|
                if self[key]
                    hash[key] = self[key].to_s
                end
            end

            # these things aren't needed in the zap, and they just muck up the UI
            # if they're there.  Especially the place_details
            hash.delete('place_details')
            hash.delete('place_id')
        end

        hash
    end

    def identify_user
        self.user.identify
    end

    private def check_professional_organization

        # If the status changes to accepted we want to make sure that the professional organization
        # associated with the user is flagged as suggestable to indicate that it is now an
        # approved organization that can be suggested to other users
        if saved_change_to_status? && self.status === 'accepted' &&
            self.professional_organization && !self.professional_organization.suggest

            # promote, ensuring we have copies for all locales
            self.professional_organization.promote_suggested_for_all_locales
        end
    end

    private def update_hidden_relationships
        if saved_change_to_status? && status == "accepted"
            # Automatically retries in the event of ActiveRecord::PreparedStatementCacheExpired
            RetriableTransaction.transaction do
                self.user.candidate_relationships.where(:hiring_manager_status=> "accepted", :candidate_status => "hidden").each do |relationship|
                    relationship.candidate_status = "pending"
                    relationship.save!
                end
            end
        end
    end

    private def log_created_event
        Event.create_server_event!(
            SecureRandom.uuid,
            user_id,
            'hiring_application:created',
            event_attributes
        ).log_to_external_systems
    end

    private def event_attributes
        {
            hiring_team_id: user.hiring_team_id,
            hiring_plan: user.hiring_team&.hiring_plan,
            status: status,
            is_hiring_team_owner: user.hiring_team&.owner_id == user.id,
            has_subscription: user.hiring_team&.subscriptions&.any?,
            has_primary_subscription: user.hiring_team&.primary_subscription.present?,
            primary_subscription_stripe_plan_id: user.hiring_team&.primary_subscription&.stripe_plan_id
        }
    end

    private def log_status_event
        if status == 'rejected'
            event_type = 'hiring_application:rejected'
        elsif status == 'accepted'
            event_type = 'hiring_application:accepted'
        end
        return unless event_type

        Event.create_server_event!(
            SecureRandom.uuid,
            user_id,
            event_type,
            event_attributes.merge({
                # Since the default is pending, this will show the status changing from
                # pending to accepted even if the application was just created.  We want to
                # identify invited team members who started in the accepted state, so we
                # set old_status to nil if the application was just created.
                old_status: saved_change_to_id? ? nil : saved_changes[:status][0]
            })
        ).log_to_external_systems
    end

    private def close_hiring_relationships_if_changed_to_rejected
        if self.saved_change_to_status? && self.status == 'rejected'
            # I thought about setting hiring_manager_closed_info, but then figured
            # an empty value along with the editor tracking would suffice if we
            # wanted to know if this auto-closing occurred.
            #
            # There is a situation where a candidate might have already closed,
            # then setting it to hiring_manager_closed tries to set candidate_status to
            # hidden, but then we have a validation that checks against being closed and
            # not accepted, so the relationship doesn't update to hiring_manager_closed.
            # I don't feel like this is worth trying to address. If
            # we find a case of a candidate re-opening a relationship for a hiring manager
            # that was rejected after being accepted, then we can do some more work here.
            #
            # There is a campaign that we probably don't want to trigger when we do this
            # artificial closing, but it is disabled. If we enable it, we should add
            # an additional clause to check for this situation, assumably by either
            # checking for a segment (we identify hiring_application.status), adding
            # the hiring_application status to the event, or setting some reason in the
            # info field.
            #
            # See https://trello.com/c/j3j8NOVM
            # See https://fly.customer.io/env/24964/campaigns/1000162/overview/trigger
            self.user.candidate_relationships_with_accepted_hiring_manager_status
                .where(hiring_manager_closed: [nil, false])
                .update(hiring_manager_closed: true)
        end
    end
end
