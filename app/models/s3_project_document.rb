# == Schema Information
#
# Table name: s3_project_documents
#
#  id                :uuid             not null, primary key
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  file_file_name    :string
#  file_content_type :string
#  file_file_size    :integer
#  file_updated_at   :datetime
#  file_fingerprint  :string
#

class S3ProjectDocument < ApplicationRecord
    has_attached_file :file,
        :path => "project_documents/:fingerprint.:extension",
        :s3_credentials => {
            :bucket => ENV['AWS_UPLOADS_BUCKET']
        },
        :s3_host_alias => ENV['AWS_UPLOADS_BUCKET']

    do_not_validate_attachment_file_type :file

    def as_json(options = {})
        {
            id: id,
            file_file_name: file_file_name,
            url: file.url
        }
    end
end
