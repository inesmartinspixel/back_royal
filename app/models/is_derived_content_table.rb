=begin

IsDerivedContentTable is mixed in to tables that are derived from our main
content tables, and updated when changes are made that will affect
published content.

IsDerivedContentTable provides some class methods that are used to set up
triggers indicating that a derived table should be changed.

 * update_when_attr_change_is_published
    When a change to a content item is published, derived
    content tables may need to be updated.

 * update_when_access_groups_change
    When access groups change on a published content item,
    derived content tables may need to be updated.  It's a
    bit of a strange feature of our system that all changes
    to content need to be published EXCEPT for access group
    changes, which take effect immediately.  That is why
    we need this extra helper.

Besides calling `update_when_attr_change_is_published` and
`update_when_access_groups_change` in the definition of the class
for a derived content table, you must also define `update_on_content_change`,
which defines what needs to happen when one of the triggers is triggered.

=end

require 'tsort'

module IsDerivedContentTable
    extend ActiveSupport::Concern

    # Since rails dynamically loads classes in devmode, we cannot
    # be sure that these will be loaded and register their dependencies.
    # So, we have to keep an explicit list of the classes that include
    # this module. (We could also grep the code programatically for
    # `include IsDerivedContentTable`, but not doing that for now)
    DERIVED_CONTENT_TABLE_CLASS_NAMES = Set.new(%w(
        PublishedCohortPeriod
        PublishedCohortPeriodsStreamLocalePack
        PublishedCohortPlaylistLocalePack
        PublishedCohortStreamLocalePack
        PublishedCohortLessonLocalePack
    ))

    included do

        if IsDerivedContentTable::DERIVED_CONTENT_TABLE_CLASS_NAMES.exclude?(self.name) && !self.respond_to?(:skip_class_name_check)
            raise "#{self.name} must be registered in DERIVED_CONTENT_TABLE_CLASS_NAMES"
        end

        define_method(:create_or_update) do
            raise "Cannot use ActiveRecord create or update methods"
        end
    end

    # method that can be mocked in specs
    def self.derived_content_table_class_names
        DERIVED_CONTENT_TABLE_CLASS_NAMES
    end

    # A Topological Sort is when you have a bunch of items, and
    # each one might have to come after some of the other ones, and
    # you need to figure out what order to do them in.
    #
    # This is exactly what we have here, and it's built in to Ruby!
    #
    # see https://ruby-doc.org/stdlib-2.4.1/libdoc/tsort/rdoc/TSort.html
    # see https://en.wikipedia.org/wiki/Topological_sorting
    class TopologicalSort < Hash
        include TSort
        alias tsort_each_node each_key
        def tsort_each_child(node, &block)
            fetch(node).each(&block)
        end
    end


    def self.get_derived_content_table_update_triggers(content_klass:, old_version:, new_version:, access_groups_did_change: false)

        # Storing the dependencies within each individual class means we don't
        # have to worry about classes getting swapped out with dynamic reloading
        # in devode. When this runs on each publish, it will get the active
        # version of each class and use its dependencies.
        derived_content_table_klasses = derived_content_table_class_names.map(&:constantize)

        dependencies = derived_content_table_klasses.map do |derived_content_table_klass|
            derived_content_table_klass.derived_content_table_dependencies[content_klass.name]
        end.flatten

        # get the list of required updates
        required_updates = dependencies.select do |opts|
            changed = if opts[:type] == :attr_change && opts[:comparison] == :eq
                old_version&.send(:[], opts[:attr]) != new_version&.send(:[], opts[:attr])
            elsif opts[:type] == :attr_change && opts[:comparison] == :sort_agnostic_collection
                old_version&.send(opts[:attr])&.sort != new_version&.send(opts[:attr])&.sort
            elsif opts[:type] == :attr_change && opts[:comparison] == :hashdiff
                Hashdiff.diff(old_version&.send(opts[:attr]), new_version&.send(opts[:attr])).present?
            elsif opts[:type] == :access_group_change && access_groups_did_change
                true
            end

            changed
        end

        # convert the required updates into triggers that can be used in
        # Publishable::make_derived_content_table_updates
        required_updates.map! do |opts|
            {
                klass_to_update: opts[:klass_to_update].constantize,
                identifier: opts[:identifier],
                old_version: old_version,
                new_version: new_version
            }
        end

        # see comments up above TopologicalSort in this file
        topological_sort = TopologicalSort.new
        klassnames = required_updates.pluck(:klass_to_update).uniq.map(&:name)
        required_updates.each do |update|
            topological_sort[update[:klass_to_update].name] = update[:klass_to_update].update_after_tables & klassnames
        end
        klass_sort = topological_sort.tsort

        required_updates.sort_by { |update| klass_sort.index(update[:klass_to_update].name) }.uniq
    end

    def self.get_changed_stream_lpids_on_playlist_change(version_pairs)
        # When a playlist changes (or is unpublished), it's possible that a
        # stream needs to be removed from all of the cohorts with that playlist, which
        # then means lessons may be removed as well.
        # But there is no way for us to know that just from the playlist's local_pack_id.
        # We need to look at the versions.
        stream_locale_pack_ids = version_pairs.map do |pair|
            old_version, new_version = pair
            a, b = (old_version&.stream_locale_pack_ids || []), (new_version&.stream_locale_pack_ids || [])
            a - b | b - a
        end.flatten.uniq
    end

    module ClassMethods

        def update_after(*other_derived_tables)
            # we have to store names here rather than actual classes to defend against
            # changed classes being reloaded in dev mode.
            # We cannot use += because update_after_tables is readonly
            other_derived_tables.map(&:name).each do |name|
                self.update_after_tables << name
            end
        end

        def update_after_tables
            @update_after_tables ||= Set.new
        end

        def derived_content_table_dependencies
            @derived_content_table_dependencies ||= Hash.new { |hash, klassname| hash[klassname] = [] }
        end

        # Calling this method sets up a trigger such that when a
        # change to the given attribute on the given class is published,
        # `update_on_content_change` will be called.
        #
        # Arguments:
        #
        # - content_klass - the class on which to watch for changes (i.e. Cohort)
        # - attr - the attribute to watch for changes (i.e. :name)
        # - opts
        #   - comparison - the operator to use to determine whether the
        #                   attribute has changed. You may need to set this if the attribute
        #                   is non-scalar, like a json hash or an array (See definition of
        #                   get_derived_content_table_update_triggers for possible values)
        #   - identifier - the attribute that defines a content item in this derived
        #                   content table.  This is `id` by default, but often needs to
        #                   be overridden to `locale_pack_id`
        def update_when_attr_change_is_published(content_klass, attr, opts = {})
            opts[:comparison] ||= :eq
            opts[:identifier] ||= :id

            self.derived_content_table_dependencies[content_klass.name] << opts.merge({
                type: :attr_change,
                attr: attr,
                klass_to_update: self.name
            })
        end

        # Calling this method sets up a trigger such that when a
        # the access groups change on a given content item,
        # `update_on_content_change` will be called.
        #
        # Arguments:
        #
        # - content_klass - the class on which to watch for changes (i.e. Cohort)
        # - opts
        #   - identifier - the attribute that defines a content item in this derived
        #                   content table.  This is `id` by default, but often needs to
        #                   be overridden to `locale_pack_id`
        def update_when_access_groups_change(content_klass, opts = {})
            opts[:identifier] ||= :id

            self.derived_content_table_dependencies[content_klass.name] << opts.merge({
                type: :access_group_change,
                klass_to_update: self.name
            })
        end

        private def select_new_records_sql(*args)
            raise NotImplementedError.new("classes that include IsDerivedContentTable must define select_new_records_sql ")
        end

        # FIXME: once we get rid of temp tables, this can be private
        def write_new_records(column, identifiers)
            begin
                retries ||= 0

                RetriableTransaction.transaction do
                    result = self.connection.execute %Q~
                        DELETE FROM #{self.table_name}
                        WHERE #{SqlIdList.get_in_clause(column, identifiers)}
                    ~

                    # select_new_records_sql should be defined in the class that
                    # includes this mixing.  It should return some sql that
                    # will select the rows to insert
                    new_records_sql = select_new_records_sql(column, identifiers)

                    column_names = self.column_names.join(',')
                    result = self.connection.execute %Q~
                        INSERT INTO #{self.table_name} (#{column_names})
                        (#{new_records_sql})
                    ~
                end
            rescue ActiveRecord::RecordNotUnique => err
                if (retries += 1) < 3
                    retry
                else
                    raise err
                end
            end
        end

        def update_when_the_role_of_a_stream_in_a_cohort_curriculum_changes
            # for the purposes of this method, `role` refers to the flags that are
            # attached to a stream, describing the role it plays in the curriculum for
            # a cohort i.e. foundations, required, elective, specialization, exam

            # if a playlist is added or removed, or if the list of streams in a
            # a playlist change, this can change the role of the
            # streams in that playlist
            update_when_attr_change_is_published(Playlist, :locale_pack_id, identifier: :locale_pack_id)
            update_when_attr_change_is_published(Playlist, :stream_entries, identifier: :locale_pack_id, comparison: :hashdiff)


            # streams can be attached to cohorts through access groups, so
            # changing access groups can change the role of a stream in a cohort
            update_when_access_groups_change(Cohort)
            update_when_access_groups_change(Lesson::Stream, identifier: :locale_pack_id)

            # streams can be made required or optional by being included in the periods
            # for the cohort
            update_when_attr_change_is_published(Cohort, :periods, :comparison => :hashdiff)

            # streams can be made required or specialization by being include in required
            # or specialization playlists
            update_when_attr_change_is_published(Cohort, :playlist_collections, comparison: :hashdiff)
            update_when_attr_change_is_published(Cohort, :specialization_playlist_pack_ids, comparison: :sort_agnostic_collection)

            # exam is defined with a flag on the stream itself
            update_when_attr_change_is_published(Lesson::Stream, :exam, identifier: :locale_pack_id)

            # locale_pack_id doesn't really change, but we should watch it
            update_when_attr_change_is_published(Lesson::Stream, :locale_pack_id, identifier: :locale_pack_id)

        end

    end

end