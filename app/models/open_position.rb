# == Schema Information
#
# Table name: open_positions
#
#  id                                :uuid             not null, primary key
#  created_at                        :datetime         not null
#  updated_at                        :datetime         not null
#  hiring_manager_id                 :uuid             not null
#  title                             :text             not null
#  salary                            :float
#  available_interview_times         :text
#  last_invitation_at                :datetime
#  place_id                          :text
#  place_details                     :json             not null
#  role                              :text
#  job_post_url                      :text
#  description                       :text
#  perks                             :text             default([]), is an Array
#  desired_years_experience          :json             not null
#  featured                          :boolean          default(FALSE)
#  archived                          :boolean          default(FALSE), not null
#  position_descriptors              :text             default([]), not null, is an Array
#  drafted_from_positions            :boolean          default(FALSE)
#  posted_at                         :datetime
#  last_curated_at                   :datetime
#  curation_email_last_triggered_at  :datetime
#  last_updated_at_by_hiring_manager :datetime
#  auto_expiration_date              :date
#  manually_archived                 :boolean          default(FALSE), not null
#  location                          :geography(Point,
#  short_description                 :text
#  subscription_id                   :uuid
#  will_sponsor_visa                 :boolean
#

class OpenPosition < ApplicationRecord

    class OldVersion < RuntimeError; end

    # For ensuring consistent ordering of ties. The title order provides consistency for screenshot specs.
    DEFAULT_ORDERS = ["open_positions.updated_at DESC", :title, :id]

    attr_accessor :just_auto_expired

    belongs_to :hiring_manager, :class_name => 'User'
    has_one :hiring_team, :through => :hiring_manager
    belongs_to :subscription, optional: true

    # skills auto-suggest (ordered)
    has_many :skills_joins, class_name: 'OpenPositionsSkillsOptionsJoin', dependent: :destroy
    has_many :skills, class_name: 'SkillsOption', through: :skills_joins, source: :skills_option, dependent: :destroy

    has_many :candidate_position_interests, :class_name => 'CandidatePositionInterest', :dependent => :destroy, :validate => false
    has_many :candidate_position_interests_needing_review, -> { where(candidate_status: 'accepted').where.not(hiring_manager_status: CandidatePositionInterest.hidden_or_reviewed_hiring_manager_statuses) }, :class_name => 'CandidatePositionInterest'
    has_many :accepted_and_unhidden_candidate_position_interests, -> { where(candidate_status: 'accepted').where.not(hiring_manager_status: 'hidden') }, :class_name => 'CandidatePositionInterest'

    # FIXME: We should name this better
    has_many :unreviewed_and_unrejected_candidate_position_interests, -> { where(admin_status: 'unreviewed').where.not(candidate_status: 'rejected') }, :class_name => 'CandidatePositionInterest'

    has_many :interested_candidates, through: :candidate_position_interests, source: :candidate
    has_many :hiring_relationships, :foreign_key => :open_position_id
    has_one :hiring_application, :through => :hiring_manager
    has_one :professional_organization, :through => :hiring_manager

    before_validation :set_posted_at, if: :will_save_change_to_featured?
    validates_presence_of :posted_at, :if => :featured?

    validates_absence_of :auto_expiration_date, :unless => :should_have_auto_expiration_date?
    validates_presence_of :auto_expiration_date, :if => :should_have_auto_expiration_date?

    validates :manually_archived, inclusion: { in: [ false ] }, unless: :archived?

    before_validation :reconcile_manually_archived_and_auto_expiration_date

    after_commit :log_archived_or_renewal_canceled_event

    # see note in cohort.rb#ActiveRecord_Relation about why this has to be a separate module
    class ActiveRecord_Relation
        include OpenPosition::OpenPositionRelationExtensions
    end

    def should_have_auto_expiration_date?
        # If we ever see that the position has been manually_archived we want to ensure that the
        # auto_expiration_date is set to null for data integrity purposes so we don't have positions
        # in the database that have the manually_archived flag set to true but have an auto_expiration_date
        # still in the future. Likewise for positions that are not manually_archived we want to ensure
        # that an auto_expiration_date is present.

        # checking both subscription_id and subscription because
        # 1. it is faster to check subscription_id
        # 2. it is easier in specs to test subscription_id
        # 3. in the wild, it is theoretically (though probably not practically) possible that an unsaved subscription
        #       would be attached to a position, and it would not yet have an id at this point in the saving process
        !manually_archived? && subscription_id.blank? && subscription.blank?
    end

    def self.create_or_update_from_hash!(hash, meta = {})
        hash = hash.unsafe_with_indifferent_access
        instance = find_by_id(hash[:id])

        if instance
            update_from_hash!(hash, meta)
        else
            create_from_hash!(hash, meta)
        end
    end

    def self.create_from_hash!(hash, meta = {})
        # Automatically retries in the event of ActiveRecord::PreparedStatementCacheExpired
        RetriableTransaction.transaction do
            hash = hash.unsafe_with_indifferent_access
            instance = new(id: hash[:id])
            instance.merge_hash(hash, meta)
            instance.save!
            instance
        end
    end

    def self.update_from_hash!(hash, meta = {})
        # Automatically retries in the event of ActiveRecord::PreparedStatementCacheExpired
        RetriableTransaction.transaction do
            hash = hash.unsafe_with_indifferent_access
            instance = find_by_id(hash[:id])
            is_admin = meta.present? && meta[:is_admin]

            if instance.nil?
                raise ActiveRecord::RecordNotFound.new("Couldn't find #{self.name} with id=#{hash[:id]}")
            end

            if !is_admin && instance.updated_at && instance.updated_at.to_timestamp > hash['updated_at'].to_timestamp
                # We decided not to handle conflicts between teammates on open positions right now, so last
                # update will win. We wanted to detect how often this occurs though to get a
                # sense of urgency.
                Raven.capture_exception('Updating position with stale data', {
                    extra: {
                        open_position_id: instance.id,
                        updated_at_before: instance.updated_at.to_timestamp,
                        updated_at_after: hash['updated_at'].to_timestamp
                    }
                })
            elsif is_admin && instance.last_updated_at_by_hiring_manager && hash['last_updated_at_by_hiring_manager'].present?
                instance.last_updated_at_by_hiring_manager.to_timestamp > hash['last_updated_at_by_hiring_manager'].to_timestamp
                # Admins should never be able to overwrite a hiring manager's changes
                raise OpenPosition::OldVersion.new
            end

            # We update last_updated_at_by_hiring_manager if a non-admin is updating
            # this position. This lets us detect specifically if an admin is conflicting
            # with a hiring_manager, without having to worry about last_curated_at causing
            # a false positive when it is set in the curation admin.
            if !is_admin
                hash["last_updated_at_by_hiring_manager"] = Time.now.to_timestamp
            end

            instance.merge_hash(hash, meta)
            instance.save!
            instance
        end

    end

    def self.includes_needed_for_json_eager_loading(options = {})
        includes = [:skills]

        if options[:fields]&.include?('ADMIN_FIELDS')
            includes << :accepted_and_unhidden_candidate_position_interests
            includes << :unreviewed_and_unrejected_candidate_position_interests
            includes << :hiring_manager
            includes << :professional_organization
        end

        if !options[:except]&.include?("hiring_application")
            includes << {:hiring_application => HiringApplication.includes_needed_for_json_eager_loading}
        end

        includes
    end

    def merge_hash(hash, meta = {})
        meta ||= {}
        hash = hash.unsafe_with_indifferent_access

        # auto_expiration_date is a date field (similar to birthdate on the users table); not datetime,
        # so we don't want to convert it to a timestamp like last_invitation_at and last_updated_at_by_hiring_manager
        %w(
            hiring_manager_id title salary available_interview_times auto_expiration_date manually_archived
            place_id place_details role position_descriptors job_post_url description short_description
            perks desired_years_experience featured archived skills drafted_from_positions will_sponsor_visa
        ).each do |key|
            if hash.key?(key)
                val = hash[key]
                val = nil if val.is_a?(String) && val.blank?

                # special handling of skills
                option_class = {
                    "skills" => SkillsOption
                }[key]
                if option_class and val.is_a?(Array)
                    # loop through all the options
                    self.send(:"#{key}_joins=", []) # reset the association collection
                    self.send(:"#{key}=", []) # reset the association collection

                    val.each_with_index do |option_vals, index|
                        # find_or_initialize an option. Since this is a has_many :through association, rails will
                        # auto-create the join record, and acts_as_list will automatically set the position on the join record
                        # http://stackoverflow.com/questions/26651811/rails-activerecord-has-many-through-association-on-unsaved-objects
                        option = option_class.find_or_initialize_by(option_vals.slice(:locale, :text))
                        self.send(key.to_sym) << option
                    end
                else
                    # normalize URLs with a leading prefix
                    if val && key.ends_with?('_url') && !(val[/^http:\/\//] || val[/^https:\/\//])
                        val = "https://#{val}"
                    end

                    self.send(:"#{key}=", val)
                end
            end
        end

        %w(last_invitation_at last_updated_at_by_hiring_manager).each do |key|
            if hash[key]&.present?
                val = Time.at(hash[key])
                self.send(:"#{key}=", val)
            end
        end

        self
    end

    def reconcile_manually_archived_and_auto_expiration_date
        reconcile_manually_archived
        reconcile_auto_expiration_date
    end

    def reconcile_manually_archived
        if self.archived_changed?(to: false)
            self.manually_archived = false
        elsif self.archived_changed?(to: true) && !self.just_auto_expired
            self.manually_archived = true
        end
    end

    def reconcile_auto_expiration_date
        if self.subscription_id

            # positions with subscriptions don't need auto-expiration dates.  They will
            # be archived when the subscription runs out
            self.auto_expiration_date = nil
        elsif self.new_record? && !self.auto_expiration_date && !self.archived? || self.archived_changed?(to: false)
            self.auto_expiration_date = Date.today + 60.days
        elsif self.archived_changed?(to: true) && self.manually_archived?
            # In general, if we ever see that the position has been manually_archived
            # we want to ensure that the auto_expiration_date is set to null for data
            # integrity purposes so we don't have positions in the database that have
            # the manually_archived flag set to true but have an auto_expiration_date
            # still in the future.
            self.auto_expiration_date = nil
        end
    end

    def event_attributes(user_id)
        {
            open_position_id: self.id,
            open_position_title: self.title,
            open_position_review_path: self.review_path,
            featured: self.featured,
            archived: self.archived,
            has_subscription: subscription_id.present?,
            current_period_end: subscription&.current_period_end.to_timestamp,
            is_hiring_team_owner: hiring_team&.owner_id == user_id,
            is_open_position_creator: user_id == hiring_manager_id,
            hiring_plan: hiring_team&.hiring_plan
        }
    end

    def trigger_curation_email(hiring_manager_id)
        interests = self.candidate_position_interests.to_a
        payload = event_attributes(hiring_manager_id).merge({
            num_outstanding: interests.select { |i|
                i.admin_status == 'outstanding' &&
                !i.hidden_or_reviewed_by_hiring_manager? &&
                i.candidate_status != 'rejected'
            }.size,
            num_approved: interests.select { |i|
                i.admin_status == 'reviewed' &&
                !i.hidden_or_reviewed_by_hiring_manager? &&
                i.candidate_status != 'rejected'
            }.size
        })

        event = nil
        ActiveRecord::Base.transaction do
            event = Event.create_server_event!(
                SecureRandom.uuid,
                hiring_manager_id,
                'open_position:send_curation_email',
                payload
            )
            event.log_to_external_systems
            self.update_column(:curation_email_last_triggered_at, event.created_at)
        end

        event
    end

    def company_logo_url
        professional_organization && professional_organization.image_url
    end

    def company_name
        professional_organization && professional_organization.text
    end

    def as_json(options = {})
        hash = super.merge(
            "updated_at" => updated_at.to_timestamp,
            "created_at" => created_at.to_timestamp,
            "last_invitation_at" => last_invitation_at.to_timestamp,
            "skills" => skills.as_json,
            "posted_at" => self.posted_at.to_timestamp,
            "last_curated_at" => self.last_curated_at.to_timestamp,
            "curation_email_last_triggered_at" => self.curation_email_last_triggered_at.to_timestamp,
            "last_updated_at_by_hiring_manager" => self.last_updated_at_by_hiring_manager.to_timestamp,
            "will_sponsor_visa" => self.will_sponsor_visa,

            # We typically call `to_timestamp` on dates to return them as timestamp integers, but we had not
            # done this for auto_expiration_date and now there is code that needs it to be in this format. Let's
            # at least go ahead and call to_s on it because apparently as_json does not turn it into a string,
            # thus there can be annoyances in controller specs where Date != the string format.
            "auto_expiration_date" => self.auto_expiration_date.to_s,

            # Stubbed out. ATM all open positions are internal, but these two tickets
            # will be changing that in the near-term:
            # https://trello.com/c/5cH9Mhwu/3213-feat-use-ziprecruiter-api-to-bolster-job-search-results-in-featured-positions
            # https://trello.com/c/ysJO9KJN/3172-feat-enable-3rd-party-jobs-in-career-network-featured-positions
            "external" => false 
        )

        if options[:fields]&.include?('ADMIN_FIELDS')
            # We should probably just build into user.rb the ability to get a basic set of
            # fields without all of the associations? Then we could just put hiring_manager on the hash.
            # The reason we are doing the following is because a user.rb#as_json is expensive.
            hash["hiring_manager_id"] = self.hiring_manager.id
            hash["hiring_manager_email"] = self.hiring_manager.email
            hash["hiring_manager_name"] = self.hiring_manager.name
            hash["hiring_manager_company_name"] = self.hiring_manager.company_name
            hash["hiring_manager_last_seen_at"] = self.hiring_manager.last_seen_at&.to_timestamp
            hash["hiring_manager_hiring_team_id"] = self.hiring_manager.hiring_team_id

            hash["num_interested_candidates"] = self.accepted_and_unhidden_candidate_position_interests.size > 0 ?
                self.accepted_and_unhidden_candidate_position_interests.size : nil
            hash["num_unreviewed_and_unrejected_interests"] = self.unreviewed_and_unrejected_candidate_position_interests.size > 0 ?
                self.unreviewed_and_unrejected_candidate_position_interests.size : nil
        else
            hash = hash.except(*%w|posted_at last_curated_at curation_email_last_triggered_at last_updated_at_by_hiring_manager|)
        end

        if !options[:except]&.include?("hiring_application")
            hash["hiring_application"] = hiring_application.as_json
        end

        hash
    end

    def review_path
        return "/hiring/positions?positionId=#{self.attributes['id']}&action=review"
    end

    private
    def set_posted_at
        if self.posted_at.nil? && self.featured
            self.posted_at = Time.now
        end
    end

    private
    # This is run in an after_commit because it has to check for changes on the associated
    # subscription event.  The after_commit means we don't have to think about which order
    # the changes are made in.  But it means that the changes have to be made to the associated
    # subscription object.  The following would break this method, since the open_position's subscription
    # object would not have any saved changes:
    #
    #   subscription = Subscription.find(open_position.subscription_id)
    #   subscription.update_cancel_at_period_end!(new_value_for_cancel_at_period_end)
    #   open_position.save!
    #
    # but this will work just fine:
    #
    #   open_position.subscription.update_cancel_at_period_end!(new_value_for_cancel_at_period_end)
    #   open_position.save!
    def log_archived_or_renewal_canceled_event
        renewal_just_canceled = subscription&.saved_change_to_cancel_at_period_end? && subscription&.cancel_at_period_end
        position_just_archived = saved_change_to_archived? && archived == true

        return unless renewal_just_canceled || position_just_archived

        users = [hiring_manager, hiring_manager&.hiring_team&.owner].uniq.compact

        users.each do |user|
            Event.create_server_event!(
                SecureRandom.uuid,
                user.id,
                "open_position:archived_or_renewal_canceled",
                event_attributes(user.id).merge({
                    position_just_archived: position_just_archived,
                    renewal_just_canceled: renewal_just_canceled
                })
            ).log_to_external_systems
        end


    end
end
