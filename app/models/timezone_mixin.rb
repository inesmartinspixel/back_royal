module TimezoneMixin
    extend ActiveSupport::Concern

    included do
        include ActiveSupport::Callbacks

        after_save :enqueue_update_user_timezone_job, if: :saved_change_to_place_id?
    end

    def enqueue_update_user_timezone_job
        UpdateUserTimezoneJob.perform_later(self.class.name, self.id) if self.place_id
    end


    # Looks up the timezone based on the records lat/lng values in its place_details hash, which should be present
    # if a place_id is also present.
    # NOTE: This method utilizes the Timezone library which reaches out to Google's Timezone API under the hood.
    # Google has a Pay-As-You-Go billing strategy for utilizing their Timezone API, so we should be mindful of
    # this when using this method (see https://developers.google.com/maps/documentation/timezone/usage-and-billing).
    # @return String|nil - The name of the timezone in region format (see
    #       https://learn.customer.io/documentation/example-timezones.html#region) if the Timezone library was
    #       successfully able to determine the timezone or nil if it could not determine the appropriate timezone.
    # @raises Timezone::Error::Base if an error occurred during the timezone lookup (see
    #       https://github.com/panthomakos/timezone#error-states-and-nil-objects)
    def lookup_timezone
        Timezone.lookup(self.place_details['lat'], self.place_details['lng']).name if self.place_id
    end
end