# == Schema Information
#
# Table name: work_experiences
#
#  id                                  :uuid             not null, primary key
#  created_at                          :datetime
#  updated_at                          :datetime
#  job_title                           :text             not null
#  start_date                          :date             not null
#  end_date                            :date
#  responsibilities                    :text             default([]), not null, is an Array
#  career_profile_id                   :uuid             not null
#  professional_organization_option_id :uuid             not null
#  featured                            :boolean          default(FALSE), not null
#  role                                :text             default("other")
#  industry                            :text             default("other")
#  employment_type                     :text             default("full_time"), not null
#

require 'date'
require 'action_view'

class CareerProfile::WorkExperience < ApplicationRecord

    include ActionView::Helpers::DateHelper

    # Current experiences will in fact be sorted properly like this.
    # See http://stackoverflow.com/a/20959470/1747491
    default_scope -> { order(end_date: :desc) }
    self.table_name = 'work_experiences'
    belongs_to :professional_organization, class_name: 'ProfessionalOrganizationOption', foreign_key: :professional_organization_option_id, primary_key: :id
    belongs_to :career_profile

    after_save :update_career_profile_fulltext, if: :should_update_career_profile_fulltext?
    after_destroy :update_career_profile_fulltext

    def update_career_profile_fulltext
        career_profile&.update_fulltext
    end

    def should_update_career_profile_fulltext?
        !!(
            saved_changes[:professional_organization_option_id] ||
            saved_changes[:job_title] ||
            saved_changes[:industry] ||
            saved_changes[:responsibilities]
        )
    end

    def as_json(options = {})
        attributes.as_json.merge({
            "professional_organization" => professional_organization.as_json,
            "start_date" => start_date.to_datetime.to_time.to_timestamp * 1000, # convert to milliseconds for the client
            "end_date" => end_date ? end_date.to_datetime.to_time.to_timestamp * 1000 : nil # convert to milliseconds for the client
        })
    end

    def sort_key
        # sort with end_date:nil first and then by end_date desc
        - (end_date.present? ? end_date.to_time.to_timestamp : Time.parse('2999/01/01').to_timestamp)
    end

    # This is used by Zapier when reporting the length of a work_experience to the Google Sheet
    def length_string
        if end_date.nil?
            return "current"
        else
            return distance_of_time_in_words(self.start_date, self.end_date)
        end
    end

end
