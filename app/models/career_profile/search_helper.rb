# == Schema Information
#
# Table name: career_profile_search_helpers_2
#
#  id                                 :uuid             not null, primary key
#  career_profile_id                  :uuid             not null
#  current_up_to                      :datetime         not null
#  work_experience_roles              :text             not null, is an Array
#  work_experience_industries         :text             not null, is an Array
#  past_work_interval                 :interval
#  work_experience_company_names      :text
#  education_experience_school_names  :text
#  current_work_experience_roles      :text             not null, is an Array
#  current_work_experience_industries :text             not null, is an Array
#  has_current_work_experience        :boolean          not null
#  in_school                          :boolean          not null
#  skills_texts                       :text             not null, is an Array
#  student_network_interests_texts    :text             not null, is an Array
#  accepted_by_hiring_managers_count  :integer          not null
#  default_prioritization             :float            not null
#  years_of_experience                :float
#  years_of_experience_reference_date :datetime
#  skills_vector                      :tsvector
#  student_network_interests_vector   :tsvector
#

class CareerProfile::SearchHelper < ApplicationRecord

    self.table_name = 'career_profile_search_helpers_2'

    # can be mocked in specs
    def self.now
        Time.now
    end

    def self.get_max_time_str
        # In the wild, we want to have a little bit of a buffer because the updated_at is
        # set in rails. So, if we got right up to the last record, it's possible that there
        # is something with a slightly earlier updated_at that is not yet in the database,
        # and we would miss it. (Since the statement timeout is 1 minute, it seems like 2 minutes
        # should be plenty. In practice the difference between rails generating an updated_at and
        # a record hitting the db will rarely be more that a couple seconds)
        (Rails.env.prod? ? now - 2.minute : now).utc.strftime('%Y-%m-%d %H:%M:%S.%N')
    end

    def self.get_select_for_update_sql(filter_where, new_current_up_to_str)

        raise "Must provide a filter_where limiting condition" unless filter_where.present?

        %Q~
            -- This assumes that all of the associated records (except for hiring_relationships)
            -- are only updated along with an update to career_profiles.updated_at.  Currently
            -- this is always true, since all of the related stuff is updated through CareerProfile.update_from_hash
            --
            -- In addition, we need to handle cases where people create work experiences with
            -- dates in the future.  When we cross the start_date or the end_date for a work
            -- experience, we need to re-create the record for the profile.
            with targeted_career_profiles AS MATERIALIZED (
                select
                    distinct on (career_profiles.id) career_profiles.*
                from career_profiles
                    left join hiring_relationships
                        on hiring_relationships.candidate_id = career_profiles.user_id
                    left join work_experiences
                        on work_experiences.career_profile_id = career_profiles.id
                where #{filter_where}
            )
           , aggregated_education_experience_data AS MATERIALIZED (
                SELECT
                    career_profiles.id as career_profile_id

                    -- use string_agg to make an indexed ilike query easy
                    , string_agg(DISTINCT educational_organization_options.text, ' ' ) as education_experience_school_names

                    -- in_school
                    , count(
                            case when
                                degree_program
                                and (
                                    -- if it is before june, than a graduation_year of this year counts as in_school
                                    (date_part('month', now()) < 6 AND graduation_year = date_part('year', now()))
                                    -- a graduation_year after this year always counts as in_school
                                    OR (graduation_year > date_part('year', now()))
                                )
                            then true
                            else null end
                        ) > 0 as in_school
                FROM
                    targeted_career_profiles career_profiles
                    left join education_experiences on career_profiles.id = education_experiences.career_profile_id
                        left join educational_organization_options on education_experiences.educational_organization_option_id = educational_organization_options.id
                where career_profiles.updated_at > career_profiles.created_at -- for performance, ignore the 1/3 of profiles that were never changed after initially created
                group by
                    career_profiles.id
            )
            , aggregated_hiring_relationship_data AS MATERIALIZED (
                SELECT
                    career_profiles.id as career_profile_id

                    , count(case
                        when hiring_relationships.hiring_manager_status = 'accepted'
                            then true
                        else
                            null
                        end
                    ) as accepted_by_hiring_managers_count

                    , max(hiring_relationships.updated_at) as max_hiring_relationship_updated_at
                FROM
                    targeted_career_profiles career_profiles
                    left join hiring_relationships
                        on hiring_relationships.candidate_id = career_profiles.user_id
                where career_profiles.updated_at > career_profiles.created_at -- for performance, ignore the 1/3 of profiles that were never changed after initially created
                group by
                    career_profiles.id
            )
            , work_experiences_with_current AS MATERIALIZED (
                SELECT
                    work_experiences.*
                    , (employment_type='full_time' and start_date < now() and (end_date > now() or end_date is null)) as current
                    , case when employment_type='full_time' and start_date < now() and end_date < now() and end_date > start_date
                            then tsrange(start_date, end_date)
                        when employment_type='full_time' and start_date < now() and (end_date is null or end_date > now())
                            then tsrange(start_date::timestamp, now()::timestamp)
                        else
                            NULL
                        end as past_work_range
                from targeted_career_profiles career_profiles
                    join work_experiences on career_profiles.id = work_experiences.career_profile_id

            )
            , aggregated_work_experience_data AS MATERIALIZED (
                SELECT
                    career_profiles.id as career_profile_id

                    -- work experience roles and industries
                    , array_agg(DISTINCT work_experiences.role) as work_experience_roles
                    , array_remove(
                            array_agg(case when work_experiences.current = true then work_experiences.role else null end)
                            , null
                        ) as current_work_experience_roles
                    , array_agg(DISTINCT work_experiences.industry) as work_experience_industries
                    , array_remove(
                            array_agg(case when work_experiences.current = true then work_experiences.industry else null end)
                            , null
                        ) as current_work_experience_industries

                    -- use string_agg to make an indexed ilike query easy
                    , string_agg(DISTINCT professional_organization_options.text, ' ' ) as work_experience_company_names

                    , count(case when work_experiences.current = true then true else null end) > 0 as has_current_work_experience

                    , sum_ranges(past_work_range order by work_experiences.start_date) as past_work_interval
                FROM
                    targeted_career_profiles career_profiles
                    join aggregated_education_experience_data
                        on career_profiles.id = aggregated_education_experience_data.career_profile_id
                    left join work_experiences_with_current work_experiences ON career_profiles.id = work_experiences.career_profile_id
                        left join professional_organization_options on work_experiences.professional_organization_option_id = professional_organization_options.id
                where career_profiles.updated_at > career_profiles.created_at -- for performance, ignore the 1/3 of profiles that were never changed after initially created
                group by
                    career_profiles.id
            )
            , aggregated_skills_data AS MATERIALIZED (
                SELECT
                    career_profiles.id as career_profile_id

                    -- skills
                    , array_agg(distinct skills_options.text) as skills_texts
                FROM
                    targeted_career_profiles career_profiles
                    left join career_profiles_skills_options on career_profiles.id = career_profiles_skills_options.career_profile_id
                        left join skills_options skills_options ON career_profiles_skills_options.skills_option_id = skills_options.id
                where career_profiles.updated_at > career_profiles.created_at -- for performance, ignore the 1/3 of profiles that were never changed after initially created
                group by
                    career_profiles.id
            )
            , aggregated_student_network_interests_data AS MATERIALIZED (
                SELECT
                    career_profiles.id as career_profile_id

                    -- student_network_interests
                    , array_agg(distinct student_network_interests_options.text) as student_network_interests_texts
                FROM
                    targeted_career_profiles career_profiles
                    left join career_profiles_student_network_interests_options on career_profiles.id = career_profiles_student_network_interests_options.career_profile_id
                        left join student_network_interests_options student_network_interests_options ON career_profiles_student_network_interests_options.student_network_interests_option_id = student_network_interests_options.id
                where career_profiles.updated_at > career_profiles.created_at -- for performance, ignore the 1/3 of profiles that were never changed after initially created
                group by
                    career_profiles.id
            )
            , career_profiles_with_aggregated_data AS MATERIALIZED (
                SELECT
                    aggregated_work_experience_data.career_profile_id

                    , '#{new_current_up_to_str}'::timestamp as current_up_to

                    -- work experience roles and industries
                    , work_experience_roles
                    , work_experience_industries
                    , past_work_interval

                    -- use string_agg to make an indexed ilike query easy
                    , work_experience_company_names
                    , education_experience_school_names

                    , current_work_experience_roles
                    , current_work_experience_industries

                    , has_current_work_experience

                    -- in_school
                    , in_school

                    -- skills
                    , skills_texts
                    , setweight(to_tsvector('english', array_to_string(skills_texts, ' ')), 'A') as skills_vector

                    -- student_network_interests
                    , student_network_interests_texts
                    , setweight(to_tsvector('english', array_to_string(student_network_interests_texts, ' ')), 'A') as student_network_interests_vector

                    -- hiring_relationships
                    , accepted_by_hiring_managers_count

                FROM
                    aggregated_work_experience_data
                    join targeted_career_profiles career_profiles on career_profiles.id = aggregated_work_experience_data.career_profile_id
                    join aggregated_education_experience_data on aggregated_education_experience_data.career_profile_id = aggregated_work_experience_data.career_profile_id
                    join aggregated_skills_data on aggregated_skills_data.career_profile_id = aggregated_work_experience_data.career_profile_id
                    join aggregated_student_network_interests_data on aggregated_student_network_interests_data.career_profile_id = aggregated_work_experience_data.career_profile_id
                    join aggregated_hiring_relationship_data on aggregated_hiring_relationship_data.career_profile_id = aggregated_work_experience_data.career_profile_id
            )

            -- subtract the start from the end to get the years
            select
                career_profiles_with_aggregated_data.*

                -- default prioritization
                ,   (
                        -- candidates in the US and Canada come first
                        1000000 *
                        case
                            when place_details #>> '{country,short}' IN ('US', 'CA')
                            then 1 else 0 end
                      +
                        -- Candidate most interested in a job come first
                        100000 *
                        case
                            when interested_in_joining_new_company='very_interested' then 9
                            when interested_in_joining_new_company='interested' then 8
                            when interested_in_joining_new_company='neutral' then 7
                            else 6
                        end
                      +
                        -- Candidates that are marked as do_not_create_relationships come last
                        10000 *
                        case
                            when do_not_create_relationships=false
                            then 1 else 0
                        end
                      +
                        -- Candidates that have been accepted by more hiring managers come first,
                        -- since apparently they are good candidates
                        case
                            when accepted_by_hiring_managers_count is not null
                            then accepted_by_hiring_managers_count else 0
                        end
                      +
                        -- we need something unique across records to ensure consistent
                        -- sorting for pagination.  created_at desc makes it more likely
                        -- for new users to get attention
                        (extract(epoch from created_at)::float / 100000000000)
                    ) as default_prioritization

                -- When there is no current work experience, then years_of_experience is fixed
                -- and will not change with the passage of time.  In that case we set
                -- years_of_experience and we do not set years_of_experience_reference_date.
                -- When there is a current_work_experience, we cannot just set a fixed value
                -- for years_of_experience because it will change with the passage of time.  In that
                -- case we set a years_of_experience_reference_date such that, at any point in the
                -- future now() - years_of_experience_reference_date will be the years of experience
                -- that the candidate has.
                , case
                    when has_current_work_experience = true
                        then null
                    else
                        EXTRACT(epoch FROM past_work_interval) / EXTRACT(epoch FROM interval '1 year')
                    end
                as years_of_experience
                , case
                    when has_current_work_experience = true
                        then now() - past_work_interval
                    else
                        null
                    end as years_of_experience_reference_date
            from career_profiles_with_aggregated_data
                join targeted_career_profiles career_profiles on career_profiles.id = career_profile_id
        ~
    end


    def self.update_records(filter_where, new_current_up_to_str = nil)

        new_current_up_to_str ||= self.get_max_time_str

        update_sql = self.get_select_for_update_sql(filter_where, new_current_up_to_str)

        columns = %w(
            career_profile_id
            current_up_to
            work_experience_roles
            work_experience_industries
            past_work_interval
            work_experience_company_names
            education_experience_school_names
            current_work_experience_roles
            current_work_experience_industries
            has_current_work_experience
            in_school
            skills_texts
            skills_vector
            student_network_interests_texts
            student_network_interests_vector
            accepted_by_hiring_managers_count
            default_prioritization
            years_of_experience
            years_of_experience_reference_date
        )

        set_clause = (columns - ['career_profile_id']).map { |col|
            "#{col} = EXCLUDED.#{col}"
        }.join(',')

        ActiveRecord::Base.connection.execute %Q~

            insert into #{self.table_name} (
                #{columns.join(',')}
            )
                (#{update_sql})
            on conflict (career_profile_id) do update
                set #{set_clause}
        ~
    end


    def self.write_new_records

        current_up_to = self.maximum(:current_up_to) || Time.at(0)
        current_up_to_str = current_up_to.utc.strftime('%Y-%m-%d %H:%M:%S.%N')
        new_current_up_to_str = self.get_max_time_str

        filter_where = %Q~
            career_profiles.updated_at between '#{current_up_to_str}' and '#{new_current_up_to_str}'
            or hiring_relationships.updated_at between '#{current_up_to_str}' and '#{new_current_up_to_str}'
            or work_experiences.start_date between '#{current_up_to_str}' and '#{new_current_up_to_str}'
            or work_experiences.end_date between '#{current_up_to_str}' and '#{new_current_up_to_str}'
        ~

        self.update_records(filter_where, new_current_up_to_str)
    end

    def get_years_of_experience
        # If they have a current work experience then we do not set the years_of_experience
        # column but rather the years_of_experience_reference_date because really their "years of experience"
        # will change with time.
        # See comment above about years_of_experience_reference_date
        if self.years_of_experience_reference_date.present?
            duration_seconds = Time.now.to_timestamp - self.years_of_experience_reference_date.to_timestamp
            return duration_seconds / 60 / 60 / 24 / 365.25
        else
            return self.years_of_experience
        end
    end
end
