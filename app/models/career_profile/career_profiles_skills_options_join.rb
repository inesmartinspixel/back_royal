# == Schema Information
#
# Table name: career_profiles_skills_options
#
#  id                :uuid             not null, primary key
#  career_profile_id :uuid             not null
#  skills_option_id  :uuid             not null
#  position          :integer          not null
#

class CareerProfile::CareerProfilesSkillsOptionsJoin < ActiveRecord::Base
    self.table_name = 'career_profiles_skills_options'
    default_scope -> {order(:position)}
    belongs_to :career_profile
    belongs_to :skills_option, class_name: 'SkillsOption'
    acts_as_list :scope => :career_profile
end
