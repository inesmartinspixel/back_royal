module CareerProfile::TranscriptRequirementHelper

    # See also: vendor/front-royal/components/careers/scripts/models/education_experience.js
    # 
    # NOTE: We need to use a hard-coded list of known degrees here because in the process
    # of determining transcript requirements, we want to weight unknown/user input degrees
    # the same as the highest level known degree for that user. Unfortunately, that means
    # we cannot use matchers here. `degree` is just a text column on the EducationExperience
    # model, so if someone inputs something that matches `associate` or `bachelor`, any 
    # logic that uses a matcher to assign a degree level would potentially categorize those
    # inputs incorrectly. 
    DEGREE_LEVELS = {
        # Associate or equivalent
        'associate_of_applied_arts' => 1,
        'associate_of_applied_science' => 1,
        'associate_of_arts' => 1,
        'associate_of_engineering' => 1,
        'associate_of_political_science' => 1,
        'associate_of_science' => 1,
        'higher_national_diploma' => 1,

        # Bachelor or equivalent
        'bachelor_of_architecture' => 2,
        'bachelor_of_arts' => 2,
        'bachelor_of_arts_2' => 2,
        'bachelor_of_business_administration' => 2,
        'bachelor_of_commerce' => 2,
        'bachelor_of_engineering' => 2,
        'bachelor_of_fine arts' => 2,
        'bachelor_of_science' => 2,
        'bachelor_of_science_intl' => 2,

        # Master or equivalent
        'master_of_advanced_studies' => 3,
        'master_of_advanced_studies_2' => 3,
        'master_of_arts' => 3,
        'master_of_arts_2' => 3,
        'master_of_business_administration' => 3,
        'master_of_engineering' => 3,
        'master_of_laws' => 3,
        'master_of_philosophy' => 3,
        'master_of_research' => 3,
        'master_of_science' => 3,
        'master_of_science_2' => 3,
        'master_of_studies' => 3,
        'master_of_technology' => 3,

        # Terminal or equivalent
        'doctor_of_education' => 4,
        'doctor_of_medicine' => 4,
        'doctor_of_philosophy' => 4,
        'doctor_of_philosophy_2' => 4,
        'doctor_of_veterinary_medicine' => 4,
        'juris_doctor' => 4
    }

    def self.determine_transcript_requirements(_career_profile, _education_experience = nil)

        # Get all education experiences that require transcripts
        education_experiences = _career_profile.education_experiences_indicating_transcript_required

        return unless education_experiences&.any?

        # Put relevant info into a hash for easy manipulation
        education_experiences_hash = education_experiences.map { |e|
            e.serializable_hash
                .extract!("id", "degree", "graduation_year", "official_transcript_required_override")
                .merge({
                    "blacklisted" => e.educational_organization.degree_blacklist,
                    "official_transcript_required" => false # default value, overwritten below for one record
                })
        }

        education_experiences = education_experiences.index_by(&:id) # for easy reference later

        # Based on DEGREE_LEVELS above, assign a numeric level to everything we can
        max_degree_level = 0
        education_experiences_hash.each do |hash|
            level = DEGREE_LEVELS[hash["degree"]]
            if level.present?
                hash["degree_level"] = level
                max_degree_level = level if level > max_degree_level
            end
        end

        # If we encounter record(s) for which we weren't able to determine a level,
        # assign the record(s) a level corresponding to the highest non-empty level.
        education_experiences_hash
            .select { |hash| hash["degree_level"].blank? }
            .each do |hash|
                hash["degree_level"] = max_degree_level
        end

        # Sort by degree_level and graduation_year desc
        education_experiences_hash = education_experiences_hash.sort_by { |e| [e["degree_level"],e["graduation_year"]] }.reverse

        # First check for an admin override. Then go with the highest level, most recent, 
        # non-blacklisted experience requires an official transcript. If no non-blacklisted 
        # experience exists, choose the highest level, most recent as a fallback.
        experience_with_override = education_experiences_hash.detect { |h| h["official_transcript_required_override"] }
        experience_requiring_official_transcript = experience_with_override ||
            education_experiences_hash.detect { |h| !h["blacklisted"] } || 
            education_experiences_hash.first
        experience_requiring_official_transcript["official_transcript_required"] = true

        # Iterate through the constructed hash and update on the corresponding
        # education experiences
        education_experiences_hash.each do |hash|
            experience_to_update = education_experiences[hash["id"]]
            # determine_transcript_requirements can be called from a lifecycle hook 
            # on the EducationExperience model. If this is the case, we don't want
            # to call `update` on the record.
            if _education_experience&.id == hash["id"]
                _education_experience.official_transcript_required = hash["official_transcript_required"]
            elsif experience_to_update.new_record?
                experience_to_update.official_transcript_required = hash["official_transcript_required"]
            elsif !experience_to_update.destroyed?
                experience_to_update.update(official_transcript_required: hash["official_transcript_required"])
            end
        end
    end

end