# == Schema Information
#
# Table name: awards_and_interests_options
#
#  id         :uuid             not null, primary key
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  text       :text             not null
#  locale     :text             default("en"), not null
#  suggest    :boolean          default(FALSE)
#

class CareerProfile::AwardsAndInterestsOption < ActiveRecord::Base

    include AutoSuggestOptionMixin

    self.table_name = 'awards_and_interests_options'
    has_and_belongs_to_many :career_profiles

    def type
        'awards_and_interest'
    end

end
