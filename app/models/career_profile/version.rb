# == Schema Information
#
# Table name: career_profiles_versions
#
#  version_id                                           :uuid             not null, primary key
#  operation                                            :string(1)        not null
#  version_created_at                                   :datetime         not null
#  id                                                   :uuid             not null
#  created_at                                           :datetime
#  updated_at                                           :datetime
#  user_id                                              :uuid
#  score_on_gmat                                        :text
#  score_on_sat                                         :text
#  score_on_act                                         :text
#  short_answers                                        :json
#  willing_to_relocate                                  :boolean
#  open_to_remote_work                                  :boolean
#  authorized_to_work_in_us                             :text
#  top_mba_subjects                                     :text             is an Array
#  top_personal_descriptors                             :text             is an Array
#  top_motivations                                      :text             is an Array
#  top_workplace_strengths                              :text             is an Array
#  bio                                                  :text
#  personal_fact                                        :text
#  preferred_company_culture_descriptors                :text             is an Array
#  li_profile_url                                       :text
#  resume_id                                            :uuid
#  personal_website_url                                 :text
#  blog_url                                             :text
#  tw_profile_url                                       :text
#  fb_profile_url                                       :text
#  job_sectors_of_interest                              :text             is an Array
#  employment_types_of_interest                         :text             is an Array
#  company_sizes_of_interest                            :text             is an Array
#  primary_areas_of_interest                            :text             is an Array
#  place_id                                             :text
#  place_details                                        :json
#  last_calculated_complete_percentage                  :float
#  sat_max_score                                        :text
#  score_on_gre_verbal                                  :text
#  score_on_gre_quantitative                            :text
#  score_on_gre_analytical                              :text
#  short_answer_greatest_achievement                    :text
#  primary_reason_for_applying                          :text
#  do_not_create_relationships                          :boolean
#  salary_range                                         :text
#  short_answer_leadership_challenge                    :text
#  interested_in_joining_new_company                    :text
#  locations_of_interest                                :text             is an Array
#  profile_feedback                                     :text
#  feedback_last_sent_at                                :datetime
#  location                                             :geography(Point,
#  github_profile_url                                   :text
#  long_term_goal                                       :text
#  last_confirmed_at_by_student                         :datetime
#  last_confirmed_at_by_internal                        :datetime
#  last_updated_at_by_student                           :datetime
#  last_confirmed_internally_by                         :string
#  short_answer_scholarship                             :string
#  consider_early_decision                              :boolean
#  student_network_looking_for                          :text             is an Array
#  native_english_speaker                               :boolean
#  earned_accredited_degree_in_english                  :boolean
#  survey_years_full_time_experience                    :text
#  survey_most_recent_role_description                  :text
#  survey_highest_level_completed_education_description :text
#  version_editor_id                                    :uuid
#  version_editor_name                                  :text
#  short_answer_why_pursuing                            :text
#  short_answer_use_skills_to_advance                   :text
#  short_answer_ask_professional_advice                 :text
#  has_no_formal_education                              :boolean
#  sufficient_english_work_experience                   :boolean
#  english_work_experience_description                  :text
#

class CareerProfile::Version < CareerProfile
    include VersionMixin

    self.primary_key = 'version_id'
    self.table_name = 'career_profiles_versions'

    belongs_to :career_profile, :foreign_key => 'id', :primary_key => 'id'
end
