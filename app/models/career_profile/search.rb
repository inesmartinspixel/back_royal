# == Schema Information
#
# Table name: career_profile_searches
#
#  id                                  :uuid             not null, primary key
#  created_at                          :datetime         not null
#  updated_at                          :datetime         not null
#  user_id                             :uuid             not null
#  only_local                          :boolean          default(FALSE), not null
#  years_experience                    :text             default([]), not null, is an Array
#  last_search_at                      :datetime
#  search_count                        :integer
#  keyword_search                      :text
#  preferred_primary_areas_of_interest :text             default([]), not null, is an Array
#  industries                          :text             default([]), not null, is an Array
#  skills                              :text             default([]), not null, is an Array
#  employment_types_of_interest        :text             default([]), not null, is an Array
#  school_name                         :text
#  in_school                           :boolean
#  company_name                        :text
#  levels_of_interest                  :text             default([]), not null, is an Array
#  places                              :json             not null, is an Array
#

class CareerProfile::Search < ActiveRecord::Base

    self.table_name = "career_profile_searches"

    # see comment in save_search about why this is ignored
    self.ignored_columns = %w(primary_areas_of_interest locations)

    def self.save_search(user_id, filters)
        filters = filters.clone
        filters[:only_local] ||= false

        # 1. this crazy `A <@ B and B <@ A` and matcher is a way to do
        #   an order-insensitive array match
        # 2. The filters we pass in to CareerProfilesController also include
        #   primary_areas_of_interest, work_experience_roles, and preferred_work_experience_roles.  Those
        #   filters, though, can be derived from preferred_primary_areas_of_interest, so there is no need
        #   to store them here separately. If you want to see what someone was actually searching for,
        #   you just need to look at preferred_primary_areas_of_interest (which should probably be called preferred_role)

        preferred_primary_areas_of_interest = filters[:roles] ? filters[:roles][:preferred_primary_areas_of_interest] : nil

        existing_search = self.where(user_id: user_id)
                            .where(case_insensitive_match_sql('places', filters[:places].map(&:to_json)))
                            .where(case_insensitive_match_sql('industries', filters[:industries]))
                            .where(case_insensitive_match_sql('preferred_primary_areas_of_interest', preferred_primary_areas_of_interest))
                            .where(case_insensitive_match_sql('employment_types_of_interest', filters[:employment_types_of_interest]))
                            .where(case_insensitive_match_sql('levels_of_interest', filters[:levels_of_interest]))
                            .where(case_insensitive_match_sql('years_experience', filters[:years_experience]))
                            .where(case_insensitive_match_sql('skills', filters[:skills]))
                            .where(only_local: filters[:only_local])
                            .where(in_school: filters[:in_school])
                            .where(keyword_search: filters[:keyword_search])
                            .where(school_name: filters[:school_name])
                            .where(company_name: filters[:company_name])
                            .first

        search = existing_search || new(
            places: filters[:places] || [],
            industries: filters[:industries] || [],
            only_local: filters[:only_local],
            in_school: filters[:in_school],
            keyword_search: filters[:keyword_search],
            school_name: filters[:school_name],
            company_name: filters[:company_name],
            preferred_primary_areas_of_interest: preferred_primary_areas_of_interest || [],
            employment_types_of_interest: filters[:employment_types_of_interest] || [],
            levels_of_interest: filters[:levels_of_interest] || [],
            years_experience: filters[:years_experience] || [],
            skills: filters[:skills] || [],
            search_count: 0,
            user_id: user_id
        )

        search.last_search_at = Time.now
        search.search_count += 1
        search.save!
        search
    end

    def self.case_insensitive_match_sql(prop, arr)
        # this crazy `A <@ B and A @> B` and matcher is a way to do
        # an order-insensitive array match
        "#{prop}::text[] <@ #{array_to_sql(arr)} and #{prop}::text[] @> #{array_to_sql(arr)}"
    end

    def self.array_to_sql(arr)
        if arr.blank?
            "Array[]::text[]"
        else

            # ActiveRecord::Base.connection.quote( should help prevent against
            # sql injection attacks
            quoted = arr.map { |e| ActiveRecord::Base.connection.quote(e) }
            "Array[#{quoted.join(',')}]"
        end
    end

end

