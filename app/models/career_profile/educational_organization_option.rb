# == Schema Information
#
# Table name: educational_organization_options
#
#  id               :uuid             not null, primary key
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  text             :text             not null
#  locale           :text             default("en"), not null
#  suggest          :boolean          default(FALSE)
#  source_id        :text
#  image_url        :text
#  degree_blacklist :boolean          default(FALSE), not null
#

class CareerProfile::EducationalOrganizationOption < ActiveRecord::Base

    include AutoSuggestOptionMixin

    self.table_name = 'educational_organization_options'
    has_one :education_experience

    before_save :set_degree_blacklist

    BLACKLISTED_INSTITUTIONS = Set.new([
        'University of Kabul',
        'Odessa National Academy of Telecommunication',
        'University of Baghdad',
        'Jawaharlal Nehru Technological University, Kakinada',
        'Mahatma Gandhi University',
        'University of Calicut',
        'ELTE Savaria University',
        'Szechenyi Istvan University',
        'University of Visveswaraya College of Engineering',
        'Visvesvaraya Technological University (VTU)',
        'Arturo Michelena University',
        'Jose Antonio Paez University',
        'Moscow State University',
        'Moscow Finance and Law Academy',
        'Nanterre University',
        'Mangaolore University',
        'American University of Beirut',
        'Manchester Business School',
        'Osmania University',
        'Karnatak University',
        'Uttar Pradhesh Technical University, Lucknow',
        'University of Mumbai',
        'Institute National des Sciences Appliquées (INSA Lyon)',
        'University of Bucharest',
        'Shanghai University',
        'Communication University of China',
        'JNTUH College of Engineering Sultanpur',
        'Birla Institute of Technology and Science, Pilani',
        'Politehnica University of Bucharest',
        'Koç University',
        'University of Salzburg',
        'Paris Lodron University of Salzburg',
        'University of Évry Val d\'Essonne',
        'Jordan University of Science and Technology',
        'ThePowerMBA',
        'WorldQuant University',
        'Suez Canal University',
        'Universite Paris XIII',
        'Universidad Politécnica de Madrid (UPM)',
        'Télécom ParisTech',
        'University of Pune',
        'CDI College'
    ])

    def type
        'educational_organization'
    end

    def set_degree_blacklist
        if BLACKLISTED_INSTITUTIONS.include?(self.text)
            self.degree_blacklist = true
        end
    end

end
