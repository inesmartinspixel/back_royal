module CareerProfile::JsonHelper

    extend ActiveSupport::Concern

    included do

        cattr_accessor :json_field_collections

        # Each call to include_in_json adds fields to the
        # json response for different situations. The
        # three arguments are:
        #
        # 1. collection_names - names for the lists that we want to add fields to
        # 2. simple fields - fields that are just the names of properties on the career profile
        # 3. complex fields - fields that need some logic for how to come up with value

        # The calls to include_in_json here start with the fields
        # that are the most available and end with those that are
        # the most restricted

        ##############################################################################################################################

        # Include fields that are visible to all viewers
        # of a career profile
        include_in_json([
                :editable,
                :career_network_open_attrs,
                :student_network_open_attrs
            ],

            %w(
                id
                user_id
                personal_fact
                place_id
                place_details
            ),

            {
                created_at: -> { created_at.to_timestamp },
                updated_at: -> { updated_at.to_timestamp },
                education_experiences: -> { education_experiences.sort_by(&:graduation_year).reverse.as_json },

                # User fields
                city: -> { user.city },
                state: -> { user.state },
                country: -> { user.country },
                pref_student_network_privacy: -> { user.pref_student_network_privacy },

                # needed on the learner or careers cards
                target_graduation: -> { user.accepted_application ? user.accepted_application.published_cohort.end_date.to_datetime.to_time.to_timestamp * 1000 : nil },
                candidate_cohort_name: -> { user.accepted_application ? user.accepted_application.published_cohort.name : nil },
                candidate_cohort_title: -> { user.accepted_application ? user.accepted_application.published_cohort.title : nil }
            }
        )

        # Include fields that show up differently when viewing
        # an anonymized profile than they do more generally
        include_in_json([
                :career_network_open_attrs,
                :student_network_open_attrs
            ], {
                name: -> { anonymized_name },
                nickname: -> { anonymized_name }
            }
        )

        # Include fields that show up differently when viewing
        # an anonymized profile in the student network than they do more generally
        include_in_json([
                :student_network_open_attrs
            ], {
                work_experiences: -> { work_experiences.sort_by(&:sort_key).slice(0, 1).as_json }
            }
        )

        # Include fields that are only included in
        # the student network, but not available to
        # hiring managers
        include_in_json([
                :editable,
                :student_network_open_attrs
            ], %w(
                student_network_interests
                student_network_looking_for
            )
        )

        # Include fields that are only available
        # to hiring managers, but not available
        # in the student network
        include_in_json([
                :editable,
                :career_network_open_attrs
            ], %w(
                score_on_gmat
                score_on_sat
                score_on_act
                willing_to_relocate
                open_to_remote_work
                authorized_to_work_in_us
                top_mba_subjects
                top_personal_descriptors
                top_motivations
                top_workplace_strengths
                bio
                preferred_company_culture_descriptors
                job_sectors_of_interest
                employment_types_of_interest
                company_sizes_of_interest
                primary_areas_of_interest
                salary_range
                interested_in_joining_new_company
                locations_of_interest
                awards_and_interests
                skills
                sat_max_score
                score_on_gre_verbal
                score_on_gre_quantitative
                score_on_gre_analytical
            ), {
                work_experiences: -> { work_experiences.sort_by(&:sort_key).as_json },
            }
        )

        # Include fields that are available in the career network
        # and the student network, but only when the viewer has
        # full access to the profile
        include_in_json([
                :editable,
                :career_network_restricted,
                :student_network_restricted
            ],

            %w(
                personal_website_url
                blog_url
                tw_profile_url
                li_profile_url
                github_profile_url
            ),

            {
                name: -> { user.name },
                nickname: -> { user.nickname },
                avatar_url: -> { user.avatar_url }
            }
        )

        # Include fields that are only available to hiring managers
        # that have full access to the profile
        include_in_json([
                :editable,
                :career_network_restricted
            ], %w(
                resume
            )
        )

        # Include fields that are available in student network,
        # but only when the viewer has full access to the profile
        include_in_json([
                :student_network_restricted
            ], {
                work_experiences: -> { work_experiences.sort_by(&:sort_key).as_json }
            }
        )

        # Include fields that are only available to admins or users
        # who have access to edit the profile/application.
        include_in_json([
                :editable
            ], %w(
                short_answers
                fb_profile_url
                last_calculated_complete_percentage
                short_answer_greatest_achievement
                has_no_formal_education
                primary_reason_for_applying
                do_not_create_relationships
                short_answer_leadership_challenge
                short_answer_why_pursuing
                short_answer_use_skills_to_advance
                short_answer_ask_professional_advice
                profile_feedback
                long_term_goal
                last_confirmed_internally_by
                short_answer_scholarship
                consider_early_decision
                resume_id
                peer_recommendations
                native_english_speaker
                earned_accredited_degree_in_english
                survey_highest_level_completed_education_description
                survey_years_full_time_experience
                survey_most_recent_role_description
                sufficient_english_work_experience
                english_work_experience_description
            ),

            {
                phone: -> { user.phone },
                sex: -> { user.sex },
                ethnicity: -> { user.ethnicity },
                race: -> { user.race },
                how_did_you_hear_about_us: -> { user.how_did_you_hear_about_us },
                address_line_1: -> { user.address_line_1 },
                address_line_2: -> { user.address_line_2 },
                zip: -> { user.zip },
                birthdate: -> { user.birthdate },
                anything_else_to_tell_us: -> { user.anything_else_to_tell_us },
                student_network_email: -> { user.student_network_email },
                feedback_last_sent_at: -> { feedback_last_sent_at.to_timestamp },
                last_confirmed_at_by_student: -> { last_confirmed_at_by_student.to_timestamp },
                last_confirmed_at_by_internal: -> { last_confirmed_at_by_internal.to_timestamp },
                last_updated_at_by_student: -> { last_confirmed_at_by_student.to_timestamp },

                # The following cohort-related things are used by admin-cohort-slack-assignments
                cohort_status: -> { user.last_application&.status },
                cohort_slack_room_id: -> { user.last_application&.cohort_slack_room_id },
                last_application_id: -> { user.last_application&.id },
                has_full_scholarship: -> { user.last_application&.has_full_scholarship? },

                # This attribute is in editable for performance rather than permission reasons.
                # See user.program_type, which will issue a query if the user is deferred, expelled, etc.
                # since it has to check the institutions count.
                program_type: -> { user.program_type_confirmed ? user.program_type : nil }
            }
        )

        ######################################################################################################################################
    end

    module ClassMethods

        # we accept two fields arguments here so that the first can
        # be just an array of strings and the second can be a hash
        # with definitions for fields that need special handling
        def include_in_json(collection_names, fields, more_fields = [])

            self.json_field_collections ||= Hash.new { |hash, collection_name| hash[collection_name] = {} }

            [fields, more_fields].each do |_fields|
                if _fields.is_a?(Array)
                    _fields.each do |field|
                        add_field_to_collections(collection_names, field, -> { send(field).as_json } )
                    end
                elsif _fields.is_a?(Hash)
                    _fields.each do |field, definition|
                        add_field_to_collections(collection_names, field, definition )
                    end
                end
            end


        end

        def add_field_to_collections(collection_names, field, definition)
            field = field.to_s
            collection_names.each do |collection_name|
                collection = self.json_field_collections[collection_name]
                if collection.key?(field)
                    raise RuntimeError.new("#{field} cannot be included twice in #{collection_name}")
                end
                collection[field] = definition
            end
        end

    end

    def json_attrs(collection_name)
        collection = self.class.json_field_collections[collection_name]
        attrs = {}
        collection.each do |field, definition|
            attrs[field] = instance_exec(&definition)
        end
        attrs
    end

    def anonymized_name
        user.name.split(' ').map {|n| n[0] + '. ' }.join('').strip
    end

    def as_json_for_edit(options)
        attrs = json_attrs(:editable)

        attrs
    end

    def as_json_for_student_network(options = {})
        attrs = json_attrs(:student_network_open_attrs)

        # By default, we anonymize.  If anonymize is set explicitly to false,
        # the we check the preference on this particular profile and
        # only return a non-anonymized profile if the user for this
        # profile has allowed it
        if !options.key?(:anonymize)
            begin
                raise "as_json_for_student_network called without explicit anonymize"
            rescue Exception => err
                Raven.capture_exception(err)
                options[:anonymize] = true
            end
        end

        anonymize = options[:anonymize] != false

        if !anonymize && ['full'].include?(user.pref_student_network_privacy)
            attrs.merge!(json_attrs(:student_network_restricted))
            attrs['anonymized'] = false
        else
            attrs['anonymized'] = true
        end

        attrs
    end

    def as_json_for_career_network(options = {})

        attrs = json_attrs(:career_network_open_attrs)

        # By default, we anonymize.  If anonymize is set explicitly to false,
        # we show a non-anonymized profile
        if !options.key?(:anonymize)
            begin
                raise "as_json_for_career_network called without explicit anonymize"
            rescue Exception => err
                Raven.capture_exception(err)
                options[:anonymize] = true
            end
        end

        anonymize = options[:anonymize] != false

        if !anonymize
            attrs.merge!(json_attrs(:career_network_restricted))
            attrs['anonymized'] = false
        else
            attrs['anonymized'] = true
        end

        attrs
    end

end