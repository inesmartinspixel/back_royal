# == Schema Information
#
# Table name: career_profiles_awards_and_interests_options
#
#  id                             :uuid             not null, primary key
#  career_profile_id              :uuid             not null
#  awards_and_interests_option_id :uuid             not null
#  position                       :integer          not null
#

class CareerProfile::CareerProfilesAwardsAndInterestsOptionsJoin < ActiveRecord::Base
    self.table_name = 'career_profiles_awards_and_interests_options'
    default_scope -> {order(:position)}
    belongs_to :career_profile
    belongs_to :awards_and_interests_option, class_name: 'AwardsAndInterestsOption'
    acts_as_list :scope => :career_profile
end
