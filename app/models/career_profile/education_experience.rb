# == Schema Information
#
# Table name: education_experiences
#
#  id                                    :uuid             not null, primary key
#  created_at                            :datetime
#  updated_at                            :datetime
#  graduation_year                       :integer          not null
#  degree                                :text
#  major                                 :text             not null
#  minor                                 :text
#  gpa                                   :text
#  career_profile_id                     :uuid             not null
#  educational_organization_option_id    :uuid             not null
#  gpa_description                       :text
#  degree_program                        :boolean          default(TRUE), not null
#  will_not_complete                     :boolean
#  transcript_waiver                     :text
#  transcript_approved                   :boolean          default(FALSE), not null
#  official_transcript_required          :boolean          default(FALSE), not null
#  official_transcript_required_override :boolean          default(FALSE), not null
#

class CareerProfile::EducationExperience < ApplicationRecord

    default_scope -> { order(graduation_year: :desc) }
    self.table_name = 'education_experiences'

    belongs_to :educational_organization, class_name: 'CareerProfile::EducationalOrganizationOption', foreign_key: :educational_organization_option_id, primary_key: :id, optional: true
    belongs_to :career_profile, inverse_of: :education_experiences

    # NOTE: We are not using `dependent: destroy` here because I don't want a situation where
    # a user gets accepted with transcripts, deletes their experiences in the Careers section,
    # then we lose those transcripts.
    has_many :transcripts, class_name: 'S3TranscriptAsset', foreign_key: :education_experience_id

    validates_inclusion_of :will_not_complete, :in => [true, false], :if => :degree_program
    validates :transcript_waiver, length: { minimum: 1, allow_nil: true, message: "can't be an empty string" }
    validate :not_waived_with_transcript
    validate :transcript_waived_or_uploaded_if_approved

    after_save :update_career_profile_fulltext, if: :should_update_career_profile_fulltext?
    after_save :identify_user, if: :should_identify_user?
    after_destroy :update_career_profile_fulltext

    # These are in different lifecyle hooks because we only want to
    # determine_transcript_requirements when absolutely necessary on
    # create/update, but always have to run it on destroy.
    before_save :determine_transcript_requirements, if: :should_determine_transcript_requirements?
    after_destroy :determine_transcript_requirements

    def self.includes_needed_for_json_eager_loading
        [:educational_organization, :transcripts]
    end

    def self.update_from_hash!(hash, meta = {})
        RetriableTransaction.transaction do
            hash = hash.unsafe_with_indifferent_access

            # The determine_transcript_requirements callback requires that this identical object (not a copy)
            # be in the list of career_profile.education_experiences.  In order to ensure that this is the case,
            # we load up the career profile first and grab the education experience from there.
            cp = CareerProfile.joins(:education_experiences).where(education_experiences: { id: hash['id'] }).first
            instance = cp.education_experiences.detect { |ee| ee.id == hash['id'] }

            if instance.nil?
                raise ActiveRecord::RecordNotFound.new("Couldn't find #{self.name} with id=#{hash[:id]}")
            end

            instance.merge_hash(hash, meta)
            instance.save!
            instance
        end
    end

    def merge_hash(hash, meta = {})
        meta ||= {}
        hash = hash.unsafe_with_indifferent_access

        if meta[:is_admin]
            keys = %w(transcript_waiver transcript_approved official_transcript_required_override)
        else
            raise UnauthorizedError
        end

        keys.each do |key|
            if hash.key?(key)
                val =  hash[key]
                val = nil if val.is_a?(String) && val.blank?
                self[key] = val
            end
        end

        self
    end

    def not_waived_with_transcript
        if transcript_waiver.present? && self.transcripts.any?
            errors.add(:transcript_waiver, "can't be waived and have a transcript")
        end
    end

    def transcript_waived_or_uploaded_if_approved
        if transcript_approved && !(transcript_waiver.present? || self.transcripts.any?)
            errors.add(:transcript_approved, "can't be approved if there's no transcript")
        end
    end

    def update_career_profile_fulltext
        career_profile&.update_fulltext
    end


    def should_update_career_profile_fulltext?
        !!(
            saved_changes[:educational_organization_option_id] ||
            saved_changes[:major] ||
            saved_changes[:minor]
        )
    end

    def identify_user
        self.career_profile.user.identify
    end

    def should_identify_user?
        !!(
            saved_changes[:transcript_waiver] ||
            saved_changes[:transcript_approved]
        )
    end

    # See also education_experience.js
    def transcript_required?
        self.degree_program && !self.will_not_complete && self.graduation_year <= Date.current.year
    end

    # See also education_experience.js
    def transcript_approved_or_waived?
        !!self.transcript_approved || self.transcript_waiver.present?
    end

    # See also education_experience.js
    def transcript_uploaded_or_waived?
        self.transcript_waiver.present? || self.transcripts.any?
    end

    def as_json(options = {})
        json = attributes.as_json.merge({
            "educational_organization" => educational_organization.as_json,
            "transcripts" => transcripts.as_json
        })

        json
    end
    
    def determine_transcript_requirements
        # We call this in both a before_save and after_destroy. If this was 
        # called from the after_destroy, we don't want to pass the experience
        # on to CareerProfile::TranscriptRequirementHelper
        if self.destroyed?
            CareerProfile::TranscriptRequirementHelper.determine_transcript_requirements(self.career_profile)
        else
            CareerProfile::TranscriptRequirementHelper.determine_transcript_requirements(self.career_profile, self)
        end        
    end

    def has_changes_that_could_affect_transcript_requirements?
        columns_impacting_transcript_requirements = %w(
            degree
            educational_organization_option_id
            graduation_year will_not_complete
            official_transcript_required_override
        )
        (changes.keys & columns_impacting_transcript_requirements).any?
    end

    # Due to the nature of how we process CareerProfile::EducationExperience changes
    # paired with the fact that the order in which this is called could and does matter,
    # we need to make sure this only gets run on one of the education experiences on the
    # career profile. To do this, we ensure that `self` is an actual object in the
    # assocation, then ensure that `self` is the _last_ object in the association that
    # has_changes_that_could_affect_transcript_requirements?.
    def should_determine_transcript_requirements?
        if career_profile.education_experiences.map(&:object_id).include?(self.object_id)
            return career_profile.education_experiences.select(&:has_changes_that_could_affect_transcript_requirements?).last == self
        end

        if self.has_changes_that_could_affect_transcript_requirements?
            Raven.capture_in_production(RuntimeError,
                extra: {
                    career_profile_id: self.career_profile.id,
                    education_experience_id: self.id
                }
            ) do
                raise RuntimeError.new("Cannot determine transcript requirements because education experience is not in association")
            end
        end

        return false
    end

end
