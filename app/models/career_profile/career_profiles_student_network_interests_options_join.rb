# == Schema Information
#
# Table name: career_profiles_student_network_interests_options
#
#  id                                  :uuid             not null, primary key
#  career_profile_id                   :uuid             not null
#  student_network_interests_option_id :uuid             not null
#  position                            :integer          not null
#

class CareerProfile::CareerProfilesStudentNetworkInterestsOptionsJoin < ActiveRecord::Base
    self.table_name = 'career_profiles_student_network_interests_options'
    default_scope -> {order(:position)}
    belongs_to :career_profile
    belongs_to :student_network_interests_option, class_name: 'StudentNetworkInterestsOption'
    acts_as_list :scope => :career_profile
end
