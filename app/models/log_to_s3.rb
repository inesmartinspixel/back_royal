module LogToS3

    def self.bucket_name
        ENV['AWS_LOGGING_BUCKET']
    end

    def self.region
        ENV['AWS_REGION']
    end

    def self.log(path, file_name_suffix, message = '')
        s3 = Aws::S3::Resource.new
        s3.bucket(bucket_name).object("#{path}/#{Time.now.strftime('%Y-%m-%d_%H-%M-%S-%12N')} - #{file_name_suffix}").put(body: message)
    end

end
