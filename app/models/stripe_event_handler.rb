# Handles inoming stripe webhook events
# SEE ALSO: https://www.masteringmodernpayments.com/stripe-webhook-event-cheatsheet
require 'back_royal/string'

class StripeEventHandler

    class EventAlreadyProcessed < RuntimeError; end

    class << self
        attr_accessor :handlers

        def handle(*args, &block)

            @handlers ||= Hash.new { |h, k| h[k] = [] }
            order = args.last.is_a?(Numeric) ? args.pop : 0

            event_names = args
            event_names.each do |event_name|
                @handlers[event_name] << {
                    order: order,
                    block: block
                }
            end
        end
    end

    class Formatter
        include ActionView::Helpers::NumberHelper
    end

    attr_reader :event

    def self.get_uuid(str)
        # we need to generate an id that has the following properties:
        # 1. if we get the same event from stripe twice, we generate the same id both times
        # 2. we can use it as an id in postgres
        Digest::MD5.new.update(str).hexdigest.dasherize_md5_hash
    end

    def self.get_customer_info(event)
        if event.data.object.respond_to?(:object) && event.data.object.object == "customer"
            customer = event.data.object
            {customer: customer, customer_id:  customer.id}
        elsif event.data.object.respond_to?(:customer)
            {customer: nil, customer_id: event.data.object.customer}
        elsif event.data.object.respond_to?(:charge)
            {customer: nil, customer_id: Stripe::Charge.retrieve(event.data.object.charge).customer}
        end
    end

    def initialize(event)
        @event = event

        # this will log to sentry in livemode if
        # no owner is found
        return unless owner

        with_raven_context(event) do
            handlers = []
            ['all', event.type].each do |key|
                self.class.handlers[key].each do |entry|
                    handlers << entry
                end
            end

            begin
                EditorTracking.transaction("StripeEventHandler") do
                    handlers.sort_by { |entry| entry[:order] }.each do |entry|
                        instance_exec event, &entry[:block]
                    end
                end
            rescue EventAlreadyProcessed
            end

        end

    end

    def with_raven_context(event, &block)
        if user_or_hiring_team_owner.present?
            Raven.user_context(
                'id' => user_or_hiring_team_owner.id,
                'email' => user_or_hiring_team_owner.email
            )
        elsif customer_id.present?
            Raven.user_context(
                'id' => customer_id
            )
        end

        Raven.extra_context(
            event_type: event.type,
            event_id: event.id,
            event_link: "https://dashboard.stripe.com/events/#{event.id}"
        )

        yield

        # NOTE: Maybe we should be calling Raven.set_user_context({}) here, but it is hard
        # to call it AFTER the exception is logged to raven.  In any case, it does not seem
        # to matter.  It seems like the context is being cleared, possible by the call
        # to Context.clear! inside of raven.  In any case, the worst that can happen is that
        # this id gets attached to a different event.
    end

    def user_or_hiring_team_owner
        unless defined?(@user_or_hiring_team_owner)
            if owner.is_a?(HiringTeam)
                hiring_team = owner
                @user_or_hiring_team_owner = hiring_team.owner
            elsif owner.is_a?(User)
                @user_or_hiring_team_owner = owner
            end
        end
        @user_or_hiring_team_owner
    end

    def get_uuid(user_id)
        # For a long time, we only ever logged each event to a single user. Once we
        # started logging for multiple users, it became necessary to generate user-specific
        # ids here.  But, since we didn't want events before the rollout to be different
        # from events after the rollout, we ignore the user_id if it is the user_or_hiring_team_owner
        if user_id == user_or_hiring_team_owner.id
            str = event.id
        else
            str =  "#{event.id}-#{user_id}"
        end
        self.class.get_uuid(str)
    end

    def set_customer
        info = StripeEventHandler.get_customer_info(event)
        @customer = info[:customer]
        @customer_id = info[:customer_id]
    end

    def customer_id
        if !defined?(@customer_id)
            set_customer
        end
        @customer_id
    end

    def customer
        if !defined?(@customer)
            set_customer
        end
        @customer
    end

    def owner
        if !defined?(@owner)
            if customer_id
                @owner = Subscription.find_owner_by_customer_id(customer_id)
                if @owner.nil? and event.livemode
                    Raven.capture_exception("No owner found for customer #{customer_id}")
                end
                if @owner
                    @owner.stripe_customer = customer if customer
                end
            else
                @owner = nil
            end
        end
        @owner
    end

    def previous_attributes
        event.data.respond_to?(:previous_attributes) ? event.data.previous_attributes : {}
    end

    def stripe_url_for_event(event)
        url_root = event.livemode ? 'https://dashboard.stripe.com' : 'https://dashboard.stripe.com/test'
        url = nil

        if ['invoice.payment_failed', 'invoice.payment_succeeded'].include?(event.type)
            charge_id = event.data.object.charge
        elsif ['charge.failed', 'charge.refunded'].include?(event.type)
            charge_id = event.data.object[:id]
        end

        if charge_id
            url = "#{url_root}/payments/#{charge_id}"
        end

        url
    end

    def invoice
        unless defined?(@invoice)
            if ['invoice.payment_failed', 'invoice.payment_succeeded', 'invoice.created', 'invoice.upcoming'].include?(event.type)
                @invoice = event.data.object
            elsif event.type.starts_with?('charge') && charge&.invoice
                begin
                    @invoice = Stripe::Invoice.retrieve(charge.invoice)
                rescue Stripe::InvalidRequestError => err
                    raise err unless err.message.match(/No such invoice/)
                    @invoice = nil # this can occur now if charges fail without a subscription being created
                end
            else
                @invoice = nil
            end
        end
        @invoice
    end

    def dispute
        unless defined?(@dispute)
            if event.type.starts_with?('charge.dispute')
                @dispute = event.data.object
            else
                @dispute = nil
            end
        end
        @dispute
    end

    def charge
        unless defined?(@charge)
            if ['charge.failed', 'charge.refunded', 'charge.succeeded'].include?(event.type)
                @charge = event.data.object
            elsif event.type.starts_with?('charge.dispute') && dispute&.charge
                @charge = Stripe::Charge.retrieve(dispute.charge)
            elsif event.type.starts_with?('invoice') && invoice&.charge
                @charge = Stripe::Charge.retrieve(invoice.charge)
            elsif event.type.starts_with?('invoice') && invoice.charge.nil? && owner.default_stripe_card.present? && invoice.amount_paid != 0
                # As far as we know, the only way to get an invoice without a charge
                # is if the user has no card.  If we get an invoice with no charge
                # and the user has a card, notify us.
                # For now, we're just using the charge for slack logging, so it's not such a big deal
                Raven.capture_exception('No charge on invoice event', {
                    extra: {
                        invoice_id: invoice.id
                    }
                })
                @charge = nil
            else
                @charge = nil
            end
        end
        @charge
    end

    def stripe_subscription_id
        unless defined?(@stripe_subscription_id)
            if invoice.present?
                # figure out the subscription id. in most situations, this is a high-level property on the invoice itself. however, if the
                # subscription was manually billed against the subscription via an ad-hoc invoice in Stripe Dashboard, then it's model includes
                # a single-element `lines.data` (line-items) entry where the subscription can be found. it will remain null on the top-level prop.
                @stripe_subscription_id = invoice.subscription || (invoice.lines && invoice.lines.data[0].subscription)
            elsif event.data.object.object == 'subscription'
                @stripe_subscription_id = stripe_subscription.id
            end
        end

        @stripe_subscription_id
    end

    def stripe_subscription
        unless defined?(@stripe_subscription)
            if event.data.object.object == 'subscription'
                @stripe_subscription = event.data.object
            elsif stripe_subscription_id
                @stripe_subscription = Subscription.find_stripe_subscription(owner, stripe_subscription_id)
            end
        end
        @stripe_subscription
    end

    def is_incomplete_stripe_subscription?
        !!stripe_subscription&.status&.starts_with?('incomplete')
    end


    def subscription
        unless defined?(@subscription)

            # We've seen network-related race conditions where this `customer.subscription.updated` event finishes reponding
            # AFTER the following `customer.subscription.deleted event` that queues later in sequence when cancelling subscriptions
            # in our admin UI. If we see there's a cancellation reason, then there's no practical reason for reconciling.
            #
            # This is probably good enough in practice, but it is at least theoretically possible to generate this same problem
            # without triggering this special exception.  For more information, read all the comments on https://trello.com/c/MFdZTJ0L
            if event.type == 'customer.subscription.updated' && event.data.object.metadata['cancellation_reason']
                @subscription = nil

            elsif event.type == 'customer.subscription.deleted'
                @subscription = Subscription.find_by_stripe_subscription_id(stripe_subscription_id)

            # If the subscription has been deleted in stripe, we do not want to create one.  So,
            # we make a call to `find_stripe_subscription` to check if it has been deleted.
            # See https://trello.com/c/GEyrJF2O
            elsif stripe_subscription && Subscription.find_stripe_subscription(owner, stripe_subscription_id).present?
                @subscription = Subscription.create_or_reconcile_from_stripe_subscription!(owner, stripe_subscription)
            else
                @subscription = nil
            end
        end
        @subscription
    end


    def delete_incomplete_stripe_subscription
        return false unless is_incomplete_stripe_subscription?
        begin
            stripe_subscription.delete
        rescue Stripe::InvalidRequestError => err
            raise err unless err.message.match(/No such subscription/)
        end
        return true
    end

    def format_currency(amount)
        Formatter.new.number_to_currency(amount / 100.0, locale: :en)
    end

    def log_to_slack(message, color)

        # determine slack channel based on owner type
        slack_channel = owner&.subscription_slack_channel || Slack.engineering_alerts_channel

        # determine identifier based on whether we have an active subscription, a reference to a
        # stripe subscription, finally falling back on UNKNOWN
        identifier = owner&.subscription_identifier_for_slack(subscription, stripe_subscription) || "UNKNOWN"

        stripe_url = stripe_url_for_event(event)
        LogToSlack.perform_later(slack_channel, nil,
            [
                {
                    color: color,
                    text: "#{message} [#{identifier}]\n#{stripe_url}",
                    mrkdwn_in: ['text']
                }
            ])
    end

    def log_charge_failure(charge, amount)
        if charge.nil? && owner.default_stripe_card.nil?
            failure_reason = 'User has no default card'
        elsif charge&.outcome&.reason || charge&.outcome&.seller_message
            failure_reason = charge&.outcome&.seller_message || ""
            failure_reason += charge&.outcome&.reason ? " (#{charge.outcome.reason})" : ""
        elsif charge&.failure_message || charge&.failure_code
            failure_reason = charge.failure_message || ""
            failure_reason += charge.failure_code ? " (#{charge.failure_code})" : ""
        else
            failure_reason = "No reason provided"
        end

        log_to_slack(
            "A payment of *#{format_currency(amount)}* from *#{owner.email}* was declined with reason - #{failure_reason}",
            '#F8000C')
    end

    def partial_refund?(refund, charge)
        refund.amount < charge.amount
    end

    # set a high priority to make sure that this happens after
    # @subscription is set by other handlers
    handle 'all', 9000 do |event|

        hash = event.as_json.merge({
            'stripe_event': true
        })

        if owner.is_a?(HiringTeam)
            # is_hiring_team_owner can be set to false below in open_position.event_attributes when we
            # send an extra event to the open position creator
            hash['is_hiring_team_owner'] = true
            hash['hiring_plan'] = owner.hiring_plan
        end

        if event.type == 'charge.succeeded'
            hash['revenue'] = event.data.object.amount.to_f / 100
        elsif event.type == 'charge.refunded'
            # Make sure that we look at the amount for the refund and not the amount_refunded
            # on the charge, which is the total amount refunded for the charge to date.
            hash['revenue'] = -event.data.object.refunds.data.first.amount.to_f / 100
        end

        if event.type.starts_with?('charge') && !event.type.starts_with?('charge.dispute')
            hash['currency'] = event.data.object.currency

            # these next 4 keys were added around 1/17/2019
            hash['captured'] = event.data.object.captured
            hash['charge_id'] = event.data.object.id
            hash['failure_code'] = event.data.object.failure_code
            hash['outcome'] = event.data.object.outcome

        end

        if event.type == 'customer.subscription.updated'
            if previous_attributes.respond_to?('cancel_at_period_end') && previous_attributes.cancel_at_period_end === false && stripe_subscription.cancel_at_period_end === true
                hash['set_to_cancel'] = true
                hash['paid_subscription_set_to_cancel'] = stripe_subscription.status == 'active'
                hash['free_trial_set_to_cancel'] = stripe_subscription.status == 'trialing'
            else
                hash['set_to_cancel'] = false
            end

        end

        if event.type == 'customer.updated'
            if previous_attributes.respond_to?('default_source')
                customer = owner.stripe_customer
                default_card = owner.card_for_id(customer.default_source)
                previous_card = owner.card_for_id(previous_attributes.default_source)

                hash['card_added'] = true
                hash['card_changed'] = !previous_card.nil?
                hash['default_card.last4'] = default_card && default_card.last4
                hash['default_card.brand'] = default_card && default_card.brand
                hash['previous_default_card.last4'] = previous_card && previous_card.last4
                hash['previous_default_card.brand'] = previous_card && previous_card.brand
            else
                hash['card_changed'] = false
                hash['card_added'] = false
            end

        end

        if event.type == 'invoice.payment_failed' || event.type == 'invoice.payment_succeeded'
            hash.merge!({
                total: invoice.total / 100.0,
                invoice_id: invoice.id,
                currency: invoice.currency,
                invoice_number: invoice.number,
                invoice_date: invoice.created,
                hosted_invoice_url: invoice.hosted_invoice_url,
                invoice_pdf: invoice.invoice_pdf
            }.stringify_keys)

            # invoice.payment* events may not have a `plan` attached if invoiced manually
            begin
                hash['plan_id'] = invoice.lines.data[0].plan.id
                hash['plan_name'] = invoice.lines.data[0].plan.nickname
            rescue NoMethodError; end

            if subscription
                product = subscription.plan.product
                hash.merge!({
                    has_free_trial: stripe_subscription.trial_start.present?,
                    metered_plan: subscription.metered?,

                    # normally the charge will have a statement descriptor, but not
                    # if you used "Add Invoice Item" in the dashboard
                    statement_descriptor: (charge && charge.statement_descriptor) || product.statement_descriptor,
                    product_name: product.name
                }.stringify_keys)

                if subscription.metered?
                    # we only expect there to be one line
                    hash['lines'] = invoice.lines.map do |line|
                        {
                            unit_quantity: line.quantity,
                            unit_amount: line.plan.amount / 100.0,
                            unit_name: "#{product.name} (per #{product.unit_label})",
                            amount: line.amount / 100.0
                        }.stringify_keys
                    end
                end
            end
        end

        if event.type == 'invoice.payment_succeeded' && stripe_subscription

            invoice_period_start = begin
                invoice.lines.data[0]['period']['start']
            rescue => exception; end

            if invoice_period_start && invoice_period_start == stripe_subscription.trial_end
                hash['first_payment_for_subscription'] = true
            elsif invoice_period_start && invoice_period_start == stripe_subscription.created
                hash['first_payment_for_subscription'] = true
            else
                hash['first_payment_for_subscription'] = false
            end

        end

        if stripe_subscription
            hash['subscription_status'] = stripe_subscription.status
            hash['next_period_amount'] = subscription.next_period_amount.to_f / 100 unless !@subscription
            hash['current_period_end'] = stripe_subscription.current_period_end
            hash['current_period_start'] = stripe_subscription.current_period_start
            hash['cancel_at_period_end'] = stripe_subscription.cancel_at_period_end

            begin
                hash['plan_id'] = stripe_subscription.plan.id
                hash['plan_name'] = stripe_subscription.plan.nickname
            rescue NoMethodError; end

        end

        # 1. Since this block actually logs an event we want it to come after everything
        # else is set in the hash
        #
        # 2. To test this, grab a stripe subscription in a console and run
        #    `sub.trial_end = (Time.now + 7.days + 1.minute).to_timestamp; sub.save`
        #    One minute later this event will be logged
        if event.type == 'invoice.upcoming'

            # this one is used for a customerio email trigger, to notify
            # hiring managers when they are going to be charged
            hash['amount'] = event.data.object.amount_due / 100.0
            hash['next_payment_attempt'] = event.data.object.next_payment_attempt

        end

        if subscription&.open_position
            # if the creator of the position is different from the hiring_manager, send
            # this event also to the creator of the position
            creator_id = subscription&.open_position&.hiring_manager_id
            if creator_id && creator_id != user_or_hiring_team_owner.id
                # Notice that we are using `merge` here instead of `merge!` so
                # that these attributes only apply to this event, not the event
                # we send to the owner of the hiring team.
                log_event(
                    creator_id,
                    event,
                    hash.merge(subscription.open_position.event_attributes(creator_id)))
            end

            hash.merge!(subscription.open_position.event_attributes(user_or_hiring_team_owner.id))
        end

        log_event(user_or_hiring_team_owner.id, event, hash)
    end

    def log_event(user_id, event, hash)
        id = get_uuid(user_id)
        event_type = "stripe.#{event.type}"
        event_created = Time.at(event.created) # more accurate to use this than a delayed Time.now

        # If the event has already been logged, that means we've processed it already.  We should
        # rollback the transaction and do nothing.
        begin
            logged_event = Event.create_server_event!(id, user_id, event_type, hash, event_created)
        rescue ActiveRecord::RecordNotUnique => err
            raise EventAlreadyProcessed.new
        end

        # Filter out a few attributes from stripe that have big values before sending
        # to customer.io.
        keys_to_log = logged_event.payload.keys - ["data", "object", "pending_webhooks", "request", "api_version"]
        logged_event.log_to_external_systems(keys_to_log)
    end


    # this should get run last so that all other changes
    # are made first, so we define the order key as 9999
    handle 'all', 9999 do |event|
        user_or_hiring_team_owner.identify
    end

    # We want to have Stripe set keep the subscription `as-is` so that subsequent invoices are generated.
    # see also: https://dashboard.stripe.com/account/recurring
    #
    # We don't want `unpaid` because `as-is` allows additional invoicing whereas `unpaid` effectively locks activity.
    handle 'invoice.payment_failed' do |event|

        next if invoice.total <= 0

        # I'm not sure why we have to abort here if there is no stripe_subscription,
        # but that's what the old cold used to do before refactoring around 05/10/2018
        # and we get a spec failure if we don't.
        next unless stripe_subscription
        log_charge_failure(charge, invoice.total)
    end


    handle 'invoice.payment_succeeded' do |event|
        next if invoice.total <= 0

        log_to_slack(
            "*#{owner.email}* made a payment of *#{format_currency(invoice.total)}*",
            '#36A64F')

        # NOTE: metered ad-hoc remainder invoices (on plan change / termination) do not associate a subscription
        subscription&.record_invoice_payment(invoice)
        if subscription&.all_payments_complete?
            log_to_slack(
                "*#{owner.email}* has completed all payments to their subscription",
                '#4444FF')
        end

    end

    handle 'charge.failed' do |event|
        # we're already logging `invoice.payment_failed`. charges that don't even make it to subscription
        # creation will not have an invoice and won't log to that hook, so log them here.
        next unless invoice.nil?
        next if charge.amount <= 0
        log_charge_failure(charge, charge.amount)
    end

    handle 'charge.succeeded' do |event|
        BillingTransaction.find_or_create_by_stripe_charge(charge)
    end

    handle 'charge.refunded' do |event|
        charge.refunds.each do |refund|
            Refund.find_or_create_by_stripe_refund(refund)
        end
    end


    handle 'charge.refunded' do |event|
        # Make sure that we look at the amount for the refund and not the amount_refunded
        # on the charge, which is the total amount refunded for the charge to date.
        refund = charge.refunds.data.first
        amount_refunded = refund.amount.to_f
        next if amount_refunded <= 0

        # All of our expected charges are attached to invoiced subscriptions if we
        # change this business requirement, we will need to protect against nil subscriptions.
        #
        # We do not call record_refund for partial refunds.  In that case, the user
        # should have already been expelled.
        owner.record_refund(stripe_subscription.plan.id) if charge.refunded

        # see also: https://stripe.com/docs/api#charge_object-refunded
        if charge.refunded
            if partial_refund?(refund, charge)
                message = "Issued final partial refund of *#{format_currency(amount_refunded.to_f)}* for a charge of *#{format_currency(charge.amount.to_f)}* to *#{owner.email}*"
            else
                message = "Refunded *#{owner.email}* a payment of *#{format_currency(amount_refunded.to_f)}*"
            end
        else
            message = "Issued a partial refund of *#{format_currency(amount_refunded.to_f)}* for a charge of *#{format_currency(charge.amount.to_f)}* to *#{owner.email}*"
        end

        log_to_slack(
            message,
            '#FF9300')
    end

    handle 'charge.dispute.created' do |event|
        log_to_slack(
            "*#{owner.email}* has *disputed* a charge of #{format_currency(dispute.amount.to_f)}. :warning:",
            '#FF9300')
    end

    handle 'charge.dispute.funds_withdrawn' do |event|
        log_to_slack(
            "*#{owner.email}* - #{format_currency(dispute.amount.to_f)} in disputed funds have been withdrawn. :warning:",
            '#F8000C'
        )
    end

    handle 'charge.dispute.funds_reinstated' do |event|
        log_to_slack(
            "*#{owner.email}* - #{format_currency(dispute.amount.to_f)} in disputed funds have been reinstated.",
            '#36A64F'
        )
    end

    handle 'charge.dispute.closed' do |event|
        message = nil
        color = nil
        if dispute.status == 'won'
            message = "*#{owner.email}* - dispute of #{format_currency(dispute.amount.to_f)} won."
            color = '#36A64F'
        elsif dispute.status == 'lost'
            message = "*#{owner.email}* - dispute of #{format_currency(dispute.amount.to_f)} lost. :warning:"
            color = '#F8000C'
        else # only other option is warning_closed
            message = "*#{owner.email}* - dispute of #{format_currency(dispute.amount.to_f)} closed without escalation."
            color = '#36A64F'
        end

        log_to_slack(
            message,
            color
        )
    end


    # It is odd when customers are in the state where the new billing period
    # has started, the invoice has been created, but the invoice has not
    # yet been charged.  Stripe delays the payment to give the webhooks time
    # to make changes if they need to.  We don't, so just pay it immediately
    handle 'invoice.created' do |event|

        begin

            # If payment fails when creating a subscription, we do not
            # want to force a retry of the payment.  We used to not have
            # the check on `stripe_subscription&.status == 'active'`, and it
            # caused a problem because the payment succeeded on retry but we
            # had already deleted the `incomplete` subscription.  See https://trello.com/c/4ThWJ9LM

            # The check on auto_advance is more theoretical than anything.  When
            # an incomplete subscription is created, the invoice will be set to
            # auto_advance=false, but that happens in a subsequent update, so when
            # we get this event, it will still be `true`, so practically it's
            # not helping us here
            if invoice.auto_advance && stripe_subscription&.status == 'active'
                invoice.pay
            end

        # rescue just in case it is already paid
        rescue Stripe::InvalidRequestError

        # some card errors are expected here
        rescue Stripe::CardError
        end

    end


    handle 'customer.subscription.created' do |event|

        next if delete_incomplete_stripe_subscription

        plan_name = stripe_subscription.plan.nickname
        log_to_slack(
            "#{owner.email} started a subscription (#{plan_name})",
            '#F3E000')
    end


    handle 'customer.subscription.deleted' do |event|

        next if is_incomplete_stripe_subscription?

        # delete the corresponding local tracking subscription if it still exists (dashboard cancellation)
        subscription&.destroy

        plan_name = stripe_subscription.plan.nickname

        # handle cancellation reason with special color codes
        reason = stripe_subscription.metadata['cancellation_reason'] || 'No reason provided'
        if reason == 'Payment complete'
            color = '#F3E000'
        elsif reason == 'Deferral'
            color = '#FF9300'
        else
            color = '#F8000C'
        end

        log_to_slack(
            "Subscription cancelled for #{owner.email} with reason - #{reason} (#{plan_name})",
            color)
    end


    handle 'customer.subscription.updated' do |event|

        next if delete_incomplete_stripe_subscription

        # referencing subscription will ensure that the subscription gets created
        subscription
    end

    # Our system is supposed to completely ignore subscriptions marked as `incomplete`, meaning that
    # payment failed when the subscription was created. We prune these subscriptions in delete_incomplete_stripe_subscription.
    # With a couple of exceptions, we're not expecting to get any events about these subscriptions.  We just
    # want to log if we get any other ones to make sure we're not accidentally processing things we don't want to.
    handle 'all' do |event|
        next unless stripe_subscription
        expected_events = %w(customer.subscription.created customer.subscription.updated customer.subscription.deleted invoice.created invoice.payment_failed charge.failed)
        if is_incomplete_stripe_subscription? && !expected_events.include?(event.type)
            Raven.capture_exception('Unexpected event logged for incomplete subscription', {
              extra: {
                subscription: stripe_subscription_id,
                event_type: event.type
              },
              level: 'warning'
            })
        end
    end

end
