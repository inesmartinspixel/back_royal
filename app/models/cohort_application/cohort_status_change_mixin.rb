# This mixin is only included in CohortApplication::Version, but seemed nice to
# keep this code isolated all in one place
#
# It it tested in cohort_status_change_spec.rb
module CohortApplication::CohortStatusChangeMixin

    def get_cohort_status_change_attrs(last_cohort_status_change)
        status = get_status_for_cohort_status_change(last_cohort_status_change)
        return nil if status.nil?

        {
            graduation_status: graduation_status || 'pending', # some old ones have nil values here
            status: status,
            cohort_application_status: self.status,
            from_time: version_created_at,
            until_time: Time.parse('2099-01-01 00:00:00 UTC'),
            user_id: user_id,
            cohort_id: cohort_id,
            was_enrolled: status == 'enrolled' || last_cohort_status_change&.was_enrolled || false,
            cohort_application_id: self['id']
        }
    end

    def get_cohort_status_change_attrs_at_enrollment_deadline(last_cohort_status_change)
        attrs = get_cohort_status_change_attrs(last_cohort_status_change)
        if self.status != "accepted"
            raise "cannot generate cohort_status_change attrs for the enrollment deadline unless accepted"
        end

        if attrs[:from_time] >= published_cohort.enrollment_deadline
            raise "cannot generate cohort_status_change attrs when from_time is after the enrollment deadline"
        end

        attrs.merge(
            status: 'enrolled',
            from_time: published_cohort.enrollment_deadline,
            was_enrolled: true
        )
    end

    private def get_status_for_cohort_status_change(last_cohort_status_change)

        if self.status == 'rejected' && last_cohort_status_change.nil?
            nil

        # deferred and rejected statuses get assigned directly
        # to the cohort_status_change
        elsif ['deferred', 'rejected'].include?(self.status)
            self.status

        # if someone is expelled after being enrolled, then the status
        # is expelled.  If before being enrolled, then did_not_enroll
        elsif self.status == 'expelled'
            last_cohort_status_change&.was_enrolled ? 'expelled' : 'did_not_enroll'

        # graduated/honors both count as graduated
        elsif ['graduated', 'honors'].include?(self.graduation_status)
            'graduated'

        # failed is failed
        elsif self.graduation_status == 'failed'
            'failed'

        # accepted after the enrollment deadline is "enrolled"
        elsif self.status == 'accepted' && self.published_cohort.enrollment_deadline < version_created_at
            'enrolled'

        # accepted ebfore the enrollment_deadline or is "pending_enrollment"
        elsif ['accepted'].include?(self.status)
            'pending_enrollment'
        else
            # for other statuses, like pending and emba_targeted, we do nothing
            nil
        end

    end
end