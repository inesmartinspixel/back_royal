=begin

    The final_score is calculated as a weighted sum of a few different things:

    avg_test_score - the avg score the user has on all required test lessons in the curriculum)
    average_assessment_score_best - the avg of all the best_scores for the user on assessment
                                    lessons in the curriculum, counting required assessments
                                    and electives
    participation_score - (deprecated in newer cohorts) a score from 0-1 indicating how much
                            the user participated in online discussions
    project_score - a combined score of the users scores on each required project in the cohort

    ============ project_score details ========================

    In the past, we did not assign scores on each individual project.  We just recorded
    a single score for the user in the user_project_scores table.  We still use those scores
    when calculating final_score for users who do not have any project_progress records.

    When calculating the project_score for a user, capstones are weighted double standard
    projects. Standard projects are weighted double presentation projects.

    Even though each project is assigned a score from 0-5, when calculating the cumulative
    project_score for a user, it only matters if you have a passing grade or not on each project.
    Capstone projects require a score of 3 for passing.  Standard projects require a score of 1.
    Presentation projects require a score of 2.

    Anyproject_progress can have the flag marked_as_passed, which means we consider it passed when
    calculating the cumulative project score, even though there is no score assigned to the individual
    project.

    Projects can also be marked waived, which means that we calculate the final score as though that
    project were not required at all for this user.  For example, emba cohorts weight tests at 0.7,
    assessments at 0.2 and projects at 0.1.  A user with a 100% test score, 100% assessment score, and
    the following project scores would have the following total_scores:

    capstone: 3, project1: 1, project2: 1, presentation1: 2, presentation2: 2
        project_score   -> (0.4 + 0.2 + 0.2 + 0.1 + 0.1) / 1.0 = 1
        final_score     -> (0.7 + 0.2 + 0.1) / 1.0 = 1

    capstone: 3, project1: 0, project2: 1, presentation1: 1, presentation2: 2
        project_score   -> (0.4 + 0 + 0.2 + 0 + .1) / 1.0 = 0.7
        final_score     -> (0.7 + 0.2 + 0.07) / 1.0 = 0.97

    capstone: waived, project1: 1, project2: 1
        project_score   -> (0.25 + 0.25) / 0.5 = 1.0
        final_score     -> (0.7 + 0.2 + 0.5) / 0.95 = 1.0

=end
module CohortApplication::FinalScoreHelper

    class << self

        def calculate_final_score(cohort_application)
            if cohort_application.status != 'accepted'
                {
                    project_score: nil,
                    final_score: nil,
                    meets_graduation_requirements: false
                }
            else
                # For most people, project_weight will be 1.  However,
                # if a project is waived, then for this user, we will lower
                # the weighting used for projects in calculating the final_score.
                # (See comments at top of file)
                project_score, project_weight = get_project_score(cohort_application)
                weights = weights(cohort_application)
                weights[:project_score] *= project_weight if weights[:project_score]
                final_score = weighted_score(cohort_application, project_score, weights)
                {
                    project_score: project_score,
                    final_score: final_score,
                    meets_graduation_requirements: !!meets_graduation_requirements(cohort_application, final_score)
                }
            end

        end

        private
        def weights(cohort_application)
            if cohort_application.published_cohort.name == 'MBA1'
                {
                    avg_test_score: 0.7,
                    average_assessment_score_best: 0.3
                }

            # MBA 2-7
            elsif  cohort_application.program_type == 'mba' && cohort_application.published_cohort.start_date < Time.parse('2017/09/18')
                {
                    avg_test_score: 0.7,
                    average_assessment_score_best: 0.2,
                    participation_score: 0.1
                }

            # MBA 8-21
            elsif cohort_application.program_type == 'mba' && cohort_application.published_cohort.start_date < Time.parse('2019/06/17')
                {
                    avg_test_score: 0.7,
                    average_assessment_score_best: 0.2,
                    project_score: 0.1
                }

            # EMBA 1-3
            elsif  cohort_application.program_type == 'emba' && cohort_application.published_cohort.start_date < Time.parse('2017/09/25')
                {
                    avg_test_score: 0.6,
                    average_assessment_score_best: 0.2,
                    project_score: 0.1,
                    participation_score: 0.1
                }

            # EMBA 4-17
            elsif cohort_application.program_type == 'emba' && cohort_application.published_cohort.start_date < Time.parse('2019/07/01')
                {
                    avg_test_score: 0.6,
                    average_assessment_score_best: 0.2,
                    project_score: 0.2
                }

            # EMBA18+, MBA22+, other program types
            else
                Cohort::ProgramTypeConfig[cohort_application.program_type].grading_weights
            end
        end

        private
        def weighted_score(cohort_application, project_score, weights)
            project_score ||= 0

            # We only count required tests (there aren't any non-required tests anyway, but theoretically
            # it seems right).  But  we count all assessments including electives.  This allows someone to
            # pull up their score by doing electives (though theoretically they could pull it down as well).
            # Note that if someone has access to assessments outside of zir cohort, those will not be
            # counted here.  This might be important, for example, with legacy users who had access to all
            # content but then did the business certificate, which only includes a small slice of it.
            record = ActiveRecord::Base.connection.execute("
                SELECT
                    COALESCE(avg(CASE WHEN required and test THEN lesson_progress.best_score ELSE null END), 0) avg_test_score,
                    COALESCE(avg(CASE WHEN assessment THEN lesson_progress.best_score ELSE null END), 0) average_assessment_score_best,
                    COALESCE(avg(user_participation_scores.score), 0) participation_score
                FROM
                    published_cohort_lesson_locale_packs
                    LEFT JOIN lesson_progress
                        ON lesson_progress.locale_pack_id = published_cohort_lesson_locale_packs.lesson_locale_pack_id
                        AND lesson_progress.user_id = '#{cohort_application.user_id}'
                    LEFT JOIN user_participation_scores
                        on user_participation_scores.user_id = '#{cohort_application.user_id}'
                WHERE
                    published_cohort_lesson_locale_packs.cohort_id = '#{cohort_application.cohort_id}'
            ").to_a.first

            record['project_score'] = project_score

            # For most users, the total_weight will be 1, but if a project
            # is waived, it can be less than 1.  See comments at top of file
            total_weight = weights.values.inject(0.0) { |sum, _val| sum + _val }
            total_weighted_scores = weights.inject(0.0) do |sum, pair|
                key, weight = pair
                val = record[key.to_s]
                sum + val*weight
            end
            total_weighted_scores / total_weight
        end

        # duplicated in admin_cohort_gradebook_dir#getProjectScoreInfo
        # This method implements the scoring described in https://trello.com/c/JZrQcHnE,
        # though the algorithm seems very different.  Basically, we are calculating the
        # percent of projects for which the user has a passing score, but weighting the
        # capstone twice as heavily as other projects.
        private
        def get_project_score(cohort_application)

            if get_project_progress_statuses(cohort_application).select {|status| !!status.gradable}.count < 1

                # We used to set project scores in the user_project_scores table.  For a user
                # who does not have any project_progresses, we will continue to check that table for
                # a score.  Once a learner has any project_progresses, we assume we can rely on project_progresses
                # to get zir project score.
                record  = ActiveRecord::Base.connection.execute(%Q~
                    select score from user_project_scores where user_id='#{cohort_application.user_id}'
                ~).to_a.first
                project_score = record ? record['score'] : nil
                return [project_score, 1]
            end

            projects = cohort_application.published_cohort.learner_projects
            total_score = 0;

            # For most users, the total_unwaived_project_weight will be the same
            # as the total_project_weight.  But if a project has been waived, then
            # it will be less.  See comments at top of file
            total_project_weight = 0
            total_unwaived_project_weight = 0
            project_progresses = cohort_application.user.project_progresses.index_by(&:requirement_identifier)
            projects.each do |project|
                project_progress = project_progresses[project.requirement_identifier]
                total_project_weight += project.scoring_weight(cohort_application.program_type)
                total_unwaived_project_weight += project.scoring_weight(cohort_application.program_type) unless project_progress&.waived?
            end

            scores = projects.map do |project|
                project_progress = project_progresses[project.requirement_identifier]
                next if project_progress&.waived?
                weight = project.scoring_weight(cohort_application.program_type).to_f / total_unwaived_project_weight

                # if the project is marked_as_passed?, then we count it as
                # though the learner had a passing score
                passing_score = project.passing_score(cohort_application.published_cohort)
                if project_progress&.marked_as_passed?
                    score = passing_score
                else project_progress&.score
                    score = project_progress&.score || 0
                end
                passed = score >= passing_score
                weighted_score = weight * (passed ? 1 : 0)
                total_score += weighted_score
                weighted_score
            end.compact

            project_score = scores.inject(0) { |sum, el| sum + el }
            [project_score, total_unwaived_project_weight.to_f / total_project_weight]
        end

        private
        def meets_graduation_requirements(cohort_application, final_score)
            # MBA before mba8
            if  cohort_application.program_type == 'mba' && cohort_application.published_cohort.start_date < Time.parse('2017/09/18')
                return final_score > 0.7 &&
                    cohort_application.curriculum_complete?
            elsif ['emba', 'mba'].include?(cohort_application.program_type)

                return final_score > 0.7 &&
                    unpassed_project_count_of_type(cohort_application, LearnerProject::CAPSTONE_PROJECT_TYPE) == 0 &&
                    unpassed_project_count_of_type(cohort_application, LearnerProject::PRESENTATION_PROJECT_TYPE) == 0 &&
                    unpassed_project_count_of_type(cohort_application, LearnerProject::STANDARD_PROJECT_TYPE) <= 1 &&
                    cohort_application.curriculum_complete? &&
                    cohort_application.required_specializations_complete?

            elsif cohort_application.program_type == 'the_business_certificate'
                return final_score > 0.7 &&
                    cohort_application.curriculum_complete?
            elsif !Rails.env.test?
                Raven.capture_in_production(RuntimeError) do
                    raise RuntimeError.new("Trying to determine meets_graduation_requirements for unsupported program #{cohort_application.published_cohort.name.inspect}")
                end

                # Note: at some point the logic for paid cert was the following.  Since we never
                # really moved forward with those, it's not in use.  I'm not sure if we still want
                # to go with the idea of having project scores being on a different scale.  Seems like
                # it makes more sense to implement this using passing_score at this point

                # min_assessment_score_best > 0.8
                # and required_streams_perc_complete = 1
                # and project_score >= 3

            end

            false
        end

        private
        def unpassed_project_count_of_type(cohort_application, project_type)
            return get_project_progress_statuses(cohort_application).select {|status| status.project_type == project_type && !!status.unpassed}.count
        end

        private
        def get_project_progress_statuses(cohort_application)
            project_progress_statuses = []

            # Iterate through all of the projects in this cohort, and
            # create an  array of structs containing pertinent information.
            cohort_application.published_cohort.learner_projects
                .each do |project|
                    progress = cohort_application.user.project_progresses.detect { |r| r.requirement_identifier == project.requirement_identifier }
                    project_progress_statuses << OpenStruct.new({
                        project_type: project.project_type,
                        requirement_identifier: project.requirement_identifier,
                        unpassed: progress.present? ? progress.unpassed_for_project?(project, cohort_application.published_cohort) : true,
                        gradable: progress.present? ?  progress.gradable? : false
                    })
                end

            project_progress_statuses
        end

    end

end