# == Schema Information
#
# Table name: cohort_applications_versions
#
#  version_id                         :uuid             not null, primary key
#  operation                          :string(1)        not null
#  version_created_at                 :datetime         not null
#  id                                 :uuid             not null
#  created_at                         :datetime         not null
#  updated_at                         :datetime         not null
#  applied_at                         :datetime         not null
#  cohort_id                          :uuid             not null
#  user_id                            :uuid             not null
#  status                             :text             not null
#  accepted_at                        :datetime
#  graduation_status                  :text
#  expelled_at                        :datetime
#  disable_exam_locking               :boolean
#  skip_period_expulsion              :boolean
#  final_score                        :float
#  completed_at                       :datetime
#  graduated_at                       :datetime
#  total_num_required_stripe_payments :integer
#  registered                         :boolean
#  retargeted_from_program_type       :text
#  should_invite_to_reapply           :boolean
#  admissions_decision                :text
#  shareable_with_classmates          :boolean
#  notes                              :text
#  scholarship_level                  :json
#  num_charged_payments               :integer
#  num_refunded_payments              :integer
#  airtable_record_id                 :string
#  stripe_plan_id                     :text
#  airtable_base_key                  :string
#  locked_due_to_past_due_payment     :boolean
#  cohort_slack_room_id               :uuid
#  registered_at                      :datetime
#  registered_early                   :boolean
#  allow_early_registration_pricing   :boolean
#  stripe_plans                       :json             is an Array
#  scholarship_levels                 :json             is an Array
#  refund_issued_at                   :datetime
#  refund_attempt_failed              :boolean
#  original_program_type              :text
#  invited_to_interview_at            :datetime
#  project_score                      :float
#  version_editor_id                  :uuid
#  version_editor_name                :text
#  meets_graduation_requirements      :boolean
#  rubric_round_1_tag                 :string
#  rubric_round_1_reason_mba_invite   :string           is an Array
#  rubric_round_1_reason_mba_reject   :string           is an Array
#  rubric_round_1_reason_emba_invite  :string           is an Array
#  rubric_round_1_reason_emba_reject  :string           is an Array
#  rubric_final_decision              :string
#  rubric_final_decision_reason       :string
#  rubric_interviewer                 :string
#  rubric_interview                   :string
#  rubric_interview_scheduled         :boolean
#  rubric_interview_recommendation    :string
#  rubric_reason_for_declining        :string           is an Array
#  rubric_likelihood_to_yield         :string
#  rubric_risks_to_yield              :string           is an Array
#  rejected_after_pre_accepted        :boolean
#  rubric_inherited                   :boolean
#  rubric_interview_date              :datetime
#  rubric_motivation_score            :text
#  rubric_contribution_score          :text
#  rubric_insightfulness_score        :text
#  payment_grace_period_end_at        :datetime
#

class CohortApplication::Version < CohortApplication
    include VersionMixin
    include CohortApplication::CohortStatusChangeMixin

    self.primary_key = 'version_id'
    self.table_name = 'cohort_applications_versions'

    belongs_to :cohort_application, :foreign_key => 'id', :primary_key => 'id'

    def [](val)
        val.to_s == 'id' ? self.attributes['id'] : super
    end
end
