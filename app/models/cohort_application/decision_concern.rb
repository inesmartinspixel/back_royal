module CohortApplication::DecisionConcern

    extend ActiveSupport::Concern

    included do
        after_update :schedule_perform_admissions_decision
        after_update :revert_rejection_and_conversion_to_emba
        attr_accessor :send_retargeted_email_for_program_type
    end

    NORMAL_REJECTIONS = ["Reject", "Reject for Accreditation", "Reject for No Interview"]

    def schedule_perform_admissions_decision
        # Don't schedule an admissions_decision job if:
        # - There was no change to admissions_decision
        # - The admissions decision is changed to "manual_admin_decision" (should only happen on our side)
        # - The admissions decision is blank (can happen if the decision was TBC, see https://trello.com/c/Hf5l84FB)
        # - The admissions decision is TBC, which just means the admissions team needs to discuss the candidate more (so nothing happens on our end)
        return if !self.saved_change_to_admissions_decision? ||
            self.admissions_decision == "manual_admin_decision" ||
            self.admissions_decision.blank? ||
            self.admissions_decision.starts_with?("TBC")

        if self.program_type == "mba" || self.program_type == "emba"
            # We decided to disable this for now for marketing; we might bring it back
            #PerformAdmissionsDecisionJob.set(wait_until: self.applied_at + 8.days).perform_later(self.id) # https://stackoverflow.com/a/27757324/1747491
            PerformAdmissionsDecisionJob.perform_later(self.id)
        else
            PerformAdmissionsDecisionJob.perform_later(self.id)
        end
    end

    # See https://trello.com/c/2oXGlfeo
    def perform_admissions_decision
        program_type_before_decision = self.program_type

        case self.admissions_decision

        when "Waitlist"
            Event.create_server_event!(
                SecureRandom.uuid,
                self.user_id,
                'cohort:waitlisted',
                self.event_attributes
            ).log_to_external_systems

            return

        when *CohortApplication::NORMAL_REJECTIONS
            # When an accepted biz cert application has been targeted
            # for inviting to interview for the EMBA, but then that interview
            # does not work out, the application may be set to a rejected decision.
            # We want to properly sync that decision but definitely DO NOT want to
            # reject them from the biz cert!
            if self.biz_cert_non_pending?
                return
            else
                self.status = rejected_or_expelled
            end

        when "Reject and Convert to EMBA"
            reject_and_convert_to_emba

        when "Pre-Accept to MBA"
            if self.program_type == 'mba'
                self.status = "pre_accepted"
            else
                # This situation should have been stopped in "sync_from_airtable_job", but just in case.
                Raven.capture_exception("Can't set decision for non-MBA application to 'Pre-Accept to MBA'", {
                    extra: {
                        application_id: self.id
                    }
                })
            end

        when "Accept to Business Certificate"
            self.cohort_id = Cohort.promoted_cohort("the_business_certificate")["id"]
            self.status = "accepted"

        when "Accept to Career Network Only"
            self.cohort_id = Cohort.promoted_cohort("career_network_only")["id"]
            self.status = "accepted"

        when "EMBA w/ Full Scholarship"
            handle_emba_decision("Full Scholarship")
        when "EMBA w/ No Scholarship"
            handle_emba_decision("No Scholarship")
        when "EMBA w/ Level 1 Scholarship"
            handle_emba_decision("Level 1")
        when "EMBA w/ Level 2 Scholarship"
            handle_emba_decision("Level 2")
        when "EMBA w/ Level 3 Scholarship"
            handle_emba_decision("Level 3")
        when "EMBA w/ Level 4 Scholarship"
            handle_emba_decision("Level 4")
        when "EMBA w/ Level 5 Scholarship"
            handle_emba_decision("Level 5")
        when "Invite to Interview"
            event_attrs = self.event_attributes

            # Somtimes, Biz Cert folks are retargeted from Biz Cert to EMBA.
            # At the beginning of this process, they are invited to interview
            # for the EMBA program. In the email/text/push sent to these folks
            # triggered by this `cohort:invite_to_interview`, we need to communicate
            # specific information regarding the currently promoted EMBA cohort.
            if self.program_type == 'the_business_certificate' && self.status == 'accepted'
                event_attrs.merge!(suggested_emba_cohort_attrs)
            end

            Event.create_server_event!(
                SecureRandom.uuid,
                self.user_id,
                'cohort:invite_to_interview',
                event_attrs
            ).log_to_external_systems
        when "Invite to Convert to EMBA"
            Event.create_server_event!(
                SecureRandom.uuid,
                self.user_id,
                'cohort:invite_to_convert_program_type',
                self.event_attributes.merge(suggested_emba_cohort_attrs)
            ).log_to_external_systems
        when "Accept w/ Career Network"
            if self.program_type_config.supports_career_network_access_on_airtable_decision?
                self.user.can_edit_career_profile = true
                self.status = "accepted"
            end
        else
            raise "'#{self.admissions_decision}' decision not found"
        end

        # If the program_type changed due to our decision then be sure to update the retargeted_from_program_type field
        if self.program_type != program_type_before_decision
            self.retargeted_from_program_type = program_type_before_decision
        end

        self.save!
    end

    def biz_cert_non_pending?
        self.program_type == 'the_business_certificate' && self.status != 'pending'
    end

    def set_scholarship_level_on_emba_decision(level_name)
        self.scholarship_level = self.published_cohort.scholarship_levels.detect do |level|
            level["name"] == level_name
        end

        # If we are changing program_type proactively we don't want to ask them for
        # money in the UI until they themselves have taken some action to express interest. In
        # the Full Scholarship case however, since we aren't asking for money, we feel that
        # we can go ahead and show them the pre_accepted UI.
        self.status = "pre_accepted" if level_name == "Full Scholarship"
    end

    def rejected_and_converted_to_emba?
        self.status == 'rejected' && self.admissions_decision == 'Reject and Convert to EMBA'
    end

    # cohort:invite_to_convert_program_type (MBA -> EMBA) events need attributes from the EMBA sister cohort.
    # cohort:invite_to_interview events need attributes from the currently promoted EMBA cohort ONLY IF the
    #  application is an accepted Biz Cert application.
    def suggested_emba_cohort_attrs
        suggested_cohort = nil
        if self.program_type == "mba" && self.published_cohort.sister_cohort.present?
            suggested_cohort = self.published_cohort.sister_cohort
        else
            suggested_cohort = Cohort.promoted_cohort('emba')
        end

        {
            suggested_program_type: suggested_cohort.program_type,
            suggested_cohort: suggested_cohort.event_attributes(self.user.timezone)
        }
    end

    private def handle_emba_decision(level_name)
        # See comment at function definition
        if self.biz_cert_non_pending?
            process_emba_decision_for_biz_cert_non_pending_application(level_name)
            return
        end

        if self.program_type == "emba"
            # We are already in the EMBA so just change the status
            self.status = "pre_accepted"
        else
            # Typically, admissions uses the "Invite to Convert" decision to send communication to prompt the user themselves
            # to go change their program type. However, they will occasionally directly convert a user from MBA to EMBA if they
            # are giving them a full scholarship. In the interest of keeping the admissions process flowing, we allow this path
            # and instead set status = "pending", since it's safer. However, if you look in set_scholarship_level_on_emba_decision,
            # you'll see that this status will be set to pre_accepted in the case of full scholarship. We log a warning in all other
            # cases, just in case it was a mistake on their part.
            self.cohort_id = self.program_type == "mba" ? self.cohort.sister_cohort["id"] : Cohort.promoted_cohort("emba")["id"]
            self.status = "pending"

            if level_name != "Full Scholarship"
                Raven.capture_exception("Unexpected EMBA decision", {
                    extra: {
                        application_id: id,
                        program_type: self.program_type
                    },
                    level: 'warning'
                })
            end
        end

        self.set_scholarship_level_on_emba_decision(level_name)
    end

    private def reject_and_create_pending_emba_application
        # I know we call save at the end of this logic but we need to do it now if we want to make
        # a new application because of the unique constraint
        self.update!(status: 'rejected')

        # We used to create the cohort application for the currently promoted EMBA cohort, which is based on
        # the application_deadline for the cohort's current admission round, but the decision_date for the
        # previously promoted cohort may not have passed yet, so rather than just getting the promoted cohort,
        # we get the first cohort whose decision_date hasn't passed yet. See https://trello.com/c/p0AU7XUa.
        cohort = AdmissionRound.where(program_type: 'emba').where("decision_date > '#{Time.now}'").reorder(:decision_date).first.cohort

        # We make sure to create the application with an initial status of 'pending' to ensure that the
        # previous_cohort_status event field gets populated properly for certain Customer.io campaigns.
        # See https://trello.com/c/lmQtiJKB
        CohortApplication.create!(
            cohort_id: cohort["id"],
            user_id: self.user_id,
            status: "pending",
            applied_at: Time.now
        )
    end

    # As of https://trello.com/c/WhQhHRpv we started allowing biz cert applications with
    # a decision to be redecided on (see Redecidable formula in Airtable) so that accepted
    # users in cohorts such as the free biz cert could be invited to
    # interview for and hopefully accepted to the EMBA. To support this we need to
    # detect when an EMBA decision is being made on a non-pending application so that
    # we can automatically reject that application and make a
    # new one. One concern I had was triggering rejected campaigns, but according to
    # https://fly.customer.io/env/24964/campaigns/1000112/overview we check that the
    # previous_cohort_status is pending. That is probably because we've had situations
    # already where a user is moved over to EMBA manually, and in those situations
    # we instructed them to reject the current application.
    #
    # Note that we check `!= pending` when calling this function because if for some reason a pending application
    # hit this logic then we could just let it flow to the else clause above to have its cohort_id
    # changed directly, as would happen if program_type changed organically through the UI while
    # still pending.
    private def process_emba_decision_for_biz_cert_non_pending_application(level_name)
        new_emba_app = reject_and_create_pending_emba_application
        new_emba_app.set_scholarship_level_on_emba_decision(level_name)
        new_emba_app.status = 'pre_accepted'

        # Set the admissions_decision AFTER the application has been created since
        # schedule_perform_admissions_decision is triggered by an after_update hook.
        new_emba_app.admissions_decision = self.admissions_decision
        new_emba_app.save!
    end

    # As of https://trello.com/c/BsQClzLk we started allowing biz cert applications
    # to be redecided on and as a side-effect, the biz cert application would be
    # rejected and a new pending EMBA application would immediately be created.
    private def reject_and_convert_to_emba
        return unless self.program_type_config.supports_reject_with_conversion_to_emba?

        new_emba_app = reject_and_create_pending_emba_application

        # Set the admissions_decision AFTER the application has been created since
        # schedule_perform_admissions_decision is triggered by an after_update hook.
        # We do this so that it's easier for the admissions team to determine how
        # the application was created.
        new_emba_app.admissions_decision = self.admissions_decision
        new_emba_app.save!
    end

    # As part of the work to support redeciding on biz cert applications with the
    # 'Reject and Convert to EMBA' admissions_decision - resulting in the rejection
    # of the biz cert application, immediately followed by the creation of a new
    # pending EMBA application (see https://trello.com/c/BsQClzLk) - we also need
    # to support reverting the conversion to the EMBA if the EMBA application is
    # rejected for whatever reason, ensuring that they end up back in the biz cert
    # and in the right state as an accepted user.
    private def revert_rejection_and_conversion_to_emba
        if self.program_type == 'emba' && self.saved_change_to_status?(to: 'rejected')
            # The current application should really be the first one, but just in case...
            index_for_current_application = self.user.cohort_applications.find_index { |cohort_application| cohort_application.id == self.id }
            previous_cohort_application = self.user.cohort_applications[index_for_current_application + 1]
            previous_cohort_application.update!(applied_at: Time.now, status: 'accepted') if previous_cohort_application&.rejected_and_converted_to_emba?
        end
    end
end
