module CohortApplication::StatusGroupsMixin
    def statuses
        return %w(pending rejected accepted expelled deferred pre_accepted)
    end

    def graduation_statuses
        return %w(pending graduated failed honors)
    end

    def valid_enrollment_statuses
        return %w(pre_accepted accepted)
    end

    def valid_deferral_statuses
        return valid_enrollment_statuses.push('deferred')
    end

    # All the statuses that typically can be or are accepted.
    # Note that we have a unique constraint in the database based on these
    # statuses. The constraint also has an "emba_targeted" status,
    # but we haven't used it in a really long time.
    def acceptable_statuses
        return %w(pending pre_accepted accepted)
    end
end