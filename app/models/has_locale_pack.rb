module HasLocalePack
    extend ActiveSupport::Concern

    module ClassMethods
    end

    included do

        after_destroy :destroy_locale_pack_if_en
        validate :validate_locale_pack_id
        validate :validate_locale_pack

    end

    def ensure_locale_pack
        if !self.locale_pack
            create_locale_pack
        end
        self.locale_pack
    end

    def create_locale_pack(id = nil)
        raise "Already has a locale_pack" unless self.locale_pack_id.nil?
        id ||= SecureRandom.uuid
        self.locale_pack = self.class::LocalePack.new(id: id)
        self.locale_pack.add_to_group 'SUPERVIEWER' if supports_access_groups?
        self.locale_pack
    end

    def supports_access_groups?
        self.class::LocalePack.instance_methods.include?(:access_groups)
    end

    def raise_unless_supports_access_groups
        raise "#{self.class.name} does not support access groups" unless supports_access_groups?
    end

    # read-only methods for groups and access_groups
    def access_groups
        raise_unless_supports_access_groups
        locale_pack.nil? ? [] : locale_pack.access_groups
    end
    alias_method :groups, :access_groups

    def in_group?(*args)
        raise_unless_supports_access_groups
        locale_pack.nil? ? false : locale_pack.in_group?(*args)
    end

    def get_translation(locale)
        locale_pack && locale_pack.content_items.where(locale: locale).first
    end

    def destroy_locale_pack_if_en
        if self.locale == 'en' && locale_pack_id
            self.locale_pack.destroy
        end
    end

    def validate_locale_pack_id
        if has_published_version? && locale_pack_id.nil?
            errors.add(:locale_pack_id, "must be present if published")
        end

        if locale == "en" && locale_pack_id.nil?
            errors.add(:locale_pack_id, "must be present if English")
        end
    end

    def validate_locale_pack
        if locale_pack && !locale_pack.valid?
            locale_pack.errors.full_messages.each do |message|
                errors.add(:locale_pack, message)
            end
        end

        # Really, this validation should be on the locale pack, but because
        # of the order in which things get saved, that causes issues.
        # For the same reason, we skip this check for English items.  The item
        # might not yet be saved, so the locale pack won't find it yet when it queries
        # for content items.  But we know the locale pack actually will have an English item
        # very soon because THIS item is English and it is in the locale pack.
        if locale_pack && locale != 'en' && !locale_pack.content_items.where(locale: "en").exists?
            errors.add(:locale_pack, "must include an English item")
        end
    end



end