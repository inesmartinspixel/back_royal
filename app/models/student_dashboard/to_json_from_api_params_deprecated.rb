require 'back_royal/object' # looks_like_uuid?

class StudentDashboard::ToJsonFromApiParamsDeprecated

    include ToJsonFromApiParamsHelpers::ToJsonFromApiParamsMixin

    def initialize(params)

        params = do_basic_params_prep(params)

        @query_params = params.slice(
            :user
        ).with_indifferent_access

        set_filters({
            view_as: /^[\w:\w\-\s]+$/,
            user_can_see: :bool,
            in_locale_or_en: :locale,
            in_users_locale_or_en: :bool,
            locale: :locale
        })
        params[:except] ||= []

        fields = set_fields({
            available_fields: %w|
                lesson_streams available_playlists
            |
        })

        @user = @query_params[:user]
        raise ArgumentError.new("user must be provided") unless !@user.nil?
        @user_id = @user.id

        @query_builder = CrazyQueryBuilder.new('users')
        @selects = @query_builder.selects
        @joins = @query_builder.joins
        @wheres = @query_builder.wheres
        @withs = @query_builder.withs
        @query_builder.limit = @limit = @query_params[:limit]

        @wheres << "users.id = '#{@user_id}'"

        set_default_selects
        @selects.slice!(*fields)
    end

    # making this a hash instead of an array so that in the
    # future the api can support filtering in/out specific fields
    def set_default_selects
        # Note: With view_as we can't get any in-progress or favorited streams, as we aren't
        # viewing as a user.
        join_and_select_streams
        select_available_playlists
    end

    def join_and_select_streams
        if filters.key?("view_as")
            # Ensures that an empty array is returned for lesson_streams
            empty = Lesson::Stream::ToJsonFromApiParams.new({
                user: @user,
                filters: {
                    locale_pack_id: []
                }
            })
            @selects['lesson_streams'] = empty.to_array_subquery
            return
        end

        streams_filters = {
            published: true
        }.merge(filters.slice(:view_as, :user_can_see, :in_users_locale_or_en, :locale)).with_indifferent_access

        stream_locale_pack_ids = []
        stream_locale_pack_ids += Lesson::Stream
            .joins("LEFT JOIN lesson_streams_progress
                        ON lesson_streams.locale_pack_id = lesson_streams_progress.locale_pack_id
                        AND lesson_streams_progress.user_id = '#{@user_id}'")
            .joins("LEFT JOIN lesson_stream_locale_packs_users
                        ON lesson_stream_locale_packs_users.locale_pack_id = lesson_streams.locale_pack_id
                        AND lesson_stream_locale_packs_users.user_id = '#{@user_id}'")
            .joins("LEFT JOIN users_relevant_cohorts
                        ON users_relevant_cohorts.user_id = '#{@user_id}'")
            .joins("LEFT JOIN published_cohort_stream_locale_packs
                        ON published_cohort_stream_locale_packs.cohort_id = users_relevant_cohorts.cohort_id") # this is a bug.   It accidentally sends down all streams.  But that is necessary for now.  See https://trello.com/c/TqRnsS3N/739-bug-revert-change-to-not-return-all-streams-for-student-dashboard-call
            .where("
                    lesson_streams_progress.completed_at IS NOT NULL
                    OR lesson_stream_locale_packs_users.user_id IS NOT NULL
                    OR published_cohort_stream_locale_packs.required = true")
            .reorder(nil)
            .pluck('locale_pack_id').compact

        # We prime the initial student_dashboard call with the active_playlist streams since they are needed immediately for the UI.
        # Currently (03-01-2017) we make a second call on the frontend that gets all of the streams in all the playlists.
        if @user.active_playlist_locale_pack_id
            stream_locale_pack_ids_for_active_playlist = Playlist.stream_locale_pack_ids_for_playlist(@user.active_playlist_locale_pack_id, @user.locale)
            if (stream_locale_pack_ids_for_active_playlist)
                stream_locale_pack_ids += stream_locale_pack_ids_for_active_playlist
            end
        end

        streams_filters["locale_pack_id"] = stream_locale_pack_ids

        streams_to_json = Lesson::Stream::ToJsonFromApiParams.new({
            user: @user,
            include_progress: true,
            filters: streams_filters
        })

        @selects['lesson_streams'] = streams_to_json.to_array_subquery
    end

    def select_available_playlists
        available_playlists_filters = filters_to_pass_along

        # Build the playlist locale_pack_id filter based on the the student_dashboard filters
        locale_pack_id = []
        if filters["view_as"]
            type, id = filters[:view_as].split(':')
            if type == 'group'
                # If we are view_as by group then we only get streams
            elsif type == 'cohort'
                cohort = Cohort.find(id)
                # Use the latest published version of the cohort (if there is one; unlikely that there isn't),
                # which is the version visible to the public, otherwise don't add any locale pack ids.
                locale_pack_id += cohort.published_version ? cohort.published_version.playlist_pack_ids : []
            elsif type == 'institution'
                locale_pack_id += Institution.find(id).playlist_pack_ids
            else
                raise ArgumentError.new('Invalid value for view_as: #{filters[:view_as].inspect}')
            end
        else
            locale_pack_id += @user.relevant_cohort.playlist_pack_ids if @user.relevant_cohort
            locale_pack_id += @user.institutions.pluck('playlist_pack_ids').flatten
        end
        available_playlists_filters['locale_pack_id'] = locale_pack_id

        playlists_to_json = Playlist::ToJsonFromApiParams.new({
            user: filters["view_as"] ? nil : @user,
            filters: available_playlists_filters
        })

        @selects['available_playlists'] = playlists_to_json.to_array_subquery
    end

    def filters_to_pass_along
        {
            published: true,
        }.merge(filters.slice(:in_users_locale_or_en, :in_locale_or_en, :locale)).with_indifferent_access
    end

    def execute

        #puts @query_builder.to_json_sql
        result = ActiveRecord::Base.connection.execute(@query_builder.to_json_sql)
        json = result.to_a[0]["json"] || "[]"
        json
    end

end
