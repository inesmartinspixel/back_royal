require 'back_royal/object' # looks_like_uuid?

class StudentDashboard::ToJsonFromApiParams

    include ToJsonFromApiParamsHelpers::ToJsonFromApiParamsMixin

    cattr_accessor :stream_json_cache
    self.stream_json_cache = {}

    def initialize(params)

        params = do_basic_params_prep(params)

        @query_params = params.slice(
            :user
        ).with_indifferent_access

        set_filters({
            view_as: /^[\w:\-\s]+$/,
            user_can_see: :bool,
            in_locale_or_en: :locale,
            in_users_locale_or_en: :bool,
            locale: :locale
        })
        params[:except] ||= []

        fields = set_fields({
            available_fields: %w|
                lesson_streams available_playlists
            |
        })

        @user = @query_params[:user]
        raise ArgumentError.new("user must be provided") unless !@user.nil?
        @user_id = @user.id

        @query_builder = CrazyQueryBuilder.new('users')
        @selects = @query_builder.selects
        @joins = @query_builder.joins
        @wheres = @query_builder.wheres
        @withs = @query_builder.withs
        @query_builder.limit = @limit = @query_params[:limit]

        @wheres << "users.id = '#{@user_id}'"

        @selects.slice!(*fields)
    end

    def json
        return_value = "[{\"lesson_streams\": #{get_streams}, \"available_playlists\": #{get_playlists}}]"
        return_value
    end

    def get_streams

        # FIXME: when using view_as, we are defaulting to English.  This is ok, because
        # the UI provides no way to provide another language anyway.  We DO, however, use
        # view_as with other languages from the stream library.  If an unauthenticated user
        # hits the stream library with zer browser set to a different language, we set view_as
        # to "cohort:PROMOTED_DEGREE" and the language to the one the browser is set to.  So if this
        # code is repurposed for loading the library, we need to think about that.

        # decide whether we are loading streams for a cohort, group, institution, or user
        use_cohort_condition = false
        use_institution_condition = false
        if view_as_type == 'group'
            query = PublishedStream.for_group_name(view_as_id, ['version_id'])
        elsif view_as_type == 'cohort'
            query = PublishedStream.for_cohort(view_as_id, ['version_id'])
            use_cohort_condition = true
        elsif view_as_type == 'institution'
            query = PublishedStream.for_institution(view_as_id, ['version_id'])
            use_institution_condition = true
        elsif filters[:user_can_see] && filters[:in_users_locale_or_en]
            query = PublishedStream.for_user(@user, ['version_id'])
            use_cohort_condition = true
            use_institution_condition = true
        else
            raise ArgumentError.new("Cannot load streams for filters: #{filters.inspect}")
        end

        # add the streams from the active playlist
        if @user.active_playlist_locale_pack_id
            stream_locale_pack_ids_for_active_playlist = Playlist.stream_locale_pack_ids_for_playlist(@user.active_playlist_locale_pack_id, @user.locale)
            if (stream_locale_pack_ids_for_active_playlist)
                active_playlist_locale_pack_ids_string = stream_locale_pack_ids_for_active_playlist.map { |id| "'#{id}'" }.join(',')
            end
        end

        if active_playlist_locale_pack_ids_string.blank?
            active_playlist_locale_pack_ids_string = "'#{SecureRandom.uuid}'"
        end

        # In some cases, PublishedStream will have joined against the
        # published_cohort_stream_locale_packs and/or institution_stream_locale_packs
        # tables.  In other cases it won't
        # be there, though, so we can only include the condition when it is relevant.
        cohort_condition = ""
        institution_condition = ""
        if use_cohort_condition
            # Load up all streams for a particular cohort, disregarding their completeness
            # See also: https://trello.com/c/Pjy7JnlX
            cohort_condition = "OR published_cohort_stream_locale_packs.cohort_id is not null"
        end

        if use_institution_condition
            institution_condition = "OR institution_stream_locale_packs.stream_locale_pack_id is not null"
        end

        # create a query that will filter down to just the streams that are
        # favorite, complete or required by the cohort or institution
        query = query
            .joins("
                LEFT JOIN lesson_streams_progress
                    ON published_streams.locale_pack_id = lesson_streams_progress.locale_pack_id
                    AND lesson_streams_progress.user_id = '#{@user_id}'
                LEFT JOIN lesson_stream_locale_packs_users
                    ON lesson_stream_locale_packs_users.locale_pack_id = published_streams.locale_pack_id
                    AND lesson_stream_locale_packs_users.user_id = '#{@user_id}'
            ")
            .where("
                -- completed
                lesson_streams_progress.completed_at IS NOT NULL

                -- or favorited
                OR lesson_stream_locale_packs_users.user_id IS NOT NULL

                -- or required by cohort
                #{cohort_condition}

                -- or required by institution
                #{institution_condition}

                -- or in active playlist
                OR published_streams.locale_pack_id in (#{active_playlist_locale_pack_ids_string})
            ")

        # convert the list of version_ids into stream json, using the cache where possible
        stream_jsons = get_streams_json_from_query(query)
        "[#{stream_jsons.join(',')}]"
    end

    def get_streams_json_from_query(query)
        version_ids = query.to_a.map { |entry| entry['version_id'] }
        Lesson::Stream::JsonCache.get_stream_jsons_for_version_ids(version_ids)
    end

    def view_as_type
        unless defined? @view_as_type
            if filters["view_as"]
                type = filters[:view_as].split(':')[0]
                if !['group', 'cohort', 'institution'].include?(type)
                    raise ArgumentError.new('Invalid value for view_as: #{filters[:view_as].inspect}')
                end
                @view_as_type = type
            else
                @view_as_type = nil
            end
        end
        @view_as_type
    end

    def view_as_id
        unless defined? @view_as_id
            if filters["view_as"]
                id = filters[:view_as].split(':')[1]
                @view_as_id = id
            else
                @view_as_id = nil
            end
        end
        @view_as_id
    end

    def get_playlists

        # Build the playlist locale_pack_id filter based on the the student_dashboard filters
        locale_pack_ids = []

        pref_locale = 'en'
        if view_as_type == 'group'
            # view_as_type we are view_as by group then we only get streams
        elsif view_as_type == 'cohort'
            cohort = Cohort.find(view_as_id)
            # Use the latest published version of the cohort (if there is one; unlikely that there isn't),
            # which is the version visible to the public, otherwise don't add any locale pack ids.
            locale_pack_ids += cohort.published_version ? cohort.published_version.playlist_pack_ids : []
        elsif view_as_type == 'institution'
            locale_pack_ids += Institution.find(view_as_id).playlist_pack_ids
        else
            locale_pack_ids += @user.relevant_cohort.playlist_pack_ids if @user.relevant_cohort
            locale_pack_ids += @user.institutions.pluck('playlist_pack_ids').flatten
            pref_locale = @user.pref_locale
        end

        playlists_json = PublishedPlaylist.for_pref_locale(pref_locale, ['version_id'])
            .where(locale_pack_id: locale_pack_ids)
            .map do |entry|

                version_id = entry['version_id']
                # NOTE: related_cohort_names is included in the json, and can change without the playlist
                # being republished.  This shouldn't be a practical issue, though, since that is just
                # used in the admin
                # NOTE: In the case where the playlists are not yet cached, we could make this a bit
                # faster by moving things around to make it one db call instead of one for each playlist. We
                # did this above with the streams.  It's less bang for the buck here though, since there
                # will only be a handful of playlists.  (Saw it taking about 200ms in a test)
                SafeCache.fetch("playlist_json/#{version_id}") do
                    json = Playlist::ToJsonFromApiParams.new({
                        filters: {
                            version_id: version_id
                        }
                    }).json

                    # remove the brackets at the beginning and end so we just
                    # have the object
                    json.slice!(0, 1)
                    json.slice!(-1, 1)
                    json
                end
            end

        "[#{playlists_json.join(',')}]"
    end

    def filters_to_pass_along
        {
            published: true,
        }.merge(filters.slice(:in_users_locale_or_en, :in_locale_or_en, :locale)).with_indifferent_access
    end

end
