# == Schema Information
#
# Table name: content_publishers
#
#  id                       :uuid             not null, primary key
#  created_at               :datetime
#  updated_at               :datetime
#  published_at             :datetime
#  object_updated_at        :datetime
#  lesson_version_id        :uuid
#  lesson_stream_version_id :uuid
#  playlist_version_id      :uuid
#  cohort_version_id        :uuid
#

#
# Models that can be published will include the ContentPublisher::Publishable module
class ContentPublisher < ApplicationRecord

    belongs_to :published_lesson_version, :primary_key => 'version_id', :foreign_key => 'lesson_version_id', :class_name => 'Lesson::Version', optional: true
    belongs_to :published_stream_version, :primary_key => 'version_id', :foreign_key => 'lesson_stream_version_id', :class_name => 'Lesson::Stream::Version', optional: true
    belongs_to :published_playlist_version, :primary_key => 'version_id', :foreign_key => 'playlist_version_id', :class_name => 'Playlist::Version', optional: true
    belongs_to :published_cohort_version, :primary_key => 'version_id', :foreign_key => 'cohort_version_id', :class_name => 'Cohort::Version', optional: true

    has_one :lesson, :through => :published_lesson_version
    has_one :lesson_stream, :through => :published_stream_version, :class_name => 'Lesson::Stream'
    has_one :playlist, :through => :published_playlist_version
    has_one :cohort, :through => :published_cohort_version

    default_scope -> {order(:created_at)}

    validates_length_of :content_items, :maximum => 1
    validates_presence_of :content_item
    validates_presence_of :version_id
    validates_presence_of :object_updated_at
    validate :validate_was_published

    def version_id
        lesson_version_id || lesson_stream_version_id || playlist_version_id || cohort_version_id
    end

    # there will always be exactly one content_item, or validations will fail
    def content_items
        [lesson, lesson_stream, playlist, cohort].compact
    end

    def content_item
        content_items.first
    end

    def content_item=(item)
        assoc_name = self.class.content_item_association(item.class)
        self.send(:"#{assoc_name}=", item)
    end

    def publish!(item_version, opts = {})
        if item_version.is_a?(Lesson::Version)
            self.lesson_version_id = item_version.version_id
        elsif item_version.is_a?(Lesson::Stream::Version)
            self.lesson_stream_version_id = item_version.version_id
        elsif item_version.is_a?(Playlist::Version)
            self.playlist_version_id = item_version.version_id
        elsif item_version.is_a?(Cohort::Version)
            self.cohort_version_id = item_version.version_id
        end
        self.object_updated_at = item_version.updated_at

        # Note: published_at should only be called when a human updates this, not
        # when a migration or something does
        if opts[:update_published_at]
            self.published_at = Time.now
        end
        save!
    end

    def unpublish!
        self.destroy!
    end

    def published_version
        if lesson_version_id
            published_lesson_version
        elsif lesson_stream_version_id
            published_stream_version
        elsif playlist_version_id
            published_playlist_version
        elsif cohort_version_id
            published_cohort_version
        end
    end

    def published_version_id
        lesson_version_id || lesson_stream_version_id || playlist_version_id || cohort_version_id
    end

    def validate_was_published
        if !published_version.was_published
            errors.add(:was_published, "must be true")
        end
    end

end
