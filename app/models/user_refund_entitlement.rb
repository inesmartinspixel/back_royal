# == Schema Information
#
# Table name: user_refund_entitlements
#
#  id                                    :uuid             not null, primary key
#  created_at                            :datetime         not null
#  updated_at                            :datetime         not null
#  user_id                               :uuid             not null
#  updated_to_last_lesson_activity_time  :datetime
#  updated_to_max_application_updated_at :datetime
#  refund_amount                         :float
#

class UserRefundEntitlement < ApplicationRecord
end
