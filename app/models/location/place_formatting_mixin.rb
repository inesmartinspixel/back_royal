# Note: See also has_location.js for identical JavaScript code. If the existing method
# implementations or special case lists change here then they probably need to change
# there as well. I considered trying to use Opal to transpile Ruby to JavaScript but
# that added too much additional dependency code on the client. I also considered execjs
# to run JavaScript from Ruby but that ultimately did not seem worth trying to do.
# See docs at https://developers.google.com/maps/documentation/geocoding/intro
module Location::PlaceFormattingMixin

    def location_string(place_details)
        city_state_names_to_exclude = ['Singapore'];
        usa_and_city_state_country_codes_to_exclude = ['US', 'USA', 'SG', 'HK', 'GI', 'VA'];

        location_string_parts = []

        city_name = Location.city_name(place_details)
        state_abbr = Location.state_abbr(place_details)
        country_code = Location.country_code(place_details)
        country_name = Location.country_name(place_details)

        if city_name || state_abbr
            # it's possible that there's no city, but there is a state
            # An example is Tokyo, JP
            if city_name
                location_string_parts.push(city_name)
            end

            if state_abbr && !city_state_names_to_exclude.include?(state_abbr) && state_abbr != city_name
                location_string_parts.push(state_abbr)
            end

            if country_code && !usa_and_city_state_country_codes_to_exclude.include?(country_code)
                location_string_parts.push(country_code)
            end
        elsif country_code
            location_string_parts.push(country_name)
        end

        return location_string_parts.join(', ')
    end

    def city_name(place_details)
        # - locality indicates an incorporated city or town political entity.
        # - administrative_area_level_3 indicates a third-order civil entity below the country level. This
        #   type indicates a minor civil division. Not all nations exhibit these administrative levels.
        # - colloquial_area indicates a commonly-used alternative name for the entity.
        (place_details['locality'] && place_details['locality']['long']) ||
        (place_details['administrative_area_level_3'] && place_details['administrative_area_level_3']['long']) ||
        (place_details['colloquial_area'] && place_details['colloquial_area']['long'])
    end

    def state_abbr(place_details)
        # administrative_area_level_1 indicates a first-order civil entity below the country level.
        # Within the United States, these administrative levels are states. Not all nations exhibit
        # these administrative levels. In most cases, administrative_area_level_1 short names will
        # closely match ISO 3166-2 subdivisions and other widely circulated lists; however this is not
        # guaranteed as our geocoding results are based on a variety of signals and location data.
        #
        # Note: 'short' typically produces an abbreviation, though in international addresses this is sometimes
        # the same as the 'long'
        place_details['administrative_area_level_1'] && place_details['administrative_area_level_1']['short']
    end

    def country_code(place_details)
        # country indicates the national political entity, and is typically the highest order type returned by the Geocoder.
        place_details['country'] && place_details['country']['short']
    end

    def country_name(place_details)
        place_details['country'] && place_details['country']['long']
    end

    def city_name_and_state_abbr(place_details)
        city_name = Location.city_name(place_details)
        state_abbr = Location.state_abbr(place_details)
        [city_name, state_abbr].compact.join(', ')
    end
end