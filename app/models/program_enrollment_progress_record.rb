# == Schema Information
#
# Table name: program_enrollment_progress_records
#
#  user_id                         :uuid
#  program_type                    :text
#  original_cohort_id              :uuid
#  final_cohort_id                 :uuid
#  deferred_out_of_original_cohort :boolean
#  final_status                    :text
#  graduated_at                    :datetime
#  could_graduate_by_deadline      :boolean
#  did_graduate_by_deadline        :boolean
#

class ProgramEnrollmentProgressRecord < ApplicationRecord

end
