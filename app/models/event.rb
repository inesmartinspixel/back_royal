# == Schema Information
#
# Table name: events
#
#  id                     :uuid             not null, primary key
#  user_id                :uuid
#  event_type             :string(255)
#  payload                :json
#  created_at             :datetime
#  updated_at             :datetime
#  estimated_time         :datetime
#  client_reported_time   :datetime
#  hit_server_at          :datetime
#  total_buffered_seconds :float
#

class Event < ApplicationRecord

    class ArchivedEvent < Event
        self.table_name = 'archived_events'
        self.inheritance_column = nil
    end

    include Skylight::Helpers

    validates_presence_of :user_id, :event_type, :estimated_time
    validates_presence_of :client_reported_time, :total_buffered_seconds, :hit_server_at, :page_load_id, :unless => :server_event?

    belongs_to :user, optional: true

    CLIENT_SIDE_EVENTS_NOT_PERSISTED_TO_RDS = [
        "click",
        "cordova:device-registered",
        "cordova:notification-opened",
        "dom_content_loaded",
        "dynamic_landing_page:clicked_next_step",
        "dynamic_landing_page:preSignupForm_initialized",
        "dynamic_landing_page:script_not_available",
        "dynamic_landing_page:viewed_success_message",
        "http_queue:queue_request",
        "http_queue:request_error",
        "http_queue:request_success",
        "http_queue:retry",
        "http_queue:send_request",
        "lesson:challenge_complete",
        "lesson:challenge_validation",
        "lesson:frame:unload",
        "loading_page",
        "marketing:pageview",
        "modal_dialog:alert",
        "modal_dialog:confirm",
        "mousedown",
        "ng_map:got_map",
        "ng_map:slow_loading_map",
        "page_load:unload",
        "route_change::enter",
        "route_change::leave",
        "route_change:admin-careers:enter",
        "route_change:admin-careers:leave",
        "route_change:admin-mba:enter",
        "route_change:admin-mba:leave",
        "route_change:admin-users:enter",
        "route_change:admin-users:leave",
        "route_change:batch-edit-users:enter",
        "route_change:batch-edit-users:leave",
        "route_change:careers:enter",
        "route_change:careers:leave",
        "route_change:complete-registration:enter",
        "route_change:complete-registration:leave",
        "route_change:deferral-link:enter",
        "route_change:deferral-link:leave",
        "route_change:edit-lesson:enter",
        "route_change:edit-lesson:leave",
        "route_change:edit-stream:enter",
        "route_change:edit-stream:leave",
        "route_change:editor-index:enter",
        "route_change:editor-index:leave",
        "route_change:forgot-password:enter",
        "route_change:hiring-choose-plan:enter",
        "route_change:hiring-choose-plan:leave",
        "route_change:institution-register:enter",
        "route_change:institution-register:leave",
        "route_change:lesson-diff:enter",
        "route_change:lesson-diff:leave",
        "route_change:onboarding-forgot-password:enter",
        "route_change:onboarding-forgot-password:leave",
        "route_change:onboarding-hybrid-forgot-password:enter",
        "route_change:onboarding-hybrid-forgot-password:leave",
        "route_change:onboarding-hybrid-login:enter",
        "route_change:onboarding-hybrid-login:leave",
        "route_change:onboarding-hybrid-questionary:enter",
        "route_change:onboarding-hybrid-questionary:leave",
        "route_change:onboarding-hybrid-register:enter",
        "route_change:onboarding-hybrid-register:leave",
        "route_change:onboarding-hybrid-start:enter",
        "route_change:onboarding-hybrid-start:leave",
        "route_change:onboarding-login:enter",
        "route_change:onboarding-login:leave",
        "route_change:reports:enter",
        "route_change:reports:leave",
        "route_change:settings:enter",
        "route_change:settings:leave",
        "route_change:show-stream:enter",
        "route_change:show-stream:leave",
        "route_change:sign-in:enter",
        "route_change:sign-in:leave",
        "route_change:stream-completed:enter",
        "route_change:stream-completed:leave",
        "route_change:stream-dashboard:enter",
        "route_change:stream-dashboard:leave",
        "route_change:stream-library:enter",
        "route_change:stream-library:leave",
        "route_change:student-dashboard:enter",
        "route_change:student-dashboard:leave",
        "route_change:student-network:enter",
        "route_change:student-network:leave",
        "scrolled_first_time",
        "splashscreen_hidden",
        "still_on_page",
        "user:set_utm_params"
    ]

    def self.create_server_event!(id, user_id, event_type, hash, estimated_time = nil)
        payload = hash.merge({
            server_event: true
        })
        check_for_unknown_properties(payload.stringify_keys)
        params = {
            id: id,
            user_id: user_id,
            event_type: event_type,
            payload: payload,
            estimated_time: estimated_time || Time.now
        }
        event = Event.create!(params)

        self.update_distinct_user_ids(user_id)

        if ENV['SAVE_TO_ARCHIVED_EVENTS'] == 'true'
            ArchivedEvent.create!(event.attributes)
        end

        Event::PutEventsInFirehose.put_and_queue_failures([event])

        event
    end

    def self.create_batch_from_hash!(event_hashes, opts, retries = 0)

        if (!opts[:user_id])
            raise ArgumentError.new('user_id must be set')
        end

        if (!opts[:hit_server_at])
            raise ArgumentError.new('hit_server_at must be set')
        end

        if (!opts[:request_queued_for_seconds])
            raise ArgumentError.new('request_queued_for_seconds must be set')
        end

        instances = []

        # Automatically retries in the event of ActiveRecord::PreparedStatementCacheExpired
        RetriableTransaction.transaction do
            Skylight.instrument do
                if !event_hashes.blank?

                    # Save the event hashes, using "upsert" (on conflict) to
                    # avoid duplicate entries
                    result = save_event_hashes(event_hashes, opts)

                    # Retrieve newly inserted events and non-persisted events
                    # for downstream consumption
                    instances = result[:new_events] || []
                    instances += result[:non_persisted_events] || []

                    # Write to Kinesis firehose
                    Event::PutEventsInFirehose.put_and_queue_failures(instances, :client_side_events)

                    begin

                        # only process newly inserted events
                        instances.each do |instance|
                            # events logged from the client call log_to_customer_io.
                            # This is because we have removed customer.io from our client
                            # logging.  We removed it because, since customer.io is used
                            # for email triggering, we need it to be completely reliable.
                            if result[:ids_to_log_to_customer_io].include?(instance.id)
                                instance.log_to_external_systems
                            end

                            instance.process_after_saving_in_batch
                        end

                        self.update_distinct_user_ids(opts[:user_id]) if instances.map(&:event_type).include?('page_load:load')
                    rescue Exception => err

                        # See https://trello.com/c/L2nzc20p
                        Raven.capture_exception("Hit an exception after saving events to firehose", {
                            extra: {
                                cause_message: err.message,
                                cause_backtrace: err.backtrace.slice(0, 5),
                                events: instances&.map { |e| e.attributes.slice('id', 'created_at', 'user_id', 'event_type') }
                            }
                        })
                        raise
                    end

                end
            end
        end

        nil
    end

    def self.update_distinct_user_ids(user_id)
        DistinctUserId.where(user_id: user_id).first_or_create!
    rescue ActiveRecord::RecordNotUnique
        # ignore duplicate key errors
    end

    def self.fallback_on_server_timestamp_error_allowed?
        Rails.env.production?
    end

    def self.save_event_hashes(event_hashes, opts)

        result = {
            :ids_to_log_to_customer_io => Set.new
        }

        return result unless event_hashes.any?

        page_load_ids = event_hashes.map { |e| e['page_load_id'] }.compact.uniq
        if page_load_ids.size != 1
            err = RuntimeError.new("Expecting exactly one page_load_id. Got #{page_load_ids.inspect}")
            err.raven_options = {
                extra: {
                    events: event_hashes
                }
            }
            raise err
        end

        # prepare single mass-insert SQL statement for performance gains
        inserts = []

        # We sort by estimated_time so that when we assign created_at below, it will
        # result in events that are in the same order when sorted by either created_at or estimated_time.
        # This should be unnecessary, since events will be passed in in estimated_time order,
        # but nothing is enforcing that, so doing the sort here to be sure.
        # (We actually set the estimated_time on each event, only to delete it inside the
        # loop, just because it's an easy way to make estimated_time available to the sort_by
        # and also available inside the loop)
        add_estimated_times(event_hashes)
        event_hashes.sort_by { |e| e['estimated_time'] }.each do |e|
            estimated_time = e.delete('estimated_time')
            check_for_unknown_properties(e)

            # return info about what should be
            # logged to customerio
            log_to_customer_io = e.delete('log_to_customerio')
            if log_to_customer_io
                result[:ids_to_log_to_customer_io] << e['id']
            end

            # build derived values
            total_buffered_seconds = opts[:request_queued_for_seconds] + e['buffered_time']
            client_reported_time = Time.at(e['client_utc_timestamp'])

            if ['page_load:load', 'loading_page'].include?(e['event_type'])
                e['ip_address'] = opts[:client_ip_address] if opts[:client_ip_address]
                e['cf_country_code'] = opts[:cloudflare_country_code] if opts[:cloudflare_country_code]
            end

            payload = ActiveRecord::Base.connection.quote(e.to_json)

            # ensure times are in UTC
            client_reported_time_str = client_reported_time.utc.strftime('%Y-%m-%d %H:%M:%S.%N')
            estimated_time_str = estimated_time.utc.strftime('%Y-%m-%d %H:%M:%S.%N')
            hit_server_at_str = opts[:hit_server_at].utc.strftime('%Y-%m-%d %H:%M:%S.%N')

            # we used to set updated_at and created_at with NOW().  But that meant
            # that all events in the same batch had the same updated_at, which was
            # inconvenient for sorting.  This changed around 04/20/2019
            created_at_str = updated_at_str = Time.now.utc.strftime('%Y-%m-%d %H:%M:%S.%N')

            # sanitize string values
            id = ActiveRecord::Base.connection.quote(e['id'])
            event_type = ActiveRecord::Base.connection.quote(e['event_type'])
            user_id = ActiveRecord::Base.connection.quote(opts[:user_id])

            if !Event::CLIENT_SIDE_EVENTS_NOT_PERSISTED_TO_RDS.include?(e['event_type']) || !Event::PutEventsInFirehose.is_enabled?
                inserts << "(#{id}, #{event_type}, #{payload}, #{user_id}, '#{hit_server_at_str}', #{total_buffered_seconds}, '#{client_reported_time_str}', '#{estimated_time_str}','#{created_at_str}', '#{updated_at_str}')"
            else
                created_at, updated_at = Time.now.utc
                result[:non_persisted_events] ||= []
                result[:non_persisted_events] << Event.new({
                    id: SecureRandom.uuid,
                    user_id: opts[:user_id],
                    event_type: e['event_type'],
                    payload: e,
                    created_at: created_at,
                    updated_at: updated_at,
                    estimated_time: estimated_time,
                    client_reported_time: client_reported_time,
                    hit_server_at: opts[:hit_server_at],
                    total_buffered_seconds: total_buffered_seconds
                })
            end
        end

        # "upsert" conflict resolution RETURNING clause return only newly inserted results!
        if !inserts.blank?
            result[:new_events] = Event.find_by_sql(insert_sql('events', inserts))

            if ENV['SAVE_TO_ARCHIVED_EVENTS'] == 'true'
                Event.find_by_sql(insert_sql('archived_events', inserts))
            end
        end

        result
    end

    def self.add_estimated_times(event_hashes)
        event_hashes.each do |e|
            if e['server_timestamp'].present?
                estimated_time = Time.at(e['server_timestamp'])

            else
                can_fallback = (e['client_utc_timestamp'] - Time.now.to_timestamp).abs < 1.hour
                message = "No server timestamp in event.  " + (can_fallback ? "Falling back to client timestamp." : "Cannot fallback to client timestamp.")

                # I guess this will log to raven twice, but I don't know how to prevent
                # that and still get the event into the payload
                Raven.capture_exception(message, {
                    extra: {
                        event_type: e['event_type']
                    }
                })
                raise message unless can_fallback && fallback_on_server_timestamp_error_allowed?
                estimated_time = Time.at(e['client_utc_timestamp'])
            end

            e['estimated_time'] = estimated_time
        end
    end

    def self.check_for_unknown_properties(payload)
        unknown_properties = payload.keys - self.known_properties
        if !unknown_properties.empty?
            message = "Encountered unknown Event properties. Please see PutEventsInFirehose for proper handling. - #{unknown_properties.inspect}"
            if raise_on_unknown_properties?
                raise UnknownPropertyException.new(message)
            else
                Raven.capture_exception(message)
            end
        end
    end


    def self.known_properties

        Event::PutEventsInFirehose.handled_properties + self.column_names + [
            'server_timestamp',
            'log_to_customerio',

            # Google Analytics property
            'category',

            # Stripe event data we don't currently need to preserve
            'request', #  open-ended name from Stripe events -- might need to prune Stripe logging if we ever want to use this column name in the future
            'data',
            'default_card.last4',
            'default_card.brand',
            'previous_default_card.last4',
            'previous_default_card.brand',

            # these are needed in customer.io for triggered emails, but
            # there is no need to send them to redshift
            'candidate_nickname',
            'hiring_manager_nickname',
            'hiring_manager_company_name',
            'period_style',
            'period_start',
            'period_end',
            'course_entries',
            'english_title',
            'title_in_users_locale',
            'english_url',
            'url_in_users_locale',
            'period_title',
            'cohort_schedule_url',
            'course_entries',
            'curriculum_percent_complete',
            'curriculum_expected_complete',
            'profile_feedback',
            'cohort_title',
            'relevant_email',
            'action_rule',
            'period_style',
            'period_exam_style',
            'period_title',
            'auto_expulsion_will_occur_at',
            'course_entries',
            'curriculum_context',
            'passed',
            'promoted_admission_rounds',
            'cohort_decision_date',
            'cohort_application_deadline',
            'cohort_registration_deadline',
            'cohort_early_registration_deadline',
            'foundations_percent_complete',
            'foundations_courses_complete_count',
            'start_content_marketing_campaign',
            'program_guide_url',
            'period',
            'starting_final_exam',
            'exam',
            'retargeted_from_program_type',
            'cohort_start_date',
            'applied_to_cohort',
            'new_message',
            'before_enrollment_deadline',
            'expelled_manually',
            'cohort_application_status',
            'closed_by',
            'num_connections',
            'num_saved_for_later',
            'hiring_relationships',
            'connected_relationships',
            'relationships_saved_for_later',
            'honors',
            'cohort_end_date',
            'inviter_name',
            'num_periods_until_next_exam',
            'next_exam_style',
            'next_exam',
            'hiring_manager_name',
            'candidate_name',
            'previous_cohort_status',
            'has_created_an_open_position',
            'num_unarchived_open_positions',
            'num_new_candidates_interested',
            'num_total_candidates_interested',
            'period_description',
            'period_project_style',
            'period_learner_projects',
            'learner_projects',
            'applicant_name',
            'peer_recommender_email',
            'peer_recommendation_form_url',
            'target_cohort',
            'tweet_template',
            'cohort_slack_url',
            'due_date',
            'consider_early_decision',
            'curriculum_status',
            'exercises',
            'exercise',
            'request',
            'suggested_cohort',
            'backfilled_courses',
            'transferred_courses',
            'hiring_manager_closed_info',
            'num_hours_in_delay',
            'is_paid_cert',
            'project_submission_email',
            'matched_by_hiring_manager',
            's_range',
            'invoice_id',
            'amount',
            'metered_plan',
            'product_name',
            'invoice_number',
            'invoice_date',
            'hosted_invoice_url',
            'invoice_pdf',
            'statement_descriptor',
            'lines',
            'next_exam_info',
            'open_position_id',
            'open_position_title',
            'num_outstanding',
            'num_approved',
            'metered_plan',
            'recipient_email',
            'message',
            'deep_link_url',
            'candidate_avatar_url',
            'candidate_position_title',
            'candidate_position_company_name',
            'positions',
            'position_titles',
            'average_assessment_score_best',
            'to',
            'reply_to',
            'subject',
            'message_body',
            'admissions_decision',
            'scan_capture_url',
            'previous_admissions_decision',
            'rubric_inherited',
            'phone',
            'bad_links',
            'survey_years_full_time_experience',
            'survey_most_recent_role_description',
            'survey_highest_level_completed_education_description',
            'template_id',
            'document_type',
            'old_hiring_plan',
            'experiment_id',

            # These are needed by Facebook events (via Segment)
            'price',

            # These are needed for remote push notifications
            'device_token',
            'platform',
            'delivery_id',
            'device_id',
            'event',

            # Useful for Sentry debugging, but not necessary in Redshift
            'error_debug',
            'recentFormats',
            'textDetails',

            # id_verification stuff that is needed in customer.io but duplicates stuff in our db
            "id_verification_period_index",
            "verification_method",

            # set when transferring progress for a user who is moved from one cohort to another
            'midterm_complete',
            'final_complete',

            # hiring_application events
            'hiring_team_id',
            'hiring_plan',
            'old_status',
            'primary_subscription_stripe_plan_id',
            'has_primary_subscription',

            # payment_succeeded events
            'first_payment_for_subscription',

            # invoice.upcoming event
            'next_payment_attempt',

            # postion events
            'is_hiring_team_owner',
            'is_open_position_creator',
            'has_subscription',
            'archived',
            'featured',
            "open_position_review_path",
            "position_just_archived",
            "renewal_just_canceled",

            #stripe subscription events
            'current_period_end',
            'current_period_start',
            'cancel_at_period_end',

            # candidate_position_interests events
            'cover_letter',

            # only used in Segment/marketing integrations
            'primary_reason_for_applying'
        ]
    end

    def self.insert_sql(table, inserts)
        # upsert sql with returning clause
        "
            INSERT INTO #{table}
                (id, event_type, payload, user_id, hit_server_at, total_buffered_seconds, client_reported_time, estimated_time, created_at, updated_at)
            VALUES
                #{inserts.join(", ")}
            ON CONFLICT DO NOTHING
            RETURNING *
         "
    end

    def self.raise_on_unknown_properties?
        Rails.env.development?
    end

    def page_load_id
        self.payload.nil? ? nil : self.payload['page_load_id']
    end

    def server_event?
        self.payload && self.payload['server_event']
    end

    def log_to_external_systems(keys = nil, only_customer_io = true, log_to_customerio_priority = LogToCustomerIoJob::DEFAULT_PRIORITY, log_to_segmentio_priority = LogToSegmentIoJob::DEFAULT_PRIORITY)
        return if ENV['DISABLE_EVENT_LOGGING'] == 'true'

        # If explicity asking for customer.io, only log to customer.io.
        LogToSegmentIoJob.set(priority: log_to_segmentio_priority).perform_later(self.id, keys) unless only_customer_io
        LogToCustomerIoJob.set(priority: log_to_customerio_priority).perform_later(self.id, keys)
    end

    def process_after_saving_in_batch
        # These events are special-cased here because they kick off
        # delayed jobs outside of the log_to_external_systems scope.
        # log_to_external_systems implies a dependency on the event
        # being processed also being present in RDS. The jobs spawned
        # by these events do not require the event to be in RDS.
        if self.event_type == 'cordova:device-registered'
            ReconcileMobileDeviceJob.perform_later(
                self.user_id,
                self.payload['device_token'],
                self.payload['platform']
            )
        elsif self.event_type == 'cordova:notification-opened'
            TrackPushNotificationJob.perform_later(self.payload)
        end

        cache_page_load_event
    end

    def cache_page_load_event
        return if self.event_type != 'page_load:load'
        # We make this feature opt-in because as_redshift_event
        # can be slow in devmode.  See comment in there.
        if Rails.env.development? && ENV['UPDATE_USER_LESSON_PROGRESS_IN_DEV_MODE'] != 'true'
            return
        end

        # For now this is only used in UpdateUserLessonProgressFromEvents, but it could potentially
        # be used elsewhere eventually, so it is here and written in such a way that we could
        # add more columns if necessary.  We are caching the event as an instance of RedshiftEvent
        # because then it is consistent with cases where we can't find the event in the cache
        # and we actually have to pull it from redshift.

        # columns = Report::UserLessonProgressRecord::UpdateUserLessonProgressFromEvents::PAGE_LOAD_EVENT_COLUMNS
        # FIXME: replace the line below with the line above
        columns =  %w(id page_load_id event_type os_name client)

        redshift_event = self.as_redshift_event(columns)
        SafeCache.write("PageLoadEvent/#{self.payload['page_load_id']}", redshift_event)
    end

    def as_redshift_event(columns, ignore_bad_dev_connection: false)
        columns = columns.map(&:to_s)
        top_level_columns = columns & self.class.column_names
        payload_colums = columns - top_level_columns

        hash = self.attributes.slice(*top_level_columns)
        hash.merge!(self.payload.slice(*payload_colums))

        # This will raise a PG::ConnectionBad in devmode if the
        # redshift connection is not setup.  When the redshift connection
        # is set up, initializing
        # a redshift event requires a connection
        # to redshift. After starting up a new server it
        # require a query to get column information, which can take
        # more than 5 seconds.  So it's probably a good idea to make
        # it easy to avoid any functionality that needs to hit this
        # in devmode.
        return RedshiftEvent.new(hash)
    end

    class UnknownPropertyException < RuntimeError; end

end
