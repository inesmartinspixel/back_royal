# == Schema Information
#
# Table name: lesson_to_stream_joins
#
#  id        :uuid             not null, primary key
#  stream_id :uuid
#  lesson_id :uuid
#  position  :integer
#

class Lesson::ToStreamJoin < ApplicationRecord
    default_scope -> {order(:position)}
    belongs_to :lesson
    belongs_to :stream, :class_name => "Lesson::Stream"
    acts_as_list :scope => :stream

    def self.streams_for_lesson(lesson)
        # Note: the compact should no longer be necessary now that
        # we have dependent: destroy in the right places
        self.where(lesson_id: lesson.id).map(&:stream).compact
    end
end
