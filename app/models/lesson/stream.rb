# == Schema Information
#
# Table name: lesson_streams
#
#  id                     :uuid             not null, primary key
#  created_at             :datetime
#  updated_at             :datetime
#  title                  :string(255)
#  description            :text
#  author_id              :uuid
#  image_id               :uuid
#  chapters               :json
#  modified_at            :datetime
#  pinned                 :boolean          default(FALSE), not null
#  pinned_title           :string(255)
#  pinned_description     :text
#  was_published          :boolean          default(FALSE), not null
#  last_editor_id         :uuid
#  entity_metadata_id     :uuid
#  credits                :text
#  what_you_will_learn    :string           default([]), is an Array
#  resource_downloads     :json
#  resource_links         :json
#  recommended_stream_ids :uuid             default([]), is an Array
#  related_stream_ids     :uuid             default([]), is an Array
#  coming_soon            :boolean          default(FALSE), not null
#  just_added             :boolean          default(FALSE), not null
#  summaries              :json
#  beta                   :boolean          default(FALSE), not null
#  just_updated           :boolean          default(FALSE), not null
#  locale_pack_id         :uuid
#  locale                 :text             default("en"), not null
#  duplicated_from_id     :uuid
#  duplicated_to_id       :uuid
#  unlocked_for_mba_users :boolean          default(FALSE)
#  exam                   :boolean          default(FALSE), not null
#  time_limit_hours       :float
#  exam_open_time         :datetime
#  exam_close_time        :datetime
#  tag                    :text
#

class Lesson::Stream < ApplicationRecord

    default_scope -> {order(:created_at)}

    include Skylight::Helpers
    include IsContentItem

    has_many :lesson_joins, :class_name => 'Lesson::ToStreamJoin', :dependent => :destroy
    has_many :lessons, ->  { reorder('lesson_to_stream_joins.position') }, :through => :lesson_joins
    belongs_to :image, :class_name => 'S3Asset', optional: true

    belongs_to :entity_metadata, optional: true
    belongs_to :locale_pack, :class_name => Lesson::Stream::LocalePack.name, :foreign_key => :locale_pack_id, optional: true

    # this can't happen until after save, so you'd better be saving in a transaction
    set_callback :publish_change, :after, :raise_unless_all_playlists_including_stream_are_valid
    after_destroy :raise_unless_all_playlists_including_stream_are_valid
    after_save :raise_unless_all_playlists_including_stream_are_valid

    validate :validate_summaries_have_lessons

    class ValidatesChapters < ActiveModel::EachValidator
    include Skylight::Helpers

        instrument_method
        def validate_each(record, attribute, chapters)
            if !chapters.nil?
                chapters.each_with_index do |chapter, i|
                    validate_chapter record, attribute, chapter, i
                end
            end
        end

        instrument_method
        def validate_chapter(record, attribute, chapter, i)
            # must have a title
            if chapter["title"].blank?
                record.errors.add(:"#{attribute}_#{i+1}", "must have a title")
            end

            # must have a list of lessons
            lesson_ids = chapter["lesson_ids"]
            # lesson_hashes are new, but will eventually replace lesson_ids
            lesson_hashes = chapter["lesson_hashes"]

            if lesson_hashes.nil?
                record.errors.add(:"#{attribute}_#{i+1}", "must have a list of lesson_hashes")
            end

            if lesson_ids.uniq != lesson_ids
                record.errors.add(:"#{attribute}_#{i+1}_lesson_ids", "must not contain any duplicates")
            end

            if lesson_ids.nil?
                record.errors.add(:"#{attribute}_#{i+1}", "must have a list of lesson_ids")
            else
                # each lesson must be a valid lesson_id
                lesson_ids.each do |lesson_id|
                    # Check whether the chapter lesson_ids are related to existing lessons already associated with this stream/course
                    if !record.lessons.map(&:id).include?(lesson_id)
                        record.errors.add(:"#{attribute}_#{i+1} lesson_id #{lesson_id}", "must be the id of a lesson contained in this course")
                    end
                end

                # if there's a lesson_hashes, ensure that it matches the lesson_ids array
                if !lesson_hashes.nil?
                    if lesson_hashes.map {|h| h["lesson_id"] } != lesson_ids
                        record.errors.add(:"#{attribute}_#{i+1}", "lesson_hashes must match lesson_ids")
                    end
                end
            end
        end

    end
    validates_with Lesson::Stream::ValidatesChapters, _merge_attributes([:chapters])

    def self.prepare_params_for_duplication(hash_for_original_item, hash_for_new_item, user, meta)
        params = hash_for_original_item.except('beta', 'coming_soon', 'just_added', 'just_updated')

        if meta[:in_same_locale_pack]
            target_locale = hash_for_new_item['locale']
            lesson_map = {}
            hash_for_original_item['lessons'].each do |lesson_hash|
                existing_lesson_entry = lesson_hash['locale_pack'] && lesson_hash['locale_pack']['content_items'].detect { |entry| entry['locale'] == target_locale}

                if existing_lesson_entry.nil?
                    target_lesson_id = Lesson.duplicate!(user, {
                        title: "Duplicated from '#{lesson_hash['title']}'",
                        locale: target_locale
                    }, {
                        in_same_locale_pack: true,
                        duplicate_from_id: lesson_hash['id'],
                        duplicate_from_updated_at: lesson_hash['updated_at']
                    }).id
                else
                    target_lesson_id = existing_lesson_entry['id']
                end

                lesson_map[lesson_hash['id']] = target_lesson_id
            end

            params['lessons'] = params['lessons'].map do |entry|
                {'id' => lesson_map[entry['id']]}
            end

            params['chapters'].each do |chapter|
                chapter['lesson_hashes'] = chapter['lesson_hashes'].map do |entry|
                    id = lesson_map[entry['lesson_id']]
                    entry.merge({'lesson_id' => id})
                end
                chapter['lesson_ids'] = chapter['lesson_ids'].map do |id|
                    lesson_map[id]
                end
            end
        end

        params

    end

    def self.progress_class
        Lesson::StreamProgress
    end

    def self.event_attributes(locale_pack_id, locale)
        streams = Lesson::Stream::JsonCache.get_attrs_for_locale_pack_ids([locale_pack_id]).index_by do |stream_attrs|
            [stream_attrs['locale_pack']['id'], stream_attrs['locale']]
        end

        english_stream = streams[[locale_pack_id, 'en']]
        localized_stream = streams[[locale_pack_id, locale]]

        {
            english_title: english_stream['title'],
            title_in_users_locale: (localized_stream || english_stream)['title'],
            english_url: english_stream['entity_metadata']['canonical_url'],
            url_in_users_locale: (localized_stream || english_stream)['entity_metadata']['canonical_url'],
            exam: english_stream['exam']
        }
    end

    def self.published_lesson_locale_pack_ids(locale_pack_id)
        stream_jsons = Lesson::Stream::ToJsonFromApiParams.new(
            fields: ['lessons'],
            lesson_fields: ['locale_pack'],
            filters: {
                locale_pack_id: locale_pack_id,
                published: true
            }
        ).to_a

        lesson_jsons = stream_jsons.map { |stream| stream['lessons'] }.flatten
        lesson_jsons.map { |lesson| lesson['locale_pack']['id'] }.uniq
    end

    # merge in a hash with json versions of author, image, and lessons
    instrument_method
    def merge_hash(hash, meta = {})
        hash = hash.unsafe_with_indifferent_access

        hash.slice('tag', 'description', 'chapters', 'credits', 'what_you_will_learn', 'recommended_stream_ids',
            'related_stream_ids', 'resource_downloads', 'resource_links', 'summaries', 'coming_soon',
            'just_added', 'beta', 'just_updated', 'unlocked_for_mba_users', 'exam', 'time_limit_hours'
        ).each do |key, value|
            self[key] = value
        end

        hash.slice('exam_open_time', 'exam_close_time').each do |key, value|
            self[key] = value.nil? ? value : Time.at(value)
        end

        # acts_as_list will not reset positions unless we blow away the
        # list and re-enter lessons one-by-one
        if hash.key?('lessons')
            self.lessons = []
            lesson_hashes = hash['lessons'] || []
            lesson_hashes.map do |lesson_hash|
                lesson = Lesson.find(lesson_hash['id']) || raise(LessonNotFound.new(lesson_hash['id']))
                self.lessons << lesson
            end
        end

        if hash.key?('image')
            id = hash['image'] && hash['image']['id']
            self.image = id.nil? ? nil : S3Asset.find(id)
        end

        if hash.key?('locale_pack') && !hash['locale_pack'].nil? && hash['locale_pack'].key?('content_topics')
            ContentTopic.update_topics(self, hash['locale_pack']['content_topics'])
        end

        if hash.key?('certificate_image')
            self.certificate_image = S3Asset.find(hash['certificate_image']['id'])
        end

        self.chapters ||= []

    end

    instrument_method
    def get_image_asset(file)
        S3Asset.new({
            file: file,
            directory: "images/",
            :styles => {

                # student dashboard
                "50x50"  => '50x50>',
                "100x100"  => '100x100>',
                "150x150"  => '150x150>',

                # stream dashboard
                "110x110"   => '110x110>',
                "220x220"   => '220x220>',
                "330x330"   => '330x330>'
            }.to_json
        })
    end

    # FIXME: Lesson#as_json has special handling when as_jsonning a version.  We
    # should probably do the same here
    instrument_method
    def as_json(options = {})
        raise "Cannot as_json an unsaved stream" if changed?
        options = options.with_indifferent_access
        options[:filters] ||= {}
        filters = options[:filters]
        filters[:published] = false unless [true, "true"].include?(filters[:published])
        json = Lesson::Stream::ToJsonFromApiParams.new(options.merge(:id => self.id)).json
        ActiveSupport::JSON.decode(json).first
    end

    # only used in specs
    def add_lesson_to_chapter!(lesson, chapter)
        chapter['lesson_ids'] << lesson.id
        chapter['lesson_hashes'] << {'lesson_id' => lesson.id, 'coming_soon' => false}
        self.lessons << lesson
        self.save!
        self
    end

    def remove_lesson!(lesson)
        # in tests, chapters is intermittently nil
        unless chapters.nil?
            chapters.each do |chapter|
                chapter['lesson_ids'].delete(lesson.id) if chapter['lesson_ids']
                chapter['lesson_hashes'].delete_if { |h| h['lesson_id'] == lesson.id } if chapter['lesson_hashes']
            end
        end

        # this is necessary so that activerecord knows that something
        # inside the json field had changed
        self.chapters_will_change!
        self.lessons.delete(lesson)
        save!
    end

    def lesson_ids_from_chapters
        (self.chapters || []).pluck('lesson_ids').flatten.uniq
    end

    def playlists_including_stream
        Playlist.all.select { |p| p.stream_locale_pack_ids.include?(self.locale_pack_id) }
    end

    def raise_unless_all_playlists_including_stream_are_valid
        playlists_including_stream.each do |playlist|
            published_version = playlist.published_version

            # check the published version
            unless published_version.nil? || published_version.valid?
                published_version.errors.full_messages.each do |message|
                    errors.add(:"playlist_#{playlist.title.underscore}", message)
                end
            end

            # and check the working version
            unless playlist.nil? || playlist.valid?
                playlist.errors.full_messages.each do |message|
                    errors.add(:"playlist_#{playlist.title.underscore}", message)
                end
            end
        end
        unless errors.empty?
            raise ActiveRecord::RecordInvalid.new(self)
        end
    end

    def validate_summaries_have_lessons
        if summaries&.any?
            summaries.each do |summary|
                errors.add(:summary, "'#{summary['title']}' must have lessons added to it.") unless summary['lessons'].any?
            end
        end
    end

    def get_exam_timestamps(user)
        return {} unless published_version && published_version.exam
        return {} unless user.relevant_cohort
        return {} if user.accepted_application && user.accepted_application.disable_exam_locking

        cohort_id = user.relevant_cohort.attributes['id']

        # since the same cohort will always return the same values, we cache it.  That
        # way if you call exam_open_time(some_user) and then exam_close_time(some_user)
        # it only makes one query
        @exam_times ||= {}

        # this query will only return one row, since each stream can only be required
        # once in a particular cohort's schedule
        #
        # Note that this logic is duplicated in Lesson::Stream#get_exam_timestamps
        @exam_times[cohort_id] ||= ActiveRecord::Base.connection.execute("
            select
                (extract(epoch from period_start))::int as exam_open_time
            from
                published_cohort_periods
                    join published_cohort_periods_stream_locale_packs
                        on published_cohort_periods_stream_locale_packs.cohort_id = published_cohort_periods.cohort_id
                        and published_cohort_periods_stream_locale_packs.index = published_cohort_periods.index
            where
                published_cohort_periods.cohort_id = '#{cohort_id}'
                and published_cohort_periods_stream_locale_packs.locale_pack_id = '#{locale_pack_id}'
                and published_cohort_periods_stream_locale_packs.required = true
        ").to_a.first || {}
    end

    # exam_open_time and #get_exam_timestamps are two separate methods because we used to
    # also have exam_close_time
    def exam_open_time(user)
        exam_open_timestamp = get_exam_timestamps(user)['exam_open_time']
        exam_open_timestamp.nil? ? nil : Time.at(exam_open_timestamp)
    end

    def editor_link(site = 'https://smart.ly')
        "#{site}/editor/course/#{id}/edit"
    end

    class LessonNotFound < RuntimeError

        def initialize(finder)
            super("No lesson found for #{finder.inspect}")
        end

    end

    class AuthorNotFound < RuntimeError

        def initialize(author_email)
            super("No user found for #{author_email.inspect}")
        end

    end

    class InvalidImage < RuntimeError

        def initialize(image_path)
            super("#{image_path.inspect} is not a valid image")
        end

    end

end
