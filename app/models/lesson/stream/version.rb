# == Schema Information
#
# Table name: lesson_streams_versions
#
#  version_id             :uuid             not null, primary key
#  operation              :string(1)        not null
#  version_created_at     :datetime         not null
#  id                     :uuid
#  created_at             :datetime
#  updated_at             :datetime
#  title                  :string
#  description            :text
#  author_id              :uuid
#  image_id               :uuid
#  chapters               :json
#  modified_at            :datetime
#  pinned                 :boolean
#  pinned_title           :string
#  pinned_description     :text
#  was_published          :boolean
#  last_editor_id         :uuid
#  entity_metadata_id     :uuid
#  credits                :text
#  what_you_will_learn    :string           is an Array
#  resource_downloads     :json
#  resource_links         :json
#  recommended_stream_ids :uuid             default([]), is an Array
#  related_stream_ids     :uuid             default([]), is an Array
#  coming_soon            :boolean
#  just_added             :boolean
#  summaries              :json
#  beta                   :boolean
#  just_updated           :boolean
#  locale_pack_id         :uuid
#  locale                 :text
#  duplicated_from_id     :uuid
#  duplicated_to_id       :uuid
#  unlocked_for_mba_users :boolean          default(FALSE)
#  exam                   :boolean
#  time_limit_hours       :float
#  exam_open_time         :datetime
#  exam_close_time        :datetime
#  tag                    :text
#  version_editor_id      :uuid
#  version_editor_name    :text
#

class Lesson::Stream::Version < Lesson::Stream

    include(ContentItemVersionMixin)

    self.primary_key = 'version_id'
    self.table_name = 'lesson_streams_versions'

    belongs_to :content_publisher, :class_name => 'ContentPublisher', :primary_key => :lesson_stream_version_id, :foreign_key => :version_id
    belongs_to :lesson_stream, :foreign_key => 'id', :primary_key => 'id' , :class_name => 'Lesson::Stream'

    delegate :published_at, :published_version_id, :to => :lesson_stream

end
