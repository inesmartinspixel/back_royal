require 'back_royal/object' # looks_like_uuid?

class Lesson::Stream::ToJsonFromApiParams

    class UnauthorizedError < RuntimeError; end

    include ToJsonFromApiParamsHelpers::ToJsonFromApiParamsMixin
    include ToJsonFromApiParamsHelpers::ContentItemMixin
    include ToJsonFromApiParamsHelpers::StreamsAccessMixin

    self.base_klass = Lesson::Stream

    attr_reader :user_id, :user, :ability

    def initialize(params, ability = nil)

        @ability = ability

        params = do_basic_params_prep(params)

        @query_params = params.slice(
            :include_progress,
            :load_full_content_for_lesson_id,
            :load_full_content_for_all_lessons,
            :load_full_content_for_stream_ids,
            :include_tags,
            :id,
            :user,
            :limit,
            :fields,
            :lesson_fields
        ).with_indifferent_access

        set_filters({
            user_can_see: :bool,
            in_users_locale_or_en: :bool,
            in_locale_or_en: :locale,
            view_as: /^[\w:\w\-\s]+$/,
            favorite: :bool,
            published: :bool,
            complete: :bool,
            id: :array_of_uuids,
            locale_pack_id: :array_of_uuids,
            locale: :locale,
            updated_since: :number,
            updated_before: :number,
            exam: :bool,
            version_id: :array_of_uuids
        })

        # if no user_id is passed in, and lesson_streams_progress was
        # not explicitly requested, then assume it was not desired
        params[:except] ||= []
        if !params[:user] && !(params[:fields] || []).include?('lesson_streams_progress')
            params[:except] << 'lesson_streams_progress'
        end

        fields = set_fields({
            available_fields: %w|
                id version_id tag title description modified_at updated_at
                author image favorite lessons entity_metadata
                chapters was_published published_at published_version_id
                lesson_streams_progress credits what_you_will_learn
                recommended_stream_ids related_stream_ids resource_downloads
                resource_links summaries coming_soon just_added beta just_updated
                locale locale_pack topics unlocked_for_mba_users exam time_limit_hours
            |,
            aliases: {
                UPDATE_FIELDS: ['updated_at', 'published_at', 'modified_at']

            }
        })

        @user = @query_params[:user]
        @user_id = @user.id unless !user

        if @user.nil?
            raise UnauthorizedError.new("user must be provided in order to include progress") if @query_params[:include_progress]
            raise ArgumentError.new("user must be provided in order to filter by 'favorite'") if @filters.key?(:favorite)
        end

        if params.key?(:published)
            raise ArgumentError.new("published no longer supported.  User filters[:published]")
        end

        @query_builder = CrazyQueryBuilder.new('lesson_streams_versions')
        @selects = @query_builder.selects
        @joins = @query_builder.joins
        @wheres = @query_builder.wheres
        @withs = @query_builder.withs
        @query_builder.limit = @limit = @query_params[:limit]

        handle_content_item_filters
        handle_access_filters

        if @query_params[:include_progress]
            join_and_select_stream_progress
            # see also the methods for selecting lessons
        end

        if @filters.key?(:complete)
            filter_by_complete
        end

        if @filters.key?(:exam)
            filter_by_exam
        end

        set_default_selects
        handle_content_item_selects

        @selects.slice!(*fields)

        if @selects.empty?
            raise "No selects provided. params: #{@query_params.inspect}"
        end

    end

    # making this a hash instead of an array so that in the
    # future the api can support filtering in/out specific fields
    def set_default_selects
        @selects['id'] = 'lesson_streams_versions.id'
        @selects['version_id'] = 'lesson_streams_versions.version_id'
        @selects['tag'] = 'lesson_streams_versions.tag'
        @selects['title'] = 'lesson_streams_versions.title'
        @selects['description'] = 'lesson_streams_versions.description'
        @selects['modified_at'] = 'cast(EXTRACT(EPOCH FROM lesson_streams_versions.modified_at) as int)'
        @selects['updated_at'] = 'cast(EXTRACT(EPOCH FROM lesson_streams_versions.updated_at) as int)'
        @selects['credits'] = 'lesson_streams_versions.credits'
        @selects['what_you_will_learn'] = 'lesson_streams_versions.what_you_will_learn'
        @selects['recommended_stream_ids'] = 'lesson_streams_versions.recommended_stream_ids'
        @selects['related_stream_ids'] = 'lesson_streams_versions.related_stream_ids'
        @selects['resource_downloads'] = 'lesson_streams_versions.resource_downloads'
        @selects['resource_links'] = 'lesson_streams_versions.resource_links'
        @selects['summaries'] = 'lesson_streams_versions.summaries'
        @selects['coming_soon'] = 'lesson_streams_versions.coming_soon'
        @selects['just_added'] = 'lesson_streams_versions.just_added'
        @selects['beta'] = 'lesson_streams_versions.beta'
        @selects['just_updated'] = 'lesson_streams_versions.just_updated'
        @selects['locale'] = 'lesson_streams_versions.locale'
        @selects['exam'] = 'lesson_streams_versions.exam'
        @selects['time_limit_hours'] = 'lesson_streams_versions.time_limit_hours'
        select_author
        join_and_select_image
        @selects['was_published'] = 'lesson_streams_versions.was_published'
        @selects['unlocked_for_mba_users'] = 'lesson_streams_versions.unlocked_for_mba_users'

        join_and_select_favorite
        join_and_select_lessons
        join_and_select_entity_metadata

        # Note: we used to filter unpublished lessons out of the chapters[0]['lesson_ids']
        # array server-side, but now we handle this client-side
        @selects['chapters'] = 'lesson_streams_versions.chapters'
    end

    def filter_by_complete
        if @filters[:complete] != false
            raise ArgumentError.new('false is the only value supported for filters[:complete]')
        end
        join_stream_progress
        @wheres << "(prepared_lesson_streams_progress IS NULL OR prepared_lesson_streams_progress.completed_at IS NULL)"
    end

    def filter_by_exam
        if @filters[:exam] != true
            raise ArgumentError.new('true is the only value supported for filters[:exam]')
        end
        @wheres << "(lesson_streams_versions.exam = TRUE)"
    end

    def join_and_select_favorite
        if @filters.key?(:favorite)
            raise ArgumentError.new('filters[:favorite] only accepts the value \'true\'') unless @filters[:favorite] == true
            filter_by_favorite = true
        end

        # we checked above to make sure no one ever tries
        # to filter by favorite when user_id is not provided
        if @user_id.nil?
            return
        end

         @joins << "LEFT OUTER JOIN lesson_stream_locale_packs_users as favorited_lesson_stream_locale_packs
                    ON favorited_lesson_stream_locale_packs.user_id = '#{@user_id}'
                        AND favorited_lesson_stream_locale_packs.locale_pack_id = lesson_streams.locale_pack_id"
        @selects['favorite'] = 'favorited_lesson_stream_locale_packs.locale_pack_id IS NOT NULL'

        if filter_by_favorite
            @wheres << 'favorited_lesson_stream_locale_packs.locale_pack_id IS NOT NULL'
        end
    end

    def join_and_select_lessons

        fields = @query_params[:lesson_fields] || ["id", "lesson_type", "frame_count", "description",
              "title", "seo_canonical_url", "seo_description",
              "seo_title", "client_requirements", "key_terms",
              "seo_image", "tag", "entity_metadata", 'assessment',
              'version_id', 'unrestricted', "updated_at", "locale_pack", "locale", "test"
        ]
        fields << "lesson_progress" if @query_params[:include_progress]

        published = filters.key?(:published) ? filters[:published] : true

        # in order to support pushing changes to lesson content, we need to
        # include the published_at in the result.  BUT, doing that destroys the
        # query for all working versions at /editor?section=courses, so we
        # only include this field conditionally
        fields << 'published_at' if published

        lessons_to_json = Lesson::ToJsonFromApiParams.new({
            user_id: @user_id,
            fields: fields,
            filters: {
                published: published
            }
        })

        lessons_to_json.wheres << "
            (lessons_versions.id IN
                -- since json_array_elements can only
                -- create things of type json, the lesson_ids
                -- are json. convert them to text and then trim
                -- the quote from the beginning and the end
                (select CAST(trim(cast(lesson_id as text), '\"') as uuid) from (

                    -- use json_array_elements again to convert
                    -- lesson_ids into one row for each lesson id
                    SELECT
                        json_array_elements((chapter->>'lesson_ids')::json) lesson_id
                    FROM
                        -- use json_array_elements to
                        -- convert chapters json for stream
                        -- into one row for each chapter
                        (SELECT json_array_elements(lesson_streams_versions.chapters) chapter) chapters
                ) lessons_for_stream)
            )
        "

        if lesson_id = @query_params[:load_full_content_for_lesson_id]
            lessons_to_json.selects["frames"] = "
                CASE
                    WHEN lessons_versions.id = '#{lesson_id}'
                        THEN (lessons_versions.content_json->>'frames')::json
                    ELSE
                        NULL
                    END
            "
        end

        stream_ids = @query_params[:load_full_content_for_stream_ids]
        if stream_ids&.any?
            id_string = stream_ids.map { |id| "'#{id}'" }.join(',')
            lessons_to_json.selects["frames"] = "
                CASE
                    WHEN lesson_streams_versions.id in (#{id_string})
                        THEN (lessons_versions.content_json->>'frames')::json
                    ELSE
                        NULL
                    END
            "
        end

        if @query_params[:load_full_content_for_all_lessons]
            lessons_to_json.selects["frames"] = "(lessons_versions.content_json->>'frames')::json"
        end

        @selects['lessons'] = lessons_to_json.to_array_subquery
    end

    def join_stream_progress
        return if @joined_stream_progress
        @joined_stream_progress = true
        # By doing this as a WITH, we are probably loading up
        # all of the lesson_streams_progress records for this user,
        # even if the larger query does not use some of those streams.  Since
        # there will not be very many lesson_streams_progress records for
        # a user, and since in most case most of them will be used, this
        # seems okay.  It seems like we should just be able to do a regular
        # old join, but we actually cannot because then we can't get the correct
        # field names into the lesson_streams_progress json.  We could also
        # get the correct fields names using a subselect (as we do with authors above),
        # or with a custom row type, but there are drawbacks to those solutions as well.
        @joins << "LEFT OUTER JOIN prepared_lesson_streams_progress
                    ON (lesson_streams.locale_pack_id = prepared_lesson_streams_progress.locale_pack_id)"
        @withs << "
            prepared_lesson_streams_progress AS MATERIALIZED (
                SELECT
                    lesson_streams_progress.user_id as user_id,
                    lesson_streams_progress.locale_pack_id as locale_pack_id,
                    lesson_streams_progress.lesson_bookmark_id as lesson_bookmark_id,
                    cast(EXTRACT(EPOCH FROM lesson_streams_progress.started_at) as int) as started_at,
                    cast(EXTRACT(EPOCH FROM lesson_streams_progress.completed_at) as int) as completed_at,
                    cast(EXTRACT(EPOCH FROM lesson_streams_progress.updated_at) as int) as last_progress_at,
                    cast(EXTRACT(EPOCH FROM lesson_streams_progress.updated_at) as int) as updated_at,
                    cast(EXTRACT(EPOCH FROM lesson_streams_progress.created_at) as int) as created_at,
                    lesson_streams_progress.completed_at IS NOT NULL as complete,
                    lesson_streams_progress.id as id,
                    lesson_streams_progress.official_test_score as official_test_score,
                    lesson_streams_progress.waiver as waiver,
                    (
                        CASE WHEN lesson_streams_progress.time_runs_out_at IS NOT NULL THEN
                            cast(EXTRACT(EPOCH FROM lesson_streams_progress.time_runs_out_at) as int)
                        ELSE
                            NULL
                        END
                    ) as time_runs_out_at,
                    (
                        CASE WHEN certificate_images.id IS NOT NULL THEN
                                row_to_json(
                                    cast(
                                        row(
                                            certificate_images.id,
                                            certificate_images.formats,
                                            certificate_images.dimensions
                                        )
                                    as image_json_v1)
                                )
                        ELSE
                            NULL
                        END
                    ) AS certificate_image
                FROM lesson_streams_progress
                LEFT OUTER JOIN s3_assets certificate_images
                    ON lesson_streams_progress.certificate_image_id = certificate_images.id
                WHERE
                user_id = '#{@user_id}'
            )
        "
    end

    def join_and_select_stream_progress
        join_stream_progress
        @selects['lesson_streams_progress'] = "row_to_json(prepared_lesson_streams_progress.*)"
    end

    def locale_pack_content_topics_subquery
        locale_packs_table = "#{base_table_name.singularize}_locale_packs"
        join_table_name = "content_topics_#{locale_packs_table}"
        foreign_key = "locale_pack_id"

        "
            -- FIXME: comments
            -- convert the desired columns for each group into json (row_to_json),
            -- group all the json values
            -- into an array (array_agg), then convert the array to json (array_to_json)
            COALESCE(
                (SELECT array_to_json(array_agg(row_to_json(prepared_content_topics))) FROM
                    (
                        -- select the desired columns from groups
                        SELECT
                            content_topics.id,
                            content_topics.locales
                        FROM content_topics
                            JOIN #{join_table_name} on content_topics.id = content_topics_#{locale_packs_table}.content_topic_id
                        WHERE #{join_table_name}.#{foreign_key}=locale_packs.id
                    ) prepared_content_topics
                ),
                '[]'::json
            )"
    end

    def execute

        result = ActiveRecord::Base.connection.execute(@query_builder.to_json_sql)

        json = result.to_a[0]["json"] || "[]"

        lesson_id = @query_params[:load_full_content_for_lesson_id]
        stream_ids = @query_params[:load_full_content_for_stream_ids] || []
        if lesson_id || stream_ids.any? || @query_params[:load_full_content_for_all_lessons]
            decoded = ActiveSupport::JSON.decode(json)
            decoded.map do |stream|
                lesson_entries = stream['lessons'].flatten

                lesson_entries.each do |lesson|
                    if lesson['id'] == lesson_id || stream_ids.include?(stream['id']) || @query_params[:load_full_content_for_all_lessons]
                        unless self.ability.can?(:load_full_content_for_lesson_id, {stream: stream, lesson: lesson})
                            raise UnauthorizedError.new("Cannot load full content for lessons in stream #{stream['id']}")
                        end
                        Lesson::Content::FrameList.decorate_frame_json(lesson['id'], lesson['frames'])
                    end
                end
            end


            json = decoded.to_json
        end

        json
    end

end
