# == Schema Information
#
# Table name: lesson_stream_locale_packs
#
#  id         :uuid             not null, primary key
#  created_at :datetime
#  updated_at :datetime
#

class Lesson::Stream::LocalePack < ApplicationRecord
    include LocalePackMixin
    include Groupable
    validate :validate_in_superviewer_group

    set_content_item_klass(Lesson::Stream)

    has_and_belongs_to_many :users, :foreign_key => :locale_pack_id
    has_and_belongs_to_many :content_topics, :foreign_key => :locale_pack_id

end
