module Lesson::Stream::JsonCache

    # note that this will return all the different locales for the
    # stream that is passed in
    def self.get_attrs_for_locale_pack_ids(locale_pack_ids)
        jsons = get_jsons_for_locale_pack_ids(locale_pack_ids)

        jsons.map do |json|
            ActiveSupport::JSON.decode(json)
        end
    end

    def self.get_stream_jsons_for_version_ids(version_ids)
        content_views_refresh_updated_at = ContentViewsRefresh.first&.updated_at
        version_ids_that_need_to_be_loaded_from_db = []

        # read whatever values from the cache we can
        stream_jsons = version_ids.map do |version_id|
            cached_value = SafeCache.read(stream_cache_key(version_id, content_views_refresh_updated_at))
            if cached_value.nil?
                version_ids_that_need_to_be_loaded_from_db << version_id
            end
            cached_value
        end.compact

        # anything that was not cached gets loaded up in one db call,
        # and then cached
        if version_ids_that_need_to_be_loaded_from_db.any?
            entries = Lesson::Stream::ToJsonFromApiParams.new({
                filters: {
                    version_id: version_ids_that_need_to_be_loaded_from_db
                }
            }).to_a

            entries.each do |entry|
                json = entry.to_json
                version_id = entry['version_id']

                # We expire these once a day so that the polling data can get updated from
                # time to time.
                #
                # NOTE: Content topics are embedded in the lessons.  Since we are caching the lessons
                # we could conceivably fail to get an update to content topics.  This is not a practical
                # issue right now because the content topics are only used in the library and this
                # caching is not used in the library.  But it could become an issue in the future.
                SafeCache.write(stream_cache_key(version_id, content_views_refresh_updated_at), json, expires_in: 1.day)
                stream_jsons << json
            end
        end

        stream_jsons
    end

    private
    def self.get_jsons_for_locale_pack_ids(locale_pack_ids)
        # FIXME: this can be cached as well (if we're gonna cache, we probably
        # want to cache the final Hash, so we don't have to do the JSON.decode
        # over and over. See above in get_attrs_for_locale_pack_ids)
        version_ids = Lesson::Stream.all_published
            .where(locale_pack_id: locale_pack_ids)
            .reorder(nil)
            .distinct
            .pluck("lesson_streams_versions.version_id")

        get_stream_jsons_for_version_ids(version_ids)
    end

    private
    def self.stream_cache_key(version_id, content_views_refresh_updated_at)
        # FIXME: We probably should not include the content_views_refresh_updated_at in this key.  That
        # means that any time ANYTHING gets published, everything gets uncached.  This is necessary at the
        # moment because lessons are embedded in streams.  If we didn't do this, then you could re-publish a
        # lesson and it's old version would still be returned inside of the cached stream.  The correct thing
        # to do is probably to use a centralized cache and when a lesson gets published, to remove any related
        # streams from the cache.
        "stream_json/#{version_id}/#{content_views_refresh_updated_at}"
    end
end