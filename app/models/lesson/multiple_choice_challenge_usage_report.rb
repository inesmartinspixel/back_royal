# == Schema Information
#
# Table name: multiple_choice_challenge_usage_reports
#
#  id              :uuid             not null, primary key
#  lesson_id       :uuid
#  frame_id        :uuid
#  editor_template :string(255)
#  challenge_id    :uuid
#  answer_id       :uuid
#  count           :integer          default(0)
#  created_at      :datetime
#  updated_at      :datetime
#  last_answer_at  :datetime
#

class Lesson::MultipleChoiceChallengeUsageReport < ApplicationRecord

    default_scope -> {order(:created_at)}

    self.table_name = 'multiple_choice_challenge_usage_reports'
    belongs_to :lesson, optional: true

    validates_presence_of :lesson_id
    validates_presence_of :frame_id
    validates_presence_of :editor_template
    validates_presence_of :challenge_id
    validates_presence_of :answer_id
    validates_presence_of :count

end
