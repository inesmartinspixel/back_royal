# == Schema Information
#
# Table name: lesson_streams_progress
#
#  id                   :uuid             not null, primary key
#  created_at           :datetime
#  updated_at           :datetime
#  lesson_bookmark_id   :uuid
#  started_at           :datetime
#  completed_at         :datetime
#  user_id              :uuid
#  certificate_image_id :uuid
#  locale_pack_id       :uuid             not null
#  time_runs_out_at     :datetime
#  official_test_score  :float
#  waiver               :text
#

class Lesson::StreamProgress < ApplicationRecord

    # See also stream_progress.js
    WAIVER_EQUIVALENT_STREAM_ALREADY_COMPLETED='equivalent_stream_already_completed'
    WAIVER_EXAM_ALREADY_COMPLETED='exam_already_completed'

    # used in a rake task used for debugging. see user.rake
    cattr_accessor :turn_off_cert_generation
    attr_accessor :skip_exam_events, :relevant_cohort_completion_percentage

    default_scope -> {order(:created_at)}

    self.table_name =  "lesson_streams_progress" # rails wanted to call this 'lesson_streams_progresses' so we override
    belongs_to :locale_pack, :class_name => "Lesson::Stream::LocalePack", :foreign_key => 'locale_pack_id'
    has_many :lesson_streams, :through => :locale_pack, :class_name => "Lesson::Stream", :source => :content_items
    belongs_to :user

    has_many :published_content_titles, :class_name => "Report::JoinablePublishedContentTitle", :foreign_key => :locale_pack_id, :primary_key => :locale_pack_id

    after_destroy :log_reset_event
    after_save :handle_started_foundations_playlist_event, if: :saved_change_to_created_at?
    after_save :handle_completed_foundations_events, if: :saved_change_to_completed_at?
    after_save :set_final_score, if: :saved_change_to_completed_at?
    after_save :mark_application_completed, if: :just_completed_curriculum? # must come after cohort_application#final_score are set in lesson_progress
    after_save :log_curriculum_complete_event, if: :just_completed_curriculum? # must come after cohort_applicationfinal_score are set in lesson_progress
    after_save :handle_exam_events
    after_save :save_relevant_application # save any changes made to the application in lifecycle hooks
    before_save :set_official_test_score
    before_save :set_relevant_cohort_completion_percentage

    # the user-specific customized certificate image, generated on course completion
    belongs_to :certificate_image, :class_name => 'S3Asset', optional: true

    validates_presence_of :locale_pack_id
    validates_presence_of :user_id

    # if a row exists in lesson_streams_progress, then the user
    # must have started the stream and so we'll have a started_at
    validates_presence_of :started_at

    class NoFinalScoreOnCompleteApplication < RuntimeError; end

    # in-memory getter/setter for associated lesson_progress so we can fold this into stream_progress before
    # json serialization, but without incurring a query per stream
    def lesson_progress
        @lesson_progress
    end
    def lesson_progress=(lesson_progress)
        @lesson_progress = lesson_progress
    end

    def lesson_stream
        unless Rails.env.test?
            begin
                # we're not handling locales correctly below
                raise RuntimeError.new("Do not use Lesson::StreamProgress#lesson_stream in the wild")
            rescue RuntimeError => err
                Raven.capture_exception(err)
            end
        end
        if @lesson_stream && @lesson_stream.locale != user.pref_locale
            @lesson_stream = nil
        end
        @lesson_stream ||= lesson_streams.where(locale: user.pref_locale).first
    end

    def for_exam_stream?
        stream = published_stream_for_exam_info
        stream ? stream.exam : false
    end

    def time_limit_hours
        published_stream_for_exam_info && published_stream_for_exam_info.time_limit_hours
    end

    def time_has_run_out?
        time_runs_out_at && time_runs_out_at < Time.now
    end

    def stream_within_launch_window_and_time_limit?

        return false if time_has_run_out?
        stream = published_stream_for_exam_info

        # return true if this is not an exam stream
        return true unless stream

        # return false if the exam is not yet open
        if stream.exam_open_time(user) && stream.exam_open_time(user) > Time.now
            return false
        end

        return true
    end

    def published_stream_for_exam_info(fields = nil)
        stream = Lesson::Stream::Version.published_versions.where(locale: 'en', locale_pack_id: locale_pack_id).select(:id, :exam, :time_limit_hours).first
        if stream.nil?
            Raven.capture_exception("No stream found for stream progress", {
                fingerprint:  ["No stream found for stream progress"],
                extra: {
                    stream_progress_id: self.id
                }
            })
        end
        stream
    end

    # Passing the user in can save some queries
    def self.create_or_update!(attrs, user = nil)

        if user && user.id != attrs[:user_id]
            raise ArgumentError.new("Unexpected user")
        end

        # Automatically retries in the event of ActiveRecord::PreparedStatementCacheExpired
        RetriableTransaction.transaction do
            locale_pack_id = attrs[:locale_pack_id]

            if locale_pack_id.nil?
                raise ActiveRecord::RecordNotSaved.new("#{self.name} could not be saved: no locale_pack_id found for #{attrs.inspect}")
            end

            instance = Lesson::StreamProgress.where(
                :user_id => attrs[:user_id],
                :locale_pack_id => locale_pack_id
            ).first

            if instance.nil?
                instance = new

                # only trust the server, not the client to record timestamps
                instance.started_at = Time.now
                instance.locale_pack_id = locale_pack_id
                if user
                    instance.user = user
                else
                    instance.user_id = attrs[:user_id]
                end

                # automatically add to user's favorite stream locale packs
                instance.user.bookmark_stream_locale_pack(instance.locale_pack)

            else
                # force updated_at to change regardless of other changes
                instance.updated_at = Time.now
                instance.user = user if user
            end

            instance.raise_if_exam_closed(attrs)

            # if nothing is changing, do not do any work
            if attrs[:complete] == instance.completed_at.present? && attrs[:lesson_bookmark_id] == instance.lesson_bookmark_id
                save = :save!
            else
                save = instance.merge_hash(attrs)
            end

            if instance.send(save)
                instance
            else
                raise ActiveRecord::RecordNotSaved.new("#{self.name} could not be saved: #{instance.errors.full_messages}")
            end

            instance
        end
    end

    def self.generate_certificate_image(instance, opts = {})

        I18n.with_locale(instance.user.locale) do
            opts[:user_name] = instance.user.name if opts[:user_name].blank?
            associated_stream_json = self.get_published_stream_json(instance)
            opts[:course_name] = associated_stream_json['title'] if opts[:course_name].blank?
            date = instance.completed_at || Time.now
            opts[:date] = I18n.l(date, format: '%b %d').upcase if opts[:date].blank?
            opts[:year] = I18n.l(date, format: '%Y') if opts[:year].blank?
            opts[:lessons_label] = I18n.t(:lessons) if opts[:lessons_label].blank?
            opts[:lesson_total] = associated_stream_json['lessons'].length if opts[:lesson_total].blank?
            opts[:exam] = instance.for_exam_stream?

            CertificateGenerator.generate_asset(instance.user, opts)
        end
    end

    def self.clear_all_progress(user_id)
        Lesson::StreamProgress.where({:user_id => user_id}).destroy_all
    end

    # Instead of an instance method like in stream.rb, this is a class method called
    # once (and not O(n) times) in render_batch_records in ApiCrudController for batch operations like index
    # for the set of Lesson::StreamProgress events collected by one of the batch queries above.
    # This method should only be doing serialization and json pruning, and never incur additional queries.
    def self.as_json(stream_progress_events, options = {})
        stream_progress_json = []
        stream_progress_events.each do |progress_event|
            stream_progress_json << Lesson::StreamProgress.progress_event_json(progress_event, options)
        end
        stream_progress_json
    end

    def self.progress_event_json(progress_event, options = {})
        progress_event_json = {
            :user_id => progress_event.user_id,
            :created_at => progress_event.created_at.to_timestamp,
            :updated_at => progress_event.updated_at.to_timestamp,
            :locale_pack_id => progress_event.locale_pack_id,
            :lesson_bookmark_id => progress_event.lesson_bookmark_id,
            :started_at => progress_event.started_at.to_timestamp,
            :completed_at => progress_event.completed_at.to_timestamp,
            :complete => !progress_event.completed_at.nil?,
            :last_progress_at => progress_event.last_progress_at.to_timestamp,
            :time_runs_out_at => progress_event.time_runs_out_at.to_timestamp,
            :official_test_score => progress_event.official_test_score,
            :waiver => progress_event.waiver,
            :id => progress_event.id # need this because iguana expects it, and uses it to determine whether its creating a new obj or updating an existing obj
        }

        # for zapier, we include the user and published content title data as well
        if options[:zapier]
            # extract user and title (these are eager loaded in stream_progress_controller.rb#index)
            progress_user = progress_event.user
            progress_streams = progress_event.lesson_streams.to_a
            published_content_title = progress_event.published_content_titles.first # we only return english for now
            # this should never happen, unless it's progress for an unpublished stream. just in case, fall back to locale_pack_id
            english_course_title = published_content_title.nil? ? progress_event.locale_pack_id : published_content_title.published_title

            # merge the extra data
            progress_event_json = progress_event_json.merge({
                user_id: progress_user.id,
                user_name: progress_user.name,
                email: progress_user.email,
                phone: progress_user.phone,
                english_course_title: english_course_title,
                invite_code: progress_user.invite_code
            })

            # remove unnecessary fields
            progress_event_json.delete(:locale_pack_id)
            progress_event_json.delete(:lesson_bookmark_id)
            progress_event_json.delete(:time_runs_out_at)
        end

        progress_event_json
    end

    def self.get_published_stream_json(instance)
        streams = Lesson::Stream::JsonCache.get_attrs_for_locale_pack_ids([instance.locale_pack_id])

        in_users_locale = streams.detect { |s| s['locale'] == instance.user.pref_locale }
        return in_users_locale if in_users_locale.present?

        in_en = streams.detect { |s| s['locale'] == 'en' }
        return in_en
    end

    def merge_hash(attrs)
        if self.time_runs_out_at.nil? && time_limit_hours = self.time_limit_hours
            self.time_runs_out_at = Time.now + time_limit_hours.hours
        end

        created_or_updated_at = Time.now
        if attrs[:lesson_bookmark_id]
            self.lesson_bookmark_id = attrs[:lesson_bookmark_id]
        end

        if attrs[:complete] == true && self.completed_at.nil?
            # only trust the server, not the client to record timestamps
            self.completed_at = created_or_updated_at
        end

        # in the wild, the certificate image should have been pre-generated. But
        # if it was not for any reason, do it now.
        if self.completed_at && self.certificate_image_id.nil? && attrs[:certificate_image_id].nil?
            save = :generate_certificate_image!
        # Only in the fixture builder will the certificate_image_id be set in the attrs.  This is because
        # we don't want to hit AWS in that case
        elsif self.completed_at && self.certificate_image_id.nil?
            self.certificate_image_id = attrs[:certificate_image_id]
            save = :save!
        else
            save = :save!
        end

        save
    end

    def raise_if_exam_closed(attrs = {})
        if !self.stream_within_launch_window_and_time_limit? && self.user.cannot?(:launch_exams_outside_of_limits, self)
                self.errors.add(:course, "is closed")

                # This is not entirely unexpected, but it is a bit fishy, so we'd like
                # to know how often it is happening
                Raven.capture_exception("Trying to save stream_progress for closed stream.", {
                    level: 'warning',
                    extra: attrs.merge({
                        time: Time.now.to_s,
                        time_has_run_out: time_has_run_out?,
                        time_runs_out_at: time_runs_out_at.to_s,
                        exam_open_time: published_stream_for_exam_info.exam_open_time(user),
                        cohort: user.relevant_cohort && user.relevant_cohort.attributes['id']
                    })
                })
                raise ActiveRecord::RecordInvalid.new(self)
            end
    end

    def generate_certificate_image!(opts = {})
        if Lesson::StreamProgress.turn_off_cert_generation
            save!
            return true
        end
        # NOTE: it is not safe to use this method to generate new formats.  Right now,
        # the ['original']['url'] check will ensure that, if you tried to do that,
        # nothing would happen.  If we removed that check, then the old_image.destroy
        # at the bottom would end up deleting urls from s3 that we still need.

        old_image = self.certificate_image

        begin
            certificate_image = self.class.generate_certificate_image(self, opts)

            # if the new image is the same as the old, do nothing
            if old_image && old_image.formats['original']['url'] == certificate_image.formats['original']['url']
                certificate_image.delete # do NOT destroy.  That will delete the image from s3
                return false
            end

            self.certificate_image = certificate_image

        # If you haven't set up S3 access in application.yml using AWS_ROLE_ARN_WEBSERVER,
        # just accept that you won't get a cert when finishing a stream
        rescue Aws::S3::Errors::AccessDenied => err
            raise err unless ignore_access_denied_errors_while_creating_cert?
        end

        self.save!

        # delete any old, existing certificate assets if they exist
        # (the equality check is only important if we've caught the AccessDenied
        # error above)
        old_image.destroy if old_image && old_image != self.certificate_image
        return true
    end

    def ignore_access_denied_errors_while_creating_cert?
        Rails.env.development?
    end

    # FIXME?: this may not be true forever, or if we did a data migration or something
    def last_progress_at
        updated_at
    end

    # is this the last lesson the user needs to complete the course?
    def on_last_lesson
        incomplete_lesson_count  == 1
    end

    def all_lessons_complete?
        incomplete_lesson_count == 0
    end

    def incomplete_lesson_count
        stream_attrs = self.class.get_published_stream_json(self)
        lesson_locale_pack_ids = stream_attrs['lessons'].map { |l| l['locale_pack']['id'] }
        completed_lesson_locale_pack_ids = LessonProgress.where(user_id: user_id, locale_pack_id: lesson_locale_pack_ids)
                                                .where.not(completed_at: nil)
                                                .pluck(:locale_pack_id)
        (lesson_locale_pack_ids - completed_lesson_locale_pack_ids).size
    end

    def as_json(options = {})
        stream_progress_json = Lesson::StreamProgress.progress_event_json(self, options)

        # zapier doesn't need any of this
        unless options[:zapier]
            stream_progress_json["certificate_image"] = self.certificate_image.as_json
            stream_progress_json["lesson_progress"] = self.lesson_progress.as_json if !self.lesson_progress.nil?
        end
        stream_progress_json
    end

    def foundations_playlist
        unless defined? @foundations_playlist
            @foundations_playlist = user.get_foundations_playlist
        end
        @foundations_playlist
    end

    def handle_started_foundations_playlist_event
        relevant_cohort = user.relevant_cohort

        # Check if the lesson_stream is in the foundations playlist
        if foundations_playlist && foundations_playlist.stream_locale_pack_ids.include?(self.locale_pack_id)

            # Check if this is the first lesson_stream to be started in the foundations_playlist
            if(Lesson::StreamProgress.where(user_id: user.id, locale_pack_id: foundations_playlist.stream_locale_pack_ids).count == 1)
                Event.create_server_event!(SecureRandom.uuid, user.id, 'cohort:started_foundations_playlist', {
                    label: relevant_cohort.name,
                    lesson_stream_locale_pack_id: self.locale_pack_id,
                    cohort_id: relevant_cohort.attributes['id'],
                    cohort_title: relevant_cohort.title
                }).log_to_external_systems
            end
        end
    end

    def handle_completed_foundations_events
        relevant_cohort = user.relevant_cohort
        relevant_application = user.application_for_relevant_cohort
        completing_foundations_stream = foundations_playlist&.stream_locale_pack_ids&.include?(self.locale_pack_id)

        if completing_foundations_stream
            # Trigger an identify in order to re-evaluate 'foundations_courses_complete'
            self.user.identify

            foundations_json = foundations_playlist.as_json(user: self.user, fields: ['percent_complete', 'courses_complete_count'])
            foundations_percent_complete = foundations_json['percent_complete']
            foundations_courses_complete_count = foundations_json['courses_complete_count']

            # log an event any time a foundations playlist is completed, in order to
            # support event triggering in customer.io.  If customer.io would let us
            # filter on multiple attributes, we probably wouldn't do the pending filtering
            # here, but given current restrictions, this is easiest
            if user.application_for_relevant_cohort &&
                user.application_for_relevant_cohort.log_foundations_completed_events?

                payload = {
                    lesson_stream_locale_pack_id: self.locale_pack_id,
                    foundations_percent_complete: foundations_percent_complete,
                    foundations_courses_complete_count: foundations_courses_complete_count
                }.merge(AdmissionRound.promoted_rounds_event_attributes(user.timezone))
                .merge(relevant_cohort.event_attributes(self.user.timezone))

                Event.create_server_event!(SecureRandom.uuid, user.id, 'cohort:completed_foundations_course', payload).log_to_external_systems
            end

            # Log an event if the playlist is being completed
            if foundations_percent_complete == 1
                Event.create_server_event!(SecureRandom.uuid, user.id, 'cohort:completed_foundations_playlist', {
                    label: relevant_cohort.name,
                    lesson_stream_locale_pack_id: self.locale_pack_id,
                    cohort_id: relevant_cohort.attributes['id'],
                    cohort_title: relevant_cohort.title
                }).log_to_external_systems
            end
        # log an event if the user just completed 50% or more of the curriculum for their relevant_cohort
        elsif @relevant_cohort_completion_percentage < 0.50 && updated_relevant_cohort_completion_percentage >= 0.50
            Event.create_server_event!(SecureRandom.uuid, self.user_id, 'cohort:completed_half_of_curriculum', curriculum_event_attributes).log_to_external_systems
        end
    end

    def just_completed_curriculum?
        saved_change_to_completed_at? &&
        user.application_for_relevant_cohort &&
        user.relevant_cohort&.requires_stream_locale_pack_id?(locale_pack_id) &&
        user.relevant_cohort_curriculum_complete?
    end

    def mark_application_completed
        relevant_application = user.application_for_relevant_cohort

        # update completed_at value
        relevant_application.completed_at = Time.now

        # if the cohort does not support graduation on completion, we
        # should not set the application's graduation_status when the
        # curriculum is complete.
        return if !user.relevant_cohort.supports_graduation_on_complete?

        # update graduation status if applicable  see also: cohort_user_progress_records.rb:322
        if relevant_application.final_score.present?
            relevant_application.graduation_status = relevant_application.final_score > 0.7 ? 'graduated' : 'failed'
        else
            Raven.capture_in_production(NoFinalScoreOnCompleteApplication,
                extra: {
                    stream_progress_id: self.id
                }
            ) do
                raise NoFinalScoreOnCompleteApplication.new("No final_score found in mark_application_completed")
            end
        end

    end

    def save_relevant_application
        user.application_for_relevant_cohort&.save!
    end

    def set_relevant_cohort_completion_percentage
        @relevant_cohort_completion_percentage = updated_relevant_cohort_completion_percentage
    end

    def updated_relevant_cohort_completion_percentage
        user.relevant_cohort && user.cohort_percent_complete(user.relevant_cohort_id) || 0
    end

    def set_official_test_score
        return unless self.official_test_score.nil?
        return unless completed_at_changed? && for_exam_stream?

        self.official_test_score = calculate_official_test_score_from_lesson_progress
    end

    def handle_exam_events
        return unless saved_change_to_completed_at? && for_exam_stream?
        return if skip_exam_events # user.rake #transfer_progress_for_select_users

        # If we are changing the completed_at field then we are setting it and a progress is being completed
        relevant_cohort = user.relevant_cohort

        extra_info = {}
        target_time = Time.now

        # If this is an exam in the schedule
        if relevant_cohort && period = relevant_cohort.period_for_required_stream_locale_pack_id(self.locale_pack_id)
            # For schedule exams we wait until the end of the period to send the results
            target_time = relevant_cohort.end_time_for_period(period) + 1.day
            extra_info[:curriculum_context] = "schedule"

            if user.application_for_relevant_cohort
                period_info = relevant_cohort.event_attributes_for_period(period)
                extra_info.merge!(period_info)
            end

        # Else if this is an exam in a specialization playlist
        elsif relevant_cohort && relevant_cohort.get_specialization_stream_locale_pack_ids.include?(self.locale_pack_id)
            extra_info[:curriculum_context] = "specialization_playlist"

        # Else if this is an exam in a required playlist (but not the schedule). This could happen with certificates.
        elsif relevant_cohort && relevant_cohort.get_required_stream_locale_pack_ids.include?(self.locale_pack_id)
            extra_info[:curriculum_context] = "required_playlist"

        # Else this should be an exam that is in the library but nowhere else (could happen with legacy users through groups)
        else
            extra_info[:curriculum_context] = "library"
        end

        Cohort::ReportExamResultJob.set(wait_until: target_time).perform_later(
            self.user_id,
            self.locale_pack_id,
            extra_info
        )
    end

    def curriculum_event_attributes
        cohort = self.user.relevant_cohort.published_version
        {
            cohort_id: self.user.relevant_cohort_id,
            cohort_title: cohort.title,
            program_type: cohort.program_type,
            is_paid_cert: cohort.program_type_config.is_paid_cert?,
            project_submission_email: cohort.formatted_project_submission_email,
            learner_projects: cohort.program_type_config.supports_cohort_level_projects? ? LearnerProject.find_cached_by_id(cohort.learner_project_ids).as_json : [],
        }
    end

    # separate method easier to mock in specs
    def log_curriculum_complete_event
        Event.create_server_event!(SecureRandom.uuid, user.id, 'cohort:curriculum_completed', curriculum_event_attributes).log_to_external_systems
    end

    # We log the reset event from the server because the user to whom it
    # applies is not the current_user that triggers the evnet in the client.
    # An institutional_report_viewer clicks the button to reset the process
    # for some other user, so it does no good if the institutional report viewer
    # logs an event. The other user needs to log it.
    def log_reset_event

        stream = Lesson::Stream::ToJsonFromApiParams.new(
            user: self.user,
            filters: {
                locale_pack_id: locale_pack_id,
                in_users_locale_or_en: true,
                published: true
            }
        ).to_a.first

        # FIXME: This will error if the stream has been deleted or even unpublished, but that
        # is not entirely unexpected.  See https://sentry.io/pedago/production-rails/issues/169805202/
        # I guess we could check the versions table in that case if we really cared, but since
        # the stream is gone it won't be in reports or anything anyway, so practically prbably not a big deal.
        # In any case, we shouldn't log a raven exception here.
        if !stream
            Raven.capture_exception('No stream found in log_reset_event', {
                extra: {
                    locale_pack_id: locale_pack_id,
                    user: user.id
                }
            })
            return
        end

        Event.create_server_event!(SecureRandom.uuid, user.id, 'lesson:stream:reset', {
            label: stream['id'],
            lesson_stream_id: stream['id'],
            lesson_stream_title: stream['title'],
            lesson_stream_complete: !self.completed_at.nil?,
            lesson_stream_version_id: stream['version_id']
        }).log_to_external_systems
    end

    def published_lesson_locale_pack_ids
        @published_lesson_locale_pack_ids ||=  Lesson::Stream.published_lesson_locale_pack_ids(locale_pack_id)
    end

    def get_lesson_progresses
        LessonProgress.where(user_id: user_id, locale_pack_id: published_lesson_locale_pack_ids)
    end

    def calculate_official_test_score_from_lesson_progress
        return nil unless completed_at
        return nil unless for_exam_stream?

        scores = get_lesson_progresses.map(&:official_test_score).compact

        if scores.size < Lesson::Stream.published_lesson_locale_pack_ids(locale_pack_id).size
            Raven.capture_exception("Setting official_test_score when not all lessons are complete.", {
                extra: {
                    stream_progress_id: self.id
                }
            })
        end
        scores.inject(0) { |sum, el| sum + el }.to_f / scores.size
    end

    def set_final_score
        user&.application_for_relevant_cohort&.set_final_score
    end
end
