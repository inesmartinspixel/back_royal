# we're not actually using mongo, but MongoMapper
# is useful for validations since in handles the embedded documents
# and stuff
class Lesson::Content
    include EmbeddableDocument
    attr_accessor :lesson

    set_type_field 'lesson_type'

    # this should be defined in frame_list.rb, but because of the way we generate
    # json, it's hard to restrict it there. Since all real lessons are frame_list lessons
    # anyway, it doesn't hurt anything to define the key here
    many :frames,  Lesson::Content::FrameList::Frame

    # overridden in frame_list
    def save_warnings
    	[]
    end

    def merge_hash(hash)
        hash.each do |key, value|
            self[key] = value
        end
        self
    end
end
