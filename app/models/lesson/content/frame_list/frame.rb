# load up all the validators
Dir.glob(File.expand_path("../frame/validations/**/*.rb", __FILE__)) do |file|
    require file
end

class Lesson::Content::FrameList::Frame
    include EmbeddableDocument

    set_type_field 'frame_type'

    embedded_in :lesson_content
    delegate :lesson, :to => :lesson_content, :allow_nil => true

    def lesson_id
        lesson.id
    end

    def index
        lesson_content.frames.index(self)
    end

    def lesson_title
        lesson.title
    end

    def editor_link(site = 'https://smart.ly')
        "#{site}/editor/lesson/#{lesson.id}/edit?frame=#{id}"
    end

    def player_link(stream, site = 'https://smart.ly')
        "#{site}/course/#{stream.id}/chapter/0/lesson/#{lesson.attributes['id']}/show?frame=#{id}"
    end

    def content_links
        self.full_text_content.scan(/\[([^\]]+)\].?\((http[^\)]+)\)/).map { |match| match[1] }
    end
end
