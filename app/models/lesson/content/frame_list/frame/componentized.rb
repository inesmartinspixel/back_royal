class Lesson::Content::FrameList::Frame::Componentized < Lesson::Content::FrameList::Frame

	many :components, Lesson::Content::FrameList::Frame::Componentized::Component

	key :continue_button_id, String
	key :main_ui_component_id, String
	key :frame_navigator_id, String
	key :author_comments, Array

	key :associated_concepts, Object # deprecated
    key :case_sensitive, Object # deprecated. not sure why this was ever defined on the frame

	validates_presence_of :continue_button
	validates_presence_of :frame_navigator
	validates_presence_of :main_ui_component

	# could use validates_associated, but it gives no useful message
	validate :validate_components

	def self.decorate_frame_json(lesson_id, frame_hashes)

		# find all usage reports for this lesson, and then put the information onto
		# the relevant challenge components
		usage_reports = Lesson::MultipleChoiceChallengeUsageReport.where(lesson_id: lesson_id)
		if usage_reports.any?
			component_hashes = {}

			# for speed, index all components by frame_id and component_id
			frame_hashes.each do |frame_hash|
				frame_id = frame_hash['id']
				frame_hash['components'].each do |component_hash|
					component_id = component_hash['id']
					component_hashes["#{frame_id}#{component_id}"] = component_hash
				end
			end

			usage_reports.each do |usage_report|
				component_hash = component_hashes["#{usage_report.frame_id}#{usage_report.challenge_id}"]
				if component_hash
					component_hash['usage_report'] ||= {'first_answer_counts' => {}}
					counts_hash = component_hash['usage_report']['first_answer_counts']
					counts_hash[usage_report['answer_id']] = usage_report['count']
				end
			end
		end

        return frame_hashes
	end

	def editor_template
		main_ui_component['editor_template']
	end

	def validate_components

		components.each do |component|
			component.validations_run = false
		end

		# We validate each component that is referenced from the
		# top-level frame itself (right not that is the main_ui_component,
		# the continue_button and the frame_navigator).  Each of those will
		# then validate any components that they reference, all the
		# way down the hierarchy.
		if main_ui_component.present? && !main_ui_component.valid?
			main_ui_component.errors.each do |attribute, message|
				errors.add(attribute, message)
			end
		end

		if continue_button && !continue_button.valid?
			continue_button.errors.each do |attribute, message|
				errors.add(:"continue_button_#{attribute}", message)
			end
		end

		if frame_navigator && !frame_navigator.valid?
			frame_navigator.errors.each do |attribute, message|
				errors.add(:"frame_navigator_#{attribute}", message)
			end
		end

		# see override of run_validations! in component.rb
		# Any component that was not referenced by any of the top-level
		# components above will not yet have been validated. Validate it now,
		# and add an unreferenced error unless allow_unreferenced? is true
		components.each_with_index do |component, i|
			if !component.validations_run
				component_identifier = "#{component.type}_component_#{i+1}"

				if component.allow_unreferenced?
					if !component.valid?
						component.errors.each do |attribute, message|
							errors.add(:"#{component_identifier}_#{attribute}", message)
						end
					end
				else
					errors.add(component_identifier, "is not referenced (or at least it was not validated. id=#{component.id.inspect})")
				end

			end
		end

	end


	# find the component that has a particular id, or a
	# list of components for a list of ids
	def dereference(id_or_ids)

		if id_or_ids.is_a? Array

			id_or_ids.map do |id|
				dereference id
			end

		elsif id_or_ids.blank?
			raise ArgumentError.new("Cannot dereference blank id")
		else

			id = id_or_ids
			@component_map ||= {}
			self.components ||= []

			@component_map[id] ||= components.detect do |component|
				if component.id == id_or_ids
					return component
				end
			end

		end


	end

	def continue_button
		continue_button_id.nil? ? nil : dereference(continue_button_id)
	end

	def frame_navigator
		frame_navigator_id.nil? ? nil : dereference(frame_navigator_id)
	end

	def main_ui_component
		main_ui_component_id.nil? ? nil : dereference(main_ui_component_id)
	end

	# used by rake lesson:frame_list:frame:componentized:blanks_styling_audit
	def get_or_create_frame_navigator
		if self.frame_navigator.nil?
			frame_navigator = Lesson::Content::FrameList::Frame::Componentized::Component::FrameNavigator.load({
				component_type: 'ComponentizedFrame.FrameNavigator',
				id: SecureRandom.uuid,
				selectable_answer_navigator_ids: []
			})
			components << frame_navigator
			self.frame_navigator_id = frame_navigator.id
		end
		self.frame_navigator
	end

	def text_content
		main_ui_component.text_content
	end

	def full_text_content
		text_components = components.select do |component|
			component.is_a?(Lesson::Content::FrameList::Frame::Componentized::Component::Text)
		end
		text_components.map(&:text).flatten.join(' ')
	end

	def images
		components.select do |component|
			component.is_a?(Lesson::Content::FrameList::Frame::Componentized::Component::Image)
		end
	end

end
