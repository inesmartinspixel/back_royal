# load up all the validations manually, as they patch the ActiveRecord::Validations
# module and would otherwise break auto-loading (see also: `application.rb` for autoload ignore rule)
Dir.glob(File.expand_path("../component/validations/**/*.rb", __FILE__)) do |file|
    require file
end

class Lesson::Content::FrameList::Frame::Componentized::Component
	include EmbeddableDocument
    attr_accessor :validations_run

    set_type_field 'component_type'
    embedded_in :frame
    delegate :lesson, :lesson_content, :to => :frame

    key :behaviors, Hash, :default => Proc.new { {} }
    key :editor_template, String

    key :_componentName, Object # just there due to an old bug

    validate :validates_all_references_resolve

    class << self
        attr_accessor :registered_references
        attr_accessor :subclass_map
    end

    def self.inspect
        self.name.split('::').last
    end

    def self.inherited(subclass)
        subclass.registered_references = Set.new
        super
    end

    def self.get_klass_for_type(type)
        Lesson::Content::FrameList::Frame::Componentized::Component.subclass_map ||= {}
        Lesson::Content::FrameList::Frame::Componentized::Component.subclass_map[type] ||= add_subclass_to_map(type)
    end

    def self.add_subclass_to_map(type)
        name = type.split('.')[1]
        raise NameError if name.blank?

        klass = nil
        klass = iteratively_find_subclass(Lesson::Content::FrameList::Frame::Componentized::Component, name)
        if klass.nil?
            raise NameError.new("No class found for #{type.inspect}")
        end

        Lesson::Content::FrameList::Frame::Componentized::Component.subclass_map[type] = klass
    end

    def self.iteratively_find_subclass(klass, name)
        begin
            return klass.const_get(name.to_sym)
        rescue NameError
            klass.subclasses.each do |subclass|
                subclass = iteratively_find_subclass(subclass, name)
                return subclass if subclass
            end
            nil
        end
    end

    # a silly little class that returns from the references method so
    # that we can use the sugary syntax:
    #
    # references('something').through('something_id')

    class Through

        def initialize(component, prop)
            @component = component
            @prop = prop
        end

        def through(id_key)
            @component.add_reference(@prop, id_key)
        end

    end

    def self.references(prop)
        Through.new(self, prop)
    end

    def self.add_reference(prop, id_key)
        self.registered_references << {
            prop: prop.to_sym,
            id_key: id_key.to_sym
        }
        self.key id_key
        define_method(prop.to_sym) do
            id = self.send(id_key.to_sym)

            # don't use .blank? here because an empty array should
            # return an empty array, not nil
            (id.nil? || id == "") ? nil : self.frame.dereference(id)
        end

        define_method(:"#{prop}=") do |component|
            if component.is_a?(Array)
                id = self.send("#{id_key}=", component.map(&:id))
            else
                id = self.send("#{id_key}=", component.id)
            end

        end
    end

    def self.all_registered_references
        if self == Lesson::Content::FrameList::Frame::Componentized::Component
            return []
        else
            self.registered_references.merge(superclass.all_registered_references)
        end
    end

    def self.allow_unreferenced
        define_method(:allow_unreferenced?) { true }
    end

    def type
        name = self.class.name.split('::').last.underscore
    end

    def continue_button
        frame.continue_button
    end

    # we want to keep track of which components have already had validations
    # run so we can be sure to validate any unreferenced components in
    # componentized.rb
    def run_validations!(*args)
        @validations_run = true
        super
    end

    def validations_run?
        !!@validations_run
    end

    def allow_unreferenced?
        false
    end

    private
    def validates_all_references_resolve
        if self.frame.nil?
            raise "Component is not in a frame and so cannot resolve references"
        end

        self.class.all_registered_references.each do |reference|
            id_key = reference[:id_key]
            id_or_ids = self.send(id_key)
            if id_or_ids.is_a?(String) && !id_or_ids.blank?

                if !self.frame.dereference(id_or_ids)
                    errors.add(id_key, "cannot be dereferenced (no component found for #{id_or_ids.inspect})")
                end
            elsif id_or_ids.is_a?(Array)
                id_or_ids.each_with_index do |id, i|
                    if !self.frame.dereference(id)
                        errors.add(:"#{id_key}_#{i+1}", "cannot be dereferenced  (no component found for #{id.inspect})")
                    end
                end
            end
        end
    end

end

# explicitly require `Layout` since the autoloader can't infer its requirements prior to `add_subclass_to_map`
require Rails.root.join('app/models/lesson/content/frame_list/frame/componentized/component/layout')

