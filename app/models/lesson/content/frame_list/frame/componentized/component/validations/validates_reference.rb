
class Lesson::Content::FrameList::Frame::Componentized
    class Component
        module Validations
            class ValidatesReference < ActiveModel::EachValidator
                # -*- SkipSchemaAnnotations

                def validate_each(record, attribute, value)
                    if value.nil? && !options[:allow_nil]
                        record.errors.add(attribute, 'must not be nil')
                    elsif value.nil?
                        return
                    elsif value.is_a? Array
                        value.each_with_index do |item, i|
                            # singularize so the message says:
                            # "Challenge 1 is invalid becuase ..." rather than
                            # "Challenges 1 is invalid because ..."
                            validate_item(record, :"#{attribute.to_s.singularize}_#{i+1}", item)
                        end
                    else
                        validate_item(record, attribute, value)
                    end
                end

                def validate_item(record, attribute, item)

                    if !item.is_a? options[:type]
                        record.errors.add(attribute, "must be an instance of #{options[:type].inspect} but is #{item.class.inspect}")
                        return
                    end

                    # check the editor template
                    if options[:editor_template]
                        valid_templates = options[:editor_template].kind_of?(Array) ? options[:editor_template] : [options[:editor_template]]
                        record.errors.add(attribute, "must have editor template #{options[:editor_template].inspect} but has #{item.editor_template.inspect}") unless valid_templates.include? item.editor_template
                    end

                    # check expected behaviors
                    if options[:has_behaviors]

                        options[:has_behaviors].each do |expected_behavior|
                            if !item.behaviors[expected_behavior]
                                record.errors.add(attribute, "must have behavior #{expected_behavior.inspect}")
                            end
                        end
                    end

                    # check unexpected behaviors
                    if options[:does_not_have_behaviors]
                        options[:does_not_have_behaviors].each do |unexpected_behavior|
                            if item.behaviors[unexpected_behavior]
                                record.errors.add(attribute, "must not have behavior #{unexpected_behavior.inspect}")
                            end
                        end
                    end

                    # we want to check if validations have already been run against the associated item for 2 reasons:
                    # 1. it prevents maximum call stack errors when we have circular references
                    # 2. it prevents many instances of the same validation error from being shown to the user
                    if !item.validations_run? && !item.valid?
                        item.errors.full_messages.each do |message|
                            record.errors.add(attribute, message)
                        end
                    end
                end

            end
        end
    end
end


module ActiveModel
    module Validations
        module HelperMethods

            def validates_reference(attr_name, type, options = {})
                validates_with Lesson::Content::FrameList::Frame::Componentized::Component::Validations::ValidatesReference, _merge_attributes([attr_name, {:type => type}.merge(options)])
            end

        end
    end
end
