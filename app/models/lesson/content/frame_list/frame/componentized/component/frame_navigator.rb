class Lesson::Content::FrameList::Frame::Componentized::Component::FrameNavigator < Lesson::Content::FrameList::Frame::Componentized::Component

    references('selectable_answer_navigators').through('selectable_answer_navigator_ids');
    key('next_frame_id');
    key('has_branching');

    validates_reference :selectable_answer_navigators, Answer::SelectableAnswerNavigator, :allow_nil => true

end
