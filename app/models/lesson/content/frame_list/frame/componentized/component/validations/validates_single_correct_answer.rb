# validates that all elements in a reference list shared some property (like the answer list should
# be shared among all challenges on a this_or_that frame)

class Lesson::Content::FrameList::Frame::Componentized
    class Component
        module Validations
            class ValidatesSingleCorrectAnswer < ActiveModel::EachValidator
                # -*- SkipSchemaAnnotations

                def validate_each(record, attribute, validator)
                    result = record.correct_answer_and_error_messages
                    if result[0].nil?
                        result[1].each do |message|
                            record.errors.add(message[0], message[1])
                        end
                    end
                end

            end
        end
    end
end

module ActiveModel
    module Validations
        module HelperMethods

            def validates_single_correct_answer(options = {})
                validates_with Lesson::Content::FrameList::Frame::Componentized::Component::Validations::ValidatesSingleCorrectAnswer,
                    _merge_attributes([:validator, options])
            end

        end
    end
end
