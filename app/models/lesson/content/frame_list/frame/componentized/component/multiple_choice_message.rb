class Lesson::Content::FrameList::Frame::Componentized::Component::MultipleChoiceMessage < Lesson::Content::FrameList::Frame::Componentized::Component

	references('challenge').through('challenge_id');
    references('answer_matcher').through('answer_matcher_id');
    references('message_text').through('message_text_id');
    key('event');

    validates_reference :challenge, Challenge::MultipleChoiceChallenge
    validates_reference :answer_matcher, AnswerMatcher::SimilarToSelectableAnswer
    validates_reference :message_text, Text
    validates_inclusion_of :event, :in => %w(validated selected)

end
