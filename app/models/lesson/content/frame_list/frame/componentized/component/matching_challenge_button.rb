class Lesson::Content::FrameList::Frame::Componentized::Component::MatchingChallengeButton < Lesson::Content::FrameList::Frame::Componentized::Component

    references('challenge').through('challenge_id')
	references('text').through('text_id')
    references('image').through('image_id')

    validates_reference :challenge, Challenge
    validates_presence_of :text_or_image, :message => 'must be set'
    validates_reference :text, Text, :allow_nil => true
    validates_reference :image, Image, :allow_nil => true

    def text_or_image
    	text || image
    end

end
