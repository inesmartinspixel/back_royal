class Lesson::Content::FrameList::Frame::Componentized::Component::AnswerMatcher < Lesson::Content::FrameList::Frame::Componentized::Component

	# this is an abstract class.  Only allow subclasses to be created
	validates_inclusion_of :is_answer_matcher_subclass?, :in => [true], :message => "must be true"

	def is_answer_matcher_subclass?
		self.class != Lesson::Content::FrameList::Frame::Componentized::Component::AnswerMatcher
	end

end
