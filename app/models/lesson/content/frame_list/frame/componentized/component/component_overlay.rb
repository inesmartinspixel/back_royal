class Lesson::Content::FrameList::Frame::Componentized::Component::ComponentOverlay < Lesson::Content::FrameList::Frame::Componentized::Component

    class ValidatesOverlayOptions < ActiveModel::EachValidator

        def validate_each(record, attribute, value)
            actual_keys = value.keys.map(&:to_s).sort
            expected_keys = record.overlay_component_ids.map(&:to_s).sort
            if actual_keys != expected_keys
                record.errors.add(attribute, "must have one entry for each overlay component (#{actual_keys.inspect} != #{expected_keys.inspect})")
            end

            value.each do |id, entry|
                entry = entry.with_indifferent_access
                ['x', 'y'].each do |pos|
                    val = entry[pos]
                    key = :"#{attribute}.#{id}.#{pos}"
                    if val.nil?
                        record.errors.add(key, "cannot be blank")
                    elsif !val.is_a?(Numeric)
                        record.errors.add(key, "must be a number but was #{val.inspect}")
                    end
                end

                allowed_units = ['px', '%']
                if !allowed_units.include?(entry['units'])
                    key = :"#{attribute}.#{id}.units"
                    record.errors.add(key, "must be one of #{allowed_units.inspect} but is #{entry['units'].inspect}")
                end
            end
        end

    end

    def self.validates_overlay_options(options = {})
        validates_with ValidatesOverlayOptions, _merge_attributes([:overlayOptions, options])
    end

    def text_or_image
        # Continue to support the legacy main_component for when duplicating old content
        # FIXME: https://trello.com/c/e5vFpRcK
        text || image || main_component
    end

    references('text').through('text_id');
    references('image').through('image_id');
    references('main_component').through('main_component_id'); # FIXME: https://trello.com/c/e5vFpRcK
    references('overlay_components').through('overlay_component_ids');

    key :background_color, String
    key('overlayOptions');

    validates_reference :overlay_components, Lesson::Content::FrameList::Frame::Componentized::Component
    validates_reference :text, Text, :allow_nil => true
    validates_reference :image, Image, :allow_nil => true
    validates_reference :main_component, Lesson::Content::FrameList::Frame::Componentized::Component, :allow_nil => true # FIXME: https://trello.com/c/e5vFpRcK
    validates_presence_of :text_or_image, :message => 'must be set'
    validates_length_of :overlay_components, :minimum => 1, :message => "must not be empty"
    validates_overlay_options
end
