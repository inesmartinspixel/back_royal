class Lesson::Content::FrameList::Frame::Componentized::Component::MatchingBoard < Lesson::Content::FrameList::Frame::Componentized::Component

    key :challenges_component_id

    references('matching_challenge_buttons').through('matching_challenge_button_ids')
    references('answer_list').through('answer_list_id')

    validates_reference :matching_challenge_buttons, MatchingChallengeButton
    validates_reference :answer_list, AnswerList

end
