# validates that the specified frame_id references a frame in the frame list


class Lesson::Content::FrameList::Frame::Componentized
    class Component
        module Validations
            class ValidatesNextFrameId < ActiveModel::EachValidator
                # -*- SkipSchemaAnnotations

                def validate_each(record, attribute, value)
                    if (value.nil? && !options[:allow_nil]) || !record.lesson.content.frames.map(&:id).include?(value)
                        record.errors.add(attribute, "must refer to a id in the frame list")
                    end
                end

            end
        end
    end
end


module ActiveModel
    module Validations
        module HelperMethods

            def validates_next_frame_id(next_frame_id, options = {})
                validates_with Lesson::Content::FrameList::Frame::Componentized::Component::Validations::ValidatesNextFrameId, _merge_attributes([:next_frame_id, options])
            end

        end
    end
end
