# validates that all elements in a reference list shared some property (like the answer list should
# be shared among all challenges on a this_or_that frame)

class Lesson::Content::FrameList::Frame::Componentized
    class Component
        module Validations
            class ValidatesConsumableAnswers < ActiveModel::EachValidator
                # -*- SkipSchemaAnnotations

                def validate_each(record, attribute, validator)
                    answers = record.consumable_answers
                    if answers.size != answers.uniq.size
                        record.errors.add(attribute, "require a distinct answer for each challenge.")
                    end
                end

            end
        end
    end
end


module ActiveModel
    module Validations
        module HelperMethods

            def validates_consumable_answers(options = {})
            	self.send(:define_method, :consumable_answers) do
            		if challenges.nil?
            			return []
            		end

            		challenges.map(&:correct_answer)
            	end
                validates_with Lesson::Content::FrameList::Frame::Componentized::Component::Validations::ValidatesConsumableAnswers, _merge_attributes([:consumable_answers, options])
            end

        end
    end
end
