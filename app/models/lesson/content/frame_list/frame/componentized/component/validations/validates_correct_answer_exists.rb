class Lesson::Content::FrameList::Frame::Componentized
    class Component
        module Validations
            class ValidatesCorrectAnswerExists < ActiveModel::EachValidator
                # -*- SkipSchemaAnnotations

                def validate_each(record, attribute, validator)
                    result = record.meets_correct_requirement
                    if result[0].nil?
                        result[1].each do |message|
                            record.errors.add(message[0], message[1])
                        end
                    end
                end

            end
        end
    end
end


module ActiveModel
    module Validations
        module HelperMethods

            def validates_correct_answer_exists(options = {})
                validates_with Lesson::Content::FrameList::Frame::Componentized::Component::Validations::ValidatesCorrectAnswerExists, _merge_attributes([:validator, options])
            end

        end
    end
end
