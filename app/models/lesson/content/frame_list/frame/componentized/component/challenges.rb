class Lesson::Content::FrameList::Frame::Componentized::Component::Challenges < Lesson::Content::FrameList::Frame::Componentized::Component

    key :venn_diagram_header_ids
    key :context_image_size
    key :context_image_2_size

	references('challenges').through('challenge_ids');
    references('layout').through('layout_id');

    # optional.  use when all challenges should share the same content for some section
    references('shared_content_for_text').through('shared_content_for_text_id');
    references('shared_content_for_first_image').through('shared_content_for_image_id');
    references('shared_content_for_second_image').through('shared_content_for_image_2_id');
    references('shared_content_for_interactive_image').through('shared_content_for_interactive_image_id');
    references('shared_content_for_interactive').through('shared_content_for_interactive_id');

    # validations for all Challenges components
    validates_reference :challenges, Challenge
    validates_length_of :challenges, :minimum => 1, :too_short => "is too short (min is 1)"
    validates_reference :layout, Layout
    validates_continue_button ContinueButton::ChallengesContinueButton

    ################ fill_in_the_blanks ########################

        validates_has_behavior :GotoNextOnChallengeComplete, :CompleteOnAllChallengesComplete,  :if => :fill_in_the_blanks?
        validates_reference :shared_content_for_text, Text, :if => :fill_in_the_blanks?
        validates_reference :shared_content_for_first_image, Image, :allow_nil => true, :if => :fill_in_the_blanks?
        validates_reference :shared_content_for_second_image, Image, :allow_nil => true, :if => :fill_in_the_blanks?

        # consumable fill_in_the_blanks validations
        validates_reference :shared_content_for_interactive, AnswerList, :if => :consumable_fill_in_the_blanks?
        validates_shared_value :challenges, :answer_list, :shared_content_for_interactive, :if => :consumable_fill_in_the_blanks?
        validates_consumable_answers :if => :consumable_fill_in_the_blanks?
        validates_reference :challenges, Challenge::MultipleChoiceChallenge,
            :editor_template => 'fill_in_the_blanks',
            :does_not_have_behaviors => ['ResetAnswersOnActivated'],
            :if => :consumable_fill_in_the_blanks?

        # sequential fill_in_the_blanks validations
        validates_reference :challenges, Challenge::MultipleChoiceChallenge,
            :editor_template => 'fill_in_the_blanks',
            :has_behaviors => ['ResetAnswersOnActivated'],
            :if => :sequential_fill_in_the_blanks?

    ################ basic_multiple_choice ########################

        validates_has_behavior :GotoNextOnChallengeComplete, :CompleteOnAllChallengesComplete,  :if => :basic_multiple_choice?
        validates_reference :shared_content_for_text, Text, :if => :basic_multiple_choice?
        validates_reference :shared_content_for_interactive, AnswerList, :if => :basic_multiple_choice?
        validates_reference :shared_content_for_first_image, Image, :allow_nil => true, :if => :basic_multiple_choice?
        validates_reference :shared_content_for_second_image, Image, :allow_nil => true, :if => :basic_multiple_choice?
        validates_reference :challenges, Challenge::MultipleChoiceChallenge, :editor_template => ['basic_multiple_choice', 'check_many'], :if => :basic_multiple_choice?
        validates_shared_value :challenges, :answer_list, :shared_content_for_interactive, :if => :basic_multiple_choice?

    ################ multiple_choice_poll ########################

        validate :validate_is_not_in_practice_lesson, :if => :multiple_choice_poll?
        validates_has_behavior :GotoNextOnChallengeComplete, :CompleteOnAllChallengesComplete,  :if => :multiple_choice_poll?
        validates_reference :shared_content_for_text, Text, :if => :multiple_choice_poll?
        validates_reference :shared_content_for_interactive, AnswerList, :if => :multiple_choice_poll?
        validates_reference :shared_content_for_first_image, Image, :allow_nil => true, :if => :multiple_choice_poll?
        validates_reference :shared_content_for_second_image, Image, :allow_nil => true, :if => :multiple_choice_poll?
        validates_reference :challenges, Challenge::MultipleChoiceChallenge, :editor_template => ['basic_multiple_choice'], :if => :multiple_choice_poll?
        validates_shared_value :challenges, :answer_list, :shared_content_for_interactive, :if => :multiple_choice_poll?


    ################ blanks_on_image ########################

        validates_has_behavior :CompleteOnAllChallengesComplete,  :if => :blanks_on_image_like?
        validates_does_not_have_behavior :GotoNextOnChallengeComplete,  :if => :blanks_on_image_like?
        validates_reference :shared_content_for_text, Text, :if => :blanks_on_image_like?
        validates_reference :shared_content_for_interactive_image, InteractiveCards, :allow_nil => true, :if => :blanks_on_image_like?
        validates_reference :images_in_interactive_cards_in_shared_content_for_interactive, Image, :if => :blanks_on_image?

        # consumable blanks_on_image validations
        validates_reference :shared_content_for_interactive, AnswerList, :if => :consumable_blanks_on_image_like?
        validates_shared_value :challenges, :answer_list, :shared_content_for_interactive, :if => :consumable_blanks_on_image_like?
        validates_consumable_answers :if => :consumable_blanks_on_image_like?
        validates_reference :challenges, Challenge::MultipleChoiceChallenge,
            :editor_template => 'blanks_on_image',
            :does_not_have_behaviors => ['ResetAnswersOnActivated'],
            :if => :consumable_blanks_on_image_like?

        # sequential blanks_on_image validations
        validates_reference :challenges, Challenge::MultipleChoiceChallenge,
            :editor_template => 'blanks_on_image',
            :has_behaviors => ['ResetAnswersOnActivated'],
            :if => :sequential_blanks_on_image_like?


    ################ venn_diagram ########################

        validates_has_behavior :GotoNextOnChallengeComplete, :CompleteOnAllChallengesComplete,  :if => :venn_diagram?
        validates_reference :shared_content_for_text, Text, :if => :venn_diagram?
        validates_length_of :challenges, :is => 1, :message => 'must have exactly one element', :if => :venn_diagram?
        validates_reference :challenges, Challenge::MultipleChoiceChallenge, :editor_template => 'venn_diagram', :if => :venn_diagram?


    ################ image_hotspot ########################

        validates_has_behavior :GotoNextOnChallengeComplete, :CompleteOnAllChallengesComplete,  :if => :image_hotspot?
        validates_reference :shared_content_for_text, Text, :if => :image_hotspot?
        validates_reference :shared_content_for_interactive_image, ComponentOverlay, :if => :image_hotspot?
        validates_length_of :challenges, :is => 1, :message => 'must have exactly one element', :if => :image_hotspot?
        validates_reference :challenges, Challenge::MultipleChoiceChallenge, :editor_template => ['basic_multiple_choice', 'check_many'], :if => :image_hotspot?
        validates_reference :shared_content_for_interactive_image_overlay, Image, :if => :image_hotspot?
        validates_reference :shared_content_for_image_first_overlay_component, AnswerList,  :if => :image_hotspot?
        validates_length_of :shared_content_for_image_overlay_components, :is => 1, :message => 'must have exactly one element', :if => :image_hotspot?
        validates_shared_value :challenges, :answer_list, :shared_content_for_image_first_overlay_component, :if => :image_hotspot?


    ################ compose_blanks ########################
        validates_has_behavior :GotoNextOnChallengeComplete, :CompleteOnAllChallengesComplete,  :if => :compose_blanks?
        validates_reference :shared_content_for_text, Text, :if => :compose_blanks?
        validates_reference :shared_content_for_first_image, Image, :allow_nil => true, :if => :compose_blanks?
        validates_reference :shared_content_for_second_image, Image, :allow_nil => true, :if => :compose_blanks?
        validates_reference :challenges, Challenge::UserInputChallenge, :editor_template => 'basic_user_input', :if => :compose_blanks?
        # validate somewhere (either here or in UserInputChallenge, that thwere is one expectedAnswerMatcher on the validator)

    ################ compose_blanks_on_image ########################
        validates_has_behavior :CompleteOnAllChallengesComplete,  :if => :compose_blanks_on_image?
        validates_does_not_have_behavior :GotoNextOnChallengeComplete,  :if => :compose_blanks_on_image?
        validates_reference :shared_content_for_text, Text, :if => :compose_blanks_on_image?
        validates_reference :shared_content_for_interactive_image, InteractiveCards, :allow_nil => true, :if => :compose_blanks_on_image?
        validates_reference :images_in_interactive_cards_in_shared_content_for_interactive, Image, :if => :compose_blanks_on_image?
        validates_reference :challenges, Challenge::UserInputChallenge, :editor_template => 'basic_user_input', :if => :compose_blanks_on_image?

    ################ matching ########################

        # some of these validations could have been moved down into
        # MatchingBoard, since it is always supposed to work this way,
        # but it is more similar to blanks_on_image to leave them here,
        # and it allows us to make the MatchingBoard test be more encapsulated,
        # whereas this one already needs to know about a number of different
        # components.

        validates_has_behavior :GotoNextOnChallengeComplete, :CompleteOnAllChallengesComplete,  :if => :matching?
        validates_reference :shared_content_for_text, Text, :if => :matching?
        validates_reference :shared_content_for_interactive_image, MatchingBoard, :allow_nil => true, :if => :matching?
        validates_absence_of :shared_content_for_interactive, :if => :matching?
        validates_absence_of :shared_content_for_second_image, :if => :matching?
        validates_one_matching_challenge_button_per_challenge :if => :matching?

        # all matching components behave like consumable
        validates_shared_value :challenges, :answer_list, :matching_board_answer_list, :if => :matching?
        validates_consumable_answers :if => :matching?
        validates_reference :challenges, Challenge::MultipleChoiceChallenge,
            :editor_template => 'matching',
            :does_not_have_behaviors => ['ResetAnswersOnActivated'],
            :if => :matching?

    ################ this or that ########################

        # see also: matching comments

        validates_has_behavior :GotoNextOnChallengeComplete, :CompleteOnAllChallengesComplete,  :if => :this_or_that?
        validates_reference :shared_content_for_text, Text, :if => :this_or_that?
        validates_reference :shared_content_for_interactive_image, TilePromptBoard, :allow_nil => true, :if => :this_or_that?
        validates_absence_of :shared_content_for_interactive, :if => :this_or_that?
        validates_absence_of :shared_content_for_first_image, :if => :this_or_that?
        validates_absence_of :shared_content_for_second_image, :if => :this_or_that?
        validates_one_tile_prompt_per_challenge :if => :this_or_that?


        validates_shared_value :challenges, :answer_list, :tile_prompt_board_answer_list, :if => :this_or_that?
        validates_reference :challenges, Challenge::MultipleChoiceChallenge,
            :editor_template => 'this_or_that',
            :has_behaviors => ['ResetAnswersOnActivated'],
            :if => :this_or_that?


	def this_or_that?
		editor_template == "this_or_that"
	end

    def venn_diagram?
        editor_template == "venn_diagram"
    end

    def basic_multiple_choice?
        editor_template == "basic_multiple_choice"
    end

    def multiple_choice_poll?
        editor_template == "multiple_choice_poll"
    end

    def fill_in_the_blanks?
        editor_template == "fill_in_the_blanks"
    end

    def sequential_fill_in_the_blanks?
        fill_in_the_blanks? && shared_content_for_interactive.nil?
    end

    def consumable_fill_in_the_blanks?
        fill_in_the_blanks? && !shared_content_for_interactive.nil?
    end

    def blanks_on_image?
        editor_template == "blanks_on_image"
    end

    # these two editor templates are almost the same
    def blanks_on_image_like?
        editor_template == "blanks_on_image" || editor_template == "multiple_card_multiple_choice"
    end

    def sequential_blanks_on_image_like?
        blanks_on_image_like? && shared_content_for_interactive.nil?
    end

    def consumable_blanks_on_image_like?
        blanks_on_image_like? && !shared_content_for_interactive.nil?
    end

    def image_hotspot?
        editor_template == "image_hotspot"
    end

    def compose_blanks?
        editor_template == "compose_blanks"
    end

    def compose_blanks_on_image?
        editor_template == "compose_blanks_on_image"
    end

    def matching?
        editor_template == "matching"
    end

    def images_in_interactive_cards_in_shared_content_for_interactive
        if shared_content_for_interactive_image && shared_content_for_interactive_image.is_a?(InteractiveCards)
            # Continue to support the legacy main_component for when duplicating old content
            # FIXME: https://trello.com/c/e5vFpRcK
            return (shared_content_for_interactive_image.overlays.map(&:image) + shared_content_for_interactive_image.overlays.map(&:main_component)).compact
        end
    end

    def shared_content_for_interactive_image_overlay
        if shared_content_for_interactive_image &&
            shared_content_for_interactive_image.is_a?(ComponentOverlay) &&
            (shared_content_for_interactive_image.image || shared_content_for_interactive_image.main_component)
                # Continue to support the legacy main_component for when duplicating old content
                # FIXME: https://trello.com/c/e5vFpRcK
                return shared_content_for_interactive_image.image || shared_content_for_interactive_image.main_component
        end
    end

    def shared_content_for_image_overlay_components
        if shared_content_for_interactive_image && shared_content_for_interactive_image.is_a?(ComponentOverlay)
            return shared_content_for_interactive_image.overlay_components
        end
    end

    def shared_content_for_image_first_overlay_component
        if shared_content_for_image_overlay_components
            return shared_content_for_interactive_image.overlay_components[0]
        end
    end

    def has_inline_blanks?
        fill_in_the_blanks? || compose_blanks?
    end

    def blank_labels
        inline_blank_texts || challenge_overlay_blanks.map(&:label)
    end

    def inline_blank_texts
        return [] unless has_inline_blanks?
        return [] unless shared_content_for_text && shared_content_for_text.respond_to?(:text) && text = shared_content_for_text.text

        blank_texts = []
        split_text_on_special_block(text) do |text_block, in_special_block|
            regular_blanks_regex = %r{
                ([^\!\[]|^)      # match beginning, or anything but exclamation or open bracket
                \[               # match open bracket
                ([^\[\]]+)       # match anything but open or close bracket
                \]               # match close bracket
                (?!\s*\()        # negative lookahead to check for open parenthesis in case of a link
            }x

            if in_special_block
                special_block_regex = /Blank\[/;
                blank_texts += text_block.scan(special_block_regex)
            else
                blank_texts += text_block.scan(regular_blanks_regex).map(&:second)
            end
        end
        blank_texts
    end

    def split_text_on_special_block(text, &block)
        # so much for being DRY :'( see text_helper.js for the JS implementation with more comments.
        text_blocks = [{
            in_special_block: nil,
            text: ''
        }]
        opening_delimiter = nil
        in_special_block = false
        in_blank = false

        start_special_block = lambda do |name, match|
            in_special_block = name
            opening_delimiter = match

            text_blocks << {
                in_special_block: in_special_block,
                text: ''
            }
        end

        continue_active_block = lambda do |active_text_block, match|
            active_text_block[:text] = active_text_block[:text] + match
        end

        end_special_block = lambda do |active_text_block, match|
            active_text_block[:text] = opening_delimiter + active_text_block[:text] + match
            in_special_block = nil

            text_blocks << {
                in_special_block: nil,
                text: ''
            }
        end

        # We want to split on mathjax and code blocks only if they
        # are not inside of blanks, because it should be
        # possible to put mathjax and code blocks inside of a blank.
        text.split(/(    |\n`|\%\%|\$\$|\[|\])/).each do |match|
            active_text_block = text_blocks.last
            if ['%%', '$$', '`', '    '].include?(match)
                # If we're not in a blank, then we can enter or eave a special block.
                if (!in_blank)
                    if in_special_block
                        if in_special_block == 'multiLineCodeTag' && match == '    '
                            continue_active_block.call(active_text_block, match)
                        else
                            end_special_block.call(active_text_block, match)
                        end
                    elsif match == '%%' || match == '$$'
                        start_special_block.call('mathjax', match)
                    elsif match == '`'
                        start_special_block.call('codeTag', match)
                    elsif match == '    '
                        start_special_block.call('multiLineCodeTag', match)
                    end
                else
                    continue_active_block.call(active_text_block, match)
                end
            elsif match == ']' || match == '['
                if (!in_special_block)
                    in_blank = match == '['
                end
                continue_active_block.call(active_text_block, match)
            else
                if in_special_block == 'multiLineCodeTag' && match == '\n'
                    end_special_block.call(active_text_block, match)
                else
                    continue_active_block.call(active_text_block, match)
                end
            end
        end

        if in_special_block == 'multiLineCodeTag'
            active_text_block = text_blocks.last
            end_special_block.call(active_text_block, '')
        end

        results = []
        text_blocks.each do |entry|
            result = yield(entry[:text], entry[:in_special_block])
            results << result
        end
        results.join('')
    end

    def challenge_overlay_blanks
        shared_content_for_interactive_image.overlays.map(&:overlay_components).flatten.sort_by do |blank|
            challenges.index(blank.challenge)
        end
    end

    def matching_challenge_button_count
        return nil unless matching?
        return nil unless shared_content_for_interactive_image.is_a?(MatchingBoard)
        return shared_content_for_interactive_image.matching_challenge_buttons.size
    end

    def matching_board_answer_list
        return nil unless matching?
        return nil unless shared_content_for_interactive_image.is_a?(MatchingBoard)
        return shared_content_for_interactive_image.answer_list
    end

    def tile_prompt_count
        return nil unless this_or_that?
        return nil unless shared_content_for_interactive_image.is_a?(TilePromptBoard)
        return shared_content_for_interactive_image.tile_prompts.size
    end

    def tile_prompt_board_answer_list
        return nil unless this_or_that?
        return nil unless shared_content_for_interactive_image.is_a?(TilePromptBoard)
        return shared_content_for_interactive_image.answer_list
    end

    def text_content
        shared_content_for_text ? shared_content_for_text.text : nil
    end

    def validate_is_not_in_practice_lesson
        if lesson && lesson.is_practice?
            errors.add(:lesson, "for poll frame must not be a practice lesson")
        end
    end


end
