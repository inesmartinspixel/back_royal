class Lesson::Content::FrameList::Frame::Componentized::Component::AnswerMatcher::MatchesExpectedText < Lesson::Content::FrameList::Frame::Componentized::Component::AnswerMatcher

	key :expectedText, String
    key :caseSensitive, Boolean
    key :correctThreshold, Numeric
    key :mode, String

	validates_presence_of :expectedText

	def expected_text
		expectedText
	end

	def expected_text=(x)
		expectedText = x
	end

end
