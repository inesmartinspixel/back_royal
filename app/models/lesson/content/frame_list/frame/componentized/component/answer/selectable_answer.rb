class Lesson::Content::FrameList::Frame::Componentized::Component::Answer::SelectableAnswer < Lesson::Content::FrameList::Frame::Componentized::Component::Answer

	references('text').through('text_id');
    references('image').through('image_id');

    # all these should be set to Numeric, but there are cases in the content
    # where it is a string
    key :x, Object
    key :y, Object
    key :height, Object
    key :width, Object
    key :hasBackground, Boolean

    validates_presence_of :text_or_image_or_position, :message => 'must be set'
    validates_reference :text, Text, :allow_nil => true
    validates_reference :image, Image, :allow_nil => true

    def text_or_image_or_position
    	text || image || position
    end

    def position
    	if !x.nil? || !y.nil?
    		{
    			x: x,
    			y: y
    		}
    	end
    end

end
