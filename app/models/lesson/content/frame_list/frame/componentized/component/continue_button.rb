class Lesson::Content::FrameList::Frame::Componentized::Component::ContinueButton < Lesson::Content::FrameList::Frame::Componentized::Component

	# this is an abstract class.  Only allow subclasses to be created
	validates_inclusion_of :is_continue_button_subclass?, :in => [true], :message => "must be true"

	def is_continue_button_subclass?
		self.class != Lesson::Content::FrameList::Frame::Componentized::Component::ContinueButton
	end

end
