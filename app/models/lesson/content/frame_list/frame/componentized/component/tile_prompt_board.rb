class Lesson::Content::FrameList::Frame::Componentized::Component::TilePromptBoard < Lesson::Content::FrameList::Frame::Componentized::Component

    references('tile_prompts').through('tile_prompt_ids')
    references('answer_list').through('answer_list_id')
    references('challenges_component').through('challenges_component_id');

    validates_reference :tile_prompts, TilePrompt
    validates_reference :answer_list, AnswerList
    validates_reference :challenges_component, Challenges

end
