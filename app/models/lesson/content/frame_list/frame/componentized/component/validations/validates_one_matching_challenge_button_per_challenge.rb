# validates that all elements in a reference list shared some property (like the answer list should
# be shared among all challenges on a this_or_that frame)


class Lesson::Content::FrameList::Frame::Componentized
    class Component
        module Validations

            class ValidatesOneMatchingChallengeButtonPerChallenge < ActiveModel::EachValidator
                # -*- SkipSchemaAnnotations
                def validate_each(record, attribute, ignore)
                    if record.matching_challenge_button_count != record.challenges.size
                        record.errors.add(attribute, "must be equal to the number of challenges (#{record.matching_challenge_button_count} != #{record.challenges.size})")
                    end
                end
            end

            class ValidatesOneTilePromptPerChallenge < ActiveModel::EachValidator
                # -*- SkipSchemaAnnotations
                def validate_each(record, attribute, ignore)
                    if record.tile_prompt_count != record.challenges.size
                        record.errors.add(attribute, "must be equal to the number of challenges (#{record.tile_prompt_count} != #{record.challenges.size})")
                    end
                end
            end

        end
    end
end


module ActiveModel
    module Validations
        module HelperMethods

            def validates_one_matching_challenge_button_per_challenge(options = {})
                validates_with Lesson::Content::FrameList::Frame::Componentized::Component::Validations::ValidatesOneMatchingChallengeButtonPerChallenge, _merge_attributes([:matching_challenge_button_count, options])
            end

            def validates_one_tile_prompt_per_challenge(options = {})
                validates_with Lesson::Content::FrameList::Frame::Componentized::Component::Validations::ValidatesOneTilePromptPerChallenge, _merge_attributes([:tile_prompt_count, options])
            end

        end
    end
end
