class Lesson::Content::FrameList::Frame::Componentized::Component::Layout < Lesson::Content::FrameList::Frame::Componentized::Component

	# this is an abstract class.  Only allow subclasses to be created
	validates_inclusion_of :is_layout_subclass?, :in => [true], :message => "must be true"

	def is_layout_subclass?
		self.class != Lesson::Content::FrameList::Frame::Componentized::Component::Layout
	end

end
