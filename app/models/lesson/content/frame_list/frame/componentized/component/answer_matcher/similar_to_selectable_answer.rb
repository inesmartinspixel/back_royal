class Lesson::Content::FrameList::Frame::Componentized::Component::AnswerMatcher::SimilarToSelectableAnswer < Lesson::Content::FrameList::Frame::Componentized::Component::AnswerMatcher

	references('answer').through('answer_id')

	validates_reference :answer, Answer::SelectableAnswer

end
