class Lesson::Content::FrameList::Frame::Componentized::Component::UserInputMessage < Lesson::Content::FrameList::Frame::Componentized::Component

    references('challenge').through('challenge_id');
    references('message_text').through('message_text_id');
    key('show_on_correct_answer');

    validates_reference :challenge, Challenge::UserInputChallenge
    validates_reference :message_text, Text
    validates_inclusion_of :show_on_correct_answer, :in => [true]

end
