class Lesson::Content::FrameList::Frame::Componentized::Component::Challenge::UserInputChallenge < Lesson::Content::FrameList::Frame::Componentized::Component::Challenge

    references('messages').through('message_ids')

    validates_length_of :expected_answer_matchers, :is => 1, :message => 'must have exactly one element'
    validates_reference :expected_answer_matchers, AnswerMatcher::MatchesExpectedText
    validates_reference :messages, UserInputMessage, :allow_nil => true

    validates_has_behavior :CompleteOnCorrect, :if => :basic_user_input?
    validates_reference :validator, ChallengeValidator,
    	:has_behaviors => ['HasAllExpectedAnswers', 'HasNoUnexpectedAnswers'],
    	:if => :basic_user_input?


    def basic_user_input?
    	editor_template == 'basic_user_input'
    end

    def correct_answer_text
    	expected_answer_matchers[0].expectedText
    end

    def correct_answer_image
    	nil
    end

end
