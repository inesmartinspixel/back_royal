class Lesson::Content::FrameList::Frame::Componentized::Component::InteractiveCards < Lesson::Content::FrameList::Frame::Componentized::Component

	key :force_card_height, Boolean
	key :wide, Boolean

	references('overlays').through('overlay_ids');
	references('challenges_component').through('challenges_component_id');

	validates_length_of :overlays, :minimum => 1, :message => 'must have at least one element'
	validates_reference :overlays, ComponentOverlay
	validates_reference :challenges_component, Challenges
	validates_reference :overlay_components, ChallengeOverlayBlank

	def overlay_components
		return [] unless overlays && overlays.map { |overlay| overlay.respond_to?(:overlay_components) }.flatten == [true]
		overlays.map(&:overlay_components).flatten
	end

end
