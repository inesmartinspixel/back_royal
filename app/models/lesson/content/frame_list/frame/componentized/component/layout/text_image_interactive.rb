class Lesson::Content::FrameList::Frame::Componentized::Component::Layout::TextImageInteractive < Lesson::Content::FrameList::Frame::Componentized::Component::Layout

    key :content_for_header_id
    key :context_image_size
    key :context_image_2_size

	# either one or more of the staticContent properties can be set ...
    references('static_content_for_text').through('content_for_text_id');
    references('static_content_for_first_image').through('content_for_image_id');
    references('static_content_for_second_image').through('content_for_image_2_id');
    references('static_content_for_interactive').through('content_for_interactive_id');

    # ... or a target component can be set, whose viewModel must
    # define contentForTextViewModel, contentForImageViewModel, ...
    references('target').through('target_id');

	validates_reference :static_content_for_text, Text, :if => :no_interaction?
	validates_reference :static_content_for_first_image, Image, :allow_nil => true, :if => :no_interaction?
    validates_reference :static_content_for_second_image, Image, :allow_nil => true, :if => :no_interaction?
	validates_absence_of :static_content_for_interactive, :if => :no_interaction?
	validates_continue_button ContinueButton::AlwaysReadyContinueButton, :if => :no_interaction?

	def no_interaction?
		editor_template == 'no_interaction'
	end

    def text_content
        static_content_for_text ? static_content_for_text.text : nil
    end

end
