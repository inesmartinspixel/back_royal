class Lesson::Content::FrameList::Frame::Componentized::Component::Challenge::MultipleChoiceChallenge < Lesson::Content::FrameList::Frame::Componentized::Component::Challenge

	references('answer_list').through('answer_list_id')
    references('messages').through('message_ids')
    key 'no_incorrect_answers', Boolean


    key 'usage_report', Object # this should not actually be there, but is there sometimes due to a bug: https://trello.com/c/Sgj6HPHY/1005-bug-usage-report-included-in-frame-data-when-duplicating-a-lesson
    key '_proxyCorrectAnswers', Object # also due to an old bug

	validates_reference :answer_list, AnswerList
    validates_reference :messages, MultipleChoiceMessage, :allow_nil => true
    validates_reference :expected_answer_matchers, AnswerMatcher::SimilarToSelectableAnswer

	validates_has_behavior :ImmediateValidation,
        :CompleteOnCorrect,
        :ResetAnswersOnActivated,
        :DisallowMultipleSelect,
        :ShowCorrectStyling,
        :FlashIncorrectStyling,
        :ClearMessagesOnAnswerSelect,
		:if => :this_or_that?
	validates_single_correct_answer :if => :this_or_that?

    validates_has_behavior :CompleteOnCorrect,
        :ResetAnswersOnActivated,
        :DisallowMultipleSelect,
        :ShowCorrectStyling,
        :FlashIncorrectStyling,
        :ClearMessagesOnAnswerSelect,
        :if => :venn_diagram?
    validates_single_correct_answer :if => :venn_diagram?

	validates_has_behavior :ImmediateValidation,
        :CompleteOnCorrect,
        # :ResetAnswersOnActivated, # may be on or off depending on sequential/consumable.  Challenges.rb validates
        :DisallowMultipleSelect,
        :ShowCorrectStyling,
        :FlashIncorrectStyling,
        :ClearMessagesOnAnswerSelect,
		:if => :blanks?
	validates_single_correct_answer :if => :blanks?


    validates_has_behavior :ImmediateValidation,
        :CompleteOnCorrect,
        :ResetAnswersOnActivated,
        :ShowCorrectStyling,
        :FlashIncorrectStyling,
        :ClearMessagesOnAnswerSelect,
        :if => :basic_multiple_choice_or_check_many_with_incorrect_answers?

    validates_has_behavior :DisallowMultipleSelect, :if => :basic_multiple_choice?
    validates_single_correct_answer :if => :basic_multiple_choice_with_incorrect_answers?
    validates_correct_answer_exists :if => :check_many_with_incorrect_answers?

    validates_has_behavior :CompleteOnCorrect,
        :ResetAnswersOnActivated,
        :FlashIncorrectStyling, # seems like this should not be there, but scared to mess up existing content
        :ClearMessagesOnAnswerSelect,
        :if => :basic_multiple_choice_or_check_many_with_no_incorrect_answers?

    class ValidatesNoIncorrectAnswers < ActiveModel::EachValidator

        def validate_each(record, attribute, validator)

            if record.validator.behaviors['HasAllExpectedAnswers']
                record.errors.add(:validator, 'must not include behavior HasAllExpectedAnswers')
            end

            if record.validator.behaviors['HasNoUnexpectedAnswers']
                record.errors.add(:validator, 'must not include behavior HasNoUnexpectedAnswers')
            end

            if record.validator.expected_answer_matchers && record.validator.expected_answer_matchers.any?
                record.errors.add(:validator, 'must not have any expected answer matchers')
            end

        end

    end
    validates_with ValidatesNoIncorrectAnswers, _merge_attributes([:validator, {:if => :no_incorrect_answers?}])


	def this_or_that?
		editor_template == "this_or_that"
	end

	def blanks?
		fill_in_the_blanks? || blanks_on_image?
	end

	def fill_in_the_blanks?
		editor_template == "fill_in_the_blanks"
	end

	def blanks_on_image?
		editor_template == "blanks_on_image"
	end

    def venn_diagram?
        editor_template == "venn_diagram"
    end

    def check_many?
        editor_template == "check_many"
    end

    def basic_multiple_choice?
        editor_template == "basic_multiple_choice"
    end

    def check_many_with_incorrect_answers?
        editor_template == "check_many" && !no_incorrect_answers?
    end

    def basic_multiple_choice_with_incorrect_answers?
        editor_template == "basic_multiple_choice" && !no_incorrect_answers?
    end

    def basic_multiple_choice_or_check_many_with_incorrect_answers?
        (basic_multiple_choice? || check_many?) && !no_incorrect_answers?
    end

    def basic_multiple_choice_or_check_many_with_no_incorrect_answers?
        (basic_multiple_choice? || check_many?) && no_incorrect_answers?
    end

	def correct_answer
		result = meets_correct_requirement(1)
        result[0].nil? ? nil : result[0][0]
	end

    def correct_answers
        result = meets_correct_requirement
        result[0].nil? ? [] : result[0]
    end

    def correct_answer_text
        correct_answer.text.nil? ? nil : correct_answer.text.text
    end

    def correct_answer_image
        correct_answer.image
    end

    def answers
        answers = nil
        begin
            answers = answer_list.answers
        rescue NoMethodError => e
        end
        return answers
    end

    # returns the correct answer, and, if it is nil, the reason
    # it is nil (messages are returned as an array of arrays, each one
    # having the relevant attribute first and the message second)
    def correct_answer_and_error_messages
        meets_correct_requirement 1
    end

    def correct_answer_exists_and_error_messages
        meets_correct_requirement
    end


    def meets_correct_requirement(correct_count = nil)

        if validator.nil?
            return [nil, [[:validator, "must be defined"]]]
        end


        missing_behavior_messages = []
        ['HasAllExpectedAnswers', 'HasNoUnexpectedAnswers'].each do |behavior|
            if !validator.behaviors.key?(behavior.to_s) && !validator.behaviors.key?(behavior.to_sym)
                missing_behavior_messages.push([:validator, "must have behavior #{behavior.inspect}"])
            end
        end

        if missing_behavior_messages.any?
            return [nil, missing_behavior_messages]
        end

        if answers.nil?
            return [nil, [[:answers, "must be defined"]]]
        end

        expected_matcher_class = Lesson::Content::FrameList::Frame::Componentized::Component::AnswerMatcher::SimilarToSelectableAnswer

        if correct_count
            # there should be exactly the supplied number of answer matchers
            if validator.expected_answer_matchers.nil? || validator.expected_answer_matchers.length != correct_count
                return [nil, [[:validator, "must have exactly #{correct_count} expected answer matcher"]]]
            end

        else
            if validator.expected_answer_matchers.nil? || validator.expected_answer_matchers.length == 0
                return [nil, [[:validator, "must have at least one expected answer matcher"]]]
            end
        end

        # if there is, then it should be a SimilarToSelectableAnswer
        if validator.expected_answer_matchers.first.class != expected_matcher_class
            return [nil, [[:validator, "answer matcher must be a #{expected_matcher_class.inspect} but is #{validator.expected_answer_matchers.first.class.inspect}"]]]
        end


        # if it is, then its answer should refer to an answer in the list
        if !answers.nil? && !answers.include?(validator.expected_answer_matchers.first.answer)
            return [nil, [[:validator, "correct answer must refer to an answer in the answer list"]]]
        end

        return [validator.expected_answer_matchers.map(&:answer), []]

    end


end
