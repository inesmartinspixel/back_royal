# validates that all elements in a reference list shared some property (like the answer list should
# be shared among all challenges on a this_or_that frame)

class Lesson::Content::FrameList::Frame::Componentized
    class Component
        module Validations
            class ValidatesContinueButton < ActiveModel::EachValidator
                # -*- SkipSchemaAnnotations

                def validate_each(record, attribute, value)
                    if !value.nil? && !value.is_a?(options[:type])
                        record.errors.add(attribute, "must be instance of #{options[:type].inspect} but is #{value.class.inspect}")
                    end
                end

            end
        end
    end
end


module ActiveModel
    module Validations
        module HelperMethods

            def validates_continue_button(type, options = {})
                validates_with Lesson::Content::FrameList::Frame::Componentized::Component::Validations::ValidatesContinueButton, _merge_attributes([:continue_button, {
                	type: type,
                }.merge(options)])
            end

        end
    end
end
