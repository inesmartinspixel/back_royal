class Lesson::Content::FrameList::Frame::Componentized::Component::ChallengeValidator < Lesson::Content::FrameList::Frame::Componentized::Component

	references('challenge').through('challenge_id');
    references('expected_answer_matchers').through('expected_answer_matcher_ids');

    validates_reference :challenge, Challenge
    validates_reference :expected_answer_matchers, AnswerMatcher, :allow_nil => true

end
