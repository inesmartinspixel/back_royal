class Lesson::Content::FrameList::Frame::Componentized::Component::ContinueButton::ChallengesContinueButton < Lesson::Content::FrameList::Frame::Componentized::Component::ContinueButton

	references('target_component').through('target_component_id');

    key :show_continue_when_ready_to_validate, Boolean
    key :mini_instructions, String # deprecated

	validates_reference :target_component, Challenges

end
