class Lesson::Content::FrameList::Frame::Componentized::Component::Image < Lesson::Content::FrameList::Frame::Componentized::Component

	key :label, String
	key :image, Hash

	allow_unreferenced

	validates_presence_of :label # mybe this should be optional.  Probably don't need it in all cases
	validates_presence_of :image
	validates_presence_of :formats, :if => :image?
	validates_presence_of :original_format_url, :if => :formats?

	def formats
		image && image['formats']
	end

	def formats?
		!formats.blank?
	end

	def image?
		!image.blank?
	end

	def original_format_url
		formats && formats['original'] && formats['original']['url']
	end

end
