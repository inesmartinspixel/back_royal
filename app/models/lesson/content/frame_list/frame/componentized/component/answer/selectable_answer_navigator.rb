class Lesson::Content::FrameList::Frame::Componentized::Component::Answer::SelectableAnswerNavigator < Lesson::Content::FrameList::Frame::Componentized::Component::Answer

    references('challenge').through('challenge_id');
    references('answer_matcher').through('answer_matcher_id');
    references('frame_navigator').through('frame_navigator_id');
    key('event')
    key('next_frame_id')

    validates_reference :challenge, Challenge::MultipleChoiceChallenge
    validates_reference :answer_matcher, AnswerMatcher::SimilarToSelectableAnswer
    validates_reference :frame_navigator, FrameNavigator

    validates_presence_of :event

end
