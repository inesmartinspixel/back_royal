class Lesson::Content::FrameList::Frame::Componentized::Component::AnswerList < Lesson::Content::FrameList::Frame::Componentized::Component

	references('answers').through('answer_ids')
    references('venn_diagram_headers').through('venn_diagram_header_ids');

	key :skin, String
    key :force_single_column, Boolean

	class ValidatesRandomizeAnswerOrder < ActiveModel::EachValidator

        def validate_each(record, attribute, value)
        	return unless value
        	return unless value.key?('ensureLast')

        	if value['ensureLast'].empty?
        		record.errors.add(:"#{attribute}.ensureLast", "cannot be empty")
        	end

        	unexpected_entries = value['ensureLast'] - record.answers.map(&:id)
        	if unexpected_entries.any?
        		record.errors.add(:"#{attribute}.ensureLast", "must include ids of answer in the list. #{unexpected_entries.inspect} are not in the list")
        	end
        end

    end

    class ValidatesImageHotspotAnswers < ActiveModel::EachValidator

        def validate_each(record, attribute, value)
            return unless value
            value.each_with_index do |answer, i|
                if answer.x.blank?
                    record.errors.add(:"answer_#{i+1}", "must have a value for 'x'")
                end

                if answer.y.blank?
                    record.errors.add(:"answer_#{i+1}", "must have a value for 'y'")
                end
            end
        end

    end


	validates_reference :answers, Answer::SelectableAnswer
	validates_presence_of :skin
	validates_inclusion_of :skin, :in => %w(buttons checkboxes venn_diagram image_hotspot matching poll)
	validates_length_of :answers, :minimum => 2, :too_short => "is too short (min is 2)", :unless => :image_hotspot?
    validates_length_of :answers, :minimum => 1, :too_short => "is too short (min is 1)", :if => :image_hotspot?
    validates_with ValidatesRandomizeAnswerOrder, _merge_attributes([:randomize_answer_order_options, {}])
    validates_with ValidatesImageHotspotAnswers, _merge_attributes([:answers, {:if => :image_hotspot?}])

    validates_reference :venn_diagram_headers, Text, :if => :venn_diagram?
    validates_length_of :venn_diagram_headers,  :is => 2, :message => 'must have exactly two elements', :if => :venn_diagram?

    def venn_diagram?
        skin == "venn_diagram"
    end

    def image_hotspot?
        skin == 'image_hotspot'
    end

    def randomize_answer_order_options
    	if behaviors && behaviors.key?('RandomizeAnswerOrder')
			behaviors['RandomizeAnswerOrder']
    	else
    		nil
    	end
    end






end
