# validates that all elements in a reference list shared some property (like the answer list should
# be shared among all challenges on a this_or_that frame)


class Lesson::Content::FrameList::Frame::Componentized
    class Component
        module Validations
            class ValidatesSharedValue < ActiveModel::EachValidator
                # -*- SkipSchemaAnnotations

                def validate_each(record, attribute, value)
                    if value.nil?
                        return
                    elsif value.is_a? Array
                        value.each_with_index do |item, i|
                            # singularize so the message says:
                            # "Challenge 1 is invalid becuase ..." rather than
                            # "Challenges 1 is invalid because ..."
                            validate_item(record, :"#{attribute.to_s.singularize}_#{i+1}", item)
                        end
                    else
                        validate_item(record, attribute, value)
                    end
                end

                def validate_item(record, attribute, item)

                    begin
                        expected_value = record.send(options[:prop_on_record])
                    rescue NoMethodError => e
                        record.errors.add("#{options[:prop_on_record]}", "is not a method")
                        return
                    end

                    begin
                        actual_value = item.send(options[:prop_on_association])
                    rescue NoMethodError => e
                        record.errors.add("#{attribute}_#{options[:prop_on_association]}", "is not a method")
                        return
                    end

                    if expected_value != actual_value
                        record.errors.add("#{attribute}_#{options[:prop_on_association]}", "must equal value of #{options[:prop_on_record]}")
                    end
                end

            end
        end
    end
end

module ActiveModel
    module Validations
        module HelperMethods

            def validates_shared_value(attr_name, prop_on_association, prop_on_record, options = {})
                validates_with Lesson::Content::FrameList::Frame::Componentized::Component::Validations::ValidatesSharedValue, _merge_attributes([attr_name, {
                	:prop_on_association => prop_on_association,
                	:prop_on_record => prop_on_record
                }.merge(options)])
            end

        end
    end
end
