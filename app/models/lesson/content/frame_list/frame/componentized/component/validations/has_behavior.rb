class Lesson::Content::FrameList::Frame::Componentized
    class Component
        module Validations
            class HasBehavior < ActiveModel::EachValidator
                # -*- SkipSchemaAnnotations

                def validate_each(record, attribute, value)
                    options[:behaviors].each do |behavior|
                        if !record.behaviors.key?(behavior)
                            record.errors.add(behavior.to_s.underscore, "behavior must be included")
                        end
                    end
                end

            end
        end
    end
end


module ActiveModel
    module Validations
        module HelperMethods

            def validates_has_behavior(*args)
            	options = args.extract_options!
                validates_with Lesson::Content::FrameList::Frame::Componentized::Component::Validations::HasBehavior, _merge_attributes([:behaviors, {:behaviors => args}.merge(options)])
            end

        end
    end
end
