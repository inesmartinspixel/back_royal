class Lesson::Content::FrameList::Frame::Componentized::Component::Challenge < Lesson::Content::FrameList::Frame::Componentized::Component

	key 'unlink_blank_from_answer', Boolean

	references('validator').through('validator_id');

    references('content_for_text').through('content_for_text_id');
    references('content_for_image').through('content_for_image_id');
    references('content_for_interactive').through('content_for_interactive_id');
    references('content_for_interactive_image').through('content_for_interactive_image_id');

    validates_reference :validator, ChallengeValidator
    key :unlink_blank_from_answer, Boolean

	# this is an abstract class.  Only allow subclasses to be created
    validates_inclusion_of :is_challenge_subclass?, :in => [true], :message => "must be true"

	def is_challenge_subclass?
		self.class != Lesson::Content::FrameList::Frame::Componentized::Component::Challenge
	end

	def expected_answer_matchers
		if validator.nil?
			return []
		elsif !validator.respond_to?(:expected_answer_matchers)
			return []
		elsif validator.expected_answer_matchers.nil?
			return []
		else
			return validator.expected_answer_matchers
		end
	end

end
