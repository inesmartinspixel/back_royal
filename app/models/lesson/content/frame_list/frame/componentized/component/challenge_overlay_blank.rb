class Lesson::Content::FrameList::Frame::Componentized::Component::ChallengeOverlayBlank < Lesson::Content::FrameList::Frame::Componentized::Component

	references('text').through('text_id');
    references('image').through('image_id');
    references('challenge').through('challenge_id');

    key :invisible, Boolean
    key :hasBackground, Boolean

    validates_presence_of :text_or_image, :message => 'must be set'
    validates_reference :text, Text, :allow_nil => true
    validates_reference :image, Image, :allow_nil => true
    validates_reference :challenge, Challenge

    #FIXME: more validations

    def text_or_image
    	text || image
    end

    def label
        if text
            text.text
        elsif image
            image.label
        end
    end



end
