class Lesson::Content::FrameList::Frame::Componentized::Component::Text < Lesson::Content::FrameList::Frame::Componentized::Component

    key :text, String
    key :formatted_text, String
    key :allowBlank, Boolean
    key :alignment, String
    key :fontSize, Numeric
    references('modals').through('modal_ids');
    references('challenges_component').through('challenges_id');

    validates_presence_of :text, allow_blank: false, unless: :allowBlank
    validates_presence_of :formatted_text, allow_blank: false, unless: :allowBlank

    # modals can be any component
    validates_reference :modals, Lesson::Content::FrameList::Frame::Componentized::Component, :allow_nil => true

    validate :validate_formatted_text_is_really_formatted
    validate :validate_formatted_text_no_mathjax_errors

    # see https://trello.com/c/olQA7q8K/746-bug-modals-aren-t-being-formatted
    def validate_formatted_text_is_really_formatted
        if formatted_text && formatted_text != '' && !formatted_text.match('<')
            err = RuntimeError.new('formatted_text is not formatted')
            Raven.capture_exception(err, {
                extra: {
                    text: self.text,
                    formatted_text: self.formatted_text,
                    lesson_id: self.lesson.id,
                    frame_id: self.frame.id,
                    frame_index: self.frame.index,
                    url: self.frame.editor_link
                }
            })
            errors.add(:formatted_text, "formatted_text is not formatted.  Make a change to it to force reformatting.")
        end
    end

    def validate_formatted_text_no_mathjax_errors
        if formatted_text && formatted_text.match(/math processing error/i)
            errors.add(:formatted_text, "formatted_text appears to contain improperly processed MathJax. This may be caused by invalid whitespaces or extended unicode. Make a change to the math expression to force reformatting.")
        end
    end

end
