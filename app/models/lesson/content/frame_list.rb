class Lesson::Content::FrameList < Lesson::Content

    validates_inclusion_of :lesson_type, :in => %w(frame_list)

    # could use validates_associated, but it gives no useful message
    validate :validate_frame_ids
    validate :validate_frames, :if => :pinned?

    def self.decorate_frame_json(lesson_id, frame_hashes)
        # we delegate this down to Componentized because the logic involved only
        # applies to componentized frames.  If we have other frame types again
        # at some point, or if there is more decoration that is not related
        # specifically to componentized stuff, then there could be multiple
        # processing steps here
        frame_hashes = Lesson::Content::FrameList::Frame::Componentized.decorate_frame_json(lesson_id, frame_hashes)
        return frame_hashes
    end

    # just used for debugging, research
    def self.get_all_components
        Lesson.where(lesson_type: 'frame_list').all.map(&:content).map(&:frames).flatten.map(&:components).flatten
    end

    def merge_hash(hash)
        hash = hash.unsafe_with_indifferent_access
        if hash.key?('frames')
            self.frames = remove_usage_reports(hash['frames'])
        end

        if hash.key?('lesson_type')
            self.lesson_type = hash['lesson_type']
        end

        self
    end

    def pinned?
        lesson.pinned
    end

    def save_warnings
        validate_frames
        errors.full_messages
    end

    def validate_frame_ids
        return unless frames
        duplicate_ids = frames.map{ |f| f['id'] }.group_by{ |id| id }.select { |k, v| v.size > 1 }.map(&:first)
        if !duplicate_ids.empty?
            errors.add(:frames,  "Duplicate frame IDs found in lesson: #{duplicate_ids.join(', ')}")
        end
    end

    def validate_frames
        return unless frames
        frames.each_with_index do |frame, i|
            if !frame.valid?
                frame.errors.each do |attr, msg|
                    errors.add(:"frame_#{i+1}.#{attr}",  msg)
                end
            end
        end
    end

    # since usage reports are sent down as part of components, they will also
    # be sent back up in save calls.  We don't want to save them to the db, so
    # we remove them here
    def remove_usage_reports(frames)
       frames.each do |frame|
            next unless frame['components']
            frame['components'].each do |component|
                component.delete('usage_report')
            end
        end
        frames
    end

end
