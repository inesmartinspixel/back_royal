class Lesson::ToJsonFromApiParams

    include ToJsonFromApiParamsHelpers::ToJsonFromApiParamsMixin
    include ToJsonFromApiParamsHelpers::ContentItemMixin

    self.base_klass = Lesson

    def initialize(params)

        params = do_basic_params_prep(params)

        filters = set_filters({
            archived: :bool,
            id: :array_of_uuids,
            version_id: :uuid,
            published: :bool,
            content_search: /.*/,
            comments_search: /.*/,
            in_users_locale_or_en: :bool,
            in_locale_or_en: :locale,
            locale: :locale,
            locale_pack_id: :array_of_uuids,
            updated_since: :number,
            updated_before: :number,
            is_practice: :bool,
            filter_editable_resources: :uuid
        })

        # if no user_id is passed in, and lesson_progress was
        # not explicitly requested, then assume it was not desired
        params[:except] ||= []
        if !params[:user_id] && !(params[:fields] || []).include?('lesson_progress')
            params[:except] << 'lesson_progress'
        end

        avaliable_fields = %w|
                id title description frame_count entity_metadata
                client_requirements lesson_type
                author updated_at modified_at archived
                old_version pinned_title pinned_description
                was_published pinned tag stream_titles version_id
                last_editor published_at published_version_id version_history
                frames lesson_progress key_terms assessment unrestricted locale
                locale_pack practice_frames test
            |

        fields = set_fields({
            available_fields: avaliable_fields,
            aliases: {

                # we need the ALL alias because lesson_Controller#index defaults to
                # a subset, but sometimes we need more
                ALL: avaliable_fields,
                EDITOR_FIELDS: ['old_version', 'pinned_title',
                                'pinned_description', 'was_published', 'pinned',
                                'tag', 'stream_titles', 'last_editor', 'published_at',
                                'published_version_id', 'version_history'],

                UPDATE_FIELDS: ['updated_at', 'published_at', 'modified_at',
                                'version_history', 'published_version_id', 'was_published',
                                'pinned', 'pinned_title', 'pinned_description', 'version_id',
                                'old_version']

            }
        })

        @user_id = params[:user_id]
        @query_builder = CrazyQueryBuilder.new('lessons_versions')
        @selects, @wheres = @query_builder.selects, @query_builder.wheres
        @query_builder.limit = @limit = params[:limit]

        handle_content_item_filters
        set_default_selects
        handle_content_item_selects
        handle_is_practice_filter
        @query_builder.selects.slice!(*fields)

        join_authors if fields.include?('author')
        join_last_editors if fields.include?('last_editor')
        join_and_select_entity_metadata if fields.include?('entity_metadata')
        join_lesson_progress if fields.include?('lesson_progress')
        join_practice_frames if fields.include?('practice_frames')

        @query_builder.wheres << "lessons_versions.archived = #{filters[:archived] ? "TRUE" : "FALSE"}"  if filters.key?(:archived)

        [:content_search, :comments_search].each do |key|
            next if filters[key].blank?
            column_prefix = key.to_s.gsub('_search', '')

            # extract any double-quoted groups for use in ilike queries
            search_text = filters[key]
            groups = /(?<=")(?:\\.|[^"\\])*(?=")/.match(search_text).to_a || []
            group_searches = groups.map { |g|
                # filter out bad characters that could mess up the query, escape single quotes
                filtered = escape_for_like_query(g)
                " AND #{column_prefix}_text ILIKE '%#{filtered}%' "
            }.join()

            vector_search = Lesson.get_vector_search_sql(search_text)

            # build query
            @query_builder.wheres << "lessons_versions.id IN (SELECT lesson_id FROM lesson_fulltext WHERE #{column_prefix}_vector @@ to_tsquery('english', #{vector_search}) #{group_searches} )"
        end


        if filters[:filter_editable_resources]
            # find all the lessons this user is an author of this user has been granted access to
            @query_builder.wheres << "lessons_versions.author_id = '#{filters[:filter_editable_resources]}' OR lessons_versions.id IN (
                SELECT resource_id
                FROM roles
                    join users_roles on roles.id = users_roles.role_id
                WHERE
                    resource_type='Lesson'
                    AND users_roles.user_id = '#{filters[:filter_editable_resources]}'
                    AND name IN ('lesson_editor', 'previewer', 'reviewer'))"
        end



    end

    def set_default_selects


        @query_builder.selects["id"] = "lessons_versions.id"
        @query_builder.selects["version_id"] = "lessons_versions.version_id"
        @query_builder.selects["lesson_type"] = "lessons_versions.lesson_type"
        @query_builder.selects["frame_count"] = "lessons_versions.frame_count"
        @query_builder.selects["description"] = "lessons_versions.description"
        @query_builder.selects["tag"] = "lessons_versions.tag"
        @query_builder.selects["title"] = "lessons_versions.title"
        @query_builder.selects["assessment"] = "lessons_versions.assessment"
        @query_builder.selects["unrestricted"] = "lessons_versions.unrestricted"
        @query_builder.selects["client_requirements"] = "cast(row(0, TRUE) as client_requirements_json_v1)"
        @query_builder.selects["archived"] = "lessons_versions.archived"
        @query_builder.selects["modified_at"] = "cast(EXTRACT(EPOCH FROM lessons_versions.modified_at) as int)"
        @query_builder.selects["updated_at"] = "cast(EXTRACT(EPOCH FROM lessons_versions.updated_at) as int)"
        @query_builder.selects["pinned"] = "lessons_versions.pinned"
        @query_builder.selects["pinned_description"] = "lessons_versions.pinned_description"
        @query_builder.selects["pinned_title"] = "lessons_versions.pinned_title"
        @query_builder.selects["was_published"] = "lessons_versions.was_published"
        @query_builder.selects["key_terms"] = "COALESCE(lessons_versions.key_terms, ARRAY[]::varchar[])"
        @query_builder.selects["author"] = cast_user("authors")
        @query_builder.selects["last_editor"] = cast_user("last_editors")
        @query_builder.selects["frames"] = "(lessons_versions.content_json->>'frames')::json"
        @query_builder.selects["locale"] = "lessons_versions.locale"
        @query_builder.selects["test"] = "lessons_versions.test"
        @query_builder.selects["old_version"] = "
            lessons_versions.updated_at != (
                SELECT updated_at
                FROM lessons_versions old_versions
                WHERE old_versions.id = lessons_versions.id
                ORDER BY updated_at DESC
                LIMIT 1
            )
        "

        @query_builder.selects['stream_titles'] = "
            COALESCE(
                (SELECT array_to_json(array_agg(title)) FROM
                    (
                        SELECT
                            lesson_streams.title
                        FROM lesson_streams
                        JOIN lesson_to_stream_joins
                            ON lesson_to_stream_joins.stream_id = lesson_streams.id
                            AND lesson_to_stream_joins.lesson_id = lessons_versions.id
                    ) stream_titles
                ),
                '[]'::json)

        "

        @query_builder.selects['version_history'] = "
            COALESCE(
                (SELECT array_to_json(array_agg(row_to_json(version_history))) FROM
                    (
                        SELECT
                            #{cast_user('version_editors')} last_editor,
                            cast(EXTRACT(EPOCH FROM version_history.updated_at) as int) updated_at,
                            version_id,
                            pinned,
                            pinned_title,
                            pinned_description,
                            was_published
                        FROM lessons_versions version_history
                            LEFT JOIN users version_editors ON version_history.last_editor_id = version_editors.id
                        WHERE version_history.id = lessons_versions.id
                        ORDER BY version_history.updated_at DESC
                    ) version_history
                ),
                '[]'::json)
        "

        @query_builder.selects["lesson_progress"] = "
            CASE WHEN lesson_progress.id IS NOT NULL THEN
                row_to_json(
                    cast(row(
                        lesson_progress.user_id,
                        cast(EXTRACT(EPOCH FROM lesson_progress.updated_at) as int),
                        cast(EXTRACT(EPOCH FROM lesson_progress.created_at) as int),
                        lesson_progress.locale_pack_id,
                        lesson_progress.frame_bookmark_id,
                        lesson_progress.frame_history,
                        lesson_progress.completed_frames,
                        lesson_progress.challenge_scores,
                        lesson_progress.frame_durations,
                        cast(EXTRACT(EPOCH FROM lesson_progress.started_at) as int),
                        cast(EXTRACT(EPOCH FROM lesson_progress.completed_at) as int),
                        lesson_progress.completed_at IS NOT NULL,
                        cast(EXTRACT(EPOCH FROM lesson_progress.updated_at) as int),
                        lesson_progress.id,
                        lesson_progress.best_score,
                        lessons_versions.assessment,
                        lessons_versions.test
                    ) as lesson_progress_json_v8)
                )
            ELSE
                NULL
            end
        "
    end

    def join_authors
        @query_builder.joins << "LEFT OUTER JOIN users authors ON lessons_versions.author_id = authors.id"
    end

    def join_last_editors
        # seems like we should be able to do a JOIN here,
        # but for some reason there are a few versions where this is nil
        # in the real db, and some in tests
        @query_builder.joins << "LEFT OUTER JOIN users last_editors ON lessons_versions.last_editor_id = last_editors.id"
    end

    def join_lesson_progress
        @query_builder.joins << "LEFT JOIN lesson_progress ON lesson_progress.locale_pack_id = lessons.locale_pack_id AND lesson_progress.user_id = '#{@user_id}'"
    end

    def join_practice_frames
        # NOTES:
        #
        # 1. we only ever load published practice_frames, regardless
        #    of the published filter
        # 2. we never call decorate_frame_json on the practice frames,
        #    because it is only used for polling and we do not use polling
        #    in practice, and not calling decorate_frame_json saves us
        #    a bit of time in the request.  We can change this in the future
        #    if need be.
        @query_builder.selects['practice_frames'] = "COALESCE(
            (SELECT (content_json->>'frames')::json FROM lessons_versions practice_lessons_versions
                JOIN content_publishers
                    ON content_publishers.lesson_version_id = practice_lessons_versions.version_id
                WHERE
                    practice_lessons_versions.locale_pack_id = locale_packs.practice_locale_pack_id
                    AND practice_lessons_versions.locale = lessons_versions.locale)
        ,   '[]'::json )"
    end

    def handle_is_practice_filter
        if filters[:is_practice] == false
            @query_builder.joins << "
                LEFT JOIN lesson_locale_packs is_practice_for_locale_packs
                    ON is_practice_for_locale_packs.practice_locale_pack_id = lessons.locale_pack_id
            "
            @query_builder.wheres << "is_practice_for_locale_packs.id is null"
        elsif filters[:is_practice] == true
            raise "only currently accept false value for is_practice"
        end
    end

    def execute
        #puts @query_builder.to_json_sql
        result = ActiveRecord::Base.connection.execute(@query_builder.to_json_sql)
        json = result.to_a[0]["json"] || "[]"

        if self.fields.include?('frames')
            decoded = ActiveSupport::JSON.decode(json)
            decoded.each do |lesson|
                Lesson::Content::FrameList.decorate_frame_json(lesson['id'], lesson['frames'])
            end

            json = decoded.to_json
        end

        json
    end

end
