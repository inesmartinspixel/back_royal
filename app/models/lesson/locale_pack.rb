# == Schema Information
#
# Table name: lesson_locale_packs
#
#  id                      :uuid             not null, primary key
#  created_at              :datetime
#  updated_at              :datetime
#  practice_locale_pack_id :uuid
#

class Lesson::LocalePack < ApplicationRecord
    include LocalePackMixin

    set_content_item_klass(Lesson)

    # a locale pack for a regular lesson that HAS a practice lesson will
    # have a value for practice_locale_pack
    belongs_to :practice_locale_pack, :class_name => 'Lesson::LocalePack', optional: true

    # a locale pack for a practice lesson will have a value for
    # is_practice_for_locale_pack
    has_one :is_practice_for_locale_pack, :class_name => 'Lesson::LocalePack', :primary_key => :id, :foreign_key => :practice_locale_pack_id



    def is_practice_for_locale_pack_id
        Lesson::LocalePack.where(practice_locale_pack_id: self.id).ids.first
    end


    def as_json(options = {})
        super(options).merge({
            'is_practice_for_locale_pack_id' => is_practice_for_locale_pack_id,
            'practice_locale_pack_id' => practice_locale_pack_id,
            'practice_content_items' => practice_locale_pack.nil? ? nil : practice_locale_pack.content_items.map { |content_item|
                {
                    'id' => content_item.id,
                    'title' => content_item.title,
                    'locale' => content_item.locale
                }
            }
        })
    end

end
