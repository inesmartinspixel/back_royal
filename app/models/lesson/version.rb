# == Schema Information
#
# Table name: lessons_versions
#
#  version_id          :uuid             not null, primary key
#  operation           :string(1)        not null
#  version_created_at  :datetime         not null
#  id                  :uuid
#  created_at          :datetime
#  updated_at          :datetime
#  content_json        :json
#  author_id           :uuid
#  title               :string(255)
#  description         :text             is an Array
#  modified_at         :datetime
#  archived            :boolean
#  pinned              :boolean
#  pinned_title        :string(255)
#  pinned_description  :text
#  was_published       :boolean
#  last_editor_id      :uuid
#  seo_title           :string
#  seo_description     :string
#  seo_canonical_url   :text
#  seo_image_id        :uuid
#  lesson_type         :string
#  frame_count         :integer
#  key_terms           :string           is an Array
#  entity_metadata_id  :uuid
#  assessment          :boolean
#  unrestricted        :boolean          default(FALSE)
#  tag                 :text
#  locale_pack_id      :uuid
#  locale              :text
#  duplicated_from_id  :uuid
#  duplicated_to_id    :uuid
#  test                :boolean
#  version_editor_id   :uuid
#  version_editor_name :text
#

class Lesson::Version < Lesson

    include(ContentItemVersionMixin)

    self.primary_key = 'version_id'
    self.table_name = 'lessons_versions'

    belongs_to :content_publisher, :class_name => 'ContentPublisher', :primary_key => :lesson_version_id, :foreign_key => :version_id
    belongs_to :lesson, :foreign_key => 'id', :primary_key => 'id'

    delegate :published_at, :published_version_id, :to => :lesson

    def old_version
        @old_version || false
    end

    def old_version=(val)
        @old_version = !!val
    end

end
