# == Schema Information
#
# Table name: cohort_user_progress_records
#
#  cohort_name                       :text
#  cohort_id                         :uuid
#  program_type                      :text
#  start_date                        :datetime
#  user_id                           :uuid
#  status                            :text
#  required_streams_complete         :bigint
#  foundations_streams_complete      :bigint
#  exam_streams_complete             :bigint
#  elective_streams_complete         :bigint
#  required_playlists_complete       :bigint
#  specialization_playlists_complete :bigint
#  required_lessons_complete         :bigint
#  foundations_lessons_complete      :bigint
#  test_lessons_complete             :bigint
#  elective_lessons_complete         :bigint
#  average_assessment_score_first    :float
#  average_assessment_score_best     :float
#  min_assessment_score_best         :float
#  avg_test_score                    :float
#  participation_score               :float
#  project_score                     :float
#  final_score                       :float
#  meets_graduation_requirements     :boolean
#  required_streams_perc_complete    :float
#  required_lessons_perc_complete    :float
#  elective_lessons_perc_complete    :float
#  elective_streams_perc_complete    :float
#

class CohortUserProgressRecord < ApplicationRecord

end
