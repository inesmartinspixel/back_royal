# == Schema Information
#
# Table name: user_chance_of_graduating_records
#
#  id                   :uuid             not null, primary key
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  user_id              :uuid             not null
#  program_type         :text             not null
#  chance_of_graduating :float            not null
#

class UserChanceOfGraduatingRecord < ApplicationRecord

end
