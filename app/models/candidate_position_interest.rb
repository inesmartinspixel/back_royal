# == Schema Information
#
# Table name: candidate_position_interests
#
#  id                           :uuid             not null, primary key
#  candidate_id                 :uuid             not null
#  open_position_id             :uuid             not null
#  candidate_status             :text             not null
#  hiring_manager_status        :text             default("hidden"), not null
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#  hiring_manager_priority      :integer
#  admin_status                 :text             default("unreviewed"), not null
#  hiring_manager_reviewer_id   :uuid
#  last_updated_at_by_non_admin :datetime
#  cover_letter                 :json             not null
#

class CandidatePositionInterest < ApplicationRecord

    class OldVersion < RuntimeError; end

    def self.candidate_statuses
        return %w(accepted rejected)
    end

    # This is also a dependency graph (was simple enough to be flat) that is used in conflict
    # resolution below in update_from_hash. Thus, order here matters. As a side note, the
    # accepted status is only here for backwards compatability; a hiring manager can only invite now.
    def self.hiring_manager_statuses
        return %w(hidden unseen pending rejected saved_for_later accepted invited)
    end

    def self.admin_statuses
        return %w(unreviewed outstanding reviewed not_applicable)
    end

    def self.reviewed_hiring_manager_statuses
        return %w(saved_for_later invited accepted rejected)
    end

    def self.hidden_or_reviewed_hiring_manager_statuses
        return ['hidden'] + self.reviewed_hiring_manager_statuses
    end

    def self.hidden_or_rejected_hiring_manager_statuses
        return ['hidden', 'rejected']
    end

    belongs_to :candidate, :class_name => 'User'
    belongs_to :hiring_manager_reviewer, :class_name => 'User', optional: true # FIXME: Maybe this should have a foreign key constraint? If so, think about what happens if a user gets deleted.
    belongs_to :open_position, :class_name => 'OpenPosition'
    delegate :hiring_manager, :hiring_manager_id, :hiring_team, :to => :open_position, :allow_nil => true # in real life these are never nil, but nil seems a better response than NoMethodError

    before_save :before_save

    validates :candidate_status, inclusion: { in: self.candidate_statuses, message: "%{value} is not a valid candidate status" }
    validates :hiring_manager_status, inclusion: { in: self.hiring_manager_statuses, message: "%{value} is not a valid hiring manager status" }
    validates :admin_status, inclusion: { in: self.admin_statuses , message: "%{value} is not a valid hiring manager status" }

    after_save :log_curated_event

     # not doing this on destroy, because it is unnecessary and can intermittently blow up
     # when running UsersControllerSpec#ensure_less_than_100_users
    after_commit :hide_if_hiring_relationship_conflict, :on => [:create, :update]

    has_one :career_profile, :through => :candidate

    def self.create_or_update_from_hash!(hash, meta = {}, candidate = nil)
        hash = hash.unsafe_with_indifferent_access
        instance = hash[:id] ? find(hash[:id]) : where("candidate_id = ? AND open_position_id = ?", hash[:candidate_id], hash[:open_position_id]).first

        if instance
            update_from_hash!(instance.attributes, meta, candidate)
        else
            create_from_hash!(hash, meta, candidate)
        end
    end

    def self.create_from_hash!(hash, meta = {}, candidate = nil)
        RetriableTransaction.transaction do
            hash = hash.unsafe_with_indifferent_access
            instance = new(id: hash[:id])
            instance.merge_hash(hash, meta)
            instance.candidate = candidate if candidate
            instance.save!
            instance
        end
    end

    def self.update_from_hash!(hash, meta = {}, candidate = nil, current_user = nil)
        RetriableTransaction.transaction do
            hash = hash.unsafe_with_indifferent_access

            instance = find_by_id(hash[:id])

            if instance.nil?
                raise ActiveRecord::RecordNotFound.new("Couldn't find interest with id=#{hash[:id]}")
            end

            is_hiring_manager_or_teammate = instance.open_position.hiring_manager_id == current_user&.id ||
                instance.open_position.hiring_manager.hiring_team_id == current_user&.hiring_team_id
            is_admin = meta.present? && meta[:is_admin]

            hiring_manager_status_changing_to_reviewed = instance.hiring_manager_status != hash['hiring_manager_status'] &&
                CandidatePositionInterest.reviewed_hiring_manager_statuses.include?(hash['hiring_manager_status'])

            # Set the hiring_manager_reviewer_id in the hash if a hiring_manager or teammate is setting the
            # hiring_manager_status to a reviewed status.
            if is_hiring_manager_or_teammate && hiring_manager_status_changing_to_reviewed
                hash['hiring_manager_reviewer_id'] = current_user.id
            end

            # If a non-admin is trying to change the hiring_manager_status then we need to check if the interest has
            # been saved more recently (e.g. by a teammate). If so, we do a further check below to determine if we
            # need to raise a conflict.
            if !is_admin && hash['hiring_manager_status'].present? && hash['updated_at'].present? && instance.updated_at &&
                instance.updated_at.to_timestamp > hash['updated_at'].to_timestamp
                before_index = CandidatePositionInterest.hiring_manager_statuses.find_index(instance.hiring_manager_status)
                after_index = CandidatePositionInterest.hiring_manager_statuses.find_index(hash['hiring_manager_status'])

                # Check to see if the operation is supported and throw an error if not. An operation
                # should be supported if it is being set to a hiring_manager_status that is the same or
                # anything after it in the hiring_manager_statuses array.
                # The only exception is when an admin first curates a position, which will set the
                # hiring_manager_status from hidden to unseen.
                if after_index < before_index
                    raise CandidatePositionInterest::OldVersion.new
                end
            elsif is_admin && (hash['hiring_manager_status'].present? || hash['candidate_status'].present?) &&
                instance.last_updated_at_by_non_admin && hash['last_updated_at_by_non_admin'].present? &&
                instance.last_updated_at_by_non_admin.to_timestamp > hash['last_updated_at_by_non_admin'].to_timestamp

                # NOTE: we have not currently built out a way for an admin to send down candidate_status when batch updating
                # interests. We also don't stop that from happening, which is why we check for both here.
                raise CandidatePositionInterest::OldVersion.new
            end

            # We update last_updated_at_by_non_admin if a non-admin is updating
            # this interest. This lets us detect specifically if an admin is conflicting
            # with a hiring_manager, without having to worry about updated_at side effects
            # in batch_update causing a false positive in the curation admin.
            if !is_admin
                hash["last_updated_at_by_non_admin"] = Time.now.to_timestamp
            end

            instance.merge_hash(hash, meta)
            instance.candidate = candidate if candidate
            instance.save!

            if meta && meta["last_curated_at"]
                instance.open_position.last_curated_at = Time.at(meta["last_curated_at"])
                instance.open_position.save!
            end

            instance
        end

    end

    def self.includes_needed_for_json_eager_loading(options = {})
        includes = [{:career_profile => CareerProfile.includes_needed_for_json_eager_loading}]

        if options[:fields]&.include?('HIRING_MANAGER_REVIEWER_FIELDS')
            includes << :hiring_manager_reviewer
        end

        includes
    end

    def merge_hash(hash, meta = {})
        meta ||= {}
        hash = hash.unsafe_with_indifferent_access

        %w(
            candidate_id open_position_id candidate_status hiring_manager_status admin_status hiring_manager_priority
            hiring_manager_reviewer_id cover_letter
        ).each do |key|
            if hash.key?(key)
                val = hash[key]
                val = nil if val.is_a?(String) && val.blank?
                self[key] = val
            end
        end

        %w(last_updated_at_by_non_admin).each do |key|
            if hash[key]&.present?
                val = Time.at(hash[key])
                self.send(:"#{key}=", val)
            end
        end

        self
    end

    def reviewed_by_hiring_manager?
        CandidatePositionInterest.reviewed_hiring_manager_statuses.include?(self.hiring_manager_status)
    end

    def hidden_or_reviewed_by_hiring_manager?
        CandidatePositionInterest.hidden_or_reviewed_hiring_manager_statuses.include?(self.hiring_manager_status)
    end

    def hide_from_hiring_manager!
        self.update!({
            admin_status: 'not_applicable',
            hiring_manager_status: 'hidden'
        })
    end

    def as_json(options = {})
        hash = super.merge(
            "updated_at" => updated_at.to_timestamp,
            "created_at" => created_at.to_timestamp,
            "last_updated_at_by_non_admin" => last_updated_at_by_non_admin.to_timestamp
        )

        if options[:fields]&.include?("HIRING_MANAGER_REVIEWER_FIELDS")
            hash["hiring_manager_reviewer_name"] = self.hiring_manager_reviewer&.name
            hash["hiring_manager_reviewer_avatar_url"] = self.hiring_manager_reviewer&.avatar_url
        end

        fields = hash.keys + ["career_profile", "hiring_manager_rejected"]

        # if options[:fields] are passed in, then that means we include those fields in addition to the default.
        if options[:fields].present?
            fields += options[:fields].map(&:to_s)
        end

        if options[:except]
            fields = fields - options[:except].map(&:to_s)
        end

        if fields.include?("career_profile")
            # we assume that if you're viewing a candidate position interest, you should
            # be able to view the full, non-anonymized profile, without checking any
            # permissions on the user that is viewing
            career_profile_options = options[:career_profile_options]
            raise ArgumentError.new("No career_profile_options passed in") unless career_profile_options
            hash["career_profile"] = career_profile.as_json(career_profile_options.merge(view: 'career_profiles'))
        end

        # We don't send hiring_manager_status back because we don't want to expose what the
        # hiring manager is doing before action is taken (e.g. unseen, saved_for_later). But
        # we do need to know if the interest is rejected or not.
        # Note: We treat reviewed, hidden interests as rejected to the user as well. This can happen in a trigger
        # on the backend, or through the position curation admin.
        if fields.include?("hiring_manager_rejected")
            hash["hiring_manager_rejected"] = self.hiring_manager_status === 'rejected' ||
                (self.admin_status === 'reviewed' && self.hiring_manager_status === 'hidden')
        end

        hash.slice!(*fields)

        return hash

    end

    def log_curated_event
        return unless self.saved_change_to_admin_status? &&
            ['outstanding', 'reviewed'].include?(self.admin_status) &&
            self.hiring_manager_status == 'unseen'

        Event.create_server_event!(
            SecureRandom.uuid,
            self.hiring_manager_id,
            'open_position:new_applicant',
            {
                candidate_avatar_url: self.candidate.avatar_url,
                candidate_name: self.candidate.name,
                candidate_nickname: self.candidate.nickname,
                cover_letter: self.cover_letter,
                open_position_title: self.open_position.title,
                open_position_review_path: self.open_position.review_path
            }
        ).log_to_external_systems
    end

    private
        # If a candidate_position_interest is not reviewed by the hiring manager and there exists a hiring_relationship
        # for the candidate from either the hiring_manager or one of their teammates then we want to hide the interest
        def hide_if_hiring_relationship_conflict
            if !self.reviewed_by_hiring_manager? && self.hiring_manager_status != 'hidden' && HiringRelationship.where(
                candidate_id: self.candidate_id,
                hiring_manager_id: self.hiring_manager.hiring_team_members.pluck(:id) + [self.hiring_manager.id],
                hiring_manager_status: ['accepted', 'rejected']).count > 0

                self.hide_from_hiring_manager!
            end
        end

        def before_save
            # Make sure that the user has a career profile
            self.candidate.ensure_career_profile if self.candidate
            self.candidate.save! if self.candidate
        end
end
