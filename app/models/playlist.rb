# == Schema Information
#
# Table name: playlists
#
#  id                 :uuid             not null, primary key
#  title              :text             not null
#  description        :text
#  stream_entries     :json             is an Array
#  image_id           :uuid
#  entity_metadata_id :uuid
#  author_id          :uuid
#  last_editor_id     :uuid
#  modified_at        :datetime
#  was_published      :boolean          default(FALSE), not null
#  created_at         :datetime
#  updated_at         :datetime
#  locale_pack_id     :uuid
#  locale             :text             default("en"), not null
#  duplicated_from_id :uuid
#  duplicated_to_id   :uuid
#  tag                :text
#

class Playlist < ApplicationRecord

    include Skylight::Helpers
    include IsContentItem

    default_scope -> {order(:created_at)}

    belongs_to :entity_metadata, optional: true
    belongs_to :image, :class_name => 'S3Asset', optional: true
    belongs_to :locale_pack, :class_name => Playlist::LocalePack.name, :foreign_key => :locale_pack_id, optional: true

    validates_presence_of :title, :if => :was_published?
    validates_presence_of :description, :if => :was_published?
    validates_size_of :stream_entries, :minimum => 1, :if => :was_published?, :message => "must have at least one entry"
    validates_truth_of :all_required_streams_are_published, :if => :was_published?
    validates_truth_of :all_stream_entries_include_existing_streams
    validates_truth_of :all_stream_entries_have_locale_packs

    set_callback :publish_change, :before, :raise_error_if_required_by_any_cohorts
    before_destroy :raise_error_if_required_by_any_cohorts

    def self.prepare_params_for_duplication(hash_for_original_item, hash_for_new_item, user, meta)
        hash_for_original_item
    end

    def self.stream_locale_pack_ids_for_playlist(playlist_locale_pack_id, locale)
        # setting the expiry to 1 second so that if you hit this over and over in one call, it doesn't have
        # to keep reloading the content views refresh.  That's super fast, but if you did it 10 times in a second,
        # it can add up.
        content_views_refresh_updated_at = SafeCache.fetch("content_views_refresh_updated_at_for_stream_locale_pack_ids_for_playlist", expires_in: content_views_refresh_updated_at_cache_expiry) do
            ContentViewsRefresh.first ? ContentViewsRefresh.first.updated_at : 0
        end

        cache_key = [
            "stream_locale_pack_ids_for_playlist",
            content_views_refresh_updated_at.to_f.to_s,
            locale
        ].inspect

        playlists_by_locale_pack_id = SafeCache.fetch(cache_key) do
            params = {
                fields: ['stream_entries', 'locale_pack'],
                filters: {
                    published: true,
                    in_locale_or_en: locale
                }
            }

            playlists = Playlist::ToJsonFromApiParams.new(params).to_a

            playlists.index_by do |playlist|
                playlist['locale_pack']['id']
            end
        end

        playlist = playlists_by_locale_pack_id[playlist_locale_pack_id]
        if playlist.nil?
            []
        else
            playlist['stream_entries'].map do |entry|
                entry['locale_pack_id']
            end
        end
    end

    # overridable in specs
    def self.content_views_refresh_updated_at_cache_expiry
        1.second
    end

    instrument_method
    def merge_hash(hash, meta = {})
        hash = hash.clone.with_indifferent_access

        image_hash = hash['image']

        %w(description stream_entries tag).each do |key|
            self[key] = hash[key] if hash.key?(key)
        end

        if hash.key?('image')
            id = hash['image'] && hash['image']['id']
            self.image = id.nil? ? nil : S3Asset.find(id)
        end
    end

    instrument_method
    def get_image_asset(file)
        S3Asset.new({
            file: file,
            directory: "images/",
            :styles => {

                # student dashboard
                "50x50"  => '50x50>',
                "100x100"  => '100x100>',
                "150x150"  => '150x150>',

                # stream dashboard
                "110x110"   => '110x110>',
                "220x220"   => '220x220>',
                "330x330"   => '330x330>'
            }.to_json
        })
    end

    # adds error messages and raises an ActiveRecord::RecordInvalid error when unpublishing
    # if the published version of a cohort requires the playlist or if the working draft version
    # of a cohort requires the playlist
    def raise_error_if_required_by_any_cohorts
        if !self.was_published
            # check the published version of the cohorts
            published_cohorts = ActiveRecord::Base.connection.execute("
                select
                    published_cohorts.name
                from published_cohorts
                where '#{self.locale_pack_id}'::uuid = any(published_cohorts.required_playlist_pack_ids)
            ").to_a

            published_cohorts.each do |published_cohort|
                errors.add(:"playlist_#{self.title.underscore}", "is required by the published version of cohort #{published_cohort['name']}")
            end

            # check the working draft version of the cohorts
            # NOTE: the cohort's required_playlist_pack_ids are derived from playlist_collections
            Cohort.all.each do |cohort|
                if cohort.required_playlist_pack_ids.include?(self.locale_pack_id)
                    errors.add(:"playlist_#{self.title.underscore}", "is required by the working draft version of cohort #{cohort.name}")
                end
            end

            unless errors.empty?
                raise ActiveRecord::RecordInvalid.new(self)
            end
        end
    end

    def all_stream_entries_include_existing_streams
        stream_entries.size == stream_locale_packs.count
    end

    def all_required_streams_are_published
        published_locale_pack_ids = streams_for_all_locales
            .where(id: Lesson::Stream.all_published.ids)
            .where(locale: ['en', self.locale])
            .pluck('locale_pack_id')

        published_locale_pack_ids.uniq.sort == stream_locale_pack_ids.sort
    end

    def all_stream_entries_have_locale_packs
        stream_entries.each do |entry|
            return false unless entry['locale_pack_id']
            return false unless Lesson::Stream::LocalePack.find_by_id(entry['locale_pack_id'])
        end
        return true
    end

    def contains_stream_locale_pack?(locale_pack)
        stream_locale_pack_ids.include?(locale_pack.id)
    end

    def stream_locale_packs
        Lesson::Stream::LocalePack.where(id: stream_locale_pack_ids)
    end

    def stream_ids
        streams.pluck('id')
    end

    def streams_for_all_locales
        Lesson::Stream.where(locale_pack_id: stream_locale_pack_ids)
    end

    def streams
        streams_for_all_locales.where(locale: self.locale)
    end

    def stream_locale_pack_ids
        stream_entries.map { |entry| entry['locale_pack_id'] }
    end

    # FIXME: Remove once this has been addressed: https://github.com/rails/rails/issues/26991
    def changed?
        return false unless super
        if changes.size == 1 &&
            changes.keys[0] == 'stream_entries' &&
            changes['stream_entries'][0] == changes['stream_entries'][1]
            return false
        end
        true
    end

    def as_json(options = {})
        raise "Cannot as_json an unsaved playlist" if changed?
        options = options.with_indifferent_access
        options[:filters] ||= {}
        filters = options[:filters]
        filters[:published] = false unless [true, "true"].include?(filters[:published])
        json = Playlist::ToJsonFromApiParams.new(options.merge(:id => self.attributes['id'])).json
        ActiveSupport::JSON.decode(json).first
    end

end
