# == Schema Information
#
# Table name: cohort_user_periods
#
#  id                                                              :uuid             not null, primary key
#  user_id                                                         :uuid             not null
#  cohort_id                                                       :uuid             not null
#  cohort_name                                                     :text             not null
#  index                                                           :integer          not null
#  required_lessons_completed_in_period                            :integer          not null
#  specialization_lessons_completed_in_period                      :integer          not null
#  period_start                                                    :datetime         not null
#  period_end                                                      :datetime         not null
#  required_lesson_pack_ids_completed_in_period                    :uuid             not null, is an Array
#  specialization_lesson_pack_ids_completed_in_period              :uuid             not null, is an Array
#  status_at_period_end                                            :text             not null
#  cumulative_required_lessons_completed                           :integer          not null
#  cumulative_required_up_to_now_lessons_completed                 :integer          not null
#  cumulative_required_and_specialization_lessons_completed_in_las :integer          not null
#  cumulative_specializations_completed                            :integer          not null
#  expected_specialization_lessons_completed                       :integer          not null
#  cumulative_specialization_lessons_completed                     :integer          not null
#  cumulative_specialization_up_to_now_lessons_completed           :integer          not null
#  total_up_to_now_lessons_completed                               :integer          not null
#  total_lessons_expected                                          :integer          not null
#  updated_at                                                      :datetime         not null
#

class CohortUserPeriod < ApplicationRecord

    attr_accessor :published_cohort_period # cannot be an association because of compound key

    LESSONS_BEHIND_BUCKETS = [0, -25, -50, -75, -100, -150, -200, -Float::INFINITY]

    has_one :program_enrollment_progress_records, :primary_key => :user_id, :foreign_key => :user_id

    def self.lessons_behind_bucket(lessons_behind)
        LESSONS_BEHIND_BUCKETS.detect do |i|
            lessons_behind >= i
        end
    end

    def lessons_behind_bucket
        period = published_cohort_period

        unless defined?(@lessons_behind_bucket)
            # cumulative_required_lesson_pack_ids_count is set to nil on the first period
            # of emba cohorts.  It should really be 0
            lessons_behind = cumulative_required_up_to_now_lessons_completed - (period.cumulative_required_lesson_pack_ids_count || 0)
            @lessons_behind_bucket = self.class.lessons_behind_bucket(lessons_behind)
        end
        @lessons_behind_bucket
    end
    def self.write_new_records

        # UPDATES:
        # We need to update any records that were written before the
        # last time the cohort was published, since the schedule could
        # have changed.  Theoretically, a change to a stream or a playlist
        # could also affect the data here, but only very slightly, and we
        # don't want to re-write records on every publication or write
        # super-complex logic to determine which publications might have
        # changed something.

        # WRITES:
        # We need to write new records for any periods in the past where
        # there is no record yet for a user who was accepted at some point

        # NOTE:
        # add the 2 minute buffer on the off chance a cohort gets published while this is running
        # Only do this on prod, so that we can test it more easily
        buffer = (Rails.env.prod? ? '2 minutes' : '0 minutes')

        # For each cohort/period, get a list of user_ids where rows need
        # to be updated or written
        periods_to_update = connection.execute(%Q~
            SELECT
                published_cohort_periods.cohort_id
                , published_cohort_periods.index
                , string_agg(cohort_applications_plus.user_id::text, ',') as user_ids
            FROM published_cohort_periods
                join published_cohorts on published_cohorts.id = published_cohort_periods.cohort_id
                join content_publishers on content_publishers.cohort_version_id = published_cohorts.version_id
                join cohort_applications_plus
                    on published_cohorts.id = cohort_applications_plus.cohort_id
                    and cohort_applications_plus.was_accepted = true
                left join cohort_user_periods
                    on cohort_user_periods.cohort_id = published_cohort_periods.cohort_id
                    and cohort_user_periods.index = published_cohort_periods.index
                    and cohort_user_periods.user_id = cohort_applications_plus.user_id
            WHERE
                published_cohort_periods.period_end < now()
                and (
                    cohort_user_periods.user_id is null

                    --
                    or cohort_user_periods.updated_at < (content_publishers.published_at + interval '#{buffer}')
                )
            group by
                published_cohort_periods.cohort_id
                , published_cohort_periods.index
            order by
                published_cohort_periods.cohort_id
                , published_cohort_periods.index

        ~).to_a

        start = Time.now

        if periods_to_update.any?
            # Since PublishedPlaylistLessons is only used here, we refresh
            # it here.  See comment in app/models/view_helpers/published_playlist_lessons.rb
            ViewHelpers::PublishedPlaylistLessons.refresh_concurrently
        end

        periods_to_update.each_with_index do |row, i|
            puts "Updating period #{i+1} of #{periods_to_update.size} (#{(Time.now - start).round} seconds elapsed)" if Rails.env.development?
            self.write_new_records_for_period(row['cohort_id'], row['index'], row['user_ids'].split(','))
        end
    end

    private
    def self.write_new_records_for_period(cohort_id, period_index, user_ids)

        columns = %w(
            user_id
            cohort_id
            cohort_name
            index
            required_lessons_completed_in_period
            specialization_lessons_completed_in_period
            period_start
            period_end
            required_lesson_pack_ids_completed_in_period
            specialization_lesson_pack_ids_completed_in_period
            status_at_period_end
            cumulative_required_lessons_completed
            cumulative_required_up_to_now_lessons_completed
            cumulative_required_and_specialization_lessons_completed_in_last_4_periods
            cumulative_specializations_completed
            expected_specialization_lessons_completed
            cumulative_specialization_lessons_completed
            cumulative_specialization_up_to_now_lessons_completed
            total_up_to_now_lessons_completed
            total_lessons_expected
            updated_at
        )

        set_clause = (columns - ['user_id', 'cohort_id', 'index']).map { |col|
            "#{col} = EXCLUDED.#{col}"
        }.join(',')

        ActiveRecord::Base.connection.execute %Q~

            insert into cohort_user_periods (
                #{columns.join(',')}
            )
                (#{select_new_records_sql(cohort_id, period_index, user_ids)})
            on conflict (user_id, cohort_id, index) do update
                set #{set_clause}
        ~
    end

    private
    def self.select_new_records_sql(cohort_id, period_index, user_ids)
        id_string = user_ids.map { |id| "'#{id}'" }.join(',')

        # NOTE: in order to write the record for a period, the records for
        # all previous periods in the cohort need to be written already.  If you
        # passed a period into the following query where the previous periods
        # were not written, it would end up writing incorrect records.
        %Q~
           WITH target_periods AS MATERIALIZED (
                SELECT
                    *
                from published_cohort_periods
                where
                    cohort_id='#{cohort_id}'
                    and index='#{period_index}'

                    -- we never really expect someone to pass in a period here
                    -- that is not in the past, but just in case
                    AND published_cohort_periods.period_end < NOW()
           )
           , lesson_progress_records AS MATERIALIZED (
                SELECT
                    user_id
                    , cohort_user_lesson_progress_records.cohort_id
                    , cohort_user_lesson_progress_records.cohort_name
                    , completed_at
                    , completed_in_periods.index AS completed_in_period_index
                    , cohort_user_lesson_progress_records.lesson_locale_pack_id
                    , published_cohort_lesson_locale_packs.required
                    , not published_cohort_lesson_locale_packs.required and published_cohort_lesson_locale_packs.specialization as specialization
                FROM
                    cohort_user_lesson_progress_records
                    JOIN published_cohort_periods completed_in_periods
                        ON cohort_user_lesson_progress_records.cohort_id = completed_in_periods.cohort_id
                           AND completed_at BETWEEN completed_in_periods.period_start AND completed_in_periods.period_end
                    JOIN published_cohort_lesson_locale_packs
                        on published_cohort_lesson_locale_packs.cohort_id = cohort_user_lesson_progress_records.cohort_id
                        and published_cohort_lesson_locale_packs.lesson_locale_pack_id = cohort_user_lesson_progress_records.lesson_locale_pack_id
                    JOIN target_periods
                        on target_periods.cohort_id = cohort_user_lesson_progress_records.cohort_id
                WHERE
                    completed_at between target_periods.period_start and target_periods.period_end
                    and user_id in (#{id_string})
            )
            , with_lesson_info AS MATERIALIZED (
                SELECT
                    cohort_applications_plus.user_id
                    , cohort_applications_plus.cohort_id
                    , cohort_applications_plus.cohort_name
                    , target_periods.index
                    , count(DISTINCT case when required then lesson_progress_records.lesson_locale_pack_id else null end) AS required_lessons_completed_in_period
                    , count(DISTINCT case when specialization then lesson_progress_records.lesson_locale_pack_id else null end) AS specialization_lessons_completed_in_period
                    , target_periods.period_start
                    , target_periods.period_end

                    , array_agg(distinct case when required then lesson_progress_records.lesson_locale_pack_id else null end) as required_lesson_pack_ids_completed_in_period
                    , array_agg(distinct case when specialization then lesson_progress_records.lesson_locale_pack_id else null end) as specialization_lesson_pack_ids_completed_in_period
                FROM
                    cohort_applications_plus
                    JOIN target_periods
                        ON cohort_applications_plus.cohort_id = target_periods.cohort_id
                    LEFT JOIN lesson_progress_records
                        ON target_periods.cohort_id = lesson_progress_records.cohort_id
                           AND cohort_applications_plus.user_id = lesson_progress_records.user_id
                           AND target_periods.index = lesson_progress_records.completed_in_period_index
                WHERE
                    was_accepted = TRUE
                    and cohort_applications_plus.user_id in (#{id_string})

                GROUP BY
                    cohort_applications_plus.user_id
                    , cohort_applications_plus.cohort_id
                    , cohort_applications_plus.cohort_name
                    , target_periods.index
                    , target_periods.period_start
                    , target_periods.period_end
            )
            , with_status_info AS MATERIALIZED (
                SELECT
                    with_lesson_info.*
                    , coalesce(cohort_status_changes.status, 'not_yet_applied') AS status_at_period_end

                FROM with_lesson_info
                    LEFT JOIN cohort_status_changes
                        ON cohort_status_changes.cohort_id = with_lesson_info.cohort_id
                           AND cohort_status_changes.user_id = with_lesson_info.user_id
                           AND cohort_status_changes.from_time <= with_lesson_info.period_end
                           AND cohort_status_changes.until_time > with_lesson_info.period_end
            )
            -- we want to know how many lessons the user completed in the last
            -- 4 standard or specialization periods.  This block figures out the index of the period
            -- that was 4 standard periods ago
            , last_4_standard_like_periods AS MATERIALIZED (
                SELECT
                    cohort_id
                    , index
                    , style
                    , (select min(index) from (
                        SELECT prev_standard_like_periods.index
                            FROM published_cohort_periods prev_standard_like_periods
                            WHERE prev_standard_like_periods.cohort_id = this_period.cohort_id
                                  AND prev_standard_like_periods.index <= this_period.index
                                  AND prev_standard_like_periods.style in ('standard', 'specialization')
                            ORDER BY index DESC
                            LIMIT 4
                    ) a) as four_standard_like_periods_ago_index
                FROM published_cohort_periods this_period
            )

            -- For the relevant cohort, we need to grab the list of lesson_locale_pack_ids
            -- for each specialization.  We convert this to json so we can pass it into
            -- the specialization_progress function below
            , specializations AS MATERIALIZED (
                SELECT
                    cohort_id
                    , array_to_json(array_agg(row_to_json(cohort_playlists))) as specializations FROM
                    (
                        SELECT
                            cohort_playlists.cohort_id
                            , cohort_playlists.playlist_locale_pack_id
                            , array_agg(published_playlist_lessons.lesson_locale_pack_id) as lesson_locale_pack_ids
                            from published_cohort_playlist_locale_packs cohort_playlists
                            join published_playlist_lessons
                                on published_playlist_lessons.playlist_locale_pack_id = cohort_playlists.playlist_locale_pack_id
                            join published_cohort_lesson_locale_packs
                                on published_cohort_lesson_locale_packs.cohort_id = cohort_playlists.cohort_id
                                and published_cohort_lesson_locale_packs.lesson_locale_pack_id = published_playlist_lessons.lesson_locale_pack_id

                                -- We only want to count specialization lessons that are not
                                -- also required, so we do not double count
                                and not published_cohort_lesson_locale_packs.required
                        where cohort_playlists.specialization
                            and cohort_playlists.cohort_id in (select distinct cohort_id from target_periods)
                        group by
                            cohort_playlists.cohort_id
                            , cohort_playlists.playlist_locale_pack_id
                    ) cohort_playlists
                group by cohort_id

            )

            -- In order to calculate the cumulative progress, we need to have all of the previous
            -- cohort_user_periods for this user/cohort (see note above about making sure that
            -- all previous periods have records written before we write this one).  We also need
            -- to include THIS period as well, so we can sum up data from all previous periods along
            -- with this new one that we are adding.
            , prev_periods_inclusive AS MATERIALIZED (
                select
                    cohort_user_periods.cohort_id
                    , cohort_user_periods.user_id
                    , cohort_user_periods.index
                    , cohort_user_periods.required_lesson_pack_ids_completed_in_period
                    , cohort_user_periods.specialization_lesson_pack_ids_completed_in_period
                    , cohort_user_periods.required_lessons_completed_in_period
                    , cohort_user_periods.specialization_lessons_completed_in_period
                from cohort_user_periods
                    join target_periods
                        on target_periods.cohort_id = cohort_user_periods.cohort_id
                        and target_periods.index > cohort_user_periods.index
                where
                    user_id in (select distinct user_id from with_status_info)
                    and cohort_user_periods.index < target_periods.index

                union

                select
                    cohort_id
                    , user_id
                    , index
                    , required_lesson_pack_ids_completed_in_period
                    , specialization_lesson_pack_ids_completed_in_period
                    , required_lessons_completed_in_period
                    , specialization_lessons_completed_in_period

                from with_status_info
            )
            , cumulative_data AS MATERIALIZED (
                SELECT
                    user_periods.user_id
                    , user_periods.cohort_id
                    , user_periods.index
                    , array_remove(array_cat(prev_periods.specialization_lesson_pack_ids_completed_in_period), null) specialization_lesson_locale_pack_ids_completed_up_to_now
                    , coalesce(sum(prev_periods.required_lessons_completed_in_period), 0) AS cumulative_required_lessons_completed
                    , sum(
                          case when prev_periods.index >= last_4_standard_like_periods.four_standard_like_periods_ago_index
                                then prev_periods.required_lessons_completed_in_period + prev_periods.specialization_lessons_completed_in_period
                                else 0 end
                      ) as cumulative_required_and_specialization_lessons_completed_in_last_4_periods

                    , coalesce(
                        array_length(
                            -- array_intersect is a custom function we've added in a migration
                            array_intersect(
                                published_cohort_periods.cumulative_required_lesson_pack_ids,
                                -- array_cat is a custom aggregate we've added in a migration
                                array_cat(DISTINCT prev_periods.required_lesson_pack_ids_completed_in_period)
                            ),
                            1
                        ),
                        0
                      ) as cumulative_required_up_to_now_lessons_completed

                FROM with_status_info user_periods
                    join published_cohort_periods
                        on published_cohort_periods.cohort_id = user_periods.cohort_id
                        and published_cohort_periods.index = user_periods.index
                    join last_4_standard_like_periods
                        on last_4_standard_like_periods.cohort_id = user_periods.cohort_id
                        and last_4_standard_like_periods.index = user_periods.index
                    LEFT JOIN prev_periods_inclusive prev_periods
                        ON user_periods.user_id = prev_periods.user_id
                           AND user_periods.cohort_id = prev_periods.cohort_id
                           AND user_periods.index >= prev_periods.index
                GROUP BY
                    user_periods.user_id
                    , user_periods.cohort_id
                    , user_periods.index
                    , published_cohort_periods.cumulative_required_lesson_pack_ids
                    , published_cohort_periods.expected_specializations_complete
            )
            , specialization_progress AS MATERIALIZED (
                SELECT
                    cumulative_data.user_id
                    , cumulative_data.cohort_id
                    , cumulative_data.index
                    , specialization_progress(
                            specializations.specializations,
                            cohorts_versions.num_required_specializations,
                            published_cohort_periods.expected_specializations_complete,
                            cumulative_data.specialization_lesson_locale_pack_ids_completed_up_to_now
                    ) as specialization_progress
                from
                    cumulative_data
                    join published_cohort_periods
                        on published_cohort_periods.cohort_id = cumulative_data.cohort_id
                        and published_cohort_periods.index = cumulative_data.index
                    join specializations
                        on cumulative_data.cohort_id = specializations.cohort_id
                    join published_cohorts
                        on published_cohorts.id = cumulative_data.cohort_id
                    join cohorts_versions
                        on cohorts_versions.version_id = published_cohorts.version_id
            )
            , with_cumulative_data AS MATERIALIZED (
                SELECT
                    with_status_info.*
                    , cumulative_data.cumulative_required_lessons_completed
                    , cumulative_data.cumulative_required_up_to_now_lessons_completed
                    , cumulative_data.cumulative_required_and_specialization_lessons_completed_in_last_4_periods
                    , coalesce((specialization_progress.specialization_progress->>'completedSpecializations')::float, 0::float) as cumulative_specializations_completed
                    , coalesce((specialization_progress.specialization_progress->>'expectedSpecializationLessonsComplete')::int, 0::int) as expected_specialization_lessons_completed
                    , coalesce((specialization_progress.specialization_progress->>'completedSpecializationLessons')::int, 0::int) as cumulative_specialization_lessons_completed
                    , coalesce(LEAST(
                        (specialization_progress.specialization_progress->>'expectedSpecializationLessonsComplete')::int
                        , (specialization_progress.specialization_progress->>'completedSpecializationLessons')::int
                    ), 0) cumulative_specialization_up_to_now_lessons_completed

                FROM with_status_info
                    JOIN cumulative_data
                        ON with_status_info.user_id = cumulative_data.user_id
                           AND with_status_info.cohort_id = cumulative_data.cohort_id
                           AND with_status_info.index = cumulative_data.index
                    left JOIN specialization_progress
                        ON with_status_info.user_id = specialization_progress.user_id
                           AND with_status_info.cohort_id = specialization_progress.cohort_id
                           AND with_status_info.index = specialization_progress.index
            )
            , with_combined_counts AS MATERIALIZED (
                SELECT
                    with_cumulative_data.*
                    , with_cumulative_data.cumulative_required_up_to_now_lessons_completed + cumulative_specialization_up_to_now_lessons_completed as total_up_to_now_lessons_completed
                    , coalesce(published_cohort_periods.cumulative_required_lesson_pack_ids_count, 0) + expected_specialization_lessons_completed as total_lessons_expected
                    , now() as updated_at
                FROM with_cumulative_data
                    join published_cohort_periods
                        on published_cohort_periods.cohort_id = with_cumulative_data.cohort_id
                        and published_cohort_periods.index = with_cumulative_data.index
            )
            SELECT *
            FROM with_combined_counts
            ORDER BY cohort_id, user_id, index
        ~
    end

end
