# == Schema Information
#
# Table name: notified_recommended_open_positions
#
#  id               :uuid             not null, primary key
#  created_at       :datetime
#  user_id          :uuid             not null
#  open_position_id :uuid             not null
#

class NotifiedRecommendedOpenPosition < ApplicationRecord
    has_one :user
    has_one :open_position
end
