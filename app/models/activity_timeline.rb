# This is an abstract class that can be used as a base for timelines related to
# different things (users, cohorts, etc.)

class ActivityTimeline
    attr_accessor :preloaded

    def self.after_preloaded(instances)
        instances.each { |i| i.preloaded = true }
    end

    # Even though right now we only really load up a single timeline at any
    # particular time, it seemed easy enough to set up the structure for preloading
    # multiple timelines at once, and it might be tricky to go back and add this later.
    def self.preload(instances)
        raise NotImplementedError.new("Subclasses of ActivityTimeline should implement a preload method")
    end

    # An instance can be preloaded on its own by passing itself into the preload class method
    def preload
        self.class.preload([self])
        self
    end

    def preloaded?
        !!preloaded
    end

    def events
        unless defined? @events
            raise "Not yet preloaded" unless self.preloaded?
            @events = get_events.reject do |event|
                if !event.valid?
                    Raven.capture_exception("Timeline event is invalid", {
                        extra: {
                            errors: event.errors.full_messages,
                            event: event.as_json
                        }
                    })
                    :reject
                end
            end.sort_by(&:time).reverse
        end
        @events
    end

    def as_json(options = {})
        {
            events: self.events
        }.as_json
    end

    private
    def get_change_events(category:, versions:, attrs:, operations: ['U'], labels: nil)
        events = []
        previous_version = nil
        versions.each do |version|
            if operations.include?(version.operation)
                changes = []
                attrs.map do |entry|
                    entry = {entry => entry} if entry.is_a?(Symbol)
                    attr = entry.keys[0]
                    proc = entry.values[0]

                    previous_val = previous_version&.instance_eval(&proc)
                    new_val = version.instance_eval(&proc)
                    if previous_val != new_val && !version.attr_changed_to_column_default_on_insert?(attr)

                        # we could support multiple attribute changes in a single modification
                        # event, but at least for now we've decided to show them as separate events
                        # in the UI (The UI is not set up to support multiple changes in an event)
                        events << ActivityTimeline::DerivedTimelineEvent.new(
                            category: category,
                            labels: labels ? labels.call(version) : [],
                            event: 'modification',
                            time: version.updated_at,
                            editor_id: version.version_editor_id,
                            editor_name: version.version_editor_name,
                            secondary_sort: version.updated_at.to_f,
                            changes: [{
                                attr: attr,
                                from: previous_val,
                                to: new_val
                            }]
                        )

                    end
                end
            end

            previous_version = version
        end

        events

    end

end