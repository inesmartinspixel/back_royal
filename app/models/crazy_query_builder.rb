class CrazyQueryBuilder

    attr_accessor :limit, :offset, :orders
    attr_reader :selects, :joins, :wheres, :withs, :havings, :group_bys

    def initialize(table_name)
        @selects = ActiveSupport::OrderedHash.new
        @joins = []
        @wheres = ['TRUE']
        @havings = []
        @table_name = table_name
        @withs = []
        @group_bys = []
        @orders = []
    end

    def parenthesize_and_join(arr, delimiter)
        arr.map { |e| "(#{e})"}.join(delimiter)
    end

    def sql

        if @selects.empty?
            raise "No selects provided"
        end

        withs_string = @withs.any? ? "WITH #{@withs.join(",\n")}" : ""

        group_by_string = if group_bys.any?
            "GROUP BY #{parenthesize_and_join(group_bys, ', ')}"
        else
            ""
        end

        having_string = havings.any? ? "HAVING #{parenthesize_and_join(@havings, "\nAND ")}" : ""

        order_string = orders.any? ? "ORDER BY #{@orders.join(", ")}" : ""

        limit_string = limit.present? ? "LIMIT #{limit}" : ""

        offset_string = offset.present? ? "OFFSET #{offset}" : ""

        sql = "
            #{withs_string}
            SELECT
                #{selects_string}
            FROM #{@table_name}
            #{@joins.join("\n")}
            WHERE #{parenthesize_and_join(@wheres, "\nAND ")}
            #{group_by_string}
            #{having_string}
            #{order_string}
            #{limit_string}
            #{offset_string}

        "
    end

    def count_sql
        withs_string = @withs.any? ? "WITH #{@withs.join(",\n")}" : ""

        group_by_string = if group_bys.any?
            "GROUP BY #{parenthesize_and_join(group_bys, ', ')}"
        else
            ""
        end

        having_string = havings.any? ? "HAVING #{parenthesize_and_join(@havings, "\nAND ")}" : ""

        "
            select count(*) from (
                #{withs_string}
                SELECT TRUE
                FROM #{@table_name}
                    #{@joins.join("\n")}
                WHERE #{parenthesize_and_join(@wheres, "\nAND ")}
                #{group_by_string}
                #{having_string}
            ) total_item_count

        "
    end

    def to_json_sql

        sql = "
            WITH prepared_records AS MATERIALIZED (
                #{self.sql}
            )
            SELECT
                array_to_json(array_agg(row_to_json(records))) json
            from (SELECT * from prepared_records) records
        "
    end

    private
    def selects_string
        @selects.map do |_alias, definition|
            "#{definition} as #{_alias}"
        end.join(",\n")
    end

end
