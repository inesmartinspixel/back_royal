module HasFulltextSupport
    extend ActiveSupport::Concern

    included do
        include ActiveSupport::Callbacks

        after_save :update_fulltext, if: :should_update_fulltext?
        before_destroy :clear_fulltext
    end

    module ClassMethods
        def sanitize_fulltext_sql(sql_string)
            ActiveRecord::Base.connection.quote(sql_string)
        end

        # sanitize input for vector search:
        # 1. remove invalid ts_query-invalid characters (see also: http://stackoverflow.com/questions/16020164/psqlexception-error-syntax-error-in-tsquery)
        # 2. convert single quotes in to escaped (double) quotes for postgres
        # 3. sanitize using active record and join with `delim` (defaults to &)
        def get_vector_search_sql(search_text, delim = "&")
            filtered_text = search_text.gsub(/[^\p{Alnum}\p{Space}\-\.']/u, '').gsub(/'/, "''")
            sanitize_fulltext_sql((filtered_text.split() * "#{delim}"))
        end

    end

    def clear_fulltext
        prefix = self.model_name.singular
        ActiveRecord::Base.connection.execute("DELETE FROM #{prefix}_fulltext WHERE #{prefix}_id='#{self.id}'")
    end

    def update_fulltext
        raise "update_fulltext must be implemented in including class"
    end

    def should_update_fulltext?
        true
    end

end