module ToJsonFromApiParamsHelpers::StreamsAccessMixin
    extend ActiveSupport::Concern

    def handle_access_filters
        if @user.nil?
            raise ArgumentError.new("user must be provided in order to filter by 'user_can_see'") if @filters[:user_can_see]
        end

        if @filters[:user_can_see]
            filter_by_user_can_see
        end

        if @filters.key?(:view_as)
            type, value = filters[:view_as].split(':')
            if type == 'group'
                view_as_group(value) # NOTE: we use a string name value instead of an ID for groups
            elsif type == 'cohort'
                view_as_cohort(value)
            elsif type == 'institution'
                view_as_institution(value)
            else
                raise ArgumentError.new('Invalid value for view_as: #{filters[:view_as].inspect}')
            end
        end
    end


    # NOTE: this business logic is duplicated in PublishedStream#for_user
    def filter_by_user_can_see
        return unless @filters[:user_can_see]

        raise "filter_user_can_see requires a user" unless @user

        @query_builder.withs << "streams_accessible_by_user AS MATERIALIZED (

            SELECT DISTINCT
                lesson_streams.id
            FROM lesson_streams
            WHERE locale_pack_id IN (

                -- Courses available via groups
                SELECT locale_pack_id
                FROM access_groups_users
                    JOIN access_groups_lesson_stream_locale_packs
                        ON access_groups_lesson_stream_locale_packs.access_group_id = access_groups_users.access_group_id
                WHERE access_groups_users.user_id = '#{@user.id}'

                UNION

                -- Courses available via cohorts
                SELECT stream_locale_pack_id
                FROM published_cohort_stream_locale_packs
                WHERE cohort_id = '#{@user.relevant_cohort ? @user.relevant_cohort.attributes['id'] : SecureRandom.uuid}'

                UNION

                -- Courses available via institutions
                SELECT stream_locale_pack_id
                FROM institution_stream_locale_packs
                    JOIN institutions_users
                        ON institutions_users.user_id = '#{@user.id}'
                        AND institution_stream_locale_packs.institution_id = institutions_users.institution_id
            )

        )"

        @query_builder.wheres << "lesson_streams_versions.id IN (SELECT id FROM streams_accessible_by_user)"
    end

    def view_as_group(name)
        @query_builder.joins << "
            JOIN lesson_stream_locale_packs
                ON lesson_stream_locale_packs.id = lesson_streams.locale_pack_id
            JOIN access_groups_lesson_stream_locale_packs
                ON access_groups_lesson_stream_locale_packs.locale_pack_id = lesson_stream_locale_packs.id
            JOIN access_groups
                ON access_groups_lesson_stream_locale_packs.access_group_id = access_groups.id
                AND access_groups.name = '#{name}'
            "
    end

    def view_as_cohort(id)


        if id == 'PROMOTED_DEGREE' # shorthand for sitemap and client defaults
            where_clause = "published_cohort_stream_locale_packs.cohort_id IN (
                                SELECT published_cohort_admission_rounds.cohort_id
                                FROM published_cohort_admission_rounds
                                WHERE published_cohort_admission_rounds.program_type IN (
                                    #{Cohort::ProgramTypeConfig.degree_program_types.map { |d| ActiveRecord::Base.connection.quote(d) }.join(',')}
                                )
                                AND published_cohort_admission_rounds.promoted = TRUE
                            )"
        else
            where_clause = "published_cohort_stream_locale_packs.cohort_id = '#{id}'"
        end


        @query_builder.withs << "streams_accessible_by_view_as_cohort AS MATERIALIZED (
            SELECT DISTINCT
                lesson_streams.id
            FROM lesson_streams
                JOIN published_cohort_stream_locale_packs
                    ON published_cohort_stream_locale_packs.stream_locale_pack_id = lesson_streams.locale_pack_id
            WHERE #{where_clause}
        )"

        @query_builder.wheres << "lesson_streams_versions.id IN (SELECT id FROM streams_accessible_by_view_as_cohort)"
    end

    def view_as_institution(id)
        @query_builder.withs << "streams_accessible_by_view_as_institution AS MATERIALIZED (
            SELECT DISTINCT
                lesson_streams.id
            FROM lesson_streams
                JOIN institution_stream_locale_packs
                    ON institution_stream_locale_packs.stream_locale_pack_id = lesson_streams.locale_pack_id
            WHERE institution_stream_locale_packs.institution_id = '#{id}'
        )"

        @query_builder.wheres << "lesson_streams_versions.id IN (SELECT id FROM streams_accessible_by_view_as_institution)"
    end

    def locale_pack_groups_subquery
        "
            -- convert the desired columns for each group into json (row_to_json),
            -- group all the json values
            -- into an array (array_agg), then convert the array to json (array_to_json)
            COALESCE(
                (SELECT array_to_json(array_agg(row_to_json(prepared_groups))) FROM
                    (
                        -- select the desired columns from groups
                        SELECT
                            access_groups.id,
                            access_groups.name
                        FROM access_groups
                            JOIN access_groups_lesson_stream_locale_packs on access_groups.id = access_groups_lesson_stream_locale_packs.access_group_id
                        WHERE access_groups_lesson_stream_locale_packs.locale_pack_id=locale_packs.id
                        ORDER BY access_groups.name asc
                    ) prepared_groups
                ),
                '[]'::json
            )"
    end

end