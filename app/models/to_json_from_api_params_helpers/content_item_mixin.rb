module ToJsonFromApiParamsHelpers::ContentItemMixin
    extend ActiveSupport::Concern

    def handle_content_item_selects
        if (['locale_pack', 'practice_frames'] & fields).any? || filters.key?(:updated_since)
            join_and_select_locale_pack
        end
        include_publishing_info_if_necessary
    end

    def handle_content_item_filters
        # this is needed for locale_packs, for progress, for
        # filter_out_not_working_version, and maybe other stuff
        join_base_table

        handle_published_filter
        handle_id_filter
        handle_locale_pack_id_filter
        handle_version_id_filter
        handle_locale_filter
        handle_locale_or_en_filter
        handle_users_locale_or_en_filter
        handle_updated_since_filter
        handle_updated_before_filter
    end

    def handle_published_filter
        if @filters[:published] && @filters[:version_id]
            raise ArgumentError.new("it is not allowed to set both version_id and published as filters")
        end

        @filters[:published] = true unless @filters[:published] == false || @filters.key?(:version_id)

        if @filters[:published]
            filter_out_unpublished
        elsif !filters[:version_id]
            filter_out_not_working_version
        end
    end

    def select_author
        @query_builder.joins << "LEFT OUTER JOIN users authors ON #{base_table_name}_versions.author_id = authors.id"

        @query_builder.selects['author'] = "
            row_to_json(

                -- We have to cast the row as the custom type user_json
                -- (created in the @types hash) in order to get the correct
                -- property names into the json hash
                #{cast_user("authors")}
            )"
    end

    def locale_pack_table
        "#{base_table_name.singularize}_locale_packs"
    end

    def get_content_items_subquery(id_string)
        subquery = base_klass::ToJsonFromApiParams.new({

            fields: ['id', 'title', 'locale'],
            filters: {
                published: self.filters[:published]
            }

        })
        subquery.wheres << "#{base_table_name}.locale_pack_id = #{id_string}"
        subquery
    end


    def join_and_select_locale_pack
        @query_builder.joins << "
                    LEFT OUTER JOIN #{locale_pack_table} locale_packs
                        ON locale_packs.id = #{base_table_name}.locale_pack_id"

        content_items_subquery = get_content_items_subquery("locale_packs.id")

        groups_subquery = self.respond_to?(:locale_pack_groups_subquery) ? locale_pack_groups_subquery : 'NULL'

        content_topics_subquery = self.respond_to?(:locale_pack_content_topics_subquery) ? locale_pack_content_topics_subquery : 'NULL'

        if base_klass::LocalePack.attribute_names.include?('practice_locale_pack_id')
            practice_locale_pack_id_subquery = 'locale_packs.practice_locale_pack_id'
            is_practice_for_locale_pack_id_subquery = "(
                SELECT id from #{locale_pack_table}
                WHERE practice_locale_pack_id=locale_packs.id
            )"
            practice_content_items_subuery = get_content_items_subquery("locale_packs.practice_locale_pack_id").to_array_subquery
        else
            practice_locale_pack_id_subquery = 'NULL'
            is_practice_for_locale_pack_id_subquery = 'NULL'
            practice_content_items_subuery = 'NULL'
        end

        @query_builder.selects['locale_pack'] = "
            CASE WHEN locale_packs.id IS NOT NULL THEN
                    row_to_json(
                        cast(
                            row(
                                locale_packs.id,
                                #{content_items_subquery.to_array_subquery},
                                #{groups_subquery},
                                #{content_topics_subquery},
                                #{practice_locale_pack_id_subquery},
                                #{is_practice_for_locale_pack_id_subquery},
                                #{practice_content_items_subuery}
                            )
                        as locale_pack_json_v4)
                    )
            ELSE
                NULL
            END"
    end


    def handle_updated_since_filter
        if filters[:updated_since]
            @query_builder.wheres << "
                (
                    extract(epoch from #{base_table_name}_versions.updated_at)::int > #{filters[:updated_since]}
                    OR
                    extract(epoch from locale_packs.updated_at)::int > #{filters[:updated_since]}
                )
            "
        end
    end

    def handle_updated_before_filter
        if filters[:updated_before]
            @query_builder.wheres << "extract(epoch from #{base_table_name}_versions.updated_at)::int < #{filters[:updated_before]}"
        end
    end

    def filter_by_locale_or_en(locale_accessor, join_user = false)
        if @filters[:published]
            maybe_join_content_items_to_publishers = "
                join #{base_table_name}_versions content_item_versions
                    on content_item_versions.id = content_items.id
                join content_publishers
                        ON content_publishers.#{base_table_name.singularize}_version_id = content_item_versions.version_id
            "

            maybe_join_translations_to_publishers = "
                join #{base_table_name}_versions translation_versions
                    on translation_versions.id = translations.id
                join content_publishers translation_content_publishers
                        ON translation_content_publishers.#{base_table_name.singularize}_version_id = translation_versions.version_id
            "
        else
            maybe_join_content_items_to_publishers = maybe_join_translations_to_publishers = ""
        end

        if join_user
            if @user_id.nil?
                raise "user_id is not defined when trying to filter by users's locale"
            end
            join_user_string = "join users
                    on users.id='#{@user_id}'"
            group_by_user_string = ", users.pref_locale"
        else
            join_user_string = ""
            group_by_user_string = ""
        end

        @query_builder.withs << "content_items_in_locale_or_en AS MATERIALIZED (
            select distinct
                content_items.id
            from #{base_table_name} content_items

                join #{locale_pack_table} locale_packs
                    on content_items.locale_pack_id = locale_packs.id
                #{maybe_join_content_items_to_publishers}

                join #{base_table_name} translations
                    on translations.locale_pack_id = locale_packs.id
                #{maybe_join_translations_to_publishers}

                #{join_user_string}

            group by content_items.id, content_items.locale #{group_by_user_string}
            having

                -- items in the user's locale
                content_items.locale = #{locale_accessor}

                -- items in english without a translation in the user's locale
                OR (
                    content_items.locale = 'en'
                    AND NOT array_agg(translations.locale) @> Array[#{locale_accessor}::text]
                )
        )"

        @query_builder.wheres << "
            #{base_table_name}_versions.id IN (SELECT id FROM content_items_in_locale_or_en)
        "
    end

    def handle_users_locale_or_en_filter
        return unless filters[:in_users_locale_or_en]

        filter_by_locale_or_en('users.pref_locale', true)
    end

    def handle_locale_or_en_filter
        return unless filters.key?(:in_locale_or_en)

        locale = filters[:in_locale_or_en]

        # FIXME: array support?
        raise "We only support string values for in_locale_or_en" unless locale.is_a?(String)

        filter_by_locale_or_en("'#{locale}'")
    end

    def join_base_table
        @query_builder.joins << "JOIN #{base_table_name} ON #{base_table_name}.id = #{base_table_name}_versions.id"
    end

    def filter_out_unpublished
        @query_builder.joins << "JOIN content_publishers
                        ON content_publishers.#{base_table_name.singularize}_version_id = #{base_table_name}_versions.version_id"
    end

    def filter_out_not_working_version
        @query_builder.withs  << %Q~
            latest_version_updated_ats AS MATERIALIZED (
                select
                    id, max(updated_at) updated_at
                from #{base_table_name}_versions
                group by id
            )
        ~
        @query_builder.joins << %Q~
            JOIN latest_version_updated_ats
                ON #{base_table_name}_versions.id = latest_version_updated_ats.id
                AND #{base_table_name}_versions.updated_at = latest_version_updated_ats.updated_at
        ~
    end

    def handle_id_filter
        return unless filters.key?(:id)

        ids = Array(filters[:id])

        if !ids.blank?
            string_id = ids.map { |id|
                "'#{id}'"
            }.join(',')
            @query_builder.wheres << "#{base_table_name}_versions.id IN (#{string_id})"
        else
            @query_builder.wheres << "FALSE"
        end
    end

    def handle_locale_pack_id_filter
        return unless filters.key?(:locale_pack_id)

        if filters[:locale_pack_id].nil?
            @query_builder.wheres << "#{base_table_name}.locale_pack_id IS NULL"
            return
        end

        ids = Array(filters[:locale_pack_id])


        col = "#{base_table_name}.locale_pack_id"
        string_id = ids.compact.map { |id|
            "'#{id}'"
        }.join(',')

        # if nil and some other value is included in the array
        if ids.include?(nil) && ids.any?
            @query_builder.wheres << "(#{col} IS NULL OR #{col} IN (#{string_id}))"
        elsif ids.any?
            @query_builder.wheres << "#{col} IN (#{string_id})"
        elsif ids.include?(nil)
            @query_builder.wheres << "#{col} IS NULL"
        elsif ids.empty?
            @query_builder.wheres << "FALSE"
        else
            raise "What other possibilites are there?"
        end
    end

    def handle_version_id_filter
        return unless filters.key?(:version_id)

        if filters[:version_id].nil?
            @query_builder.wheres << "#{base_table_name}_versions.version_id IS NULL"
            return
        end

        ids = Array(filters[:version_id])


        col = "#{base_table_name}_versions.version_id"
        string_id = ids.compact.map { |id|
            "'#{id}'"
        }.join(',')

        # if nil and some other value is included in the array
        if ids.include?(nil) && ids.any?
            @query_builder.wheres << "(#{col} IS NULL OR #{col} IN (#{string_id}))"
        elsif ids.any?
            @query_builder.wheres << "#{col} IN (#{string_id})"
        elsif ids.include?(nil)
            @query_builder.wheres << "#{col} IS NULL"
        elsif ids.empty?
            @query_builder.wheres << "FALSE"
        else
            raise "What other possibilites are there?"
        end
    end

    def handle_locale_filter
        return unless filters.key?(:locale)

        locales = Array(filters[:locale])

        if !locales.blank?
            string_locales = locales.map { |locale|
                "'#{locale}'"
            }.join(',')
            @query_builder.wheres << "#{base_table_name}.locale IN (#{string_locales})"
        else
            @query_builder.wheres << "FALSE"
        end
    end

    def join_and_select_entity_metadata
        @query_builder.joins << "LEFT OUTER JOIN entity_metadata
                    ON entity_metadata.id = #{base_table_name}_versions.entity_metadata_id
                    LEFT OUTER JOIN s3_assets seo_images
                    ON seo_images.id = entity_metadata.image_id"

        @query_builder.selects['entity_metadata'] = "
            CASE WHEN entity_metadata.id IS NOT NULL THEN
                    row_to_json(
                        cast(
                            row(
                                entity_metadata.id,
                                entity_metadata.title,
                                entity_metadata.description,
                                entity_metadata.canonical_url,
                                entity_metadata.tweet_template,
                                case
                                    when seo_images.id is not null then
                                        row_to_json(
                                            cast(
                                                row(
                                                    seo_images.id,
                                                    seo_images.formats,
                                                    seo_images.dimensions
                                                )
                                            as image_json_v1)
                                        )
                                    else
                                        null
                                    end
                            )
                        as entity_metadata_json_v2)
                    )
            ELSE
                NULL
            END"
    end

    def publishing_info_with()

        base_table_id_key = base_record + "_id"
        id_key_in_cp_table = base_record + "_version_id"

        "publishing_info AS MATERIALIZED (
            SELECT
                #{id_key_in_cp_table} as published_version_id,
                published_at,
                #{versions_table}.id as #{base_table_id_key}
            FROM content_publishers
            JOIN #{versions_table}
                ON content_publishers.#{id_key_in_cp_table} = #{versions_table}.version_id
        )"
    end

    def include_publishing_info_if_necessary
        if (['published_at', 'published_version_id'] & fields).any?
            base_table_id_key = base_record + "_id"

            @query_builder.withs  << publishing_info_with()
            @query_builder.joins << "LEFT OUTER JOIN publishing_info ON
                            publishing_info.#{base_table_id_key} = #{versions_table}.id"

            @query_builder.selects["published_at"] = "cast(EXTRACT(EPOCH FROM publishing_info.published_at) as int)"
            @query_builder.selects["published_version_id"] = "publishing_info.published_version_id"
        end
    end


    def join_and_select_image

        images_table = "#{base_record}_images"

        @joins << "LEFT OUTER JOIN s3_assets #{base_record}_images
                    ON #{images_table}.id = #{versions_table}.image_id"


        @selects['image'] = "
            CASE WHEN #{images_table}.id IS NOT NULL THEN
                    row_to_json(
                        cast(
                            row(
                                #{images_table}.id,
                                #{images_table}.formats,
                                #{images_table}.dimensions
                            )
                        as image_json_v1)
                    )
            ELSE
                NULL
            END"
    end

    def base_record
        base_table_name.singularize
    end

    def versions_table
        base_table_name + "_versions"
    end

end