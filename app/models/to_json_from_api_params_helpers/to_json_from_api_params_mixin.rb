require 'back_royal/object' # looks_like_uuid?

module ToJsonFromApiParamsHelpers::ToJsonFromApiParamsMixin
    extend ActiveSupport::Concern

    included do
        cattr_accessor :base_klass
        attr_accessor :fields
        attr_reader :params, :filters, :limit
        attr_reader :selects, :wheres
    end

    def base_klass
       raise "base_klass not set" unless self.class.base_klass
       self.class.base_klass
    end

    def base_table_name
       raise "base_klass not set" unless base_klass
       base_klass.table_name
    end

    def cast_user(table)
        "case when #{table}.id is null
            then null
        else
            cast(row(
                #{table}.id,
                (
                    initcap(case when #{table}.name is null then '' else #{table}.name end)
                )
            ) as user_json_v1)
        end"
    end

    def do_basic_params_prep(params)
        params = params.unsafe_with_indifferent_access
        params[:filters] ||= {}
        booleanize(params)

        # we support :id in the filters, because it seems like
        # it sort of belongs there, but also in the top of the params,
        # because that's where REST seems to expect it
        if params[:id]
            params[:filters][:id] = params[:id]
        end

        user_id = params[:user] ? params[:user].id : params[:user_id]

        if user_id && !user_id.looks_like_uuid?
            raise ArgumentError.new("user_id must be a uuid")
        end

        @params = params
    end

    def set_fields(opts)

        available_fields = opts[:available_fields]

        except = params[:except] || []
        fields = if params.key?(:fields)
            params[:fields].map(&:to_s)
        else
            available_fields
        end

        (opts[:aliases] || []).each do |_alias, _fields|

            if except.include?(_alias) || except.include?(_alias.to_s)
                except += _fields
            end

            if fields.include?(_alias) || fields.include?(_alias.to_s)
                fields += _fields
            end
        end

        fields -= except

        @fields = fields
    end

    # Protect against sql injection by requiring all filters
    # to be checked for a type or, if a string, that they match
    # a regex.  Since filters should be the only thing coming in
    # from the api that is included in the sql, this should
    # be sufficient to protect us from sql injection attacks.
    def set_filters(opts)
        opts = opts.with_indifferent_access
        filters = {}
        (params[:filters] || {}).each do |key, value|

            type = opts[key]
            if type.is_a?(Regexp)
                matcher = type
                type = :regexp
            end
            raise ArgumentError.new("Filter #{key.inspect} is not supported") unless type

            value_is_allowed = if type == :bool
                [nil, true, false].include?(value)
            elsif type == :uuid
                value.looks_like_uuid?
            elsif type == :array_of_uuids
                arr = Array.wrap(value)
                arr.compact.all?(&:looks_like_uuid?) # accept nils or uuids
            elsif type == :time
                value.is_a?(Time)
            elsif type == :number
                value.is_a?(Numeric)
            elsif type == :regexp
                value.is_a?(String) && value.match(matcher)
            elsif type == :locale
                arr = Array.wrap(value)
                arr.all? { |locale| I18n.is_locale?(locale) }
            else
                raise ArgumentError.new("Unknown filter type: #{type.inspect}")
            end

            raise ArgumentError.new("Unsupported value for filter #{key.inspect} of type #{type}: #{value.inspect}") unless value_is_allowed

            filters[key] = value

        end

        @filters = filters.with_indifferent_access
    end

    def escape_for_like_query(value)
        value.gsub(/'/, "''''").gsub(/["&!]/, "")
    end

    # convert this to a subquery that will return a
    # json array of items
    def to_array_subquery
        "
            -- convert the desired columns for each stream into json,
            -- group all the json values
            -- into an array, then convert the array to json
            -- (coalesce allows us to make it an emtpy array where there are no lessons)
            COALESCE(
                (SELECT array_to_json(array_agg(row_to_json(prepared_records))) FROM
                    (
                        -- select the desired columns from lessons after
                        -- extracting the list of lesson_ids from the
                        -- chapters json
                        #{sql}
                    ) prepared_records
                ),
                '[]'::json)
            "

    end

    # convert this to a subquery that will return a single
    # json item
    def to_item_subquery
        "
            -- convert the desired columns for each lesson into json,
            -- group all the json values
            -- into an array, then convert the array to json
            -- (coalesce allows us to make it an emtpy array where there are no lessons)
            COALESCE(
                (SELECT row_to_json(prepared_lessons_versions) FROM
                    (
                        -- select the desired columns from lessons after
                        -- extracting the list of lesson_ids from the
                        -- chapters json
                        #{sql}
                    ) prepared_lessons_versions
                ),
                NULL)
        "
    end

    def orders
        @query_builder.orders
    end

    # deeply replace "true" and "false" values
    # with true and false, respectively
    def booleanize(hash)
        hash.each do |key, value|
            if value.is_a?(String) && value == "true"
                hash[key] = true
            elsif value.is_a?(String) && value == "false"
                hash[key] = false
            elsif value.is_a?(Hash)
                booleanize(value)
            end
        end
    end

    def sql
        @query_builder.sql
    end


    def json
        @json ||= execute
    end

    def to_a
        ActiveSupport::JSON.decode(json)
    end

end
