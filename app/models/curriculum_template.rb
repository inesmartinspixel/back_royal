# == Schema Information
#
# Table name: curriculum_templates
#
#  id                                      :uuid             not null, primary key
#  created_at                              :datetime         not null
#  updated_at                              :datetime         not null
#  name                                    :text             not null
#  description                             :text
#  specialization_playlist_pack_ids        :uuid             default([]), not null, is an Array
#  periods                                 :json             not null, is an Array
#  num_required_specializations            :integer          default(0), not null
#  program_type                            :text             not null
#  admission_rounds                        :json             is an Array
#  registration_deadline_days_offset       :integer          default(0)
#  enrollment_deadline_days_offset         :integer
#  stripe_plans                            :json             is an Array
#  scholarship_levels                      :json             is an Array
#  project_submission_email                :string
#  early_registration_deadline_days_offset :integer          default(0)
#  id_verification_periods                 :json             is an Array
#  graduation_days_offset_from_end         :integer
#  diploma_generation_days_offset_from_end :integer
#  learner_project_ids                     :uuid             default([]), not null, is an Array
#  enrollment_agreement_template_id        :text
#  playlist_collections                    :json             not null, is an Array
#

class CurriculumTemplate < ApplicationRecord

    # FIXME: See https://trello.com/c/Jz1ubPBe.
    self.ignored_columns = %w(required_playlist_pack_ids)

    include Groupable
    include Cohort::Schedulable
    include Cohort::CurriculumWithProjects
    include CrudFromHashHelper

    validates_presence_of :name

    def self.create_from_hash!(user, hash, meta = {})
        RetriableTransaction.transaction do
            instance = self.new
            instance.merge_hash(hash)
            instance.save!
            instance
        end
    end

    def self.update_from_hash!(user, hash, meta = {})
        RetriableTransaction.transaction do
            hash, meta, instance = prepare_update_from_hash(hash, meta)
            instance.merge_hash(hash)
            instance.save!
            instance
        end
    end

    def merge_hash(hash)
        %w(name description specialization_playlist_pack_ids periods num_required_specializations program_type
             admission_rounds id_verification_periods enrollment_deadline_days_offset graduation_days_offset_from_end
             registration_deadline_days_offset early_registration_deadline_days_offset stripe_plans scholarship_levels
             project_submission_email diploma_generation_days_offset_from_end learner_project_ids enrollment_agreement_template_id playlist_collections).each do |key|

            if hash.key?(key)
                val =  hash[key]
                val = nil if val.is_a?(String) && val.blank?
                self[key] = val
            end
        end

        # Update associated groups
        groups = hash["groups"]
        if groups
            self.access_groups = AccessGroup.where(id: groups.map { |g| g['id'] } )
        end
    end

    def as_json(opts={})
        opts = opts.with_indifferent_access

        if opts[:fields] == ['UPDATE_FIELDS']
            fields = ['id', 'updated_at', 'created_at']
        else
            fields = opts[:fields]
        end

        curriculum_template_json = {
            id: self.id,
            name: self.name,
            description: self.description,
            specialization_playlist_pack_ids: self.specialization_playlist_pack_ids,
            periods: self.periods,
            groups: self.access_groups.as_json,
            num_required_specializations: num_required_specializations,
            program_type: program_type,
            admission_rounds: admission_rounds,
            id_verification_periods: id_verification_periods,
            registration_deadline_days_offset: registration_deadline_days_offset,
            early_registration_deadline_days_offset: early_registration_deadline_days_offset,
            enrollment_deadline_days_offset: enrollment_deadline_days_offset,
            graduation_days_offset_from_end: graduation_days_offset_from_end,
            stripe_plans: self.stripe_plans,
            scholarship_levels: self.scholarship_levels,
            diploma_generation_days_offset_from_end: self.diploma_generation_days_offset_from_end,
            project_submission_email: self.project_submission_email,
            learner_project_ids: learner_project_ids,
            enrollment_agreement_template_id: self.enrollment_agreement_template_id,
            playlist_collections: self.playlist_collections || [] # FIXME: Remove this conditional fallback after playlist_collections has been backfilled for all curriculum templates (see https://trello.com/c/zOJC4xOq)
        }
        curriculum_template_json['updated_at'] = self.updated_at.to_timestamp if self.has_attribute?('updated_at')
        curriculum_template_json['created_at'] = self.created_at.to_timestamp if self.has_attribute?('created_at')

        curriculum_template_json = curriculum_template_json.slice(*fields) if fields.present?
        curriculum_template_json
    end

end
