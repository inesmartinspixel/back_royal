# == Schema Information
#
# Table name: lesson_progress
#
#  id                               :uuid             not null, primary key
#  created_at                       :datetime
#  updated_at                       :datetime
#  frame_bookmark_id                :uuid
#  started_at                       :datetime
#  completed_at                     :datetime
#  user_id                          :uuid
#  frame_history                    :json
#  completed_frames                 :json
#  challenge_scores                 :json
#  frame_durations                  :json
#  locale_pack_id                   :uuid             not null
#  best_score                       :float
#  last_checked_for_cert_generation :datetime
#

class LessonProgress < ApplicationRecord

    self.table_name =  "lesson_progress" # rails wanted to call this 'lesson_progresses' so we override
    belongs_to :locale_pack, :class_name => "Lesson::LocalePack", :foreign_key => 'locale_pack_id', autosave: false
    has_many :lessons, :class_name => "Lesson", :primary_key => :locale_pack_id, :foreign_key => :locale_pack_id, autosave: false
    has_one :english_lesson,  -> {where("lessons.locale" => 'en')}, :through => :locale_pack, :class_name => "Lesson", :source => :content_items, autosave: false

    belongs_to :user
    after_destroy :log_reset_event
    after_save :identify_if_necessary
    after_save :sync_with_airtable_if_necessary

    after_save :update_application_final_score, if: :saved_change_to_best_score_on_test_or_assessment?
    after_destroy  :update_application_final_score, if: :for_test_or_assessment_lesson?

    validates_presence_of :locale_pack_id
    validates_presence_of :user_id

    # if a row exists in lesson_progress, then the user
    # must have started the stream and so we'll have a started_at
    validates_presence_of :started_at

    def self.create_or_update!(attrs)
        RetriableTransaction.transaction do
            attrs = attrs.unsafe_with_indifferent_access
            locale_pack_id = attrs[:locale_pack_id]

            if locale_pack_id.nil?
                raise ActiveRecord::RecordNotSaved.new("#{self.name} could not be saved: no locale_pack_id found for #{attrs.inspect}")
            end

            # Since we have the big json objects like challenge_scores, there is danger
            # of simultaneous saves blowing away data.  So we lock the record.
            instance = LessonProgress.where(
                :user_id => attrs[:user_id],
                :locale_pack_id => locale_pack_id
            ).lock.first

            if instance.nil?
                instance = new

                # only trust the server, not the client to record timestamps
                instance.user_id = attrs[:user_id]
                instance.started_at = Time.now
                instance.locale_pack_id = locale_pack_id
            end

            instance.updated_at = Time.now

            # since checking for which streams might need to have certs generated is expensive,
            # we do not do it more than once per hour
            unless instance.last_checked_for_cert_generation && instance.last_checked_for_cert_generation > Time.now - 1.hour
                instance.pregenerate_stream_completion_certificates
            end

            if attrs[:complete] == true && instance.completed_at.present? && instance.for_test_lesson? && !instance.user.can?(:reopen_completed_test_lessons, instance)
                instance.errors.add(:test_lesson, "is complete")
                Raven.capture_exception("Trying to save lesson_progress for completed test lesson.", {
                    level: 'warning',
                    extra: attrs.merge({
                        time: Time.now.to_s
                    })
                })
                raise ActiveRecord::RecordInvalid.new(instance)
            elsif attrs[:complete] == true && instance.completed_at.nil?
                # only trust the server, not the client to record timestamps
                instance.completed_at = Time.now
            end

            attrs.slice(:frame_bookmark_id, :frame_history,
                    :frame_durations
            ).each do |key, value|
                instance[key] = value
            end

            instance.merge_completed_frames(attrs[:completed_frames])
            instance.merge_challenge_scores(attrs[:challenge_scores])

            if attrs[:best_score] && attrs[:best_score] - (instance.best_score || -1) > 0.001
                instance.warn_on_potential_best_score_hack(attrs[:best_score])
                instance.best_score = attrs[:best_score]
            end

            test_hook if respond_to?(:test_hook)

            begin
                connection.execute("SAVEPOINT before_lesson_progress_save")
                instance.save!
                return instance
            rescue Exception => err
                if err.is_a?(ActiveRecord::RecordNotUnique)

                    connection.execute("ROLLBACK TO SAVEPOINT before_lesson_progress_save")

                    # I tried to use attrs.slice here but led to forbidden mass assignment error
                    existing_instance = LessonProgress.where(
                        user_id: attrs[:user_id],
                        locale_pack_id: attrs[:locale_pack_id]
                    ).first
                    ignorable_attrs = ["id", "created_at", "updated_at", "started_at", "last_checked_for_cert_generation"]
                    if existing_instance && existing_instance.attributes.except(*ignorable_attrs) == instance.attributes.except(*ignorable_attrs)
                        return existing_instance
                    end
                end

                raise err
            end
        end
    end

    def self.clear_all_progress(user_id)
        LessonProgress.where({:user_id => user_id}).destroy_all
    end

    def get_associated_stream_progresses
        # i tried to set this up as an association but couldn't get it to work
        streams_query = Lesson::Stream.joins(:content_publishers).joins(:lesson_joins => :lesson).where("lessons.locale_pack_id = ?", locale_pack_id).select("lesson_streams.locale_pack_id")
        Lesson::StreamProgress.where(user_id: self.user_id).where("locale_pack_id in (#{streams_query.to_sql})")
    end

    def pregenerate_stream_completion_certificates
        # Loop through all streams associated with this lesson, and check whether we need to pregenerate any certificates
        self.get_associated_stream_progresses.each do |associated_stream_progress|
            # We pre-generate the certificate images so they are for sure in s3 by the time the user finishes the course.
            # There are a few cases where certificate pregeneration and regeneration can happen:
            # 1. If the user just started the last lesson in the course, and there is no cert image, we pregenerate
            # 2. If the user just resumed the last lesson in the course, and there is a cert image but its more than a day old, we regenerate
            # 3. If the user has just completed the course, but its been more than a day since the cert was pregenerated, we regenerate
            if ((associated_stream_progress.on_last_lesson &&
                    # and theres no cert yet
                    (associated_stream_progress.certificate_image_id == nil ||
                    # or there is a cert but its more than a day old
                    (associated_stream_progress.certificate_image.updated_at.strftime("%d %^b %Y") != self.updated_at.strftime("%d %^b %Y") )
                )))

                # re-generate and update the certificate image (will also prune old version)
                associated_stream_progress.generate_certificate_image!
            end
        end

        self.last_checked_for_cert_generation = Time.now
    end

    def lesson
        unless Rails.env.test?
            begin
                # we're not handling locales correctly below
                raise RuntimeError.new("Do not use LessonProgress#lesson in the wild")
            rescue RuntimeError => err
                Raven.capture_exception(err)
            end
        end
        if @lesson && @lesson.locale != user.pref_locale
            @lesson = nil
        end
        @lesson ||= lessons.where(locale: user.pref_locale).first
    end

    def for_test_lesson?
        unless defined? @for_test_lesson
            @for_test_lesson = Lesson.test_lesson_locale_pack_id?(locale_pack_id)
        end
        @for_test_lesson
    end

    def for_assessment_lesson?
        unless defined? @for_assessment_lesson
            @for_assessment_lesson = Lesson.assessment_lesson_locale_pack_id?(locale_pack_id)
        end
        @for_assessment_lesson
    end

    def for_test_or_assessment_lesson?
        for_test_lesson? || for_assessment_lesson?
    end

    def lesson_attrs_in_users_locale(fields = nil)
        params = {
            user_id: user_id,
            filters: {
                in_users_locale_or_en: true,
                published: true,
                locale_pack_id: locale_pack_id
            }
        }
        params[:fields] = fields unless fields.nil?
        attrs = Lesson::ToJsonFromApiParams.new(params).to_a[0]
        if attrs.nil?
            Raven.capture_exception(message, {
                fingerprint:  ["No lesson found for lesson progress"],
                extra: {
                    lesson_progress_id: self.id
                }
            })
        end
        attrs
    end

    # FIXME?: this may not be true forever, or if we did a data migration or something
    def last_progress_at
        updated_at
    end

    def official_test_score
        for_test_lesson? ? best_score : nil
    end

    def as_json(options = {})
        attrs = {
            :user_id => self.user_id,
            :created_at => self.created_at.to_timestamp,
            :updated_at => self.updated_at.to_timestamp,
            :locale_pack_id => self.locale_pack_id,
            :frame_bookmark_id => self.frame_bookmark_id,
            :frame_history => self.frame_history,
            :challenge_scores => self.challenge_scores,
            :completed_frames => self.completed_frames,
            :started_at => self.started_at.to_timestamp,
            :completed_at => self.completed_at.to_timestamp,
            :complete => !self.completed_at.nil?,
            :last_progress_at => self.last_progress_at.to_timestamp,
            :best_score => self.best_score,
            :frame_durations => self.frame_durations,
            :for_assessment_lesson => for_assessment_lesson?,
            :for_test_lesson => for_test_lesson?,
            :id => self.id # need this because iguana expects it, and uses it to determine whether its creating a new obj or updating an existing obj
        }

        # Do not set special attributes here if :format_for_update_response
        # is passed in.  If someone is working in two browsers
        # at once, we want to push down the updated results so that both browsers
        # agree on the challenge_scores and things.

        attrs.as_json
    end

    def identify_if_necessary
        # if we are completing a lesson then identify to
        # be sure that the lessons_completed attrs are updated
        if saved_changes.key?('completed_at')
            user.identify
        end
    end

    def sync_with_airtable_if_necessary
        # if we are completing a lesson and have a pending cohort application, sync
        if saved_changes.key?('completed_at') && user.pending_application
            user.update_in_airtable
        end
    end

    # see comment in Lesson:StreamProgress about why we log this from the server
    def log_reset_event
        lesson = Lesson::ToJsonFromApiParams.new(
            user_id: user.id,
            filters: {
                locale_pack_id: locale_pack_id,
                in_users_locale_or_en: true,
                published: true
            }
        ).to_a.first

        if !lesson
            Raven.capture_exception('No lesson found in log_reset_event', {
                extra: {
                    locale_pack_id: locale_pack_id,
                    user: user.id
                }
            })
            return
        end

        Event.create_server_event!(SecureRandom.uuid, user.id, 'lesson:reset', {
            lesson_id: lesson['id'],
            lesson_title: lesson['title'],
            lesson_complete: !self.completed_at.nil?,
            lesson_version_id: lesson['version_id']
        }).log_to_external_systems
    end

    def saved_change_to_best_score_on_test_or_assessment?
        saved_change_to_best_score? &&
        for_test_or_assessment_lesson?
    end

    def update_application_final_score
        user.application_for_relevant_cohort&.set_final_score&.save!
    end

    # duplicated in lessonProgressInterceptor.js
    def merge_completed_frames(incoming_completed_frames)
        # completed_frames is an object with frame_ids as keys and `true`
        # as every value.  By merging, we make it so any frame that has
        # been completed in any client will ve marked as completed.
        return unless incoming_completed_frames

        # after an assessment lesson, we need to be able to
        # reset the hash back to empty.  As a tiny level
        # of protection against cheating, disallow this for test lessons
        if incoming_completed_frames.empty? && !self.for_test_lesson?
            self.completed_frames = {}
            return
        end

        self.completed_frames ||= {}
        self.completed_frames.merge!(incoming_completed_frames)
    end

    # duplicated in lessonProgressInterceptor.js
    def merge_challenge_scores(incoming_challenge_scores)
        return unless incoming_challenge_scores

        # after an assessment lesson, we need to be able to
        # reset the hash back to empty.  As a tiny level
        # of protection against cheating, disallow this for test lessons
        if incoming_challenge_scores.empty? && !self.for_test_lesson?
            self.challenge_scores = {}
            return
        end

        self.challenge_scores ||= {}
        incoming_challenge_scores.each do |key, score|

            current_score = self.challenge_scores[key]
            has_current_score = !!current_score

            # On test and assessment lessons, we preserve the very first score the user enters.
            # On regular lessons the score can change.
            # (Note that on assessment lessons, even though the score does not change if you do
            # a frame over again, it could get reset back to null after you finish the lesson)
            # This logic is duplicated client-side in setChallengeScore
            should_update_score = (for_test_or_assessment_lesson? && !has_current_score) ||
                                (!for_test_or_assessment_lesson? && score != current_score)

            if should_update_score
                self.challenge_scores[key] = score;
            end
        end
    end

    def warn_on_potential_best_score_hack(new_best_score)
        attack_types = []

        # We already prevent the saving of a lesson progress for a test lesson that was
        # already marked as complete, so I don't see how we could hit this.  Not bad
        # to have it though
        if self.for_test_lesson? && self.best_score && new_best_score - self.best_score > 0.001
            attack_types << 'increasing best score on test lesson'
        end

        # - Check that a user didn't just hack together a request with a high `best_score`
        # without setting up the challenge_scores.
        # - On an assessment lesson, the same request
        # that sets the best_score will also unset the challenge_scores, so we may need to
        # grab the old object from the `changes`.  (This probably relies on the assumption that
        # there are no challenges on the last frame of a lesson, but that should be a reliable
        # assumption for now.  If we get erroneous warnings, we'll adjust)
        # - NOTE: It is possible for the user to re-do an
        # assessment lesson, so there can be situations where the best_score does not line
        # up with what is in challenge_scores.  However, in any case where the best_score
        # changes, we expect that the challenge_scores hash will reflect the provided best_score.
        if challenge_scores_changed? && self.challenge_scores.empty? && self.for_assessment_lesson?
            challenge_scores = changes[:challenge_scores][0]
        else
            challenge_scores = self.challenge_scores
        end
        scores = challenge_scores.values
        expected_score = scores.any? ? (scores.reduce(0) { |sum, val| sum + val }.to_f / scores.size) : nil
        if expected_score.nil? || (expected_score - new_best_score).abs > 0.001
            attack_types << 'challenge scores do not add up to provided score'
        end

        if attack_types.any? && !user.has_role?('admin')
            Raven.capture_exception("Unexpected update to best_score.  Could be a hacker", {
                level: 'warning',
                extra: {
                    attack_types: attack_types,
                    user_id: self.user_id,
                    locale_pack_id: self.locale_pack_id,
                    lesson_progress_id: self.id,
                    old_best_score: self.best_score,
                    new_best_score: new_best_score,
                    expected_score: expected_score
                }
            })
        end
    end

    class LessonProgressError < RuntimeError; end
    class LessonNotFoundError < RuntimeError; end

end
