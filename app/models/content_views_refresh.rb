# == Schema Information
#
# Table name: content_views_refresh
#
#  updated_at :datetime
#


class ContentViewsRefresh < ApplicationRecord

    default_scope -> { order(:updated_at) }
    self.table_name = 'content_views_refresh'

    def self.updated_at
        self.maximum(:updated_at) || Time.at(0)
    end

    def self.debounced_value
        SafeCache.fetch("content_views_refresh_updated_at", expires_in: 5.minutes) do
            ContentViewsRefresh.first&.updated_at || 0
        end
    end

    def as_json(options = {})
        {
            updated_at: updated_at
        }
    end

end
