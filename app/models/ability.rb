# The devise + cancan + rolify tutorial this was initially based on: https://github.com/EppO/rolify/wiki/Tutorial
# More info on defining roles: https://github.com/ryanb/cancan/wiki/Defining-Abilities
class Ability

    include CanCan::Ability

    def initialize(user_or_authorized_institution)

        ######################################
        ## Action Aliases
        ######################################

        alias_action :update, :create, :to => :write
        alias_action :save!, :update, :update_from_hash!, :create_from_hash!, :show_record, :get_image_asset, :images, :index, :show, :create, :destroy, :destroy_all_progress, :to => :crud
        alias_action :content, :show, :get_image_asset, :to => :read
        alias_action :index, :show, :create, :update, :to => :admin_without_delete

        ######################################
        ## Global Roles
        ######################################

        can :show, :coupon
        can :show, Lesson
        can :create, Event
        can :read, Lesson::Stream
        can :get_outdated, Lesson::Stream
        can :index, :auto_suggest_option
        can :show, HiringRelationship
        can :index, CareerProfileList

        if user_or_authorized_institution.is_a?(Institution)
            institution = user_or_authorized_institution
        elsif user_or_authorized_institution.is_a?(User)
            user = user_or_authorized_institution
        end

        user_id = user && user.id

        can :load_full_content_for_lesson_id, Object do |stream_and_lesson|

            stream_and_lesson = stream_and_lesson.with_indifferent_access
            lesson_json = stream_and_lesson[:lesson]

            if institution || user || (lesson_json && lesson_json['unrestricted'])
                true
            else
                false
            end
        end

        can :read_streams, Object do |params|
            filters = params[:filters]

            if self.can?(:crud, Lesson::Stream)
                # do nothing. these guys can do anything
                true
            elsif filters.key?(:view_as)
                view_as = filters[:view_as]
                type, value = view_as.split(':') unless view_as.nil?

                # Non-admins cannot use view_as, unless they use
                # it to ask for the PROMOTED_DEGREE cohorts (which is what happens if you're
                # not logged in and you go to the library, or on marketing pages)
                (type == 'cohort' && ['PROMOTED_DEGREE'].include?(value))
            elsif params[:user_id] && filters[:user_can_see]

                # User can view themselves
                if params[:user_id] == user_id
                    true
                # institutional reports viewers can view people in their institutions
                elsif self.can?(:view_reports_for_user_id, params[:user_id])
                    true
                else
                    false
                end
            else
                false
            end
        end

        # yes it is odd that not being logged in gives you MORE access, but that's
        # what our current sharing functionality requires
        can :view_full_hiring_manager_experience, Object do
            user.nil? || user.hiring_team&.has_full_access?
        end

        add_abilities_for_user(user) unless user.nil?
    end

    def add_abilities_for_user(user)
        can :read, :student_dashboard

        can :view_reports_for_user_id, Object do |user_id|
            user.is_institutional_reports_viewer? &&
            user.views_reports_for_institutions.map { |inst| inst.users.ids }.flatten.compact.uniq.include?(user_id)
        end

        can :read_student_dashboards, Object do |params|
            filters = params[:filters]
            allowed = true

            # non-super-editors cannot use view_as
            if filters.key?(:view_as) && user.ability.cannot?(:crud, StudentDashboard)
                allowed = false

            # one or the other of these always must be set
            elsif !filters.key?(:view_as) && !filters.key?(:user_can_see)
                allowed = false
            end

            allowed
        end

        can :index_hiring_relationships, Object do |params|
            allowed = true
            filters = params[:filters]

            is_candidate = (filters[:candidate_id] && filters[:candidate_id] == user.id)
            is_hiring_manager = (filters[:hiring_manager_id] && filters[:hiring_manager_id] == user.id)
            limited_to_users_team = (filters[:hiring_team_id] && filters[:hiring_team_id] == user.hiring_team_id)
            limited_to_a_hiring_manager_or_team = is_hiring_manager || limited_to_users_team
            only_asking_for_id_and_status = params[:fields] && (params[:fields] - ['id', 'hiring_manager_status', 'candidate_status']).empty?

            # check to see if the user is filtering out hidden profiles
            requesting_hidden = nil
            candidate_statuses = Array(filters[:candidate_status])
            hiring_manager_statuses = Array(filters[:hiring_manager_status])
            if is_candidate && (
                (candidate_statuses.any? && !candidate_statuses.include?('hidden')) ||
                (Array(filters[:candidate_status_not]).include?('hidden'))
            )
                requesting_hidden = false
            elsif limited_to_a_hiring_manager_or_team && (
                (hiring_manager_statuses.any? && !hiring_manager_statuses.include?('hidden')) ||
                (Array(filters[:hiring_manager_status_not]).include?('hidden'))
            )
                requesting_hidden = false
            else
                requesting_hidden = true
            end

            if !is_candidate && !limited_to_a_hiring_manager_or_team
                allowed = false
            end

            # you HAVE to filter out hidden hiring relationships UNLESS
            # you are just asking for the ids and statuses.  This supports removing
            # elements from the client-side cache
            if requesting_hidden && !only_asking_for_id_and_status
                allowed = false
            end

            if filters.key?(:hiring_team_id) && filters[:hiring_team_id] != user.hiring_team_id
                allowed = false
            end

            # hiring managers are only allowed to index records for candidates they are allowed to view
            if limited_to_a_hiring_manager_or_team && !filters[:viewable_by_hiring_manager]
                allowed = false
            end

            allowed
        end

        # Hiring managers can create relationships for themselves from the hiring marketing pages
        can :create_hiring_relationships, Object do |params|
            if user.hiring_application&.status != 'accepted' && !user.pending_pay_per_post?
                false
            elsif !user.hiring_team&.has_full_access? && !user.hiring_team&.is_freemium_candidate?(params[:candidate_id])
                false
            else
                (params.key?(:hiring_manager_id) && params[:hiring_manager_id] == user.id)
            end
        end

        can :update, HiringRelationship do |hiring_relationship|

            if hiring_relationship.candidate_id == user.id
                true
            elsif hiring_relationship.hiring_manager_id == user.id || (user.hiring_team_id && hiring_relationship.hiring_manager&.hiring_team_id == user.hiring_team_id)

                if user.hiring_team&.has_full_access?
                    true

                # if the hiring manager has already accepted this relationship, then ze
                # are allowed to continue sneding messages regardless of zir current standing
                elsif hiring_relationship.hiring_manager_status == 'accepted'
                    true

                # it would be rare for a hiring manager to be updating a relationship that
                # is not already accepted and is for a freemium candidate.  But it could happen
                # if the freemium candidate happened to be in the list of featured candidates
                elsif user.hiring_team&.is_freemium_candidate?(hiring_relationship.candidate_id)
                    true
                else
                    false
                end
            else
                false
            end

        end

        can :index_stream_progress, Object do |params|
            if params[:zapier]
                user.views_reports_for_institutions.pluck('id').include?(params[:institution_id])
            elsif params[:filters]
                params[:filters][:user_id] == user.id
            end
        end

        can :write, CohortApplication, :user_id => user.id
        can :update, HiringApplication, :user_id => user.id
        can :create, CareerProfile, :user_id => user.id
        can :write, CareerProfile, :user_id => user.id
        can :index, CareerProfile
        can :share, CareerProfile

        can :show_career_profile, Object do |opts|
            career_profile = opts[:career_profile]

            if career_profile.user_id == user.id
                true
            elsif career_profile.viewable_by_hiring_manager_through_deep_link?(user)
                true
            else
                false
            end
        end

        can :browse_non_anonymized_career_profiles, Object do
            user.hiring_team&.has_full_access?
        end

        can :index_using_career_profiles_controller, Object do |params|
            params = params.unsafe_with_indifferent_access
            (params['view'] == 'career_profiles' && can?(:index_career_profiles, params)) ||
            (params['view'] == 'student_network_profiles' && can?(:index_student_network_profiles, params))
        end

        can :index_career_profiles, Object do |params|
            params = params.unsafe_with_indifferent_access # get with indifferent access
            filters = params['filters'] || {}

            disallowed = false

            if user.has_accepted_hiring_manager_access? && filters['available_for_relationships_with_hiring_manager'] == user.id
                # accepted hiring managers can index career profiles
                # that are available for them to connect with, if they have
                # an allowed hiring_plan
                is_legacy = user.hiring_team.hiring_plan == HiringTeam::HIRING_PLAN_LEGACY
                is_unlimited_with_subscription = user.hiring_team.hiring_plan == HiringTeam::HIRING_PLAN_UNLIMITED_WITH_SOURCING &&
                    user.hiring_team.primary_subscription.present?
                disallowed = !(is_legacy || is_unlimited_with_subscription || !user.hiring_team.subscription_required)
            elsif filters['user_id'] == user.id
                # anyone can view their own profile
            else
                disallowed = true
            end

            !disallowed
        end

        can :update_hiring_team_with_params, Object do |params|
            params = params.stringify_keys
            hiring_team = HiringTeam.find(params['id'])

            disallowed = false

            # users can only update their own teams
            if hiring_team != user.hiring_team
                disallowed = true
            end

            # users can only update the hiring_plan
            if (params.keys - ['id', 'hiring_plan']).any?
                disallowed = true
            end

            # users can only update the hiring_plan if it is currently nil
            if hiring_team.hiring_plan.present? && hiring_team.hiring_plan != params['hiring_plan']
                disallowed = true
            end

            # users can only update the hiring_plan to the unlimited plan (pay_per_post
            # is set when creating a subscription for a featured position)
            if params['hiring_plan'] != HiringTeam::HIRING_PLAN_UNLIMITED_WITH_SOURCING
                disallowed = true
            end

            !disallowed
        end

        can :view_full_hiring_manager_experience, Object do
            user&.hiring_team&.has_full_access?
        end

        can :index_student_network_profiles, Object do |params|
            user.has_student_network_access?
        end

        can :index, :student_network_map
        can :index_student_network_maps, Object do |params|

            params = params.unsafe_with_indifferent_access # get with indifferent access

            disallowed = true

            if user.has_student_network_access?
                disallowed = false
            end

            !disallowed
        end

        can :create_student_network_messages, Object do |record, message_recipient|
            record = record.unsafe_with_indifferent_access
            def all_message_fields_are_present(record)
                !!(record['reply_to'] && record['subject'] && record['message_body'])
            end

            !!(user.has_full_student_network_access? && message_recipient&.can_receive_student_network_messages? && all_message_fields_are_present(record))
        end

        can :index_student_network_events, Object do |params|
            params = params.unsafe_with_indifferent_access # get with indifferent access

            allowed = true
            if params[:fields]&.include?("ADMIN_FIELDS") && !user.is_admin?
                allowed = false
            end
            allowed
        end

        can :read, OpenPosition
        can :index_open_positions, Object do |params|
            params = params.unsafe_with_indifferent_access # get with indifferent access
            filters = params['filters'] || {}

            allowed = true

            limited_to_current_user = (filters[:hiring_manager_id] == user.id)
            limited_to_team_for_current_user = (filters[:hiring_team_id] && filters[:hiring_team_id] == user.hiring_team_id)
            limited_to_featured_and_non_archived = (user.can_edit_career_profile && filters[:featured] == true && filters[:archived] == false)

            # Only allow hiring_manager interest-filtering if the candidate_id provided is for the current user
            # and ensure we require it for all candidate-based searches
            if limited_to_featured_and_non_archived
                allowed = false
                interested_in = filters[:hiring_manager_might_be_interested_in]
                recommended_for = filters[:recommended_for]

                if interested_in.present? && recommended_for.present?
                    allowed = interested_in == user.id && recommended_for == user.id
                elsif interested_in.present? && recommended_for.nil?
                    allowed = interested_in == user.id
                elsif interested_in.nil? && recommended_for.present?
                    allowed = recommended_for == user.id
                end
            end

            # For hiring managers you have to limit to your own or your team's open positions.
            # For candidates with can_edit_career_profile=true, you have to limit to featured and non-archived open positions.
            if !limited_to_current_user && !limited_to_team_for_current_user && !limited_to_featured_and_non_archived
                allowed = false
            end

            if params[:fields]&.include?("ADMIN_FIELDS")
                allowed = false
            end

            allowed
        end

        # only the current user or their teammates should be able to create or update their open positions
        can :create_or_update_open_positions, Object do |params|
            params = params.unsafe_with_indifferent_access # get with indifferent access
            record = params['record'] || (params['meta'] && params['meta']['open_position']) || {}
            is_hiring_manager_or_teammate = record['hiring_manager_id'] == user.id ||
                user.hiring_team_members.pluck(:id).include?(record['hiring_manager_id'])
            disallowed = false

            if !is_hiring_manager_or_teammate
                disallowed = true
            end

            # pay-per-post users cannot set featured to true through open_positions api.  They
            # can only do it by creating a subscription through subscriptions controller
            open_position = OpenPosition.find_by_id(record[:id])
            unarchiving = open_position&.archived && !record[:archived] && open_position&.featured
            publishing = !open_position&.featured && record[:featured]

            if user.hiring_team.hiring_plan == HiringTeam::HIRING_PLAN_PAY_PER_POST &&
                (unarchiving || publishing) &&
                !open_position&.subscription
                    disallowed = true
            end

            !disallowed
        end

        can :index_conversations, Object do |record|
            # a user can only index his own conversations
            user == record
        end

        # Conversations are created via the Api::HiringRelationshipsController, which has a lot more custom
        # validations ensuring that conversations are only created and updated between the appropriate users,
        # so I didn't feel like it was necessary to duplicate that work here.
        can :create_or_update_conversation, Object do |params|
            params = params.unsafe_with_indifferent_access # get with indifferent access
            allowed = true

            if params['candidate_position_interest_id']
                candidate_position_interest = CandidatePositionInterest.find_by_id(params['candidate_position_interest_id'])
                if candidate_position_interest
                    allowed = !!(user.hiring_team_id && candidate_position_interest.hiring_team&.id == user.hiring_team_id)
                else
                    allowed = false
                end
            end

            allowed
        end

        # Institutional reports viewers
        can :index, Lesson::StreamProgress do
            user.is_institutional_reports_viewer?
        end

        can :read_filter_options, Institution do |institution|
            user.views_reports_for_institutions.include?(institution)
        end

        can :create_hiring_team_invite_with_params, Object do |params|
            status = user.hiring_application&.status
            hiring_plan = user.hiring_team&.hiring_plan

            if params[:inviter_id] != user.id
                false
            elsif status == 'accepted'
                true
            elsif user.pending_pay_per_post?
                true
            else
                false
            end
        end

        can :create_report_with_params, Object do |params|
            filters = (params && params['filters']) || []
            inst_filter = filters.detect { |f| f['filter_type'] == 'InstitutionFilter' }
            if inst_filter.nil?
                false
            else
                allowed_ids = user.views_reports_for_institutions.pluck('id')
                (inst_filter['value'] - allowed_ids).empty?
            end
        end

        can :request_idology_link_with_params, Object do |params|
            params = params.unsafe_with_indifferent_access

            params['user_id'] == user.id &&

                # I don't think deferred users will need to do this, but no need to forbid it.  We're
                # just trying to prevent hackers from running up our idology bill
                ['accepted', 'pre_accepted', 'deferred'].include?(user.last_application&.status) &&
                user.relevant_cohort&.id_verification_periods&.any? &&
                !(user.idology_links_requested_in_last(1.minute) >= 5) &&
                !(user.idology_links_requested_in_last(1.day) >= 30)
        end

        can :ping_idology_with_params, Object do |params|
            params['user_id'] == user.id
        end

        # institution reports viewers can destroy reports for people in their
        # institutions
        can :destroy, Lesson::StreamProgress do |stream_progress|
            (stream_progress.user.institutions & user.views_reports_for_institutions).any?
        end

        can :create_candidate_position_interests, Object do |params|
            params[:record] && params[:record][:candidate_id] == user.id
        end

        can :update, CandidatePositionInterest do |candidate_position_interest|
            if candidate_position_interest.candidate_id == user.id
                true
            elsif user.hiring_team_id && candidate_position_interest.hiring_team&.id == user.hiring_team_id
                user.hiring_team&.using_pay_per_post_hiring_plan? ||
                user.hiring_application&.status == 'accepted' && user.hiring_team&.should_anonymize_interest?(candidate_position_interest) == false
            end
        end

        can :index_candidate_position_interests, Object do |params|
            allowed = true
            filters = params[:filters]

            is_candidate = filters[:candidate_id].present? && filters[:candidate_id] == user.id
            is_hiring_manager_or_teammate = (filters[:hiring_manager_id].present? && filters[:hiring_manager_id] == user.id) ||
                (filters[:hiring_team_id].present? && filters[:hiring_team_id] == user.hiring_team_id)

            filter_out_hiring_manager_status = !!params[:except]&.include?("hiring_manager_status")
            filter_out_admin_status = !!params[:except]&.include?("admin_status")

            if !is_candidate && !is_hiring_manager_or_teammate
                allowed = false
            end

            # Do not allow candidates to see the hiring_manager_status nor admin_status
            if !(filter_out_hiring_manager_status || filter_out_admin_status) && !is_hiring_manager_or_teammate
                allowed = false
            end

            if is_hiring_manager_or_teammate && (filters[:candidate_status] != 'accepted' || filters[:hiring_manager_status_not] != 'hidden')
                allowed = false
            end

            allowed
        end

        can :index_cohorts, Object do |params|
            allowed = true
            filters = params[:filters] || {}

            if !(filters[:promoted] && filters[:published])
                allowed = false
            end

            if params[:fields]&.include?("ADMIN_FIELDS")
                allowed = false
            end

            if params[:get_cohort_promotions]
                allowed = false
            end

            if params[:get_stripe_metadata]
                allowed = false
            end

            allowed
        end

        can :destroy, CohortApplication do |cohort_application|
            cohort_application.published_cohort_or_cohort.user_can_destroy_application?(cohort_application.status)
        end

        can :show_deferral_link, Object do |deferral_link|
            deferral_link.user_id == user.id
        end

        # We had an issue where an admin accidentally deferred a student into a cohort
        # by updating their deferral link from the UI, so we do some extra configuration
        # further down this ability file to make sure that admins can't update deferral
        # links that don't belong to them.
        can :update_deferral_link, Object do |params, deferral_link|
            params = params.unsafe_with_indifferent_access
            meta = params['meta'] || {}

            # Only allow a learner to use this endpoint if the link is for them, they
            # are providing a valid cohort to defer to, and it's currently active
            deferral_link.user_id == user.id &&
                deferral_link.upcoming_cohorts.pluck(:id).include?(meta['to_cohort_id']) &&
                deferral_link.active?
        end

        ######################################
        ## Interviewers
        ######################################

        # We give interviewers access to (most) read functionality in the Users and Applicants
        # admin screens. One exception is the Transactions tab that shows Stripe payments.
        if user.has_role?(:interviewer)
            can :read, User
            can :show, ActivityTimeline::UserTimeline
            can :index_cohorts, Object
            can :show_career_profile, Object
            can :index_hiring_relationships, Object
        end

        ######################################
        ## Editors
        ######################################

        if user.has_role?(:editor)
            can :index, Lesson
            can :crud, [
                Lesson,
                Lesson::StreamProgress, # allows editors to clear progress
                EntityMetadata
            ]
            can :index, ContentTopic
            can :view_unpublished, [Lesson, Lesson::Stream]
            can :view_specific_version, Lesson

            # By default all editors can create lessons, but now its possible for a user to
            # have the editor role but be blocked from lesson creation. Instead of modeling this
            # as can_create, its more space efficient to blacklist when needed as cannot_create
            if user.has_role? :cannot_create
                cannot :create, Lesson
                cannot :create, EntityMetadata
            end

        end

        ######################################
        ## Super Editors
        ######################################

        if user.has_role? :super_editor
            can :crud, [ # only crud because they can't publish
                Lesson,
                Lesson::Stream,
                Playlist,
                Lesson::StreamProgress,
                EntityMetadata,
                StudentDashboard
            ]
            can :read, [
                Cohort,
                Institution,
                :group,
                ContentTopic
            ]
            can :index_cohorts, Object # super_editors can index Cohorts like admins
            can :admin_without_delete, User
            can :view_unpublished, [Lesson, Lesson::Stream]
            can :view_specific_version, Lesson
            can :read_filter_options, Institution
            can :read_admin_filter_options, NilClass
            can :create_report_with_params, Object
        end

        ######################################
        ## Admins
        ######################################

        if user.has_role? :admin
            can :manage, :all

            # We had an issue where an admin was able to defer a student into a cohort by accident
            # because admins can manage all objects. Since abilities defined further down the file
            # will override previously defined abilities, we need to put this constraint on admins
            # AFTER we've specified that they can manage all objects. Refer to the following page in
            # the CanCan docs to understand why we need to use `cannot` here and why it doesn't work
            # to just put the `can` rule for the `update_deferral_link` abilities here instead:
            # https://github.com/CanCanCommunity/cancancan/wiki/Ability-Precedence.
            cannot :update_deferral_link, Object do |params, deferral_link|
                user.id != deferral_link.user_id
            end
        end


        ######################################
        ## Authenticated Users
        ######################################
        can :write, LessonProgress
        can :write, User

        # Billable
        can :create, Subscription

        def can_edit_subscription(user, params)
            if params['hiring_team_id']
                is_user_hiring_team = user.hiring_team_id == params['hiring_team_id']
                hiring_team = user.hiring_team if is_user_hiring_team
                is_owner = hiring_team&.owner_id == user.id
            end

            # users can update subscriptions they own
            if params['user_id'] == user.id
                true

            # if the hiring_plan is set to pay-per-post
            elsif hiring_team&.hiring_plan == HiringTeam::HIRING_PLAN_PAY_PER_POST

                # any member of the team can create a pay-per-post subscription
                HiringTeam.is_pay_per_post_plan?(params['stripe_plan_id']) ||

                # owners can convert pay-per-post teams to an unlimited subscription
                is_owner && HiringTeam.is_unlimited_with_sourcing_plan?(params['stripe_plan_id'])

            # after selecting an unlimited plan owners can sign up for an unlimited subscription
            # if there isn't a subscription already
            elsif hiring_team&.hiring_plan == HiringTeam::HIRING_PLAN_UNLIMITED_WITH_SOURCING
                is_owner && hiring_team.primary_subscription.nil?

            # owners on legacy plans can switch subscriptions back and forth between plans
            elsif hiring_team&.hiring_plan == HiringTeam::HIRING_PLAN_LEGACY
                is_owner

            # hiring team members can create pay-per-post subscriptions at the same time they make
            # the team's first position, which means the hiring_plan will be nil
            elsif hiring_team&.hiring_plan.nil?
                HiringTeam.is_pay_per_post_plan?(params['stripe_plan_id'])

            else
                false
            end
        end

        can :create_subscription, Object do |params|
            can_edit_subscription(user, params.stringify_keys)
        end

        can :destroy, Subscription do |subscription|

            allowed = true

            # only hiring team subscriptions can be deleted,
            hiring_team = subscription.hiring_team
            if !hiring_team
                allowed = false
            end

            # only the team's owner can delete a subscription
            if hiring_team&.owner != user
                allowed = false
            end

            # only primary subscriptions can be deleted (pay-per-post subscriptions are
            # deleted through the open_positions controller)
            if hiring_team&.primary_subscription != subscription
                allowed = false
            end

            allowed
        end

        can :modify_payment_details_with_params, Object do |params|
            owner_id = params[:owner_id]
            owner = User.find_by_id(owner_id) || HiringTeam.find_by_id(params[:owner_id])

            allowed = true
            if owner.nil? ||
                owner.is_a?(User) && owner.id != user.id ||
                owner.is_a?(HiringTeam) && owner.owner_id != user.id
                allowed = false
            end

            allowed
        end

        # Asset APIs
        can :create, S3IdentificationAsset, :user_id => user.id
        can :destroy, S3IdentificationAsset, :user_id => user.id
        can :create, S3EnglishLanguageProficiencyDocument, :user_id => user.id
        can :destroy, S3EnglishLanguageProficiencyDocument, :user_id => user.id
        can :upload_avatar, User
        can :upload_resume, User # FIXME: Should move to CareerProfile
        can :show_transcript_data, User

        can :index, Cohort # see index_cohorts for additional rules
        can :index, Playlist # see additional rules in the controller endpoint
    end

end
