# == Schema Information
#
# Table name: s3_identification_assets
#
#  id                :uuid             not null, primary key
#  created_at        :datetime
#  updated_at        :datetime
#  file_file_name    :string
#  file_content_type :string
#  file_file_size    :integer
#  file_updated_at   :datetime
#  file_fingerprint  :string
#  user_id           :uuid
#  persisted_path    :string
#

class S3IdentificationAsset < ActiveRecord::Base

    include PrivateDocumentMixin

    belongs_to :user

    default_scope -> {order(:created_at)}

    after_create do
        self.user&.identify
    end

    after_destroy do
        self.user&.identify
    end

    validates_attachment_content_type :file, :content_type => %w(image/jpeg image/jpg image/pjpeg image/pipeg image/png application/pdf)
    validates_attachment_size :file, less_than: 10.megabytes

    def as_json(options = {})
        hash = {
            :id => self.id,
            :file_file_name => self.file_file_name,
            :file_updated_at => self.file_updated_at,
            :file_content_type => self.file_content_type,
            :file_file_size => self.file_file_size
        }

        return hash
    end
end
