# == Schema Information
#
# Table name: cohort_promotions
#
#  id         :uuid             not null, primary key
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  cohort_id  :uuid             not null
#

class CohortPromotion < ApplicationRecord

    belongs_to :cohort, :primary_key => 'id', :foreign_key => 'cohort_id'

    after_save :refresh_materialized_views
    validates_truth_of :cohort_supports_manual_promotion?

    def as_json(options = {})
        {
            'id' => id,
            'cohort_id' => cohort_id,
            'program_type' => cohort && cohort.program_type
        }
    end

    def refresh_materialized_views
        RefreshMaterializedContentViews.refresh
    end

    def cohort_supports_manual_promotion?
        cohort.supports_manual_promotion?
    end

end
