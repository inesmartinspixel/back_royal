# == Schema Information
#
# Table name: roles
#
#  id            :uuid             not null, primary key
#  name          :string(255)
#  resource_id   :uuid
#  resource_type :string(255)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class Role < ApplicationRecord

    default_scope -> { order(:created_at) }

    has_and_belongs_to_many :users, join_table: :users_roles
    belongs_to :resource, polymorphic: true, optional: true

    scopify

    def self.as_json(roles, opts = {})
        roles.map { |role| role.as_json(opts) }
    end

end
