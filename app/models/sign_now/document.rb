class SignNow::Document

    attr_accessor :id, :name

    def self.create(template_id:)
        response = SignNow.post("/template/#{template_id}/copy").to_h
        self.archive(response['id'])
        self.new(response)
    end

    def self.archive(document_id)
        SignNow.post("/document/#{document_id}/move",
            body: {
                # https://help.signnow.com/reference#move-a-document indicates `folder_id` isn't required, but it is
                folder_id: ENV['SIGN_NOW_ARCHIVE_FOLDER']
            }.to_json
        )
    end

    def initialize(attrs = {})
        attrs = attrs.with_indifferent_access
        self.id = attrs['id']
        self.name = attrs['name']
    end

    def get_attributes(document_id)
        SignNow.get("/document/#{document_id}")
    end

    # this is not tested, since it seems like we would basically be mocking everything out
    def download(&block)
        Dir.mktmpdir do |tmpdir|
            filepath = Rails.root.join(tmpdir, "#{id}.pdf")
            File.open(filepath, "w") do |file|
                file.binmode
                SignNow.get("/document/#{id}/download?type=collapsed", stream_body: true) do |fragment|
                    file.write(fragment)
                end
            end

            File.open(filepath, 'r') do |file|
                yield(file)
            end

        end
    end

    def generate_signing_token
        credentials = SignNow.signer_credentials
        SignNow.generate_access_token(
            credentials.email,
            credentials.password,
            "signer_limited_scope_token document/#{id}"
        )
    end

    def generate_signing_link
        ensure_invite
        response = generate_signing_token
        expires_in = response["expires_in"]
        access_token = response["access_token"]
        OpenStruct.new({
            url: "#{SignNow.root_signing_link_url}/dispatch?route=fieldinvite&document_id=#{id}&access_token=#{access_token}&redirect_uri=https%3a%2f%2fsignnow.com%2fdocument-saved-successfully&theme=neutral&disable_email=true&mobileweb=mobileweb_only",
            expiry: Time.now + expires_in
        })
    end

    def destroy
        SignNow.delete("/document/#{id}")
    end


    def update_smart_fields(attrs)
        attrs_as_array = []
        attrs.each do |key, value|
            attrs_as_array << {key => value}
        end
        SignNow.post(
            "/document/#{id}/integration/object/smartfields",
            body: {
                data: attrs_as_array,
                client_timestamp: Time.now.to_timestamp
            }.to_json,
            headers: {
                "Content-Type": "application/json"
            }
        )
    end

    def ensure_invite

        SignNow.post(
            "/document/#{id}/invite?email=disable",
            body: {
                to: [{
                    email: SignNow.signer_credentials.email,
                    role: 'Student', # this is configured on the template in signnow.  Hardcoding here for now,
                    order: 1,
                    role_id: ""
                }],
                from: SignNow.sender_credentials.email,
                cc:[],
                subject:"",
                message:""
            }.to_json
        )

        true
    rescue SignNow::ApiRequestError => err
        # if we already have one, that's fine
        raise unless err.message == 'Could not create duplicate field invite'

        true
    end

end