module DbLinker

    # I didn't feel like coding this, but here is how to setup
    # a dblink from your local to prod:

    # CREATE EXTENSION IF NOT EXISTS postgres_fdw;
    # CREATE EXTENSION IF NOT EXISTS dblink;
    # DROP SERVER IF EXISTS back_royal_production CASCADE;
    # CREATE SERVER back_royal_production
    #     FOREIGN DATA WRAPPER postgres_fdw
    #     OPTIONS (
    #         host 'localhost'
    #         , port '54323'
    #         , dbname 'back_royal'
    #     );

    # CREATE USER MAPPING FOR #{username}
    #     SERVER back_royal_production
    #     OPTIONS (user 'readonly', password '#{password}');
    # grant usage on foreign server back_royal_production to #{username};

    def self.setup_red_royal_dblink
        return if @red_royal_dblink_already_setup
        setup_dblink('red_royal')
        @red_royal_dblink_already_setup = true
    end

    def self.setup_dblink(server_name, usernames = nil, confirm = false)
        if usernames.nil?
            # on local connections, the username is not in the config, so
            # we have to pull it from the db
            rds_username = ActiveRecord::Base.connection_config[:username] || ActiveRecord::Base.connection.execute('select user as u').to_a[0]['u']
            usernames = [rds_username]
        end
        redshift_connection_info = RedshiftEvent.connection_config

        if confirm
            base_connection_info = ActiveRecord::Base.connection_config
            puts ""
            puts ""
            puts ""
            puts "Setup dblink named '#{server_name}' from #{base_connection_info[:host]}/#{usernames.join(',')} to #{redshift_connection_info[:host]}/#{redshift_connection_info[:username]}?"
            puts "(Y/n)"
            response = STDIN.gets.chomp
            if response != 'Y'
                return
            end
        end


        ensure_extensions
        drop_dblink(server_name)

        ActiveRecord::Base.connection.execute(%Q~
            CREATE SERVER #{server_name}
                FOREIGN DATA WRAPPER postgres_fdw
                OPTIONS (
                    host '#{redshift_connection_info[:host]}'
                    , port '#{redshift_connection_info[:port]}'
                    , dbname '#{redshift_connection_info[:database]}'
                );
        ~)

        usernames.each do |username|
            ActiveRecord::Base.connection.execute(%Q~
                CREATE USER MAPPING FOR \"#{username}\"
                    SERVER #{server_name}
                    OPTIONS (user '#{redshift_connection_info[:username]}', password '#{redshift_connection_info[:password]}');
                grant usage on foreign server #{server_name} to \"#{username}\" ;
            ~)
        end

    end

    def self.drop_dblink(server_name, confirm=false)

        if confirm
            base_connection_info = ActiveRecord::Base.connection_config
            puts ""
            puts ""
            puts ""
            puts "Drop dblink named '#{server_name}' from #{base_connection_info[:host]}?"
            puts "(Y/n)"
            response = STDIN.gets.chomp
            if response != 'Y'
                return
            end
        end

        ensure_extensions
        ActiveRecord::Base.connection.execute(%Q~
            DROP SERVER IF EXISTS #{server_name} CASCADE;
        ~)
    end

    def self.ensure_extensions
        unless @extensions_ensured
            ActiveRecord::Base.connection.execute(%Q~
                CREATE EXTENSION IF NOT EXISTS postgres_fdw;
                CREATE EXTENSION IF NOT EXISTS dblink;
            ~)
            @extensions_ensured = true
        end
    end
end