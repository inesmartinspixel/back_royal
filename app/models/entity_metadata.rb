# == Schema Information
#
# Table name: entity_metadata
#
#  id             :uuid             not null, primary key
#  created_at     :datetime
#  updated_at     :datetime
#  title          :string
#  description    :string
#  canonical_url  :text
#  image_id       :uuid
#  tweet_template :text
#

class EntityMetadata < ApplicationRecord
    self.table_name = "entity_metadata"

    belongs_to :image, :class_name => 'S3Asset', optional: true

    validates_presence_of :title

    def self.update_from_hash!(hash, meta = {})
        hash = hash.unsafe_with_indifferent_access
        meta = (meta || {}).unsafe_with_indifferent_access
        instance = find_by_id(hash[:id])
        if instance.nil?
            raise ActiveRecord::RecordNotFound.new("Couldn't find #{self.name} with id=#{hash[:id]}")
        end

        instance.merge_hash(hash, meta)
        instance.save!
        instance

    end

    def merge_hash(hash, meta = {})
        hash = hash.clone.with_indifferent_access
        %w(title description canonical_url tweet_template).each do |key|
            self[key] = hash[key] if hash.key?(key)
        end

        if hash.key?('image') && !hash['image'].nil? && hash['image'].key?('id') && !hash['image']['id'].nil?
            self.image = S3Asset.find(hash['image']['id'])
        else
            self.image = nil
        end

    end

    def as_json(options = {})

        json = {}
        json['id'] = self.id
        json['title'] = self.title
        json['description'] = self.description
        json['canonical_url'] = self.canonical_url
        json['tweet_template'] = self.tweet_template
        json['image'] = self.image.as_json
        json
    end

    def get_image_asset(file)
        S3Asset.new({
            :file => file,
            :directory => "images/",

            # see also: https://github.com/thoughtbot/paperclip/wiki/Thumbnail-Generation
            # see also: image_model.js
            :styles => {}.to_json
        })
    end

end
