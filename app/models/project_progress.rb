# == Schema Information
#
# Table name: project_progress
#
#  id                     :uuid             not null, primary key
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  user_id                :uuid             not null
#  requirement_identifier :text             not null
#  score                  :float
#  waived                 :boolean          default(FALSE)
#  marked_as_passed       :boolean          default(FALSE)
#  status                 :text             default("unsubmitted"), not null
#  id_verified            :boolean
#

class ProjectProgress < ApplicationRecord

    self.table_name = 'project_progress'
    belongs_to :user

    # FIXME: https://trello.com/c/T0HsMIUE
    self.ignored_columns = %w(score_history)

    validates :score, numericality: { allow_nil: true, greater_than_or_equal_to: 0, less_than_or_equal_to: 5 }
    validate :validate_score_set_in_only_one_way

    def self.valid_statuses
        return %w(unsubmitted submitted delayed_grading will_not_submit plagiarized plagiarized_resubmitted plagiarized_resubmission_ok plagiarized_resubmission_not_ok
            live_presentation_scheduled live_presentation_complete video_presentation_submitted)
    end
    validates :status, inclusion: { in: self.valid_statuses, message: "%{value} is not a status" }

    after_save :update_application_final_score, if: :should_update_application_final_score?
    after_destroy  :update_application_final_score

    def self.create_or_update!(user, editor, hash)
        hash = hash.unsafe_with_indifferent_access
        progress_record = user.project_progresses.detect { |p| p.requirement_identifier == hash['requirement_identifier']}

        updatable_attrs = [:score, :waived, :marked_as_passed, :status, :id_verified]
        updatable_values_set_in_attrs = hash.slice(*updatable_attrs).values.any?

        # The controller can send dummy progresses that were created in the UI
        # through to here. We need to explicitly check if any of the updatable_attrs
        # have values before actually creating  a record.
        if !progress_record && !updatable_values_set_in_attrs
            return nil

        # If the record does not yet exist, instantiate it
        elsif !progress_record
            progress_record = ProjectProgress.new(
                user_id: user.id,
                requirement_identifier: hash['requirement_identifier']
            )
        end

        progress_record.merge_hash(hash)
        progress_record.user = user if user
        EditorTracking.transaction(editor) do
            progress_record.save!
        end
        progress_record
    end

    def merge_hash(hash)
        hash = hash.unsafe_with_indifferent_access

        keys = %w(score waived marked_as_passed status id_verified)

        keys.each do |key|
            if hash.key?(key)
                val =  hash[key]
                val = nil if val.is_a?(String) && val.blank?
                self[key] = val
            end
        end

        self
    end

    def as_json
        attributes.slice('id', 'user_id', 'requirement_identifier', 'score', 'waived', 'marked_as_passed', 'status', 'id_verified').merge({
            created_at: created_at.to_timestamp,
            updated_at: updated_at.to_timestamp
        })
    end

    def should_update_application_final_score?
        return saved_change_to_score? || saved_change_to_waived? || saved_change_to_marked_as_passed? || saved_change_to_id_verified?
    end

    def update_application_final_score
        # We reload the project_progresses here because there's a change we just saved
        # a record and added it to the user's `project_progresses` relation. We want to
        # make sure final_score_helper has up-to-date progresses.
        user.project_progresses.reload
        user.application_for_relevant_cohort&.set_final_score&.save!
    end

    def validate_score_set_in_only_one_way
        if [waived, marked_as_passed, score].count(&:present?) > 1
            errors.add(:only_one, "of waived, marked_as_passed, and score can be set")
        end
    end

    # duplicated in project_progress.js
    def unpassed_for_project?(project, cohort)
        if score.present?
            # If score is present, a ProjectProgress should be considered unpassed
            # if the score is less than the corresponding project's passing_score.
            # Presentation projects also require id_verified, so records with a
            # passing score but !id_verified should be considered unpassed.
            return (project.project_type == LearnerProject::PRESENTATION_PROJECT_TYPE && !id_verified) ||
                score < project.passing_score(cohort)
        end
        return !waived && !marked_as_passed
    end

    def gradable?
        return score.present? || waived? || marked_as_passed?
    end

end
