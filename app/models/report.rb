class Report

    # FIXME: eventually this will be an active record class and
    # we will actually persist these
    def self.create_from_hash!(params)
        report_type = params['report_type']
        klass = self.get_klass_from_report_type(report_type)
        klass.new(params)
    end

    def self.get_klass_from_report_type(report_type)
        klass = self.const_get(report_type.to_sym)
    end

    def self.available_joins
        @available_joins ||= {
            'users' =>  Proc.new { |query, locale|
                            query.joins(:user)
                        },

            'groups' =>  Proc.new { |query, locale|
                            query.joins('LEFT JOIN access_groups_users ON users.id = access_groups_users.user_id')
                                .joins('LEFT JOIN access_groups_institutions ON institutions.id = access_groups_institutions.institution_id')
                                .joins('LEFT JOIN access_groups ON (access_groups.id = access_groups_users.access_group_id OR access_groups.id = access_groups_institutions.access_group_id)')
                        },
            'roles' =>  Proc.new { |query, locale|
                            query.joins('INNER JOIN users_roles ON users_roles.user_id = users.id')
                                .joins('INNER JOIN roles ON roles.id = users_roles.role_id')
                                .where('roles.resource_id IS NULL')
                        },

            # Overridden in UsersReport:available_joins
            'institutions' => Proc.new { |query, locale|
                query.left_outer_joins(user: :institutions)
            },

            # Overridden in UsersReport:available_joins
            'cohorts' => Proc.new { |query, locale|
                query.left_outer_joins(user: {cohort_applications: :cohort})
            },

            'cohort_applications' => Proc.new { |query, locale|
                            query.joins('LEFT JOIN cohort_applications ON users.id = cohort_applications.user_id')
                        },

            'published_lesson_titles' => Proc.new { |query, locale|
                            query.joins("
                                JOIN published_content_titles as published_lesson_titles
                                    ON lesson_locale_pack_id = published_lesson_titles.locale_pack_id
                                    AND published_lesson_titles.target_locale = '#{locale}'
                                ")
                        },

            'published_stream_titles' => Proc.new { |query, locale|
                            query.joins("
                                JOIN published_content_titles as published_stream_titles
                                    ON lesson_stream_locale_pack_id = published_stream_titles.locale_pack_id
                                    AND published_stream_titles.target_locale = '#{locale}'
                                ")
                        },

            'internal_lesson_titles' => Proc.new { |query, locale|
                            query.joins("
                                JOIN  internal_content_titles as internal_lesson_titles
                                    ON lesson_id = internal_lesson_titles.id
                                ")
                        }
        }
    end

    def self.filter_configs
        {
            'SignUpCodeFilter' => {
                'type' => 'list',
                'column' => 'users.sign_up_code',
                'joins' => ['users'],
                'blank_value' => I18n.t(:filter_empty_sign_up_codes)
            },
            'EmailNameFilter' => {
                'type' => 'text',
                'columns' => ['users.email', 'users.name', 'users.id::text', 'users.phone'],
                'joins' => ['users'],
                'blank_value' => nil
            },
            "RoleFilter" => {
                'type' => 'list',
                'column' => 'roles.name',
                'joins' => ['users', 'roles'],
                'blank_value' => I18n.t(:filter_empty_roles)
            },
             "GroupFilter" => {
                'type' => 'list',
                'column' => 'access_groups.name',
                'joins' => ['users', 'institutions', 'groups'],
                'blank_value' => I18n.t(:filter_empty_groups)
            },
            "InstitutionFilter" => {
                'type' => 'list',
                'column' => 'institutions.id',
                'joins' => ['users', 'institutions'],
                'blank_value' => I18n.t(:filter_empty_institutions)
            },
            "CohortFilter" => {
                'type' => 'list',
                'column' => 'cohorts.id',
                'joins' => ['users', 'cohorts'],
                'blank_value' => I18n.t(:filter_empty_cohorts)
            },
            "CohortStatusFilter" => {
                'type' => 'list',
                'column' => ['cohort_applications.status', 'cohort_applications.graduation_status'],
                'joins' => ['users', 'cohort_applications'],
                'blank_value' => I18n.t(:filter_empty_cohort_status)
            },
            "UserIdFilter" => {
                'type' => 'list',
                'joins' => ['users'],
                'column' => 'users.id'
            }
        }
    end

    def self.group_by_configs
        {
            'sign_up_code' => {
                'column' => 'users.sign_up_code',
                'joins' => ['users'],
                'blank_value' => I18n.t(:filter_empty_sign_up_codes)
            },
            "institution_name" => {
                'column' => 'institutions.name',
                'joins' => ['institutions', 'users'],
                'blank_value' => I18n.t(:filter_empty_institutions)
            },
            "cohort_name" => {
                'column' => 'cohorts.name',
                'joins' => ['cohorts', 'users'],
                'blank_value' => I18n.t(:filter_empty_cohorts)
            },
            "cohort_status" => {
                'column' => ['cohort_applications.status', 'cohort_applications.graduation_status'],
                'joins' => ['cohort_applications', 'users'],
                'blank_value' => I18n.t(:filter_empty_cohort_status)
            },
            "role_name" => {
                'column' => 'roles.name',
                'joins' => ['users', 'roles'],
                'blank_value' => I18n.t(:filter_empty_roles)
            },
            "group_name" => {
                'column' => 'access_groups.name',
                'joins' => ['users', 'institutions', 'groups'],
                'blank_value' => I18n.t(:filter_empty_groups)
            }
        }
    end

    # extracted getter for easier extending/overriding in sub-classes
    def self.internal_get_filter_options_for_institution(institution, locale)

        users_in_institution = User
                            .joins(:institutions)
                            .where(:institutions => {:id => institution.id})

        [
            {
                filter_type: 'GroupFilter',
                options: users_in_institution
                            .joins(:access_groups)
                            .distinct.reorder(nil)
                            .pluck('access_groups.name')
                            .map { |group_name|
                                {
                                    value: group_name,
                                    text: group_name
                                }
                            } + [{value: nil, text: self.filter_configs['GroupFilter']['blank_value']}]
            }
        ]
    end

    # extracted getter for easier extending/overriding in sub-classes
    def self.internal_get_filter_options(locale)
        [
            {
                filter_type: 'SignUpCodeFilter',
                options: User.reorder('sign_up_code')
                    .distinct
                    .pluck('sign_up_code').map { |sign_up_code|
                        {
                            value: sign_up_code,
                            text: sign_up_code || self.filter_configs['SignUpCodeFilter']['blank_value']
                        }
                    }
            },

            {
                filter_type: 'RoleFilter',
                options: Role
                    .select('name')
                    .where('resource_id IS NULL and name != \'cannot_create\'')
                    .group('name', 'created_at')
                    .map { |role|
                        {
                            value: role.name,
                            text: role.name || self.filter_configs['RoleFilter']['blank_value']
                        }
                    }
            },

            {
                filter_type: 'GroupFilter',
                options: AccessGroup
                    .pluck('name')
                    .map { |group|
                        {
                            value: group,
                            text: group
                        }
                    } + [{value: nil, text: self.filter_configs['GroupFilter']['blank_value']}]
            },

            {
                filter_type: 'InstitutionFilter',
                options: Institution
                    .select('id', 'name').map { |inst|
                        {
                            value: inst.id,
                            text: inst.name
                        }
                    } + [{value: nil, text: self.filter_configs['InstitutionFilter']['blank_value']}],
            },

            {
                filter_type: 'CohortFilter',
                options: Cohort
                    .select('id', 'name').map { |cohort|
                        {
                            value: cohort.id,
                            text: cohort.name
                        }
                    } + [{value: nil, text: self.filter_configs['CohortFilter']['blank_value']}],
            },

            {
                filter_type: 'CohortStatusFilter',
                # we want to group together status and graduation_status together
                options: CohortApplication.find_by_sql("SELECT DISTINCT(status)
                        FROM cohort_applications
                        UNION SELECT DISTINCT(graduation_status)
                        FROM cohort_applications
                        ORDER BY status ASC")
                    .pluck(:status)
                    .map { |status|
                        {
                            value: status,
                            text: status
                        }
                    } + [{value: nil, text: self.filter_configs['CohortStatusFilter']['blank_value']}]
            }
        ]
    end

    def self.fix_null_filter_options(options)
        options.map do |entry|
            entry[:options].each do |option|

                # selectize does not like null values.  The allowEmptyOption
                # config value is supposed to allow this, but it does not seem
                # to work,  See time_series_data method below for where we convert it back
                option[:value] ||= '__NULL__'
            end
            entry
        end
    end

    def self.get_filter_options_for_institution(institution, locale)
        SafeCache.fetch("Report::#{self.to_s}::filter_options::#{institution.id}::#{locale}", expires_in: 1.hours) do
            self.fix_null_filter_options(
                self.internal_get_filter_options_for_institution(institution, locale)
            )
        end
    end

    def self.get_filter_options(locale)
        SafeCache.fetch("Report::#{self.to_s}::filter_options::#{locale}", expires_in: 1.hours) do
            self.fix_null_filter_options(
                self.internal_get_filter_options(locale)
            )
        end
    end


    def initialize(config)
        # Some configs are stored on the server and determined
        # based on the report_type (just basic denormalization).
        # Get those and merge them
        # with all the other configs passed up from the client.
        report_type_config = self.class.const_get(:REPORT_CONFIG)
        @config = config.merge(report_type_config)
    end

    def filter(query, join_identifiers)
        @config['filters'].each do |filter|

            # list filters have a column and a finite list of allowed
            # values for that column
            filter_config = self.class.filter_configs[filter['filter_type']]

            # FIXME: be smarter here. issues caused when switching report types
            # in client and copying filters over
            next if filter_config.nil?

            if filter_config['field']
                filter_config = filter_config.clone
                field_config = self.class.field_configs.fetch(filter_config['field'])
                filter_config.merge!(field_config)
            end

            if filter_config['type'] == 'list'

                # see above in get_filter_options for why null values are labeled '__NULL__'
                # see http://stackoverflow.com/questions/6362112/in-clause-with-null-or-is-null for
                # why we can't just put the null into the IN() list

                # filter out the null value and, if there is no,
                # set include_null to false
                include_null = false
                values = filter['value'].reject do |value|
                    if value == '__NULL__'
                        include_null = true
                    end
                end
                col = filter_config['column']
                array_column = filter_config['array_column']
                parts = []
                additional_values = false

                if array_column
                    vals = values.map{|v| "'#{v}'" }.join(', ')
                    parts << "#{col} @> ARRAY[#{vals}]::uuid[]" if values.any?
                    parts << "#{col} = ARRAY[]::uuid[] OR #{col} IS NULL" if include_null
                elsif col.is_a?(Array)
                    col.each do |col|
                        if values.any?
                            additional_values = values
                            parts << "#{col} IN (?)"
                        elsif include_null
                            parts << "#{col} IS NULL"
                        end
                    end
                else
                    parts << "#{col} IN (?)"  if values.any?
                    parts << "#{col} IS NULL " if include_null
                end

                query = query.where([parts.join(" OR "), values]) if !additional_values
                query = query.where([parts.join(" OR "), values, additional_values]) if additional_values

            elsif filter_config['type'] == 'text'

                value = filter['value']
                if value.nil?
                    next
                else
                    cols = filter_config['columns']
                    if (cols.nil?)
                        cols = [filter_config['column']]
                    end

                    parts = []
                    values = []
                    cols.each do |col|
                        parts << "#{col} ILIKE ?"
                        values << "%#{value}%"
                    end
                    query = query.where([parts.join(" OR "), *values])
                end
            elsif filter_config['type'] == 'bool'
                value = filter['value']
                if value.nil?
                    next
                else
                    cols = filter_config['columns']
                    if (cols.nil?)
                        cols = [filter_config['column']]
                    end

                    parts = []
                    values = []
                    cols.each do |col|
                        parts << "#{col} = ?"
                        values << "#{value}"
                    end

                    if filter_config['aggregate']
                        query = query.having([parts.join(" OR "), *values])
                    else
                        query = query.where([parts.join(" OR "), *values])
                    end
                end
            else
                raise "Unknown filter type #{filter_config['type'].inspect}"
            end

            join_identifiers += (filter_config['joins'] || [])

        end

        [query, join_identifiers]
    end

    def join(query, join_identifiers, locale)
        join_identifiers.each do |join_identifier|
            join = self.class.available_joins[join_identifier]
            if join.nil?
                raise RuntimeError.new("Unsupported join #{join_identifier.inspect}")
            end

            # do not join, for example, users against users.  The way this is
            # implemented is not really "correct", because we shouldn't assume
            # that the join_identifier is the same as the table name.  But for now
            # this is only an issue with the users report and this works fine
            next if join_identifier == self.class::REPORT_CONFIG['klass'].table_name

            query = join.call(query, locale)
        end

        query
    end

    # FIXME: if this exists at all, it should probably just be for
    # time series reports.  Tabular reports do not need to treat
    # the time filter specially. It should just be another filter. Time series
    # reports probably need one special time filter that defines the y-axis,
    # but I'm not sure the server really needs to handle it special.  Maybe
    # just the client in  drawing the graph.  See https://trello.com/c/rw3sYcGO/374-feat-make-timefilter-class-and-use-it-to-allow-filtering-by-registered-at-on-acitivity-report
    def filter_by_time(query)

        if @config['date_range']['type'] != 'all'
            query = query.where(
                        "#{@config['time_column']} between ? and ?",
                        start_time.utc,
                        finish_time.utc)
        end

        # FIXME: if we decide to do reports for anonymous users in the future then
        # we will need to selectively do this based on both the registered_column being present and
        # another config value stating if the report should include anonymous users or not. For now
        # just filter out all anonymous users based on registered_at
        if !@config['registered_column'].nil?
            query = query.where(
                "#{@config['registered_column']} is not null"
            )
        end

        query
    end

    # see FIXME on filter_by_time
    def start_time
        date_range = @config['date_range']

        if date_range['type'] == 'last'
            unit = date_range['unit']
            raise ArgumentError.new("Invalid unit #{unit}") unless ['day', 'week', 'month'].include?(unit)
            meth = :"#{unit}s"
            Time.now.utc - date_range['value'].send(meth)
        else
            Time.utc(*date_range['start_day'].split('-').map(&:to_i))
        end
    end

    # see FIXME on filter_by_time
    def finish_time
        date_range = @config['date_range']

        if date_range['type'] == 'last' || date_range['type'] == 'since'
            Time.now.utc
        else
            Time.utc(*date_range['finish_day'].split('-').map(&:to_i)).end_of_day
        end
    end


end