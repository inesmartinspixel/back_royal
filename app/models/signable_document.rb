# == Schema Information
#
# Table name: signable_documents
#
#  id                           :uuid             not null, primary key
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#  document_type                :text             not null
#  user_id                      :uuid             not null
#  sign_now_document_id         :text
#  sign_now_signing_link        :text
#  sign_now_signing_link_expiry :datetime
#  signed_at                    :datetime
#  metadata                     :json             not null
#  file_file_name               :string
#  file_content_type            :string
#  file_file_size               :bigint
#  file_updated_at              :datetime
#  file_fingerprint             :string
#  persisted_path               :string
#

class SignableDocument < ApplicationRecord

    include PrivateDocumentMixin

    TYPE_ENROLLMENT_AGREEMENT = 'enrollment_agreement'

    # see also: QuanticRoutes and below signing_link generation
    ENROLLMENT_AGREEMENT_SIGN_ROUTE = "/ea/sign"

    validates_attachment_content_type :file, :content_type => %w(
        application/pdf
        application/msword
        application/vnd.openxmlformats-officedocument.wordprocessingml.document
        image/jpg
        image/jpeg
        image/pjpeg
        image/pipeg
        image/png
        )


    belongs_to :s3_asset, :class_name => 'S3SignableDocumentAsset', :foreign_key => :s3_asset_id, :dependent => :destroy, optional: true
    belongs_to :user

    before_save :update_signed_at
    after_save :log_completed_event_if_necessary

    validates_inclusion_of :document_type, :in => [TYPE_ENROLLMENT_AGREEMENT]

    after_save :log_signing_link_generated_event_if_necessary

    def self.create_with_sign_now(document_type:, template_id:, user_id:, smart_field_values:, metadata: {})

        sign_now_document = SignNow::Document.create(template_id: template_id)
        sign_now_document.update_smart_fields(smart_field_values) unless smart_field_values.blank?
        signing_link = sign_now_document.generate_signing_link

        metadata['template_id'] = template_id

        # NOTE: In the unlikely scenario where the following call actually hits a dupe key error
        # and rescues, the signnow document we created above will be left in signnow without an
        # associated signable_document in our db.  This is really unlikely and it doesn't seem like
        # that would really cause any problems, so we're not doing anything about it.
        create_or_rescue_dupe_key_error!(
            document_type: document_type,
            user_id: user_id,
            sign_now_document_id: sign_now_document.id,
            sign_now_signing_link: signing_link.url,
            sign_now_signing_link_expiry: signing_link.expiry,
            metadata: metadata
        )
    end

    def self.create_or_rescue_dupe_key_error!(*args)
        begin
            create!(*args)
        rescue ActiveRecord::RecordNotUnique => err

            user_id, document_type, metadata = args[0][:user_id], args[0][:document_type], args[0][:metadata]

            # There is a unique index ensuring that there is only one enrollment agreement for a particular
            # user and program type.
            if args[0][:document_type] == TYPE_ENROLLMENT_AGREEMENT
                instance = where("user_id=? AND metadata->>'program_type'=? AND document_type=?", user_id, metadata[:program_type], TYPE_ENROLLMENT_AGREEMENT).first
            end

            if instance.nil?
                err2 = RuntimeError.new("No instance found while recovering from duplicate key error in SignableDocument")
                err2.raven_options = {
                    extra: {
                        document_type: document_type,
                        user_id: user_id,
                        original_exception: err.message
                    }
                }
                raise err2
            end

            instance
        end
    end

    def signing_link
        self.document_type == TYPE_ENROLLMENT_AGREEMENT && self.metadata['cohort_application_id'] ?
            "#{UrlHelper.domain_url}#{ENROLLMENT_AGREEMENT_SIGN_ROUTE}?id=#{self.metadata['cohort_application_id']}" :
            self.sign_now_signing_link
    end

    def log_info
        {
            document_type: self.document_type,
            signable_document_id: self.id,
            link: self.signing_link
        }.merge(self.metadata.symbolize_keys) # includes `follow_up_attempt`
    end

    def log_signing_link_generated_event_if_necessary
        if saved_change_to_sign_now_signing_link && self.sign_now_signing_link

            Event.create_server_event!(
                SecureRandom.uuid,
                user_id,
                'signable_document:sign_now_link_generated',
                log_info
            ).log_to_external_systems

        end
    end

    def download_completed_document_from_sign_now
        return if self.file_fingerprint
        RetriableTransaction.transaction do
            sign_now_document.download do |file|
                self.file = file
            end

            self.save!
        end
    end

    def check_and_queue_download_if_necessary
        return if self.file_fingerprint

        attributes = sign_now_document.get_attributes(sign_now_document_id)

        # user-supplied fields that are marked as required should be complete at this point
        return unless attributes['fields'].select{ |field| field['json_attributes']['required'] }.all? { |field| field['element_id'] }

        # field invite tracking
        # see also: https://github.com/signnow/SignNowRubySDK/blob/master/lib/signnow/document.rb#L63-L65
        return unless attributes['field_invites'].present? && attributes['field_invites'].all? { |f| f['status'] == 'fulfilled' }

        ProcessCompletedSignNowDocumentJob.ensure_job(sign_now_document_id)

    end

    def unsigned?
        signed_at.nil?
    end

    def is_enrollment_agreement?
        self.document_type == TYPE_ENROLLMENT_AGREEMENT
    end

    def sign_now_document
        @sign_now_document ||= SignNow::Document.new(id: sign_now_document_id)
    end

    def update_signed_at
        if signed_at.nil? && file_fingerprint
            self.signed_at = Time.now
        end
    end

    def log_completed_event_if_necessary
        if saved_change_to_signed_at? && saved_changes[:signed_at][0].nil?
            Event.create_server_event!(
                SecureRandom.uuid,
                user_id,
                'signable_document:complete',
                log_info
            ).log_to_external_systems
        end
    end

    def as_json(options = {})
        # FIXME: https://trello.com/c/TKsRSQuM - Remove sign_now_signing_link once clients have min version
        attributes.slice(*%w(id document_type user_id sign_now_signing_link metadata file_updated_at file_file_name file_content_type)).merge({
            'signing_link' => signing_link,
            'signed_at' => signed_at.to_timestamp
        })
    end

end
