module ContentItemVersionMixin

    extend ActiveSupport::Concern

    included do
        include VersionMixin

        foreign_key = {
            'Lesson' => :lesson_version_id,
            'Lesson::Stream' => :lesson_stream_version_id,
            'Playlist' => :playlist_version_id,
            'Cohort' => :cohort_version_id
        }[superclass.name]

        has_one :content_publisher, :foreign_key => foreign_key, :primary_key => :version_id, :autosave => false

    end

    module ClassMethods

        class ActiveRecord::Relation
            def published_versions
                eager_load(:content_publisher).joins(:content_publisher)
            end
        end

        def published_versions
            all.published_versions
        end
    end

end
