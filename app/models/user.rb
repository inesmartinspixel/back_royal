# == Schema Information
#
# Table name: users
#
#  id                                              :uuid             not null, primary key
#  email                                           :string(255)      default("")
#  encrypted_password                              :string(255)      default(""), not null
#  reset_password_token                            :string(255)
#  reset_password_sent_at                          :datetime
#  remember_created_at                             :datetime
#  sign_in_count                                   :integer          default(0)
#  current_sign_in_at                              :datetime
#  last_sign_in_at                                 :datetime
#  current_sign_in_ip                              :string(255)
#  last_sign_in_ip                                 :string(255)
#  created_at                                      :datetime         not null
#  updated_at                                      :datetime         not null
#  failed_attempts                                 :integer          default(0)
#  unlock_token                                    :string(255)
#  locked_at                                       :datetime
#  sign_up_code                                    :string(255)
#  reset_password_redirect_url                     :string(255)
#  provider                                        :string(255)
#  uid                                             :string(255)      default(""), not null
#  tokens                                          :json
#  has_seen_welcome                                :boolean          default(FALSE), not null
#  notify_email_newsletter                         :boolean          default(TRUE), not null
#  school                                          :text
#  provider_user_info                              :json
#  free_trial_started                              :boolean          default(FALSE), not null
#  confirmed_profile_info                          :boolean          default(TRUE), not null
#  pref_decimal_delim                              :string           default("."), not null
#  pref_locale                                     :string           default("en"), not null
#  active_playlist_locale_pack_id                  :uuid
#  avatar_url                                      :text
#  additional_details                              :json
#  mba_content_lockable                            :boolean          default(FALSE), not null
#  job_title                                       :text
#  phone                                           :text
#  country                                         :text
#  has_seen_accepted                               :boolean          default(FALSE), not null
#  name                                            :text
#  nickname                                        :text
#  phone_extension                                 :text
#  can_edit_career_profile                         :boolean          default(FALSE), not null
#  professional_organization_option_id             :uuid
#  pref_show_photos_names                          :boolean          default(TRUE), not null
#  identity_verified                               :boolean          default(FALSE), not null
#  pref_keyboard_shortcuts                         :boolean          default(FALSE), not null
#  sex                                             :text
#  ethnicity                                       :text
#  race                                            :text             default([]), not null, is an Array
#  how_did_you_hear_about_us                       :text
#  anything_else_to_tell_us                        :text
#  birthdate                                       :date
#  address_line_1                                  :text
#  address_line_2                                  :text
#  city                                            :text
#  state                                           :text
#  zip                                             :text
#  last_seen_at                                    :datetime
#  fallback_program_type                           :string
#  program_type_confirmed                          :boolean          default(FALSE), not null
#  invite_code                                     :string
#  notify_hiring_updates                           :boolean          default(TRUE), not null
#  notify_hiring_spotlights                        :boolean          default(TRUE), not null
#  notify_hiring_content                           :boolean          default(TRUE), not null
#  notify_hiring_candidate_activity                :boolean          default(TRUE), not null
#  hiring_team_id                                  :uuid
#  referred_by_id                                  :uuid
#  has_seen_featured_positions_page                :boolean          default(FALSE), not null
#  pref_one_click_reach_out                        :boolean          default(FALSE), not null
#  has_seen_careers_welcome                        :boolean          default(FALSE), not null
#  can_purchase_paid_certs                         :boolean          default(FALSE), not null
#  has_seen_hide_candidate_confirm                 :boolean          default(FALSE), not null
#  pref_positions_candidate_list                   :boolean          default(TRUE), not null
#  skip_apply                                      :boolean          default(FALSE), not null
#  has_seen_student_network                        :boolean          default(FALSE), not null
#  pref_student_network_privacy                    :text             default("full"), not null
#  notify_hiring_spotlights_business               :boolean          default(TRUE), not null
#  notify_hiring_spotlights_technical              :boolean          default(TRUE), not null
#  has_drafted_open_position                       :boolean          default(FALSE), not null
#  deactivated                                     :boolean          default(FALSE), not null
#  avatar_id                                       :uuid
#  has_seen_first_position_review_modal            :boolean          default(FALSE), not null
#  timezone                                        :text
#  pref_allow_push_notifications                   :boolean          default(TRUE), not null
#  english_language_proficiency_comments           :text
#  english_language_proficiency_documents_type     :text
#  english_language_proficiency_documents_approved :boolean          default(FALSE), not null
#  transcripts_verified                            :boolean          default(FALSE), not null
#  has_logged_in                                   :boolean          default(FALSE), not null
#  has_seen_mba_submit_popup                       :boolean          default(FALSE), not null
#  has_seen_short_answer_warning                   :boolean          default(FALSE), not null
#  recommended_positions_seen_ids                  :uuid             default([]), is an Array
#  allow_password_change                           :boolean          default(FALSE), not null
#  notify_candidate_positions                      :boolean
#  notify_candidate_positions_recommended          :text
#  has_seen_hiring_tour                            :boolean          default(FALSE), not null
#  academic_hold                                   :boolean          default(FALSE), not null
#  experiment_ids                                  :string           default([]), not null, is an Array
#  enable_front_royal_store                        :boolean          default(FALSE), not null
#  pref_sound_enabled                              :boolean          default(TRUE), not null
#  student_network_email                           :string(255)
#

class User < ApplicationRecord

    # this can be set to true in rake tasks that backfill lots of users
    cattr_accessor :use_mass_identify_priority

    # DeviseTokenAuth / Devise configuration (NOTE: This excludes "confirmable" from the configuration)
    devise :database_authenticatable, :recoverable,
        :trackable, :validatable, :registerable,
        :omniauthable #:confirmable
    include DeviseTokenAuth::Concerns::User

    include Subscription::SubscriptionConcern # we wrote this one
    include User::SubscriptionConcern # we wrote this one
    include Groupable
    acts_as_messageable # mailboxer gem

    attr_accessor :just_called_register_content_access

    attr_accessor :skip_hiring_team_owner_validation

    delegate :can?, :cannot?, :to => :ability

    # institution and cohort support
    # Note: We need to dependent destroy the applications first to avoid triggering
    # the institution_joins validation
    has_many :cohort_applications, -> { reorder('applied_at DESC') }, :class_name => 'CohortApplication', :dependent => :destroy, :validate => false

    has_many :institution_joins, class_name: 'InstitutionUserJoin', dependent: :destroy
    has_many :institutions, through: :institution_joins

    before_save :ensure_institution_state_for_fallback_program_type, if: Proc.new { self.fallback_program_type_changed? && self.fallback_program_type.present? }

    before_save :unset_timezone_if_present_and_invalid

    has_and_belongs_to_many :views_reports_for_institutions, :class_name => 'Institution', :join_table => :institutions_reports_viewers, :validate => false

    # favorites
    has_and_belongs_to_many :favorite_lesson_stream_locale_packs, :class_name => 'Lesson::Stream::LocalePack', :association_foreign_key => :locale_pack_id, :validate => false

    # Company
    belongs_to :professional_organization, class_name: 'ProfessionalOrganizationOption', foreign_key: :professional_organization_option_id, primary_key: :id, optional: true

    # playlists
    # this array includes all playlists in the active_playlist_locale_pack, regardless
    # of locale.  There is a method for active_playlist which includes only the
    # one for the active locale
    belongs_to :active_playlist_locale_pack, :class_name => 'Playlist::LocalePack', optional: true
    has_many :active_playlists, :through => :active_playlist_locale_pack, :source => :content_items, :class_name => 'Playlist'

    # progress
    has_many :lesson_progresses, :class_name => 'LessonProgress' # no dependent => :destroy (see delete_user_progresses)
    has_many :lesson_streams_progresses, :class_name => 'Lesson::StreamProgress' # no dependent => :destroy (see delete_user_progresses)
    has_many :project_progresses, :class_name => 'ProjectProgress' # no dependent => :destroy (see delete_user_progresses)
    has_many :cohort_user_progress_records

    belongs_to :hiring_team, optional: true
    has_many :hiring_team_members, -> { where.not(hiring_team_id: nil) }, :class_name => 'User', :foreign_key => 'hiring_team_id', :primary_key => 'hiring_team_id'

    # once that bug is fixed we can remove if we want to
    has_one :career_profile, :dependent => :destroy, :autosave => true # need to update if can_edit_career_profile changes
    has_one :hiring_application, :dependent => :destroy

    has_many :persisted_timeline_event_user_joins, class_name: 'ActivityTimeline::PersistedTimelineEventUserJoin', :dependent => :destroy
    has_many :persisted_timeline_events, :through => :persisted_timeline_event_user_joins, :dependent => :destroy

    has_many :open_positions, :foreign_key => 'hiring_manager_id' # no dependent => :destroy (see destroy_candidate_relationships)
    has_many :unarchived_open_positions, -> { where(archived: false) }, :class_name => 'OpenPosition', :foreign_key => 'hiring_manager_id'
    has_many :hiring_team_open_positions, :through => :hiring_team, :source => :open_positions
    has_many :hiring_team_candidate_position_interests, :through => :hiring_team, :source => :candidate_position_interests
    before_destroy :destroy_candidate_relationships # need to do this manually to prevent foreign key issues

    # open position interest
    has_many :candidate_position_interests, :foreign_key => 'candidate_id', :class_name => 'CandidatePositionInterest', :dependent => :destroy, :validate => false

    # Note: "Featured" candidates will have a relationship, but the hiring_manager_status will be "pending" until the hiring manager takes action
    has_many :candidate_relationships, :foreign_key => 'hiring_manager_id', :class_name => 'HiringRelationship'  # no dependent => :destroy (see destroy_candidate_relationships)
    has_many :candidates, :through => :candidate_relationships
    has_many :candidate_relationships_with_pending_hiring_manager_status, -> { where(hiring_manager_status: 'pending') }, :foreign_key => 'hiring_manager_id', :class_name => 'HiringRelationship'

    has_many :candidate_relationships_with_accepted_hiring_manager_status, -> { where(hiring_manager_status: 'accepted') }, :foreign_key => 'hiring_manager_id', :class_name => 'HiringRelationship'
    has_many :candidate_relationships_with_non_pending_hiring_manager_status, -> { where.not(hiring_manager_status: 'pending')}, :foreign_key => 'hiring_manager_id', :class_name => 'HiringRelationship'
    has_many :candidate_relationships_saved_for_later, -> { where(hiring_manager_status: 'saved_for_later') }, :foreign_key => 'hiring_manager_id', :class_name => 'HiringRelationship'
    has_many :connected_candidate_relationships, -> { matched.not_closed.viewable_by_hiring_manager }, :foreign_key => 'hiring_manager_id', :class_name => 'HiringRelationship'
    has_many :relationships_from_team, :through => :hiring_team_members, :source => :candidate_relationships
    has_many :relationships_from_team_with_accepted_hiring_manager_status, :through => :hiring_team_members, :source => :candidate_relationships_with_accepted_hiring_manager_status

    has_many :hiring_manager_relationships, :foreign_key => 'candidate_id', :class_name => 'HiringRelationship', :dependent => :destroy
    has_many :career_profile_searches, :class_name => 'CareerProfile::Search'

    belongs_to :avatar, class_name: 'AvatarAsset', dependent: :destroy, optional: true
    validates_associated :avatar

    # MBA documentation
    has_many :s3_transcript_assets, :class_name => 'S3TranscriptAsset', :dependent => :destroy
    has_many :s3_english_language_proficiency_documents, :class_name => 'S3EnglishLanguageProficiencyDocument', :dependent => :destroy
    has_one :s3_identification_asset, :class_name => 'S3IdentificationAsset', :dependent => :destroy

    has_many :referred_users, :class_name => 'User', :foreign_key => 'referred_by_id', :dependent => :nullify
    belongs_to :referred_by, :class_name => 'User', :foreign_key => 'referred_by_id', optional: true

    has_many :user_id_verifications, :dependent => :destroy
    has_many :idology_verifications, :dependent => :destroy
    has_many :signable_documents, :dependent => :destroy
    has_many :deferral_links, dependent: :destroy

    # student_network_inclusion
    has_one :student_network_inclusion, :dependent => :destroy
    after_save :handle_network_inclusion, :if => :saved_change_to_pref_student_network_privacy?

    after_create :tell_slack_about_create, :log_create_event

    after_update :update_in_airtable, :if => :has_substantive_saved_changes?

    after_commit :hide_candidate_positions_interests_if_conflict_on_hiring_team_change

    # When a hiring manager's team changes reconcile pending and saved relationships for the hiring manager and the team
    before_save :hide_relationships_accepted_by_team, if: :hiring_team_id_changed?
    before_save :hide_teammates_relationships_accepted_by_you, if: :hiring_team_id_changed?

    validate :validate_hiring_team_owner, unless: :skip_hiring_team_owner_validation

    # Deactivation/Reactivation
    before_save :deactivate_user, if: :should_deactivate_user?
    before_save :reactivate_user, if: :should_reactivate_user?

    before_validation :ensure_valid_pref_locale
    validate :validate_pref_locale

    # here be dragons: when this was an after_create, it somehow ended up creating duplicate roles, but only after we moved to devise_token_auth
    before_create :assign_default_role

    after_save :bump_check_in_with_inactive_user_job_run_at, if: :saved_change_to_last_seen_at?
    after_save :update_career_profile_fulltext, if: :should_update_career_profile_fulltext?

    before_save :handle_avatar_changes
    after_save :destroy_old_avatar, if: :saved_change_to_avatar_id?

    after_save :handle_pref_allow_push_notifications_changes, if: :saved_change_to_pref_allow_push_notifications

    # we want to re-identify the user after any possible changed values. have to do it in `after_commit` do
    # to `changed?` checking in `identify`
    # NOTE: this will not trigger due to association updates. `identify` should be called explicitly in that case
    after_commit :identify, :if => :has_substantive_saved_changes?

    # we do this in an after_commit to make sure the user actually saved
    # before logging this event.  It would be better if we were wrapping
    # user creation in a tranaction, but devise makes that tricky.
    after_commit :log_pre_application_create_event

    # If the `can_edit_career_profile` flag changes to true we need to check if the user should have a `career_profile` created for them
    # see also can_edit_career_profile= below, which also handles updates that need to be
    # made to the career profile when can_edit_career_profile changes.  It would be kind of
    # nice to consolidate all this logic in one place, but it's a bit awkward and I couldn't
    # get it to work immediately.  So since this works, I decided to leave it
    before_validation :ensure_career_profile, :if => Proc.new { |user|
        # since consuer_user? might require extra queries, only do this when
        # there are substantive changes
        user.has_substantive_changes? &&
        (user.can_edit_career_profile? ||
        user.consumer_user?)
    }

    validate :validate_hiring_application_not_accepted, if: :will_save_change_to_hiring_team_id?

    before_destroy :raise_invalid_error_if_not_destroyable

    # If the `can_edit_career_profile` flag changes from true to false we need to check that they're not in a career profile list
    validate :validate_appropriate_can_edit_career_profile_change, :unless => :can_edit_career_profile

    # validations
    validates_presence_of :name, :if => :confirmed_profile_info
    validates_presence_of :name, :if => :using_email_provider?

    validates_presence_of :email, :if => :invalid_without_email?
    validates_uniqueness_of :email, :allow_nil => true, :if => :will_save_change_to_email_even_though_stupid_devise_token_auth_has_overridden_the_method? # the if saves a query
    validates_presence_of :pref_locale
    validates :phone, phone: { allow_blank: true, possible: true }
    validates_presence_of :phone, :if => :using_phone_no_password_provider?
    validates_uniqueness_of :phone, conditions: -> { where(provider: 'phone_no_password') }, :if => :using_phone_no_password_provider?

    validates_presence_of :career_profile, :if => Proc.new { |user| user.can_edit_career_profile? && user.will_save_change_to_can_edit_career_profile? } # the if saves a query

    # validate some institution email domains
    validate :validate_email_domain, :if => :should_validate_email_domain?

    # FIXME: I can't recall what this was used for, but it seems possible we could gut
    # the institutional_demo stuff as we move towards an "all users in an institution" world.
    # See https://trello.com/c/QFVa6l5W
    # institutional demo-only validations
    validates_presence_of :professional_organization, :if => :institutional_demo_registration?
    validates_presence_of :job_title, :if => :institutional_demo_registration?
    validates_presence_of :phone, :if => :institutional_demo_registration?
    validates_presence_of :country, :if => :institutional_demo_registration?

    validates_inclusion_of :notify_candidate_positions_recommended, :in => ['daily', 'bi_weekly', 'never', nil]

    before_destroy :delete_user_progresses
    after_destroy :delete_user_data

    # notification preferences and their pretty human-readable identifiers (see unsubscribe_controller.rb)
    # Note that this list only includes email notifications.  push notifications are not included here (implemented with pref_allow_push_notifications)
    NOTIFICATIONS = {
        'notify_email_newsletter' => 'Newsletter',

        # hiring manager stuff
        'notify_hiring_updates' => 'Smartly Updates',
        'notify_hiring_content' => 'Content Updates',
        'notify_hiring_candidate_activity' => 'Candidate Activity',
        'notify_hiring_spotlights' => 'Student Spotlights',
        'notify_hiring_spotlights_business' => 'Business Student Spotlights',
        'notify_hiring_spotlights_technical' => 'Technical Student Spotlights',

        # candidate stuff.
        'notify_candidate_positions' => 'Curated Positions',
        'notify_candidate_positions_recommended' => 'Recommended Positions'
    }

    KEYS_TO_DEACTIVATE_OR_REACTIVATE = [
        'can_edit_career_profile',
        'pref_student_network_privacy',
        'pref_allow_push_notifications'
    ].push(*NOTIFICATIONS.keys)

    class DisallowedBracketAccess < RuntimeError; end

    class ActiveRecord_Relation

        def external_users
            self.joins("RIGHT JOIN external_users ON external_users.id = users.id")
        end

        def who_receives_pre_application_communications

            # We only want users created during the last 4 admission rounds
            # (including the currently active one)

            # We limit to 4 rounds that ended before today.  That means
            # that we will not match the current round, and we will match the
            # end of the round before the one we care about.  The application deadline for
            # that earliest round, then, is the beginning of the round we care about
            #
            # (specs fail in a strange way if you don't have '::' before AdmissionRound)
            last_4_admission_rounds = ::AdmissionRound.where('application_deadline < ?', Time.now)
                                        .where(:program_type => 'mba')
                                        .select('application_deadline')
                                        .reorder('cohort_start_date desc', 'application_deadline desc')
                                        .limit(4)

            # the future will always be like this
            if last_4_admission_rounds.size == 4
                min_created_at = last_4_admission_rounds.map(&:application_deadline).min
            # the day we rollout we're here
            elsif last_4_admission_rounds.any?
                min_created_at = last_4_admission_rounds.map(&:application_deadline).min - 10.days
            # tests and dev might be here sometimes
            else
                min_created_at = Time.now - 30.days
            end

            self.left_outer_joins(:cohort_applications)
                .where("fallback_program_type in (?)", Cohort::ProgramTypeConfig.signup_program_types)
                .where("cohort_applications.id is null")
                .where("users.created_at > ?", min_created_at)
        end

    end

    def self.external_users
        self.all.external_users
    end

    def self.who_receives_pre_application_communications
        self.all.who_receives_pre_application_communications
    end

    # Returns an User::ActiveRecord_Relation containing external users that have an active career profile
    # and can have hiring relationships created for them. If a professional organization option id is provided,
    # the users returned in the ActiveRecord_Relation will also be users that do not have current work experiences
    # at the professional organization option with that id.
    def self.available_for_relationships(professional_organization_option_id = nil, include_do_not_create_relationships = true)
        # Retrieve the users available for relationships that don't have ids in blacklisted_user_ids and that aren't internal users.
        query = external_users.joins(:career_profile)
            .where("career_profiles.id IN (#{CareerProfile.active.select(:id).to_sql})")

        if !include_do_not_create_relationships
            query = query.where("career_profiles.do_not_create_relationships = false")
        end

        if professional_organization_option_id
            # blacklist users with current work experiences at companies associated with the professional_organization_option_id param
            blacklisted_user_ids = User.joins("JOIN career_profiles ON career_profiles.user_id = users.id
                    LEFT JOIN work_experiences ON work_experiences.career_profile_id = career_profiles.id
                    AND work_experiences.end_date IS NULL")
                .where(career_profiles: {work_experiences: {professional_organization_option_id: professional_organization_option_id}})
                .select('id')

            query = query.where.not("users.id IN (#{blacklisted_user_ids.to_sql})")
        end

        query
    end

    def self.clear_all_progress(user_id)
        Lesson::StreamProgress.clear_all_progress(user_id)
        LessonProgress.clear_all_progress(user_id)
        User.clear_all_favorites(user_id)
    end

    def self.dirty_fields_to_ignore

        # added avatars to here so that copying an avatar to s3 does not kick off identify and
        # airtable jobs
        return ["tokens", "updated_at", "last_seen_at", "sign_in_count", "current_sign_in_at", "last_sign_in_at",
                    "avatar_url", "avatar_id"]
    end

    # total hack.  devise token auth gives us no way to
    # set a different provider than 'email', so we just prevent
    # the registrations_controller from overriding it
    # when we set it to phone
    def provider=(val)
        unless self.provider == 'phone_no_password' && new_record?
            write_attribute('provider', val)
        end
        self.provider
    end

    def phone=(val)
        parsed_val = Phonelib.parse(val)&.e164
        write_attribute(:phone, parsed_val.present? ? parsed_val : val)
        self.phone
    end

    def invalid_without_email?
        if using_phone_no_password_provider?
            return false
        end
        requires_email? && confirmed_profile_info
    end

    def set_random_password
        set_password SecureRandom.urlsafe_base64(nil, false)
    end

    def set_password(password)
        self.password = self.password_confirmation = password
    end

    def join_config
        if !defined?(@join_config) || @join_config['signup_code'] != self.sign_up_code
            @join_config = AppConfig.join_config.values.detect { |entry| entry['signup_code'] == self.sign_up_code }
            @join_config ||= AppConfig.join_config['default']
        end
        @join_config
    end

    def requires_email?
        join_config['requires_email']
    end

    def institutional_demo_registration?
        # look up sign_up_code in AppConfig to check if it's an institutional demo signup
        is_demo = false

        if self.sign_up_code.blank?
            return false
        end

        AppConfig.join_config.each do |key, value|
            if value["signup_code"] == self.sign_up_code && value["institutional_demo"] == true
                is_demo = true
                break
            end
        end

        is_demo
    end

    def institutional_sign_up_codes
        %w(
                BOSHIGH
                OPMANGEORGETOWN
                GEORGETOWNMSB
                uniminuto
                UPM
                REBOOT
                EFFICACY
                EFFICACY-STAT
                ASUGSV
                WWF
                JMU
                EMIRATESDB
                FREEDEMO
                SHRMDEMO
                EMUMATH-A
                EMUMATH-B
                GWMBASTATISTICS
                NYU
                MSM
                IEBUSINESS
                DUBAIPOLICE
                INTUIT_PILOT
                ADVANCED_PLACEMENT
                inframark
                GEORGETOWNMIM
            )
    end

    def register_content_access
        # a new user that has not yet been saved will need an id
        # for event logging
        self.id ||= SecureRandom.uuid

        # If the user logged in with SAML, then the provider is also the signup_code for an institution.
        if using_saml_authentication?
            register_with_institution_based_on_provider

        # Handle institutional codes
        elsif institutional_sign_up_codes.include?(self.sign_up_code)
            register_with_institution

        # One-off for Miya Miya project's landing page
        elsif self.sign_up_code == 'MIYAMIYA'
            register_miya_miya

        # Handle remaining codes
        else
            default_sign_up_code = 'FREEMBA'
            sign_up_code = self.sign_up_code

            if sign_up_code.nil?
                raise RuntimeError.new("User #{self.email.inspect} created with no sign_up_code.") if raise_on_no_sign_up_code?
                Raven.capture_exception("User #{self.email.inspect} created with no sign_up_code.  Defaulting to #{default_sign_up_code}.")
                self.sign_up_code = sign_up_code = default_sign_up_code
            end

            # Handle special codes not covered in the institutional list
            unless ['ADMIN', 'FREEMBA', 'HIRING'].include?(sign_up_code)
                begin
                    self.add_to_group(sign_up_code)
                rescue NoSuchGroup
                    # this might be evidence of a bug, but it seems better to get the user into the product as a vanilla user rather than block them
                    Raven.capture_message("Attempted to add a user, #{self.id}, to a non-existent group based on sign_up_code #{sign_up_code}. Defaulting to FREEMBA.")
                    self.sign_up_code = sign_up_code = 'FREEMBA'
                end
            end

            # Handle the normal codes (for hiring managers, MBA, and career network)
            if sign_up_code == 'HIRING'
                self.fallback_program_type = 'demo'

            elsif sign_up_code == 'FREEMBA'
                # Note: it's possible that the fallback_program_type was already set via query params; don't override it here
                if self.fallback_program_type.nil?
                    self.fallback_program_type = 'mba'
                end
                self.mba_content_lockable = true
                self.active_playlist_locale_pack_id = self.relevant_cohort && self.relevant_cohort.required_playlist_pack_ids.first
                self.add_to_group('OPEN COURSES')
                self.ensure_institution(Institution.quantic)
            end
        end

        # Handle special situations
        if self.join_config['auto_favorite_all_courses']
            self.favorite_lesson_stream_locale_packs = AccessGroup.find_by_name(sign_up_code).lesson_stream_locale_packs
        end

        self.fallback_program_type ||= 'demo'

        self.just_called_register_content_access = true

        if !['HIRING', 'HIRING_TEAM_INVITE', 'ADMIN'].include?(sign_up_code)
            # As with the NoSuchGroup handling above, hitting this for learners would mean we did
            # not properly handle setting the user's institution explicitly. But let's just put
            # the user into Quantic and allow them through.
            if self.institutions.empty?
                Raven.capture_message("Did not configure institutions correctly for learner #{self.id} based on sign_up_code #{sign_up_code}. Defaulting to Quantic.")
                self.ensure_institution(Institution.quantic)
            end

            # Most callers of register_content_access have already created the user record,
            # but custom_omniauth_mixin calls on an unsaved user object. So if we detect this
            # is a new record then assume that active_institution will be configured by the
            # caller after the user object is actually saved.
            if self.persisted?
                self.active_institution = self.institutions.first
            end
        end
    end

    def locale
        I18n.locale_available?(pref_locale) ? pref_locale : I18n.default_locale
    end

    def institution
        institutions.first
    end

    def account_id
        if provider == 'phone_no_password'
            phone
        elsif email
            email
        else
            id
        end
    end

    def in_group_or_institution_group?(group_name)
        all_groups = groups.map(&:name)
        first_institution = institution
        if (first_institution)
            all_groups.concat(first_institution.groups.map(&:name))
        end

        return all_groups.include?(group_name)
    end

    def bookmark_stream_locale_pack(locale_pack)
        if !self.favorite_lesson_stream_locale_packs.include?(locale_pack)
            self.favorite_lesson_stream_locale_packs << locale_pack
            true
        else
            false
        end
    end

    def unbookmark_stream_locale_pack(locale_pack)
        if self.favorite_lesson_stream_locale_packs.include?(locale_pack)
            self.favorite_lesson_stream_locale_packs.delete(locale_pack)
            true
        else
            false
        end
    end

    def provider_user_info=(val)
        raise RuntimeError.new("Do not set provider_user_info directly.  Use set_provider_user_info")
    end

    # whenever we set the provider_user_info, we also update the avatar
    # and the avatar source, which means we need to know where the provider_user_info
    # came from, se we have a special method for setting it
    def set_provider_user_info(provider, user_info)

        # Apple will only send user context values once during initial registration, then never again!
        return if provider.starts_with?('apple') && self.provider_user_info

        write_attribute('provider_user_info', user_info)

        image = user_info['image'] || user_info['imageUrl'] || (user_info["picture"] && user_info["picture"]["data"] && user_info["picture"]["data"]["url"])

        # do not use the blank google image: https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg
        image = nil if (image && image.match(/4252rscbv5M/))

        # if there is an existing avatar, do not override it at all.
        if self.avatar_url.blank?
            self.avatar_url = image
        end
    end

    def raise_on_no_sign_up_code?
        Rails.env != 'production'
    end

    def register_miya_miya
        # Note: Creating the application will set up the institution state
        application = CohortApplication.create!(
            user: self,
            cohort_id: Cohort.promoted_cohort("jordanian_math")["id"],
            status: "accepted",
            applied_at: Time.now
        )
        self.association(:cohort_applications).reset
    end

    def register_with_institution_based_on_provider
        # only because it is convenient for data analysis, we
        # set the sign_up_code to the provider, since it is acting
        # here as a sign_up_code
        self.sign_up_code = self.provider
        register_with_institution
    end

    def register_with_institution

        # FIXME: This is a hack to allow for multiple sign_up_codes to put a user in the same
        # institution while allowing for the user to be assigned different groups. Ultimately, we
        # might want to change the institution data model to allow for multiple sign_up_codes.
        # See https://trello.com/c/wKXMSdct
        if self.sign_up_code == 'EMUMATH-A' or self.sign_up_code == 'EMUMATH-B'
            if self.sign_up_code == 'EMUMATH-A'
                begin
                    self.add_to_group('EMUMATH_BLENDED')
                rescue NoSuchGroup
                    Raven.capture_message("Attempted to add a user, #{self.id}, to a non-existent group EMUMATH_BLENDED")
                end

                self.active_playlist_locale_pack_id = AppConfig.playlist_locale_pack_id(:math101a_playlist_id)
            elsif self.sign_up_code == 'EMUMATH-B'
                begin
                    self.add_to_group('EMUMATH_ALL_SMARTLY')
                rescue NoSuchGroup
                    Raven.capture_message("Attempted to add a user, #{self.id}, to a non-existent group EMUMATH_ALL_SMARTLY")
                end

                self.active_playlist_locale_pack_id = AppConfig.playlist_locale_pack_id(:math101b_playlist_id)
            end

            # Reassign the user to the base sign_up_code for the institution assignment
            self.sign_up_code = 'EMUMATH'
        end

        institution = Institution.find_by_sign_up_code(self.sign_up_code)

        if institution.nil?
            raise "No institution exists with the sign_up_code #{self.sign_up_code.inspect}"
        end

        self.ensure_institution(institution)

        # Special case content assignments here.

        # JMU students get assigned to a specific year group and get a default playlist
        if self.sign_up_code == 'JMU'
            begin
                self.add_to_group('JMUFall2016')
            rescue NoSuchGroup
                Raven.capture_message("Attempted to add a user, #{self.id}, to a non-existent group JMUFall2016")
            end
            self.active_playlist_locale_pack_id = AppConfig.playlist_locale_pack_id(:jmu_playlist_id)

        # Fallback: auto-assign first playlist and favorite streams
        else
            # if institution has playlists...
            if institution.playlist_pack_ids.length > 0

                # auto-assign the first like we do for cohorts, if the option is set on the join config
                if self.join_config['auto_select_first_playlist']
                    self.active_playlist_locale_pack_id = institution.playlist_pack_ids.first
                end

            # if no playlists, just pre-favorite all of the streams available to the learner in this institution
            else
                self.favorite_lesson_stream_locale_packs = institution.lesson_stream_locale_packs
            end
        end

        # HACK: each year, we set up a new group for these institutions and auto-assign it to students
        if self.sign_up_code == 'GEORGETOWNMSB'
            # get the most recent group by year
            georgetowngroup = AccessGroup.where("name like 'GEORGETOWNMSB20%'").reorder('created_at desc').first
            self.add_to_group(georgetowngroup.name)

        elsif self.sign_up_code == 'GEORGETOWNMIM'
            # get the most recent group by year
            georgetowngroup = AccessGroup.where("name like 'GEORGETOWNMIM20%'").reorder('created_at desc').first
            self.add_to_group(georgetowngroup.name)

        elsif self.sign_up_code == 'NYU'
            # get the most recent group by year
            nyugroup = AccessGroup.where("name like 'NYUSTERN20%'").reorder('created_at desc').first
            self.add_to_group(nyugroup.name)
        end
    end

    def should_validate_email_domain?
        # Don't validate emails for users who were created before
        # the email domain validation was introduced.
        # See https://bitbucket.org/pedago-ondemand/back_royal/commits/dd4cd40a0f8ffcaf6b43181c4e71855bd2c82cdd#chg-app/models/user.rb
        # and https://trello.com/c/i9T98DU3/1166-bug-do-not-send-sentry-errors-on-email-validation-error
        return false if created_at and created_at < Time.parse('2016/08/25')

        !join_config['email_pattern_domain'].nil? || !join_config['email_pattern'].nil?
    end

    def validate_email_domain
        if !join_config['email_pattern'].nil?
            regex = join_config['email_pattern']
        else
            # Ex. ^.+@pedago\.com$
            regex = /^.+@#{Regexp.escape(join_config['email_pattern_domain'])}$/
        end

        if should_validate_email_domain? && email.match(regex).nil?
            if join_config['email_pattern_error_message']
                errors.add(:email, join_config['email_pattern_error_message'])
            else
                errors.add(:email, "must be your @#{join_config['email_pattern_domain']} account")
            end
        end
    end

    def email_domain
        email ? Mail::Address.new(email.downcase).domain : nil
    end

    def internal_has_substantive_changes?(changes)
        !changes.keys.select { |key| !User.dirty_fields_to_ignore.include?(key.to_s) }.empty?
    end

    def has_substantive_changes?
        internal_has_substantive_changes?(changes)
    end

    def has_substantive_saved_changes?
        internal_has_substantive_changes?(saved_changes)
    end

    def update_in_airtable
        self.cohort_applications.select { |a| a.should_sync_with_airtable?(self) }. each do |cohort_application|
            SyncWithAirtableJob.perform_later_with_debounce(cohort_application.id, SyncWithAirtableJob::UPDATE)
        end
    end

    def ensure_career_profile
        self.career_profile ||= CareerProfile.initialize_from_hash({
            user_id: self.id
        })
    end

    def validate_appropriate_can_edit_career_profile_change
        if can_edit_career_profile_changed?(from: true, to: false) && career_profile && CareerProfileList.pluck(:career_profile_ids).flatten.include?(career_profile.id)
            errors.add(:can_edit_career_profile, "can only be false if candidate is not in a CareerProfileList")
        end
    end

    def validate_hiring_application_not_accepted
        if hiring_team.nil? && hiring_application&.status == 'accepted'
            errors.add(:hiring_team, "cannot be unset for a user with an accepted HiringApplication")
        end
    end

    def identify(priority = nil)
        # Note: this may be undefined in local development
        if defined? Analytics

            if priority.nil? && self.use_mass_identify_priority
                priority = IdentifyUserJob::MASS_IDENTIFY_PRIORITY
            elsif priority.nil?
                priority = IdentifyUserJob::DEFAULT_PRIORITY
            end
            IdentifyUserJob.perform_later_with_debounce(self.id, priority)
        end
    end

    def using_saml_authentication?
        OMNIAUTH_SAML_PROVIDERS.include?(self.provider)
    end

    def using_email_provider?
        self.provider == 'email'
    end

    def using_phone_no_password_provider?
        self.provider == 'phone_no_password'
    end

    def global_role
        global_role_names = Set.new(["admin", "learner", "editor", "super_editor"])
        roles.detect { |r| global_role_names.include?(r.name) }
    end

    rolify # allow for adding roles to this model
    # Note: instead of blacklisting all actions on the user model by default like we are for lessons
    # instead we explicitly requiring auth with the cancan authorize! method next to each
    # users_controller method that needs it

    def assign_default_role
      add_role(:learner) if self.roles.blank?
    end

    # gets the Cohort::CheckInWithInactiveUser delayed job (should only ever be one per user)
    def check_in_with_inactive_user_job
        Delayed::Job.where("queue = 'cohort__check_in_with_inactive_user' AND handler like ?", "%#{self.id}%").first
    end

    # destroy the Cohort::CheckInWithInactiveUser delayed job, if it exists (should only ever be one per user)
    def destroy_check_in_with_inactive_user_job
        check_in_with_inactive_user_job&.destroy
    end

    # if the user's cohort application indicates that the user should be checked in with,
    # the run_at value on the existing pending Cohort::CheckInWithInactiveUser delayed job
    # is updated to 2.weeks from now, if it exists.
    def bump_check_in_with_inactive_user_job_run_at
        return unless self.cohort_application_indicates_user_should_be_checked_in_with?

        if pending_job = self.check_in_with_inactive_user_job
            pending_job.update_attribute(:run_at, Time.now + 2.weeks)
        end
    end

    # logic for determining if the user should be checked in with based off of their cohort application
    def cohort_application_indicates_user_should_be_checked_in_with?
        return !!(self.application_for_relevant_cohort&.indicates_user_should_be_checked_in_with?)
    end

    # Logic for determining if the user should be checked in with based on the user's last_seen_at value.
    # If the user has not been seen for 2.weeks and the user's cohort application (if it exists) indicates
    # that the user should be checked in with, true is returned; otherwise, false.
    def should_be_checked_in_with?
        return (!self.last_seen_at || self.last_seen_at < 2.weeks.ago) && self.cohort_application_indicates_user_should_be_checked_in_with? ? true : false
    end

    # make can? and cannot? methods available on user
    def ability
        @abilities ||= {}
        @abilities[self.roles.map(&:name).sort.join] ||= Ability.new(self)
    end

    def editable_resources(resource_type)
        self.roles.where(resource_type: resource_type, name: ["lesson_editor", "previewer", "reviewer"])
    end

    def expire
        UserMailer.expire_email(self).deliver
        destroy
    end

    def preferred_name
        nickname.blank? ? name : nickname
    end

    def self.clear_all_favorites(user_id)
        user = User.find(user_id)
        user.favorite_lesson_stream_locale_packs = []
        user.save!
    end

    def self.as_json(users_to_serialize, options = {}, cached_roles_map = nil)
        users_json = []

        user_ids = users_to_serialize.map(&:id).map(&:to_s)

        if (options[:for] != :public && options[:for] != :email_list)

            # run one query to get all user roles
            if cached_roles_map.blank? && users_to_serialize.count > 0
                cached_roles_map = Hash.new { |h, user_id| h[user_id] = []}
                roles = []
                user_ids = users_to_serialize.map(&:id)
                user_id_arr_str = user_ids.map { |id| "\'#{id}\'" }.join(',')

                # one query to get all associated roles plus the linked user_id
                roles_users_join = "SELECT roles.*, users.id as user_id FROM roles
                INNER JOIN users_roles ON users_roles.role_id = roles.id
                INNER JOIN users ON users.id = users_roles.user_id
                WHERE users.id IN (#{user_id_arr_str})";
                results = ActiveRecord::Base.connection.execute(roles_users_join);

                results.each do |result|
                    user_id = result["user_id"]

                    # reconstitute the role for serialization
                    result.delete("user_id")
                    role = Role.new(result)
                    roles << role

                    # serialize the role
                    cached_roles_map[user_id] << role.id
                end

                cached_roles_json_map = {}
                Role.as_json(roles).each do |role_json|
                    cached_roles_json_map[role_json['id']] = role_json
                end

                cached_roles_map.each do |user_id, role_ids|
                    cached_roles_map[user_id] = role_ids.map do |id|
                        role_json = cached_roles_json_map[id]
                        raise "No role json for #{id}" unless role_json
                        role_json
                    end
                end

            end

            # run one query to get all available groups
            available_group_names = AccessGroup.pluck('name')

        end

        users_to_serialize.each do |user|
            # make sure to clone the options, otherwise more and more keys get added
            serialized_user = user.as_json(options.clone, cached_roles_map, available_group_names)

            if options[:for] != :public && options[:for] != :email_list

                # FIXME: probably should remove this check once we see that
                # we're not hitting it after the access_groups refactor
                raise "Expected groups to be eager loaded" if users_to_serialize.size > 1 && !user.access_groups.loaded?

                serialized_user["groups"] = user.access_groups.reorder(:name).as_json
                serialized_user["available_groups"] = available_group_names # todo: replicate this in all users?
                User::NOTIFICATIONS.each_key { |notification| serialized_user[notification] = user.send("#{notification}") }
                serialized_user.merge("roles" => cached_roles_map[user.id])
            end

            users_json << serialized_user
        end
        users_json
    end

    def has_accepted_hiring_manager_access?
        hiring_application && hiring_application.status == 'accepted'
    end

    def as_json(options = {}, cached_roles_map = nil, available_group_names = nil)
        if (options[:for] == :public)
                {
                    'name' => name,
                    'id' => id
                }
        elsif  (options[:for] == :email_list)

                {
                    'email' => email,
                    'name' => name,
                    'id' => id
                }
        else
                user_json = super(options)

                # Serialize dates used on the client
                user_json["last_seen_at"] = self.last_seen_at.to_timestamp
                user_json["updated_at"] = self.updated_at.to_timestamp

                # use cached roles, available groups, if passed in to avoid going to the database again
                if !available_group_names.blank?
                    user_json["available_groups"] = available_group_names
                else
                    user_json["available_groups"] = AccessGroup.pluck('name').sort.as_json(options)
                end

                cohort_applications = self.cohort_applications # Is this optimization necessary?
                user_json["cohort_applications"] = cohort_applications.as_json(options)
                user_json["curriculum_status"] = self.last_application&.curriculum_status

                user_json["hiring_application"] = self.hiring_application.as_json
                user_json["hiring_team"] = self.hiring_team&.as_json

                user_json['groups'] = self.access_groups.reorder(:name).as_json
                user_json["institutions"] = self.institutions.as_json
                user_json["active_institution"] = self.active_institution.as_json

                user_json["has_seen_welcome"] = self.has_seen_welcome
                user_json["has_seen_careers_welcome"] = self.has_seen_careers_welcome
                user_json["has_seen_first_position_review_modal"] = self.has_seen_first_position_review_modal
                user_json["confirmed_profile_info"] = self.confirmed_profile_info
                user_json["school"] = self.school
                user_json["country"] = self.country
                user_json["job_title"] = self.job_title
                user_json["phone"] = self.phone
                user_json["phone_extension"] = self.phone_extension
                user_json['avatar_url'] = self.avatar_url
                user_json['is_institutional_reports_viewer'] = is_institutional_reports_viewer?
                user_json["pref_decimal_delim"] = self.pref_decimal_delim
                user_json["pref_show_photos_names"] = self.pref_show_photos_names
                user_json["pref_sound_enabled"] = self.pref_sound_enabled
                user_json["has_seen_accepted"] = self.has_seen_accepted
                user_json["professional_organization"] = self.professional_organization.as_json
                user_json["has_seen_featured_positions_page"] = self.has_seen_featured_positions_page
                user_json["has_seen_hide_candidate_confirm"] = self.has_seen_hide_candidate_confirm
                user_json["has_seen_student_network"] = self.has_seen_student_network
                user_json["has_seen_short_answer_warning"] = self.has_seen_short_answer_warning
                user_json["pref_one_click_reach_out"] = self.pref_one_click_reach_out
                user_json["can_purchase_paid_certs"] = self.can_purchase_paid_certs
                user_json["pref_positions_candidate_list"] = self.pref_positions_candidate_list
                user_json["skip_apply"] = self.skip_apply
                user_json["has_drafted_open_position"] = self.has_drafted_open_position
                user_json["hiring_teammate_emails"] = self.active_hiring_teammates.pluck(:email)
                user_json["deactivated"] = self.deactivated
                user_json["notify_candidate_positions"] = self.notify_candidate_positions
                user_json["notify_candidate_positions_recommended"] = self.notify_candidate_positions_recommended
                user_json["student_network_email"] = self.student_network_email

                user_json["career_profile"] = self.career_profile.as_json(view: 'editable')

                # calculated dynamically now
                user_json["mba_enabled"] = self.mba_enabled?

                # for determining which content to lock
                user_json['mba_content_lockable'] = self.mba_content_lockable # users made before a certain point will not have content locked

                user_json['relevant_cohort'] = self.relevant_cohort.as_json(user_id: self.id) # make sure this includes the list of playlist pack ids and stream pack ids

                user_json["subscriptions"] = self.subscriptions.as_json

                user_json["favorite_lesson_stream_locale_packs"] = favorite_lesson_stream_locale_packs_for_json

                if !cached_roles_map.blank?
                    user_json.merge!("roles" => cached_roles_map[self.id])
                else
                    roles_json = self.roles.as_json if self.roles
                    user_json.merge!("roles" => roles_json)
                end

                user_json["pref_locale"] = self.pref_locale
                user_json["s3_transcript_assets"] = self.s3_transcript_assets.as_json
                user_json["s3_identification_asset"] = self.s3_identification_asset.as_json
                user_json["s3_english_language_proficiency_documents"] = self.s3_english_language_proficiency_documents.as_json

                if referred_by
                    user_json["referred_by"] = referred_by.as_json.slice("id", "name", "email")
                end

                user_json["pref_allow_push_notifications"] = self.pref_allow_push_notifications

                user_json["user_id_verifications"] = user_id_verifications_for_json
                user_json["idology_verifications"] = idology_verifications_for_json
                user_json["signable_documents"] = self.signable_documents.map(&:as_json)
                user_json["recommended_positions_seen_ids"] = self.recommended_positions_seen_ids
                user_json["net_amount_paid"] = self.net_amount_paid

                if options[:fields]
                    user_json.slice(*options[:fields])
                else
                    user_json
                end

        end
    end

    def favorite_lesson_stream_locale_packs_for_json
        self.favorite_lesson_stream_locale_packs.pluck('id').map { |id| {'id' => id}}
    end

    def user_id_verifications_for_json
        self.user_id_verifications.order('verified_at DESC').map(&:as_json)
    end

    def idology_verifications_for_json
        [idology_verification_with_most_recent_activity].compact.map(&:as_json)
    end

    def idology_verification_with_most_recent_activity
        # We want the one with the most recent activity, either that it
        # was updated with a response or that it was newly created
        self.idology_verifications.order(Arel.sql('coalesce(response_received_at, created_at) DESC')).first
    end

    # See https://trello.com/c/7kdkrPj6 for more info
    def missing_verification_documents?
        missing_identification_document? || missing_transcripts?
    end

    # See https://trello.com/c/7kdkrPj6 for more info
    def missing_identification_document?
        !!(self.last_application&.indicates_user_should_upload_identification_document?) && !self.identity_verified && self.s3_identification_asset.nil?
    end

    # See https://trello.com/c/7kdkrPj6 for more info
    def missing_transcripts?
        !self.transcripts_verified && !!self.last_application&.indicates_user_should_upload_transcripts? &&
            !!self.career_profile&.indicates_user_should_upload_transcripts? && self.career_profile&.missing_transcripts?
    end

    def should_have_transcripts_auto_verified?
        !!(
            !self.transcripts_verified &&
            !self.career_profile&.education_experiences_indicating_transcript_required.blank? &&
            self.career_profile&.education_experiences_indicating_transcript_required
                .select { |e| !e.transcript_approved_or_waived? }
                .size == 0
        )
    end

    # See https://trello.com/c/7kdkrPj6 for more info
    def missing_english_proficiency_documents?
        !self.english_language_proficiency_documents_approved? &&
        !!(self.career_profile&.indicates_user_should_upload_english_language_proficiency_documents?) &&
        !!(self.last_application&.indicates_user_should_upload_english_language_proficiency_documents?) &&
        self.s3_english_language_proficiency_documents.empty?
    end

    # FIXME: This checks for an external institution in order to save a
    # query, but if we decide to have reports viewers for our internal institutions
    # we will need to change this.
    # See https://trello.com/c/QFVa6l5W
    def is_institutional_reports_viewer?
        # check has_external_institution? first just to save a query
        self.has_external_institution? && self.views_reports_for_institutions.any?
    end

    def concerning_email_domains
        %w(aevy.com altschool.com alueducation.com alustudent.com amazinghiring.com angel.co bettercompany.com careercake.com cengage.com clinch.io closeriq.com codefights.com codefights.com codeity.com connectifier.com coursera.org credibll.com crowded.com drafted.us dx.org employeereferrals.com entelo.com extensionengine.com firstbird.com firstjob.com gohachi.com greatlearning.in hireart.com hired.com hiretual.com include.io indeed.com interviewing.io interviewjet.com jobbatical.com khanacademy.org landing.jobs leap.ai linkedin.com linkmatch.net mailinator.com meritocracy.is monster.com mosaiclearning.com netin.co novoed.com onewire.com openlearning.com originate.com piazza.com powertofly.com prosky.co pymetrics.com rakuna.co recruitifi.com referralmob.com ripplehire.com ripplematch.com rivierapartners.com roikoi.com rolepoint.com savvy.jobs scouted.io simplyhired.com singlesprout.com smarthires.com splashthat.com stella.ai switchapp.com swooptalent.com talentbin.com teamable.com theladders.com themuse.com triplebyte.com udacity.com uncommon.co uncubed.com underdog.io underdog.io untapt.com upsider.co usescout.com vettery.com vsource.io wayup.com whitetruffle.com woo.io workey.co works-hub.com workshape.io yborder.com yopmail.com ziprecruiter.com)
    end

    def boring_email_domains
        %w(gmail.com yahoo.com yahoo.gr hotmail.com hotmail.co.uk live.com icloud.com outlook.com msn.com aol.com me.com ymail.com rogers.com)
    end

    def company_id
        professional_organization && professional_organization.id
    end

    def company_name
        professional_organization && professional_organization.text
    end

    def company_logo_url
        professional_organization && professional_organization.image_url
    end

    def tell_slack_about_create

        message = "New user: #{self.name} / *#{self.email || 'No Email'}*"

        if self.email && self.email.downcase.split('.').last == 'edu'
            message += " 🚌" # school bus
        elsif self.provider == 'phone_no_password'
            message += " 🚗"
        elsif self.sign_up_code == 'HIRING'
            message += " 👥"
        end

        provider_name = I18n.provider(self.provider, :en)
        infos = [
            "registered with *#{provider_name}*",
            "signed up with code *#{sign_up_code}*"
        ]
        infos << "attends *#{self.school}*" if self.school
        info_bullets = infos.map do |info|
            "    •  #{info}"
        end.join(" \n")

        message = "#{message}\n#{info_bullets}"

        # Handle special case domain display
        color = nil
        if self.email
            domain = self.email_domain
            if concerning_email_domains.include?(domain)
                color = 'FF0000'
                message += "\n(cc: @channel)"
            elsif !boring_email_domains.include?(domain)
                color = 'F3E000'
            end
        elsif self.provider == 'phone_no_password'
            color = '000000';
        end

        LogToSlack.perform_later(Slack.signups_channel, nil,
            [
                {
                    color: color,
                    text: message,
                    mrkdwn_in: ['text']
                }
            ])


    rescue Exception => err
        Raven.capture_exception(err)
    end

    def log_create_event
        Event.create_server_event!(SecureRandom.uuid, self.id, 'user:created', {
            sign_up_code: self.sign_up_code,
            label: self.sign_up_code, # set the sign_up_code for GA
            provider: self.provider
        }).log_to_external_systems(nil, false) # we want user:created events in amplitude
    end

    def log_pre_application_create_event

        is_new_user = saved_change_to_id.present?

        # Normally, we log this event when consumer users first
        # call register_content_access.
        # Some facebook users, however, will not have an email address yet at that
        # point.  So whenever a consumer user updates their email from blank
        # to something, we also log this.
        should_log = (just_called_register_content_access && !email.blank?) ||
                        (!is_new_user && saved_change_to_email && saved_change_to_email.first.blank?)

        if should_log && consumer_user?
            # regardless of the user's program_type, we are showing info from the
            # promoted mba round
            Event.create_server_event!(SecureRandom.uuid, self.id, 'pre_application:user_created', AdmissionRound.promoted_rounds_event_attributes(self.timezone)).log_to_external_systems

            # prevent multiple saves from sending multiple events after registering content access
            self.just_called_register_content_access = false
        end
    end

    def share_career_profile(career_profile, recipient, message)
        Event.create_server_event!(
            SecureRandom.uuid,
            self.id,
            'hiring_manager:share_career_profile',
            {
                recipient_id: recipient.id,
                recipient_email: recipient.email,
                message: message,
                deep_link_url: career_profile.deep_link_url,
                candidate_nickname: career_profile.user.nickname || career_profile.user.name,
                candidate_name: career_profile.user.name,
                candidate_avatar_url: career_profile.user.avatar_url_with_fallback,
                candidate_position_title: career_profile.featured_work_experience&.job_title,
                candidate_position_company_name: career_profile.featured_work_experience&.professional_organization&.text
            }
        ).log_to_external_systems
    end

    # Note: see also vendor/front-royal/components/translation/scripts/locale.js#availablePreferenceLocales
    def ensure_valid_pref_locale
        unless pref_locale_valid?
            self.pref_locale = 'en'
        end
    end

    def pref_locale_valid?
        I18n.available_locales.include?(self.pref_locale.to_sym)
    end

    def validate_pref_locale
        unless pref_locale_valid?
            errors.add(:pref_locale, "is not set to an allowed value")
        end
    end

    def consumer_user?
        # this assumes that every consumer user has one of the program types
        # defined in ProgramTypeConfig (i.e. not institutional or demo)
        program_type && Cohort::ProgramTypeConfig[program_type]&.consumer_user?
    end

    def last_application
        # The cohort_applications should be sorted by "applied_at DESC", so first gives us the most recent
       self.cohort_applications.first
    end

    # there can be only one
    def application_for_relevant_cohort
        self.pending_application || self.accepted_application || self.pre_accepted_application
    end

    def accepted_application
        ensure_applications_reference_self
        cohort_applications.detect { |app| app.status == 'accepted' }
    end

    def pending_application
        ensure_applications_reference_self
        cohort_applications.detect { |app| app.status == 'pending' }
    end

    def pre_accepted_application
        ensure_applications_reference_self
        cohort_applications.detect { |app| app.status == 'pre_accepted' }
    end

    def ensure_applications_reference_self
        # this is meant to solve the issue that
        # self is not the same object as self.cohort_applications.first.user.  So,
        # if we make changes to this object, then the applications will not know about
        # them unless we explicitly setup the reference.  This might be a bit of a hack,
        # but I don't know if active record gives us a better way to handle it
        cohort_applications.each { |ca|
            if ca.association(:user).loaded? && ca.user.object_id != self.object_id
                raise "Cannot reset user association on application because user has changed" if ca.user.changed?
                ca.user = self
            end
        }
    end

    # Defers a user into the target cohort, maintaining the user's monthly subscription
    # into said cohort or creating a new monthly subscription if applicable
    def defer_into_cohort(current_cohort_application, target_cohort_id)
        target_cohort = Cohort.cached_published(target_cohort_id)

        RetriableTransaction.transaction do
            # When a user defers into a new cohort, we want to retain their original subscription,
            # so we need to make sure that we transfer their application's payment_info over to their
            # new_cohort_application. Some of this payment info can be cleared from the application
            # when marked as deferred (payment_grace_period_end_at), so we collect the payment_info
            # before we mark the application as deferred to ensure proper transfer of the payment_info
            # to the new_cohort_application.
            payment_info_from_current_application = current_cohort_application.payment_info
            current_cohort_application.update!(status: 'deferred')

            new_cohort_application = CohortApplication.create!({
                user_id: self.id,
                cohort_id: target_cohort_id,
                applied_at: Time.now,
                status: 'pre_accepted',
                admissions_decision: 'manual_admin_decision',
                registered_at: current_cohort_application.registered_at
            }.merge(payment_info_from_current_application))

            # Only after the new_cohort_application has been created can we mark their current_cohort_application
            # (technically now their previous application) as unregistered since `registered` is part of the `payment_info`
            # that gets transferred over to the new_cohort_application from the current_cohort_application.
            current_cohort_application.update!(registered: false)

            # When a user is deferred into a cohort, we'd like to keep their previous subscription
            # so that they don't have to register again when their new cohort starts. We accomplish
            # this by giving them a trial period without proration that will end at the target cohort's
            # enrollment deadline. This isn't applicable for users who chose the
            # single-payment plan, however. NOTE: We expect there to be a subscription, but for users
            # that chose the single-payment plan, it's possible, albeit unlikely, that their subscription
            # has already been deleted if the webhooks were processed quickly enough.
            #
            # If there is no subscription, the user hasn't paid off their subscription (see note below),
            # the user is deferring into a cohort that supports payments, and the user does not have a full
            # scholarship, then we should create a monthly subscription with a free trial that ends at the
            # target cohort's enrollment deadline.
            #
            # Note: We check for "hasn't paid off their subscription" by checking if
            # total_num_required_stripe_payments is not zero, because a value of zero means they made a payment at
            # some point but no longer require any. See also the note in cohort_application.js#hasMadePayments
            # and its related methods.
            # See https://trello.com/c/yZKCV3E3
            will_need_subscription = !self.primary_subscription &&
                new_cohort_application.total_num_required_stripe_payments != 0 &&
                target_cohort.supports_payments? &&
                !new_cohort_application.has_full_scholarship?

            if self.primary_subscription && new_cohort_application.stripe_plan['frequency'] != 'once'
                trial_end = target_cohort.trial_end_date.to_timestamp
                Stripe::Subscription.update(self.primary_subscription.stripe_subscription_id, {trial_end: trial_end, proration_behavior: 'none'})
            elsif will_need_subscription
                raise "Does not support creating a new subscription"
            end

            new_cohort_application
        end
    end

    # FIXME: We should be able to make this just check `active_institution` once we have
    # run the backfills
    # See https://trello.com/c/QFVa6l5W
    def has_external_institution?
        self.institutions.any?(&:external?)
    end

    def ensure_institution(institution)
        if self.institutions.exclude?(institution)
            self.institutions << institution
        end
    end

    def active_institution
        self.institution_joins.detect(&:active)&.institution
    end

    def active_institution=(institution)
        RetriableTransaction.transaction do
            active_join = self.institution_joins.detect(&:active)

            # If calling with the existing active institution_join then do nothing
            if active_join&.institution_id == institution&.id
                return institution
            end

            # Unset an existing active institution_join
            if active_join.present?
                active_join.update!(active: false)
            end

            # If setting to nil then exit here
            if institution.nil?
                return nil
            end

            new_active_join = self.institution_joins.detect { |j| j.institution_id == institution.id }

            if new_active_join.nil?
                raise 'User must be in the institution for it to become their active'
            end

            # Change the new institution's join record to be active
            new_active_join.update!(active: true)

            return self.active_institution
        end
    end

    def is_quantic_user?
        # For now, we determine if the user is a Quantic user by checking their active_institution,
        # but in the future, we may want to base this off of their cohort program rather than just
        # their active_institution.
        self.active_institution&.id == Institution::QUANTIC_ID
    end

    # Returns the latest published version of the cohort related to the user's pending, accepted, or pre_accepted
    # application. If no published version can be found, an error is thrown. If the user
    # has no application with one of the aforementioned statuses, the currently promoted cohort version for the
    # user's fallback_program_type is returned. NOTE: If no error is thrown, a Cohort::Version object is always
    # returned, so if you want to reference the relevant cohort's id you have to use attributes['id'] to be in
    # accordance with ContentItemVersionMixin.
    def relevant_cohort
        # hiring managers have a program_type of "demo", but the client expects hiring managers to never have a
        # relevant_cohort, so return nil early if we're trying to determine the relevant_cohort for a hiring manager
        return nil if self.hiring_application.present?

        application = application_for_relevant_cohort
        if application
            raise "No published version found for relevant cohort #{application.cohort.name}" unless application.published_cohort
            application.published_cohort
        else
            # For demo users, we return an MBA cohort as their relevant _cohort, so their
            # demo experience more closely matches that of a normal MBA user.
            Cohort.promoted_cohort(self.program_type == 'demo' ? 'mba' : fallback_program_type)
        end
    end

    def in_isolated_network_cohort?
        !!(self.relevant_cohort&.isolated_network)
    end

    def relevant_cohort_id
        cohort = relevant_cohort
        cohort && cohort.attributes['id']
    end

    def completed_lesson_locale_pack_ids
        return self.lesson_progresses
            .where.not({completed_at: nil})
            .pluck(:locale_pack_id)
    end

    def completed_stream_locale_pack_ids
        return self.lesson_streams_progresses
            .where.not({completed_at: nil})
            .pluck(:locale_pack_id)
    end

    def completed_specializations_count(cohort = nil)
        cohort ||= self.relevant_cohort
        # Preemptively return nil if no relevant_cohort or the
        # cohort does not support specializations
        return nil if cohort.nil? || !Cohort::ProgramTypeConfig[cohort.program_type].supports_specializations?

        # Most of this logic is duplicated from ViewHelpers::CohortUserProgressRecords
        # and ViewHelpers::PlaylistProgress. Since this count is needed in a real-time
        # context, we cannot rely on materialized views that are not kept up-to-date.
        #
        # We can, however, rely on the content views to help us get the data we need,
        # as those views are refreshed after any updates. See ContentPublisher::Publishable.
        record = ActiveRecord::Base.connection.execute(%Q~
            with playlist_progress_live AS MATERIALIZED (
                select
                  lesson_streams_progress.user_id                        as user_id,
                  playlist_locale_pack_id                                as locale_pack_id,
                  count(distinct case
                                 when lesson_streams_progress.completed_at is not null
                                   then lesson_streams_progress.locale_pack_id
                                 else null
                                 end) = published_playlists.stream_count as completed
                from
                  lesson_streams_progress
                  join published_playlist_streams
                    on published_playlist_streams.stream_locale_pack_id = lesson_streams_progress.locale_pack_id
                  join published_playlists
                    on published_playlists.locale_pack_id = published_playlist_streams.playlist_locale_pack_id
                where
                  lesson_streams_progress.user_id = '#{self.id}'
                  and published_playlists.locale = 'en'
                group by
                  lesson_streams_progress.user_id
                  , playlist_locale_pack_id
                  , published_playlists.stream_count
            )
            , playlist_counts AS MATERIALIZED (
                select count(*) as specialization_playlists_complete
                from
                  published_cohort_playlist_locale_packs
                  join playlist_progress_live
                    on playlist_progress_live.locale_pack_id = published_cohort_playlist_locale_packs.playlist_locale_pack_id
                where
                  playlist_progress_live.user_id = '#{self.id}'
                  and playlist_progress_live.completed = TRUE
                  and published_cohort_playlist_locale_packs.specialization = TRUE
                  and published_cohort_playlist_locale_packs.cohort_id = '#{cohort.attributes['id']}'
                group by
                  published_cohort_playlist_locale_packs.cohort_id
                  , playlist_progress_live.user_id
            )
            select specialization_playlists_complete
            from playlist_counts
        ~).to_a.first

        record && record['specialization_playlists_complete'] || 0
    end

    def get_completed_foundations_stream_count
        # Get the user's completed stream progresses
        completed_locale_pack_ids = Lesson::StreamProgress.where("user_id = '#{self.id}' AND completed_at IS NOT NULL").map(&:locale_pack_id).uniq
        foundation_stream_locale_pack_ids = relevant_cohort && relevant_cohort.get_foundations_lesson_stream_locale_pack_ids(self.locale)

        if !foundation_stream_locale_pack_ids || !completed_locale_pack_ids
            return 0 # should only happen in tests, unless we abandon a cohort promotion
        end

        (completed_locale_pack_ids & foundation_stream_locale_pack_ids).size
    end

    def get_foundations_playlist
        if self.relevant_cohort && self.relevant_cohort.get_foundations_playlist_locale_pack
            # fall back to english if playlist doesn't exist in this locale
            playlist = self.relevant_cohort.get_foundations_playlist_locale_pack.content_items.where(locale: [locale, :en]).all.sort_by do |content_item|
                if content_item.locale == locale
                    0
                else
                    9999
                end
            end.first
            playlist.published_version
        end
    end

    def is_first_concentration_playlist_complete?
        first_concentration_stream_locale_pack_ids = relevant_cohort && relevant_cohort.get_first_concentration_lesson_stream_locale_pack_ids(self.locale)

        if !first_concentration_stream_locale_pack_ids || !self.completed_stream_locale_pack_ids
            return false
        end

        (self.completed_stream_locale_pack_ids & first_concentration_stream_locale_pack_ids).size == first_concentration_stream_locale_pack_ids.size
    end

    def has_accepted_cohort_application?
        self.cohort_applications.map(&:status).include?('accepted')
    end

    # FIXME: I suspect this is going to morph into a check on the user's institution
    # instead as we move towards creating rules on institutions and their programs instead.
    # See https://trello.com/c/QFVa6l5W
    def has_student_network_access?
        self.program_type != 'external' && (self.program_type == 'demo' || Cohort::ProgramTypeConfig[self.program_type]&.supports_student_network_access?) && !self.hiring_application
    end

    def is_admin?
        self.has_role?(:admin)
    end

    def is_interviewer?
        self.has_role?(:interviewer)
    end

    def is_admin_or_interviewer?
        self.is_admin? || self.is_interviewer?
    end

    # this is tested, surprisingly, in student_network_filters_mixin_spec.rb.  See assert_inclusion_of_user
    def has_full_student_network_access?
        return self.is_admin_or_interviewer? || self.has_application_that_provides_student_network_inclusion?
    end

    def has_full_student_network_events_access?
        self.is_admin_or_interviewer? || self.student_network_inclusion&.status == 'accepted'
    end

    def deferred_info
        statuses_after_deferred = Set.new
        last_deferred_application = self.cohort_applications.detect do |ca|
            statuses_after_deferred << ca.status
            ca.status == 'deferred'
        end

        return OpenStruct.new({
            statuses_after_deferred: statuses_after_deferred,
            last_deferred_application: last_deferred_application
        })
    end

    def has_application_that_provides_student_network_inclusion?
        # The following logic duplicates what is in sql at ControllerMixins::StudentNetworkFiltersMixin#join_relevant_cohort_applications
        # basically, you need an accepted application or a deferred application that does not have a rejected or expelled application after it.
        # It is also tested there.  See note above `has_full_student_network_access?`
        return false if self.accepted_application&.graduation_status == 'failed'
        return true if self.accepted_application&.program_type_config&.provides_full_student_network_access?
        deferred_info = self.deferred_info
        if deferred_info.last_deferred_application.present? &&
            deferred_info.last_deferred_application.program_type_config.provides_full_student_network_access? &&
            (deferred_info.statuses_after_deferred - ['deferred', 'pre_accepted']).empty?
            return true
        end
        return false
    end

    def can_receive_student_network_messages?
        # on the client, student A can only contact student B if student B
        # has their pref_student_network_privacy setting set to 'full',
        # so it holds true that  student B should only be able to receives
        # messages if their pref_student_network_privacy setting is 'full'
        self.pref_student_network_privacy == 'full'
    end

    # takes a hash of candidate_ids pointing to properties for the
    # newly created relationships
    def ensure_candidate_relationships(arg)
        if arg.is_a?(Array)
            candidate_map = {}
            arg.each do |user_id|
                candidate_map[user_id] = {}
            end
        else
            candidate_map = arg
        end

        self.candidate_relationships += candidate_map.map do |user_id, attrs|
            # we currently ignore attrs if the relationship exists.  Not sure if
            # that will always be the right thing, but seems good with current use cases
            next if self.candidates.pluck('id').include?(user_id)
            hr = HiringRelationship.new({candidate_id: user_id})
            attrs.each do |key, value|
                hr.send(:"#{key}=", value)
            end
            hr
        end.compact
    end

    def active_career_profile
        (career_profile && career_profile.career_profile_active?) ? career_profile : nil
    end

    # FIXME: user.program_type and user.fallback_program_type are likely to be removed at some point
    # See discussion at https://trello.com/c/QFVa6l5W
    def program_type
        # check for application first, because this is the most common case, and is often preloaded (see career_profile.rb#self.includes_needed_for_json_eager_loading)
        if application = self.application_for_relevant_cohort
            application.program_type
        elsif self.has_external_institution?
            # This used to be "institutional" until we moved to a concept of all
            # users, even normal learners, having an institution. While making that
            # transition, the existing meaning of having an institution was replaced
            # by having an "external" institution.
            # See https://trello.com/c/QFVa6l5W
            'external'
        else
            self.fallback_program_type
        end
    end

    def program_type=(val)
        new_cohort = nil

        # For mba and emba users, ensure the user is switched into the sister_cohort for their relevant_cohort.
        # This will prevent them from being switched into a newly promoted cohort for the other program type
        # because the deadline for the last admission round for the new_cohort has passed.
        if self.mba_enabled? && val != self.program_type && ['mba', 'emba'].include?(val)
            new_cohort = self.relevant_cohort.sister_cohort
        else # if the user is not switching between mba and emba, get the promoted cohort for the program_type passed in
            new_cohort = Cohort.promoted_cohort(val)
        end

        _pending_application = self.pending_application

        if _pending_application && new_cohort && _pending_application.published_cohort && _pending_application.published_cohort.program_type != new_cohort.program_type

            # Users can only have one cohort application per cohort, so if we happen to see that the user has already
            # applied to the new_cohort, we want to make sure that we update the pre_existing_application and destroy
            # their currently _pending_application. This is unlikely to happen for cohort's with a program type where
            # supports_admission_rounds? is true, but for others, it's possible for them to reapply to the very same
            # cohort that they applied to in the past.
            pre_existing_application = self.cohort_applications.detect { |application| application.cohort_id == new_cohort.attributes['id'] }
            if pre_existing_application
                if new_cohort.program_type_config.supports_reapply?

                    # we treat any new application's original_program_type as immutable throughout the lifecycle
                    # of subsequent program_type changes, but since we're hijacking a previous application to ferry
                    # state (see below), we also need to ferry the originating application's original_program_type
                    pending_application_original_program_type = _pending_application.original_program_type

                    # a user can only have one pending application at a time, so destroy their currently
                    # _pending_application and update their pre_existing_application instead
                    _pending_application.destroy

                    pre_existing_application.status = 'pending'
                    pre_existing_application.original_program_type = pending_application_original_program_type
                    pre_existing_application.applied_at = Time.now
                    pre_existing_application.save!

                    # we've modified the cohort_applications association by destruction and modification,
                    # so we need to ensure that the association is reset so any additional references will
                    # reload the appropriate records and data
                    self.association(:cohort_applications).reset
                else
                    Raven.capture_exception("Attempting to reapply to cohort that doesn't support reapply", {
                        extra: {
                            user_id: self.id,
                            new_cohort_id: new_cohort.attributes['id'],
                            pre_existing_application_id: pre_existing_application.id
                        }
                    })
                end
            else
                # Note: can't do the simple approach here, i.e.:
                #
                #   _pending_application.cohort = new_cohort
                #
                # ...because new_cohort is a published version, and doing the above will
                # invoke the `id` getter as part of the ActiveRecord autosave associations logic,
                # which will cause an exception to be thrown in content_item_version_mixin.rb
                # So, we avoid all that by manually updating the foreign key
                _pending_application.cohort_id = new_cohort.attributes['id']
                _pending_application.association(:cohort).reset
                _pending_application.save!
            end
        end
        self.fallback_program_type = val
        self.program_type_confirmed = true
    end

    def mba_enabled?
        ['mba', 'emba'].include?(program_type)
    end

    def relevant_cohort_curriculum_complete?
        curriculum_complete?(relevant_cohort)
    end

    def curriculum_complete?(cohort)
        required_courses_completed_overall(cohort.attributes['id']) == cohort.get_required_stream_count
    end

    def required_courses_completed_overall(cohort_id)
        ActiveRecord::Base.count_by_sql("
            SELECT count(*)
            FROM published_cohort_stream_locale_packs
              JOIN lesson_streams_progress
                ON lesson_streams_progress.locale_pack_id = published_cohort_stream_locale_packs.stream_locale_pack_id
            WHERE lesson_streams_progress.user_id = '#{self.id}'
                  AND published_cohort_stream_locale_packs.cohort_id = '#{cohort_id}'
                  AND published_cohort_stream_locale_packs.required = TRUE
                  AND lesson_streams_progress.completed_at IS NOT NULL;
        ")
    end

    def cohort_percent_complete(cohort_id)
        record = ActiveRecord::Base.connection.execute("
            select
                count(lesson_streams_progress.completed_at)::float / nullif(count(*), 0) as percent_complete
                , count(lesson_streams_progress.completed_at)::float complete
                , count(*) total
            from published_cohort_stream_locale_packs
                left join lesson_streams_progress
                    on published_cohort_stream_locale_packs.stream_locale_pack_id = lesson_streams_progress.locale_pack_id
                    and user_id = '#{self.id}'
            where cohort_id = '#{cohort_id}'
                and required=true
        ").to_a.first
        record && record['percent_complete']
    end

    # FIXME: This is currently used by cohort#with_users_for_rule. We could make this a lot more
    # effecient by doing one big query over there rather than breaking out into a query for each user here.
    def has_met_requirements_for_period?(cohort, period_or_index)
        # if an index is passed in here, it should be 1-indexed.  This is so that
        # we can follow the way that published_cohort_periods defines period 0 as
        # all of the periods before the start date
        index = period_or_index.is_a?(Integer) ? period_or_index : cohort.periods.index(period_or_index) + 1

        applicable_periods_indexes = [*1..(index)] # We care about periods from 1 to index inclusive, 1-indexed

        incomplete_required_stream_pack_ids_query = PublishedCohortPeriodsStreamLocalePack.select(:locale_pack_id)
            .joins("LEFT JOIN lesson_streams_progress
                        ON lesson_streams_progress.user_id = '#{self.id}'
                        AND lesson_streams_progress.locale_pack_id = #{PublishedCohortPeriodsStreamLocalePack.table_name}.locale_pack_id")
            .where({
                index: applicable_periods_indexes,
                cohort_id: cohort["id"],
                required: true
            }).where("lesson_streams_progress.completed_at IS NULL").distinct

        return incomplete_required_stream_pack_ids_query.count == 0
    end

    def destroy_candidate_relationships
        candidate_relationships.destroy_all
        open_positions.destroy_all
    end

    def get_teammate_relationship(candidate_id)
        relationships_from_team_with_accepted_hiring_manager_status
            .where(candidate_id: candidate_id)
            .where.not(hiring_manager_id: self.id)
            .first
    end

    def active_hiring_teammates
        self.hiring_team_members.where(deactivated: false).where.not(id: self.id)
    end

    def ensure_hiring_team!
        if hiring_team.nil?
            self.hiring_team = HiringTeam.create!(
                title: company_name.blank? ? "Team for #{name}" : company_name,
                domain: email_domain
            )
            save!
        end
    end

    def updated_at_for_json_check
        [
            updated_at,
            career_profile.nil? ? Time.at(0) : career_profile.updated_at,
            last_application.nil? ? Time.at(0) : last_application.updated_at
        ].max
    end

    def check_if_json_changed(&block)
        orig_updated_at = updated_at_for_json_check
        yield

        reloaded = User.where(id: self.id).select('updated_at', 'id').first
        db_updated_at = reloaded.updated_at_for_json_check

        if db_updated_at != orig_updated_at
            return :changed
        else
            return :unchanged
        end
    end

    def []=(key, val)
        if key.to_s == 'avatar_url'
            Raven.capture_in_production(DisallowedBracketAccess) do
                raise DisallowedBracketAccess.new("It is unsafe to set avatar_url with brackets")
            end
            self.avatar_url = val
        else
            super
        end
    end

    def timezone_with_fallback
        timezone || 'America/Los_Angeles'
    end

    # in our UI, we handle a URL that fails to load with JS.
    # but, in some cases, such as in Customer.io emails, we can't do
    # that. So, we use this method to include an avatar URL if its been
    # uploaded to our servers (which we trust) with fallback to a
    # hard-coded placeholder avatar if it either hasn't yet been imported
    # to S3, or because it's failing to load (e.g.: a legacy LinkedIn URL,
    # for example).
    def avatar_url_with_fallback
        if avatar.present?
            avatar_url
        else
            'https://uploads.smart.ly/emails/talent/profile_photo_default.png'
        end
    end

    # It is not ideal that we are overriding the getter for an
    # attribute.  But the other options are either dealing
    # with backwards-compatability issues in the client or
    # having the server-side naming disagree with what gets sent
    # down to the client in the json, so maybe this is the least
    # bad option.
    def avatar_url
        if avatar.present?
            avatar.url
        else
            read_attribute(:avatar_url)
        end
    end

    def avatar_url=(val)
        # For a user with an avatar in s3, when an update is called from the users controller,
        # it will assign avatar_url here with the existing value.  However, we do not want
        # to set the `avatar_url` column in that case, which would blow away the `avatar_id`
        # column.  We only want to update if the new value is different
        #
        # Similarly, if the user is sending up the current avatar's source_url, then
        # it is the same image, although it has been copied to our servers.
        avatar_source_url = self.avatar&.source_url
        if val == self.avatar_url || (avatar_source_url && val == avatar_source_url)
            return val
        end

        # If the avatar has a url from our aws bucket, then it should be saved via
        # s3_assets.  We have had bugs before where race conditions and things ended
        # up having us assign the current url from s3_assets to the avatar_url column.  This
        # causes the file to be deleted from s3 in the copy_avatar task, so it is never
        # what we want.  If someone tries to do it, send a warning to sentry and don't
        # do it
        if val && (val.match('uploads-development.smart.ly') || val.match('uploads-dev.smart.ly') || val.match('uploads.smart.ly') || val.match(ENV['AWS_UPLOADS_BUCKET']))
            Raven.capture_exception("Trying to set avatar_url to a url in smartly\'s aws bucket", {
                extra: {
                    url: val
                }
            })
            return val
        end

        super
    end

    def handle_avatar_changes

        # NOTE:  We do not currently support setting the avatar_url to nil,
        # since setting user.avatar_url=nil does not do anything.

        # If a user has an associated avatar in s3_assets, then the client
        # receives a value for `avatar_url`.
        # If we wanted to allow a user to remove zir avatar in the UI, we might
        # implement that by having the client set `avatar_url` to nil and
        # save the user through the users api.  That wouldn't do anything right
        # now, since the `avatar_url` column is already `nil`.  Doing
        # `user.avatar_url = nil` will not unset the avatar.  This is not
        # a practical issue right now, but could be at some point.

        # When your `avatar_url` gets set, either because
        # the client sent one up that was pulled from linkedin or
        # gravatar, or because one was assigned during oauth login
        # from your Facebook or Google account, we temporarily
        # use that url directly.  However, we schedule a delayed job
        # to copy it into s3, and after that we serve it from there.
        if avatar_url_changed? && avatar_url.present?

            # if you have an avatar saved in s3, remove it
            #(it will be destroyed and deleted from s3 below)
            self.avatar = nil

            # in a delayed job, copy the file at the new `avatar_url`
            # to s3, so you will once again have a value for avatar
            # and `avatar_url` set back to nil
            CopyAvatarJob.perform_later(self.id)
        end

        # If we have an avatar saved in s3, then we do not need to have
        # the `avatar_url` column set
        if self.avatar.present?
            self.write_attribute(:avatar_url, nil)
        end
    end

    def destroy_old_avatar
        # If at any point you change away from an avatar that we
        # have saved in s3, we want to destroy the old one (which
        # will remove it from s3)
        old_id = saved_changes[:avatar_id][0]
        AvatarAsset.where(id: old_id).destroy_all unless old_id.nil?
    end

    def average_assessment_score_best
        cohort_user_progress_records.where(cohort_id: relevant_cohort_id).first&.average_assessment_score_best
    end

    def handle_pref_allow_push_notifications_changes
        ReconcileMobileDeviceJob.perform_later(self.id)
    end

    def update_user_id_verifications_from_hashes(hashes, verifier)
        existing_ids = self.user_id_verifications.map(&:id).to_set
        new_user_id_verification_hashes = hashes.reject { |h| existing_ids.include?(h['id']) }
        new_user_id_verification_hashes.each do |hash|
            raise "Wrong user_id" unless hash['user_id'] == self.id
            raise "update_user_id_verifications_from_hashes can only be used to create verifications with verification_method=verified_by_admin" unless hash['verification_method'] == 'verified_by_admin'
            self.user_id_verifications << UserIdVerification.create!(hash.merge(
                verified_at: Time.now,
                verifier_id: verifier.id,
                verifier_name: verifier.name
            ))
        end
        self.user_id_verifications
    end

    def idology_links_requested_in_last(interval)
        idology_verifications.where("created_at > ?", Time.now - interval).count
    end

    # The student_network_inclusions table contains one row for every user that is either a current
    # 'accepted' student in one of the degree programs or is actively deferred from a degree program
    # (i.e. their last application was deferred from a degree program or they've been 'pre_accepted'
    # into a new degree program). There's logic elsewhere in the app that greatly depends on these
    # conditions being true and assumes that the lack of a record in the student_network_inclusions
    # table indicates as such. If the behvaior of the student_network_inclusions table is ever changed,
    # make sure that the new behavior doesn't break any other dependent logic.
    # See https://trello.com/c/rFSEI4h5 and https://trello.com/c/W5MRKn72.
    def handle_network_inclusion
        result = ActiveRecord::Base.connection.execute(%Q~
            with accepted_cohort_applications AS MATERIALIZED (
                select cohort_applications.id
                    from cohort_applications
                        join cohorts on cohort_applications.cohort_id = cohorts.id
                    where status='accepted'
                        and cohorts.program_type in ('mba', 'emba')
                        and graduation_status != 'failed'
                        and user_id='#{self.id}'
            )
            , active_deferred_applications AS MATERIALIZED (
                SELECT
                    deferred_cohort_applications.user_id
                    , (array_agg(deferred_cohort_applications.id order by deferred_cohort_applications.applied_at desc))[1] as id
                from cohort_applications deferred_cohort_applications
                    join cohorts on deferred_cohort_applications.cohort_id = cohorts.id
                    left join cohort_applications subsequent_applications
                        on subsequent_applications.user_id = deferred_cohort_applications.user_id
                        and subsequent_applications.status not in ('deferred', 'pre_accepted')
                where deferred_cohort_applications.status='deferred'
                    and cohorts.program_type in ('mba', 'emba')
                    and deferred_cohort_applications.user_id='#{self.id}'
                group by deferred_cohort_applications.user_id
                HAVING

                    -- This user only gets access from zir deferrals if the last deferral
                    -- has no expelled, rejected, or accepted applications after it
                    count(subsequent_applications.id) = 0

                    OR
                    (
                        max(deferred_cohort_applications.applied_at)
                        >
                        max(subsequent_applications.applied_at)
                    )
            )
            SELECT
                cohort_applications.id
            from cohort_applications
            where id in (
                select id from accepted_cohort_applications
                UNION
                select id from active_deferred_applications
            )
        ~).to_a[0]

        application_id_that_provides_inclusion = result && result['id']
        cohort_application = self.cohort_applications.reload.detect { |ca| ca.id == application_id_that_provides_inclusion }

        if cohort_application
            ActiveRecord::Base.connection.execute(%Q~
                INSERT INTO student_network_inclusions
                    (user_id, cohort_application_id, program_type, cohort_id, pref_student_network_privacy, status, graduation_status)
                    VALUES
                    (
                        '#{cohort_application.user_id}',
                        '#{cohort_application.id}',
                        '#{cohort_application.program_type}',
                        '#{cohort_application.cohort_id}',
                        '#{self.pref_student_network_privacy}',
                        '#{cohort_application.status}',
                        '#{cohort_application.graduation_status}'
                    )
                ON CONFLICT (user_id) DO UPDATE
                    SET
                        cohort_application_id='#{cohort_application.id}',
                        program_type='#{cohort_application.program_type}',
                        cohort_id='#{cohort_application.cohort_id}',
                        pref_student_network_privacy='#{self.pref_student_network_privacy}',
                        status='#{cohort_application.status}',
                        graduation_status='#{cohort_application.graduation_status}'

            ~)
        else
            StudentNetworkInclusion.where(user_id: self.id).destroy_all
        end
    end

    def pending_pay_per_post?
        !!(self.hiring_application&.status == 'pending' && self.hiring_team&.hiring_plan == HiringTeam::HIRING_PLAN_PAY_PER_POST)
    end

    def net_amount_paid
        self.billing_transactions.reduce(0) { |sum, bt| sum + (bt.amount - bt.amount_refunded) }
    end

    private def hide_relationships_accepted_by_team
        candidate_ids_accepted_by_team = self.relationships_from_team_with_accepted_hiring_manager_status.select(:candidate_id)
        relationships_to_hide = self.candidate_relationships
            .where.not(hiring_manager_status: ['hidden', 'accepted'])
            .where("candidate_id in (#{candidate_ids_accepted_by_team.to_sql})")

        relationships_to_hide.each do |relationship|
            relationship.update!(hiring_manager_status: 'hidden')
        end
    end

    private def hide_teammates_relationships_accepted_by_you
        candidate_ids_accepted_by_you = self.candidate_relationships_with_accepted_hiring_manager_status.select(:candidate_id)
        relationships_to_hide = self.relationships_from_team
            .where.not(hiring_manager_status: ['hidden', 'accepted'])
            .where("candidate_id in (#{candidate_ids_accepted_by_you.to_sql})")

        relationships_to_hide.each do |relationship|
                relationship.update!(hiring_manager_status: 'hidden')
            end
    end

    private def hiring_team_change
        return saved_changes[:hiring_team_id] && saved_changes[:hiring_team_id][1] ||
            changes[:hiring_team_id] && changes[:hiring_team_id][1] ||
            previous_changes[:hiring_team_id] && previous_changes[:hiring_team_id][1]
    end

    private def hide_candidate_positions_interests_if_conflict_on_hiring_team_change
        if !hiring_team_change.nil?

            # Explanation of .where.not(hiring_manager_status: CandidatePositionInterest.hidden_or_rejected_hiring_manager_statuses):
            # It is possible for a hiring manager to take an action on a CandidatePositionInterest in the front-end, so
            # we hide any interests that aren't already 'hidden' or 'rejected' (since these can't be acted upon) if there
            # is a conflict on a hiring_team change.
            query = CandidatePositionInterest
                .joins(:open_position)
                .joins("join users candidate_position_interests_hiring_managers
                            on candidate_position_interests_hiring_managers.id = open_positions.hiring_manager_id
                            and (candidate_position_interests_hiring_managers.id = '#{self.id}' or candidate_position_interests_hiring_managers.hiring_team_id = '#{hiring_team_change}')")
                .joins("join hiring_relationships
                            ON hiring_relationships.candidate_id = candidate_position_interests.candidate_id
                        join users hiring_relationship_hiring_managers
                            on hiring_relationships.hiring_manager_id = hiring_relationship_hiring_managers.id
                            and (hiring_relationship_hiring_managers.id = '#{self.id}' or hiring_relationship_hiring_managers.hiring_team_id = '#{hiring_team_change}')")
                .where("hiring_relationships.hiring_manager_status = ? ", 'accepted')
                .where("hiring_relationships.hiring_manager_id != candidate_position_interests_hiring_managers.id")
                .where("candidate_position_interests.hiring_manager_status not in (?)", CandidatePositionInterest.hidden_or_rejected_hiring_manager_statuses)

            query.each do |interest|
                interest.update!(hiring_manager_status: "hidden")
            end
        end
    end

    # since we include the name and nickname in the career profile
    # fulltext search, and since the privacy setting matters for that,
    # we need to update the career profile fulltext search when those
    # things change here
    private def update_career_profile_fulltext
        career_profile&.update_fulltext
    end

    private def should_update_career_profile_fulltext?
        !!(
            saved_changes[:name] ||
            saved_changes[:nickname] ||
            saved_changes[:pref_student_network_privacy] ||
            saved_changes[:can_edit_career_profile]
        )
    end

    private def validate_hiring_team_owner
        old_hiring_team_id = changes[:hiring_team_id] && changes[:hiring_team_id][0]
        if old_hiring_team_id && HiringTeam.find(old_hiring_team_id).owner == self
            errors.add(:hiring_team_id, "cannot be changed away from a team that the user owns")
        end
    end

    private def raise_invalid_error_if_not_destroyable
        if hiring_team&.owner == self
            errors.add(:hiring_team_id, "has this user as the owner so the user cannot be destroyed.")
            raise ActiveRecord::RecordInvalid.new(self)
        end
    end

    private def should_deactivate_user?
        return will_save_change_to_deactivated?(to: true, from: false)
    end

    private def should_reactivate_user?
        return will_save_change_to_deactivated?(to: false, from: true)
    end

    # Deactivate a user by
    #  1. Removing access to the Career Network
    #  2. Removing access to the Student Network
    #  3. Set notification preferences to false so they get no emails
    #  4. Set push notification preference to false so they get no push notifications
    private def deactivate_user
        User::KEYS_TO_DEACTIVATE_OR_REACTIVATE.each do |key|
            type = User.columns_hash[key].type

            if type == :boolean
                self.send("#{key}=", false)
            elsif type == :text && key == "pref_student_network_privacy"
                self.send("#{key}=", "hidden")
            elsif type == :text && key == "notify_candidate_positions_recommended"
                self.send("#{key}=", "never")
            else
                raise "Unexpected type/key: #{type} - #{key}"
            end
        end
    end

    # Reactivate a user by
    #  1. Grabbing the last record in users_versions before their deactivation
    #  2. Restoring the attributes that were manipulated by their deactivation
    private def reactivate_user
        version = User::Version
            .where(id: self.id, deactivated: false, operation: ['U', 'I'])
            .order("version_created_at desc")
            .first

        # Gracefully return if there is no corresponding version to restore from
        return unless !version.nil?

        User::KEYS_TO_DEACTIVATE_OR_REACTIVATE.each do |key|
            self.send("#{key}=", version.send(key))
        end
    end

    # We decided that it would be best to delete LessonProgress, Lesson::StreamProgress,
    # records for a particular user before that
    # user is destroyed.
    private def delete_user_progresses

        # NOTE: we are purposefully calling .delete_all for two reasons:
        #  1. It's fast and these tables tend to have lots of records.
        #  2. It bypasses the object's destruction lifecycle which means
        #     lesson:reset and lesson:stream:reset won't get logged.
        self.lesson_progresses.delete_all
        self.lesson_streams_progresses.delete_all
        self.project_progresses.delete_all
    end

    private def delete_user_data
        self.destroy_check_in_with_inactive_user_job
        DeleteAssociatedUserDataJob.perform_later(self.id, self.career_profile&.id)
    end

    # see devise_token_auth-1.0.0/app/models/devise_token_auth/concerns/user.rb
    private def will_save_change_to_email_even_though_stupid_devise_token_auth_has_overridden_the_method?
        changes[:email]
    end

    # This ensures that if a user changes their fallback_program_type in the
    # program_choice_form_dir to something different than what their current
    # institution state shows then we'll proactively ensure the proper state.
    # Initially we only did this in the cohort_application before_save, but
    # then realized that changing the fallback_program_type affects messaging
    # elsewhere in the app, thus we need to make sure branding, which is based
    # on active_institution, properly reflects.
    private def ensure_institution_state_for_fallback_program_type
        fallback_institution = Cohort.promoted_cohort(self.fallback_program_type)&.institution
        if fallback_institution.present?
            self.ensure_institution(fallback_institution)
            self.active_institution = fallback_institution
        end
    end

    # We had a situation where a client tried to send america/new_york
    # (no capitals) when registering, which is not valid to Rails and
    # caused issues with our time.rb code that calls in_time_zone, which
    # actually seeped into some job code and essentially DoS'ed ourselves.
    # So just unset a timezone that is present but invalid to Rails.
    private def unset_timezone_if_present_and_invalid
        if self.timezone.present? && ActiveSupport::TimeZone[self.timezone].nil?
            Raven.capture_message('Unsetting an invalid timezone',
                extra: {
                    user_id: self.id,
                    timezone: self.timezone
                }
            )
            self.timezone = nil
        end
    end
end
