# == Schema Information
#
# Table name: published_cohort_admission_rounds
#
#  cohort_id            :uuid
#  name                 :text
#  index                :bigint
#  program_type         :text
#  isolated_network     :boolean
#  application_deadline :datetime
#  decision_date        :datetime
#  cohort_start_date    :datetime
#  promoted             :boolean
#

class AdmissionRound < ApplicationRecord

    self.table_name = 'published_cohort_admission_rounds'

    belongs_to :cohort

    # separate method for mocking
    def self.now
        Time.now
    end

    def self.cache_key(program_type)
        content_views_refresh_updated_at = ContentViewsRefresh.debounced_value
        "promoted_admission_round/#{program_type}/#{content_views_refresh_updated_at}"
    end

    def self.promoted_without_cache(program_type)
        self.where(program_type: program_type, promoted: true).first
    end

    def self.promoted_with_cache(program_type)
        # NOTE: we also bust the cache in `self.promoted` if the application deadline
        # is in the past
        SafeCache.fetch(self.cache_key(program_type)) do 
            admission_round = self.promoted_without_cache(program_type)

            # by referencing published_versions, we ensure that published_version is
            # in the cache, so when we call `promoted_admission_round.cohort.published_version`
            # in `Cohort.promoted_cohort` we do not have to make another query
            admission_round&.cohort&.published_versions.to_a
            admission_round
        end
    end

    # Note: we go to some trouble here to cache this result because we're querying a view that has
    # no indices; on prod, we saw this query taking up a measurable percentage of the DB CPU because
    # it runs as part of so many requests.
    def self.promoted(program_type)
        now = self.now
        round = self.promoted_with_cache(program_type)

        # Note: if we had cached a nil round, we bust the cache and try again every time until
        # we get a valid round that we can use to compare dates against
        if !round || round.application_deadline <= now || round.cohort_start_date <= now
            SafeCache.delete(self.cache_key(program_type))
            round = self.promoted_with_cache(program_type)
        end

        round
    end

    def self.promoted_admission_rounds
        program_types = Cohort::ProgramTypeConfig.program_types.select { |program_type| Cohort::ProgramTypeConfig[program_type].supports_admission_rounds? }
        program_types.map { |program_type| self.promoted(program_type) }.compact
    end

    # We support the option of passing in the promoted_admission_rounds explicitly because this
    # method is often times used in loops and if the promoted_admission_rounds aren't passed in
    # we'll be making a bunch of unnecessary queries.
    def self.promoted_rounds_event_attributes(timezone, promoted_admission_rounds = nil)
        promoted_admission_round_event_attributes = {}
        (promoted_admission_rounds || self.promoted_admission_rounds).each do |promoted_admission_round|
            promoted_admission_round_event_attributes[:"#{promoted_admission_round.program_type}"] = promoted_admission_round.event_attributes(timezone)
        end
        {
            promoted_admission_rounds: promoted_admission_round_event_attributes
        }
    end

    def midpoint
        unless defined? @midpoint
            previous_round = AdmissionRound.where(program_type: self.program_type)
                                .where('application_deadline < ?', self.application_deadline)
                                .reorder('cohort_start_date desc', 'application_deadline desc')
                                .first
            if previous_round
                @midpoint = previous_round.application_deadline + (application_deadline - previous_round.application_deadline) / 2
            else
                @midpoint = nil
            end
        end
        @midpoint
    end

    def published_cohort
        @published_cohort ||= cohort.published_version
    end

    def cohort_has_multiple_rounds?
        cohort.admission_rounds.size > 1
    end

    def event_attributes(timezone)
        {
            # Removing decision date because this is not relevant at
            # the round level anymore.  Only the last one matters.
            # See Cohort#decision_date and https://trello.com/c/yYJJqO7n/2442-chore-mode-decision-date-from-admission-round-to-cohort
            application_deadline: application_deadline.relative_to_threshold(timezone: timezone).to_timestamp,
            cohort_start_date: published_cohort.start_date.to_timestamp,
            cohort_title: published_cohort.title,
            last_round_in_cycle: last_round_in_cycle?
        }
    end

    # is this the last round for this cohort?
    def last_round_in_cycle?
        unless defined? @last_round_in_cycle
            @last_round_in_cycle = AdmissionRound.where(cohort_id: cohort_id).maximum(:index) == index
        end
        @last_round_in_cycle
    end

    # the beginning of the first round for this cohort
    def start_of_cycle
        unless defined? @start_of_cycle
            last_round_of_previous_cycle = AdmissionRound.where("cohort_start_date < ?", cohort.start_date)
                    .where(program_type: program_type)
                    .reorder("cohort_start_date desc, application_deadline desc")
                    .first
            @start_of_cycle = last_round_of_previous_cycle && last_round_of_previous_cycle.application_deadline
        end
        @start_of_cycle
    end

end
