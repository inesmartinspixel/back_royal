# == Schema Information
#
# Table name: cohort_applications
#
#  id                                 :uuid             not null, primary key
#  created_at                         :datetime         not null
#  updated_at                         :datetime         not null
#  applied_at                         :datetime         not null
#  cohort_id                          :uuid             not null
#  user_id                            :uuid             not null
#  status                             :text             default("pending"), not null
#  accepted_at                        :datetime
#  graduation_status                  :text             default("pending")
#  expelled_at                        :datetime
#  disable_exam_locking               :boolean          default(FALSE)
#  skip_period_expulsion              :boolean          default(FALSE)
#  final_score                        :float
#  completed_at                       :datetime
#  graduated_at                       :datetime
#  total_num_required_stripe_payments :integer
#  registered                         :boolean          default(FALSE), not null
#  retargeted_from_program_type       :text
#  should_invite_to_reapply           :boolean
#  admissions_decision                :text
#  shareable_with_classmates          :boolean
#  notes                              :text
#  scholarship_level                  :json
#  num_charged_payments               :integer
#  num_refunded_payments              :integer
#  airtable_record_id                 :string
#  stripe_plan_id                     :text
#  airtable_base_key                  :string
#  locked_due_to_past_due_payment     :boolean          default(FALSE), not null
#  cohort_slack_room_id               :uuid
#  registered_at                      :datetime
#  registered_early                   :boolean          default(FALSE)
#  allow_early_registration_pricing   :boolean          default(FALSE)
#  stripe_plans                       :json             is an Array
#  scholarship_levels                 :json             is an Array
#  refund_issued_at                   :datetime
#  refund_attempt_failed              :boolean          default(FALSE), not null
#  original_program_type              :text             not null
#  invited_to_interview_at            :datetime
#  project_score                      :float
#  meets_graduation_requirements      :boolean
#  rubric_round_1_tag                 :string
#  rubric_round_1_reason_mba_invite   :string           is an Array
#  rubric_round_1_reason_mba_reject   :string           is an Array
#  rubric_round_1_reason_emba_invite  :string           is an Array
#  rubric_round_1_reason_emba_reject  :string           is an Array
#  rubric_final_decision              :string
#  rubric_final_decision_reason       :string
#  rubric_interviewer                 :string
#  rubric_interview                   :string
#  rubric_interview_scheduled         :boolean
#  rubric_interview_recommendation    :string
#  rubric_reason_for_declining        :string           is an Array
#  rubric_likelihood_to_yield         :string
#  rubric_risks_to_yield              :string           is an Array
#  rejected_after_pre_accepted        :boolean          default(FALSE), not null
#  rubric_inherited                   :boolean          default(FALSE), not null
#  rubric_interview_date              :datetime
#  rubric_motivation_score            :text
#  rubric_contribution_score          :text
#  rubric_insightfulness_score        :text
#  payment_grace_period_end_at        :datetime
#

class CohortApplication < ApplicationRecord
    extend CohortApplication::StatusGroupsMixin
    include CohortApplication::DecisionConcern

    class CurrencyFormatter
        include ActionView::Helpers::NumberHelper
    end

    class UnauthorizedError < RuntimeError; end

    attr_accessor :user_expelled_due_to_period_expulsion_action, :just_used_deferral_link

    def self.payment_info_attrs
        return %w(
            stripe_plans stripe_plan_id scholarship_levels scholarship_level total_num_required_stripe_payments
            num_charged_payments num_refunded_payments registered registered_early payment_grace_period_end_at
            locked_due_to_past_due_payment
        )
    end

    default_scope -> {order(:created_at)}

    # for explanation of why we order by version_created_at/ updated_at,
    # see explanation above the `<=>`` method in VersionMixin
    has_many :versions, -> {order('version_created_at', 'updated_at')}, :class_name => "#{self.name}::Version", :foreign_key => :id


    before_save :ensure_institution_state, if: Proc.new { self.acceptable_status? }

    # IMPORTANT: These after_create and after_update callbacks to reset the user's cohort_applications
    # association need to executed before any other callbacks to ensure that the user's cohort_applications
    # association knows about the newly created or updated cohort application.
    after_create :reset_user_cohort_applications_association
    after_update :reset_user_cohort_applications_association

    after_save :handle_network_inclusion, :if =>  Proc.new { saved_change_to_graduation_status? || saved_change_to_status? || saved_change_to_cohort_id? }
    after_destroy :handle_network_inclusion

    belongs_to :cohort, :primary_key => 'id', :foreign_key => 'cohort_id'
    belongs_to :user

    before_create :set_notify_candidate_positions_preferences_on_first_submission
    before_validation :set_stripe_plans_and_scholarship_levels
    before_save :before_save
    before_validation :reset_payment_information_if_needed
    before_save :accept_on_registration, if: :supports_accept_on_registration?
    before_save :set_invited_to_interview_at, if: :should_set_invited_to_interview_at?
    before_save :set_rejected_after_pre_accepted, if: :status_changed?
    before_validation :transfer_payment_info_from_previous_application, :on => :create

    # using before_save here because it can change things
    before_save :handle_invite_to_reapply

    before_save :set_final_score, if: Proc.new {status_changed? || cohort_id_changed?}

    before_save :handle_payment_grace_period

    before_update :reset_decision_if_reapplying_to_promotable_program # note: this should be before the update_in_airtable

    after_commit :identify_user, if: :should_identify_after_commit?

    # Fix accidental "Reject for No Interview" decision if applicable.
    # Note that this should be scheduled after the triggering transaction is commited so
    # as not to schedule a perform_admissions_decision job.
    # See https://trello.com/c/8N32IZaX and https://trello.com/c/5VzkKady
    after_commit :fix_reject_for_no_interview_if_conducted

    after_save :create_status_changed_event
    after_save :create_registered_changed_event
    after_save :create_graduation_status_changed_event
    after_save :remove_from_career_network, if: :just_failed?
    after_save :create_retargeted_changed_event
    after_save :clear_subscription_on_zero_payments
    after_save :update_career_profile_fulltext, if: :should_update_career_profile_fulltext?

    after_save :add_user_to_extra_courses_access_group
    after_save :destroy_all_pending_lock_content_access_jobs, if: :should_destroy_all_pending_lock_content_access_jobs?
    after_save :log_cohort_content_locked_event
    after_save :verify_lock_content_access_job, if: Proc.new { payment_grace_period_end_at.present? }

    after_save :enqueue_check_in_with_inactive_user_job, if: :should_enqueue_check_in_with_inactive_user_job?
    after_save :destroy_check_in_with_inactive_user_job, if: :should_destroy_check_in_with_inactive_user_job?

    after_save :queue_ensure_enrollment_agreement_job_if_necessary

    # do this on create or update, in case we create an accepted application
    # in the UI
    after_save :transfer_progress_from_deferred_or_expelled_cohort, if: :saved_change_to_accepted_or_pre_accepted?
    after_save :handle_id_verifications_migration, if: :saved_change_to_accepted_or_pre_accepted?

    after_save :queue_ensure_stripe_customer_job, if: Proc.new {
        self.saved_change_to_accepted_or_pre_accepted? && self.program_type_config.supports_payments?
    }

    after_save :sync_fallback_program_type, if: :should_sync_fallback_program_type?

    after_save :queue_update_cohort_status_changes
    after_destroy :delete_cohort_status_changes

    after_create :log_peer_recommendation_request_events

    # Airtable related callbacks
    before_create :set_original_program_type
    after_create :create_in_airtable

    # If we ever decide to send the slack room id to airtable we need to tweak this unless.  But that
    # will change the batch saving through CohortApplication@batch_update.
    # Also note that the unless proc is separate from should_sync_with_airtable? because we only
    # want those checks to occur when the cohort_application itself is being updated.
    after_update :update_in_airtable, unless: Proc.new {
        only_changing_cohort_slack_room_id? ||
        only_changing_airtable_base_key? ||
        only_changing_rubric_fields? ||
        inheriting_rubric?
    }
    after_destroy :destroy_in_airtable
    after_destroy :destroy_all_pending_lock_content_access_jobs
    after_destroy :destroy_check_in_with_inactive_user_job

    validate :valid_status
    validates :graduation_status, inclusion: { in: self.graduation_statuses, message: "%{value} is not a valid graduation status" } # FIXME: localize?
    validates_presence_of :cohort_slack_room_id, if: :requires_slack_room_id?
    validate :validate_pending_graduation_status_if_not_accepted
    validate :validate_payment_counts
    validates_not_nil :disable_exam_locking # we were not able to add a not null constraint because this column was added later

    validate :validate_stripe_details
    before_commit :validate_some_other_stripe_details_before_commit

    delegate :program_type, :to => :published_cohort_or_cohort, :allow_nil => true
    delegate :career_profile, :to => :user
    delegate :supports_accept_on_registration?, :supports_deferral?, :supports_reapply?, :supports_airtable_sync?, :to => :program_type_config

    def self.create_from_hash!(hash, meta = {}, user = nil)
        # Automatically retries in the event of ActiveRecord::PreparedStatementCacheExpired
        RetriableTransaction.transaction do
            hash = hash.unsafe_with_indifferent_access
            instance = new(
                applied_at: Time.now,
                cohort_id: hash['cohort_id'],
                user_id: hash['user_id']
            )

            instance.merge_hash(hash, meta)
            instance.user = user if user
            instance.save!
            instance
        end
    end

    def self.update_from_hash!(hash, meta = {}, user = nil)
        # Automatically retries in the event of ActiveRecord::PreparedStatementCacheExpired
        RetriableTransaction.transaction do
            hash = hash.unsafe_with_indifferent_access
            instance = find_by_id(hash[:id])

            if instance.nil?
                raise ActiveRecord::RecordNotFound.new("Couldn't find #{self.name} with id=#{hash[:id]}")
            end

            instance.merge_hash(hash, meta)
            instance.user = user if user
            instance.save!
            instance
        end

    end

    def merge_hash(hash, meta = {})
        meta ||= {}
        hash = hash.unsafe_with_indifferent_access
        auto_assign_slack_room = false

        if meta[:is_admin]
            status_keys = (
                %w(status graduation_status cohort_id skip_period_expulsion
                    disable_exam_locking retargeted_from_program_type notes
                    cohort_slack_room_id allow_early_registration_pricing
                ) + self.class.payment_info_attrs
            )

            # If the status has been updated to 'rejected_or_expelled' by an admin, we should check
            # to see if the application should be considered 'rejected' or 'expelled', based on the
            # user's application history.
            if hash['status'] == 'rejected_or_expelled'
                hash['status'] = self.rejected_or_expelled
            end

            # If the client set the value of cohort_slack_room_id to AUTO_ASSIGN, then
            # we go ahead and do an auto-assignment.  I don't love this solution.  Normally,
            # I would prefer to set something like this in the metadata of the api, rather than have the client
            # set something on the model that is not just simply the actual value expected for the property.
            # But it would have been a lot more work, both in the UX code and in the API to do that,
            # so going with this approach.
            if hash['cohort_slack_room_id'] == 'AUTO_ASSIGN'
                status_keys.delete('cohort_slack_room_id')
                auto_assign_slack_room = true
            end
        else
            status_keys = %w(status)

            # We don't want to allow learners to change their status UNLESS they're re-applying and are allowed to
            # or are setting a paid cert application to pre_accepted or accepted.
            # We also allow no-ops that are simply setting the status to what it already is.
            if hash['status'].present? && hash['status'] != self['status']
                unless published_cohort.user_can_change_status?(self.status, hash['status'])
                    raise UnauthorizedError
                end
            end
        end

        status_keys.each do |key|
            if hash.key?(key)
                val =  hash[key]
                val = nil if val.is_a?(String) && val.blank?

                if ['payment_grace_period_end_at'].include?(key) && val.is_a?(Integer)
                    val = Time.at(val)
                end

                self[key] = val
            end
        end

        if meta[:update_applied_at]
            self.applied_at = Time.now
        end

        if meta[:user_expelled_due_to_period_expulsion_action]
            self.user_expelled_due_to_period_expulsion_action = true
        end

        if meta[:is_admin]
            self.set_manual_admin_decision_if_applicable
        end

        # The client is responsible for telling the server if it should auto-assign the application to a
        # Slack room, but it's possible for the client to have done this even though the cohort's program
        # type doesn't support Slack rooms, in which case we just avoid the auto-assignment altogether.
        # Note that we do the check to see if the program type supports_slack_rooms? here, after the
        # rest of the attrs have been set on the record because the cohort_id may have changed.
        if auto_assign_slack_room && self.program_type_config.supports_slack_rooms?
            Cohort::SlackRoomAssignmentJob.assign_one_application(self)
        end

        self
    end

    def set_manual_admin_decision_if_applicable
        status_changes = self.changes['status']
        return if status_changes.nil?

        old_status = status_changes[0]
        new_status = status_changes[1]
        if old_status != new_status && new_status != "pending"
            self.admissions_decision = 'manual_admin_decision'
        end
    end

    # We are currently basing non-fee values off single-payment standard plan structure. For applications to Cohorts
    # that don't support `once` interval plans, we'll use `monthly` since that aligns historically
    def default_stripe_plan
        single_payment_plan = self.stripe_plans&.detect { |plan| plan['frequency'] == 'once' }
        monthly_plan = self.stripe_plans&.detect { |plan| plan['frequency'] == 'monthly' }
        single_payment_plan || monthly_plan
    end

    def applicable_registration_period

        return "standard" if (!published_cohort.supports_early_registration_deadline?)

        return "early" if self.registered_early == true

        # if this user has been deferred and didn't originally have the early registration discount,
        # then they do not get it now
        return "standard" if !self.registered_early && self.has_made_payments?

        return (
            published_cohort.early_registration_deadline.to_timestamp > Time.now.to_timestamp ||
            self.registered_early || allow_early_registration_pricing
        ) ? "early" : "standard"
    end

    # separate method for test mocking
    def connection_execute(sql)
        ActiveRecord::Base.connection.execute(sql)
    end

    # when processing a reject decision, if someone has been deferred in the past, we want to
    # mark them as expelled rather than rejected (it's a litte more complex than that. see code/comments below)
    def rejected_or_expelled

        # It is really unlikely anyway for a cohort application that does not support deferral (like business cert) to
        # be for a user who was previously deferred.  But, we might as well abort early anyway in this case.
        return 'rejected' unless supports_deferral?

        currently_in_network = user.student_network_inclusion.present?

        # If the user is currently in the network, this tells us that the user has
        # been previously deferred or accepted and is still in good standing (not rejected or expelled since)
        # then we need to find the last MBA or EMBA cohort that was applied to since the last expulsion or
        # rejection and check its acceptance_date.
        # Really the only reason we have to do that check rather than just returning expelled is the edge case
        # where someone defers but then gets rejected in the same cycle; they'd still show up in the view
        # but really should be marked as rejected.
        if currently_in_network
            # Grab all the mba or emba applications and order them by applied_at desc
            mba_or_emba_applications = self.user.cohort_applications
                .joins(:cohort)
                .where(cohorts: { program_type: ['mba', 'emba'] })
                .order(applied_at: :desc)
                .to_a

            # Then step through looking for the next one being expelled or nil at each iteration
            # to determine when we found the applicable one to use for the acceptance_date check.
            application_to_use = nil
            mba_or_emba_applications.each_with_index do |app, i|
                next_app = mba_or_emba_applications[i + 1]
                if ['expelled', 'rejected', nil].include?(next_app&.status)
                    application_to_use = app
                    break
                end
            end

            return application_to_use.published_cohort.acceptance_date > Time.now ? 'rejected' : 'expelled'
        elsif self.graduation_status == 'failed'
            return 'expelled'
        else
            return 'rejected'
        end
    end

    def applicable_coupon_info(plan_id = nil)
        stripe_plan_id = plan_id || self.stripe_plan_id || (default_stripe_plan && default_stripe_plan['id'])
        coupon_info = self.scholarship_level && stripe_plan_id &&
            self.scholarship_level[applicable_registration_period][stripe_plan_id]

        # https://trello.com/c/8DYOzgwC
        if coupon_info.nil?
            Raven.capture_exception('applicable_coupon_info is nil',
                level: 'warning',
                extra: {
                    plan_id: plan_id,
                    self_stripe_plan_id: self.stripe_plan_id,
                    default_stripe_plan: default_stripe_plan,
                    stripe_plan_id: stripe_plan_id,
                    scholarship_level: self.scholarship_level,
                    applicable_registration_period: applicable_registration_period,
                }
            )
            if Rails.env.development?
                raise 'applicable_coupon_info is nil'
            end
        end
        coupon_info
    end

    def payment_past_due?

        # This check is arguably unnecessary because it should not be possible for
        # has_paid_in_full? to be true when any of the following checks are also true.
        # But, if a user had a past_due subscription and then made a payment outside of
        # stripe and we haven't (yet) deleted the subscription, then has_paid_in_full? could
        # be true when `primary_subscription&.past_due` is also true.
        if has_paid_in_full?
            return false
        end

        # If the user has a subscription, we use the past_due value
        # on the subscription
        if primary_subscription
            return primary_subscription.past_due
        end

        # Users will be in this state after LockContentAccessDueToPastDuePayment runs.  That
        # job deletes payment_grace_period_end_at, so we have to check locked_due_to_past_due_payment
        # after the job runs.  We can no longer check payment_grace_period_end_at (FIXME? maybe we should
        # not delete payment_grace_period_end_at, and maybe we actually don't need the locked_due_to_past_due_payment flag.
        # we can just check if Time.now > payment_grace_period_end_at)
        if locked_due_to_past_due_payment?
            return true
        end

        # there isn't really any way to get into this state for more than
        # a few seconds, because once you're in this state, the LockContentAccessDueToPastDuePayment job
        # will set locked_due_to_past_due_payment=true and payment_grace_period_end_at=nil
        if past_payment_grace_period?
            return true
        end

        return false
    end

    def past_payment_grace_period?
        !!(self.payment_grace_period_end_at && self.payment_grace_period_end_at <= Time.now)
    end

    def primary_subscription
        # (Nate and Isaac moved this logic here from in_good_standing? in June 2020.  We suspect
        # this check might no longer be necessary, but didn't want to mess with it)

        # check that we have an primary_subscription that matches our plan
        _primary_subscription = user.primary_subscription
        if !self.stripe_plans || (_primary_subscription && !self.stripe_plan_ids.include?(_primary_subscription.stripe_plan_id))
            _primary_subscription = nil
        end
        _primary_subscription
    end

    def warn_if_switching_from_mba_emba?
        mba_or_emba? && self.status == 'pending' && ['Invite to Interview', 'Invite to Convert to EMBA'].include?(self.admissions_decision)
    end

    def converted_pending_application_to_emba_upon_invitation?
        self.program_type == 'emba' && self.status == 'pending' && self.admissions_decision == 'Invite to Convert to EMBA'
    end

    def can_convert_to_emba?
        self.program_type != 'emba' && self.status == 'pending' && self.admissions_decision == 'Invite to Convert to EMBA'
    end

    # Determines if the cohort application converted to the EMBA after it was invited to do so by
    # looking at the cohort application versions and comparing each version's program_type
    # and admissions_decision values.
    def converted_to_emba?
        program_type_and_admissions_decision_pairs = CohortApplication::Version
            .includes(:cohort) # allows us to pluck the program_type
            .where(id: self.id)
            .order(:version_created_at)
            .pluck(:program_type, :admissions_decision)

        converted_to_emba = false
        previous_program_type = nil
        previous_admissions_decision = nil

        program_type_and_admissions_decision_pairs.each do |pair|
            current_program_type = pair[0]
            current_admissions_decision = pair[1]

            # If we see that the program_type changed to 'emba' and the admissions_decision
            # remained 'Invite to Convert to EMBA', we can deduce that the cohort application
            # was successfully converted to the EMBA upon invitation.
            converted_to_emba = previous_program_type != 'emba' &&
                current_program_type == 'emba' &&
                previous_admissions_decision == 'Invite to Convert to EMBA' &&
                current_admissions_decision == previous_admissions_decision

            break if converted_to_emba

            previous_program_type = current_program_type
            previous_admissions_decision = current_admissions_decision
        end

        converted_to_emba
    end

    def self.includes_needed_for_json_eager_loading
        [
            {:user => [
                {:career_profile => [
                    {:education_experiences => [:educational_organization]},
                    {:work_experiences => [:professional_organization]},
                    :peer_recommendations
                ]},
                :subscriptions,
                :billing_transactions
            ]}
        ]
    end

    def as_json(opts = {})
        hash = {
            "id" => self.id,
            "updated_at" => self.updated_at.to_timestamp,
            "applied_at" => self.applied_at.to_timestamp,
            "cohort_id" => self.cohort_id,
            "cohort_application_deadline" => published_cohort.application_deadline.to_timestamp,
            "cohort_name" => published_cohort.name,
            "cohort_title" => published_cohort.title,
            "cohort_start_date" => published_cohort.start_date.to_timestamp,
            "accepted_at" => self.accepted_at.to_timestamp,
            "status" => self.status,
            "graduation_status" => self.graduation_status,
            "program_type" => self.program_type,
            "skip_period_expulsion" => self.skip_period_expulsion,
            "disable_exam_locking" => self.disable_exam_locking,
            "completed_at" => self.completed_at.to_timestamp,
            "graduated_at" => self.graduated_at.to_timestamp, # we should probably do this for all optional dates
            "retargeted_from_program_type" => self.retargeted_from_program_type,
            "registered" => self.registered,
            "cohort_program_guide_url" => published_cohort.program_guide_url,
            "scholarship_level" => self.scholarship_level,
            "stripe_plan_id" => self.stripe_plan_id,
            "num_charged_payments" => self.num_charged_payments,
            "num_refunded_payments" => self.num_refunded_payments,
            "total_num_required_stripe_payments" => self.total_num_required_stripe_payments,
            "in_good_standing" => !self.payment_past_due?,
            "locked_due_to_past_due_payment" => self.locked_due_to_past_due_payment,
            "shareable_with_classmates" => self.shareable_with_classmates,
            "stripe_plans" => self.stripe_plans,
            "scholarship_levels" => self.scholarship_levels,
            "invited_to_interview" => self.invited_to_interview?,
            "invited_to_interview_url" => self.invited_to_interview_url,
            "cohort_slack_room_id" => self.cohort_slack_room_id,
            "registered_early" => self.registered_early,
            "allow_early_registration_pricing" => self.allow_early_registration_pricing, # really this means "force"
            "cohort_early_registration_deadline" => published_cohort.early_registration_deadline.to_timestamp,
            "warn_if_switching_from_mba_emba" => self.warn_if_switching_from_mba_emba?,
            "converted_pending_application_to_emba_upon_invitation" => self.converted_pending_application_to_emba_upon_invitation?,
            "can_convert_to_emba" => self.can_convert_to_emba?,
            "rejected_after_pre_accepted" => self.rejected_after_pre_accepted,
            "decision_date" => published_cohort&.decision_date.to_timestamp,
            "payment_grace_period_end_at" => payment_grace_period_end_at.to_timestamp
        }

        if opts[:is_admin]
            hash["notes"] = self.notes

            # in the gradebook admin, if a project score is updated, we need to get the new
            # final score
            hash["final_score"] = self.final_score
            hash["meets_graduation_requirements"] = self.meets_graduation_requirements
        end

        if opts[:zapier] || opts[:airtable]

            # Cohort Application
            # See https://trello.com/c/Hb3wIbgO that talks about the ideal way to handle dates
            hash["submit_date"] = self.applied_at
            hash["registered_date"] = self.registered_at
            hash["invited_to_interview_date"] = self.invited_to_interview_at
            hash["cohort_name"] = published_cohort&.name
            hash["admissions_decision"] = self.admissions_decision
            hash["original_program_type"] = self.original_program_type

            # Currently only needed for Airtable and it results in an extra query even when preloading,
            # so for now it's only included in the as_json hash if Airtable is involved.
            hash["converted_to_emba"] = self.converted_to_emba?

            # User
            hash["user_id"] = user.id
            hash["name"] = user.name
            hash["nickname"] = user.nickname
            hash["email"] = user.email
            hash["city"] = user.city
            hash["state"] = user.state
            hash["country"] = user.country
            hash["phone"] = user.phone
            hash["birthdate"] = user.birthdate
            hash["sex"] = user.sex
            hash["race"] = user.race
            hash["how_did_you_hear_about_us"] = user.how_did_you_hear_about_us
            hash["anything_else_to_tell_us"] = user.anything_else_to_tell_us

            # note: we are not preloading this right now, so this is an extra query for each user
            hash["completed_lessons"] = user.completed_lesson_locale_pack_ids.size

            hash["prior_applications_string"] = user.cohort_applications
                .reorder("applied_at ASC")
                .where("cohort_applications.applied_at < ?", self.applied_at)
                .to_a
                .map { |application| "#{application.published_cohort.name} - #{application.status}" }
                .join(", ")
                .upcase

            hash["prior_applications_decisions_string"] = prior_applications_decisions_string

            if user.deferred_info.last_deferred_application
                hash["deferred_and_good_standing"] = (user.deferred_info.statuses_after_deferred - ['deferred', 'pre_accepted', 'pending']).empty?
            end

            hash["editor_url"] = "https://smart.ly/admin/users/applicants?id=#{user.id}"

            if user.career_profile.career_profile_active?
                hash["deep_link_url"] = user.career_profile.deep_link_url
            end

            # Career Profile
            if career_profile
                hash["start_date"] = career_profile.created_at
                hash["updated_at"] = [career_profile.updated_at, self.updated_at].max
                hash["score_on_gmat"] = career_profile.score_on_gmat
                hash["score_on_act"] = career_profile.score_on_act
                hash["score_on_sat"] = career_profile.score_on_sat
                hash["sat_max_score"] = career_profile.sat_max_score
                hash["score_on_gre_verbal"] = career_profile.score_on_gre_verbal
                hash["score_on_gre_quantitative"] = career_profile.score_on_gre_quantitative
                hash["score_on_gre_analytical"] = career_profile.score_on_gre_analytical
                hash["primary_reason_for_applying"] = career_profile.primary_reason_for_applying
                hash["short_answer_greatest_achievement"] = career_profile.short_answer_greatest_achievement
                hash["short_answer_why_pursuing"] = career_profile.short_answer_why_pursuing
                hash["short_answer_use_skills_to_advance"] = career_profile.short_answer_use_skills_to_advance
                hash["short_answer_ask_professional_advice"] = career_profile.short_answer_ask_professional_advice
                hash["li_profile_url"] = career_profile.li_profile_url
                hash["salary_range"] = career_profile.salary_range
                hash["career_profile_active"] = career_profile.career_profile_active?
                hash["willing_to_relocate"] = career_profile.willing_to_relocate
                hash["open_to_remote_work"] = career_profile.open_to_remote_work
                hash["authorized_to_work_in_us "] = career_profile.authorized_to_work_in_us
                hash["interested_in_joining_new_company"] = career_profile.interested_in_joining_new_company
                hash["locations_of_interest"] = career_profile.locations_of_interest
                hash["employment_types_of_interest"] = career_profile.employment_types_of_interest
                hash["company_sizes_of_interest"] = career_profile.company_sizes_of_interest
                hash["primary_areas_of_interest"] = career_profile.primary_areas_of_interest
                hash["profile_feedback"] = career_profile.profile_feedback
                hash["feedback_last_sent_at"] = career_profile.feedback_last_sent_at
                hash["last_confirmed_at_by_student"] = career_profile.last_confirmed_at_by_student
                hash["last_updated_at_by_student"] = career_profile.last_updated_at_by_student
                hash["last_confirmed_at_by_internal"] = career_profile.last_confirmed_at_by_internal
                hash["sufficient_english_work_experience"] = career_profile.sufficient_english_work_experience
                hash["english_work_experience_description"] = career_profile.english_work_experience_description
                hash["has_no_formal_education"] = career_profile.has_no_formal_education
                hash["resume_url"] = career_profile.resume&.file&.url
                hash["place_details_formatted_address"] = career_profile.place_details && career_profile.place_details["formatted_address"]
                hash["survey_highest_level_completed_education_description"] = career_profile.survey_highest_level_completed_education_description
                hash["survey_years_full_time_experience"] = career_profile.survey_years_full_time_experience
                hash["survey_most_recent_role_description"] = career_profile.survey_most_recent_role_description
                hash["awards_and_interests"] = career_profile.awards_and_interests.pluck(:text).to_sentence # expected as a single text field
                hash["long_term_goal"] = career_profile.long_term_goal

                # EMBA fields
                hash["short_answer_leadership_challenge"] = career_profile.short_answer_leadership_challenge
                hash["short_answer_scholarship"] = career_profile.short_answer_scholarship

                # Education Experience
                # use ruby sorting and limiting here so that preloading will work
                # only include formal degree programs
                _degree_program_education_experiences = career_profile.education_experiences.select {|e| e['degree_program'] }.sort_by(&:graduation_year).reverse
                degree_program_education_experiences = Array.new(3) { |index| _degree_program_education_experiences[index] }

                _non_degree_program_education_experiences = career_profile.education_experiences.select {|e| !e['degree_program'] }.sort_by(&:graduation_year).reverse
                non_degree_program_education_experiences = Array.new(3) { |index| _non_degree_program_education_experiences[index] }

                education_experiences = degree_program_education_experiences + non_degree_program_education_experiences
                education_experiences.each_with_index do |education_experience, i|
                    hash["education_#{i+1}_name"] = education_experience && education_experience.educational_organization.text
                    hash["education_#{i+1}_graduation_year"] = education_experience && education_experience.graduation_year
                    hash["education_#{i+1}_degree"] = education_experience && education_experience.degree
                    hash["education_#{i+1}_major"] = education_experience && education_experience.major
                    hash["education_#{i+1}_minor"] = education_experience && education_experience.minor
                    hash["education_#{i+1}_gpa"] = education_experience && education_experience.gpa
                    hash["education_#{i+1}_gpa_description"] = education_experience && education_experience.gpa_description
                    hash["education_#{i+1}_degree_program"] = education_experience && education_experience.degree_program
                    hash["education_#{i+1}_will_not_complete"] = education_experience && education_experience.will_not_complete
                end

                # Work Experience
                # use ruby sorting and limiting here so that preloading will work
                _full_time_work_experiences = career_profile.work_experiences.select { |work_exp| work_exp.employment_type == 'full_time' }.sort_by(&:sort_key)
                full_time_work_experiences = Array.new(2) { |index| _full_time_work_experiences[index] }

                _part_time_work_experiences = career_profile.work_experiences.select { |work_exp| work_exp.employment_type == 'part_time' }.sort_by(&:sort_key)
                part_time_work_experiences = Array.new(2) { |index| _part_time_work_experiences[index] }

                work_experiences = full_time_work_experiences + part_time_work_experiences
                work_experiences.each_with_index do |work_experience, i|
                    work_experience = work_experiences[i]
                    hash["work_#{i+1}_name"] = work_experience && work_experience.professional_organization.text
                    hash["work_#{i+1}_job_title"] = work_experience && work_experience.job_title
                    hash["work_#{i+1}_industry"] = work_experience && work_experience.industry
                    hash["work_#{i+1}_role"] = work_experience && work_experience.role
                    hash["work_#{i+1}_responsibilities"] = work_experience && work_experience.responsibilities.to_sentence
                    hash["work_#{i+1}_length_string"] = work_experience && work_experience.length_string
                    hash["work_#{i+1}_employment_type"] = work_experience && work_experience.employment_type
                end
            end
        end

        hash
    end

    def identify_user
        self.user.identify
    end

    def program_type_config
        Cohort::ProgramTypeConfig[self.program_type]
    end

    def institution
        self.program_type_config.institution
    end

    def saved_change_to_accepted_or_pre_accepted?
        change_to_status = saved_changes['status']
        return false unless change_to_status
        !['accepted', 'pre_accepted'].include?(change_to_status[0]) && ['accepted', 'pre_accepted'].include?(change_to_status[1])
    end

    def reset_user_cohort_applications_association
        self.user.association(:cohort_applications).reset if self.association(:user).loaded?
    end

    def log_peer_recommendation_request_events
        self.career_profile.log_peer_recommendation_request_events
    end

    def is_archived_in_airtable?
        return self.airtable_base_key&.start_with?("AIRTABLE_BASE_KEY_ARCHIVE_")
    end

    def update_in_airtable(priority = SyncWithAirtableJob::DEFAULT_PRIORITY)
        return if !should_sync_with_airtable?

        # If we are updating the applied_at and the record has been archived in Airtable then we
        # need to issue a CREATE instead of an UPDATE to put the record back in the live base.
        if saved_changes && saved_changes["applied_at"] && self.is_archived_in_airtable?
            SyncWithAirtableJob.perform_later_with_debounce(self.id, SyncWithAirtableJob::CREATE, priority)
        else
            SyncWithAirtableJob.perform_later_with_debounce(self.id, SyncWithAirtableJob::UPDATE, priority)
        end
    end

    def should_sync_with_airtable?(preloaded_user = nil)
        return false if !self.supports_airtable_sync?

        preloaded_user = preloaded_user || user

        # We always sync users in the hiring network
        if preloaded_user.can_edit_career_profile
            return true
        else
            # We should not sync_with_airtable if
            # - The status is not changing and the status is rejected or expelled
            # - The status is changing between rejected and expelled

            before = saved_changes&.[]("status")&.[](0) # maybe this is abuse of the safe navigation operator, but still more succinct! haha.
            after = saved_changes&.[]("status")&.[](1)
            no_sync_statuses = ['rejected', 'expelled']

            if !saved_change_to_status? && no_sync_statuses.include?(status)
                return false
            elsif saved_change_to_status? && no_sync_statuses.include?(before) && no_sync_statuses.include?(after)
                return false
            else
                return true
            end
        end
    end

    def period_start_info(period, relevant_email = nil)
        period_index = published_cohort.index_for_period(period)
        previous_period = PublishedCohortPeriod.where(cohort_id: self.cohort_id, index: period_index-1).first

        next_exam_info = published_cohort.next_exam_info(period)
        if next_exam_info
            next_exam_info_entry = Lesson::Stream.event_attributes(next_exam_info[:locale_pack_id], user.locale).merge({
                start_time: next_exam_info[:start_time].to_timestamp
            })
        end
        published_cohort.event_attributes_for_period(period).merge({
            relevant_email: relevant_email,
            required_courses_completed_actual: self.user.required_courses_completed_overall(self.cohort_id),
            required_courses_completed_target: previous_period && previous_period.cumulative_required_stream_pack_ids.size,
            required_courses_entire_schedule: published_cohort.get_required_stream_count,
            curriculum_percent_complete: (100*self.user.cohort_percent_complete(published_cohort.attributes['id'])).round,
            curriculum_status: self.curriculum_status,

            # we use period_index - 1 because we want to know how much is expected to be complete at the end of the previous period
            curriculum_expected_complete: (100*published_cohort.expected_percent_complete(period_index-1)).round,

            course_entries: Cohort.get_course_entries_event_array(period, self.user),
            next_exam_info: next_exam_info.present? ? next_exam_info_entry : nil
        })
    end

    def curriculum_status
        current_period_index = published_cohort.current_period_index
        completed_stream_lpids = self.user.completed_stream_locale_pack_ids || []

        # If the user is at least on the second period then calculate if they have completed the prior period. When the
        # user is past the final period in the schedule then just use the final period for this calculation.
        expected_stream_lpids_complete_prior_period = []
        past_due_stream_lpids = []
        if current_period_index.present? && current_period_index >= 1
            expected_stream_lpids_complete_prior_period = published_cohort.expected_stream_lpids_complete(current_period_index) || []
            past_due_stream_lpids = expected_stream_lpids_complete_prior_period - completed_stream_lpids
        elsif current_period_index.nil?
            expected_stream_lpids_complete_prior_period = published_cohort.expected_stream_lpids_complete(published_cohort.periods.size) || []
            past_due_stream_lpids = expected_stream_lpids_complete_prior_period - completed_stream_lpids
        end

        # If the user is on a period in the schedule then calculate if they have completed that period
        has_completed_this_period = false
        if current_period_index.present? && current_period_index >= 0
            expected_stream_lpids_complete_this_period = published_cohort.expected_stream_lpids_complete(current_period_index + 1) || []
            has_completed_this_period = (expected_stream_lpids_complete_this_period - completed_stream_lpids).empty?
        end

        if ['failed', 'graduated', 'honors'].include?(graduation_status)
            return graduation_status
        elsif current_period_index == nil && past_due_stream_lpids.empty?
            return 'finished'
        elsif current_period_index == -1
            return 'week_0'
        elsif current_period_index == 0
            return 'week_1'
        elsif past_due_stream_lpids.size > 4
            return 'not_on_track'
        elsif past_due_stream_lpids.any? && past_due_stream_lpids.size <= 4
            return 'almost_there'
        elsif has_completed_this_period
            return 'on_track_finished'
        elsif past_due_stream_lpids.empty?
            return 'on_track'
        else
            Raven.capture_exception('No curriculum_status computed', {
                extra: {
                    cohort_application_id: self.id
                }
            })
            return nil
        end
    end

    def status_changed_info(enrollment_deadline, before_status)
        info = {
            admissions_decision: self.admissions_decision,
            previous_cohort_status: before_status
        }

        if self.status == 'expelled'
            info[:expelled_manually] = !self.user_expelled_due_to_period_expulsion_action
            info[:before_enrollment_deadline] = Time.now < enrollment_deadline
        end

        if self.status == 'deferred'
            info[:just_used_deferral_link] = self.just_used_deferral_link
        end

        return info
    end

    def published_cohort
        if @published_cohort && @published_cohort["id"] != cohort_id
            remove_instance_variable(:@published_cohort)
        end
        unless defined? @published_cohort
            @published_cohort = Cohort.cached_published(cohort_id)
        end
        @published_cohort
    end

    # in the wild, there will always be a published cohort for every application,
    # but this helps in specs sometimes
    def published_cohort_or_cohort
        published_cohort || cohort
    end

    # determines if a server event should be created
    def create_status_changed_event
        if published_cohort.nil?
            raise "cohort not published" unless Rails.env.test?
            return
        end

        if self.saved_change_to_status? && self.status != 'pending'
            enrollment_deadline = published_cohort.enrollment_deadline

            # We need to check if the application was just created or is being updated to know whether
            # the before_status argument to status_changed_info should be set. It doesn't make sense to set a
            # before_status in the just created case, but since we call new then save in create_from_hash it would
            # in fact register a pending to pre_accepted change on creation. This confused Allison when a deferred user
            # was given an immediately pre_accepted application that triggered a campaign meant for newly accepted
            # applications only.
            #
            # Note: It seems like the logic later in this method that checks for status changing from pending to pre_accepted
            # should also behave this way, but I'm too afraid to mess with that since it was there already.
            newly_created_application = saved_change_to_id?
            before_status = newly_created_application ? nil : saved_change_to_status[0]
            payload = self.event_attributes
                .merge(self.status_changed_info(enrollment_deadline, before_status))

            # This is only really important for the case of someone retargeted into
            # the emba who never expresses interest.  When that person gets rejected,
            # they don't need an email, because we already told them they didn't
            # get into the MBA. (In practice maybe we should delete the application
            # in that case.)
            if saved_change_to_status?(to: 'rejected') && mba_or_emba? && !retargeted_from_program_type
                payload[:relevant_email] = 'you are rejected from mba/emba'
            end

            if saved_change_to_status?(from: 'pending', to: 'pre_accepted') && !retargeted_from_program_type
                payload[:relevant_email] = "you are pre_accepted to #{program_type}"
            end

            if saved_change_to_status?(to: 'pre_accepted') && retargeted_from_program_type && has_full_scholarship?
                payload[:relevant_email] = "you are retargeted and pre_accepted with full scholarship to #{program_type}"
            end

            # this is tested in DecisionConcernSpec
            if self.status === 'rejected' && self.send_retargeted_email_for_program_type
                payload['send_retargeted_email_for_program_type'] = self.send_retargeted_email_for_program_type
                payload['target_cohort'] = Cohort.promoted_cohort(self.send_retargeted_email_for_program_type).event_attributes(self.user.timezone)
            end

            if self.status === 'accepted' && self.cohort_slack_url
                payload[:cohort_slack_url] = self.cohort_slack_url
            end

            if career_profile
                payload[:consider_early_decision] = career_profile.consider_early_decision
            end

            Event.create_server_event!(
                SecureRandom.uuid,
                self.user_id,
                "cohort:#{self.status}",
                payload
            ).log_to_external_systems(nil, false)
        end
    end

    def cohort_slack_url
        slack_room = self.published_cohort.slack_rooms.detect { |room| room.id == self.cohort_slack_room_id }
        return slack_room&.url
    end

    def create_graduation_status_changed_event
        if published_cohort.nil?
            raise "cohort not published" unless Rails.env.test?
            return
        end

        if self.saved_change_to_graduation_status? && self.graduation_status != 'pending'

            if ['honors', 'graduated'].include?(graduation_status)
                event_type = 'cohort:graduated'
            elsif graduation_status == 'failed'
                event_type = 'cohort:failed'
            else
                Raven.capture_exception("Do not know what event to log for graduation_status='#{graduation_status}'")
                return
            end

            Event.create_server_event!(
                SecureRandom.uuid,
                self.user_id,
                event_type,
                event_attributes.merge(
                    honors: graduation_status == 'honors',
                    cohort_end_date: published_cohort.end_date.to_timestamp
                )
            ).log_to_external_systems(nil, false)
        end
    end

    def just_failed?
        return saved_change_to_graduation_status? && self.graduation_status == 'failed'
    end

    def remove_from_career_network
        # NOTE: this will blow up if the learner's career_profile is in a career_profile_lists record.
        # The chances of this happening should be highly unlikely, and we'd like to get an error if
        # it does ever happen.
        self.user.can_edit_career_profile = false
        self.user.save! if self.user&.changed?
    end

    def create_registered_changed_event
        if published_cohort.nil?
            raise "cohort not published" unless Rails.env.test?
            return
        end

        if self.saved_change_to_registered?
            log_toggle_registered_event
        end
    end

    # separate method to be mocked in specs
    def log_toggle_registered_event
        # If we process a Stripe webhook event in parallel to the create subscription web request
        # between front-royal and back-royal, it's possible for this event to be logged twice.
        # We could fix this, but we're okay with the duplicate events for now since they're not
        # really hurting anything and we can easily configure our Customer.io campaigns to not
        # send twice for the same user.
        Event.create_server_event!(
            SecureRandom.uuid,
            self.user_id,
            "cohort:" + (self.registered ? "registered" : "unregistered"),
            event_attributes
        ).log_to_external_systems(nil, false)
    end

    def create_retargeted_changed_event
        if published_cohort.nil?
            raise "cohort not published" unless Rails.env.test?
            return
        end

        if self.saved_change_to_retargeted_from_program_type?

            Event.create_server_event!(
                SecureRandom.uuid,
                self.user_id,
                "cohort:retargeted_from_program_type_changed",
                event_attributes
            ).log_to_external_systems
        end
    end

    def handle_invite_to_reapply

        # if we accept someone to a cohort that requires regitration (emba)
        # and they decline to register (pay), then in the future we will
        # encourage them to re-apply
        if changes['status'] == ['pre_accepted', 'rejected'] && !registered && published_cohort.supports_invite_to_reapply?
            self.should_invite_to_reapply = true
        end

        if status != 'rejected'
            self.should_invite_to_reapply = false
        end

    end

    def event_attributes
        attrs = published_cohort.event_attributes(self.user.timezone).merge({
            :retargeted_from_program_type => self.retargeted_from_program_type,
            :cohort_application_status => self.status
        })

        if published_cohort.supports_payments?
            # NOTE: the `stripe_coupon_id` field must be retained as existing email campaigns rely on it.
            attrs = attrs.merge({
                :stripe_coupon_id => self.applicable_coupon_info && self.applicable_coupon_info["id"],
                :scholarship_level_name => self.scholarship_level && self.scholarship_level["name"],
            })
        end

        attrs = attrs.merge({
            :admissions_decision => self.admissions_decision,
            :previous_admissions_decision => self.previous_admissions_decision,
            :rubric_inherited => self.rubric_inherited
        })

        attrs
    end

    def clear_subscription_on_zero_payments
        if self.saved_change_to_total_num_required_stripe_payments? && self.total_num_required_stripe_payments == 0 && user.primary_subscription
            # may have already been deleted via `UsersController.update` meta handling, so reload to verify
            _primary_subscription = user.primary_subscription.reload
            _primary_subscription.destroy if _primary_subscription
        end
    end

    def mba_or_emba?
        ['mba', 'emba'].include?(published_cohort.program_type)
    end

    def before_save

        # Make sure that the user has a career profile
        self.user.ensure_career_profile if self.user

        if (
            self.status_changed?(to: 'pre_accepted') ||
            self.status_changed?(to: 'accepted') ||
            self.registered_changed?
        )
            published_cohort.ensure_slack_room_assignment_job
        end

        if self.status_changed?

            if self.status ==  'accepted'
                self.accepted_at = Time.now

                if program_type_config.supports_career_network_access_on_acceptance?
                    # ensure the user can edit their career profile when their application has been accepted
                    self.user.can_edit_career_profile = true
                elsif program_type_config.supports_delayed_career_network_access_on_acceptance?
                    # enqueue a job to set can_edit_career_profile two weeks from now
                    # see https://trello.com/c/KEhqEwvc
                    wait_n_days = ENV['SET_CAN_EDIT_CAREER_PROFILE_N_DAYS_AFTER_ACCEPTANCE']
                    SetCanEditCareerProfileAfterAcceptanceJob.set(wait: wait_n_days.nil? ? 14.days : wait_n_days.to_i.days).perform_later(self.user_id)
                end

                # When a user is accepted into the MBA or EMBA we need to set their active playlist to the first concentration, which is
                # the second required playlist after foundations, if they haven't already completed it.
                if program_type_config.supports_auto_active_playlist_selection_on_acceptance? &&
                    published_cohort.get_first_concentration_playlist_locale_pack &&
                    !self.user.is_first_concentration_playlist_complete?

                        self.user.active_playlist_locale_pack_id = published_cohort.get_first_concentration_playlist_locale_pack.id
                end
            elsif self.status ==  'expelled'
                self.expelled_at = Time.now
                self.user.career_profile.interested_in_joining_new_company = 'not_interested'
            end
        end

        # If the graduation status is changing to graduated
        if self.graduation_status_changed? && ['graduated', 'honors'].include?(graduation_status)
            self.graduated_at = Time.now
        end

        # If the application is becoming registered
        if self.registered_changed?
            if self.registered?
                self.registered_at = Time.now
                self.registered_early = self.applicable_registration_period == "early"
            else
                self.registered_at = nil
            end
        end

        # FIXME - https://trello.com/c/0Tz0Hjk2 - Ori and Matt are unsure if doing these flips back to nil are correct
        # See https://bitbucket.org/pedago-ondemand/back_royal/pull-requests/3123/feat-cohort-automation-trigger-warning/activity#LC37174485F233T237
        if self.status != 'accepted'
            self.accepted_at = nil
        end

        if self.status != 'expelled'
            self.expelled_at = nil
        end

        # determine if we need to reset scholarship_levels or not
        if program_type_config.supports_scholarship_levels?
            self.scholarship_level = self.scholarship_level || published_cohort.scholarship_levels.first
        else
            self.scholarship_level = nil
        end

        self.total_num_required_stripe_payments = nil unless published_cohort&.supports_payments?

        # active record is smart enough not to actually save the record if it hasn't changed,
        # but it will still run all of the callbacks and validations
        self.user.save! if self.user&.changed?
    end

    def required_to_provide_payment?
        published_cohort&.supports_payments? && !has_full_scholarship? && self.total_num_required_stripe_payments != 0
    end

    def handle_payment_grace_period
        if !!(self.status_changed?(to: 'accepted') && !self.registered && self.required_to_provide_payment?)
            self.payment_grace_period_end_at = self.default_payment_grace_period_end_at
        elsif !self.registered
            self.payment_grace_period_end_at = nil
        end
    end

    def default_payment_grace_period_end_at
        now = Time.now
        grace_period = 1.week
        [(self.published_cohort.registration_deadline || now) + grace_period, now + grace_period].max
    end

    def ensure_payment_grace_period
        self.payment_grace_period_end_at ||= self.default_payment_grace_period_end_at
    end

    def verify_lock_content_access_job
        return unless self.payment_grace_period_end_at

        jobs = self.pending_lock_content_access_jobs.to_a
        if jobs.empty?
            LockContentAccessDueToPastDuePayment.set(wait_until: self.payment_grace_period_end_at).perform_later(self.id)
        else
            # We should only ever have a single LockContentAccessDueToPastDuePayment job for an application.
            # If we find ourselves with more than one, just update the run_at for one and delete the others.
            job = jobs.pop
            job.update!(run_at: self.payment_grace_period_end_at)
            jobs.each(&:destroy)
        end
    end

    def pending_lock_content_access_jobs
        # need to check that the handler column includes the id of the cohort application that was used to create
        # the LockContentAccessDueToPastDuePayment delayed job to ensure that we don't accidentally destroy delayed jobs
        # that weren't created for this cohort application
        Delayed::Job
            .where(
                queue: 'lock_content_access_due_to_past_due_payment',
                locked_at: nil
            )
            .where("handler like ?", "%#{self.id}%")
    end

    # destroys all LockContentAccessDueToPastDuePayment delayed jobs created with this cohort application
    def destroy_all_pending_lock_content_access_jobs
        self.pending_lock_content_access_jobs.destroy_all
    end

    def should_destroy_all_pending_lock_content_access_jobs?
        return false if only_changing_cohort_slack_room_id?

        if self.saved_change_to_payment_grace_period_end_at?(to: nil) || !['pre_accepted', 'accepted'].include?(self.status)
            true
        else
            false
        end
    end

    def log_cohort_content_locked_event
        if self.saved_change_to_locked_due_to_past_due_payment? && self.locked_due_to_past_due_payment?
            Event.create_server_event!(
                SecureRandom.uuid,
                self.user_id,
                'cohort:content_locked_due_to_past_due_payment',
                {}
            ).log_to_external_systems
        end
    end

    def should_enqueue_check_in_with_inactive_user_job?
        return self.saved_change_to_status? && self.indicates_user_should_be_checked_in_with?
    end

    # determines if the application indicates whether the user should be checked in with or not due to inactivity
    def indicates_user_should_be_checked_in_with?
        return self.status == 'accepted' && !self.completed_at && self.program_type_config.supports_checking_in_with_inactive_user? ? true : false
    end

    # creates a new Cohort::CheckInWithInactiveUser delayed job is one doesn't already exist for the user
    # since there should only be one at most per user
    def enqueue_check_in_with_inactive_user_job
        return if self.user.check_in_with_inactive_user_job
        Cohort::CheckInWithInactiveUser.set(wait_until: Time.now + 2.weeks).perform_later(self.user_id)
    end

    # determines if the Cohort::CheckInWithInactiveUser delayed job should be destroyed or not
    def should_destroy_check_in_with_inactive_user_job?
        return ((self.saved_change_to_status? && ['deferred', 'expelled'].include?(self.status)) || (self.saved_change_to_completed_at? && self.completed_at)) &&
            self.program_type_config.supports_checking_in_with_inactive_user? ? true : false
    end

    def destroy_check_in_with_inactive_user_job
        self.user&.destroy_check_in_with_inactive_user_job
    end

    # See https://trello.com/c/7kdkrPj6 for more info
    def indicates_user_should_upload_identification_document?
         self.status == 'accepted' && self.graduation_status != 'failed' && self.published_cohort.requires_id_upload?
    end

    def indicates_user_should_upload_transcripts?
        self.status == 'accepted' && self.graduation_status != 'failed' && self.published_cohort.supports_document_upload?
    end

    # See https://trello.com/c/7kdkrPj6 for more info
    def indicates_user_should_upload_english_language_proficiency_documents?
         self.acceptable_status? && self.graduation_status != 'failed' && self.program_type_config.requires_english_language_proficiency?
    end

    def get_plan_by_id(stripe_plan_id)
        self.stripe_plans&.find { |plan| plan['id'] == stripe_plan_id }
    end

    def stripe_plan
        get_plan_by_id(self.stripe_plan_id)
    end

    def stripe_plan_num_intervals
        return nil unless stripe_plan
        {
            monthly: 12,
            bi_annual: 2,
            once: 1
        }[stripe_plan['frequency'].to_sym]
    end

    def stripe_plan_amount
        ((stripe_plan && stripe_plan['amount']) || (default_stripe_plan && default_stripe_plan['amount'])).to_i
    end

    def get_plan_frequency(stripe_plan_id)
        target_plan = get_plan_by_id(stripe_plan_id)
        target_plan['frequency']
    end

    def set_total_num_default_value(stripe_plan_id)
        frequency = get_plan_frequency(stripe_plan_id)
        if frequency == 'once'
            self.total_num_required_stripe_payments = 1
        elsif frequency == 'bi_annual'
            self.total_num_required_stripe_payments = 2
        elsif frequency == 'monthly'
            self.total_num_required_stripe_payments = 12
        else
            raise "Invalid frequency for plan #{stripe_plan_id}!"
        end
    end

    def payment_info
        self.class.payment_info_attrs.inject({}) { |hash, attr| hash.merge("#{attr}" => self[attr]) }
    end

    def reset_payment_information_if_needed
        if !published_cohort&.supports_payments?
            self.class.payment_info_attrs.each { |attr| self[attr] = nil unless attr == 'registered' } # `registered` can't be null

            self.refund_issued_at = nil
            self.locked_due_to_past_due_payment = false
            self.registered_early = false
            self.allow_early_registration_pricing = false
            self.refund_attempt_failed = false
        end
    end

    def valid_status
        if !self.class.statuses.include?(status)
            errors.add(:status, "value of #{status} is invalid") # FIXME: localize?
        end
    end

    def validate_pending_graduation_status_if_not_accepted
        # Note the third clause, we DO need to allow graduated biz certs to be rejected for the
        # situation where they are joining the EMBA
        if self.status != 'accepted' && self.graduation_status != 'pending' && !(self.program_type == 'the_business_certificate' && self.status == 'rejected')
            errors.add(:graduation_status, "must be pending if status is not accepted (unless biz cert)")
        end
    end

    def validate_stripe_details
        if published_cohort&.supports_payments?

            if stripe_plans.blank?
                errors.add(:stripe_plans, "must be set if the cohort has stripe plans")
            end

            # we do validate the plans though
            if self.stripe_plan_id && !self.stripe_plan_ids.include?(self.stripe_plan_id)
                errors.add(:stripe_plan_id, "must be included in stripe_plans")
            end

            # stripe details expected in registration or after a successful charge state (deferral coverage)
            registered_or_charged = self.registered || self.num_charged_payments
            if self.total_num_required_stripe_payments.nil? && registered_or_charged && !has_full_scholarship?
                errors.add(:total_num_required_stripe_payments, "is required for billable cohorts once registered or deferred")
            end
            if self.stripe_plan_id.nil? && registered_or_charged && !has_full_scholarship?
                errors.add(:stripe_plan_id, "is required for billable cohorts once registered or deferred")
            end

            return unless cohort.supports_scholarship_levels? # paid certs, etc have none

            if scholarship_levels.blank?
                errors.add(:scholarship_levels, "must be set if the cohort has scholarship levels")
            end

            # we do not check that the scholarship_level actually exists in scholarship_levels, because
            # we support custom scholarships

            # we default scholarship_level if necessary in before_save, but we want to validate that it's not improperly set elsewhere
            self.scholarship_level&.each do |period_key, registration_period|
                next if period_key === 'name' # skip the level's "name" key

                # validate the coupon definition for each period ("standard", "early", and the now legacy "coupons")
                registration_period.each do |plan_key, coupon_info|
                    amount_off = coupon_info && coupon_info["amount_off"]
                    percent_off = coupon_info && coupon_info["percent_off"]
                    if amount_off.blank? && percent_off.blank?
                        errors.add(:scholarship_level, ": a scholarship_level with a valid coupon for #{period_key}.#{plan_key} is required for billable cohorts")
                    end
                end
            end

        end
    end

    def validate_some_other_stripe_details_before_commit

        # because of the order of things in create_or_reconcile_from_stripe_subscription!,
        # this has to happen after everything is saved and before commit.
        if (
            self.registered &&
            self.total_num_required_stripe_payments.to_i > 0 &&
            user.primary_subscription.nil?
        )

            errors.add(:registered, "must not be true if there are required payments and no subscription.")
            raise ActiveRecord::RecordInvalid.new(self)
        end
    end

    def acceptable_status?
        CohortApplication.acceptable_statuses.include?(status)
    end

    def pending_or_pre_accepted?
        ['pending', 'pre_accepted'].include?(status)
    end

    def requires_slack_room_id?
        return false unless status == 'accepted'

        # we check this instead of checking supports_slack_rooms? because
        # there are old cohorts with no rooms, and we don't want to invalidate
        # their students
        return false unless cohort.slack_rooms.any?
        return true
    end

    # We only want to send emails about completed foundations playlists
    # in certain cases.  We don't implement this with a `relevant_email`
    # property or a `send_email` flag because we're already using our
    # one property filter to separate the 1st one from the half way one
    # from the all complete one.  Ugh.
    def log_foundations_completed_events?
        program_type_config.log_foundations_completed_events?(status)
    end

    def check_if_json_changed(&block)
        orig_updated_at = [updated_at, user.updated_at, career_profile.updated_at].max
        yield
        db_updated_at = CohortApplication
            .where(id: self.id)
            .includes(:user => :career_profile)
            .pluck(Arel.sql("GREATEST(cohort_applications.updated_at, users.updated_at, career_profiles.updated_at) as updated_at"))
            .first

        if db_updated_at.nil?
            return :destroyed
        elsif db_updated_at != orig_updated_at
            return :changed
        else
            return :unchanged
        end

    end


    def handle_id_verifications_migration
        if user.identity_verified?
            backfill_for_legacy_id_verifications
        else
            transfer_id_verifications_from_previous_cohort
        end
    end

    def backfill_for_legacy_id_verifications
        waive_verification_for_late_enrollment(published_cohort.id_verification_periods)
    end

    def transfer_id_verifications_from_previous_cohort
        # the ca != self seems pointless, since the applied_ats should be equal and it should
        # be filtered out, but you go tell that to ci
        previous_application = user.cohort_applications.select{ |ca| ca != self && ca.applied_at < self.applied_at}.sort_by(&:applied_at).last
        return unless previous_application
        previous_cohort = previous_application.cohort

        verification_count_in_previous_cohort = user.user_id_verifications.where(cohort_id: previous_cohort.id).count
        verification_periods = published_cohort.verification_periods_before_active_one
        if verification_count_in_previous_cohort > verification_periods.size
            verification_periods << published_cohort.active_verification_period
        end
        waive_verification_for_late_enrollment(verification_periods)
    end

    def waive_verification_for_late_enrollment(verification_periods)
        user_id_verifications = self.user.user_id_verifications.where(cohort_id: self.cohort_id).to_a

        # Be defensive, because we had a situation where a user with
        # prior applications had become identity_verified, but then
        # later they were being accepted to a program with no verification_periods
        verification_periods&.each_with_index do |period, i|
            # Sometimes a user may already have an ID verification record for a verification period on this cohort.
            # This could happen if a user was accepted into the cohort, verified their ID for a verification period,
            # was later expelled, but then re-admitted into the program a bit later. As it turns out, this is not an
            # uncommon scenario for users who fail to make payments. If we that the user has already verified their
            # ID for the verification period in question, just skip it so we retain the original record's info.
            # See https://trello.com/c/51P5PAd9.
            has_already_verified_id_for_verification_period = user_id_verifications.find { |uiv| uiv.id_verification_period_index == i }
            if !has_already_verified_id_for_verification_period
                UserIdVerification.create!(
                    cohort_id: self.cohort_id,
                    user_id: self.user_id,
                    id_verification_period_index: i,
                    verified_at: Time.now,
                    verification_method: 'waived_due_to_late_enrollment'
                )
            end
        end
    end

    def transfer_progress_from_deferred_or_expelled_cohort(stream_lpids_to_force_if_new_progress = [])
        return unless ['accepted', 'pre_accepted'].include?(status)

        # We are looking at all deferred or expelled applications here.  I THINK this is better than just
        # looking at the last one, but not sure it matters.  Down below, we only grab the most
        # recently completed progress record
        deferred_or_expelled_cohorts = CohortApplication
            .where(status: ['deferred', 'expelled'], user_id: user_id)
            .where("applied_at < ?", applied_at)
            .map(&:published_cohort).uniq

        return unless deferred_or_expelled_cohorts.any?

        midterm_complete = final_complete = false

        # Transfer the midterm and final in case there are new version(s) of them.
        # Only mba cohorts have a midterm and final. See midterm_exam_locale_pack_id
        # and final_exam_locale_pack_id.
        if program_type == 'mba'
            midterm_complete = transfer_exam_progress(:midterm_exam_locale_pack_id, cohort, deferred_or_expelled_cohorts, stream_lpids_to_force_if_new_progress)
            final_complete = transfer_exam_progress(:final_exam_locale_pack_id, cohort, deferred_or_expelled_cohorts, stream_lpids_to_force_if_new_progress)
        end

        # This mostly applies to exams that changed in playlists, but normal streams could be
        # put in this whitelist as well.
        transferred_courses = []
        equivalent_stream_locale_pack_ids.each do |stream_locale_pack_ids|
            should_force = (stream_lpids_to_force_if_new_progress & stream_locale_pack_ids).any?
            course_payload = transfer_stream_progress_for_equivalent_streams(
                stream_locale_pack_ids,
                should_force
            )

            # Users expect their exam stream progress to be transferred upon (pre-)acceptance into a cohort after
            # having been deferred or expelled, so we don't need to include those streams in the event payload
            # because there's no action for them to do for exam streams.
            if course_payload.present? && !course_payload[:to][:exam]
                transferred_courses << course_payload
            end
        end

        # NOTE: This logic should come AFTER the transfer logic has occurred in case a
        # transferred course causes backfilling logic to trigger
        #
        # Once all exams and whitelisted courses' progress has been transferred, go through
        # the required schedule courses for MBA or the playlists for EMBA to see if any backfilling
        # of courses needs to be done.
        # See https://trello.com/c/6xyHPQuo
        backfilled_courses = []
        if program_type == 'mba'
            backfilled_courses += ensure_required_schedule_streams_complete_if_exam_completed
        elsif program_type == 'emba'
            backfilled_courses += ensure_playlist_streams_complete_if_exam_completed
        end

        # Log an event if we backfilled or transferred courses
        if backfilled_courses.any? || transferred_courses.any?
            # Note that midterm_complete and final_complete mean
            # "were they actually transferred?", not "are they complete?"
            event = Event.create_server_event!(
                SecureRandom.uuid,
                user.id,
                'cohort:transferred_progress',
                {
                    backfilled_courses: backfilled_courses,
                    transferred_courses: transferred_courses,
                    midterm_complete: midterm_complete,
                    final_complete: final_complete
                }
            )
            event.log_to_external_systems
            event
        end
    end

    protected def equivalent_stream_locale_pack_ids
        [[
            # Probability Fundamentals
            'f9140971-4b98-4578-9f14-53986f02eb7b',
            '9b20b827-a7b2-4203-93d8-5c918352e34e'
        ], [
            # Data and Decisions Exam
            'a846219d-3dcd-49e6-a6a6-29935326e944',
            '549ec44a-56e8-454e-866b-bba7424e70b9',
            'b9040117-b82e-487c-8d8f-ec9b6104f125',
            '9c52b04a-0b3e-4083-a0c3-0ed540665a86'
        ], [
            # Marketing and Pricing Exam
            '3eaac7e0-408a-4970-b396-cbe393cd15dc',
            'd68d3fdd-edbf-4aa4-9162-92c59307a702'
        ], [
            # Finance Exam
            'acea3c85-f0c1-48e8-9898-4305074e3190',
            'cc2c7235-4a82-45fb-af6a-66c86b888744'
        ], [
            # Advanced Finance Exam
            '8838331e-2d73-4a9f-834b-6da1835733a3',
            '8bd64acd-44b2-4cb4-be05-5ee1fc3f3063'
        ], [
            # Strategy & Innovation Exam
            '95c18ea3-4884-483d-b77d-806ea92cef83',
            'f989f8c7-4562-41e4-b793-0b788e13faca'
        ]]
    end

    protected def transfer_exam_progress(locale_pack_id_meth, cohort, deferred_or_expelled_cohorts, stream_lpids_to_force_if_new_progress)
        exam_lpids_from_deferred_or_expelled_cohorts = deferred_or_expelled_cohorts.select{|cohort| cohort.program_type == 'mba'}.map(&locale_pack_id_meth)&.uniq
        exam_lpid_from_this_cohort = published_cohort.send(locale_pack_id_meth)
        event_payload = transfer_stream_progress_for_equivalent_streams(
            [exam_lpids_from_deferred_or_expelled_cohorts, exam_lpid_from_this_cohort].flatten.uniq,
            stream_lpids_to_force_if_new_progress.include?(exam_lpid_from_this_cohort)
        )
        exam_complete = event_payload.present?
        return exam_complete
    end

    protected def transfer_stream_progress_for_equivalent_streams(equivalent_lpids, force_if_new_progress = false)
        # from the list of locale_pack_ids that are equivalents to each other, find the one
        # that is in the curriculum for this cohort
        lpid_from_this_cohort = (stream_locale_pack_ids & equivalent_lpids).first

        return if equivalent_lpids.nil? || lpid_from_this_cohort.nil?

        lpids_from_other_cohorts = equivalent_lpids - [lpid_from_this_cohort]

        # we only need to go on if the old cohorts had a different stream than
        # this one
        return unless lpids_from_other_cohorts.any?

        old_completed_stream_progress = user.lesson_streams_progresses.where(locale_pack_id: lpids_from_other_cohorts)
                                        .where.not(completed_at: nil)
                                        .reorder("completed_at desc")
                                        .first

        # we do not need to do anything if the user has not completed a previous stream
        return unless old_completed_stream_progress

        new_stream_progress = user.lesson_streams_progresses.where(locale_pack_id: lpid_from_this_cohort).first

        # If the user already has progress for the new version of the course but we specified
        # that we still want to do the transfer then just update the completed_at for that new progress.
        # Otherwise if the user does not have progress for the new version of the course then make one.
        # Otherwise do nothing. (This extra complexity was necessary when we added Probability Fundamentals
        # after some people had already deferred into new cohorts and started the new course.  In general, this
        # should be unnecessary if we have the correct list of streams in here before deferring people.)
        if new_stream_progress.present? && force_if_new_progress
            new_stream_progress.completed_at = Time.now
        elsif new_stream_progress.nil?
            new_stream_progress = Lesson::StreamProgress.new(old_completed_stream_progress.attributes.except(
                "id",
                "locale_pack_id",
                "certificate_image_id",
                "created_at",
                "updated_at"
            ))
            new_stream_progress.locale_pack_id = lpid_from_this_cohort
        else
            return
        end

        # They could have organically completed it already hypothetically (though unlikely),
        # in which case we don't want to mark it as waived
        if !new_stream_progress.all_lessons_complete?
            new_stream_progress.waiver = Lesson::StreamProgress::WAIVER_EQUIVALENT_STREAM_ALREADY_COMPLETED
        end

        if new_stream_progress.for_exam_stream?
            new_stream_progress.official_test_score = old_completed_stream_progress.official_test_score

            # We will not send a score report for the new progress record.  In fact, it
            # would probably be better if we DID send the email at the end of the exam
            # period, in order to make it clear to
            # them that they are getting credit for completing the exam, but:
            #
            # 1.  Since this task might be run months before the exam period, there is
            #     danger of the schedule changing and the email going out at the wrong
            #     time.  See https://trello.com/c/EfNTLTq3
            new_stream_progress.skip_exam_events = true
        end

        # generate_certificate_image! will save the record.  Note that if
        # we change the code above to start transferring progress for incomplete
        # stream_progresses, then we would need to call save! here for incomplete
        # records rather than calling generate_certificate_image!
        new_stream_progress.generate_certificate_image! # this will save it

        # Note: If we ever try to transfer incomplete progress we'll need to change the hardcoding of status to "complete"
        from_payload = Lesson::Stream.event_attributes(old_completed_stream_progress.locale_pack_id, user.locale).merge({
            status: 'complete'
        })
        to_payload = Lesson::Stream.event_attributes(new_stream_progress.locale_pack_id, user.locale).merge({
            status: 'complete'
        })

        return {
            from: from_payload,
            to: to_payload
        }
    end

    protected def backfill_stream_progresses_for_incomplete_streams_whose_exam_is_completed(locale_pack_ids = [])
        backfilled_courses = []
        locale_pack_ids.each do |pack_id|
            new_stream_progress = Lesson::StreamProgress.create_or_update!(
                user_id: user.id,
                locale_pack_id: pack_id,
                complete: true
            )

            # They could have organically completed it already hypothetically (though unlikely),
            # in which case we don't want to mark it as waived
            if !new_stream_progress.all_lessons_complete?
                new_stream_progress.update!(waiver: Lesson::StreamProgress::WAIVER_EXAM_ALREADY_COMPLETED)
            end

            backfilled_courses << Lesson::Stream.event_attributes(pack_id, user.locale).merge({
                status: 'complete'
            })
        end
        backfilled_courses
    end

    protected def ensure_required_schedule_streams_complete_if_exam_completed
        highest_completed_exam_query = Lesson::StreamProgress.where(user_id: self.user_id).where.not(completed_at: nil)
        highest_completed_exam = nil

        # If the final is complete, we will backfill all required courses
        if highest_completed_exam_query.where(locale_pack_id: published_cohort.final_exam_locale_pack_id).exists?
            highest_completed_exam = :final

        # If the midterm is complete, we will backfill all required courses up to the midterm
        elsif highest_completed_exam_query.where(locale_pack_id: published_cohort.midterm_exam_locale_pack_id).exists?
            highest_completed_exam = :midterm

        # Otherwise, we don't need to backfill anything
        else
            return []
        end

        ensure_locale_pack_ids_complete = []
        published_cohort.periods.each do |p|
            ensure_locale_pack_ids_complete += p['stream_entries']
                .select { |entry| entry['required'] }
                .map { |entry| entry['locale_pack_id'] }

            break if highest_completed_exam == :midterm && p['style'] == 'exam' && p['exam_style'] == 'intermediate' ||
                highest_completed_exam == :final && p['style'] == 'exam' && p['exam_style'] == 'final'
        end

        completed_stream_progresses_by_pack_id = get_completed_progresses_for_user_for_streams_by_pack_id(
            user_id: self.user_id,
            stream_pack_ids: ensure_locale_pack_ids_complete.uniq
        )
        backfill_stream_progresses_for_incomplete_streams_whose_exam_is_completed(ensure_locale_pack_ids_complete - completed_stream_progresses_by_pack_id.keys)
    end

    protected def ensure_playlist_streams_complete_if_exam_completed
        ensure_locale_pack_ids_complete = []

        # NOTE: We are just assuming the en playlists' stream_entries since this is only used for
        # emba cohorts, where we are only using en playlists right now.
        Playlist.where(locale_pack_id: published_cohort.playlist_pack_ids, locale: 'en').each do |playlist|
            stream_pack_ids = playlist.published_version.stream_entries.map { |entry| entry["locale_pack_id"] }

            # Get a list of all the completed progresses for the user for the streams in the playlist,
            # grouped by locale_pack_id, and if any are for an exam
            # then we know this playlist should be considered completed. Then we can
            # compare the streams that are actually completed against the stream_pack_ids from the new
            # version of the playlist to know which new streams we should mark as completed.
            completed_stream_progresses_by_pack_id = get_completed_progresses_for_user_for_streams_by_pack_id(
                user_id: self.user_id,
                stream_pack_ids: stream_pack_ids
            )
            exam_completed = completed_stream_progresses_by_pack_id.values.pluck(:exam).include?(true)
            if exam_completed
                ensure_locale_pack_ids_complete += (stream_pack_ids - completed_stream_progresses_by_pack_id.keys)
            end
        end
        backfill_stream_progresses_for_incomplete_streams_whose_exam_is_completed(ensure_locale_pack_ids_complete.uniq)
    end

    protected def get_completed_progresses_for_user_for_streams_by_pack_id(user_id:, stream_pack_ids:)
        completed_stream_progresses_by_pack_id = Lesson::StreamProgress.where(user_id: user_id, locale_pack_id: stream_pack_ids)
            .joins(:lesson_streams)
            .group(:locale_pack_id, :completed_at)
            .select(:locale_pack_id, :completed_at, "count(case when lesson_streams.exam then true else null end) > 0 as exam")
            .where.not(completed_at: nil)
            .reorder(:locale_pack_id)
            .to_a
            .index_by(&:locale_pack_id)
    end

    def required_stream_locale_pack_ids
        @required_stream_locale_pack_ids ||= published_cohort.get_required_stream_locale_pack_ids
    end

    def stream_locale_pack_ids
        @stream_locale_pack_ids ||= published_cohort.get_stream_locale_pack_ids
    end

    # Adds the associated user to the EXTRA COURSES access group upon being rejected
    # from the MBA or EMBA if not already in the group
    def add_user_to_extra_courses_access_group
        if saved_change_to_status? && self.status == 'rejected' &&
            self.mba_or_emba? && !self.user.in_group?('EXTRA COURSES')

            self.user.add_to_group('EXTRA COURSES')
        end
    end

    def accept_on_registration
        if self.registered && status == 'pre_accepted'
            self.status = 'accepted'
        end
    end

    def chance_of_graduating
        return nil unless status == 'accepted'
        @chance_of_graduating ||= UserChanceOfGraduatingRecord.where(user_id: user_id, program_type: program_type).first&.chance_of_graduating
    end

    # since we include the accepted cohort title in the fulltext search on the
    # profile, we need to update that if the status changes here
    def update_career_profile_fulltext
        user.career_profile&.update_fulltext
    end

    def should_update_career_profile_fulltext?
        status_change = !!saved_changes[:status]&.include?('accepted')
    end

    def amount_per_interval
        coupon_info = applicable_coupon_info
        amount = stripe_plan_amount.to_f
        return nil unless amount
        if coupon_info.nil?
            amount
        elsif coupon_info['amount_off']
            amount - coupon_info['amount_off']
        elsif coupon_info['percent_off']
            amount * ((100 - coupon_info["percent_off"])/100.0)
        end
    end

    def total_required_stripe_amount
        return 0 unless stripe_plan && stripe_plan_num_intervals
        amount_per_interval * stripe_plan_num_intervals
    end


    def fee_amount_per_interval
        return 0 unless stripe_plans

        if self.stripe_plan_id == 'emba_875'
            return 7500
        end

        return 0
    end

    def total_fee_amount
        return 0 unless stripe_plans
        (fee_amount_per_interval && stripe_plan_num_intervals) ? fee_amount_per_interval * stripe_plan_num_intervals : 0
    end

    def total_fees_plus_tuition
        return 0 unless stripe_plans
        if has_full_scholarship?
            total_base_tuition
        else
            (stripe_plan_amount && stripe_plan_num_intervals) ? stripe_plan_amount * stripe_plan_num_intervals : 0
        end
    end

    def total_base_tuition
        return 0 unless stripe_plans
        # this is assuming that there are no fees associated with the one_time_payment_plan (that is,
        # it is all tuition.  no fees)
        one_time_payment_plan = self.stripe_plans.detect { |plan| plan['frequency'] == 'once' }

        # old applications may not have a one_time_payment_plan. The tuition for those
        # was always 9600
        one_time_payment_plan ? one_time_payment_plan['amount'].to_i : 9600*100
    end


    def discount_per_interval
        return 0 unless stripe_plans
        # We could define this for people who have not yet registered, but
        # we do not do that in the discount_per_interval sql helper, so we
        # are not doing it here either. See the comment in the migration where
        # that helper is defined.
        if !self.registered
            0
        end

        if self.has_full_scholarship?
            # in this case, there will be no amount_off in the coupon, so the next
            # logic will not work
            0
        elsif self.registered_early
            scholarship_level['early'][stripe_plan_id]['amount_off'] - scholarship_level['standard'][stripe_plan_id]['amount_off']
        else
            0
        end
    end

    def total_discount
        return 0 unless stripe_plans
        if discount_per_interval.nil?
            0
        elsif stripe_plan_num_intervals.nil? # full scholarship
            0
        else
            discount_per_interval * stripe_plan_num_intervals
        end
    end

    def total_scholarship
        return 0 unless stripe_plans
        if applicable_coupon_info.nil?
            0
        elsif applicable_coupon_info['percent_off'] == 100
            return total_fees_plus_tuition - total_discount
        else
            (applicable_coupon_info['amount_off'] - discount_per_interval) * (stripe_plan_num_intervals || 0)
        end
    end

    def net_required_payment
        return 0 unless stripe_plans
        total_fees_plus_tuition - total_discount - total_scholarship
    end

    def total_amount_paid_in_stripe
        total_num_successful_payments * amount_per_interval
    end

    def validate_payment_counts
        return unless total_num_required_stripe_payments.to_i > 0
        return unless num_charged_payments.to_i > 0

        # if all payments have been made, then total_num_required_stripe_payments should
        # be set back to 0 and the subscription should be destroyed
        if total_num_successful_payments >= total_num_required_stripe_payments
            errors.add(:total_num_successful_payments, "should never be as many as total_num_required_stripe_payments")
        end
    end

    def get_refund_percent_details

        # To determine if the user has started a concentration in this
        # context, we need to determine if ze has started any stream
        # that is in a concentration but NOT in foundations.
        # Since different locales for a playlist CAN have different lists
        # of streams, we have to pick one locale.  `en` is the official
        # source of truth.  In practice, we use the same list for each
        # locale anyway.  It's just kind of a hole in our data model.
        stream_lpids_by_playlist = published_cohort.get_required_lesson_stream_locale_pack_ids_by_playlist(9999, 'en')
        progress = user.lesson_streams_progresses.index_by(&:locale_pack_id)
        foundations_stream_pack_ids = stream_lpids_by_playlist[0]
        started_concentration_count = 0
        completed_concentration_count = 0

        # slice to remove the foundations playlist
        stream_lpids_by_playlist.slice(1, 999).each_with_index do |stream_lpids, i|
            non_foundations_lpids = stream_lpids - foundations_stream_pack_ids
            started = non_foundations_lpids.detect { |lpid| progress[lpid] }.present?
            completed = stream_lpids.map { |lpid| progress[lpid]&.completed_at.present? }.all?

            started_concentration_count += 1 if started
            completed_concentration_count += 1 if completed
        end

        # see also - /help/article/337-tuition-refund-policy
        if started_concentration_count == 0
            refund_percent = 1
        elsif completed_concentration_count == 0
            refund_percent = 0.9
        elsif completed_concentration_count == 1
            refund_percent = 0.75
        elsif completed_concentration_count >= 2 and completed_concentration_count < 4
            refund_percent = 0.5
        else
            refund_percent = 0
        end

        {
            refund_percent: refund_percent,
            started_concentration_count: started_concentration_count,
            completed_concentration_count: completed_concentration_count
        }
    end

    def refund_details

        if !cohort.program_type_config.supports_partial_tuition_refund? || !stripe_plan_id || !published_cohort&.supports_payments?
            return nil
        elsif total_amount_paid_in_stripe == 0 && !promised_payment_outside_of_stripe?
            return nil
        end

        refund_percent_details = get_refund_percent_details
        refund_percent = refund_percent_details[:refund_percent]
        total_amount_owed_after_refund = (1-refund_percent) * total_required_stripe_amount
        refund_amount = [total_amount_paid_in_stripe - total_amount_owed_after_refund, 0].max

        custom_message = nil
        if refund_attempt_failed?
            custom_message = "A refund attempt failed in the past, but some charges may have been refunded.
                                We cannot recover from this automatically.  You will have to go to the Stripe
                                dashboard and select the charges to refund."
        elsif has_full_scholarship?
            custom_message = "No refund will be issued because this user has a full scholarship and has not made any payments."
        elsif promised_payment_outside_of_stripe?
            custom_message = "This user appears to have made payments outside of stripe, so no refund can be automatically issued."
        elsif refund_issued_at
            custom_message = "A refund was already issued on #{refund_issued_at.in_time_zone('Eastern Time (US & Canada)').strftime('%Y-%m-%d')}."
        end

        {
            custom_message: custom_message,

            # if the calculated refund_amount is 0 or if there is a custom message
            # indicating some special situation, then we cannot actually issue a refund,
            # so the client should not try
            can_issue_refund: refund_amount == 0 ? false : !custom_message,
            total_required_stripe_amount: total_required_stripe_amount,
            total_tuition_retained_after_refund: (1- refund_percent) * total_required_stripe_amount,
            total_amount_paid_in_stripe: total_amount_paid_in_stripe,
            refund_percent: refund_percent,
            has_started_playlist: refund_percent_details[:started_concentration_count] > 0,
            playlists_completed: refund_percent_details[:completed_concentration_count],
            refund_amount: refund_amount,
            stripe_plan_id: stripe_plan && stripe_plan['id'],
            coupon_id: applicable_coupon_info && applicable_coupon_info['id']
        }
    end

    def issue_refund!

        mark_as_errored_on_error = false
        raise "Cannot issue refund until subscription has been destroyed" if user.primary_subscription
        raise "Cannot issue refund if a previous refund attempt failed" if refund_attempt_failed?

        subscriptions = Stripe::Subscription.list(customer: self.user_id, status: 'canceled')
        raise "No canceled subscription found in stripe" unless subscriptions.any?

        # Maybe this would be fine to get invoices from subscriptions with different plans, but it's not
        # an expected situation right now and I'm
        # worried it might bring up issues we haven't thought about.  I'd rather wait until it
        # really happens and look at a real-life context before we decide to allow it.
        raise "Cannot issue refund for user with multiple past plans" if subscriptions.map(&:plan).map(&:id).uniq.size > 1

        invoices = Stripe::Invoice.list(customer: self.user_id, expand: ['data.charge', 'data.charge.refunds'])

        amount_left_to_refund = refund_details[:refund_amount]

        charges_to_refund = invoices.map(&:charge).compact.reject { |c| c.amount_refunded > 0 || c.paid != true}
        amount_available_to_refund = charges_to_refund.inject(0) { |sum, charge| sum + charge.amount }

        # Above this, we haven't made any changes yet, and if there was error it is
        # potentially something we can resolve and retry (like if the subscription is
        # deleted yet). Beyond this, if there is an error, we want to mark the application
        # as such so that we do not try to issue a refund again.
        mark_as_errored_on_error = true
        if amount_available_to_refund < amount_left_to_refund
            raise RuntimeError.new("Not enough charges found to issue the required refund")
        end

        # invoices are ordered with newest first
        charges_to_refund.each do |charge|

            amount_to_refund = [charge.amount, amount_left_to_refund].min
            Stripe::Refund.create(
                charge: charge.id,
                amount: amount_to_refund.to_i, # stripe requires integers here
                metadata: {
                    # This flag indicates that this refund was part
                    # of refunding a user's tuition when they were removed
                    # from a program.  i.e. not just a refund of a particular charge
                    cohort_tuition_refund: true
                }
            )
            amount_left_to_refund -= amount_to_refund

            break if amount_left_to_refund <= 0
        end

        self.update(refund_issued_at: Time.now)
        true
    rescue Exception => err
        if mark_as_errored_on_error
            self.update(refund_attempt_failed: true)
        end

        # We capture the exception here and log to sentry with
        # some extra info.  We then raise the error, but the controller
        # will catch it and use it to report out to the user.
        Raven.capture_exception(err, {
            extra: {
                cohort_application_id: self.id,
                refund_details: refund_details
            }
        })
        raise err
    end

    def set_stripe_plans_and_scholarship_levels
        self.stripe_plans ||= published_cohort.stripe_plans
        self.scholarship_levels ||= published_cohort.scholarship_levels
    end

    def stripe_plan_ids
        self.stripe_plans.map { |e| e['id'] }
    end

    def previous_admissions_decisions
        CohortApplication::Version
            .where(id: self.id)
            .reorder(:version_created_at)
            .pluck(:admissions_decision)
            .uniq
    end

    def previous_admissions_decision
        self.previous_admissions_decisions[-2]
    end

    def set_original_program_type
        self.original_program_type = self.program_type
    end

    def should_sync_fallback_program_type?
        # When the cohort application has an acceptable status, we want to sync the user's fallback_program_type
        # because then if they ever get rejected, expelled, or deferred from the cohort, their experience in the
        # app and the messaging they receive from customer.io remains consistent. We also only want to sync their
        # fallback_program_type if the application's program_type is one of the program types that can be assigned
        # when the user signs up.
        self.acceptable_status? && Cohort::ProgramTypeConfig.signup_program_types.include?(self.program_type)
    end

    def sync_fallback_program_type
        self.user.update!(fallback_program_type: self.program_type)
    end

    def should_set_invited_to_interview_at?
        self.admissions_decision_changed?(to: 'Invite to Interview')
    end

    def set_invited_to_interview_at
        self.invited_to_interview_at = Time.now
    end

    def set_rejected_after_pre_accepted
        self.rejected_after_pre_accepted = self.status_changed?(from: 'pre_accepted', to: 'rejected') ? true : false
    end

    def interview_conducted?
        self.rubric_interview == 'Conducted'
    end

    def invite_to_interview_link_valid?
        !!(self.status == 'pending' &&
            self.invited_to_interview_at &&
            !self.interview_conducted?)
    end

    # Note: this is sent down with the JSON to indicate whether to show the invite to interview sidebar in the app
    # It's a little more strict than the invite_to_interview_link_valid? above, in that it also ensures that the
    # current decision is still Invite to Interview and that the invite isn't older than 2 weeks.
    def invited_to_interview?
        !!(self.invite_to_interview_link_valid? &&
            self.admissions_decision == 'Invite to Interview' &&
            self.invited_to_interview_at.to_timestamp > 2.weeks.ago.to_timestamp)
    end

    def invited_to_interview_url
        # This URL should match the URL included in our customer.io emails.
        # https://fly.customer.io/env/24964/campaigns/1000272
        # https://fly.customer.io/env/24964/campaigns/1000218
        "#{UrlHelper.domain_url}/interview?email=#{user.email}&name=#{user.name}&tz=#{user.timezone_with_fallback}&a1=#{user.country || 'US'}"
    end

    def set_final_score
        return if !Cohort::ProgramTypeConfig[program_type].supports_final_score?

        scores = CohortApplication::FinalScoreHelper.calculate_final_score(self)

        # Note: We had a situation where scores were identical in Postgres, but
        # slightly different due to rounding in Ruby. Ignore this if it happens
        # again in the future.
        # e.g. app.changes was {"final_score"=>[0.666974171945426, 0.6669741719454261]}
        unless abs_diff(self.final_score, scores[:final_score]) < 0.000001
            self.final_score = scores[:final_score]
        end

        unless abs_diff(self.project_score, scores[:project_score]) < 0.000001
            self.project_score = scores[:project_score]
        end

        self.meets_graduation_requirements = scores[:meets_graduation_requirements]
        self
    end

    private def abs_diff(val1, val2)
        val1 ||= 0
        val2 ||= 0
        (val1 - val2).abs
    end

    def curriculum_complete?
        user.curriculum_complete?(cohort)
    end

    def required_specializations_complete?
        user.completed_specializations_count(cohort) >= published_cohort.num_required_specializations
    end

    def enrollment_agreement_template_id
        published_cohort.enrollment_agreement_template_id
    end

    def sign_now_smart_field_values
        return unless mba_or_emba?

        attrs = {
            email: user.email,
            program_name: published_cohort.title,
            program_start: published_cohort.start_date.strftime("%m/%d/%Y"),
            program_end: published_cohort.end_date.strftime("%m/%d/%Y"),
            tuition: CurrencyFormatter.new.number_to_currency(total_base_tuition / 100 || 0),
            fees: CurrencyFormatter.new.number_to_currency(total_fee_amount / 100 || 0),
            scholarship: CurrencyFormatter.new.number_to_currency(total_scholarship / 100 || 0),
            discount: CurrencyFormatter.new.number_to_currency(total_discount / 100 || 0),
            total_cost: CurrencyFormatter.new.number_to_currency(net_required_payment / 100 || 0),
            created_at: DateTime.now.strftime("%m/%d/%Y")
        }
        attrs.except!(:scholarship, :discount) unless program_type == 'emba'
        attrs
    end

    def create_enrollment_agreement(follow_up_attempt)

        raise "Cannot create enrollment agreement without template_id" unless self.enrollment_agreement_template_id

        # This hits the SignNow api, so should not be called from a web request, beyond the SignableDocument::ENROLLMENT_AGREEMENT_SIGN_ROUTE
        SignableDocument.create_with_sign_now(
            document_type: SignableDocument::TYPE_ENROLLMENT_AGREEMENT,
            user_id: self.user_id,
            template_id: self.enrollment_agreement_template_id,
            smart_field_values: sign_now_smart_field_values,
            metadata: {
                program_type: self.program_type,
                cohort_id: self.cohort_id,
                cohort_application_id: self.id,
                follow_up_attempt: follow_up_attempt
            }
        )

    end

    def need_to_generate_new_enrollment_agreement?
        return false unless should_have_enrollment_agreement?

        # True if there is an existing enrollment agreement that has not yet been
        # signed, and is either expired or for a previous cohort
        if enrollment_agreement&.unsigned? &&
            (enrollment_agreement.sign_now_signing_link_expiry <= Time.now ||
            enrollment_agreement.metadata['cohort_id'] != self.cohort_id)

            return true

        end

        # if there already is an enrollment agreement, we don't need a new one
        enrollment_agreement.nil?
    end

    def queue_ensure_enrollment_agreement_job_if_necessary
        ensure_enrollment_agreement_if_necessary(true)
    end

    # If need_to_generate_new_enrollment_agreement? delete and create a new one
    # either immediately or via queued job
    def ensure_enrollment_agreement_if_necessary(enqueue = false)
        return unless need_to_generate_new_enrollment_agreement?
        EnsureEnrollmentAgreementJob.send(enqueue ? :ensure_job : :perform_now, self.id)
    end

    def should_have_enrollment_agreement?
        return false unless ['pre_accepted', 'accepted'].include?(status)
        return false unless enrollment_agreement_template_id.present?
        return false unless published_cohort&.requires_enrollment_agreement?

        return false if user.deferred_info.last_deferred_application && published_cohort.decision_date > Time.now

        # For emba cohorts, we cannot generate the enrollment agreement until
        # we know how they're paying
        if published_cohort.supports_payments?
            return self.registered
        else
            return true
        end
    end

    def enrollment_agreement
        # We sort by signed_at on the off chance that there are multiple agreements
        # for this program_Type.  In that case, if one is signed and one is not, we
        # want the signed one
        self.user.signable_documents.sort_by { |doc| doc.signed_at.to_timestamp || 0 }.reverse.detect do |document|
            document.is_enrollment_agreement? &&
            document.metadata['program_type'] == self.program_type
        end
    end

    # See also cohort_application.js#hasFullScholarship
    def has_full_scholarship?
        self.scholarship_level.present? && self.scholarship_level["name"] == 'Full Scholarship'
    end

    # See also cohort_application.js#tuitionNumSuccessfulPayments
    # Note the slight naming difference that I'm not going to fix
    def total_num_successful_payments
        return 0 if num_charged_payments.blank?
        num_charged_payments - (num_refunded_payments || 0)
    end

    # See also cohort_application.js#hasMadeStripePayments
    def has_made_stripe_payments?
        return self.total_num_successful_payments > 0;
    end

    # See also cohort_application.js#promisedPaymentOutsideOfStripe
    # Also, this is not totally accurate.  See promisedPaymentOutsideOfStripe for more explanation
    def promised_payment_outside_of_stripe?
        return self.cohort&.supports_payments? &&
            !self.has_full_scholarship? &&
            self.total_num_required_stripe_payments === 0 &&
            self.num_charged_payments === 0
    end

    # See also cohort_application.js#hasMadePayments
    def has_made_payments?
        # NOTE: we simplify the logic in this method by simply checking if the user promised_payment_outside_of_stripe?
        # rather than actually checking their billing_transactions to see if they actually did pay outside of Stripe.
        # So, this logic is tehchnically incorrect, but we're okay with it for now.
        return self.has_made_stripe_payments? || self.promised_payment_outside_of_stripe?
    end

    def has_paid_in_full?
        if !required_to_make_payments?
            return true
        end

        self.user.net_amount_paid >= (self.net_required_payment / 100)
    end

    def required_to_make_payments?
        if !published_cohort&.supports_payments?
            return false
        end

        if has_full_scholarship?
            return false
        end

        return true
    end

    # When an accepted or pre_accepted cohort application is
    # created, we need to ensure that a corresponding user
    # exists in Stripe.
    # See also: https://trello.com/c/KULA0ydp
    private def queue_ensure_stripe_customer_job
        EnsureStripeCustomerJob.perform_later(self.user_id)
    end

    private def set_notify_candidate_positions_preferences_on_first_submission
        return if user.cohort_applications.size > 0 # this does a count query

        if !['very_interested', 'interested', 'neutral', 'not_interested'].include?(user.career_profile.interested_in_joining_new_company)
            Raven.capture_exception("Cannot set notify_candidate_positions preferences with this interested_in_joining_new_company value", {
                extra: {
                    interested_in_joining_new_company: user.career_profile.interested_in_joining_new_company
                }
            })
        end

        interested_in_joining_new_company = user.career_profile.interested_in_joining_new_company

        if user.notify_candidate_positions.nil?
            if interested_in_joining_new_company == 'not_interested'
                user.notify_candidate_positions = false
            elsif ['very_interested', 'interested', 'neutral'].include?(interested_in_joining_new_company)
                user.notify_candidate_positions = true
            end
        end

        if user.notify_candidate_positions_recommended.nil?
            case interested_in_joining_new_company
            when 'very_interested'
                user.notify_candidate_positions_recommended = 'daily'
            when 'interested'
                user.notify_candidate_positions_recommended = 'daily'
            when 'neutral'
                user.notify_candidate_positions_recommended = 'bi_weekly'
            when 'not_interested'
                user.notify_candidate_positions_recommended = 'never'
            end
        end

        user.save! if user.changed?
    end

    private def prior_applications_decisions_string
        results = CohortApplication::Version.connection.execute(%Q~
            SELECT cohorts.name as cohort_name, admissions_decision
            FROM cohort_applications_versions
            JOIN cohorts ON cohorts.id = cohort_applications_versions.cohort_id
            WHERE cohort_applications_versions.applied_at < '#{self.applied_at}'
                AND cohort_applications_versions.user_id = '#{self.user_id}'
                AND admissions_decision IS NOT NULL
            GROUP BY cohorts.name, admissions_decision
            ORDER BY min(applied_at), min(version_created_at);
        ~)
        .map { |result| "#{result['cohort_name']} - #{result['admissions_decision']}" }
        .join(",\n")
        .upcase
    end

    # In sync_with_airtable#maybe_transfer_prior_airtable_fields we could have transferred over
    # rubric fields, in which case we set a flag in our database, also in which case we don't
    # want to trigger an update sync as we just transferred the necessary fields manually
    # to the freshly created application.
    private def inheriting_rubric?
        saved_changes.keys&.include?('rubric_inherited')
    end

    private def create_in_airtable
        return if !self.supports_airtable_sync?
        SyncWithAirtableJob.perform_later_with_debounce(self.id, SyncWithAirtableJob::CREATE)
    end

    private def destroy_in_airtable
        return if self.is_archived_in_airtable? || !self.supports_airtable_sync?
        SyncWithAirtableJob.perform_later_with_debounce(self.id, SyncWithAirtableJob::DESTROY)
    end

    private def reset_decision_if_reapplying_to_promotable_program
        # If you change the status of a promotable cohort applicaton from rejected to pending then that
        # indicates the user is reapplying, so reset the admissions_decision.
        # See https://trello.com/c/UtftVAYt and submit_application_form_dir.js
        if changes["status"] &&
            changes["status"][0] && changes["status"][0] == 'rejected' &&
            changes["status"][1] && changes["status"][1] == 'pending' &&
            self.supports_reapply?

            new_decision = nil
            self.admissions_decision = new_decision

            if self.supports_airtable_sync?
                SyncWithAirtableJob.perform_later(self.id, SyncWithAirtableJob.generate_fix_decision_action(new_decision))
            end
        end
    end

    private def fix_reject_for_no_interview_if_conducted
        if self.admissions_decision == "Reject for No Interview" && self.rubric_interview == "Conducted"
            if self.supports_airtable_sync?
                new_decision = "Reject"
                EditorTracking.transaction("fix_reject_for_no_interview_if_conducted") do
                    self.update_column(:admissions_decision, new_decision)
                    SyncWithAirtableJob.perform_later(self.id, SyncWithAirtableJob.generate_fix_decision_action(new_decision))
                end
            else
                Raven.capture_exception('Trying to fix_reject_for_no_interview_if_conducted for an application that does not support airtable sync', {
                    extra: {
                        application_id: self.id
                    }
                })
            end
        end
    end

    private def handle_network_inclusion
        user.handle_network_inclusion
    end

    # Normally, we defer someone in the admin UI.  However if an expelled user
    # who previously made payments re-applies on their own, then we need to
    # transfer their payment information.
    # This should duplicate the logic in edit_user_details_enrollment_dir.js#addCohortApplication,
    # near the comment that says "pass along payment information"
    private def transfer_payment_info_from_previous_application
        index = user.cohort_applications.index(self)
        previous_index = index ? index + 1 : 0
        previous_application = user.cohort_applications[previous_index]
        return unless previous_application
        return unless previous_application.program_type == self.program_type

        if previous_application.total_num_successful_payments > 0 && self.total_num_successful_payments == 0
            self.class.payment_info_attrs.each do |key|
                # This method still triggers for new applications created as part of a deferral enrollment action
                # in the users admin. By the time this new application gets created, the payment_grace_period_end_at
                # will have already been cleared from the previous_application (see #handle_payment_grace_period),
                # but on the client we ensure that this value still gets appropriately transferred over.
                self[key] = previous_application[key] unless ['registered', 'payment_grace_period_end_at', 'locked_due_to_past_due_payment'].include?(key)
            end
        end

        self
    end

    # We batch update cohort_slack_room_id in cohort_applications_controller#batch_update.  In that
    # case, we can skip some callbacks in order to speed things up
    private def only_changing_cohort_slack_room_id?
        (saved_changes.keys - ['cohort_slack_room_id', 'updated_at']).empty?
    end

    # In a sync_with_airtable create job we update the application with the airtable_base_key that
    # airtable gives us back in the response. Don't trigger another update sync when we do that.
    private def only_changing_airtable_base_key?
        (saved_changes.keys - ['airtable_base_key', 'updated_at']).empty?
    end

    # We sync various rubric fields from Airtable to our database using a "last modified at" feature
    # in Airtable to detect new changes. Since we sync the "last modified at" back to Airtable at the
    # time that the update is occuring we do not need to trigger a sync_with_airtable job.
    private def only_changing_rubric_fields?
        (saved_changes.keys - Airtable::Application.from_airtable_column_map.keys - ['updated_at']).empty?
    end

    private def should_identify_after_commit?
        # If we are only changing some properties that we know should not trigger an identify,
        # or if we are destroying the application directly (notthrough the user being destroyed).
        !(only_changing_cohort_slack_room_id? || only_changing_airtable_base_key?) ||
        (self.destroyed? && self.destroyed_by_association&.active_record&.name != User.name)
    end

    # Handle any unexpected paths, such as "Invite to Convert to EMBA" decision
    # or users applying during the Quantic transition, by ensuring the user's
    # institution state with a lifecycle hook. Essentially, if the user manages
    # to have an acceptable application created or updated but they don't have the
    # correct institution state we'll ensure it is correct here.
    # See https://trello.com/c/QFVa6l5W
    private def ensure_institution_state
        self.user.ensure_institution(self.institution)
        self.user.active_institution = self.institution
    end

    private def queue_update_cohort_status_changes
        if self.status == 'pending' || self.saved_changes[:status] == ['pending', 'rejected']
            return
        end

        if self.saved_change_to_status? || self.saved_change_to_graduation_status? || self.saved_change_to_cohort_id?
            UpdateCohortStatusChangesJob.perform_later_with_debounce(self.id)
        end
    end

    private def delete_cohort_status_changes
        # this is tested in cohort_status_change_spec
        CohortStatusChange.where(cohort_application_id: self.id).delete_all
    end
end
