module WebpackHelper

    def webpack_bundle_tag(key, defer=true)
        # see home_controller_spec.rb
        return if controller.respond_to?(:disable_webpack_helpers?)
        javascript_include_tag get_webpack_module_value_for_key(key), :defer => defer
    end

    def webpack_style_tag(key)
        # see home_controller_spec.rb
        return if controller.respond_to?(:disable_webpack_helpers?)
        stylesheet_link_tag get_webpack_module_value_for_key(key), :media => 'all'
    end

    def webpack_bundle_tags_for_chunk(chunk, defer=true)
        keys = wepback_module_bundle_keys(chunk)
        raw keys.map { |key| webpack_bundle_tag(key, defer) }.join("\n")
    end

    def webpack_style_tags_for_chunk(chunk)
        keys = wepback_style_bundle_keys(chunk)
        raw keys.map { |key| webpack_style_tag(key) }.join("\n")
    end

    def inline_chunk(chunk)
        raw wepback_module_bundle_keys(chunk).map { |key| inline_script(key) }.join("\n")
    end

    def inline_script(key)
        # see home_controller_spec.rb
        return if controller.respond_to?(:disable_webpack_helpers?)
        path = get_webpack_module_value_for_key(key)

        SafeCache.fetch("Cached webpack script #{path}", force: !Rails.env.production?) do
            
            if Rails.env.production?
                path = path.gsub('/assets', '/deployment_assets')

                # this should match the DEPLOYMENT_BUCKET in deploy.sh
                base_url = "https://uploads.smart.ly"
            else
                base_url = request.base_url.sub('docker.for.mac.host.internal', 'localhost')
            end

            file_contents = HTTParty.get("#{base_url}#{path}")
            file_contents = file_contents.encode("utf-8", invalid: :replace, undef: :replace, replace: '?')
            raw "<script type='text/javascript' name='#{key}'>\n#{file_contents}\n</script>"
        end
    end
    

    def webpack_module_bundle_keys_cache
        @webpack_module_bundle_keys_cache ||= {}
    end

    def webpack_style_bundle_keys_cache
        @webpack_style_bundle_keys_cache ||= {}
    end

    # Get the keys from the webpack manifest that have code needed by the target_module,
    # excluding any that are already included in the webpack_module_bundle_keys_cache.
    def wepback_module_bundle_keys(target_module)
        webpack_manifest_hash.keys.select do |key|
            if !webpack_module_bundle_keys_cache[key] && key =~ /~?#{Regexp.quote(target_module)}(~.*.js$|.js$)/
                webpack_module_bundle_keys_cache[key] = true
                true
            end
        end
    end

    # Get the keys from the webpack manifest that have css needed by the target_module,
    # excluding any that are already included in the webpack_style_bundle_keys_cache.
    def wepback_style_bundle_keys(target_module)
        webpack_manifest_hash.keys.select do |key|
            if !webpack_style_bundle_keys_cache[key] && key =~ /~?#{Regexp.quote(target_module)}(.*css$)/
                webpack_style_bundle_keys_cache[key] = true
                true
            end
        end
    end

    def webpack_manifest_script

        # the locale manifest is needed when switching to any language other than English.
        # see translation_module.js
        javascript_tag "window.webpackManifest = #{webpack_manifest_hash.merge(webpack_manifest_hash('locale')).to_json}"
    end

    def webpack_manifest_hash(klass='module')
        # Depending on a configuration setup by our Webpack initializer is not suitable
        # for local development. If Webpack's output has changed since we initialized our
        # Webpack configuration, that configuration will be stale. SO, we grab and read it
        # manifests every single time we need them in local development/testing.
        return Rails.env.production? ? get_configuration_file_for_klass(klass) : get_parsed_manifest_file_for_klass(klass)
    end

    def webpack_module_manifest_cache_key
        cache_key_for_manifest('module')
    end

    def webpack_asset_manifest_cache_key
        cache_key_for_manifest('asset')
    end

    def cache_key_for_manifest(klass)
        # In production, cache the cache key, since it might be expensive to compute
        if Rails.env.production?
            @webpack_cache_keys ||= {}
            @webpack_cache_keys[klass] || (@webpack_cache_keys[klass] = webpack_manifest_hash(klass).hash)
        else
            webpack_manifest_hash(klass).hash
        end
    end

    def get_webpack_module_value_for_key(key)
        webpack_manifest_hash('module').fetch(key)
    end

    def get_webpack_asset_value_for_key(key)
        quantic_key = key.clone
        if controller.send(:is_quantic?)
            i = /\.\w+$/ =~ quantic_key
            quantic_key.insert(i, '_quantic') if i # the quantic version of assets have `_quantic` appended to the file name
        end
        manifest_hash = webpack_manifest_hash('asset')
        manifest_hash[quantic_key] || manifest_hash[key]
    end

    def get_configuration_file_for_klass(klass)
        return configuration_file =
            if klass == 'module'
                Rails.configuration.webpack[:manifest_file]
            elsif klass == 'asset'
                Rails.configuration.webpack[:manifest_asset_file]
            elsif klass == 'locale'
                Rails.configuration.webpack[:manifest_locale_file]
            end
    end

    def get_parsed_manifest_file_for_klass(klass)
        filename =
            if klass == 'module'
                'webpack_manifest.json'
            elsif klass == 'asset'
                'webpack_asset_manifest.json'
            elsif klass == 'locale'
                'webpack_locale_manifest.json'
            end
        manifest_file = Rails.root.join('config', filename)
        return File.exist?(manifest_file) ? JSON.parse(File.read(manifest_file)) : {}
    end

    # Used in config/initializers/webpack.rb
    module_function :get_parsed_manifest_file_for_klass

end