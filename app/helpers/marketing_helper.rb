module MarketingHelper

    # This is a wrapper around the Rails CacheHelper API. We used to see occasional ERRNO/EACCESS errors with
    # the file store cache, so by using SafeCache.rescue_from_errors, we can ensure that the site will still 
    # return data if the cache throws an error.
    #
    # In addition, this wrapper also automatically adds some common dependencies to the cache key 
    # automatically, which makes our views a little cleaner.
    #
    # To give some background on how I initially used this to cache views: 
    
    # I started out by finding the “leaf nodes” of the template tree in our marketing  area (things like 
    # _full-content.html.erb) and then surrounding them with appropriate cache statements, ensuring that each 
    # cache statement included any dependencies found within those files. The most common dependencies are 
    # webpack_asset_manifest_cache_key (i.e.: the file makes use of cache-busted assets that are looked up in 
    # webpack) and the local_assigns (i.e.: the “arguments” we're passing into the template as local variables, 
    # so each unique set of passed in values needs its own cache). Additional dependencies that make an 
    # appearance in some files are things like is_quantic? and @ variables that are set in controllers.
    # 
    # Once I had working caching for leaf nodes, I worked my way backwards up the tree, ensuring that parent 
    # templates also included all of their children’s dependencies.
    # 
    # In some cases, I found that caching a file actually made it load slower, usually because the cost of 
    # hashing its dependencies was greater than just rendering the file! An example of this was the various 
    # “accordions” on the Free MBA and Exec MBA curriculum pages that render all of the course content. It’s 
    # possible that an alternative strategy, like simply caching that content with a fixed expiry, would work 
    # well; for now, I left it uncached.
    # 
    # Additional note: I use to_json on the local_assigns because if they contain an ActiveRecord object, I 
    # noticed that Ruby’s hash method would already return a new value for it across page refreshes, even when 
    # the record looked identically (must be some internal AR data it’s including in the hash). Using the json 
    # version avoids this problem.
    def safe_cache(name = [], options = {}, &block)
        name << webpack_asset_manifest_cache_key unless options[:skip_webpack_asset_manifest_cache_key].present?
        name = name.join("/")
        SafeCache.rescue_from_errors(block) do 
            cache(name, options) do
                yield
            end
        end
    end

    SCHOOL_NAMES = {
        "risd" => "RISD",
        "caltech" => "Caltech",
        "middlebury" => "Middlebury",
        "wellesley" => "Wellesley",
        "smith" => "Smith",
        "morehouse" => "Morehouse",
        "vassar" => "Vassar",
        "spelman" => "Spelman",
        "hampton" => "Hampton",
        "dartmouth" => "Dartmouth",
        "sloan" => "Sloan",
        "parsons" => "Parsons",
        "princeton" => "Princeton",
        "howard" => "Howard",
        "mit" => "MIT",
        "wharton" => "Wharton",
        "yale" => "Yale",
        "cmu" => "CMU",
        "georgetown" => "Georgetown",
        "hbs" => "HBS",
        "harvard" => "Harvard",
        "chicago" => "Chicago",
        "duke" => "Duke",
        "stanford" => "Stanford",
        "cornell" => "Cornell",
        "uva" => "UVA",
        "amherst" => "Amherst",
        "berkeley" => "Berkeley",
        "illinois" => "Illinois",
        "michigan" => "Michigan",
        "ucla" => "UCLA",
        "harvey_mudd" => "Harvey Mudd",
        "haas" => "Haas",
        "anderson" => "Anderson",
        "tepper" => "Tepper",
        "darden" => "Darden",
        "tuck" => "Tuck",
        "johnson" => "Johnson",
        "fuqua" => "Fuqua",
        "lbs" => "LBS",
        "insead" => "INSEAD",
        "williams" => "Williams",
        "swarthmore" => "Swarthmore",
        "colgate" => "Colgate",
        "wl" => "W&L",
        "kellogg" => "Kellogg",
        "stern" => "Stern",
        "columbia" => "Columbia",
        "goizueta" => "Goizueta",
        "kenan_flagler" => "Kenan-Flagler",
        "mccombs" => "McCombs",
        "mcdonough" => "McDonough",
        "said" => "Saïd",
        "georgia_tech" => "Georgia Tech",
        "bc" => "Boston College",
        "bu" => "Boston University",
        "brandeis" => "Brandeis",
        "tufts" => "Tufts",
        "babson" => "Babson",
        "olin" => "Olin",
        "bfit" => "BFIT",
        "wentworth" => "Wentworth",
        "upenn" => "UPenn",
        "jhu" => "Johns Hopkins",
        "brown" => "Brown",
        "georgiatech" => "Georgia Tech" # Note: duplicates the georgia_tech entry above for backwards compatibility with old landing pages
    }

    SCHOOL_WITH_ALUMNI_HEADLINES = {
        "harvard" => "Join over 200 Harvard Alumni in our network!",
        "tufts" => "Join over 50 Tufts Alumni in our network!",
        "upenn" => "Join over 100 UPenn Alumni in our network!",
        "jhu" => "Join over 100 Johns Hopkins Alumni in our network!",
        "nyu" => "Join over 100 NYU Alumni in our network!",
        "georgiatech" => "Join over 200 Georgia Tech Alumni in our network!"
    }

    def self.get_school_name_for_code(school)
        SCHOOL_NAMES[school]
    end

    def self.get_school_alumni_headline_for_code(school)
        SCHOOL_WITH_ALUMNI_HEADLINES[school]
    end

    def render_svg(file_path)
        return File.read(file_path).html_safe if File.exists?(file_path)
        ''
    end

    def self.is_degree_program?(program_type)
        Cohort::ProgramTypeConfig.degree_program_types.include?(program_type)
    end

    def self.get_curriculum_for_program_type(program_type)
        begin
            promoted_cohort = Cohort.promoted_cohort(program_type)

            # verify it's valid...
            if promoted_cohort.required_playlist_pack_ids.length == 0
                raise
            end
        rescue Exception
            if Rails.env.development?
                return self.get_fake_curriculum_for_program_type(program_type)
            else
                raise "Missing promoted cohort for program type #{program_type}!"
            end
        end
        return unless promoted_cohort

        curriculum = SafeCache.fetch("marketing/curriculum_#{promoted_cohort.attributes['id']}", expires_in: 2.hours) do
            playlist_hash = Playlist.
                all_published.
                where(locale: 'en').
                index_by {|p| p['locale_pack_id'] }

            course_hash = Lesson::Stream.
                all_published.
                includes(:image).
                joins(:image).
                where(locale: 'en').
                index_by {|s| s.locale_pack_id}

            lessons_hash = Lesson.all.
                select(:title, :id, :assessment).
                where(locale: 'en').
                index_by {|s| s.id}

            cohort_hash = {}
            [
                {
                    source: 'required_playlist_pack_ids',
                    destination: 'concentrations'
                },
                {
                    source: 'specialization_playlist_pack_ids',
                    destination: 'specializations'
                }
            ].each do |obj|
                cohort_hash[obj[:destination]] = promoted_cohort.send(obj[:source]).map do |playlist_pack_id|
                    playlist = playlist_hash[playlist_pack_id]
                    streams = playlist.stream_entries.map {|s| course_hash[s['locale_pack_id']] }.compact.select {|s| !s.exam }
                    {
                        title: playlist.title,
                        description: playlist.description,
                        image_url: streams.last.image.formats['220x220']['url'],
                        streams: streams.map do |s|
                            {
                                title: s.title,
                                description: s.description,
                                image_url: s.image.formats['220x220']['url'],
                                canonical_url: is_degree_program?(program_type) ? s.entity_metadata.canonical_url : nil,
                                lessons: s.chapters.map {|c| c['lesson_hashes'] }.flatten.map {|h| h['lesson_id'] }.flatten.map do |id|
                                    {
                                        title: lessons_hash[id].title,
                                        assessment: lessons_hash[id].assessment
                                    }
                                end
                            }
                        end
                    }
                end
            end

            # exclude business foundations from the data returned
            if Cohort::ProgramTypeConfig[program_type].supports_business_foundations_playlist?
                cohort_hash['concentrations'].shift
            end

            # add dates if necessary
            if Cohort::ProgramTypeConfig[program_type].supports_schedule?
                dates = []

                start_date = promoted_cohort.start_date
                admission_rounds = promoted_cohort.admission_rounds

                # only include the closest upcoming admission round here
                dates << admission_rounds.map.with_index do |r, i|
                    {
                        date: start_date.add_dst_aware_offset(r['application_deadline_days_offset'].days),
                        text: "Round #{i + 1} Application Deadline",
                        type: "registration_deadline"
                    }
                end.sort_by {|d| d[:date].to_timestamp }.find {|r| r[:date] > Time.now }

                dates << {
                    date: start_date.add_dst_aware_offset(admission_rounds.last['decision_date_days_offset'].days),
                    text: "Final Admissions Decisions",
                    type: "decision"
                }

                if Cohort::ProgramTypeConfig[program_type].supports_registration_deadline?
                    dates << {
                        date: promoted_cohort.registration_deadline,
                        text: "Registration Deadline",
                        type: "registration_deadline"
                    }
                end

                dates << {
                    date: promoted_cohort.acceptance_date,
                    text: "Welcome and Orientation",
                    type: "orientation"
                }

                dates << {
                    date: start_date,
                    text: "Start of Classes",
                    type: "start"
                }

                cohort_hash[:dates] = dates.sort_by {|d| d[:date].to_timestamp }
            end

            cohort_hash.with_indifferent_access
        end

        curriculum
    end

    def self.get_fake_curriculum_for_program_type(program_type)
        fake_lesson = {
            title: 'Fake Lesson'
        }

        fake_course = {
            title: 'Fake Course',
            description: 'This is a long fake description that is here for you to see how the UI will look.',
            image_url: 'https://uploads.smart.ly/images/2a58fa32d05a17ed2381bc44c4eb8ea7/220x220/2a58fa32d05a17ed2381bc44c4eb8ea7.png',
            canonical_url: is_degree_program?(program_type) ? '/course/fake-course/12345' : nil,
            lessons: (1..8).map do |num|
                fake_lesson.merge({
                    title: "#{fake_lesson[:title]} #{num}"
                })
            end
        }

        fake_playlist = {
            title: 'Fake Playlist',
            description: 'This is a long fake description that is here for you to see how the UI will look.',
            image_url: 'https://uploads.smart.ly/images/2a58fa32d05a17ed2381bc44c4eb8ea7/220x220/2a58fa32d05a17ed2381bc44c4eb8ea7.png',
            streams: (1..8).map do |num|
                fake_course.merge({
                    title: "#{fake_course[:title]} #{num}"
                })
            end
        }

        {
            dates: [
                {
                    date: Time.new(2018, 1, 15, 4, 0, 0, 0),
                    text: 'Round 2 Application Deadline',
                    type: 'round_deadline'
                },
                {
                    date: Time.new(2018, 1, 25, 4, 0, 0, 0),
                    text: 'Final Admissions Decisions',
                    type: 'decision'
                },
                {
                    date: Time.new(2018, 2, 6, 4, 0, 0, 0),
                    text: 'Registration Deadline',
                    type: 'registration_deadline'
                },
                {
                    date: Time.new(2018, 2, 8, 4, 0, 0, 0),
                    text: 'Welcome and Orientation',
                    type: 'orientation'
                },
                {
                    date: Time.new(2018, 2, 13, 4, 0, 0, 0),
                    text: 'Start of Classes',
                    type: 'start'
                }
            ],
            concentrations: (1..8).map do |num|
                fake_playlist.merge({
                    title: "Fake Concentration #{num}"
                })
            end,
            specializations: (1..4).map do |num|
                fake_playlist.merge({
                    title: "Fake Specialization #{num}"
                })
            end
        }
    end

end
