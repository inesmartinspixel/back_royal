module RedshiftHelper

    def self.sanitize_encodes(sql)
        if ActiveRecord::Base.connection_config[:store_type] != 'redshift'
            sql.gsub!(/ENCODE [^,)]*?^/, ";").strip!
        end
    end

end
