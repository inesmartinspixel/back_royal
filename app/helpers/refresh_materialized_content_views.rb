class RefreshMaterializedContentViews

    # can be stubbed to true in specs
    def self.force_refresh_views
        false
    end

    def self.all_populated?
        views.last.populated?
    end

    def self.ensure_all_populated
        return if all_populated?

        self.refresh
    end

    def self.tables
        [
            PublishedCohortPlaylistLocalePack,
            PublishedCohortPeriod,
            PublishedCohortPeriodsStreamLocalePack,
            PublishedCohortStreamLocalePack,
            PublishedCohortLessonLocalePack
        ]
    end

    def self.views
        # None of the views here should reference users.  They will be refreshed whenever
        # a content item is published or has its groups changed.
        [
            ViewHelpers::PublishedStreams,
            ViewHelpers::PublishedPlaylists,
            ViewHelpers::PublishedPlaylistStreams,
            ViewHelpers::PublishedStreamLocalePacks,
            ViewHelpers::PublishedPlaylistLocalePacks,
            ViewHelpers::InstitutionPlaylistLocalePacks,
            ViewHelpers::InstitutionStreamLocalePacks,
            ViewHelpers::PublishedLessons,
            ViewHelpers::PublishedLessonLocalePacks,
            ViewHelpers::PublishedStreamLessons,
            ViewHelpers::PublishedStreamLessonLocalePacks
        ]
    end

    def self.refresh_and_rebuild_derived_content_tables
        self.refresh(true)
        tables.each do |klass|
            klass.rebuild
        end
    end

    def self.refresh(force = false)
        return if Rails.env.test? && !(force || self.force_refresh_views)

        ActiveRecord::Base.with_statement_timeout(0) do
            ActiveRecord::Base.connection.execute('refresh materialized view published_content_titles')
        end

        self.views.each do |klass|
            klass.refresh_concurrently
        end

        ActiveRecord::Base.transaction do
            ActiveRecord::Base.connection.execute('
                DELETE FROM content_views_refresh;
                INSERT INTO content_views_refresh (updated_at) VALUES (NOW());
            ')
        end

    end
end
