module TableHelper

    # The controller using sortable must define
    # - sort_column
    # - sort_direction
    # - permitted_query_params
    # See users_controller for an example

    def sortable(column, title = nil, options = {})
        title ||= column.titleize
        options = options.with_indifferent_access
        options['css_class'] = "" unless options['css_class']
        options['css_class'] += " " + (column == sort_column ? "current #{sort_direction}" : "")
        options['css_class'] += " sortable"
        direction = column == sort_column && sort_direction == "asc" ? "desc" : "asc"
        link_to title, {:sort => column, :direction => direction}.merge(permitted_query_params), options
    end

end
