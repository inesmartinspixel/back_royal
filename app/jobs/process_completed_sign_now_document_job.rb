class ProcessCompletedSignNowDocumentJob < ApplicationJob

    queue_as :process_completed_sign_now_document

    def self.ensure_job(sign_now_document_id)
        existing_job = Delayed::Job.where(queue: self.queue_name)
                    .where("handler ILIKE '\%arguments\%#{sign_now_document_id}\%'")

        return existing_job if existing_job.present?

        self.perform_later(sign_now_document_id)
    end


    def perform(sign_now_document_id)
        EditorTracking.transaction(self.class.name) do
            signable_document = SignableDocument.find_by_sign_now_document_id(sign_now_document_id)

            # Since we can only have 1 hook endpoint per event, and only have the one environment to work
            # from, any signing of Local Development or Staging Documents will trigger this error. If this
            # is frequent and annoying, we can perform a GET on the document to determine the `parent_id`
            # which will indicate which folder (Smartly environment) it originated from and disregard.
            if signable_document.nil?
                Raven.capture_exception("No signable_document found", {
                    extra: {
                        sign_now_document_id: sign_now_document_id
                    }
                })
                return
            end

            signable_document.download_completed_document_from_sign_now
        end

    end
end
