class ApplicationJob < ActiveJob::Base

    # If this is called with no arguments, then `perform_later`
    # must be called with a single argument, and it will be used
    # as the identifier
    #
    # If this is called with a block, the block will be invoked
    # with the arguments passed to `perform_later` and the return
    # value will be used as the identifier (NOTE: keeping this
    # commented out until someone wants to use it, but I've tested
    # in a console)
    def self.use_identifier(&block)
        self.class_eval do
            # if block_given?
            #     define_singleton_method(:get_identifier) do |*arguments|
            #         yield(*arguments)
            #     end
            # else
                define_singleton_method(:get_identifier) do |*arguments|
                    if arguments.size == 1
                        arguments[0]
                    else
                        raise "Canonly determine identifier when is exactly one argument"
                    end
                end
            # end
        end
    end
end