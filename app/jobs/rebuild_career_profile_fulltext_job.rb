class RebuildCareerProfileFulltextJob < ApplicationJob

    queue_as :rebuild_career_profile_fulltext

    def self.perform_later_with_debounce(career_profile_id)

        pending_job = Delayed::Job.where(queue: "rebuild_career_profile_fulltext")
            .where(locked_at: nil)
            .where(failed_at: nil)
            .where("handler ILIKE '\%arguments\%#{career_profile_id}\%'")
            .first

        if pending_job
            # If a job already exists that is not locked or failed and has the same arguments then
            # just update that job's run_at time
            pending_job.update_attribute(:run_at, Time.now + 1.minute)
        else
            # Otherwise schedule a new job for the future (giving a window during which subsequent jobs
            # will trigger the above if statement)
            self.set(wait_until: Time.now + 1.minute).perform_later(career_profile_id)
        end
    end


    def get_keyword_text(career_profile, fields)
        vals = [

            fields[:education],
            fields[:work],
            # Allow keyword searching to find city names. See https://trello.com/c/DJL7iZ5h
            career_profile.city_name

        ]

        extra_fields = fields[:extra]

        if extra_fields.include?('career_network_stuff')
            vals = vals += [

                # general free-text localized values
                career_profile.personal_fact,

                # user-supplied  / localized array values
                career_profile.skills ? career_profile.skills.map(&:text).join(' ') : nil,
                career_profile.awards_and_interests ? career_profile.awards_and_interests.map(&:text).join(' ') : nil,

                # non-localized locale-key (EN) array values
                #
                # Note: since we're currently limiting exposure to english speaking
                # hiring managers, this is probably sufficient for now since the keys generall
                # reflect the translated English values (see: `careers_module.js`).  We've
                # confirmed that this works ok with underscores in the keys as well
                career_profile.top_personal_descriptors ? career_profile.top_personal_descriptors.join(' ') : nil,
                career_profile.top_workplace_strengths ? career_profile.top_workplace_strengths.join(' ') : nil,
                career_profile.job_sectors_of_interest ? career_profile.job_sectors_of_interest.join(' ') : nil
            ]
        end

        if extra_fields.include?('student_network_anonymous_stuff')
            vals = vals += [

                # if we change which applications we look at (for example, if
                # we decide to include pre_accepted or deferred people in the student
                # network), we will need to update CohortApplicaiton#should_update_career_profile_fulltext?
                career_profile.user.accepted_application&.published_cohort&.title,

                # user-supplied  / localized array values
                career_profile.student_network_interests ? career_profile.student_network_interests.map(&:text).join(' ') : nil,

                # see comment in career_network_stuff about non-localized locale-key values
                career_profile.student_network_looking_for ? career_profile.student_network_looking_for.join(' ') : nil
            ]
        end

        # See note in update_fulltext about why we check pref_student_network_privacy here
        if extra_fields.include?('student_network_personal_stuff') && career_profile.user.pref_student_network_privacy == 'full'
            vals = vals += [

                # if we get more stuff from the user, we will
                # need to update User#should_update_career_profile_fulltext?
                career_profile.user.name,
                career_profile.user.nickname
            ]
        end

        CareerProfile.sanitize_fulltext_sql(vals.compact.join(' ')) || 'NULL'
    end


    def perform(career_profile_id)

        career_profile = CareerProfile.find_by_id(career_profile_id)

        return unless career_profile&.career_profile_active? || career_profile&.student_network_active?

        student_network_keyword_elements = {extra: ['student_network_anonymous_stuff']}
        career_profile_keyword_elements = {extra: ['career_network_stuff']}

        # prep more involved experiences info
        if career_profile.education_experiences
            career_profile_keyword_elements[:education] = (career_profile.education_experiences.map do |experience|
                [experience.educational_organization.text, experience.major, experience.minor].join(' ')
            end).join(' ')

            student_network_keyword_elements[:education] = (career_profile.education_experiences.map do |experience|
                # we do not show minors in the student network
                [experience.educational_organization.text, experience.major].join(' ')
            end).join(' ')
        end

        if career_profile.work_experiences

            career_profile_keyword_elements[:work] = (career_profile.work_experiences.map do |experience|
                [experience.professional_organization.text, experience.job_title, experience.responsibilities.join(' '), experience.industry].join(' ')
            end).join(' ')

            student_network_keyword_elements[:work] = (career_profile.work_experiences.map do |experience|
                # we do not show responsibilities in the student network
                [experience.professional_organization.text, experience.job_title, experience.industry].join(' ')
            end).join(' ')
        end

        # get and sanitize relevant strings.  If this user is
        # configured to only include anonymous information in the network, then
        # the student_network_full_keyword and student_network_anonymous_keyword fields will
        # have the same value, both with no personal information in them.  Which field gets searched
        # is based on the permissions of the user doing the searching.
        keyword_text = get_keyword_text(career_profile, career_profile_keyword_elements)
        student_network_anonymous_keyword_text = get_keyword_text(career_profile, student_network_keyword_elements)
        student_network_keyword_elements[:extra] << 'student_network_personal_stuff'
        student_network_full_keyword_text = get_keyword_text(career_profile, student_network_keyword_elements)

        # build out sql snippets
        keyword_sql = "setweight(to_tsvector('english', coalesce(#{keyword_text},'')), 'A')"
        student_network_full_keyword_sql = "setweight(to_tsvector('english', coalesce(#{student_network_full_keyword_text},'')), 'A')"
        student_network_anonymous_keyword_sql = "setweight(to_tsvector('english', coalesce(#{student_network_anonymous_keyword_text},'')), 'A')"

        if ActiveRecord::Base.connection.execute("SELECT 1 FROM career_profile_fulltext WHERE career_profile_id='#{career_profile.id}'").count == 0
            sql = "INSERT INTO career_profile_fulltext (
                        career_profile_id,
                        keyword_vector,
                        keyword_text,
                        student_network_full_keyword_vector,
                        student_network_full_keyword_text,
                        student_network_anonymous_keyword_vector,
                        student_network_anonymous_keyword_text,
                        created_at,
                        updated_at
                    )
                    VALUES (
                        '#{career_profile.id}',
                        #{keyword_sql},
                        #{keyword_text},
                        #{student_network_full_keyword_sql},
                        #{student_network_full_keyword_text},
                        #{student_network_anonymous_keyword_sql},
                        #{student_network_anonymous_keyword_text},
                        NOW(),
                        NOW()
                    )"
        else
            sql = "UPDATE career_profile_fulltext SET
                    keyword_vector = #{keyword_sql},
                    keyword_text = #{keyword_text},
                    student_network_full_keyword_vector = #{student_network_full_keyword_sql},
                    student_network_full_keyword_text = #{student_network_full_keyword_text},
                    student_network_anonymous_keyword_vector = #{student_network_anonymous_keyword_sql},
                    student_network_anonymous_keyword_text = #{student_network_anonymous_keyword_text},
                    updated_at = NOW()
                    WHERE career_profile_id = '#{career_profile.id}'"
        end

        # update or insert the fulltext
        ActiveRecord::Base.connection.execute(sql)
    end

end