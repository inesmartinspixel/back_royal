class SendHiringManagerTrackingEmailsJob < ApplicationJob

    include ScheduledJob

    queue_as :send_hiring_manager_tracking_emails
    self.run_at_hour 0
    self.run_on_day :monday

    def perform
        # Automatically retries in the event of ActiveRecord::PreparedStatementCacheExpired
        EditorTracking.transaction('SendHiringManagerTrackingEmailsJob') do

            hiring_team_ids_with_interests_needing_review = OpenPosition.joins(:candidate_position_interests_needing_review)
                .joins(:hiring_manager)
                .where(archived: false)
                .distinct
                .pluck(:hiring_team_id)

            User.includes(
                :connected_candidate_relationships => {:career_profile => :featured_work_experience},
                :candidate_relationships_saved_for_later => {:career_profile => :featured_work_experience},
                :unarchived_open_positions => [:candidate_position_interests_needing_review, :accepted_and_unhidden_candidate_position_interests]
            ).where(hiring_team_id: hiring_team_ids_with_interests_needing_review).find_each do |hiring_manager|

                # this logic should be in line with careersNetworkViewModel#loadCandidatePositionInterests
                num_total_candidates_interested = 0
                num_new_candidates_interested = 0
                unarchived_open_positions = hiring_manager.hiring_team.unarchived_open_positions
                unarchived_open_positions.each do |open_position|
                    num_new_candidates_interested += open_position.candidate_position_interests_needing_review.size
                    num_total_candidates_interested += open_position.accepted_and_unhidden_candidate_position_interests.size
                end

                # For connections we don't count the teammates
                connected_relationships = hiring_manager.connected_candidate_relationships
                relationships_saved_for_later = hiring_manager.candidate_relationships_saved_for_later

                # See https://fly.customer.io/env/24964/campaigns/1000161/overview
                pre_pivot_attributes = {
                    has_created_an_open_position: hiring_manager.open_positions.any?,
                    num_unarchived_open_positions: unarchived_open_positions.size,
                    num_new_candidates_interested: num_new_candidates_interested,
                    num_total_candidates_interested: num_total_candidates_interested,
                    num_connections: connected_relationships.size,
                    num_saved_for_later: relationships_saved_for_later.size,
                    connected_relationships: hiring_relationships_json(connected_relationships),
                    relationships_saved_for_later: hiring_relationships_json(relationships_saved_for_later)
                }

                unarchived_open_positions_with_interests_needing_review = unarchived_open_positions
                    .select { |position| position.candidate_position_interests_needing_review.size > 0 }
                    .sort_by { |position| [position.candidate_position_interests_needing_review.size, position.title] }
                    .reverse

                positions_attributes = unarchived_open_positions_with_interests_needing_review.map do |position|
                    {
                        id: position.id,
                        title: position.title,
                        hiring_manager_name: position.hiring_manager.name,
                        num_interests_needing_review: position.candidate_position_interests_needing_review.size
                    }
                end

                Event.create_server_event!(
                    SecureRandom.uuid,
                    hiring_manager.id,
                    'hiring_manager:send_tracker_email',
                    {
                        positions: positions_attributes
                    }.merge(pre_pivot_attributes)
                ).log_to_external_systems
            end
        end
    end

    private
    def hiring_relationships_json(relationships)
        relationships.sort_by(&:last_activity_at).reverse.slice(0, 8).map do |hiring_relationship|
            hiring_relationship_json(hiring_relationship)
        end
    end

    private
    def hiring_relationship_json(hiring_relationship)
        has_unread_messages = !!(hiring_relationship&.conversation&.messages&.map(&:receipts)&.flatten&.select { |r| r.receiver_id == hiring_relationship.hiring_manager_id }&.map(&:is_read)&.include?(false))

        featured_work_exp = hiring_relationship.career_profile.featured_work_experience
        {
            "avatar_url" => hiring_relationship.career_profile.user.avatar_url_with_fallback,
            "name" => hiring_relationship.career_profile.user.name,
            "matched_at" => hiring_relationship.matched_at.to_timestamp,
            "days_since_matched" => hiring_relationship.days_since_matched,
            "current_position_title" => featured_work_exp && featured_work_exp.job_title,
            "current_position_company_name" => featured_work_exp && featured_work_exp.professional_organization.text,
            "position_title" => hiring_relationship.open_position && hiring_relationship.open_position.title,
            "conversation_url" => hiring_relationship.hiring_manager_status == 'saved_for_later' ? nil : "https://smart.ly/hiring/tracker?connectionId=#{hiring_relationship.candidate_id}",
            "has_unread_messages" => has_unread_messages
        }
    end

end