# Refer to SyncFromAirtableBatchHelper information on how this job works.
class SyncFromAirtableJob < ApplicationJob

    include ScheduledJob
    include SyncFromAirtableBatchHelper::MasterJobHelper

    queue_as :sync_from_airtable

    # 3:30 AM Eastern Time
    self.run_at_hour 3
    self.run_at_minute 30

    def perform(database_sync: true, decision_sync: true)
        self.enqueue_batch_jobs(SyncFromAirtableBatchJob, database_sync: database_sync, decision_sync: decision_sync)
    end
end
