=begin

    We have consistently seen a small number of events that do
    not get copied from postgres to redshift.  This job checks
    once per day for any events that are in postgres but not in
    redshift and copies them over to redshift.

    * Events that were copied to redshift by this job
        can be found at https://s3.console.aws.amazon.com/s3/buckets/uploads.smart.ly/ensure-events-copied-to-redshift/?region=us-east-1&tab=overview

    * A record of how many events were copied to redshift
        by this job is logged to CloudWatch under the
        "Kinesis Firehose" namespace

        * If any events are found in redshift that are not in
        postgres, that is logged as an error to sentry

=end
class EnsureEventsCopiedToRedshiftJob < ApplicationJob

    include ScheduledJob

    queue_as :ensure_events_copied_to_redshift

    # 4:00 AM Eastern Time
    self.run_at_hour 4
    self.run_at_minute 0

    # Normally, we do not expect a min_created_at and max_created_at to
    # be passed in here.  When running from a console, though, this can
    # be useful:
    #
    # 1. In development, if you want to copy all of the events from your
    #       postgresql database over to redshift
    # 2. For recovering old events from a snapshot
    def perform(min_created_at = nil, max_created_at = nil)
        inconsistencies = get_inconsistencies(min_created_at, max_created_at)
        counts, total_count = put_missing_events_in_firehose(inconsistencies)
        log_to_cloudwatch(counts, total_count)
        send_sentry_warning_about_extra_events(inconsistencies)
    end

    def get_inconsistencies(min_created_at, max_created_at)
        DbLinker.setup_red_royal_dblink

        # For the beginning of the window in which we're looking for inconsistencies,
        # start a little while after the oldest event in postgres. That way, if the oldest
        # events end up getting deleted while this is running, they won't look like inconsistencies.
        # (Even if that were to happen, import_and_prune_redshift_events would be smart enough
        # to prevent inserting duplicates).  Aso, limit to 3 days ago so that if we've increased the window
        # in which we're keeping events in postgres, we don't end up running this repeatedly over
        # many days of events.
        if min_created_at.nil?
            min_created_at = [Event.minimum(:created_at) + 1.hour, 3.days.ago].max
            min_created_at = min_created_at.utc.strftime('%Y-%m-%d %H:%M:%S.%N')
        end

        # For the end of the window in which we're looking for inconsistencies, use the
        # max created_at in redshift.  That way, if something has gone wrong with import_and_prune_redshift_events,
        # and events are not getting copied over to redshift at all, this job will not try to push
        # thousands or millions of events into the firehose when they are already in s3 waiting
        # to be processed by import_and_prune_redshift_events.
        if max_created_at.nil?
            max_created_at = (RedshiftEvent.maximum(:created_at) - 1.hour)
            max_created_at = max_created_at.utc.strftime('%Y-%m-%d %H:%M:%S.%N')
        end

        ActiveRecord::Base.with_statement_timeout(0) do
            ActiveRecord::Base.connection.execute(%Q~
                with redshift_events AS MATERIALIZED (
                    select * from
                    dblink('red_royal', $REDSHIFT$
                        select
                            id
                            , user_id
                            , created_at
                            , event_type
                            , substring(page_load_id::text  from 1 for 4) as page_load
                        from events
                        where events.created_at >= '#{min_created_at}' and events.created_at < '#{max_created_at}'
                            -- We know these won't be in RDS, so we don't treat them an inconsistencies
                            AND events.event_type NOT IN (#{Event::CLIENT_SIDE_EVENTS_NOT_PERSISTED_TO_RDS.map { |e| ActiveRecord::Base.connection.quote(e) }.join(',')})
                    $REDSHIFT$) AS redshift_events (
                                id uuid
                                , user_id uuid
                                , created_at timestamp
                                , event_type text
                                , page_load text
                            )
                    )
                    , local_events AS MATERIALIZED (
                        select
                            id
                            , user_id
                            , created_at
                            , event_type
                            , substring((payload->>'page_load_id')::text  from 1 for 4) as page_load
                        from events where
                            events.created_at >= '#{min_created_at}' and events.created_at < '#{max_created_at}'
                    )
                    select
                        case when local_events.id is not null then 'postgres' else 'redshift' end as location,
                        users_deletions.created_at deleted_at,
                        coalesce(local_events.id::text, redshift_events.id::text) id,
                        coalesce(local_events.created_at, redshift_events.created_at) created,
                        coalesce(local_events.event_type, redshift_events.event_type) event_type,
                        coalesce(local_events.user_id::text, redshift_events.user_id::text) usr,
                        coalesce(local_events.page_load, redshift_events.page_load) page_load
                    from (local_events full outer join redshift_events using (id))
                        left join users_deletions on redshift_events.user_id = users_deletions.user_id
                    where (redshift_events.id is null or local_events.id is null)

                        -- we only rolled out code after 2019-05-01 that made sure events would not be
                        -- sitting in the firehose when a user was deleted.  It takes about 6 hours now before
                        -- a deleted users events are deleted.  So, we can safely ignore any events for a user
                        -- who was deleted in the last 6 hours.
                        and (users_deletions.created_at < '2019-05-02' or users_deletions.created_at > now() - interval '7 hours') is distinct from true
                    order by
                        users_deletions.created_at,
                        redshift_events.created_at,
                        local_events.created_at;
            ~).to_a
        end
    end

    def put_missing_events_in_firehose(inconsistencies)
        # find any events that are in postgres but not redshift.  Put them in them
        # firehose and log which events had to be copied in this way
        event_ids = inconsistencies.map { |row| row['location'] == 'postgres' ? row['id'] : nil }.compact
        counts = Hash.new { |hash, event_type| hash[event_type] = 0 }
        total_count = 0

        # we do our own batching because passing Event.where(id: event_ids).find_in_batches could
        # end up generating a huge query if there are lots of ids
        while event_ids.any?
            batch = event_ids.slice!(0, 500)
            events = Event.where(id: batch)

            Event::PutEventsInFirehose.put(events)
            lines = events.map do |event|
                counts[event.event_type] += 1
                total_count += 1
                [event.id, event.created_at.utc.strftime('%Y-%m-%d %H:%M:%S.%N'), event.event_type].join(',')
            end
            LogToS3.log('ensure-events-copied-to-redshift', "#{events.size} events", lines.join("\n") )
        end

        [counts, total_count]
    end

    def log_to_cloudwatch(counts, total_count)
        metric_data = counts.map do |pair|
            event_type, count = pair

            {
                metric_name: "Events copied by Ensure Job - #{event_type}",
                value: count,
                unit: 'Count'
            }
        end

        metric_data << {
            metric_name: "Events copied by Ensure Job - __Total",
            value: total_count,
            unit: 'Count'
        }

        # we can only log 20 metrics with each call
        metric_data.each_slice(20) do |chunk|
            Cloudwatch.put_metric_data(
                ns_suffix: 'Kinesis Firehose',
                metric_data: chunk)
        end
    end

    def send_sentry_warning_about_extra_events(inconsistencies)
        event_ids = inconsistencies.map { |row| row['location'] == 'redshift' ? row['id'] : nil }.compact
        return if event_ids.empty?
        Raven.capture_exception(
            "Events are in redshift that are not in postgres",
            {
                extra: {
                    first_100_event_ids: event_ids.slice(0, 100),
                    count: event_ids.size
                }
            })
    end

end