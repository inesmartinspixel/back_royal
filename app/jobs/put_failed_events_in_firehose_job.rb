class PutFailedEventsInFirehoseJob < ApplicationJob
    queue_as :put_failed_events_in_firehose

    def perform(event_id)

        event = Event.find(event_id)
        response = Event::PutEventsInFirehose.put([event])

        event_response = response.request_responses[0]
        if event_response.error_code
            err = RuntimeError.new("Firehose error: #{event_response.error_message}")
            err.raven_options = Event::PutEventsInFirehose.raven_options_for_response(event_response, event)
            raise err
        end
    end
end