require 'json'

class SendRecommendedPositionsEmailJob < ApplicationJob

    queue_as :send_recommended_positions_email

    def build_subject(positions)
        raise "Cannot build subject with no positions" if positions.nil? || positions.size == 0

        base_string = "#{positions.first.title} at #{positions.first.hiring_manager.company_name}"

        if positions.size == 1
            return base_string
        elsif positions.size == 2
            return "#{base_string} and more"
        elsif positions.length > 2
            return "#{base_string} + #{positions.size - 1} jobs for you"
        end
    end

    def perform(candidate_id)
        # Note that it's important for this logic to be wrapped in a transaction
        # so that a single failure will revert all of the events.
        EditorTracking.transaction(self.class.name) do
            candidate = User.find_by_id(candidate_id)

            # There's a window, albeit small, during the time this job was scheduled and
            # when it actually runs in which the user could have been deleted.
            return if candidate.nil?

            # When we trigger a recommended position email in customer.io we keep record in
            # the notified_recommended_open_positions table so that we don't send an email to a user
            # for the same position twice. At one time we were using Redshift events for this, but the
            # query became too slow.
            # See open_position_relation_extensions#not_notified
            new_recommended_positions = OpenPosition.all
                .recommended_and_viewable(candidate_or_id: candidate)
                .not_notified(candidate_or_id: candidate)
                .to_a

            return if new_recommended_positions.empty?

            new_recommended_positions.each do |position|
                NotifiedRecommendedOpenPosition.create!(user_id: candidate_id, open_position_id: position.id)
            end

            # Create a server event to send to customer.io for triggering an email with
            # the new recommended positions
            subject = build_subject(new_recommended_positions)
            positions_hash = new_recommended_positions.map do |position|
                {
                    title: position.title,
                    company_logo_url: position.company_logo_url,
                    company_name: position.company_name,
                    location_string: Location.location_string(position.place_details)
                }
            end
            Event.create_server_event!(
                SecureRandom.uuid,
                candidate.id,
                'candidate:send_recommended_positions_email',
                {
                    subject: subject,
                    positions: positions_hash
                }
            ).log_to_external_systems
        end
    end
end