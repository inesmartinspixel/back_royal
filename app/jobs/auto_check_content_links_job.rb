class AutoCheckContentLinksJob < ApplicationJob

    attr_accessor :bad_links, :passing_links

    include ScheduledJob

    queue_as :auto_check_content_links

    # 3:00 AM Eastern Time every Monday
    self.run_at_hour 3
    self.run_at_minute 0
    self.run_on_day :monday

    def initialize
        super
        @bad_links = {}
        @passing_links = {}
    end

    def perform
        count = 0
        all_published_streams = Lesson::Stream.includes(:lessons).all_published
        total_stream_count = all_published_streams.count

        puts "Starting to verify external links for #{total_stream_count} streams"

        all_published_streams.in_batches(of: 2) do |streams|
            count += streams.count
            self.verify_links_for_streams(streams)
            puts "Processed #{count}/#{total_stream_count} streams"
        end

        self.create_and_log_event_to_external_systems!
        puts "Finished processing #{count}/#{total_stream_count} published streams"
    end

    def verify_links_for_streams(streams)
        streams.each do |stream|
            puts "Verifying external links for #{stream.title}"

            ['summaries', 'resource_links', 'resource_downloads'].each do |attr|
                verify_links_for_stream_attr(stream, attr)
            end

            verify_links_for_stream_lessons(stream)
        end
    end

    def verify_links_for_stream_attr(stream, attr)
        puts "Verifying external links in stream #{attr}"
        stream[attr]&.each do |val|
            verify_link(val['url'], stream)
        end
    end

    def verify_links_for_stream_lessons(stream)
        stream.lessons.each do |lesson|
            verify_links_for_stream_lesson(lesson)
        end
    end

    def verify_links_for_stream_lesson(lesson)
        puts "Verifying external links in stream lesson: '#{lesson.title}'"
        lesson.content.frames.each do |frame|
            # text content: look for external links
            frame.content_links.each do |url|
                verify_link(url, lesson, frame) if url
            end
        end
    end

    def verify_link(url, content_item, frame = nil)
        normalized_url = Addressable::URI.parse(url).normalize

        # If we see that the URL has already been processed as a good link, then we can skip it.
        # Similarly, if we see that the link points to an internal source, like an Smartly course,
        # then we can also skip it.
        if !url.nil? && (@passing_links[normalized_url] || url.include?('//smart.ly/course/') || url.include?('//quantic.mba/course/') || url.include?('//quantic.edu/course/'))
            puts "Skipping known link to #{url}"
            return
        end

        # If we see that this URL has been previously recognized as a bad link,
        # we can skip performing the head request and simply add the content_item
        # to the list of editor links for this bad URL. NOTE: We use to to_s value
        # of the normalized_url in the bad_links because the bad_links hash gets
        # included in the event payload that gets sent to Customer.io, which expects
        # strings as the event object keys, not objects.
        if @bad_links[normalized_url.to_s]
            add_content_item_info_to_bad_links_map(normalized_url, content_item, frame)
            return
        end

        http = Net::HTTP.new(normalized_url.host, normalized_url.port)
        initial_response = nil

        begin
            initial_response = http.request_head(normalized_url.path)
        rescue Exception => e
            # Some links may experience an "execution expired" error because the request took
            # too long to complete. While most occurrences of this result in an actual bad link,
            # we've found instances where a GET request will produce a 200, which can be verified
            # in the browser. So, before we mark it as a bad link, we first attempt a GET request
            # to see if it truly is a bad link.
            initial_response = issue_get_request(normalized_url, content_item, frame)
        end

        return unless initial_response

        if initial_response.code == '404'
            # We've noticed that some links result in a 404 status code for the HEAD request,
            # but when you load up the link in a browser, the request is successful. So, similar
            # to what we do above, we issue a GET request before we mark the link as a bad link.
            second_response = issue_get_request(normalized_url, content_item, frame)

            return unless second_response

            if second_response.code == '404'
                add_content_item_info_to_bad_links_map(normalized_url, content_item, frame)
            else
                @passing_links[normalized_url] = true
            end
        else
            @passing_links[normalized_url] = true
        end
    end

    def issue_get_request(normalized_url, content_item, frame)
        get_response = nil
        begin
            # NOTE: This GET request is done with the HTTParty gem because it doesn't result in
            # an "execution expired" error for links that load just fine in a browser, unlike the
            # Net::HTTP#request_get method
            get_response = HTTParty.get(normalized_url)
        rescue Exception => e
            # if for whatever reason this GET request fails, mark the link as a bad link
            add_content_item_info_to_bad_links_map(normalized_url, content_item, frame)
        end
        get_response
    end

    def add_content_item_info_to_bad_links_map(normalized_url, content_item, frame = nil)
        # The bad_links gets included in the event payload that gets sent to Customer.io,
        # so we need to use the to_s value of the normalized_url object as the key in bad_links
        # because Customer.io expects the event object keys to be strings, not objects.
        url = normalized_url.to_s
        puts "Found bad link (#{url}) for #{content_item.class} '#{content_item.title}'"

        # The passed in content_item can be a lesson and lessons can be used in more than one stream.
        # If we recognize that the editor_link for the lesson (content_item) has already been added
        # for the passed in normalized_url, then we should return early to prevent adding the lesson's
        # info to the bad_links map again.
        editor_link = (frame || content_item).editor_link
        if @bad_links[url].present?
            editor_link_already_included = !!@bad_links[url].detect { |content_info| content_info[:editor_link] == editor_link }
            return if editor_link_already_included
        end

        @bad_links[url] = (@bad_links[url] || []) << { editor_link: editor_link, title: content_item.title }
    end

    # separate method for specs and for easier manual testing
    def create_and_log_event_to_external_systems!
        Event.create_server_event!(
            SecureRandom.uuid,
            'a576a6fc-175f-4013-8637-0005b2f64374', # Alexie
            'auto_check_content_links:bad_links',
            {
                bad_links: @bad_links
            }
        ).log_to_external_systems
    end
end
