class DeleteOldEventsJob < ApplicationJob
    include ScheduledJob

    queue_as :delete_old_events

    # 3:15 AM Eastern Time
    self.run_at_hour 3
    self.run_at_minute 15

    def perform
        if days_to_keep_events_in_postgres && days_to_keep_events_in_postgres >= 0

            now = Time.now
            has_more = true
            total_deleted = 0
            query = Event.where("created_at < ?", Time.now - days_to_keep_events_in_postgres.days)
                .limit(delete_old_events_batch_size)
                .select(:id).to_sql

            Cloudwatch.put_timing_metric_data(metric_name: 'DeleteOldEventsJob.delete_events') do
                while has_more
                    ActiveRecord::Base.with_statement_timeout(0) do
                        delete_count = Event.where("id in (#{query})").delete_all
                        total_deleted += delete_count
                        has_more = delete_count > 0
                    end
                    sleep delete_old_events_batch_sleep
                end
            end

            Cloudwatch.put_metric_data(
                ns_suffix: 'Performance',
                metric_data: [{
                    metric_name: 'DeleteOldEventsJob.batch_size',
                    value: delete_old_events_batch_size,
                    unit: 'Count'
                }])

            Cloudwatch.put_metric_data(
                ns_suffix: 'Performance',
                metric_data: [{
                    metric_name: 'DeleteOldEventsJob.delete_old_events_batch_sleep',
                    value: delete_old_events_batch_sleep,
                    unit: 'Seconds'
                }])

            Cloudwatch.put_metric_data(
                ns_suffix: 'Performance',
                metric_data: [{
                    metric_name: 'DeleteOldEventsJob.events_deleted',
                    value: total_deleted,
                    unit: 'Count'
                }])

            # Vacuum cannot be run within a test's DB transaction
            if !Rails.env.test?
                Cloudwatch.put_timing_metric_data(metric_name: 'DeleteOldEventsJob.vacuum') do
                    ActiveRecord::Base.with_statement_timeout(0) do
                        ActiveRecord::Base.connection.execute("VACUUM ANALYZE #{Event.table_name}")
                    end
                end
            end

        end
    end

    def days_to_keep_events_in_postgres
        unless defined? @days_to_keep_events_in_postgres
            @days_to_keep_events_in_postgres = ENV['DAYS_TO_KEEP_EVENTS_IN_POSTGRES'].nil? ? nil : ENV['DAYS_TO_KEEP_EVENTS_IN_POSTGRES'].to_i
        end
        @days_to_keep_events_in_postgres
    end

    def delete_old_events_batch_size
        unless defined? @delete_old_events_batch_size
            @delete_old_events_batch_size = ENV['DELETE_OLD_EVENTS_BATCH_SIZE'].nil? ? 10000 : ENV['DELETE_OLD_EVENTS_BATCH_SIZE'].to_i
        end
        @delete_old_events_batch_size
    end

    def delete_old_events_batch_sleep
        unless defined? @delete_old_events_batch_sleep
            @delete_old_events_batch_sleep = ENV['DELETE_OLD_EVENTS_BATCH_SLEEP'].nil? ? 1 : ENV['DELETE_OLD_EVENTS_BATCH_SLEEP'].to_i
        end
        @delete_old_events_batch_sleep
    end

end
