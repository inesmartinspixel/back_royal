class LogToCustomerIoJob < ApplicationJob
    queue_as :log_to_customerio

    attr_accessor :event_id, :keys, :properties

    # By default, log_to_customerio jobs are given top priority (0) because we'd like these jobs
    # to occur as soon as possible in response to an user-triggered event, but there may be times
    # when we'd like these jobs to have a lower priority level (higher numerical value) because
    # they were enqueued due to a backfill, rake task, or a large delayed job. In those instances,
    # MASS_LOG_PRIORITY should be used.
    DEFAULT_PRIORITY = 0
    MASS_LOG_PRIORITY = LogToCustomerIoJob::DEFAULT_PRIORITY + 1

    def perform(event_id, keys = nil)
        self.event_id = event_id
        event

        self.keys = keys
        self.properties = get_payload_properties

        # Return if the user no longer exists
        # This prevents us from resurrecting a user that
        # has been deleted from this third-party system
        return if !User.where(id: event.user_id).exists?

        track
    end

    def event
        @event ||= Event.find(event_id)
    end

    def track
        if defined? $customerio
            $customerio.track(
                event.user_id,  # required
                event.event_type,  # required
                properties
            )
        end
    end

    def get_payload_properties
        properties = event.payload.with_indifferent_access
        properties = properties.slice(*keys) unless keys.nil?
        properties
    end

end
