class UpdateCohortStatusChangesJob < ApplicationJob

    queue_as :update_cohort_status_changes
    use_identifier

    # We debounce these because the code in CohortStatusChange#update_cohort_status_changes_for_application is not
    # resilient to being run concurrently on the same cohort application on multiple processes
    def self.perform_later_with_debounce(cohort_application_id)

        pending_job = Delayed::Job.where(queue: "update_cohort_status_changes", identifier: cohort_application_id)
            .where(locked_at: nil)
            .where(failed_at: nil)
            .first

        if pending_job
            pending_job.update(run_at: Time.now + 1.minute)
        else
            self.set(wait_until: Time.now + 1.minute).perform_later(cohort_application_id)
        end
    end

    def perform(cohort_application_id)
        CohortStatusChange.update_cohort_status_changes_for_application(cohort_application_id)
    end
end