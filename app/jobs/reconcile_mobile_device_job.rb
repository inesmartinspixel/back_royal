class ReconcileMobileDeviceJob < ApplicationJob
    queue_as :reconcile_mobile_device

    attr_accessor :user_id, :device_token, :platform

    def perform(user_id, device_token = nil, platform = nil)
        self.user_id = user_id
        self.device_token = device_token
        self.platform = platform

        # Return if the user no longer exists
        return if !user.present?

        # If we passed in a device_token and platform, we need to either
        # create a new mobile_devices record or update an existing one.
        if device_token.present? && platform.present?
            create_or_update_mobile_device
        end

        # Since the user is saved synchronously, we can use `pref_allow_push_notifications`
        # to determine if the user does, or doesn't, want push notifications. This enables
        # us to disregard job ordering and/or debouncing - if the user toggles this pref quickly
        # and queues several jobs, the jobs will still perform the correct action.
        if user.pref_allow_push_notifications == true
            add_all_mobile_devices
        elsif
            delete_all_mobile_devices
        end
    end

    def user
        @user ||= User.find_by_id(user_id)
    end

    def mobile_devices
        @mobile_devices ||= MobileDevice.where(user_id: user_id).to_a
    end

    # Creates or updates a mobile_devices record in back_royal
    def create_or_update_mobile_device
        device = MobileDevice.where(user_id: user_id, device_token: device_token).to_a.first

        if !device.present?
            # NOTE: this can cause a duplicate key error, but then the job fails and it
            # resolves on the retry, so not worrying about it right now: https://sentry.io/pedago/production-rails/issues/815839940/
            device = MobileDevice.create!({
                user_id: user_id,
                device_token: device_token,
                platform: platform,
                last_used_at: Time.now
            })
        else
            device.update_attribute(:last_used_at, Time.now)
        end
    end

    # Identifies all mobile devices associated with user on customer.io
    def add_all_mobile_devices
        if defined? $customerio
            mobile_devices.each do |device|
                $customerio.add_device(
                    device.user_id,
                    device.device_token,
                    device.platform,
                    {
                        :last_used => device.last_used_at.to_timestamp
                    }
                )
            end
        end
    end

    # Delete all mobile devices associated with user from customer.io
    #
    # We do not delete the device from FCM because we would then never
    # be able to re-register it (and because there is less chance of
    # us accidentally messaging it when it is in FCM than there would be
    # if we left it in customer.io)
    def delete_all_mobile_devices
        if defined? $customerio
            mobile_devices.each do |device|
                $customerio.delete_device(user_id, device.device_token)
            end
        end
    end

end

