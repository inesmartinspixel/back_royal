# The finance team needs a report that shows the refund that each of our users
# would be entitled to today if they requested a refund.  Since the logic for
# calculating a refund is quite complex, I did not want to reproduce it in sql.
#
# Instead, we have this scheduled job which populates the user_refund_entitlements table.
class UpdateRefundReportJob < ApplicationJob

    include ScheduledJob

    queue_as :update_refund_report

    # 3:45 AM Eastern Time
    self.run_at_hour 3
    self.run_at_minute 45

    def perform
        EditorTracking.transaction("UpdateRefundReportJob") do

            # Find cohort applications that do not yet have a record in user_refund_entitlements
            # or for which something has happened that could change the refund amount, and re-write the
            # record in user_refund_entitlements. (The logic for which ones need to be updated is
            # in `relevant_users_query` below)
            #
            # We are also selecting the two `updated_to...` values from the relevant_users_query
            # so that we can use them to update the columns in user_refund_entitlements
            cohort_applications = CohortApplication.with(relevant_users: relevant_users_query)
                .select(CohortApplication.column_names + [
                    'relevant_users.updated_to_max_application_updated_at',
                    'relevant_users.updated_to_last_lesson_activity_time'
                ])
                .includes(CohortApplication.includes_needed_for_json_eager_loading)
                .includes(user: :lesson_streams_progresses)
                .joins("join relevant_users on relevant_users.last_cohort_application_id = cohort_applications.id")
                .where("relevant_users.requires_update = true")

            # create a hash of all existing user_refund_entitlements records.
            # use Hash.new so that the default value in the hash in a newly initialized
            # instance of UserRefundEntitlement
            user_refund_entitlements = Hash.new do |hash, user_id|
                hash[user_id] =  UserRefundEntitlement.new(user_id: user_id)
            end

            UserRefundEntitlement.where(user_id: cohort_applications.map(&:user_id)).each do |record|
                user_refund_entitlements[record.user_id] = record
            end

            cohort_applications.each do |cohort_application|
                record = user_refund_entitlements[cohort_application.user_id]
                record.refund_amount = cohort_application.refund_details && cohort_application.refund_details[:refund_amount]

                # We selected these two `updated_to...` values above, so active record shoved them
                # onto the cohort_application instances.
                record.updated_to_max_application_updated_at = cohort_application.updated_to_max_application_updated_at
                record.updated_to_last_lesson_activity_time = cohort_application.updated_to_last_lesson_activity_time
                record.save!
            end

            # This is unlikely, but find cases where an application has been deleted or something
            # has changed such that an existing refund record is no longer valid, we delete the record
            # from user_refund_entitlements
            query = UserRefundEntitlement.with(relevant_users: relevant_users_query)
                .joins("left outer join relevant_users using (user_id)")
                .where("relevant_users is null")
                .select(:id)

            # `delete_all` does not play nice with `ctes_in_my_pg` gem
            UserRefundEntitlement.where("id IN (#{query.to_sql})").delete_all
        end

    end

    def relevant_users_query
        payment_program_types = Cohort::ProgramTypeConfig.configs.select(&:supports_payments?).map(&:program_type)
        program_type_string = payment_program_types.map { |program_type| "'#{program_type}'" }.join(',')

        # We need to update a record in user_refund_entitlements if
        # 1. the user has made progress since the last update to user_refund_entitlements
        # 2. the cohort application has been updated since the last update to user_refund_entitlements
        %Q~
        select
            cohort_applications.user_id
            , (array_agg(cohort_applications.id order by applied_at desc))[1] as last_cohort_application_id
            , user_progress_records.last_lesson_activity_time
            , max(cohort_applications.updated_at) last_application_updated_at
            , coalesce(user_refund_entitlements.updated_to_max_application_updated_at, '1979/01/01'::timestamp) < max(cohort_applications.updated_at)
                OR
                coalesce(user_refund_entitlements.updated_to_last_lesson_activity_time, '1970/01/01'::timestamp) < user_progress_records.last_lesson_activity_time
                AS requires_update
            , max(cohort_applications.updated_at) updated_to_max_application_updated_at
            , user_progress_records.last_lesson_activity_time updated_to_last_lesson_activity_time
        from cohort_applications
            join cohorts on cohorts.id = cohort_applications.cohort_id
            left outer join user_progress_records using (user_id)
            left outer join user_refund_entitlements
                using (user_id)
        where (registered or status='deferred')
            and cohorts.program_type in (#{program_type_string})
        group by
            cohort_applications.user_id
            , user_progress_records.last_lesson_activity_time
            , user_refund_entitlements.updated_to_max_application_updated_at
            , user_refund_entitlements.updated_to_last_lesson_activity_time
        ~
    end

end
