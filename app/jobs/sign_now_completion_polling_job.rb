class SignNowCompletionPollingJob < ApplicationJob

    include ScheduledJob

    queue_as :sign_now_completion_polling

    # 4:30 AM Eastern Time
    self.run_at_hour 4
    self.run_at_minute 30

    def perform
        count = 0
        total_count = unsigned_signable_documents.count

        puts "Checking for signing completion on #{total_count} unsigned documents"

        unsigned_signable_documents.find_each do |document|
            count += 1
            document.check_and_queue_download_if_necessary
            # I don't trust SignNow enough to hammer their API relentlessly, but who knows?
            sleep 0.5 unless Rails.env.test?
            puts "Processed #{count}/#{total_count} unsigned documents" if count % 25 == 0
        end

        puts "Finished processing #{count}/#{total_count} unsigned documents"
    end

    def unsigned_signable_documents
        @unsigned_signable_documents ||= SignableDocument.where(file_fingerprint: nil).where('sign_now_signing_link_expiry >= ?', Time.now)
        @unsigned_signable_documents
    end

end
