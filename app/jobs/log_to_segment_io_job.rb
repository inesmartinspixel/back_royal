class LogToSegmentIoJob < ApplicationJob
    queue_as :log_to_segment

    attr_accessor :event_id, :keys

    # By default, log_to_segment jobs are given top priority (1) because we'd like these jobs to
    # occur as soon as possible in response to an user-triggered event, but there may be times
    # when we'd like these jobs to have a lower priority level (higher numerical value) because
    # they were enqueued due to a backfill, rake task, or a large delayed job. In those instances,
    # MASS_LOG_PRIORITY should be used.
    DEFAULT_PRIORITY = 1
    MASS_LOG_PRIORITY = LogToSegmentIoJob::DEFAULT_PRIORITY + 1

    # FIXME: we can remove the integrations param once there are
    # 0 enqueued LogToSegmentIoJobs with an integrations argument
    def perform(event_id, keys = nil, integrations = {})

        self.event_id = event_id
        self.keys = keys

        # Return if the user no longer exists
        # This prevents us from resurrecting a user that
        # has been deleted from this third-party system
        return if !User.where(id: event.user_id).exists?

        track
    end

    def event
        @event ||= Event.find(event_id)
    end

    def track
        properties = event.payload.with_indifferent_access
        properties = self.flatten_hash(properties)
        properties = properties.slice(*keys) unless keys.nil?

        Analytics.track({
            user_id: event.user_id,
            event: event.event_type,
            timestamp: event.estimated_time,
            properties: properties
        }.with_indifferent_access)
    end

    # http://stackoverflow.com/questions/23521230/flattening-nested-hash-to-a-single-hash-with-ruby-rails
    def flatten_hash(hash)
        hash.each_with_object({}) do |(k, v), h|
            if v.is_a? Hash
                flatten_hash(v).map do |h_k, h_v|
                    h["#{k}.#{h_k}".to_sym] = h_v
                end
            else
                h[k] = v
            end
        end
    end
end