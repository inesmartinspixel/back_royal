class Cohort::OnPeriodActionJob < ApplicationJob

    queue_as :cohort__on_period_action

    def perform(cohort_id, period_index, action_id)
        cohort = Cohort.cached_published(cohort_id)
        period = cohort.periods[period_index - 1]
        action = period["actions"].find { |hash| hash["id"] == action_id }
        target_time = cohort.relative_time_from_end_of_period(action["days_offset_from_end"], period)

        action_attributes = cohort.event_attributes_for_period(period).merge({
            action_rule: action["rule"],
            action_id: action_id,
        })

        # if somehow things have gone wrong and this job is running more than
        # 24 hours after we expected it to, just do nothing
        if target_time + 1.day < Time.now
            return
        end

        if action["type"] == "expulsion_warning"

            # Add the time that auto-expulsion will occur. We assume there is only one auto-expulsion action, though that
            # is not enforced.
            expulsion_action = period["actions"].find { |hash| hash["type"] == "expulsion" }
            if expulsion_action
                auto_expulsion_will_occur_at = cohort.relative_time_from_end_of_period(expulsion_action["days_offset_from_end"], period).to_timestamp
            end

            period_index = cohort.index_for_period(period)
            next_exam_period = cohort.next_exam_period(period_index)
            cohort.with_users_for_action(period, action) do |user|
                Event.create_server_event!(SecureRandom.uuid, user.id, 'period_action:expulsion_warning', action_attributes.merge({
                    auto_expulsion_will_occur_at: auto_expulsion_will_occur_at,
                    course_entries: Cohort.get_course_entries_event_array(period, user),
                    before_enrollment_deadline: target_time < cohort.enrollment_deadline,
                    num_periods_until_next_exam: next_exam_period && (cohort.index_for_period(next_exam_period) - period_index),
                    next_exam_style: next_exam_period && next_exam_period["exam_style"],
                    next_exam: next_exam_period && Cohort.get_course_entries_event_array(next_exam_period, user)[0]
                })).log_to_external_systems
            end
        elsif action["type"] == "expulsion"
            cohort.with_users_for_action(period, action) do |user|
                EditorTracking.transaction('OnPeriodActionJob') do
                    CohortApplication.update_from_hash!({
                        id: user.accepted_application.id,
                        status: 'expelled'
                    }, {
                        user_expelled_due_to_period_expulsion_action: true,
                        is_admin: true
                    })

                    Event.create_server_event!(SecureRandom.uuid, user.id, 'period_action:expulsion', action_attributes.merge({
                        course_entries: Cohort.get_course_entries_event_array(period, user),
                        before_enrollment_deadline: target_time < cohort.enrollment_deadline
                    })).log_to_external_systems
                end
            end
        end
    end
end