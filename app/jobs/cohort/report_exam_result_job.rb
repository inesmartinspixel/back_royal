class Cohort::ReportExamResultJob < ApplicationJob

    queue_as :cohort__report_exam_result

    def perform(user_id, stream_locale_pack_id, payload = {})
        streams = Lesson::Stream.all_published.where(locale_pack_id: stream_locale_pack_id).includes(:entity_metadata).index_by(&:locale)
        user = User.find(user_id)
        english_stream = streams['en']
        localized_stream = streams[user.locale]

        score = Lesson::StreamProgress.where(user_id: user_id, locale_pack_id: stream_locale_pack_id).first.official_test_score

        Event.create_server_event!(SecureRandom.uuid, user.id, 'exam:report_score', {
            stream_locale_pack_id: stream_locale_pack_id,
            english_title: english_stream.title,
            title_in_users_locale: (localized_stream || english_stream).title,
            english_url: english_stream.entity_metadata.canonical_url,
            url_in_users_locale: (localized_stream || english_stream).entity_metadata.canonical_url,
            tweet_template: english_stream.entity_metadata.tweet_template,
            score: (100*score).round,
            passed: score >= 0.70,
            average_assessment_score_best: user.average_assessment_score_best
        }.merge(payload)).log_to_external_systems
    end
end