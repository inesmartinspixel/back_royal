class Cohort::UpdateCohortStatusChangesAtEnrollmentDeadlineJob < ApplicationJob

    queue_as :cohort__update_cohort_status_changes_at_enrollment_deadline

    def perform(cohort_id)
        cohort = Cohort.cached_published(cohort_id)
        cohort_status_changes = []

        # it probably would be safe to just look for accepted applications, but
        # theoretically the status could have changed, so look for anyone who was ever
        # accepted
        cohort_application_ids = CohortApplication
                                .joins(:versions)
                                .where(cohort_applications_versions: {status: 'accepted'}, cohort_id: cohort_id)
                                .distinct
                                .reorder(nil)
                                .pluck(:id)

        EditorTracking.transaction('UpdateCohortStatusChangesAtEnrollmentDeadlineJob') do
            cohort_application_ids.each do |id|
                UpdateCohortStatusChangesJob.perform_later_with_debounce(id)
            end
        end
    end
end
