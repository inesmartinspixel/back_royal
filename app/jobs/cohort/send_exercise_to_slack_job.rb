require 'tempfile'

class Cohort::SendExerciseToSlackJob < ApplicationJob
    queue_as :cohort__send_exercise_to_slack

    attr_accessor :cohort, :period_index

    def perform(cohort_id, period_index, exercise_id, slack_room_id)
        # Since this uses a slack token that is stored on the cohort itself we
        # added an environment variable to opt-in allowing an environment to actually run the
        # contents of this job. This let's us opt-in production while not accidentally sending messages
        # to the cohort slack channels from other environments. If we do really need to test this
        # job then just ensure that all slack_rooms are removed in the test environment, then set
        # up a new one with admin_token (probaby to same value as ENV['SLACK_TOKEN']) and channel
        # for a period that you want to test with. (Instead of deleting them all, you could just set all
        # of them to the same test token)
        return unless ENV['ENABLE_SLACK_COHORT_MESSAGES'] == 'true'

        self.period_index = period_index
        self.cohort = Cohort.cached_published(cohort_id)
        period = cohort.periods[period_index - 1]
        exercise = period["exercises"].find { |hash| hash["id"] == exercise_id }

        # if the message has been customized for this slack room, use
        # the customized message and document
        overrides = (exercise['slack_room_overrides'] && exercise['slack_room_overrides'][slack_room_id]) || {}
        message = overrides['message']&.present? ? overrides['message'] : exercise['message']
        document =  overrides['document']&.present? ? overrides['document'] : exercise['document']

        slack_room = self.cohort.slack_rooms.find_by_id(slack_room_id)

        # if somehow things have gone wrong and this job is running more than
        # 24 hours after we expected it to, just do nothing
        target_time = cohort.relative_time_from_end_of_period(exercise["hours_offset_from_end"], period, 'hours')
        if target_time + 24.hours < Time.now
            return
        end

        # Scope these here since we set them in the transaction but use them afterwards
        slack_result = nil
        temp_file = nil

        if document.present?
            s3_document = S3ExerciseDocument.find(document["id"])
            temp_file = Tempfile.new(s3_document.file_file_name)

            temp_file.write(read_paperclip_file(s3_document.file))
            temp_file.close
        end

        # For Slack to properly parse and notify a @mention we need to format it as <!mention>. There is an
        # API parameters called link_names that will do this behind the seems, but unfortunately the
        # files_upload method does not support it.
        mention_regex = /(?<=^|\s)@([^\s]+)/
        message_with_escaped_mentions = message.gsub(mention_regex) { |mention| "<!#{mention.tr("@", '')}>" }

        slack_result = log_to_slack_room(
            # The admin_token on the slack_room is a legacy Slack token. Slack deprecated the use of legacy Slack
            # tokens and has since prevented users from generating new ones as of 5/6/2020. As an alternative,
            # they recommend using a Slack app instead. As part of https://trello.com/c/bYRW5kZH, we migrated
            # away from legacy Slack tokens and created a custom Slack app to replace the functionality achieved
            # through the use of legacy Slack tokens. However, the strategy remains largely the same for sending
            # these cohort curriculum exercise messages to the slack rooms as the Slack app still requires an
            # access token. The bot_user_access_token is the access token for the Slack app we've created,
            # so if we see that the slack_room has one, we should use it instead of the legacy admin_token
            # as Slack may very well remove support for legacy Slack tokens entirely in the future.
            token: slack_room.bot_user_access_token || slack_room.admin_token,
            # According to this page in Slack's docs (https://api.slack.com/methods/chat.postMessage#arg_as_user),
            # if the token is a bot token, then we shouldn't be supplying the as_user argument when we call chat_postMessage.
            with_as_user_arg: !slack_room.bot_user_access_token,
            channel: exercise['channel'],
            document_title: document && document['title'],
            temp_file: temp_file,
            s3_document: s3_document,
            message_with_escaped_mentions: message_with_escaped_mentions
        )

        # Note: Anything after the slack message needs to be rescued and reported since we don't want
        #   an error to cause the job to rerun and send the message a second time.
        # Note #2: This passing around of the slack result is more complex than it needs be now
        #   that this job only applies to a single slack room, but not worth refactoring right now.
        add_pins(slack_result)

        begin
            retries ||= 0
            if temp_file.present?
                temp_file.unlink
            end
        rescue
            retries += 1
            retry if retries < 3
            Raven.capture_exception("Error deleting #{temp_file.path} after sending to Slack")
        end
    end

    private

    def log_to_slack_room(token:, with_as_user_arg:, channel:, document_title:, temp_file:, s3_document:, message_with_escaped_mentions:)
        # The token passed into the initializer will take precendece over the global token passed to configure
        # in config/initializers/slack.rb
        # See https://github.com/slack-ruby/slack-ruby-client#use-the-api-token
        client = Slack::Web::Client.new(token: token)

        if temp_file.present?

            response = client.files_upload(
                channels: channel,
                file: Faraday::UploadIO.new(temp_file.path, s3_document.file_content_type),
                title: document_title,
                filename: s3_document.file_file_name,
                initial_comment: message_with_escaped_mentions
            )
        else
            if with_as_user_arg
                response = client.chat_postMessage(channel: channel, text: message_with_escaped_mentions, as_user: true)
            else
                response = client.chat_postMessage(channel: channel, text: message_with_escaped_mentions)
            end
        end

        {
            client: client,
            response: response,
            document_title: document_title,
            channel: channel,
            temp_file: temp_file
        }
    end

    def add_pins(slack_result)
        client = slack_result[:client]
        response = slack_result[:response]
        temp_file = slack_result[:temp_file]
        begin
            retries ||= 0

            if temp_file.present?
                channel = response.file.channels.first
                thread_ts = response.file.shares['public'][channel][0].ts
                file_id = response.file.id

                args = {
                    channel: response.file.channels.first,
                }

                # Since we call `files.upload` with an `initial_comment`, files are typically
                # going to belong to a parent thread. If this is  the case, we cannot pin the
                # file by using its ID. We need to pin the entire thread, using the thread's timestamp.
                #
                # see https://api.slack.com/methods/files.upload
                # and https://api.slack.com/methods/pins.add
                if thread_ts.present?
                    args = args.merge({timestamp: thread_ts})
                else
                    args = args.merge({file: file_id})
                end

                client.pins_add(args)
            else
                client.pins_add({
                    channel: response.channel,
                    timestamp: response.ts
                })
            end
        rescue Exception => err
            retries += 1
            retry if retries < 3

            Raven.capture_exception(err, :extra => {
                title: slack_result[:document_title],
                channel: slack_result[:channel],
                cohort: cohort.name,
                period_index: period_index
            })
        end
    end

    # Moved to method for easier unit testing
    # See https://stackoverflow.com/a/23208454/1747491 and https://stackoverflow.com/a/45109488/1747491
    def read_paperclip_file(s3_document_file)
        Paperclip.io_adapters.for(s3_document_file).read.force_encoding("UTF-8")
    end
end