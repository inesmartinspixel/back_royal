class Cohort::AdmissionRoundEndsSoonJob < ApplicationJob
    include Cohort::AdmissionRoundBatchJobMixin

    queue_as :cohort__admission_round_ends_soon

    def self.batch_event_type
        'pre_application:admission_round_ends_soon'
    end

    def self.include_relevant_email_in_event_payload?
        true
    end
end