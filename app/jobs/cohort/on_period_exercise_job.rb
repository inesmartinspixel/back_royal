require 'tempfile'

class Cohort::OnPeriodExerciseJob < ApplicationJob
    queue_as :cohort__on_period_exercise

    def perform(cohort_id, period_index, exercise_id)

        # Since there are multiple steps to logging to each slack room, we
        # want each room to retry on it's own.  Otherwise, if the second room
        # fails then we could end up re-sending a message to the first room.
        EditorTracking.transaction('OnPeriodExerciseJob') do
            cohort = Cohort.cached_published(cohort_id)
            period = cohort.periods[period_index - 1]
            exercise = period["exercises"].find { |hash| hash["id"] == exercise_id }

            # Note: This event is not currently being used in customer.io and Alexie expressed that there
            # is not an intention to do so. So for now we are just commenting it out, and can bring it
            # back if needed.
            #
            # We'll roll these back if the creation of the individual jobs for each room below fails,
            # but note that these are, of course, being logged independently of whether or not those individual
            # jobs have finished successfully or not. We discussed this with Ori and decided that it is okay.
            # cohort.accepted_users.each do |user|
            #     Event.create_server_event!(
            #         SecureRandom.uuid,
            #         user.id,
            #         'period_exercise',
            #         cohort.event_attributes_for_period(period).merge({
            #             exercise: exercise
            #         })
            #     ).log_to_external_systems
            # end

            cohort.slack_rooms.each do |slack_room|
                Cohort::SendExerciseToSlackJob.perform_later(cohort_id, period_index, exercise_id, slack_room.id)
            end
        end
    end
end