class Cohort::UpdateFinalScoresJob < ApplicationJob

    queue_as :cohort__update_final_scores

    def perform(cohort_id)
        Cohort.find(cohort_id)
            .cohort_applications
            .where(status: 'accepted')
            .find_each(batch_size: 20) do |cohort_application|
                EditorTracking.transaction("UpdateFinalScoresJob") do
                    cohort_application.set_final_score

                    # ActiveRecord is smart enough to not actually save the record if
                    # nothing changed, but we also want to prevent callbacks from triggering.
                    cohort_application.save! if cohort_application.final_score_changed?
                end
            end
    end
end