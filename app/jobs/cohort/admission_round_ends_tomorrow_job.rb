class Cohort::AdmissionRoundEndsTomorrowJob < ApplicationJob
    include Cohort::AdmissionRoundBatchJobMixin

    queue_as :cohort__admission_round_ends_tomorrow

    def self.batch_event_type
        'pre_application:admission_round_ending_tomorrow'
    end

    def self.include_relevant_email_in_event_payload?
        true
    end
end