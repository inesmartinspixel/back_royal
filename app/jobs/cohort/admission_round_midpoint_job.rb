class Cohort::AdmissionRoundMidpointJob < ApplicationJob
    include Cohort::AdmissionRoundBatchJobMixin

    queue_as :cohort__admission_round_midpoint

    def self.batch_event_type
        'pre_application:send_mid_round_communication'
    end

    def self.include_relevant_email_in_event_payload?
        false
    end

    # The Cohort::AdmissionRoundBatchJobMixin supports an after_final_batch_success method
    # on the including class. If it sees that one is present, it will run it inside it's
    # transaction after the final batch job has determined that no more users need to be
    # batch processed.
    def after_final_batch_success
        log_invite_to_reapply_events
    end

    # There are only a couple dozen users where should_invite_to_reapply is true each
    # time this job gets run, so we don't bother splitting up this logic into batches.
    def log_invite_to_reapply_events
        # during the first round of each cycle, we invite people to
        # re-apply if they were pre_accepted in the past but never
        # registered.  We only do this during the first round because
        # we want people to have plenty of time to decide to re-apply
        # after receiving the email. The way the schedule is set up,
        # you will get the invite to re-apply two cohorts later. So if
        # you are pre-accepted to EMBA72, when the middle of the first
        # round comes around for EMBA73 you will not yet have hit the
        # registration deadline and so will not yet be rejected. Then
        # you get rejected and when the middle of the first round for
        # EMBA74 comes along, then you get the email.
        if self.promoted_mba_admission_round.index == 1
            User.where("cohort_applications.should_invite_to_reapply = true")
                .includes(:cohort_applications => :cohort) # => :cohort helps, but we're still not preloading the published_cohort, sadly
                .joins(:cohort_applications)
                .find_each do |user|

                application = user.cohort_applications.detect { |app| app.should_invite_to_reapply == true }

                # if the user has created a new application since the one that
                # was marked as should_invite_to_reapply, then unmark it
                if application == user.last_application
                    Event.create_server_event!(
                        SecureRandom.uuid,
                        user.id,
                        'cohort:invite_to_reapply',
                        AdmissionRound.promoted_rounds_event_attributes(user.timezone, self.promoted_admission_rounds).merge({
                            applied_to_cohort: application.published_cohort.event_attributes(user.timezone)
                        })
                    ).log_to_external_systems
                end
                application.update_attribute(:should_invite_to_reapply, false)
            end
        end
    end
end
