=begin
    OVERVIEW:

        Before this mixin was created, we were seeing some jobs with fairly long-running transactions that
        were also reading and writing tens of thousands of rows to and from the database. This put a lot
        of strain on the database and, in general, just isn't a good idea. To fix this, we decided to split
        those long-running jobs into multiple smaller "batch" jobs where each job would be resposible for
        a fraction of the work, resulting in shorter transactions and fewer reads/writes per job to the
        database. When a job runs that includes this mixin, it will find a batch of users, create a server
        event for each user, and then enqueue a new job that will be responsible for the next batch of users
        that need to be processed.

    HOW TO USE:

        1. Include this mixin in the job class (e.g. `include Cohort::AdmissionRoundBatchJobMixin`).
        2. Optionally, if you have logic that you'd like to run after the final batch job has determined
            that no more users need to be processed, you can define a #after_final_batch_success instance
            method in the including class and place this logic there. NOTE: The after_final_batch_success
            method will be called inside of the same transaction as the job's main logic.
=end
module Cohort::AdmissionRoundBatchJobMixin
    extend ActiveSupport::Concern

    BATCH_SIZE = 500

    module ClassMethods
        def batch_event_type
            raise 'batch_event_type must be implemented in including class as a class method'
        end

        def include_relevant_email_in_event_payload?
            raise 'include_relevant_email_in_event_payload? must be implemented in including class as a class method'
        end
    end

    # Finds users in a batch of size BATCH_SIZE from the who_receives_pre_application_communications User relation
    # and creates server events for each user and then ensures that a new job is enqueued if we see that more users
    # in the relation may need to be found and processed.
    # @param cohort_id - The cohort_id for the promoted MBA admission round (the schedule for this stuff is based
    #       on the promoted MBA cohort). If the cohort_id doesn't match the cohort_id for the promoted MBA admission
    #       round, the job exits early and no users are processed.
    # @param batch_start_user_id (optional) - An id that's used in conjunction with the batch_start_user_created_at_timestamp
    #       param to denote where the job should start querying for users in the who_receives_pre_application_communications
    #       relation. If not passed in, the job will start querying for users at the beginning of the relation, ordered by
    #       the user's created_at (asc) and the user's id (asc).
    #       NOTE: batch_start_user_id must be present if batch_start_user_created_at_timestamp is passed in and vice versa.
    # @param batch_start_user_created_at_timestamp (optional) - A timestamp - must be represented as an integer,
    #       otherwise ActiveJob can't serialize it - that's used in conjunction with the batch_start_user_id param
    #       to denote where the job should start querying for users in the who_receives_pre_application_communications
    #       relation. If not passed in, the job will start querying for users at the beginning of the relation, ordered by
    #       the user's created_at (asc) and the user's id (asc).
    #       NOTE: batch_start_user_created_at_timestamp must be present if batch_start_user_id is passed in and vice versa.
    # @param debug_mode (optional) - If true, logs debug statements to logger for easier debugging, but only in development.
    def perform(cohort_id, batch_start_user_id = nil, batch_start_user_created_at_timestamp = nil, debug_mode = false)
        # Automatically retries in the event of ActiveRecord::PreparedStatementCacheExpired
        EditorTracking.transaction('AdmissionRoundBatchJobMixin') do
            # Verify that the expected arguments have been passed in. If batch_start_user_id is present,
            # then we require batch_start_user_created_at_timestamp to also be passed in and vice versa.
            if batch_start_user_id.present? && batch_start_user_created_at_timestamp.nil? || batch_start_user_id.nil? && batch_start_user_created_at_timestamp.present?
                raise "Must supply both a batch_start_user_id and a batch_start_user_created_at_timestamp argument."
            end

            if self.promoted_mba_admission_round.cohort_id != cohort_id
                debug_log("The promoted_mba_admission_round.cohort_id ('#{self.promoted_mba_admission_round.cohort_id}') doesn't match the passed in cohort_id ('#{cohort_id}'). Exiting early.", debug_mode)
                return
            end

            # Finds users from the who_receives_pre_application_communications User relation, ordered by the user's created_at asc
            # and the user's id asc. We order by the user's created_at first so that each job finds users that were created after
            # the users that previous jobs processed, providing some assurance that any new users who enter the relation while the
            # jobs are running still get picked up by the last job. However, users can be created at the same time, so we also order
            # by the user's id, which should help cover this edge case. I suppose it's still possible for a couple users to slip
            # through, but there really isn't much we can do about it.
            users = User.who_receives_pre_application_communications.reorder(:created_at, :id).limit(self.class::BATCH_SIZE)
            if batch_start_user_id && batch_start_user_created_at_timestamp
                debug_log("Retrieving users created on or after #{batch_start_user_created_at_timestamp} with id > '#{batch_start_user_id}'", debug_mode)

                users = users.where(%Q~
                    cast(extract(epoch from users.created_at) as int) > '#{batch_start_user_created_at_timestamp}'
                        or (
                            cast(extract(epoch from users.created_at) as int) = #{batch_start_user_created_at_timestamp}
                                and users.id > '#{batch_start_user_id}'
                        )
                ~)
            end

            debug_log("Processing #{users.size} users...", debug_mode)
            users.each do |user|
                payload = AdmissionRound.promoted_rounds_event_attributes(user.timezone, self.promoted_admission_rounds)

                if self.class.include_relevant_email_in_event_payload?
                    user_signed_up_in_this_cycle = user.created_at > self.promoted_mba_admission_round.start_of_cycle
                    relevant_email_prefix = user_signed_up_in_this_cycle ? 'signed_up_this_cycle' : 'signed_up_prev_cycle'
                    relevant_email_suffix = self.promoted_mba_admission_round.last_round_in_cycle? ? 'end_of_cycle' : 'end_of_round'
                    relevant_email = [relevant_email_prefix, relevant_email_suffix].join('_')
                    payload.merge!({
                        # relevant_email is one of
                        # signed_up_this_cycle_end_of_round (users never get this email if they sign up in the second round of a cycle)
                        # signed_up_this_cycle_end_of_cycle
                        # signed_up_prev_cycle_end_of_round
                        # signed_up_prev_cycle_end_of_cycle
                        relevant_email: relevant_email
                    })
                end

                Event.create_server_event!(
                    SecureRandom.uuid,
                    user.id,
                    self.class.batch_event_type,
                    payload
                ).log_to_external_systems(nil, true, LogToCustomerIoJob::MASS_LOG_PRIORITY)
            end
            debug_log("Finished processing #{users.size} users!", debug_mode)

            # If we see that the number of users that were retrieved is less than the BATCH_SIZE,
            # then we can be pretty certain that there are no more users that should be picked up
            # by these jobs. Yes, there is an edge case where a user could potentially slip through
            # because they were created after the last job queried for users, but we're okay with it.
            if users.size < self.class::BATCH_SIZE
                debug_log("Number of retrieved users (#{users.size}) is less than BATCH_SIZE (#{self.class::BATCH_SIZE}). Calling after_final_batch_success and exiting early.", debug_mode)
                self.after_final_batch_success if self.respond_to?(:after_final_batch_success)
                return
            end

            last_user = users.last
            self.class.perform_later(cohort_id, last_user.id, last_user.created_at.to_timestamp, debug_mode)
        end
    end

    def promoted_admission_rounds
        @promoted_admission_rounds ||= AdmissionRound.promoted_admission_rounds
    end

    def promoted_mba_admission_round
        # the schedule for this stuff is based on the promoted mba cohort
        @promoted_mba_admission_round ||= self.promoted_admission_rounds.detect { |admission_round| admission_round.program_type == 'mba' }
    end

    # Logs a statement to the logger. For development purposes only.
    def debug_log(string, debug_mode)
        Delayed::Worker.logger.debug(string) if debug_mode && Rails.env.development?
    end
end
