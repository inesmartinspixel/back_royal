class Cohort::DecisionDayEnrollmentAgreementsJob < ApplicationJob

    queue_as :cohort__decision_day_enrollment_agreements

    def perform(cohort_id)

        EditorTracking.transaction('DecisionDayEnrollmentAgreementsJob') do

            cohort = Cohort.find_by_id(cohort_id)

            return unless cohort.requires_enrollment_agreement? && cohort.enrollment_agreement_template_id.present?

            cohort.cohort_applications.select {|application| ['accepted','pre_accepted'].include?(application.status)}.each do |application|
                application.queue_ensure_enrollment_agreement_job_if_necessary
            end
        end

    end

end