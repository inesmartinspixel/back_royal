=begin
    We frequently upload files to channels that we send automated messages to. While Slack doesn't
    require the bot user to be a member of the channels that it sends regular messages to, it does
    require the bot user to be a member of the channel if it wants to upload files to that channel.
    So we need to figure out from the cohort what channels we send automated messages to and then
    add the Slack app to those channels, so that uploading files works. We could just add the app
    to channels that the app uploads files to, but these instances are so few, it's probably safer
    to just add the app to all channels that receive automated messages from the bot user.
=end
class Cohort::SetupSlackChannelsJob < ApplicationJob

    class ForcedRetryError < RuntimeError; end

    use_identifier
    queue_as :cohort__setup_slack_channels
    retry_on ForcedRetryError, wait: :exponentially_longer

    def perform(slack_room_id)
        slack_room = CohortSlackRoom.find_by_id(slack_room_id)
        return if !slack_room || !slack_room.bot_user_access_token || !slack_room.cohort.published_version

        # convert to set for faster lookup later on
        channels_to_join = slack_room.cohort.published_version.slack_channels_that_receive_automated_messages.to_set
        return if channels_to_join.empty?

        # Create a client using the bot user access token returned from Slack so that
        # the app bot user can perform additional operations in the Slack workspace.
        client = Slack::Web::Client.new(token: slack_room.bot_user_access_token)

        # This API request isn't wrapped in any error handling because we want any error that gets
        # raised to be picked up by Sentry and so that the job will automatically try again later.
        conversations_list_response = client.conversations_list

        # Create any channels that are currently missing.
        existing_channels = conversations_list_response['channels'].select { |channel| channels_to_join.include?(channel['name']) }
        missing_channels = channels_to_join - existing_channels.pluck('name')
        created_channels, channels_we_failed_to_create = self.create_channels(client, missing_channels)

        # Add the Slack app to the available channels.
        available_channels = existing_channels + created_channels
        channels_we_failed_to_join = self.add_slack_app_to_channels(client, available_channels)

        # If we failed to create and/or join any channels, send alert(s) to Sentry and force the job to retry.
        if channels_we_failed_to_create.any? || channels_we_failed_to_join.any?
            error_message_parts = []

            if channels_we_failed_to_create.any?
                error_message_parts << 'failed to create channel(s)'
                Raven.capture_exception("#{self.class.name}: Slack failed to create the following channels",
                    extra: { failed_channels: channels_we_failed_to_create }
                )
            end

            if channels_we_failed_to_join.any?
                error_message_parts << 'failed to join channel(s)'
                Raven.capture_exception("#{self.class.name}: Slack failed to add the Slack app to the following channels",
                    extra: { failed_channels: channels_we_failed_to_join }
                )
            end

            error_message = error_message_parts.join(' and ')
            raise ForcedRetryError.new(error_message)
        end
    end

    # Creates each of the channels in the Slack workspace.
    def create_channels(client, channels)
        created_channels = []
        failed_channels = []

        channels.each do |channel|
            begin
                # It may seem more intuitive to use the `Slack::Web::Client#channels_create` method,
                # but it's been deprecated. Instead, Slack recommends using `#conversations_create`.
                # See https://api.slack.com/methods/channels.create.
                conversations_create_response = client.conversations_create(name: channel)
                created_channels << conversations_create_response['channel']
            rescue Slack::Web::Api::Error => error
                # Collect some info about what channel failed to be created and why,
                # and then continue creating the rest of the channels. This way we
                # create as many channels as possible rather than exiting early when
                # we first encounter an error.
                failed_channels << { name: channel, error: error }
            end
        end

        return [created_channels, failed_channels]
    end

    # Adds the Slack app to each of the channels.
    def add_slack_app_to_channels(client, channels)
        failed_channels = []

        channels.each do |channel|
            # It may seem more intuitive to use the `Slack::Web::Client#channels_join` method,
            # but it's been deprecated. Instead, Slack recommends using `#conversations_join`.
            # See https://api.slack.com/methods/channels.join.
            begin
                client.conversations_join(channel: channel['id'])
            rescue Slack::Web::Api::Error => error
                # Collect some info about what channel failed to be joined and why,
                # and then continue creating the rest of the channels. This way we
                # join as many channels as possible rather than exiting early when
                # we first encounter an error.
                failed_channels << { channel_id: channel['id'], channel_name: channel['name'], error: error }
            end
        end

        return failed_channels
    end
end
