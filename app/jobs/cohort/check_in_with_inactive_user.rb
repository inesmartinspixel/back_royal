class Cohort::CheckInWithInactiveUser < ApplicationJob

    queue_as :cohort__check_in_with_inactive_user

    def perform(user_id)
        user = User.find_by_id(user_id)

        # return early unless the user was found and the user should be checked in with
        unless user && user.should_be_checked_in_with?
            return
        end

        application = user.application_for_relevant_cohort
        Event.create_server_event!(
            SecureRandom.uuid,
            user.id,
            'cohort:check_in_with_inactive_user',
            {
                cohort_title: application.published_cohort.title,
                is_paid_cert: application.program_type_config.is_paid_cert?
            }
        ).log_to_external_systems
    end
end