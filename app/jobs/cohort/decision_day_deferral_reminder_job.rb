class Cohort::DecisionDayDeferralReminderJob < ApplicationJob

    queue_as :cohort__decision_day_deferral_reminder

    def perform(cohort_id)
        EditorTracking.transaction('DecisionDayDeferralReminderJob') do
            cohort = Cohort.find_by_id(cohort_id).published_version

            users = cohort.pre_accepted_users.select do |user|
                pre_accepted_application = user.pre_accepted_application
                penultimate_cohort_application = user.cohort_applications.second

                if penultimate_cohort_application.present?

                    # The EMBA has a pretty straightforward way of conducting deferrals: the user's cohort application
                    # status is marked as deferred and a new cohort application is created for their new cohort with
                    # a preset status of pre-accepted. No exception to this flow exists because otherwise the user's
                    # payment info won't be carried over to the new cohort application. Any EMBA user outside of this
                    # flow should not be considered "deferred into a later cohort".
                    if cohort.program_type == 'emba'
                        !pre_accepted_application.registered && penultimate_cohort_application.status == 'deferred'

                    # Unlike the EMBA, the MBA has a more complicated way of conducting deferrals. Occasionally, a user
                    # who has been expelled will request to be let back into the program. If we grant this request, the
                    # user's expelled cohort application is left as expelled and a new cohort application is created for
                    # their new cohort with a preset status of pre-accepted. Going forward, the admissions team will try
                    # to more closely mirror the EMBA deferral process: mark application as deferred, then create a new
                    # cohort application with a preset status of pre-accepted. Any MBA user outside either of these flows
                    # should not be considered "deferred into a later cohort".
                    elsif cohort.program_type == 'mba'
                        !pre_accepted_application.registered && ['expelled', 'deferred'].include?(penultimate_cohort_application.status)
                    end
                end
            end

            create_server_events(cohort, users)
        end
    end

    def create_server_events(cohort, users = [])
        users.each do |user|
            Event.create_server_event!(
                SecureRandom.uuid,
                user.id,
                'cohort:deferral_registration_reminder',
                {
                    program_type: cohort.program_type
                }
            ).log_to_external_systems
        end
    end
end