=begin

*****************
You can test this on a real, live cohort with the following task.  Nothing
will be saved

     rake slack_room_assignment:test cohort=EMBA25

The approach here is based on that described in the following paper:
    https://ieeexplore.ieee.org/document/5518761/?part=1

Applying that strategy to our situation would mean:

1. Define a state as some distribution of users between the expected number of groups
2. Define the value of a state as the sum of the following:
    1. A diversity score as described in the paper, based on gender, has_full_scholarship,
        and location of each user.
    1. Some penalty for having too few users from one city/country in a group
3. Run the tabu search as described in that paper to find a state with a high value

----------------------------------------------------
However, the strategy in that paper does not scale well past a couple dozen
users, because there are multiple things that scale exponentially:

1. The number of possible moves in each state
2. The number of pairs of users in each group that need to be considered
    in order to determine the value of each state.
3. Even worse, those two things are multiplied together, since we need
    to figure out the value of the result state for each possible move.


-----------------------------------------------------
So, we do a few optimizations to allow us to apply the strategy from the
paper to our situation.

1. We do not compare each user to each other user, but rather each type of
    user to each other type of user. This limits the number of possible
    moves we have to consider.

2. Instead of using euclidean distance to calculate a diversity score for
    each room, we look at each property value and penalize ourselves if it
    is not distributed evenly across rooms.  For example, having 31 women in
    room A and 31 in room B results in no penalty.  But having 20 in room A
    and 42 in room B does.

3. When considering possible moves, instead of generating the result state
    for each move and calculating its value, we calculate the difference in
    value that would result from making a move, without actually calculating
    the full state that would result from the move.  This makes things a lot faster.

4. We generate an initial state that already has people distributed pretty well
    by location.

5. For locations with small numbers of users, we fix them in a group and do not
    allow for moving them.  So, if there are only 4 users from Cleveland, and we
    are splitting into 2 slack rooms, we simply pick a room for those 4 people and
    then disallow the algorithm from moving them.  This means that there are
    fewer possible moves for us to consider.

    When we have two rooms, this means that we basically assign all of the small
    locations first, and then move people around from the big locations in order
    to balance the genders and full_scholarship values.  As long as we have enough
    people in the big groups to balance things, there is no real downside.

    When we have three+ rooms, there is another downside.  If, for example, there
    are 8 users from Cleveland and we are splitting into 3 rooms, we will put 4
    of those users into room A and 4 into room B.  We're not smart about it though,
    so it could be that the 4 men are put into room A and the 4 women in room B.  The
    whole room will still be balanced, but the Clevelanders will not be.

=end

class Cohort::SlackRoomAssignmentJob < ApplicationJob

    queue_as :cohort__slack_room_assignment

    # see comment near the instance method
    def self.assign_one_application(cohort_application)
        self.new.assign_one_application(cohort_application)
    end

    # We use this method when adding someone to a cohort that has
    # already started.  In that case, we can quickly use the search
    # algorithm to place them in the appropriate room, since all the other
    # users are already assigned.
    def assign_one_application(cohort_application)
        cohort_id = cohort_application.cohort_id
        cohort_application.cohort_slack_room_id = nil

        cohort, slack_room_ids, cohort_applications = get_cohort_and_applications(cohort_id)
        cohort_applications_list = cohort_applications.to_a

        # Add this cohort application to the list that will be included when running the
        # assignment job.  That way, the user is included even if they are not yet registered
        unless cohort_applications_list.include?(cohort_application)
            cohort_applications_list << cohort_application
        end

        raise_if_too_many_unassigned_applications(cohort_applications_list, cohort_application, slack_room_ids)

        # run the algorithm
        slack_room_assignments, locations_by_user_id = do_search(cohort, slack_room_ids, cohort_applications_list)

        # find the room that the provided user was assigned to, and
        # set it on the application
        room_entry = slack_room_assignments.detect do |entry|
            entry[:cohort_application_ids].include?(cohort_application.id)
        end
        cohort_application.cohort_slack_room_id = room_entry[:slack_room_id]
        cohort_application
    end

    # # This can be useful for debugging purposes
    # def run_assignment(cohort_id)
    #     cohort, slack_room_ids, cohort_applications = get_cohort_and_applications(cohort_id)
    #     cohort_applications_list = cohort_applications.to_a

    #     slack_room_assignments, locations_by_user_id = do_search(cohort, slack_room_ids, cohort_applications_list)

    #     [slack_room_assignments, locations_by_user_id]
    # end

    def perform(cohort_id)

        cohort, slack_room_ids, cohort_applications = get_cohort_and_applications(cohort_id)
        cohort_applications_list = cohort_applications.to_a

        if cohort.slack_room_assignment
            # This is not expected, but due to race conditions, it's possible that we would
            # create multiple jobs at the same time.  It's not a problem, so we just return.
            return
        end

        slack_room_assignments, locations_by_user_id = do_search(cohort, slack_room_ids, cohort_applications_list)
        return unless slack_room_assignments.present?

        EditorTracking.transaction('SlackRoomAssignmentJob') do

            # We always destroy the existing record when scheduling a job,
            # so there should not be a record and this point and this
            # should not lead to a duplicate key error.  But, there could
            # be a race and two of these jobs could be running at the same time.
            # In that case, we can just let the first one win.
            begin
                Cohort::SlackRoomAssignment.create!(
                    cohort_id: cohort['id'],
                    slack_room_assignments: slack_room_assignments,
                    location_assignments: locations_by_user_id
                )
            rescue ActiveRecord::RecordNotUnique
            end

        end

        # If a cohort application was updated while the job was running, or a slack room was added or
        # removed.  Then we should retry the job and create assignments again.
        cohort_applications_changed = cohort_applications_list.map(&:id).sort != cohort_applications.reload.pluck(:id).sort
        slack_room_ids_changed = slack_room_ids.sort != cohort.slack_rooms.reload.pluck('id').sort

        if cohort_applications_changed || slack_room_ids_changed
            cohort.association(:slack_room_assignment).reload
            slack_room_assignment = cohort.slack_room_assignment
            slack_room_assignment&.destroy

            raise "Need to re-run slack room assignment because something changed while it was running."
        end
    end

    def get_cohort_and_applications(cohort_id)
        # since we only reference slack_rooms, id, and supports_registration_deadline?
        # on the cohort, it doesn't really matter if we use the published one or
        # not, but might as well right?
        cohort = Cohort.find(cohort_id).published_version

        slack_room_ids = cohort.slack_rooms.pluck('id')
        if slack_room_ids.empty?
            Raven.capture_in_production(RuntimeError) do
                raise RuntimeError.new("No slack rooms")
            end
            return
        end

        cohort_applications = CohortApplication.where(cohort_id: cohort_id, status: ['accepted', 'pre_accepted'])
        if cohort.supports_registration_deadline?
            cohort_applications = cohort_applications.where(registered: true)
        end

        [cohort, slack_room_ids, cohort_applications]

    end

    def do_search(cohort, slack_room_ids, cohort_applications)
        cohort_applications_list = cohort_applications.to_a
        search = TabuSearch.new(slack_room_ids, cohort_applications_list).exec

        slack_room_assignments = []
        search.best_state[:rooms].each do |room|
            slack_room_assignments << {
                slack_room_id: room[:slack_room_id],
                cohort_application_ids: room[:cohort_applications].map(&:id)
            }
        end

        [slack_room_assignments, search.locations_by_user_id]
    end

    def raise_if_too_many_unassigned_applications(cohort_applications_list, cohort_application, slack_room_ids)
        # As mentioned in the comment above, we expect this to run when only
        # 1 or a small handful of users are not yet assigned to slack rooms.
        # A bit of experimentation showed that the algorithm will run in well under
        # 1 second if there are up to 20 unassigned users.  If there are more
        # than that, then we will just randomly assign the user to a room
        # and log to sentry.
        unassigned = cohort_applications_list.count { |app| app.cohort_slack_room_id.nil? }
        if unassigned > 20
            err = RuntimeError.new("Cannot assign a single application to a slack room when there are many unassigned applications")
            err.raven_options = {
                extra: {
                    unassigned_count: unassigned,
                    application: cohort_application.id,
                    cohort_id: cohort_application.cohort_id,
                    cohort: cohort_application.published_cohort.name
                }
            }
            raise err
        end
    end

    attr_accessor :cohort

    class TabuSearch < ::TabuSearch
        attr_accessor :locations_by_user_id, :recent_moves, :movable_vectors,
                        :slack_room_ids, :cohort_applications, :locations
        MIN_LOCATION_SIZE = 3

        def initialize(slack_room_ids, cohort_applications)
            super() # we need the parentheses here so that nothing is passed to super
            ActiveRecord::Associations::Preloader.new.preload(cohort_applications, {
                :user => {},
                :cohort => {:published_versions => {}}
            })
            self.cohort_applications = cohort_applications
            map_applications_to_locations
            self.recent_moves = []
            self.movable_vectors = Set.new
            self.slack_room_ids = slack_room_ids
        end

        ########## methods required by TabuSearch #####################

        def max_iterations
            100
        end

        def minimum_substantive_value_change
            0.0001
        end

        def generate_initial_state
            rooms = {}
            slack_room_ids.each do |slack_room_id|
                rooms[slack_room_id] = {
                    slack_room_id: slack_room_id,
                    cohort_applications: []
                }
            end

            # Accepted cohort applications should not be moved from the slack room they
            # are already assigned to.  So, we place them in the correct room here and
            # then filter them out below in `sample_movable_application`
            un_assigned_applications = []
            cohort_applications.each do |cohort_application|
                if cohort_application.status == 'accepted' && cohort_application.cohort_slack_room_id
                    rooms[cohort_application.cohort_slack_room_id][:cohort_applications] << cohort_application
                else
                    un_assigned_applications << cohort_application
                end
            end
            # Assign applications to rooms based on location, starting from the locations
            # with the smallest number of applications.  In slack rooms wihere the number
            # of people from a location is <= MIN_LOCATION_SIZE, those people cannot be
            # moved out of the room.  We enforce this by leaving them out of `movable_vectors`
            applications_by_location = un_assigned_applications.group_by do |cohort_application|
                locations_by_user_id[cohort_application.user_id]
            end

            lists = rooms.map { |slack_room_id, room| room[:cohort_applications]}
            applications_by_location.sort_by { |pair|
                cohort_applications = pair[1]
                cohort_applications.size
            }.each do |pair|
                cohort_applications = pair[1]

                # order the list for each room by how many applications are already
                # included in that list
                lists_by_size = lists.sort_by(&:size)

                # We should use as many rooms as possible so long as we can split up
                # the people we have without putting fewer than MIN_LOCATION_SIZE people in
                # a room.
                movable = cohort_applications.size > MIN_LOCATION_SIZE*lists.size

                # If we do not have enough people to split up between all of the
                # different lists, then put an equal number in each list.  These applications
                # cannot be moved later on.  We prevent them from being moved for
                # performance reasons.  This optimization is not totally necessary anymore,
                # as we've sped up other parts of the actual algorithm.  But it makes things
                # faster and it's tested and all, so why not?
                if !movable
                    count_of_lists_to_use = (cohort_applications.size / MIN_LOCATION_SIZE).floor
                    count_of_lists_to_use = 1 if count_of_lists_to_use == 0
                    lists_to_use = lists_by_size.slice(0, count_of_lists_to_use)

                    # FIXME: if there were some accepted applications already assigned to this location,
                    # we would want to pick that room.  But not sure it's worth the extra complexity right now
                    count_in_each_room = (cohort_applications.size.to_f / lists_to_use.size).floor
                    apps = cohort_applications.clone
                    # put count_in_each_room people in each room, and then whoever is
                    # leftover in the last room
                    lists_to_use.each_with_index do |list, i|
                        last_one = (i == lists_to_use.size - 1)
                        if last_one
                            list.push(*apps)
                        else
                            list.push(*(apps.slice!(0, count_in_each_room)))
                        end
                    end

                # If there are plenty of people from this location, then spread them out
                # amongst the lists and make them movable. The algorithm will move these
                # people around in order to balance the other features we care about.
                else
                    self.movable_vectors += vectorize(cohort_applications)
                    cohort_applications.each do |cohort_application|
                        smallest_list = lists.sort_by(&:size).first
                        smallest_list << cohort_application
                    end
                end
            end


            rooms.each do |slack_room_id, room|
                room[:cohort_applications_by_vector] = room_applications_by_vector(room[:cohort_applications])
            end

            {
                rooms: rooms.values
            }
        end

        def possible_moves(state)
            moves = []
            rooms = state[:rooms]

            # For each pair of rooms, we can swap any vector that exists in room 1
            # for any vector that exists in room 2, provided that there is a movable
            # application for each vector.
            rooms.each_with_index do |room1, room_index_1|
                apps1 = self.movable_vectors.map { |vector|
                    sample_movable_application(room1, vector)
                }.compact

                ((room_index_1+1)..(rooms.size-1)).each do |room_index_2|
                    room2 = rooms[room_index_2]
                    apps2 = self.movable_vectors.map { |vector|
                        sample_movable_application(room2, vector)
                    }.compact

                    apps1.each do |app1|
                        apps2.each do |app2|

                            next if vectorize(app1) == vectorize(app2)

                            # make one move which is a swap between rooms
                            swap = {
                                changes: [
                                    {
                                        current_room_index: room_index_1,
                                        new_room_index: room_index_2,
                                        cohort_application: app1
                                    },
                                    {
                                        current_room_index: room_index_2,
                                        new_room_index: room_index_1,
                                        cohort_application: app2
                                    }
                                ]
                            }

                            moves << swap

                            # It seems like we should be able to consider moves that change
                            # the sizes of the rooms, but it's hard to write a value function
                            # that can compare the values of different-sized rooms.

                            # # make two moves, each of which just moves one of
                            # # the users to the other room, without swapping
                            # moves << {
                            #     changes: [swap[:changes][0]]
                            # }
                            # moves << {
                            #     changes: [swap[:changes][1]]
                            # }
                        end
                    end
                end
            end
            moves
        end

        def result_of_move(state, move)

            # clone the current state
            rooms = state[:rooms]
            result_rooms = rooms.map do |room|
                cloned_applications = time('clone cohort_application') do
                    room[:cohort_applications].clone
                end
                {
                    cohort_applications: cloned_applications,
                    slack_room_id: room[:slack_room_id]
                }
            end

            # move the cohort applications as indicated in the provided move
            move[:changes].each do |change|
                current_room = result_rooms[change[:current_room_index]]
                new_room = result_rooms[change[:new_room_index]]
                cohort_application = change[:cohort_application]
                current_room[:cohort_applications].delete(cohort_application)
                new_room[:cohort_applications] << cohort_application
            end

            # room the applications by vector
            result_rooms.each do |room|
                room[:cohort_applications_by_vector] = time('room_applications_by_vector') do
                    room_applications_by_vector(room[:cohort_applications])
                end
            end

            new_state = {
                rooms: result_rooms
            }

            new_state
        end

        def move_is_tabu?(state, move)
            # We will not move any application out of a room if it was
            # recently moved in.  This follows the recommendation in the paper
            # mentioned at the top of this file.
            self.recent_moves.detect do |recent_move|
                recent_move[:move][:changes].detect do |recent_change|
                    move[:changes].detect do |this_change|
                        recent_change[:new_room_index] == this_change[:current_room_index] &&
                        vectorize(recent_change[:cohort_application]) == vectorize(this_change[:cohort_application])
                    end
                end
            end.present?
        end

        def value_after_move(move, current_state, current_state_value)
            ensure_counts_on_state(current_state)

            new_counts = {}
            pairs = Set.new
            rooms = current_state[:rooms]

            vectors = vectorize(move[:changes][0][:cohort_application]), vectorize(move[:changes][1][:cohort_application])
            # I tried using HashDiff here, but it just added compelxity with the location being an array
            diffs = [:sex, :full_scholarship, :location].map do |key|
                val0, val1 = [vectors[0][key], vectors[1][key]]
                if val0 != val1
                    [key, val0, val1]
                end
            end.compact

            diffs.each do |diff|
                key, old_val, new_val = diff
                key = key.to_sym

                room1_id = rooms[move[:changes][0][:current_room_index]][:slack_room_id]
                room2_id = rooms[move[:changes][1][:current_room_index]][:slack_room_id]

                # Initialize the counts in new_counts to whatever the current values are
                # in current_state[:counts]
                rooms.each do |room|
                    slack_room_id = room[:slack_room_id]
                    new_counts[slack_room_id] ||= {}
                    new_counts[slack_room_id][key] ||= {}
                    new_counts[slack_room_id][key][old_val] = current_state[:counts][slack_room_id][key][old_val]
                    new_counts[slack_room_id][key][new_val] = current_state[:counts][slack_room_id][key][new_val]
                    pairs << [key, old_val]
                    pairs << [key, new_val]
                end

                # decrement and increment as necessary to account for the swapping
                # of users
                new_counts[room1_id][key][old_val] -= 1
                new_counts[room1_id][key][new_val] += 1
                new_counts[room2_id][key][old_val] += 1
                new_counts[room2_id][key][new_val] -= 1
            end

            diff = pairs.map do |pair|
                key, val = pair
                old_score = state_score_for_val(rooms, current_state[:counts], key, val)
                new_score = state_score_for_val(rooms, new_counts, key, val)
                new_score - old_score
            end.sum

            value = current_state_value + diff

            value
        end

        # this method takes a particular key and value (like
        # `sex:0`, or `location:['France', 'Paris]``) and determines
        # how many points should be removed from the score because
        # of the counts of that value in the various rooms.
        def state_score_for_val(rooms, counts, key, val)
            score = 0

            # we lose points if we have not spread people with this
            # value out evenly amongst all the rooms
            rooms.each_with_index do |room1, i|
                rooms.slice(i,rooms.size).each do |room2|
                    room1_ct = counts[room1[:slack_room_id]][key][val]
                    room2_ct = counts[room2[:slack_room_id]][key][val]
                    score -= (room1_ct - room2_ct).abs
                end
            end

            # We lose points if we have fewer than the
            # MIN_LOCATION_SIZE students from a particular
            # location in a room. In that case, we would rather
            # put all of the students from the location into
            # one room and have none in the other rooms.  The
            # magnitude of the effect on the score is more here
            # than above, because this should override trying to
            # get more variety.
            if key == :location
                rooms.each do |room|
                    ct = counts[room[:slack_room_id]][key][val]
                    if ct > 0 && ct < MIN_LOCATION_SIZE
                        # If we have 3 students in one room and
                        # 2 in another, we want to move to having 5
                        # in one room and 0 in the other. But,
                        # in order for the algorithm to do that, 4/1
                        # needs to be better than 3/2 and 5/0 needs to
                        # be better than 4/1
                        # Here are the scores that result from the below:
                        # 3/2 -> -140
                        # 4/1 -> -120
                        # 5/0 -> 0
                        penalty = 100 + 20*ct
                        score -= penalty
                    end
                end
            end
            score
        end

        def ensure_counts_on_state(state)
            if !state[:counts]
                state[:counts] = {}
                rooms = state[:rooms]

                vector_lists = rooms.map { |r| vectorize(r[:cohort_applications]) }

                score = 0
                rooms.each_with_index do |room, i|
                    slack_room_id = room[:slack_room_id]
                    state[:counts][slack_room_id] ||= {}
                    {
                        :sex => [0, 0.5, 1],
                        :full_scholarship => [0, 1],
                        :location => locations
                    }.each do |key, vals|
                        state[:counts][slack_room_id][key] ||= {}
                        vals.each do |val|
                            state[:counts][slack_room_id][key][val] = vector_lists[i].count { |v| v[key] == val }
                        end
                    end
                end
            end
        end

        def value_of_state(state, log:false)

            ensure_counts_on_state(state)

            score = 0
            score_components = []
            {
                :sex => [0, 0.5, 1],
                :full_scholarship => [0, 1],
                :location => locations
            }.each do |key, vals|
                vals.each do |val|
                    marginal_score = state_score_for_val(state[:rooms], state[:counts], key, val)
                    score += marginal_score
                    if marginal_score != 0
                        score_components << {
                            marginal_score: marginal_score,
                            key: key,
                            val: val
                        }
                    end
                end
            end

            if log
                score_components.sort_by { |entry| entry[:marginal_score].abs }.reverse.each do |entry|
                    puts "#{entry[:marginal_score]} #{entry[:key]}: #{entry[:val]}"
                end
            end

            score
        end

        def after_move(state, move_details)
            self.recent_moves << move_details
            self.recent_moves = self.recent_moves.last(5)
        end

        ########## helper methods that are used by the methods required by TabuSearch #####################

        def sample_movable_application(room, vector)
            # If an application is accepted and has already been assigned a room, it cannot be
            # moved. (In practice every accepted application will have been assigned a room already,
            # so the check on slack_room_id is unnecessary. But,
            # when I was profiling the performance, it was useful to remove the slack room from accepted
            # users and have them be re-assigned. Also, conceptually, if a user does not yet have a
            # slack room, even if they are already accepted, they should be included in the auto-assignment.)
            room[:cohort_applications_by_vector][vector]&.reject { |ca| ca.status == 'accepted' && ca.cohort_slack_room_id.present? }&.sample
        end

        def map_applications_to_locations
            self.locations_by_user_id = {}

            # users_in_the_boondocks_by_country = Hash.new {|hash, country| hash[country] = [] }
            countries = {}

            # Generate clusters from the provided applications.  Each cluster has a center
            # (which is a latitude and longitude) and will include some of the applications.

            # 6 and 12 were determined to be the correct values to pass into get_geo_cluster_json
            # by zooming in and out on the student network until
            # Philadelphia and New York were distinct cities (so were DC and Baltimore)
            cluster_json = CareerProfile
                .where(user_id: cohort_applications.map(&:user_id))
                .get_geo_cluster_json(6, 12)
            clusters = ActiveSupport::JSON.decode(cluster_json)['features'] || []

            clusters.each do |cluster|

                # for each cluster, find all the profiles in it
                geometry = cluster['properties']['cluster_hull']
                profiles = CareerProfile.where(user_id: cohort_applications.map(&:user_id))
                    .select(:user_id, :place_details, :location)

                # profiles with no place details will show up with a geometry of nil
                if geometry.nil?
                    profiles = profiles.where(location: nil)
                    city = country = nil

                # for all actual clusters, determine a city name and a country name
                # for the cluster, and grab all of the profiles for it
                else
                    profiles = profiles.where(Location.within_geometry_sql_string('location', geometry))

                    # find the city that shows up most often in the cluster
                    most_common_city_and_profiles = profiles.group_by { |profile|
                        profile.city_name_and_state_abbr
                    }.sort_by { |pair| pair[1].size }.last
                    city = most_common_city_and_profiles[0]
                    place_details = most_common_city_and_profiles[1][0]['place_details']

                    # it is possible for the same country to have two different
                    # names in different place details.  For example, CH can be
                    # "Switzerland" or "Švica"
                    country_code = Location.country_code(place_details)
                    country_name = Location.country_name(place_details)
                end

                countries[country_code] ||= {
                    names: Hash.new { |hash, _country_name| hash[_country_name] = 0 },
                    clusters: []
                }

                # some countries will have multiple names (because place_details
                # is localized, so we might have Switzerland and Švica). Keep track
                # of how many profiles are associated with each name so we can
                # use the name with the most profiles.
                countries[country_code][:names][country_name] += profiles.size
                countries[country_code][:clusters] << {
                    city: city,
                    profiles: profiles
                }

            end

            # For each country, we want to define a location for any city
            # that has at least MIN_LOCATION_SIZE people in it.  For anyone outside
            # of those cities, we want to group them into a location for the
            # whole country.
            countries.each do |country_code, entry|
                # get the country with the most profiles associated with it (see comment above)
                country_name = entry[:names].sort_by { |pair| pair[1] }.last[0]
                has_at_least_one_city = false
                small_clusters, large_clusters = entry[:clusters].partition { |cluster| cluster[:profiles].size < MIN_LOCATION_SIZE }
                large_clusters = large_clusters.sort_by { |cluster| cluster[:profiles].size }

                # as long as there are more than MIN_LOCATION_SIZE people left in the country, check
                # to see if the next largest city has at least MIN_LOCATION_SIZE people in it.  For any city
                # that has at least MIN_LOCATION_SIZE people in it, set the locations for each
                # person in `locations_by_user_id`
                while largest_cluster = large_clusters.pop
                    largest_cluster[:profiles].each do |profile|
                        self.locations_by_user_id[profile['user_id']] = [country_name, largest_cluster[:city]]
                    end
                    entry[:clusters].delete(largest_cluster)
                    has_at_least_one_city = true
                end

                # Anybody who is left is in a city with fewer than MIN_LOCATION_SIZE people in it.
                # If there is only one person left, but them in the 'other countries' group.  If there
                # are at least 2 people, make a group for the country.  Then add each person to
                # `locations_by_user_id`
                if small_clusters.any?
                    profiles = entry[:clusters].map { |cluster| cluster[:profiles] }.flatten
                    if profiles.size == 1
                        cluster_name = ['Other Countries', '']
                    elsif has_at_least_one_city
                        cluster_name = [country_name, '(other cities)']
                    else
                        cluster_name = [country_name, '(all cities)']
                    end
                    profiles.each do |profile|
                        self.locations_by_user_id[profile['user_id']] = cluster_name
                    end
                end
            end

            self.locations = locations_by_user_id.values.uniq
        end

        def room_applications_by_vector(cohort_applications)
            cohort_applications.group_by do |application|
                vectorize(application)
            end
        end

        def vectorize(cohort_applications)
            if cohort_applications.is_a?(Array) || cohort_applications.is_a?(Set)
                return_type = :array
            else
                cohort_applications = [cohort_applications]
                return_type = :instance
            end

            vectors = cohort_applications.map do |cohort_application|
                vectorize_application(cohort_application)
            end

            return_type == :array ? vectors : vectors.first
        end

        def vectorize_application(cohort_application)
            @vectorized_applications ||= {}
            if @vectorized_applications[cohort_application].nil?
                full_scholarship = cohort_application.has_full_scholarship? ? 1 : 0
                vector = {
                    sex: sex_to_i(cohort_application),
                    full_scholarship: full_scholarship,
                    location: locations_by_user_id[cohort_application.user_id]
                }

                @vectorized_applications[cohort_application] = vector
            end
            @vectorized_applications[cohort_application]
        end

        def sex_to_i(app)
            if app.user.sex == 'male'
                1
            elsif app.user.sex == 'female'
                0
            else
                0.5
            end
        end

        ########## methods used for debugging #####################

        def stats(state = nil)
            state ||= best_state
            stats = {}
            [:sex, :full_scholarship].each do |key|
                stats[key] = []
                best_state[:rooms].each_with_index do |room, i|
                    entry = ActiveSupport::OrderedHash.new
                    stats[key] << entry
                    vectors = vectorize(room[:cohort_applications])
                    vectors.group_by { |v| v[key] }.sort_by { |pair| pair[0] }.each do |pair|
                        val, vectors_with_val = pair
                        entry[val] = vectors_with_val.size
                    end
                end
            end

            stats[:locations] = []
            locations_by_counts = self.cohort_applications.group_by { |ca|
                self.locations_by_user_id[ca.user_id]
            }.sort_by { |pair|
                pair[1].size
            }.reverse.map(&:first)

            locations_by_counts.map { |location|
                counts = self.best_state[:rooms].map { |room|
                    room[:cohort_applications].select { |ca|
                        self.locations_by_user_id[ca.user_id] == location
                    }.size
                }
                stats[:locations] << [location, counts]
            }

            stats
        end

    end

end