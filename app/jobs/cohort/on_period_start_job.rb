class Cohort::OnPeriodStartJob < ApplicationJob

    queue_as :cohort__on_period_start
    attr_accessor :cohort, :period, :period_index, :period_start, :period_end

    def perform(cohort_id, period_index)
        # Automatically retries in the event of ActiveRecord::PreparedStatementCacheExpired
        EditorTracking.transaction('OnPeriodStartJob') do
            self.cohort = Cohort.cached_published(cohort_id)

            # period_index is defined such that 1 is the first period.  We treat period 0
            # as all the time before the cohort's start date for data analysis purposes. 0
            # should never be passed in here as the period_index
            self.period_index = period_index
            self.period_start = cohort.start_time_for_period(period_index)
            self.period_end = cohort.end_time_for_period(period_index)

            # if somehow things have gone wrong and this job is running more than
            # 24 hours after we expected it to, just do nothing
            if period_start + 1.day < Time.now
                return
            end

            log_weekly_update_events
        end
    end

    def log_weekly_update_events

        # see note above about 1-indexing
        period = self.cohort.periods[period_index-1]

        # Calculate relevant_email field used in customer.io
        if period_index === 1
            relevant_email = "first_period_#{cohort.program_type}"
        else
            relevant_email = "#{period['style']}_period_#{cohort.program_type}"
        end

        cohort.accepted_users.each do |user|
            # skip users with no accepted application
            # see https://trello.com/c/yF4v2OPw
            next unless user.accepted_application

            payload = user.accepted_application.period_start_info(period, relevant_email)
            if (exercises = period["exercises"]).present?
                payload = payload.merge({
                    exercises: exercises
                })
            end

            Event.create_server_event!(
                SecureRandom.uuid,
                user.id,
                'cohort:period_started',
                payload
            ).log_to_external_systems
        end
    end

    def get_relevant_email_for_period(period_index)
        period = self.periods[period_index]

        if period_index === 1
            relevant_email = "first_period_#{self.program_type}"
        else
            relevant_email = "#{period['style']}_period_#{self.program_type}"
        end
        return relevant_email
    end
end