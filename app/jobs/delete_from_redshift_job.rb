class DeleteFromRedshiftJob < ApplicationJob

    queue_as :delete_from_redshift

    def perform(user_id)

        RedshiftEvent.where(user_id: user_id).delete_all

    end

end
