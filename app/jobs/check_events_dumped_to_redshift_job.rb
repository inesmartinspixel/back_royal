# This job just double-checks once each day that all of yesterday's events were copied from
# postgres to redshift
class CheckEventsDumpedToRedshiftJob < ApplicationJob

    queue_as :check_events_dumped_to_redshift
    include ScheduledJob
    self.run_at_hour 6 # 6:00 AM Eastern Time

    def perform

        yesterday = Time.now - 1.day
        start = yesterday.beginning_of_day
        finish = yesterday.end_of_day

        # Ideally, we would just filter by created_at here, but to speed things up we're gonna limit to a single
        # event type and add an estimated_time filter as well
        conditions = ["event_type='page_load:load' and created_at between ? and ? and estimated_time between ? and ?", start, finish, start, finish]
        postgres_count = Event.where(*conditions).count
        redshift_count = RedshiftEvent.where(*conditions).count

        if postgres_count != redshift_count
            Raven.capture_exception("Not all of yesterday's events found in redshift", {
                extra: {
                    postgres_count: postgres_count,
                    redshift_count: redshift_count
                }
            })
        end

    end
end