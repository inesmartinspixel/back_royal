=begin
    OVERVIEW:

        This module came into being to address an issue with our SyncFromAirtableJobs. Originally, these jobs
        would retrieve all of the records from Airtable that it needed to process, load up the ActiveRecord
        objects, and then loop through each record and update it, resulting in thousands of Airrecord objects
        and ActiveRecord objects and a thousands more reads/writes from/to the database. This implementation
        consumed more memory than what was deemed healthy for our EC2 instances and put a lot of strain on the
        database. To fix this issue, we decided to refactor these jobs to process records in smaller batches.

        This module contains the logic needed to get batching to work when syncing from Airtable to our database.
        Ideally, the batches would be ordered by the cohort application id and we would fetch batches based on a
        fixed size limit - similar to how Cohort::AdmissionRoundBatchJobMixin handles its batches - thus ensuring
        no records are processed more than once and no records are skipped. However, Airtable's API (see
        https://support.airtable.com/hc/en-us/articles/203255215-Formula-field-reference) doesn't seem to have a
        good way of fetching records with an id that alphabetically comes after a specified id because Airtable's
        ordering of records follows natural sorting order rather than alphabetical order (see
        https://support.airtable.com/hc/en-us/articles/360016471593-Guide-to-sorting-and-record-order#rules).
        Specifying an offset and a batch size is out of the question as well since records in Airtable will
        be removed from the view as they are synced back to Airtable from the SyncWithAirtableJobs that
        get enqueued due to the updated call, resulting in an inaccurate offset value.

        The current implementation employs a bucketing strategy. Each batch job is responsible for processing all
        of the Airtable records that belong to a specific "bucket". Each bucket is represented by a character that
        can be used as the first character in a valid uuid. Each SyncFromAirtableBatchJob will then fetch all of
        the Airtable records that have an application id that starts with the bucket character that its responsible
        for and since uuids are random, there should be a fairly even distribution of records that each batch job
        is responsible for fetching and processing.
=end
module SyncFromAirtableBatchHelper

    BUCKETS = ((0..9).to_a.map(&:to_s) + ('a'..'f').to_a)

    module MasterJobHelper

        # Depending on the use case, a "master" job may be used to enqueue a series of batch jobs.
        # This method enqueues all of the batch jobs required by the batching strategy outlined above.
        # NOTE: The batch_job_klass#perform method signature should require a `bucket:` keyword arg.
        def enqueue_batch_jobs(batch_job_klass, *var_args, **keyword_args)
            BUCKETS.each do |bucket|
                batch_job_klass.perform_later(*var_args, keyword_args.merge(bucket: bucket))
            end
        end
    end

    module BatchJobHelper

        # Each batch job is responsible for fetching and processing the retrieved records for the bucket
        # that it's responsible for (see comment above for more info on this batching strategy). How the
        # records are processed is left to the passed in block.
        def fetch_and_process_records(view, fields, bucket, base_filter, &block)
            application_id_column = Airtable::Application.column_map["id"]

            bucket_filter = "LEFT({#{application_id_column}}, 1) = '#{bucket}'"
            filter = base_filter ? "AND(#{base_filter}, #{bucket_filter})" : bucket_filter

            airtable_applications = Airtable::Application.all(
                view: view,
                fields: fields,
                filter: filter
            )

            application_ids = airtable_applications.map { |airtable_application| airtable_application[application_id_column] }
            database_applications_by_id = CohortApplication.where(id: application_ids).index_by(&:id)

            airtable_applications.each do |airtable_application|
                yield(airtable_application, database_applications_by_id)
            end
        end
    end
end
