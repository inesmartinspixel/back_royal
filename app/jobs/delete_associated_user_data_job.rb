class DeleteAssociatedUserDataJob < ApplicationJob

  queue_as :delete_associated_user_data

  attr_accessor :user_id, :career_profile_id

  def perform(user_id = nil, career_profile_id = nil)

    raise "Cannot delete user records without a user_id!" unless !user_id.nil?

    self.user_id = user_id
    self.career_profile_id = career_profile_id

    ###########################
    # Airtable data aggregation
    ###########################
    # Before deleting the associated records, we need to get some things for our deletion jobs, specifically
    # airtable stuff from cohort_applications_versions. Since these jobs will be enqueued after the associated
    # record in the DB has been destroyed, we need to make sure we have the information we need for the jobs to
    # work properly.
    #
    # There are four ways DeleteFromAirtableJob looks for applications in airtable:
    #  1. (Live base) Airtable::Application.find_by_id_with_fallback attempts to find an application using
    #     airtable_record_id and falls back to the id of the application if that does not work.
    #  2. (Archived base) We create an ad hoc API based on the application's airtable_base_key value and
    #     use a filter call that uses the id of the application.
    #  3. (Staging base) same as archived
    #  4. (Local Development base) same as archived
    #
    # We need to be able to guarantee zero Airtable retention when deleting a user, so we'll need to send all
    # of those values to DeleteFromAirtableJob.
    cohort_applications_versions_attrs = []
    CohortApplication::Version.where("user_id = '#{user_id}' AND airtable_base_key IS NOT NULL").each do |_version|

        version_attrs = _version.attributes.slice(
            'id',
            'airtable_record_id',
            'airtable_base_key'
        )
        cohort_applications_versions_attrs.concat([version_attrs])

        if ENV['AIRTABLE_BASE_KEY_STAGING'].present?
            version_attrs_staging = version_attrs.merge({
                'airtable_base_key' => 'AIRTABLE_BASE_KEY_STAGING'
            })
            cohort_applications_versions_attrs.concat([version_attrs_staging])
        end

        if ENV['AIRTABLE_BASE_KEY_LOCAL'].present?
            version_attrs_local = version_attrs.merge({
                'airtable_base_key' => 'AIRTABLE_BASE_KEY_LOCAL'
            })
            cohort_applications_versions_attrs.concat([version_attrs_local])
        end

    end
    cohort_applications_versions_attrs = cohort_applications_versions_attrs.uniq

    ###########################
    # Third Party jobs
    ###########################
    DeleteFromAirtableJob.perform_later(cohort_applications_versions_attrs)

    # Account for any Kinesis/Firehose delay. We have seen events get put in the
    # Firehose right before a user is deleted, and make it to Redshift _after_ this
    # job has run. In which case, we will have orphaned Redshift events.
    #
    # See also: https://trello.com/c/sTKSSbnV
    DeleteFromRedshiftJob.set(wait_until: Time.now + 6.hours).perform_later(user_id)
    DeleteFromCustomerIoJob.perform_later(user_id)
    DeleteFromSegmentJob.perform_later(user_id)

    ###########################
    # back_royal
    ###########################
    DeleteFromBackRoyalJob.perform_later(user_id, career_profile_id)

  end

end
