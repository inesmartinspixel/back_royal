class LogToSlack < ApplicationJob
    queue_as :log_to_slack

    def perform(channel, text, attachments = nil)
        Slack.send(channel, text, attachments)
    end
end
