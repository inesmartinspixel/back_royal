class EnsureStripeCustomerJob < ApplicationJob

    queue_as :ensure_stripe_customer

    def perform(user_id)
        user = User.find(user_id)
        user&.ensure_stripe_customer
    end

end