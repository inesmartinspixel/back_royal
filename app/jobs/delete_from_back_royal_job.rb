class DeleteFromBackRoyalJob < ApplicationJob

    queue_as :delete_from_back_royal

    attr_accessor :user_id, :career_profile_id

    def perform(user_id, career_profile_id = nil)

        self.user_id = user_id
        self.career_profile_id = career_profile_id

        # In order to be very careful and deliberate with this code, we decided to hard-code
        # the lists of tables.
        # These lists will need to be updated whenever we add a new table to the schema that
        # contains records that should be deleted when the corresponding User is destroyed.
        delete_from_user_id_tables
        delete_from_candidate_id_tables
        delete_from_hiring_manager_id_tables
        delete_from_career_profile_id_tables
        delete_from_mailboxer_receipts_and_notifications
        delete_from_users_versions
    end

    def delete_from_user_id_tables
        #  SELECT table_name FROM information_schema.columns
        #     WHERE column_name = 'user_id'
        #       AND is_updatable = 'YES'
        %w(
            subscriptions_users
            access_groups_users
            access_groups_users_versions
            career_profile_searches
            career_profiles_versions
            cohort_applications
            cohort_applications_versions
            lesson_streams_progress
            user_participation_scores
            user_project_scores
            editor_lesson_sessions
            events
            hiring_applications
            hiring_applications_versions
            institutions_reports_viewers
            lesson_progress
            lesson_stream_locale_packs_users
            lesson_stream_locale_packs_users_versions
            player_lesson_sessions
            s3_identification_assets
            s3_transcript_assets
            user_chance_of_graduating_records
            mobile_devices
        ).each do |table|
            delete_records_in_batches(table, 'user_id', user_id)
        end

        # These tables don't have an 'id' column, so we handle them differently
        # because we can't do a sub-select with a limit.
        %w(
            users_roles
            users_roles_versions
            user_lesson_progress_records
            institutions_users
            institutions_users_versions
            distinct_user_ids
            old_assessment_scores
        ).each do |table|
            delete_records_at_once(table, 'user_id', user_id)
        end
    end

    def delete_from_candidate_id_tables
        #  SELECT table_name FROM information_schema.columns
        #     WHERE column_name = 'candidate_id'
        #       AND is_updatable = 'YES'
        %w(
            candidate_position_interests
            candidate_position_interests_versions
            hiring_relationships
            hiring_relationships_versions
        ).each do |table|
            delete_records_in_batches(table, 'candidate_id', user_id)
        end
    end

    def delete_from_hiring_manager_id_tables
        # There's an 'open_position_id' FK constraint on candidate_position_interests,
        # so we need to delete any corresponding candidate_position_interests before
        # attempting to delete this user's open_positions records.
        delete_candidate_position_interests_for_open_positions

        #  SELECT table_name FROM information_schema.columns
        #     WHERE column_name = 'hiring_manager_id'
        #       AND is_updatable = 'YES'
        %w(
            hiring_relationships
            hiring_relationships_versions
            open_positions
            open_positions_versions
        ).each do |table|
            delete_records_in_batches(table, 'hiring_manager_id', user_id)
        end
    end

    def delete_from_career_profile_id_tables
        #  SELECT table_name FROM information_schema.columns
        #     WHERE column_name = 'career_profile_id'
        #       AND is_updatable = 'YES'
        if career_profile_id
            delete_transcripts_for_education_experiences

            %w(
                career_profiles_skills_options
                education_experiences
                work_experiences
                career_profiles_awards_and_interests_options
                education_experiences_versions
                peer_recommendations
                peer_recommendations_versions
                work_experiences_versions
                career_profiles_student_network_interests_options
            ).each do |table|
                delete_records_in_batches(table, 'career_profile_id', career_profile_id)
            end

            # This table doesn't have an 'id' column, so we handle it differently
            # because we can't do a sub-select with a limit.
            delete_records_at_once('career_profile_fulltext', 'career_profile_id', career_profile_id)
        end
    end

    def delete_from_mailboxer_receipts_and_notifications
        # Delete mailboxer messages/notifications/receipts.
        # Get the ids of the notications we need to delete,
        # and delete the receipts first because of the FK constraint.
        notification_ids_to_delete = Mailboxer::Notification.where(sender_id: user_id).select(:id).to_sql
        ActiveRecord::Base.connection.execute(%Q~
            DELETE FROM mailboxer_receipts WHERE notification_id IN (#{notification_ids_to_delete});
        ~)
        ActiveRecord::Base.connection.execute(%Q~
            DELETE FROM mailboxer_notifications WHERE id IN (#{notification_ids_to_delete});
        ~)
    end

    def delete_from_users_versions
        ActiveRecord::Base.connection.execute(%Q~
            DELETE FROM users_versions WHERE id = '#{user_id}';
        ~)
    end

    def delete_candidate_position_interests_for_open_positions
        ActiveRecord::Base.connection.execute(%Q~
            DELETE FROM candidate_position_interests WHERE open_position_id IN (SELECT id FROM open_positions WHERE hiring_manager_id = '#{user_id}');
        ~)
    end

    def delete_transcripts_for_education_experiences
        # Note: We check versions because a transcript could have been
        # orphaned, which we allow so as not to lose transcripts for users
        # that change their education_experiences after being accepted.
        ActiveRecord::Base.connection.execute(%Q~
            DELETE FROM s3_transcript_assets
                WHERE education_experience_id IN (
                    SELECT distinct id FROM education_experiences_versions WHERE career_profile_id = '#{career_profile_id}'
                );
        ~)
    end

    def delete_records_in_batches(table_name, column_name, value)
        identifier = table_name.ends_with?('_versions') ? 'version_id' : 'id'

        sql = "SELECT #{identifier} FROM #{table_name} WHERE #{column_name} = '#{value}' LIMIT 1;"

        has_more = ActiveRecord::Base.connection.execute(sql).to_a

        while has_more.present?
            ActiveRecord::Base.connection.execute(%Q~
                DELETE FROM #{table_name} WHERE #{identifier} IN (SELECT #{identifier} FROM #{table_name} WHERE #{column_name} = '#{value}' LIMIT 1000);
            ~)

            has_more = ActiveRecord::Base.connection.execute(sql).to_a
        end
    end

    def delete_records_at_once(table_name, column_name, value)
        ActiveRecord::Base.connection.execute(%Q~
            DELETE FROM #{table_name} WHERE #{column_name} = '#{value}';
        ~)
    end

end
