class UpdateMultipleChoiceChallengeUsageReportsJob < ApplicationJob

    include ScheduledJob

    # a little more than a day, so if everything is working normally, we will get everything in a single batch
    BATCH_INTERVAL = 25.hours

    queue_as :update_multiple_choice_challenge_usage_report
    self.run_at_hour 2  # 2:00 AM Eastern Time (This job uses a not tiny amount of memory, so we run it a bit before when we restart app servers each night)

    def now
        Time.now
    end

=begin
    Overall strategy:

    1. Find the maximum last_answer_at in multiple_choice_challenge_usage_reports.  Assume we have processed all
        events in redshift with a created_at up to that date.
    2. If we are more than 24 hours behind, then process later events in batches of 24 hours (or TARGET_INTERVAL
        if we have overridden the 24 hour default)
    3. For each batch, run a query inside redshift that returns a record for each answer that needs to have its
        count incremented based on data within that batches time (see get_increment for details on the query)
    4. Inside of a transaction, look at each record that came back from redshift and upsert a record in
        multiple_choice_challenge_usage_reports, either creating a new record or incremeting the count on an
        existing one.  In addition to incrementing the count, set the last_answer_at so we can figure out
        where to start looking for new events next time.
=end
    def perform
        last_answer_at = Lesson::MultipleChoiceChallengeUsageReport.maximum(:last_answer_at) || Time.parse('2018/01/01')
        interval = BATCH_INTERVAL

        while last_answer_at < now
            update(last_answer_at, last_answer_at += interval)
        end
    end

    def update(start, finish)

        # returns an array of records each record has
        #  - lesson_id, frame_id, editor_template, challenge_id, answer_id
        #  - count (the number of users who gave this as their first answer to this challenge in the time range)
        #  - last_answer_at (the latest time at which a user gave this as their first answer to this challenge)
        increments = get_increments(start, finish)

        # The transaction needs to be wrapped around all of the updates because last_answer_at
        # has to be tracked consistently so we know which events have already been processed
        EditorTracking.transaction(self.class.name) do
            increments.each do |record|

                # Do an upsert, incrementing by the new count if a record
                # already exists for this answer
                statement = %Q~
                    insert into multiple_choice_challenge_usage_reports (
                        lesson_id
                        , frame_id
                        , editor_template
                        , challenge_id
                        , answer_id
                        , count
                        , created_at
                        , updated_at
                        , last_answer_at
                    )
                    values
                    (
                        '#{record['lesson_id']}'
                        , '#{record['frame_id']}'
                        , '#{record['editor_template']}'
                        , '#{record['challenge_id']}'
                        , '#{record['answer_id']}'
                        , #{record['ct']}
                        , now()
                        , now()
                        , '#{record['last_answer_at']}'
                    )
                    on conflict (lesson_id, frame_id, challenge_id, answer_id) do update
                        set
                            count = multiple_choice_challenge_usage_reports.count + excluded.count
                            , last_answer_at = excluded.last_answer_at

                ~

                Lesson::MultipleChoiceChallengeUsageReport.connection.execute(statement)
            end
        end
    end

    # This method is untested because it is too hard to translate the
    # redshift-specific listagg and regexp_substr into postgresql (Even after
    # replacing them with the analogous functions, I end up with the
    # error "aggregate function calls cannot contain set-returning function calls")
    def get_increments(start, finish)
        RedshiftEvent.connection.execute(%Q~
            /*
                This query has 3 steps:

                1. `user_challenge_pairs`: For the time range, figure out all of
                    the distinct pairs of users and challenges they have answered. This
                    is efficient because we are searching for a single event type in a
                    limited time range.
                2. `first_answers`: For each user/challenge pair, determine what the first
                    answer for the user is and whether that first answer came within the
                    time range.  This still ends up doing a full scan of all challenge_validation
                    events, but since we are limiting to particular user/challenge pairs, most events
                    can be skipped over and only a relatively small number of groups needs to
                    considered.
                3. `increments`: For each distinct answer, count up how many users gave this answer
                    as their first answer within the time range.  This is efficient because we
                    are querying the `first_answers` CTE
            */
            with user_challenge_pairs AS (
                select
                    distinct
                    user_id
                    , lesson_id
                    , editor_template
                    , frame_id
                    , challenge_id
                from events
                where event_type='lesson:challenge_validation'
                    -- we really want to base this stuff on the created_at, but adding in the
                    -- estimated_time allows us to benefit from the sort_key.  This means that we
                    -- might miss a few instances that have estimated_times that are way off, but we
                    -- don't need precision in this report anyhow
                    and estimated_time between
                        '#{(start - 1.hour).utc.strftime('%Y-%m-%d %H:%M:%S.%N')}' and
                        '#{(finish + 1.hour).utc.strftime('%Y-%m-%d %H:%M:%S.%N')}'
                    and created_at between
                        '#{start.utc.strftime('%Y-%m-%d %H:%M:%S.%N')}' and
                        '#{finish.utc.strftime('%Y-%m-%d %H:%M:%S.%N')}'
            )
            , first_answers AS (
                select
                    user_id
                    , events.lesson_id
                    , events.frame_id
                    , events.editor_template
                    , events.challenge_id
                        -- order the events by estimated time and then extract the answer_id
                        -- from the answer in the challenge_responses of the first event
                    , regexp_substr(
                        listagg(
                            distinct regexp_substr(challenge_responses, 'answer_id\":\"(........-....-....-.................)', 1, 1, 'e'), ',')
                            within group (order by estimated_time)
                        , '[^,]+') answer_id
                    , max(created_at) as last_answer_at
                from events
                    join user_challenge_pairs
                        using (user_id, lesson_id, frame_id, challenge_id)
                where event_type='lesson:challenge_validation'

                    -- only consider events that happened before the end of the time range
                    and created_at < '#{finish.utc.strftime('%Y-%m-%d %H:%M:%S.%N')}'
                group by
                    events.user_id
                    , events.lesson_id
                    , events.frame_id
                    , events.editor_template
                    , events.challenge_id

                -- only count users whose first answer is during the time range
                having min(created_at) > '#{start.utc.strftime('%Y-%m-%d %H:%M:%S.%N')}'
            )
            , increments AS (
                select
                    lesson_id
                    , frame_id
                    , editor_template
                    , challenge_id
                    , answer_id
                    , count(*) as ct
                    , max(last_answer_at) as last_answer_at
                from first_answers
                where answer_id != ''
                group by
                    lesson_id
                    , frame_id
                    , editor_template
                    , challenge_id
                    , answer_id
            )
            select * from increments;
        ~).to_a
    end


end