# Note: Update the wiki file with any changes made here.
# See https://pedago.atlassian.net/wiki/display/COM/Valid+Customer.io+fields
class IdentifyUserJob < ApplicationJob

    queue_as :identify_user
    attr_accessor :user

    # IdentifyUserJob delayed jobs that are triggered organically in the wild should
    # have a priority value equal to DEFAULT_PRIORITY, whereas jobs that are triggered
    # as part of a mass re-identification process should user the MASS_IDENTIFY_PRIORITY
    # value, which is a higher numerical value, resulting in lower a precedence in priority
    # (see identify_all_users task in events.rake)
    DEFAULT_PRIORITY = 1
    MASS_IDENTIFY_PRIORITY = Delayed::LOW_PRIORITY

    # Very similar to the debounce in sync_with_airtable_job.
    # See https://trello.com/c/tpljX6pB for why we had to do this.
    def self.perform_later_with_debounce(user_id, priority = IdentifyUserJob::DEFAULT_PRIORITY)
        pending_job = Delayed::Job.where(queue: "identify_user")
            .where(locked_at: nil)
            .where(failed_at: nil)
            .where("handler ILIKE '\%arguments\%#{user_id}\%'")
            .first

        if pending_job
            # if we see that there's already a pending_job, we want to ensure that we don't
            # override the priority, otherwise we could be preventing an organically
            # triggered identify call from executing, which may be needed by email campaigns
            pending_job.update(run_at: Time.now + 1.minute, priority: [pending_job.priority, priority].min)
        else
            self.set(wait_until: Time.now + 1.minute, priority: priority).perform_later(user_id)
        end
    end

    def perform(user_id)
        begin
            self.user = User.find(user_id)
        rescue ActiveRecord::RecordNotFound => exception
            return
        end

        traits = basic_user_info
            .merge(progress_info)
            .merge(institution_info)
            .merge(cohort_info)
            .merge(subscription_info)
            .merge(career_profile_info)
            .merge(hiring_application_info)

        result = user.check_if_json_changed do
            Analytics.identify({
                user_id: user.id,
                traits: traits
            }.with_indifferent_access)

            $customerio.identify(
                traits
            )
        end

        if result == :changed
            raise Delayed::ForceRetry.new("Need to retry Identify later", {
                max_attempts: 3
            })
        end
    end

    def basic_user_info
        info = {
            id: user.id, # required for customer.io
            created_at: user.created_at.to_timestamp, # required for customer.io
            email: user.email, # required for customer.io
            student_network_email: user.student_network_email || user.email, # required for customer.io network campaigns
            email_domain: user.email_domain,
            name: user.name,
            nickname_or_name: user.preferred_name, # convienance field for email templates
            groups: user.group_names.join(','),
            institutions: user.institutions.map(&:name).sort.join(','),
            global_role: user.global_role ? user.global_role.name : nil,
            sign_up_code: user.sign_up_code,
            program_type: user.program_type,
            program_type_confirmed: user.program_type_confirmed,

            company: user.company_name,
            job_title: user.job_title,
            phone: user.phone,
            address_line_1: user.address_line_1,
            address_line_2: user.address_line_2,
            city: user.city,
            state: user.state,
            zip: user.zip,
            country: user.country,

            # In Customer.io, 'timezone' is a specially named attribute used to better localize
            # email campaigns so that the campaigns trigger at a time that's better for the user
            # and so the email content itself can be localized according to the user's timezone.
            # See https://learn.customer.io/documentation/timezone-match.html#the-timezone-attribute.
            timezone: user.timezone_with_fallback,

            # This is needed for triggered campaigns on customer.io
            pref_allow_push_notifications: user.pref_allow_push_notifications
        }
        User::NOTIFICATIONS.each_key { |notification| info[notification] = user.send("#{notification}") }
        info
    end

    def career_profile_info
        career_profile = user.career_profile
        {
            can_edit_career_profile: user.can_edit_career_profile?,
            career_profile_complete_percentage: career_profile ? career_profile.last_calculated_complete_percentage.to_i : '',
            career_profile_active: career_profile ? career_profile.career_profile_active? : '',
            career_profile_salary_range: career_profile ? career_profile.salary_range : '',
            career_profile_resume_uploaded: career_profile && !career_profile.resume.nil?,
            career_profile_status: career_profile ? career_profile.interested_in_joining_new_company : ''
        }
    end

    def hiring_application_info
        hiring_application = user.hiring_application
        {
            hiring_application_status: hiring_application ? hiring_application.status : '',
            hiring_profile_complete_percentage: hiring_application ? hiring_application.last_calculated_complete_percentage.to_i : ''
        }
    end

    def progress_info
        {
            lessons_complete_count: completed_lesson_locale_pack_ids.size
        }
    end

    def institution_info
        active_institution = user.active_institution
        {
            institution_id: active_institution&.id,
            # FIXME: Override domain in a better way - https://trello.com/c/mmsYgraV
            institution_domain: active_institution&.domain || 'smart.ly',
            institution_branding: active_institution&.branding,
            institution_name: active_institution&.name,
            institution_brand_name_short: active_institution&.brand_name_short,
            institution_brand_name_standard: active_institution&.brand_name_standard,
            institution_brand_name_long: active_institution&.brand_name_long
        }
    end

    def cohort_info

        cohort_attrs = {}

        last_application = user.last_application
        cohort = last_application && last_application.published_cohort

        cohort_attrs.merge!({
            cohort_id: last_application ? last_application.cohort_id : nil,
            cohort_title: cohort && cohort.title,
            cohort_start_date: cohort && cohort.start_date && cohort.start_date.to_timestamp,
            cohort_graduation_date: cohort && cohort.graduation_date && cohort.graduation_date.to_timestamp,
            cohort_application_status: last_application ? last_application.status : nil,
            cohort_application_graduation_status: last_application ? last_application.graduation_status : nil,
            cohort_application_skip_period_expulsion: last_application ? last_application.skip_period_expulsion : nil,
            cohort_application_registered: last_application ? last_application.registered : nil,
            cohort_locked_due_to_past_due_payment: last_application ? last_application.locked_due_to_past_due_payment? : nil,
            chance_of_graduating: last_application&.chance_of_graduating,
            cohort_missing_english_proficiency_documents: last_application ? user.missing_english_proficiency_documents? : nil,
            cohort_missing_documents: last_application ? (user.missing_verification_documents? || user.missing_english_proficiency_documents?) : nil,
            cohort_interview_scheduled: last_application && last_application.rubric_interview_scheduled ? true : nil,
            cohort_interview_conducted: last_application && last_application.interview_conducted? ? true : nil,
        })

        if last_application && ['accepted'].include?(last_application.status)
            cohort_attrs.merge!({
                cohort_end_date: cohort.end_date && cohort.end_date.to_timestamp
            })
        else
            cohort_attrs.merge!({
                cohort_end_date: nil
            })
        end

        # In the footer of a bunch of different triggered emails, we include
        # the decision date and start date of the currently promoted mba and
        # emba cohorts.
        #
        # We have to implement this in two different ways depending on the
        # status of the user.
        #
        # 1. For users who have already applied but not yet been accepted, we set
        # this information in a user attribute (immediately below this comment).
        # For these users, we assume that the cohorts they are interested in remain
        # the same until their application status changes.
        #
        # In order to get the start date of both cohorts, `sister_cohort` must
        # return a value, meaning there must be an mba cohort and an emba cohort
        # with the same application deadline.  THIS MEANS THAT IF WE EVER HAVE A PENDING
        # APPLICATION FOR AN MBA OR EMBA COHORT WITH NO SISTER COHORT, IDENTIFY WILL FAIL.
        #
        # Putting the values in a user attribute allows us to build drip campaigns
        # in customer.io that refer to these values days or weeks after an event
        # triggered the start of the campaign, even if the value has changed in
        # the interim.  For an example, see https://fly.customer.io/env/24964/v2/campaigns/1000116/overview
        #
        # 2. In emails to users who have not yet applied, we include these dates in
        # the events that trigger the emails, rather than relying on user
        # attributes.  This means that we cannot set up drip campaigns for these users,
        # because we have no way of knowing if the values in the initial events are
        # still valid or not.
        if last_application && ['pending', 'pre_accepted'].include?(last_application.status) && last_application.mba_or_emba?
            if cohort.sister_cohort.nil? && !cohort.isolated_network
                # If you get this error, you should probably read the long comment above.
                # tldr; The promoted mba and emba cohorts at any particular time should have
                #       the same application deadline.  If this is not possible for some reason
                #       then marketing requirements have changed and we need to consider our
                #       messaging in triggered emails.
                raise "I cannot identify a user with a pending application for a cohort that has no sister cohort"
            end

            # Currently only `isolated_network` EMBA cohorts won't have a `sister_cohort`.
            # Every other MBA/EMBA cohort should have a sister cohort. So, I decided it
            # wasn't worth the extra logic to check/juggle `program_type`s here. We can
            # just assume that if a `sister_cohort` isn't present, and we didn't raise
            # an error above, that we should use the currently promoted MBA cohort as a
            # stand-in for the non-existent `sister_cohort`.
            sister_or_promoted_cohort = cohort.sister_cohort || Cohort.promoted_cohort('mba')
            cohorts_by_program_type = [cohort, sister_or_promoted_cohort].index_by(&:program_type)

            cohort_attrs.merge!({
                pending_user_cohort_info: {
                    mba: cohorts_by_program_type['mba'].event_attributes(user.timezone),
                    emba: cohorts_by_program_type['emba'].event_attributes(user.timezone)
                }
            })
        else
            cohort_attrs.merge!({
                pending_user_cohort_info: nil
            })
        end

        if cohort && cohort.isolated_network
            cohort_attrs.merge!({
                cohort_isolated_network: true
            })
        else
            cohort_attrs.merge!({
                cohort_isolated_network: nil
            })
        end

        if last_application && last_application.pending_or_pre_accepted?
            cohort_attrs.merge!({
                retargeted_from_program_type: last_application.retargeted_from_program_type
            })
        else
            cohort_attrs.merge!({
                retargeted_from_program_type: nil
            })
        end

        cohort_attrs.merge!({
            foundations_courses_complete: user.get_completed_foundations_stream_count
        })

        if last_application && ['accepted'].include?(last_application.status) && Cohort::ProgramTypeConfig[user.program_type].supports_specializations?
            cohort_attrs.merge!({
                #  Both EMBA and MBA cohorts technically support specializations, but only EMBA
                #  currently makes use of them. That said, these checks are still safe since
                # `num_required_specializations` is 0 for all MBA cohorts.
                cohort_specializations_complete_count: completed_specializations_count,
                cohort_coursework_complete: last_application.curriculum_complete? && last_application.required_specializations_complete?
            })
        else
            # Explicitly set to nil to reduce chance of incorrect campaign
            # triggers, i.e. expelled learners getting reminders about being
            # behind on specializations.
            cohort_attrs.merge!({
                cohort_specializations_complete_count: nil,
                cohort_coursework_complete: nil
            })
        end

        cohort_attrs
    end

    def subscription_info
        {
            has_subscription: !!user.primary_subscription,
            subscription_past_due: user.primary_subscription&.past_due,
            stripe_plan_id: user.primary_subscription&.stripe_plan_id
        }
    end

    def completed_lesson_locale_pack_ids
        @completed_lesson_locale_pack_ids ||= user.completed_lesson_locale_pack_ids
    end

    def completed_specializations_count
        @completed_specializations_count ||= user.completed_specializations_count
    end
end