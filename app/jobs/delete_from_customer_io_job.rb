class DeleteFromCustomerIoJob < ApplicationJob

    queue_as :delete_from_customer_io

    def perform(user_id)

        $customerio.delete(user_id)

    end

end
