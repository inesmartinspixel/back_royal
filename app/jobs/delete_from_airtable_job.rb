class DeleteFromAirtableJob < ApplicationJob

    queue_as :delete_from_airtable
    attr_accessor :user_id

    def perform(cohort_applications_versions_attrs)

        cohort_applications_versions_attrs.each do |attrs|

            version = OpenStruct.new(attrs)

            # NOTE: If you are receiving "Airrecord::Error: HTTP 404: :" on local development or staging
            # when calling find_by_id_with_fallback from this job, it is most likely caused by the absence
            # of an associated archived base in Airtable. Do NOT point your application.yml to the LIVE
            # archives to fix this. Either ignore it, or duplicate the archives and point your
            # application.yml to the duplicated archives.
            # Also, see the comment in the specs to understand why it's VERY important that we use the
            # `find_by_id_with_fallback` method rather than just `find_by_id`.
            airtable_record = Airtable::Application.klass_for_base_key(version.airtable_base_key).find_by_id_with_fallback(version)

            # If we didn't find an application, just go to the next version
            next if airtable_record.nil?

            airtable_record.destroy

        end

    end

end
