class Ensure3xPhotoSizes < ApplicationJob
    queue_as :ensure_3x_photo_sizes

    def self.has_images_to_process?(lesson)
        images_to_process(lesson).any?
    end

    def self.images_to_process(lesson)
        lesson.content_json['frames'].
            map { |f| f['components'] }.
            flatten.map do |component|

                next unless component && component['component_type'] == 'ComponentizedFrame.Image'
                missing_formats = get_missing_formats(component)
                if missing_formats.any?
                    [component, missing_formats]
                else
                    nil
                end
            end.compact
    end

    def self.get_missing_formats(image_model)
        begin
            formats = image_model['image']['formats'] || {}
        rescue NoMethodError => err
            return []
        end

        Lesson.image_formats.except(*formats.keys)
    end

    def self.queued?(lesson, version_types)
        !!Delayed::Job.where(queue: Ensure3xPhotoSizes.queue_name).detect do |job|
            lesson_id, _version_types = YAML.load(job.handler).job_data['arguments']

            lesson_id == lesson.id && version_types == _version_types
        end
    end


    # when this job is run with perform_later, support_reschedule
    # should always be true.  When run from the command line for development purposes,
    # you probably want to set it to false so you can run it again and
    # again on a particular lesson and so that it won't schedule tasks.
    def perform(lesson_id, version_types, support_reschedule = true)

        raise ArgumentError.new("No lesson id") unless lesson_id
        raise ArgumentError.new("No version_types") unless version_types

        @cached_formats = Hash.new { |h, url| h[url] = {} }
        @lesson_id = lesson_id
        @version_types = version_types.map(&:to_s) # symbols not supported in queued jobs
        @support_reschedule = support_reschedule
        @user = User.find_or_create_by(email: '3x-photo-migrator@pedago.com') do |user|
            user.name = 'Image Migrator'
            user.provider = 'email'
            user.password = user.password_confirmation = 'password'
            user.add_role(:admin)
        end

        ['live', 'published', 'old'].each do |version_type|
            if version_types.include?(version_type)
                send(:"process_#{version_type}")
            end
        end

    end

    def process_live
        should_save = false
        if Time.now - lesson.updated_at < 1.hour
            return reschedule if @support_reschedule
        end
        if process_instance(lesson)
            Lesson.update_from_hash!(@user, {
                'id' => lesson.id,
                'updated_at' => lesson.updated_at,
                'frames' => lesson.content_json['frames'],
                'lesson_type' => 'frame_list'
            })
        end
    rescue CrudFromHashHelper::OldVersion => err
        # if someone else is editing this lesson, reschedule
        # for later
        if @support_reschedule
            reschedule
        else
            raise err
        end
    end

    def process_published
        return unless version = lesson.published_version
        update_version(version)
    end

    def process_old
        lesson.versions.each do |version|
            next unless version.operation === 'U'
            update_version(version)
        end
    end

    def reschedule
        self.class.set(wait: 1.hour).perform_later(
            @lesson_id,
            @version_types,
            @support_reschedule)
    end

    def process_instance(instance)
        images_to_process = self.class.images_to_process(instance)

        LogToS3.log('3x-image-migration', "generating new formats for #{images_to_process.size} images in #{instance.title}-#{instance['version_id'] || 'live'}")
        return false unless images_to_process.any?

        results = images_to_process.map do |pair|
            component, missing_formats = pair
            add_formats(component, missing_formats)
        end
        LogToS3.log('3x-image-migration', "new formats generated for #{instance.title}-#{instance['version_id'] || 'live'}", results.join("\n"))

        true
    end

    def update_version(version)
        if process_instance(version)
            version.content_json_will_change!
            version.commit_changes
        end
    end

    def add_formats(image_model, missing_formats)
        url = image_model['image']['formats']['original']['url']

        return "#{image_model['label'].inspect} has unprocessable url #{url.inspect}" unless url.starts_with?('http')

        image = image_model['image']
        formats = image['formats']

        # this is the id of the original s3asset.  It was never used
        # for anything, but since it is now inaccurate, let's
        # delete it so no one accidentally tries to use it for
        # something in the future
        image.delete('id')

        # theoretically, it does not make sense to cache the whole
        # hash of new formats together, because some of the formats
        # might be cached an others not.  In practice though, it's always
        # the same set of new formats, so it's ok to simplify things
        # by only pulling when the exact set of new formats is cached
        if new_formats = get_cached_formats(url, missing_formats)
            result = "#{image_model['label'].inspect} new formats pulled from cache"
        else
            start = Time.now
            begin
                s3_asset = S3Asset.new({
                    file: url,
                    :directory => "images/",
                    :styles => missing_formats.to_json
                })
            rescue OpenURI::HTTPError => err
                if err.message.match(/403/)
                    return "#{image_model['label'].inspect} could not be processed because image has vanished"
                else
                    raise err
                end
            end
            s3_asset.save!(validate: false) # we want to skip the image size validation

            new_formats = s3_asset.as_json.with_indifferent_access['formats'].slice(*(missing_formats.keys))

            result = "#{image_model['label'].inspect} new formats generated in #{(Time.now - start).to_timestamp} seconds"

            save_cached_formats(url, missing_formats, new_formats)
            # we do not need this record in the db
            s3_asset.delete
        end

        formats.merge!(new_formats)
        result
    end

    def lesson
        @lesson ||= Lesson.find(@lesson_id)
    end

    def formats_to_hash_key(formats)
        formats.keys.sort.to_s
    end

    def get_cached_formats(url, missing_formats)
        @cached_formats[url][formats_to_hash_key(missing_formats)]
    end

    def save_cached_formats(url, missing_formats, new_formats)
        @cached_formats[url][formats_to_hash_key(missing_formats)] = new_formats
    end
end
