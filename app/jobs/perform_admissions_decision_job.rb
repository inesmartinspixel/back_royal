class PerformAdmissionsDecisionJob < ApplicationJob

    queue_as :perform_admissions_decision

    def perform(application_id)
        application = CohortApplication.find(application_id)
        EditorTracking.transaction('PerformAdmissionsDecisionJob') do
            application.perform_admissions_decision
        end
    end
end