class Report::RefreshInternalContentTitles < ApplicationJob
    queue_as :refresh_internal_content_titles

    def perform(force = false)
        return if Rails.env.test? && !force

        ActiveRecord::Base.with_statement_timeout(0) do
            ActiveRecord::Base.connection.execute('refresh materialized view internal_content_titles')
        end
    end
end