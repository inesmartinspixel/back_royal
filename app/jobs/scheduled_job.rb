# Usage
#
# See RefreshSharedSitemaps for an example
#
# 1. include ScheduledJob
# 2. in initializers/delayed_job.rb, add class to ScheduledJob.registered_klasses
# 3. set a unique queue using `queue_as`.  This must be unique because ScheduledJob assumes
#     that there should be only one job in this queue at any time.  (It's possible that we might
#     want to extend ScheduledJob to support scheduling multiple jobs of the same type, but we have
#     not needed it yet)
# 4. set either run_at_hour or schedule_interval
#    - schedule_interval is some number of seconds to wait between each run of the job.  See
#           RefreshSharedSitemaps for an example
#    - run_at_hour is an integer from 0-23 which determines what hour of the day in EST the job should run.
#           See DailyMarketingUpdate for an example
#       - run_at_minute is an integer from 0-59 which determines at what minute of the run_at_hour (EST)
#               the job should be run.
#       - run_on_day is a symbol like :monday, :tuesday, etc.  If it is set, then run_at_hour should be
#               set too, and the job will run once per week on that day at that hour (EST)
# 5. if you want to ensure that only one job exists at a time, then create an index like
#       add_index :delayed_jobs, :queue, where: "queue = 'MY_QUEUE'", unique: true
#    (this will practically probably always be the case regardless, but if you need a guarantee, this will do it).
#
# 6. for local development, you will have to run `rake delayed_job:prepare_scheduled_jobs`
#    - see .ebextensions/94_prepare_scheduled_jobs.config for how this happens on staging/prod
#
#    NOTE: if schedule_interval and run_at_hour are both nil, the job will not be scheduled at
#           all.  We use this to turn off RefreshSharedSitemaps on staging, by linking schedule_interval
#           to ah environment variable
#
#    NOTE: the job_scheduler queue must be included in DELAYED_JOB_QUEUES, along with the queue
#           for your job

module ScheduledJob

    extend ActiveSupport::Concern

    def self.registered_klasses
        [
            DailyMarketingUpdate,
            RefreshSharedSitemaps,
            WriteRegularlyIncrementallyUpdatedTablesJob,
            RefreshMaterializedInternalReportsViews,
            SyncFromAirtableJob,
            SendHiringManagerTrackingEmailsJob,
            AutoExpireOpenPositionsJob,
            AutoCheckContentLinksJob,
            UpdateRefundReportJob,
            UpdateMultipleChoiceChallengeUsageReportsJob,
            ScheduleRecommendedPositionsEmailsJob,
            ImportAndPruneRedshiftEventsJob,
            SignNowCompletionPollingJob,
            EnsureEventsCopiedToRedshiftJob,
            DeleteOldEventsJob
        ]
    end

    # make a schedule_all_jobs method that
    # can be run when the app is initialized to
    # schedule the first run of any job that is not
    # yet scheduled
    def self.schedule_all_jobs
        self.registered_klasses.each(&:schedule_with_default_interval)
    end


    included do
        attr_accessor :schedule_interval
        attr_accessor :run_at_hour
        attr_accessor :run_at_minute
        attr_accessor :run_on_day

        # after each run of a job, the next run is scheduled
        set_callback :perform, :after do
            self.class.schedule_with_default_interval
        end
    end

    module ClassMethods
        def schedule(run_at)
            ScheduledJob::Schedule.perform_later(self.name, run_at.to_s)
        end

        def schedule_with_default_interval
            run_at = self.get_run_at
            if run_at.present?
                self.schedule(run_at)
            end
        end

        def schedule_interval(*args)
            if args.length == 0
                @schedule_interval
            else
                @schedule_interval = args[0]
                @schedule_interval = nil unless @schedule_interval && @schedule_interval > 0 # if configured to 0, it will not run
            end
        end

        def run_at_hour(*args)
            if args.length == 0
                @run_at_hour
            else
                @run_at_hour = args[0]
            end
        end

        def run_at_minute(*args)
            if args.length == 0
                @run_at_minute
            else
                @run_at_minute = args[0]
            end
        end

        def run_on_day(*args)
            if args.length == 0
                @run_on_day
            else
                @run_on_day = args[0]
            end
        end

        def get_run_at
            if self.schedule_interval
                Time.now + self.schedule_interval
            elsif self.run_at_hour
                # If any other jobs have been configured with the same run_at_hour and run_at_minute as this job,
                # send a warning to Sentry so we can reconfigure the jobs to run at different times.
                if self.run_at_minute
                    jobs_with_same_run_at = []
                    ScheduledJob.registered_klasses.each do |job_klass|
                        if job_klass.queue_name != self.queue_name && job_klass.run_at_hour == self.run_at_hour && job_klass.run_at_minute == self.run_at_minute
                            jobs_with_same_run_at.push(job_klass.name)
                        end
                    end

                    if jobs_with_same_run_at.any?
                        jobs_with_same_run_at.push(self.name)
                        Raven.capture_exception('Jobs have been configured with the same run_at_hour and run_at_minute. Reconfigure the following jobs to run at different times.', {
                            level: 'warning',
                            extra: {
                                jobs_with_same_run_at: jobs_with_same_run_at
                            }
                        })
                    end
                end

                run_at = Time.now.in_time_zone('Eastern Time (US & Canada)').change({
                    hour: self.run_at_hour,
                    min: self.run_at_minute || 0,
                    sec: 0
                })

                # We have a time at the correct hour, but it might be in
                # the past or it might be on the wrong weekday.  Keep adding
                # days to it until we find one that is in the future and
                # on the correct weekday (of applicable)
                while true
                    break if acceptable_run_at?(run_at)
                    run_at = run_at + 1.day
                end
                run_at
            end
        end

        private
        def acceptable_run_at?(run_at)
            # this method should always take a time that is at the appropriate
            # hour.  However it might be in the past or it might be on the wrong
            # weekday.
            if self.run_on_day.nil? && run_at > Time.now
                true
            elsif self.run_on_day.nil?
                false
            elsif run_at.send(:"#{run_on_day}?") && run_at > Time.now
                true
            else
                false
            end
        end

    end

    # Schedule is a separate job that is responsible for
    # making sure the main job is run on a recurring
    # basis.  This has to be a separate job so that the main
    # job can finish and get deleted from the delayed_jobs table before
    # we try to schedule the next.
    class Schedule < ApplicationJob

        queue_as :job_scheduler

        def self.schedule(klass, run_at)

            # there is some tiny chance that two processes running this simultaneously
            # could each schedule a job.  Hopefully that's not too scary. See the note
            # at the top about creating a unique index to prevent this if you need a 100% guarantee
            locked_jobs = get_locked_jobs(klass)
            pending_jobs = get_pending_jobs(klass)
            existing_jobs = get_existing_jobs(klass)

            # if there are no jobs at all, create one
            if existing_jobs.empty?
                klass.set(wait_until: run_at).perform_later

            # if all the existing jobs are locked, try again in 10 seconds. Most likely,
            # the existing job has already run the after callback that is trying to schedule the
            # next job, but it hasn't been cleaned up yet for some reason.
            elsif locked_jobs.any? && pending_jobs.empty?
                Schedule.set(wait: 10.seconds).perform_later(klass.name, run_at.to_s)
            end
        end

        def self.delete_queued_and_schedule(klass, run_at)
            get_pending_jobs(klass).each(&:destroy)
            self.schedule(klass, run_at)
        end

        def self.get_existing_jobs(klass)
            Delayed::Job.where(:queue => klass.queue_name)
        end

        def self.get_locked_jobs(klass)
            get_existing_jobs(klass).select { |j| j.locked_at.present? && j.failed_at.nil? }
        end

        def self.get_pending_jobs(klass)
            get_existing_jobs(klass).select { |j| j.locked_at.nil? && j.failed_at.nil? }
        end

        def perform(class_name, run_at_str)
            klass = class_name.constantize
            run_at = Time.parse(run_at_str)
            self.class.schedule(klass, run_at)
        end

    end

end


