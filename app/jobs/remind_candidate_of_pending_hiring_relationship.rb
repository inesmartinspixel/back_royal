class RemindCandidateOfPendingHiringRelationship < ApplicationJob
    queue_as :remind_candidate_of_pending_hiring_relationship

    def perform(hiring_relationship_id, num_hours_in_delay)
        hr = HiringRelationship.find_by_id(hiring_relationship_id)

        # Return early if we shouldn't notify the candidate about the relationship
        if hr.nil? || hr.candidate_status != 'pending'|| hr.hiring_manager_status != 'accepted' || hr.closed? ||
            hr.hiring_manager.hiring_application.status != 'accepted' || !!hr.conversation&.contains_message_from_user?(hr.candidate_id)
            return
        end

        hr.log_event('pending_request_reminder', 'candidate', {
            num_hours_in_delay: num_hours_in_delay
        }).log_to_external_systems
    end
end
