class GetIdologyResultJob < ApplicationJob

    queue_as :get_idology_result

    def self.ensure_job(idology_id_number, delay)
        existing_job = Delayed::Job.where(queue: self.queue_name)
                    .where("handler ILIKE '\%arguments\%#{idology_id_number}\%'")

        return existing_job if existing_job.present?

        self.set(wait: delay).perform_later(idology_id_number)
    end

    def perform(idology_id_number)
        idology_verification = IdologyVerification.find_by_idology_id_number(idology_id_number)
        EditorTracking.transaction(self.class.name) do
            idology_verification&.fetch_result_and_record_verification
        end
    end

end