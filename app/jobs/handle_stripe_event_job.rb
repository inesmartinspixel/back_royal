class HandleStripeEventJob < ApplicationJob
    queue_as :handle_stripe_event

    def self.stripe_jobs_for_user(customer_id)
        Delayed::Job
            .where(queue: HandleStripeEventJob.queue_name)
            .where("handler ILIKE '\%arguments\%#{customer_id}\%'")
    end

    # Since we'll error if trying to run multiple stripe event jobs for a particular
    # user, we might as well go ahead and space them a bit as we queue them up.
    def self.perform_later_with_delay(customer_id, event_id, serialized_event)
        # Number of existing jobs for the user, plus one for the coming up job, times five seconds
        delay = (HandleStripeEventJob.stripe_jobs_for_user(customer_id).size + 1) * 5.seconds
        HandleStripeEventJob.set(wait_until: Time.now + delay)
            .perform_later(customer_id, event_id, serialized_event)
    end

    # Note: We expect to see failures on staging due to our development environment
    # being used in both staging and local development. So let's just cap the retry limit
    # to three and tell failed jobs to be terminated when we are on staging so that we don't
    # have constant failures on staging, nor have to constantly cleanup manually.
    # See https://github.com/collectiveidea/delayed_job#gory-details
    def is_staging?
        ENV['APP_ENV_NAME']&.match?(/staging/i)
    end

    def max_attempts
        is_staging? ? 3 : Delayed::Worker.max_attempts
    end

    def destroy_failed_jobs?
        is_staging? ? true : Delayed::Worker.destroy_failed_jobs
    end

    # Tweaked the exponential backoff to first try up to five times
    # with a quick random delay, then exponentially backoff.
    # We don't want to delay too much with these Stripe events if we can help it.
    # See https://github.com/collectiveidea/delayed_job/blob/master/lib/delayed/backend/base.rb#L106
    def reschedule_at(current_time, attempts)
        attempts_before_exponential_backoff = 5
        random_offset = rand(0.0...5.0)
        if attempts <= attempts_before_exponential_backoff
            current_time + random_offset
        else
            # Modified to not count the prior quick attempts
            current_time + ((attempts - attempts_before_exponential_backoff) ** 4) + 5
        end
    end

    # We pass in the event_id so that we can use it in the "other running"
    # logic. I couldn't figure out how to get this job's id that corresponds to
    # the one in the delayed_jobs table, and the actual event is serialized
    # (so we can't run an ILIKE query to see the id that way).
    def perform(customer_id, event_id, serialized_event)
        event = Marshal::load(serialized_event)
        event_type = event.type

        # We only check the particular customer for currently processing events since
        # we believe that locking objects for the same customer in parallel transactions
        # is the source of a deadlock situation. So essentially, we process the bursts
        # of Stripe events for a particular customer serially.
        # See https://trello.com/c/Nn0EYWst
        other_running_stripe_jobs_for_user = HandleStripeEventJob.stripe_jobs_for_user(customer_id)
            .where.not("handler ILIKE '\%arguments\%#{event_id}\%'")
            .where.not(locked_at: nil)

        if other_running_stripe_jobs_for_user.any?
            raise Delayed::ForceRetry.new('Waiting to handle Stripe event')
        else
            StripeEventHandler.new(event)
        end
    end
end