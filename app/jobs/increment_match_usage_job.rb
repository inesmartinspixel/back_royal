class IncrementMatchUsageJob < ApplicationJob

    queue_as :increment_match_usage_job

    def perform(hiring_relationship_id)
        EditorTracking.transaction('IncrementMatchUsageJob') do
            hr = HiringRelationship.find_by_id(hiring_relationship_id)

            # additional safeguard to ensure we never re-bill for the same relationship twice
            return unless hr&.stripe_usage_record_info.nil?

            # validate we have a subscription
            subscription = hr.hiring_team&.primary_subscription
            raise "No active subscription for hiring team found!" unless subscription

            # do not continue processing if we're not metered
            return unless subscription.metered?

            # Set the timestamp in advance in case anything below fails during processing. We want to
            # explicitly commit this save prior to attempting to save usage, so that we can fall back on
            # stripe_usage_record_info safeguarding of potential double-billing. This could occur if a
            # job were to fail after successfully recording Usage in Stripe (network errors, etc).
            recorded_at_ts = Time.now.to_timestamp
            hr.update(stripe_usage_record_info: { timestamp: recorded_at_ts })

            begin
                # generate a usage record and save the id for future checks
                usage_record = subscription.increment_usage(recorded_at_ts)

                # save UsageRecord details
                hr.stripe_usage_record_info = usage_record.to_h.except(:livemode, :object)
                hr.save!

            rescue => err

                # This may be hard to verify until Stripe opens up a list / retrieval API for UsageRecords.
                # The timestamp at least gives us the ability to use a `set` action to re-write the historical record.
                msg = "Failure while recording usage for HiringRelationship. Job retries will succeed. " +
                        "Please validate that Usage was properly recorded."
                Raven.capture_exception(msg, {
                    extra: {
                        reason: err.message,
                        hiring_relationship_id: hiring_relationship_id,
                        subscription_id: subscription.id
                    }
                })

            end
        end


    end
end
