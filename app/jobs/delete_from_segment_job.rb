# For more insight, visit:
# https://segment.com/docs/guides/best-practices/user-deletion-and-suppression/
# https://reference.segmentapis.com
class DeleteFromSegmentJob < ApplicationJob

    require "httparty"

    queue_as :delete_from_segment
    attr_accessor :user_id

    class DeleteFromSegmentJob::SuppressionError < RuntimeError; end

    def perform(user_id)
        return unless user_id.present?
        self.user_id = user_id
        suppress_and_delete_user
    end

    def suppress_and_delete_user

        segment_api_response = HTTParty.post(endpoint, {
            headers: {
                'Authorization' => "Bearer #{access_token}",
                'Content-Type' => 'application/json'
            },
            body: {
                "regulation_type" => "Suppress_With_Delete",
                "attributes" => {
                    "name" => "userId",
                    "values" => [
                        user_id
                    ]
                }
            }.to_json
        })

        return segment_api_response unless !segment_api_response.success?

        err = segment_api_response['error']

        # If the error is due to rate limiting or an upstream timeout, 
        # we should retry later. Otherwise, let's capture the exception
        # on Sentry for visibility.
        # NOTE: we don't use `max_attempts` here for a reason. These deletion/
        # suppression requests can take up to 30 minutes each. If we're getting
        # dinged for rate limiting, we'll need to retry until we get a large
        # enough back off for the job to run successfully.
        if err.match(/(too many concurrent requests|upstream request timeout)/i)
            raise DeleteFromSegmentJob::SuppressionError.new("Need to retry deletion later!")
        else
            Raven.capture_exception("Unable to suppress and delete user!", {
                extra: {
                    user_id: user_id,
                    errors: err.to_json
                }
            })
        end

    end

    private
    def access_token
        ENV['SEGMENT_DELETION_ACCESS_TOKEN']
    end

    private
    def endpoint
        "https://platform.segmentapis.com/v1beta/workspaces/#{ENV['SEGMENT_WORKSPACE_SLUG']}/sources/#{ENV['SEGMENT_SOURCE_SLUG']}/regulations"
    end

end