class RefreshMaterializedInternalReportsViews < ApplicationJob

    # This job is run once per day.
    #
    # The name of it is totally wrong at this point.  These are not all internal
    # anymore, and they are not all reports anymore.
    #
    # * PlayerLessonSession records are available to institutional clients
    # * UserChanceOfGraduatingRecords affect the communications that users receive.
    # * PlaylistProgress, LessonActivityByCalendarDateRecords, UserProgressRecords affect reports pages

    include ScheduledJob

    queue_as :refresh_materialized_internal_reports_views
    if ENV['INTERNAL_REPORT_VIEWS_REFRESH_HOURS']
        self.schedule_interval ENV['INTERNAL_REPORT_VIEWS_REFRESH_HOURS'].to_f.hours
    else
        self.run_at_hour 8 # run this just before we come into work, so hopefully everything is as up-to-date as possible at the beginning of the day
    end

    # See https://trello.com/c/f4V9IQVV for why we had to do this.
    # NOTE: With the current implementation, there's a possibility that a job may have been set to run a half hour
    # or so before the job is normally scheduled to run (see run_at_hour above). This could potentially lead to an
    # edge case where the job runs and then ScheduledJob does its thing to schedule a new job at the run_at_hour.
    # The newly scheduled job will then run shortly after the previous job, even though there may not be any new
    # data to refresh. Despite the unnecessary redundancy, we're okay with this for now.
    def self.perform_later_with_debounce(run_at = nil, options = {})
        existing_jobs = Delayed::Job.where(queue: "refresh_materialized_internal_reports_views").where(failed_at: nil).to_a

        now = Time.now
        five_minutes_from_now = now + 5.minutes
        twenty_minutes_from_now = now + 20.minutes

        currently_running_job = existing_jobs.find { |job| job.locked_at.present? }
        pending_jobs = existing_jobs.find_all { |job| job.locked_at.nil? }.sort_by(&:run_at) # sorting isn't necessary, but helps with specs

        if currently_running_job.present? && pending_jobs.empty?
            # If a currently_running_job is found, it may not have been able to refresh the views with the most
            # up-to-date data, so we enqueue a new job to be run twenty_minutes_from_now, which should hopefully
            # be enough time so that there's no overlap between the two jobs.
            self.set(wait_until: twenty_minutes_from_now).perform_later(options)
        elsif pending_jobs.any?
            # Theoretically, we should only ever have one pending job, but as a fail-safe, if we ever see that we have more
            # than one, we just grab one of them, update its run_at value, and then destroy the remaining pending jobs since
            # they aren't necessary. The new run_at value is determined by whether or not a currently_running_job exists.
            # If one exists, we update the run_at to be twenty_minutes_from_now so that the new job hopefully won't overlap
            # with the currently_running_job, otherwise we update the run_at to five_minutes_from_now so that the new data
            # is made available sooner rather than later.
            pending_job = pending_jobs.shift
            pending_jobs.each(&:destroy)
            pending_job.update(run_at: currently_running_job.present? ? twenty_minutes_from_now : run_at || five_minutes_from_now)
        end
    end

    def perform(options = {})

        # right after a migration that changes views, some views
        # will not be populated
        RefreshMaterializedContentViews.ensure_all_populated

        ActiveRecord::Base.with_statement_timeout(0) do
            # allow these guys to fail individually without blocking the rest of the job
            # we can follow up later and fix them
            Raven.capture_in_production(Exception) do
                Cloudwatch.put_timing_metric_data(metric_name: 'Report::EditorLessonSession.write_new_records') do
                    Report::EditorLessonSession.write_new_records
                end
            end

            Raven.capture_in_production(Exception) do
                Cloudwatch.put_timing_metric_data(metric_name: 'Report::PlayerLessonSession.write_new_records') do
                    Report::PlayerLessonSession.write_new_records
                end
            end

            [
                # Up until 2019/06/05, these 3 queries were in RefreshMaterializedProgressViews,
                # and were updated every 10 minutes.  We decided to move them here and do them
                # less often, even though that means the reports pages are not so up-to-date
                ViewHelpers::PlaylistProgress,
                ViewHelpers::LessonActivityByCalendarDateRecords,
                ViewHelpers::UserProgressRecords,

                ViewHelpers::CohortApplicationsPlus
            ].each do |klass|
                klass.refresh_concurrently
            end

            [
                ViewHelpers::CohortUserProgressRecords,
                ViewHelpers::ProgramEnrollmentProgressRecords,
                ViewHelpers::CompletedAndExpectedPayments
            ].each do |klass|
                klass.refresh_concurrently
            end

            Raven.capture_in_production(Exception) do
                Cloudwatch.put_timing_metric_data(metric_name: 'CohortUserPeriod.write_new_records') do
                    CohortUserPeriod.write_new_records
                end
            end

            # this relies on the output from the MdpSuccessPredictor, so must be run after
            ViewHelpers::CohortConversionRates.refresh_concurrently
        end
    end

end