class AutoExpireOpenPositionsJob < ApplicationJob

    include ScheduledJob

    queue_as :auto_expire_open_positions

    # We run the job in the morning before work typically starts for hiring managers.
    # Before changing this, see expirationCancellationOrRenewalDateForPosition in the client
    self.run_at_hour 7

    def perform
        today = Date.today

        open_positions_to_expire_today_by_hiring_manager_id = OpenPosition.where("archived = FALSE AND auto_expiration_date <= '#{today.to_s}'").group_by(&:hiring_manager_id)
        open_positions_to_expire_in_2_weeks_by_hiring_manager_id = OpenPosition.where("archived = FALSE AND auto_expiration_date = '#{(today + 2.weeks).to_s}'").group_by(&:hiring_manager_id)

        EditorTracking.transaction('AutoExpireOpenPositionsJob') do
            open_positions_to_expire_today_by_hiring_manager_id.each do |hiring_manager_id, positions|
                auto_expire_positions!(hiring_manager_id, positions)
            end

            open_positions_to_expire_in_2_weeks_by_hiring_manager_id.each do |hiring_manager_id, positions|
                log_event_to_external_systems!('auto_expiring_positions_warning', hiring_manager_id, positions)
            end
        end
    end

    def auto_expire_positions!(hiring_manager_id, positions)
        positions.each do |position|
            position.just_auto_expired = true

            position.update!(archived: true, manually_archived: false)

            position.candidate_position_interests_needing_review.each do |candidate_position_interest|
                candidate_position_interest.update!(hiring_manager_status: 'rejected')
            end
        end

        log_event_to_external_systems!('auto_expired_positions', hiring_manager_id, positions)
    end

    def log_event_to_external_systems!(event_label, hiring_manager_id, positions)
        hiring_manager = User.find(hiring_manager_id)
        return unless hiring_manager.hiring_application.accepted?

        Event.create_server_event!(
            SecureRandom.uuid,
            hiring_manager_id,
            "hiring_manager:#{event_label}",
            {
                position_titles: positions.map(&:title).sort
            }
        ).log_to_external_systems
    end
end
