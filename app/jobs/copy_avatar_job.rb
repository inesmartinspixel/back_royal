class CopyAvatarJob < ApplicationJob

    queue_as :copy_avatar
    attr_accessor :user_id, :log_rescued_http_errors, :avatar_url

    def perform(user_id, log_rescued_http_errors = true)
        self.user_id = user_id
        self.log_rescued_http_errors = log_rescued_http_errors
        user = User.find_by_id(user_id)

        # If we used user.avatar_url, it would look in user.avatar.file.url
        # We don't want that.  Right here we just want to see what is
        # saved in the database column, so we use user.attributes['avatar_url']
        self.avatar_url = user && user.attributes['avatar_url']
        return unless avatar_url

        begin

            avatar = AvatarAsset.new
            avatar.fetch_file_from_url(avatar_url)

            user.avatar = avatar
            EditorTracking.transaction("CopyAvatarJob") do
                user.save!
            end

        rescue OpenURI::HTTPError => err

            status = err.io.status[0]

            # Some errors we can be pretty sure will not be helped by a retry. In that
            # case we log to sentry so we can keep track of how often they are happening,
            # and let the job succeed so it will not retry
            if ['400', '403', '404', '500', '502'].include?(status)
                handle_error(err, status)

            # Some kind of network errors will be fixed with a retry, so it is good
            # to raise and let delayed job retry
            else
                raise
            end

        rescue AvatarAsset::DisallowedScheme => err
            handle_error(err, 'DisallowedScheme')

        rescue ActiveRecord::RecordInvalid => err
            handle_error(err, 'RecordInvalid')

        end

    end

    def handle_error(err, code)
        # we support turning off sentry logging because we wanted that
        # when we ran the initial backfill to prevent thousands
        # of errors going to sentry
        if log_rescued_http_errors

            source = url_source(avatar_url)
            Raven.capture_exception(err, {
                extra: {
                    user_id: user_id,
                    url: avatar_url,
                    source: source
                },
                :fingerprint => [source, code]
            })
        end
    end

    def url_source(url)
        source = 'unknown'
        {
            'facebook' => 'facebook',
            'fbsbx' => 'facebook',
            'fbcdn' => 'facebook',
            'google' => 'google',
            'licdn' => 'linkedin',
            'gravatar' => 'gravatar'
        }.each do |matcher, _source|
            if url.match(matcher)
                source = _source
            end
        end
        source
    end

end
