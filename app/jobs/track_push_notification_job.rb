class TrackPushNotificationJob < ApplicationJob

    queue_as :track_push_notification

    attr_accessor :hash

    ENDPOINT = 'https://track.customer.io/push/events'

    def perform(hash)
        uri = URI.parse(ENDPOINT)

        keys = ['delivery_id', 'device_id', 'event']
        properties = hash.with_indifferent_access.slice(*keys).reject{ |key, val| val.blank? }
        return unless (keys - properties.keys).empty?

        session = Net::HTTP.new(uri.host, uri.port)
        session.use_ssl = true

        request = Net::HTTP::Post.new(uri)
        request.basic_auth ENV['CUSTOMER_IO_SITE_ID'], ENV['CUSTOMER_IO_API_KEY']
        request.body = properties.merge({
            timestamp: Time.now.to_timestamp
        }).to_json

        session.start do |http|
            http.request(request)
        end
    end

end