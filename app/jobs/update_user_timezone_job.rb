class UpdateUserTimezoneJob < ApplicationJob
    queue_as :update_user_timezone # if this name ever changes, be sure to update the delayed_job initializer

    # Timezone::Error::Lookup errors are raised on network errors (e.g. establishing a connection, network timeouts, etc.).
    # Since we don't particularly want to know about these kinds of errors in Sentry since there's probably not much we can
    # do to fix the underlying issue, we configure these jobs to retry on their own since they might self-correct given time.
    # See http://api.rubyonrails.org/v5.2.0/classes/ActiveJob/Exceptions/ClassMethods.html#method-i-retry_on.
    retry_on Timezone::Error::Lookup, wait: :exponentially_longer, attempts: 3

    def perform(class_name, id)
        klass = class_name.constantize
        record = klass.find_by_id(id)
        timezone = nil

        if record
            # If there's another record that has the same place_id that's associated with a user that already
            # has a timezone value set, use that timezone instead of making an API request to Google since that's
            # going to cost us money (see https://developers.google.com/maps/documentation/timezone/usage-and-billing).
            if record.place_id
                records = []

                # career_profiles and hiring_applications support a place_id, so we check both tables for good measure
                [CareerProfile, HiringApplication].each do |_klass|
                    records.concat(_klass.joins(:user)
                        .where.not(users: {id: record.user_id})
                        .where.not(users: {timezone: nil})
                        .where(place_id: record.place_id).limit(1).to_a)
                end

                if records.any?
                    timezone = records.first.user.timezone
                end
            end

            # No need to lookup the timezone if we've already determined the user's
            # timezone from another record that has the same place_id. We also prevent
            # timezone lookups in development because Google charges for timezone lookups.
            unless timezone || Rails.env.development?
                begin
                    timezone = record.lookup_timezone
                rescue Timezone::Error::InvalidZone => err
                    Raven.capture_exception(err, {
                        extra: {
                            "#{klass.table_name.singularize}_id": record.id,
                            "user_id": record.user_id,
                            "place_id": record.place_id,
                            "lat": record.place_details['lat'],
                            "lng": record.place_details['lng']
                        }
                    })
                end
            end

            EditorTracking.transaction('UpdateUserTimezoneJob') do
                record.user.update!(timezone: timezone) if timezone
            end
        end
    end
end
