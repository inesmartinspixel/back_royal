class DailyMarketingUpdate < ApplicationJob

    include ScheduledJob

    queue_as :daily_marketing_update
    self.run_at_hour 0 # midnight eastern time

    def perform
        ActiveRecord::Base.with_statement_timeout(0) do
            ensure_refreshed
            LogToSlack.perform_later(Slack.marketing_notify_channel, "*Daily Smart.ly Update - #{yesterday_start.strftime('%B %d')}*", attachments)
        end
    end

    def attachments
        [
            {
                pretext: "_Except where indicated, all numbers include all external, consumer users. (i.e. all non-institutional, non-editor users with email addresses other than @pedago.com, @workaround.com, etc.)  Visitors who never login are assumed to be external consumer users and so are also included._",
                color: '#36A64F',
                fields: metrics.map { |metric|  { title: metric[0], value: metric[1], short: true }  },
                mrkdwn_in: ['pretext']
            }
        ]
    end

    def metrics
        [
            ["New visitors", new_visitor_count],
            ["Returning users with lesson activity", returning_users_in_lessons_count],
            ["Website Bounce rate", format_perc(bounce_rate)],
            ["Website conversion rate", format_perc(registration_rate)],
            ["New Visitor Registrations (FREEMBA)", new_visitor_registrations_yesterday_count],
            ["Registrations (FREEMBA)", total_yesterday_freemba_registrations_count],
            ["Registrations All-Time (FREEMBA)", total_freemba_registrations_count],
            ["% New visitors launched lesson", format_perc(started_lesson_rate)],
            ["% New visitors who registered and finished a lesson (Activation rate)", format_perc(activation_rate)],
            ["New institutional users", new_visitor_count(true)],
            ["Returning institutional users with lesson activity", returning_users_in_lessons_count(true)]
        ]
    end

    def ensure_refreshed
        unless @refreshed
            ActiveRecord::Base.connection.execute('refresh materialized view acquisitions')
        end
        @refreshed = true
        self
    end

    # for debugging
    def set_date(date)
        @yesterday_start = date.utc.beginning_of_day
        self
    end

    def yesterday_start
        unless defined? @yesterday_start
            @yesterday_start = Time.now.utc.beginning_of_day - 1.day
        end
        @yesterday_start
    end

    def yesterday
        yesterday_start..(yesterday_start+1.day-1.second)
    end

    def new_visitor_registrations_yesterday_count
        new_visitors
            .where("users.sign_up_code" => 'FREEMBA')
            .count
    end

    def total_yesterday_freemba_registrations_count
        User
            .where(sign_up_code: 'FREEMBA', created_at: yesterday)
            .count
    end

    def total_freemba_registrations_count
        User
            .where(sign_up_code: 'FREEMBA')
            .count
    end

    def new_visitor_count(institutional = false)
        @new_visitor_counts ||= {}
        @new_visitor_counts[institutional] ||= new_visitors(institutional).count
    end

    def new_visitors(institutional = false)

        query = Acquisition.left_outer_joins(:user_progress_record)
        if institutional
            query = query.joins(:user => :institutions)
                .where("institutions.id not in ('#{Institution.quantic.id}', '#{Institution.smartly.id}')")
        else
            query = query.left_outer_joins(:user => :institutions)
                .where("institutions.id is null or institutions.id in ('#{Institution.quantic.id}', '#{Institution.smartly.id}')")

        end

        query.where(
            acquired_at: yesterday
        )
    end

    def new_web_visitors
        new_visitors.where(acquired_on_cordova: false)
    end

    def format_perc(num)
        num.nan? ? 'N/A' : "#{(100*num).round}%"
    end

    def bounce_rate
        new_web_visitors.where(did_not_bounce: false).count.to_f / new_web_visitors.count
    end

    def registration_rate
        new_web_visitors.where.not("users.id": nil).count.to_f / new_web_visitors.count
    end

    def started_lesson_rate
        new_visitors.where("started_lesson_count > ?", 0).count.to_f/ new_visitor_count
    end

    def activation_rate
        new_visitors.where("completed_lesson_count > ?", 0).count.to_f/ new_visitor_count
    end

    def returning_users_in_lessons_count(institutional = false)
        returning_users_in_lessons(institutional).count
    end

    def returning_users_in_lessons(institutional = false)
       query = Report::LessonActivityByCalendarDateRecord
            .joins(:user)
            .joins("LEFT JOIN institutions_users ON users.id = institutions_users.user_id")
            .where({
                date: yesterday
            })
            .where("users.created_at < ? ", yesterday_start)

        if (institutional)
            query = query.where("institutions_users.institution_id is not null")
                .where("institutions_users.institution_id not in ('#{Institution.quantic.id}', '#{Institution.smartly.id}')")
        else
            query = query.where("institutions_users.institution_id is null or institutions_users.institution_id in ('#{Institution.quantic.id}', '#{Institution.smartly.id}')")
        end

        query
    end

end