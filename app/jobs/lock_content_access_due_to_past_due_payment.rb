# Locks the user's content access privileges if they're not in good standing.
class LockContentAccessDueToPastDuePayment < ApplicationJob

    queue_as :lock_content_access_due_to_past_due_payment

    def perform(cohort_application_id)
        EditorTracking.transaction('LockContentAccessDueToPastDuePayment') do
            cohort_application = CohortApplication.find(cohort_application_id)

            # ensure that we don't lock the user's content access privileges
            # if they're in good standing now
            lock_content_access = cohort_application.payment_past_due?

            # FIXME: it's very unlikely, but if in between the last line of code
            # and this line of code, a payment came in, then the user would end
            # up locked_due_to_past_due_payment=true incorrectly.
            cohort_application.update!(locked_due_to_past_due_payment: lock_content_access, payment_grace_period_end_at: nil)
        end
    end
end
