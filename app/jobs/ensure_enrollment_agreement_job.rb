class EnsureEnrollmentAgreementJob < ApplicationJob

    queue_as :ensure_enrollment_agreement

    def self.ensure_job(cohort_application_id)
        existing_job = Delayed::Job.where(queue: self.queue_name)
                    .where("handler ILIKE '\%arguments\%#{cohort_application_id}\%'")

        return existing_job if existing_job.present?

        self.perform_later(cohort_application_id)
    end


    def perform(cohort_application_id)
        cohort_application = CohortApplication.find_by_id(cohort_application_id)

        return unless cohort_application

        EditorTracking.transaction(self.class.name) do

            # This job should never have been queued unless this was true, but check all the same
            if cohort_application.need_to_generate_new_enrollment_agreement?

                # Track whether or not this was a follow_up_attempt
                follow_up_attempt = false
                if cohort_application.enrollment_agreement
                    follow_up_attempt = cohort_application.enrollment_agreement.metadata['cohort_application_id'] == cohort_application.id
                    cohort_application.enrollment_agreement.destroy
                end

                # Create the new enrollment agreement
                cohort_application.create_enrollment_agreement(follow_up_attempt)

            end
        end
    end

end