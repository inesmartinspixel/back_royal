class WriteRegularlyIncrementallyUpdatedTablesJob < ApplicationJob

    # This job refreshes views that are needed to support user-facing reports.  Views that
    # support only internal reporting are refreshed in RefreshMaterializedInternalReportsViews, which
    # is run less often to save resources on RDS boxes.

    include ScheduledJob

    queue_as :write_regularly_incrementally_updated_tables
    self.schedule_interval (ENV['REGULARLY_INCREMENTALLY_UPDATED_TABLES_MINUTES'].nil? ? 10.minutes : ENV['REGULARLY_INCREMENTALLY_UPDATED_TABLES_MINUTES'].to_i.minutes)

    # the are views related to user progress. They get updated by
    # a scheduled job
    def perform

        ActiveRecord::Base.with_statement_timeout(0) do

            Cloudwatch.put_timing_metric_data(metric_name: 'Report::UserLessonProgressRecord.write_new_records') do
                Report::UserLessonProgressRecord.write_new_records
            end
            Cloudwatch.put_timing_metric_data(metric_name: 'CareerProfile::SearchHelper.write_new_records') do
                CareerProfile::SearchHelper.write_new_records
            end

        end
    end

end