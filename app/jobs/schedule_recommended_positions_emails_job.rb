class ScheduleRecommendedPositionsEmailsJob < ApplicationJob
    include ScheduledJob

    queue_as :schedule_recommended_positions_emails

    # 4:15 AM Eastern Time
    self.run_at_hour 4
    self.run_at_minute 15

    def has_existing_job?(candidate_id)
        Delayed::Job.where(queue: "send_recommended_positions_email")
            .where("handler ILIKE '\%arguments\%#{candidate_id}\%'")
            .first
            .present?
    end

    def perform
        EditorTracking.transaction(self.class.name) do
            User.where(can_edit_career_profile: true, notify_candidate_positions_recommended: ['daily', 'bi_weekly'])
                .select(:id, :notify_candidate_positions_recommended).in_batches do |candidates|

                candidates.each do |candidate|
                    # If for some reason we get backed up a day, prevent scheduling duplicate jobs
                    # that would produce duplicate emails in customer.io
                    next if has_existing_job?(candidate.id)

                    if candidate.notify_candidate_positions_recommended == 'bi_weekly'
                        # Check what day of the week it is, Wednesday is 3.
                        # See https://apidock.com/ruby/Date/wday
                        #
                        # Check if we are in an even week of the year.
                        # See https://stackoverflow.com/a/12430779/1747491
                        #
                        # Then this conditional will trigger on every other Wednesday.
                        week_number_1_indexed = (Date.today.strftime("%U").to_i + 1)
                        if Date.today.wday == 3 && week_number_1_indexed.even? # decided to start on the week after New Year's, which is week 2 of 52
                            SendRecommendedPositionsEmailJob.perform_later(candidate.id)
                        end
                    elsif candidate.notify_candidate_positions_recommended == 'daily'
                        SendRecommendedPositionsEmailJob.perform_later(candidate.id)
                    else
                        raise "There is an error with the query"
                    end
                end
            end
        end
    end
end