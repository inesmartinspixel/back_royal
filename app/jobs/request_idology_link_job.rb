class RequestIdologyLinkJob < ApplicationJob

    queue_as :request_idology_link

    def perform(params)
        IdologyVerification.fetch_idology_capture_token(params)
    end
end