class SyncFromAirtableBatchJob < ApplicationJob

    include SyncFromAirtableBatchHelper::BatchJobHelper

    queue_as :sync_from_airtable_batch

    def perform(database_sync: true, decision_sync: true, bucket:)
        perform_airtable_to_database_sync(bucket) if database_sync
        perform_decision_sync(bucket) if decision_sync
    end

    def perform_airtable_to_database_sync(bucket)
        sync_view = 'Dev Usage Only - Sync'
        application_id_column = Airtable::Application.column_map["id"]
        airtable_modified_at_column = 'Dev - Last Modified At For Sync Logic'
        database_synced_at_column = 'Dev - Last Modified At When Synced'
        fields = [application_id_column, airtable_modified_at_column, database_synced_at_column] + Airtable::Application.from_airtable_column_map.values
        base_filter = nil
        orphaned_airtable_application_ids = []

        fetch_and_process_records(sync_view, fields, bucket, base_filter) do |airtable_application, database_applications_by_id|
            begin
                application_id = airtable_application[application_id_column]
                database_application = database_applications_by_id[application_id]

                # If we can't find a corresponding cohort application record in our database for a record
                # in Airtable, we add the application id to the orphaned_airtable_application_ids array,
                # which will get sent to Sentry so we can investigate further if needed.
                if database_application.nil?
                    orphaned_airtable_application_ids << application_id
                    next
                end

                EditorTracking.transaction(self.class.name) do
                    # Create an update_hash for the database using the data from Airtable for the
                    # columns in from_airtable_column_map
                    update_hash = {}
                    Airtable::Application.from_airtable_column_map.each do |database_key, airtable_key|
                        update_hash[database_key] = airtable_application[airtable_key]
                    end

                    # Update our database record
                    database_application.update(update_hash)
                end

                # Sync the airtable_modified_at_column to the database_synced_at_column to filter the
                # corresponding row out of the Airtable sync_view.
                # We need to retry here since we're doing a network request to Airtable, which will
                # definitely fail at some point. I decided to sync this value to Airtable right here
                # and now so we don't have to deal with converting and storing the date.
                airtable_application[database_synced_at_column] = airtable_application[airtable_modified_at_column]
                begin
                    retries ||= 0

                    # NOTE: This should be the last operation to ensure that we do not risk missing
                    # an update due to a failure after having updated the database_synced_at_column
                    # value in Airtable.
                    airtable_application.save
                rescue => e
                    if (retries += 1) < 3
                        retry
                    else
                        raise e
                    end
                end
            rescue => e
                Raven.capture_exception(e.message, {
                    extra: {
                        application_id: application_id
                    }
                })
                next
            end
        end

        self.log_orphaned_airtable_applications(orphaned_airtable_application_ids, 'database')
    end

    def perform_decision_sync(bucket)
        decidable_view = "Dev Usage Only - Decidable"
        decision_column = "Decision"
        application_id_column = Airtable::Application.column_map["id"]
        synced_decision_column = Airtable::Application.column_map["admissions_decision"]
        fields = [application_id_column, decision_column]
        base_filter = "{#{decision_column}} != {#{synced_decision_column}}"
        orphaned_airtable_application_ids = []

        fetch_and_process_records(decidable_view, fields, bucket, base_filter) do |airtable_application, database_applications_by_id|
            id = airtable_application[application_id_column]
            airtable_decision = airtable_application[decision_column]
            database_application = database_applications_by_id[id]

            # If we can't find a corresponding cohort application record in our database for a record
            # in Airtable, we add the application id to the orphaned_airtable_application_ids array,
            # which will get sent to Sentry so we can investigate further if needed.
            if database_application.nil?
                orphaned_airtable_application_ids << id
                next
            end

            program_type_config = Cohort::ProgramTypeConfig[database_application.program_type]

            # If the decision_column changes in Airtable then update the admissions_decision field in our database
            if database_application.admissions_decision != airtable_decision

                # Safeguard for a very unlikely edge-case where an application was created as a
                # syncable program type, then changed to a non-syncable program type, then decided on.
                if !program_type_config.supports_airtable_sync?
                    Raven.capture_exception("Trying to decide on an application that does not support airtable sync", {
                        extra: {
                            application_id: id,
                            airtable_decision: airtable_decision
                        }
                    })
                    next
                end

                # If the decision was previously set to something other than "TBC" and is now being set back to nil then
                # raise an exception and skip since actions could have already been taken in our system.
                if !database_application.admissions_decision.blank? &&
                    !database_application.admissions_decision.starts_with?("TBC") &&
                    airtable_decision.blank?
                    Raven.capture_exception("Can't change decision to nil unless it was TBC", {
                        extra: {
                            application_id: id
                        }
                    })
                    next
                end

                # We now allow non-MBA applications to be re-decided on after acceptance so that candidates in programs
                # like the biz cert can be invited to interview for the EMBA. Because of this business
                # logic change we opened up the "Decidable" view in Airtable to look at any non-MBA applications that already
                # have a decision. Let's go ahead and proactively check that this additional decision is "Invite to Interview"
                # "EMBA w/", "Reject and Convert to EMBA", or one of the NORMAL_REJECTIONS for application statuses that would
                # have previously been filtered out in the "Decidable" view so that a redecision that we didn't anticipate doesn't
                # cause unintended consequences.
                # See https://trello.com/c/k8yNye3f and https://trello.com/c/BsQClzLk.
                allowed_redecision = (['Invite to Interview', 'Accept w/ Career Network', 'Reject and Convert to EMBA'] + CohortApplication::NORMAL_REJECTIONS).include?(airtable_decision) ||
                    airtable_decision&.starts_with?('EMBA w/')
                if !database_application.admissions_decision.blank? &&
                    ['accepted', 'rejected', 'expelled', 'deferred'].include?(database_application.status) &&
                    !allowed_redecision
                    Raven.capture_exception('Cannot process redecision for application', {
                        extra: {
                            application_id: id,
                            admissions_decision: database_application.admissions_decision,
                            redecision: airtable_decision
                        }
                    })
                    next
                end

                # See https://trello.com/c/BsQClzLk.
                if airtable_decision == 'Reject and Convert to EMBA' && !program_type_config.supports_reject_with_conversion_to_emba?
                    Raven.capture_exception("Can't change decision to 'Reject and Convert to EMBA' unless it supports_reject_with_conversion_to_emba?", {
                        extra: {
                            application_id: id
                        }
                    })
                    next
                end

                # Capture an exception if the decision is 'Accept w/ Career Network' and this decision
                #  was made for a program_type that handles career network access based on some other
                #  criteria other than the Airtable decision.
                if airtable_decision == "Accept w/ Career Network" && !program_type_config.supports_career_network_access_on_airtable_decision?
                    Raven.capture_exception("Can't change decision to 'Accept w/ Career Network' unless it supports_career_network_access_on_airtable_decision?", {
                        extra: {
                            application_id: id
                        }
                    })
                    next
                end

                # If a change is detected and the Airtable decision is manual_admin_decision then let Sentry know
                # and skip the update because that situation would mean that someone is trying to use manual_admin_decision
                # on the Airtable side, and that should never happen.
                if airtable_decision == "manual_admin_decision"
                    Raven.capture_exception("Detected manual_admin_decision being set in Airtable", {
                        extra: {
                            application_id: id
                        }
                    })
                    next
                end

                # If we detect that a non-MBA application is given the 'Pre-Accept to MBA' decision then something isn't right.
                # Note that we do allow this scenario with other program types, but a change from something else to MBA
                # doesn't make sense.
                if airtable_decision == "Pre-Accept to MBA" && database_application.program_type != 'mba'
                    Raven.capture_exception("Can't set decision for non-MBA application to 'Pre-Accept to MBA'", {
                        extra: {
                            application_id: id
                        }
                    })
                    next
                end

                # It wouldn't make sense to allow a non-pending application to be waitlisted, so let's
                # proactively detect this to avoid an event accidentally being sent if this situation occurs.
                if airtable_decision == "Waitlist" && database_application.status != 'pending'
                    Raven.capture_exception("Can't set decision for non-pending application to 'Waitlist'", {
                        extra: {
                            application_id: id
                        }
                    })
                    next
                end

                # We shouldn't allow 'Invite to Interview' after the 'Reject and Convert to EMBA'
                # decision has been rendered, since the interview was done on the former
                # application (the biz cert application) that was rejected and converted.
                # See discussion at https://trello.com/c/x8xnIMCq
                if airtable_decision == 'Invite to Interview' && database_application.admissions_decision == 'Reject and Convert to EMBA'
                    Raven.capture_exception("Can't Invite to Interview an application that was rejected and converted", {
                        extra: {
                            application_id: id
                        }
                    })
                    next
                end

                EditorTracking.transaction(self.class.name) do
                    database_application.update(admissions_decision: airtable_decision)
                end
            end
        end

        self.log_orphaned_airtable_applications(orphaned_airtable_application_ids, 'decision')
    end

    def log_orphaned_airtable_applications(orphaned_airtable_application_ids, sync_type)
        # We often times get orphaned airtable applications on staging because of Airtable
        # archives, so we don't bother alerting in Sentry if we see that we're on staging.
        if !self.is_staging? && orphaned_airtable_application_ids.any?
            Raven.capture_exception("#{self.class.name}: Could not find corresponding cohort application in our database for record(s) in Airtable during #{sync_type} sync", {
                extra: {
                    orphaned_airtable_application_ids: orphaned_airtable_application_ids
                }
            })
        end
    end

    def is_staging?
        ENV['APP_ENV_NAME']&.match?(/staging/i)
    end
end
