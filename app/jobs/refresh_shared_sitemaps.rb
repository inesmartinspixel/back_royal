class RefreshSharedSitemaps < ApplicationJob

    require 'sitemap_generator'
    include ScheduledJob

    queue_as :refresh_shared_sitemaps
    self.schedule_interval (ENV['SITEMAP_REFRESH_MINUTES'].nil? ? nil : ENV['SITEMAP_REFRESH_MINUTES'].to_i.minutes)

    def perform
        # We used to generate sitemaps here, but that happens in sitemap_controller now
        SitemapHelper.all_sitemap_urls.each do |url|
            SitemapGenerator::Sitemap.ping_search_engines url
        end
    end

end