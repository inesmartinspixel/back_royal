class SetCanEditCareerProfileAfterAcceptanceJob < ApplicationJob
  queue_as :set_can_edit_career_profile_after_acceptance

  def perform(user_id)
    # Automatically retries in the event of ActiveRecord::PreparedStatementCacheExpired
    EditorTracking.transaction("SetCanEditCareerProfileAfterAcceptanceJob") do
      # Ensure the application is still accepted
      user = User.joins(:cohort_applications).where("cohort_applications.status = 'accepted' and users.id = ?", user_id).first

      if user.nil?
          return
      else
          user.can_edit_career_profile = true
          user.save!
      end
    end
  end
end
