class ImportAndPruneRedshiftEventsJob < ApplicationJob
    attr_reader :timeout

    include ScheduledJob

    queue_as :import_and_prune_redshift_events

    # We run this every minute, but 9 times out of 10, there is not a new
    # file in s3, so we don't try to dump aything.  But it means that events
    # make it into redshift sooner.
    self.schedule_interval 1.minutes

    def perform(timeout = nil, bucket_override = nil, prefix_override = nil)
        @timeout = timeout || 0.9*Delayed::Worker.max_run_time
        @bucket_override = bucket_override
        @prefix_override = prefix_override
        manifest, manifest_key = copy_from_s3_to_redshift

        delete_s3_files(manifest, manifest_key) if manifest

        delete_old_events
    end

    def build_manifest
        manifest_contents = {entries: []}

        objects = bucket.objects.each do |obj|

            # Try not to consume events that might not have committed yet to the application store.  If
            # we do, they will be filtered out below when we filter out events that are not in postgres.
            # We could still have this issue if a transaction is open for more than 5 minutes, but
            # in that case EnsureEventsCopiedToRedshiftJob will pick them up later.
            next unless obj.last_modified < Time.now - 5.minutes

            # match objects that start with a year and date so we don't get the
            # manifests that firehose creates
            if obj.key.match?(/^#{prefix}\d\d\d\d\/\d\d\/\d\d/)
                manifest_contents[:entries] << {
                    url: "s3://#{bucket.name}/#{obj.key}",
                    mandatory: false, # if someone else has deleted this file, they must have copied it to redshift, so we don't have to blow up
                    key: obj.key # aws does not use this, but we do in the delete below
                }
            end
        end

        return [nil, nil] if manifest_contents[:entries].empty?

        key = "#{prefix}ImportAndPruneRedshiftEventsJob/manifests/#{Time.now.strftime('%Y-%m-%d_%H-%M-%S-%12N')}"
        bucket.object(key).put(body: JSON.pretty_generate(manifest_contents))

        [manifest_contents, key]
    end

    def copy_from_s3_to_redshift
        start = Time.now
        if timeout
            orig_timeout = RedshiftEvent.connection.execute('show statement_timeout').to_a[0]['statement_timeout']
            RedshiftEvent.connection.execute("set statement_timeout to #{(1000*timeout).to_i}").to_a
        end

        manifest, manifest_key = build_manifest
        return [nil, nil] if manifest.nil?

        temp_table_name = "events_temp_#{Time.now.strftime('%Y%m%d_%s')}"

        begin

            # Initial population of unfiltered events for load must be committed before attempting to filter out any uncommitted events,
            # since that requires a DBLink from RDS to poll the temp table for any absent event IDs. It's conceivable that this could leave
            # a temporary table sitting around if this code were to abort prior to running the `ensure` block below. For this reason, we've made
            # the table names human readable for manual cleanup if this were to ever occur. We cannot use `TEMPORARY` keyword on table creation
            # since the table would not persist across sessions.

            RedshiftEvent.connection.execute("
                -- Create a temp table to hold the batch of new events
                CREATE TABLE #{temp_table_name} (LIKE events);
            ")


            # We've attached expected Roles on the cluster so it can communicate from its Staging account to the
            # Development account's Kinesis stream's bucket, that developers can use for credentials in this instance.
            # see also - https://docs.aws.amazon.com/redshift/latest/dg/copy-parameters-authorization.html
            # see also - `config/initializers/aws.rb`
            credentials_info = "IAM_ROLE '#{ENV['AWS_ROLE_ARN_REDSHIFT']}'"


            # Perform the COPY into the temp table
            RedshiftEvent.connection.execute("
                -- Copy the new events from s3 into the temp table
                COPY #{temp_table_name} from 's3://#{bucket.name}/#{manifest_key}'
                #{credentials_info}
                MANIFEST
                FORMAT AS JSON 'auto'
                TIMEFORMAT 'YYYY-MM-DD HH:MI:SS'
                MAXERROR 0
                COMPUPDATE OFF
                STATUPDATE OFF
                GZIP;
            ")

            # Determine what what events have yet to be committed to Postgres RDS and therefore should be pruned in load to Redshift
            ids_to_prune = uncommitted_event_ids(temp_table_name)
            if ids_to_prune.any?

                # We don't want to stop this process, but we do want to understand when we encounter aberrations
                if ids_to_prune.length > 1000
                    Raven.capture_message("Encountered a large batch of uncommitted ids!",
                        extra: {
                            uncommitted_events_size: ids_to_prune.length
                        }
                    )
                end

                RedshiftEvent.connection.execute("
                    -- Delete any events missing from application DB
                    DELETE FROM #{temp_table_name} WHERE id IN (#{ ids_to_prune.map { |id| "'#{id}'" }.join(',') })
                ")
            end

            # These inserts all affect data integrity and should commit or roll back together via a transaction
            RedshiftEvent.transaction do

                RedshiftEvent.connection.execute("
                    -- Copy events from the temp table into the live table,
                    -- removing duplicates.

                    -- In most cases, if duplicates exist,
                    -- they will be within the new batch, as firehose sometimes dumps
                    -- the same event twice into s3.  In that case, it is the DISTINCT
                    -- that prevents duplication.

                    -- It is at least possible, however, that firehose could write the same
                    -- event into two different files.  For that reason
                    -- we also do the LEFT JOIN to filter out events that have already been
                    -- written.  We go for any event that is a week before the most recent event
                    -- so that even if we're somehow backed up and writing events that are a
                    -- few days old, we have the same protections.

                    INSERT INTO events (
                        WITH recent_ids AS (
                            SELECT distinct id
                            FROM events
                            WHERE created_at > ((select max(created_at) from events) - interval '1 week')
                        ),
                        distinct_temp_events AS (
                            SELECT *, ROW_NUMBER() OVER (PARTITION BY id ORDER BY id) AS id_ranked
                            FROM #{temp_table_name}
                        )
                        SELECT #{RedshiftEvent.column_names.join(',')}
                            FROM distinct_temp_events
                        LEFT JOIN recent_ids USING (id)
                        WHERE distinct_temp_events.id_ranked = 1
                            AND recent_ids.id IS NULL
                        ORDER BY distinct_temp_events.created_at ASC
                    );

                    -- The distinct here will not always guarantee that
                    -- we don't get dupes, but it will prevent most of them
                    INSERT INTO user_aliases (
                        select
                            user_id
                        from #{temp_table_name}
                        where event_type='anonymous_user:login'
                        order by user_id
                    );

                    -- see comment near user_aliases above about dupes
                    INSERT INTO user_utm_params (
                        select
                            distinct user_id
                            , created_at
                            , utm_campaign
                            , utm_content
                            , utm_medium
                            , utm_source
                        from #{temp_table_name}
                        where event_type='user:set_utm_params'
                        order by created_at
                    );
                ")

            end

            # I guess this is unnecessary as long as MAXERROR is hard-coded to 0, since
            # you could never get here if you had an stl_error
            log_stl_errors(start)

        rescue Exception => err
            log_stl_errors(start)
            raise err, RedshiftEvent.sanitized_error_message(err), err.backtrace

        ensure
            # Ensure the temporary table is always explicitly dropped
            RedshiftEvent.connection.execute("
                -- Drop the temp table
                DROP TABLE IF EXISTS #{temp_table_name};
            ")
        end

        return [manifest, manifest_key]

    ensure
        if timeout
            RedshiftEvent.connection.execute("set statement_timeout to #{orig_timeout}")
        end
    end

    def uncommitted_event_ids(temp_table_name)
        DbLinker.setup_red_royal_dblink

        min_created_at, max_created_at = RedshiftEvent.connection.execute("
            -- Determine min and max created_at for current batch
            SELECT min(created_at), max(created_at) FROM #{temp_table_name}
        ").to_a[0].values

        ActiveRecord::Base.with_statement_timeout(0) do
            Event.connection.execute("
                -- Find any events being loaded that are actually uncommitted to application DB
                WITH current_batch_event_ids AS (
                    SELECT distinct id
                    FROM events
                    WHERE created_at BETWEEN '#{min_created_at.utc.strftime('%Y-%m-%d %H:%M:%S.%N')}' and '#{max_created_at.utc.strftime('%Y-%m-%d %H:%M:%S.%N')}'
                )
                SELECT
                    firehose_events.id
                FROM
                    dblink('red_royal', $REDSHIFT$
                        SELECT id, event_type from #{temp_table_name}
                    $REDSHIFT$) AS firehose_events (
                        id uuid
                        , event_type text
                    )
                LEFT OUTER JOIN current_batch_event_ids USING (id)
                WHERE current_batch_event_ids.id IS NULL
                    AND firehose_events.event_type NOT IN (#{Event::CLIENT_SIDE_EVENTS_NOT_PERSISTED_TO_RDS.map { |e| ActiveRecord::Base.connection.quote(e) }.join(',')})
            ").map { |row| row['id'] }
        end
    end

    def delete_s3_files(manifest, manifest_key)
        manifest[:entries].map { |e| e[:key] }.each do |key|
            bucket.object(key).delete
        end

        bucket.object(manifest_key).delete
    end

    def log_stl_errors(start)
        # subtract a second to allow for differences in the ruby clock
        # and the clock on the redshift server
        formatted_startime = (start - 1.second).utc.strftime('%Y-%m-%d %H:%M:%S.%N')
        RedshiftEvent.connection.execute("SELECT * FROM stl_load_errors WHERE starttime > '#{formatted_startime}' AND filename LIKE 'temp'").each do |row|

            log JSON.pretty_generate(row) # STDOUT

            Raven.capture_exception(RedshiftLoadError.new("Redshift load error"), {
                extra: {
                    load_error_row: row
                }
            })
        end
    end

    def log(msg)
        return if Rails.env.test?

        if !@logger
            if Rails.env.development?
                @logger = Logger.new(STDOUT)
            else
                # in production, log to log/import_and_prune_redshift_events_job
                path = File.join(Rails.root, 'log', 'import_and_prune_redshift_events_job')
                FileUtils.mkdir_p(path)
                @logger = Rails.env.development? ? Logger.new(STDOUT) : Logger.new(File.join(path, start_time.to_s))
            end
        end

        @logger.debug(msg)
    end

    def firehose_destination_info
        unless defined? @firehose_destination_info
            firehose = Aws::Firehose::Client.new
            destination_description = firehose.describe_delivery_stream({delivery_stream_name: Event::PutEventsInFirehose.delivery_stream_name})
                                            .delivery_stream_description
                                            .destinations[0]
                                            .s3_destination_description

            @firehose_destination_info = {
                bucket_name: destination_description.bucket_arn.split(':').last,
                prefix: destination_description.prefix
            }
        end
        @firehose_destination_info
    end

    def bucket
        unless defined? @bucket
            bucket_name = @bucket_override || firehose_destination_info[:bucket_name]
            options = {}
            if timeout
                options[:http_read_timeout] = timeout
            end
            s3 = Aws::S3::Resource.new(options)
            @bucket = s3.bucket(bucket_name)
            raise "No bucket for #{bucket_name.inspect}" unless @bucket
        end
        @bucket
    end

    def prefix
        unless defined? @prefix
            @prefix = @prefix_override || firehose_destination_info[:prefix]

            # this will only work correctly if the prefix in firehose ends with '/',
            # but that's more convenient anyway
            raise "invalid prefix" unless @prefix.ends_with?('/')
        end
        @prefix
    end

    def delete_old_events
        # we used to delete postgres events here, but now
        # we do that in the DeleteOldEventsJob
        delete_old_click_events_from_redshift
        delete_old_error_events_from_redshift
    end

    def delete_old_click_events_from_redshift
        if days_to_keep_click_events_in_redshift && days_to_keep_click_events_in_redshift >= 0
            RedshiftEvent.where(event_type: 'click')
                .where("created_at < ?", Time.now - days_to_keep_click_events_in_redshift.days)
                .delete_all
        end
    end

    def delete_old_error_events_from_redshift
        if days_to_keep_error_events_in_redshift && days_to_keep_error_events_in_redshift >= 0
            RedshiftEvent.where(event_type: 'error')
                .where("created_at < ?", Time.now - days_to_keep_error_events_in_redshift.days)
                .delete_all
        end
    end

    def days_to_keep_click_events_in_redshift
        unless defined? @days_to_keep_click_events_in_redshift
            @days_to_keep_click_events_in_redshift = ENV['DAYS_TO_KEEP_CLICK_EVENTS_IN_REDSHIFT'].nil? ? nil : ENV['DAYS_TO_KEEP_CLICK_EVENTS_IN_REDSHIFT'].to_i
        end
        @days_to_keep_click_events_in_redshift
    end

    def days_to_keep_error_events_in_redshift
        unless defined? @days_to_keep_error_events_in_redshift
            @days_to_keep_error_events_in_redshift = ENV['DAYS_TO_KEEP_ERROR_EVENTS_IN_REDSHIFT'].nil? ? nil : ENV['DAYS_TO_KEEP_ERROR_EVENTS_IN_REDSHIFT'].to_i
        end
        @days_to_keep_error_events_in_redshift
    end

end