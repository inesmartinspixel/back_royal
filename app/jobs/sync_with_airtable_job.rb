class SyncWithAirtableJob < ApplicationJob
    queue_as :sync_with_airtable

    # Actions
    CREATE = "create"
    UPDATE = "update"
    DESTROY = "destroy"

    # Mimic the strategy that IdentifyUserJob uses for queueing up bulk operations
    # after the organic ones
    DEFAULT_PRIORITY = 3
    MASS_SYNC_PRIORITY = SyncWithAirtableJob::DEFAULT_PRIORITY + 1

    def self.generate_fix_decision_action(new_decision)
        new_decision.nil? ? "fix_decision" : "fix_decision::#{new_decision}"
    end

    # Removes any `<airtable:mention></airtable:mention>` tags and associated @ symbols from
    # the given text, leaving the text in the @mention tag, and returns the sanitized text value.
    #
    #   E.g.
    #       "<airtable:mention id=\"men2xck840Gaz7ePl\">@Johnny Appleseed</airtable:mention> foo bar baz."
    #       => "Johnny Appleseed foo bar baz."
    #
    # See https://trello.com/c/8L8XSKhY.
    def self.sanitize_airtable_mentions(text)
        text&.gsub(/<airtable:mention id=\"[\w]+">\@|<\/airtable:mention>/, '')
    end

    # We contemplated adding this logic to application_job but we decided to wait and see
    # if we need debouncing again before spending the time to generalize this.
    # See https://trello.com/c/FGFADWfb
    def self.perform_later_with_debounce(application_id, action, priority = SyncWithAirtableJob::DEFAULT_PRIORITY)
        pending_job = Delayed::Job.where(queue: "sync_with_airtable")
            .where(locked_at: nil)
            .where(failed_at: nil)
            .where("handler ILIKE '\%arguments\%#{application_id}\%#{action}\%'")
            .first

        # This will generally mean that we have only one job at a time for any given application, but there
        # is no guarantee of that when things are running in parallel. It should be fine though, creating
        # multiple jobs every now and then is not a big deal. The job itself has special logic to handle the
        # dangers of concurrency (see cohort_application#check_if_json_changed).
        if pending_job
            # If a job already exists that is not locked or failed and has the same arguments then
            # just update that job's run_at time
            pending_job.update(run_at: Time.now + 1.minute, priority: SyncWithAirtableJob::DEFAULT_PRIORITY)
        else
            # Otherwise schedule a new job for the future (giving a window during which subsequent jobs
            # will trigger the above if statement)
            self.set(wait_until: Time.now + 1.minute, priority: priority).perform_later(application_id, action)
        end
    end

    def perform(application_id, action)
        # Hacky data throttling. See https://trello.com/c/SWJvo8pe
        delayed_job_throttle_time_ms = 250
        sleep delayed_job_throttle_time_ms.to_f / 1000.to_f

        if action == SyncWithAirtableJob::DESTROY
            # The main record is destroyed by this point so get the airtable_record_id from the most recent version
            cohort_application_version = CohortApplication::Version.where(id: application_id).order(version_created_at: :desc).first

            # If the associated user was destroyed, we enqueued a DeleteAssociatedUserDataJob that deleted that user's
            # cohort_application_versions and enqueued a DeleteFromAirtableJob.
            return if cohort_application_version.nil?

            Airtable::Application.destroy(cohort_application_version)
        elsif action == SyncWithAirtableJob::CREATE
            cohort_application = CohortApplication.find_by_id(application_id)
            return if cohort_application.nil?

            # I think the find_or_create here (I say think, because I'm writing this two years later)
            # is so that if a failure happened after the Airtable creation, which would cause a
            # delayed_job retry, we wouldn't create a duplicate row in Airtable.
            airtable_record = Airtable::Application.find_or_create(cohort_application, self.class.name)

            maybe_transfer_prior_airtable_fields(cohort_application, airtable_record)
            cohort_application.airtable_base_key = 'AIRTABLE_BASE_KEY_LIVE'

            # Avoid including Airtable API requests in the EditorTracking transaction.
            # Otherwise, we may end up with a long-running transaction if a request takes a long time.
            EditorTracking.transaction(self.class.name) do
                cohort_application.save!
            end
        elsif action == SyncWithAirtableJob::UPDATE
            cohort_application = CohortApplication.find_by_id(application_id)
            return if cohort_application.nil? || cohort_application.is_archived_in_airtable?

            airtable_record = nil
            result = cohort_application.check_if_json_changed do
                airtable_record = Airtable::Application.update(cohort_application)
            end

            # If the application has not been created in Airtable yet (and has not been deleted in our database),
            # or if the application changed in the database since we originally loaded it, then we should
            # raise an error to make this job be picked up later. See https://trello.com/c/tlnzhiv7
            if (airtable_record.nil? && result != :destroyed) || result == :changed
                raise Delayed::ForceRetry.new("Need to retry Airtable update later", {
                    max_attempts: 3
                })
            end
        elsif action.start_with?("fix_decision")
            new_decision = action.split("::")[1]
            cohort_application = CohortApplication.find_by_id(application_id)
            return if cohort_application.nil?
            Airtable::Application.fix_decision(cohort_application, new_decision)
        end
    end

    private def maybe_transfer_prior_airtable_fields(current_application, current_airtable_record)

        # There are several circumstances where we want to copy over data,
        # in Airtable only, from a prior application. On a creation sync, let's
        # check for these situations, and just copy over the data via Airtable API
        # calls. There is some Airtable data that we sync to our database
        # for use in Metabase, but we decided not to bother trying to sync a lot of the
        # information that needs to be copied over in Airtable only if isn't necessary
        # for any db metrics.
        # See https://trello.com/c/YkeLkKdh

        # List of fields that we don't care to sync to our db, but that we need to copy over to new
        # applications sometimes. This is reliant on them not changing the column names, but it prevents
        # us from having to add schema and store information we don't need.
        a2a_rubric_fields = [
            'Interview Date',
            'Motivation Score',
            'Motivation Score Reason',
            'Contribution Score',
            'Contribution Score Reason',
            'Insightfulness Score',
            'Insightfulness Score Reason',
            'Interviewer-Director Notes',
            'High Potential Applicant [emba/mba]',
            'Tagger-Interviewer Notes'
        ]

        # List of fields that we sync to our database
        # Note that we decided not to transfer some of these -- see https://trello.com/c/nAPzfACa
        db_synced_rubric_fields_map = Airtable::Application.from_airtable_column_map.reject do |k, v|
            [
                "Round 1 Tag",
                "Round 1 Reason - MBA Invite",
                "Round 1 Reason - MBA Reject",
                "Round 1 Reason - EMBA Invite",
                "Round 1 Reason - EMBA Reject",
                "Final Decision",
                "Final Decision Reason"
            ].include?(v)
        end

        # List of fields that we do sync to our database
        # Note that we decided not to transfer some of these -- see https://trello.com/c/nAPzfACa
        db_synced_rubric_fields = db_synced_rubric_fields_map.values

        all_rubric_fields = a2a_rubric_fields + db_synced_rubric_fields
        prior_application_to_transfer_from = nil
        fields_to_transfer = []
        rubric_inherited = false

        # Only try to transfer if there has actually been a prior application
        if current_application.user.cohort_applications.size > 1
            user = current_application.user
            career_profile = user.career_profile

            # If an application to the mba or emba within the last six months
            # had an interview conducted, and this application being created is
            # to the mba or emba, then we'll transfer that prior application's
            # rubric and interview fields to this application.
            # See https://trello.com/c/SUy5tyc4
            # See https://trello.com/c/gzSzlZt6
            prior_degree_application_with_interview = user.cohort_applications
                .joins(:cohort)
                .where(rubric_interview: 'Conducted',
                    rubric_inherited: false,
                    cohorts: { program_type: ['mba', 'emba'] })
                .where('applied_at < ?', current_application.applied_at)
                .where('applied_at > ?', Time.now - 6.months)
                .first
            should_transfer_degree_interview = current_application.mba_or_emba? &&
                prior_degree_application_with_interview.present?

            # If we see the exact conditions of a redecided biz cert application then
            # we'll transfer the biz cert's rubric and interview fields.
            # See https://trello.com/c/YkeLkKdh
            #
            # Note: We can't check for the 'Reject and Convert to EMBA' decision because
            # that gets set in an update that comes after the initial application create
            # in decision_concern.rb. But I think this criteria is specific enough.
            #
            # Note: This is assuming they are redeciding on an accepted application, which
            # means the user couldn't have applied to anything after, which means the second-to-last
            # application is the one we want to check since the new emba application was just made.
            second_to_last_application = user.cohort_applications.second
            is_redecided_biz_cert_case = current_application.program_type == 'emba' &&
                second_to_last_application.present? &&
                second_to_last_application.status == 'rejected' &&
                second_to_last_application.published_cohort_or_cohort.program_type == 'the_business_certificate' &&
                second_to_last_application.rubric_interview == 'Conducted'

            if should_transfer_degree_interview || is_redecided_biz_cert_case
                fields_to_transfer += all_rubric_fields
                rubric_inherited = true

                if is_redecided_biz_cert_case
                    # We only transfer these fields in the biz cert redecision case
                    # See https://trello.com/c/nAPzfACa
                    fields_to_transfer += [
                        'Rubric - Employment',
                        'Rubric - Role / Title Score',
                        'Rubric Company Score Override',
                        'Rubric - Years of Experience Override'
                    ]
                end
            end

            if career_profile.peer_recommendations.any?
                # If there are any peer_recommendations requested on the career_profile
                # then try to copy over the Airtable field. Note that our db is just telling us if any
                # were requested; they are actually filled out in Typeform and sent to Airtable
                # via Zapier.
                # See https://trello.com/c/8q7SSnGC
                fields_to_transfer.push('Peer Recommendations')
            end

            if should_transfer_degree_interview
                prior_application_to_transfer_from = prior_degree_application_with_interview
            elsif is_redecided_biz_cert_case
                prior_application_to_transfer_from = second_to_last_application
            elsif career_profile.peer_recommendations.any?
                prior_application_to_transfer_from = user.cohort_applications.second
            end
        end

        airtable_fields_transferred = false
        if prior_application_to_transfer_from.present? && fields_to_transfer.size > 0
            airtable_fields_transferred = transfer_airtable_fields(current_application, prior_application_to_transfer_from, current_airtable_record, fields_to_transfer, rubric_inherited)
        end

        return false if !airtable_fields_transferred

        if rubric_inherited
            db_synced_rubric_fields_map.keys.each do |field|
                current_application[field] = prior_application_to_transfer_from[field]
            end
            current_application.rubric_inherited = true
        end
    end

    # Separate method for mocking purposes
    private def transfer_airtable_fields(current_application, prior_application, current_airtable_record, fields, rubric_inherited)
        # The prior_application may not have an airtable_base_key if its record in Airtable
        # hasn't been created yet. We don't expect this to organically happen in the wild,
        # but we did see this happen while manually testing cohort application deferrals.
        return false if prior_application.airtable_base_key.nil?

        # Create an ad-hoc API to lookup the prior application's Airtable record
        # since it could have been archived

        # Prevent annoying failures on staging, since we don't actually makes clones
        # of the archive bases and the cloned state isn't exactly in line with prod.
        # So check for either there being no ENV variable for the particular Airtable base,
        # or the application record not being found.
        prior_airtable_record = nil
        begin
            prior_airtable_record = Airtable::Application.klass_for_base_key(prior_application.airtable_base_key)
                .find_by_id_with_fallback(prior_application)
        rescue RuntimeError => e
            return false
        end
        return false if prior_airtable_record.nil?

        fields.each do |field|
            # There's an edge case where we try to grab a value from the old version
            # of the Peer Recommendations Airtable column, which stored references to a
            # Peer Recommendations table. That value will be an array of record ids, which
            # will then error when syncing to the new Peer Recommendations string column
            # for the new row.
            # See https://sentry.io/organizations/pedago/issues/1246137517
            prior_value = prior_airtable_record[field]

            next if field == 'Peer Recommendations' && prior_value&.is_a?(Array)

            # We need to remove any Airtable user @mentions from String values. Otherwise, the job fails
            # with a 422 INVALID_MENTION_ID error even though the @mentioned Airtable user may exist.
            # See https://trello.com/c/8L8XSKhY.
            if prior_value.is_a?(String)
                prior_value = self.class.sanitize_airtable_mentions(prior_value)
            end

            current_airtable_record[field] = prior_value
        end

        # They would like to know this in both Airtable and in Metabase, so go ahead and sync it
        # after we do the transfer
        if rubric_inherited
            current_airtable_record['Rubric Inherited?'] = true
        end

        # I initially thought this could create an edge-case situation where the create sync
        # occured earlier, then another delayed_job box processes an update for this application,
        # then we do this save and potentially overwrite with stale information. But, after some
        # digging into the Airtable API and the Airrecord gem I discovered that only changed
        # fields on the Airrecord object will be sent to Airtable via a PATCH request, and since we are only
        # changing fields that are in Airtable we should have no chance of a conflict with this save.
        # See https://github.com/sirupsen/airrecord/blob/master/lib/airrecord/table.rb#L148
        # See https://airtable.com/app9E9bVteZrHfT5J/api/docs#curl/table:applications:update
        current_airtable_record.save
    end
end