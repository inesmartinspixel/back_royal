require "uri"
require "net/http"
require "httparty"

class LinkedinController < ApplicationController

    STATE_COOKIE_KEY = :linkedin_oauth_state

    def auth
        # The state value is a random UUID that will be stored in a cookie and
        # matched against the value returned from LinkedIn to detect CSRF attacks. Store
        # an array of them to handle situations like where the endpoint is hit multiple times
        # in quick succession.
        state = SecureRandom.uuid
        cookies.encrypted[LinkedinController::STATE_COOKIE_KEY] = {
            value: (cookies.encrypted[LinkedinController::STATE_COOKIE_KEY] || []).push(state),
            expires: 1.week.from_now
        }

        # Send user to LinkedIn to authenticate, which will return to our redirect_uri with an authorization code
        # See https://docs.microsoft.com/en-us/linkedin/shared/authentication/authorization-code-flow?context=linkedin/consumer/context#step-2-request-an-authorization-code
        params = {
            'response_type' => 'code',
            'client_id' => ENV['OAUTH_LINKEDIN_KEY'],
            'redirect_uri' => redirect_uri,
            'state' => state,
            'scope' => 'r_liteprofile'
        }

        redirect_to "https://www.linkedin.com/oauth/v2/authorization?#{params.to_query}"
    end

    def popup_callback

        # After authorization LinkedIn will hit this endpoint (our redirect_uri callback) with an authorization code and state variable, or error and description
        # See https://docs.microsoft.com/en-us/linkedin/shared/authentication/authorization-code-flow#application-is-rejected
        code = params[:code]
        state_from_linked_in = params[:state]

        # If LinkedIn sent us an error go ahead and let the client know
        @error = params[:error]
        @error_description = params[:error_description]
        if @error.present?
            render 'popup_callback' and return
        end

        # Check for CSRF attack
        # See last paragraph in https://docs.microsoft.com/en-us/linkedin/shared/authentication/authorization-code-flow#your-application-is-approved
        # Grab the state variable that we generated and stored in the auth endpoint step
        states_in_cookie = cookies.encrypted[LinkedinController::STATE_COOKIE_KEY]
        if !states_in_cookie&.include?(state_from_linked_in)
            Raven.capture_exception('Possible CSRF attack detected',
                extra: {
                    state_from_linked_in: state_from_linked_in,
                    states_in_cookie: states_in_cookie,
                    code: code
                }
            )
            # FIXME: Re-enable after we have figured out why our session cookie state value is sometimes being lost
            # @error = 'possible_csrf_attack'
            # @error_description = 'Possible CSRF attack detected'
            # render 'popup_callback' and return
        end

        # Exchange authorization code for an access token
        # See https://docs.microsoft.com/en-us/linkedin/shared/authentication/authorization-code-flow#step-3-exchange-authorization-code-for-an-access-token
        access_token_response = HTTParty.post(
            'https://www.linkedin.com/oauth/v2/accessToken',
            body: {
                grant_type: 'authorization_code',
                code: code,
                redirect_uri: redirect_uri,
                client_id: ENV['OAUTH_LINKEDIN_KEY'],
                client_secret: ENV['OAUTH_LINKEDIN_SECRET']
            }
        )

        if !access_token_response.ok?
            @error_description = access_token_response.body
            render 'popup_callback' and return
        end

        # Use access token to get the authenticated user's LinkedIn profile
        # See https://docs.microsoft.com/en-us/linkedin/shared/authentication/authorization-code-flow#step-4-make-authenticated-requests
        # See https://docs.microsoft.com/en-us/linkedin/shared/integrations/people/profile-api?context=linkedin/consumer/context#retrieve-authenticated-members-profile
        access_token = JSON.parse(access_token_response)['access_token']
        profile_api_response = HTTParty.get('https://api.linkedin.com/v2/me?projection=(id,profilePicture(displayImage~:playableStreams))', {
            headers: {
                'Host' => 'api.linkedin.com',
                'Connection' => 'Keep-Alive',
                'Authorization' => "Bearer #{access_token}"
            }
        })

        if profile_api_response.ok?
            @profile_api_response_body = profile_api_response.body
        else
            @error_description = profile_api_response.body
        end

        # Render view that will give information to the window that opened the initial popup
        render 'popup_callback'
    end

    private
    def redirect_uri
        # Note: using our custom request_protocol logic from application_controller.rb
        "#{request_protocol}#{request.host_with_port}/linkedin/popup_callback"
    end
end
