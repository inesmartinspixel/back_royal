class HelpScoutProxyController < ApplicationController
    skip_before_action :verify_authenticity_token
    def proxy
        require 'net/http'
        require 'net/https'
        require "httparty"

        @mailbox = ENV['HELPSCOUT_MAILBOX']
        @email = params[:email]
        @message = params[:message]
        @notes = params[:notes]
        @subject = @message[0,50] + "..."

        @payload = {
            "type"     => "email",
            "subject"  => @subject,
            "mailboxId"  => @mailbox,
            "status" => "active",
            "customer" => {
                "email" => @email
            },
            "threads" => [{
                "type" => "customer",
                "customer" => {
                    "email" => @email
                },
                "text" => @message
            }, {
                "type" => "note",
                "text" => @notes
            }]
        }.to_json

        # fetch oauth credentials. they expire in 2 hours, but let's expire ours in 1 hour to be safe
        token_response = SafeCache.fetch("helpscout/token", expires_in: 1.hour) do
            HTTParty.post(
                'https://api.helpscout.net/v2/oauth2/token',
                body: {
                    grant_type: 'client_credentials',
                    client_id: ENV['HELPSCOUT_CLIENT_ID'],
                    client_secret: ENV['HELPSCOUT_CLIENT_SECRET']
                }
            )
        end

        unless token_response.success?
            render :status => :internal_server_error, :json => token_response.body
            return
        end

        access_token = token_response['access_token']

        # comment these lines out if you want to test the end-to-end flow locally
        if Rails.env.development?
            pp @payload
            pp token_response
            return
        end

        create_response = HTTParty.post(
            'https://api.helpscout.net/v2/conversations',
            headers: {
                'Authorization' => "Bearer #{access_token}",
                'Content-Type' => 'application/json; charset=UTF-8'
            },
            body: @payload
        )

        unless create_response.success?
            render :status => :internal_server_error, :json => create_response.body
            return
        end

        render :status => 200, :json => create_response.body || {}

    rescue => e
        render :status => :internal_server_error, :json => {"error" => "failed #{e}"}
    end
end
