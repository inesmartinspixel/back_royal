class Api::LessonProgressController < Api::ApiCrudControllerBase

    authorize_resource :class => LessonProgress

    # see https://trello.com/c/5KexJXW7 to understand why this is done before.  We also
    # call set_progress_max_updated_at after the save below. We
    # do a similar thing with hiring relationships in set_last_relationship_updated_at
    before_action :set_progress_max_updated_at_before_changes, :only => [:create, :update]

    def record_params
        unless defined? @record_params
            @record_params = params['record'].clone

            # only allow an array of strings for frame_history
            if @record_params['frame_history']
                @record_params['frame_history'] = @record_params['frame_history'].select { |el| el.is_a?(String) }.compact
            end

            # only allow a hash with string keys and true values for completed_frames
            if @record_params['completed_frames']
                hash = @record_params['completed_frames']
                @record_params['completed_frames'] = {}
                hash.each do |key, value|
                    if key.is_a?(String) || key.is_a?(Symbol)
                        @record_params['completed_frames'][key] = true
                    end
                end
            end

            # only allow a hash with string keys and values between 0 and 1
            if @record_params['challenge_scores']
                hash = @record_params['challenge_scores']
                @record_params['challenge_scores'] = {}
                hash.each do |key, raw_value|
                    next unless raw_value.is_a?(Numeric)
                    next unless raw_value >= 0
                    next unless raw_value <= 1
                    next unless key.is_a?(String) || key.is_a?(Symbol)
                    @record_params['challenge_scores'][key] = raw_value.to_f
                end
            end

            # only allow a hash with string keys and number values
            if @record_params['frame_durations']
                hash = @record_params['frame_durations']
                @record_params['frame_durations'] = {}
                hash.each do |key, raw_value|
                    next unless raw_value.is_a?(Numeric)
                    next unless key.is_a?(String) || key.is_a?(Symbol)
                    @record_params['frame_durations'][key] = raw_value.to_f
                end
            end
        end
        @record_params
    end

    def deprecated_stream_progress_params
        # lesson_streams_progress is not required (daily lessons).
        params['meta'] && params['meta']['lesson_streams_progress']
    end

    # POST /#{model.table_name}.json
    def create
        create_or_update
    end

    # PUT /#{model.table_name}.json
    def update
        create_or_update
    end

    private
    def create_or_update
        rescue_validation_error_or_render_record({format_for_update_response: true}) do
            # LessonProgress needs to be saved before the Lesson::StreamProgress record
            # below. See Lesson::StreamProgress#mark_application_completed.
            lesson_progress = LessonProgress.create_or_update!(record_params.merge({
                :user_id => current_user.id
            }))

            # Before we started using the FrontRoyalStore, we would pass up
            # a single record in meta['lesson_streams_progress'].  With FrontRoyalStore
            # it is generally still one record, but theoretically there could be
            # more, so we accept an array called `stream_progress_records`
            # FIXME: cleanup. See https://trello.com/c/mf1NPdyO
            stream_progress_records_from_meta = params['meta'] && params['meta']['stream_progress_records']
            if deprecated_stream_progress_params.present?
                @meta[:lesson_streams_progress] = save_stream_progress(deprecated_stream_progress_params, lesson_progress)
            elsif stream_progress_records_from_meta&.any?
                @meta[:stream_progress_records] = stream_progress_records_from_meta.map do |attrs|
                    save_stream_progress(attrs, lesson_progress)
                end
            else
                raise_if_stream_progress_required(lesson_progress.locale_pack_id)
            end

            # set this after the save so the client can know when to bust the cache
            set_progress_max_updated_at

            lesson_progress
        end
    rescue UnauthorizedError => err
        # We don't expect to ever get either of these Unauthorized errors.
        # So if we do, log them to sentry so we know
        Raven.capture_exception(err, {
            extra: {
                lesson_progress: record_params,
                meta: params['meta'],
                user_id: current_user_id
            }
        })
        render_unauthorized_error
    end

    def save_stream_progress(attrs, lesson_progress)
        start = Time.now
        stream_progress = Lesson::StreamProgress.create_or_update!({
            :locale_pack_id => attrs["locale_pack_id"],
            :lesson_bookmark_id => attrs["lesson_bookmark_id"],
            :complete => attrs["complete"],
            :user_id => current_user.id
        }, current_user)

        raise_unless_lesson_is_in_stream(stream_progress.locale_pack_id, lesson_progress.locale_pack_id)

        # The stream progress will only bookmark the stream
        # when it is first started, so we can skip this
        # query otherwise (for performance)
        stream_progress_is_new = stream_progress.created_at > start
        if stream_progress_is_new
            @meta[:favorite_lesson_stream_locale_packs] = current_user.favorite_lesson_stream_locale_packs.pluck(:id).map { |id| {'id' => id} }
        end

        all_lessons_complete = stream_progress.all_lessons_complete?

        # This is covering for a super-edge case.  See https://trello.com/c/3B31JBXA
        if all_lessons_complete && !stream_progress.completed_at

            # have to use create_or_update! so the cert gets generated
            stream_progress = Lesson::StreamProgress.create_or_update!({
                :locale_pack_id => attrs["locale_pack_id"],
                :complete => true,
                :user_id => current_user.id
            })
        end

        # This is detecting a situation where we had transferred or backfilled
        # a stream progress, but then the user organically does complete all
        # the lesson content. We would have probably set a waiver on the progress because
        # the user would have been given a completed stream progress while probably
        # not having completed all the new / changed lessons. So if we detect that
        # they do complete all the lessons, let's take that waiver away.
        # See cohort_application.rb#transfer_progress_from_deferred_or_expelled_cohort
        # See https://trello.com/c/6xyHPQuo
        if all_lessons_complete && stream_progress.completed_at && stream_progress.waiver.present?
            stream_progress.update!(waiver: nil)
        end

        stream_progress.as_json
    end

    def raise_unless_lesson_is_in_stream(stream_locale_pack_id, lesson_locale_pack_id)
        unless lesson_locale_pack_id_in_stream(lesson_locale_pack_id, locale_pack_id: stream_locale_pack_id)
            raise UnauthorizedError.new("Lesson is not in stream")
        end
    end

    def raise_if_stream_progress_required(lesson_locale_pack_id)
        # I think this is required so that we can be sure that time_runs_out_at gets set
        if lesson_locale_pack_id_in_stream(lesson_locale_pack_id, exam: true)
            raise UnauthorizedError.new("Lesson progress can only be saved along with stream progress since lesson is in an exam stream.")
        end
    end

    def lesson_locale_pack_id_in_stream(lesson_locale_pack_id, filters)
        Lesson::Stream::Version.published_versions
            .where(filters)
            .joins(:lessons)
            .where("lessons.locale_pack_id" => lesson_locale_pack_id)
            .any?
    end

    class UnauthorizedError < RuntimeError; end


end
