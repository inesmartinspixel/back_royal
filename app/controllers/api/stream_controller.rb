# Totally weird gotcha: if stream_controller is moved into app/controller/api/lesson instead of
# app/controller/api, then *lesson_controller* (not stream_controller) starts getting errors on every test
# like 'undefined method `new' for Api::Lesson:Module'. It's not clear if this is a bug in the
# testing framework or possibly our method generation code, but it seems that if one controller assumes
# a particular module / namespace, then for some reason the model the other controller is using is
# suddenly assumed to be namespaced too, and then cant be found.
# Instead of diving further into this, just fixing for now by lessing stream_controller and lesson_controller
# be siblings.

class Api::StreamController < Api::ApiCrudControllerBase
    # I don't quite understand why, but load_and_authorize resource was causing cancan to override
    # our destroy methods, but only in Lesson::Stream.
    # using just authorize resource seems to work, though
    authorize_resource :class => Lesson::Stream
    skip_before_action :verify_signed_in, only: [:index, :show]
    before_action :check_params, only:[:index, :show]

    # create/update/destroy/images
    Api::ApiContentController.create_endpoints(self, ::Lesson::Stream)

    class UnauthorizedError < RuntimeError; end

    # GET /#{model.table_name}.json
    def index
        return if render_unauthorized_error_if_trying_to_view_disallowed_unpublished_content

        filters = params[:filters]

        if params[:get_has_available_incomplete_streams]
            get_has_available_incomplete_streams
        end

        to_json_params = params.merge({
            user: params[:user_id] ? User.find(params[:user_id]) : nil,
            filters: filters
        })

        # https://trello.com/c/vFDqFvIR/3662-perf-stream-index-call-is-super-slow-in-the-cohort-editor
        # It is possible that this endpoint is only used for loading stuff in the editor.  It's also
        # possible that this caching would make sense even for published streams.  But, I'm just being 
        # conservative here and applying this to calls for unpublished content.
        if filters['published'] == false 
            serialized_params = to_json_params.to_unsafe_h.slice('filters', 'fields[]').to_json
            cache_key="index_unpublished_streams_#{serialized_params}_#{Lesson::Stream.maximum(:updated_at)}"
            json = SafeCache.fetch(cache_key) do 
                Lesson::Stream::ToJsonFromApiParams.new(to_json_params, current_ability).json
            end
        else
            json = Lesson::Stream::ToJsonFromApiParams.new(to_json_params, current_ability).json
        end

        render_contents_json_string(Lesson::Stream, json)
    end

    # GET /#{model.table_name}/1.json
    def show(instance = nil)
        return render_error_response(I18n.t(:no_id_provided), :not_acceptable) if params[:id].blank?
        return if render_unauthorized_error_if_trying_to_view_disallowed_unpublished_content

        # We use a begin / rescue here rather than relying on check_params since we need to do
        # permission checks after the content is loaded.
        begin
            to_json_params = params.merge({
                user: current_user,
                filters: params[:filters]
            })
            json = Lesson::Stream::ToJsonFromApiParams.new(to_json_params, current_ability).json

            # if nothing was found, but the stream exists (i.e.: it only wasn't found due to extra filters)
            # then raise an UnauthorizedError here
            if json == '[]' && Lesson::Stream.where(id: params[:id]).exists?
                raise UnauthorizedError
            end
        rescue Lesson::Stream::ToJsonFromApiParams::UnauthorizedError, UnauthorizedError => err
            if !current_user
                return render_logged_out_error
            else
                return render_unauthorized_error
            end
        end
        render_not_found_or_contents_json_string(Lesson::Stream, params[:id], json)
    end

    # POST /#{model.table_name}/get_outdated.json
    def get_outdated
        published_ats_in_db = {}
        result = Lesson::Stream::ToJsonFromApiParams.new(
            {filters: {id: params[:streams].pluck(:id)}, 
            fields: [:id, :lessons, :published_at], 
            lesson_fields: [:published_at]}
        ).to_a

        result.each do |stream|
            published_ats_in_db[stream['id']] = [stream['published_at'], stream['lessons'].pluck('published_at')].flatten.max
        end

        # We do not do check_params here because
        # 1. Since we are building the filters explicitly, we don't 
        #    need to check for read_streams ability
        # 2. Since we are asking for specific stream ids, we don't
        #    need in_users_locale_or_en
        json_params = {
            user_id: current_user_id,
            filters: {id: []}, 
            load_full_content_for_stream_ids: []
        }
        params['streams'].each do |entry|
            next if entry[:content_updated_at] == published_ats_in_db[entry[:id]]

            json_params[:filters][:id] << entry[:id]
            if entry[:load_full_content]
                json_params[:load_full_content_for_stream_ids] << entry[:id]
            end

        end

        json = Lesson::Stream::ToJsonFromApiParams.new(json_params, current_ability).json

        render_contents_json_string(Lesson::Stream, json)
    end

    def check_params
        params[:filters] ||= {}
        filters = params[:filters]

        # Set user_id to current_user if not specified
        if !params[:user_id]
            params[:user_id] = current_user && current_user.id # do not allow auid fallback
        end

        # Set defaults
        filters[:user_can_see] = true unless filters.key?(:user_can_see)
        if !filters.key?(:in_users_locale_or_en) && params[:action].to_s == 'index'
            filters[:in_users_locale_or_en] = true
        end

        # Do the ability check
        if current_ability.cannot?(:read_streams, params)
            render_unauthorized_error
            return false
        end
    end

    def get_has_available_incomplete_streams
        available_streams_json = Lesson::Stream::ToJsonFromApiParams.new(params.merge({
            user: current_user, # there must be a current user
            filters: {
                published: true,
                user_can_see: true,
                complete:  false
            },
            fields: [:id],
            limit: 1
        })).json
        result = ActiveSupport::JSON.decode(available_streams_json)
        self.meta['has_available_incomplete_streams'] = result.any?
    end

end
