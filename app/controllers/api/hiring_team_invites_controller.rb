class Api::HiringTeamInvitesController < Api::ApiCrudControllerBase

    before_action :verify_create_params, :only => [:create]

    def create
        HiringTeamInvite.invite!(params["record"])
        render_record_hashes([{}], 'hiring_team_invites')
    rescue HiringTeamInvite::InviteeExists => err
        @meta.merge!(err.details)
        return render_error_response(err.message, :not_acceptable)
    end

    def verify_create_params
        unless current_ability.can?(:create_hiring_team_invite_with_params, params['record'])
            return render_unauthorized_error
            return false
        end
    end

end
