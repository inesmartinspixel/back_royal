class Api::StreamProgressController < Api::ApiCrudControllerBase

    before_action :verify_allowed_index_params, :only => [:index]

    PERMITTED_CREATE_UPDATE_PARAMS = :locale_pack_id, :time_runs_out_at

    # destroy handles the authorization inside of delete_record
    authorize_resource :class => Lesson::StreamProgress, :except => :destroy

    def permitted_record_params(params)
        params.require(:record).permit(PERMITTED_CREATE_UPDATE_PARAMS)
    end

    def verify_allowed_index_params
        unless current_ability.can?(:index_stream_progress, params)
            render_unauthorized_error
            return false
        end
    end

    # GET /api/stream_progress.json
    # Note: this method is used by the institutional admin Zapier CLI app; if we open this up further, we'd be opening up that as well.
    # Tests in the Zapier Internal app will also fail in that case.
    def index
        filters = params[:filters] || {}
        if params[:zapier]
            render_records(preload_stream_progress_for_zapier_index)
        elsif filters[:user_id]
            render_record_hashes(load_stream_progress_for_user_index, 'lesson_streams_progress')
        end
    end

    def preload_stream_progress_for_zapier_index
        # Index by default only shows completed stream progress records
        query = Lesson::StreamProgress
                    .joins(user: :institutions)
                    .joins(:published_content_titles)
                    .includes(:user)
                    .includes(:published_content_titles)
                    .where('institution_id = ?', params[:institution_id])
                    .where('lesson_streams_progress.completed_at is not null')

        if !sort_column.blank?
            query = query.reorder("#{sort_column} #{sort_direction}")
        end

        if !limit.blank?
            query = query.limit(limit)
        end

        query = query.all.to_a
    end

    def load_stream_progress_for_user_index
        filters = params[:filters] || {}
        user = filters[:user_id] == current_user_id ? current_user : User.find(filters[:user_id])
        updated_since = Time.at(filters[:updated_since] ? filters[:updated_since].to_f : 0)

        stream_progress_entries = Lesson::StreamProgress.includes(:certificate_image)
                                    .where(user_id: user.id)
                                    .where("updated_at > ?", updated_since)
                                    .as_json

        if params[:include_lesson_progress]
            meta[:lesson_progress] = LessonProgress
                                    .where(user_id: user.id)
                                    .where("updated_at > ?", updated_since)
                                    .as_json
        end

        if params[:include_favorite_streams]
            set = {}
            current_user.favorite_lesson_stream_locale_packs.pluck(:id).each do |id|
                set[id] = true
            end
            meta[:favorite_streams_set] = set
        end

        return stream_progress_entries
    end

    def limit
        limit = params[:limit]
        !limit.blank? ? [limit.to_i, 0].max : nil
    end

    def sort_column
        column = params[:sort] ? params[:sort].first : nil

        allowed_columns = Lesson::StreamProgress.column_names
        allowed_columns.include?(column) ? column : nil
    end

    def sort_direction
        %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
    end

    # PUT /api/stream_progress.json
    def update
        rescue_validation_error_or_render_record do
            stream_progress = Lesson::StreamProgress.find(params['id'])

            # this endpoint is only used to reset the timer from the reports page
            if params[:record].key?('time_runs_out_at') && params[:record][:time_runs_out_at].nil?
                stream_progress.update_attribute(:time_runs_out_at, nil)
            end

            stream_progress
        end
    end

    # DELETE /api/stream_progress.json
    def destroy
        delete_record(Lesson::StreamProgress, params['id']) do |stream_progress|
            lesson_locale_pack_ids = stream_progress.lesson_streams.map(&:lessons).flatten.map(&:locale_pack_id)
            LessonProgress.where(user_id: stream_progress.user_id, locale_pack_id: lesson_locale_pack_ids).destroy_all
            true
        end
    end

    # DELETE /api/stream_progress/destroy_all_progress.json
    def destroy_all_progress
        # This will do O(n) queries because activerecord's destroy_all wants to destroy each instance individually
        # so that it can run before_destroy and after_destroy callbacks and rollback the delete if there are errors.
        # You could also manually do the destroy_all in one query, but either you'd have to not run callbacks
        # (which could be dangerous if we or libraries decided to add before_destroy or after_destroy methods),
        # or run the callbacks manually but then rollback the entire batch delete if there were problems with any of the instance callbacks.
        # Doesn't seem worth doing this yet for an action that wont get taken very often.
        #
        # Automatically retries in the event of ActiveRecord::PreparedStatementCacheExpired
        in_transaction_with_editor_tracking do
            User.clear_all_progress(current_user.id)
        end
        render_records([])
    end

end
