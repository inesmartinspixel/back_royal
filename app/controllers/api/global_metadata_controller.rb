class Api::GlobalMetadataController < Api::ApiCrudControllerBase

    authorize_resource :class => :global_metadata

    def collection_key(record)
        "global_metadata"
    end

        # POST /api/global_metadata
        def create
            if params[:record].nil? || params[:record][:site_name].nil?
                return render_error_response('Create failed: site_name is required.', :not_acceptable)
            end

            existing_global_metadata = GlobalMetadata.find_by_site_name(params[:record][:site_name])

            if !existing_global_metadata.nil?
                return render_error_response('Validation failed: Global Metadata with this site name already exists.', :not_acceptable)
            end

            rescue_validation_error_or_render_record({}) do
                @global_metadata = GlobalMetadata.create_from_hash!(params[:record], params[:meta])
                @global_metadata
            end
        end

        # GET /api/global_metadata
        def index

            @meta["available_actions"] = GlobalMetadata.available_actions

            render_records(GlobalMetadata.all.order(:site_name).to_a)
        end

        # PATCH/PUT /api/global_metadata
        def update
            rescue_validation_error_or_render_record({}) do
                @global_metadata = GlobalMetadata.update_from_hash!(params.require(:record), params[:meta])
                @global_metadata
            end
        end

        # POST /#{model.table_name}/1/images.json
        # expects params to have an "images" property, which is an array whose elements can be either
        # - file uploads (in the case that the user used the file input in the interface)
        # - url strings (in the case that the user edited an image and we passed the url we got back from Aviary)
        def images
            render_error_if_not_found(GlobalMetadata, params[:id]) do |record|
                assets = params['images'].map do |image_upload_or_url|
                    asset = record.get_image_asset(image_upload_or_url)
                    asset.save!
                    asset
                end
                render_successful_response({'images' => assets})
            end
        end

        # DELETE /api/global_metadata/1
        def destroy
            delete_record(GlobalMetadata, params['id'])
        end

end
