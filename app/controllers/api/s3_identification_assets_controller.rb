class Api::S3IdentificationAssetsController < Api::ApiCrudControllerBase

    load_and_authorize_resource
    include ControllerMixins::HandlesEnrollmentDocuments
    include ControllerMixins::SendsDocuments

    # POST /api/s3_identification_assets
    def create
        rescue_validation_error_or_render_record do

            # Get user using the user_id param or fallback to the currently authenticated user
            user = User.find(permitted_post_params[:user_id] || current_user.id)

            # Destory the old identification first to ensure that the S3 asset is deleted
            if user.s3_identification_asset
                user.s3_identification_asset.destroy!
            end

            if user.identity_verified?

                # make sure that the client has the correct value for identity_verified
                set_user_push_messages

                # the conflict response is handled specially by frontRoyalUpload
                return render_error_response("Identity already verified", :conflict)
            end

            # Create the new identification asset
            asset = S3IdentificationAsset.create!({
                :file => permitted_post_params[:file],
                :user_id => user.id
            })
            self.create_upload_timeline_event(asset, 'id', current_user)

            # Associate the new identification asset with the currently authenticated user
            user.s3_identification_asset = asset
            user.save!

            if current_ability.cannot?(:create, asset)
                render_unauthorized_error
                raise ActiveRecord::Rollback.new

                # return nil so render_records will not try to render
                nil
            else
                # render the new record
                asset
            end
        end
    end

    def destroy
        rescue_validation_error_or_render_record do
            asset = S3IdentificationAsset.find(params[:id])
            self.create_deletion_timeline_event(asset, 'id', current_user)
            asset.destroy
        end
    end

    # GET /api/s3_identification_assets/:id/file
    def show_file
        send_document(S3IdentificationAsset, 'private_user_documents:identification_accessed')
    end

    def permitted_post_params
        params.require(:record).permit('file', 'user_id')
    end
end