class Api::EntityMetadataController < Api::ApiCrudControllerBase

    authorize_resource :class => EntityMetadata

    def collection_key(record)
        "entity_metadata"
    end

    # PATCH/PUT /api/entity_metadata
    def update
        rescue_validation_error_or_render_record({}) do
            @entity_metadata = EntityMetadata.update_from_hash!(params.require(:record), params[:meta])
            @entity_metadata
        end
    end

    # POST /#{model.table_name}/1/images.json
    # expects params to have an "images" property, which is an array whose elements can be either
    # - file uploads (in the case that the user used the file input in the interface)
    # - url strings (in the case that the user edited an image and we passed the url we got back from Aviary)
    def images
        render_error_if_not_found(EntityMetadata, params[:id]) do |record|
            assets = params['images'].map do |image_upload_or_url|
                asset = record.get_image_asset(image_upload_or_url)
                asset.save!
                asset
            end
            render_successful_response({'images' => assets})
        end
    end

end
