class Api::SignNowController < Api::ApiCrudControllerBase

    skip_before_action :verify_signed_in


    # {
    #     "meta": {
    #         "timestamp": 1550670387,
    #         "event": "document.update",
    #         "environment": "https://api-eval.signnow.com",
    #         "callback_url": "https://nbrusteinsmartly.ngrok.io/api/sign_now/hook",
    #         "access_token": "TOKEN"
    #     },
    #     "content": {
    #         "document_id": "dd21df2d4625671e5fe3bc5434134612931692e2",
    #         "document_name": "Enrollment Agreement Smartly MBA",
    #         "user_id": "e55b67295057842a1da797144831230fc801e7d0"
    #     },
    #     "controller": "api/sign_now",
    #     "action": "hook",
    #     "sign_now": {
    #         "meta": {
    #             "timestamp": 1550670387,
    #             "event": "document.update",
    #             "environment": "https://api-eval.signnow.com",
    #             "callback_url": "https://nbrusteinsmartly.ngrok.io/api/sign_now/hook",
    #             "access_token": "TOKEN"
    #         },
    #         "content": {
    #             "document_id": "dd21df2d4625671e5fe3bc5434134612931692e2",
    #             "document_name": "Enrollment Agreement Smartly MBA",
    #             "user_id": "e55b67295057842a1da797144831230fc801e7d0"
    #         }
    #     }
    # }

    def hook
        if event == 'document.complete' && document_id
            ProcessCompletedSignNowDocumentJob.ensure_job(document_id)
        else
            Raven.capture_exception("Unexpected request to SignNowController", {
                extra: {params: params}
            })
        end

        head :ok
    end

    def document_id
        params['sign_now']['content']['document_id']
    rescue NoMethodError => err
        nil
    end

    def event
        params['sign_now']['meta']['event']
    rescue NoMethodError => err
        nil
    end
end