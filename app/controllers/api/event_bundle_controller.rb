class Api::EventBundleController < Api::ApiCrudControllerBase
    include Skylight::Helpers

    authorize_resource :class => Event
    skip_before_action :verify_signed_in, only: [:create]
    before_action :verify_current_user_id, only: [:create]

    def permitted_create_params
        params.require(:record).permit!
    end

    # POST /#{model.table_name}.json
    instrument_method
    def create

        # # uncomment to mock a request failing the first time before events get saved
        # if (!params.require(:http_queue)[:retry])
        #     start = Time.now
        #     while Time.now - start < 3
        #     end
        #     render_error_response("", 408)
        #     return
        # end

        # create event
        save_event_bundle(permitted_create_params["events"])

        # # uncomment to mock a request failing the first time after events get saved
        # if (!params.require(:http_queue)[:retry])
        #     start = Time.now
        #     while Time.now - start < 3
        #     end
        #     render_error_response("", 408)
        #     return
        # end

        # render no response on success; client should be fire-and-forget-ing
        render_successful_response({})
    end

end
