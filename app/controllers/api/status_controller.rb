class Api::StatusController < Api::ApiCrudControllerBase
    include Api::UnloadedChangeDetector

    skip_before_action :verify_signed_in
    before_action do
        add_last_updated_to_push_messages(:hiring_relationship, "current")
        add_last_updated_to_push_messages(:open_position, "current")
        add_last_updated_to_push_messages(:candidate_position_interest, "current")
    end
    before_action :set_progress_max_updated_at

    def set_config_push_messages
        @meta['push_messages'].deep_merge!({
            'event_logger' => {
                'disable_auto_events_for' => AppConfig.disable_auto_events_for
            }
        })
    end


    def set_content_refresh_push_messages
        content_views_refresh_updated_at = ContentViewsRefresh.debounced_value

        # For efficiency sake we only call add_cohort_applications_to_push_messages in the student_dashboards#index call. In commit 9e5f5c7 we
        # introduced front-end caching for the student_dashboards#index call. Thus, we introduced a bug where a user will not find out about
        # a changed application state until the cache is invalidated and a call to student_dashboards#index is made. To fix this let's
        # do a fast query to get the most recent updated_at for a user's cohort_applications and piggyback off of the
        # content_views_refresh_updated_at value to invalidate the cache and trigger a student_dashboard refresh if needed.
        cohort_applications_updated_at = nil
        if current_user
            cohort_applications_updated_at = CohortApplication.where(user_id: current_user.id).maximum("updated_at")
        end

        @meta['push_messages'].deep_merge!({

            # See comment above. Either set to when the last content change happened or the most recent updated_at for the user's cohort
            # applications (whichever is greater).
            'content_views_refresh_updated_at' => [cohort_applications_updated_at, content_views_refresh_updated_at].compact.max.to_timestamp
        })
    end

    instrument_method
    def set_push_messages
        @meta['push_messages'] ||= {}

        # elvis operator doesn't work here because current_user is false instead of nil
        # when there is no current_user
        set_user_push_messages(extra: {favorite_lesson_stream_locale_packs: current_user&.favorite_lesson_stream_locale_packs_for_json})
        set_relevant_cohort_push_messages
        set_institution_push_messages
        set_primary_subscription_push_messages
        set_config_push_messages
        set_careers_push_messages
        set_content_refresh_push_messages
        add_cohort_applications_to_push_messages
        set_id_verification_push_messages
        add_num_recommended_positions_to_push_messages
    end

    def render_successful_response(response)
        set_push_messages
        super(response)
    end

    def render_contents_json_string(klass_or_collection_key, json)
        set_push_messages
        super(klass_or_collection_key, json)
    end

    def simple
        render_successful_response({'ping' => 'pong'})
    end

end