require 'back_royal/paginate_fastly'

class Api::CareerProfilesController < Api::ApiCrudControllerBase

    # play nice with CanCan and move permission stuff into ability.rb
    before_action :authorize_index_with_params, :only => :index
    before_action :set_limit_when_not_has_full_access, :only => :index

    # we have custom authorization handling for the show action (see below)
    skip_load_and_authorize_resource :only => :show

    load_and_authorize_resource

    include ControllerMixins::CareerProfilesFiltersMixin
    include ControllerMixins::HiringManagerFiltersMixin
    include ControllerMixins::StudentNetworkFiltersMixin

    # GET /api/career_profiles
    def index
        if params[:view] == 'student_network_profiles'
            result = preload_student_network_profiles_for_index

        # the admin page uses the filtering in preload_career_profiles_for_index but
        # wants to get back editable career profiles.  So even though the view
        # is `editable`, it makes sense to use preload_career_profiles_for_index
        elsif params[:view] == 'career_profiles' || params[:view] == 'editable'
            # - we saw a tiny bit of evidence that maybe wrapping the save_search and the
            # query to career profiles in a transaction led to locked queries and
            # database issues on 10/12/2017
            # - Since this can be a post or a get, the filter could be stringified or not
            if ['true', true].include?(params["save_search"])
                save_search # must be before the render, because it sets meta
            end
            result = preload_career_profiles_for_index
        end

        render_records(result, career_profile_json_options)
    end

    def show
        career_profile = CareerProfile.find_by_id(params["id"])

        return render_not_found_error(CareerProfile, params["id"]) if career_profile.nil?

        if current_ability.cannot?(:show_career_profile, {params: params, career_profile: career_profile})
            return params['deep_link'] == 'true' ? render_error_response(I18n.t(:unsupported_career_profile_deep_link), :unauthorized) : render_unauthorized_error
        end

        render_records([career_profile], career_profile_json_options(params['view']))
    end

    def share
        career_profile = CareerProfile.find_by_id(share_params["id"])
        recipent = User.find_by_email(share_params['email'])
        current_user.share_career_profile(career_profile, recipent, share_params['message'])
        render_records(career_profile, career_profile_json_options('career_profiles'))
    end

    def career_profile_json_options(view = nil)
        view ||= params[:view]
        anonymize = true

        if view == 'career_profiles'
            if current_ability.can?(:view_full_hiring_manager_experience, nil)
                anonymize = false
            end

        elsif view == 'student_network_profiles'
            if current_user.has_full_student_network_access?
                anonymize = false
            end
        elsif view != 'editable'
            raise "Unexpected view #{view.inspect}"
        end

        {
            view: view,
            anonymize: anonymize
        }
    end

    def share_params
        params.require(:record).permit(:id, :email, :message)
    end

    def cancan_model_class
        CareerProfile
    end

    def quote_collection(arr)
        arr.map { |e| ActiveRecord::Base.connection.quote(e) }.join(',')
    end

    def authorize_index_with_params
        if current_ability.cannot?(:index_using_career_profiles_controller, params)
            render_unauthorized_error
            false
        else
            true
        end
    end

    def preload_student_network_profiles_for_index
        filters = params['filters'] || {}

        # See student_network_filters_mixin.rb
        query = process_student_network_filters(filters, query)
        query = process_career_profiles_filters(filters, query)
        query = apply_anonymization_ordering(query)
        query = apply_default_student_network_ordering(query)

        includes_needed = CareerProfile.includes_needed_for_json_eager_loading_student_network

        apply_limit(query, includes_needed)
    end

    # separate method for preload testing
    def preload_career_profiles_for_index

        filters = params['filters'] || {}

        query = CareerProfile.all
        # See hiring_manager_filters_mixin.rb
        query = process_hiring_manager_filters(filters, query)
        query = process_career_profiles_filters(filters, query)

        # this is used in the cohorts admin
        if params[:view] == 'editable'
            query = process_admin_filters(filters, query)
        end
        includes_needed = CareerProfile.includes_needed_for_json_eager_loading

        if filters.key?('can_edit_career_profile')
            query = query.joins(:user).where("can_edit_career_profile = ?", filters['can_edit_career_profile'])
        end

        if filters.key?('available_for_relationships_with_hiring_manager')
            available_query_sql = User.available_for_relationships(current_user.professional_organization_option_id).select('id').to_sql;
            query = query.where("user_id IN (#{available_query_sql})")
        end

        if filters.key?('user_id_not')
            query = query.where.not(user_id: filters['user_id_not'])
        end

        if params.key?('limit')
            query = query.limit(params['limit'])
        end

        query = order(filters, query)

        # The naming here isn't quite right since really has_candidate_relationship means
        # "has been procecssed", because a featured candidate will have a relationship with a
        # hiring_manager_status = "pending" and we want to include those candidates here. I didn't
        # think it was worth changing the name of the filter for the special featured case.
        # See https://trello.com/c/pUrsPR6Y
        if filters['has_candidate_relationship'] == false

            already_processed_candidate_ids = current_user.candidate_relationships_with_non_pending_hiring_manager_status.select(:candidate_id)
            liked_by_team_candidate_ids = current_user.relationships_from_team_with_accepted_hiring_manager_status.select(:candidate_id)

            query = query.with(unviewable_candidates: "#{already_processed_candidate_ids.to_sql} union #{liked_by_team_candidate_ids.to_sql}")
                        .where("user_id not in (select candidate_id from unviewable_candidates)")
        end

        apply_limit(query, includes_needed)
    end

    def process_admin_filters(filters, query)
        if filters.key?('accepted_or_pre_accepted_in')
            query = query.joins(user: :cohort_applications)
                            .where({
                                cohort_applications: {
                                    status: ['accepted', 'pre_accepted'],
                                    cohort_id: filters['accepted_or_pre_accepted_in']
                                }
                            })
        end

        if filters.key?('registered')
            raise "We only accept a true value for registered" unless filters['registered'] = true
            # it is safe to add the same join here that we added above
            query = query.joins(user: :cohort_applications)
                            .where({
                                cohort_applications: {
                                    registered: true
                                }
                            })
        end

        query
    end

    def apply_limit(query, includes_needed)
        query, total_count = query.paginate_fastly(limit: params[:limit], offset: params[:offset], max_total_count: params[:max_total_count])

        # doing the preload trick here rather than includes performed better in local testing
        career_profiles = query.to_a
        ActiveRecord::Associations::Preloader.new.preload(career_profiles, includes_needed)
        @meta['total_count'] = total_count
        career_profiles
    end

    def order(filters, query)
        sort = params['sort'].is_a?(Array) ? params['sort'] : [params['sort']]

        sort.each do |key|
            if key == 'BEST_SEARCH_MATCH_FOR_HM'

                employment_types_of_interest = filters['employment_types_of_interest'] || []
                years_experience = filters['years_experience'] || []

                # roles
                if filters['roles'].present?
                    query = order_by_roles(filters, query)
                end

                # places
                if filters['places']&.any?
                    query = order_by_places(filters, query)
                end

                # years_experience
                if years_experience.any?
                    query = order_by_years_experience(filters, query)
                end

                # industries
                if filters['industries']&.any?
                    query = order_by_industries(filters, query)
                end

                # in_school
                if filters['in_school'].nil? && !employment_types_of_interest.include?('internship') && !years_experience.include?('0_1_years')
                    query = deprioritize_in_school(filters, query)
                end

                query = default_prioritization(query)
            elsif key == 'PRIORITIZE_HIRING_INDEX'
                query = hiring_index_prioritization(query)
            end
        end

        query
    end

    def default_prioritization(query)
        query = join_search_helpers(query)
        # first, order by the default_prioritization value stored in the career_profile_search_helpers table
        # and then by the career profile id to ensure consistent ordering in the event that two profiles have
        # the same default_prioritization value
        query = query.order("career_profile_search_helpers_2.default_prioritization desc, career_profiles.id ASC")
    end

    def hiring_index_prioritization(query)
        # Prioritize results from hiring_index
        # see https://trello.com/c/gPXURE7d
        query = query.order(Arel.sql("
            case
                when career_profiles.id = ANY((SELECT career_profile_ids from career_profile_lists where name='hiring_index')::uuid[]) then 0
                else 1
            end
        "))

        query
    end

    def order_by_roles(filters, query)

        query = join_search_helpers(query)

        # 1. preferred_areas matches in both work_experiences_roles and primary_areas_of_interest
        # 2. preferred_areas match in work_experiences_roles / all_areas match in primary_areas_of_interest
        # 3. all_areas match in work_experiences_roles / preferred_areas match in primary_areas_of_interest
        # 4. all_areas matches in both work_experiences_roles and primary_areas_of_interest
        # 5. preferred_areas match in work_experiences_roles
        # 6. all_areas match in work_experiences_roles
        # 7. preferred_areas match in primary_areas_of_interest
        # 8. all_areas match in primary_areas_of_interest

        # Or, said another way:

        # * 2 matches is better than 1
        # * any match in work_experiences_roles is better than any match in primary_areas_of_interest
        # * all else equal, a match in preferred_areas is better than a match in all_areas

        # After coming up with all that, we added the rule that matches in the current work
        # experience are better than matches in previous work experiences.  See https://trello.com/c/H5fAM37k

        if filters['roles'].present?
            roles_filters = filters['roles']
            preferred_areas_of_interest = quote_collection(roles_filters['preferred_primary_areas_of_interest'])
            all_areas_of_interest = quote_collection(roles_filters['primary_areas_of_interest'])
            preferred_work_experience_roles = quote_collection(roles_filters['preferred_work_experience_roles'])
            all_work_experience_roles = quote_collection(roles_filters['work_experience_roles'])
        end

        query = query.order(Arel.sql("
            (
                CASE
                    when current_work_experience_roles && ARRAY[#{preferred_work_experience_roles}] then 14.1
                    when work_experience_roles && ARRAY[#{preferred_work_experience_roles}] then 14
                    when current_work_experience_roles && ARRAY[#{all_work_experience_roles}] then 12.1
                    when work_experience_roles && ARRAY[#{all_work_experience_roles}] then 12
                    else 0
                end
                +
                CASE
                    when primary_areas_of_interest && ARRAY[#{preferred_areas_of_interest}] then 11
                    when primary_areas_of_interest && ARRAY[#{all_areas_of_interest}] then 10
                    else 0
                end
            ) DESC
        "))

        query
    end

    def order_by_industries(filters, query)
        industries = quote_collection(filters['industries'])
        query = query.order(Arel.sql("
            (
                CASE
                    when job_sectors_of_interest && ARRAY[#{industries}] then 1
                    else 0
                end
                +
                CASE
                    when work_experience_industries && ARRAY[#{industries}] then 2
                    else 0
                end
            ) DESC
        "))
        query
    end

    def order_by_places(filters, query)
        # NOTE: if this is edited or removed, see comment below
        # about sql injection
        is_local_condition = filters['places'].map { |place_details|
            Location.at_location_sql_string(place_details)
        }.join(' or ')

        # prefer local folks. desc nulls last puts the true values first
        query = query.order(Arel.sql("(#{is_local_condition}) desc nulls last"))

        # prefer people who actually have one of the locations in their
        # locations_of_interest, as opposed to just "flexible"
        # desc nulls last puts true values first
        key_list = location_keys_close_to_requested_places(filters).map { |key|
            "'#{key}'"
        }

        if key_list.any?
            query = query.order(Arel.sql("locations_of_interest && ARRAY[#{key_list.join(',')}] desc nulls last"))
        end

        query
    end

    def order_by_years_experience(filters, query)
        query = join_search_helpers(query).order(Arel.sql("
            case
                when years_of_experience is not null
                    then (years_of_experience || ' years')::interval
                when years_of_experience_reference_date is not null and years_of_experience_reference_date < now()
                    then now() - years_of_experience_reference_date
                when years_of_experience_reference_date >= now()
                    then interval '0 years'
                end
            desc
        "))
    end

    def deprioritize_in_school(filters, query)
        query = join_search_helpers(query)
        query = query.order("career_profile_search_helpers_2.in_school")
    end

    # PUT /api/career_profiles
    def update
        rescue_validation_error_or_render_record(view: 'editable') do

            meta = params[:meta] || {}
            career_profile = CareerProfile.update_from_hash!(career_profile_params, meta.merge({is_admin: current_user.has_role?(:admin)}))

            # Update the appropriate fields on the user if they are changed. This convienantly
            # lets us deal with only the one profile object on the client.
            user = User.find(career_profile.user_id)
            changed = false

            # Tricky: if saving city, state, or country, clear out the other 2 because we don't want
            # to end up with something odd like Boulder, Tokyo, JP if a user was originally in Boulder, CO
            # but then switches to a new location like Tokyo, JP that doesn't have a city supplied.
            changing_location = false
            ['city', 'state', 'country'].each do |key|
                if !career_profile_params[key].nil?
                    changing_location = true
                    changed = true
                end
            end

            # update all locations keys if updating any of them
            if changing_location
                ['city', 'state', 'country'].each do |key|
                    # clear it out, then update it if it was specified
                    user.send(:"#{key}=", nil)

                    if !career_profile_params[key].nil?
                        user.send(:"#{key}=", career_profile_params[key])
                    end
                end
            end

            # handle the rest of the fields
            ['name', 'nickname', 'phone', 'avatar_url',
                'sex', 'ethnicity', 'race', 'how_did_you_hear_about_us',
                'anything_else_to_tell_us',
                'address_line_1', 'address_line_2',
                'zip', 'birthdate', 'program_type',
                'pref_student_network_privacy', 'student_network_email'].each do |key|

                new_val = career_profile_params[key]
                if key == 'student_network_email' && new_val.blank?
                    new_val = nil
                end

                # if the field is changing OR the field is program_type, update it on the user
                if !career_profile_params[key].nil? and ((user.send(key) != new_val) or key == 'program_type')
                    user.send(:"#{key}=", new_val)
                    changed = true
                end
            end

            if changed
                user.save!
                current_user.reload
                career_profile.reload
                add_career_profile_user_fields_to_push_messages
                add_cohort_applications_to_push_messages # a change to program_type can update cohort applications
                set_relevant_cohort_push_messages # change to program_type can change relevant_cohort
                set_institution_push_messages # change to program_type can change institution state
            end

            add_num_recommended_positions_to_push_messages

            career_profile
        end
    end

    # Note: leave this as it contains a good cancan example
    # POST /api/career_profiles
    def create
        rescue_validation_error_or_render_record(view: 'editable') do
            career_profile = CareerProfile.create_from_hash!(career_profile_params)
            if current_ability.cannot?(:create, career_profile)
                render_unauthorized_error
                raise ActiveRecord::Rollback.new

                # return nil so render_records will not try to render
                nil
            else
                # render the new record
                career_profile
            end
        end
    end

    private

        # Only allow a trusted parameter "white list" through. Note: relying on `to_h` instead causes oddities
        def career_profile_params
            return @career_profile_params if defined? @career_profile_params

            allowed_params = [
                # FIXME: organize these by their form mappings like in career_profile.rb

                :id, :created_at, :updated_at, :user_id, :do_not_create_relationships, :last_calculated_complete_percentage,
                :willing_to_relocate, :short_answers,

                :native_english_speaker, :earned_accredited_degree_in_english, :sufficient_english_work_experience, :english_work_experience_description, :has_no_formal_education,

                :score_on_gmat, :score_on_sat, :score_on_act, :score_on_gre_verbal, :score_on_gre_quantitative, :score_on_gre_analytical,
                :sat_max_score,

                :short_answer_greatest_achievement, :short_answer_leadership_challenge, :primary_reason_for_applying,
                :short_answer_why_pursuing, :short_answer_use_skills_to_advance, :short_answer_ask_professional_advice,

                :profile_feedback, :feedback_last_sent_at,

                :survey_years_full_time_experience, :survey_most_recent_role_description, :survey_highest_level_completed_education_description,

                { :work_experiences => [:id, :job_title, :start_date, :end_date, :featured, :industry, :role, :employment_type, { :responsibilities => [] }, :professional_organization => [:text, :locale] ] },
                { :education_experiences => [:id, :degree, :major, :minor, :graduation_year, :gpa, :gpa_description, :degree_program, :will_not_complete, :educational_organization => [:text, :locale] ] },

                { :peer_recommendations => [:id, :email, :contacted] },

                :open_to_remote_work, :authorized_to_work_in_us, :salary_range, :interested_in_joining_new_company,
                { :locations_of_interest => [] }, { :top_mba_subjects => [] },
                { :top_personal_descriptors => [] }, { :top_motivations => [] }, { :top_workplace_strengths => [] }, :bio, :long_term_goal, :personal_fact,
                { :preferred_company_culture_descriptors => [] }, { :job_sectors_of_interest => [] }, { :employment_types_of_interest => [] }, { :levels_of_interest => [] },
                { :company_sizes_of_interest => [] }, { :primary_areas_of_interest => [] }, { :preferred_primary_areas_of_interest => [] }, :place_id,
                { :skills => [:id, :text, :locale] },
                { :student_network_interests => [:id, :text, :locale] },
                { :student_network_looking_for => [] },
                { :awards_and_interests => [:id, :text, :locale] },

                :li_profile_url, :resume_id, :personal_website_url, :blog_url, :tw_profile_url, :fb_profile_url, :github_profile_url,

                # application_questions
                :short_answer_scholarship,

                # submit_application_form
                :consider_early_decision,

                # User fields
                :name, :nickname, :phone, :avatar_url, :program_type,
                :sex, :ethnicity, { :race => [] }, :how_did_you_hear_about_us, :anything_else_to_tell_us,
                :address_line_1, :address_line_2, :city, :state, :zip, :country, :birthdate,
                :pref_student_network_privacy, :student_network_email
            ]

            p = params.require(:record).permit(allowed_params)

            # We don't own the place_details object (Google does) so we would rather not have to whitelist
            # every field.
            # See https://github.com/rails/rails/pull/14317
            if params[:record].key?(:place_details)

                # nil is actually not valid, but let the model error in that case.  The client should not
                # ever send up nil
                p[:place_details] = params[:record][:place_details].nil? ? nil : params[:record][:place_details].to_unsafe_h
            end

            # We want to make sure that we only update the last_updated_at_by_student timestamp if the record belongs to the current_user
            p[:last_updated_at_by_student] = Time.now if params[:record][:user_id] == current_user.id

            @career_profile_params = p
        end

        def save_search
            filters = params[:filters] || {}

            search = CareerProfile::Search.save_search(current_user_id, filters)
            @meta['career_profile_search'] = search.as_json
        end

        def add_career_profile_user_fields_to_push_messages
            extra = {
                'avatar_url' => current_user.avatar_url,
                'name' => current_user.name,
                'phone' => current_user.phone,

                # These values can changed when the user updates the place_details on their career profile, but they are stored
                # on the user, so we add these values to the current_user push messages to make sure that the currentUser on the
                # client gets updated appropriately. See https://trello.com/c/uLwkGz3z.
                'country' => current_user.country,
                'city' => current_user.city,
                'state' => current_user.state,

                # the career_profile_controller supports setting program_type on the user.  One
                # of the effects of that is to change the fallback_program_type, so we need
                # to send that down to update the currentUser in the client
                'fallback_program_type' => current_user.fallback_program_type,
                'mba_enabled' => current_user.mba_enabled?
            }
            set_user_push_messages(extra: extra)
        end

        def set_limit_when_not_has_full_access
            if params[:view] == 'career_profiles' && current_ability.cannot?(:view_full_hiring_manager_experience, nil)
                params[:limit] = 3
            end
        end

        # We don't use the locations filter anymore.  We use the places
        # filter.  Since it is a hassle to keep backwards compatability for
        # saving searches with both, we just show a deprecation error to
        # any hiring manager trying to use an old client on the browse page
        # (which is the only place, other than maybe the admin, using this filter)
        def using_deprecated_client?
            !!(
                params['filters'] &&
                params['filters']['locations']&.any?
            )
        end

end