class Api::HiringTeamsController < Api::ApiCrudControllerBase

    before_action :authorize_manage, :except => :update
    before_action :authorize_update_with_params, :only => :update

    def index
        render_records(preload_hiring_teams_for_index)
    end

    def show
        hiring_team = HiringTeam.find_by_id(params["id"])
        render_records([hiring_team], params.to_unsafe_h.slice(:fields))
    end

    def create

        rescue_validation_error_or_render_record do
            hiring_team = HiringTeam.create_from_hash!(params["record"])
            hiring_team
        end
    end

    def update
        rescue_validation_error_or_render_record do
            hiring_team = HiringTeam.update_from_hash!(update_params)
            hiring_team
        end
    end

    def destroy
        delete_record(HiringTeam, params['id'])
    end

    def preload_hiring_teams_for_index
        HiringTeam.includes(*HiringTeam.includes_needed_for_json_eager_loading).all.to_a
    end

    def update_params
        unless @update_params
            @update_params = params['record'].clone
            unless can?(:manage, HiringTeam)
                @update_params.slice!('hiring_plan', 'id')
            end
        end
        @update_params
    end

    def authorize_manage
        if current_ability.cannot?(:manage, HiringTeam)
            render_unauthorized_error
            false
        else
            true
        end

    end

    def authorize_update_with_params
        if current_ability.cannot?(:update_hiring_team_with_params, update_params)
            render_unauthorized_error
            false
        else
            true
        end
    end

end
