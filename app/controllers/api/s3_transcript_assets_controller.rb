class Api::S3TranscriptAssetsController < Api::ApiCrudControllerBase

    load_and_authorize_resource
    include ControllerMixins::HandlesEnrollmentDocuments
    include ControllerMixins::SendsDocuments

    # POST /api/s3_transcript_assets
    def create
        rescue_validation_error_or_render_record do
            params = permitted_post_params

            education_experience_id = params[:education_experience_id]
            education_experience = CareerProfile::EducationExperience.find_by_id(education_experience_id)

            raise 'Must provide an existing education experience' if education_experience.nil?

            transcript = S3TranscriptAsset.new(file: params[:file])

            # Need to set this now for the path method to work
            transcript.education_experience = education_experience

            # Handles edge-case where they un-waive then upload directly
            # through the file upload UI. If we are uploading a transcript
            # then we know it is being attached to an education_experience,
            # and thus we can nullify the transcript_waiver.
            if transcript.education_experience.transcript_waiver.present?
                transcript.education_experience.transcript_waiver = nil
                transcript.education_experience.save!
            end

            transcript.save!

            self.create_upload_timeline_event(transcript, "#{education_experience.educational_organization.text} transcript", current_user)

            transcript
        end
    end

    def destroy
        transcript = S3TranscriptAsset.find(params[:id])

        if transcript.education_experience.transcript_approved
            return render_error_response("Can't delete transcript if approved", :not_acceptable)
        end

        rescue_validation_error_or_render_record do
            self.create_deletion_timeline_event(transcript, "#{transcript.education_experience.educational_organization.text} transcript", current_user)
            transcript.destroy
        end
    end

    # GET /api/s3_transcript_assets/:id/file
    def show_file
        send_document(S3TranscriptAsset, 'private_user_documents:transcript_accessed')
    end

    def permitted_post_params
        params.require(:record).permit('file', 'user_id', 'education_experience_id')
    end
end