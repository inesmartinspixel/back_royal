require 'back_royal/paginate_fastly'

class Api::OpenPositionsController < Api::ApiCrudControllerBase
    include Api::UnloadedChangeDetector

    before_action :authorize_index_with_params, :only => :index
    before_action :authorize_create_or_update_with_params, :only => [:update, :create]

    before_action(:only => [:create, :update]) do
        add_last_updated_to_push_messages(:open_position, "before")
    end

    # GET /api/open_positions
    def index
        # Automatically retries in the event of ActiveRecord::PreparedStatementCacheExpired
        in_transaction_with_editor_tracking do
            if params["save_search"] == "true"
                save_search # must be before the render, because it sets meta
            end

            query = OpenPosition.all
            filters = params[:filters] || {}

            # If using a param or filter that involves a candidate_id then grab the candidate and career_profile
            candidate_id = params[:candidate_id] || filters[:recommended_for] || filters[:hiring_manager_might_be_interested_in]
            if candidate_id.present?
                @candidate = candidate_id == current_user&.id ? current_user : User.find(candidate_id)
            end

            if filters.key?(:hiring_manager_might_be_interested_in)
                query = query.viewable_when_considering_relationships_and_interests(
                    candidate_or_id: filters[:hiring_manager_might_be_interested_in],
                    candidate_has_acted_on: filters[:candidate_has_acted_on]
                )
            end

            if filters.key?(:id)
                query = query.where(id: filters[:id])
            end

            if filters.key?(:hiring_manager_email) || params[:sort]&.include?('hiring_manager_email') ||
                filters.key?(:hiring_manager_company_name) || params[:sort]&.include?('hiring_manager_company_name')

                # Instead of saying `query.joins(:hiring_manager)` we assign a custom table name so we
                # can reference it in filters and sorting
                query = query.joins("JOIN users hiring_managers_custom ON hiring_managers_custom.id = open_positions.hiring_manager_id")
            end

            if filters.key?(:hiring_manager_company_name) || params[:sort]&.include?('hiring_manager_company_name')
                query = query.joins(:professional_organization)
            end

            if params[:sort]&.include?('num_unreviewed_and_unrejected_interests')
                query = query.left_outer_joins(:unreviewed_and_unrejected_candidate_position_interests)

                # Would be nice to just do this always at the bottom rather than conditionally here,
                # but that causes issues when sorting by hiring_manager_email.
                query = query.group(:id)
            end

            if filters.key?(:status)
                status = filters[:status]
                if status === 'live'
                    filters[:featured] = true
                    filters[:archived] = false
                elsif status === 'drafted'
                    filters[:featured] = false
                    filters[:archived] = false
                elsif status === 'archived'
                    filters[:archived] = true
                end
            end

            if filters.key?(:hiring_manager_id)
                query = query.where(hiring_manager_id: filters[:hiring_manager_id])
            end

            if filters.key?(:hiring_manager_email)
                query = query.where("hiring_managers_custom.email ilike ?", "%#{filters[:hiring_manager_email]}%")
            end

            if filters.key?(:hiring_manager_company_name)
                query = query.where("professional_organization_options.text ilike ?", "%#{filters[:hiring_manager_company_name]}%")
            end

            if filters.key?(:title)
                query = query.where("open_positions.title ilike ?", "%#{filters[:title]}%")
            end

            if filters.key?(:hiring_team_id)
                query = query.joins(:hiring_manager).where(users: {hiring_team_id: filters[:hiring_team_id]})
            end

            if filters.key?(:featured)
                query = query.where(featured: filters[:featured])
            end

            if filters.key?(:archived)
                query = query.where(archived: filters[:archived])
            end

            if filters[:role]&.any?
                query = query.where('ARRAY[role] && ARRAY[?]', filters[:role])
            end

            if filters[:industry]&.any?
                query = query.joins("join users hiring_managers on hiring_managers.id = hiring_manager_id")
                    .joins("join hiring_applications on hiring_applications.user_id = hiring_managers.id")
                    .where('ARRAY[hiring_applications.industry] && ARRAY[?]', filters[:industry])
            end

            if filters[:position_descriptors]&.any?
                query = query.where('open_positions.position_descriptors && ARRAY[?]', filters[:position_descriptors])
            end

            if filters[:years_experience]&.any?
                years_experience_query = OpenPosition.select(:id)

                filters[:years_experience].each_with_index do |filter_val, i|
                    pieces = filter_val.split('_') # ex. 1_2_years, 6_plus_years
                    min = pieces.first.to_i
                    max = pieces.second == 'plus' ? '99' : pieces.second.to_i

                    # https://www.postgresql.org/docs/9.3/rangetypes.html
                    # ex. SELECT numrange(11.1, 22.2) && numrange(20.0, 30.0);
                    # Note: the third '[]' argument means inclusive on both ends
                    sql_clause = "numrange(?, ?, '[]') && numrange((desired_years_experience->>'min')::integer, (desired_years_experience->>'max')::integer, '[]')"

                    if i == 0
                        years_experience_query = years_experience_query.where(sql_clause, min, max)
                    else
                        years_experience_query = years_experience_query.or(OpenPosition.select(:id).where(sql_clause, min, max))
                    end
                end

                query = query.where("open_positions.id IN (#{years_experience_query.to_sql})")
            end

            if filters[:places]&.any?
                location_clauses = []

                filters[:places].each do |place_details|
                    location_clauses << Location.at_location_sql_string(place_details)
                end

                query = query.where(location_clauses.join(" OR "))
            end

            if filters.key?(:recommended_for)
                query = query.recommended(candidate_or_id: @candidate)
            end

            query = apply_order(query)

            open_positions = preload_open_positions_for_index(query)

            add_num_recommended_positions_to_push_messages
            render_records(open_positions, self.api_options)
        end
    end

    # GET /api/open_positions/1
    def show
        rescue_validation_error_or_render_record({
            :fields => params[:fields]
        }) do
            open_position = OpenPosition.find_by_id(params["id"])
            open_position
        end
    end

    # POST /api/open_positions
    def create
        rescue_validation_error_or_render_record do
            open_positon = OpenPosition.create_from_hash!(params['record'])
            add_last_updated_to_push_messages(:open_position, "current")
            open_positon
        end
    end

    # PUT /api/open_positions
    def update
        rescue_validation_error_or_render_record(is_admin: current_ability.can?(:manage, OpenPosition)) do
            open_position = OpenPosition.update_from_hash!(params['record'], {is_admin: current_ability.can?(:manage, OpenPosition)})
            update_subscription_if_necessary(open_position)
            add_last_updated_to_push_messages(:open_position, "current")
            open_position
        end
    rescue OpenPosition::OldVersion
        instance = OpenPosition.find(params['record']['id'])
        add_last_updated_to_push_messages(:open_position, "current")
        @meta['open_position'] = instance.as_json({is_admin: current_ability.can?(:manage, OpenPosition)})

        # We render :not_acceptable instead of :conflict because only admins will get this
        # error right now, so just let the default error handling handle it. If we ever do conflict
        # detection for teammates then we'll need to render :conflict and handle the error in the UI
        # at the callsite
        render_error_response("Position has been saved more recently. Please reload.", :not_acceptable)
    end

    # DELETE /api/open_positions/1
    def destroy
        open_position = OpenPosition.find(params['id'])
        if open_position.hiring_relationships.any?
            render_error_response("Positions linked to hiring relationships cannot be deleted", :not_acceptable)
            return false
        end
        delete_record(OpenPosition, params['id'])
    end

    # GET /api/send_curation_email/1
    def send_curation_email
        if current_ability.cannot?(:manage, OpenPosition)
            render_unauthorized_error
            return false
        end

        open_position = OpenPosition.find(params['id'])
        hiring_manager_id = params['hiring_manager_id']

        # If an email to the hiring manager was triggered within the last 24 hours then don't
        # create an event and let the admin user know.
        # Note: We keep events in Postgres for three days before offloading to Redshift, so this check is fine for our purposes.
        last_event_ts = Event.where(event_type: 'open_position:send_curation_email', user_id: hiring_manager_id).maximum(:created_at).to_timestamp
        if last_event_ts && (Time.now - 24.hours).to_timestamp < last_event_ts
            render_error_response("Curation email has been triggered within the last 24 hours", :not_acceptable)
            return false
        end

        event = open_position.trigger_curation_email(hiring_manager_id)
        render_successful_response(event.as_json.merge({
            created_at: event.created_at.to_timestamp
        }))
    end

    def update_subscription_if_necessary(open_position)
        new_value_for_cancel_at_period_end = params[:meta] && params[:meta][:new_value_for_cancel_at_period_end]
        return if new_value_for_cancel_at_period_end.nil?
        # before making any changes here, read the comment at OpenPosition#log_archived_or_renewal_canceled_event
        open_position.subscription.update_cancel_at_period_end!(new_value_for_cancel_at_period_end)
        set_careers_push_messages
    end

    def filters
        params.permit(:filters => [:hiring_manager_id])[:filters]
    end

    def authorize_index_with_params
        if current_ability.cannot?(:index_open_positions, params)
            render_unauthorized_error
            false
        else
            true
        end
    end

    def authorize_create_or_update_with_params
        if current_ability.cannot?(:create_or_update_open_positions, params)
            render_unauthorized_error
            false
        else
            true
        end
    end

    def apply_order(query)
        sort = params[:sort]&.first # can support multiple later if we need

        if sort == 'RECOMMENDED_FOR_CANDIDATE'
            query = query.order_by_recommended(candidate_or_id: @candidate)
        else
            direction = params[:direction]

            # Note that we have to translate created_at and updated_at in case we join other
            # tables to avoid an ambiguous column error.
            if sort == 'created_at'
                sort = 'open_positions.created_at'
            elsif sort == 'updated_at'
                sort = 'open_positions.updated_at'
            elsif sort == 'hiring_manager_email'
                sort = 'hiring_managers_custom.email'
            elsif sort == 'hiring_manager_company_name'
                sort = 'professional_organization_options.text'
            elsif sort == 'num_unreviewed_and_unrejected_interests'
                sort = 'NULLIF(COUNT(candidate_position_interests.id), 0)'
            elsif sort == 'status'
                sort = "CASE
                    WHEN archived = true THEN 'archived'
                    WHEN archived = false AND featured = true THEN 'live'
                    WHEN archived = false AND featured = false THEN 'drafted'
                    END"
            end

            if sort.present?
                query = query.order(Arel.sql("#{sort} #{direction} NULLS LAST"))
            end
        end

        query = query.order(*OpenPosition::DEFAULT_ORDERS)

        query
    end

    def api_options
        {
            fields: params[:fields],
            except: params[:except],
            is_admin: current_ability.can?(:manage, OpenPosition)
        }
    end

    def preload_open_positions_for_index(query)
        query, total_count = query.paginate_fastly(
            limit: params[:limit],
            offset: params[:offset],
            page: params[:page],
            max_total_count: params[:max_total_count]
        )

        # If our filters involved a candidate then tag the positions as recommended
        # so that the client will know.
        if @candidate.present?
            query = query.select_recommended(candidate_or_id: @candidate)
        end

        # since we are generating a custom select statement in `select_recommended`, we cannot
        # use `includes`.  This is because `includes` might decide to do a join, which would
        # then remove the custom select. See https://github.com/rails/rails/issues/15185. (There is
        # a gem that supposedly fixes this, but as of now, it doesn't support rails 6, and is a bit
        # scary anyway.)
        open_positions = query.select('open_positions.*').to_a
        ActiveRecord::Associations::Preloader.new.preload(open_positions, OpenPosition.includes_needed_for_json_eager_loading(self.api_options))

        @meta['total_count'] = total_count
        open_positions
    end
end