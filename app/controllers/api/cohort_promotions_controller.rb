class Api::CohortPromotionsController < Api::ApiCrudControllerBase

    authorize_resource

    # POST /api/cohort_promotions
    def create
        rescue_validation_error_or_render_record do
            @cohort = CohortPromotion.create!(permitted_params)
            @cohort
        end
    end

    # PATCH/PUT /api/cohort_promotions

    def update
        rescue_validation_error_or_render_record  do
            render_error_if_not_found(CohortPromotion, params['record']['id'] ) do |cohort_promotion|
                cohort_promotion.update!(permitted_params)
                cohort_promotion
            end
        end
    end

    def permitted_params
        params.require(:record).permit(:id, :cohort_id)
    end

end
