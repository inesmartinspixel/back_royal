class Api::CandidatePositionInterestsController < Api::ApiCrudControllerBase
    include Api::UnloadedChangeDetector

    before_action :verify_allowed_index_params, :only => [:index]
    before_action :verify_create_params, :only => [:create]
    before_action :verify_update_params, :only => [:update]

    before_action(:only => [:create, :update]) do
        add_last_updated_to_push_messages(:candidate_position_interest, "before")
    end

    #####################
    # Controller Actions
    #####################

    def index
        results = preload_for_index.map do |interest|
            anonymize = false
            unless current_ability.can?(:manage, CandidatePositionInterest) || current_user_id == interest.candidate_id
                anonymize = current_user.hiring_team.should_anonymize_interest?(interest)
            end

            interest.as_json({
                fields: params[:fields],
                except: params[:except],
                career_profile_options: {
                    anonymize: anonymize
                }
            })
        end.flatten

        add_last_updated_to_push_messages(:candidate_position_interest, "current")
        render_record_hashes(results, 'candidate_position_interests')
    end

    def preload_for_index
        options = {
            fields: params[:fields],
            except: params[:except],
            is_admin: current_ability.can?(:manage, OpenPosition)
        }
        filters = params[:filters] || {}
        query = CandidatePositionInterest

        includes = CandidatePositionInterest.includes_needed_for_json_eager_loading(options)
        query = query.includes(*includes) unless includes.empty?

        if filters.empty?
            query = query.where('TRUE')
        end

        if filters.key?(:hiring_manager_status_not)
            query = query.where.not(hiring_manager_status: filters[:hiring_manager_status_not])
        end

        if filters.key?(:candidate_id)
            query = query.where(candidate_id: filters[:candidate_id])
        end

        if filters.key?(:hiring_manager_id)
            query = query.joins(:open_position).where(open_positions: { hiring_manager_id: filters[:hiring_manager_id] })
        end

        if filters.key?(:hiring_team_id)
            query = query.joins(:open_position => :hiring_manager).where(open_positions: {users: {hiring_team_id: filters[:hiring_team_id]}})
        end

        if filters.key?(:open_position_id)
            query = query.where(open_position_id: filters[:open_position_id])
        end

        if filters.key?(:candidate_status)
            query = query.where(candidate_status: filters[:candidate_status])
        end

        if filters.key?(:candidate_status_not)
            query = query.where.not(candidate_status: filters[:candidate_status_not])
        end

        query.to_a
    end

    def create
        rescue_validation_error_or_render_record(career_profile_options: {anonymize: false}) do
            @candidate_position_interest = CandidatePositionInterest.create_or_update_from_hash!(@create_params, permitted_meta_params)
            add_last_updated_to_push_messages(:candidate_position_interest, "current")
            add_num_recommended_positions_to_push_messages
            @candidate_position_interest
        end
    end

    def update
        rescue_validation_error_or_render_record(career_profile_options: {anonymize: false}) do
            meta = permitted_meta_params.merge({is_admin: current_ability.can?(:manage, CandidatePositionInterest)})
            instance = CandidatePositionInterest.update_from_hash!(@update_params, meta, nil, current_user)
            add_last_updated_to_push_messages(:candidate_position_interest, "current")
            add_num_recommended_positions_to_push_messages
            instance
        end
    rescue CandidatePositionInterest::OldVersion
        instance = CandidatePositionInterest.find(@update_params[:id])
        @meta['candidate_position_interest'] = instance.as_json(career_profile_options: {anonymize: false})
        render_error_response("Interest has been saved more recently", :conflict)
    end

    def batch_update
        if !current_ability.can?(:manage, CandidatePositionInterest)
            render_unauthorized_error
            return false
        end

        begin
            instances = []
            in_transaction_with_editor_tracking do
                meta = permitted_meta_params.merge({is_admin: current_ability.can?(:manage, CandidatePositionInterest)})
                params[:records].each do |record|
                    @id = record[:id]
                    instance = CandidatePositionInterest.update_from_hash!(record.permit(permitted_keys), meta, nil, current_user)
                    instances.push(instance)
                end
            end
            render_records(instances.to_a, {career_profile_options: {anonymize: false}})
        rescue CandidatePositionInterest::OldVersion
            instance = CandidatePositionInterest.find(@id)
            @meta['candidate_position_interest'] = instance.as_json(career_profile_options: {anonymize: false})
            render_error_response("Interest has been saved more recently", :conflict)
        rescue ActiveRecord::RecordInvalid => e
            render_error_response(e.message, :not_acceptable)
        end

        # Leaving this here in case we ever feel the need to optimize this endpoint more.
        # # Ex. ('id1'::uuid, 0), ('id2::uuid, 1'), ('id3'::uuid, 2)
        # values_string = params[:ids].each_with_index.map { |id, i| "('#{id}'::uuid, #{i})" }.join(',')

        # # See https://stackoverflow.com/a/18799497/1747491
        # CandidatePositionInterest.connection.execute(%Q~
        #     UPDATE candidate_position_interests AS cpi SET
        #         hiring_manager_priority = new.hiring_manager_priority
        #     FROM (VALUES#{values_string}) AS new(id, hiring_manager_priority)
        #     WHERE cpi.id = new.id;
        # ~)
    end

    #####################
    # Parameter Support
    #####################

    def verify_allowed_index_params
        unless current_ability.can?(:index_candidate_position_interests, params)
            render_unauthorized_error
            return false
        end
    end

    def verify_create_params
        if current_user.has_role?(:admin)
            @create_params = permitted_write_params
        elsif current_ability.can?(:create_candidate_position_interests, params)
            @create_params = permitted_write_params.except(:hiring_manager_status, :admin_status, :hiring_manager_priority)
        else
            render_unauthorized_error
            return false
        end

        return true
    end

    def verify_update_params
        id = params['record'] && params['record']['id']
        @candidate_position_interest = CandidatePositionInterest.find(id)

        if current_user.has_role?(:admin)
            @update_params = permitted_write_params
        elsif current_ability.can?(:update, @candidate_position_interest)
            is_candidate = @candidate_position_interest.candidate_id == current_user.id
            is_hiring_manager_or_teammate = @candidate_position_interest.open_position.hiring_manager_id == current_user.id ||
                @candidate_position_interest.open_position.hiring_manager.hiring_team_id == current_user.hiring_team_id

            if is_candidate
                # The client only supports updating the cover_letter when the user reapplies to a position.
                # We don't have any logic in ability.rb that enforces this, so right now the server allows
                # the candidate to update the cover_letter on the candidate_position_interest whenever
                # they want, but we're okay with this for now; however, we may want to revisit this in
                # the future.
                @update_params = permitted_write_params.slice('candidate_status', 'id', 'cover_letter')
            elsif is_hiring_manager_or_teammate
                @update_params = permitted_write_params.slice('hiring_manager_status', 'id', 'updated_at')
            else
                raise "How did we get here?"
            end
        else
            render_unauthorized_error
            return false
        end

        return true
    end

    def permitted_meta_params
        current_user.has_role?(:admin) && params[:meta].present? ? params.require(:meta).permit([:last_curated_at]) : {}
    end

    def permitted_write_params
        params.require(:record).permit(permitted_keys)
    end

    def permitted_keys
        [:id, :updated_at, :candidate_id, :open_position_id, :candidate_status, :hiring_manager_status, :admin_status, :hiring_manager_priority, :last_updated_at_by_non_admin, {:cover_letter => [:content, :created_at]}]
    end

end