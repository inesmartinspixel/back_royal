class Api::ProjectProgressTimelinesController < Api::ApiCrudControllerBase

    authorize_resource class: ActivityTimeline::ProjectProgressTimeline

    def show
        instance = ActivityTimeline::ProjectProgressTimeline.new(params['id']).preload
        render_records([instance])
    end

    def collection_key(record)
        'project_progress_timelines'
    end
end