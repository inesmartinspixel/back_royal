class Api::CouponsController < Api::ApiCrudControllerBase
    authorize_resource :class => false
    skip_before_action :verify_signed_in, only: [:show]
    skip_before_action :require_guid_id

    def show

        # Validates with allowable `coupon_prefix` given a `cohort_id` on the `meta`

        id = params['id']

        # Get Cohort information
        is_valid_for_cohort = false
        cohort_id = params[:filters] && params[:filters][:cohort_id]
        if cohort_id
            cohort = Cohort.find_by_id(cohort_id)
        end

        # Continue if valid Cohort is found, and prefix matches
        if cohort
            id = nil unless cohort.published_version&.valid_coupon_id?(id)

            # Retrieve the coupon and default to nil otherwise
            begin
                coupon = Stripe::Coupon.retrieve(id)
            rescue Stripe::InvalidRequestError
                coupon = nil
            end

        end

        # Render the coupons or lack thereof
        contents = coupon ? [coupon] : []
        render_record_hashes(contents, 'coupons')
    end

end
