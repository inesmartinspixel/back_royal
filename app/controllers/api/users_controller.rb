class Api::UsersController < Api::ApiCrudControllerBase

    # sync with mailchimp on user create and update
    load_and_authorize_resource
    include Devise::Controllers::SignInOut
    include ControllerMixins::HiringManagerFiltersMixin
    include ControllerMixins::CareerProfilesFiltersMixin

    helper_method :sort_column, :sort_direction, :permitted_query_params

    def index
        # NOTE: I don't think we've been very careful about sql injection attacks here.  If this is
        # ever available to non-admins, we need to look at that.
        @role_id = user_index_params['role_id']
        @hiring_application_status = [user_index_params['hiring_application_status']].compact
        @group_name = user_index_params['group_name']
        @institution_id = user_index_params['institution_id']
        @cohort_id = [user_index_params['cohort_id']].compact # if no cohort_id param is passed along, remove nil from the array
        @cohort_status = [user_index_params['cohort_status']].compact # if no cohort_status param is passed along, remove nil from the array
        @last_cohort_status = [user_index_params["last_cohort_status"]].compact # if no last_cohort_status param is passed along, remove nil from the array
        @graduation_status = [user_index_params['graduation_status']].compact # if no graduation_status param is passed along, remove nil from the array
        @freetext_search_param = user_index_params['freetext_search_param'].downcase if user_index_params['freetext_search_param']
        @can_edit_career_profile = user_index_params['can_edit_career_profile']
        @select_career_profile = user_index_params['select_career_profile']
        @select_cohort_user_progress_record = user_index_params['select_cohort_user_progress_record']
        @select_project_progress = user_index_params['select_project_progress']
        @select_exam_progress = user_index_params['select_exam_progress']
        @select_education_experiences = user_index_params['select_education_experiences']
        @select_transcripts = user_index_params['select_transcripts']
        @select_subscriptions = user_index_params['select_subscriptions']
        @has_cohort_application = user_index_params["has_cohort_application"]
        @select_professional_organization = user_index_params['select_professional_organization']
        @select_signable_documents = user_index_params['select_signable_documents']
        @do_not_create_relationships = user_index_params['do_not_create_relationships']
        @available_for_relationships_with_hiring_manager = user_index_params['available_for_relationships_with_hiring_manager']
        @identifiers = params['identifiers']
        @application_registered = user_index_params["application_registered"]
        @hiring_team_id = user_index_params['hiring_team_id']
        @available_for_hiring_team = user_index_params["available_for_hiring_team"]
        @select_active_deferral_link = user_index_params['select_active_deferral_link']

        # Note: We have some top-level params that should have been passed in as filters. Going forward we will be
        # transitioning to that structure since it aligns with how we do filtering in other areas of our backend and
        # the front-end. I am leaving the current ones alone for now.
        if !params["filters"].nil?
            @name = params["filters"]["name"]
            @email = params["filters"]["email"]
            @cohort_name = params["filters"]["cohort_name"]
            @cohort_id = Array.wrap(params["filters"]["cohort_id"]) # Note: We switched to passing an array straight into the filters version
            @cohort_status = Array.wrap(params["filters"]["cohort_status"]) # Note: We switched to passing an array straight into the filters version
            @last_cohort_status = Array.wrap(params["filters"]["last_cohort_status"]) # Note: We switched to passing an array straight into the filters version
            @city_state = params["filters"]["city_state"]
            @career_profile_complete = params["filters"]["career_profile_complete"]
            @can_edit_career_profile = params["filters"]["can_edit_career_profile"]
            @career_profile_disabled = params["filters"]["career_profile_disabled"]
            @has_profile_feedback = params["filters"]["has_profile_feedback"]
            @profile_updated_after_feedback_sent = params["filters"]["profile_updated_after_feedback_sent"]
            @career_profile_employment_types_of_interest = params["filters"]["career_profile_employment_types_of_interest"]
            @hiring_relationship_statuses = params["filters"]["hiring_relationship_statuses"]
            @career_profile_active_status = params["filters"]["career_profile_active_status"]
            @career_profile_location = params["filters"]["career_profile_location"]
            @career_profile_last_confirmed_at_by_student = params["filters"]["career_profile_last_confirmed_at_by_student"]
            @career_profile_last_confirmed_at_by_internal = params["filters"]["career_profile_last_confirmed_at_by_internal"]
            @has_hiring_application = params["filters"]["has_hiring_application"]
            @hiring_application_status = params["filters"]["hiring_application_status"]
            @available_for_all_hiring_teams = params["filters"]["available_for_all_hiring_teams"]
            @application_registered = params["filters"]["application_registered"]
            @interested_in_open_position = params["filters"]["interested_in_open_position"]

            # Filter params that should mirror the hiring manager UX on the Browse page
            @career_profile_places = params["filters"]["career_profile_places"]
            @career_profile_only_local = params["filters"]["career_profile_only_local"]
            @career_profile_roles = params["filters"]["career_profile_roles"]
            @career_profile_industries = params["filters"]["career_profile_industries"]
            @career_profile_skills = params["filters"]["career_profile_skills"]
            @career_profile_keyword_search = params["filters"]["career_profile_keyword_search"]
            @career_profile_years_experience = params["filters"]["career_profile_years_experience"]

            # We group the graduation statuses and the cohort statuses together on the client-side,
            # so we have to check if the cohort_status filter is supposed to be a cohort status or graduation status.
            # Since we support multiple statuses in this field now, it's possible that you might search for combos like
            # GRADUATED_WITH_HONORS + EXPELLED, which we choose to interpret as the OR of the two conditions below.
            if !@cohort_status.blank?
                # we treat pending as a cohort status, not graduation status for filtering purposes
                valid_filterable_graduation_statuses = CohortApplication.graduation_statuses.select { |status| status != 'pending' }
                @graduation_status = valid_filterable_graduation_statuses & @cohort_status
                @cohort_status = @cohort_status - @graduation_status
                @search_for_either_graduation_or_cohort_status = true # tell the filter below how to behave
            end
        end

        @per_page = params[:per_page] ? params[:per_page].to_i : 100
        @page = params[:page] ? params[:page].to_i : 1

        ids, user_count = get_ids_for_index
        json = get_json_for_index(ids)

        @meta = {
            total_count: user_count,
            per_page: @per_page,
            page: @page
        }

        unless params['do_not_load_extra_data']
            @global_roles = Role.where("resource_id is NULL and name in ('learner', 'interviewer', 'admin', 'editor', 'super_editor')")
            # Note @scoped_roles.as_json will magically replace the lesson_id in the
            # resource_id field with lesson_id the frontend needs. See role.rb
            @scoped_roles = Role.where("resource_id is not NULL")

            @available_group_name = AccessGroup.pluck('name').sort

            @available_institutions = Institution.includes(:access_groups => :lesson_stream_locale_packs)
                                        .reorder('name asc')

            # There is so much stuff in the regular cohorts json that we don't need.
            # It's easier to just implement an as_json override here with the things
            # we do need
            # Note: We have since improved the as_json for cohorts. But, this still needs more
            # than we currently send back with BASIC_FIELDS; so just leave it as-is.
            cohort_fields = [:name, :title, :id, :start_date, :program_type, :stripe_plans, :scholarship_levels].map(&:to_s)
            @available_cohorts = Cohort.joins(:content_publishers)
                                    .includes(:slack_rooms, :access_groups => :lesson_stream_locale_packs)
                                    .select(*cohort_fields)
                                    .sort_by(&:name)
                                    .map do |cohort|
                                        attrs = cohort.attributes.slice(*cohort_fields)
                                        attrs.merge({
                                            groups: cohort.access_groups.map(&:as_json),
                                            slack_rooms: cohort.slack_rooms.map(&:as_json),
                                            start_date: attrs["start_date"].to_timestamp
                                        })
                                    end

            @meta.merge!({
                global_roles: Role.as_json(@global_roles),
                scoped_roles: Role.as_json(@scoped_roles),
                available_groups: @available_group_name.as_json,
                institutions: @available_institutions.as_json,
                cohorts: @available_cohorts,
                cohort_promotions: CohortPromotion.includes(:cohort).all.map(&:as_json),
            })
        end
        render_contents_json_string(User, json)

    end

    # show is called by the Admin Applicants page when using an ?id parameter since the requested
    # user is probably not in the initial page of results and thus needs to be fetched.
    # When users view their profile page, they hit /users/edit, which is managed by
    # devise without our custom overrides
    def show
        render_records([User.find(user_get_params[:id])])
    end

    def create
        group_params = permitted_post_params[:groups]
        params[:record].delete(:groups)
        role_params = permitted_post_params[:roles]
        params[:record].delete(:roles)

        institution_params = permitted_post_params[:institutions]
        params[:record].delete(:institutions)

        active_institution_param = permitted_post_params[:active_institution]
        params[:record].delete(:active_institution)

        params[:record].delete(:cohorts_users)
        professional_organization_params = permitted_post_params[:professional_organization]
        params[:record].delete(:professional_organization)

        @user_resource = User.new(permitted_post_params)
        @user_resource.provider = "email"

        # honor devise configuration for case_insensitive_keys
        if User.case_insensitive_keys.include?(:email)
            @user_resource.email = permitted_post_params[:email].downcase
        else
            @user_resource.email = permitted_post_params[:email]
        end
        @user_resource.uid = @user_resource.email

        rescue_validation_error_or_render_record({}) do

            @user_resource.save!

            process_institution_updates(@user_resource, {institutions: institution_params})
            process_active_institution_update(@user_resource, {active_institution: active_institution_param})
            process_role_updates(@user_resource, {roles: role_params})
            process_group_updates(@user_resource, {groups: group_params})
            process_professional_organization_updates(@user_resource, {professional_organization: professional_organization_params})
            process_cohort_application_and_subscription_updates(@user_resource)
            @user_resource.register_content_access
            @user_resource.save!
            @user_resource

        end

    end

    def log_in_as
        update_params = permitted_post_params

        # since adding sessions, we must ensure that any sessions are cleared before impersonation
        reset_session

        if current_ability.can?(:manage, User) # admin users have full manage permissions
            @user_resource = User.where(id: update_params[:logged_in_as_user_id])

            if @user_resource.size == 1
                @user_resource = @user_resource.first

                Event.create_server_event!(
                    SecureRandom.uuid,
                    current_user.id,
                    'logged_in_as_user',
                    {
                        label: update_params[:logged_in_as_user_id]
                    }
                ).log_to_external_systems

                Event.create_server_event!(
                    SecureRandom.uuid,
                    update_params[:logged_in_as_user_id],
                    'impersonated_by_admin',
                    {
                        label: current_user_id
                    }
                ).log_to_external_systems

                # use devise to sign the user in
                # Note: The function `bypass_sign_in` will prevent the tracking module from updating fields like
                # sign_in_count, last_sign_in_at, etc. See http://www.rubydoc.info/github/plataformatec/devise/master/Devise/Controllers/SignInOut#bypass_sign_in-instance_method
                bypass_sign_in(@user_resource)

                @token = @user_resource.create_token
                @user_resource.save
                @resource = @user_resource
            else
                render_not_found_error(User, update_params[:logged_in_as_user_id])
                return
            end
        else
            render_unauthorized_error
            return
        end


        respond_to do |format|
            format.json { render json: {
                data: @user_resource.token_validation_response
            }}
        end

    end

    # POST /api/users/:id/upload_avatar
    def upload_avatar
        render_error_if_not_found(User, params[:id]) do |record|

            avatar = AvatarAsset.new({
                :file => params['avatar_image']
            })

            begin
                in_transaction_with_editor_tracking do
                    record.avatar = avatar
                    record.save!
                end
            rescue ActiveRecord::RecordInvalid => e
                return render_error_response(e.message, :not_acceptable)
            end

            render_successful_response({'avatar' => record.avatar})
        end
    end

    # POST /api/users/:id/upload_resume
    def upload_resume
        render_error_if_not_found(User, params[:id]) do |record|
            asset = S3Asset.new({
                :file => params['resume_file'],
                :file_file_name => params['resume_file_name'],
                :directory => "resumes/"
            })

            begin
                asset.save!
            rescue ActiveRecord::RecordInvalid => e
                return render_error_response(e.message, :not_acceptable)
            end

            render_successful_response({'resume' => asset})
        end
    end

    def update
        update_params = permitted_post_params
        @user_resource = User.find(update_params[:id])

        if @user_resource.nil?
            render_not_found_error(User, update_params[:id])
            return
        elsif current_ability.cannot?(:admin_without_delete, User) && @user_resource != current_user
            render_unauthorized_error
            return
        end

        is_admin = current_ability.can?(:admin_without_delete, User)

        rescue_validation_error_or_render_record({
            # This fields array should contain fields/associations that may change on the server
            # as an indirect result of the update. These fields/associations will be sent back
            # to the client so it can update the state of the user.
            :fields => [

                # Because of locale_packs, this can change on the server.  For an example of the potential issue:
                #
                # 1. unfavorite a stream that has multiple locales in the browser
                # 2. unfavorite some other stream
                # 3. refresh the page and the first stream will be back
                'favorite_lesson_stream_locale_packs',

                # since this is updated due to something sent in the metadata (in the admin UI),
                # it won't be changed in the actual json in the client unless we send it back down.
                'subscriptions',

                # when the user has their identity verified, we destroy their s3_identification_asset
                's3_identification_asset',

                # this can change on the server, needed to make a second update
                'updated_at',

                # maybe we need more comments about why these are included?
                'id', 'cohort_applications', 'can_edit_career_profile',

                # this can become true when all required transcripts are approved or waived
                'transcripts_verified',

                # updating official_transcript_required_override can cause server changes that need to be reflected back
                'career_profile',

                # cohort_applications might ensure institution state
                'institutions', 'active_institution'
            ],
            :is_admin => is_admin
        }) do
            if is_admin # admin users have full manage permissions
                perform_update(@user_resource, update_params, true)
            elsif @user_resource == current_user
                perform_update(@user_resource, update_params, false)

                # These next 2 lines are here to prevent an edge-case like this:
                # 1. a ping request gets sent by the client, and has not returned
                # 2. the client initiates a user update that will change something
                #       that comes down in the ping (e.g. active playlist)
                # 3. the ping request returns, and the interceptor updates the client-side user object
                # 4. the update request goes and succeeds, but the client-side user still has the older
                #       value from the ping.  By adding in the user info here, we make sure the client
                #       gets the latest info.  This is a bit "wrong" since it's best practice for
                #       an update endpoint to return the same things that were sent up to it, but it should work.
                current_user.reload
                set_user_push_messages
            else
                raise "How did we get here?"
            end

            @user_resource
        end

    end

    def batch_update
        unless current_ability.can?(:batch_update, User)
            return render_unauthorized_error
        end


        updater = User::BatchUpdater.new(current_user, params['user_ids'], params.slice(
            'cohort_id',
            'cohort_status',
            'graduation_status',
            'can_edit_career_profile',
            'application_registered'
        ))
        updater.exec

        errors = updater.errors
        if errors.any?
            render_error_response("", :not_acceptable, {"errors" => errors})
        else
            json = get_json_for_index(params['user_ids'])
            render_contents_json_string(User, json)
        end
    end

    def batch_update_grades
        unless current_ability.can?(:manage, User)
            return render_unauthorized_error
        end
        changed_user_ids = []

        in_transaction_with_editor_tracking do
            ids = params[:records].map { |record| record[:id] }
            users_by_id = User.includes(:project_progresses, :cohort_applications).find(ids).index_by(&:id)

            params[:records].each do |attrs|
                user_attrs = ["id"]
                updatable_attrs = [:score, :waived, :marked_as_passed, :status, :id_verified]
                permitted_attrs = attrs.permit(
                    *user_attrs,
                    "project_progress" => ["requirement_identifier"] + updatable_attrs
                )
                user = users_by_id[permitted_attrs[:id]]
                changed = false

                permitted_attrs['project_progress'].each do |progress_attrs|
                    if ProjectProgress.create_or_update!(user, current_user, progress_attrs)
                        changed = true
                    end
                end

                if changed
                    changed_user_ids << user.id
                end
            end
        end

        changed_users = User.includes(:project_progresses, :cohort_applications).select(:id).find(changed_user_ids)

        # using project_progress in the json instead of project_progresses because it lines
        # up with the way we call exam_progress in the json.  No way to be consistent with both
        render_record_hashes(changed_users.map { |u|
            {
                id: u.id,
                project_progress: u.project_progresses,

                # since the final_score may have changed due to a change to a project score, we
                # need to get the new final_score from the cohort applications
                cohort_applications: u.cohort_applications.as_json(is_admin: true)
            }.as_json
        }, 'users')
    end

    def batch_update_cohort_enrollment_status
        unless current_ability.can?(:manage, User)
            return render_unauthorized_error
        end

        unless params[:meta] && params[:meta][:cohort_id]
            return render_error_response("Must specify a cohort_id in the meta params")
        end

        ids = params[:records].map { |record| record[:id] }
        users_by_id = User.find(ids).index_by(&:id)

        in_transaction_with_editor_tracking do
            params[:records].each do |attrs|
                user_attrs = ["id", "english_language_proficiency_documents_approved"]
                permitted_attrs = attrs.permit(
                    *user_attrs
                )
                user = users_by_id[permitted_attrs[:id]]

                # A separate API exists for updating user_id_verifications, so rather than updating
                # them via update along with the other user_attrs, we update them separately.
                user_id_verification_hashes = (attrs['user_id_verifications'] || []).map { |e| e.permit("id", "cohort_id", "id_verification_period_index", "user_id", "verification_method") }
                user.update_user_id_verifications_from_hashes(user_id_verification_hashes, current_user)
                user.update(permitted_attrs.slice(*user_attrs))
            end
        end

        render_records(users_by_id.values, {fields: ['id', 'user_id_verifications']})
    end

    def raise_if_old_version(record, incoming_updated_at)

        # if there is no original version of the record, then there is no issue
        return if record.nil?

        # This method is only called when is_admin is true, because
        # we think the only issues are with admins overriding changes.  Users are relatively
        # limited in what they can update, so hopefully we are not overriding any changes.
        # If an admin is updating zirself, then they are probably in the app behaving like
        # a regular user, so we return here.
        return if record.id == current_user_id

        # if the client had the current version of this record, then there is no problem.
        # We need to allow for a second of difference because the users controller creates the timestamp
        # in postgresql, instead of using `to_i`
        return if (record.updated_at.to_i - incoming_updated_at).abs <= 1

        # rescue_validation_error_or_render_record will handle this specially, rendering a 406 error
        raise OldVersionException.new("Changes have been made to this user since you last loaded them.  Please refresh and try again.")
    end

    def perform_update(user, params, is_admin)
        if params.key?('active_playlist_locale_pack_id')
            params.delete('active_playlist_id')
        end

        # If this param has not explicity changed, do not perform the assignment.
        # process_cohort_application_and_subscription_updates may set can_edit_career_profile on the model,
        # if an admin accepts the user's cohort application. If this happens, we should
        # not override the model.
        # see https://trello.com/c/kormW6A7
        if !params[:can_edit_career_profile].nil? && user.can_edit_career_profile == params[:can_edit_career_profile]
            params.delete('can_edit_career_profile')
        end

        if is_admin
            raise_if_old_version(user, self.params[:record][:updated_at])

            params = process_role_updates(user, params)
            params = process_group_updates(user, params)
            params = process_institution_updates(user, params)
            params = process_active_institution_update(user, params)
            params = process_identity_verified_update(user, params)
            params = process_professional_organization_updates(user, params)

            process_cohort_application_and_subscription_updates(user)
            process_refund(user) # this has to come after process_cohort_application_and_subscription_updates
            process_hiring_application_updates(user)
            process_career_profile_updates(user)
            process_education_experience_updates(user, is_admin)

            if user.should_have_transcripts_auto_verified?
                user.update!(transcripts_verified: true)
            end
        end

        params = process_favorite_lesson_stream_locale_packs_updates(user, params)

        if !is_admin
            params = params.slice(
                'has_seen_welcome',
                'has_logged_in',
                'has_seen_mba_submit_popup',
                'has_seen_careers_welcome',
                'pref_decimal_delim',
                'pref_locale',
                'pref_show_photos_names',
                'pref_keyboard_shortcuts',
                'pref_sound_enabled',
                'active_playlist_locale_pack_id',
                'has_seen_accepted',
                'phone',
                'phone_extension',
                'name',
                'job_title',
                'avatar_url',
                'has_seen_featured_positions_page',
                'pref_one_click_reach_out',
                'has_seen_hide_candidate_confirm',
                'has_seen_first_position_review_modal',
                'has_seen_student_network',
                'has_seen_short_answer_warning',
                'pref_positions_candidate_list',
                'has_drafted_open_position',
                'pref_allow_push_notifications',
                'english_language_proficiency_comments',
                'english_language_proficiency_documents_type',
                'recommended_positions_seen_ids',
                'has_seen_hiring_tour',
                'student_network_email',

                # mailing address fields
                'address_line_1',
                'address_line_2',
                'city',
                'state',
                'zip',
                'country',
                'phone',

                *User::NOTIFICATIONS.keys
            )
        end

        user.assign_attributes(params)
        user.save!
    end

    def destroy
        authorize! :destroy, current_user, :message => 'Not authorized as an administrator.'
        delete_record(User, user_destroy_params[:id])
    end

    # GET /api/users/:id/transcript_data
    def show_transcript_data
        if params[:id] != current_user.id && !current_user.has_role?(:admin)
            render_unauthorized_error
            return false
        end

        user = User.find(params[:id])
        response_json = {
            required_stream_locale_pack_ids: user.relevant_cohort.get_required_stream_locale_pack_ids,
            optional_stream_locale_pack_ids: user.relevant_cohort.get_optional_stream_locale_pack_ids,
            avg_test_score: CohortUserProgressRecord.where(user_id: user.id).first&.avg_test_score
        }

        respond_to do |format|
            format.json {
                render json: response_json,
                status: :ok
            }
        end
    end

    def sort_column
        column = params[:sort] ? params[:sort].first : nil

        # Be explicit if sorting by users.created_at
        if(column == 'created_at')
            column = 'users.created_at'
        end

        allowed_columns = User.column_names +
            ['users.created_at', 'roles.name', 'career_profiles.created_at', 'career_profiles.updated_at', 'career_profiles.do_not_create_relationships',
                'career_profiles.last_calculated_complete_percentage', 'formatted_location.city_state', 'hiring_relationships.hiring_manager_priority',
                'career_profiles.profile_feedback', 'career_profiles.last_confirmed_at_by_student', 'career_profiles.interested_in_joining_new_company']
        allowed_columns.include?(column) ? column : "users.name"
    end

    def sort_direction
        %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
    end

    private

    def permitted_query_params
        permitted_index_params
    end

    def user_index_params
        params.permit("group_name", "role_id", "hiring_application_status", "hiring_team_id", "institution_id",
            "cohort_id", "cohort_status", "last_cohort_status", "graduation_status",
            "freetext_search_param", "identifiers", "can_edit_career_profile", "can_purchase_paid_certs",
            "select_career_profile", "select_cohort_user_progress_record",  "select_project_progress",
            "select_exam_progress", "select_subscriptions", "select_active_deferral_link",
            "has_cohort_application", "do_not_create_relationships",
            "available_for_relationships_with_hiring_manager", "available_for_hiring_team", "select_professional_organization", "application_registered",
            "select_education_experiences", "select_transcripts", "select_signable_documents",

            "page", "per_page", "sort", "direction", "filters" => ["name", "email", "cohort_id", "cohort_name", "cohort_status", "city_state"]) if (!params.blank?)
    end

    def user_get_params
        params.permit(:id)
    end

    def user_destroy_params
        params.permit(:id)
    end

    def user_groups_post_params
        params.require(:user).permit(:id, :group_name)
    end

    def user_groups_destroy_params
        params.permit(:id, :group_name)
    end

    def permitted_post_params
        params.require(:record).permit("id", "logged_in_as_user_id", "name", "email", "password", "sign_up_code", "password_confirmation",
            "remember_me", "has_seen_welcome", "has_logged_in", "has_seen_mba_submit_popup", "has_seen_careers_welcome",
            "pref_decimal_delim", "pref_locale", "pref_show_photos_names", "pref_sound_enabled", "pref_keyboard_shortcuts", "pref_one_click_reach_out",
            "active_playlist_id", "active_playlist_locale_pack_id", "mba_content_lockable",
            'job_title', 'phone', 'phone_extension', 'country', "hiring_team_id", 'has_seen_accepted', 'has_seen_featured_positions_page',
            'has_seen_hide_candidate_confirm', 'has_seen_first_position_review_modal', 'has_seen_student_network', 'can_edit_career_profile',
            'can_purchase_paid_certs', 'pref_positions_candidate_list', 'identity_verified', 'avatar_url', 'fallback_program_type',
            'has_drafted_open_position', 'deactivated', 'pref_allow_push_notifications', 'english_language_proficiency_comments',
            'english_language_proficiency_documents_type', 'english_language_proficiency_documents_approved',
            'address_line_1', 'address_line_2', 'city', 'state', 'zip', 'country', 'phone', 'has_seen_short_answer_warning', 'has_seen_hiring_tour',
            'academic_hold', 'student_network_email',
            *User::NOTIFICATIONS.keys,
            'recommended_positions_seen_ids' => [],
            "professional_organization" => ["locale", "text"],
            "favorite_lesson_stream_locale_packs" => ["id"],
            "groups" => ["name"],
            "roles" => ["name", "resource_id", "resource_type"],
            "institutions" => ["id"],
            "active_institution" => {})
    end

    def process_favorite_lesson_stream_locale_packs_updates(user, params)

        # user admin page does not need to return favorite_lesson_stream_locale_packs, so
        # ensure that this is not a required parameter. if no key is found, bypass update.
        if params.key?('favorite_lesson_stream_locale_packs')

            if !params[:favorite_lesson_stream_locale_packs].nil?
                locale_pack_ids = params[:favorite_lesson_stream_locale_packs].map do |entry|
                    entry["id"]
                end
                user.favorite_lesson_stream_locale_packs = Lesson::Stream::LocalePack.where(id: locale_pack_ids)
                params.delete(:favorite_lesson_stream_locale_packs)
            else
                # Note: we are assuming here that all user API updates will be restful, and
                # if favorite_lesson_stream_locale_packs is ever empty this means the caller intended to delete all favorite_lesson_stream_locale_packs from this user
                user.favorite_lesson_stream_locale_packs = []
            end

            user.save

        end

        params
    end

    def process_group_updates(user, params)
        if params[:groups]
            user.replace_groups(params[:groups].map { |g| g['name'] })
        end
        params.delete(:groups)
        params
    end

    def process_institution_updates(user, params)
        institutions =  []
        if !params[:institutions].nil?
            params[:institutions].each do |institution|
                inst = Institution.find(institution[:id])
                institutions << inst if !inst.nil?
            end
            user.institutions = institutions
            params.delete(:institutions)
        end
        params
    end

    def process_active_institution_update(user, params)
        new_active_institution = params[:active_institution]
        if new_active_institution.present?
            new_id = new_active_institution[:id]
            user.active_institution = user.institutions.find_by_id(new_id)
        end
        params.delete(:active_institution)
        params
    end

    def sanitize_application(application, is_admin)
        application['applied_at'] = Time.at(application['applied_at'])

        # Ensure that only an admin can change the application status from pending
        if !is_admin
            application.slice('status')
        end

        application
    end

    def process_career_profile_updates(user)
        # this method is only called by admins

        # I am using self.params here because the whitelisting stuff
        # is complex and unnecessary.  The code below will cherry-pick expected params
        return unless self.params[:record].key?(:career_profile)

        attrs = self.params[:record][:career_profile]

        if !attrs.nil?
            hash = {
                "id": attrs['id'],
                "profile_feedback": attrs['profile_feedback'],
                "interested_in_joining_new_company": attrs['interested_in_joining_new_company']
            }.with_indifferent_access
            # params[:meta] should include the send_feedback_email flag used to determine if the Customer.io emails should be triggered.
            CareerProfile.update_from_hash!(hash, params[:meta])
        end
    end

    def process_refund(user)
        if params[:meta] && params[:meta][:issue_refund]
            # we do not want an error here to stop things from happening
            # or to stop the transaction from committing (see note inside
            # if error handling in issue_refund!), but we do want to
            # show an error in the client
            begin
                user.last_application.issue_refund!
            rescue Exception => err
                @meta['error_issuing_refund']  = err.message
            end
        end
    end

    def process_cohort_application_and_subscription_updates(user)
        # this method is only called by admins
        # if we are updating a cohort application in a way that will cause the subscription
        # to get destroyed, we do not want to cancel the subscription first, because that
        # can lead to `registered` getting toggled off and back on.  See https://trello.com/c/AQx1no5e
        # and the spec in users_controller_spec that references that ticket
        reason = params[:meta] && params[:meta]['subscription_cancellation_reason']

        self.params[:record][:cohort_applications]&.each do |application_params|
            cohort_application = user.cohort_applications.detect { |app| app.id == application_params['id'] }
            raise_if_old_version(cohort_application, application_params['updated_at'])
        end

        if reason && user.primary_subscription
            user.primary_subscription.cancel_and_destroy(reason: params[:meta]['subscription_cancellation_reason']) do
                process_cohort_application_updates(user)
            end
            user.subscriptions.reload
        else
            process_cohort_application_updates(user)
        end

        enrollment_action_label = params[:meta] && params[:meta]['enrollment_action_label']
        if enrollment_action_label
            ActivityTimeline::PersistedTimelineEvent.create_for_user!(user, {
                time: Time.now.to_timestamp,
                event: 'performed_action',
                text: enrollment_action_label,
                subtext: reason ? "with reason: #{reason}" : nil,
                editor_id: current_user.id,
                editor_name: current_user.name,
                category: 'enrollment'
            })
        end

        billing_transaction_attrs = params[:meta] && params[:meta]['billing_transaction_to_record']
        if billing_transaction_attrs&.slice(:amount, :provider, :provider_transaction_id, :transaction_time)&.values&.map(&:present?)&.all?
            user.billing_transactions << BillingTransaction.create!(
                transaction_time: Time.at(billing_transaction_attrs[:transaction_time]),
                transaction_type: billing_transaction_attrs[:transaction_type],
                provider: billing_transaction_attrs[:provider],
                provider_transaction_id: billing_transaction_attrs[:provider_transaction_id],
                amount: billing_transaction_attrs[:amount],
                currency: 'usd'
            )
        end
    end

    def process_cohort_application_updates(user)
        # this method is only called by admins

        # I am using self.params here because the whitelisting stuff
        # is complex and unnecessary.  CohortApplication.update_from_hash and
        # create_from_hash will cherry-pick expected params
        return unless self.params[:record].key?(:cohort_applications)

        cohort_applications = []

        self.params[:record][:cohort_applications].each do |application_params|
            if application_params[:id]
                application = CohortApplication.update_from_hash!(application_params.merge(user_id: user.id), {is_admin: true}, user)
            else
                application = CohortApplication.create_from_hash!(application_params.merge(user_id: user.id), {is_admin: true}, user)
            end
            cohort_applications << application

            # Ensure that any institution state changes are reflected back
            # to this original user object.
            # Not doing so caused a problem when reverting a biz cert redecision.
            user.association(:institutions).reset
            user.association(:institution_joins).reset
        end

        user.cohort_applications = cohort_applications
    end

    def process_hiring_application_updates(user)
        # this method is only called by admins

        # I am using self.params here because the whitelisting stuff
        # is complex and unnecessary.  The code below will cherry-pick expected params
        return unless self.params[:record].key?(:hiring_application)

        attrs = self.params[:record][:hiring_application]

        if attrs.nil? && user.hiring_application
            user.hiring_application.destroy
        elsif !attrs.nil? && user.hiring_application.nil?
            HiringApplication.create!(
                status: attrs['status'],
                user_id: user.id
            )
        elsif !attrs.nil?
            user.hiring_application.status = attrs['status']
            user.hiring_application.save!
        end
    end

    def process_professional_organization_updates(user, params)
        # this method is only called by admins

        if !params[:professional_organization].blank?
            # we need to make sure that the text isn't a blank string and that a locale is present
            # in order to find the proper professional organization option to associate with the user
            if params[:professional_organization][:text].present? && !params[:professional_organization][:text].blank? && params[:professional_organization][:locale].present?
                hash = params[:professional_organization].to_h
                user.professional_organization = ProfessionalOrganizationOption.find_or_create_by(hash)
            else
                user.professional_organization = nil
            end
            user.save
            params.delete(:professional_organization)
        end
        params
    end

    def process_role_updates(user, params)
        if !params[:roles].blank?
            new_roles = []
            # replace existing roles for this user with these new roles
            params[:roles].each do |role|
                role = Role.find_or_create_by(name: role[:name], resource_id: role[:resource_id], resource_type: role[:resource_type])
                new_roles << role
            end
            user.roles = new_roles
            params.delete(:roles)
        end
        params
    end

    def process_identity_verified_update(user, params)
        # If the user's identity_verified flag is being set to true then log an event
        if params[:identity_verified] and !user.identity_verified
            Event.create_server_event!(
                SecureRandom.uuid,
                current_user.id,
                'private_user_documents:identity_verified',
                {
                    label: user.id,
                    verified_by: current_user.id
                }
            ).log_to_external_systems
        end

        # If the user's identity is being verified then we want to ensure that the associated identification
        # asset is deleted as it is sensitive information that we would rather not keep around
        if params[:identity_verified] and user.s3_identification_asset
            user.s3_identification_asset = nil
        end
        params
    end

    def process_education_experience_updates(user, is_admin)
        education_experiences_hash = params[:meta] && params[:meta][:education_experiences]
        return if education_experiences_hash.nil?

        in_transaction_with_editor_tracking do
            education_experiences_hash.each do |education_experience_hash|
                CareerProfile::EducationExperience.update_from_hash!(education_experience_hash, {is_admin: is_admin})

                education_experience_hash['transcripts']&.each do |transcript_hash|
                    # Since we're only updating one field on transcript, I'm not going
                    # to make an update_from_hash method right now
                    type_val = transcript_hash['transcript_type'] == '' ? nil : transcript_hash['transcript_type']
                    transcript = S3TranscriptAsset.find(transcript_hash['id'])
                    if type_val != transcript.transcript_type
                        transcript.update!(transcript_type: type_val)
                    end
                end
            end
        end

        user.career_profile.education_experiences.reload
    end

    def set_user_resource
        if !permitted_post_params[:id].nil?
            @user_resource = User.find(permitted_post_params[:id])
        else
            # honor devise configuration for case_insensitive_keys
            if User.case_insensitive_keys.include?(:email)
                @user_resource = User.find_by_email(permitted_post_params[:email].downcase)
            else
                @user_resource = User.find_by_email(permitted_post_params[:email])
            end
        end
    end

    def sanitize_string(str)
        ActiveRecord::Base.connection.quote_string(str)
    end

    def get_ids_for_index
        query_builder = CrazyQueryBuilder.new('users')
        query_builder.selects['id'] = 'users.id'
        query_builder.selects['user_count'] = 'count(*) OVER()'

        query_builder.limit = @per_page
        query_builder.offset = (@per_page * (@page - 1))

        # I feel like we would always want NULLS LAST behavior. If I am mistaken, we can make this conditional
        query_builder.orders << "#{sort_column} #{sort_direction} NULLS LAST"
        query_builder.group_bys << "users.id"

        if @available_for_relationships_with_hiring_manager.present?
            professional_organization_option_id = User.where(id: @available_for_relationships_with_hiring_manager).pluck('professional_organization_option_id').first
            query_builder.wheres << "users.id IN (#{User.available_for_relationships(professional_organization_option_id).select(:id).to_sql})"
        else # only add these wheres if @professional_organization_option_id is blank
            if @can_edit_career_profile.present?
                query_builder.wheres << "users.can_edit_career_profile = #{@can_edit_career_profile}"
            end

            if @do_not_create_relationships.present?
                query_builder.joins << "
                    JOIN career_profiles AS career_profiles_join
                    ON career_profiles_join.user_id = users.id"
                query_builder.wheres << "career_profiles_join.do_not_create_relationships = #{@do_not_create_relationships}" if @do_not_create_relationships.present?
            end
        end



        if @freetext_search_param&.looks_like_uuid?
            query_builder.wheres << "users.id = '#{@freetext_search_param}'"
        elsif @freetext_search_param&.match(/@\w+\.\w+$/)
            query_builder.wheres << "users.email ILIKE '%#{@freetext_search_param}%'"
        elsif !@freetext_search_param.blank?
            query_builder.joins ||= []

            quoted_freetext_search_param = sanitize_string(@freetext_search_param)

            query_builder.wheres << "users.name ilike '%#{quoted_freetext_search_param}%'
                    OR users.nickname ilike '%#{quoted_freetext_search_param}%'
                    OR users.email ilike '%#{quoted_freetext_search_param}%'
                    "
        end

        # Roles
        if sort_column == 'roles.name'
            query_builder.joins << "LEFT OUTER JOIN users_roles on users_roles.user_id = users.id
                        left outer join roles on users_roles.role_id = roles.id"
            query_builder.group_bys << "roles.name"
        end

        # Filter by role
        query_builder.joins ||= []
        if !@role_id.blank?
            query_builder.joins << "
                JOIN users_roles as users_roles_join
                ON users_roles_join.role_id = '#{@role_id}' AND users_roles_join.user_id = users.id"

        end

        if !@hiring_relationship_statuses.blank? || sort_column == 'hiring_relationships.hiring_manager_priority'
            # If we're not filtering by hiring_relationship status and only sorting by the hiring_manager_priority column,
            # perform a LEFT JOIN against the hiring_relationships table to avoid the appearance of filtering, otherwise
            # we should be filtering by hiring_relationship status, which means we should be performing an INNER JOIN.
            # NOTE: available_for_relationships_with_hiring_manager needs to be present in order for the join to properly
            # restrict the number of records that get returned.
            if sort_column == 'hiring_relationships.hiring_manager_priority' && @hiring_relationship_statuses.blank? && !@available_for_relationships_with_hiring_manager.blank?
                query_builder.joins << "LEFT JOIN hiring_relationships ON users.id = hiring_relationships.candidate_id
                    AND hiring_relationships.hiring_manager_id = '#{@available_for_relationships_with_hiring_manager}'"
            else
                query_builder.joins << "JOIN hiring_relationships ON users.id = hiring_relationships.candidate_id"
            end

            if !@hiring_relationship_statuses.blank?
                hiring_relationship_statuses_query = []
                @hiring_relationship_statuses.each do |hiring_relationship_status|
                    hiring_manager_status, candidate_status = hiring_relationship_status.split(',')
                    hiring_relationship_statuses_query << "(hiring_relationships.hiring_manager_status = '#{hiring_manager_status}' AND hiring_relationships.candidate_status = '#{candidate_status}')"
                end
                query_builder.wheres << "(#{hiring_relationship_statuses_query.join("\nOR ")})"
                query_builder.wheres << "hiring_relationships.hiring_manager_id = '#{@available_for_relationships_with_hiring_manager}'" unless @available_for_relationships_with_hiring_manager.nil?
            end

            query_builder.group_bys << 'hiring_relationships.hiring_manager_priority' if sort_column == 'hiring_relationships.hiring_manager_priority'
        end

        # Career Profiles
        if ['career_profiles.created_at', 'career_profiles.updated_at', 'career_profiles.profile_feedback', 'career_profiles.last_confirmed_at_by_student',
            'career_profiles.last_calculated_complete_percentage', 'career_profiles.do_not_create_relationships', 'career_profiles.interested_in_joining_new_company'].include?(sort_column) ||
            !@career_profile_complete.nil? ||
            !@career_profile_disabled.nil? ||
            !@career_profile_employment_types_of_interest.nil? ||
            !@has_profile_feedback.nil? ||
            !@profile_updated_after_feedback_sent.nil? ||
            !@career_profile_active_status.nil? ||
            !@career_profile_location.nil? ||
            !@career_profile_last_confirmed_at_by_student.nil? ||
            !@career_profile_last_confirmed_at_by_internal.nil? ||
            # Filter params that should mirror the hiring manager UX on the Browse page
            !@career_profile_years_experience.nil? ||
            !@career_profile_places.nil? ||
            !@career_profile_roles.nil? ||
            !@career_profile_industries.nil? ||
            !@career_profile_skills.nil? ||
            !@career_profile_keyword_search.nil?

            query_builder.joins << "JOIN career_profiles ON career_profiles.user_id = users.id"

            query_builder.group_bys << "career_profiles.created_at" if sort_column == 'career_profiles.created_at'
            query_builder.group_bys << "career_profiles.updated_at" if sort_column == 'career_profiles.updated_at'
            query_builder.group_bys << "career_profiles.last_calculated_complete_percentage" if sort_column == 'career_profiles.last_calculated_complete_percentage'
            query_builder.group_bys << "career_profiles.do_not_create_relationships" if sort_column == 'career_profiles.do_not_create_relationships'
            query_builder.group_bys << "career_profiles.profile_feedback" if sort_column == 'career_profiles.profile_feedback'
            query_builder.group_bys << "career_profiles.last_confirmed_at_by_student" if sort_column == 'career_profiles.last_confirmed_at_by_student'
            query_builder.group_bys << "career_profiles.interested_in_joining_new_company" if sort_column == 'career_profiles.interested_in_joining_new_company'

            if !@career_profile_complete.nil?
                if ["true", true].include?(@career_profile_complete)
                    query_builder.wheres << "career_profiles.last_calculated_complete_percentage = 100"
                else
                    query_builder.wheres << "career_profiles.last_calculated_complete_percentage < 100"
                end
            end

            if !@career_profile_disabled.nil?
                # - Since this can be a post or a get, the filter could be stringified or not, so we check both 'true' and true
                query_builder.wheres << "career_profiles.do_not_create_relationships = #{['true', true].include?(@career_profile_disabled)}"
            end

            if !@career_profile_employment_types_of_interest.nil? && !@career_profile_employment_types_of_interest.empty?
                sql_array_string = "'#{@career_profile_employment_types_of_interest.join("','")}'"
                query_builder.wheres << "career_profiles.employment_types_of_interest && Array[#{sql_array_string}]"
            end

            if !@has_profile_feedback.nil?
                # - Since this can be a post or a get, the filter could be stringified or not, so we check both 'true' and true
                if ['true', true].include?(@has_profile_feedback)
                    query_builder.wheres << "career_profiles.profile_feedback IS NOT NULL"
                else
                    query_builder.wheres << "career_profiles.profile_feedback IS NULL"
                end
            end

            if !@profile_updated_after_feedback_sent.nil?
                query_builder.wheres << "career_profiles.profile_feedback IS NOT NULL"

                time_offset = '2 seconds'
                if [true, 'true'].include?(@profile_updated_after_feedback_sent)
                    # We add a 2 second interval offset to account for the very small time gap between when feedback_last_sent_at
                    # is set on the career profile and when the career profile is actually saved
                    query_builder.wheres << "career_profiles.feedback_last_sent_at IS NOT NULL AND
                        career_profiles.updated_at > (career_profiles.feedback_last_sent_at + INTERVAL '#{time_offset}')"
                else
                    query_builder.wheres << "career_profiles.feedback_last_sent_at IS NOT NULL AND
                        (career_profiles.feedback_last_sent_at + INTERVAL '#{time_offset}') >= career_profiles.updated_at"
                end
            end

            # NOTE: This code should duplicate the logic over in CareerProfile::ActiveRecord::Relation#active.
            if !@career_profile_active_status.blank?
                interested_in_joining_new_company_string = @career_profile_active_status.map { |status| "'#{status}'" }.join(',')
                query_builder.wheres << "can_edit_career_profile = TRUE
                    AND last_calculated_complete_percentage = 100
                    AND interested_in_joining_new_company IN (#{interested_in_joining_new_company_string})"
            end

            if !@career_profile_location.nil?
                query_builder.wheres << "(replace(array_to_string(locations_of_interest, ','), '_', ' ') ILIKE '%#{@career_profile_location}%'
                    OR place_details::json->>'formatted_address' ILIKE '%#{@career_profile_location}%')"
            end

            if !@career_profile_last_confirmed_at_by_student.nil?
                if @career_profile_last_confirmed_at_by_student == 'NULL' || @career_profile_last_confirmed_at_by_student == 'NOT NULL'
                    query_builder.wheres << "career_profiles.last_confirmed_at_by_student IS #{@career_profile_last_confirmed_at_by_student}"
                else
                    query_builder.wheres << "career_profiles.last_confirmed_at_by_student > '#{@career_profile_last_confirmed_at_by_student}'"
                end
            end

            if !@career_profile_last_confirmed_at_by_internal.nil?
                if @career_profile_last_confirmed_at_by_internal == 'NULL' || @career_profile_last_confirmed_at_by_internal == 'NOT NULL'
                    query_builder.wheres << "career_profiles.last_confirmed_at_by_internal IS #{@career_profile_last_confirmed_at_by_internal}"
                else
                    query_builder.wheres << "career_profiles.last_confirmed_at_by_internal > '#{@career_profile_last_confirmed_at_by_internal}'"
                end
            end

            # Process hiring manager filters (see hiring_manager_filters_mixin.rb)
            if @career_profile_years_experience&.any? || @career_profile_places&.any? || @career_profile_roles.present? ||
                @career_profile_industries&.any? || @career_profile_skills&.any? || @career_profile_keyword_search&.present?
                # cherry pick the filters we want to send through
                filters = {
                    'years_experience': @career_profile_years_experience || [],
                    'places': @career_profile_places || [],
                    'only_local': @career_profile_only_local,
                    'roles': @career_profile_roles || [],
                    'industries': @career_profile_industries || [],
                    'skills': @career_profile_skills || [],
                    'keyword_search': @career_profile_keyword_search
                }.with_indifferent_access
                query = process_hiring_manager_filters(filters)
                query = process_career_profiles_filters(filters, query)
                query_builder.wheres << "career_profiles.id IN (#{query.select(:id).to_sql})"
            end
        end

        # Location
        if sort_column == "formatted_location.city_state" || !@city_state.nil?
            # Sort by location
            query_builder.withs << "formatted_location AS MATERIALIZED (
                SELECT
                    id,
                    CASE WHEN city IS NOT NULL THEN concat(city, ', ', state)
                        WHEN city IS NULL AND state IS NOT NULL THEN state
                        WHEN city IS NULL AND state IS NULL THEN NULL
                    END AS city_state
                FROM users
            )"

            query_builder.joins << "JOIN formatted_location ON formatted_location.id = users.id"
            query_builder.group_bys << "formatted_location.city_state"

            # Filter by location
            if !@city_state.nil?
                query_builder.wheres << "formatted_location.city_state ILIKE '%#{@city_state}%'"
            end
        end

        # hiring applications
        if !@has_hiring_application.nil? || !@hiring_application_status.blank?
            query_builder.joins << "
                JOIN hiring_applications as hiring_applications_join
                    ON hiring_applications_join.user_id = users.id
            "

            # filter by hiring application status
            if !@hiring_application_status.blank?
                hiring_application_status_string = @hiring_application_status.map { |status| "'#{status}'" }.join(',')
                query_builder.wheres << "hiring_applications_join.status IN (#{hiring_application_status_string})"
            end
        end

        # filter by hiring team directly
        if !@hiring_team_id.blank?
             query_builder.wheres << "users.hiring_team_id = '#{@hiring_team_id}'"
        end

        # filter by hiring team availability
        if !@available_for_hiring_team.blank?
            query_builder.wheres << "(users.hiring_team_id IS NULL OR users.hiring_team_id = '#{@available_for_hiring_team}')"
        elsif !@available_for_all_hiring_teams.blank?
            query_builder.wheres << "users.hiring_team_id IS NULL"
        end

        # filter by group_name
        if !@group_name.blank?
            query_builder.joins ||= []
            query_builder.joins << "
                JOIN access_groups_users ON access_groups_users.user_id = users.id
                JOIN access_groups ON  access_groups.id = access_groups_users.access_group_id
                    AND access_groups.name = '#{@group_name}'
                "

        end
        # filter by institution
        if !@institution_id.blank?
            query_builder.joins ||= []
            query_builder.joins << "
                JOIN institutions_users as institutions_users_join
                ON institutions_users_join.user_id = users.id AND institutions_users_join.institution_id='#{@institution_id}'"

        end

        if !@cohort_id.blank? || !@cohort_status.blank? || !@last_cohort_status.blank? ||
            !@has_cohort_application.blank? || !@application_registered.blank?
            query_builder.joins << "
                JOIN cohort_applications as cohort_applications_join
                ON cohort_applications_join.user_id = users.id"

            # filter by cohort application
            if !@cohort_id.blank? && @last_cohort_status.blank?
                cohort_id_string = @cohort_id.map { |cohort_id| "'#{cohort_id}'" }.join(',')

                if @search_for_either_graduation_or_cohort_status
                    # The Enrollment Status page in the cohort editor uses this unique cohort_status to find
                    # users that are either accepted or pre-accepted and registered. We used to return all
                    # accepted and pre-accepted users, but realized that they really only care about users
                    # that are either pre-accepted and registered or accepted for that page.
                    if @cohort_status == ['accepted_or_pre_accepted_registered']
                        query_builder.wheres << "
                            cohort_applications_join.cohort_id IN (#{cohort_id_string})
                            AND (
                                cohort_applications_join.status = 'accepted'
                                OR (
                                    cohort_applications_join.status = 'pre_accepted'
                                    AND cohort_applications_join.registered = TRUE
                                )
                            )
                        "
                    else
                        # Tricky: we want this to work additively between cohort statuses and graduation statuses in this case. In other words,
                        # if you search for ACCEPTED + FAILED, we take that to mean all learners with an accepted application plus
                        # all learners with a failed graduation status on any application, NOT all accepted applications with a failed
                        # graduation status.

                        cohort_statuses_string = @cohort_status.nil? ? '' : @cohort_status.map { |status| "'#{status}'" }.join(',')
                        graduation_statuses_string = @graduation_status.nil? ? '' : @graduation_status.map { |graduation_status| "'#{graduation_status}'" }.join(',')

                        if !@cohort_status.blank? && !@graduation_status.blank?
                            query_builder.wheres << "
                            cohort_applications_join.cohort_id IN (#{cohort_id_string})
                            AND (
                                cohort_applications_join.status in (#{cohort_statuses_string})
                                OR cohort_applications_join.graduation_status in (#{graduation_statuses_string})
                            )"

                        elsif !@cohort_status.blank?
                            query_builder.wheres << "
                            cohort_applications_join.cohort_id IN (#{cohort_id_string})
                            AND cohort_applications_join.status in (#{cohort_statuses_string})"

                        elsif !@graduation_status.blank?
                            query_builder.wheres << "
                            cohort_applications_join.cohort_id IN (#{cohort_id_string})
                            AND cohort_applications_join.graduation_status in (#{graduation_statuses_string})"

                        else
                            query_builder.wheres << "cohort_applications_join.cohort_id IN (#{cohort_id_string})"
                        end
                    end
                else
                    # otherwise, we simply want to search for the intersection of the passed in statuses
                    # thus, if you searched for cohort_status: ACCEPTED, graduation_status: FAILED, we want all learners
                    # with an application that is both accepted and failed.

                    @cohort_status = @cohort_status.blank? ? CohortApplication.statuses : @cohort_status
                    @graduation_status = @graduation_status.blank? ? CohortApplication.graduation_statuses : @graduation_status
                    cohort_statuses_string = @cohort_status.map { |status| "'#{status}'" }.join(',')
                    graduation_statuses_string = @graduation_status.map { |graduation_status| "'#{graduation_status}'" }.join(',')

                    query_builder.wheres << "
                        cohort_applications_join.cohort_id IN (#{cohort_id_string})
                        AND cohort_applications_join.status in (#{cohort_statuses_string})
                        AND cohort_applications_join.graduation_status in (#{graduation_statuses_string})"
                end
            end

            # filter by last cohort application
            if !@last_cohort_status.blank?
                last_cohort_statuses_string = @last_cohort_status.map { |status| "'#{status}'" }.join(',')
                query_builder.havings << "(array_agg(cohort_applications_join.status order by cohort_applications_join.applied_at desc))[1] IN (#{last_cohort_statuses_string})"

                if !@cohort_id.blank?
                    cohort_id_string = @cohort_id.map { |cohort_id| "'#{cohort_id}'" }.join(',')
                    query_builder.havings << "(array_agg(cohort_applications_join.cohort_id order by cohort_applications_join.applied_at desc))[1] IN (#{cohort_id_string})"
                end
            end

            if !@application_registered.blank?
                query_builder.wheres << "cohort_applications_join.registered = #{@application_registered}"
            end
        end

        if !@interested_in_open_position.blank?
            query_builder.joins << "JOIN candidate_position_interests ON candidate_position_interests.candidate_id = users.id"
            query_builder.joins << "JOIN open_positions ON open_positions.id = candidate_position_interests.open_position_id"
            query_builder.wheres << "open_positions.id = '#{@interested_in_open_position}'"
        end

        if !@identifiers.nil?
            id_string = @identifiers.map { |identifier| "'#{identifier}'" if identifier.looks_like_uuid? }.compact.join(',')
            email_string = @identifiers.map { |identifier| "'#{identifier}'" }.join(',')
            conditions = []
            conditions << "users.id in (#{id_string})" unless id_string.blank?
            conditions << "users.email in (#{email_string})" unless email_string.blank?
            if conditions.any?
                query_builder.wheres << conditions.join(' or ')
            else
                query_builder.wheres << "FALSE"
            end
        end

        # Handle simple filters
        if @name
            quoted_name = sanitize_string(@name)
            query_builder.wheres << "users.name ILIKE '%#{quoted_name}%'" unless quoted_name.nil?
        end

        if @email
            quoted_email = sanitize_string(@email)
            query_builder.wheres << "users.email ILIKE '%#{quoted_email}%'" unless quoted_email.nil?
        end

        result =  ActiveRecord::Base.connection.execute(query_builder.sql)
        user_count = (result.to_a[0] && result.to_a[0]["user_count"].to_i) || 0
        ids = result.to_a.map { |h| h['id'] }

        [ids, user_count]
    end

    def get_json_for_index(ids)

        query_builder = CrazyQueryBuilder.new('users')
        query_builder.selects['id'] = 'users.id'
        query_builder.selects['created_at'] = 'cast(EXTRACT(EPOCH FROM users.created_at) as int)'
        query_builder.selects['email'] = 'users.email'
        query_builder.selects['name'] = 'users.name'
        query_builder.selects['has_seen_welcome'] = 'users.has_seen_welcome'
        query_builder.selects['has_logged_in'] = 'users.has_logged_in'
        query_builder.selects['has_seen_mba_submit_popup'] = 'users.has_seen_mba_submit_popup'
        query_builder.selects['has_seen_short_answer_warning'] = 'users.has_seen_short_answer_warning'
        query_builder.selects['has_seen_careers_welcome'] = 'users.has_seen_careers_welcome'
        query_builder.selects['pref_locale'] = 'users.pref_locale'
        query_builder.selects['mba_content_lockable'] = 'users.mba_content_lockable'
        query_builder.selects['pref_decimal_delim'] = 'users.pref_decimal_delim'
        query_builder.selects['pref_show_photos_names'] = 'users.pref_show_photos_names'
        query_builder.selects['pref_sound_enabled'] = 'users.pref_sound_enabled'
        query_builder.selects['provider'] = 'users.provider'
        query_builder.selects['provider_user_info'] = 'users.provider_user_info'
        query_builder.selects['reset_password_redirect_url'] = 'users.reset_password_redirect_url'
        query_builder.selects['school'] = 'users.school'
        query_builder.selects['job_title'] = 'users.job_title'
        query_builder.selects['phone'] = 'users.phone'
        query_builder.selects['phone_extension'] = 'users.phone_extension'
        query_builder.selects['country'] = 'users.country'
        query_builder.selects['sign_up_code'] = 'users.sign_up_code'
        query_builder.selects['uid'] = 'users.uid'
        query_builder.selects['updated_at'] = 'cast(EXTRACT(EPOCH FROM users.updated_at) as int)'
        query_builder.selects['last_sign_in_at'] = 'cast(EXTRACT(EPOCH FROM users.last_sign_in_at) as int)'
        query_builder.selects['last_seen_at'] = 'cast(EXTRACT(EPOCH FROM users.last_seen_at) as int)'
        query_builder.selects['can_edit_career_profile'] = 'users.can_edit_career_profile'
        query_builder.selects['fallback_program_type'] = 'users.fallback_program_type'
        query_builder.selects['identity_verified'] = 'users.identity_verified'
        query_builder.selects['hiring_team_id'] = 'users.hiring_team_id'
        query_builder.selects['has_seen_featured_positions_page'] = 'users.has_seen_featured_positions_page'
        query_builder.selects['has_seen_hide_candidate_confirm'] = 'users.has_seen_hide_candidate_confirm'
        query_builder.selects['has_seen_first_position_review_modal'] = 'users.has_seen_first_position_review_modal'
        query_builder.selects['has_seen_student_network'] = 'users.has_seen_student_network'
        query_builder.selects['pref_one_click_reach_out'] = 'users.pref_one_click_reach_out'
        query_builder.selects['can_purchase_paid_certs'] = 'users.can_purchase_paid_certs'
        query_builder.selects['pref_positions_candidate_list'] = 'users.pref_positions_candidate_list'
        query_builder.selects['deactivated'] = 'users.deactivated'
        query_builder.selects['pref_allow_push_notifications'] = 'users.pref_allow_push_notifications'
        query_builder.selects['english_language_proficiency_comments'] = 'users.english_language_proficiency_comments'
        query_builder.selects['english_language_proficiency_documents_type'] = 'users.english_language_proficiency_documents_type'
        query_builder.selects['english_language_proficiency_documents_approved'] = 'users.english_language_proficiency_documents_approved'
        query_builder.selects['transcripts_verified'] = 'users.transcripts_verified'
        query_builder.selects['relevant_cohort_id'] = 'users_relevant_cohorts.cohort_id'
        query_builder.selects['academic_hold'] = 'users.academic_hold'
        query_builder.selects['student_network_email'] = 'users.student_network_email'
        User::NOTIFICATIONS.keys.each { |notification| query_builder.selects[notification] = 'users.' + notification }

        query_builder.joins << "LEFT JOIN users_relevant_cohorts ON users_relevant_cohorts.user_id = users.id"

        # include serialized group context with each user row
        query_builder.selects['groups'] = "
            -- convert the desired columns for each group into json (row_to_json),
            -- group all the json values
            -- into an array (array_agg), then convert the array to json (array_to_json)
            COALESCE(
                (SELECT array_to_json(array_agg(row_to_json(prepared_groups))) FROM
                    (
                        -- select the desired columns from groups
                        SELECT
                            access_groups.id,
                            access_groups.name
                        FROM access_groups
                            JOIN access_groups_users on access_groups.id = access_groups_users.access_group_id
                                AND access_groups_users.user_id=users.id
                        ORDER BY access_groups.name asc
                    ) prepared_groups
                ),
                '[]'::json
            )"


        # include serialized role context with each user row
        query_builder.selects['roles'] = "
            -- convert the desired columns for each role into json (row_to_json),
            -- group all the json values
            -- into an array (array_agg), then convert the array to json (array_to_json)
            (SELECT array_to_json(array_agg(row_to_json(prepared_roles))) FROM
                (
                    -- select the desired columns from groups
                    SELECT
                        roles.id,
                        roles.name,
                        roles.resource_id,
                        roles.resource_type
                    FROM roles JOIN users_roles
                    ON roles.id = users_roles.role_id
                    AND users_roles.user_id = users.id
                ) prepared_roles
            )"

        # include serialized institution context with each user row
        query_builder.selects['institutions'] = "
            -- convert the desired columns for each institution into json (row_to_json),
            -- group all the json values
            -- into an array (array_agg), then convert the array to json (array_to_json)
            (SELECT array_to_json(array_agg(row_to_json(prepared_institutions))) FROM
                (
                    -- select the desired columns from institutions
                    SELECT
                        institutions_users.institution_id as id
                    FROM institutions_users
                    WHERE institutions_users.user_id = users.id
                ) prepared_institutions
            )"

        query_builder.selects['active_institution'] = "
            (SELECT row_to_json(prepared_active_institution) FROM
                (
                    select institutions_users.institution_id as id
                    FROM institutions_users
                    WHERE institutions_users.user_id = users.id AND
                        institutions_users.active = TRUE
                ) prepared_active_institution
            )"

        query_builder.selects['hiring_application'] = "
            (SELECT row_to_json(prepared_hiring_applications) FROM
                (
                    SELECT
                        id,
                        cast(EXTRACT(EPOCH FROM applied_at) as int) applied_at,
                        cast(EXTRACT(EPOCH FROM accepted_at) as int) accepted_at,
                        status
                    FROM hiring_applications
                    WHERE hiring_applications.user_id = users.id
                ) prepared_hiring_applications
            )"


        query_builder.selects['hiring_team'] = "
            (SELECT row_to_json(prepared_hiring_team) FROM
                (
                    SELECT
                        id,
                        title,
                        owner_id,
                        subscription_required,
                        cast(EXTRACT(EPOCH FROM created_at) as int) created_at,
                        cast(EXTRACT(EPOCH FROM updated_at) as int) updated_at
                    FROM hiring_teams
                    WHERE hiring_teams.id = users.hiring_team_id
                ) prepared_hiring_team
            )"

        query_builder.selects['user_id_verifications'] = "
            (SELECT array_to_json(array_agg(row_to_json(prepared_user_id_verifications))) FROM
                (
                    SELECT
                        id,
                        cohort_id,
                        user_id,
                        id_verification_period_index,
                        verification_method,
                        cast(EXTRACT(EPOCH FROM verified_at) as int) verified_at
                    FROM user_id_verifications
                    WHERE user_id_verifications.user_id = users.id
                ) prepared_user_id_verifications
            )"

        if @select_signable_documents
            query_builder.selects['signable_documents'] = "
                (SELECT array_to_json(array_agg(row_to_json(prepared_signable_documents))) FROM
                    (
                        SELECT
                            id,
                            document_type,
                            user_id,
                            sign_now_signing_link,
                            metadata,
                            cast(EXTRACT(EPOCH FROM signed_at) as int) signed_at,
                            file_updated_at,
                            file_file_name,
                            file_content_type
                        FROM signable_documents
                        WHERE signable_documents.user_id = users.id
                    ) prepared_signable_documents
                )"
        end


        if @select_professional_organization
            query_builder.selects['professional_organization'] = "
                (SELECT row_to_json(prepared_professional_organizations) FROM
                    (
                        SELECT
                            id,
                            text,
                            locale
                        FROM professional_organization_options
                        WHERE professional_organization_options.id = users.professional_organization_option_id
                    ) prepared_professional_organizations
                )"
        end

        if @select_career_profile
            query_builder.selects['career_profile'] = "
                (SELECT row_to_json(prepared_career_profile) FROM
                    (
                        SELECT
                            id,
                            user_id,
                            cast(EXTRACT(EPOCH FROM created_at) as int) created_at,
                            cast(EXTRACT(EPOCH FROM updated_at) as int) updated_at,
                            cast(EXTRACT(EPOCH FROM last_confirmed_at_by_student) as int) last_confirmed_at_by_student,
                            last_calculated_complete_percentage,
                            do_not_create_relationships,
                            employment_types_of_interest,
                            interested_in_joining_new_company,
                            city,
                            state,
                            country,
                            place_details,
                            profile_feedback,
                            native_english_speaker,
                            earned_accredited_degree_in_english,
                            sufficient_english_work_experience,
                            english_work_experience_description,
                            users.email,
                            users.can_edit_career_profile" + (@select_education_experiences ? ",
                            COALESCE(
                                (SELECT array_to_json(array_agg(row_to_json(prepared_education_experiences))) FROM
                                    (
                                        -- select the desired columns from cohort_applications
                                        SELECT
                                            education_experiences.id,
                                            education_experiences.degree_program,
                                            education_experiences.will_not_complete,
                                            education_experiences.graduation_year,
                                            education_experiences.transcript_waiver,
                                            education_experiences.transcript_approved" + (@select_transcripts ? ",
                                            COALESCE(
                                                (SELECT array_to_json(array_agg(row_to_json(prepared_transcripts))) FROM
                                                    (
                                                        -- select the desired columns from s3_transcript_assets
                                                        SELECT
                                                            id,
                                                            file_updated_at,
                                                            file_file_name,
                                                            file_content_type
                                                        FROM s3_transcript_assets
                                                        WHERE s3_transcript_assets.education_experience_id = education_experiences.id
                                                    ) prepared_transcripts
                                                ),
                                                '[]'::json
                                            ) as transcripts
                                        " : "") + "
                                        FROM education_experiences
                                        WHERE education_experiences.career_profile_id = career_profiles.id
                                    ) prepared_education_experiences
                                ),
                                '[]'::json
                            ) as education_experiences
                        " : "") + "
                        FROM career_profiles
                        WHERE career_profiles.user_id = users.id
                    ) prepared_career_profile
                )"
        end

        if @select_cohort_user_progress_record
            query_builder.selects['cohort_user_progress_record'] = "
                (SELECT row_to_json(prepared_record) FROM
                    (
                        SELECT
                            cohort_user_progress_records.*
                        FROM cohort_user_progress_records
                            join cohort_applications
                                on cohort_applications.cohort_id = cohort_user_progress_records.cohort_id
                                and cohort_applications.user_id = cohort_user_progress_records.user_id
                                and cohort_applications.status = 'accepted'
                        WHERE cohort_user_progress_records.user_id = users.id
                    ) prepared_record
                )"
        end

        if @select_project_progress
            query_builder.selects['project_progress'] = "
            COALESCE(
                (SELECT array_to_json(array_agg(row_to_json(prepared_records))) FROM
                    (
                        SELECT
                                project_progress.*
                            FROM project_progress
                            WHERE
                                project_progress.user_id = users.id
                    ) prepared_records
                ),
                '[]'::json)"

            # override is a bad name here, because it does not actually override
            # the project scores if they are set in project_progress.  It is only
            # used when there are no project_progress records
            query_builder.selects['project_score_override'] = "
                (
                    SELECT score
                    FROM user_project_scores
                    WHERE
                        user_project_scores.user_id = users.id
                )
            "
        end

        if @select_exam_progress
            query_builder.selects['exam_progress'] = "
            (SELECT array_to_json(array_agg(row_to_json(prepared_records))) FROM
                (
                    SELECT
                            lesson_streams_progress.*
                        FROM lesson_streams_progress
                            join lesson_streams
                                on lesson_streams.locale_pack_id = lesson_streams_progress.locale_pack_id
                                and lesson_streams.locale = 'en'
                        WHERE
                            lesson_streams_progress.user_id = users.id
                            and lesson_streams.exam = true
                ) prepared_records
            )"
        end

        # Note that this only loads up user subscriptions.  Not hiring_team subscriptions.
        # Be careful if extending this to hiring teams, as they can have subscriptions that are
        # not the active one.
        if @select_subscriptions
            query_builder.selects['subscriptions'] = "
            COALESCE(
                (SELECT array_to_json(array_agg(row_to_json(prepared_records))) FROM
                    (
                        SELECT
                            subscriptions.id,
                            stripe_plan_id,
                            subscriptions_users.user_id,
                            past_due,
                            cancel_at_period_end,
                            cast(EXTRACT(EPOCH FROM current_period_end) as int) current_period_end,
                            cast(EXTRACT(EPOCH FROM created_at) as int) created_at,
                            cast(EXTRACT(EPOCH FROM updated_at) as int) updated_at,
                            stripe_subscription_id
                        FROM subscriptions
                            JOIN subscriptions_users
                                ON subscriptions_users.subscription_id = subscriptions.id
                        WHERE subscriptions_users.user_id = users.id
                        LIMIT 1
                    ) prepared_records
                ),
                '[]'::json
            )"
        end

        if !@interested_in_open_position.blank?
            query_builder.selects['position_interest'] = "
                (SELECT row_to_json(prepared_candidate_position_interest) FROM
                    (
                        SELECT
                            candidate_position_interests.id,
                            cast(EXTRACT(EPOCH FROM candidate_position_interests.created_at) as int) created_at,
                            cast(EXTRACT(EPOCH FROM candidate_position_interests.updated_at) as int) updated_at,
                            candidate_position_interests.candidate_status,
                            candidate_position_interests.hiring_manager_status
                        FROM candidate_position_interests
                        JOIN open_positions ON open_positions.id = candidate_position_interests.open_position_id
                        WHERE candidate_position_interests.candidate_id = users.id
                        AND open_positions.id = '#{@interested_in_open_position}'
                    ) prepared_candidate_position_interest
                )"
        end

        query_builder.selects['cohort_applications'] = "
            -- convert the desired columns for each cohort_application into json (row_to_json),
            -- group all the json values
            -- into an array (array_agg), then convert the array to json (array_to_json)
            COALESCE(
                (SELECT array_to_json(array_agg(row_to_json(prepared_cohort_applications))) FROM
                    (
                        -- select the desired columns from cohort_applications
                        SELECT
                            cohort_applications.id,
                            cohort_applications.cohort_id,
                            (extract(epoch from date_trunc('seconds', cohort_applications.applied_at)))::int as applied_at,
                            (extract(epoch from date_trunc('seconds', cohort_applications.updated_at)))::int as updated_at,
                            cohorts.name as cohort_name,
                            cohorts.title as cohort_title,
                            cohorts.program_type as program_type,
                            (extract(epoch from date_trunc('seconds', cohorts.start_date)))::int as cohort_start_date,
                            cohort_applications.status,
                            cohort_applications.graduation_status,
                            cohort_applications.skip_period_expulsion,
                            cohort_applications.disable_exam_locking,
                            cohort_applications.retargeted_from_program_type,
                            cohort_applications.stripe_plan_id,
                            cohort_applications.num_charged_payments,
                            cohort_applications.num_refunded_payments,
                            cohort_applications.total_num_required_stripe_payments,
                            cohort_applications.scholarship_level,
                            cohort_applications.registered,
                            cohort_applications.notes,
                            cohort_applications.cohort_slack_room_id,
                            cohort_applications.stripe_plans,
                            cohort_applications.scholarship_levels,
                            cohort_applications.registered_early,
                            cohort_applications.allow_early_registration_pricing,
                            cohort_applications.final_score,
                            cohort_applications.meets_graduation_requirements,
                            cohort_applications.locked_due_to_past_due_payment,
                            (extract(epoch from date_trunc('seconds', cohort_applications.payment_grace_period_end_at)))::int as payment_grace_period_end_at,
                            case when status = 'rejected' and admissions_decision = 'Reject and Convert to EMBA' then true else false end as rejected_and_converted_to_emba
                        FROM cohort_applications
                        JOIN cohorts ON cohort_applications.cohort_id = cohorts.id
                        WHERE cohort_applications.user_id = users.id
                    ) prepared_cohort_applications
                ),
                '[]'::json
            )"

        query_builder.selects['s3_identification_asset'] = "
            (SELECT row_to_json(prepared_s3_identification_assets) FROM
                (
                    SELECT
                        id,
                        file_updated_at,
                        file_file_name,
                        file_content_type
                    FROM s3_identification_assets
                    WHERE s3_identification_assets.user_id = users.id
                ) prepared_s3_identification_assets
            )"

        query_builder.selects['s3_transcript_assets'] = "
            COALESCE(
                (SELECT array_to_json(array_agg(row_to_json(prepared_s3_transcript_assets))) FROM
                    (
                        SELECT
                            id,
                            file_updated_at,
                            file_file_name,
                            file_content_type
                        FROM s3_transcript_assets
                        WHERE s3_transcript_assets.user_id = users.id
                    ) prepared_s3_transcript_assets
                ),
                '[]'::json
            )"

        query_builder.selects['s3_english_language_proficiency_documents'] = "
            COALESCE(
                (SELECT array_to_json(array_agg(row_to_json(prepared_s3_english_language_proficiency_documents))) FROM
                    (
                        SELECT
                            id,
                            file_updated_at,
                            file_file_name,
                            file_content_type
                        FROM s3_english_language_proficiency_documents
                        WHERE s3_english_language_proficiency_documents.user_id = users.id
                    ) prepared_s3_english_language_proficiency_documents
                ),
                '[]'::json
            )"

        # See also deferral_link.rb#active?
        if @select_active_deferral_link
            query_builder.selects['active_deferral_link'] = "
                (SELECT row_to_json(active_deferral_link) FROM
                    (
                        SELECT
                            deferral_links.id,
                            deferral_links.user_id,
                            deferral_links.used,
                            deferral_links.expires_at,
                            deferral_links.from_cohort_application_id,
                            deferral_links.to_cohort_application_id
                        FROM deferral_links
                        JOIN cohort_applications deferral_link_application
                            ON deferral_links.from_cohort_application_id = deferral_link_application.id
                            AND deferral_links.user_id = deferral_link_application.user_id
                        WHERE deferral_links.used = false
                            AND deferral_links.expires_at > now()
                            AND deferral_link_application.status = ANY(#{CohortApplication.valid_deferral_statuses.to_sql})
                            AND deferral_link_application.id = (
                                SELECT id FROM cohort_applications
                                WHERE cohort_applications.user_id = users.id
                                ORDER BY cohort_applications.applied_at DESC
                                LIMIT 1
                            )
                    ) active_deferral_link
                )"
        end

        if ids.any?
            id_string = ids.map { |id| "'#{id}'" }.join(',')

            # Practically, this does not change the result of the query, but
            # we've seen this query take forever in certain cases if we do not
            # do this.  Setting up the CTE this way seems to help the query planner.
            query_builder.withs << %Q~users_relevant_cohorts AS MATERIALIZED (
                select
                    *
                from users_relevant_cohorts
                where user_id in (#{id_string})
            )~

            query_builder.wheres << "id in (#{id_string})"

            # SEE ALSO: http://stackoverflow.com/questions/866465/order-by-the-in-value-list
            id_string = ids.join(',')
            query_builder.joins << "JOIN unnest('{#{id_string}}'::uuid[]) WITH ORDINALITY t(id, ord) USING (id)"
            query_builder.orders = ["t.ord"]
        else
            query_builder.wheres << "FALSE"
        end

        result = ActiveRecord::Base.connection.execute(query_builder.to_json_sql)
        result.to_a[0]["json"] || "[]"

    end

    def extra_json_fields
        ['identifiers']
    end
end
