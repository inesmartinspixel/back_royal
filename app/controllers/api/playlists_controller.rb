class Api::PlaylistsController < Api::ApiCrudControllerBase

    authorize_resource

    # create/update/destroy/images
    Api::ApiContentController.create_endpoints(self, Playlist)

    # GET /#{model.table_name}.json
    def index
        return if render_unauthorized_error_if_trying_to_view_disallowed_unpublished_content

        if params[:user_id].present? && params[:user_id] != current_user.id && !current_user.has_role?(:admin)
            render_unauthorized_error
            return false
        end

        to_json_params = params.merge({
            user: params[:user_id] ? User.find(params[:user_id]) : current_user,
            filters: params[:filters]
        })

        json = Playlist::ToJsonFromApiParams.new(to_json_params).json
        render_contents_json_string(Playlist, json)
    end

    # GET /#{model.table_name}/1.json
    def show(instance = nil)
        return render_error_response(I18n.t(:no_id_provided), :not_acceptable) if params[:id].blank?
        return if render_unauthorized_error_if_trying_to_view_disallowed_unpublished_content

        to_json_params = params.merge({
            user: current_user,
            filters: params[:filters]
        })
        json = Playlist::ToJsonFromApiParams.new(to_json_params).json
        render_not_found_or_contents_json_string(Playlist, params[:id], json)
    end

end
