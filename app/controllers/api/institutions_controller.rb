class Api::InstitutionsController < Api::ApiCrudControllerBase

    load_and_authorize_resource

    before_action :set_institution, only: [:show, :update, :destroy]

    # POST /api/institutions
    def create
        existing_institution = Institution.where("name = ?", params[:record]['name'])

        if existing_institution.size > 0
            return render_error_response('Validation failed: Institution with this name already exists.', :not_acceptable)
        end

        rescue_validation_error_or_render_record do
            @institution = Institution.create_from_hash!(params[:record], params[:meta])
            @institution
        end

    end

    def show
        query = params[:fields]&.include?('users') ? preload_users : Institution
        render_records(query.find(params['id']), {
            fields: params[:fields]
        })
    end

    private def preload_users
        Institution.includes(:users)
    end

    # GET /api/institutions
    def index
        institutions = Institution.includes(:reports_viewers)
        render_records(institutions.all.to_a)
    end

    # PATCH/PUT /api/institutions
    def update
        rescue_validation_error_or_render_record({}) do
            @institution = Institution.update_from_hash!(params[:record], params[:meta])
            @institution
        end
    end

    # DELETE /api/institutions/1
    def destroy
        delete_record(Institution, params['id'])
    end

    # Use callbacks to share common setup or constraints between actions.
    private def set_institution
        id = params['id'] || params[:record]["id"]
        if !id.blank?
            @institution = Institution.find(id)
        end
    end
    
end
