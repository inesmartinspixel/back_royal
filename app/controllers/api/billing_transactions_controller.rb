class Api::BillingTransactionsController < Api::ApiCrudControllerBase

    authorize_resource

    def index
        billing_transactions = BillingTransaction.all_for_owner_id(params[:owner_id]).includes(:refunds).to_a
        render_records(billing_transactions)
    end

    # editable-things list will sometimes end up calling this on the client, though
    # in general we usually rely on the index
    def show
        billing_transaction = BillingTransaction.find_by_id(params[:id])
        render_records([billing_transaction])
    end

    def create
        owner_id = params[:meta] && params[:meta][:owner_id]
        owner = User.find_by_id(owner_id) || HiringTeam.find_by_id(owner_id)
        if owner.nil?
            Raven.capture_exception("Owner not found while creating a billing_transaction.",
                extra: {
                    owner_id: meta[:owner_id]
                }
            )
            return render_error_response("Owner not found", :not_acceptable)
        end

        rescue_validation_error_or_render_record do
            billing_transaction = BillingTransaction.create_from_hash!(owner, params["record"])
            billing_transaction
        end
    end

    def update
        rescue_validation_error_or_render_record do
            billing_transaction = BillingTransaction.update_from_hash!(params["record"])
            billing_transaction
        end
    end

    def destroy
        delete_record(BillingTransaction, params['id'])
    end

end
