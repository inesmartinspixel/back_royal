class Api::DeferralLinksController < Api::ApiCrudControllerBase
    # skip actions we have custom ability checks for
    skip_load_and_authorize_resource only: [:show, :update]
    load_and_authorize_resource

    def show
        deferral_link = DeferralLink.find(params['id'])

        if current_ability.cannot?(:show_deferral_link, deferral_link)
            render_unauthorized_error
            return false
        end

        @meta[:upcoming_cohorts] = deferral_link.upcoming_cohorts.as_json
        render_records(DeferralLink.find(params['id']), fields: ['active', 'from_cohort_application'])
    end

    def create
        permitted_params = params.require(:record).permit([:user_id, :from_cohort_application_id])
        rescue_validation_error_or_render_record do
            DeferralLink.create!(
                user_id: permitted_params[:user_id], 
                expires_at: Time.now + 30.days, 
                from_cohort_application_id: permitted_params[:from_cohort_application_id]
            )
        end
    end

    # Note: If we ever use this endpoint to change a deferral link but not actually
    # use it then we'll need to make some tweaks.
    def update
        permitted_params = params.required(:record).permit(:id, :user_id)
        permitted_meta_params = params.required(:meta).permit(:to_cohort_id)
        deferral_link = DeferralLink.find(params[:id])

        if current_ability.cannot?(:update_deferral_link, params, deferral_link)
            render_unauthorized_error
            return false
        end

        rescue_validation_error_or_render_record(fields:['active']) do
            deferral_link.from_cohort_application.just_used_deferral_link = true # for events
            to_cohort_application = deferral_link.user.defer_into_cohort(deferral_link.from_cohort_application, permitted_meta_params[:to_cohort_id])
            deferral_link.update!(used: true, to_cohort_application: to_cohort_application)

            @meta['to_cohort_application'] = to_cohort_application.as_json
            
            set_relevant_cohort_push_messages
            add_cohort_applications_to_push_messages

            deferral_link
        end
    end
end
