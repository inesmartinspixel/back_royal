class Api::AssetsController < Api::ApiCrudControllerBase

    # only admins can do this
    load_and_authorize_resource class: S3Asset

    # POST /api/assets
    def create
        rescue_validation_error_or_render_record do

            # I've seen this as an Array and as an instance of ActionController::Parameters
            # and couldn't quite figure out what was causing the difference, so supporting both.
            if params['files'].is_a?(ActionController::Parameters)
                files = params['files'].to_unsafe_h.values
            else
                files = params['files']
            end

            assets = (files || []).map do |file|
                S3Asset.create!({
                    :file => file,
                    :directory => 'assets/'
                })
            end

            # rescue_validation_error_or_render_record can handle multiple records being returned
            assets
        end
    end
end