class Api::LearnerProjectsController < Api::ApiCrudControllerBase

    authorize_resource

    # POST /api/learner_projects
    def create

        rescue_validation_error_or_render_record do
            @cohort = LearnerProject.create_from_hash!(params[:record], params[:meta])
            @cohort
        end
    end

    # GET /api/learner_projects
    def index
        options = {
            cohorts: Cohort.includes(:published_versions).all,
            curriculum_templates: CurriculumTemplate.all
        }
        options[:fields] = ['ADMIN_FIELDS'] if current_ability.can?(:manage, LearnerProject)
        render_records(LearnerProject.all.to_a, options)
    end


    # PATCH/PUT /api/learner_projects
    def update
        rescue_validation_error_or_render_record(:fields => ['UPDATE_FIELDS']) do
            instance = LearnerProject.update_from_hash!(params[:record], params[:meta])
            instance
        end

        rescue CrudFromHashHelper::OldVersion => e
            render_error_response("Validation failed: a more recent version has been saved since you started editing, please refresh and try again.", :not_acceptable)
        rescue CrudFromHashHelper::NoDateStampProvided => e
            render_error_response("No updated_at provided", :not_acceptable)
    end

    # DELETE /api/cohorts/1
    def destroy
        delete_record(LearnerProject, params['id'])
    end

end
