class Api::CurriculumTemplatesController < Api::ApiCrudControllerBase

    authorize_resource

    def create
        rescue_validation_error_or_render_record do
            CurriculumTemplate.create_from_hash!(current_user, params[:record], params[:meta])
        end
    end

    def index

        set_stripe_admin_meta

        if params['get_learner_projects']
            set_learner_projects_meta
        end

        templates = CurriculumTemplate.all

        filters = params[:filters] || {}
        if filters.key?(:name)
            templates = CurriculumTemplate.where(:name => filters[:name])
        end

        render_records(templates.to_a)
    end

    def update
        rescue_validation_error_or_render_record({:fields => ['UPDATE_FIELDS']}) do
            instance = CurriculumTemplate.update_from_hash!(current_user, params[:record], params[:meta])
            instance
        end
    end

    def destroy
        delete_record(CurriculumTemplate, params['id'])
    end
end
