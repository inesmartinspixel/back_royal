# This handles creating subscriptions.
# It is important that personal billing info is never sent here to be
# PCI compliant.

class Api::SubscriptionsController < Api::ApiCrudControllerBase
    include Api::UnloadedChangeDetector

    skip_before_action :require_guid_id

    before_action(:only => [:create]) do
        # we need to push down the subscription_id that can be added to an open_position
        @position_updated_at_before_changes = add_last_updated_to_push_messages(:open_position, "before")
    end

    ###############################
    # Error Handling
    ###############################

    # priority of rescue_from is in reverse order of when they are defined, so
    # specific stripe errors come last so they will be processed instead of
    # the generic Stripe::Error
    rescue_from Exception, :with => :render_error
    rescue_from Stripe::StripeError, :with => :render_stripe_error
    rescue_from Stripe::CardError, :with => :render_card_error

    # Stripe transitioned to always succeed in Subscription creation, even on failed card charges,
    # so we are raising if we create an incomplete Subscription and treating it like a CardError
    rescue_from Subscription::IncompleteSubscription, :with => :render_card_error

    class Formatter
        include ActionView::Helpers::NumberHelper
    end

    def render_error(err, status = 500, capture_exception = true, payload = {})

        # We used to try to implement this by passing :only to rescue_from, but it
        # turns out that is not supported.  It was just a mistake to try.
        # Also, we might not be mocking controller params 100% of the time in specs
        raise err if Rails.env != 'test' && !['update', 'create'].include?(params['action'])

        # comment out this line to test errors locally
        raise err if Rails.env == 'development' && status == 500

        Raven.capture_exception(err) if capture_exception

        render_error_response(err.message, status, {
            card_data_saved: card_data_saved?
        }.merge(payload))
    end

    def render_stripe_error(err, status = nil, capture_exception = true, payload = {})

        status ||= err.http_status

        if err.message
            payload.merge!({
               error: err.message
            })
        end

        # The UI should prevent this, but if for some reason a user
        # manages to send an expired or invalid coupon, we want
        # to show that error in the form rather than as a server error
        if err.is_a?(Stripe::InvalidRequestError) &&
            (err.message.match(/No such coupon/) || err.message.match(/Coupon expired/))

            status = 406
        end

        render_error(err, status || 500, capture_exception, payload)
    end

    def render_card_error(err)

        payload = {}

        # If this is a Stripe::CardError, then we are going to show the (untranslated)
        # error from stripe. However, in most (all?) cases, when a card fails, we end up triggering
        # a Subscription::IncompleteSubscription error (see subscription.rb).  In that case,
        # we'll let the client translate the generic error message.
        if err.is_a?(Subscription::IncompleteSubscription)
            has_default_source = owner.stripe_customer.default_source.present?
            payload[:message_key] = has_default_source ? 'there_was_an_issue_charging_to_card' : 'there_is_no_payment_source'
            payload[:has_default_source] = has_default_source
        end

        render_stripe_error(err, 402, false, payload)
    end

    def relevant_stripe_plan_id
        if (params[:record] && params[:record]['stripe_plan_id'])
            (params[:record] && params[:record]['stripe_plan_id'])
        elsif owner.is_a?(User)
            # not sure this is still needed
            application = current_user.last_application
            application.stripe_plan_id || application.stripe_plans.first['id']
        else
            raise NotImplementedError.new
        end
    end


    ###############################
    # Helpers
    ###############################

    def update_stripe_card_if_specified
        if source_id
            owner.update_stripe_card(source_id)
            @card_data_saved = true

            return true
        else
            return false
        end
    end

    # this is the stripe card token.  If it is
    # set, that means the user is sending up credit
    # card info
    def source_id
        # the `update` and `create` endpoints put source in the meta.
        # the modify_payment_details endpoint puts it at the top level
        (params[:meta] && params[:meta][:source]) || params[:source]
    end

    # nil if no card data was passed up.
    # true or false otherwise
    def card_data_saved?
        return nil unless source_id
        return @card_data_saved || false
    end


    ###############################
    # Controller Actions
    ###############################

    def create

        unless current_ability.can?(:create_subscription, params[:record])
            render_unauthorized_error
            return false
        end

        # ensures customer record and updates billing source
        # This only makes updates in stripe, not our db, so it does
        # not need to be within the transaction with EditorTracking
        # Note that we are not calling update_stripe_card_if_specified_and_pay_unpaid_invoices.  See
        # the comments on https://trello.com/c/nqPLE9Ud for an explanation of why
        update_stripe_card_if_specified

        begin
            EditorTracking.transaction(current_user) do
                @subscription = owner.handle_create_subscription_request(
                    params[:record],
                    params[:meta],
                    relevant_stripe_plan_id)
            end
        rescue User::SubscriptionConcern::InvalidCoupon
            return render_error_response("You must supply a valid coupon", :not_acceptable)
        end

        set_push_messages_after_subscription_update
        set_push_messages_after_subscription_create

        # we don't currently use Subscription in the front-end,
        # so we can just return an empty object here
        @meta['default_card'] = owner.default_stripe_card
        render_record_hashes([@subscription.as_json], 'subscriptions')
    end


    def destroy
        in_transaction_with_editor_tracking do
            render_error_if_not_found(Subscription, params['id']) do |subscription|

                render_unauthorized_error and return unless current_ability.can?(:destroy, subscription)

                subscription.cancel_and_destroy(reason: 'Deleted through API', prorate: false)

                # subscriptions can be in the current_user or in the hiring_team,
                # so push down both
                set_primary_subscription_push_messages
                set_careers_push_messages

                render_records([])
            rescue ActiveRecord::RecordInvalid => err
                render_error_response(err.message, :not_acceptable)
            end
        end
    end

    # Endpoint for updating payment details in Stripe.
    # Note that this does not require a subscription, but it didn't make
    # sense to put it anywhere else.
    def modify_payment_details
        owner_id = params[:owner_id]
        source = params[:source]

        owner = User.find_by_id(owner_id) || HiringTeam.find_by_id(params[:owner_id])

        if source.nil?
            render_error_response('No source token', :not_acceptable)
        elsif !current_ability.can?(:modify_payment_details_with_params, params)
            render_unauthorized_error
        else
            # if update_stripe_card_if_specified_and_pay_unpaid_invoices renders an error, it will return false,
            # so we shouldn't try to render again in that case
            return unless update_stripe_card_if_specified_and_pay_unpaid_invoices
            render_successful_response({})
        end
    end

    ###############################
    # Helpers
    ###############################

    private
    def update_stripe_card_if_specified_and_pay_unpaid_invoices
        # update customer's payment source
        return unless update_stripe_card_if_specified # ensures customer record and updates billing source

        # when updating the customer’s payment source, pay any open and unpaid invoices
        old_num_unpaid_invoices = 0
        new_num_unpaid_invoices = 0
        remaining_invoices_error = nil
        owner.subscriptions.each do |subscription|
            old_num_unpaid_invoices += subscription.num_unpaid_invoices
            begin
                EditorTracking.transaction(current_user) do
                    subscription.charge_unpaid_invoices
                end
            rescue Stripe::CardError => err
                new_num_unpaid_invoices += subscription.num_unpaid_invoices
                remaining_invoices_error = err
            end
        end

        # render appropriate response
        if remaining_invoices_error
            num_successfully_paid_invoices = old_num_unpaid_invoices - new_num_unpaid_invoices
            client_error = "#{num_successfully_paid_invoices} of #{old_num_unpaid_invoices} overdue invoices were paid. Last payment failed with error: #{remaining_invoices_error.message}" # FIXME: localize
            render_error_response(client_error, 402)
            return false
        end

        set_push_messages_after_subscription_update
        return true
    end

    private
    def owner
        unless defined? @owner
            # modify_payment_details endpoint has an owner_id param.
            # When dealing with a subscription, the subscription has a
            # user_id or a hiring_team_id
            if params[:owner_id] && params[:owner_id] == current_user_id
                @owner = current_user
            elsif params[:owner_id]
                @owner = HiringTeam.find_by_id(params[:owner_id]) || User.find_by_id(params[:owner_id])
                raise "No owner found" unless @owner
            elsif params[:record][:user_id]
                @owner = current_user
            elsif params[:record][:hiring_team_id]
                hiring_team = HiringTeam.find_by_id(params[:record][:hiring_team_id])
                @owner = hiring_team
            end
        end
        @owner
    end

    private
    def set_push_messages_after_subscription_update
        @meta['push_messages'] ||= {}

        if owner.is_a?(User)
            current_user.cohort_applications.reload # not sure why this is necessary, but otherwise add_cohort_applications_to_push_messages does not work
            add_cohort_applications_to_push_messages
        else
            add_last_updated_to_push_messages(:open_position, "current")
            @meta['push_messages'].deep_merge!({
                'current_user' => {
                    'hiring_team' => current_user.hiring_team.reload.as_json
                }
            })
        end
    end

    private
    def set_push_messages_after_subscription_create
        return unless owner.is_a?(HiringTeam)
        return unless @subscription&.id

        # There are two ways that a position might have changed due to this request:
        # 1. The subscription created is related to the position
        # 2. The user is switching from pay-per-post to unlimited, at which point
        #       all of the existing positions will have their subscriptions canceled
        #       and will be assigned auto_expiration_dates
        # The CareersNetworkViewModelInterceptor will handle the `open_positions` passed
        #       down in the metadata
        return unless @position_updated_at_before_changes
        @meta['open_positions'] = owner.open_positions.where("open_positions.updated_at > ?", Time.at(@position_updated_at_before_changes)).map(&:as_json)
    end
end
