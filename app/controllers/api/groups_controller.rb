class Api::GroupsController < Api::ApiCrudControllerBase

	authorize_resource :class => :group

	def collection_key(record)
		"groups"
	end

	# POST /api/groups
    def create
        rescue_validation_error_or_render_record do
            AccessGroup.create_from_hash!(params['record'])
        end
    end

	# GET /api/groups
	def index
		access_groups = AccessGroup.includes(:lesson_stream_locale_packs).reorder(:name).all
		render_records(access_groups.as_json)

	end

	# PATCH/PUT /api/groups
    def update
        rescue_validation_error_or_render_record do
            render_error_if_not_found(AccessGroup, params['record']['id'] ) do |record|
                record.update_from_hash!(params['record'])
            end
        end
    end

    # DELETE /api/groups/1
    def destroy
        delete_record(AccessGroup, params['id'])
    end

end
