class Api::StudentNetworkMapsController < Api::ApiCrudControllerBase
    authorize_resource :class => false
    before_action :authorize_index_with_params, :only => :index

    include ControllerMixins::StudentNetworkFiltersMixin
    include ControllerMixins::CareerProfilesFiltersMixin

    # GET /#{model.table_name}.json
    instrument_method
    def index
        filters = params[:filters]

        # clean up params
        # TODO: Sanitize all params to prevent SQLinjection (i.e.: ensure they're within a valid range)?
        filters[:zoom] = filters[:zoom].to_i || 3
        filters[:marker_width] = filters[:marker_width].to_i || 20

        json = get_json_for_index(filters)
        render_contents_json_string("student_network_maps", "[#{json}]")
    end

    def get_json_for_index(filters)
        zoom = filters[:zoom]
        marker_width = filters[:marker_width]
        query = process_career_profiles_filters(filters)
        query = process_student_network_filters(filters, query).get_geo_cluster_json(zoom, marker_width)
        query
    end

    def authorize_index_with_params
        if current_ability.cannot?(:index_student_network_maps, params)
            render_unauthorized_error
            false
        else
            true
        end
    end
end
