class Api::ContentTopicsController < Api::ApiCrudControllerBase
    authorize_resource

    # GET /api/content_topics
    def index
        render_records(ContentTopic.includes(:en_lesson_streams).reorder(Arel.sql("locales->>'en'")).to_a)
    end

    # POST /api/content_topics
    def create
        rescue_validation_error_or_render_record do
            ContentTopic.create_from_hash!(params['record'])
        end
    end

    # PUT /api/content_topics
    def update
        rescue_validation_error_or_render_record do
            render_error_if_not_found(ContentTopic, params['record']['id'] ) do |record|
                record.update_from_hash!(params['record'])
            end
        end
    end

    # DELETE /api/content_topics
    def destroy
        rescue_validation_error_or_render_record do
            ContentTopic.destroy(params[:id])
        end
    end
end