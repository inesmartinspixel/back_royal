# 1. When the user wants to verify zir ID, ze will click a button
# in the UI and hit `request_idology_link`, queueing up a `RequestIdologyLinkJob`
#
# 2. That job makes the request to idology to get a link, saves an
# instance of IdologyVerification, sends a message to customer.io which
# triggers a message to be sent to the user with the link.
#
# 3. As long as the user leaves the id verification modal open in
# zir browser, a request will hit ping every 5 seconds.  Starting 30 seconds
# after the link was originally created, this will create a `GetIdologyResultJob`
# every 30 seconds.  (In the normal case this is unnecessary.  See next step.)
#
# 4. Once the user has gone over to IDology and completed the verification process,
# IDology will hit the #hook endpoint here, creating a `GetIdologyResultJob`.  (This is
# why step 3 is unnecessary.  We only need step 3 if IDology fails to hit the #hook
# endpoint, which is possible because they have no retry logic)
#
# 5. The `GetIdologyResultJob` will fetch the result from IDology, save it on the
# idology_verification and create a user_id_verification
#
# 6. The client will detect the existence of the new user_id_verification and
# show the success message

class Api::IdologyController < Api::ApiCrudControllerBase

    skip_before_action :verify_signed_in

    # IDology will make a request to this endpoint when a person uploads
    # an id and selfie.  We then queue up a job that will request the full
    # info from IDology. (Also check out ping below, which we also use to
    # check for results)
    def hook

        # <response>
        #   <id-number>2099850841</id-number>
        #   <id-scan-result>
        #       <key>capture.completed</key>
        #       <message>Completed</message>
        #   </id-scan-result>
        # </response>"
        #
        # maps to
        # {"response"=>{
        #   "id_number"=>"2114694837",
        #   "id_scan_result"=>{"key"=>"capture.completed", "message"=>"Completed"}
        # }}

        idology_response = request.body.read

        id_number = begin
            idology_response = Hash.from_xml(idology_response)['response']
            idology_response['id_number']
        rescue Exception => e
            nil
        end

        if id_number
            GetIdologyResultJob.perform_later(id_number)
        else
            Raven.capture_exception("IDology hook with no id_number.",
                extra: {
                    response: idology_response
                }
            )
        end

        head :ok
    end

    def request_idology_link
        # Sometimes, `can?` might return false here because of rate limiting, in which
        # case it would be better for us to return a 429.  But it's not important enough to bother with now
        return render_unauthorized_error unless current_ability.can?(:request_idology_link_with_params, params)
        RequestIdologyLinkJob.perform_later({
            user_id: params[:user_id],
            email: params[:email],
            phone: params[:phone]
        })
    end

    def ping
        return render_unauthorized_error unless current_ability.can?(:ping_idology_with_params, params)

        # The client will hit this endpoint every 5 seconds so that we can
        # get a quick response once id verification is completed.  But, we
        # also need to poll idology (see comment at top of file for more info).
        # We don't want to hit IDology
        # every 5 seconds, so we use `ensure_job_if_necessary` so that we can hit them every 30 seconds.
        user = current_user&.id == params['user_id'] ? current_user : User.find(params['user_id'])
        user.idology_verification_with_most_recent_activity&.ensure_job_if_necessary

        set_id_verification_push_messages
        head :ok
    end
end