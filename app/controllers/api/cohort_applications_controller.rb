class Api::CohortApplicationsController < Api::ApiCrudControllerBase


    skip_load_and_authorize_resource only: [:sign]
    skip_before_action :verify_signed_in, only: [:sign]

    load_and_authorize_resource

    # Note: this method is used by the internal Zapier CLI app; if we open this up to non-admins, we'd be opening up that as well.
    # Tests in the Zapier Internal app will also fail in that case.
    def index
        render_records(preload_cohort_applications_for_index)
    end

    # POST /api/cohort_applications
    def create
        return unless sanitize_application(params)

        rescue_validation_error_or_render_record do
            cohort_application = CohortApplication.create_from_hash!(params['record'], {is_admin: current_ability.can?(:manage, CohortApplication)})
            set_relevant_cohort_push_messages
            set_user_push_messages(reload: true)
            set_institution_push_messages
            cohort_application
        end
    end

    def update
        rescue_validation_error_or_render_record do
            begin
                instance = CohortApplication.update_from_hash!(params[:record], params[:meta])
                set_institution_push_messages
                instance
            rescue CohortApplication::UnauthorizedError
                render_unauthorized_error
                return false
            end
        end
    end

    def batch_update
        render_unauthorized_error and return unless current_ability.can?(:manage, CohortApplication)
        in_transaction_with_editor_tracking do
            ids = params[:records].map { |e| e['id'] }

            # we don't actually render json, but `includes_needed_for_json_eager_loading` is probably
            # a pretty good guess of what we're gonna need for the callbacks that run on save, so
            # we include it to speed things up
            cohort_applications = CohortApplication.includes(CohortApplication.includes_needed_for_json_eager_loading).find(ids).index_by(&:id)
            params[:records].each do |attrs|
                permitted_attrs = attrs.permit(:id, :cohort_slack_room_id)
                id = permitted_attrs[:id]
                cohort_applications[id].update(permitted_attrs)
            end
        end

        render_successful_response({})
    end

    def refund_details
        render_unauthorized_error and return unless current_ability.can?(:manage, CohortApplication)
        render_successful_response({
            refund_details: User.find(params['filters']['user_id']).last_application&.refund_details
        })
    end

    def sign
        cohort_application = CohortApplication.find(params["id"])

        render_not_found_error(CohortApplication, params["id"]) and return unless cohort_application.should_have_enrollment_agreement?

        cohort_application.ensure_enrollment_agreement_if_necessary
        cohort_application.user.signable_documents.reload
        signable_document = cohort_application.enrollment_agreement

        render_not_found_error(CohortApplication, params["id"]) and return if signable_document.nil?

        redirect_to signable_document.sign_now_signing_link
    end

    def destroy
        delete_record(CohortApplication, params[:id])
    end

    def after_destroy_record(record)
        set_relevant_cohort_push_messages
    end

    # FIXME: See if we can migrate this and possibly the cohort_application.rb#merge_hash permission logic to ability.rb
    def sanitize_application(params)
        cohort = Cohort.find_by_id(params['record']['cohort_id'])
        if !cohort.user_can_create_application?(current_user) && current_ability.cannot?(:manage, CohortApplication)
            render_unauthorized_error
            return false
        end

        # only admins can create applications for users other than themselves
        if params['record']['user_id'] && current_ability.cannot?(:manage, CohortApplication)
            render_unauthorized_error
            return false
        elsif !params['record']['user_id']
            params['record']['user_id'] = current_user.id
        end

        return true
    end

    def preload_cohort_applications_for_index
        query = CohortApplication.includes(*CohortApplication.includes_needed_for_json_eager_loading)

        if params[:zapier] == "true"

            # For actions that trigger on both new and updated cohort applications we need to also factor in updates to the career_profile
            if params[:include_updates] == "true"
                # when setting up a zap, we need the api to return one profile.  Get any that have been
                # update in the last day, plus one more just to be sure
                pending_application = CohortApplication.where(status: 'pending').first
                query = query.where(status: 'pending')

                query = query.where("(career_profiles.updated_at > ? OR cohort_applications.updated_at > ?) or cohort_applications.id = ?",
                    zapier_updated_at_threshold, zapier_updated_at_threshold, pending_application.id)
                query = query.joins("join career_profiles on career_profiles.user_id = cohort_applications.user_id")

                # This makes it easier to test changes locally since we know that we will get the most recently updated application (or profile)
                query = query.reorder(Arel.sql("GREATEST(cohort_applications.updated_at, career_profiles.updated_at) DESC"))
            end
        end

        if !sort_column.blank?
            query = query.reorder("#{sort_column} #{sort_direction}")
        end

        if !limit.blank?
            query = query.limit(limit)
        end

        cohort_applications = query.all.to_a

        # this is not defined as an association and so can't use active record preloading. It's not
        # bad though since it's just one query per cohort, and those are shared across applications.  Preload it
        # here so that specs can check for all other things
        cohort_applications.map(&:published_cohort).uniq.map(&:application_deadline)

        cohort_applications
    end

    # separate method for testing
    def zapier_updated_at_threshold
        1.day.ago
    end

    def limit
        limit = params[:limit]
        !limit.blank? ? [limit.to_i, 0].max : nil
    end

    def sort_column
        column = params[:sort] ? params[:sort].first : nil

        allowed_columns = CohortApplication.column_names
        allowed_columns.include?(column) ? column : nil
    end

    def sort_direction
        %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
    end
end