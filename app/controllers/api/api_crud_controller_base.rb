class Api::ApiCrudControllerBase < ApplicationController

    include Skylight::Helpers
    include DeviseTokenAuth::Concerns::SetUserByToken

    before_action :set_headers
    # do not use CSRF for CORS requests
    # see http://nils-blum-oeste.net/cors-api-with-oauth2-authentication-using-rails-and-angularjs/
    skip_before_action :verify_authenticity_token
    before_action :verify_signed_in
    before_action :convert_record_json
    before_action :decode_json_params, :decode_array_params

    # set_client_config has to be after decode_json_params if using_deprecated_client?
    # is going to use the params
    before_action :set_client_config
    before_action :require_guid_id
    before_action :copy_id_into_params, :only => :update
    before_action :save_event_bundle_from_meta
    before_action :update_last_seen_at
    #  before_action :verify_admin_access, :except => [:show, :index]

    attr_reader :client_config
    attr_reader :meta

    class TooManyEventsInBundle < RuntimeError; end
    rescue_from TooManyEventsInBundle do |exception|

        # We don't really expect this, so log it to raven
        Raven.capture_exception(exception, {level: 'warning'})
        render_error_response("Request too large", 413)
    end

    class UserFromAuidDeleted < RuntimeError; end
    rescue_from UserFromAuidDeleted do |exception|
        Raven.capture_exception(exception, {
            level: 'warning',
            extra: {
                user_from_auid_deleted: @user_from_auid_deleted
            }
        })
        render_error_response("AUID matches deleted user", 406, {
            user_from_auid_deleted: true
        })
    end

    class MalformedUserId < RuntimeError; end
    rescue_from MalformedUserId do |exception|

        # We don't really expect this, so log it to raven
        Raven.capture_exception(exception, exception.raven_options)
        render_error_response("Malformed user id", 400)
    end


    def initialize
        @meta = {}
    end

    def collection_key(record)
         record.class.table_name
    end

    def require_guid_id
        if params.key?(:id) && !params[:id].match(/[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/i)
            if Rails.env.development?
                raise "That ain't no guid"
            end

            # Investigating https://trello.com/c/nDngIof6
            Raven.capture_exception("That ain't no guid", {
                level: 'warning',
                extra: {
                    id: params[:id]
                }
            })

            render_not_found_error
            return false
        end
    end

    # React forms send data up as multipart form data rather
    # than as application/json data.  One of the values in the
    # form is a json encoded record.  Since the request is not encoded
    # as application/json, rails will not automatically decode it,
    # so we have to do so here.
    def convert_record_json
        if params[:record].is_a?(String)
            params[:record] = ActiveSupport::JSON.decode(params[:record])
        end
        if params[:meta].is_a?(String)
            params[:meta] = ActiveSupport::JSON.decode(params[:meta])
        end
    end

    # we do this in order to play nice with CanCan.  CanCan only works in update calls
    # as expected if there is an id parameter at the top level
    def copy_id_into_params
        params[:id] ||= params[:record] && params[:record][:id]
    end

    # Removes null-bytes from String values
    def sanitize_str(str)
        str.gsub("\u0000", "")
    end

    # See also: https://www.owasp.org/index.php/OWASP_Periodic_Table_of_Vulnerabilities_-_Null_Byte_Injection
    # Recursively modifies in-place the provided Parameters object, stripping null-bytes from String values.
    # Favors re-assignment to prevent any FrozenError's for things coming off headers, etc.
    def sanitize_params(params_to_sanitize)
        params_to_sanitize.keys.each do |key|
            val = params_to_sanitize[key.to_sym]
            if val.is_a?(Array)
                val.each_with_index do |v, i|
                    if v.is_a?(String)
                        val[i] = sanitize_str(v)
                    elsif v.is_a?(ActionController::Parameters)
                        sanitize_params(v)
                    end
                end
            elsif val.is_a?(ActionController::Parameters)
                sanitize_params(val)
            elsif val.is_a?(String)
                params_to_sanitize[key.to_sym] = sanitize_str(val)
            end
        end

        params_to_sanitize
    end

    # This is a bit of a heavy hammer, but we've seen null-bytes make it down to the pg adapter
    # level, which raises and prevents any writes. Seems reasonable to attempt to eliminate them
    # in a generic fashion, using the sanitization strategy above.
    def params
        unless @params_sanitized
            sanitize_params(super)
            @params_sanitized = true
        end
        super
    end

    # When we have objects or arrays as the values in get queryparams,
    # they end up getting sent to rails as strings. (See the following
    # javascript code for an example of how this would happen).
    #
    # decode_json_params and decode_array_params convert a few fields that
    # we expect to be json or array fields into ruby objects or arrays
    #
    # Lesson.index({
    #     filters: {
    #         published: false,
    #         archived: scope.archived
    #     },
    #     fields: ['id', 'title', 'tag',
    #         'modified_at', 'published_at', 'updated_at',
    #         'author', 'stream_titles', 'lesson_type', 'archived'
    #     ]
    # })

    # can be overridden
    def extra_json_fields
        []
    end

    def decode_json_params
        # FIXME: Irregular input can break this scheme -- for example, supply a value for `[:filters]` in the
        # Browse Candidates Keywords section that contains a `;`  ... ie - `asdf;asdf`
        # By the time it is accessible via `params` it has been converted to `"asdf", "asdf"`
        ([:filters, :meta] + extra_json_fields).each do |key|
            if params[key] && params[key].is_a?(String)
                begin
                    decoded = ActiveSupport::JSON.decode(params[key])
                    params[key] = decoded.respond_to?(:with_indifferent_access) ? decoded.with_indifferent_access : decoded
                rescue Exception => e
                    raise ArgumentError.new("Could not decode json param: #{key}=#{params[key].inspect} (#{e.class}: #{e.message})")
                end
            end
        end
    end

    def decode_array_params
        [:fields, :lesson_fields, :except, :sort].each do |key|
            params[key] = Array.wrap(params[key]) if params.key?(key)
        end
    end

    # Following the example of an ID-based json API at https://gist.github.com/wycats/5500104,
    # we will render an array for each type of record returned.
    #
    # We also include metadata in the response, to be used in a similar way to
    # flash messages.  For example, the update action in LessonController sometimes
    # adds warnings to the metadata
    #
    # Example:
    # {
    #   'metadata' => {'warning' => ['A warning']},
    #   'response' => {
    #     "posts": {
    #       "id": 1,
    #       "title": "Rails is Omakase",
    #       "rels": {
    #         "author": 9
    #       }
    #     },
    #     "people": [{
    #       "id": 9,
    #       "name": "@d2h"
    #     }]
    #   }
    # }

    # This calls the model's instance method as_json once for each record
    # which encourages you to put the db query into the model's as_json method,
    # and do one query per record. This will scale in O(n) where n is the number
    # of model instances (lessons or streams) - fine for now but a candidate for
    # later optimiation.
    instrument_method
    def render_records(records, options = {})
        options = options.merge(:user_id => user_id, :zapier => params[:zapier])
        records = [records] if !records.is_a?(Array)

        _collection_key = nil
        record_hashes = records.map do |record|
            collection_key_for_record = collection_key(record)
            if _collection_key && _collection_key != collection_key_for_record
                raise RuntimeError.new("Only one type of record can be passed into render_records. #{_collection_key} != #{collection_key_for_record}")
            end
            _collection_key = collection_key_for_record
            record.as_json(options)
        end

        render_record_hashes(record_hashes, _collection_key)
    end

    def render_record_hashes(record_hashes, collection_key)
        response = Hash.new { |hash, key| hash[key] = [] }
        response[collection_key] = []
        record_hashes.each do |record_hash|
            response[collection_key] << record_hash
        end

        render_successful_response response
    end

    instrument_method
    def render_successful_response(response)
        respond_to do |format|
            format.json { render json: {
                :contents => response,
                :meta => @meta
            }.as_json(:public => true, :user_id => user_id), status: :ok  }
        end
    end

    def render_not_found_or_contents_json_string(klass, id, json)
        if json == '[]'
            render_not_found_error(klass, id)
        else
            render_contents_json_string(klass, json)
        end
    end

    instrument_method
    def render_contents_json_string(klass_or_collection_key, json)
        collection_key = klass_or_collection_key.is_a?(String) ? klass_or_collection_key : klass_or_collection_key.table_name
        response_hash = {meta: @meta, contents: {}}
        response_hash[:contents][collection_key] = "REPLACE"

        # we need to doble-up the backslashes so that lessons like this one work:
        # http://localhost:3001/stream/222d17ff-718d-4bbc-b4df-444e71e7aec9/chapter/0/lesson/fd6c37dc-c9c3-4b07-bff3-bfb4a8b18fa9/show
        response_json = response_hash.to_json.gsub("\"REPLACE\"", json.gsub(/(\\)/) { |str| $1+$1 })

        respond_to do |format|
            format.json {
                render json: response_json,
                status: :ok
            }
        end
    end

    def render_error_if_not_found(klass, value)
        if record = klass.find_by_id(value)
            yield record
        else
            render_not_found_error(klass, value)
            nil
        end
    end

    def render_not_found_error(klass = nil, value = nil)
        if klass && value
            message = I18n.t(:no_x_found_for_id, {
                klass_name: klass.table_name.singularize,
                id: value.inspect
            })
        else
            message = I18n.t(:we_cannot_find_what_you_are_looking_for)
        end
        render_error_response(message, :not_found)
    end

    def show_record(klass, value)
        render_error_if_not_found(klass, value, block) do |record|
            render_records(record)
        end
    end

    def delete_record(klass, value, &block)
        # if a block is passed in, run that and the destroy in a transaction
        in_transaction_with_editor_tracking do
            render_error_if_not_found(klass, value) do |record|

                render_unauthorized_error and return unless current_ability.can?(:destroy, record)

                if block_given?
                    return unless yield(record)
                end

                begin
                    record.destroy

                    # If we need to set push messages then define this method in the appropriate
                    # controller.
                    after_destroy_record(record) if respond_to?(:after_destroy_record)

                    render_records([])
                rescue ActiveRecord::RecordInvalid => err
                    render_error_response(err.message, :not_acceptable)
                end
            end
        end
    end

    def in_transaction_with_editor_tracking(&block)
        EditorTracking.transaction(current_user) do
            yield
        end
    end

    def rescue_validation_error_or_render_record(options = {}, with_transaction = true, &block)
        begin
            # Automatically retries in the event of ActiveRecord::PreparedStatementCacheExpired
            if with_transaction
                record = in_transaction_with_editor_tracking { yield }
            else
                record = yield
            end

            render_records(record, options) if record
        rescue ActiveRecord::RecordInvalid => e
            render_error_response(e.message, :not_acceptable)

        # We have a number of different instances of OldVersion handling out there.  It probably
        # makes sense to migrate at least some of them to use this catch here.
        rescue OldVersionException => e
            render_error_response(e.message, :not_acceptable)

        rescue ActiveRecord::RecordNotUnique => e

            render_error_response("The thing you tried to save already exists in the database and cannot be duplicated.", :not_acceptable)

        end
    end

    def add_message(message, type = :info)
        @meta[type] ||= []
        @meta[type] << message
    end

    # for CORS stuff, see http://nils-blum-oeste.net/cors-api-with-oauth2-authentication-using-rails-and-angularjs/
    def options
        render :text => '', :content_type => 'text/plain'
    end

    def set_headers
        headers['Access-Control-Allow-Origin'] = '*'
        headers['Access-Control-Allow-Methods'] = 'POST, GET, PUT, DELETE, OPTIONS'
        headers['Access-Control-Allow-Headers'] = '*, X-Requested-With, X-Prototype-Version, X-CSRF-Token, Content-Type'
        headers['Access-Control-Max-Age'] = "1728000"


        head(:ok) if request.request_method == "OPTIONS"
    end

    def user_id
        current_user ? current_user.id : nil
    end

    def set_client_config

        if client_params['fr-client'].nil? || client_params['fr-client-version'].nil?
            # marketing pages will not set client info, and do not expect any to come back
            return
        end

        client_version = client_params['fr-client-version'].to_i
        client = client_params['fr-client']

        # if client=web, for example, the class is WebClientConfig
        client_config_class = ("ClientConfig::" + "#{client}_client_config".camelize).constantize
        @client_config = client_config_class.new(client_version)

        # A specific controller can implement `using_deprecated_client?` to show
        # a deprecation message for particular api requests.
        # For example, one time we said that any hiring manager using the browse
        # feature from an old client should be forced to upgrade.
        #
        # This can only be done, however, from web clients, so it's applicability
        # is limited.  The reason for this is that updating a mobile client is
        # not as simple as telling the user the refresh the page. In the example
        # described above, it was ok since all hiring managers are using web clients.
        if using_deprecated_client? && client_config_class != ClientConfig::WebClientConfig
            raise RuntimeError.new("We can only force reloads on deprecated web clients")
        elsif using_deprecated_client?
            @meta['min_allowed_client_version'] = client_version + 1
            @meta['min_suggested_client_version'] =  @meta['min_allowed_client_version']
        else
            @meta['min_allowed_client_version'] = @client_config.min_allowed_version
            @meta['min_suggested_client_version'] = @client_config.min_suggested_version
        end

        @meta['client_app_store_name'] = @client_config.app_store_name
        @meta['client_app_store_name_miyamiya'] = @client_config.app_store_name_miyamiya

        #@meta['upgrade_message'] = "Here *is* a paragraph.\n\nAnd **another**."
        # NOTE: we have not thought about how to localize custom upgrade messages.  For now, we never
        # use them, but if we ever do, we'll have to consider that.

        abort_if_using_deprecated_version

    end

    # see comment above in set_client_config
    def using_deprecated_client?
        nil
    end

    def abort_if_using_deprecated_version
        if using_deprecated_client? || @client_config.deprecated?
            render_successful_response({})
        end
    end

    # needed by StatusController, StudentDashboardsController, and CohortApplicationsController
    def set_user_push_messages(reload: false, extra: {})
        return unless current_user
        current_user.reload if reload

        @meta['push_messages'] ||= {}
        @meta['push_messages'].deep_merge!({
            'current_user' => {
                'active_playlist_locale_pack_id' => current_user.active_playlist_locale_pack_id,
                'can_purchase_paid_certs' => current_user.can_purchase_paid_certs,
                'mba_content_lockable' => current_user.mba_content_lockable,
                'curriculum_status' => current_user.last_application&.curriculum_status,
                'deactivated' => current_user.deactivated,
                'avatar_url' => current_user.avatar_url, # add since this can be updated by a delayed job
                'identity_verified' => current_user.identity_verified,
                'transcripts_verified' => current_user.transcripts_verified,
                's3_identification_asset' => current_user.s3_identification_asset.as_json,
                's3_transcript_assets' => current_user.s3_transcript_assets.as_json,
                's3_english_language_proficiency_documents' => current_user.s3_english_language_proficiency_documents.as_json,
                'english_language_proficiency_documents_approved' => current_user.english_language_proficiency_documents_approved,
                'english_language_proficiency_documents_type' => current_user.english_language_proficiency_documents_type,
                'english_language_proficiency_comments' => current_user.english_language_proficiency_comments,
                'notify_candidate_positions' => current_user.notify_candidate_positions,
                'notify_candidate_positions_recommended' => current_user.notify_candidate_positions_recommended,
                'signable_documents' => current_user.signable_documents.as_json,
                'updated_at' => current_user.updated_at.to_timestamp,
                'academic_hold' => current_user.academic_hold,
                'enable_front_royal_store' => current_user.enable_front_royal_store,
                'net_amount_paid' => current_user.net_amount_paid
            }.merge(extra)
        })
    end

    # needed by StatusController, SubscriptionsController
    def set_primary_subscription_push_messages
        return unless current_user

        @meta['push_messages'] ||= {}
        @meta['push_messages'].deep_merge!({
            'current_user' => {
                'subscriptions' => current_user.subscriptions.as_json
            }
        })
    end

    # needed by StatusController and IdologyController
    def set_id_verification_push_messages
        return unless current_user
        @meta['push_messages'] ||= {}
        @meta['push_messages'].deep_merge!({
            'current_user' => {
                'user_id_verifications' => current_user.user_id_verifications_for_json,
                'idology_verifications' => current_user.idology_verifications_for_json
            }
        })
    end

    # needed by StatusController, CareerProfilesController, CohortApplicationsController,
    # and DeferralLinksController#update
    def set_relevant_cohort_push_messages
        return unless current_user && current_user.relevant_cohort
        @meta['push_messages'] ||= {}
        @meta['push_messages'].deep_merge!({
            'current_user' => {
                'relevant_cohort' => current_user.relevant_cohort.as_json(user_id: current_user_id),
                'program_type_confirmed' => current_user.program_type_confirmed
            }
        })
    end

    def set_institution_push_messages
        return unless current_user
        @meta['push_messages'] ||= {}
        @meta['push_messages'].deep_merge!({
            'current_user' => {
                'institutions' => current_user.institutions.as_json,
                'active_institution' => current_user.active_institution.as_json
            }
        })
    end

    # needed by StatusController, OpenPositionsController, SubscriptionsController
    def set_careers_push_messages
        return unless current_user

        @meta['push_messages'] ||= {}
        @meta['push_messages'].deep_merge!({
            'current_user' => {
                'can_edit_career_profile' => current_user.can_edit_career_profile
            }
        })

        if (current_user.career_profile || current_user.hiring_application)

            if current_user.career_profile
                @meta['push_messages'].deep_merge!({
                    'career_profile' => {
                        'updated_at' => current_user.career_profile.updated_at.to_timestamp
                    }
                })

                # Since we need to do a query to send this information back let's make sure that we
                # only do it when necessary, which is when a user is not already transcripts_verified
                # and is one of the valid_enrollment_statuses, which is when the student-dashboard-enrollment
                # UI is shown
                if !current_user.transcripts_verified &&
                    CohortApplication.valid_enrollment_statuses.include?(current_user.last_application&.status)
                    @meta['push_messages'].deep_merge!({
                        'career_profile' => {
                            'education_experiences' => current_user.career_profile.education_experiences
                                .includes(CareerProfile::EducationExperience.includes_needed_for_json_eager_loading)
                                .as_json
                        }
                    })
                end
            end

            if current_user.hiring_application

                @meta['push_messages'].deep_merge!({
                    'current_user' => {
                        'hiring_team' => current_user.hiring_team,
                        'hiring_team_id' => current_user.hiring_team_id
                    }
                })

                # Also update the status of the hiring manager's hiring_application
                @meta['push_messages'].deep_merge!({
                    'hiring_application' => {
                        'status' => current_user.hiring_application.status
                    }
                })
            end
        end

    end

    # needed by StatusController, CareerProfilesController, SubscriptionsController,
    # and DeferralLinksController#update
    def add_cohort_applications_to_push_messages
        return unless current_user
        @meta['push_messages'] ||= {}
        @meta['push_messages'].deep_merge!({
            'current_user' => {
                'cohort_applications' => current_user.cohort_applications.as_json
            }
        })
    end

    # needed by StatusController, StudentDashboardsController, CareerProfilesController, and CandidatePositionInterestsController
    instrument_method
    def add_num_recommended_positions_to_push_messages
        # If the user has an editable_and_complete career_profile then recompute the
        # number of unreviewed, recommended positions.
        return unless current_user && current_user.career_profile&.career_profile_editable_and_complete?

        query = OpenPosition.all.recommended_and_viewable(candidate_or_id: current_user)

        @meta['push_messages'] ||= {}
        @meta['push_messages'].deep_merge!({
            'current_user' => {
                'num_recommended_positions' => query.count
            }
        })
    end

    instrument_method
    def set_progress_max_updated_at_before_changes
        set_progress_max_updated_at('progress_max_updated_at_before_changes')
    end

    instrument_method
    def set_progress_max_updated_at(key = 'progress_max_updated_at')
        @meta['push_messages'] ||= {}

        if current_user

            # find the last updated progress record.  This has to be done
            # before the action so that we don't change it in calls to lesson_progress
            # or lesson_streams_progress
            sql = "SELECT MAX(max_updated_at) as progress_max_updated_at FROM (
                       SELECT MAX(updated_at) as max_updated_at FROM lesson_progress WHERE user_id = '#{current_user.id}'
                            UNION ALL
                       SELECT MAX(updated_at) as max_updated_at FROM lesson_streams_progress WHERE user_id = '#{current_user.id}'
                   ) as both_maxes"
            progress_max_updated_at = ActiveRecord::Base.connection.execute(sql)[0]["progress_max_updated_at"]

            # initialize to 0 to make sure that it works even for
            # a user who starts off with no progress.  See https://trello.com/c/v6sxsVuO
            @meta['push_messages'][key] = progress_max_updated_at.to_timestamp
        end

    end

    def set_learner_projects_meta
        @meta['learner_projects'] = LearnerProject.all.map(&:as_json)
    end

    def set_stripe_admin_meta
        # we may want to relax these at some point -- will effectively require an app-restart to update atm
        @stripe_plans = SafeCache.fetch("stripe/plans_list", expires_in: 1.year) do
            Stripe::Plan.list({ :limit => 100, :expand => ['data.product'] }).data.map do |plan|

                h = plan.to_h

                plan_name = plan.nickname

                # FIXME: 'bi_annual' doesn't align with other naming conventions. potentially annoying refactor.
                if (h[:interval] == "month" && h[:interval_count] == 6)
                    frequency = "bi_annual"
                elsif (h[:interval] == "month" && h[:interval_count] == 1)
                    frequency = "monthly"
                elsif (h[:interval] == "year" && h[:interval_count] == 1 && h[:metadata]["total_num_required_payments"] == "1")
                    frequency = "once"
                else
                    raise "Unknown plan frequency for plan #{plan_name}!"
                end

                h.slice(:id, :amount).merge({ frequency: frequency, name: plan_name }).as_json
            end
        end

        @stripe_coupons = SafeCache.fetch("stripe/coupons_list", expires_in: 1.year) do
            # Ordering percent_off smallest -> biggest and amount_off smallest -> biggest
            Stripe::Coupon.list(limit: 100).map(&:to_h).
                unshift( {:id => "none", :amount_off => 0, :percent_off => 0} ).
                sort_by { |h| h[:percent_off] ? (-1.0 / h[:percent_off]) : h[:amount_off] }.
                map { |h| h.slice(:id, :amount_off, :percent_off).as_json }
        end

        @meta['stripe_coupons'] = @stripe_coupons
        @meta['stripe_plans'] = @stripe_plans
    end


    def assert_error_response(code, expected_response_body = nil)
        expect(response.status).to eq(code)
        check_body(expected_response_body) if expected_response_body
    end

    def save_event_bundle_from_meta
        if event_bundle = params[:meta] && params[:meta][:event_bundle]
            save_event_bundle(event_bundle["events"])
        end
    end

    # method that can be mocked in tests
    def now
        Time.now
    end

    instrument_method
    def save_event_bundle(events)

        # See https://trello.com/c/kcE0Hv1R.  This isn't really
        # enough to prevent someone intentionally trying to DOS us, but it might
        # help and it provides a server-side backstop to prevent any client issues
        # that lead to huge numbers of events being logged at once.
        if events.size > 2000
            err = TooManyEventsInBundle.new("Too many events in bundle.")
            err.raven_options = {
                extra: {event_count: events.size}
            }
            raise err
        end

        if user_from_auid_deleted?
            raise UserFromAuidDeleted.new("Trying to save events for a deleted user.")
        end

        # after an anonymous user logs in, we log one last event as the
        # anonymous user, even though there is now a current user. see
        # ValidationResponder
        force_auid = params[:meta] && params[:meta][:force_auid]

        begin
            # see http://docs.aws.amazon.com/ElasticLoadBalancing/latest/DeveloperGuide/x-forwarded-headers.html
            client_ip_address = request.env["HTTP_X_FORWARDED_FOR"].split(',')[0].strip
        rescue NoMethodError
            client_ip_address = nil
        end

        # See https://trello.com/c/MDafsCQb
        user_id = force_auid || current_user_id
        if !user_id&.looks_like_uuid?
            err = MalformedUserId.new("Bad user_id in event logger")
            err.raven_options = {
                extra: {
                    force_auid: force_auid,
                    current_user_id: current_user_id,
                    auid_from_headers: "-->#{request.headers['auid']}<--",
                    auid_from_cookies: "-->#{cookies['auid']}<--",
                    event_count: events.size,
                    event_types: events.slice(0, 40).pluck('event_type')
                }
            }
            raise err
        end

        # the .compact just makes testing easier, so we don't
        # always have to check for nil client_ip_address
        opts = {
            user_id: force_auid || current_user_id,
            hit_server_at: now,
            request_queued_for_seconds: params[:http_queue].nil? ? 0 : params[:http_queue][:queued_for_seconds],
            client_ip_address: client_ip_address,
            cloudflare_country_code: cloudflare_country_code
        }.compact
        Event.create_batch_from_hash!(events, opts)

        true

    end

    def update_last_seen_at
        return if params[:ghost_mode] == "true"

        if current_user && (current_user.last_seen_at.nil? || current_user.last_seen_at < 4.hours.ago)
            current_user.update(last_seen_at: Time.now)
            set_user_push_messages
        end
    end

end
