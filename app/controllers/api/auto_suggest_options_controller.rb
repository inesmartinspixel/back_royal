class Api::AutoSuggestOptionsController < Api::ApiCrudControllerBase

    include AutoSuggestOptionMixin

    authorize_resource :class => false
    skip_before_action :verify_signed_in, only: [:index, :show]

    # GET /api/auto_suggest_options
    def index
        options_query = AutoSuggestOption::ToJsonFromApiParams.new(params, current_user && current_user.has_role?(:admin))
        render_contents_json_string(options_query.base_klass, options_query.json)
    end

    # CREATE /api/auto_suggest_options
    def create
        rescue_validation_error_or_render_record do
            klass = get_klass_from_type(params[:record][:type])
            klass.create_from_hash!(klass.new(), params[:record])
        end
    end

    # PUT /api/auto_suggest_options
    def update
        rescue_validation_error_or_render_record do
            klass = get_klass_from_type(params[:record][:type])
            klass.update_all_locales_from_hash!(params[:record])
            klass.find(params[:record][:id])
        end
    end

    # POST /api/auto_suggest_options/upload_icon
    def upload_icon
        asset = S3Asset.create!({
            :file => params[:organization_icon],
            :directory => "organization_icons/",
            :styles => {

                # student dashboard
                "50x50"  => '50x50>',
                "100x100"  => '100x100>',
                "150x150"  => '150x150>',

                # stream dashboard
                "110x110"   => '110x110>',
                "220x220"   => '220x220>',
                "330x330"   => '330x330>'
            }.to_json
        })

        # Update the image_url if an existing record was passed in
        if params[:record] && params[:record][:type]
            klass = get_klass_from_type(params[:record][:type])

            return unless [ProfessionalOrganizationOption, CareerProfile::EducationalOrganizationOption].include?(klass)

            record = klass && klass.find_by_id(params[:record][:id])
            if !record.nil?
                record.image_url = asset[:formats]["original"]["url"]
                record.save!
            end
        end

        render_successful_response({'organization_icon' => asset})
    end

end