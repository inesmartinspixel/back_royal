class Api::ReportsController < Api::ApiCrudControllerBase

    before_action :check_maintenance_mode

    def create
        return render_unauthorized_error unless current_ability.can?(:create_report_with_params, params['record'])

        report = ::Report.create_from_hash!(params['record'])

        render_record_hashes(
            [report.as_json(locale: current_user.locale)],
            'reports'
        )
    rescue ActiveRecord::StatementInvalid => err
        if err.cause.is_a?(PG::ObjectNotInPrerequisiteState)
            enable_maintenance_mode
        else
            raise err
        end
    end

    # GET filter_options
    def filter_options
        report_type = params['report_type'] || 'Report'
        reportKlass = ::Report.get_klass_from_report_type(report_type)

        if params['institution_id']
            institution = Institution.find(params['institution_id'])
            return render_unauthorized_error unless current_ability.can?(:read_filter_options, institution)
            filter_options = reportKlass.get_filter_options_for_institution(institution, current_user.locale)
        elsif current_ability.can?(:read_admin_filter_options, nil)
            filter_options = reportKlass.get_filter_options(current_user.locale)
        else
            return render_unauthorized_error
        end

        render_record_hashes(filter_options.as_json, 'filter_options')
    end

    def render_empty_response
        render_record_hashes(
            [],
            'reports'
        )
    end

    def check_maintenance_mode
        if AppConfig.reporting_maintenance && AppConfig.reporting_maintenance.downcase == 'true'
            enable_maintenance_mode
        # if no records have yet been added to the user_lesson_progress_record table (which
        # happens as part of WriteRegularlyIncrementallyUpdatedTablesJob), then show maintenance mode
        elsif !Report::UserLessonProgressRecord.exists?
            enable_maintenance_mode
        else
            @meta['reporting_maintenance'] = 'false'
        end
    end

    def enable_maintenance_mode
        @meta['reporting_maintenance'] = 'true'
        render_empty_response
    end

end