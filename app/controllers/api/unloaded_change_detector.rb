=begin
    see https://trello.com/c/5KexJXW7 for details on the last_updated_at
    caching strategy and why we need to set the before and current values.

    This class works together with the client-side UnloadedChangeDetector.

    To track the last_updated at for a particula class of things, do the following:

    1. Call `track_last_updated_at` in this file to define the last_updated_at:

        track_last_updated_at(:thing) do
            current_user.things.maximum(:updated_at)
        end

    2. In the ping controller, call `add_last_updated_to_push_messages` to push down the
        current last_updated_at value

        before_action do
            add_last_updated_to_push_messages(:thing, 'current')
        end

    3. In any actions which might result in a change to the last_updated_at value,
        call `add_last_updated_to_push_messages` twice.  Once before the update
        and once after:

        before_action do
            add_last_updated_to_push_messages(:thing, 'before')
        end

        def update
            ...
            add_last_updated_to_push_messages(:thing, 'current')
        end

    4. In the client, create an UnloadedChangeDetector class linked to the :thing

        UnloadedChangeDetector.createDetectorKlass('thing');
=end
module Api::UnloadedChangeDetector

    cattr_accessor :last_updated_at_trackers

    def self.track_last_updated_at(key, &proc)
        Api::UnloadedChangeDetector.last_updated_at_trackers ||= {}

        raise "Cannot redefine last_updated_at tracker for #{key}" if Api::UnloadedChangeDetector.last_updated_at_trackers.key?(key)

        Api::UnloadedChangeDetector.last_updated_at_trackers[key] = {
            proc: proc
        }
    end

    ##################### Trackers

    track_last_updated_at(:hiring_relationship) do

        last_relationship_updated_at = [
            # FIXME: removed as part of https://trello.com/c/oyQYlWv6
            # current_user&.hiring_manager_relationships&.maximum(:updated_at),

            # users who are not yet accepted do not yet have hiring teams, but we don't
            # load up relationships for them anyway
            current_user&.hiring_team_id && current_user.relationships_from_team.maximum(:updated_at)
        ].compact.max
        last_relationship_updated_at ? last_relationship_updated_at.to_f : nil
    end

    track_last_updated_at(:open_position) do
        current_user&.hiring_team_open_positions&.maximum(:updated_at).to_f
    end

    track_last_updated_at(:candidate_position_interest) do
        current_user&.hiring_team_candidate_position_interests&.maximum(:updated_at).to_f
    end

    ##################### End trackers

    def add_last_updated_to_push_messages(key, before_or_current)
        entry = Api::UnloadedChangeDetector.last_updated_at_trackers.fetch(key)
        val = self.instance_eval(&entry[:proc])

        merge_message(key.to_s, val, before_or_current, 'unloaded_change_detector')
        val
    end

    def merge_message(prop, val, before_or_current, meta_block)
        if before_or_current == 'before'
            prop = "#{prop}_before_changes"
        end
        @meta['push_messages'] ||= {}
        @meta['push_messages'].deep_merge!({

                meta_block => {
                    prop => val
                }
            })
    end

end