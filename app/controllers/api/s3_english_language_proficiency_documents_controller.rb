class Api::S3EnglishLanguageProficiencyDocumentsController < Api::ApiCrudControllerBase

    load_and_authorize_resource
    include ControllerMixins::HandlesEnrollmentDocuments
    include ControllerMixins::SendsDocuments

    # POST /api/s3_english_language_proficiency_documents
    def create
        rescue_validation_error_or_render_record do
            user = User.find(permitted_post_params[:user_id] || current_user.id)
            document = S3EnglishLanguageProficiencyDocument.create!({
                :file => permitted_post_params[:file],
                :user_id => user.id
            })
            self.create_upload_timeline_event(document, 'english_proficiency', current_user)

            if current_ability.cannot?(:create, document)
                render_unauthorized_error
                raise ActiveRecord::Rollback.new

                # return nil so render_records will not try to render
                nil
            else
                # render the new record
                document
            end
        end
    end

    def destroy
        rescue_validation_error_or_render_record do
            document = S3EnglishLanguageProficiencyDocument.find(params[:id])
            self.create_deletion_timeline_event(document, 'english_proficiency', current_user)
            document.destroy
        end
    end

    # GET /api/s3_english_language_proficiency_documents/:id/file
    def show_file
        send_document(S3EnglishLanguageProficiencyDocument, 'private_user_documents:english_language_proficiency_document_accessed')
    end

    def permitted_post_params
        params.require(:record).permit('file', 'user_id')
    end
end