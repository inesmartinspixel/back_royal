class Api::ConfigController < Api::ApiCrudControllerBase
    skip_before_action :verify_signed_in
    skip_before_action :set_client_config
    skip_after_action :update_auth_header

    # GET /api/config
    def index
        client_version = client_params['fr-client-version'].to_i || 0
        gdpr_user = gdpr_country_request? ? 'true' : 'false'
        is_quantic = is_quantic? ? 'true' : 'false'
        config = AppConfig.all(client_version: client_version).merge(gdpr_user: gdpr_user, is_quantic: is_quantic)
        response = { 'config' => [config] }

        # see ConfigFactory in the client for how this is used
        @meta = {'server_timestamp' => Time.now.to_timestamp}
        render_successful_response(response)
    end

end
