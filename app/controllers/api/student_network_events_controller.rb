require 'back_royal/paginate_fastly'

class Api::StudentNetworkEventsController < Api::ApiCrudControllerBase

    include ControllerMixins::StudentNetworkEventsFiltersMixin

    before_action :authorize_index_with_params, :only => :index
    before_action :authorize_action, :only => [:create, :update, :destroy]

    # GET /api/student_network_events
    def index
        render_records(preload_events_for_index, self.index_api_options)
    end

    def preload_events_for_index

        filters = params[:filters] || {}

        query = apply_student_network_event_filters(filters)
        query = apply_order(query)
        query, total_count = query.paginate_fastly(limit: params[:limit], offset: params[:offset])
        @meta['total_count'] = total_count
        events = query.to_a

        if params['fields']&.include?('ADMIN_FIELDS')
            ActiveRecord::Associations::Preloader.new.preload(events, StudentNetworkEvent.includes_needed_for_json_eager_loading(self.index_api_options))
        end
        events
    end

    # POST /api/student_network_events
    def create
        rescue_validation_error_or_render_record(admin_options) do
            params['record']['author_id'] = current_user_id
            save_image_asset
            student_network_event = StudentNetworkEvent.create_from_hash!(params["record"])
            student_network_event
        end
    end

    # PUT /api/student_network_events
    def update
        rescue_validation_error_or_render_record(admin_options) do
            save_image_asset
            student_network_event = StudentNetworkEvent.update_from_hash!(params["record"])
            student_network_event
        end
    end

    # DELETE /api/student_network_events
    def destroy
        delete_record(StudentNetworkEvent, params['id'])
    end

    def authorize_index_with_params
        if current_ability.cannot?(:index_student_network_events, params)
            render_unauthorized_error
            false
        else
            true
        end
    end

    def authorize_action
        if current_ability.cannot?(:manage, StudentNetworkEvent)
            render_unauthorized_error
            false
        else
            true
        end
    end

    def admin_options
        full_access_option.merge({
            fields: ['ADMIN_FIELDS']
        })
    end

    def full_access_option
        {
            user_has_full_student_network_events_access: current_user.has_full_student_network_events_access?
        }
    end

    def index_api_options
        full_access_option.merge({
            fields: params[:fields],
            user_place_details: current_user.career_profile&.place_details,
            user_is_alumnus: current_user.student_network_inclusion&.alumnus?
        })
    end

    def apply_order(query)
        sort = params[:sort]&.first # can support multiple later if we need

        direction = params[:direction]

        allowed_keys = %w(title start_time)
        sort = nil unless allowed_keys.include?(sort)

        if sort.present?
            query = query.order(Arel.sql("#{sort} #{direction} NULLS LAST"))
        end

        query
    end

    def save_image_asset
        # the image file is sent up as part of the multipart form data
        # in it's own property. We need to create an s3_asset from it
        # and update params['record']
        if params['image']
            image = S3Asset.create!({
                :file => params['image'],
                :file_file_name => params['image'].original_filename,
                :directory => "images/"
            })
            params['record']['image_id'] = image.id
        end
    end

end
