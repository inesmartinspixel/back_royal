# Creates endpoint related to fetching and updating content items.  Assumes that
# the related models are using paper trail, and have create_from_hash!, update_from_hash!,
# and get_image_asset methods

module Api::ApiContentController

    def self.create_endpoints(controller, model)

        controller.cattr_accessor :model
        controller.model = model

        controller.class_eval do

            # POST /#{model.table_name}.json
            def create
                rescue_validation_error_or_render_record do

                    if params[:meta] && params[:meta].key?(:duplicate_from_id)
                        instance = model.duplicate!(current_user, params[:record], params[:meta])
                    else
                        # ignore any author that is passed in, and use the current user
                        instance = model.create_from_hash!(params[:record].merge({
                            author: current_user
                        }), params[:meta] || {})
                    end

                    add_save_warnings(instance)
                    instance
                end
            end

            # PUT /#{model.table_name}.json
            def update
                rescue_validation_error_or_render_record({
                    :fields => ['UPDATE_FIELDS']
                }) do
                    # author cannot be changed on update
                    instance = model.update_from_hash!(current_user, params[:record], params[:meta])
                    add_save_warnings(instance)
                    instance
                end

            rescue ContentPublisher::Publishable::UnauthorizedError => e
                render_error_response(e.message, :unauthorized)
            rescue CrudFromHashHelper::OldVersion => e
                render_error_response("Validation failed: a more recent version has been saved since you started editing, please refresh and try again.", :not_acceptable)
            rescue CrudFromHashHelper::NoDateStampProvided => e
                render_error_response("No updated_at provided", :not_acceptable)
            end

            def add_save_warnings(instance)
                # we only show warnings if the instance is valid.  Otherwise
                # we will be returning errors
                if instance &&
                    instance.valid? &&
                    instance.respond_to?(:save_warnings) &&
                    (warnings = instance.save_warnings) &&
                    warnings.any?

                    @meta['save_warnings'] = warnings
                end
            end

            # DELETE /#{model.table_name}/1.json
            def destroy
                delete_record(model, params[:id]) do |instance|
                    if instance.has_published_version?
                        render_error_response("Validation failed: Cannot delete a published item.  Unpublish first if you wish to delete.", :not_acceptable)
                        false
                    else
                        true
                    end
                end
            end

            # POST /#{model.table_name}/1/images.json
            # expects params to have an "images" property, which is an array whose elements can be either
            # - file uploads (in the case that the user used the file input in the interface)
            # - url strings (in the case that the user edited an image and we passed the url we got back from Aviary)
            def images
                render_error_if_not_found(model, params[:id]) do |record|
                    assets = params['images'].map do |image_upload_or_url|
                        asset = record.get_image_asset(image_upload_or_url)
                        asset.save!
                        asset
                    end
                    render_successful_response({'images' => assets})
                end
            end

            def render_unauthorized_error_if_trying_to_view_disallowed_unpublished_content

                filters = params[:filters] || {}

                if filters[:published].to_s == 'false' && current_ability.cannot?(:view_unpublished, self.model)
                    render_unauthorized_error
                    return true
                end

                if (filters[:version_id] && current_ability.cannot?(:view_specific_version, self.model))
                    return render_unauthorized_error
                    return true
                end

                return false
            end

        end

    end

    #class UnauthorizedError < RuntimeError; end

end
