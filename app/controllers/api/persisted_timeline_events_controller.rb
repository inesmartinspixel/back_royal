class Api::PersistedTimelineEventsController < Api::ApiCrudControllerBase

    authorize_resource class: ActivityTimeline::PersistedTimelineEvent

    def create
        rescue_validation_error_or_render_record do
            ActivityTimeline::PersistedTimelineEvent.create_from_hash!(
                params["record"].merge(
                    editor_id: current_user.id,
                    editor_name: current_user.name
                ),
                params["meta"])
        end
    end

    def destroy
        delete_record(ActivityTimeline::PersistedTimelineEvent, params['id'])
    end

end
