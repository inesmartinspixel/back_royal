class Api::LessonController < Api::ApiCrudControllerBase

	# I don't quite understand why, but load_and_authorize resource was causing cancan to override
	# our destroy methods, but only in Lesson::Stream.
	# using just authorize resource seems to work, though
	authorize_resource

	# gating of lesson-specific permissions overrides for editors
	skip_before_action :verify_signed_in, only: [:show]
	before_action :check_editor_can_create, only: [:create]
	before_action :check_editor_can_update, only: [:update]
	before_action :check_editor_can_destroy, only: [:destroy]
	before_action :check_can_view, only: [:show]

	def has_global_edit_permission?
		current_user.has_role?(:super_editor) || current_user.has_role?(:admin)
	end

	def check_editor_can_create
		editor_who_can_create = current_user.has_role?(:editor)

		render_unauthorized_error if !(has_global_edit_permission? || editor_who_can_create)
	end

	def check_editor_can_update
		instance = model.where({:id => params[:record][:id]}).first
		editor_who_can_update = user_is_author(instance) || (current_user.has_role?(:editor)  && (current_user.has_role?(:lesson_editor, instance) || current_user.has_role?(:reviewer, instance)))

		render_unauthorized_error if !(has_global_edit_permission? || editor_who_can_update)

        if (params[:meta] && params[:meta][:update_version])
            # note: permissions for update_old are not actually defined in ability.rb,
            # because we're fine falling back to the :manage permissions here.  Only admins
            # can do it
            render_unauthorized_error unless current_ability.can?(:update_old, instance)
        end
	end

	def check_editor_can_destroy
		instance = model.where({:id => params[:id]}).first
		editor_who_can_destroy = user_is_author(instance) || (current_user.has_role?(:editor)  && current_user.has_role?(:lesson_editor, instance))

		render_unauthorized_error if !(has_global_edit_permission? || editor_who_can_destroy)
	end

	def check_can_view

		instance = Lesson.where(id: params[:id]).select('id', 'author_id', 'unrestricted').first

		if instance.nil? || instance.allow_unrestricted_access?
			return
		end

		if !current_user
			render_unauthorized_error
			return
		end

		editor_who_can_view = user_is_author(instance) || (current_user.has_role?(:editor) && (current_user.has_role?(:lesson_editor, instance) || current_user.has_role?(:reviewer, instance) || current_user.has_role?(:previewer, instance)))

		if !(has_global_edit_permission? || editor_who_can_view)
			render_unauthorized_error
		end
	end

    # create/update/destroy/images
    Api::ApiContentController.create_endpoints(self, ::Lesson)

    # GET /#{model.table_name}.json
    def index
        filters = params[:filters] || {}
        fields = params[:fields] || [:id, :tag, :title,
                :modified_at, :published_at, :updated_at,
                :author, :stream_titles, :lesson_type,
                :archived]

        if current_user.has_role?(:editor)
            filters[:filter_editable_resources] = current_user.id
        end

        filters[:published] = false unless filters.key?(:published)

        json = Lesson::ToJsonFromApiParams.new({
            limit: params[:limit],
            fields: fields,
            filters: filters
        }).json
        render_contents_json_string(Lesson, json)
    end

    # GET /#{model.table_name}/1.json
    def show(instance = nil)
        return if render_unauthorized_error_if_trying_to_view_disallowed_unpublished_content
        return render_error_response(I18n.t(:no_id_provided), :not_acceptable) if params[:id].blank?

        # there may be no current_user, since standalone lessons support that
        extra_json_params = current_user ? {user_id: current_user.id} : {}
        json = Lesson::ToJsonFromApiParams.new(extra_json_params.merge(params.to_unsafe_h)).json
        render_not_found_or_contents_json_string(Lesson, params[:id], json)
    end

    def user_is_author(resource)
	   user_is_author = (!resource.nil? && !resource.author.nil?) ? (resource.author.id == current_user_id) : false
    end

end
