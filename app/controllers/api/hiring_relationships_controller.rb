class Api::HiringRelationshipsController < Api::ApiCrudControllerBase
    include Api::UnloadedChangeDetector

    attr_reader :hiring_relationship, :open_position_params, :candidate_position_interest, :conversation_params

    before_action :verify_allowed_index_params, :only => [:index]
    before_action :verify_update_params, :only => [:update]
    before_action :verify_open_position_params, :only => [:update, :create]
    before_action :verify_conversation_params, :only => [:update, :create]
    before_action :verify_candidate_position_interest_params, :only => [:update, :create]
    before_action :set_limit_when_not_has_full_access, :only => :index

    before_action(:only => [:create, :update]) do
        add_last_updated_to_push_messages(:hiring_relationship, "before")
        add_last_updated_to_push_messages(:candidate_position_interest, "before")
    end

    # GET /api/hiring_relationships
    def index
        filters = params[:filters] || {}
        query = HiringRelationship

        if filters.empty?
            query = query.where('TRUE')
        end

        if filters.key?(:candidate_id)
            query = query.where(candidate_id: filters[:candidate_id])
        end

        if filters.key?(:hiring_manager_id)
            query = query.where(hiring_manager_id: filters[:hiring_manager_id])
        end

        if filters.key?(:candidate_status_not)
            query = query.where.not(candidate_status: filters[:candidate_status_not])
        end

        if filters.key?(:hiring_manager_status_not)
            query = query.where.not(hiring_manager_status: filters[:hiring_manager_status_not])
        end

        if filters.key?(:candidate_status)
            query = query.where(candidate_status: filters[:candidate_status])
        end

        if filters.key?(:hiring_manager_status)
            query = query.where(hiring_manager_status: filters[:hiring_manager_status])
        end

        if filters.key?(:updated_since)
            query = query.where("hiring_relationships.updated_at > ?", Time.at(filters[:updated_since]))
        end

        if filters.key?(:hiring_manager_not)
            query = query.where.not(hiring_manager_id: filters[:hiring_manager_not])
        end

        if filters.key?(:hiring_team_id)
            query = join_hiring_managers(query).where(hiring_managers: { hiring_team_id: filters[:hiring_team_id] })
        end

        if filters.key?(:open_position_id)
            query = query.where(open_position_id: filters[:open_position_id])
        end

        if filters[:closed]
            query = query.closed
        elsif filters[:closed] == false
            query = query.not_closed
        end

        if filters[:rejected_by_hiring_manager_or_closed]
            query = query.rejected_by_hiring_manager_or_closed
        end

        if filters[:rejected_by_candidate_or_closed]
            query = query.rejected_by_candidate_or_closed
        end

        if filters.key?(:viewable_by_hiring_manager)
            # Hiring managers can only view inactive profiles from candidates they have
            # liked or saved.
            # See hiring_relationship_relation_extensions.rb
            query = query.viewable_by_hiring_manager
        end

        if params.key?(:limit)
            query = query.limit(params[:limit])
        end

        hiring_relationships = preload_hiring_relationships_for_index(query)
        add_has_liked_candidate(hiring_relationships)


        # this is needed so that the careers network view model
        # knows what the initial updated_at is
        add_last_updated_to_push_messages(:hiring_relationship, "current")

        if params[:hiring_applications_in_meta]
            @meta['hiring_applications'] = hiring_relationships.map(&:hiring_application).uniq.map(&:as_json)
        end

        results = hiring_relationships.map do |hiring_relationship|
            # Hiring managers who are not all paid up only see anonymous profiles on the featured page.  If someone
            # had full access in the past then they will have profiles that they have acted on in the past.  Continue
            # to let them view non-anonymized versions of those profiles.
            anonymize = (
                            hiring_relationship.hiring_manager_status == 'pending' &&
                            current_ability.cannot?(:view_full_hiring_manager_experience, nil)
                        )

            hiring_relationship.as_json(index_as_json_options.merge({
                career_profile_options: {
                    anonymize: anonymize
                }
            }))
        end

        render_record_hashes(results, 'hiring_relationships')
    end

    # this is silly at this point, since we only have one place
    # where we need to join against hiring managers, but if there
    # comes to be another place, then it will make sense, to make
    # sure we don't unecessarily join twice
    def join_hiring_managers(query)
        if !@joined_hiring_managers
            query = query.joins("join users hiring_managers on hiring_managers.id = hiring_manager_id")
        end
        query
    end

    #separate method for specs
    def index_as_json_options
        {
            is_admin: current_ability.can?(:manage, HiringRelationship),
            fields: params[:fields],
            except: params[:except]
        }
    end

    def create
        # We allow hiring managers to create relationships for themselves from the hiring marketing
        # pages, so we have to guard against users other than hiring managers and admins since this
        # endpoint is now publicly accessible.
        unless current_ability.can?(:create_hiring_relationships, create_params)
            render_unauthorized_error
            return false
        end

        begin
            # ensure this always happens prior to any potential SAVEPOINT rollbacks
            save_candidate_position_interest_params

            wrap_save do

                save_open_position

                @hiring_relationship = HiringRelationship.find_by(hiring_manager_id: create_params[:hiring_manager_id], candidate_id: create_params[:candidate_id])
                if @hiring_relationship

                    # If a relationship already exists, use hiring_manager_status if passed in with create_params
                    if create_params.key?(:hiring_manager_status)
                        @hiring_relationship.hiring_manager_status = create_params[:hiring_manager_status]
                    end

                    # hiring managers can "Invite to Connect" with a candidate that they already have a pending
                    # relationship with, so we need to ensure that the open_position_id also gets set, if present
                    if create_params.key?(:open_position_id)
                        @hiring_relationship.open_position_id = create_params[:open_position_id]
                    end
                else

                    @hiring_relationship = HiringRelationship.new(create_params)
                    # This save is 100% needed for the save_conversation call below to successfully link
                    # the hiring relationship and the conversation.
                    #
                    # do not use create! because we have to set the instance variable first in case of an error on save
                    @hiring_relationship.save!

                    # store an id from the career_profile_searches table that was sent up by the client
                    if params[:meta] && params[:meta][:found_with_search_id]
                        @hiring_relationship.selection_details = {
                            found_with_search_id: params[:meta][:found_with_search_id]
                        }
                    end
                end

                # need to save again to get the conversation_id on there
                save_conversation

                @hiring_relationship.save!
                @hiring_relationship.notify_participants_if_just_accepted
                @hiring_relationship
            end
        rescue CandidatePositionInterest::OldVersion
            instance = CandidatePositionInterest.find(@candidate_position_interest_params[:id])
            @meta['error_type'] = 'interest_conflict'
            @meta['candidate_position_interest'] = instance.as_json(career_profile_options: {anonymize: false})
            render_error_response("Interest has been saved more recently", :conflict)
        end
    end

    def add_meta_for_multiple_team_connections_error(my_relationship, teammate_relationship)
        @meta['error_type'] = "multiple_team_connections_to_candidate"

        # Since we hit this error, that means that there are changes in the
        # db that the client does not know about.  Since the save is coming
        # up from an instance of HiringRelationship.js, it is kind of clean
        # in the client to treat THIS relationship as different from other ones.
        # If I was starting from scratch I might not do it this way.  I might just
        # include both in @meta['hiring_relationships']
        @meta['hiring_relationship'] = my_relationship.as_json(except: ['career_profile', 'hiring_application'])

        # Note: the above hiring_relationship meta gets merged into the existing hiring relationship in the client
        # (e.g.: see hiring_relationship_view_model.js#_handleSaveError). This field, hiring_relationships, gets
        # used to rebuild new hiring relationships from scratch, and therefore must include all fields, including
        # the career_profile (see hiring_relationship_view_model.js#_handlePushedUpdates).
        @meta['hiring_relationships'] = [teammate_relationship.as_json({
            career_profile_options: {
                anonymize: false
            }
        })]
        @meta['teammate_name'] = teammate_relationship.hiring_manager.name
    end

    def update
        begin
            # ensure this always happens prior to any potential SAVEPOINT rollbacks
            save_candidate_position_interest_params

            wrap_save do
                save_open_position
                @hiring_relationship.assign_attributes(@update_params)
                save_conversation
                @hiring_relationship.updated_at = Time.now

                # we can't log this until the status has been updated
                # and the conversation has been created.  We can't put this
                # into an active record life-cycle hook because the hiring relationship
                # gets auto-saved inside of save_conversation
                @hiring_relationship.save!
                @hiring_relationship.notify_participants_if_just_accepted
                @hiring_relationship
            end
        rescue CandidatePositionInterest::OldVersion
            instance = CandidatePositionInterest.find(@candidate_position_interest_params[:id])
            @meta['error_type'] = 'interest_conflict'
            @meta['candidate_position_interest'] = instance.as_json(career_profile_options: {anonymize: false})
            render_error_response("Interest has been saved more recently", :conflict)
        end
    end

    def save_conversation
        if conversation_params
            conversation = Mailboxer::Conversation.create_or_update_from_hash!(conversation_params, current_user, hiring_relationship)
            hiring_relationship.conversation = conversation

            # if a new message is being created along with the update to the hiring relationship,
            # we want to include it in the hiring_relationship:accepted
            hiring_relationship.new_message = conversation.new_message
        end
    end

    # This is used on the hiring manager details page in the Manage Careers admin section
    # to batch create hiring relationships between a hiring manager and candidates whose
    # career profiles are in a career profile list.
    def batch_create
        if !current_ability.can?(:batch_create, HiringRelationship)
            render_unauthorized_error
            return false
        end

        hiring_relationships = []
        career_profile_ids = batch_create_params[:career_profile_ids]
        hiring_manager_id = batch_create_params[:hiring_manager_id]
        career_profiles = CareerProfile.select(:user_id).where(id: career_profile_ids)
        career_profiles.each do |career_profile|
            begin
                hiring_relationship = HiringRelationship.create!(hiring_manager_id: hiring_manager_id, candidate_id: career_profile.user_id)
                hiring_relationships << hiring_relationship
            rescue ActiveRecord::RecordNotUnique
                # noop
            end
        end
        render_records(hiring_relationships)
    end


    # separate method for preload testing
    def preload_hiring_relationships_for_index(query)
        includes = HiringRelationship.includes_needed_for_json_eager_loading

        # if we don't need one of the profiles, do not preload it.
        # However, in that case we need to add in the relevant user
        # otherwise the users will not be preloaded.
        # (This actually may not be that important in practice, since you generally
        # skip the hiring_application because you are a hiring manager and so all the
        # relationships will have the same one anyway.  It would be important, though,
        # if we have another good reason to load up relationships without profiles)
        {
            "career_profile" => "candidate",
            "hiring_application" => "hiring_manager"
        }.each do |profile_key, user_key|

            # if the user has request to have the hiring applications included in the
            # meta, then we should eager load them regardless of the values in fields or except
            next if params['hiring_applications_in_meta'] && profile_key == 'hiring_application'

            if params["except"]&.include?(profile_key) || params["fields"]&.exclude?(profile_key)
                includes.delete(profile_key.to_sym)
                includes[user_key.to_sym] = []
            end
        end

        if params["except"]&.include?('conversation') || params["fields"]&.exclude?('conversation')
            includes.delete('conversation'.to_sym)
        end

        hiring_relationships = query.to_a

        # In order to simplify the filtering, we always do the preloading AFTER
        # the initial group of hiring relationships have been loaded.  This prevents
        # us from having to do crazy things like guessing what active record
        # is going to name join tables.
        ActiveRecord::Associations::Preloader.new.preload(hiring_relationships, includes)
        hiring_relationships
    end

    def verify_allowed_index_params
        unless current_ability.can?(:index_hiring_relationships, params)
            render_unauthorized_error
            return false
        end
    end

    def verify_update_params
        id = params['record'] && params['record']['id']
        @hiring_relationship = HiringRelationship.find(id)
        raise ActiveRecord::RecordNotFound if @hiring_relationship.nil?

        if current_ability.cannot?(:update, @hiring_relationship)
            render_unauthorized_error
            return false
        end

        if @hiring_relationship.candidate_id == current_user.id
            @update_params = {
                'candidate_status' => params['record']['candidate_status'],
                'candidate_closed' => params['record']['candidate_closed']
            }
        elsif @hiring_relationship.hiring_manager_id == current_user.id
            @update_params = {
                'hiring_manager_status' => params['record']['hiring_manager_status'],
                'open_position_id' => params['record']['open_position_id'],
                'hiring_manager_closed' => params['record']['hiring_manager_closed'],
                'hiring_manager_closed_info' => params['record']['hiring_manager_closed_info'] || {} # support legacy clients with the logical or
            }
        elsif @hiring_relationship.hiring_manager.hiring_team_id && @hiring_relationship.hiring_manager.hiring_team_id == current_user.hiring_team_id
            # teammates cannot update anything, but can send messages
            @update_params = {}
        elsif current_user.has_role?(:admin)
            @update_params = {
                'hiring_manager_priority': params['record']['hiring_manager_priority'],
                'hiring_manager_closed': params['record']['hiring_manager_closed'],
                'candidate_closed': params['record']['candidate_closed']
            }
        else
            raise RuntimeError.new("How did we get here?")
        end

        return true
    end

    def save_open_position
        if @open_position_params
            # set the timestamp on the server since we don't want
            # to trust the client's clock
            @open_position_params[:last_invitation_at] = Time.now.to_timestamp
            add_last_updated_to_push_messages(:open_position, "before")
            open_position = OpenPosition.create_or_update_from_hash!(@open_position_params)
            add_last_updated_to_push_messages(:open_position, "current")
            meta['open_position'] = open_position.as_json
        end
    end

    def save_candidate_position_interest_params
        if @candidate_position_interest_params

            candidate_position_interest = in_transaction_with_editor_tracking do
                CandidatePositionInterest.update_from_hash!(@candidate_position_interest_params, nil, nil, current_user)
            end

            # if you have access to save the candidate_position_interest, then we
            # assume you have access to see the restricted fields on it
            meta['candidate_position_interest'] = candidate_position_interest.as_json(career_profile_options: {anonymize: false})
        end
    end

    def wrap_save(&block)

        # we assume that if you can update a hiring relationship that you
        # are allowed to see the full profile on it
        json_options = {
            career_profile_options: {
                anonymize: false
            }
        }

        # we turn off the transaction that rescue_validation_error_or_render_record
        # normally creates, because we need to create our own transaction so we can
        # edit the meta after the after_commit has written hiring_relationship#side_effects
        rescue_validation_error_or_render_record(json_options, false) do
            in_transaction_with_editor_tracking do
                begin

                    HiringRelationship.connection.execute("SAVEPOINT before_hiring_relationship_save")
                    yield

                    # we need to set this after the save so it can be bumped in the client
                    add_last_updated_to_push_messages(:hiring_relationship, "current")

                rescue ActiveRecord::StatementInvalid => e
                    # See triggers in 20170915171428_create_hiring_teams.rb
                    if e.message.include?('Multiple connections to candidate')
                        HiringRelationship.connection.execute("ROLLBACK TO SAVEPOINT before_hiring_relationship_save")
                        add_meta_for_multiple_team_connections_error(
                            @hiring_relationship.new_record? ? @hiring_relationship : @hiring_relationship.reload,
                            current_user.get_teammate_relationship(@hiring_relationship.candidate_id)
                        )
                        @hiring_relationship.errors.add(:candidate_id, "has already been accepted by your team")
                        raise ActiveRecord::RecordInvalid.new(@hiring_relationship)
                    else
                        raise e
                    end
                end
            end

            # there can be side effects from a change, like hiding teammates' relationships or
            # hiding teamates' candidate position interests
            # Push those down to the client (set these in the meta after the transaction
            # is committed to make sure after_commits have run)
            if @hiring_relationship.side_effects.key?(:hiring_relationships)
                @meta['hiring_relationships'] = @hiring_relationship.side_effects[:hiring_relationships].values.as_json(json_options)
            end

            if @hiring_relationship.side_effects.key?(:candidate_position_interests)
                @meta['candidate_position_interests'] = @hiring_relationship.side_effects[:candidate_position_interests].values.as_json(json_options)
            end

            add_last_updated_to_push_messages(:candidate_position_interest, "current")

            @hiring_relationship
        end
    end

    def verify_open_position_params
        @open_position_params = params['meta'] && params['meta']['open_position']
        if @open_position_params.nil?
            return true
        end

        if current_ability.cannot?(:create_or_update_open_positions, params)
            render_unauthorized_error
            return false
        end

        # we always expect that an open position passed up is attached to the relationship.  Otherwise
        # something odd is going on
        if @open_position_params['id'] != params['record']['open_position_id']
            render_error_response("open_position in meta does not match id in relationship", :not_acceptable)
            return false
        end

        return true
    end

    def verify_candidate_position_interest_params
        @candidate_position_interest_params = params['meta'] && params['meta']['candidate_position_interest']
        if @candidate_position_interest_params.nil?
            return true
        end

        @candidate_position_interest = CandidatePositionInterest.find(params['meta']['candidate_position_interest']['id'])

        # hiring managers can update their's or teammates' open position interests, (and admins can do what they want),
        # but for any other situation we error
        if current_ability.cannot?(:update, @candidate_position_interest)
            render_unauthorized_error
            return false
        end

        # only allow hiring managers to update status in this case
        if current_user.cannot?(:manage, CandidatePositionInterest)
            @candidate_position_interest_params.slice!(:id, :updated_at, :hiring_manager_status)
        end

        return true
    end

    def create_params
        params.require(:record).permit(:candidate_id, :hiring_manager_id, :candidate_status, :hiring_manager_status, :hiring_manager_priority, :open_position_id)
    end

    def batch_create_params
        params.permit(:career_profile_list_id, :hiring_manager_id, :career_profile_ids => [])
    end

    def verify_conversation_params
        unless defined? @conversation_params
            # old clients will send up both, but they will just be ignoring params['record']['conversation']
            # They're just sending back up what was sent down to them.  Any updates will only be in the
            # meta
            @conversation_params = params['record']&.dig('conversation')
        end

        if current_user.cannot?(:create_or_update_conversation, @conversation_params || {})
            return false
        end

        return true
    end

    def add_has_liked_candidate(hiring_relationships_to_return)
        # first check the list we've already loaded.  If we find an accepted one in there, then we don't
        # need to do another query.  If we don't find it, though, we should do another query just in case
        # it was filtered out (maybe not possible right now, but could be if we add a limit or something in the future)
        has_liked_candidate = hiring_relationships_to_return.any?{ |hiring_relationship|
            hiring_relationship['hiring_manager_id'] == current_user_id &&
            hiring_relationship.hiring_manager_status == "accepted"
        }

        if !has_liked_candidate
            has_liked_candidate = HiringRelationship.where(hiring_manager_id: current_user_id, hiring_manager_status: 'accepted').exists?
        end

        @meta['has_liked_candidate'] = has_liked_candidate
    end

    def set_limit_when_not_has_full_access
        filters = params[:filters] || {}
        if current_ability.cannot?(:view_full_hiring_manager_experience, nil) && filters[:hiring_manager_status] == 'pending'
            params[:limit] = 3
        end
    end

end