class Api::UserTimelinesController < Api::ApiCrudControllerBase

    authorize_resource class: ActivityTimeline::UserTimeline

    def show
        instance = ActivityTimeline::UserTimeline.new(params['id']).preload
        render_records([instance])
    end

    def collection_key(record)
        'user_timelines'
    end
end