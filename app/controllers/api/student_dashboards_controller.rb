class Api::StudentDashboardsController < Api::ApiCrudControllerBase
    authorize_resource :class => false
    before_action :check_params, only:[:index]

    # make sure the initial value of progress_max_updated_at gets set in the client
    before_action :set_progress_max_updated_at

    # We're going to use index instead of show so that we don't have to
    # pass up an id, even though we always only return one.

    # GET /#{model.table_name}.json
    instrument_method
    def index
        filters = params[:filters]

        if params[:get_has_available_incomplete_streams]
            get_has_available_incomplete_streams
        end
        to_json_params = {
            user: current_user,
            filters: filters
        }
        set_user_push_messages
        add_num_recommended_positions_to_push_messages
        json = StudentDashboard::ToJsonFromApiParams.new(to_json_params).json
        render_contents_json_string("student_dashboards", json)
    end

    def check_params
        params[:filters] ||= {}

        if params[:filters][:view_as]
            params[:filters].merge!({
                in_locale_or_en: params[:in_locale_or_en] || current_user.locale # This could support a front-end locale view_as
            })
        else
            # Set defaults if not trying to view_as
            params[:filters].merge!({
                user_can_see: true,
                in_users_locale_or_en: true
            })
        end

        # Do the ability check
        if current_ability.cannot?(:read_student_dashboards, params)
            render_unauthorized_error
            return false
        end
    end

    def get_has_available_incomplete_streams
        available_streams_json = Lesson::Stream::ToJsonFromApiParams.new(params.merge({
            user: current_user, # there must be a current user
            filters: {
                published: true,
                user_can_see: true,
                in_users_locale_or_en: true,
                complete:  false
            },
            fields: [:id],
            limit: 1
        })).json
        result = ActiveSupport::JSON.decode(available_streams_json)
        self.meta['has_available_incomplete_streams'] = result.any?
    end
end
