class Api::CohortsController < Api::ApiCrudControllerBase

    authorize_resource

    before_action :verify_allowed_index_params, :only => [:index]

    # POST /api/cohorts
    def create
        existing_cohort = Cohort.where("name = ?", params[:record]['name'])

        if existing_cohort.size > 0
            return render_error_response('Validation failed: Cohort with this English name already exists.', :not_acceptable)
        end

        rescue_validation_error_or_render_record(fields: ['ADMIN_FIELDS']) do
            @cohort = Cohort.create_from_hash!(current_user, params[:record], params[:meta])
            @cohort
        end
    end

    # GET /api/cohorts
    # FIXME: https://trello.com/c/2JFtApod
    def index
        filters = params[:filters] || {}

        if params['get_cohort_promotions']
            @meta['cohort_promotions'] = CohortPromotion.includes(:cohort).all.map(&:as_json)
        end

        if params['get_stripe_metadata']
            set_stripe_admin_meta
        end

        if params['get_learner_projects']
            set_learner_projects_meta
        end

        query = nil
        if filters[:published] && filters[:promoted]
            promoted_and_published = true
        elsif filters.key?(:published) || filters.key?(:promoted)
            # There is no real reason we couldn't support this, but it's extra work and no one
            # needs it right now
            raise NotImplementedError.new("Unsupported filters")
        else
            promoted_and_published = false
        end

        json_opts = {}
        if params[:fields]
            json_opts[:fields] = params[:fields]
        end

        # NOTE: In the first case we are returning published versions
        # from the cohorts_versions table. In the second we are using
        # the cohorts table
        # FIXME: We could potentially move the caching we are doing in
        # the else up to here, but let's first use it for awhile in the
        # admin-facing API.
        if promoted_and_published
            query = Cohort.all_published.promoted
            result = query.map(&:published_version).to_json(json_opts)
        else
            result = get_cached_results(json_opts)
        end
        render_contents_json_string(Cohort, result)
    end

    private def get_cached_results(json_opts = {})
        key = "cohorts index - #{Api::CohortsController.max_updated_at_for_cache_key} - #{json_opts}"
        result = SafeCache.fetch(key) do
            preload_working_cohorts_for_index.to_a.to_json(json_opts)
        end
    end

    private def preload_working_cohorts_for_index
        query = Cohort.includes(:content_publishers, :published_versions, :slack_rooms, :slack_room_assignment, :access_groups => :lesson_stream_locale_packs)
    end

    def self.max_updated_at_for_cache_key
        # Notes:
        #   We only preload lesson_stream_locale_packs for the locale_pack ids;
        #   the client actually loads the content in separate requests.
        #   Also, you can't update access_groups joins without saving the cohort,
        #   which updates the updated_at on cohorts.
        klasses = [
            Cohort,
            AccessGroup,
            CohortSlackRoom,
            Cohort::SlackRoomAssignment
        ]
        queries = klasses.map { |klass| "(SELECT max(updated_at) FROM #{klass.table_name})" }
        ActiveRecord::Base.connection.execute("SELECT GREATEST(#{queries.join(', ')}) AS max_updated_at")[0]['max_updated_at']
    end

    # need to override this since we sometimes return cohort versions and
    # sometimes cohorts
    def collection_key(record)
        if record.is_a?(Cohort) || record.is_a?(Cohort::Version)
            "cohorts"
        else
            raise ArgumentError.new("unexpected record of type #{record.class.name.inspect}")
        end
    end


    # PATCH/PUT /api/cohorts
    def update
        rescue_validation_error_or_render_record(:fields => ['UPDATE_FIELDS']) do
            # author cannot be changed on update
            instance = Cohort.update_from_hash!(current_user, params[:record], params[:meta])
            instance
        end

        rescue ContentPublisher::Publishable::UnauthorizedError => e
            render_error_response(e.message, :unauthorized)
        rescue CrudFromHashHelper::OldVersion => e
            render_error_response("Validation failed: a more recent version has been saved since you started editing, please refresh and try again.", :not_acceptable)
        rescue CrudFromHashHelper::NoDateStampProvided => e
            render_error_response("No updated_at provided", :not_acceptable)
    end

    # DELETE /api/cohorts/1
    def destroy
        delete_record(Cohort, params['id'])
    end

    # POST /api/cohorts/upload_project_document
    def upload_project_document
        new_document = S3ProjectDocument.new({
            :file => params[:file]
        })
        existing_document = S3ProjectDocument.find_by_file_fingerprint(new_document.file_fingerprint)
        project_document = existing_document || new_document

        begin
            project_document.save!
        rescue ActiveRecord::RecordInvalid => e
            return render_error_response(e.message, :not_acceptable)
        end

        render_successful_response({'project_document' => project_document})
    end

    # POST /api/cohorts/upload_exercise_document
    def upload_exercise_document
        new_document = S3ExerciseDocument.new({
            :file => params[:file]
        })
        existing_document = S3ExerciseDocument.find_by_file_fingerprint(new_document.file_fingerprint)
        exercise_document = existing_document || new_document

        begin
            exercise_document.save!
        rescue ActiveRecord::RecordInvalid => e
            return render_error_response(e.message, :not_acceptable)
        end

        render_successful_response({'exercise_document' => exercise_document})
    end

    private def verify_allowed_index_params
        unless current_ability.can?(:index_cohorts, params)
            render_unauthorized_error
            return false
        end
    end

end
