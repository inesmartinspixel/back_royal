class Api::StudentNetworkMessagesController < Api::ApiCrudControllerBase
    attr_accessor :message_recipient

    skip_load_and_authorize_resource :only => :create
    before_action :authorize_create_with_params, :only => :create

    # POST /api/student_network_messages
    def create
        rescue_validation_error_or_render_record do

            recipient_email = @message_recipient.student_network_email || @message_recipient.email

            sanitizer = ActionView::Base.full_sanitizer
            sanitized_params = create_params.merge({
                to: recipient_email,
                sender_id: current_user.id,
                reply_to: sanitizer.sanitize(create_params[:reply_to]),
                subject: sanitizer.sanitize(create_params[:subject]),
                message_body: sanitizer.sanitize(create_params[:message_body])
            })

            # NOTE: we log both a `message_sent` and `message_received` below.
            # The `message_sent` will have a user_id corresponding to the sender,
            # while the `message_received` will have a user_id corresponding to
            # the recipient.
            # We do this because it's convenient to look for events by user_id; it
            # makes it easier for us to answer questions like "how many messages
            # has X sent?" and "how many messages has Y received?".

            Event.create_server_event!(
                SecureRandom.uuid,
                current_user.id,
                'student_network:message_sent',
                {
                    recipient_id: @message_recipient.id
                }
            )

            record = Event.create_server_event!(
                SecureRandom.uuid,
                @message_recipient.id,
                'student_network:message_received',
                sanitized_params
            )
            record.log_to_external_systems
            record
        end
    end

    def create_params
        params.require(:record).permit(:recipient_id, :reply_to, :subject, :message_body)
    end

    def authorize_create_with_params
        @message_recipient = User.find_by_id(create_params[:recipient_id])
        if current_ability.cannot?(:create_student_network_messages, create_params, @message_recipient)
            render_unauthorized_error
            false
        else
            true
        end
    end
end
