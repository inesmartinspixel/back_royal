class Api::HiringApplicationsController < Api::ApiCrudControllerBase

    # play ncie with CanCan and move permission stuff into ability.rb
    load_and_authorize_resource

    # Note: this method is used by the internal Zapier CLI app; if we open this up to non-admins, we'd be opening up that as well.
    # Tests in the Zapier Internal app will also fail in that case.
    def index
        render_records(preload_hiring_applications_for_index)
    end

    def show
        rescue_validation_error_or_render_record do
            hiring_application = HiringApplication.find_by_id(params["id"])
            hiring_application
        end
    end

    def update_hiring_team_if_specified(user)
        meta = params[:meta] || {}

        if current_ability.can?(:manage, HiringTeam)
            if meta.key?(:hiring_team_title) && meta[:hiring_team_title]
                user.hiring_team = HiringTeam.new(title: meta[:hiring_team_title])
            elsif meta.key?(:hiring_team_id)
                user.hiring_team = meta[:hiring_team_id] ? HiringTeam.find(meta[:hiring_team_id]) : nil
            end
            user.hiring_team.owner ||= user unless user.hiring_team.nil?
        end
    end

    def update
        rescue_validation_error_or_render_record do

            # Update the appropriate fields on the user if they are changed. This convienantly
            # lets us deal with only the one profile object on the client.
            user = User.find(HiringApplication.where(id: hiring_application_params[:id]).limit(1).pluck(:user_id).first)

            # Admin only: Handle HiringTeam associations, prior to application update if they
            # are accepted in order to avoid potential validation errors due to flux state where
            # acceptance requires first having hiring team set.
            if hiring_application_params[:status] == 'accepted'
                update_hiring_team_if_specified(user)
                user.save!
            end

            # Update the application, itself
            hiring_application = HiringApplication.update_from_hash!(hiring_application_params)

            # Handle user-specific properties that can be set by the user
            changed = false
            ['name', 'nickname', 'phone', 'avatar_url', 'job_title'].each do |key|
                if !hiring_application_params[key].nil? and user[key] != hiring_application_params[key]
                    user.send(:"#{key}=", hiring_application_params[key])
                    changed = true
                end
            end

            # Admin only: Handle HiringTeam associations, after application update if they are
            # not accepted in order to avoid potential validation errors due to flux state where
            # possibly setting hiring_team to nil requires first having become unaccepted. We
            # currently do not have a way to do this in the UI, but we may in the future.
            if hiring_application_params[:status] != 'accepted'
                update_hiring_team_if_specified(user)
                changed ||= user.will_save_change_to_hiring_team_id? # changing hiring_team_id
                changed ||= user.hiring_team_id.nil?  && user.hiring_team # creating a new team
            end

            # If anything registered a viable change to the user, handle accordingly
            if changed
                user.save!
                current_user.reload
                hiring_application.reload
                add_hiring_application_user_fields_to_push_messages()
            end

            # return the application for the response
            hiring_application
        end
    end

    def preload_hiring_applications_for_index
        query = HiringApplication.includes(*HiringApplication.includes_needed_for_json_eager_loading)
        if !sort_column.blank?
            query = query.reorder("#{sort_column} #{sort_direction}")
        end
        if !limit.blank?
            query = query.limit(limit)
        end
        query = query.all.to_a
    end

    def limit
        limit = params[:limit]
        !limit.blank? ? [limit.to_i, 0].max : nil
    end

    def sort_column
        column = params[:sort] ? params[:sort].first : nil
        allowed_columns = HiringApplication.column_names
        allowed_columns.include?(column) ? column : nil
    end

    def sort_direction
        %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
    end

    private

        # Only allow a trusted parameter "white list" through. Note: relying on `to_h` instead causes oddities
        def hiring_application_params
            return @hiring_application_params if defined? @hiring_application_params

            p = params.require(:record).permit(
                :id, :created_at, :updated_at, :user_id, :last_calculated_complete_percentage, :has_seen_welcome,

                # User fields
                :name, :nickname, :phone, :avatar_url, :job_title,

                # Company
                :logo_url, :place_id, :place_details, :company_year, :company_employee_count, :company_annual_revenue, :company_sells_recruiting_services, :website_url, :industry, :funding,

                # Team
                :hiring_for, :team_name, :team_mission, { :role_descriptors => [] },

                # You
                :job_role, :fun_fact,

                # Candidates
                { :where_descriptors => [] }, { :position_descriptors => [] }, { :salary_ranges => [] }
            )

            # We don't own the place_details object (Google does) so we would rather not have to whitelist
            # every field.
            # See https://github.com/rails/rails/pull/14317
            if params[:record].key?(:place_details)

                # nil is actually not valid, but let the model error in that case.  The client should not
                # ever send up nil
                p[:place_details] = params[:record][:place_details].nil? ? nil : params[:record][:place_details].to_unsafe_h
            end

            if current_ability.can?(:manage, @hiring_application)
                p[:status] = params[:record][:status]
            end

            @hiring_application_params = p
        end

        def add_hiring_application_user_fields_to_push_messages
            extra = {
                'avatar_url' => current_user.avatar_url,
                'name' => current_user.name,
                'phone' => current_user.phone,
                'job_title' => current_user.job_title
            }
            set_user_push_messages(extra: extra)
        end
end