class Api::CareerProfileListsController < Api::ApiCrudControllerBase

    # play nice with CanCan and move permission stuff into ability.rb
    load_and_authorize_resource
    skip_before_action :verify_signed_in, only: [:index, :show]

    # GET /api/career_profile_lists
    def index
        filters = params[:filters] || {}
        fields = params[:fields] || ['id', 'name', 'career_profile_ids', 'career_profiles']
        career_profiles_filters = {}

        query = CareerProfileList.all

        if filters.key?(:name)
            query = CareerProfileList.where(:name => filters[:name])
        end

        if filters.key?(:career_profiles_limit)
            career_profiles_filters[:limit] = filters[:career_profiles_limit].to_i
        end

        render_records(query.to_a, {fields: fields, filters: career_profiles_filters})
    end

    # POST /api/career_profile_lists
    def create
        rescue_validation_error_or_render_record do
            CareerProfileList.create!(permitted_record_params)
        end
    end

    # PUT /api/career_profile_lists
    def update
        rescue_validation_error_or_render_record do
            @career_profile_list.update!(permitted_record_params)
            @career_profile_list
        end
    end

    # DELETE /api/career_profile_lists
    def destroy
        delete_record(CareerProfileList, params['id'])
    end

    private

        def permitted_record_params
            return @permitted_record_params if defined? @permitted_record_params
            @permitted_record_params = params.require(:record).permit(:id, :name, :career_profile_ids => [], :role_descriptors => [])
        end

end