class CohortScheduleController < ApplicationController

    # eventually we will just get rid of this controller and treat the
    # schedule as a regulat front-royal page
    def index

        begin
            cohort = Cohort.find(params[:cohort_id])
        rescue ActiveRecord::RecordNotFound => err
            return four_o_four
        end

        return four_o_four unless published_cohort = cohort.published_version
        return four_o_four if published_cohort.external_schedule_url.nil?

        redirect_to published_cohort.external_schedule_url

    end

end
