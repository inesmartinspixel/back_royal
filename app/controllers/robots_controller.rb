class RobotsController < ApplicationController

    def robots

        # I could not get this to work using a template for 
        # some reason.  Something about the .txt format
        render plain: %Q~
User-agent: *
Sitemap: #{SitemapHelper.sitemap_url(current_domain)}
        ~
    end
end
