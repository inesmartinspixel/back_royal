class ApplicationController < ActionController::Base

    ERROR_ACCOUNT_DEACTIVATED_CODE = 'error_account_deactivated'
    class DeactivatedUserError < RuntimeError; end

    ERROR_HIRING_WEB_ONLY_CODE = 'error_hiring_web_only'
    class HiringWebOnlyError < RuntimeError; end

    # default is :null_session; :reset_session is more aggressive and has the effect of logging you out after the
    # first json api call attempt that does not include 'X-XSRF-TOKEN' in the request header
    protect_from_forgery :with => :reset_session

    before_action :set_headers

    # SSL Redirect + HTTP Basic Auth (Staging) + Custom HTML class
    before_action :handle_staging
    before_action :handle_local

    # add app-specific params to devise's whitelist
    before_action :configure_permitted_parameters, if: :devise_controller?

    # wrap requests with a proper sentry user context
    before_action :set_raven_context
    after_action :unset_raven_context

    # configure location-specific flags
    before_action :set_location_specific_flags

    # Trying to silence MissingTemplate and RoutingErrors by providing fingerprints
    rescue_from ActionView::MissingTemplate, ActionController::RoutingError, ActionController::UnknownFormat do |exception|
        Raven.capture_exception(exception, { :fingerprint => [request.original_fullpath, request.method] })
        four_o_four
    end

    # localization
    before_action :set_locale

    # Devise does not support out of the box tracking for :registerable and :rememberable. This
    # means that sign_in_count, last_sign_in_*, current_sign_in_* do not get set/updated when 1) a
    # User registers an account, or 2) a User who's logged in returns after an arbitrary
    # amount of time
    before_action :update_tracked_user_fields

    # if a non-admin user tries to access a page or take an action they dont have role/rights to,
    # instead of throwing an exception back, just redirect them to the browse page.
    rescue_from CanCan::AccessDenied do |exception|
        if Rails.env.development?
            puts exception.inspect
            exception.backtrace.each { |l| puts l }
        end
        render_unauthorized_error
    end

    # don't allow invalid urls to server error
    rescue_from ActiveRecord::RecordNotFound, with: :four_o_four

    def quantic
        @quantic ||= Institution.quantic
    end

    def smartly
        @smartly ||= Institution.smartly
    end

    def set_location_specific_flags
        @gdpr_user = gdpr_country_request? ? 'true' : 'false'
    end

    # This method was originally placed in api_crud_controller_base.rb, but was moved here so that
    # it could be accessible from our custom devise_token_auth controllers. Should probably be kept
    # here for any future instances where we need to know the client build number for backwards
    # compatibility purposes.
    def client_params
        @client_params ||= {
            'fr-client' => request.headers['fr-client'],
            'fr-client-version' => request.headers['fr-client-version'],

            # short-lived / deprecated flags
            'fr-only-push-on-ping' => request.headers['fr-only-push-on-ping'] # remove via: https://trello.com/c/fo43M8GY
        }
    end

    # This method was originally placed in api_crud_controller_base.rb, but was moved here so that
    # it could be accessible from our custom devise_token_auth controllers. Should probably be kept
    # here for any future instances where we need to know the client build number for backwards
    # compatibility purposes.
    def client_build_number
        client_params['fr-client-version'].to_i
    end

    def cloudflare_country_code
        # HACK: allow overriding the country code with a query param
        params['HTTP_CF_IPCOUNTRY'] || request.headers['HTTP_CF_IPCOUNTRY']
    end

    def us_request?
        cloudflare_country_code.in? %w(US)
    end

    def gdpr_country_request?
        # European Union countries, plus any others that decide to adopt the GDPR
        cloudflare_country_code.in? %w(AT BE BG HR CY CZ DK EE FI FR DE GR HU IE IT LV LT LU MT NL PL PT RO SK SI ES SE GB)
    end

    def cordova_client?(client_identifier = nil)
        client_identifier ||= client_params['fr-client']
        client_identifier != 'web'
    end

    # Add _FULL_ cache-busting response headers for controllers. Some proxies may cache max-age=0, must-revalidate
    # without no-store / no-cache, pragma cache header, and forced expiration
    # see also: http://stackoverflow.com/questions/711418/how-to-prevent-browser-page-caching-in-rails
    def set_cache_buster
        response.headers["Cache-Control"] = "no-cache, no-store, max-age=0, must-revalidate"
        response.headers["Pragma"] = "no-cache"
        response.headers["Expires"] = "Fri, 01 Jan 1990 00:00:00 GMT"
    end

    def set_headers
        set_cache_buster

        if Rails.env.development?
            response.headers.delete('X-Frame-Options')
        end

    end

    def is_staging?
        request.host.match(/staging\.smart\.ly/) || request.host.match(/staging\.quantic\.mba/) || request.host.match(/staging\.quantic\.edu/)
    end

    def is_local?
        request.host == 'localhost'
    end

    def is_pingdom?
        request.user_agent =~ %r{PingdomTMS}
    end

    # We use this instead of request.protocol; see comment in request_protocol.rb for details
    def request_protocol
        UrlHelper.protocol(request)
    end

    def handle_staging

        # ensure we're going over SSL
        if request.host.match(/smartly-staging\.elasticbeanstalk\.com/)
            redirect_to "https://staging.smart.ly"

        elsif is_staging?
            # used in index.html.erb and home.html.erb to set a class on the HTML element
            @is_staging = true

            # and use HTTP_AUTH if provided if so
            if ENV['HTTP_AUTH'] =~ %r{(.+)\:(.+)} && request.path != '/health' && !is_pingdom?
                unless authenticate_with_http_basic { |user, password|  user == $1 && password == $2 }
                    request_http_basic_authentication
                end
            end
        end
    end

    def handle_local
        if is_local?
            # always set is_local to false when in test environment to avoid
            # mucking up casper images with the striped banner at the top
            @is_local = !Rails.env.test?
        end
    end

    def verify_signed_in
        if current_user
            return true
        else
            render_logged_out_error
            return false
        end
    end

    def verify_current_user_id
        render_logged_out_error unless current_user_id
    end

    # override default CanCan definition of current_ability to support tokens
    # from scorm
    def current_ability
        if defined? @current_ability
            return @current_ability
        end

        @current_ability = if current_user
            Ability.new(current_user)
        elsif !request.headers['scorm-token'].blank?
            token = request.headers['scorm-token']
            institution = Institution.find_by_scorm_token(token)
            Ability.new(institution)
        else
            Ability.new(nil)
        end
    end

    def set_raven_context
        if current_user
            Raven.user_context( {
                'id' => current_user.id,
                'email' => current_user.email
            } )
        end
    end

    def unset_raven_context
        Raven.user_context({})
    end

    def render_logged_out_error
        render_error_response(I18n.t(:you_must_be_logged_in_to_do_that), :unauthorized, {
            not_logged_in: true
        })
    end

    def render_unauthorized_error
        render_error_response(I18n.t(:you_are_not_authorized_to_do_that), :unauthorized)
    end

    # `render_deactivated_error` and `render_hiring_web_only_error` are not internationalized for several reasons:
    #  1. At least one locale we wish to display contains HTML.
    #  2. I18n passes strings without modification.
    #  3. There are several "entry" points for getting these errors.
    #     CustomSessionsController is the only place we explicitly call these methods.
    #     Anything that uses omniauth will render an authFailure error, and place the error in
    #     the URL's query params, which can't contain HTML.
    def render_deactivated_error
        render_error_response(ERROR_ACCOUNT_DEACTIVATED_CODE, :unauthorized, {
            not_logged_in: true
        })
    end
    def render_hiring_web_only_error
        render_error_response(ERROR_HIRING_WEB_ONLY_CODE, :unauthorized, {
            not_logged_in: true
        })
    end

    def render_error_response(message = "", code = :bad_request, payload = {})
        payload[:meta] = @meta
        respond_to do |format|
            format.json { render json: {message: message}.merge(payload), status: code }
            format.html { render html: message, status: code }
        end
    end

    def cast_query_params(params)
        converted = params.clone
        converted.each do |key, value|
            if value == "true"
                converted[key] = true
            elsif value == "false"
                converted[key] = false
            end
        end
        converted
    end

    def four_o_four
        # We don't want to perform any non-404 status redirects here, as that throws off everything.
        # Instead, delegate to the MarketingController since that is where this layout / template
        # were designed and since we encapsulate a lot of dynamic content in callbacks that must
        # be invoked via the `.process`. We should potentially simplify this particular template
        # and bring it directly into ApplicationController in the future.
        delegate = MarketingController.new
        delegate.request = request
        delegate.response = response
        delegate.process(:four_o_four)
    end

    def smartly_ios
        redirect_to "https://itunes.apple.com/us/app/#{ENV['APP_NAME_IOS']}"
    end

    def smartly_android
        redirect_to "https://play.google.com/store/apps/details?id=#{ENV['APP_NAME_ANDROID']}"
    end

    def miyamiya_ios
        redirect_to "https://itunes.apple.com/us/app/#{ENV['APP_NAME_MIYAMIYA_IOS']}"
    end

    def miyamiya_android
        redirect_to "https://play.google.com/store/apps/details?id=#{ENV['APP_NAME_MIYAMIYA_ANDROID']}"
    end

    def current_user_id
        if current_user
            current_user.id
        else
            auid
        end
    end

    def auid
        # we put this in both places in order to support both
        # page requests (cookies) and requests from cordova (headers) (api requests
        # from a browser will have both the cookie and the header)
        #
        # We have seen the auid in headers and cookies be the string 'undefined'
        # or 'null'. We don't fully understand why that is, because we have some
        # client side code that attempts to ensure this doesn't happen.
        #
        # See also: https://sentry.io/organizations/pedago/issues/1707455561
        return request.headers['auid'] if request.headers['auid'].looks_like_uuid?
        return cookies['auid'] if cookies['auid'].looks_like_uuid?
        return nil
    end

    def user_from_auid_deleted?
        return false unless !current_user && auid
        @user_from_auid_deleted ||= UsersDeletion.find_by_user_id(auid)
        @user_from_auid_deleted.present?
    end

    def institutional_subdomain
        match = request.host.match(/(\w+)\.smart\.ly/)
        subdomain = match && match[1]

        # we have a CNAME alias for our staging EB environment
        if subdomain == 'staging'
            subdomain = nil
        end

        # In an omniauth callback, the original url will be stored in params['origin']
        # In local testing, rather than using subdomains, we user the query param 'subdomain'.
        # Check for that so that local testing will behave more like prod.
        if subdomain.nil? && (params['origin'] || params['subdomain'])

            if params['subdomain']
                subdomain = params['subdomain']
            else
                uri = URI.parse(params['origin'])
                query_hash = (uri.query && Rack::Utils.parse_nested_query(uri.query)) || {}
                subdomain = query_hash['subdomain']
            end
        end

        subdomain
    end

    def at_institutional_subdomain?
        !institutional_subdomain.blank?
    end

    # see https://github.com/plataformatec/devise/blob/master/lib/devise/hooks/trackable.rb
    #
    # The linked hook above will work properly for after_set_user Warden callback, but not
    # for registration. Here, we modify the condition for update_tracked_fields from above
    # to fire on registration or when the user returns after 24 hours time. We still rely on
    # the linked hook for login/authentication.
    def should_update_tracked_user_fields?
        return current_user &&
            current_user.respond_to?(:update_tracked_fields!) &&
            !request.env['warden'].request.env['devise.skip_trackable'] &&
            request.params[:ghost_mode] != "true" &&
            (current_user.current_sign_in_at.nil? || current_user.current_sign_in_at < 24.hours.ago)
    end

    protected

    def configure_permitted_parameters
        devise_parameter_sanitizer.permit(:sign_up, keys: [
            :name, :sign_up_code, :provider, :school, :id, :pref_locale,
            :job_title, :phone, :phone_extension, :country
            # We left out professional_organization and program_type here because we handle it specifically in custom_registrations_controller
        ])
        devise_parameter_sanitizer.permit(:sign_in, keys: [
            :email, :provider, :password, :phone
        ])

        # See https://trello.com/c/1I2tA1BG
        devise_parameter_sanitizer.permit(:account_update, keys: [
            :id, :name, :nickname, :email, :job_title, :phone,
            :country, :password, :sign_up_code, :password_confirmation,
            :confirmed_profile_info, :role_ids => []
        ])
    end

    def set_action_metadata
        global_metadata = GlobalMetadata.find_by_action(
            controller: params['controller'],
            action: params['action']
        )
        @action_metadata = ActionMetadata.new(global_metadata)
    end

    def set_locale

        # If we have a logged-in user, use their locale. Note we do not use #pref_locale.  The #locale method is a bit smarter.
        # Otherwise, try to grab a supported locale from Allow-Language or use config.default_locale
        if current_user
            I18n.locale = current_user.locale
        elsif !params['force_locale'].blank?
            compatible_locale = (I18n.available_locales & [params['force_locale'].to_sym]).first
            I18n.locale = compatible_locale.blank?  ? I18n.default_locale : compatible_locale
        else
            compatible_locale = http_accept_language.compatible_language_from(I18n.available_locales)
            I18n.locale = compatible_locale.blank?  ? I18n.default_locale : compatible_locale
        end

        # For storing the first Accept-Language value on the browser window object.
        # window.navigator.language can't be trusted -- see http://stackoverflow.com/a/2678437/1747491
        @server_determined_locale = I18n.locale

    end

    # see https://github.com/plataformatec/devise/blob/master/lib/devise/models/trackable.rb
    def update_tracked_user_fields
        return if !should_update_tracked_user_fields?
        current_user.update_tracked_fields!(request.env['warden'].request)
    end

    ########################################################################################################
    # ZipRecruiter requires us to put some extra contact info in the footer for users in Germany
    # This getter helps us conditionally add it if Cloudflare tells us the user is accessing from that country.
    ########################################################################################################

    helper_method :include_impressum?
    def include_impressum?
        cloudflare_country_code == 'DE'
    end

    ########################################################################################################
    # Methods to enable pages to dynamically present as Smartly vs. Quantic
    ########################################################################################################

    private def domain_with_overrides
        Rails.env.development? ? ( request.params[:force_host].present? ? request.params[:force_host] : request.domain ) : request.domain
    end

    helper_method :is_quantic?
    def is_quantic?
        domain_with_overrides != AppConfig.smartly_domain
    end

    helper_method :brand_name_short
    def brand_name_short
        is_quantic? ? quantic.brand_name_short : smartly.brand_name_short
    end

    helper_method :brand_name_standard
    def brand_name_standard
        is_quantic? ? quantic.brand_name_standard : smartly.brand_name_standard
    end

    helper_method :brand_name_long
    def brand_name_long
        is_quantic? ? quantic.brand_name_long : smartly.brand_name_long
    end

    helper_method :brand_social_name
    def brand_social_name
        is_quantic? ? AppConfig.quantic_social_name : AppConfig.smartly_social_name
    end

    helper_method :current_domain
    def current_domain
        is_quantic? ? AppConfig.quantic_domain : AppConfig.smartly_domain
    end
end
