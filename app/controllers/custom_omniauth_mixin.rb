module CustomOmniauthMixin

    def setup_new_record(resource, registration_details)

        # ensure the user has an email address, even if it's temporarily non-existant
        if registration_details['email'].blank?
            # the UID may actually be an email address ... if so, just assign that value
            if registration_details['uid'] =~ /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
                registration_details['email'] = registration_details['uid']
            end
        end

        # we'll be updating provider_info each login, but we'll only bake the original
        # uid and provider on initial oauth user creation
        # once, we saw a facebook oauth registration on production return the literal string "undefined" for sign_up_code
        # Paranoia means we'll check for both blank? and literal undefined below
        resource.uid = registration_details['uid']
        resource.provider = registration_details['provider']
        resource.email = registration_details['email']
        resource.name = registration_details['name'] unless registration_details['name'].blank? || registration_details['name'] === 'undefined'
        resource.school = registration_details['school'] unless registration_details['school'].blank? || registration_details['school'] === 'undefined'
        resource.sign_up_code = (registration_details['sign_up_code'].blank? || registration_details['sign_up_code'] == 'undefined') ? 'FREEMBA' : registration_details['sign_up_code']
        resource.pref_locale = (registration_details['pref_locale'].blank? || registration_details['pref_locale'] == 'undefined') ? 'en' : registration_details['pref_locale']

        # the timezone column has a with_fallback accessor, so we don't need to set it here if 'undefined' or blank?
        resource.timezone = registration_details['timezone'] unless registration_details['timezone'].blank? || registration_details['timezone'] == 'undefined'

        # Check for an experiment being set
        # See https://trello.com/c/AF4bThbe
        experiment_id = sanitize_null(registration_details['experiment_id'])
        if experiment_id.present?
            resource.experiment_ids << experiment_id
        end

        # if a program_type was provided, and it matches our whitelist, default the fallback and confirm it
        program_type = registration_details['program_type']
        program_type_config = Cohort::ProgramTypeConfig[program_type]
        if program_type_config&.supports_signup?
            resource.fallback_program_type = registration_details['program_type']
            resource.program_type_confirmed = true
        end

        # if specified, we'll skip the apply redirect (used for open courses signups)
        @resource.skip_apply = registration_details['skip_apply'] == 'true'

        # If the user was referred by someone then set it
        if registration_details['referred_by_id'] and referrer = User.find_by_id(registration_details['referred_by_id'])
            resource.referred_by = referrer
        end

        resource.set_random_password

        resource.register_content_access

        # If we didn't get a name or an email then we will pop up a box asking for it
        if resource.email.nil? || resource.name.nil?
            resource.confirmed_profile_info = false
        end

        resource.save!
        resource.active_institution = resource.institutions.first

        if resource.career_profile
            resource.career_profile.primary_reason_for_applying ||= sanitize_null(registration_details['primary_reason_for_applying'])
            resource.career_profile.survey_years_full_time_experience ||= sanitize_null(registration_details['survey_years_full_time_experience'])
            resource.career_profile.survey_most_recent_role_description ||= sanitize_null(registration_details['survey_most_recent_role_description'])
            resource.career_profile.survey_highest_level_completed_education_description ||= sanitize_null(registration_details['survey_highest_level_completed_education_description'])
            resource.career_profile.salary_range ||= sanitize_null(registration_details['salary'])
            resource.career_profile.save!
        end

        resource
    end

    private
    def sanitize_null(value)
        value == 'null' ? nil : value
    end
end