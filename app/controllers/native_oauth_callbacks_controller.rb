# Handles native oauth callbacks from Cordova
# Adopted from -- http://stackoverflow.com/questions/14812179/using-omniauth-for-facebook-login-with-only-access-token
class NativeOauthCallbacksController < DeviseTokenAuth::ApplicationController

    include CustomOmniauthMixin

    class InvalidAccessToken < RuntimeError; end

    rescue_from Exception, :with => :render_error_exception

    attr_accessor :raise_exceptions

    skip_after_action :update_auth_header

    # devise_token_auth has a `render_error` method which handles known failures in its flow
    # whereas, we want to insert additional error handling for our own purposes
    def render_error_exception(exception, message = I18n.t(:login_failed))
        raise exception if raise_exceptions # useful in tests

        if exception.is_a?(DeactivatedUserError)
            message = ERROR_ACCOUNT_DEACTIVATED_CODE

        elsif exception.is_a?(HiringWebOnlyError)
            message = ERROR_HIRING_WEB_ONLY_CODE

        elsif !exception.is_a?(InvalidAccessToken)
            Raven.capture_exception(exception)
        end

        return render_error_response(message, :unauthorized)
    end


    def facebook

        # use the provided access token to retrieve user info
        facebook_response = HTTParty.get("https://graph.facebook.com/me", query: { access_token: params[:access_token], fields: "id,email,name,picture.width(200)" })
        facebook_data = facebook_response.parsed_response

        # the only time we have seen this was when facebook went down.  In that
        # case, the response was a string, not a hash, so there was no 'error' property
        if !facebook_response.success?
            err = RuntimeError.new("Unsuccesful response from facebook.")
            err.raven_options = {
                extra: {
                    response: facebook_data
                }
            }
            raise err
        end

        # someone's playing around or the access token is stale
        if !facebook_data || facebook_data['error']
            raise InvalidAccessToken.new
        end

        # Check to see if the user exists given email (if it exists) / or the ID and provider
        email = facebook_data['email']
        name = facebook_data['name']
        uid = facebook_data['id']
        sign_up_code = params[:sign_up_code]
        timezone = params[:timezone]

        # perform the guts of the retrieval / creation / auth
        find_or_create_and_sign_in('facebook', uid, email, name, sign_up_code, timezone, facebook_data)

        yield if block_given?

        # render user merged with token values for ng-token-auth consumption
        create_auth_params_and_render

    end


    def google_oauth2

        # Unlike FB, we should get the email and userId directly as part of the initial sign-in exchange
        client_auth_info = JSON.parse(params[:provider_data])
        id_token = client_auth_info['idToken']

        # use the provided idToken to retrieve user info
        # see also: https://developers.google.com/identity/sign-in/web/backend-auth
        google_response = HTTParty.get("https://www.googleapis.com/oauth2/v3/tokeninfo", query: { id_token: id_token }).parsed_response

        # see also: https://developers.google.com/identity/protocols/OpenIDConnect#obtainuserinfo
        email = google_response['email']
        uid = google_response['sub']
        name = google_response['name']
        sign_up_code = params[:sign_up_code]
        timezone = params[:timezone]

        # validate that the request is being made by one of our clients
        client_app_id_matches = [
            AppConfig.google_oauth_id,
            AppConfig.google_oauth_id_ios,
            AppConfig.google_oauth_id_android
        ].include?(google_response['aud'])

        # someone's playing around or the id_token is stale
        if uid.blank? || !client_app_id_matches
            raise InvalidAccessToken.new
        end

        # perform the guts of the retrieval / creation / auth
        find_or_create_and_sign_in('google_oauth2', uid, email, name, sign_up_code, timezone, google_response)

        yield if block_given?

        # render user merged with token values for ng-token-auth consumption
        create_auth_params_and_render
    end


    def apple

        client_auth_info = JSON.parse(params[:provider_data])
        full_name = client_auth_info['fullName']

        # We should get the email and fullName fields directly as part of the INITIAL account creation, but never again.
        # They will remain as empty strings until the user unlinks and re-creates their account via Apple > Manage Apps
        email = client_auth_info['email'].present? ? client_auth_info['email'] : nil
        name = full_name && full_name['givenName'].present? ? "#{full_name['givenName']} #{full_name['familyName']}" : nil

        # we should get user info each subsequent call
        uid = client_auth_info['user']

        sign_up_code = params[:sign_up_code]
        timezone = params[:timezone]

        if uid.blank?
            raise InvalidAccessToken.new
        end

        # perform the guts of the retrieval / creation / auth
        provider = is_quantic? ? 'apple_quantic' : 'apple_smartly'

        find_or_create_and_sign_in(provider, uid, email, name, sign_up_code, timezone, client_auth_info)

        yield if block_given?

        # render user merged with token values for ng-token-auth consumption
        create_auth_params_and_render
    end


    def find_or_create_and_sign_in(provider, provider_user_id, email, name, sign_up_code, timezone, provider_data)

        # First check existence of uid / provider combination
        @resource = resource_class.where(:provider => provider, :uid => provider_user_id).first_or_initialize

        # Plan B ...
        if @resource.new_record?

            email_values = [
                email.blank? ? nil : email,
                provider_user_id.blank? ? nil : provider_user_id
            ].compact

            # we are really going to trust our OAuth providers' email validation here
            if email_values.any?
                @resource = resource_class.where(:email => [email, provider_user_id].compact).first_or_initialize
            end
        end

        raise DeactivatedUserError.new if @resource&.deactivated

        raise HiringWebOnlyError.new if @resource&.hiring_application

        # register the user if they don't exist
        if @resource.new_record?
            registration_info = {
                'email' => email,
                'uid' => provider_user_id,
                'provider' => provider,
                'sign_up_code' => sign_up_code,
                'timezone' => timezone,
                'name' => name
            }
            setup_new_record(@resource, registration_info)
        end

        # perform devise sign-in
        sign_in(:user, @resource, store: false, bypass: false)

        # create and set token info on the user
        set_token_on_resource

        # update the provider_user_info to the most recent info
        # from the provider on every login.  We're not relying on this
        # info for anything right now, but it's good to have.
        @resource.set_provider_user_info(provider, provider_data)

        # save out changes to token and provider_user_info
        @resource.save

        return @resource
    end

    def set_token_on_resource
        @token  = @resource.create_token
    end

    def create_auth_params
      @auth_params = {
        auth_token: @token.token,
        client_id:  @token.client,
        expiry:     @token.expiry,
        config:     @config
      }
      @auth_params
    end

    def create_auth_params_and_render
        # render user merged with token values for ng-token-auth consumption
        create_auth_params
        render json: {
            data: @auth_params.merge(@resource.token_validation_response())
        }
    end

end
