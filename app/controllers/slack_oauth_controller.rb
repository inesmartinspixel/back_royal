# Handles OAuth 2.0 flow for installing a Slack app to a workspace.
# See https://api.slack.com/authentication/oauth-v2.
class SlackOauthController < ApplicationController
    attr_accessor :state, :code, :slack_room_id # for specs

    SLACK_APP_COOKIE_IDENTIFIER_PREFIX = 'slack_app_cookie_'

    # See https://api.slack.com/authentication/oauth-v2#asking
    SLACK_OAUTH_REDIRECT_URL = 'https://slack.com/oauth/v2/authorize'

    # See https://api.slack.com/apps/A012LAWE5JT/oauth for explanations
    # of why these specific scopes are requested. NOTE: This list should
    # match the scopes listed on that page.
    COHORT_EXERCISES_SLACK_APP_SCOPES = [
        'channels:join',
        'channels:manage',
        'channels:read',
        'chat:write',
        'chat:write.customize',
        'chat:write.public',
        'files:write',
        'pins:write'
    ]

    # This endpoint is responsible for redirecting the user to Slack to request authorization for
    # scopes that the Slack app needs to work. See https://api.slack.com/authentication/oauth-v2#asking.
    def auth
        return render_unauthorized_error unless current_user&.has_role?(:admin)

        # We store some data in a cookie for use later on when Slack sends a request to the auth_callback
        # endpoint after the user has either approved or denied the Slack app and its requested scopes.
        cookie_value = {
            # We'll use this to find and update the CohortSlackRoom record with the access token from Slack.
            slack_room_id: params[:slack_room_id],
            # We'll send the user back to this URL after the OAuth flow has been completed.
            request_origin_url: params[:request_origin_url] || '/'
        }

        # The `state` value is a random UUID that will be used as a unique identifier for retrieving
        # the cookie and later matched against the value returned from Slack in the `auth_callback`
        # method to detect CSRF attacks. See https://api.slack.com/authentication/oauth-v2#asking.
        state = SecureRandom.uuid
        cookie_identifier = slack_app_cookie_identifier(state)
        cookies.encrypted[cookie_identifier] = {
            value: cookie_value,
            expires: 1.hour.from_now
        }

        # Redirect the user to Slack to request authorization for scopes that the Slack app requires to work.
        # See https://api.slack.com/authentication/oauth-v2#asking and the Scopes section at https://api.slack.com/apps/A012LAWE5JT/oauth.
        query_params = {
            'scope' => self.class::COHORT_EXERCISES_SLACK_APP_SCOPES.join(','),
            'client_id' => ENV['COHORT_EXERCISES_SLACK_APP_CLIENT_ID'],
            'redirect_uri' => redirect_uri,
            'state' => state
        }
        redirect_to "#{self.class::SLACK_OAUTH_REDIRECT_URL}?#{query_params.to_query}"
    end

    # The OAuth Redirect URL endpoint responsible for handling the request sent from Slack after the
    # user has either authorized or denied authorization for the Slack app and its requested scopes.
    # See https://api.slack.com/apps/A012LAWE5JT/oauth for where we've configured the Slack app with
    # a list of Redirect URLs that point to this endpoint. For use in local development, you'll need
    # to whitelist your ngrok domain at that page.
    def auth_callback
        return render_unauthorized_error unless current_user&.has_role?(:admin)

        # Included in the HTTP request from Slack is a `code` param, which acts as a temporary authorization
        # code. We then exchange this temporary authorization code for an access token that won't expire.
        # See https://api.slack.com/authentication/oauth-v2#exchanging.
        @code = params[:code]
        @state = params[:state]

        # Use the `state` included in the request sent from Slack to check for CSRF attack.
        # The `state` value should match the same `state` value that we sent along in the
        # authorization request, so if we can't find a cookie for the `state` value included
        # in the request from Slack, then we can consider it a forgery.
        # See https://api.slack.com/authentication/oauth-v2#exchanging.
        cookie_identifier = slack_app_cookie_identifier(@state)
        cookie_value, error = check_for_csrf_attack(cookie_identifier)

        return render_error_response(error, :forbidden) if error

        @slack_room_id = cookie_value[:slack_room_id]
        request_origin_url = cookie_value[:request_origin_url]

        # We've gotten all of the information from the cookie at this point,
        # so we can get rid of it since it's no longer needed.
        cookies.delete(cookie_identifier)

        error = check_for_auth_callback_request_error
        if error
            # `access_denied` means that the user denied authorization for the Slack app and the scopes
            # that it requested. Not much to do except to respect their decision and send them back to
            # where they came from.
            return redirect_to request_origin_url if error == 'access_denied'
            return redirect_to generate_url_with_query_params(request_origin_url, {slack_oauth_error: error})
        end

        oauth_access_response, error = exchange_temporary_auth_code_for_access_token
        return redirect_to generate_url_with_query_params(request_origin_url, {slack_oauth_error: error}) if error

        access_token = oauth_access_response['access_token']
        error = update_slack_room(access_token)
        return redirect_to generate_url_with_query_params(request_origin_url, {slack_oauth_error: error}) if error

        redirect_to generate_url_with_query_params(request_origin_url, {slack_oauth_success: @slack_room_id})
    end

    # The URL that Slack will send a request to after the user has either granted or denied access
    # to the Slack app and its requested scopes.
    private def redirect_uri
        # We're installing this Slack app on multiple workspaces. One of the requirements for doing
        # this is that the Redirect URLs must be configured with HTTPS. So if you're testing this
        # on local development, you'll need to use your ngrok tunnel and whitelist it here:
        # https://api.slack.com/apps/A012LAWE5JT/oauth.
        "https://#{request.host_with_port}/slack/auth_callback"
    end

    private def slack_app_cookie_identifier(state)
        "#{SLACK_APP_COOKIE_IDENTIFIER_PREFIX}#{state}"
    end

    private def check_for_csrf_attack(cookie_identifier)
        cookie_value = cookies.encrypted[cookie_identifier]
        if !cookie_value
            Raven.capture_exception('Possible CSRF attack detected in OAuth callback request from Slack',
                level: 'warning',
                extra: { state: @state, code: @code }
            )
            error = 'A possible CSRF attack was detected in the OAuth callback request sent from Slack. Engineers have been notified. Please retry adding the Slack app to the workspace or contact an engineer directly if the issue persists.'
        end
        return [cookie_value, error]
    end

    private def check_for_auth_callback_request_error
        # Slack includes an `error` param in the request if it encountered an error during the OAuth flow.
        error = params[:error]

        # Anything other than an 'access_denied' error is an error that we weren't expecting,
        # so we log an event to Sentry so we can look into it futher to see if we should adjust
        # this error handling logic in some way to provide better handling for it.
        if error.present? && error != 'access_denied'
            Raven.capture_exception('Slack OAuth sent request to auth_callback with an unexpected error.',
                extra: extra_for_sentry(error: error)
            )
            error = "Slack responded to the authorization request with the following error: #{error}. Engineers have been notified. Contact an engineer directly if the issue persists."
        end

        return error
    end

    # Exchanges the temporary authorization code for an access_token for the bot user.
    private def exchange_temporary_auth_code_for_access_token
        # Exchange the temporary authorization code for the bot user access token.
        # This exchange doesn't need a token, so we explicitly create the client
        # without a token so that it overrides the default token configured in
        # the Slack initializer. See the following docs:
        #   https://api.slack.com/authentication/oauth-v2#exchanging
        #   https://api.slack.com/methods/oauth.v2.access
        #   https://github.com/slack-ruby/slack-ruby-client.
        begin
            oauth_access_response = Slack::Web::Client.new(token: nil).oauth_v2_access(
                code: @code,
                client_id: ENV['COHORT_EXERCISES_SLACK_APP_CLIENT_ID'],
                client_secret: ENV['COHORT_EXERCISES_SLACK_APP_CLIENT_SECRET'],
                redirect_uri: redirect_uri
            )

            @slack_workspace_name = oauth_access_response['team'] && oauth_access_response['team']['name']
        rescue Slack::Web::Api::Error => error
            Raven.capture_exception('Slack OAuth responded with an error for oauth.v2.access method.',
                extra: extra_for_sentry(error: error.message)
            )
            error_message = "Slack responded to oauth.v2.access request with the following error: #{error.message}."
        end
        return [oauth_access_response, error_message]
    end

    # Finds and updates the associated slack room with the given access_token.
    private def update_slack_room(access_token)
        slack_room = CohortSlackRoom.find_by_id(@slack_room_id)

        if slack_room
            if !slack_room.update_attribute(:bot_user_access_token, access_token)
                error = "Failed to update bot_user_access_token for CohortSlackRoom record #{@slack_room_id}."
                return error
            end
        else
            Raven.capture_exception('Could not find CohortSlackRoom during Slack OAuth flow.', extra: extra_for_sentry)
            error = "Could not find a CohortSlackRoom record in our database for the following id: #{@slack_room_id}."
            return error
        end
    end

    private def extra_for_sentry(extra = {})
        {
            slack_room_id: @slack_room_id,
            state: @state,
            code: @code
        }.merge(extra)
    end

    # Generates a new URL with the query_params added to the given url in a clean way.
    private def generate_url_with_query_params(url, query_params)
        uri = URI(url)
        decoded_params = URI.decode_www_form(uri.query || '')
        query_params.each { |key, value| decoded_params << [key, value] }
        uri.query = decoded_params.to_h.to_query
        uri.to_s
    end
end
