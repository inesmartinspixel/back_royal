class HomeController < ApplicationController

    before_action :set_action_metadata
    before_action :set_client_identifier

    # after logging in as a user, we refresh to get out of that user, and don't want to update their trackable attributes afterward
    skip_before_action :update_tracked_user_fields, only: [:index]

    INSTITUTIONAL_TITLE = "Smartly - Learn Online, Everywhere"

    class UnsupportedFormat < StandardError
    end

    def index

        # NYU requested their join pages not be visible to search engines
        if request.path.start_with?("/nyu/")
            @visible_to_search = false
        else
            @visible_to_search = true
        end

        if at_institutional_subdomain?
            @action_metadata.title = INSTITUTIONAL_TITLE
        end

        # we have to specify index because we alias a few different actions
        # to this method in order to support global metadata stuff
        render "index"

    # Silence MissingTemplate errors caused by bad formats or lame attempts at hacking
    rescue ActionView::MissingTemplate
        four_o_four
    end

    # in order to support metadata for these
    # pages, they each need to have their own
    # action
    alias_method :library, :index
    alias_method :join, :index
    alias_method :"sign-in", :index
    alias_method :"forgot-password", :index
    alias_method :settings, :index

    def course_context
        @visible_to_search = true

        stream_id = params[:stream_id]
        lesson_id = params[:lesson_id] # only here if we followed a link directly to a lesson/frame

        if !stream_id.blank?
            stream = Lesson::Stream.find(stream_id)
            if !stream.nil?
                @action_metadata.title = stream.entity_metadata.title
                @action_metadata.description = stream.entity_metadata.description
                @action_metadata.canonical_url = "#{request_protocol}#{request.host}#{stream.entity_metadata.canonical_url}"
                if !stream.entity_metadata.image.nil?
                    @action_metadata.image = stream.entity_metadata.image
                elsif stream.image
                    # fall back to course image
                    @action_metadata.image = stream.image
                end

            end
        end

        if !lesson_id.blank?
            lesson = Lesson.find(lesson_id) # we do this just to force a 404 if a bad lesson ID was passed in (no need to render the app then)
        end

        render "index"
    end

    # let routing handle
    def lesson_index
        @visible_to_search = true

        id = params[:lesson_id]
        if !id.blank?
            lesson = Lesson.find(id)
            urlpath = request.fullpath.gsub("?#{request.query_string}", "")

            if !lesson.nil?
                # if the incoming url is not the canonical url, redirect to canonical url
                if urlpath != lesson.entity_metadata.canonical_url
                    redirect_to "#{request_protocol}#{request.host}:#{request.port}#{lesson.entity_metadata.canonical_url}"
                else
                    if !lesson.entity_metadata.image.nil?
                        @action_metadata.image = lesson.entity_metadata.image
                    end

                    @action_metadata.title = lesson.entity_metadata.title
                    @action_metadata.description = lesson.entity_metadata.description
                    @action_metadata.canonical_url = "#{request_protocol}#{request.host}#{lesson.entity_metadata.canonical_url}"
                    render "index"
                end
            end
        end

    end

    def set_client_identifier
        @client_identifier = 'web'
    end

end
