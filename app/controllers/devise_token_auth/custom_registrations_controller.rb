module DeviseTokenAuth
    class CustomRegistrationsController < DeviseTokenAuth::RegistrationsController

        prepend_before_action :check_captcha, only: [:create], unless: :captcha_not_required

        # We have to implement this stuff by overriding render, rather than
        # just with an after_action, because we need this to get run
        # after the user is created but before it is rendered by
        # devise_token_auth's registrations_controller.  This is not
        # a great solution.
        def render(opts)
            set_user_resource

            begin
                if @user_resource
                    if params[:action] == "create"
                        # If we see that we're trying to render an error response, we shouldn't even try
                        # to setup the user for saving in our database. Instead, just raise a RecordInvalid
                        # error so that our custom render_existing_user_with_email_provider_error and
                        # render_existing_user_with_phone_no_password_provider_error methods can attach
                        # any errors they see fit to the `opts` and let the error response render.
                        if opts[:json][:status] == 'error'
                            raise ActiveRecord::RecordInvalid.new(@user_resource)
                        end

                        # set up access control
                        @user_resource.register_content_access

                        # support defaulted-locales fed from join_config
                        join_config = AppConfig.join_config_for_sign_up_code(@user_resource.sign_up_code)
                        if join_config && !join_config["locale"].blank?
                            @user_resource.pref_locale = join_config["locale"]
                        end

                        # save access control and locale changes
                        @user_resource.save!

                        apply_career_profile_params

                        # FIXME: we should have a transaction here: https://trello.com/c/xjnYLfa4/1076-chore-figure-out-how-to-wrap-registrations-in-a-transaction

                        # * career profile auto-creation handled by the user model itself
                        # * hiring_application auto-creation is here because it requires extra params
                        #   that are only available here and because hiring managers can only sign up
                        #   though this controller, not through oauth
                        if join_config == AppConfig.join_config["hiring"]
                            create_hiring_application
                        end
                    end

                    opts[:json][:data] = @user_resource.as_json
                end
            rescue ActiveRecord::RecordInvalid
                # no need to save validation errors; the validations will run again in super and return the correct json format

                # except for these particular cases where we want to give the user information when
                # about a duplicate key error.
                render_existing_user_with_email_provider_error(opts)
                render_existing_user_with_phone_no_password_provider_error(opts)
            end

            super(opts)
        end


        def create

            # Hack: block spammers?
            if sign_up_params[:name] =~ /\.(com|net|org|ru)/i ||
                sign_up_params[:name] =~ /\s{5,}/ ||
                sign_up_params[:name] =~ /[@\:!?！，一、。]/ ||
                sign_up_params[:name].length > 50 ||
                sign_up_params[:email] =~ /[0-9]+@qq\.com/ ||
                sign_up_params[:email] =~ /[0-9]+@126\.com/ ||
                sign_up_params[:email] =~ /[0-9]+@163\.com/

                Raven.capture_exception("Possible Spammer!", {
                    level: 'warning'
                })

            end

            build_resource

            unless @resource.present?
                raise DeviseTokenAuth::Errors::NoResourceDefinedError,
                    "#{self.class.name} #build_resource does not define @resource,"\
                    ' execution stopped.'
            end

            # give redirect value from params priority
            @redirect_url = params[:confirm_success_url]

            # fall back to default value if provided
            @redirect_url ||= DeviseTokenAuth.default_confirm_success_url

            # success redirect url is required
            if confirmable_enabled? && !@redirect_url
            return render_create_error_missing_confirm_success_url
            end

            # if whitelist is set, validate redirect_url against whitelist
            return render_create_error_redirect_url_not_allowed if blacklisted_redirect_url?(@redirect_url)


            do_custom_create_stuff # Smartly - custom attribute assignment

            # override email confirmation, must be sent manually from ctrl
            callback_name = defined?(ActiveRecord) && resource_class < ActiveRecord::Base ? :commit : :create
            resource_class.set_callback(callback_name, :after, :send_on_create_confirmation_instructions)
            resource_class.skip_callback(callback_name, :after, :send_on_create_confirmation_instructions)

            if @resource.respond_to? :skip_confirmation_notification!
                # Fix duplicate e-mails by disabling Devise confirmation e-mail
                @resource.skip_confirmation_notification!
            end

            if @resource.save
                yield @resource if block_given?

                unless @resource.confirmed?
                    # user will require email authentication
                    @resource.send_confirmation_instructions({
                        client_config: params[:config_name],
                        redirect_url: @redirect_url
                    })
                end

                if active_for_authentication?
                    # email auth has been bypassed, authenticate user
                    @token = @resource.create_token
                    @resource.save!
                    update_auth_header
                end

                render_create_success
            else
                clean_up_passwords @resource
                render_create_error
            end

        end

        def update
            if @resource
                EditorTracking.transaction(@resource) do
                    if @resource.send(resource_update_method, account_update_params)
                        yield @resource if block_given?
                        @resource.career_profile.touch if should_touch_career_profile # update the updated_at field on the user's career profile if necessary
                        render_update_success
                    else
                        render_update_error
                    end
                end
            else
                render_update_error_user_not_found
            end
        end

        def do_custom_create_stuff
            # Set the user's company if provided
            if params.key?("professional_organization")
                professional_organization_params = params.permit(:professional_organization => [:locale, :text])[:professional_organization]
                @resource.professional_organization = ProfessionalOrganizationOption.find_or_initialize_by(professional_organization_params)
            end

            # if a program_type was provided, and it matches our whitelist, default the fallback and confirm it
            program_type = params.permit(:program_type)[:program_type]
            program_type_config = Cohort::ProgramTypeConfig[program_type]
            if program_type_config&.supports_signup?
                @resource.fallback_program_type = program_type
                @resource.program_type_confirmed = true
            end

            # if specified, we'll skip the apply redirect (used for open courses signups)
            @resource.skip_apply = params.permit(:skip_apply)[:skip_apply] == 'true'

            # If the user was referred by someone then set it
            if params.key?("referred_by_id") and referrer = User.find_by_id(params["referred_by_id"])
                @resource.referred_by = referrer
            end

            if params.key?("timezone") && Timezone[params[:timezone]].valid?
                @resource.timezone = params.permit(:timezone)["timezone"]
            end

            # Check for an experiment being set
            # See https://trello.com/c/AF4bThbe
            experiment_id = sanitize_null(params[:experiment_id])
            if experiment_id.present?
                @resource.experiment_ids << experiment_id
            end
        end

        # Determines if the account info changes to the user record should result in the user's career profile
        # being touched (if present), forcing the career profile's updated_at field to be changed. We do this
        # to ensure that if a user updates their account info, their career profile also gets updated, allowing
        # Zapier to pick up the updated information the next time it polls the server.
        def should_touch_career_profile
            previous_changes = @resource.previous_changes

            # make sure to check for a pending application since that's what our Zapier app filters for anyway
            if @resource.career_profile.present? && @resource.pending_application.present? &&
                (previous_changes["email"] || previous_changes["name"] || previous_changes["nickname"])
                return true
            else
                return false
            end
        end

        def sign_up_params
            if !defined? @sign_up_params

                # should be filtered using configure_permitted_parameters before_action
                @sign_up_params = super

                # If the client assigned an id to the user to store in events,
                # we want to assign that id to the real user now.  It should never
                # happen, but on the off chance that the same id has been passed up
                # twice, and a user with this id exists in the db, just ignore the
                # id and create the user with a new id
                id = @sign_up_params['id']
                if id && User.exists?(id: id)
                    # if a user logs out and then creates a new user, they will still have
                    # the id for their actual user stored in local storage as an auid.  We
                    # should ignore that id and create a new user with a new id.  This would
                    # be a little cleaner if we stored the auid and the last_logged_in_user_id
                    # separately in local storage, but not worth the effort right now.
                    @sign_up_params.delete('id')
                end
            end

            if @sign_up_params['provider'] == 'phone_no_password'
                @sign_up_params['password'] = "fake password"
                @sign_up_params['uid'] = SecureRandom.uuid
            end

            @sign_up_params
        end

        def set_user_resource
            @user_resource = @resource
        end

        def build_resource
            @resource            = resource_class.new(sign_up_params)
            @resource.provider   = provider

            # honor devise configuration for case_insensitive_keys
            if resource_class.case_insensitive_keys.include?(:email)
              @resource.email = sign_up_params[:email].try(:downcase)
            else
              @resource.email = sign_up_params[:email]
            end
        end

        # Since devise token auth
        # 1. assumes any duplicate key error is due to a duplicate email
        # 2. eats the error up internally
        #
        # We have to use circumstantial evidence to find out if there is a duplicate user
        # and then assume that that is the error that got us here
        def render_existing_user_with_email_provider_error(opts)
            return unless sign_up_params['provider'] == 'email'
            existing_resource = @user_resource.email && resource_class.where(email: @user_resource.email.downcase).first
            if existing_resource && (['facebook', 'google_oauth2'].include?(existing_resource.provider) || existing_resource.provider.start_with?('apple'))
                message = I18n.t(:please_try_logging_in_with, { provider: I18n.provider(existing_resource.provider) })
                errors = {
                    email: [message],
                    full_messages: [message]
                }
                opts[:json][:errors] = errors.as_json
            end
        end

        # see comment above render_existing_user_with_email_provider_error
        def render_existing_user_with_phone_no_password_provider_error(opts)
            return unless sign_up_params['provider'] == 'phone_no_password'
            existing_resource = @user_resource.phone && resource_class.where(phone: @user_resource.phone).first
            if existing_resource
                message = I18n.t(:an_account_with_that_phone_number_already_exists)
                errors = {
                    phone: [message],
                    full_messages: [message]
                }
                opts[:json][:errors] = errors.as_json
            end
        end

        def apply_career_profile_params
            if @user_resource.career_profile
                @user_resource.career_profile.primary_reason_for_applying = @user_resource.career_profile.primary_reason_for_applying ||
                    sanitize_null(params[:primary_reason_for_applying])
                @user_resource.career_profile.survey_years_full_time_experience = @user_resource.career_profile.survey_years_full_time_experience ||
                    sanitize_null(params[:survey_years_full_time_experience])
                @user_resource.career_profile.survey_most_recent_role_description = @user_resource.career_profile.survey_most_recent_role_description ||
                    sanitize_null(params[:survey_most_recent_role_description])
                @user_resource.career_profile.survey_highest_level_completed_education_description = @user_resource.career_profile.survey_highest_level_completed_education_description ||
                    sanitize_null(params[:survey_highest_level_completed_education_description])
                @user_resource.career_profile.salary_range = @user_resource.career_profile.salary_range ||
                    sanitize_null(params[:salary])
                @user_resource.career_profile.save!
            end
        end

        def create_hiring_application
            return if @user_resource.hiring_application.present?

            @user_resource.hiring_application = HiringApplication.create!({
                user_id: @user_resource.id,
                job_role: params[:job_role],
                company_year: params[:company_year],
                company_employee_count: params[:company_employee_count],
                company_annual_revenue: params[:company_annual_revenue],
                company_sells_recruiting_services: params[:company_sells_recruiting_services],
                place_id: params[:place_id],
                place_details: params[:place_details]
            })
        end

        private
        def captcha_not_required
            # FIXME: Ideally we should add the captcha to the /bos/start page now that it is resurrected, rather
            # than skipping for its sign_up_code.
            # See https://trello.com/c/BWmNizQ6
            [AppConfig.google_oauth_id_ios, AppConfig.google_oauth_id_android].compact.include?(params[:client_id]) ||
            params[:sign_up_code] == 'BOSHIGH' ||
            captcha_validation_turned_off? ||
            params[:using_dynamic_landing_page] == 'true' && ENV['CAPTCHA_NOT_REQUIRED_FOR_DLP'] == 'true'
        end

        private
        def captcha_validation_turned_off?
            ENV['CAPTCHA_NOT_REQUIRED_FOR_PUPPET'] == 'true' || # used in puppet specs
            ENV['SKIP_CAPTCHA_VALIDATION'] == 'true' # set this in dev mode if you're having issues with captcha (CAPTCHA_NOT_REQUIRED_FOR_PUPPET prevents me from submitting the form in the client)
        end

        private
        def check_captcha
            unless verify_recaptcha
                self.resource = resource_class.new sign_up_params
                resource.validate # Look for any other validation errors besides Recaptcha
                Raven.capture_exception('recaptcha blocked registration', {
                    level: 'warning',
                    extra: {
                        client_id: params[:client_id],
                        sign_up_code: params[:sign_up_code]
                    }
                })
                render_unauthorized_error
            end
        end

        private
        def sanitize_null(value)
            value == 'null' ? nil : value
        end
    end
end
