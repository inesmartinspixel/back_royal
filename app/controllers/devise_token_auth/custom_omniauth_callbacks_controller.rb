module DeviseTokenAuth
    class CustomOmniauthCallbacksController < DeviseTokenAuth::OmniauthCallbacksController

        include CustomOmniauthMixin

        rescue_from Exception, :with => :render_error_exception
        before_action :log_saml_response
        attr_accessor :raise_exceptions

        def log_saml_response
            return unless ENV['LOG_ALL_SAML_RESPONSES'] == 'true'

            if request.params['SAMLResponse']
                response = OneLogin::RubySaml::Response.new(request.params['SAMLResponse'])

                # this is before the redirect, so auth_hash has not
                # been copied into the session by redirect_callbacks yet
                auth_hash = get_auth_hash(request.env['omniauth.auth'])
                message = "**** SAML Response ****\n\n#{response.response}\n\n**** auth_hash ****\n\n#{auth_hash.to_h.inspect}"
                LogToS3.log("omniauth-debugging", "#{request['action']}:saml_response", message)
            end
        rescue Exception => err
            # silence all errors
            Raven.capture_exception(err)
        end

        # devise_token_auth has a `render_error` method which handles known failures in its flow
        # whereas, we want to insert additional error handling for our own purposes
        def render_error_exception(exception)

            taken_message = I18n.t('errors.messages.taken')

            # I think that this first block is only now accessible in
            # the case where you are trying to login with saml and the
            # account already exists.  If you are using a different
            # provider than you used in the past, we just log you in now.
            # In the saml case, it is the RecordInvalid exception with "has already been taken" message
            if @resource && (
                exception.is_a?(PG::UniqueViolation) ||
                exception.is_a?(ActiveRecord::RecordNotUnique) ||
                (exception.is_a?(ActiveRecord::RecordInvalid) && exception.message.match(taken_message))
            )

                # In this case, the exception will be the RecordInvalid with the message "has already been taken".
                # This actually works even in a locale other than english because the
                if OMNIAUTH_SAML_PROVIDERS.include?(@resource.provider)
                    Raven.capture_exception(RuntimeError.new("Insitutional login attempted with existing account: provider: #{@resource.provider}, uid: #{@resource.uid}, email: {@resource.email}"))
                    return render_plain_text_error(I18n.t(:account_exists_contact_admin))
                end

                existing_user = User.find_by_uid(@resource.uid) || User.find_by_email(@resource.email)

                provider = existing_user.provider
                params[:message] = if provider == 'email'
                    I18n.t(:an_account_with_that_email_already_exists)
                else
                    I18n.t(:that_account_appears_to_a_x_account, {provider: I18n.provider(provider)})
                end
            elsif exception.is_a?(DeactivatedUserError)
                params[:message] = ERROR_ACCOUNT_DEACTIVATED_CODE

            elsif exception.is_a?(HiringWebOnlyError)
                params[:message] = ERROR_HIRING_WEB_ONLY_CODE
            else
                exception.raven_options = {
                    extra: {
                        params: params,
                        session: request.session
                    }
                }
                raise exception if should_raise_exceptions?
                Raven.capture_exception(exception)
                params[:message] ||= I18n.t(:login_failed)
            end

            omniauth_failure
        end

        def should_raise_exceptions?
            # raise_exceptions is sometimes set to true in tests
            raise_exceptions || Rails.env == 'development'
        end

        def render_plain_text_error(message)
            render inline: %Q|

                <html>
                        <head></head>
                        <body>
                                #{message}
                        </body>
                </html>|
        end

        def redirect_callbacks
            # the way devise_token_auth gets the devise_mapping seems to only support SSO-initiated
            # login, Not IDP-initiated (i.e. when a user clicks on a Smartly
            # link from their JLL portal). They are assuming that the initial
            # request added that value as a query_param, but since the initial request
            # does not come from us in the IDP-initiated case, we can't set it.  Fortunately,
            # we know that it is always user, so no problem.
            devise_mapping = :user
            redirect_route = "/#{Devise.mappings[devise_mapping].as_json["path"]}/#{params[:provider]}/callback"

            # preserve omniauth info for success route. ignore 'extra' in twitter
            # auth response to avoid CookieOverflow.
            session['dta.omniauth.auth'] = get_auth_hash(request.env['omniauth.auth'])
            session['dta.omniauth.params'] = request.env['omniauth.params']

            redirect_to redirect_route
        end

        def get_auth_hash(full_hash)
            # We attempt to prevent CookieOverflow issues here by reducing the auth_hash as able
            auth_hash = (full_hash || {}).except('extra', 'credentials')

            # google and facebook never send us emails with capital letters,
            # but saml integrations sometimes do.
            auth_hash['uid'] = auth_hash['uid'].downcase if auth_hash['uid'].present?
            if auth_hash['info']

                # only take what we use
                auth_hash['info'].slice!('name', 'email', 'first_name', 'last_name', 'image', 'picture', 'imageUrl')

                # normalize emails to downcase
                if auth_hash['info']['email']
                    auth_hash['info']['email'] = auth_hash['info']['email'].downcase
                end
            end

            auth_hash
        end

        def get_resource_from_auth_hash

            raise RuntimeError.new("No auth_hash") unless auth_hash.present?

            # NOTE: if you make a change to where the email is read from, see
            # get_auth_hash, where we are downcasing the email
            email = auth_hash['info'] ? auth_hash['info']['email'] : nil

            # find or create user by provider and provider uid
            @resource = resource_class.where({
                uid:      auth_hash['uid'],
                provider: auth_hash['provider']
            }).first_or_initialize

            # If there was no record for the uid and provider, check
            # to see if there is one for the email from the provider.
            # We are really going to trust our OAuth providers' email validation here
            if @resource.new_record? && email.present?
                @resource = resource_class.where(:email => email).first_or_initialize
            end

            raise DeactivatedUserError.new if @resource&.deactivated

            # we can't rely on header values at this point since we're not going to be hitting the
            # client-config interceptor in this case. however - we're now explicitly passing `fr_client`
            # in omniauth.js to support this use-case
            raise HiringWebOnlyError.new if cordova_client?(omniauth_params['fr_client']) && @resource&.hiring_application

            # sync user info with provider, update/generate auth token
            assign_provider_attrs(@resource, auth_hash)

            # update the provider_user_info to the most recent info
            # from the provider on every login.  We're not relying on this
            # info for anything right now, but it's good to have.
            @resource.set_provider_user_info(auth_hash['provider'], auth_hash['info'])

            if @resource.new_record?
                @oauth_registration = true
                registration_details = omniauth_params.merge({
                    'email' => email,
                    'provider' => auth_hash['provider'],
                    'uid' => auth_hash['uid']
                })
                setup_new_record(@resource, registration_details)
            end

            @resource
        end

        # this can be hit either
        # 1. from the render_error handler above, which happens if there is
        #    and exception raised in our ruby code
        # 2. from a redirect initiated by the omniauth gem, which happens
        #    in error cases during authentication.  For example, if someone
        #    clicks "Cancel" when asked if it's okay to give smart.ly access
        #    to their profile on the Facebook or Google oauth screen.  We
        #    have to setup this route by hand because devise_token_auth does not
        #    seem to do it right.  See https://github.com/lynndylanhurley/devise_token_auth/issues/262
        def omniauth_failure

            set_user_facing_error_message

            # If we send SAML users back to the sign in page, we will
            # just try to log them in automatically again.  This will
            # create an infinite loop, assuming the error is consistent.
            # Instead, just show the error. (repro this by deleting
            # the institution associated with a provider and trying
            # to login as a new user)
            # (Note: we check for resource first because, if someone refreshes
            # the page after seeing this error they will hit this route with
            # no params)
            if institutional?
                return render_plain_text_error I18n.t(:please_contact_your_administrator, {message: params[:message]})
            end

            super
        end

        def set_user_facing_error_message
            # expecting user_denied, access_denied, or csrf_detected
            # see "omniauth error codes" section of en.yml
            if ![ERROR_ACCOUNT_DEACTIVATED_CODE, ERROR_HIRING_WEB_ONLY_CODE].include?(params[:message])
                params[:message] = I18n.t(params[:message] || :login_failed, default: :login_failed)
            end
        end

        def institutional?
            # As far as I know, this first check will always be true when the
            # second check is true, so we could just use at_institutional_subdomain?,
            # but just to be save, I'm checking both
            OMNIAUTH_SAML_PROVIDERS.include?(params['provider']) || at_institutional_subdomain?
        end

        def assign_provider_attrs(user, auth_hash)

            # if users have changed their info in our system, we want
            # to keep that.  (This is especially true for email)
            return unless user.new_record?

            id = omniauth_params['id']
            if id && User.exists?(id: id)
                # if a user logs out and then creates a new user, they will still have
                # the id for their actual user stored in local storage as an auid.  We
                # should ignore that id and create a new user with a new id.  This would
                # be a little cleaner if we stored the auid and the last_logged_in_user_id
                # separately in local storage, but not worth the effort right now.
                omniauth_params.delete('id')
            else
                user.id = id
            end

            auth_info = auth_hash['info']
            if auth_info['name']
                name = auth_info['name']

            # At least some SAML integrations (OneLogin) do not support
            # a full name parameter.  It's best if we can just get "name", but
            # this will allow us to support first_name and last_name as well
            elsif auth_info['first_name'] && auth_info['last_name']
                name = "#{auth_info['first_name']} #{auth_info['last_name']}"

            # fall back to email if necessary (not sure when it would be)
            else
                name = auth_info['email']
            end

            user.assign_attributes({
                name:     name,
                email:    auth_hash['info']['email']
            })


        end

        # FIXME: the naming in devise_token_auth is bad.  The url you go to after logging in
        # is called auth_origin_url, which implies that it is ALWAYS the same one you came from.
        # Here, we are sending you to a different url.  This works fine, but it's odd to call it
        # auth_origin_url.  Seems like there should be a `redirect_url`, and if that is nil
        # then maybe you fall back to the auth_origin_url
        #
        # In addition, it should be possible to send failure and success to different locations.
        #
        # This became important when we switched to having the sign up form on a marketing page,
        # because it looks odd to get forwarded to that page after success and then have the front-royal
        # app take you somewhere else instead.
        def auth_origin_url
            if !defined?(@auth_origin_url)

                # If using IDP-initiated login (i.e. when a user clicks on a Smartly
                # link from their JLL portal), then there will not be an auth_origin_url
                # provided (the request did not originate from smartly).
                # We have to generate a url based on the user's provider,
                # depending on whether or not we are using subdomains (in prod mode)
                # or not (in dev mode)
                if @auth_origin_url.nil? && @resource && @resource.using_saml_authentication?
                    @auth_origin_url = saml_redirect_url
                elsif action_name == 'omniauth_success'
                    path = @resource&.deactivated ? deactivated_redirect_path : success_redirect_path

                    # on success, we always send you to /home
                    if Rails.env.development? && request.port == 3000 # assume you have the full stack running and redirect to 3001
                        @auth_origin_url = "#{request_protocol}#{request.host}:3001/#{path}"
                    else
                        @auth_origin_url = "#{request_protocol}#{request.host_with_port}/#{path}"
                    end

                    # Ensure this redirect url is whitelisted for DTA consumption!
                    # see also: https://github.com/lynndylanhurley/devise_token_auth/pull/699
                    DeviseTokenAuth.redirect_whitelist << @auth_origin_url unless DeviseTokenAuth::Url.whitelisted?(@auth_origin_url)

                else
                    # on failure, we send you back from whence you came, without explicitly whitelisting (since it could be Open Redirect)
                    @auth_origin_url = super
                end

            end

            @auth_origin_url
        end

        def saml_redirect_url
            # If using IDP-initiated login (i.e. when a user clicks on a Smartly
            # link from their JLL portal), then there will not be an auth_origin_url
            # provided (the request did not originate from smartly).
            # We have to generate a url based on the user's provider,
            # depending on whether or not we are using subdomains (in prod mode)
            # or not (in dev mode)
            if @auth_origin_url.nil? && @resource && @resource.using_saml_authentication?
                if Rails.application.config.use_saml_subdomains
                    subdomain = "#{@resource.provider}."
                else
                    subdomain = ""
                    params['subdomain'] = @resource.provider
                end
            end

            host = request.host_with_port.starts_with?(subdomain) ? request.host_with_port : "#{subdomain}#{request.host_with_port}"
            "#{request_protocol}#{host}/#{success_redirect_path}"
        end

        def success_redirect_path
            "home"
        end

        def deactivated_redirect_path
            "sign-in"
        end

        # In failure cases, and in IDP-initiated logins (i.e. when a user clicks on a Smartly
        # link from their JLL portal), there will be no resource class provided in the
        # parameters.  For us it's always User, so just use that
        def resource_class(mapping = nil)
            User
        end

    end
end
