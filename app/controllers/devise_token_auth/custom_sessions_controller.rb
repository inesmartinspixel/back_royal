# FIXME: Drop `phone_no_password` support. Will assist in simplifying code for devise_token_auth updates. See also: User / front-end

module DeviseTokenAuth
    class CustomSessionsController < DeviseTokenAuth::SessionsController
        attr_accessor :provider

        def render_create_error_provider_suggestion(provider)
          render json: {
            success: false,
            errors: [ I18n.t(:please_try_logging_in_with, { provider: I18n.provider(provider) }) ]
          }, status: 401
        end

        def render_account_does_not_exist
          render json: {
            success: false,
            errors: [ I18n.t(:that_account_does_not_exist) ]
          }, status: 401
        end

        def create

            # NB: CUSTOM MOD TO ALLOW PASSING IN phone_no_password PROVIDER
            @provider = resource_params['provider'] || 'email'
            if provider == 'phone_no_password'
                field = 'phone'
            else
                field = (resource_params.keys.map(&:to_sym) & resource_class.authentication_keys).first
            end

            @resource = nil
            if field
                q_value = get_case_insensitive_field_from_resource_params(field)
                @resource = find_resource(field, q_value)
            end

            return render_deactivated_error if @resource&.deactivated

            custom_invite_handling
            valid_password = custom_password_handling

            if @resource and valid_params?(field, q_value) and valid_password and (!@resource.respond_to?(:active_for_authentication?) or @resource.active_for_authentication?)

                # Don't allow hiring managers to access the site through Cordova app
                if cordova_client? && @resource&.hiring_application
                    return render_hiring_web_only_error
                end

                # Don't allow non-miyamiya users to login on miyamiya app
                if params['client'] == 'miya_miya' && !@resource.institutions.include?(Institution.miya_miya)
                    return render_account_does_not_exist
                end

                begin
                    @token = @resource.create_token
                    @resource.save!

                    sign_in(:user, @resource, store: false, bypass: false)

                    yield @resource if block_given?

                    render_create_success
                rescue ActiveRecord::RecordInvalid => err
                    # We want to be informed of when this happens because it's most likely our fault that the user is invalid
                    # and we should fix the user's account as soon as possible.
                    Raven.capture_exception("Unsuccessful login attempt due to invalid user", {
                        level: 'warning',
                        extra: {
                            reason: err.message,
                            time: Time.now.to_s,
                            user_id: @resource.id
                        },
                        fingerprint: [err.message]
                    })
                    render_error_response(I18n.t(:login_failed_for_invalid_user), :not_acceptable)
                end

            elsif @resource and not (!@resource.respond_to?(:active_for_authentication?) or @resource.active_for_authentication?)

                render_create_error_not_confirmed

            else
                existing_resource = @resource && @resource.email && resource_class.where(email: @resource.email.downcase).first

                # If the email exists when the auth fails and the email was registered
                # with a provider then let them know that maybe they need to log in with
                # their provider.
                if existing_resource && (['facebook', 'google_oauth2'].include?(existing_resource.provider) || existing_resource.provider.start_with?('apple'))
                    render_create_error_provider_suggestion(existing_resource.provider)
                else
                    render_create_error_bad_credentials
                end
            end
        end

        def destroy


            # remove auth instance variables so that after_action does not run
            user = remove_instance_variable(:@resource) if @resource
            client = @token.client if @token.client
            @token.clear!

            if user && client && user.tokens[client]
                begin
                    user.tokens.delete(client)
                    user.save!

                    yield user if block_given?

                    render_destroy_success
                rescue ActiveRecord::RecordInvalid => err
                    # We want to be informed of when this happens because it's most likely our fault that the user is invalid
                    # and we should fix the user's account as soon as possible.
                    Raven.capture_exception("Unsuccessful logout attempt due to invalid user", {
                        level: 'warning',
                        extra: {
                            reason: err.message,
                            time: Time.now.to_s,
                            user_id: user.id
                        }
                    })
                    raise ActiveRecord::RecordInvalid.new(user)
                end
            else
                render_destroy_error
            end
        end

        # NB: CUSTOM MOD TO ALLOW HAVING NO password if provider is phone_no_password
        def valid_params?(field, q_value)
            if provider == 'phone_no_password'
                !q_value.nil?
            else
                super
            end
        end

        # override method from ResourceFinder
        def find_resource(field, q_value)
            if provider == 'phone_no_password'
                parsed_q_value = Phonelib.parse(q_value)&.e164
                q_value = parsed_q_value.present? ? parsed_q_value : q_value
                q = "#{field.to_s} = ? AND provider='phone_no_password'" # NB: CUSTOM MOD TO ALLOW phone_no_password provider
            else
                q = "email = ?" # BD: CUSTOM MOD TO ALLOW EMAIL SUPREMECY
            end

            if ActiveRecord::Base.connection.adapter_name.downcase.starts_with? 'mysql'
                q = "BINARY " + q
            end

            @resource = resource_class.where(q, q_value).first
        end

        def custom_password_handling
            # NB: CUSTOM MOD TO ALLOW PASSING IN phone_no_password provider
            valid_password = (provider == 'phone_no_password' ? true : @resource && @resource.valid_password?(resource_params[:password]))

            # in dev-mode, allow logging in as any user with the password 'password'
            if Rails.env.development? && ['password', 'testest'].include?(resource_params[:password])
                valid_password = true
            end

            valid_password
        end

        def custom_invite_handling
            # NB: CUSTOM MOD TO ALLOW PASSING IN hiring_invite provider
            # the first time an invitee logs in, set their password
            # and convert them to a regular old email user.
            # see HiringTeamInvite.invite! for more info
            if @resource && @resource.provider == 'hiring_team_invite'
                @resource.set_password(resource_params[:password])
                @resource.provider = 'email'
                @resource.save!
            end
        end

    end
end