module DeviseTokenAuth
    class CustomPasswordsController < DeviseTokenAuth::PasswordsController

        rescue_from Exception, :with => :render_error_exception

        # this action is responsible for generating password reset tokens and sending emails
        def create
          return render_create_error_missing_email unless resource_params[:email]

          @email = get_case_insensitive_field_from_resource_params(:email)
          @resource = find_resource(:uid, @email)

          if @resource
            yield @resource if block_given?

            # BD: Big departure from standard DTA flow here.
            # We first calculate the final URL via the Devise UrlHelpers method
            # that is normally used in mailer templates.
            # Then, if the @resource is valid, we send a password reset request
            # via Customer.io event-triggered campaign.
            token = @resource.send(:set_reset_password_token)
            if @resource.errors.empty?

              url = edit_password_url(
                @resource,
                reset_password_token: token,
                config: params[:config_name].to_s,
                redirect_url: @redirect_url
              )

              # log this event to customer_io only (default)
              Event.create_server_event!(
                SecureRandom.uuid,
                @resource.id,
                'user:reset_password_request',
                {
                  url: url
                }
              ).log_to_external_systems

              return render_create_success
            else
              render_create_error @resource.errors
            end
          else
            render_not_found_error
          end
        end

        # BD: see `create` modifications above
        def edit_password_url(resource, *args)
          scope = Devise::Mapping.find_scope!(resource)
          _devise_route_context.send("edit_#{scope}_password_url", *args)
        end

        # overriding default meth in ResourceFinder
        def find_resource(*args)
          # q = "uid = ? AND provider='email'"
          q = "email = ?" # BD: CUSTOM MOD TO ALLOW EMAIL SUPREMECY

          # fix for mysql default case insensitivity
          if ActiveRecord::Base.connection.adapter_name.downcase.starts_with? 'mysql'
            # q = "BINARY uid = ? AND provider='email'"
            q = "BINARY email = ?" # BD: CUSTOM MOD TO ALLOW EMAIL SUPREMECY
          end

          @resource = resource_class.where(q, @email).first
        end

        def update
          # make sure user is authorized
          if require_client_password_reset_token? && resource_params[:reset_password_token]
            @resource = resource_class.with_reset_password_token(resource_params[:reset_password_token])
            return render_update_error_unauthorized unless @resource

            @token = @resource.create_token
          else
            @resource = set_user_by_token
          end

          return render_update_error_unauthorized unless @resource

          # # make sure account doesn't use oauth2 provider
          # # BD: CUSTOM MOD TO ALLOW EMAIL SUPREMECY
          # unless @resource.provider == 'email'
          #   return render_update_error_password_not_required
          # end

          # ensure that password params were sent
          unless password_resource_params[:password] && password_resource_params[:password_confirmation]
            return render_update_error_missing_password
          end

          if @resource.send(resource_update_method, password_resource_params)
            @resource.allow_password_change = false if recoverable_enabled?
            @resource.save!

            yield @resource if block_given?
            return render_update_success
          else
            return render_update_error
          end
        end

        # override create so we can catch the case where the user was created via oauth and
        # provide a better error message
        def render(opts)
            begin
                if @resource.nil?
                    # check whether this user was created via another provider
                    if resource_class.case_insensitive_keys.include?(:email)
                        email = resource_params[:email].downcase
                    else
                        email = resource_params[:email]
                    end
                    @resource = resource_class.where(email: email).first
                    if @resource && @resource.provider

                        errors = [
                            I18n.t(:please_try_logging_in_with, {
                                provider: I18n.provider(@resource.provider)
                            })
                        ]
                        opts[:json][:errors] = errors.as_json
                    end

                end
            rescue Exception => e
                # no need to save errors; the validations will run again in super and return the correct json format
            end

            super(opts)
        end

        # devise_token_auth has a `render_error` method which handles known failures in its flow
        # whereas, we want to insert additional error handling for our own purposes
        def render_error_exception(exception)
            if exception.is_a?(ActionController::RoutingError)
                error_explanation = "The password reset link you used is no longer valid."
            else
                error_explanation = "Something went wrong."
                Raven.capture_exception(exception)
            end

            resoluton_suggestion = "Please <a href='/forgot-password'>try again</a>."


            render inline: %Q|

<html>
    <head></head>
    <body>
        #{error_explanation}  #{resoluton_suggestion}
    </body>
</html>|

        end

    end
end
