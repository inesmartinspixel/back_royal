class SitemapController < ApplicationController

    def sitemap

        # It's kind of silly that we regenerate sitemaps whenever someone requests them,
        # but it's not very intensive to generate, and they will be cached by cloudflare
        # anyway, so this endpoint should be very rarely hit in practice.
        #
        # It's also silly that we generate the sitemaps for both domains when we're only 
        # going to return one of them, but SitemapGenerator doesn't seem to provide an 
        # easy way to be dynamic about this.
        SitemapGenerator::Interpreter.run

        if !Rails.env.production?
            domain = request.params[:force_host]
            if !domain
                domain = AppConfig.quantic_domain
            end
        else  
            domain = request.domain
        end

        file = open(Rails.root.join(SitemapHelper.main_sitemap_path(domain)))

        send_data(
            file.read,
            :filename => "sitemap.xml.gz",
            :type => 'application/gzip',
            :disposition => "attachment"
        )
    end
end
