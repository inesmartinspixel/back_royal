class UnsubscribeController < ApplicationController

    def index

        # http://localhost:3000/unsubscribe?id=0cfb65dd-d9ec-4eca-90a1-be7aadd7ee89&email=brent%40pedago.com
        # - or -
        # http://localhost:3000/unsubscribe?type=notify_hiring_content&id=0cfb65dd-d9ec-4eca-90a1-be7aadd7ee89&email=brent%40pedago.com

        # translate legacy 'newsletter' parameter
        # this can be removed once all customer.io templates are updated with the new param
        if params[:type] == 'newsletter'
            params[:type] = 'notify_email_newsletter'
        end


        # email and ID must match an existing user
        user = User.find_by(:id => params[:id], :email => params[:email])
        if !user
            redirect_to "/"
            return
        end

        # updating each / all notification type accordingly
        User::NOTIFICATIONS.each do |key, value|
            orig_value =  user.send(key)
            if key == 'notify_candidate_positions_recommended'
                user.send("#{key}=", should_unsubscribe(key) ? 'never' : orig_value)
            else
                # We cast to boolean in case of a nilable preference, such as notify_candidate_positions. We
                # assume that if the preference hasn't been set yet, by either the user or logic in our system,
                # that we should still go ahead and set to false.
                user.send("#{key}=", !!orig_value && !should_unsubscribe(key))
            end
        end
        user.save!

        @type_pretty = User::NOTIFICATIONS[sanitized_type] || "ALL"
    end

    def sanitized_type
        params[:type] && User::NOTIFICATIONS.has_key?(params[:type]) ? params[:type] : nil
    end

    def should_unsubscribe(type)
        # default to all notification types unless otherwise specified
        sanitized_type.blank? || sanitized_type == type
    end

end
