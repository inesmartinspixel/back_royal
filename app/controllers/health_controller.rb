class HealthController < ApplicationController

    def index

        # Cache Health Check
        SafeCache.fetch("health_controller/status") do
            'OK'
        end

        # DB Health Check
        ActiveRecord::Base.connection.execute('select 1')

        respond_to do |format|
            format.json { render json: {}, status: 200 }
            format.html { render html: '', status: 200 }
        end

    end

end