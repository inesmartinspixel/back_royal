class IdologyController < ApplicationController
    before_action :set_action_metadata
    layout "idology"

    # idology will forward users to this route after id verification (docs say
    # it will show in an iframe on their site, but that's not what I've seen)
    def success
        render "success"
    end

    # idology will forward users to this route after id verification (docs say
    # it will show in an iframe on their site, but that's not what I've seen)
    def failure
        render "failure"
    end

    # users will come to this page, where we will show an iframe with the idology
    # site inside of it if the idology_verification is still usable
    def scan
        @idology_verification = IdologyVerification.find(params[:id])
        @scan_capture_url = @idology_verification.scan_capture_url
        @body_class = 'scan'

        Event.create_server_event!(
            SecureRandom.uuid,
            @idology_verification.user_id,
            "viewed_idology_scan_page",
            {
                idology_verification_id: @idology_verification.id,
                verified: @idology_verification.verified
            }
        ).log_to_external_systems

        render "scan"
    end

end