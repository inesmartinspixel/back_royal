class BlueOceanStrategyController < ApplicationController

    before_action :set_action_metadata

    # this version of BOS allows high school students to join
    def high_school
        render "highschool"
    end

end
