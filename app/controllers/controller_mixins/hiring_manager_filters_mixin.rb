module ControllerMixins::HiringManagerFiltersMixin

    def process_hiring_manager_filters(filters, query = nil)
        if filters && filters.respond_to?(:with_indifferent_access)
            filters = filters.with_indifferent_access
        elsif filters.nil?
            filters = {}
        end

        query = CareerProfile if query.nil?

        if filters.key?('user_id')
            query = filter_career_profiles_by_user_id(filters, query)
        end

        if filters['industries']&.any?
            query = filter_career_profiles_by_industry(filters, query)
        end

        if filters['roles'].present?
            query = filter_career_profiles_by_roles(filters, query)
        end

        if filters.key?('keyword_search') && filters['keyword_search'].present?
            query = filter_career_profiles_by_keyword_search(filters, query)
        end

        if filters.key?('years_experience') && filters['years_experience'].any?
            query = filter_career_profiles_by_years_experience(filters, query)
        end

        if filters.key?('skills') && filters['skills'].any?
            query = filter_career_profiles_by_skills(filters, query)
        end

        if filters.key?('company_name')
            query = filter_career_profiles_by_company_name(filters, query)
        end

        if filters.key?('school_name')
            query = filter_career_profiles_by_school_name(filters, query)
        end

        if filters.key?('employment_types_of_interest') && filters['employment_types_of_interest'].any?
            query = filter_career_profiles_by_employment_types_of_interest(filters, query)
        end

        if filters.key?('levels_of_interest') && filters['levels_of_interest'].any?
            query = filter_career_profiles_by_interested_in_joining_new_company(filters, query)
        end

        if [true, false].include?(filters['in_school']) # nil means to skip this filter
            query = filter_career_profiles_by_in_school(filters, query)
        end

        query
    end

    def join_search_helpers(query)
        if !@joined_search_helpers
            # Doing an outer join here because career_profile_search_helpers_2 will not inculde
            # any career profile that was never updated
            # after being created (created_at = updated_at).  We ignore those so that the materialized view
            # can write 1/3 fewer records.  In practice, it might be fine
            # to use an inner join and filter those out here, but it
            # breaks specs where we use unrealistic combinations of filters and ordering rules. (i.e. ordering
            # by BEST_SEARCH_MATCH_FOR_HM without also filtering for active profiles)
            # query = query.joins(:career_profile_search_helper)
            query = query.left_outer_joins(:career_profile_search_helper)
        end
        query
    end

    def filter_career_profiles_by_user_id(filters, query)
        query = query.where(user_id: filters['user_id'])
    end

    def filter_career_profiles_by_roles(filters, query)
        query = join_search_helpers(query)
        query = query.where("primary_areas_of_interest && ARRAY[:primary_areas_of_interest] OR work_experience_roles && ARRAY[:work_experience_roles]",
            {
                :primary_areas_of_interest => filters['roles']['primary_areas_of_interest'],
                :work_experience_roles => filters['roles']['work_experience_roles']
            }
        )
    end

    def deprecated_filter_career_profiles_by_primary_areas_of_interest(filters, query)

        query = join_search_helpers(query)
        query = query.where("primary_areas_of_interest && ARRAY[:roles] OR work_experience_roles && ARRAY[:roles]",
            {
                :roles => filters['primary_areas_of_interest']
            }
        )
    end

    def filter_career_profiles_by_industry(filters, query)

        query = join_search_helpers(query)
        query = query.where("job_sectors_of_interest && ARRAY[:industries] OR work_experience_industries && ARRAY[:industries]",
            {
                :industries => filters['industries']
            }
        )
    end

    def filter_career_profiles_by_keyword_search(filters, query)
        vector_search = CareerProfile.get_vector_search_sql(filters['keyword_search']) # string
        query = query.where("career_profiles.id IN (SELECT career_profile_id FROM career_profile_fulltext WHERE keyword_vector @@ to_tsquery('english', #{vector_search}))")
        query
    end

    def filter_career_profiles_by_years_experience(filters, query)

        query = join_search_helpers(query)

        # I don't think ActiveRecord's or support is flexible enough to let us
        # do this as a simple "or", so we build up a bunch of clauses in a hackish way instead
        years_experience_clauses = []

        filters['years_experience'].each do |option|

            # we need to be careful of sql injection here, but I think that
            # I have restricted this such that only integers that get sent up
            # could be included in our sql
            parts = option.match(/(\d+)_([^_]+)/).to_a.slice(1, 2)
            min = begin

                # subtract -1 just be a little bit greedy
                # so we don't miss someone because of a leap day or something silly
                Integer(parts[0]) - 0.1
            rescue ArgumentError
            end

            max = begin
                # add +1 just to be be little bit greedy
                # so we don't miss someone because of a leap day or something silly
                Integer(parts[1]) + 0.1
            rescue ArgumentError
            end

            if min.present? && parts[1] == "plus"
                range = min..Float::INFINITY
            end

            if min.present? && max.present?
                range = min..max
            end

            if range

                min_reference_date = range.max == Float::INFINITY ? Time.now - 200.years : Time.now - (range.max) * 1.year

                # if we are looking for people with 0 years of experience, we should
                # include someone who has entered one work experience with a start date
                # in the future.  That's why we have the `Time.now + 200.years`
                max_reference_date = range.min <= 0 ? Time.now + 200.years : Time.now - (range.min) * 1.year

                or_clause = [
                    # for people without a current work experience, we rely on years_of_experience
                    CareerProfile.joins(:career_profile_search_helper).where({career_profile_search_helpers_2: {years_of_experience: range}}).to_sql.split('WHERE')[1],

                    # for people with a current work experience, we rely on years_of_experience_reference_date, which
                    # accounts for the fact that they get more experience with each day that passes
                    CareerProfile.joins(:career_profile_search_helper).where("career_profile_search_helpers_2.years_of_experience_reference_date between ? and ?", min_reference_date, max_reference_date).to_sql.split('WHERE')[1]
                ].join(" OR ")
                years_experience_clauses << or_clause
            end
        end

        query.where(years_experience_clauses.join(" OR "))
    end

    def filter_career_profiles_by_skills(filters, query)
        query = join_search_helpers(query)
        vector_search = CareerProfile.get_vector_search_sql(filters['skills'].join(' '), '|') # string[]

        query = query.where("skills_texts && ARRAY[:skills_texts] OR skills_vector @@ to_tsquery('english', #{vector_search})",
            {
                :skills_texts => filters['skills']
            }
        )
        query
    end

    def filter_career_profiles_by_company_name(filters, query)
        query = join_search_helpers(query)

        # this will use the gin index on work_experience_company_names
        quoted_company_name = ActiveRecord::Base.connection.quote("%#{filters['company_name']}%")
        query = query.where("work_experience_company_names ilike #{quoted_company_name}")
    end

    def filter_career_profiles_by_school_name(filters, query)
        query = join_search_helpers(query)

        # this will use the gin index on education_experience_school_names
        quoted_school_name = ActiveRecord::Base.connection.quote("%#{filters['school_name']}%")
        query = query.where("education_experience_school_names ilike #{quoted_school_name}")
    end

    def filter_career_profiles_by_employment_types_of_interest(filters, query)
        query = query.where("employment_types_of_interest && #{array_to_sql(filters['employment_types_of_interest'])}")
    end

    def filter_career_profiles_by_interested_in_joining_new_company(filters, query)
        query = query.where("interested_in_joining_new_company = ANY(#{array_to_sql(filters['levels_of_interest'])})")
    end

    def filter_career_profiles_by_in_school(filters, query)
        query = join_search_helpers(query)
        query = query.where(career_profile_search_helpers_2: {in_school: filters['in_school']})
    end

    def array_to_sql(arr)
        if arr.blank?
            "Array[]::text[]"
        else

            # ActiveRecord::Base.connection.quote( should help prevent against
            # sql injection attacks
            quoted = arr.map { |e| ActiveRecord::Base.connection.quote(e) }
            "Array[#{quoted.join(',')}]"
        end
    end

end