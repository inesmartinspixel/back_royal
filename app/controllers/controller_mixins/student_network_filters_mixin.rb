module ControllerMixins::StudentNetworkFiltersMixin

    def process_student_network_filters(filters = {}, query = nil)

        filters = filters || {}
        filters = filters.with_indifferent_access if filters.respond_to?(:with_indifferent_access) # in specs, Hash responds to with_indifferent_access.  In the wild this is ActionController::Parameters

        query = CareerProfile if query.nil?

        # basic filters to exclude users not in the student network
        query = filter_for_access_to_network(filters, query)

        query = filter_for_isolated_network_cohorts(query)

        if filters.key?('within_geometry')
            query = filter_student_network_within_geometry(filters, query)
        end

        if filters.key?('cohort_id')
            query = filter_student_network_for_cohort_id(filters, query)
        end

        if filters.key?('program_type')
            query = filter_student_network_for_program_type(filters, query)
        end

        if filters['active_in_last'].present?
            query = filter_student_network_for_active_in_last(filters, query)
        end

        if filters['student_network_looking_for']&.any?
            query = filter_student_network_for_student_network_looking_for(filters, query)
        end

        if filters['student_network_interests']&.any?
            query = filter_student_network_for_student_network_interests(filters, query)
        end

        if filters.key?('alumni')
            query = filter_student_network_for_alumni(filters, query)
        end

        if filters.key?('industries')
            query = filter_student_network_for_industries(filters, query)
        end

        if filters.key?('keyword_search') && filters['keyword_search'].present?
            query = filter_student_network_by_keyword_search(filters, query)
        end

        # NOTE: this filter is only used in the student_network_maps_controller right now,
        # although its inclusion in this file means it *could* be used by the career_profiles_controller
        if filters.key?('viewports')
            query = filter_student_network_for_viewports(filters, query)
        end

        query
    end

    # NOTE: This should be in accordance with CareerProfile#student_network_active?. Any changes
    # here will need to be reflected there
    def filter_for_access_to_network(filters, query)
        query = filter_for_non_hidden_network_inclusion(query)
        query = filter_for_has_location(query)
        query
    end

    def filter_for_non_hidden_network_inclusion(query)
        query = query.joins(:student_network_inclusion)
            .where.not(student_network_inclusions: {pref_student_network_privacy: 'hidden'})
    end

    def filter_for_has_location(query)
        query = query.where.not(location: nil)
    end

    def filter_for_isolated_network_cohorts(query)

        # Users in isolated cohorts should not see users outside of their own cohort
        if current_user&.in_isolated_network_cohort?
            query = query.joins(:student_network_inclusion).where(student_network_inclusions: {cohort_id: current_user.relevant_cohort.attributes['id']})

        # Users in regular cohorts should not see users in isolated cohorts
        else
            # NOTE: we explicitly do not use the published_cohorts view to make this query cheap.
            # We don't care if some unpublished cohort ids get included in this query, because there
            # shouldn't be any associated `student_network_inclusions` records. And even if they did,
            # an unpublished cohort with `isolated_network = TRUE` should be excluded from this query's
            # results anyway.
            query = query.joins(:student_network_inclusion).where.not(student_network_inclusions: {cohort_id: Cohort.where(isolated_network: true).pluck(:id)})
        end

        query
    end

    def filter_student_network_for_viewports(filters, query)
        viewports = filters['viewports'].map {|v| v&.split(',')&.map {|m| m.to_f } }

        widths = viewports.map {|v| (v[0] - v[2]).abs }
        heights = viewports.map {|v| (v[1] - v[1]).abs }

        width = widths.sum
        height = heights.max # viewports are always side-by-side

        corner_distance = ((width ** 2) + (height ** 2)) ** 0.5

        # We observed that large enough rectangles don't behave as expected in PostGIS. According to the docs,
        # the distance between points is computed via the shortest great circle for geography types. This seems
        # to manifest with rectangles wrapping around the back of the globe, e.g.: instead of a viewport
        # stretching from Alaska across the US and Europe to China, it would instead go from China to Alaska,
        # despite the "min" and "max" lat/lngs indicating it should seemingly do the larger sweep.
        #
        # We could do something like split up the viewport into separate, smaller viewports that stitch
        # together into the larger viewport, but since these situations usually mean we're not filtering many
        # users out anyway, we just disable this filtering when the corner-to-corner distance of the rectangle
        # is large enough.
        if corner_distance < 100
            query = Location.within_viewports(query, 'career_profiles.location', viewports, '::geometry')
        end

        query
    end

    def filter_student_network_within_geometry(filters, query)
        query = query.where(Location.within_geometry_sql_string('location', filters[:within_geometry]))
    end

    def filter_student_network_for_cohort_id(filters, query)
        query = (query).joins(:student_network_inclusion).where(student_network_inclusions: {cohort_id: filters[:cohort_id]})
    end

    def filter_student_network_for_program_type(filters, query)
        query = (query).joins(:student_network_inclusion).where(student_network_inclusions: {program_type: filters[:program_type]})
    end

    def filter_student_network_for_active_in_last(filters, query)
        # we don't have to worry that the query might already have joined
        # against user.  ActiveRecord is smart enough to skip it if so.
        query = query.joins(:user).where("users.last_seen_at > ? ", Time.now - filters['active_in_last'])
    end

    def filter_student_network_for_student_network_looking_for(filters, query)
        query = query.where("student_network_looking_for && ARRAY[:student_network_looking_for]",
            {
                :student_network_looking_for => filters['student_network_looking_for']
            }
        )
    end

    def filter_student_network_for_student_network_interests(filters, query)
        query = join_search_helpers(query)
        vector_search = CareerProfile.get_vector_search_sql(filters['student_network_interests'].join(' '), '|') # string[]
        query = query.where("student_network_interests_texts && ARRAY[:student_network_interests] OR student_network_interests_vector @@ to_tsquery('english', #{vector_search})",
            {
                :student_network_interests => filters['student_network_interests']
            }
        )
        query
    end

    def filter_student_network_for_alumni(filters, query)
        query = query.joins(:student_network_inclusion)

        if filters['alumni'] == true
            # this will only be graduated and honors people, since `failed` people are not
            # included in student_network_inclusions
            query = query.where("student_network_inclusions.graduation_status != 'pending'")
        elsif filters['alumni'] == false
            query = query.where("student_network_inclusions.graduation_status = 'pending'")
        end
    end

    def filter_student_network_for_industries(filters, query)
        # this is different from filter_student_network_by_industry in hiring_manager_filters_mixin, since it only looks
        # at experience, ignoring job_sectors_of_interest
        query = join_search_helpers(query)
        query = query.where("work_experience_industries && ARRAY[:industries]",
            {
                :industries => filters['industries']
            }
        )
    end

    def join_search_helpers(query)
        if !@joined_search_helpers
            # See note in hiring_manager_filters_mixin about using a join vs. an outer join here.
            # Basically, the join filters out any profiles that were created automatically when a user
            # registered and then never updated again
            query = query.joins(:career_profile_search_helper)
        end
        query
    end

    def filter_student_network_by_keyword_search(filters, query)
        vector_search = CareerProfile.get_vector_search_sql(filters['keyword_search']) # string

        # Which field gets searched is based entirely on who is doing the searching.  Users in the
        # network who have set their preferences to 'anonymous' will only have anonymous information
        # in BOTH fields.
        field = current_user&.has_full_student_network_access? ? 'student_network_full_keyword_vector' : 'student_network_anonymous_keyword_vector'

        query = query.where("career_profiles.id IN (SELECT career_profile_id FROM career_profile_fulltext WHERE #{field} @@ to_tsquery('english', #{vector_search}))")
        query
    end

    def apply_anonymization_ordering(query)
        query = query.joins(:student_network_inclusion).order(Arel.sql("student_network_inclusions.pref_student_network_privacy != 'full'"))
    end

    def apply_default_student_network_ordering(query)
        query = query.joins(:user)
                    .order(Arel.sql("users.name ASC"))
                    .order(Arel.sql("users.id ASC"))
    end
end