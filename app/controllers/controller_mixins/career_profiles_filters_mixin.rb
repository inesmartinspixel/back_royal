module ControllerMixins::CareerProfilesFiltersMixin

    def process_career_profiles_filters(filters, query = nil)
        if filters && filters.respond_to?(:with_indifferent_access)
            filters = filters.with_indifferent_access
        elsif filters.nil?
            filters = {}
        end

        query = CareerProfile if query.nil?

        if filters.key?('user_id')
            query = filter_career_profiles_by_user_id(filters, query)
        end

        if filters.key?('places') && filters['places'].any?
            query = filter_career_profiles_by_places(filters, query)
        end

        query
    end

    def location_keys_close_to_requested_places(filters)
        # There is a certain hard-coded list of location_keys that users can mark as locations of interest.  Find any
        # of those that are close to the locations passed up in the filter, and show users who are willing to move to
        # those places
        filters['places'].map { |place_details| Location.location_keys_close_to(place_details) }.flatten.uniq
    end

    def filter_career_profiles_by_user_id(filters, query)
        query = query.where(user_id: filters['user_id'])
    end

    def filter_career_profiles_by_places(filters, query)
        # I don't think ActiveRecord's or support is flexible enough to let us
        # do this as a simple "or", so we build up a bunch of clauses in a hackish way instead
        location_clauses = []

        # first, add learners who live close to one of the provided locations
        filters['places'].each do |place_details|
            location_clauses << Location.at_location_sql_string(place_details)
        end

        # also, if only_local is not set, add learners who are willing to move to
        # one of the provided locations
        if !filters['only_local']
            # Be sure that we explicitly check for willing_to_relocate being true. We had an issue where
            # users who had "flexible" in their locations_of_interest while being willing_to_relocate=false were showing up
            # in the results. See https://trello.com/c/U6TQqTmq
            #
            location_keys = location_keys_close_to_requested_places(filters)
            location_clauses << CareerProfile.where("locations_of_interest && ARRAY[?]", location_keys + ['flexible'])
                                    .where(willing_to_relocate: true)
                                    .to_sql.split('WHERE')[1]
        else
            # I'm leaving the following comment and hack here for posterity.  In June 2019,
            # we realized that this hack was causing issues with some other queries (see https://trello.com/c/027YdUCM).
            # However, removing the hack at the time brought back the old bug described below.
            # We retested in October with v11.5 and it seemed like the hack was no longer needed for to make the original
            # query referenced below work.  So we're removing the hack and crossing our fingers.

            # This is nuts, but if postgres uses the locations gist index here in combination with other
            # conditions, it can hang indefinetely (as of 10/12/2017).  Adding in this OR forces it to NOT use the locations
            # index.  See https://trello.com/c/OpYUwfRu.
            #location_clauses << "locations_of_interest = '{}'" # always false
        end

        query = query.where(location_clauses.join(" OR "))

        query
    end

end