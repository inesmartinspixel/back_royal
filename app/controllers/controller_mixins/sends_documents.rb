module ControllerMixins::SendsDocuments

    def send_document(klass, event_type)

        return render_unauthorized_error unless current_ability.can?(:download, klass)

        id = params[:id]
        render_error_if_not_found(klass, id) do |record|
            file = open(record.file.expiring_url(20))

            Event.create_server_event!(
                SecureRandom.uuid,
                current_user_id,
                event_type,
                {
                    label: record.id
                }
            ).log_to_external_systems

            send_data(
                file.read,
                :filename => record.file_file_name,
                :type => record.file_content_type,
                :disposition => "attachment"
            )
        end
    end

end