module ControllerMixins::HasStartAndEndTimes

    def self.included(target)
        target.before_action :setup_start_and_end_times
    end

    def permitted_time_params
        params.permit("start_date", "end_date") if (!params.blank?)
    end

    def setup_start_and_end_times
        @time_zone = 'Eastern Time (US & Canada)'

        @start_date_str = permitted_index_params["start_date"] if !permitted_index_params["start_date"].blank?
        @end_date_str = permitted_index_params["end_date"] if !permitted_index_params["end_date"].blank?

        if @start_date_str.nil?
            @start_date_in_time_zone = default_start_date.in_time_zone(@time_zone).end_of_day # want 0 initial results
            @start_date_str = @start_date_in_time_zone.strftime("%Y-%m-%d")
            @start_date = @start_date_in_time_zone.utc
        else
            @start_date_in_time_zone = ActiveSupport::TimeZone[@time_zone].parse(@start_date_str).beginning_of_day
            @start_date = @start_date_in_time_zone.utc
        end

        if @end_date_str.nil?
            @end_date_in_time_zone = Time.now.in_time_zone(@time_zone).end_of_day
            @end_date_str = @end_date_in_time_zone.strftime("%Y-%m-%d")
            @end_date = @end_date_in_time_zone.utc
        else
            @end_date_in_time_zone = ActiveSupport::TimeZone[@time_zone].parse(@end_date_str).end_of_day
            @end_date = @end_date_in_time_zone.utc
        end
    end

    def default_start_date
        Date.today
    end

end
