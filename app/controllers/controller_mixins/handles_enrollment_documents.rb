module ControllerMixins::HandlesEnrollmentDocuments
    include ActionView::Helpers::TextHelper

    def create_upload_timeline_event(asset, label, current_user)
        ActivityTimeline::PersistedTimelineEvent.create_for_user!(asset.user, {
            time: asset.created_at.to_timestamp,
            event: 'performed_action',
            labels: [label, 'upload'],
            text: "Uploaded #{truncate(asset.file_file_name, length: 25, seperator: '...')}",
            editor_id: current_user.id,
            editor_name: current_user.name,
            category: 'enrollment'
        })
    end

    def create_deletion_timeline_event(asset, label, current_user)
        ActivityTimeline::PersistedTimelineEvent.create_for_user!(asset.user, {
            time: Time.now.to_timestamp,
            event: 'performed_action',
            labels: [label, 'deletion'],
            text: "Deleted #{truncate(asset.file_file_name, length: 25, seperator: '...')}",
            editor_id: current_user.id,
            editor_name: current_user.name,
            category: 'enrollment'
        })
    end
end
