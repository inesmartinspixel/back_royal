module ControllerMixins::StudentNetworkEventsFiltersMixin

    def apply_student_network_event_filters(filters = {})
        query = StudentNetworkEvent
        query = filter_student_network_events_by_user_access(query)
        query = filter_student_network_events_by_id(filters, query)
        query = filter_student_network_events_by_published(filters, query)
        query = filter_student_network_events_by_date(filters, query)
        query = filter_student_network_events_by_keywords(filters, query)
        query = filter_student_network_events_by_location(filters, query)
        query = filter_student_network_events_by_event_type(filters, query)
        query
    end

    def filter_student_network_events_by_user_access(query)
        wheres = []


        # Admins and interviewers see everything
        if current_user.is_admin_or_interviewer?
            wheres << "id IS NOT NULL"
        else
            # Full access users who aren't admins receive event visibility based on
            # their `student_network_inclusions` record.
            if current_user.has_full_student_network_events_access?
                %w(
                    visible_to_current_degree_students
                    visible_to_graduated_degree_students
                    visible_to_non_degree_users
                ).each do |column|
                    if current_user.student_network_inclusion.send(:"provides_access_to_student_network_events_#{column}?")
                        wheres << "#{column} = TRUE"
                    end
                end

                # `visible_to_accepted_degree_students_in_cohorts` is a list, unlike the other
                # `visible_to_*` columns which are booleans.
                if current_user.student_network_inclusion.provides_access_to_student_network_events_visible_to_accepted_degree_students_in_cohorts?
                    wheres << "'#{current_user.student_network_inclusion.cohort_id}' = ANY(visible_to_accepted_degree_students_in_cohorts)"
                end

            # Anyone that isn't an admin_or_interviewer and doesn't explicitly have full access
            # is considered a limited access user. Limited access users experience an anonymized
            # version of what an accepted, not yet graduated, full access user experiences.
            else
                # They see (anonymized) events that current degree students see
                wheres << "visible_to_current_degree_students = TRUE"

                # And they see (anonymized) events we've marked as visible to non-students
                wheres << "visible_to_non_degree_users = TRUE"

                # And they see (anonymized) events earmarked for accepted degree students in specific cohorts.
                #
                # NOTE: in this case, we use the currently promoted EMBA cohort because a limited access user
                # may have not yet applied.
                wheres << "'#{Cohort.promoted_cohort('emba')['id']}' = ANY(visible_to_accepted_degree_students_in_cohorts)"
            end

            # We also want to make sure that old conference events are made visible to the user under certain conditions.
            # See https://trello.com/c/ueGVLnTF.
            #
            # NOTE: As a matter of policy, conference events are only for EMBA users. This logic relies on this assumption
            # specifically for limited access users. This assumption, coupled with the fact that limited access users are
            # shown events that are visible_to_accepted_degree_students_in_cohorts for the promoted EMBA cohort, is why
            # we don't bother checking the user's program type for limited access users.
            if !current_user.has_full_student_network_events_access? || current_user.student_network_inclusion.provides_access_to_old_conference_events?
                wheres << "(event_type = 'conference' AND end_time < '#{Time.now.utc - 1.week}')"
            end
        end

        query = query.where(wheres.join(" OR "))
        query
    end

    def filter_student_network_events_by_id(filters, query)
        if filters[:id]
            query = query.where(id: filters[:id])
        end

        query
    end

    def filter_student_network_events_by_published(filters, query)
        # Unless the request is for an admin user inside the editor,
        # we should never return unpublished events for display.
        supports_filter = filters[:editor] && current_user.is_admin?
        val = (filters[:published] && filters[:published].is_a?(Array)) ? filters[:published].first : filters[:published]
        val = ActiveModel::Type::Boolean.new.cast(val)
        if supports_filter && [true, false].include?(val)
            query = query.where(published: val)
        elsif !supports_filter
            query = query.where(published: true)
        end
        query
    end

    def filter_student_network_events_by_date(filters, query)

        if filters[:only_tbd]

            # only return indeterminate date events, ignoring any possible conflicting filters
            query = query.where(date_tbd: true)

        else

            # The Admin and Learner interfaces can contextually specify to include_tbd based on the intention
            # of the search. This will optionally include or hide indeterminate date events
            date_tbd_clause = filters[:include_tbd] ? "student_network_events.date_tbd = TRUE" : "FALSE"

            if filters[:start_time]
                query = query.where("(#{date_tbd_clause} OR student_network_events.start_time >= ?)", Time.at(filters[:start_time]).utc)
            end

            if filters[:end_time]
                query = query.where("(#{date_tbd_clause} OR student_network_events.end_time <= ?)", Time.at(filters[:end_time]).utc)
            end

            if !filters[:start_time] && !filters[:end_time]
                # If no start_time nor end_time was given, default to checking for events that
                # are either upcoming or currently happening, which essentially equates to checking
                # if the end_date is in the future.
                # See https://trello.com/c/n2VriEss
                query = query.where("(#{date_tbd_clause} OR student_network_events.end_time >= ?)", Time.now.utc)
            end

        end

        query
    end

    def filter_student_network_events_by_keywords(filters, query)
        # if an empty string is passed in, present? is false and
        # we do not filter here
        if filters[:keyword_search].present?
            column = current_user&.has_full_student_network_events_access? ? 'full_keyword_vector' : 'anonymous_keyword_vector'
            vector_search = StudentNetworkEvent.get_vector_search_sql(filters[:keyword_search]) # string
            query = query.where("student_network_events.id IN (SELECT student_network_event_id FROM student_network_event_fulltext WHERE #{column} @@ to_tsquery('english', #{vector_search}))")
        end
        query
    end

    def filter_student_network_events_by_location(filters, query)
        places = Array.wrap(filters[:places]).compact
        if places.any?
            location_clauses = []
            column = current_user&.has_full_student_network_events_access? ? 'location' : 'location_anonymized'

            places.each do |place_details|
                # 1 mile = 1.60934 KM
                location_clauses << Location.at_location_sql_string(place_details, column, 50 * 1.60934)
            end

            query = query.where(location_clauses.join(" OR "))
        end
        query
    end

    def filter_student_network_events_by_event_type(filters, query)
        if filters[:event_type]&.any?
            query = query.where("ARRAY[student_network_events.event_type] && ARRAY[?]", filters[:event_type])
        end
        query
    end

end