SCORM Integration Notes
=======================

Here's where templates for SCO files might go. We have one example already that has been shown to work in
Moodle and ScormCloud. The example is currently set to point to localhost with a fixed COURSE identifier.
In the future, these values will be generated, presumably re-stamped for each individual lesson.

Each SCO will also have to be stamped with a valid institutional `scorm_token` found in the Institutions Admin.
These are standard UUID values and should never be shared with the client, outside of packaging this value in
the manifest or stamped source for each SCO file.

Currently, to test, perform the following:

```
cd smartly_basic
subl assets/index.html
<REPLACE THE `SCORM_TOKEN` VALUE WITH SOMETHING VALID>
zip -r ../smartly_basic_sco.zip ./*
cd ..
open .
```

Then upload this new zip into any SCORM compliant LMS. It should connect to your local devserver without any
cross-site security issues in the `postMessage` RPC communication.

Additional points of interest will be the `imsmanifest.xml` file which contains course organization information,
including titles, which may also be stamped. We may also find that this is the more suitable place for token
storage, etc.