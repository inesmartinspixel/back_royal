#!/bin/sh
echo "Stamping build information into application source ..."

SMARTLY_VERSION=$(($CIRCLE_BUILD_NUM + $SMARTLY_VERSION_OFFSET))
BUILD_TIME="$(date -u +%FT%T.%3NZ)"


sed -i "s/APPLICATION_BUILD_NUMBER_CI_STAMPED/$SMARTLY_VERSION/" config/initializers/raven.rb

sed -i "s/APPLICATION_BUILD_NUMBER_CI_STAMPED/$SMARTLY_VERSION/" build/deploy.sh
sed -i "s/CIRCLE_BUILD_NUMBER_CI_STAMPED/$CIRCLE_BUILD_NUM/" build/deploy.sh

sed -i "s/APPLICATION_BUILD_NUMBER_CI_STAMPED/$SMARTLY_VERSION/" vendor/front-royal/modules/BuildConfig/BuildConfig.js
sed -i "s/APPLICATION_BUILD_TIMESTAMP_CI_STAMPED/$BUILD_TIME/" vendor/front-royal/modules/BuildConfig/BuildConfig.js
sed -i "s/APPLICATION_COMMIT_CI_STAMPED/$CIRCLE_SHA1/" vendor/front-royal/modules/BuildConfig/BuildConfig.js