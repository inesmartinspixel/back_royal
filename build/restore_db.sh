#!/bin/sh

# checks return value from previous command and exits if non-zero
bailOnFail() {
    if [ $? -ne 0 ]
    then
      exit 1
    fi
}


CI_ARTIFACT_DIR=db/ci_artifacts

echo "Restoring back_royal_test ..."
createdb -h localhost -U circleci back_royal_test
bailOnFail
pg_restore -h localhost -U circleci -Fd -d back_royal_test -v ${CI_ARTIFACT_DIR}/back_royal_test.dir || true
bailOnFail

# FIXME?: we only need one db or the other here
echo "Restoring back_royal_puppet ..."
createdb -h localhost -U circleci back_royal_puppet
bailOnFail
pg_restore -h localhost -U circleci -Fd -d back_royal_puppet -v ${CI_ARTIFACT_DIR}/back_royal_puppet.dir || true
bailOnFail
# since pgdump does not handle roles, we need to do that too
bundle exec rake test:puppet:ensure_puppetmaster_role


echo "Restoring red_royal_test ..."
createdb -h localhost -U circleci red_royal_test
bailOnFail
pg_restore -h localhost -U circleci -Fd -d red_royal_test -v ${CI_ARTIFACT_DIR}/red_royal_test.dir || true
bailOnFail

echo "Ensuring materialized views are populated ..."
bundle exec rake db:ensure_all_materialized_views_populated
bailOnFail