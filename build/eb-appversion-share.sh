    #!/bin/bash

    GET_PROFILE=$1
    PUT_PROFILE=$2
    VERSION=$3
    APP_NAME=$4

    if [ "$#" -ne 4 ]
    then
        echo "Usage: ./<script name> <AWS source profile name> <AWS source destination name> <version> <application>"
        exit 1
    fi

    echo "Using AWS profile for getting application version: ${GET_PROFILE}"
    echo "Getting application version ${VERSION}"
    RESULT=$(aws elasticbeanstalk describe-application-versions --version-labels ${VERSION} --profile ${GET_PROFILE} --output json | jq '.ApplicationVersions[0]' -M)
    if [ -z "$RESULT" ]; then
        echo "Version provided is invalid."
        exit
    fi

    BUCKET=$(echo ${RESULT} | jq '.SourceBundle.S3Bucket' -M -r)
    KEY=$(echo ${RESULT} | jq '.SourceBundle.S3Key' -M -r)
    DESCRIPTION=$(echo ${RESULT} | jq '.Description' -M -r)
    echo "Bucket: ${BUCKET}"
    echo "S3 Object key: ${KEY}"
    DEST=$(mktemp -d)

    aws s3 cp s3://$BUCKET/$KEY $DEST/$KEY --profile $GET_PROFILE

    if [ $? -eq 0 ]
    then
        echo "Successfully copied version ${VERSION} file ${KEY}"
    else
        echo "Could not copy version ${VERSION} file ${KEY}" >&2
    fi

    RESULT=$(aws elasticbeanstalk describe-application-versions --application-name ${APP_NAME} --profile ${PUT_PROFILE} --output json | jq '.ApplicationVersions[0]' -M)
    DEST_BUCKET=$(echo ${RESULT} | jq '.SourceBundle.S3Bucket' -M -r)
    if [ -z "$RESULT" ]; then
        echo "Cannot find a bucket to push new application version."
        exit
    fi

    aws s3 cp ${DEST}/${KEY} s3://${DEST_BUCKET}/${KEY} --profile ${PUT_PROFILE}

    aws elasticbeanstalk create-application-version --application-name ${APP_NAME} --version-label "${VERSION}" --description "${DESCRIPTION}" --source-bundle \
        S3Bucket="${DEST_BUCKET}",S3Key="${KEY}" --profile ${PUT_PROFILE}
    echo "New version successfully created in application ${APP_NAME}."

    rm -rf $DEST