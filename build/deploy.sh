#!/bin/bash

# checks return value from previous command and exits if non-zero
bailOnFail() {
    if [ $? -ne 0 ]
    then
      exit 1
    fi
}

waitOnReady() {
    # no aws elasticbeanstalk wait ...  =[
    while true; do
        case "$(eb status $1 --profile staging | grep Status)" in
            *Ready*) break ;;
            *) ;;
        esac
        sleep 30
    done
}


if [ $# -ne 1 ]; then
    echo "$0: usage: deploy.sh environment-name"
    exit 1
fi


# parse and trim whitespace from environment-name
env_name_raw=$1
environment_name="$(echo "${env_name_raw}" | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//')"

###############################################
########## Deploy Stats & Monitoring ##########
###############################################

if [ "$environment_name" = "smartly-staging" ]
then
    # get readable bundle sizes and names
    js_bundle_sizes="$(ls -Ss public/assets/scripts --block-size=k | grep '.*\.js$' | sed 's/-.\{8\}.js/.js/g')"
    css_bundle_sizes="$(ls -Ss public/assets/styles --block-size=k | grep '.*\.css$' | sed 's/-.\{8\}.css/.css/g')"

    # send js metrics to cloudwatch
    while IFS= read -r line; do
        filename="$(echo $line | sed 's/\([0-9]*K \)\(.*\)/\2/')"
        filesize="$(echo $line | sed 's/\([0-9]*\).*/\1/')"
        aws cloudwatch put-metric-data --namespace "Deploy Size Statistics (JS)" --metric-name $filename --value $filesize --unit Kilobytes --profile staging
    done <<< "$js_bundle_sizes"

    # send css metrics to cloudwatch
    while IFS= read -r line; do
        filename="$(echo $line | sed 's/\([0-9]*K \)\(.*\)/\2/')"
        filesize="$(echo $line | sed 's/\([0-9]*\).*/\1/')"
        aws cloudwatch put-metric-data --namespace "Deploy Size Statistics (CSS)" --metric-name $filename --value $filesize --unit Kilobytes --profile staging
    done <<< "$css_bundle_sizes"

    # create audit file to track deploy stats and push to s3
    filename="APPLICATION_BUILD_NUMBER_CI_STAMPED.$(date +"%m-%d-%Y_%H:%M:%S").txt"
    touch tmp/$filename
    echo -e "----\nBUILD: APPLICATION_BUILD_NUMBER_CI_STAMPED\nDATE: $(date -u)" >> tmp/$filename
    echo -e "\n\nScripts:\n${js_bundle_sizes}" >> tmp/$filename
    echo -e "\n\nStylesheets:\n${css_bundle_sizes}" >> tmp/$filename
    aws s3 cp tmp/$filename s3://${DEPLOYMENT_BUCKET}/deployment_assets/deploy_stats/$filename --profile production
fi

###############################################

# Warm critical application assets in S3
declare -a proxied_types=("scripts" "styles" "locales" "fonts")
for i in "${proxied_types[@]}"
do
    aws s3 cp public/assets/${i} s3://${DEPLOYMENT_BUCKET}/deployment_assets/${i} --profile production --include "*" --recursive
    bailOnFail
done


# copy PGAntomizer / CDB dump configs
if [ "$environment_name" = "smartly-staging" ]
then
    echo "Copying dump config files: $(ls db/ci_artifacts/dump_config) to S3 ..."
    aws s3 cp db/ci_artifacts/dump_config s3://smartly-anonymized-developer-dumps/dump_configs --profile development --include "*" --recursive
    bailOnFail
fi

# remove anything not needed in prod
rm -rf vendor/bundle \
    .sass_cache \
    log/* \
    tmp/* \
    spec \
    node_modules \
    vendor/bower_components \
    config/database.yml \
    config/database_redshift.yml \
    db/ci_artifacts \
    public/assets/scripts \
    public/assets/styles \
    public/assets/locales \
    public/assets/fonts

# create a "new" repo for EB
git init
bailOnFail

# set some defaults for author and gitignores
git config user.email "coolbrent@pedago.com"
git config user.name "Cool Brent"
git add . --all
git commit -m "initial commit"
git checkout -b tmp-branch
sed -i '/public/d' .gitignore
sed -i '/\*manifest\*.json/d' .gitignore
bailOnFail

git add . --all

newly_created=false

if [ "$environment_name" = "smartly-staging" ]
then
    git commit -m "circle-ci: CIRCLE_BUILD_NUMBER_CI_STAMPED - smartly: APPLICATION_BUILD_NUMBER_CI_STAMPED"
    bailOnFail
else
    sed -i "s/https:\/\/staging\.smart\.ly/http:\/\/$environment_name\.us-east-1\.elasticbeanstalk\.com/" config/initializers/devise_token_auth.rb
    printf "\n  - option_name: ALTERNATIVE_STAGING_ENVIRONMENT\n    value: $environment_name" >> .ebextensions/00_command_timeout.config
    git commit -am "!!! CUSTOM BUILD FOR $environment_name -- DO NOT DEPLOY !!!! circle-ci: CIRCLE_BUILD_NUMBER_CI_STAMPED - smartly: APPLICATION_BUILD_NUMBER_CI_STAMPED"
    bailOnFail

    # determine if the env exists and if not, create it
    case "$(eb status $environment_name --profile staging)" in
        *NotFoundError*)
            echo "Environment $environment_name not found -- creating!";

            # Clone from smartly-staging
            eb clone "smartly-staging" --profile staging --clone_name $environment_name --cname $environment_name --exact --timeout 30
            bailOnFail
            waitOnReady $environment_name
            bailOnFail
            newly_created=true
            ;;

        *) # else
            echo "Environment $environment_name found!"
            ;;
    esac
fi

bailOnFail

GIT_VERSION="$(git rev-parse --short HEAD^1)"
bailOnFail

# NOTE: If we wished to support custom RDS via another switch, we could conceivably do it here, such that
# it executed before the initial feature code deploy, so migrations don't touch actual staging RDS.

# Deploy to the staging instance
eb deploy --profile staging --label git-$GIT_VERSION $environment_name --timeout 15
bailOnFail

# Copy the version to the production environment
./build/eb-appversion-share.sh staging production git-$GIT_VERSION Smartly
bailOnFail

if [ "$newly_created" = true ] ; then
    # Apply a couple of customizations
    aws elasticbeanstalk update-environment --profile staging --region "us-east-1" --environment-name $environment_name --option-settings "file://.ebextensions/alt_deploy.json"
    bailOnFail
    waitOnReady $environment_name
    bailOnFail
fi