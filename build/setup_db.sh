#!/bin/sh

# checks return value from previous command and exits if non-zero
bailOnFail() {
    if [ $? -ne 0 ]
    then
      exit 1
    fi
}

# initial value
ex=0

echo "Copying DB configuration ..."
cp config/database.sample.yml config/database.yml
cp config/database_puppet.sample.yml config/database_puppet.yml
cp config/database_redshift.sample.yml config/database_redshift.yml

echo "Running DB setup ..."
bundle exec rake test:redshift:setup
bailOnFail
bundle exec rake test:setup
bailOnFail
bundle exec rake test:puppet:setup
bailOnFail

echo "Validating pgantomizer config ..."
cp db/back_royal_pgantomizer.yml db/back_royal_test_pgantomizer.yml
bailOnFail
printf "\
\n\narchived_events:\n    truncate: true \
\n\nfulltext_items:\n    truncate: true \
\n\ntime_series_things:\n    truncate: true \
\n\nschedulable_items:\n    truncate: true \
\n\ntabular_things:\n    truncate: true \
" >> db/back_royal_test_pgantomizer.yml
bailOnFail
psql -h localhost -U circleci -v back_royal_test -c "CREATE TABLE pg_stat_statements (id integer); INSERT INTO pg_stat_statements VALUES (1);"
bailOnFail
pgantomizer --schema db/back_royal_test_pgantomizer.yml --host localhost --user circleci --dbname back_royal_test --skip-restore --disable-schema-changes --verbose
bailOnFail

# kind of indirect, but we don't want / need to expose postgres container setup in deploy
# job, so grab version now and suspend it in a path that will be uploaded to s3 later
echo "Copying DB dump config and script ..."
# see also: `anonymize_and_dump.sh`
DB_VERSION=2019_$(psql -h localhost -U circleci -c "SELECT count(*) FROM schema_migrations WHERE version > '20190000000000';" -qtAX back_royal_test)
CI_ARTIFACT_DIR=db/ci_artifacts
PGANTOMIZER_CONFIG_DIR=${CI_ARTIFACT_DIR}/dump_config/${DB_VERSION}
mkdir -p $PGANTOMIZER_CONFIG_DIR
cp db/back_royal_pgantomizer.yml $PGANTOMIZER_CONFIG_DIR
cp db/anonymize_and_dump.sh $PGANTOMIZER_CONFIG_DIR

echo "Generating back_royal_test dump ..."
pg_dump -Fd -j 8 -f $CI_ARTIFACT_DIR/back_royal_test.dir -h localhost -U circleci -v back_royal_test
bailOnFail

echo "Generating back_royal_puppet dump ..."
pg_dump -Fd -j 8 -f $CI_ARTIFACT_DIR/back_royal_puppet.dir -h localhost -U circleci -v back_royal_puppet
bailOnFail

echo "Generating red_royal_test dump ..."
pg_dump -Fd -j 8 -f $CI_ARTIFACT_DIR/red_royal_test.dir -h localhost -U circleci -v red_royal_test
bailOnFail

# return the code
exit $ex