# FIXME: make this directory portable
df <- read.csv("~/code/back_royal/tmp/logit_industry_and_state.csv", na = "NA")

df$location_match_descriptor <- factor(df$location_state_match_descriptor)

matchTrain <- subset(df, df$slice=='train')
matchTest <- subset(df, df$slice == "evaluate")

model <- glm(result ~ location_state_match_descriptor + matching_industries_count, data = matchTrain, family = binomial)



library(ROCR)
p <- predict(model, newdata=matchTest, type="response")
pr <- prediction(p, matchTest$result)
prf <- performance(pr, measure = "tpr", x.measure = "fpr")
plot(prf)

auc <- performance(pr, measure = "auc")
auc <- auc@y.values[[1]]

summary(model)

