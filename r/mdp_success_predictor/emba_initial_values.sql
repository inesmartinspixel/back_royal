with enrollment_records as (
    select cohorts.name, * from program_enrollment_progress_records
        join cohorts on cohorts.id = original_cohort_id
    where program_enrollment_progress_records.program_type='emba'
        and final_status in ('graduated', 'did_not_graduate')
        and cohorts.end_date < now() - interval '3 weeks'
    order by cohorts.name
)
, step_1 as (
    SELECT
        cohort_user_periods.cumulative_required_up_to_now_lessons_completed - cumulative_required_lesson_pack_ids_count as lessons_behind
        , case when normalized_index like '%1' then 1
                 when normalized_index like '%2' then 2
                  when normalized_index like '%3' then 3
                   when normalized_index like '%4' then 4
                    when normalized_index like '%5' then 5
                     when normalized_index like '%6' then 6
                      when normalized_index like '%7' then 7
                      when normalized_index like '%8' then 8
                      end as test
        , enrollment_records.final_status
    from enrollment_records
        join cohort_user_periods on cohort_user_periods.user_id = enrollment_records.user_id
            and cohort_user_periods.cohort_id = enrollment_records.original_cohort_id
        join cohorts on cohorts.id = enrollment_records.original_cohort_id
        join published_cohort_periods
            on published_cohort_periods.index = cohort_user_periods.index
            and published_cohort_periods.cohort_id = cohort_user_periods.cohort_id
        join cohort_period_graduation_rate_predictions period_predictions
            on period_predictions.cohort_id = published_cohort_periods.cohort_id
            and period_predictions.index = published_cohort_periods.index
)
, step_2 as (
    SELECT
        case when lessons_behind > -25 then 0
            when  lessons_behind > -50 then -25
            when  lessons_behind > -75 then -50
            when  lessons_behind > -100 then -75
            when  lessons_behind > -150 then -100
            when  lessons_behind > -200 then -150
            else -500 end as bucket
        , step_1.*
    from step_1
)
select
    test
    , bucket
    , count(*)
    , avg(case when final_status = 'graduated' then 1 else 0 end) as result
from step_2
 group by test, bucket
  order by test, bucket desc
