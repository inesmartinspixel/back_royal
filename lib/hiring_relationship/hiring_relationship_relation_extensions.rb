module HiringRelationship::HiringRelationshipRelationExtensions

    def matched
        self.where(
            hiring_manager_status: 'accepted',
            candidate_status: 'accepted'
        )
    end

    def matched_or_saved_for_later
        or_block = HiringRelationship.matched.or(HiringRelationship.where(hiring_manager_status: 'saved_for_later'))
        self.merge(or_block)
    end

    def not_closed
        self.where("coalesce(hiring_relationships.hiring_manager_closed, false) != true AND coalesce(hiring_relationships.candidate_closed, false) != true")
    end

    def closed
        self.where("hiring_relationships.hiring_manager_closed = true OR hiring_relationships.candidate_closed = true")
    end

    def rejected_by_hiring_manager_or_closed
        or_block = HiringRelationship.closed.or(HiringRelationship.where(hiring_manager_status: 'rejected'))
        self.merge(or_block)
    end

    def rejected_by_candidate_or_closed
        or_block = HiringRelationship.closed.or(HiringRelationship.where(candidate_status: 'rejected'))
        self.merge(or_block)
    end

    def viewable_by_hiring_manager
        self.joins(:career_profile)
            .merge(CareerProfile.editable_and_complete)
            .where("
                    career_profiles.interested_in_joining_new_company IN (:candidate_active_levels)
                    OR hiring_manager_status IN (:hiring_manager_statuses)
                ",
                candidate_active_levels: CareerProfile.active_interest_levels,
                hiring_manager_statuses: HiringRelationship.viewable_hiring_manager_statuses
            )
    end
end

module HiringRelationshipRelationHelpers
    extend ActiveSupport::Concern

    module ClassMethods
        def matched
            self.all.matched
        end

        def matched_or_saved_for_later
            self.all.matched_or_saved_for_later
        end

        def not_closed
            self.all.not_closed
        end

        def closed
            self.all.closed
        end

        def rejected_by_hiring_manager_or_closed
            self.all.rejected_by_hiring_manager_or_closed
        end

        def rejected_by_candidate_or_closed
            self.all.rejected_by_candidate_or_closed
        end

        def viewable_by_hiring_manager
            self.all.viewable_by_hiring_manager
        end
    end
end