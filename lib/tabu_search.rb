=begin
The approach here is based on that described in the following paper:
    https://ieeexplore.ieee.org/document/5518761/?part=1

For an example, see Cohort::SlackRoomAssignmentJob


=end
class TabuSearch
    attr_accessor :best_state, :best_state_value, :current_state,
                :iterations, :value_at_iteration, :start, :current_state_value

    # You must define the following methods
    def generate_initial_state
        raise NotImplementedError.new("Subclasses of TabuSearch should define generate_initial_state")
    end

    def possible_moves(state)
        raise NotImplementedError.new("Subclasses of TabuSearch should define possible_moves")
    end

    def result_of_move(state, move)
        raise NotImplementedError.new("Subclasses of TabuSearch should define result_of_move")
    end

    def move_is_tabu?(state, move)
        raise NotImplementedError.new("Subclasses of TabuSearch should define result_of_move")
    end

    def value_of_state(state)
        raise NotImplementedError.new("Subclasses of TabuSearch should define value_of_state")
    end

    def after_move(state, move_details)
        raise NotImplementedError.new("Subclasses of TabuSearch should define after_move")
    end

    def minimum_substantive_value_change
        raise NotImplementedError.new("Subclasses of TabuSearch should define minimum_substantive_value_change")
    end

    # You may want to override the following methods
    def max_iterations
        200
    end

    def max_iterations_without_change_to_best_state
        10
    end

    def verbose
        true
    end

    private
    def move_satisfies_aspiration_criteria?(old_state, move_details)
        move_details[:result_state_value] > self.best_state_value
    end

    private
    def satisfies_stopping_criteria?
        if self.iterations >= self.max_iterations
            return true
        end

        # This lookback really would need to be configurable on the particular implementation
        # of TabuSearch.  We need to balance a couple of things.
        # * If you know that the value is going to be monotonically increasing until you
        #       reach the maximum, a lookback of 1 is actually fine.  Once you make a move
        #       that doesn't help, you can stop.  In some implementations (like the slack assignment
        #       job), that will be the case.  In other implementations, however, you may need
        #       to go backwards in order to go forwards.  The longer you might need to go
        #       backward, the longer the lookback would need to be.
        # * The slower each iteration is, the more pain there is in having a lookback that is longer
        #       than you need.  If you only need a lookback of 1, but you've set it to 10, Then you're
        #       wasting 9*TIME_FOR_ONE_ITERATION.
        lookback = 10
        value_n_iterations_ago = self.iterations > lookback ? self.value_at_iteration[self.iterations - lookback] : nil

        if value_n_iterations_ago && self.best_state_value - value_n_iterations_ago < minimum_substantive_value_change
            return true
        end

        return false
    end

    private
    def log_iteration_progress(force = false)
        return unless self.verbose
        @last_puts ||= Time.now

        if (force || Time.now - @last_puts > 10.seconds) && Rails.env.development?

            if @iterations_at_last_puts == self.iterations
                puts " -  #{Time.now - start} seconds."
            else
                puts "#{self.iterations} iterations complete in #{(Time.now - start).round(1)} seconds. Best value: #{self.best_state_value}"
            end

            # pp @timers
            @last_puts = Time.now
            @iterations_at_last_puts = self.iterations
        end
    end

    # These are the guys.  You probably don't want to override anything below here
    public
    def exec
        self.best_state = self.current_state = generate_initial_state
        self.best_state_value = self.current_state_value = value_of_state(self.best_state)
        self.start = Time.now

        self.iterations = 0
        self.value_at_iteration = []
        while !satisfies_stopping_criteria?
            old_state = self.current_state

            max_value = -Float::INFINITY

            _possible_moves = time('possible_moves') do
                possible_moves(self.current_state)
            end

            possible_move_details = _possible_moves.map do |move|

                # FIXME: there should be some way for us to decide that
                # the move we've found is good enough, and to stop searching
                # for the best one.  Like maybe if this one is 95% as good as
                # some recent move that we knew was the best one or soemthing like
                # that

                # It used to be, that instead of defining value_after_move,
                # subclasses would only define value_of_state.  For performance
                # reasons, we switched to this for slack room assignements.  It would
                # be possible, though, to make a default value for value_after_move
                # that used result_of_move and value_of_state, which have to be
                # defined anyway.  Then implementations could choose if they wanted
                # to bother defining value_after_move explicitly for performance reasons or
                # not
                value = time('value_after_move') do
                    value_after_move(move, self.current_state, self.current_state_value)
                end

                # to save memory, we don't bother saving any that
                # already have lower than the max value
                next unless value >= max_value

                details = {
                    move: move,
                    result_state_value: value
                }

                next unless move_satisfies_aspiration_criteria?(old_state, details) ||
                    !move_is_tabu?(old_state, move)

                max_value = [max_value, value].max
                log_iteration_progress
                details

            end.compact

            details_for_best_moves = possible_move_details.select do |details|
                details[:result_state_value] == max_value
            end

            break if details_for_best_moves.empty?

            best_move_details = details_for_best_moves.sample

            best_move_details[:result_state] = result_of_move(self.current_state, best_move_details[:move])
            self.current_state = best_move_details[:result_state]
            self.current_state_value = best_move_details[:result_state_value]

            if best_move_details[:result_state_value] > self.best_state_value
                self.best_state = self.current_state
                self.best_state_value = best_move_details[:result_state_value]
            end

            after_move(old_state, best_move_details)
            self.value_at_iteration[self.iterations] = self.best_state_value
            self.iterations += 1
            log_iteration_progress(true)
        end

        if Rails.env.development?
            log_iteration_progress
            puts "------------------"
            pp @timers
            puts "------------------"
            pp stats if self.respond_to?(:stats)
            puts "------------------"
            value_of_state(self.best_state, log: true)
        end

        self
    end

    def time(key, &block)
        @timers ||= Hash.new { |hash, k| hash[k] = {key: k, total_time: 0}}
        start = Time.now
        result = yield
        @timers[key][:total_time] += Time.now - start
        result
    end

end