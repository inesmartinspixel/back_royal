## Question: what about global fallback routes and in-app routes? maybe we redirect those via the user's institution later?

module HttpHelpersExtensions

    ####################################################
    # Custom Constraints
    ####################################################

    # Redirect this request to the quantic domain (AppConfig.quantic_domain), but only if the request domain
    # isn't already quantic's domain (or an alternative staging environment).
    # Also include magic override params to allow testing in development and on prod prior to the switch
    #
    # Note: The domain mismatch redirect will only occur in production or if you've forced it
    # via the force_host param in development
    class QuanticRedirectConstraint
        def matches?(request)
            (
                (Rails.env.production? && request.domain != AppConfig.quantic_domain && AppConfig.is_alternative_staging_environment != "true") ||
                (!Rails.env.production? && request.params[:force_host].present? && request.params[:force_host] != AppConfig.quantic_domain)
            )
        end
    end

    class QuanticConstraint
        def matches?(request)
            !QuanticRedirectConstraint.new.matches?(request)
        end
    end

    # Only handle this request if domain is smart.ly.
    # Also include magic override params to allow testing in development and on prod prior to the switch
    #
    # Note: To test a smart.ly route locally post-switch, you'll have to include ?force_host=smart.ly
    class SmartlyConstraint
        def matches?(request)
            (request.domain == AppConfig.smartly_domain || AppConfig.is_alternative_staging_environment == "true") ||
            (!Rails.env.production? && request.params[:force_host].present? && request.params[:force_host] == AppConfig.smartly_domain)
        end
    end

    class OpenConstraint
        def matches?(request)
            true
        end
    end

    ####################################################
    # Helpers methods for defining routes
    ####################################################

    # After quantic switchover, redirect to quantic if host doesn't match
    def get_quantic(*args, &block)

        get "#{args[0]}",
            to: redirect {
                |params, request| "#{UrlHelper.protocol(request)}#{AppConfig.quantic_domain}#{request.fullpath}"
            },
            constraints: QuanticRedirectConstraint.new

        # Note that, unlike with the other two methods, here we're
        # passing in a different constraint to register_route, since
        # the constraint is used in the opposite way here.  That is,
        # it defines the routes that you CANNOT get to.
        SitemapHelper.register_route(args[0], QuanticConstraint.new)
        get(*args, &block)
    end

    # After quantic switchover, only match if host is smart.ly
    def get_smartly(*args, &block)

        options = args.extract_options!
        constraint = {
            constraints: SmartlyConstraint.new
        }
        SitemapHelper.register_route(args[0], SmartlyConstraint.new)
        get(*args, options.merge(constraint), &block)
    end

    # This method does not do anything special to the route itself.  A route
    # defined with get_with_sitemap_support will be available on all domains.
    # But it registers that route with the SitemapHelper so it can be included in
    # sitemaps for any domain.
    def get_with_sitemap_support(*args, &block)
        SitemapHelper.register_route(args[0], OpenConstraint.new)
        get(*args, &block)
    end
end

module ActionDispatch
    module Routing
        class Mapper
            prepend HttpHelpersExtensions
        end
    end
end