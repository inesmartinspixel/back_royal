class Time

    # In theory, we generally want thresholds to be relative to PST. This is because it's theoretically
    # the last time zone in which a lot of users reside. So, when calculating dates relative to a threshold,
    # we don't want folks in EST to see a different date than folks in PST.
    DEFAULT_RELATIVE_THRESHOLD_TIMEZONE = 'Pacific Time (US & Canada)'

    # In certain situations, we apply an offset to a particular date in order
    # to determine when a job should run.
    #
    # Although our servers run on UTC time, we are making an assumption that
    # schedules are created in the context of EST. Therefore, we need to add
    # DST aware offsets in order to maintain hour fidelity and make sure these
    # jobs run when they are expected to.
    def add_dst_aware_offset(interval)
        (self.in_time_zone("Eastern Time (US & Canada)") + interval).in_time_zone(Time.zone.name)
    end

    # In the front-end, we communicate deadlines to the end-user using a threshold. If a deadline
    # is in the early AM, we communicate the deadline as the preceding day. We also allow the method
    # caller to specify a timezone. This is handy if you're trying to calculate a time relative to
    # a user's local timezone.
    #
    # NOTE: The value of the threshold that's used in this method should be the same as the threshold used
    # in shiftMonthDayForThreshold in date_helper.js.
    def relative_to_threshold(threshold: 23, timezone: self.class::DEFAULT_RELATIVE_THRESHOLD_TIMEZONE)
        timezone ||= self.class::DEFAULT_RELATIVE_THRESHOLD_TIMEZONE # handles case when timezone is passed in as nil
        return self.present? && self.in_time_zone(timezone).hour < threshold ? self.yesterday : self
    end

end