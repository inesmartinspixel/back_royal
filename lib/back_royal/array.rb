class Array
    def to_sql
        # Toss out any nil values
        compacted_array = self.compact

        # Postgres will error if you specify an empty array without an
        # explicit typecast, so just return an array of NULL if the
        # compacted_array is empty
        return 'ARRAY[NULL]' if compacted_array.empty?

        # Build a sanitized string that can be inserted into `Array` in sql
        comma_separated_values = compacted_array&.map { |val|  ActiveRecord::Base.send(:sanitize_sql_array, ["?", val]) }.join(',')

        return "ARRAY[#{comma_separated_values}]"
    end

    def to_sql_list
        csv = self.map { |item| "'#{item}'" }.join(',')
        "(#{csv})"
    end
end
