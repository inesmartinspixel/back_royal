class Object

    def looks_like_uuid?
        return false unless is_a?(String)
        match(/^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/i).present?
    end

end