require 'active_support/hash_with_indifferent_access'
require 'action_controller/metal/strong_parameters.rb'

class Hash
    alias_method :unsafe_with_indifferent_access, :with_indifferent_access
end

class HashWithIndifferentAccess
    alias_method :unsafe_with_indifferent_access, :with_indifferent_access
end

class ActionController::Parameters
    def unsafe_with_indifferent_access
        to_unsafe_h.with_indifferent_access
    end
end