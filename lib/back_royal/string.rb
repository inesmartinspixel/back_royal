class String

    def dasherize_md5_hash
        raise "unexpected string format" unless self.size == 32
        start = 0
        [8, 4, 4, 4, 12].inject([]) do |parts, length|
            parts << self.slice(start, length)
            start = start + length
            parts
        end.join('-')
    end

end