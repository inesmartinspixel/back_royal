module ToTimestamp

    def to_timestamp
        if self.nil?
            nil
        else
            to_f.round
        end
    end

end

class Numeric
    include ToTimestamp
end

class NilClass
    include ToTimestamp
end

class Time
    include ToTimestamp
end

class String
    include ToTimestamp
end

class DateTime
    include ToTimestamp
end

class ActiveSupport::TimeWithZone
    include ToTimestamp
end