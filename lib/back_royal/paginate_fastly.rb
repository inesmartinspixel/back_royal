require 'active_record/relation'

class ActiveRecord::Relation

    # will_paginate, the general pagination solution for rails,
    # has to do 2 separate queries.  It will do a `count` query to
    # get the total number of records, and a separate query to
    # get the actual results for the current page.
    #
    # With certain filtering logic on certain tables, both of those
    # queries can be slow, so we want a more performant solution.
    #
    # paginate_fastly follows the following steps
    #
    # 1. Query for all of the ids that match the provided filters
    # 2. Using the provided limit, offset, and ordering logic, determine
    #       the set of ids for the current page. (Note: this is done
    #       in the same query as step 1.)
    # 3. Generate a new ActiveRecord::Relation object that is filtered
    #       for the ids needed for the requested page.  Executing this
    #       requires a second query, but that query should be really
    #       fast since it is an indexed query for a very limited set of records.
    # 4. Return the total_count and the query generated in step #3. It is possible
    #       to call `select` on this returned query, but `limit`, `order`, and
    #       `where` will probably not give you what you want.
    #
    # In addition, this method supports a `max_total_count`.  This can be
    # used to ensure that the query does not take too long.
    def paginate_fastly(limit: nil, offset: nil, page: nil, max_total_count: 9999999999999)

        limit = limit.to_i if limit.present?
        page = page.to_i if page.present?
        offset = offset.to_i if offset.present?
        max_total_count = max_total_count.to_i if max_total_count.present?

        # setup defaults
        if offset.nil? && limit.present? && page.present?
            offset = limit * (page - 1) # page is 1-indexed
        end
        limit = limit ? limit.to_i : 9999999999999
        offset = offset ? offset.to_i : 0
        max_total_count = max_total_count ? max_total_count.to_i : 9999999999999

         # If we've set the max_total_count, but then moved onto a page beyond it,
        # we should not return a total_count that is less that the offset we're at.
        max_ids_to_fetch = [max_total_count.to_i, limit + offset].max

        # query for the total_count and the list of ids for this page
        query = %Q~
            with ids AS MATERIALIZED (#{self.select(:id).limit(max_ids_to_fetch).to_sql})
            , limited_ids AS MATERIALIZED (
                select
                    id
                from ids
                limit #{limit} offset #{offset}
            )
            , total_count AS MATERIALIZED (
                select count(*) as total_count from ids
            )
            select
                id,
                total_count
            from total_count
                left join limited_ids on true
        ~

        result = ActiveRecord::Base.connection.execute(query).to_a

        # total_count can be greater than max_total_count if offset is greater than max_total_count
        total_count = [result[0]['total_count'], max_total_count].min
        ids = result.map { |r| r['id'] }.compact # compact only applies when no ids are returned for the limit and offset


        # generate the query that can be used to fetch the records for this page.
        if ids.any?

            # see https://stackoverflow.com/questions/866465/order-by-the-in-value-list
            # the join here will restrict to the ids found above and also
            # allow us to keep the order from the list of ids found above
            values = []
            ids.each_with_index do |id, i|
                values << "('#{id}'::uuid, #{i})"
            end

            query = klass.joins(%Q(
                join (
                    values
                        #{values.join(",\n")}
                    ) as x (id, ordering) on #{klass.table_name}.id = x.id
            )).reorder("x.ordering")
        else
            query = klass.where('false')
        end
        [query, total_count]
    end

end