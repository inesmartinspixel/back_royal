# Any task in here can be deleted once it has been run on production
#
# Ex.
# task :backfill_foo => :environment do
#     count = Foo.count
#     Foo.all.each_with_index do |record, i|
#         record.log_foo
#         puts "#{i+1} of #{count} events logged" if (i+1) % 25 == 0
#     end
# end

# https://trello.com/c/do8Mepai
task :backfill_payment_grace_period_end_at => :environment do
    jobs = Delayed::Job.where(queue: LockContentAccessDueToPastDuePayment.queue_name, locked_at: nil, failed_at: nil)
    puts "Backfilling payment_grace_period_end_at for #{jobs.size} applications..."
    jobs.each do |job|
        cohort_application_id = job.payload_object.job_data['arguments'].first
        cohort_application = CohortApplication.find(cohort_application_id)
        cohort_application.update_column(:payment_grace_period_end_at, job.run_at)
    end
    puts "Rake task complete!"
end
