task :import_module_into_scripts do
    Dir.glob('vendor/front-royal/modules/*/angularModule/').sort.each do |angular_module_dir|

        module_filepath = get_script_that_defines_angular_module(angular_module_dir)[0]
        module_relpath = module_filepath.sub('vendor/front-royal/modules/', '').sub('.js', '')
        
        module_contents = File.read(module_filepath)

        unless module_contents.match("export default")
            module_contents.sub!("angular.module", "export default angular.module")
            module_contents.sub!("angular\n    .module", "export default angular\n    .module")
            File.write(module_filepath, module_contents)
        end

        puts angular_module_dir

        Dir.glob("#{angular_module_dir}scripts/**/*js").each do |script_filepath|

            next if script_filepath == module_filepath
            contents = File.read(script_filepath)
            next if contents.match("import angularModule")
            contents = "import angularModule from '#{module_relpath}';\n" + contents
            contents.gsub!(/angular\.module\('[^']+'\)/, 'angularModule')
            contents.gsub!(/cacheAngularTemplate\('[^']+'/, "cacheAngularTemplate(angularModule")
            File.write(script_filepath, contents)
        end

    end
end

task :module_info do

    moved_modules = {}
    unmoved_modules = {}
    modules = get_all_files_that_define_angular_modules.map do |file|
        entry = {
            file: file,
            angular_module_name: get_angular_module_name(file),
            dependencies: get_dependencies_for_angular_module_file(file),
            moved: !!file.match(/vendor\/front-royal\/modules/)
        }
        if entry[:moved]
            moved_modules[entry[:angular_module_name]] = entry
        else
            unmoved_modules[entry[:angular_module_name]] = entry
        end
        entry
    end
    
    i = 1
    last_count = nil
    while (last_count.nil? || unmoved_modules.size != last_count) && i < 10
        last_count = unmoved_modules.size
        mock_moving_modules(moved_modules, unmoved_modules, i)
        i += 1
    end

    puts "modules left #{unmoved_modules.values.pluck(:angular_module_name).sort.inspect}"
end

task :fix_spec_locale_issue do
    locale_dir_map = Hash[Dir.glob("vendor/front-royal/modules/*/locales/*").map { |f| parts = f.split("/").reverse; [parts[0], parts[2]] }]

    failures = []
    Dir.glob('vendor/front-royal/modules/Admin/angularModule/spec/admin_users/admin_edit_career_profile_dir_spec.js').sort.each do |spec_file|
        result = fix_missing_locale_issues_in_spec(spec_file, locale_dir_map)
        failures << spec_file if result == :failed
    end

    puts "FAILURES:"
    failures.each { |f| puts f }
    #spec_file = "vendor/front-royal/modules/Careers/angularModule/spec/featured_positions_dir_spec.js"
end

def fix_missing_locale_issues_in_spec(spec_file, locale_dir_map)
    puts "#{Time.now} #{spec_file}"
    result = :run_again
    while result == :run_again
        result = run_spec_and_fix_missing_locale_issue(spec_file, locale_dir_map)
    end

    if result == :passed
        puts " * PASSED"
    else
        puts " * FAILED"
    end

    return result
end

def run_spec_and_fix_missing_locale_issue(spec_file, locale_dir_map)
    cmd = "jest -c jest/config.modules.json --runInBand --ci #{spec_file}"
    Open3.popen3(cmd)  do |stdin, stdout, stderr, thread|
        out = stderr.read 

        if out.match(/^PASS/)
            return :passed
        end


        #Possibly unhandled rejection: careers.position_card.compensation
        missing_translations = out.scan(/Possibly unhandled rejection: ([\w_\.]+)/).flatten.uniq

        # expect(received).toEqual(expected) // deep equality

        # - Expected  - 1
        # + Received  + 1
    
        #   Object {
        # -   "text for: .tabs button.open-positions": "Open",
        # +   "text for: .tabs button.open-positions": "careers.positions.open",
        #   }
        missing_translations += out.scan(/text for: [^:]+: "([[a-z0-9\-]\._]+)/).flatten.uniq.select { |t| t.match(".") }
        

        # Missing translation \"careers.field_options.hiring_manager
        missing_translations += out.scan(/Missing translation "([[a-z0-9\-]\._]+)/).flatten.uniq

        # - Expected
        # + Received
    
        #   Object {
        # -   "content": "This applicant has already been been reviewed by another teammate.",
        # +   "content": "careers.careers_network_view_model.interest_save_conflict",
        missing_translations += out.scan(/"content": [^:]+: "([[a-z0-9\-]\._]+)/).flatten.uniq.select { |t| t.match(".") }

        # Expected: "Washington, DC (+2 locations)"
        # Received: "careers.field_options.washington_dc (+2 locations)"
        missing_translations += out.scan(/Received: "([[a-z0-9\-]\._]+)/).flatten.uniq.select { |t| t.match(".") }

        #  "error", {"label": "Missing translation \"careers.edit_career_profile.select_skills_form.content_marketing\" for lan
        missing_translations += out.scan(/Missing translation \\"([[a-z0-9\-]\._]+)/).flatten.uniq

        # just look for anything that lookks like a translation key
        # (probably makes all the above unnecessary)
        missing_translations += out.scan(/([[a-z0-9\-]\._]+)/).flatten.select { |m| m.scan(/\./).size > 1 && m.match(/^[a-z]/) && m.match(/[a-z]$/) }

        # check for any translations in the template.  These can fail silently
        if missing_translations.empty? && spec_file.match("_dir.js")
            directive_file = spec_file.sub("/spec/", "/scripts/").sub("_dir_spec.js", "_dir.js")
            imported_templates = File.read(directive_file).split("\n").map { |line| line.match(/import \w+ from '([^\.]+\.html)'/) }.compact.map { |m| m[1] }
            imported_templates.each do |template|
                contents = File.read("vendor/front-royal/modules/" + template)
                missing_translations += (contents.scan(/translate-once[\s=]+["']([^"']+)["']/) + contents.scan(/translate[\s=]+["']([^"']+)["']/)).flatten.uniq
            end
        end

        missing_file_keys = missing_translations.map { |key| parts = key.split("."); parts.pop; parts.join('.') }
        
        missing_files = {}
        missing_file_keys.map do |k|
            next if k == 'uery.fn.init'
            next if k == ''
            parts = k.split(".")
            toplevel = parts.shift
            dir = "vendor/front-royal/modules/#{locale_dir_map[toplevel]}"

            raise "This does not look like a local key #{k.inspect}" if parts.last.nil?
            locale_var_name = parts.last.camelize(:lower) + "Locales"
            file = "#{dir}/locales/#{toplevel}/#{parts.join("/")}-en.json"
            next unless File.exist?(file)
            missing_files[locale_var_name] = "#{dir}/locales/#{toplevel}/#{parts.join("/")}-en.json"
        end

        contents = File.read(spec_file)
        contents.gsub!(/setSpecLocales\(([^\)]+)\);/) { |a,b,c| "setSpecLocales(#{$1.gsub(/\s+/, " ")});" }
        lines = contents.split("\n")
        insert_after_line = lines.select { |l| l.match(/^import/) && l.match(".json") }.last
        if insert_after_line.nil?
            insert_after_line = lines.select { |l| l.match(/^import/) }.last
        end
        index = lines.index(insert_after_line)

        made_changes = false
        missing_files.each do |locale_var_name, locale_filepath|
            next if contents.match(locale_var_name)
            
            relpath = locale_filepath.sub('vendor/front-royal/modules/', '')
            puts " - #{relpath}"
            lines.insert(index+1, "import #{locale_var_name} from '#{relpath}';")
            index += 1
            made_changes = true
        end

        if !made_changes
            return :failed
        end


        set_spec_locales_line = lines.detect {|l| l.match(/setSpecLocales\(/) }
        if set_spec_locales_line.nil?
            set_spec_locales_line = 'setSpecLocales();'
            lines.insert(index+1, "import setSpecLocales from 'Translation/setSpecLocales';", "", set_spec_locales_line)
        end
        match = set_spec_locales_line.match(/\(([^\)]*)/)
        locale_vars = match[1].split(',')
        locale_vars += missing_files.keys
        new_line = "setSpecLocales(#{locale_vars.map(&:strip).join(', ')});"

        contents = lines.join("\n")+"\n"
        contents.sub!(set_spec_locales_line, new_line)
        File.write(spec_file, contents)

        return :run_again
    end
end

task :list_dependent_modules do
    angular_module_name = ENV['m']
    get_dependent_modules(angular_module_name).each do |pair|
        name, file = pair
        puts name
    end
end

task :essixify do 
    raise "need to add locale imports"
    essixify(ENV['dir'])
    
end

task :add_dependency do
    import_statement = %Q~import { TweenMax } from 'FrontRoyalGsap';~
    cmd = %Q~grep -rEl "new TimelineMax" vendor/front-royal/modules~
    puts cmd
    files_that_use_directive = `#{cmd}`.split("\n")
    pp files_that_use_directive

    files_that_use_directive.each do |file|
        lines = File.read(file).split("\n")
        next if lines.include?(import_statement)
        insert_after_line = lines.select { |l| l.match(/^import/) &&  l < import_statement }.last
        index = lines.index(insert_after_line)
        index = index || -1
        lines.insert(0, import_statement)
        File.write(file, lines.join("\n")+"\n")
    end
end

task :stubCareersNetworkViewModel do
    cmd = %Q~grep -rEl "stubCareersNetworkViewModel" vendor/front-royal~
    files = `#{cmd}`.split("\n")
    files.each do |file|
        next if file == 'vendor/front-royal/modules/Careers/angularModule/spec/careers_spec_helper.js'
        next if file == 'vendor/front-royal/modules/Careers/angularModule/spec/candidate_full_card_actions_dir_spec.js'
        lines = File.read(file).split("\n")

        insert_after_line = lines.select { |l| l.match(/let SpecHelper/)  }.first
        raise file unless insert_after_line
        whitespace = insert_after_line.match(/^\s+/)[0]
        index = lines.index(insert_after_line)
        lines.insert(index+1, "#{whitespace}let CareersSpecHelper;")

        line = lines.detect { |l| l.match("angular.mock.module")}
        raise file unless line
        index = lines.index(line)
        lines[index] = line.gsub('angular.mock.module(', "angular.mock.module('CareersSpecHelper', ")

        insert_after_line = lines.select { |l| l.match(/injector.get\('SpecHelper'/)  }.first
        raise file unless insert_after_line
        whitespace = insert_after_line.match(/^\s+/)[0]
        index = lines.index(insert_after_line)
        lines.insert(index+1, "#{whitespace}CareersSpecHelper = $injector.get('CareersSpecHelper');")

        contents = lines.join("\n")+"\n"
        contents.gsub!('SpecHelper.stubCareersNetworkViewModel', 'CareersSpecHelper.stubCareersNetworkViewModel');
        File.write(file, contents)
    end
end

task :cache_directive_template do
    Dir.glob('vendor/front-royal/modules/*/angularModule/**/*_dir.js').each do |filepath|
        content = File.read(filepath)
        #module_dir = filepath
        next unless content.match(/import template from/)
        next unless content.match(/template,/)
        angular_module_name = get_angular_module_name(filepath.split('angularModule')[0] + 'angularModule/')

        lines = content.split("\n")
        insert_after_line = lines.select { |l| l.match(/^import/) }.last
        index = lines.index(insert_after_line)
        insert_whitespace = !lines[index+1].blank?

        newlines = []
        if !content.match(/import cacheAngularTemplate/)
            newlines << "import cacheAngularTemplate from 'cacheAngularTemplate';"
        end
        newlines << ""
        newlines << "const templateUrl = cacheAngularTemplate('#{angular_module_name}', template);"
        newlines << "" if insert_whitespace

        lines.insert(index+1, *newlines)
        contents = lines.join("\n")+"\n"
        contents.sub!(/template,/, "templateUrl,")
        File.write(filepath, contents)
        puts filepath
        # lines = content.split("\n")
    end
end

task :require_images do 
    cmd = %Q~grep -rEl "src=.images\/" vendor/front-royal/modules~
    files_with_images = `#{cmd}`.split("\n")
    files_with_images.each do |file|
        directive_file = file.gsub("views/", "scripts/").gsub(".html", "_dir.js")
        if file == 'vendor/front-royal/modules/Authentication/angularModule/views/sign_up_sidebars.html'
            directive_file = 'vendor/front-royal/modules/Authentication/angularModule/scripts/sign_up_form_dir.js'
        elsif !File.exists?(directive_file)
            puts "NO directive file found for #{file}"
            next
        end

        contents = File.read(file)
        subbed_contents = contents.clone
        requires = {}
        contents.scan(/(src=.)([vectors|images][^'"]+)/).uniq.each do |entry|
            prefix, img_path = entry
            next unless img_path.match(/^vectors\//) || img_path.match(/^images\//)
            old_str="#{prefix}#{img_path}"
            match = img_path.match(/\/([^\.]+)/)
            debugger if match.nil?
            var = match[1].gsub('-', '_').camelize(:lower)
            new_str = "#{prefix.gsub('src', 'ng-src')}{{::#{var}}}"
            subbed_contents.gsub!(old_str, new_str)
        
            requires[var] = img_path        
        end

        File.write(file, subbed_contents)

        directive_file_contents = File.read(directive_file)
        lines = directive_file_contents.split("\n")
        insert_after_line = lines.select { |l| l.match(/^import/) }.last
        add_whitespace_after = false
        require_lines = []
        var_lines = []

        if insert_after_line
            index = lines.index(insert_after_line)
            if !(lines[index+1]).blank?
                add_whitespace_after = true
            else
                require_lines << ""
            end
        end

        # add require statements
        requires.each do |var_name, path|
            require_lines << "const #{var_name} = require('#{path}');"
            var_lines << "scope.#{var_name} = #{var_name};"
        end
        require_lines << "" if add_whitespace_after
        var_lines << ""
        lines.insert(index+1, *require_lines)


        # add scope variables
        insert_after_line = lines.detect { |l| l.match(/link\(/) || l.match(/link: function/) }
        whitespace = insert_after_line.match(/^\s+/)[0] + "    "
        index = lines.index(insert_after_line)
        var_lines = var_lines.map { |l| l.blank? ? l : "#{whitespace}#{l}"}
        lines.insert(index+1, *var_lines)


        new_contents = lines.join("\n")+"\n"
        File.write(directive_file, new_contents)
    end
end

task :essixify_specific_dirs do
    dirs = Dir.glob("vendor/front-royal/components/*")
    pp dirs

    dirs.each do |dir|
        next if dir == 'vendor/front-royal/components/pagination'
        next if dir == 'vendor/front-royal/components/spec_helper'
        next if dir == 'vendor/front-royal/components/telephone'
        next if dir == 'vendor/front-royal/components/frame-rpc'
        next if dir == 'vendor/front-royal/components/oo_directive'
        puts "#{dir}"
        essixify dir
    end
end

task :add_locale_imports do
    Dir.glob('vendor/front-royal/modules/*').each do |dir|
        locales = Dir.glob("#{dir}/locales/**/*-en.json")
        index_file = "#{dir}/angularModule/index.js"
        next unless locales.any? && File.exist?(index_file)
        
        contents = File.read(index_file)
        lines = contents.split("\n")

        locales.sort.each do |locale|
            rel_path = locale.gsub('vendor/front-royal/modules/', '')
            import_statement = "import '#{rel_path}';"
            lines << import_statement unless lines.include?(import_statement)
        end

        contents = lines.join("\n")+"\n"
        File.write(index_file, contents)
    
    end
end

def essixify(dir)
    dir += '/' unless dir.ends_with?('/')

    old_name = dir.split('/').last
    es6_module_name = old_name.camelize

    move_component_files_to_modules(dir, old_name, es6_module_name)
    #puts get_all_files_that_define_angular_modules

    angular_module_name = get_angular_module_name("vendor/front-royal/modules/#{es6_module_name}/angularModule/")
    # declare_undeclared_dependencies dir
    add_import_statements_to_dependent_modules(angular_module_name, es6_module_name)
    add_import_statements_to_spec_files(angular_module_name, es6_module_name)
    add_template_imports(es6_module_name)
    add_locale_imports_to_spec_files(es6_module_name)
end

def declare_undeclared_dependencies(dir)
    angular_module_name = get_angular_module_name(dir)
    files = get_files_that_inject_factories(dir)
    files += get_files_that_use_directives(dir)
    ensure_dependency_declared(angular_module_name, files)

end

def get_files_that_use_directives(dir)

    
    result =get_directives(dir).map do |directive|
        cmd = %Q~grep -rEl "#{directive}" vendor/front-royal~
        files_that_use_directive = `#{cmd}`.split("\n").reject { |f| f.match("spec.js")}
    end.flatten.uniq.compact
    
    # not sure why grep sometimes returns things with '//', so hacking arund it
    result.map{ |file| file.gsub('//', '/') }
    
end

def get_files_that_inject_factories (dir) 

    result = files_that_inject_factories = get_factories(dir).map do |factory|
        cmd = %Q~grep -rEl "\\.get\\('#{factory}'\\)" vendor/front-royal/~
        files_that_inject_factory = `#{cmd}`.split("\n").reject { |f| f.match("spec.js")}
    end.flatten.uniq.compact

    # not sure why grep sometimes returns things with '//', so hacking arund it
    result.map{ |file| file.gsub('//', '/') }
end

def ensure_dependency_declared(angular_module_name, files_that_depend_on_it)

    directories_that_depend_on_it = files_that_depend_on_it.map { |file| 
        if file == "vendor/front-royal/scripts/app.js" || 
            file.match('vendor/front-royal/scripts') || 
            file.match('vendor/front-royal/modules/ZipRecruiterSearch') ||
            file.match('AdminEditStudentNetworkEvent') ||
            file.match('vendor/front-royal/modules/Markdown') || 
            file.match('vendor/front-royal/deps.js') ||
            file.match('vendor/front-royal/styles/oreo') ||
            file.match('vendor/front-royal/components/spec_helper') || 
            file.match('vendor/front-royal/modules/DynamicLandingPage')
            
            return nil
        else
            match = file.match(/vendor\/front-royal\/[\w-]+\/[\w-]+\//)
            debugger if match.nil?
            match[0] 
        end
    }.uniq.compact

    directories_that_depend_on_it.each do |dir|
        
        begin
            _angular_module_name = get_angular_module_name(dir)
        rescue Exception => err
            debugger
        end

        # skip if this is the directory for the module itself
        next if angular_module_name == _angular_module_name
        module_file = get_script_that_defines_angular_module(dir)[0]
        dependencies = get_dependencies_for_angular_module_file(module_file)
        
        # skip if the dependency is already there
        next if dependencies.include?(angular_module_name)
        
        # We could probably figure this out, but I haven't bothered 
        if dependencies.empty?
            puts "*********** CANNOT auto-add dependency #{angular_module_name} to #{module_file}. Do it manually"
            return
        end

        contents = File.read(module_file)
        dependency_string = contents.match(/angular\s*\.module\('[^']+',\s*(\[[^\]]*\])/)[1]
        insert_after_dependency = dependencies.detect { |d| d < angular_module_name } || dependencies.first
        match = dependency_string.match(/(\s*)('#{insert_after_dependency}')(,?)/)
        full_match, whitespace_before, prev_dep, trailing_comma = match.to_a
        begin
            contents.gsub!(full_match, "#{full_match}#{trailing_comma ? '' : ','}#{whitespace_before}'#{angular_module_name}'#{trailing_comma}")
        rescue
            debugger
        end

        File.write(module_file, contents)
    end

end

def get_factories(dir)
    factories = []
    Dir.glob("#{dir}**/*.js").map do |file|
        next if file.match("spec.js")
        contents = File.read(file)
        match = contents.match(/factory\('([^']+)/)
        match && match[1]
    end.compact
end

def get_directives(dir)
    get_directives = []
    Dir.glob("#{dir}**/*.js").map do |file|
        next if file.match("spec.js")
        contents = File.read(file)
        match = contents.match(/directive\('([^']+)/)
        match && match[1].underscore.gsub('_', '-')
    end.compact
end


def mock_moving_modules(moved_modules, unmoved_modules, i)
    modules_to_move = []
    unmoved_modules.values.each do |entry|
        unmoved_dependencies = entry[:dependencies].select { |d| unmoved_modules[d] }
        if unmoved_dependencies.empty?
            modules_to_move << entry
        end
    end

    puts "Step #{i}: #{modules_to_move.pluck(:angular_module_name).sort.inspect}"

    modules_to_move.each do |entry|
        unmoved_modules.delete(entry[:angular_module_name])
        moved_modules[entry[:angular_module_name]] = entry
    end


end

def add_template_imports(es6_module_name)
    Dir.glob("vendor/front-royal/modules/#{es6_module_name}/angularModule/scripts/**/*_dir.js").each do |filepath|
        contents = File.read(filepath)
        matcher = /templateUrl:\s*'([^']+)'.+$/
        match = contents.match(matcher)
        next unless match
        template = match[1]
        relative_path = template.split('views/')[1]
        lines = contents.split("\n")
        insert_after_line = lines.select { |l| l.match(/^import/) }.last
        index = lines.index(insert_after_line)
        index = index || -1
        lines.insert(index + 1, "import template from '#{es6_module_name}/angularModule/views/#{relative_path}';")
        contents = lines.join("\n")+"\n"
        contents.gsub!(matcher, 'template,')
        File.write(filepath, contents)
    end
end

def add_locale_imports_to_spec_files(es6_module_name)
    Dir.glob("vendor/front-royal/modules/#{es6_module_name}/angularModule/spec/**/*_dir_spec.js").each do |spec_filepath|
        basename = File.basename(spec_filepath).gsub('_dir_spec.js', '')
        locale_filename =  "#{basename}-en.json"
        locale_filepath = Dir.glob("vendor/front-royal/modules/#{es6_module_name}/locales/**/#{locale_filename}").first
        next unless locale_filepath
        
        contents = File.read(spec_filepath)
        locale_var_name = "#{basename}_locales".camelize(:lower)
        next if contents.match(locale_var_name)

        lines = contents.split("\n")
        insert_after_line = lines.select { |l| l.match(/^import/) }.last
        index = lines.index(insert_after_line)

        # only add trailing whitespace if there is not already an empty line after the import statements
        trailing_whitespace = lines[index + 1] == '' ? nil : ''
        index = index || -1
        relative_path = locale_filepath.split('locales/')[1]

        newlines = [
            "import setSpecLocales from 'Translation/setSpecLocales';",
            "import #{locale_var_name} from '#{es6_module_name}/locales/#{relative_path}';",
            "",
            "setSpecLocales(#{locale_var_name});",
            trailing_whitespace
        ].compact
        lines.insert(index + 1, newlines)
        contents = lines.join("\n")+"\n"
        File.write(spec_filepath, contents)
    end

end

def add_import_statements_to_spec_files(angular_module_name, es6_module_name)
    import_statements = [
        "import 'AngularSpecHelper';",
        "import '#{es6_module_name}/angularModule';"
    ]
    Dir.glob("vendor/front-royal/modules/#{es6_module_name}/angularModule/spec/**/*spec.js").each do |spec_file|
        lines = File.read(spec_file).split("\n")
        lines_to_add = import_statements.reject { |import_statement| lines.include?(import_statement) }
        next unless lines_to_add.any?
        lines_to_add << ''
        lines.insert(0, *lines_to_add)
        File.write(spec_file, lines.join("\n")+"\n")
    end
end

def add_import_statements_to_dependent_modules(angular_module_name, es6_module_name)
    import_statement = "import '#{es6_module_name}/angularModule';"

    #We could use get_dependent_modules(angular_module_name) here, but it works,
    # so leaving it
    get_all_files_that_define_angular_modules.select do |file|
        dependencies = get_dependencies_for_angular_module_file(file)
        if dependencies.include?(angular_module_name)
            lines = File.read(file).split("\n")
            next if lines.include?(import_statement)
            insert_after_line = lines.select { |l| l.match(/^import/) &&  l < import_statement }.last
            index = lines.index(insert_after_line)
            index = index || -1
            lines.insert(index + 1, import_statement)
            File.write(file, lines.join("\n")+"\n")
        end        
    end
end

def get_dependent_modules(angular_module_name)
    module_files = get_all_files_that_define_angular_modules.select do |file|
        dependencies = get_dependencies_for_angular_module_file(file)
        dependencies.include?(angular_module_name)
    end

    module_files.map do |file|
        angular_module_name = get_angular_module_name(file)
        [angular_module_name, file]
    end
end

def get_dependencies_for_angular_module_file(file)
    contents = File.read(file)
    match = contents.match(/angular\s*\.module\('[^']+',\s*(\[[^\]]*\])/)
    dependencies = ActiveSupport::JSON.decode(match[1].gsub(/'/, '"'))
end

def get_all_files_that_define_angular_modules
    script_directories = Dir.glob('vendor/{front-royal/components/*/scripts,common/components/*/scripts,front-royal/modules/*/angularModule/scripts}')
    
    matchers = [
        /_module.js/,
        /\/dialog_modal.js/,
        /\/oo_directive.js/,
        /\/drag_and_drop.js/,
        /\/timeout_helpers_module.js/,
        /\/formats_text.js/,
        /\/front_royal_api_error_handler.js/,
        /\/porter-stemmer.js/,
        /\/angular_http_queue_and_retry/,
        /\/TelephoneModule/
    ]

    scripts = script_directories.map do |dir|
        scripts = Dir.glob("#{dir}/**/*.js")
        module_script = if scripts.size == 1
            scripts[0]
        else
            result = nil
            matchers.detect do |matcher|
                matched = scripts.grep(matcher)
                result = matched.first
            end
            raise "no module file in #{dir}" unless result
            result
        end
        module_script
    end

    scripts.reject do |script|
        # these are not angular modules
        script.match(/frame-rpc.js/) ||
        script.match(/oo_directive.js/) 
    end
end

def get_angular_module_name(dir_or_file)
    if dir_or_file.ends_with?('.js')
        module_script = dir_or_file
    else
        module_script, other_scripts = get_script_that_defines_angular_module(dir_or_file)
    end
    raise "No module script found in #{dir_or_file}" unless module_script
    File.read(module_script).match(/angular\s*\.module\('([^\']+)'/)[1]
end

def get_script_that_defines_angular_module(dir)
    scripts = Dir.glob("#{dir}scripts/**/*.js") +  Dir.glob("#{dir}angularModule/scripts/**/*.js")
    
    return [nil, []] unless scripts&.any?
    
    module_script = (scripts & get_all_files_that_define_angular_modules).first
    [module_script, scripts - [module_script]]
end

def move_component_files_to_modules(dir, old_name, new_name)
    
    
    base_module_dir = "vendor/front-royal/modules/#{new_name}/"
    module_angular_dir = "#{base_module_dir}angularModule/"
    FileUtils.mkdir_p(module_angular_dir)

    module_script, other_scripts = get_script_that_defines_angular_module(dir)

    raise dir if module_script.nil?
    puts "\n\n#{module_script}"

    File.open("#{module_angular_dir}index.js", 'w+') do |f|
        f.puts "import '#{relative_script_path(module_script)}';"
        f.puts "" if other_scripts.any?
        other_scripts.each do |script|
            f.puts "import '#{relative_script_path(script)}';"
        end
    end

    old_locales_dir = "#{dir}locales"
    if File.exist?(old_locales_dir)
        new_locales_dir = "#{base_module_dir}locales/#{old_name}"
        `mkdir -p #{new_locales_dir}`
        `git mv #{old_locales_dir}/* #{new_locales_dir}`
        `rm -rf #{old_locales_dir}`
    end

    `git mv #{dir}* #{module_angular_dir}`
    `rm -r #{dir}`
end

def relative_script_path(path)
    rest = path.split('scripts/')[1]
    "./scripts/#{rest}".gsub('.js', '')
end