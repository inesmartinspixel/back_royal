require 'csv'
require 'net/http'

namespace :ip do
    desc "summarize IP addresses and output geolocation + spamhaus information"
    task :summarize_by_class_b => :environment do

        require 'charon'

        # This script takes a CSV that looks like the following (header required)...
        #
        #    ip_address
        #    117.29.198.145
        #    110.83.120.220
        #    27.150.137.195
        #    ...
        #
        # ...and outputs rows like so...
        #
        #    range,unique_ips,success,COUNTRY,COUNTRY CODE,REGION CODE,REGION NAME,CITY,ZIP CODE,LATITUDE,LONGITUDE,TIME ZONE,ISP NAME,ORGANIZATION NAME,AS NUMBER / NAME,IP ADDRESS USED FOR QUERY,blocklist
        #    27.150.0.0/16,1,success,China,CN,FJ,Fujian,Fuzhou,,26.0614,119.3061,Asia/Shanghai,"China Telecom fujian","China Telecom fujian","AS4134 No.31,Jin-rong Street",27.150.0.0,
        #    49.67.0.0/16,1,success,China,CN,JS,Jiangsu,Nanjing,,32.0617,118.7778,Asia/Shanghai,"China Telecom jiangsu","China Telecom jiangsu","AS4134 No.31,Jin-rong Street",49.67.0.0,[11, "Policy Block List"]
        #    110.83.0.0/16,1,success,China,CN,FJ,Fujian,Fuzhou,,26.0614,119.3061,Asia/Shanghai,"China Telecom fujian","China Telecom fujian","AS4134 No.31,Jin-rong Street",110.83.0.0,[11, "Policy Block List"]
        #    ...
        #
        # This data can be used to check where an attack is coming from, assuming you have the raw IP list.
        #
        # A convenient way to get the raw IPs from an attack is to use GetSentry and view the "Users" page for a Rails event, then download the CSV.

        path = Rails.root.join('tmp', 'ip_address.csv')
        class_a_groups = {}
        class_b_groups = {}
        class_c_groups = {}

        CSV.foreach(path, headers: true, encoding: 'utf-8') do |row|
            ip = row['ip_address']
            a = ip.split('.')[0,1].join('.')
            b = ip.split('.')[0,2].join('.')
            c = ip.split('.')[0,3].join('.')

            class_a_groups[a] ||= []
            class_b_groups[b] ||= []
            class_c_groups[c] ||= []

            class_a_groups[a] << ip
            class_b_groups[b] << ip
            class_c_groups[c] << ip
        end

        # Use the code below to get roll-up counts of how many IPs are in each class
        #
        # puts "Class A group totals:"
        # class_a_groups.keys.sort_by(&:to_i).each do |k|
        #     puts " * #{k}.0.0.0/8   -->   #{class_a_groups[k].length}"
        # end

        # puts "\n\nClass B group totals:"
        # class_b_groups.keys.sort_by(&:to_i).each do |k|
        #     lookup = Net::HTTP.get('ip-api.com', "/csv/#{k}.0.0")
        #     puts " * #{k}.0.0/16  -->   #{class_b_groups[k].length} :: #{lookup}"
        # end

        # puts "\n\nClass C group totals:"
        # class_c_groups.keys.sort_by(&:to_i).each do |k|
        #     puts " * #{k}.0/24  -->   #{class_c_groups[k].length}"
        # end


        # Output CSV that includes geolocation info + whether the IP range is in the Spamhaus ZEN registry for misbehavior
        puts "range,unique_ips,success,COUNTRY,COUNTRY CODE,REGION CODE,REGION NAME,CITY,ZIP CODE,LATITUDE,LONGITUDE,TIME ZONE,ISP NAME,ORGANIZATION NAME,AS NUMBER / NAME,IP ADDRESS USED FOR QUERY,blocklist"
        class_b_groups.keys.sort_by(&:to_i).each do |k|
            ip_lookup = Net::HTTP.get('ip-api.com', "/csv/#{k}.0.0")
            blocklist_lookup = Charon.query("#{k}.0.0")
            puts "#{k}.0.0/16,#{class_b_groups[k].length},#{ip_lookup},#{blocklist_lookup}"
        end
    end
end