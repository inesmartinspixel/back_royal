# To run, simply execute this script with no arguments.
# It will show errors if any of the IPs are already added to the groups, but continue on without stopping.
#
# What does this script do?
#
# 1. Adds an entry to each EB VPC security group to setup the SSH bastion tunnel.
# 2. Removes any global security group setting for public access to SSH.
#
# Why do we do want to do this?
#
# The default AWS security group config allows 0.0.0.0/0 to port 22 on the EC2 instance (the whole world).
# That means that if someone finds the IP address(es) of our load balancer, they could circumvent attempt
# to connect to SSH without coming from a known bastion user / IP combo.
#
# To avoid this, we want to remove the default 0.0.0.0/0 rule from the VPC security groups and only allow requests to port 80
# for the whitelisted bastion internal IP.


# This helper function adds a set of ingress rules
function setupVpcSecurityGroup() {

    echo ""
    echo "Setting up" $2 "VPC security group..."
    echo " > IPv4..."
    aws ec2 authorize-security-group-ingress --profile $1 --group-id $3 --protocol tcp --port 22 --cidr 10.30.1.100/32
    aws ec2 revoke-security-group-ingress --profile $1 --group-id $3 --protocol tcp --port 22 --cidr 0.0.0.0/0
}

# Here are the actual Load Balancers we want to secure
setupVpcSecurityGroup "tools" "metabase" "sg-0c3a4913db364a51d"
setupVpcSecurityGroup "staging" "staging" "sg-06061b5ebc0064f1f"
setupVpcSecurityGroup "production" "production" "sg-0dd91d75a16b78db0"


echo ""
echo ""
echo ""
echo "================================================"
echo "                    ALL DONE!"
echo "================================================"
echo ""
echo "At this point, you probably want to go into the EC2 Security Groups and..."
echo ""
echo " 1. Make sure that there there is only one SSH entry for the bastion server, in each VPC security group"
echo " 2. Ensure that you have performed similar steps for the load balancers via add_cloudflare_ips.sh"