namespace :content_localization do

    desc "run data migrations after rollout_1"
    task :rollout_1_migrations => [
        :migrate_content_items,
        :update_favorites_and_active_playlists_for_locale_packs,
        :update_lesson_progress # must come after migrate_content_items
    ]

    desc "add locale pack ids to lesson_progress"
    task :update_lesson_progress => :environment do

        puts "Updating progress tables"

        {
            LessonProgress => 'lessons',
            Lesson::StreamProgress => 'lesson_streams'
        }.each do |progress_klass, content_table|
            issues = ActiveRecord::Base.connection.execute("
                SELECT user_id, email, #{content_table}.locale_pack_id, array_agg(#{content_table}.title) titles from
                    #{progress_klass.table_name}
                        join #{content_table} on #{progress_klass.table_name}.#{content_table.singularize}_id = #{content_table}.id
                        join users on users.id = #{progress_klass.table_name}.user_id
                GROUP BY user_id, email, #{content_table}.locale_pack_id
                HAVING COUNT(*) > 1
            ").to_a

            # pp issues
            # puts "Deleting #{issues.size} duplicate #{progress_klass.table_name.singularize} records.  Press c to continue"
            # debugger

            issues.each do |issue|
                user_id = issue['user_id']
                locale_pack_id = issue['locale_pack_id']

                progress_records = progress_klass
                    .joins(:"#{content_table.singularize}")
                    .where("#{progress_klass.table_name}.user_id = '#{user_id}'")
                    .where("#{content_table}.locale_pack_id = ?", locale_pack_id)
                    .reorder('completed_at desc').to_a


                progress_records.pop
                progress_records.map(&:destroy)
            end
        end

        ActiveRecord::Base.connection.execute("
            update lesson_progress
                set locale_pack_id=(SELECT locale_pack_id from lessons where id=lesson_progress.lesson_id);

            update lesson_streams_progress
                set locale_pack_id=(SELECT locale_pack_id from lesson_streams where id=lesson_streams_progress.lesson_stream_id);
        ")

        if LessonProgress.where(locale_pack_id: nil).exists?
            raise "Still have lesson_progress records with no locale packs"
        end

        if Lesson::StreamProgress.where(locale_pack_id: nil).exists?
            raise "Still have lesson_stream_progress records with no locale packs"
        end
    end

    desc "Run the migration necessary after implementing locale_pack stuff to update favorite streams and active playlist ids"
    task :update_favorites_and_active_playlists_for_locale_packs => :environment do

        puts "Updating favorite streams"
        ActiveRecord::Base.connection.execute(%Q|
            INSERT INTO lesson_stream_locale_packs_users (user_id, locale_pack_id) (
                SELECT
                    distinct lesson_streams_users.user_id, lesson_streams.locale_pack_id
                FROM lesson_streams_users
                    JOIN lesson_streams
                        ON lesson_streams_users.lesson_stream_id = lesson_streams.id
                    LEFT JOIN lesson_stream_locale_packs_users
                        ON lesson_stream_locale_packs_users.user_id = lesson_streams_users.user_id
                        AND lesson_stream_locale_packs_users.locale_pack_id = lesson_stream_locale_packs_users.locale_pack_id
                WHERE lesson_stream_locale_packs_users.locale_pack_id IS NULL
            )
        |)
        
        puts "Updating active playlists"
        ActiveRecord::Base.connection.execute(%Q|
            UPDATE users SET active_playlist_locale_pack_id = (SELECT locale_pack_id from playlists where id = active_playlist_id);
        |)

    end

    desc "Use the VersionMigrator to change existing comment.  Requires editor downtime"
    task :migrate_content_items => :environment do
        [Lesson::Stream, Lesson, Playlist].each do |klass|

            klass::LocalePack.includes(:content_items).each do |locale_pack|
                if !locale_pack.content_items.map(&:locale).include?('en')
                    raise "Cannot have locale packs without english items"
                end
            end

            working_versions = {}

            superviewer = AccessGroup.find_by_name('SUPERVIEWER')

            groups_by_locale_pack_id = Hash.new { |h,k| h[k] = Set.new }

            fields = ['locale', 'locale_pack_id']
            if klass == Playlist
                fields << 'stream_entries'
            end

            VersionMigrator.new(klass).select(*fields).each do |content_item|

                if !content_item.is_a?(klass::Version)

                    # if this is the working version

                    # we cannot published versions without a locale pack.  We also cannot
                    # have locale packs without an English version.  So there is no way for
                    # us to know what to do with a published non-English version.  Convert
                    # it to English
                    if content_item.locale != 'en' && content_item.has_published_version? && content_item.locale_pack_id.nil?
                        content_item.locale = 'en'
                    end

                    # For all English versions, published or not, ensure a locale pack
                    if content_item.locale == 'en' && !content_item.locale_pack_id
                        locale_pack = klass::LocalePack.create!
                        content_item.locale_pack = locale_pack
                    end

                    if content_item.locale_pack && klass != Lesson
                        # append groups, so in the end we have all the groups from all the translations for this locale pack
                        groups_by_locale_pack_id[content_item.locale_pack_id] += content_item.access_groups.map(&:id)
                        groups_by_locale_pack_id[content_item.locale_pack_id] << superviewer.id
                    end

                    # store the locale and locale_pack_id for each working version
                    # so we can copy it back to all previous versions
                    working_versions[content_item.id] = {
                        locale: content_item.locale,
                        locale_pack_id: content_item.locale_pack_id
                    }
                else

                    # if this is an old version, set the locale and locale_pack_id to
                    # match current
                    working_version = working_versions[content_item.attributes['id']]
                    # deleted items have no working version
                    if working_version
                        content_item.locale = working_version[:locale]
                        content_item.locale_pack_id = working_version[:locale_pack_id]
                    end

                end

                if klass == Playlist
                    playlist = content_item
                    playlist.stream_entries.each do |stream_entry|
                        locale_pack_id = Lesson::Stream.where(id: stream_entry['stream_id']).pluck('locale_pack_id').first
                        raise "streams in playlists must have locale_packs" if locale_pack_id.nil?
                        stream_entry['locale_pack_id'] = locale_pack_id
                    end
                end


            end

            if groups_by_locale_pack_id.any?
                # In this branch, we have not yet setup ActiveRecord to know about the relationship
                # between locale packs and groups, so we have to build the query ourselves.  Saves time
                # anyway
                query = "INSERT INTO access_groups_#{klass.table_name.singularize}_locale_packs (locale_pack_id, access_group_id) VALUES "
                query += groups_by_locale_pack_id.map do |locale_pack_id, group_ids|
                    group_ids.map do |group_id|
                        "('#{locale_pack_id}', '#{group_id}')"
                    end
                end.flatten.join(',')

                ActiveRecord::Base.connection.execute("TRUNCATE access_groups_#{klass.table_name.singularize}_locale_packs")
                ActiveRecord::Base.connection.execute(query)
            end
        end
    end
end