require 'zip'

namespace :sign_now do

    ##############################
    ## Subscriptions
    ##############################

    desc "Subscribe to an event. ex `url=http://path/to/endpoint event=document.complete rake sign_now:subscribe_to_event"
    task :subscribe_to_event => :environment do
        existing_subscription = SignNow.list_subscriptions.first
        if existing_subscription
            raise "You will delete the following subscription by running this. Run unsubscribe_from_event first if you want to do that: #{existing_subscription.inspect}"
        end
        SignNow.subscribe_to_event(ENV['event'], ENV['url'])
        list_subscriptions
    end

    desc "Unsbscribe from an event. ex `url=http://path/to/endpoint event=document.complete rake sign_now:unsubscribe_from_event"
    task :unsubscribe_from_event => :environment do
        SignNow.unsubscribe_from_event(ENV['event'], ENV['url'])
        list_subscriptions
    end

    def list_subscriptions
        subscriptions = SignNow.list_subscriptions

        puts ""
        puts "Completed updating subscriptions."
        puts ""

        if subscriptions.empty?
            puts "Current subscriptions: None"
        else
            puts "Current subscriptions:"
            subscriptions.each do |subscription|
                puts " - #{subscription['event']} | #{subscription['callback_url']}"
            end
        end
        puts ""
    end

    ##############################
    ## DocuSign Backfill
    ##############################

    desc "Backfill DocuSign documents into SignableDocument entries. ex - `rake sign_now:docusign_backfill`"
    task :docusign_backfill => :environment do
        Zip::File.open(Rails.root.join('tmp', 'CompletedDocuSignDocs.zip')) do |zip_file|
            Dir.mktmpdir do |tmpdir|

                zip_file.glob('**/*.pdf').each do |entry|

                    next if entry.name.starts_with?("__MACOSX")

                    # grab filename / email from entry
                    file_name = entry.name.split('/').last
                    email = file_name.split('.pdf')[0]

                    # try to determine user
                    user = User.find_by_email(email)
                    if !user

                        puts "Cannot find user for email: #{email} - Manually check version history !!!"
                        next

                        # This was exceeding statement_timeout on prod, so for now, we're just logging
                        # user_version = User::Version.where(email: email).reorder('version_created_at DESC').limit(1).first
                        # user_id = user_version && user_version.attributes['id']
                        # user = User.find_by_id(user_id) unless !user_id
                        # if !user
                        #     puts "Cannot find user for email: #{email} !!!"
                        #     next
                        # end
                    end

                    # skip if already exists
                    found_document = SignableDocument.where(user_id: user.id, document_type: SignableDocument::TYPE_ENROLLMENT_AGREEMENT).first
                    if found_document
                        if !found_document.signed_at
                            puts "Document already found for #{user.email}, but NOT SIGNED. Skipping ..."
                        else
                            puts "Document already found for #{user.email}. Skipping ..."
                        end
                        next
                    end

                    # Determine relevant application and program type
                    statuses = ['deferred', 'pre_accepted', 'accepted', 'expelled']
                    relevant_application = user.cohort_applications.includes(:cohort).select do |ca|
                        statuses.include?(ca.status)
                    end.sort_by(&:applied_at).last

                    if !relevant_application
                        puts "No application found for signing application for #{user.email} !!!"
                        next
                    end
                    program_type = relevant_application.program_type

                    # generate a File and SignableDocument for the given content
                    file_path = File.join(tmpdir, file_name)
                    entry.extract(file_path) # unfortunately, this returns a ZipEntry, not a File
                    document_file = File.open(file_path, 'r'); # ... so, open it back up
                    SignableDocument.create!(
                        document_type: SignableDocument::TYPE_ENROLLMENT_AGREEMENT,
                        user_id: user.id,
                        metadata: {
                            program_type: program_type
                        },
                        signed_at: Time.now,
                        file: document_file
                    )

                end
            end
        end
    end


end