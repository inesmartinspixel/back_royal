namespace :test do

    task :build_fixtures do
        require 'puppet_db_config'
        db = PuppetDbConfig.enabled? ? PuppetDbConfig.database : 'back_royal_test'
        puts "Building fixtures for #{db}"
        require File.expand_path("../../../spec/spec_helper", __FILE__)

        start = Time.now
        if PuppetDbConfig.enabled?
            FixtureBuilder::PuppetFixtureBuilder.create_fixtures
        else
            FixtureBuilder::RubyFixtureBuilder.create_fixtures
        end

        puts "#{(Time.now - start).round} seconds to build fixtures"
    end

    task :load_fixtures do
        ENV['FIXTURES_PATH']='spec/fixtures/'
        ENV['RAILS_ENV']='test'

        Rake::Task['db:fixtures:load'].invoke

        # fixturies does not support multiple connections,
        # so tests have to buidl redshift events themselves
        RedshiftEvent.connection.execute('truncate events')

        Rake::Task['db:refresh_materialized_views'].invoke
    end

    task :setup do

        ENV['RAILS_ENV'] = 'test'
        require 'puppet_db_config'
        db = PuppetDbConfig.enabled? ? PuppetDbConfig.database : 'back_royal_test'

        puts "re-creating #{db}"
        # do not use rake db:drop because it fails if the db does not exist
        # There has to be a block here. Otherwise the code will not wait for the delete to finish
        Open3.popen3("dropdb -h localhost #{db}") { |stdin, stdout, stderr, wait_thr|
            err = stderr.read
            if !err.blank? && !err.match("does not exist")
                raise err
            end
        }

        puts "db:create ..."
        invoke_silently("db:create")

        puts "db:structure:load ..."
        invoke_silently("db:structure:load")

        # this needs to be done after the structure load but before db:migrate
        # to make sure that any views that are created with data are populated
        # in case another migration needs them to be
        puts "db:ensure_all_materialized_views_populated ..."
        invoke_silently("db:ensure_all_materialized_views_populated")

        puts "db:migrate ..."
        invoke_silently("db:migrate")

        begin
            ActiveRecord::Base.descendants.each(&:reset_column_information)
        rescue ActiveRecord::NoDatabaseError => err
            if err.message.match(/red_royal/)
                raise "No red_royal_test db. run `rake test:redshift:setup` first"
            else
                raise
            end
        end
        ActiveRecord::Base.connection.schema_cache.clear!

        # build_fixtures does not work here because of things that have been
        # cached at a global level in the process of running migrations.  Tried
        # reset_column_information and connection.schema_cache.clear!.  It helped,
        # but there were still issues
        #
        # Same with redshift setup
    end

    task :"redshift:setup" do
        puts "re-creating test redshift db"
        # do not use rake db:drop because it fails if the db does not exist
        # There has to be a block here. Otherwise the code will not wait for the delete to finish
        Open3.popen3('dropdb -h localhost red_royal_test') { |stdin, stdout, stderr, wait_thr| }
        Open3.popen3('createdb -h localhost red_royal_test') { |stdin, stdout, stderr, wait_thr| }

        # Alias the Postgres NOW function to GETDATE for Redshift parity
        Open3.popen3('psql -h localhost -c "CREATE FUNCTION getdate() RETURNS timestamp with time zone VOLATILE AS \$\$ SELECT NOW() \$\$ LANGUAGE SQL" red_royal_test') { |stdin, stdout, stderr, wait_thr| }

        puts "Running redshift migrations"
        invoke_silently("redshift:db:migrate")


    end

    namespace :puppet do |ns|
        task :setup do
            Rake::Task['test:setup'].invoke

            # this will set up the puppetmaster role (which is used when connecting
            # directly to the db from a puppeteer spec) from developer machines.  On ci,
            # we have to run this task separately in restore_db, since roles are not included
            # in pgdump
            Rake::Task['test:puppet:ensure_puppetmaster_role'].invoke
        end

        task :migrate do
            Rake::Task['db:migrate'].invoke
        end

        task :build_fixtures do
            Rake::Task['test:build_fixtures'].invoke
        end

        task :ensure_puppetmaster_role do
            Rake::Task['environment'].invoke
            begin
                ActiveRecord::Base.connection.execute('create role puppetmaster;')
            rescue => exception
                raise unless exception.message.match(/already exists/)
            end

            ActiveRecord::Base.connection.execute('
                ALTER ROLE puppetmaster WITH LOGIN;
                GRANT CONNECT ON DATABASE back_royal_puppet TO puppetmaster;
                GRANT USAGE ON SCHEMA public TO puppetmaster;
                GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO puppetmaster;
                GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO puppetmaster;
            ');
        end

        # append and prepend proper tasks to all the tasks defined here above
        ns.tasks.each do |task|
            task.enhance ["test:USE_PUPPET_DB"] do
                Rake::Task["test:stop_using_puppet_db"].invoke
            end
        end
    end

    task :USE_PUPPET_DB do
        ENV['USE_PUPPET_DB'] = 'true'
    end

    task :stop_using_puppet_db do
        # If you have already configured the rails app, unsetting this environment variable
        # is not going to switch you back to the default connection
        ENV.delete('USE_PUPPET_DB')
    end

end

def invoke_silently(task)
    silently do
        Rake::Task[task].invoke
    end
end

def silently(&block)
    orig = $stdout
    $stdout = StringIO.new
    yield
ensure
    $stdout = orig
end

Rake.application.instance_variable_get('@tasks').delete('db:test:prepare')
namespace 'db' do
    namespace 'test' do
        task 'prepare' do
            # DO NOTHING! We don't actually use structure.sql since it's never properly
            # exported by db:migrate via postgres adapter. Specifically, we've seen
            # schema ownership excluded from DDL.

            # See also: https://github.com/rails/rails/issues/23202
        end
    end
end