
require 'csv'
require 'unicode'

namespace :user do

    desc "create or modify user for careers access"
    task :careers => :environment do
        # See if email was passed in
        email = ENV['email']

        # If not just create based on the time
        if email.nil?
            email = "test#{Time.now.to_i}@pedago.com"
        end

        # Try to find the user
        user = User.find_by_email(email)

        # Create a new user if one doesn't exist
        if user.nil?
            user = User.create!({
                uid: email,
                email: email,
                name: email.split("@").first,
                created_at: Time.now,
                updated_at: Time.now,
                password: "password",
                password_confirmation: "password"
            })
        end

        # Do the necessary steps for them to have career networks access
        cohort_application = CohortApplication.create!({
            user_id: user.id,
            cohort_id: Cohort.first.id,
            status: 'accepted',
            applied_at: Time.now
        })
        user.cohort_applications = [cohort_application]
        user.save!

        # Log the user that now has careers access
        puts "#{user.email}"
    end

    desc "complete all published streams for user eg (rake user:complete_all_streams[user@doman.com]"
    task :complete_all_streams, [:arg1] => :environment do |t, args|
        unless Rails.env == 'development' or Rails.env == 'staging'
            raise "Not in production"
        end

        user = User.find_by_email(args.first)

        unless !user.nil?
            raise "User does not exist"
        end

        stream_locale_pack_ids = Lesson::Stream::ToJsonFromApiParams.new({
            user_id: user.id,
            filters: {
                in_users_groups: true,
                in_users_locale_or_en: true,
                published: true
            },
            fields: ['locale_pack']
        }).to_a.map { |e| e['locale_pack']['id']}

        complete_streams(user, stream_locale_pack_ids)
    end

    desc "complete all published playlists for user"
    task :complete_all_playlists, [:arg1] => :environment do |t, args|
        unless Rails.env == 'development' or Rails.env == 'staging'
            raise "Not in production"
        end

        user = User.find_by_email(args.first)

        unless !user.nil?
            raise "User does not exist"
        end

        stream_entries = Playlist::ToJsonFromApiParams.new({
            user_id: user.id,
            filters: {
                in_users_groups: true,
                in_users_locale_or_en: true,
                published: true
            },
            fields: ['stream_entries']
        }).to_a.map { |p| p['stream_entries'] }.flatten
        stream_locale_pack_ids = stream_entries.map { |entry| entry['locale_pack_id'] }.flatten.uniq

        complete_streams(user, stream_locale_pack_ids)
    end

    desc "complete foundations for user"
    task :complete_foundations, [:arg1] => :environment do |t, args|
        unless Rails.env == 'development' or Rails.env == 'staging'
            raise "Not in production"
        end

        user = User.find_by_email(args.first)

        unless !user.nil?
            raise "User does not exist"
        end

        complete_streams(user, user.get_foundations_playlist.stream_entries.map { |entry| entry['locale_pack_id']}.flatten.uniq)
    end

    task :trigger_transfer_progress_from_deferred_or_expelled_cohort => :environment do
        ids = ENV['ids'].split(',')
        User.where(id: ids).each do |user|
            event = user.last_application&.transfer_progress_from_deferred_or_expelled_cohort
            pp event
        end
    end

    # This rake task is similar to the one above it, but is useful for ad-hoc progress transfers
    # and it doesn't create server events. This is very useful if you need to transfer stream
    # progress for midterm or final exams for a user but don't want to trigger Customer.io emails.
    desc "Transfer stream progress for a user from one equivalent stream to another"
    task :transfer_stream_progress_for_equivalent_streams => :environment do
        user_id = ENV['user_id']
        raise "Must supply a user_id" unless user_id

        old_locale_pack_id = ENV['old_locale_pack_id']
        new_locale_pack_id = ENV['new_locale_pack_id']
        raise "Must supply an old_locale_pack_id and a new_locale_pack_id" unless old_locale_pack_id && new_locale_pack_id

        force_if_new_progress = ENV['force_if_new_progress'] == 'true'

        user = User.find(ENV['user_id'])
        equivalent_locale_pack_ids = [old_locale_pack_id, new_locale_pack_id]
        user.last_application.send(:transfer_stream_progress_for_equivalent_streams, equivalent_locale_pack_ids, force_if_new_progress)
    end

    desc "set all passwords to 'password'"
    task :set_fake_passwords => :environment do
        unless Rails.env == 'development'
            raise "Only development mode"
        end

        users = User.all
        users.each_with_index do |user, i|
            user.password = user.password_confirmation = 'password'
            user.save!
            puts "#{i+1} of #{users.size} users complete"
        end
    end

    desc "add/update all users in mailchimp"
    task :update_all_in_mailchimp => :environment do
        users = User.all
        users.each_with_index do |user, i|
            EmailService.new({
                user: user,
                include_groups: true,
                include_subscriptions: true
            }).subscribe_or_update
            puts "#{i+1} of #{users.size} users complete"
        end
    end

    desc "add/update all users in mailchimp"
    task :update_in_mailchimp => :environment do
        email = ENV['email']
        if email.nil?
            puts "You must specify email=somebody@pedago.com"
        end
        user = User.find_by_email(email)
        if !user.nil?
          EmailService.new({
            user: user,
            include_groups: true,
            include_subscriptions: true
          }).subscribe_or_update

        else
            puts "User #{email} not found."
        end

    end

    desc "add MBA favorites for BLUEOCEAN users"
    task :add_mba_favorites_for_blueocean_users => :environment do

        stream_ids = AppConfig.all['mba_favorites']
        streams = Lesson::Stream.where(id: stream_ids)

        users = all_blue_ocean_learners
        total = users.count
        users.each_with_index do |user, i|
            streams.each do |stream|
                user.favorite_lesson_streams << stream unless user.favorite_lesson_streams.include?(stream)
            end
            if (i+1) % 100 == 0
                puts "processed #{i+1} of #{total} users"
            end
        end

        puts "processed #{total} users"

    end

    desc "check customer.io"
    task :check_customer_io => :environment do

        path = Rails.root.join('tmp', 'customers.csv')
        customer_io_users = {}
        CSV.foreach(path, headers: true, encoding: 'utf-8') do |row|
            customer_io_users[row["id"]] = row
        end

        problems = Hash.new { |h, k| h[k] = {missing: [], no_first_name: [], good: []} }

        User.where('created_at > ?', Time.parse('2015/08/1')).each do |user|
            customer_io_user = customer_io_users[user.id]
            period = user.created_at.beginning_of_day.strftime('%Y-%m-%d')
            if customer_io_user.nil?
                problems[period][:missing] << user
            elsif customer_io_user['firstName'].nil?
                problems[period][:no_first_name] << user
            else
                problems[period][:good] << user
            end
        end

        emails = Set.new

        problems.keys.sort.each do |period|
            puts "period #{period}"
            puts " - missing #{problems[period][:missing].size}"
            puts " - no_first_name #{problems[period][:no_first_name].size}"
            puts " - good #{problems[period][:good].size}"
        end

        puts "--------------------------"
        problems.keys.sort.each do |period|
            puts "period #{period}"
            puts " - missing #{problems[period][:missing].map(&:email).inspect}"
            puts " - no_first_name #{problems[period][:no_first_name].map(&:email).inspect}"
            emails += problems[period][:missing].map(&:email)
            emails += problems[period][:no_first_name].map(&:email)
        end

        puts emails.map { |e| "'#{e}'" }.join(', ')

    end

    # NOTE: This backfill process updates users by using the update_all method, which is a single SQL statement executed
    # directly at the database level, which means ActiveRecord validations and callbacks are skipped. This is intentional
    # so we don't enqueue a bunch of IdentifyUserJob delayed jbos that would block up the queue, preventing organically
    # triggered IdentifyUserJob delayed jobs that may be needed for a Customer.io email campaign from running. If you
    # would like to re-identify users that have had their timezone backfilled, you can run the identify_all_users rake
    # task (see events.rake), which should be configured to enqueue IdentifyUserJob delayed jobs with a lower priority
    # than IdentifyUserJob delayed jobs that are triggered organically in the wild, so they won't block up the queue.
    desc "Backfill timezone for all users"
    task :backfill_timezone => :environment do
        user_ids = []

        # Create an array containing all of the unique place_id values so we can batch update the timezone for users.
        # NOTE: Currently (06/28/2018), career profiles and hiring applications are the only models that support a place_id.
        career_profile_place_ids = CareerProfile.select(:place_id).distinct.where.not(place_id: nil).pluck(:place_id)
        hiring_application_place_ids = HiringApplication.select(:place_id).distinct.where.not(place_id: nil).pluck(:place_id)
        uniq_place_ids = (career_profile_place_ids + hiring_application_place_ids).uniq

        # for each unique place_id, get the timezone for the place_id and update the timezone for all
        # users with a career_profile or hiring_application with a matching place_id to this timezone
        uniq_place_ids.each do |place_id|
            record = nil
            timezone = nil

            # get a career_profile or hiring_application that has a place_id that matches the place_id
            # in question so we can lookup the timezone
            [CareerProfile, HiringApplication].each do |klass|
                record = klass.where(place_id: place_id).limit(1).first
                break if record
            end

            # Since each place_id in the uniq_place_ids array was directly from the database,
            # we expect a record to be found for the place_id in question. In the unlikely
            # even that no record is found, raise an error so we can investigate later.
            unless record
                puts "No career_profile or hiring_application found with a place_id matching #{place_id}"
                next
            end

            # if for whatever reason this rake task didn't complete on the initial execution,
            # it should be safe to re-run this rake task because this line prevents unnecessary
            # timezone lookups if we see that users with this place_id have already been backfilled
            next if record.user.timezone && CareerProfile.joins(:user).where(place_id: place_id, users: {timezone: nil}).count == 0 &&
                HiringApplication.joins(:user).where(place_id: place_id, users: {timezone: nil}).count == 0

            # attempt to lookup the timezone
            begin
                timezone = record.user.timezone || record.lookup_timezone
            rescue Timezone::Error::Base => error
                # If we reach this point, we know that the timezone should be able to be calculated,
                # but we encountered an unexpected error during the timezone lookup process. Output
                # a message to the console for debugging purposes so we can investigate later.
                failed_place_ids.concat(record.place_id)
                puts "Failed to backfill timezone for all users with career_profile or hiring_application place_id of #{place_id} due to: #{error.message}"
                next
            end

            # The lookup_timezone method used above can return nil if the timezone lookup produced
            # a Timezone::NilZone object, which is expected behavior if it's unable to successfully
            # lookup the timezone, so we safegaurd against that here.
            unless timezone
                puts "Failed to lookup valid timezone for all users with career_profile or hiring_application place_id of #{place_id}"
                next
            end

            # Update the timezone for all users with a career_profile or hiring_application with a place_id
            # that matches the place_id in question. update_all is used here to ensure that no IdentifyUserJob
            # delayed jobs are enqueued as they would block up the queue, preventing organically triggered
            # IdentifyUserJob delayed jobs that may be needed for email campaigns to run.
            users = User.left_outer_joins(:career_profile, :hiring_application).where("career_profiles.place_id = '#{place_id}' OR hiring_applications.place_id = '#{place_id}'")
            puts "Backfilling timezone for #{users.length} users with career_profile or hiring_application with a place_id of #{place_id} to #{timezone}"
            users.update_all(timezone: timezone)
            user_ids.concat(users.pluck(:id))
        end

        puts "Successfully backfilled timezone for #{user_ids.length} users"
        puts "IDs of updated users:"
        puts "#{user_ids.map { |user_id| "'" + user_id + "'" }.join(', ') }"
    end

    desc "Create accounts for fake passwords"
    task :create_fake_accounts => :environment do

        # set configuration here for the users you want to create
        institution_title = nil
        base_email = "testing+INDEX@pedago.com"
        first_name = 'Telefonica'
        last_name_base = "TelefonicaINDEX"
        sign_up_code = "TELEFONICA"

        # if favorite_course_titles is left nil,
        # then all courses in groups will be favorited
        favorite_course_titles = nil
        # favorite_course_titles = [
        #     'Accounting I: Fundamentals',
        #     'Data Collection',
        #     'Finance: Time Value of Money',
        #     'Microeconomics I: Supply and Demand',
        #     'One-Variable Statistics',
        #     'Two-Variable Statistics'
        # ]
        user_count = 50
        group_names = ['Spanish Demo', 'Math Basics']
        start_index = 30
        # end configuration


        # load groups
        groups = AccessGroup.where(name: group_names)
        missing_groups = group_names - groups.map(&:name)
        raise "missing groups #{missing_groups.join(', ')}" if missing_groups.any?

        # load courses
        favorite_course_titles ||= groups.map(&:lesson_streams).flatten.uniq.map(&:title)
        favorite_courses = Lesson::Stream.where(title: favorite_course_titles)
        published_courses = favorite_courses.select(&:has_published_version?)
        missing_courses = favorite_course_titles - published_courses.map(&:title)
        raise "Missing published courses: #{missing_courses}" if missing_courses.any?

        # load institutions
        if institution_title
            institution = Institution.where(name: institution_title).first
            raise "No institution #{institution_title}" unless institution
        end

        # check that courses are in groups
        isntitution_group_names = institution ? institution.groups.map(&:name)  : []
        all_group_names = isntitution_group_names + group_names
        published_courses.each do |course|
            raise "#{course.title} has none of user's groups" unless (course.groups.map(&:name) & all_group_names).any?
        end

        ActiveRecord::Base.transaction do
            start_index.upto(start_index + user_count - 1) do |i|

                email = base_email.gsub('INDEX', i.to_s)

                u = User.create!(
                    email: email,
                    uid: email,
                    provider: 'email',
                    first_name: first_name,
                    last_name: last_name_base.gsub('INDEX', i.to_s),
                    password: 'p@ssword!',
                    password_confirmation: 'p@ssword!',
                    sign_up_code: sign_up_code
                )

                u.institutions << institution if institution

                group_names.each do |group_name|
                    u.add_to_group(group_name)
                end

                u.favorite_lesson_streams = published_courses

                puts "created #{u.email}"
            end
        end

    end

    def all_blue_ocean_learners
        User.
            joins(:groups).
            joins(:roles).
            where("tags.text = 'BLUEOCEAN'").
            where("roles.name = 'learner'")
    end

    def unicode_titleize(str)
        str ? str.split(/ +/).map{ |word| Unicode::capitalize(word) }.join(' ') : ''
    end

    desc "Imports users given a CSV with columns email, name, password, sign_up_code, locale [, institution_sign_up_code, nickname, groups, cohort, has_seen_welcome, additional_details]"
    task :import_users => :environment do

        puts "Batch importing users ..."

        def create_cohort_application(user, cohort, cohort_status)
            user.cohort_applications << CohortApplication.new(
                cohort_id: cohort.id,
                status: cohort_status,
                applied_at: Time.now,

                # since the cohort_application might update the user, we need to
                # ensure it's the same user object that we're using
                user: user
            )

            user.active_playlist_locale_pack_id = user.relevant_cohort.playlist_pack_ids.first

            user.save!
        end

        ActiveRecord::Base.transaction do

            path = Rails.root.join('tmp', 'import_users.csv')
            CSV.foreach(path, headers: true, encoding: 'utf-8') do |row|

                user_attrs = {
                    # titleize doesn't support unicode. have to use a separate gem
                    name: unicode_titleize(row['name']),
                    nickname: row['nickname'] ? row['nickname'] : unicode_titleize(row['name']),
                    provider: 'email',
                    email: row['email'],
                    uid: row['email'],
                    password: row['password'],
                    password_confirmation: row['password'],
                    sign_up_code: row['sign_up_code'],
                    pref_locale: row['locale']
                }

                if user_attrs.any?{|v| v.blank?}
                    raise "Missing values for row: #{row.inspect}"
                end

                additional_details = nil

                row.each do |col, val|
                    detail_field = col.split('additional_details.')[1]
                    if !detail_field.blank? && !val.blank?
                        additional_details ||= {}
                        additional_details[detail_field] = val
                    end
                end

                if additional_details
                    user_attrs[:additional_details] = additional_details
                end

                print "Creating user #{user_attrs[:first_name]} #{user_attrs[:last_name]} <#{user_attrs[:email]}> ... "


                begin
                    user = User.create!(user_attrs)
                    user.register_content_access

                    if row['institution_sign_up_code']
                        sign_up_code = row['institution_sign_up_code']
                        institution = Institution.find_by_sign_up_code(sign_up_code)

                        if institution.nil?
                            raise "No institution exists with the sign_up_code #{sign_up_code}"
                        end

                        user.institutions << institution
                    end

                    if row['groups']
                        user.replace_groups(row['groups'].split(',').map(&:strip))
                        user.favorite_lesson_stream_locale_packs = user.access_groups.map(&:lesson_stream_locale_packs).flatten.uniq
                    end

                    if row['has_seen_welcome']
                        user.has_seen_welcome = (row['has_seen_welcome'].to_s.downcase == "true")
                    end

                    user.save!

                    if row['cohort']
                        cohort = Cohort.where("name_locales->>'en' = ?", row['cohort']).first

                        if cohort.nil?
                            raise "Cohort #{row['cohort']} not found!"
                        end

                        create_cohort_application(user, cohort, 'pending')
                    end

                    puts "Success!"
                rescue ActiveRecord::RecordInvalid => ex
                    if ex.message =~ /Email.*already/
                        puts "Already exists!"
                    else
                       raise ex
                    end
                end
            end

        end

        puts "Batch import complete.\n"

    end

    desc "backfill avatar images"
    task :backfill_avatar_images => :environment do

        query = User.where.not(provider_user_info: nil).where(avatar_url: nil)
        total = query.count
        skipped_count = 0
        updated_count = 0

        query.find_in_batches(batch_size: 50) do |users|

            users.each do |user|

                image_url =  user.provider_user_info['image']
                if image_url.nil?
                    skipped_count += 1
                    next
                end

                # it's possible that the provider_user_info we have
                # for a user is not from that user's provider, if they've
                # logged in with multiple providers, so check the image url
                if image_url.match(/facebook/)
                    provider = 'facebook'
                elsif image_url.match(/google/)
                    provider = 'google_oauth2'
                else
                    raise "What is the provider?"
                end

                user.set_provider_user_info(provider, user.provider_user_info)
                user.save!

                updated_count += 1
            end

            puts "Completed #{skipped_count + updated_count} of #{total} users. (Updated #{updated_count} users. Skipped #{skipped_count})"

        end

    end

    desc "Migrate users to a different set of groups given a CSV with columns email, groups, sign_up_code, clear_institution"
    task :migrate_between_groups => :environment do

        puts "Batch migrating users ..."

        ActiveRecord::Base.transaction do

            path = Rails.root.join('tmp', 'migrate_users.csv')
            CSV.foreach(path, headers: true, encoding: 'utf-8') do |row|

                user = User.find_by_email(row['email'].downcase)
                group_names = row['groups']
                sign_up_code = row['sign_up_code']
                clear_institution = row['clear_institution'] == 'true'

                print "Migrating #{user.email} ... "


                begin
                    if clear_institution
                        user.institutions = []
                    end

                    user.sign_up_code = sign_up_code
                    user.replace_groups(group_names.split(',').map(&:strip))
                    user.save!
                    puts "Success!"
                rescue ActiveRecord::RecordInvalid => ex
                    puts "Failed -- #{ex.message}"
                    # raise ex
                end
            end

        end
    end

    desc "copy progress from one user into another and delete the merged user"
    task :merge_progress => :environment do
        from_id = ENV['from']
        to_id = ENV['to']
        User::ProgressMerger.new(User.find(from_id), User.find(to_id)).merge!
    end

    desc "reassign to default role"
    task :reassign_to_default_role => :environment do
        raise "Specify a where clause" unless ENV['where'].present?

        where = ENV['where']

        total_count = 0
        ActiveRecord::Base.transaction do
            User.left_outer_joins(:roles, :access_groups, :institutions).where(where).each do |user|
                user.roles = []
                user.assign_default_role
                user.save!
                total_count += 1
            end
        end

        puts "Set #{total_count} users to the default role"
    end

    desc "set initial last_seen_at"
    task :set_last_seen_at => :environment do
        DbLinker.setup_red_royal_dblink
        ActiveRecord::Base.connection.execute(%q~
            with last_seen_ats AS MATERIALIZED (
                select
                    *
                from dblink('red_royal', $REDSHIFT$
                            select
                                user_id
                                , max(created_at) as last_seen_at
                            from events
                            group by user_id
                        $REDSHIFT$) AS challenges (
                            user_id uuid
                            , last_seen_at timestamp
                        )
            )
            update users
                set last_seen_at = last_seen_ats.last_seen_at
            from last_seen_ats
                where last_seen_ats.user_id = users.id
        ~)
    end

    desc "update can_edit_career_profile for career profile list candidates"
    task :update_list_candidates_can_edit_career_profile => :environment do
        ActiveRecord::Base.transaction do
            users = User.joins(:career_profile).where(can_edit_career_profile: false, career_profiles: {id: CareerProfileList.pluck(:career_profile_ids).flatten}).to_a
            users.each do |user|
                user.can_edit_career_profile = true
                user.save!
            end
        end
    end

    desc "lookup data for list of emails"
    task :lookup_email_list => :environment do
        path = Rails.root.join('tmp', 'emails.csv')

        puts "\n\nemail\tnickname\tname\tpending_cohort"

        CSV.foreach(path, headers: true, encoding: 'utf-8') do |row|

            user_attrs = {
                email: row['email']
            }

            if user_attrs.any?{|v| v.blank?}
                raise "Missing values for row: #{row.inspect}"
            end

            nickname = ''
            name = ''
            pending_application = ''
            user = User.where(email: user_attrs[:email]).first
            if !user.nil?
                nickname = user.nickname
                name = user.name

                if (user.pending_application)
                    pending_application = "#{user.pending_application.cohort.name} / #{user.pending_application.status}"
                end
            end

            puts "#{user_attrs[:email]}\t#{nickname}\t#{name}\t#{pending_application}"
        end
    end

    desc "audit remaining pending users after an admissions round"
    task :audit_pending_after_admissions => :environment do
        program_type = ENV['program_type']
        program_type ||= 'mba'

        status = ENV['status']
        status ||= 'pending'

        exclude_promoted = ENV['exclude_promoted']
        exclude_promoted ||= 'true'
        exclude_promoted = exclude_promoted == 'true' ? true : false

        # find all users with PENDING status in a cohort that is not promoted
        promoted_cohort = Cohort.promoted_cohort(program_type)
        users_in_pending = User.joins(:cohort_applications, cohort_applications: :cohort)
            .where("cohorts.program_type = '#{program_type}'")
            .where("cohort_applications.status = '#{status}'")

        if exclude_promoted
            users_in_pending = users_in_pending.where("cohort_applications.cohort_id != '#{promoted_cohort.id}'")
        end

        puts "#{program_type} pending users:"
        puts "\n\nemail\tnickname\tname\tpending_cohort"
        users_in_pending.all.each do |user|
            pending_application = ''

            if (user.pending_application)
                pending_application = "#{user.pending_application.cohort.name} / #{user.pending_application.status}"
            end

            puts "#{user.email}\t#{user.nickname}\t#{user.name}\t#{pending_application}"
        end
    end

    desc "delete customerio users given a csv"
    task :delete_customer_io_users_from_csv => :environment do

        # don't conflate existing CustomerIO credentials out of paranoia
        unless ENV['SITE_ID'] && ENV['API_KEY']
            raise "You must provide a SITE_ID and API_KEY!"
        end

        skip = ENV['SKIP'].to_i || 0
        puts "Skipping #{skip} rows."

        # more paranoia
        if ENV['SITE_ID'] == '82553bf88cac922b4693'
            puts "WARNING: You are about to delete on production! Continue? (y/n) "
            input = STDIN.gets.strip
            if input != 'y'
                exit
            end
        end

        customerio = Customerio::Client.new(ENV['SITE_ID'], ENV['API_KEY'])

        path = Rails.root.join('tmp', 'customerio_users_to_delete.csv')

        index = 0
        failed_users = []
        CSV.foreach(path, headers: true, encoding: 'utf-8') do |row|
            if index < skip
                puts "Skipping #{index}"
                index += 1
                next
            end

            id = row['id']
            puts "Deleting: #{id}   (#{index})"
            begin
                customerio.delete(id)
            rescue URI::InvalidURIError
                failed_users << id
            end
            index += 1
        end

        if !failed_users.empty?
            puts "The following users failed to be deleted: \n#{failed_users.join('\n')}"
        end
    end


    desc "delete customerio users from target environments"
    task :delete_all_customer_io_users => :environment do

        # don't conflate existing CustomerIO credentials out of paranoia
        unless ENV['SITE_ID'] && ENV['API_KEY']
            raise "You must provide a SITE_ID and API_KEY!"
        end

        # more paranoia
        if ENV['SITE_ID'] == '82553bf88cac922b4693'
            raise "You cannot run this on the production environment!"
        end

        customerio = Customerio::Client.new(ENV['SITE_ID'], ENV['API_KEY'])

        batch = 0
        failed_users = []
        User.find_in_batches(batch_size: 10) do |users|
            puts " ************ Processing user batch ##{batch}"
            users.each_with_index do |user, i|
                print "Deleting #{user.email} ..."
                begin
                    customerio.delete(user.id)
                    puts "Success!"
                rescue ActiveRecord::RecordInvalid => ex
                    puts "Failed -- #{ex.message}"
                    failed_users << user.email
                    # raise ex
                end
            end
            batch += 1
        end

        if !failed_users.empty?
            puts "The following users failed to be deleted: \n#{failed_users.join('\n')}"
        end
    end

    desc "Update fallback_program_type to match program_type on user's last cohort application"
    task :update_fallback_program_type => :environment do
        cohort_program_types_str = Cohort::ProgramTypeConfig.program_types.map { |program_type| "'#{program_type}'" }.join(', ')
        signup_program_types_str = Cohort::ProgramTypeConfig.signup_program_types.map { |program_type| "'#{program_type}'" }.join(', ')
        result = ActiveRecord::Base.connection.execute(%Q~
            select
                u.id
            from users u
                join cohort_applications ca on ca.user_id = u.id
                join cohorts c on c.id = ca.cohort_id
            where
                -- Exclude users that have a fallback_program_type outside
                -- of the program types that can be used by our cohorts
                u.fallback_program_type in (#{cohort_program_types_str})
            group by
                u.id
            having (array_agg(c.program_type order by ca.applied_at desc))[1] in (#{signup_program_types_str})
                and u.fallback_program_type != (array_agg(c.program_type order by ca.applied_at desc))[1]
        ~)
        user_ids = result.to_a.map { |user| user['id'] }

        puts "Updating fallback_program_type for #{user_ids.size} users..."

        query = User.includes(:cohort_applications, :institutions, :career_profile, :avatar).where(id: user_ids)

        User.use_mass_identify_priority = true

        query.in_batches(of: 200).each_record do |user|
            puts "Updating fallback_program_type for #{user.id} from #{user.fallback_program_type} to #{user.last_application.program_type}"
            user.update!(:fallback_program_type => user.last_application.program_type)
        end

        puts "Rake task complete!"
    end

    desc "backfills notification options for hiring managers"
    task :backfill_hiring_notification_options => :environment do

        learner_mode = ENV['learner_mode'] == 'true'

        query = learner_mode ? User.all : User.joins(:hiring_application)
        query = query.where('notify_hiring_updates IS NULL')

        batch = 0
        batch_size = 100
        options = User::HIRING_NOTIFICATIONS.keys

        total_count = query.size
        query.find_in_batches(batch_size: batch_size) do |users|
            puts " ************ Processing user batch ##{batch} of #{total_count / batch_size}"
            users.each_with_index do |user, i|
                print "Backfilling notifications for #{user.email} ..."

                if learner_mode
                    # no need to re-identify
                    col_updates = Hash[options.map {|x| [x, user.notify_email_newsletter]}]
                    user.update_columns(col_updates)
                else
                    # re-identify, etc
                    options.each do |option|
                        user.send("#{option}=", user.notify_email_newsletter)
                    end
                    user.save!
                end

                puts "Success!"
            end
            batch += 1
        end


    end

    task :backfill_column_to_false => :environment do
        column = ENV["column"]
        raise "Please provide column" if column.nil?
        User.where("#{column} IS NULL").update_all("#{column} = false")
    end

    task :transfer_progress => :environment do
        application_ids = ENV['application_ids'].split(' ')
        users_to_force_if_new_progress = ENV['force_user_ids']&.split(' ') || []

        CohortApplication.where(id: application_ids).each do |application|
            if users_to_force_if_new_progress.include?(application.user.id)
                application.transfer_progress_from_deferred_cohort(['9b20b827-a7b2-4203-93d8-5c918352e34e'])
            else
                application.transfer_progress_from_deferred_cohort
            end
        end
    end

    # NOTE: this task does not re-identify users, since we don't include
    # can_purchase_paid_certs as a user attribute anyway
    task :open_up_paid_certs => :environment do
        # example: f=tmp/user_ids.txt rake user:open_up_paid_certs
        # file can be a comma separated or whitespace-separated list of ids
        filename = ENV['f']
        emails = File.read(filename).split(/[\s,]/).map(&:strip)


        # Rails.logger.level = Logger::DEBUG
        # ActiveRecord::Base.logger = Logger.new(STDOUT)

        updated_emails = []
        batch_size = 500
        _updated_emails = open_up_paid_certs_update(emails, batch_size)
        while _updated_emails&.any?
            updated_emails += _updated_emails
            _updated_emails = open_up_paid_certs_update(emails, batch_size)
            puts "#{updated_emails.size} users updated"
        end

        output_file = Rails.root.join('tmp', "can_purchase_paid_certs_updates.#{Time.now.to_s.gsub(' ', '_')}.txt")
        File.open(output_file, 'w+') do |f|
            updated_emails.each do |email|
                f.puts(email)
            end
        end

        puts "#{updated_emails.size} updated emails written to #{output_file.to_s}"
    end

    def open_up_paid_certs_update(emails, batch_size)
        email_string = emails.map { |email| "'#{email}'" }.join(',')

        sql = "
            WITH user_ids AS MATERIALIZED (
                SELECT
                    users.id
                FROM users
                    join cohort_applications on cohort_applications.user_id = users.id
                WHERE
                    can_purchase_paid_certs is distinct from TRUE
                    AND email in (#{email_string})
                GROUP BY users.id
                HAVING (array_agg(cohort_applications.status order by cohort_applications.applied_at desc))[1] in ('rejected', 'expelled')
                LIMIT #{batch_size}
            )
            UPDATE users SET can_purchase_paid_certs=true WHERE id IN (SELECT * FROM user_ids)
            RETURNING email
        "
        result = ActiveRecord::Base.connection.execute(sql)
        emails = result.to_a.map { |r| r['email'] }
        emails
    end


    def rebuild_last_application_for_deferral_version(email, version_id)

        CohortApplication.skip_callback(:save, :after, :create_status_changed_event)
        CohortApplication.skip_callback(:save, :after, :create_registered_changed_event)
        CohortApplication.skip_callback(:save, :after, :create_graduation_status_changed_event)
        CohortApplication.skip_callback(:save, :after, :create_retargeted_changed_event)
        CohortApplication.skip_callback(:save, :after, :clear_subscription_on_zero_payments)
        CohortApplication.skip_callback(:commit, :after, :identify_user)

        begin
            puts "Processing #{email} ..."

            # get current application as well as the version just prior to changing the cohort
            u = User.find_by_email(email)
            existing_application = u.last_application
            vers_prior_to_update = CohortApplication.find_by_sql("SELECT * FROM cohort_applications_versions WHERE version_id='#{version_id}'").first

            # create a new CohortApplication based on the existing
            new_application = CohortApplication.new(existing_application.attributes.merge({"id" => SecureRandom.uuid, "created_at" => vers_prior_to_update.updated_at}))
            new_application.applied_at = vers_prior_to_update.updated_at

            # update the existing application
            existing_application.cohort_id = vers_prior_to_update.cohort_id
            existing_application.updated_at = vers_prior_to_update.updated_at
            existing_application.num_charged_payments = vers_prior_to_update.num_charged_payments
            existing_application.num_refunded_payments = vers_prior_to_update.num_refunded_payments
            existing_application.status = 'deferred'
            existing_application.save!

            # write the new application
            new_application.save!

            # indentify the user only once
            u.identify

        ensure
            CohortApplication.set_callback(:save, :after, :create_status_changed_event)
            CohortApplication.set_callback(:save, :after, :create_registered_changed_event)
            CohortApplication.set_callback(:save, :after, :create_graduation_status_changed_event)
            CohortApplication.set_callback(:save, :after, :create_retargeted_changed_event)
            CohortApplication.set_callback(:save, :after, :clear_subscription_on_zero_payments)
            CohortApplication.set_callback(:commit, :after, :identify_user)
        end

    end

    task :ensure_deferred_application_consistency => :environment do
        puts "WARNING: This task is not idempotent. Only run once for the selected user / version combination!"
        rebuild_last_application_for_deferral_version(ENV["u"], ENV["v"])
    end

    # See https://trello.com/c/tXadUX4z
    # Here is a sql query that will give you the new score you should use if you are asked to add
    # 1 point to somebody's test score.  To add more than one point, change the `1` in `(num_correct + 1) / num_challenges::float as new_score`.
    #
    # If there are multiple lessons in the exam, it will show you the results of adding this many
    # points to each lesson.  Pick the one you care about.
=begin
    with step_1 AS MATERIALIZED (
        SELECT
            lesson_streams.title,
            lesson_progress.locale_pack_id,
            lesson_progress.best_score,
            (CHAR_LENGTH(lesson_progress.challenge_scores :: TEXT) -
             CHAR_LENGTH(REPLACE(lesson_progress.challenge_scores :: TEXT, '":', ''))) / CHAR_LENGTH('":')   num_challenges,
            (CHAR_LENGTH(lesson_progress.challenge_scores :: TEXT) -
             CHAR_LENGTH(REPLACE(lesson_progress.challenge_scores :: TEXT, '1.0', ''))) / CHAR_LENGTH('1.0') num_correct,
            lesson_progress.challenge_scores
        FROM lesson_progress
            JOIN users ON lesson_progress.user_id = users.id
            JOIN published_stream_lessons ON lesson_locale_pack_id = lesson_progress.locale_pack_id
            JOIN lesson_streams ON lesson_streams.locale_pack_id = stream_locale_pack_id
        WHERE
            lesson_streams.exam = TRUE

            ----------  CHANGE THIS
            AND email = 'nate@pedago.com'
            AND lesson_streams.title LIKE '%Strategic%'
            ----------
    )
    SELECT
        case when abs(best_score - num_correct/num_challenges::float) > 0.000001 then 'WARNING: SOMETHING IS WRONG' else '' end all_ok
        , title
        , locale_pack_id
        , best_score original_score
        , (num_correct + 1) / num_challenges::float as new_score
    from step_1

=end
    task :change_lesson_score => :environment do
        user_identifier = ENV['u']
        user = User.find_by_email(user_identifier) || User.find(user_identifier)
        locale_pack_id = ENV['lp']
        score = ENV['s'].to_f

        change_lesson_score(user, locale_pack_id, score)
    end

    task :change_lesson_score_for_multiple_users => :environment do
        if !ENV['file']
            raise "Must supply a path to a CSV file."
        end

        file = ENV['file'] # expects a CSV file

        CSV.foreach(file, headers: true, encoding: 'utf-8') do |row|
            user_id = row['user_id']
            lesson_locale_pack_id = row['lesson_locale_pack_id']
            new_score = row['new_score']

            user = User.find_by_id(user_id)
            change_lesson_score(user, lesson_locale_pack_id, new_score)
        end
    end

    def change_lesson_score(user, locale_pack_id, score)
        ActiveRecord::Base.transaction do
            lesson_progress = user.lesson_progresses.find_by_locale_pack_id(locale_pack_id)
            lesson_progress.update_attributes(best_score: score)

            stream_locale_pack_ids = ActiveRecord::Base.connection.execute("select stream_locale_pack_id from published_stream_lesson_locale_packs where lesson_locale_pack_id = '#{locale_pack_id}'")
                                        .to_a.map { |r| r['stream_locale_pack_id']}

            stream_progresses = user.lesson_streams_progresses.where(locale_pack_id: stream_locale_pack_ids)
            raise "Expecting one stream progress" unless stream_progresses.size == 1
            stream_progress = stream_progresses.first
            new_official_test_score = stream_progress.calculate_official_test_score_from_lesson_progress
            stream_progress.update_attributes(official_test_score: new_official_test_score)

            puts "Updated lesson score for #{user.id} (#{user.email}) to #{score}, resulting in a new official_test_score of #{new_official_test_score} for the stream."

            # This will cause the record in user_lesson_progress_records to get updated
            Event.create_server_event!(
                SecureRandom.uuid,
                user.id,
                'lesson:touched',
                {
                    lesson_id: lesson_progress.lessons.find_by_locale('en').id,
                    label: "changed score"
                }
            ).log_to_external_systems
        end
    end

    task :ensure_accepted_hiring_managers_teams => :environment do
        User.joins(:hiring_application).where("hiring_applications.status = 'accepted'").where(hiring_team_id: nil).each do |user|
            puts "Backfilling for #{user.email} ..."
            user.ensure_hiring_team!
        end
    end

    desc "Queues up DeleteAssociatedUserDataJobs for any orphaned user ids in users_versions"
    task :delete_associated_data_from_deleted_users => :environment do

        puts "\nLooking for users_versions with no corresponding record in users..."

        # Try to find a career_profile_id from career_profiles_versions,
        # since the career profile was deleted when the user was destroyed.
        # NOTE: we need to be cognizant of users that may have been undeleted here!
        user_records_to_delete = ActiveRecord::Base.connection.execute(%Q~
            SELECT
                distinct
                users_versions.id
                , career_profiles_versions.id AS career_profile_id
            FROM users_versions
                LEFT JOIN career_profiles_versions
                    ON career_profiles_versions.user_id = users_versions.id
                LEFT JOIN users
                    ON users.id = users_versions.id
            WHERE users_versions.operation = 'D'
                AND users.id IS NULL;
        ~).to_a
        puts "Found #{user_records_to_delete.size} deleted records in users_versions..."

        count = 0
        now = Time.now + 1.minute
        user_records_to_delete.each_with_index do |record, i|

            user_id = record['id']
            career_profile_id = record['career_profile_id']

            wait_until = now + i.seconds

            DeleteAssociatedUserDataJob.set(wait_until: wait_until).perform_later(user_id, career_profile_id)

            count += 1
            if count % 100 == 0 || count == user_records_to_delete.size
                puts "Queued #{count} of #{user_records_to_delete.size} deletion jobs..."
            end

        end

        puts "Done!"

    end

    desc "Queue up copy job for all users with raw urls saved in the avatar_url column"
    task :queue_copy_avatar_jobs => :environment do
        log_rescued_http_errors = ENV['log_rescued_http_errors'] && ENV['log_rescued_http_errors'] == 'false' ? false : true
        limit = ENV.key?('limit') ? ENV['limit'].to_i : nil

        query = User.where.not(avatar_url: nil)
        if limit
            query = query.limit(limit)
        end
        user_ids = query.pluck(:id)

        puts "Queueing jobs for #{user_ids.size} users"
        user_ids.each_with_index do |user_id, i|

            # disable retriable HTTP error logging for our initial batch
            CopyAvatarJob.perform_later(user_id, log_rescued_http_errors)

            progress = i+1
            if progress % 500 == 0 || progress == user_ids.size
                puts "#{progress} of #{user_ids.size} jobs queued"
            end
        end
    end

    # https://trello.com/c/nEvYNvHo
    desc "Fix avatars that were messed up by avatar_url update bug"
    task :fix_avatar_urls => :environment do

        sql = %Q~
            SELECT
                users.email
                , users.id
                , users.created_at
                , users.updated_at
                , users.avatar_url
                , (array_agg(users_versions.avatar_url order by users_versions.version_created_at desc))[1] as source_url
            from users
                    left join users_versions
                        on users_versions.id = users.id
                        and users_versions.version_created_at > '2018/06/11'
                        and users_versions.avatar_url is not null
                        and users_versions.avatar_url not like '%#{ENV['AWS_UPLOADS_BUCKET']}%'
                where users.avatar_url like '%#{ENV['AWS_UPLOADS_BUCKET']}%'
            group by
                users.email
                , users.id
                , users.created_at
                , users.updated_at
                , users.avatar_url
            order by
                (array_agg(users_versions.avatar_url order by users_versions.version_created_at desc))[1] is null,
                users.updated_at
            ~
        records = ActiveRecord::Base.connection.execute(sql).to_a

        puts "Updating #{records.size} records"
        records.each_with_index do |record, i|
            user = User.find(record['id'])
            user.avatar_url = record['source_url']
            user.save!
            puts "#{i} of #{records.size} complete" if i % 100 == 0
        end

        User.where("avatar_url like '%#{ENV['AWS_UPLOADS_BUCKET']}%'").find_in_batches(batch_size: 100) do |batch|
            puts "unsetting avatar_url for #{batch.size} users"
            User.where(id: batch.map(&:id)).update_all(avatar_url: nil)
        end

    end

    desc "Delete s3_identification_asset for unverified users now that we're on IDology"
    task :delete_s3_identification_assets => :environment do
        users = User.joins(:s3_identification_asset).where(identity_verified: false).all
        puts "Found #{users.length} users to update..."
        users.each_with_index do |u, i|
            puts "Unsetting s3_identification_asset for user #{i+1}..."
            u.s3_identification_asset = nil
            u.save!
        end
    end

    # See https://trello.com/c/uLwkGz3z
    desc "Backfill country, state, and city for all users who prevously had some of these values, but now no longer do"
    task :backfill_country_state_city => :environment do
        target_file = ENV['file']

        if target_file.blank?
            puts "No file specified; example usage: `rake user:backfill_country_state_city file=tmp/user_ids.csv`"
            puts "Proper file format:"
            puts %Q~
                '182fca99-1fc9-47bc-a3b3-5f8571468bfb',
                'f736f6a5-3638-4385-a0e9-6192f757b8b4',
                ...
                '38d2e406-d1f2-427b-b962-d89f64baa7d0',
                '9ee52cc7-edf0-4a8e-ac7a-764fe6e010a7'
            ~
            raise
        end


        file_contents = File.read(target_file)

        query = User.where("id in (#{file_contents})")
        total_count = query.count
        batch_size = 100
        count = 0

        query.in_batches(of: batch_size).each_record do |user|
            # We only check for the most recent user version where both the country and city are not
            # null because a place will always have a country and a city, but not necessarily a state.
            version = User::Version.where(id: user.id).where.not(country: nil, city: nil).reorder(version_created_at: :desc).first

            if version
                user.update_attributes(:country => version.country, state: version.state, city: version.city)
            else
                puts "Could not find users_version record for id '#{user.id}'"
            end

            count += 1
            puts "Processed batch #{count/batch_size}/#{(total_count/batch_size).ceil}..." if count % batch_size == 0
        end

        puts "Finished processing #{count} users"
        puts "Rake task complete!"
    end


    task :backfill_legacy_identity_verified_verifications => :environment do

        query = User.where("id IN (
                            select u.id
                            from users u
                                join cohort_applications ca on ca.user_id = u.id
                                join cohorts c on c.id = ca.cohort_id
                                left join user_id_verifications uiv on uiv.user_id = u.id
                            where u.identity_verified = true
                                and ca.status in ('accepted', 'pre_accepted')
                                and c.id_verification_periods is not null
                                and c.id_verification_periods::text != '{}'
                                and uiv.id is null
                            )")
        total_count = query.count
        batch_size = 50
        count = 0

        query.in_batches(of: batch_size).each_record do |user|
            puts "Updating #{user.email} ..."
            user.last_application.backfill_for_legacy_id_verifications
            count += 1
            puts "Processed batch #{count/batch_size}/#{(total_count/batch_size).ceil}..." if count % batch_size == 0
        end

        puts "Finished processing #{count} users"

    end

    desc "Resets state for users used by Pixelmatters during video creation"
    task :reset_pixelmatters_video_state => :environment do

        primary_user = User.find('e28920b3-6c15-4504-9f25-f222756f1621') # allison+pixelmatters@example.com
        hiring_user_id = 'e7aa4c67-0263-4f75-b012-5ce0fb1a127e' # allison+pixelmatters_adelaide@example.com

        # Nothing currently needs to be changed for this conversation
        # '83f6addf-491f-46f0-9481-0071d85fc5b8' # allison+pixelmatters_david@example.com

        # reset lesson progress state
        User.clear_all_progress(primary_user.id)

        # backfill "Finance: Time Value of Money" stream, except for Smartcase
        complete_streams(primary_user, ['e57f12b6-d5b2-49b1-b9e5-02848d7972cc'], ['316d9d92-bb7a-4fa6-b9f1-943626d36272'])

        # backfill "Customer Discovery" stream, except for Smartcase
        complete_streams(primary_user, ['2f7607cf-3423-4d91-a5b1-7a0589a3b91c'], ['811bccd7-1688-425a-bc3a-9592e917f2dc'])


        # reset mailboxer conversation state
        primary_user.hiring_manager_relationships.where(hiring_manager_id: hiring_user_id).each do |relationship|

            # grab messages
            messages = relationship.conversation.messages

            # delete any replies
            messages.where(sender_id: primary_user.id).each do |message|
                message.receipts.destroy_all
                message.destroy
            end

            # update pending status
            relationship.candidate_status = 'pending'
            relationship.save!

            # mark the originating messages as unread
            messages.where(sender_id: hiring_user_id).first.receipts.each { |receipt| receipt.mark_as_unread }

        end

    end

    desc "Set up the daniel@holy.agency user"
    task :backill_agency_user => :environment do
        primary_user = User.find_by_email('daniel@holy.agency')

        # reset lesson progress state
        User.clear_all_progress(primary_user.id)

        # complete the streams
        complete_streams(primary_user, [
            'e57f12b6-d5b2-49b1-b9e5-02848d7972cc',
            '34876e32-8034-4be7-89b0-58a55773ceac',
            '8ce2e7d7-4ea0-4076-9fac-b5470b90aa90',
            '41a793cc-5e32-49b7-ea7a-728b3c591145',
            'baa518c6-0fb7-4315-8fe4-23340a692923',
            '75a02078-5942-453e-e096-b22c480c1e5b',
            'a1c65943-0cfc-4acf-c6a3-3343c0496add',
            'df8b337a-10f0-4319-94b5-ca1cd3abd739',
            '18136b58-ef32-4115-b7f3-d0a69aa58a5a',
            '24e80aae-ac28-4962-a468-11276264bd86',
            '717744b7-53bf-4794-b480-e196b8068259',
            '44902a5f-7232-45c8-946f-3a1fcbced944',
            '99e10e53-f80a-48cb-a040-826405acb156',
            'bb039aff-afcf-43cd-8be5-99636a4f747e',
            '60413e17-72b6-4906-bd4b-f4d0247e5426',
            'ad77719d-9f29-4a38-99f5-5bf3b431445e',
            '11a6fd59-a5fe-4563-94f8-06c067939ea0',
            'f31e34d8-b950-4aec-862b-ce2d90560027',
            '9b20b827-a7b2-4203-93d8-5c918352e34e',
            'f3732a54-0558-4d7b-ba1b-11c8e48308b5',
            '7d1fd2bf-b100-40a5-a81d-b86b33062b29',
            'c48f8c11-e274-4dc2-8337-c1518c7bbe16',
            '08ef652c-aae3-4ea7-a5e9-b2cdc16ed362'
        ])

        # Set some fake test scores to prevent jobs from freaking out
        Lesson::StreamProgress.where(user_id: primary_user.id, locale_pack_id: ['8ce2e7d7-4ea0-4076-9fac-b5470b90aa90', '99e10e53-f80a-48cb-a040-826405acb156']).update_all(official_test_score: 1)
    end


    # NOTE: This also completes the lessons in the stream, which is NOT
    # something we want to do when transferring or backfilling progress.
    def complete_streams(user, stream_locale_pack_ids, lesson_locale_pack_ids_to_skip = [])
        stream_locale_pack_ids.each do |stream_locale_pack_id|
            stream = Lesson::Stream.all_published.where(locale_pack_id: stream_locale_pack_ids).first
            lesson_locale_pack_ids = Lesson.where(id: stream.lesson_ids).pluck(:locale_pack_id).uniq

            lessons_to_complete = lesson_locale_pack_ids - lesson_locale_pack_ids_to_skip

            lessons_to_complete.each_with_index do |locale_pack_id, i|
                LessonProgress.create_or_update!({
                    :user_id => user.id,
                    :locale_pack_id => locale_pack_id,
                    :complete => true
                })
                puts "Completed #{i+1} of #{lessons_to_complete.size} lessons and 0 of #{stream_locale_pack_ids.size} streams"
            end

            # Only complete streams for lessons that were not ignored
            if (lesson_locale_pack_ids & lesson_locale_pack_ids_to_skip).empty?
                Lesson::StreamProgress.turn_off_cert_generation = true
                stream_locale_pack_ids.each_with_index do |locale_pack_id, j|
                    Lesson::StreamProgress.create_or_update!({
                        :user_id => user.id,
                        :locale_pack_id => locale_pack_id,
                        :complete => true
                    })
                    puts "Completed #{lessons_to_complete.size} of #{lessons_to_complete.size} lessons and #{j+1} of #{stream_locale_pack_ids.size} streams"
                end
            end

        end
    end


end