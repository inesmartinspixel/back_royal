namespace :slack_room_assignment do

    task :test => :environment do

        cohort_id = Cohort.find_by_name(ENV['cohort']).id

        instance = Cohort::SlackRoomAssignmentJob.new(cohort_id)

        cohort, slack_room_ids, cohort_applications = instance.get_cohort_and_applications(cohort_id)
        cohort_applications_list = cohort_applications.to_a
        cohort_applications_list.each { |ca| ca.status='pre_accepted' }

        count = cohort_applications_list.size
        puts "**************** APPLICATIONS: #{count}"
        slack_room_assignments, search = instance.do_search(
            cohort,
            slack_room_ids,
            cohort_applications_list.slice(0, count)
        )



    end
end