namespace :lesson do
    task :urls_for_editor_template => :environment do
        template = ENV['template'] || 'multiple_card_multiple_choice'
        prefix = ENV['prefix'] || 'http://localhost:3000'
        editor_urls = []
        Lesson.joins(:content_publishers).where(locale: 'en').each do |lesson|
            lesson.frames.each do |frame|
                frame.components.each do |component|
                    if component.editor_template == template
                        editor_url = "#{prefix}/editor/lesson/#{lesson.id}/edit?frame=#{frame['id']}"

                        some_stream = lesson.streams.first
                        if !editor_urls.include?(editor_url)
                            editor_urls << editor_url
                            puts lesson.title
                            puts "\tEditor URL: #{editor_url}"

                            if some_stream.present? && some_stream.was_published?
                                player_url = "#{prefix}/course/#{some_stream.id}/chapter/0/lesson/#{lesson.id}/show?frame=#{frame['id']}"
                                puts "\tPlayer URL: #{player_url}"
                            end
                        end
                    end
                end
            end
        end
    end
end