require 'csv'
require 'digest'

namespace :marketing do
    desc "SHA256 digest of emails for tapfwd"
    task :hash_emails_for_tapfwd => :environment do
        path = Rails.root.join('tmp', 'emails_for_tapfwd.csv')
        digest = Digest::SHA256.new

        CSV.foreach(path, headers: true, encoding: 'utf-8') do |row|
            email = row['email']
            hashed = digest.hexdigest(email)
            row['hashed email'] = hashed
            puts row
        end
    end
end