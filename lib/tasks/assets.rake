
namespace :assets do

    desc "finds all unused references for given module (defaults to marketing) and cleans if desired - eg: rake assets:find_unused_images module=common clean=true"
    task :find_unused_images => :environment do
        
        clean = ENV['clean'] == 'true'

        images = Dir.glob('vendor/{images,vectors}/_minified/**/*')
        unused_images = []
        puts "Validating #{images.size} images ..."
        images.each_with_index do |image, index|
            unless File.directory?(image)
                base_name = File.basename(image).sub('@', '\@').sub('&', '\&')
                # we may house them in certain modules, but i've found they're referenced all over the place regardless
                result = `grep -nr #{base_name}* app/ vendor/marketing/styles/ vendor/front-royal/ vendor/common/`
                if result.empty?
                    unused_images << image
                    if clean
                        `rm -rf #{image}`
                    end
                end
            end
            if index % 100 === 0
                puts "[#{index}/#{images.size}]"
            end
        end

        puts "Unused images: \n\n" + unused_images.join("\n")
        puts "REMOVED!" if clean
    end
end