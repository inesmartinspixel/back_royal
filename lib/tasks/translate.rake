namespace :translate do
    # Backfill missing translations with Google Translate
    desc "Backill missing translations using Google Translate API."
    task :backfill do
        targetLanguage = ENV['target']
        force = ENV['force']
        verbose = ENV['verbose']
        simulate = ENV['simulate']
        auto = ENV['auto'].blank? ? 'true' : ENV['auto'] # default to true


        # helper Proc to run backfills for specific languages
        invokeForLanguage = Proc.new do |lang|
            exportFilename = "#{Date.today.to_s}-#{lang}-pedago.csv"

            ENV['target'] = lang
            ENV['verbose'] = verbose
            ENV['file'] = exportFilename
            ENV['auto'] = auto
            Rake::Task["translate:export_consolidated_file"].execute

            ENV['file'] = "tmp/#{exportFilename}"
            ENV['force'] = force
            ENV['verbose'] = verbose
            ENV['translate'] = 'true'
            ENV['simulate'] = simulate
            Rake::Task["translate:import_consolidated_file"].execute
        end

        if targetLanguage.blank?
            puts "Missing arugments. No target language specified."
            puts "Usage `rake translate:backfill target=es [force=false] [simulate=false]`"
            puts "force :: if false, only translate missing and unverified locale entries."
            puts "verbose :: if true, include lots of debug output. useful with simulate=true."
            puts "simulate :: if false, write changes to disk. else, simply output what would change."
            puts "\n"
            puts "Do you want to run backfill for all non-English locales (es, ar, zh, sw)? [y/n]: "

            input = STDIN.gets.strip
            if input != 'y'
                exit
            end

            ['es', 'zh', 'it', 'ar', 'sw'].each do |lang|
                puts "**** INVOKING FOR #{lang} ****"
                invokeForLanguage.call(lang)
            end

            puts 'Done backfilling all non-English locales.'
            exit
        end

        invokeForLanguage.call(targetLanguage)
    end

    # Generate a consolidated translation file for use with a translation service
    desc "Import consolidated translation file from translation service"
    task :import_consolidated_file do
        targetFile = ENV['file']
        targetLocale = ENV['target']
        force = ENV['force'] == 'true'
        translate = ENV['translate'] == 'true'
        verbose = ENV['verbose'] == 'true'
        simulate = ENV['simulate'] == 'true'
        auto = ENV['auto'] == 'true'

        if targetFile.blank?
            raise "No import file specified; use `rake translate:import_consolidated_file file=/file/path/locales.csv` target=es [force=false] [translate=false] [verbose=false] [simulate=false]"
        end

        if targetLocale.blank?
            raise "No locale specified; use `rake translate:import_consolidated_file file=/file/path/locales.csv` target=es [force=false] [translate=false] [verbose=false] [simulate=false]"
        end

        puts "--------------------------------------------------"
        puts "Importing '#{targetLocale}' locale from #{targetFile}"

        if force
            puts "force=true :: All existing translations in '#{targetLocale}' locale files WILL be overridden, including verified ones."
        else
            puts "force=false :: Existing translations in '#{targetLocale}' locale files will NOT be overridden unless unverified."
        end

        if translate
            puts "translate=true :: Blank translations in input CSV WILL be auto-translated using Google Translate."
        else
            puts "translate=false :: Blank translations in input CSV will NOT be translated using Google Translate."
        end

        if verbose
            puts "verbose=true :: Output result for every locale entry"
        else
            puts "verbose=false :: Minimal output, only show changed files"
        end

        if simulate
            puts "simulate=true :: Don't actually write any files or spend money with Google Translate."
        else
            puts "simulate=false :: Write actual files to disk and use Google Translate, if enabled."
        end

        puts "--------------------------------------------------"
        puts "Continue with import? (y/n): "

        if auto
            puts "(AUTO RUNNING)"
        else
            input = STDIN.gets.strip

            if input != 'y'
                exit
            end
        end

        contents = File.read(targetFile).gsub("[[[", "").gsub("]]]", "")
        csv = CSV.new(contents, :headers => true)
        localeArray = csv.to_a.map {|row| row.to_hash }

        google_api_key = ENV['GOOGLE_API_KEY']
        EasyTranslate.api_key = google_api_key

        # bucket the locale entries into arrays for each filepath
        localeHash = localeArray.reduce({}) do |hash, e|
            hash[e['full_path']] ||= []
            hash[e['full_path']].push(e)
            hash
        end

        # iterate over each locale file to update
        localeHash.each do |filepath, locales|
            if filepath.blank?
                puts "Skipping blank filepath; probably just empty lines."
                next
            end

            puts "PROCESSING: #{filepath}, #{locales.length} locales"
            filepath = filepath.gsub('en.json', "#{targetLocale}.json")

            # open file, search and replace locales within it
            if verbose then puts "\n *** Opening locale file to write locale entries: #{filepath} *** \n" end

            # open file and read text; if file does not exist, create a blank one with {\n} as the content and save it
            targetFileContents = "{\n}"
            if File.file?(filepath)
                targetFileContents = File.read(filepath)
            else
                if verbose then puts " ...file does not exist! Creating empty JSON object in file." end
                File.open(filepath, "w") {|file| file.puts targetFileContents }
            end

            numberOfChanges = 0

            # iterate over each locale row to update
            locales.each do |localeRow|
                key = localeRow['key']
                en_US = localeRow['en_US']
                translated_value = localeRow[targetLocale]
                file = localeRow['file']

                # search for existing locale entry
                existingEntry = nil
                verified = false
                targetFileContents.match(/\s*"#{Regexp.escape(key)}" *:.*?"(.*)"(.*?)\n/) do |m|
                    existingEntry = m[1]
                    remainder = m[2]
                    if !remainder.blank? && remainder.match(/\/\/VERIFIED/)
                        verified = true
                    end
                end

                # if existing locale entry exists, skip unless it's not verified or force=true
                if !existingEntry.blank? && verified && force == false
                    if verbose then puts "SKIPPING EXISTING '#{key}' because it already exists, is verified, and force=false :: #{existingEntry}" end
                    next
                end

                # if translated_value is blank, skip unless translate=true
                googleTranslated = false
                if translated_value.blank?
                    if !existingEntry.blank? && force == false
                        if verbose then puts "SKIPPING TRANSLATING '#{key}' because it's already translated and force=false :: #{existingEntry}" end
                        next
                    end

                    if translate == false
                        if verbose then puts "SKIPPING BLANK '#{key}' because its value is blank and translate=false" end
                        next
                    # else translate it if not simulating
                    elsif simulate == false
                        puts "TRANSLATING '#{key}' because its value is blank and translate=true :: (#{en_US})"

                        # don't translate our {{values}} by wrapping in a notranslate span
                        # see: http://stackoverflow.com/a/15960455/1747491
                        en_US = en_US.gsub(/{{.*?}}/) do |m|
                            "<span class='notranslate'>" + m + "</span>"
                        end

                        translated_value = EasyTranslate.translate(en_US, :to => targetLocale)

                        # strip away the notranslate span
                        translated_value = translated_value.gsub(/<span class='notranslate'>|<\/span>/) do |m|
                            ""
                        end

                        puts " --> Google Translate result: #{translated_value}"
                        googleTranslated = true
                    else
                        puts "SIMULATING TRANSLATING '#{key}' because its value is blank and translate=true :: (#{en_US})"
                        translated_value = 'FAKE_GOOGLE_TRANSLATE'
                        googleTranslated = true
                    end
                end


                # TRICKY: Gengo's process ended up removing our escaped characters from the strings
                # So, we want to re-escape strings before inserting them into the document
                #
                # final transformation: escape the string before insertion into the file
                # remove the surrounding \" ... \" marks
                translated_value = translated_value.inspect[1..-2]
                verifyComment = googleTranslated ? '' : '//VERIFIED'

                # if existing entry does not exist at all, add it fresh to the bottom of the file
                localeUpdated = false
                if existingEntry.nil?
                    if verbose then puts "ADDING #{key} with value #{translated_value}" end

                    # add the entry to the end of the file after the last existing entry
                    targetFileContents = targetFileContents.gsub(/"(,?)( \S+)?\s*?\n}\s*$/) do |m|
                        localeUpdated = true
                        numberOfChanges += 1
                        "\",#{$2}\n    \"#{key}\": \"#{translated_value}\" #{verifyComment}\n}"
                    end

                    # if that didn't work, maybe the file is COMPLETELY empty?
                    if targetFileContents =~ /^{\n*}/
                        targetFileContents = "{\n    \"#{key}\": \"#{translated_value}\" #{verifyComment}\n}"
                        localeUpdated = true
                        numberOfChanges += 1
                    end
                # else, if the existing value matches what we already have...
                elsif existingEntry == translated_value
                    # if it's not verified, update it to be verified
                    if !verified
                        if verbose then puts "UPDATING UNCHANGED '#{key}' from #{existingEntry} to be \/\/VERIFIED" end
                        targetFileContents = targetFileContents.gsub(/"#{Regexp.escape(key)}" *: *"[^\n]+"(,?)/) do |m|
                            localeUpdated = true
                            numberOfChanges += 1
                            "\"#{key}\": \"#{translated_value}\"#{$1} #{verifyComment}"
                        end
                    # else, skip it
                    else
                        if verbose then puts "SKIPPING UNCHANGED '#{key}' because it is already set to #{translated_value}" end
                        next
                    end
                # otherwise, search and replace it
                else
                    # if the entry is verified, we could only be here if force=true
                    if verified && force == true
                        if verbose then puts "UPDATING VERIFIED '#{key}' from #{existingEntry} to #{translated_value}" end
                    else
                        if verbose then puts "UPDATING '#{key}' from #{existingEntry} to #{translated_value}" end
                    end

                    targetFileContents = targetFileContents.gsub(/"#{Regexp.escape(key)}" *: *"[^\n]+"(,?)/) do |m|
                        localeUpdated = true
                        numberOfChanges += 1
                        if verified
                            "\"#{key}\": \"#{translated_value}\"#{$1}"
                        else
                            "\"#{key}\": \"#{translated_value}\"#{$1} #{verifyComment}"
                        end
                    end
                end

                if localeUpdated == false
                    puts targetFileContents
                    puts "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
                    puts "!! ERROR: failed to update '#{key}' -- check the regexes!!"
                    puts "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"

                    exit
                end
            end

            if numberOfChanges > 0
                puts "\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
                puts "#{numberOfChanges} changes to file! Writing file #{filepath}"
                if verbose then puts targetFileContents end
                puts "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n"

                if !simulate then File.open(filepath, "w") {|file| file.puts targetFileContents } end
            end
        end
    end

    # Generate a consolidated translation file for use with a translation service
    desc "Generate consolidated translation file to send to translation service"
    task :export_consolidated_file do
        targetLanguage = ENV['target']
        verbose = ENV['verbose'] == 'true'
        review = ENV['review'] === 'true'
        exportFilename = ENV['file']

        if targetLanguage.blank?
            targetLanguage = 'es'
            puts "No target language specified, defaulting to es. To specify, use `rake translate:export_consolidated_file target=es [review=false] [file=filename]`"
        end

        if exportFilename.blank?
            exportFilename = "#{Date.today.to_s}-#{targetLanguage}-pedago.csv"
        end

        path = File.join(Rails.root, 'tmp', exportFilename)
        CSV.open( path, 'w' ) do |csv|
            csv << ['[[[file]]]', '[[[key]]]', '[[[en_US]]]', "[[[#{targetLanguage}]]]", '[[[comments]]]', '[[[translator_notes]]]', '[[[full_path]]]']

            globPath = Rails.root.join('vendor', '**/*en.json')
            puts globPath
            Dir.glob(globPath).each do |filename|
                if filename.include?('node_modules') || filename.include?('/public/assets')
                    puts "Skipping #{filename}..."
                    next
                end

                puts "Reading #{filename}..."

                # read en_US file
                contents = File.read(filename)

                # attempt to read corresponding target locale file, if it exists
                targetFilename = filename.gsub(/en\.json$/, "#{targetLanguage}.json")
                targetLocaleContents = ""
                if File.exists?(targetFilename)
                    targetLocaleContents = File.read(targetFilename)
                end

                # extract any associated comments
                comments = {}
                contents.split("\n").each do |line|
                    line.match(/\s*"(.*?)" *:.*, *\/\/(.*)$/) do |m|
                        comments[m[1]] = "[[[#{m[2]}]]]"
                    end
                end

                # get the json
                translationJSON = ActiveSupport::JSON.decode(contents)

                # iterate over key/value locale entries
                translationJSON.each do |key, value|
                    # look to see if this is a verified key; if so, skip it!
                    verified = false
                    translatedValue = reviewValue = ""
                    targetLocaleContents.match(/\s*"#{Regexp.escape(key)}" *:.*?"(.*)"(.*?)\n/) do |m|
                        translatedValue = m[1]
                        remainder = m[2]
                        if !remainder.blank? && remainder.match(/\/\/VERIFIED/)
                            verified = true
                        end
                    end

                    if verified && !review
                        if verbose then puts "SKIPPING VERIFIED '#{key}' because it's already \/\/VERIFIED with value #{translatedValue}" end
                        next
                    end

                    if verified && review
                        if verbose then puts "EXPORTING '#{key}' FOR REVIEW with value #{translatedValue}" end
                        # TRICKY: for some reason, ActiveSupport::JSON.decode escapes the \n, etc. attributes
                        # since we want these to be literal representations of return, etc. we explicitly unescape them below
                        reviewValue = translatedValue.gsub(/\\n/, "\n").gsub(/\\t/, "\t")
                    end

                    # filter the value to ensure that non-translated text is wrapped
                    # angular interpolation
                    value = value.gsub(/\{\{([^{}]+)\}\}/) do |m|
                        "[[[{{#{$1}}}]]]"
                    end

                    reviewValue = reviewValue.gsub(/\{\{([^{}]+)\}\}/) do |m|
                        "[[[{{#{$1}}}]]]"
                    end

                    # common html entities (maybe OK to leave as-is? Do translators understand HTML?)
                    # value = value.gsub(/<a(.*?)>(.*?)<\/a>/, '[[[<a\1>]]] \2 [[[</a>]]]')
                    # value = value.gsub(/<strong(.*?)>(.*?)<\/strong>/, '[[[<strong\1>]]] \2 [[[</strong>]]]')

                    #puts "value : #{value}"
                    #puts "review: #{reviewValue}"

                    csv << [
                        "[[[#{filename[filename.rindex('/')+1..-1]}]]]",
                        "[[[#{key}]]]",
                        "#{value}",
                        "#{reviewValue}",
                        "#{comments[key]}",
                        '',
                        "[[[#{filename[filename.index('vendor')..-1]}]]]"
                    ]
                end
            end
        end

        puts "written to #{path}"
    end

    # Each developer will need to run this locally whenever dump_reporting_schema
    # has been run and the changes it causes have been committed to version control.
    desc "Audit which directives have translations"
    task :audit_directive_translations => :environment do

        results = {true => [], false => []}

        ignore = [
            /sortable/,
            /zoom_container/,
            /admin/,
            /click_key_dir/,
            /compile_dir/,
            /content_item_image_edit_dir/,
            /content_item_image_upload_dir/,
            /drag_and_drop/,
            /dynamic_node_dir/,
            /editor/,
            /event_logger/,
            /formats_text/,
            /front-royal-spinner/,
            /image_fade_in_on_load/,
            /json_file_input/,
            /lazy_loaded_panel_body/,
            /on_image_load_dir/,
            /positionable/,
            /reports/,
            /dialog_modal_alert_dir/,
            /front_royal_footer_content_dir/,
            /answer_list_dir/,
            /answer_list_image_hotspot_dir/,
            /challenge_blank_dir/,
            /challenge_overlay_blank_dir/,
            /challenges_dir/,
            /component_overlay_dir/,
            /image_dir/,
            /interactive_cards_dir/,
            /matching_board_dir/,
            /matching_challenge_button_dir/,
            /text_dir/,
            /tile_prompt_dir/,
            /ui_component_dir/,
            /componentized_continue_button_dir/,
            /componentized_dir/,
            /componentized_info_panel_dir/,
            /show_lesson_dir/,
            /show_standalone_lesson_dir/,
            /in_dom_style_preloader_dir/,
            /lesson_progress_bar_dir/,
            /modal_popup_dir/,
            /slide_message_dir/,
            /course_talk_review_widget_dir/,
            /share_stream_sidebar_dir/,
            /app_shell_dir/,
            /repeat_complete_dir/
        ]

        components_dir = Rails.root.join('vendor', 'front-royal', 'components')
        Dir.glob(components_dir.join('*', 'scripts', '**/*_dir.js')) do |path|

            next if ignore.detect { |matcher| path.match(matcher) }

            match = path.match(/components\/([^\/]+)\/scripts\/(.+)/)
            module_name, relpath = [match[1], match[2]]
            locale_relpath = relpath.split('_dir.')[0] + '-en.json'
            locale_path = components_dir.join(module_name, 'locales', locale_relpath)
            exists = File.exist?(locale_path)

            results[exists] << File.join(module_name, relpath)
        end

        puts "Translations complete for: "
        results[true].each do |path|
            puts " - #{path}"
        end

        puts ""
        puts ""
        puts ""
        puts "Translations not complete for: "
        results[false].each do |path|
            puts " - #{path}"
        end

        puts ""
        puts ""
        puts ""
        puts "#{results[true].size} of #{results[true].size + results[false].size} directives translated"

    end

    desc "Move a set of translation files (all languages) from one location to another"
    task :move_translation_files => :environment do
        basename = ENV['f']
        raise "Please include a filename (f), like `candidate_stack` (relative to locales/)" unless basename
        basepath = Rails.root.join('vendor', '*', 'components', '*', 'locales', "#{basename}-*")

        target = ENV['t']
        raise "Please include a target (t), like `vendor/front-royal/components/careers/locales/candidate_action_buttons`" unless target
        target_path = Rails.root.join(target)

        Dir.glob(basepath) do |filepath|
            suffix = filepath.split('-').last
            dest = "#{target_path.to_s}-#{suffix}"
            puts "#{filepath.gsub(Rails.root.join('vendor').to_s, '')} -> #{dest.gsub(Rails.root.join('vendor').to_s, '')}"
            FileUtils.mv(filepath, dest)
        end
    end

    desc "remove a translation key from all languages: (ex.  k=careers.candidate_action_buttons.connect rake translate:remove_key)"
    task :remove_key => :environment do
        remove_key
    end

    desc "move a translation key in all languages from one location to another (ex. k=careers.hiring_featured_candidates.like t=vendor/front-royal/components/careers/locales/candidate_action_buttons rake translate:move_key)"
    task :move_key => :environment do

        target = ENV['t']
        raise "Please include a target (t), like `vendor/front-royal/components/careers/locales/candidate_action_buttons`" unless target
        target_path = Rails.root.join(target)

        remove_key do |src_filepath, key, lang, translation, trailing_comments|
            target_filepath = "#{target_path}-#{lang}.json"
            contents = File.read(target_filepath)

            updated_contents = contents.gsub(/(\s?\/\/[^\n]+)?\s+}(\s+)?$/) do
                m = Regexp.last_match.to_a
                comments_on_last_line = m[1] || ''
                ",#{comments_on_last_line}\n    \"#{key}\": \"#{translation}\"#{trailing_comments}\n}"
            end
            File.open(target_filepath, "w") {|file| file.puts updated_contents }
            puts "#{src_filepath} -> #{target_filepath}"
        end
    end

    def remove_key(&block)
        key = ENV['k']
        raise "Please include a key (k), like `careers.candidate_action_buttons.liked`" unless key
        parts = key.split('.')
        key = parts.pop
        component = parts.shift
        parts << "#{parts.pop}-*"

        Dir.glob(Rails.root.join(*(['vendor', '*', 'components', component, 'locales'] + parts))) do |filepath|
            puts filepath unless block_given?
            contents = File.read(filepath)
            lang = filepath.match(/-(\w\w).json/)[1]

            # Match from the comma before the line (if there is one) to the end
            # of the line.  If there is a comma at the beginning and the end, then
            # add one comma back in.  If this is the first or last line, then do
            # not add a comma back in
            translation = nil
            trailing_comments = nil

            # NOTE: sadly, this does not work for strings with escaped quotes or with commas in them
            updated_contents = contents.gsub(/(,)?\s?(\/\/.+)?\s+\"#{key}\": ([^,\n]+)(,)?(\s?\/\/.+)?\n/) do
                m = Regexp.last_match.to_a
                leading_comma = (m[1] == ',')
                previous_line_comments = (m[2] || '')
                translation = m[3].strip
                translation = translation.slice(1, translation.size - 2) # remove quotes from translation
                trailing_comma = (m[4] == ',')
                trailing_comments = m[5] || ''
                (leading_comma && trailing_comma) ? ",#{previous_line_comments}\n" : "#{previous_line_comments}\n"
            end

            yield(filepath, key, lang, translation, trailing_comments) if block_given?
            File.open(filepath, "w") {|file| file.puts updated_contents }
        end
    end

end