namespace :institution do
    task :create_quantic_and_smartly => :environment do
        Institution.create!({
            id: '85fec419-8dc5-45a5-afbd-0cc285a595b9',
            name: 'Quantic',
            sign_up_code: 'QUANTIC',
            external: false,
            domain: 'quantic.edu'
        })

        Institution.create!({
            id: 'ebc4730d-19d8-4cb3-b2ca-e20308bec8e3',
            name: 'Smartly',
            sign_up_code: 'SMARTLY',
            external: false,
            domain: 'smart.ly'
        })

        puts 'Created Quantic and Smartly institutions'
    end

    task :create_miya_miya => :environment do
        Institution.create!({
            id: 'a297c552-8cef-4c78-83b3-465c8711377c',
            name: 'Miya Miya',
            sign_up_code: 'MIYAMIYA',
            external: false,
            domain: 'smart.ly'
        })

        puts 'Created Miya Miya institution'
    end

    def backfill_institutions_users(users, institution_id)
        return if users.size == 0

        user_ids_sql_list = users.pluck(:id).map { |id| "'#{id}'" }.join(',')
        ActiveRecord::Base.connection.execute %Q~
            insert into institutions_users (institution_id, user_id)
            (select '#{institution_id}' as institution_id, id as user_id from users where id in (#{user_ids_sql_list}))

            -- In case we have to rerun backfill_internal, I'd rather just do nothing on the
            -- unique conflicts than try to write some logic in the ruby task.
            ON CONFLICT DO NOTHING
        ~
    end

    def is_hiring_manager(user)
        user.hiring_application.present? || user.sign_up_code == 'HIRING' || user.provider == 'hiring_team_invite'
    end

    def is_quantic(user)
        ['mba', 'emba'].include?(user.program_type)
    end

    def is_smartly(user)
        ['the_business_certificate', 'career_network_only'].include?(user.program_type) ||
        user.program_type&.include?('paid_cert')
    end

    def is_privileged_user(user)
        (user.roles.pluck(:name) & ['admin', 'interviewer', 'super_editor', 'editor', 
            'lesson_editor', 'previewer', 'reviewer']).size > 0
    end

    task :backfill_internal => :environment do
        batch_size = 5000
        processed = 0  
        now = Time.now

        # By default orders by id, and we are making sure to cut off at Time.now to avoid
        # new users being included, so the find_in_batches offset should be good.
        User.includes(:hiring_application, :institutions, :roles, cohort_applications: :cohort)
            .where("created_at < '#{now}'")
            .find_in_batches(batch_size: batch_size) do |users|

            quantic_users = []
            smartly_users = []
            miya_miya_users = []

            users.each do |user|                  
                if is_hiring_manager(user)
                    # It doesn't make sense to give hiring managers an institution
                elsif user.has_external_institution?
                    # External users have always had an institution, and we don't currently
                    # allow being external and also having an application
                elsif is_privileged_user(user)
                    # Give both Quantic and Smartly to all users with a privileged role
                    quantic_users.push(user)
                    smartly_users.push(user)
                elsif is_quantic(user)
                    quantic_users.push(user)
                elsif is_smartly(user)
                    smartly_users.push(user)
                elsif user.program_type&.include?('jordanian_math')
                    miya_miya_users.push(user)
                else
                    quantic_users.push(user)
                end
            end

            backfill_institutions_users(quantic_users, Institution.quantic.id)
            backfill_institutions_users(smartly_users, Institution.smartly.id)
            backfill_institutions_users(miya_miya_users, Institution.miya_miya.id)

            processed += batch_size
            puts "Processed #{processed} users"
        end

        puts "Took #{(Time.now - now).to_i / 60} minutes to run"
    end

    def backfill_active_institution(users, institution_id)
        return if users.size == 0

        user_ids_sql_list = users.pluck(:id).map { |id| "'#{id}'" }.join(',')
        ActiveRecord::Base.connection.execute %Q~
            update institutions_users
            set active = true
            where institution_id = '#{institution_id}' and user_id in (#{user_ids_sql_list})
        ~
    end

    task :backfill_active => :environment do
        batch_size = 5000
        processed = 0
        now = Time.now

        # By default orders by id, and we are making sure to cut off at Time.now to avoid
        # new users being included, so the find_in_batches offset should be good.
        User.includes(:hiring_application, :institutions, :roles, cohort_applications: :cohort)
            .where("created_at < '#{now}'")
            .find_in_batches(batch_size: batch_size) do |users|
                active_simple_external_users = []
                edge_case_users = []
                active_quantic_users = []
                active_smartly_users = []
                active_miya_miya_users = []

                users.each do |user|
                    if is_hiring_manager(user)
                        # It doesn't make sense to give hiring managers an active_institution
                    elsif user.active_institution.present?
                        # Theoretically it could be set by admin before backfill, but not likely. 
                        # However, this should make re-running the task in the event of an issue 
                        # quicker too.
                    elsif user.institutions.size == 1 && user.has_external_institution?
                        # Most of our users with an external institution only have one, so
                        # just make their active_institution that.
                        active_simple_external_users.push(user)
                    elsif user.institutions.size > 1 && user.has_external_institution?
                        # These ~31 edge-case users are handled below
                        edge_case_users.push(user)
                    elsif is_privileged_user(user)
                        active_quantic_users.push(user)
                    elsif is_quantic(user)
                        active_quantic_users.push(user)
                    elsif is_smartly(user)
                        active_smartly_users.push(user)
                    elsif user.program_type&.include?('jordanian_math')
                        active_miya_miya_users.push(user)
                    else
                        active_quantic_users.push(user)
                    end
                end

                # The simple external users only have one institution, which is the case for
                # most of them (we will need to specially handle ~31 edge cases). So for these
                # We can just simply set active on their institutions_users join record without
                # checking anything institution-wise.
                if active_simple_external_users.size > 0
                    ActiveRecord::Base.connection.execute %Q~
                        update institutions_users
                        set active = true
                        where user_id in (#{active_simple_external_users.pluck(:id).map { |id| "'#{id}'" }.join(',')})
                    ~
                end

                backfill_active_institution(active_quantic_users, Institution.quantic.id)
                backfill_active_institution(active_smartly_users, Institution.smartly.id)
                backfill_active_institution(active_miya_miya_users, Institution.miya_miya.id)

                # Individually handle these ~31 users -- see https://metabase.pedago-tools.com/question/607
                edge_case_users.each do |user|
                    # Try to use the first non-demo institution. It doesn't really matter, but that feels more correct.
                    first_non_demo = user.institutions.detect { |i| !i.name.downcase.include?('demo') }
                    user.active_institution = first_non_demo || user.institutions.first
                end

                processed += batch_size
                puts "Processed #{processed} users"
        end

        puts "Took #{(Time.now - now).to_i / 60} minutes to run"
    end
end
