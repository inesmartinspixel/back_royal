namespace :role do
    
    desc "fix roles with null resource_type"
    task :fix_empty_resource_types => :environment do
        # all the lesson specific permissions should have resource_type = Lesson
        Role.where("name in ('lesson_editor', 'previewer', 'reviewer') and resource_type is NULL").update_all(resource_type: "Lesson")
    end
    
end