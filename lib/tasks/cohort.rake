namespace :cohort do

    desc "Reject pending applications for a certain cohort"
    task :reject_pending_applications => :environment do

        id = ENV['id']
        cohort_name = ENV['n']

        if id.nil? && cohort_name.nil?
            puts "Please provide the cohort id or name for which you want to reject applications"
            puts "rake cohort:reject_pending_applications id=9a9f77d1-069b-4ab4-a8b0-28d9d3ab29f3"
            puts "rake cohort:reject_pending_applications n=MBA2"
            raise
        end

        if id
            applications_for_cohort = CohortApplication.where(cohort_id: id)
        else
            applications_for_cohort = CohortApplication.joins(:cohort).where("name_locales->>'en' = ?", cohort_name)
        end


        if applications_for_cohort.where(status: 'accepted').count == 0
            raise "There are no accepted applications for this cohort so you probably did not mean to do this yet"
        end

        applications = applications_for_cohort.where(status: 'pending')
        applications.each_with_index do |application, i|
            application.status = 'rejected'
            application.save!
            puts "#{i+1} of #{applications.size} complete" if i % 100 === 0
        end
    end

    desc "Add _EXAM group to accepted users for a certain cohort name"
    task :add_exam_group => :environment do
        cohort_name = ENV['n']

        if cohort_name.nil?
            puts "Please provide the cohort name for which you want to add an _EXAM group for"
            puts "rake cohort:add_exam_group n=MBA2"
            raise
        end

        applications_for_cohort = CohortApplication.joins(:cohort).where("name_locales->>'en' = ?", cohort_name)
        applications = applications_for_cohort.where(status: 'accepted')

        if applications.size == 0
            raise "There are no accepted applications for this cohort so you probably did not mean to do this yet"
        end

        puts "Adding #{cohort_name}_EXAM group to users for #{applications.size} accepted applications"

        ActiveRecord::Base.transaction do
            applications.each_with_index do |application, i|
                application.user.add_to_group("#{cohort_name}_EXAM")
                application.user.save!
                puts "#{i+1} of #{applications.size} complete" if i % 10 === 0
            end
        end
    end

    desc "reject everyone who applied before a certain date"
    task :reject_folks => :environment do

        dry_run = ENV['dry_run']

        # We are rejecting pending applications up to this user.
            # email: vamshikurra@gmail.com
            # created_at: 2016-07-19 19:40:48.832901
        last_application_to_reject = CohortApplication.where(user_id: 'ef87e86f-32b8-4358-9cff-7edbfad8299b').first

        # Reject all pending applications that were made up to the stated above user
        applications = CohortApplication.where(status: 'pending')
                                .where("applied_at <= ?", last_application_to_reject.applied_at)

        puts "Rejecting #{applications.size} applications"

        exit if dry_run

        ActiveRecord::Base.transaction do
            applications.each do |application|
                application.status = 'rejected'
                application.save!
            end
        end

    end

    desc "delete applications for a hard-coded list of people"
    task :delete_folks => :environment do

        dry_run = ENV['dry_run']

        # Delete applications for these users who mistakingly applied
        # sn.perez@hotmail.com - c708bd86-3fb1-4c42-8165-6df00009adde
        # dwtaylo1@asu.edu - 832c8bd7-e1f3-45b4-810f-42c4ba938e46
        applications = CohortApplication.where(user_id: ['c708bd86-3fb1-4c42-8165-6df00009adde', '832c8bd7-e1f3-45b4-810f-42c4ba938e46'])

        puts "Deleting #{applications.size} applications"

        exit if dry_run

        applications.destroy_all
    end

    desc "migrate applications made during the window between MBA4 rejection and MBA5 promotion"
    task :migrate_folks => :environment do
        dry_run = ENV['dry_run']

        cohort4 = Cohort.where("name_locales->>'en' = ?", 'MBA4').first
        cohort5 = Cohort.where("name_locales->>'en' = ?", 'MBA5').first
        last_considered_application = User.find_by_email('jennie.g.lei@hotmail.com')
                                        .cohort_applications
                                        .where("cohort_id = ?", cohort4.id)
                                        .first

        # Migrate applications made to MBA1 that weren't dealt with in the above tasks (e.g. applications made
        # before the promoted cohort was switched)
        applications = CohortApplication.joins(:cohort)
                            .where(status: 'rejected')
                            .where("cohort_id = ?", cohort4.id)
                            .where("applied_at > ?", last_considered_application.applied_at)
                            .reorder("applied_at")

        puts "Migrating #{applications.size} applications made between #{applications.first.applied_at} and #{applications.last.applied_at} to MBA5"

        exit if dry_run

        ActiveRecord::Base.transaction do
            applications.each do |application|
                existing_application = application.user.cohort_applications.where("cohort_id = ?", cohort5.id).first
                if existing_application && existing_application.status == 'pending'
                    puts "Skipping #{application.user.email} because he/she has already re-applied to mba5"
                    next
                elsif existing_application
                    raise "Wtf?"
                end
                application.cohort_id = cohort5.id
                application.status = 'pending'
                puts "Migrating #{application.user.email}"
                application.save!
            end
        end
    end

    desc "given a list of users, find those that have any matching cohort names / statuses"
    task :filter_users_by_cohort_stasuses => :environment do

        statuses = ENV['statuses']

        if statuses.blank?
            puts "Usage: rake cohort:filter_users_by_cohort_stasuses statuses='EMBA1 / rejected,EMBA2 / pending'"
            puts "Purpose: finds all users that match any of the given cohort / status pairs."
            exit
        end

        valid_statuses = statuses.split(',')

        path = Rails.root.join('tmp', 'users_to_filter.csv')

        puts "Loading users from #{path}...\n\n\n"

        puts "name\temail\tstatuses\twas_accepted\tmatched_status_search\temail_we_found_in_db"

        # sometimes we want to look into the past...
        users_versions = Class.new(ActiveRecord::Base) do
            self.table_name = 'users_versions'
        end

        CSV.foreach(path, headers: true, encoding: 'utf-8') do |row|

            row_attrs = {
                email: row['email'].downcase,
                first_name: row['first_name'],
                last_name: row['last_name']
            }

            if row_attrs.any?{|v| v.blank?}
                raise "Missing values for row: #{row.inspect}"
            end

            user = User.where(email: row_attrs[:email]).first

            # can't find them by email
            if user.nil?

                fullname = "#{row_attrs[:first_name]} #{row_attrs[:last_name]}".squeeze(" ")

                # look for old users with this email
                old_users = users_versions.where(email: row_attrs[:email]).last

                # can't find them by old version
                if old_users.nil?

                    # try looking by first and last name
                    similar_user = User.where(name: fullname).joins(:cohort_applications).first

                    if !similar_user.nil?
                        user = similar_user
                    end
                else
                    user = User.where(id: old_users.id).first
                end

                if user.nil?
                    puts "#{fullname}\t#{row_attrs[:email]}"
                    next
                end
            end

            cohort_applications = user.cohort_applications.includes(:cohort)
            cohort_application_statuses = cohort_applications.map {|c| c.cohort.name + ' / ' + c.status}.sort

            intersection = valid_statuses & cohort_application_statuses
            was_accepted = cohort_applications.select {|c| ['expelled', 'deferred', 'accepted'].include?(c.status) }.length > 0

            puts "#{user.name}\t#{row_attrs[:email]}\t#{cohort_application_statuses.inspect}\t#{was_accepted}\t#{!intersection.blank?}\t#{user.email}"
        end
    end

    desc "migrate cohort applications for users from one cohort to another given a CSV with columns email, from, to (from and to are cohort names)"
    task :migrate_cohort_applications => :environment do
        dry_run = ENV['dry_run']

        puts "Batch migrating cohort applications..."

        ActiveRecord::Base.transaction do

            path = Rails.root.join('tmp', 'users_to_migrate.csv')
            CSV.foreach(path, headers: true, encoding: 'utf-8') do |row|

                row_attrs = {
                    # titleize doesn't support unicode. have to use a separate gem
                    email: row['email'],
                    from: row['from'],
                    to: row['to']
                }

                if row_attrs.any?{|v| v.blank?}
                    raise "Missing values for row: #{row.inspect}"
                end

                user = User.where(email: row_attrs[:email]).first
                from_cohort = Cohort.all.reject {|c| c.name !=  row_attrs[:from]}.first
                to_cohort = Cohort.all.reject {|c| c.name !=  row_attrs[:to]}.first

                # user not found
                if user.nil?
                    raise "Cannot find user with email: #{row_attrs[:email]}"
                end

                # source cohort doesn't exist
                if from_cohort.nil?
                    raise "Cannot find source cohort #{row_attrs[:from]} for email: #{row_attrs[:email]}"
                end

                # destination cohort doesn't exist
                if to_cohort.nil?
                    raise "Cannot find destination cohort #{row_attrs[:to]} for email: #{row_attrs[:email]}"
                end

                cohort_applications = user.cohort_applications.includes(:cohort)
                cohort_application_statuses = cohort_applications.map {|c| c.cohort.name + ' / ' + c.status}.sort

                from_cohort_application = cohort_applications.reject {|c| c.cohort.name != row_attrs[:from]}.first
                to_cohort_application = cohort_applications.reject {|c| c.cohort.name != row_attrs[:to]}.first

                # no cohorts found for this user
                if cohort_applications.blank?
                    puts "!!! Skipping #{user.email} because they don't have any cohort applications"
                    next
                end

                # no cohort application found to migrate to the destination cohort
                if from_cohort_application.nil?
                    puts "!!! Skipping #{user.email} because they don't have a cohort application for #{row_attrs[:from]} to migrate || #{cohort_application_statuses.inspect}"
                    next
                end

                # user already has a cohort application for the destination cohort
                if !to_cohort_application.nil?
                    puts "!!! Skipping #{user.email} because they ALREADY have an application for cohort #{row_attrs[:to]} || #{cohort_application_statuses.inspect}"
                    next
                end

                puts "> Migrating #{user.email} cohort application from #{from_cohort_application.cohort.name} to #{to_cohort.name} || #{cohort_application_statuses.inspect}"

                next if dry_run

                from_cohort_application.cohort_id = to_cohort.id
                from_cohort_application.save!
            end
        end

        puts "Batch migration complete!"
    end

    desc "Set participation scores given a file"
    task :set_participation_scores => :environment do
        set_scores_from_file('user_participation_scores')
    end

    desc "Set project scores given a file"
    task :set_project_scores => :environment do
        set_scores_from_file('user_project_scores')
    end

    desc "Output curriculum_status"
    task :curriculum_status => :environment do
        name = ENV['name']
        raise "Please specify cohort name" if name.nil?

        output = []
        cohort = Cohort.find_by_name(name)
        cohort.accepted_users.each do |user|
            curriculum_status = user.last_application.curriculum_status
            if curriculum_status.present?
                output << "#{user.email} #{curriculum_status}"
            else
                raise "No curriculum_status computed"
            end
        end
        puts output
    end

    # this can be removed once it has been run once on prod
    # See https://bitbucket.org/pedago-ondemand/back_royal/pull-requests/4547
    task :set_initial_registration_deadline_days_offset => :environment do
        Cohort.where.not(registration_deadline_days_offset: nil).edit_and_republish do |cohort|
            cohort.early_registration_deadline_days_offset = cohort.registration_deadline_days_offset - 5
        end
    end

    def set_scores_from_file(table)
        target_file = ENV['file']

        if target_file.blank?
            # NOTE:
            # - participation scores are a float from 0-1 (though we sometimes give scores higher than 1)
            # - emba/mba project scores are a float from 0-1
            # - paid cert project scores are a float from 0-5
            puts "No scores file specified; example usage: `rake cohort:set_participation_scores file=tmp/scores.csv`"
            puts "Proper file format:"
            puts %Q~
                ('182fca99-1fc9-47bc-a3b3-5f8571468bfb', 1.363905325),
                ('f736f6a5-3638-4385-a0e9-6192f757b8b4', 1.246301775),
                ('559802a9-6731-4d77-9f04-7c3e489bf347', 1.179240631),
                ... etc. ...
            ~
            raise
        end

        file_contents = File.read(target_file)

        ActiveRecord::Base.connection.execute "
            insert into #{table} (user_id, score)
            values
            #{file_contents}
            ON CONFLICT (user_id) DO UPDATE SET score = EXCLUDED.score WHERE EXCLUDED.score > #{table}.score; -- there are users who were deferred from previous and already had a participation score
        "
    end

    desc "call ensure_slack_room_assignment_job for all cohorts with a slack room"
    task :ensure_slack_room_assignment_job => :environment do
        Cohort.joins(:slack_rooms).uniq.each(&:ensure_slack_room_assignment_job)
    end

    desc "show any user-facing changes that have not yet been published"
    task :show_unpublished_changes => :environment do
        Cohort.all_published.reorder(:program_type, :start_date)
            .select { |co| !co.diff_from_published_version(true).blank? }
            .each do |cohort|
                puts "======== #{cohort.name} ================== "
                pp cohort.diff_from_published_version(true)
                puts ""
                puts ""
                puts ""
        end
    end

    desc "transfer progress from for users who have been deferred or expelled from a previous cohort for the specified cohort application ids (e.g. rake cohort:transfer_progress_from_deferred_or_expelled_cohort cohort_application_ids=f18d583d-0a77-4619-9a91-619e32365c5c,7e7da368-89ca-4bed-fbd1-e03e5f4e555e,27037620-8d85-4ef4-e54b-eaa06a2c7d84)"
    task :transfer_progress_from_deferred_or_expelled_cohort => :environment do
        cohort_application_ids = ENV['cohort_application_ids'].split(',').map(&:strip)
        cohort_applications = CohortApplication.where(id: cohort_application_ids)
        puts "Transferring progress for #{cohort_applications.count} cohort applications"
        cohort_applications.each do |cohort_application|
            cohort_application.transfer_progress_from_deferred_or_expelled_cohort
        end
        puts "Finished transferring progress for #{cohort_applications.count} cohort applications"
    end

    desc "backfill invited_to_interview_at for all cohort applications that have/had an admissions_decision = 'Invite to Interview'"
    task :backfill_invited_to_interview_at => :environment do
        # Only a fraction of all cohort applications have/had an admissions_decision of 'Invite to Interview',
        # so to speed things up, we only backfill invited_to_interview_at for these cohort applications since
        # they're the only cohort applications that need to be backfilled.
        query = CohortApplication
            .joins("JOIN cohort_applications_versions ON cohort_applications_versions.id = cohort_applications.id")
            .where("cohort_applications_versions.admissions_decision = 'Invite to Interview'")
            .group(:id)
        batch_size = 100
        backfilled_applications_count = 0
        total_count = query.count.keys.count # the query groups by cohort application id, so we count the number of keys (cohort application ids) instead

        puts "Backfilling invited_to_interview_at for #{total_count} cohort applications"

        # Make sure that we skip the identify_user lifecycle hook so we don't clutter up the delayed jobs queue.
        # Also, as of the initial creation of this backfill process, we aren't syncing any of these values to Airtable,
        # so we skip the update_in_airtable lifecycle hook as well.
        CohortApplication.skip_callback(:commit, :after, :identify_user)
        CohortApplication.skip_callback(:update, :after, :update_in_airtable)

        query.find_each(batch_size: batch_size) do |cohort_application|
            # get the updated_at value for the cohort_application version when they were first invited to interview
            initially_invited_to_interview_at = CohortApplication::Version
                .select('MIN(cohort_applications_versions.updated_at) AS initially_invited_to_interview_at')
                .where(id: cohort_application.id, admissions_decision: 'Invite to Interview')
                .reorder('') # remove the default ordering to prevent unnecessary grouping
                .first
                .initially_invited_to_interview_at

            cohort_application.update!(invited_to_interview_at: initially_invited_to_interview_at)

            backfilled_applications_count += 1
            if backfilled_applications_count % batch_size == 0
                puts "Backfilled #{backfilled_applications_count}/#{total_count} cohort applications"
            end
        end
        puts "Finished backfilling invited_to_interview_at for #{backfilled_applications_count}/#{total_count} cohort applications"
    end

    desc "edit_and_republish all cohorts that satisfy the specified where condition"
    task :edit_and_republish => :environment do
        where = ENV['where']
        raise 'Must specify a where condition e.g. rake cohort:edit_and_republish where="id = \'d1f65bd5-251e-43db-a3a7-30ee9818e67c\'"' unless where

        Cohort.where(where).edit_and_republish
    end

    desc "set final scores for all accepted applications"
    task :set_final_scores => :environment do
        # NOTE: it's only safe to skip these callbacks because we don't
        # send the final_score to any third parties.  If that changes at
        # some point, beware!
        CohortApplication.skip_callback(:commit, :after, :identify_user)
        CohortApplication.skip_callback(:update, :after, :update_in_airtable)

        # Backfill final_score for accepted applications
        query = CohortApplication.where(status: 'accepted')
            .joins(:cohort)
            .where(cohorts: {program_type: ['mba', 'emba', 'the_business_certificate']})
        puts "Backfilling final_score for #{query.count} accepted applications"
        count = 0
        query.find_each do |app|
            app.set_final_score
            app.save!

            count += 1
            puts "#{count} completed..." if count % 100 == 0
        end

        # Backfill final_score to nil for non-accepted applications
        query = CohortApplication.where.not(status: 'accepted').where.not(final_score: nil)
        puts "Backfilling final_score to nil for #{query.count} non-accepted applications"
        query.update_all(final_score: nil)
    end

    desc "Backfill specialization_style for cohorts and curriculum_templates and edit_and_republish them"
    task :backfill_specialization_style => :environment do
        Cohort::ProgramTypeConfig.configs.each do |config|
            if config.supports_specializations?
                cohorts = Cohort.where(program_type: config.program_type)
                puts "Backfilling specialization_style for #{cohorts.size} #{config.program_type} cohorts"
                cohorts.each do |cohort|
                    puts "Backfilling specialization_style for #{cohort.name}"
                    set_specialization_styles(cohort)
                end
                cohorts.edit_and_republish

                curriculum_templates = CurriculumTemplate.where(program_type: config.program_type)
                puts "Backfilling specialization_style for #{curriculum_templates.size} #{config.program_type} curriculum_templates"
                curriculum_templates.each do |curriculum_template|
                    puts "Backfilling specialization_style for #{curriculum_template.name}"
                    set_specialization_styles(curriculum_template)
                    curriculum_template.save!
                end
            end
        end
    end

    def set_specialization_styles(cohort_or_curriculum_template)
        cohort_or_curriculum_template.periods.each do |period|
            if match = period['title'].match(/Specialization Period (\d+)/)
                period['specialization_style'] ||= "specialization_#{match[1]}"
            end
        end
    end

    desc "Emulate setting the admissions_decision to 'Invite to Interview' in Airtable by setting it on the application_for_relevant_cohort for a list of users, ensuring callbacks are invoked to set invited_to_interview_at timestamp and Customer.io events are triggered"
    task :invite_applicants_to_interview => :environment do
        target_file = ENV['file']

        if target_file.blank?
            puts "No file specified; example usage: `rake cohort:invite_applicants_to_interview file=tmp/applicant_ids.txt`"
            puts "Proper file format:"
            puts %Q~
                '182fca99-1fc9-47bc-a3b3-5f8571468bfb',
                'f736f6a5-3638-4385-a0e9-6192f757b8b4',
                ...
                '38d2e406-d1f2-427b-b962-d89f64baa7d0',
                '9ee52cc7-edf0-4a8e-ac7a-764fe6e010a7'
            ~
            raise
        end

        file_contents = File.read(target_file)

        query = User.includes(:cohort_applications).where("id in (#{file_contents})")

        total_count = query.count
        successful_users_count = 0
        processed_users_count = 0
        batch_size = 100
        failed_user_ids = []

        puts "Attempting to set admissions_decision to 'Invite to Interview' for #{total_count} users"

        query.in_batches(of: batch_size).each_record do |user|

            cohort_application = user.application_for_relevant_cohort
            if cohort_application
                cohort_application.update_attribute(:admissions_decision, 'Invite to Interview')
                successful_users_count += 1
            else
                failed_user_ids << user.id
            end

            processed_users_count += 1
            puts "Processed batch #{processed_users_count/batch_size}/#{(total_count/batch_size).ceil}" if processed_users_count % batch_size == 0
        end

        puts "Successfully processed #{successful_users_count}/#{total_count} users"

        if failed_user_ids.any?
            puts "Failed to process the following users:"
            puts "--------------------------------------"
            puts failed_user_ids.map { |user_id| "'#{user_id}'" }.join(',')
        end

        puts "Rake task complete!"
    end

    desc "Silently change the admissions_decision to 'Invite to Interview' on the FoB application for a list of user ids and dates without triggering Customer.io events"
    task :silently_invite_fob_applicants_to_interview => :environment do
        target_file = ENV['file']

        if target_file.blank?
            puts "No file specified; example usage: `rake cohort:silently_invite_fob_applicants_to_interview file=tmp/formatted_user_ids_and_dates.csv`"
            puts "Proper file format:"
            puts %Q~
                182fca99-1fc9-47bc-a3b3-5f8571468bfb,11/14/18
                f736f6a5-3638-4385-a0e9-6192f757b8b4,8/21/18
                ...
                38d2e406-d1f2-427b-b962-d89f64baa7d0,11/13/18
                9ee52cc7-edf0-4a8e-ac7a-764fe6e010a7,5/30/18
            ~
            raise
        end

        missing_fob_cohort_application_user_ids = []
        skipped_fob_cohort_application_user_ids = []
        missing_airtable_record_user_ids = []
        failed_airtable_record_update_user_ids = []
        user_ids_to_invited_to_interview_at_map = {}

        puts "Attempting to determine earliest invited_to_interview_at timestamp for each user..."

        File.open(target_file, 'r') do |file|
            file.readlines.each do |line|
                user_id, invited_to_interview_at_date = line.split(',')
                invited_to_interview_at = Time.strptime(invited_to_interview_at_date, '%m/%d/%y')

                # if we see that a user has been listed multiple times in the file we want to set
                # their invited_to_interview_at value to the earliest timestamp that we come across
                if user_ids_to_invited_to_interview_at_map[user_id].nil? || user_ids_to_invited_to_interview_at_map[user_id].present? && user_ids_to_invited_to_interview_at_map[user_id] > invited_to_interview_at
                    user_ids_to_invited_to_interview_at_map[user_id] = invited_to_interview_at
                end
            end
        end

        query = User.includes(:cohort_applications).where(id: user_ids_to_invited_to_interview_at_map.keys)
        total_count = query.count
        processed_users_count = 0
        batch_size = 100

        puts "Attempting to invite #{total_count} users to interview..."

        query.in_batches(of: batch_size).each_record do |user|
            cohort_application = user.cohort_applications.find_by_cohort_id('67c13e6e-3e5f-4e6d-bc13-315609ced129') # Get their FoB cohort application

            if cohort_application
                allowed_redecision = (['Invite to Interview'] + CohortApplication::NORMAL_REJECTIONS).include?(cohort_application.admissions_decision) || cohort_application.admissions_decision&.starts_with?('EMBA w/')
                if !allowed_redecision
                    cohort_application.update_columns(admissions_decision: 'Invite to Interview', invited_to_interview_at: user_ids_to_invited_to_interview_at_map[user.id])

                    if cohort_application.airtable_record_id && !cohort_application.is_archived_in_airtable?
                        airtable_record = nil

                        begin
                            airtable_record = Airtable::Application.find(cohort_application.airtable_record_id)
                        rescue Exception => e
                            puts "Failed to find Airtable record #{cohort_application.airtable_record_id} for #{user.id}: #{e.message}"
                            missing_airtable_record_user_ids << user.id
                        end

                        if airtable_record
                            begin
                                airtable_record["Decision"] = cohort_application.admissions_decision
                                airtable_record["Dev - Synced Decision"] = cohort_application.admissions_decision
                                airtable_record.save
                            rescue Exception => e
                                failed_airtable_record_update_user_ids << user.id
                                puts "Failed to update Airtable record #{cohort_application.airtable_record_id} for #{user.id}: #{e.message}"
                            end
                        end
                    end
                else
                    puts "Skipping FoB cohort application for #{user.id} because admissions_decision of #{cohort_application.admissions_decision} can't be changed"
                    skipped_fob_cohort_application_user_ids << user.id
                end
            else
                puts "Failed to find FoB cohort application for #{user.id}"
                missing_fob_cohort_application_user_ids << user.id
            end

            processed_users_count += 1
            puts "Processed batch #{processed_users_count/batch_size}/#{(total_count/batch_size).ceil}" if processed_users_count % batch_size == 0
        end

        if missing_fob_cohort_application_user_ids.any?
            puts "Failed to find FoB cohort applications for the following users"
            puts "--------------------------------------------------------------"
            puts missing_fob_cohort_application_user_ids.map { |id| "'#{id}'" }.join(',')
            puts
        end

        if skipped_fob_cohort_application_user_ids.any?
            puts "Skipped FoB cohort applications for the following users"
            puts "--------------------------------------------------------------"
            puts skipped_fob_cohort_application_user_ids.map { |id| "'#{id}'" }.join(',')
            puts
        end

        if missing_airtable_record_user_ids.any?
            puts "Failed to find Airtable records for the following users"
            puts "-------------------------------------------------------"
            puts missing_airtable_record_user_ids.map { |id| "'#{id}'" }.join(',')
            puts
        end

        if failed_airtable_record_update_user_ids.any?
            puts "Failed to update Airtable records for the following users"
            puts "---------------------------------------------------------"
            puts failed_airtable_record_update_user_ids.map { |id| "'#{id}'" }.join(',')
            puts
        end

        puts "Rake task complete!"
    end

    desc "For all cohorts and curriclum templates, the playlist_collections attribute is updated to contain a single playlist collection with an empty title with required_playlist_pack_ids set to the record's required_playlist_pack_ids"
    task :backfill_playlist_collections => :environment do
        def set_playlist_collections(record)
            playlist_collection = {
                # We set the title to an empty string because we only create one playlist collection
                # for each cohort/curriculum template and the UI doesn't really need a title for the
                # playlist collection if only one collection is present because we don't even bother
                # showing the title if that's the case.
                title: '',
                required_playlist_pack_ids: record.required_playlist_pack_ids
            }
            record.playlist_collections = [playlist_collection]
        end

        Cohort.all.edit_and_republish do |cohort|
            set_playlist_collections(cohort)
        end

        CurriculumTemplate.all.each do |curriculum_template|
            set_playlist_collections(curriculum_template)
            curriculum_template.save!
        end
    end

    def convert_cohort_or_template(cohort_or_template)
        if cohort_or_template.is_a?(Cohort) && cohort_or_template.internal_notes.present?
            cohort_or_template.internal_notes.gsub!('quantic.mba', 'quantic.edu')
        end

        cohort_or_template.project_submission_email = 'projects@quantic.edu'

        if cohort_or_template.description.present?
            cohort_or_template.description.gsub!('Smartly', 'Quantic')
        end

        cohort_or_template.periods.each do |period|
            if period['description'].present?
                period['description'].gsub!('quantic.mba', 'quantic.edu')
            end
            
            if period['exercises'].present?
                period['exercises'].each do |exercise|
                    if exercise['message'].present?
                        exercise['message'].gsub!('quantic.mba', 'quantic.edu')
                    end
                end
            end
        end
    end


    task :convert_quantic_references_in_cohorts => :environment do
        Cohort.where(program_type: ['mba', 'emba'], isolated_network: false).edit_and_republish do |cohort|
            convert_cohort_or_template(cohort)
        end
    end

    task :convert_quantic_references_in_templates => :environment do
        CurriculumTemplate.where(program_type: ['mba', 'emba']).each do |template|
            convert_cohort_or_template(template)
            template.save!
        end
    end
end
