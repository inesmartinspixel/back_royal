namespace :rollout do
    desc "Document rollouts. ex. rake rollout:document_rollouts bc=BUILD_COMMIT pc=PREVIOUS_BUILD_COMMIT bn=BUILD_NUMBER bt=BUILD_TIMESTAMP rt=ROLLOUT_TIME (optional) announce=BOOLEAN (optional)"
    task :document_rollouts do

        time_zone = 'Eastern Time (US & Canada)'

        users_by_os_name = {
            'brent' => 'Brent Dearth',
            'oratner' => 'Ori Ratner',
            'isaacmast' => 'Isaac Mast',
            'colbymelvin' => 'Colby Melvin',
            'tammyhart' => 'Tammy Hart',
            'theblang' => 'Matt Langston',
            'nbrustein' => 'Nate Brustein'
        }

        champion = users_by_os_name[ENV['USER']] || 'Unknown'

        if !ENV['bc']
            production_info = parse_config 'https://quantic.edu/vendor/front-royal/modules/BuildConfig/BuildConfig.js'
            staging_info = parse_config 'https://staging.quantic.edu/vendor/front-royal/modules/BuildConfig/BuildConfig.js'

            new_build_commit = staging_info[2]
            last_build_commit = production_info[2]
            build_number = staging_info[0]
            build_time = ActiveSupport::TimeZone[time_zone].parse(staging_info[1])

        else
            new_build_commit = ENV['bc']
            last_build_commit = ENV['pc']
            build_number = ENV['bn']
            build_time = ActiveSupport::TimeZone[time_zone].parse(ENV['bt'])
        end

        announcement = ENV['announce'] && ENV['announce'].downcase == 'true'
        line_prefix = announcement ? "" : "- "
        underscore_replacement = announcement ? "_" : "\\_"

        rollout_time = ENV['rt'] ? ActiveSupport::TimeZone[time_zone].parse(ENV['rt']) : Time.now.in_time_zone(time_zone)

        output_text = ''

        {'feat' => 'Feature', 'fix' => 'Bugfix', 'perf' => 'Performance', 'chore' => 'Miscellaneous', 'Revert' => 'Reverts'}.each do |log_type, pretty_type|

            pattern = log_type == 'Revert' ? "^#{log_type}" : "^#{log_type}\\\("

            command = "git log --grep='#{pattern}' -E --abbrev-commit #{last_build_commit}..#{new_build_commit}"
            changelog = Run.new(command).out
            changes = changelog.split(/^commit/).select { |l| !l.blank? }

            next if changes.empty?

            output_lines = ActiveSupport::OrderedHash.new

            changes.map do |change_string|
                lines = change_string.split("\n").drop(2)

                begin
                    if log_type =="Revert"
                        message = lines.join("\n").split("\n")[2].strip.sub(/^#{log_type} \"/,"- ").gsub(/_/, underscore_replacement)[0...-1]
                    else
                        message = line_prefix + lines.grep(/#{log_type}\(/)[0].strip.sub(/^#{log_type}/,"").gsub(/_/, underscore_replacement)
                    end

                rescue NoMethodError => e
                    message = log_type == "Revert" ? nil : "- No message"
                end
                pr_line = lines.grep(/Merged in/)[0]

                output = message
                if pr_line
                    pr_line.gsub!(/_/, underscore_replacement)
                    output = "#{message} - #{pr_line.strip}"
                end

                # prevent duplicates by only including the one with the PR message in it
                if output_lines[message].nil? || output.size > output_lines[message].size
                    output_lines[message] = output
                end

            end

            if announcement
                output_text += "*#{pretty_type}*\n\n" + output_lines.values.join("\n\n") + "\n\n"
            else
                output_text += "#### " + pretty_type + "\n\n" + output_lines.values.join("\n\n") + "\n\n"
            end

        end


        # if Run.new("git diff 9093c7a0ba21b73235dd9357326f1127cf27613d..b0da3d6dbfe23702209437a0a47a0659da7b2afc -G \"[drop|remove]\" -i db/migrate").out.length > 0
        check_for_danger = "git diff #{last_build_commit}..#{new_build_commit} -G \"[drop|remove|alter]\" -i db/migrate"
        puts "Checking for dangerous db operations..."
        puts check_for_danger
        danger_ouput = Run.new(check_for_danger).out

        if !announcement && danger_ouput.length > 0

puts "Found the following: #{danger_ouput}"

puts %Q~


,---------------------------------------------------------------------,
|                                                                     |
|  WARNING: Potentially destructive database operation found. Please  |
|           ensure that automated migrations are disabled prior to    |
|           deployment, and migrations are run manually after code    |
|           rolled to all production instances.                       |
|                                                                     |
`---------------------------------------------------------------------'

~
        end


if announcement
    puts "@channel - Latest rolled to prod. Please reload! Changes include (https://pedago.atlassian.net/wiki/display/TECH/Rollouts#Rollouts-Build#{build_number}) -- \n\n#{output_text}"
else

puts %Q|
----

## **Build #{build_number}**
- **Built:** #{build_time.in_time_zone(time_zone).strftime('%Y/%m/%d %H:%M')}
- **Commit:** #{new_build_commit}
- **Rollout:** #{rollout_time}
- **Champion:** #{champion}

### Changes

#{output_text}
|

end

    end
end

require 'open-uri'
require 'open3'
require 'active_support/core_ext/object/blank'


def parse_config(url)
    config = read_url(url)
    # if build_config ever changes order, etc, this will have to change
    config.scan(/^.*:\s*'(.*)'/i).flatten
end

def read_url(url)
    page_string = ''
    open(url) do |f|
        page_string = f.read
    end
    page_string
end

class Run
    attr_reader :out, :err

    def initialize(cmd, options = {}, &block)
        @options = {
            ignore: [],
            verbose: false
        }.merge(options)
        @options[:ignore] = [@options[:ignore]] if @options[:ignore].is_a?(String)
        @cmd = cmd
        if block_given?
            yield self
        end
        run
    end

    def run
        puts "Running `#{@cmd}`" if @options[:verbose]
        stdin, stdout, stderr = Open3.popen3(@cmd)
        @out = stdout.read
        @err = stderr.read
        if !@err.blank? && !ignore_error(@err) && !call_error_callback(@err)
            raise "Error occurred while running `#{@cmd}`: #{@err.inspect}"
        end
        if @options[:verbose]
            puts "-- STDOUT:"
            puts @out
            unless @err.blank?
                puts "\n\n-- STDERR:"
                puts @err
            end
        end
        @out
    end

    def on_error(message, &block)
        on_error_callbacks[message] << block
    end

    def call_error_callback(err)
        found_callback = false
        on_error_callbacks.each do |matcher, callbacks|
            if err.match(/#{matcher}/)
                callbacks.each do |callback|
                    callback.call
                    found_callback = true
                end
            end
        end
        return found_callback
    end

    def on_error_callbacks
        @on_error_callbacks ||= Hash.new { |hash, key| hash[key] = [] }
    end

    def ignore_error(err)
        @options[:ignore].each do |message_to_ignore|
            if err.match(/#{message_to_ignore}/)
                return true
            end
        end
        false
    end

    # def run(cmd, raise_on_error = true)
    #     puts cmd
    #
    #     out, err = stdout.read, stderr.read
    #     if err != "" && raise_on_error
    #         raise RuntimeError.new(err)
    #     end
    #     [out, err]
    # end

end