namespace :delayed_job do

    desc "Initializes ScheduleJob tasks"
    task :prepare_scheduled_jobs => :environment do

        ScheduledJob.schedule_all_jobs unless Rails.env.test?

    end

end