namespace :events do

    task :identify_all_users_quickly => :environment do
        batch_size = ENV['batch_size'] || 5000
        batch_sleep = ENV['batch_sleep'] || 1
        single_sleep = ENV['single_sleep']
        joins = ENV['joins']
        where = ENV['where']

        puts "Backfilling with batch_size=#{batch_size}, batch_sleep=#{batch_sleep}, single_sleep=#{single_sleep}"

        query = User.select(:id)

        if joins.present?
            query = query.joins(joins)
        end

        if where.present?
            query = query.where(where)
        end

        total_count = query.size
        processed = 0;
        query.find_in_batches(batch_size: batch_size.to_i) do |users_with_only_id|
            users_with_only_id.each do |u|
                processed += 1
                IdentifyUserJob.set(priority: IdentifyUserJob::MASS_IDENTIFY_PRIORITY).perform_later(u.id)
                sleep single_sleep.to_f if single_sleep.present?
            end
            puts "Scheduled #{processed} of #{total_count} jobs"
            puts "Processed up to #{users_with_only_id.last.id}"
            sleep batch_sleep.to_f
        end
    end

    desc "Identify users with customer.io: supports an options where env var" # i.e. rake events:identify_users where='{"email": "nate@pedago.com"}'
    task :identify_users => :environment do
        total_count = 0

        where = ENV['where'].nil? ? 'TRUE' : ENV['where']

        # In order to DISTINCT when there are json (not jsonb) fields: http://stackoverflow.com/a/23512013/1747491
        query = User.select("DISTINCT ON (users.id) users.*")
            .left_outer_joins(:access_groups, :institutions, :career_profile, :hiring_application, :cohort_applications => :cohort)
            .includes(:access_groups, :institutions, :career_profile, :hiring_application, :cohort_applications => :cohort)
            .where(where)
        total_count = query.size

        count = 0;
        query.find_in_batches(batch_size: 500) do |users|
            users.each do |user|
                count += 1
                user.identify(IdentifyUserJob::MASS_IDENTIFY_PRIORITY)
            end
            puts "Scheduled #{count} of #{total_count} jobs"
        end

        puts "Finished. Scheduled #{total_count} jobs"
    end

    desc "Identify all accepted users of specific program_type(s): supports an optional program_types env var" # i.e. rake events:identify_all_accepted_users_in_program_types program_types="emba mba"
    task :identify_all_accepted_users_in_program_types => :environment do

        subquery = CohortApplication.select('user_id').joins(:cohort).where("cohort_applications.status" => 'accepted')

        # if this env var isn't supplied, this rake task will identify
        # all users that have been accepted into ANY cohort.
        if ENV['program_types'].present?
            program_types_string = ENV['program_types'].split(' ').map { |program_type| "'#{program_type}'" }.join(',')
            subquery = subquery.where("cohorts.program_type IN (#{program_types_string})")
        end

        query = User.where("id IN (#{subquery.to_sql})")

        puts "re-identifying #{query.count} users in #{ENV['program_types']} program types"

        count = 0
        query.find_in_batches(batch_size: 50) do |users|
            users.each do |user|
                count += 1
                user.identify
            end
            puts "Scheduled #{count} of #{query.count} jobs"
        end

        puts "Finished. Scheduled #{count} jobs."
    end

    # Note: Almost identical to the above task, but I couldn't quite do what I wanted with
    # the `where` argument
    desc "Identify all users that have a career_profile with customer.io"
    task :identify_all_users_with_career_profile => :environment do
        total_count = 0

        query = User.includes(:career_profile).where.not(career_profiles: { id: nil })
        total_count = query.count

        query.find_in_batches(batch_size: 500) do |users|
            count = 0;
            users.each do |user|
                count += 1
                user.identify
            end
            puts "Scheduled #{count} of #{total_count} jobs"
        end

        puts "Finished. Scheduled #{total_count} jobs"
    end

    desc "useful for backfilling a range of events that haven't been synced since kinesis stream was disabled"
    task :manual_kinesis_backfill => :environment do

        processed_up_to = ENV['start_time'] ? Time.parse(ENV['start_time']) : RedshiftEvent.maximum(:created_at)

        last_log = Time.at(0)
        ActiveRecord::Base.logger = Logger.new(STDOUT)

        max_event_time = ENV['end_time'] ? Time.parse(ENV['end_time']) : Event.maximum(:created_at)

        # In order to sort by created_at, we have to implement the batching manually
        while processed_up_to < max_event_time

            # since multiple events have the same created_at, it's more
            # complex than just limit 400 every time
            query = Event.reorder(:created_at)
                        .where("created_at > ? and created_at < ?", processed_up_to, max_event_time)

            # we limit to 400 because each batch has to be smaller than 500 to prevent a firebase error
            next_created_at = query.offset(400).first&.created_at || max_event_time
            events = query.where("created_at <= ?", next_created_at).to_a

            break if events.empty?

            all_succeeded = Event::PutEventsInFirehose.put_and_queue_failures(events)
            if !all_succeeded
                raise "Some events not written to the firehose"
            end

            processed_up_to = next_created_at
            if Time.now - last_log > 10.seconds
                puts "Sent events up to #{processed_up_to}"
                last_log = Time.now
            end
        end
    end

    desc "logs an arbitrary message to sentry"
    task :log_to_sentry => :environment do
        raise "Must provide a message value!" unless ENV['message'].present?
        level = ENV['level'] || 'info'
        Raven.capture_message(ENV['message'], { level: level })
    end

end