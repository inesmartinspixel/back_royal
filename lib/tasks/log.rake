require 'zlib'
require 'stringio'

namespace :log do

    desc "pull some logs down from all the prod servers. Ex: rake log:get s='2016/07/17' f='2016/07/18'"
    task :get => :environment do

        client = Aws::S3::Client.new

        start = Time.parse(ENV['s'])
        finish = Time.parse(ENV['f'])
        started = false
        next_marker = nil
        keys = Set.new

        puts "Listing files ... "
        while ( (resp = get_more_log_keys(client, next_marker)) && resp.contents.any?)
            all_keys = resp.contents.map(&:key)
            next_marker = all_keys.last # should be able to use next_marker, but it is nil for some reason
            all_keys.grep(/_var_app_containerfiles_logs_rotated_production.log/).each do |key|
                time = Time.at(key.match(/_var_app_containerfiles_logs_rotated_production.log(\d+).gz/)[1].to_i)
                keys << key if (time > start && time < finish)
            end

            puts "#{all_keys.size} #{keys.size} #{all_keys[0].inspect} - #{all_keys.last.inspect}"
        end

        lines = []

        keys.each_with_index do |key, i|
            puts "Loading #{keys.size} log files: #{(100*i.to_f / keys.size).round}% complete"

            gzipped = client.get_object(bucket: "elasticbeanstalk-us-east-1-889355051875", key: key).body.read
            gz = Zlib::GzipReader.new(StringIO.new(gzipped))
            uncompressed_string = gz.read
            lines += uncompressed_string.split("\n")
        end

        path = Rails.root.join('tmp', "logs_from_s3.#{start.to_s}.#{finish.to_s}.log")
        File.open(path, 'w') do |f|
            f.write(lines.sort.join("\n"))
        end

        puts "logs written to #{path}"
    end

    def get_more_log_keys(client, next_marker)
        client.list_objects({
            bucket: "elasticbeanstalk-us-east-1-889355051875", # required
            # delimiter: "Delimiter",
            # encoding_type: "url", # accepts url
             marker: next_marker,
            # max_keys: 100,
            prefix: "resources/environments/logs/publish/e-vpdztn4xvp",
            # use_accelerate_endpoint: false,
        })
    end

end