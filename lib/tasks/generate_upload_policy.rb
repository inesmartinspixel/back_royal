require 'base64'
require 'openssl'
require 'digest/sha1'


policy_document = '
{
   "expiration": "2025-01-01T00:00:00Z",
    "conditions": [
       {"bucket": "uploads.smart.ly"},
       ["starts-with", "$key", "downloads/"],
       {"acl": "public-read"},
       ["starts-with", "$Content-Type", ""],
       ["starts-with", "$filename", ""],
       ["content-length-range", 0, 524288000]
    ]
}
'

aws_secret_key = '<SMARTLY WEBSERVER SECRET KEY GOES HERE>'


policy = Base64.encode64(policy_document).gsub("\n","")

signature = Base64.encode64(
    OpenSSL::HMAC.digest(
        OpenSSL::Digest::Digest.new('sha1'),
        aws_secret_key, policy)
    ).gsub("\n","")

puts "POLICY: #{policy}"
puts "SIGNATURE: #{signature}"