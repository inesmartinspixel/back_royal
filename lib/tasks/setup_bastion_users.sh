#!/bin/bash

# Run this as sudo on a new EC2 instance to generate user accounts for bastion access.
# see also https://aws.amazon.com/premiumsupport/knowledge-center/new-user-accounts-linux-instance/

# update users as necessary
users=(ori brent nate isaac matt colby stitchdata tableau adam)

for user in "${users[@]}"; do
    user_home_dir=/home/$user
    user_ssh_dir=$user_home_dir/.ssh
    echo "Adding user - ${user} ..."
    adduser $user
    mkdir $user_ssh_dir
    touch $user_ssh_dir/authorized_keys
    read -p "Please paste the public key generated via \`ssh-keygen -y -f smartly-bastion-${user}.pem\`: " public_key
    echo "$public_key" > $user_ssh_dir/authorized_keys
    chmod 700 $user_ssh_dir
    chmod 600 $user_ssh_dir/authorized_keys
    chown -R $user:$user $user_home_dir
done