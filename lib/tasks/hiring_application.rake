namespace :hiring_application do

    desc "backfill salary_ranges to 'any'"
    task :backfill_salary_ranges => :environment do
        hiring_applications = HiringApplication.all
        hiring_applications.each { |hiring_application| hiring_application.update(salary_ranges: ['any']) }
    end

end