require 'csv'
require 'unicode'

namespace :career_profile do

    desc "Determine official/unofficial transcript requirements for all career profiles with education experiences"
    task :determine_and_backfill_transcript_reqs => :environment do
        now = Time.now
        query = CareerProfile.includes(:education_experiences => :educational_organization)

        sql = "
            select ee.career_profile_id
            from education_experiences ee
            where ee.degree_program = true
                and ee.will_not_complete is distinct from true
                and ee.graduation_year <= date_part('year', now())
            group by ee.career_profile_id
            having count(*) filter (where official_transcript_required = true) = 0;
        "
        records = ActiveRecord::Base.connection.execute(sql).to_a

        puts "Determining transcript requirements for #{records.size} career profiles"

        while records.any?
            puts "#{records.size} left..."
            ids = records.slice!(0, 500).map { |record| record['career_profile_id'] }
            query.where(id: ids).each do |career_profile|
                CareerProfile::TranscriptRequirementHelper.determine_transcript_requirements(career_profile)
            end
            sleep 2
        end
        
        puts "Done!\nelapsed = #{Time.now - now}"
    end

    desc "import option values from a text file (eg - `TABLE_NAME=skills_options FILE_NAME=skills.txt rake career_profile:import_option_values`"
    task :import_option_values => :environment do

        # get desired table and filenames
        table_name = ENV['TABLE_NAME']
        file_name = ENV['FILE_NAME'] # expects a newline-delim list of option entries

        if !table_name || !file_name
            raise "Must supply both a table name and a file name"
        end

        # init available locales and normalize options
        available_locales = I18n.available_locales.map(&:to_s)
        inserts = []
        options = File.open(file_name).read
        options.gsub!(/\r\n?/, "\n")

        if options.empty?
            raise "Unable to find any options given path #{file_name}"
        end

        # get translation service
        google_api_key = ENV['GOOGLE_API_KEY']
        EasyTranslate.api_key = google_api_key

        # translate raw optins as necessary, building value hashes
        options.each_line do |option|
            option.strip!
            available_locales.each do | locale |
                translated = locale == "en" ? option : EasyTranslate.translate(option, :to => locale)
                translated = ActiveRecord::Base.sanitize(translated)
                inserts << "(#{translated}, '#{locale}', true, NOW(), NOW())"
            end
        end

        # perform a batch insert
        sql = "INSERT INTO #{table_name}
                (text, locale, suggest, created_at, updated_at)
            VALUES
                #{inserts.join(", ")}
            ON CONFLICT DO NOTHING"
        ActiveRecord::Base.connection.execute(sql)

    end


    desc "import organization-specific option values from a CSV file (eg - `FILE_NAME=organizations.csv rake career_profile:import_organization_option_values`"
    task :import_organization_option_values => :environment do

        file_name = ENV['FILE_NAME'] # expects a csv list of organization option entries

        if !file_name
            raise "Must supply a file name"
        end

        # init available locales and normalize options
        available_locales = I18n.available_locales.map(&:to_s)

        CSV.foreach(file_name, headers: true, encoding: 'utf-8') do |row|

            # expected columns: crunchbase_uuid, primary_role, name, profile_image_url
            type = row['primary_role']


            # we only care about a subset of roles
            next unless ['company', 'group', 'school'].include?(type)

            # determine proper class ot create
            table_name = (type == 'school' ? 'educational_organization_options' : 'professional_organization_options')

            # save each set atomically, using the supplied name for all locale values
            source_id = ActiveRecord::Base.sanitize(row['crunchbase_uuid'])
            text = ActiveRecord::Base.sanitize(row['name'])
            image_url = ActiveRecord::Base.sanitize(row['profile_image_url'])
            inserts = []

            # prep inserts
            available_locales.each do | locale |
                inserts << "(#{text}, '#{locale}', true, NOW(), NOW(), #{source_id}, #{image_url})"
            end

            begin
                print "Adding organization (#{type}) -- #{text} ... "
                # perform a batch insert
                sql = "INSERT INTO #{table_name}
                        (text, locale, suggest, created_at, updated_at, source_id, image_url)
                    VALUES
                        #{inserts.join(", ")}"
                ActiveRecord::Base.connection.execute(sql)
                puts "Complete!"
            rescue
                puts "Failure!"
            end


        end

    end

    desc "backfill english options for existing locale (eg - `LOCALE=ar rake career_profile:backfill_options_for_locale`"
    task :backfill_options_for_locale => :environment do
        target_locale = ENV['LOCALE'] # expects a csv list of organization option entries

        # init available locales and normalize options
        available_locales = I18n.available_locales.map(&:to_s)

        if !target_locale
            raise "Must supply a target locale. example - `LOCALE=ar`"
        elsif !available_locales.include?(target_locale)
            raise "LOCALE \"#{target_locale}\" not available. Valid options - #{available_locales.join(',')}"
        end

        # get translation service
        google_api_key = ENV['GOOGLE_API_KEY']
        EasyTranslate.api_key = google_api_key

        # use reflection to get list of all AutoSuggestOption types
        Rails.application.eager_load! # ensure all classes are loaded
        option_types = ActiveRecord::Base.descendants.select { |c| c.included_modules.include? AutoSuggestOptionMixin }

        option_types.each do |klass|

            puts "\n--- Migrating records for #{klass.to_s} ---\n\n"

            # iterate over all options within the given type for "en"
            klass.where("locale = 'en' AND suggest = true").find_in_batches(batch_size: 500)  do |options|

                # certain tables we wish to localize, but for proper organization names, retain them until otherwise desired
                translate_text = ![CareerProfile::EducationalOrganizationOption, ProfessionalOrganizationOption].include?(klass)

                options.each do |option|

                    if translate_text
                        translated_text = EasyTranslate.translate(option['text'], :to => target_locale)
                        puts "\t#{option.text} -> #{translated_text}"
                    else
                        puts "\t#{option.text}"
                    end

                    # get relevant info, stripping out timestamps and IDs, re-assigning locale
                    target_attrs = option.attributes
                        .except('id', 'created_at', 'updated_at')
                        .merge({ 'locale' => target_locale.to_s })

                    # only translate if told
                    if !translated_text.blank?
                        target_attrs.merge!({'text' => translated_text})
                    end

                    # backfill or not, if unnecessary
                    klass.find_or_create_by!(target_attrs)


                end #batch

            end # en options

        end # type

    end

    desc "shorten text for skill options with text longer than 25 characters"
    task :shorten_skills_options => :environment do
        # initialize hash of desired translated skill option texts
        # the keys are the values returned from the query SELECT text FROM skills_options WHERE length(text) > 25 AND locale = 'en' AND suggest = true;
        # they represent the skill options that have text values with a length greater than 25 characters
        skills_text_hash = {
            "Customer Relationship Management": "CRM",
            "Business Requirement gathering": "Requirements Gathering",
            "Sales Strategy and Management": ["Sales Strategy", "Sales Management"],
            "Object Oriented Programming": "OOP",
            "Leadership and Team Inspiration": "Leadership",
            "Sales/Marketing and Strategic Partnerships": ["Sales", "Marketing", "Strategic Partnerships"],
            "JSON / XML / HTML / Javascript / JQuery": ["JSON", "XML", "HTML", "JavaScript", "JQuery"],
            "Mobile Application Development": "Mobile App Development",
            "Cross-Functional Team Leadership": "Cross-Functional Leader",
            "Excellent Communication Skills": "Communication Skills",
            "Microsoft Active Directory": "Active Directory",
            "Algorithms & Data Structures": ["Algorithms", "Data Structures"],
            "Full-Stack Web Development": "Full-Stack Development"
        }.with_indifferent_access

        # get available locales
        locales = I18n.available_locales.map(&:to_s)

        # get translation service
        google_api_key = ENV['GOOGLE_API_KEY']
        EasyTranslate.api_key = google_api_key

        klass = SkillsOption
        # get skill options with text lengths greater than 25 characters
        skills = klass.where("length(text) > 25 AND locale = 'en' AND suggest = true")

        # shorten the text length of each skill option
        skills.each do |skill|
            puts "Processing: \"#{skill.text}\""
            localized_texts = [] # initialize empty array for holding localized texts
            # update the skill with the shortened and possibly translated text
            shortened_text = skills_text_hash[skill.text]
            if shortened_text.is_a?(Array) # process skill options that need to be split
                shortened_text.each do |text|
                    locales.each do |locale|
                        localized_text = locale == "en" ? text : EasyTranslate.translate(text, :to => locale)
                        localized_original_text = locale == "en" ? skill.text : EasyTranslate.translate(skill.text, :to => locale)
                        # Add the localized origin text attributes to the localized_texts array
                        # for later removal of its related database record. We skip the English
                        # locale since we already have the record in the `skill` variable.
                        if locale != "en"
                            localized_texts.push({
                                "text": localized_original_text,
                                "locale": locale
                            })
                        end
                        begin
                            klass.create_from_hash!(klass.new(), {
                                "text": localized_text,
                                "locale": locale,
                                "suggest": true
                            })
                            puts "#{skill.text} --> #{localized_text} (#{locale})"
                        rescue ActiveRecord::RecordNotUnique
                            puts "ActiveRecord::RecordNotUnique: SkillsOption with text=\"#{localized_text}\" already exists"
                        end
                    end
                end
            else # process skill options that don't need to be split
                locales.each do |locale|
                    localized_text = locale == "en" ? shortened_text : EasyTranslate.translate(shortened_text, :to => locale)
                    localized_original_text = locale == "en" ? skill.text : EasyTranslate.translate(skill.text, :to => locale)
                    # Add the localized origin text attributes to the localized_texts array
                    # for later removal of its related database record. We skip the English
                    # locale since we already have the record in the `skill` variable.
                    if locale != "en"
                        localized_texts.push({
                            "text": localized_original_text,
                            "locale": locale
                        })
                    end
                    begin
                        klass.create_from_hash!(klass.new(), {
                            "text": localized_text,
                            "locale": locale,
                            "suggest": true
                        })
                        puts "#{skill.text} --> #{localized_text} (#{locale})"
                    rescue ActiveRecord::RecordNotUnique
                        puts "ActiveRecord::RecordNotUnique: SkillsOption with text=\"#{localized_text}\" (#{locale}) already exists"
                    end
                end
            end
            puts "Destroying SkillsOption record with text=\"#{skill.text}\""
            skill.destroy # remove the unnecessary skill option record
            localized_texts.each do |attrs|
                record = SkillsOption.where(text: attrs[:text], locale: attrs[:locale]).first
                puts "Destroying SkillsOption record with text=\"#{record.text}\"" if record
                record.destroy if record # remove the records that have the translated original text from the database
            end
        end
    end

    desc "backfill career_profiles for FREEMBA users"
    task :backfill => :environment do
        missing_param = nil
        missing_param = "WHERE" unless ENV['WHERE'].present?
        error_message = "Must specify a #{missing_param} clause, e.g. rake career_profile:backfill JOINS=\"JOIN career_profiles ON career_profiles.user_id = users.id\" WHERE=\"career_profiles.id IS NULL\""
        raise error_message if !missing_param.nil?

        joins = ENV['JOINS']
        where = ENV['WHERE']
        query = User.left_outer_joins(:career_profile).where(career_profiles: {id: nil})
        query = query.joins(joins) unless joins.blank?
        query = query.where(where)

        puts "Creating career_profile for #{query.count} users"
        count = 0
        total = query.count


        #ActiveRecord::Base.transaction do
            while query.any?
                query.limit(1000).each do |user|
                    user.ensure_career_profile

                    count += 1
                    if(count % 100 == 0)
                        puts "Created #{count} of #{total} career_profiles"
                    end
                end
            end
        #end

        puts "Created #{count} career_profiles"
    end

    desc "backfill salary_range to 'prefer_not_to_disclose'"
    task :backfill_salary_range => :environment do
        career_profiles = CareerProfile.all
        career_profiles.each { |career_profile| career_profile.update(salary_range: "prefer_not_to_disclose") }
    end

    desc "backfill featured work experiences for career profiles"
    task :backfill_featured_work_experiences => :environment do
        career_profiles = CareerProfile.includes([:work_experiences]).to_a
        career_profiles.each do |career_profile|
            work_experiences = career_profile.work_experiences.to_a
            if !work_experiences.empty?

                featured_work_experience = work_experiences.select { |work_experience| work_experience.featured? }

                if featured_work_experience.empty?

                    current_work_experiences = work_experiences.select { |work_experience| work_experience.end_date == nil }
                    if work_experiences.length == 1
                        work_experiences.first.featured = true
                    elsif current_work_experiences.length > 0
                        # If the career profile has current work experiences, default the featured work experience
                        # to the current work experience with the most recent start_date.
                        current_work_experiences.sort_by! { |work_experience| work_experience.start_date }
                        current_work_experiences.last.featured = true
                    else
                        # If the career profile has no current work experiences, default the featured work
                        # experience to the work exp with the most recent end_date. If multiple work exps
                        # have the most recent end_date, default the featured work exp to the exp with the
                        # most recent start_date.
                        work_experiences.sort_by! { |work_experience| work_experience.end_date }
                        sameEndDateWorkExps = work_experiences.select { |work_experience| work_experience.end_date == work_experiences.last.end_date }
                        sortedWorkExps = sameEndDateWorkExps.sort_by! { |work_experience| work_experience.start_date }
                        sortedWorkExps.last.featured = true
                    end

                    begin
                        career_profile.save!
                    rescue ActiveRecord::RecordInvalid => e
                        raise e unless e.message.include?("can only have one featured work experience") # may have been dirtied by this point
                    end

                end
            end
        end

        career_profiles = CareerProfile.includes([:work_experiences]).to_a
        career_profiles.each do |career_profile|
            work_experiences = career_profile.work_experiences.to_a
            if !work_experiences.empty?
                featured_work_experience = work_experiences.select { |work_experience| work_experience.featured? }
                if featured_work_experience.empty?
                    raise "No featured work experience for #{career_profile.user_id}"
                elsif featured_work_experience.length > 1
                    raise "More than one featured work experience for #{career_profile.user_id}"
                end
            end
        end
    end

    desc "backfill last_calculated_complete_percentage complete for career profiles"
    task :backfill_last_calculated_complete_percentage => :environment do

        dry_run = ENV['dry_run']

        # Some profiles might actually be incomplete because they switched cohort types after submitting.
        # To guard against this, we search for profiles that are less than 80% complete. If you had filled out a complete
        # career network only profile, you're guaranteed to be at least 83% complete after switching, so any % less than that means you switched
        # prior to submitting and creating a cohort_application, or you were some odd case, like an admin created your cohort_application from scratch.
        #
        # Some profiles might be basically empty, because an admin created the cohort_application from scratch.
        # We don't want to set them to 100% complete, so as a proxy we join again work_experiences to ensure that we only look at profiles
        # that have some work experiences specified.
        #
        # Thus, if you have work experiences specified, and are less than 80% complete, we assume you should actually be 100% complete
        # and update your profile as such.
        query = User.joins(:cohort_applications, :career_profile, cohort_applications: :cohort, career_profile: :work_experiences)
            .includes([:career_profile, career_profile: :work_experiences])
            .where('career_profiles.last_calculated_complete_percentage < 80 OR career_profiles.last_calculated_complete_percentage is null')
            .where("cohort_applications.status = 'pending'")

        puts "Updating career_profile for #{query.count} users"
        count = 0

        while query.any?
            query.limit(50).each do |user|
                career_profile = user.career_profile

                if dry_run
                    next
                end

                career_profile.last_calculated_complete_percentage = 100
                career_profile.save!

                count += 1

                if(count % 10 == 0)
                    puts "Updating #{count} career_profiles"
                end
            end

            # sleep a bit so that Zapier doesn't overflow
            if dry_run
                puts "DRY RUN: pretending to sleep for 5 minutes"
                sleep 1
            else
                puts "Sleeping for 5 minutes..."
                5.times do |n|
                    sleep 60
                    puts " --> asleep for #{n+1} minutes"
                end
            end
        end

        puts "Updated #{count} career_profiles to 100\%"
    end

    desc "Fix users who were willing_to_relocate=false but had locations_of_interest"
    task :fix_locations_of_interest_for_unwilling_to_relocate => :environment do
        query = CareerProfile.where(willing_to_relocate: false)

        puts "Switching #{query.count} users to locations_of_interest = [none]"

        query.each do |career_profile|
            career_profile.locations_of_interest = ["none"]
            career_profile.save!
        end

        puts "Migration complete"
    end

    desc "backfills career_profile_fulltext"
    task :backfill_career_profile_fulltext => :environment do

        class CareerProfileQueryHelper
            include ControllerMixins::StudentNetworkFiltersMixin

            def profiles_active_in_career_or_student_network
                CareerProfile.with(in_careers_network: CareerProfile.active.select(:id).to_sql)
                    .with(in_student_network: self.process_student_network_filters.select(:id).to_sql)
                    .where("id in (select id from in_careers_network union select id from in_student_network)")
            end
        end

        query = CareerProfileQueryHelper.new.profiles_active_in_career_or_student_network
        total_count = query.count

        puts "Backfilling #{total_count} career profiles for fulltext searching"

        count = 0
        batch_size = 200

        query.find_in_batches(batch_size: batch_size) do |career_profiles|
            count += 1
            puts "Batch #{count} of #{(total_count / batch_size).ceil}"
            career_profiles.each do |career_profile|
                RebuildCareerProfileFulltextJob.perform_now(career_profile.id)
            end
        end

        puts "Backfill complete"
    end


    desc "backfills career_profile work_experience industry and role values"
    task :backfill_career_new_work_experience => :environment do
        query = CareerProfile.joins(:work_experiences).where('work_experiences.role IS NULL OR work_experiences.industry IS NULL')
        total_count = query.count

        puts "Backfilling #{total_count} career profiles for new work_experience industry and role values ..."

        count = 0
        batch_size = 50

        query.find_in_batches(batch_size: batch_size) do |career_profiles|
            count += 1
            puts "Batch #{count} of #{(total_count / batch_size).ceil}"
            career_profiles.each do |career_profile|
                career_profile.reconcile_new_work_experience_fields
                career_profile.updated_at = DateTime.now
                career_profile.save!
            end
        end

        puts "Backfilling all role values for any remaining work_experiences ..."
        CareerProfile::WorkExperience.where(role: nil).update_all(role: 'other')

        puts "Backfill complete"
    end


    desc "restores a career profile and associated data to a previous version: supports required FROM_VERSION_ID and TO_VERSION_ID env vars" # i.e. FROM_VERSION_ID=123465789 TO_VERSION_ID=987654321 rake career_profile:restore_career_profile_and_data_to_version
    task :restore_career_profile_and_data_to_version => :environment do
        # get desired from_version_id
        from_version_id = ENV['FROM_VERSION_ID']
        raise "Must supply FROM_VERSION_ID" unless from_version_id
        from_version = CareerProfile::Version.find_by_version_id(from_version_id)
        raise "Cannot find careerProfile::Version with id #{from_version_id}" unless from_version

        # get desired to_version_id
        to_version_id = ENV['TO_VERSION_ID']
        raise "Must supply TO_VERSION_ID" unless to_version_id
        to_version = CareerProfile::Version.find_by_version_id(to_version_id)
        raise "Cannot find careerProfile::Version with id #{to_version_id}" unless to_version

        raise "from_version and to_version must belong to same user!" unless from_version.user_id == to_version.user_id

        puts "Restoring CareerProfile to career_profiles_versions with version_id #{to_version_id} ..."
        # restore to the previous version, minus the career_profiles_versions attrs
        career_profile = CareerProfile.update_from_hash!(to_version.attributes.without(:version_id, :operation, :version_created_at))

        # work_experience (if deleted in timeframe)
        work_experiences_versions = CareerProfile::WorkExperience.connection.execute("
            select *
            from work_experiences_versions
            where career_profile_id = '#{career_profile.id}'
                and operation = 'D'
                and version_created_at > ('#{from_version.version_created_at}'::timestamp - interval '5 seconds')
                and version_created_at < ('#{from_version.version_created_at}'::timestamp + interval '5 seconds')
            group by id, version_id
        ").to_a;
        # restore work_experiences from the versions
        idx = 1
        work_experiences_versions.each do |version|
            puts "Restoring #{idx} of #{work_experiences_versions.count} work_experiences ..."
            career_profile.work_experiences << CareerProfile::WorkExperience.create!(version.except('version_id', 'operation', 'version_created_at', 'id'))
            idx += 1
        end

        # education_experience (if deleted in timeframe)
        education_experiences_versions = CareerProfile::EducationExperience.connection.execute("
            select *
            from education_experiences_versions
            where career_profile_id = '#{career_profile.id}'
                and operation = 'D'
                and version_created_at > ('#{from_version.version_created_at}'::timestamp - interval '5 seconds')
                and version_created_at < ('#{from_version.version_created_at}'::timestamp + interval '5 seconds')
            group by id, version_id
        ").to_a;
        # restore education_experiences from the versions
        idx = 1
        education_experiences_versions.each do |version|
            puts "Restoring #{idx} of #{education_experiences_versions.count} education_experiences ..."
            career_profile.education_experiences << CareerProfile::EducationExperience.create!(version.except('version_id', 'operation', 'version_created_at', 'id'))
            idx += 1
        end


        # FIXME: We had to drop the versions tables due to inflation / performance, as these are managed via
        # an ActiveRecord delete + re-insert scheme. If we are able to restore these tables, we should try to
        # restore these versions. Otherwise, just remove this dead code.
        #
        # see also: db/migrate/20180315220831_drop_options_versions.rb
        # see also: https://trello.com/c/j9VnfGNb/612-perf-optimize-careerprofile-saves
        #
        #
        # # skills_options (+career_profile_skills_options join table)
        # career_profile_skills_options_versions = ActiveRecord::Base.connection.execute("
        #     select *
        #     from career_profiles_skills_options_versions
        #     where career_profile_id = '#{career_profile.id}'
        #         and operation = 'D'
        #         and version_created_at > ('#{from_version.version_created_at}'::timestamp - interval '5 seconds')
        #         and version_created_at < ('#{from_version.version_created_at}'::timestamp + interval '5 seconds')
        #     group by skills_option_id, version_id
        # ").to_a;
        # # restore career_profile_skills_options from the versions
        # idx = 1
        # career_profile_skills_options_versions.each do |version|
        #     puts "Restoring #{idx} of #{career_profile_skills_options_versions.count} career_profile_skills_options ..."
        #     # Check to see if one exists for whatever reason (avoid unqiue violation)
        #     if CareerProfile::CareerProfilesSkillsOptionsJoin.find_by(:career_profile_id => career_profile.id, :skills_option_id => version['skills_option_id']).nil?
        #         CareerProfile::CareerProfilesSkillsOptionsJoin.create!(version.except('version_id', 'operation', 'version_created_at', 'id'))
        #     end
        #     idx += 1
        # end

        # # awards_and_interests_options (+career_profile_awards_and_interests_options join table)
        # career_profile_awards_and_interests_options_versions = ActiveRecord::Base.connection.execute("
        #     select *
        #     from career_profiles_awards_and_interests_options_versions
        #     where career_profile_id = '#{career_profile.id}'
        #         and operation = 'D'
        #         and version_created_at > ('#{from_version.version_created_at}'::timestamp - interval '5 seconds')
        #         and version_created_at < ('#{from_version.version_created_at}'::timestamp + interval '5 seconds')
        #     group by awards_and_interests_option_id, version_id
        # ").to_a;
        # # restore career_profiles_awards_and_interests_options from the versions
        # idx = 1
        # career_profile_awards_and_interests_options_versions.each do |version|
        #     puts "Restoring #{idx} of #{career_profile_awards_and_interests_options_versions.count} career_profile_awards_and_interests_options ..."
        #     # Check to see if one exists for whatever reason (avoid unqiue violation)
        #     if CareerProfile::CareerProfilesAwardsAndInterestsOptionsJoin.find_by(:career_profile_id => career_profile.id, :awards_and_interests_option_id => version['awards_and_interests_option_id']).nil?
        #         CareerProfile::CareerProfilesAwardsAndInterestsOptionsJoin.create!(version.except('version_id', 'operation', 'version_created_at', 'id'))
        #     end
        #     idx += 1
        # end

        career_profile.save!
        puts "Restore complete"
    end

    desc "Fix career profiles with invalid locations_of_interest"
    task :fix_invalid_locations_of_interest => :environment do
        query = CareerProfile
            .where("locations_of_interest && ARRAY[?]", ['none'])
            .where("array_length(locations_of_interest, 1) > 1")

        total_count = query.count
        puts "Fixing locations_of_interest for #{total_count} career profiles"

        idx = 50
        query.find_in_batches(batch_size: 50) do |career_profiles|
            puts "Processing #{idx} of #{total_count} career profiles"

            career_profiles.each do |cp|
                updated_locations_of_interest = cp.locations_of_interest.reject{|e| e == 'none'}
                cp.update_attributes(:locations_of_interest => updated_locations_of_interest)
            end

            idx += 50
        end

        puts "Task complete"
    end

    desc "Find info about cases where the last_calculated_complete_percentage when down"
    task :find_decreasing_percent_complete => :environment do
        start = ENV['start'] ? Time.parse(ENV['start']) : Time.now - 4.weeks

        sql = %Q~
            SELECT
                career_profiles_versions.id
                , career_profiles_versions.user_id
                , career_profiles_versions.version_id
                , career_profiles_versions.version_created_at
                , career_profiles_versions.last_calculated_complete_percentage
            FROM career_profiles_versions
            WHERE version_created_at > '#{start.utc.strftime('%Y-%m-%d %H:%M:%S.%N')}'
            ORDER BY
                career_profiles_versions.id
                , career_profiles_versions.version_created_at
        ~

        # grab all versions
        cases_of_decreasing_value = CareerProfile::Version.connection.execute(sql).to_a
            .group_by { |record| record['id'] } # group by user
            .map { |id, records_for_user|
                user_id = records_for_user[0]['user_id']

                # look for cases where a previous version has
                # a higher last_calculated_complete_percentage than
                # a subsequent one
                _cases_of_decreasing_value = []
                records_for_user = records_for_user.sort_by {|r| r['version_created_at'] }
                records_for_user.each_with_index do |record, i|
                    next unless i > 1
                    last_record = records_for_user[i-1]
                    next unless last_record && last_record['last_calculated_complete_percentage'].present?
                    next unless last_record['last_calculated_complete_percentage'] == 100
                    next unless record['last_calculated_complete_percentage'].present?

                    if last_record['last_calculated_complete_percentage'] > record['last_calculated_complete_percentage']
                        _cases_of_decreasing_value << {
                            user_id: user_id,
                            old_version_id: last_record['version_id'],
                            new_version_id: record['version_id'],
                            diff: [last_record['last_calculated_complete_percentage'], record['last_calculated_complete_percentage']],
                            new_version_created_at: record['version_created_at']
                        }
                    end
                end
                _cases_of_decreasing_value
            }.flatten.compact

        cases_of_decreasing_value.sort_by { |e| e[:new_version_created_at] }.each do |entry|
            user = User.find(entry[:user_id])
            old_career_profile_record = CareerProfile::Version.find_by_version_id(entry[:old_version_id])
            new_career_profile_record = CareerProfile::Version.find_by_version_id(entry[:new_version_id])
            puts "#{user.email} - #{new_career_profile_record.version_created_at} - #{user.id}"
            #puts "  #{old_career_profile_record.last_calculated_complete_percentage} -> #{new_career_profile_record.last_calculated_complete_percentage}"

            ##### Career profile changes

            # NOTE career profile fields that have been removed
            new_career_profile_record.attributes.each do |key, new_val|
                next if %w(version_id version_created_at updated_at id).include?(key)
                old_val = old_career_profile_record[key]
                if new_val.blank? && !old_val.blank?
                    puts " **** REMOVED (PROFILE) #{key}: #{old_val} -> #{new_val}"
                end
            end

            # NOTE career profile fields that changed
            new_career_profile_record.attributes.each do |key, new_val|
                next if %w(version_id version_created_at updated_at id).include?(key)
                old_val = old_career_profile_record[key]
                if !new_val.blank? && new_val != old_val
                    puts " CHANGED (PROFILE) #{key}: #{old_val} -> #{new_val}"
                end
            end

            ##### User changes
            new_user_version = User::Version.where(id: user.id).where("version_created_at between ? and ?",
                new_career_profile_record.version_created_at,
                new_career_profile_record.version_created_at + 5.seconds)
                .first
            if new_user_version
                old_user_version = User::Version.where(id: user.id)
                                    .where("version_created_at < ?", new_user_version.version_created_at)
                                    .reorder("version_created_at").last

                # NOTE user fields that have been removed
                new_user_version.attributes.each do |key, new_val|
                    next if %w(version_id version_created_at updated_at id).include?(key)
                    old_val = old_user_version[key]
                    if new_val.blank? && !old_val.blank?
                        puts " **** REMOVED (USER   ) #{key}: #{old_val} -> #{new_val}"
                    end
                end

                # NOTE user fields that changed
                new_user_version.attributes.each do |key, new_val|
                    next if %w(version_id version_created_at updated_at id).include?(key)
                    old_val = old_user_version[key]
                    if !new_val.blank? && new_val != old_val
                        puts " CHANGED (USER   ) #{key}: #{old_val} -> #{new_val}"
                    end
                end
            end

            ##### Events
            events = RedshiftEvent.select('estimated_time', 'page_load_id', 'event_type', 'label')
                .where(user_id: user.id)
                .where("estimated_time < ?", new_career_profile_record.version_created_at)
                .reorder("estimated_time desc")
                .limit(20)

            puts ""
            puts " - Events"
            events.reverse.each do |event|
                line = [
                    event.estimated_time,
                    event.page_load_id,
                    '%-50.50s' % event.event_type,
                    '%-80.80s' % event.label
                ].join(', ')
                puts "   #{line}"
            end

            puts ""
            puts "--------------------------------------"
            puts ""

        end

        puts cases_of_decreasing_value.map { |e| "'#{e[:user_id]}'" }.join(',')

        # we should also be checking these, but we don't have any record of them historically, so we can't.
        # But, I was able to confirm that they were not the issue with at least 2 profiles that had this issue
        # in June 2018, since those profiles had not been touched since being messed up and they both had awards
        # and skills.
        # awards and interests
        # skills

    end

    desc "backfills SearchHelper entries where interests and skills fulltext vectors are not populated"
    task :backfill_helpers_fulltext => :environment do
        query = CareerProfile::SearchHelper.select(:id, :career_profile_id).where('student_network_interests_vector IS NULL OR skills_vector IS NULL')
        total_count = query.count(:id)

        puts "Backfilling #{total_count} SearchHelper entries fulltext values ..."

        count = 0
        batch_size = 500

        query.find_in_batches(batch_size: batch_size) do |helper_entries|
            count += 1
            puts "Batch #{count} of #{(total_count / batch_size).ceil}"

            career_profile_ids_str = helper_entries.pluck(:career_profile_id).map{|s| "'#{s}'"}.join(', ')

            where_clause = "career_profiles.id IN (#{career_profile_ids_str})"
            CareerProfile::SearchHelper.update_records(where_clause)
        end

        puts "Backfill complete"
    end

    desc "Strip hashtags from student_network_interests_options and update career_profiles_student_network_interests_options"
    task :sanitize_career_profiles_student_network_interests_options => :environment do
        # Gather all student_network_interests_options that contain a hashtag
        results = StudentNetworkInterestsOption.where("text ILIKE '%#%'").to_a
        puts "Found #{results.size} student_network_interests_options to process..."

        # If a StudentNetworkInterestsOption record exists for the text minus the hashtag, update
        # any career_profiles_student_network_interests_options records to point to that record.
        #
        # If one doesn't exist, just update the text of the student_network_interests_options record
        # to not contain a hashtag.
        start_time = Time.now
        results.each_with_index do |option, i|
            sanitized_text = option.text.gsub("#", "")

            # If we find a StudentNetworkInterestsOption that matches the sanitized text and locale,
            # update any career_profiles_student_network_interests_options records to point to it
            # and delete the un-sanitized record.
            matching_record = StudentNetworkInterestsOption.where("text = '#{sanitized_text}' AND locale = '#{option.locale}'").to_a.first
            if matching_record.present?
                CareerProfile::CareerProfilesStudentNetworkInterestsOptionsJoin
                    .where(student_network_interests_option_id: option.id)
                    .to_a.each do |record|

                        # If this particular career profile references the student_network_interests_option_id already,
                        # delete it - we have an index on career_profile_id and student_network_interests_option_id.
                        #
                        # As an example, this could happen if the user added 'blockchain' and '#blockchain' or '#block#chain#'.
                        existing_record = CareerProfile::CareerProfilesStudentNetworkInterestsOptionsJoin
                            .where(career_profile_id: record.career_profile_id)
                            .where(student_network_interests_option_id: matching_record.id)
                            .to_a.first

                        if existing_record.present?
                            record.delete
                            next
                        end

                        record.update_column(:student_network_interests_option_id, matching_record.id)
                    end

                option.delete
            else
                option.update_column(:text, sanitized_text)
            end
        end
        elapsed = Time.now - start_time

        puts "Task complete - elapsed time: #{elapsed} seconds"
    end

    desc "Backfill peer recommendation request events for users who have a pending application for a cohort that supports_peer_recommendations?"
    task :backfill_peer_recommendation_request_events => :environment do
        query = User
            .includes(:career_profile, :cohort_applications => [:cohort => :published_versions])
            .joins(:career_profile => :peer_recommendations, :cohort_applications => :cohort)
            .where(peer_recommendations: {contacted: false}, cohort_applications: {status: 'pending'}, cohorts: {program_type: ['mba', 'emba']})
            .group(:id)

        total_count = query.count.keys.count
        batch_size = 100
        processed_users_count = 0

        puts "Backfilling peer recommendation request events for #{total_count} users..."

        query.in_batches(of: 100).each_record do |user|
            user.career_profile.log_peer_recommendation_request_events
            processed_users_count += 1
            puts "Processed batch #{processed_users_count/batch_size}/#{(total_count/batch_size).ceil}" if processed_users_count % batch_size == 0
        end

        puts "Rake task complete!"
    end
end
