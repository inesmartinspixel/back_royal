# see http://www.ostinelli.net/setting-multiple-databases-rails-definitive-guide/

#task spec: ["redshift:db:test:prepare"]

namespace :redshift do

  namespace :db do |ns|

    desc "validates if redshift connection can currently be established"
    task :validate_connection do
        Rake::Task["environment"].invoke
        begin
            RedshiftEvent.connection.execute('SELECT 1')
            puts 'success'
        rescue PG::ConnectionBad => err
            puts 'failure'
        end
    end

    desc "completely rebuilds the redshift schema, running migrations from the very beginning, preserving data"
    task :rebuild_redshift_schema  do

        puts "#{Time.now}: Starting ..."
        Rake::Task["environment"].invoke

        # NOTE: Audit these tables and ensure all CURRENT columns are accounted for in these column lists
        events_column_list = RedshiftEvent.column_names.join(',')
        user_utm_params_column_list = "user_id, created_at, utm_campaign, utm_content, utm_medium, utm_source"
        user_aliases_column_list = "alias_id"

        puts "#{Time.now}: Renaming events, user_aliases, and user_utm_params ..."
        RedshiftEvent.connection.execute(%Q~
            ALTER TABLE events RENAME TO events_bak;
            ALTER TABLE user_aliases RENAME TO user_aliases_bak;
            ALTER TABLE user_utm_params RENAME TO user_utm_params_bak;
            TRUNCATE TABLE schema_migrations;
        ~)

        puts "#{Time.now}: Creating and migrating events ..."
        Rake::Task["redshift:db:migrate"].invoke

        puts "#{Time.now}: Copying events_bak to events ..."
        RedshiftEvent.connection.execute("INSERT INTO events (#{events_column_list}) SELECT #{events_column_list} FROM events_bak ORDER BY created_at ASC")

        puts "#{Time.now}: Copying user_aliases_bak to user_aliases ..."
        RedshiftEvent.connection.execute("INSERT INTO user_aliases (#{user_aliases_column_list}) SELECT #{user_aliases_column_list} FROM user_aliases_bak")

        puts "#{Time.now}: Copying user_utm_params_bak to user_utm_params ..."
        RedshiftEvent.connection.execute("INSERT INTO user_utm_params (#{user_utm_params_column_list}) SELECT #{user_utm_params_column_list} FROM user_utm_params_bak")

        puts "#{Time.now}: Running VACUUM FULL ..."
        RedshiftEvent.connection.execute(%Q~
              SET wlm_query_slot_count TO 10;
              VACUUM FULL;
              SET wlm_query_slot_count TO 1;
          ~)
        puts "#{Time.now}: Done!"
        puts "Be sure to validate events and events_bak consistency before dropping events_bak,"
        puts "as well as repeating the process for user_aliases and user_utm_params tables."
    end

    task :drop do
      Rake::Task["db:drop"].invoke
    end

    task :create do
      Rake::Task["db:create"].invoke
    end

    task :setup do
      Rake::Task["environment"].invoke

      ActiveRecord::Base.connection.execute(%Q|
        CREATE TABLE IF NOT EXISTS schema_migrations (
            version character varying(255) NOT NULL
        );

        CREATE TABLE IF NOT EXISTS ar_internal_metadata (
          key character varying NOT NULL,
          value character varying,
          created_at timestamp without time zone NOT NULL,
          updated_at timestamp without time zone NOT NULL
        );
      |)

    end

    # alias for migrate
    task :setup_and_migrate do
      Rake::Task["redshift:db:migrate"].invoke
    end

    task :migrate do
      Rake::Task["redshift:db:setup"].invoke
      Rake::Task["db:migrate"].invoke
    end

    task :"migrate:up" do
      Rake::Task["db:migrate:up"].invoke
    end

    task :rollback do
      Rake::Task["db:rollback"].invoke
    end

    task :seed do
      Rake::Task["db:seed"].invoke
    end

    task :version do
      Rake::Task["db:version"].invoke
    end

    desc "Execute a .sql file against Redshift"
    task :execute_sql_file do
        Rake::Task["environment"].invoke

        target_file = ENV['file']

        unless target_file
            puts "Must specify a target file e.g. rake redshift:db:execute_sql_file file=path/to/file"
        end

        file_contents = File.read(target_file)
        puts "Executing contents of #{target_file}"
        start_time = Time.now
        RedshiftEvent.connection.execute(%Q|
            #{file_contents}
        |)
        elapsed = ((Time.now - start_time) / 60).ceil
        puts "Finished in #{elapsed} minutes"
    end

    namespace :schema do
      task :load do
        Rake::Task["db:schema:load"].invoke
      end

      task :dump do
        Rake::Task["db:schema:dump"].invoke
      end
    end

    namespace :test do
      task :prepare do
        Rake::Task["db:test:prepare"].invoke
      end
    end

    # append and prepend proper tasks to all the tasks defined here above
    ns.tasks.each do |task|
      task.enhance ["redshift:set_custom_config"] do
        Rake::Task["redshift:revert_to_original_config"].invoke
      end
    end
  end

  task :set_custom_config do
    # save current vars
    @original_config = {
      env_schema: ENV['SCHEMA'],
      config: Rails.application.config.dup
    }

    # set config variables for custom database
    ENV['SCHEMA'] = "db_redshift/schema.rb"
    Rails.application.config.paths['db'] = ["db_redshift"]
    Rails.application.config.paths['db/migrate'] = ["db_redshift/migrate"]
    Rails.application.config.paths['db/seeds'] = ["db_redshift/seeds.rb"]
    Rails.application.config.paths['config/database'] = ["config/database_redshift.yml"]
  end

  task :revert_to_original_config do
    # reset config variables to original values
    ENV['SCHEMA'] = @original_config[:env_schema]
    Rails.application.config = @original_config[:config]
    ActiveRecord::Base.establish_connection("#{Rails.env}".to_sym)
  end
end