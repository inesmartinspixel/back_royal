namespace :s3_documents do

    desc "Prunes legacy S3Assets from /avatars directory in uploads.smart.ly"
    task :prune_legacy_avatars => :environment do
        query = S3Asset.where(directory: 'avatars/')
        total = query.count
        batch = 0

        puts "About to delete #{total} s3_assets records with directory => \'avatars/\'"

        start_time = Time.now
        query.in_batches do |relation|
            relation.delete_all

            batch += 1
            puts "Completed #{batch} of #{total/1000} batches."
        end
        elapsed = Time.now - start_time

        puts "Done - total elapsed time: #{elapsed} seconds"

    end

end