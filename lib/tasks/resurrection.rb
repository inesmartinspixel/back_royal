#!/bin/ruby


# NOTES:
# This was used on 2017/05/18 as part of https://trello.com/c/YkzqoTJa

# To use this,
# 1. set the USER_ID and CAREER_PROFILE_ID
# 2. set the list of S3_ASSETS.  This has to include any assets that are
#    deleted by user.destroy and that have foreign key constraints.  The underlying
#    files will have been delete from s3, however, so you will end up with records
#    in s3_assets but no files in s3 attached to them
# 3. set SOURCE_HOST, SOURCE_DB, SOURCE_USER
# 4. set TARGET_HOST, TARGET_DB, TARGET_USER
# 5. ./lib/resurrection.rb should put the user into the target database


require 'open3'

ROOT = "#{__dir__}/.."

USER_ID='77b6e4ba-1827-4038-add4-cd17807d58c5'
CAREER_PROFILE_ID='6d524b1c-38dc-480f-9d64-1a1bd58ef423'
S3_ASSETS= [
    'e9f4d358-9121-46f6-88e3-6b3ee1e7b1b3' # resume_id
]

DIR="#{ROOT}/tmp/resurrection"
SOURCE_HOST='smartly-production.csjz83rbdbqn.us-east-1.rds.amazonaws.com'
SOURCE_DB='back_royal'
SOURCE_USER='readonly'

TARGET_HOST='localhost'
TARGET_DB='back_royal_development'
TARGET_USER='nbrustein'

# TARGET_HOST='smartly-staging.cgueubjoqruk.us-east-1.rds.amazonaws.com'
# TARGET_DB='back_royal'
# TARGET_USER='ebroot'

# TARGET_HOST='smartly-production.csjz83rbdbqn.us-east-1.rds.amazonaws.com'
# TARGET_DB='back_royal'
# TARGET_USER='ebroot'

def run_sql(host, db, user, cmd)
    puts cmd

    # see docs here for why we can't use -c when we're trying to
    # use \copy: https://www.postgresql.org/docs/9.2/static/app-psql.html
    #
    # instead, we use -f
    filename = DIR + '/cmd.sql'
    File.open(filename, 'w+') do |f|
        f.write(cmd)
    end

    run %Q~psql -h #{host} #{db} \
    -U #{user} \
    -f #{filename}~
ensure
    File.unlink(filename)
end

def on_source(cmd)
    run_sql(SOURCE_HOST, SOURCE_DB, SOURCE_USER, cmd)
end

def on_target(cmd)
    run_sql(TARGET_HOST, TARGET_DB, TARGET_USER, cmd)
end

def run(cmd)
    Open3.popen3(cmd) do |stdin, stdout, stderr, wait_thr|
        err = stderr.read
        if err != ""
            raise RuntimeError.new(err)
        end
    end
end

on_source %Q~\\copy (Select users.* from users where id='#{USER_ID}') To '#{DIR}/users.csv' With CSV;~

# the actual files have been deleted out of s3, but in order to resolve foriegn keys,
# we need to put the records back
id_string = S3_ASSETS.map { |id| "'#{id}'" }.join(',')
on_source %Q~\\copy (Select s3_assets.* from s3_assets where id in (#{id_string})) To '#{DIR}/s3_assets.csv' With CSV;~

user_id_tables = %w(
    access_groups_users
    career_profiles
    career_profile_searches
    cohort_applications
    users_roles
    lesson_streams_progress
    lesson_progress
    lesson_stream_locale_packs_users
    s3_identification_assets

    user_participation_scores
    institutions_users
    editor_lesson_sessions
    experiment_variations_users
    hiring_applications
    institutions_reports_viewers
    subscriptions_users
    user_chance_of_graduating_records
    user_project_scores
)

user_id_tables.each do |table|
    on_source %Q~\\copy (Select #{table}.* from #{table} where user_id='#{USER_ID}') To '#{DIR}/#{table}.csv' With CSV;~
end

candidate_id_tables = %w(
    hiring_relationships
    candidate_position_interests
)
candidate_id_tables.each do |table|
    on_source %Q~\\copy (Select #{table}.* from #{table} where candidate_id='#{USER_ID}') To '#{DIR}/#{table}.csv' With CSV;~
end

career_profile_id_tables = %w(
    career_profile_fulltext
    career_profiles_skills_options
    education_experiences
    work_experiences
    career_profiles_awards_and_interests_options
    peer_recommendations
    career_profiles_student_network_interests_options
)

career_profile_id_tables.each do |table|
    on_source %Q~\\copy (Select #{table}.* from #{table} where career_profile_id='#{CAREER_PROFILE_ID}') To '#{DIR}/#{table}.csv' With CSV;~
end


################################################

restore_commands = (
    ['users', 's3_assets'] +
    user_id_tables +
    candidate_id_tables +
    career_profile_id_tables
).map do |table|
    # we have to use \copy here instead of copy so it can work on prod where
    # we don't have superuser access
    "\\copy #{table} FROM '#{DIR}/#{table}.csv' DELIMITER ',' CSV;"
end.join("\n")

restore_cmd = %~
begin;
#{restore_commands}
end;
~


on_target restore_cmd