namespace :airtable do

    # See https://trello.com/c/OM8JohtG for notes on the archiving process

    def lookup_and_destroy_airtable_application_with_retry(klass, database_record, dry_run)
        airtable_record = nil
        begin
            retries ||= 0

            # The class for the live API will have the find_by_id_with_fallback method and thus we should
            # use it for much faster lookup. The class for the adhoc archive API will need to use a filter
            # search since the airtable_record_id changes when the base is duplicated.

            # NOTE: Do not copy and paste this code.  There is a better way to do this now, using
            # Airtable::Application.klass_for_base_key.  See DeleteFromAirtableJob for an example
            if klass.respond_to?(:find_by_id_with_fallback)
                airtable_record = klass.find_by_id_with_fallback(database_record)
            else
                airtable_record = klass.all(filter: "{Application ID} = '#{database_record.id}'").first
            end
        rescue => e
            retries += 1
            retry if retries < 3

            puts "#{database_record.id} application not found in Airtable"
            throw e
        end

        if airtable_record.nil?
            puts "#{database_record.id} application not found in Airtable"
            return nil
        end

        begin
            retries ||= 0
            if !dry_run
                airtable_record.destroy
            end
        rescue => e
            retries += 1
            retry if retries < 3
            throw e
        end

        return airtable_record
    end

    def get_archive_info(mba_cutoff_name, non_mba_cutoff_date)
        archive_border_cohort = nil
        if mba_cutoff_name.present?
            archive_border_cohort = Cohort.find_by_name(mba_cutoff_name)
        else
            # Use the cohort from two cycles ago so that we keep both the current and last cycle in the live base.
            archive_border_cohort = Cohort.where(program_type: 'mba')
                .to_a
                .reject { |c| c.application_deadline.nil? || c.application_deadline >= Time.now}
                .sort_by(&:application_deadline)
                .reverse
                .second
        end

        raise "Cutoff cohort not found" if archive_border_cohort.nil?

        # If we specified a non_mba_cutoff_date override then use it, otherwise use the border cohort's
        # application_deadline
        archive_non_mba_date = non_mba_cutoff_date&.to_time || archive_border_cohort.application_deadline

        # Just in case we have a DST issue or something, we wouldn't want to archive the MBA cohort without
        # the EMBA cohort in the same cycle.
        raise "Check the sister EMBA cohort" if archive_border_cohort.sister_cohort.nil? ||
            archive_border_cohort.sister_cohort.application_deadline != archive_border_cohort.application_deadline

        # We check for application_deadline.present? because some older cohorts have a nil application_deadline, though
        # now that we have done the first archive run this could be removed.
        archive_mba_emba_cohorts = Cohort.where(program_type: ['mba', 'emba'])
            .to_a
            .reject {|c| c.application_deadline.present? && c.application_deadline > archive_border_cohort.application_deadline}

        return {
            archive_border_cohort: archive_border_cohort,
            archive_mba_emba_cohorts: archive_mba_emba_cohorts,
            archive_non_mba_date: archive_non_mba_date,
            archive_base_key: "AIRTABLE_BASE_KEY_ARCHIVE_#{archive_border_cohort.application_deadline.strftime('%Y%m%d')}"
        }
    end

    task :show_archive_info => :environment do
        mba_cutoff_name = ENV["mba_cutoff_name"]
        non_mba_cutoff_date = ENV["non_mba_cutoff_date"]
        archive_info = get_archive_info(mba_cutoff_name, non_mba_cutoff_date)

        puts "#{archive_info[:archive_mba_emba_cohorts].pluck(:name)} will be archived"
        puts "Other applications before #{archive_info[:archive_non_mba_date]} will be archived"
        puts "Border cohort is #{archive_info[:archive_border_cohort].name}"
        puts "Archive base key will be #{archive_info[:archive_base_key]}"
    end

    # Note: Before running this task ensure that you have duplicated the live base into a new archive base
    # and that you have set up the appropriate environment variable for the new archive base. Use
    # rake airtable:show_archive_info to see where we will split the base.
    desc "Prune the archive base"
    task :prune_archive => :environment do
        dry_run = ENV["dry_run"]
        mba_cutoff_name = ENV["mba_cutoff_name"]
        non_mba_cutoff_date = ENV["non_mba_cutoff_date"]
        archive_info = get_archive_info(mba_cutoff_name, non_mba_cutoff_date)

        # Create Ad-hoc API for the archive base. See https://github.com/sirupsen/airrecord#ad-hoc-api
        # NOTE: Do not copy and paste this code.  There is a better way to do this now, using
        # Airtable::Application.klass_for_base_key.  See DeleteFromAirtableJob for an example
        ArchivedApplication = Airrecord.table(ENV["AIRTABLE_API_KEY"], ENV[archive_info[:archive_base_key]], 'Applications')

        # Prune any applications from the archive base that should not be archived yet. Copying all of the data
        # first in the UI and then pruning here allows us to retain comments in Airtable.
        pruned_from_archive = []
        query = CohortApplication
            .where("applied_at >= ?", archive_info[:archive_non_mba_date])
            .where.not(cohort_id: archive_info[:archive_mba_emba_cohorts].pluck(:id))

        puts query.to_sql

        query.each do |database_application|
            # This is a bit annoying. The lookup_and_destroy_airtable_application_with_retry function can return nil or throw an exception
            # depending on which `find` logic is run. This is probably due to the fact that sometimes we want the find logic to throw an exception
            # in order for delayed_jobs to retry. I think we should have made it consistently throw in hindsight, but I'm not going to touch
            # anything but here right now. We need to `next` on exception here because when pruning the archive after re-enabling the sync_with_airtable
            # job there will be applications in the above query that are not in the archive base, which was copied from the live base while the job
            # was turned off.
            record = nil
            begin
                record = lookup_and_destroy_airtable_application_with_retry(ArchivedApplication, database_application, dry_run)
            rescue => e
                next
            end
            next if record.nil? # I believe this is unnecessary now because we'll always get an exception when doing this `find` logic

            puts "#{[database_application.id, database_application.applied_at, database_application.cohort.name]} pruned from archive base"
            pruned_from_archive << database_application.id
        end

        puts "#{pruned_from_archive.length} pruned from archive base"
    end

    # Note: Before running this task ensure that you have duplicated the live base into a new archive base
    # and that you have set up the appropriate environment variable for the new archive base. After running
    # verify that the number of applications with the airtable_base_key set to the ARCHIVE_X ENV variable matches
    # the number of applications obtained with the below query.
    desc "Prune the live base"
    task :prune_live => :environment do
        dry_run = ENV["dry_run"]
        mba_cutoff_name = ENV["mba_cutoff_name"]
        non_mba_cutoff_date = ENV["non_mba_cutoff_date"]
        archive_info = get_archive_info(mba_cutoff_name, non_mba_cutoff_date)

        # Prune any applications from the live base that are being archived and set their new airtable database fields.
        #
        # Note: This could be a lot faster if we issued a filter on Airtable's side using the "Submit Date" field that we
        # sync from our database's "applied_at" field, rather than iterating through each application on our end like this task
        # is currently doing. The problem right now is that this "Submit Date" isn't completely accurate due to various reasons
        # noted in the below Trello ticket.
        # See https://trello.com/c/Hb3wIbgO
        pruned_from_live = []

        archive_non_mba_date = archive_info[:archive_non_mba_date]
        archive_mba_emba_cohort_ids_string = archive_info[:archive_mba_emba_cohorts].pluck(:id).map { |id| "'#{id}'" }.join(',')
        query = CohortApplication
            .joins(:cohort)
            .where(
                "airtable_base_key = 'AIRTABLE_BASE_KEY_LIVE'
                AND (
                        (applied_at < '#{archive_non_mba_date}' AND cohorts.program_type not in ('mba', 'emba'))
                        OR cohort_applications.cohort_id IN (#{archive_mba_emba_cohort_ids_string})
                    )"
            )

        puts query.to_sql

        query.each do |database_application|

            if lookup_and_destroy_airtable_application_with_retry(Airtable::Application, database_application, dry_run).nil?
                next
            end

            if !dry_run
                database_application.update_column(:airtable_base_key, archive_info[:archive_base_key])
            end

            puts "#{[database_application.id, database_application.applied_at, database_application.cohort.name]} pruned from live base"
            pruned_from_live << database_application.id
        end

        puts "#{pruned_from_live.length} pruned from live base"
    end

    # ex. rake airtable:resync_cohort_applications where="cohorts.program_type = 'emba'"
    desc "Resync cohort applications"
    task :resync_cohort_applications => :environment do
        total_count = 0
        where = ENV['where'].nil? ? 'TRUE' : ENV['where']

        query = CohortApplication.joins(:cohort, :user).where(where).where(airtable_base_key: 'AIRTABLE_BASE_KEY_LIVE')
        total_count = query.size

        count = 0;
        query.find_in_batches(batch_size: 500) do |cohort_applications|
            cohort_applications.each do |cohort_application|
                count += 1
                cohort_application.update_in_airtable(SyncWithAirtableJob::MASS_SYNC_PRIORITY)
            end
            puts "Scheduled #{count} of #{total_count} jobs"
        end

        puts "Finished. Scheduled #{total_count} jobs"
    end

    desc "Detect duplicates in Airtable. Only run this locally as we are populating all rows into memory."
    task :find_duplicates => :environment do
        base_key = ENV["base_key"]

        application_ids = []
        AdhocApplication = Airrecord.table(ENV["AIRTABLE_API_KEY"], base_key, "Applications")
        AdhocApplication.all.each do |application|
            application_ids << application["Application ID"]
        end

        puts "Checked #{application_ids.size} applications"

        application_ids = application_ids.compact

        # https://stackoverflow.com/a/8922049/1747491
        application_ids.group_by{ |e| e }.select { |k, v| v.size > 1 }.map(&:first)
        puts application_ids.sort.chunk{ |e| e }.select { |e, chunk| chunk.size > 1 }.map(&:first)
    end

    task :backfill_record_id => :environment do
        dry_run = ENV['dry_run']
        base_key = ENV['base_key']
        base_key_value = ENV[base_key]

        throw "Please pass a valid base key" if base_key_value.nil?

        AdhocApplication = Airrecord.table(ENV["AIRTABLE_API_KEY"], base_key_value, 'Applications')

        CohortApplication.where(airtable_base_key: base_key).find_in_batches(batch_size: 100) do |cohort_applications|
            cohort_applications.each do |database_application|
                search = nil

                begin
                    search = AdhocApplication.all(filter: "{Application ID} = '#{database_application.id}'")
                rescue => e
                    puts "Trouble connecting to Airtable for #{database_application.id}"
                    next
                end

                if search.length > 1
                    puts "Found multiple Airtable applications for #{database_application.id}"
                    next
                elsif search.length == 0
                    puts "Could not find Airtable application for #{database_application.id}"
                    next
                end

                airtable_application = search.first
                database_application.update_column(:airtable_record_id, airtable_application.id) unless dry_run
                puts "Backfilling #{database_application.id} with airtable_record_id #{airtable_application.id}"
            end
        end
    end

    task :ensure_present => :environment do
        raise "Don't specify both 'names' and 'all' arguments" if !ENV['names'].nil? && !ENV['all'].nil?

        if ENV['names'].present?
            names = ENV['names'].split(' ')
        elsif ENV['all'] == "true"
            names = Cohort.all.pluck(:name)
        else
            names = [Cohort.promoted_cohort("mba").name, Cohort.promoted_cohort("emba").name, Cohort.promoted_cohort("career_network_only").name]
        end

        names.each do |name|
            # See https://airtable.com/apphAK88bOEUndRMb/api/docs#curl/table:main
            # See https://support.airtable.com/hc/en-us/articles/203255215-Formula-Field-Reference
            records = Airtable::Application.all(filter: "IF({Cohort Name} = '#{name}', 1, 0)")

            air_ids = records.pluck(:application_id)

            # Now verify that all applications in the database are represented in the airtable
            database_ids = CohortApplication.includes(:cohort).where(status: 'pending', cohorts: {name: name}).pluck("id")
            ids_missing = database_ids - air_ids

            puts "Applications missing from Airtable for #{name} (#{ids_missing.size}):"

            ids_missing_sql_string = ids_missing.map{ |id| "'#{id}'"}.join(',')
            puts "SELECT u.name, u.email, ca.updated_at AS ca_updated_at, cp.updated_at AS cp_updated_at, ca.*
                FROM cohort_applications ca
                JOIN users u ON ca.user_id = u.id
                JOIN career_profiles cp ON cp.user_id = u.id
                WHERE ca.id IN (#{ids_missing_sql_string});"
        end
    end

    task :backfill => :environment do
        raise "Please specify a where clause (use where='TRUE' for all records)" if ENV['where'].nil?

        total_count = 0
        where = ENV['where']

        # Note: The query is simpler if done with respect to users (similarly to the identify backfill) since users connects to
        # both cohort_applications and career_profiles. Be sure that the users query is DISTINCT as there was a bug in the first run of this script that
        # queued the same sync jobs for the same user multiple times, and since the jobs were being picked up by multiple workers at the around same time the
        # workers did not find an existing Airtable row and thus duplicate rows were made. This shouldn't be a problem in the future anyway since going forward
        # we should be creating new rows in real-time, but just wanted to make note of this mistake nonetheless.
        query = User.select("DISTINCT ON (users.id) users.*")
            .left_outer_joins(:career_profile, :cohort_applications)
            .includes(:career_profile, :cohort_applications)
            .where('cohort_applications.id IS NOT NULL')
            .where(where)

        total_count = query.size

        count = 0
        query.find_in_batches(batch_size: 1000) do |users|
            users.each do |user|
                count += 1
                user.update_in_airtable
            end
            puts "Scheduled #{count} of #{total_count} users"
        end

        puts "Finished. Scheduled #{total_count} users"
    end

    task :backfill_missing => :environment do
        names = Cohort.all.pluck(:name)
        names.each do |name|
            records = Airtable::Application.all(filter: "IF({Cohort Name} = '#{name}', 1, 0)")
            air_ids = records.pluck(:application_id)
            database_ids = CohortApplication.includes(:cohort).where(cohorts: {name: name}).pluck(:id)
            ids_missing = database_ids - air_ids

            CohortApplication.where(id: ids_missing).each do |cohort_application|
                cohort_application.create_or_update_in_airtable
            end
        end
    end

    task :backfill_all => :environment do
        total_count = CohortApplication.count

        count = 0
        CohortApplication.find_in_batches(batch_size: 500) do |cohort_applications|
            cohort_applications.each do |cohort_application|
                cohort_application.create_or_update_in_airtable
                count += 1
            end
            puts "Scheduled #{count} of #{total_count} applications"
        end

        puts "Finished. Scheduled #{total_count} total applications"
    end

    # Backfill every non-pending application with "manual_admin_decision". This was to mark every currently
    # decided application at the time that https://trello.com/c/2oXGlfeo rolled.
    task :backfill_manual_decision => :environment do
        query = CohortApplication.where(admissions_decision: nil).where.not(status: "pending")
        total_count = query.count

        count = 0
        query.find_in_batches(batch_size: 500) do |cohort_applications|
            cohort_applications.each do |cohort_application|
                cohort_application.admissions_decision = "manual_admin_decision"
                cohort_application.save!
                count += 1
            end
            puts "Scheduled #{count} of #{total_count} applications"
        end

        puts "Finished. Scheduled #{total_count} total applications"
    end

    task :backfill_base_key_live => :environment do
        dry_run = ENV['dry_run']
        CohortApplication.where(airtable_base_key: nil).update_all(airtable_base_key: "AIRTABLE_BASE_KEY_LIVE")
    end

    # rake airtable:sync_archived_rubric_data date=20181008
    task :sync_archived_rubric_data => :environment do
        date = ENV['date']

        # Only the 20181008 has rubric_interview_scheduled
        fields = [
            Airtable::Application.column_map["id"],
            Airtable::Application.from_airtable_column_map['rubric_interview'],
            Airtable::Application.from_airtable_column_map['rubric_interviewer']
        ]
        if date == '20181008'
            fields.push(Airtable::Application.from_airtable_column_map['rubric_interview_scheduled'])
        end

        num_not_synced = 0

        AdhocApplication = Airrecord.table(ENV["AIRTABLE_API_KEY"], ENV["AIRTABLE_BASE_KEY_ARCHIVE_#{date}"], "Applications")
        AdhocApplication.all(
            fields: fields,
            view: 'Dev - Sandbox'
        ).each do |airtable_application|

            id = airtable_application[Airtable::Application.column_map["id"]]

            # guard against empty, accidentally created rows
            if id.nil?
                puts "empty id row"
                num_not_synced += 1
                next
            end

            # guard against deleted cohort applications
            database_application = CohortApplication.find_by_id(id)
            if database_application.nil?
                puts "application no longer exists"
                num_not_synced += 1
                next
            end

            # ensure that a one-off cohort app wasn't recreated in a newer base
            if database_application.airtable_base_key != "AIRTABLE_BASE_KEY_ARCHIVE_#{date}"
                puts "airtable_base_key mismatch"
                num_not_synced += 1
                next
            end

            database_application.update({
                rubric_interview: airtable_application[Airtable::Application.from_airtable_column_map['rubric_interview']],
                rubric_interviewer: airtable_application[Airtable::Application.from_airtable_column_map['rubric_interviewer']],
                rubric_interview_scheduled: airtable_application[Airtable::Application.from_airtable_column_map['rubric_interview_scheduled']]
            })
        end

        puts "did not sync #{num_not_synced}"
    end
end