namespace :location do

    # Backfill Google Places for users who need a place_details and require manual verification
    desc "Output Google Place ids and details for career_profiles"
    task :backfill_places_verify => :environment do
        dry_run = ENV['dry_run']

        # We wanted to go ahead and backfill the 11kish users who have filled out the mailing address fields (thus giving us data to backfill
        # a location with), but only want to manually verify the smaller subset of about 1k that have either been accepted into the career network
        # or have a pending application (and thus may be accepted).
        # https://sqlpad.pedago-tools.com/queries/xY9mFJbFzk1gV1sA
        user_ids_to_manually_check = ActiveRecord::Base.connection.execute("
            select u.id
            from users u
            join career_profiles cp on cp.user_id = u.id
            left join cohort_applications ca on ca.user_id = u.id
            where city != '' and (state != '' or country != '') and
                cp.place_id is null and (u.can_edit_career_profile = true or ca.status = 'pending')
            group by u.id;
        ").values.flatten

        # Had to do an OR for the place fields because for some reason dfc8c362-f3be-48f3-979f-b5ca546cbe50 (abbytmars@gmail.com) has place_details but no place_id
        # https://sqlpad.pedago-tools.com/queries/44X92DiJZ4cpKhBY
        users_query = User.joins(:career_profile).where("city != '' and (state != '' or country != '')
                and (career_profiles.place_id IS NULL OR career_profiles.place_details::TEXT = '{}'::TEXT)").includes(:career_profile)

        puts "Attempting to backfill locations for and printing out #{user_ids_to_manually_check.size} users for manual verification"

        users_query.find_in_batches(batch_size: 1000) do |users|
            users.each do |user|
                begin
                    if user_ids_to_manually_check.include?(user.id)
                        query_string = "#{user.city} #{user.state} #{user.country}"
                        if !user.career_profile.backfill_place(query_string)
                            next # Skip if there is an error
                        end

                        if !dry_run
                            user.career_profile.save!
                        end
                    end
                rescue StandardError => e
                    puts "#{user.id} FAILED: #{e.message}"
                end
            end
        end
    end

    task :backfill_places_no_verify => :environment do
        dry_run = ENV['dry_run']

        # We wanted to go ahead and backfill the 11kish users who have filled out the mailing address fields (thus giving us data to backfill
        # a location with), but only want to manually verify the smaller subset of about 1k that have either been accepted into the career network
        # or have a pending application (and thus may be accepted).
        # https://sqlpad.pedago-tools.com/queries/xY9mFJbFzk1gV1sA
        user_ids_to_manually_check = ActiveRecord::Base.connection.execute("
            select u.id
            from users u
            join career_profiles cp on cp.user_id = u.id
            left join cohort_applications ca on ca.user_id = u.id
            where city != '' and (state != '' or country != '') and
                cp.place_id is null and (u.can_edit_career_profile = true or ca.status = 'pending')
            group by u.id;
        ").values.flatten

        # Had to do an OR for the place fields because for some reason dfc8c362-f3be-48f3-979f-b5ca546cbe50 (abbytmars@gmail.com) has place_details but no place_id
        # https://sqlpad.pedago-tools.com/queries/44X92DiJZ4cpKhBY
        users_query = User.joins(:career_profile).where("city != '' and (state != '' or country != '')
                and (career_profiles.place_id IS NULL OR career_profiles.place_details::TEXT = '{}'::TEXT)").includes(:career_profile)

        puts "Attempting to backfill #{users_query.size} locations for career_profiles that do not require manual verification"

        users_query.find_in_batches(batch_size: 1000) do |users|
            users.each do |user|
                begin
                    if user_ids_to_manually_check.include?(user.id)
                        puts "Skipping #{user.id} as they require manual verification"
                        next
                    end

                    query_string = "#{user.city} #{user.state} #{user.country}"
                    if !user.career_profile.backfill_place(query_string)
                        next
                    end

                    if !dry_run
                        user.career_profile.save!
                    end
                rescue StandardError => e
                    puts "#{user.id} FAILED: #{e.message}"
                end
            end
        end
    end

    # Backfill the locations_of_interest table with cherry picked data retrieved from the
    # Google Places API. This information, specifically the lat/lng values, will be used
    # by the matchmaking algorithm.
    desc "backfill locations of interest for career profiles"
    task :backfill_locations_of_interest => :environment do
        API_KEY = ENV["GOOGLE_API_KEY"]
        client = GooglePlaces::Client.new(API_KEY)
        dry_run = ENV['dry_run']

        locations_of_interest_map = {
            "flexible": "I'm flexible",
            "san_francisco": "San Francisco Bay Area",
            "new_york": "New York, NY",
            "seattle": "Seattle, WA",
            "denver": "Denver, CO",
            "boston": "Boston, MA",
            "los_angeles": "Los Angeles, CA",
            "san_diego": "San Diego, CA",
            "washington_dc": "Washington, DC",
            "chicago": "Chicago, IL",
            "austin": "Austin, TX",
            "dallas": "Dallas, TX",
            "london": "London",
            "raleigh_durham": "Raleigh/Durham, NC",
            "miami": "Miami, FL",
            "houston": "Houston, TX",
            "seoul": "Seoul, South Korea",
            "singapore": "Singapore",
            "dubai": "Dubai, UAE",
            "paris": "Paris, France",
            "berlin": "Berlin, Germany",
            "hong_kong": "Hong Kong",
            "beijing": "Beijing, China",
            "shanghai": "Shanghai, China",
            "tokyo": "Tokyo, Japan"
        }.with_indifferent_access

        # For each entry in the locations_of_interest_map, get the location info from the
        # Google Places API and cherry pick the information that we need.
        locations_of_interest_map.each_pair do |key, location|
            begin
                place_details = {}
                place_id = 'flexible'

                # skip this stuff if the key is 'flexible', since it has no location
                if key != 'flexible'
                    places = client.spots_by_query(location)

                    if places.nil? || places.size == 0
                        puts "#{location} could not be backfilled because no places were found"
                        next
                    end

                    full_place = client.spot(places.first.place_id)

                    if full_place.lat.nil? || full_place.lng.nil?
                        puts "#{location} could not be backfilled because it has no lat/lng"
                        next
                    end

                    if full_place.place_id.nil?
                        puts "#{location} could not be backfilled because it has no place_id"
                        next
                    end
                    place_id = full_place.place_id

                    # Build out place_details object with information we want to cache in our database.
                    # Note: Mirrors logic in location_autocomplete_dir.js
                    full_place.address_components.each do |address_component|
                        place_details[address_component["types"][0]] = {
                            "short": address_component["short_name"],
                            "long": address_component["long_name"]
                        }
                    end
                    place_details["formatted_address"] = full_place.formatted_address
                    place_details["utc_offset"] = full_place.utc_offset
                    place_details["lat"] = full_place.lat
                    place_details["lng"] = full_place.lng
                end

                existing_record = ActiveRecord::Base.connection.execute("
                    SELECT * FROM locations_of_interest WHERE key = '#{key}'
                ").values

                # update the existing record if there is one, otherwise insert a new record
                if existing_record.present?
                    ActiveRecord::Base.connection.execute("
                        UPDATE locations_of_interest SET updated_at = now(), key = '#{key}', place_id = '#{place_id}', place_details = '#{place_details}' WHERE key = '#{key}'
                    ")
                else
                    ActiveRecord::Base.connection.execute("
                        INSERT INTO locations_of_interest (created_at, updated_at, key, place_id, place_details) VALUES (now(), now(), '#{key}', '#{place_id}', '#{place_details.to_json}')
                    ")
                end
                puts "Successfully backfilled #{location}"
            rescue StandardError => e
                puts "#{location} FAILED: #{e.message}"
            end
        end
    end

    # Before https://trello.com/c/O7N94tmP we were not capturing lat/lng so backfill them using the place_id
    desc "backfill lat/lng for existing Google Places"
    task :backfill_lat_lng => :environment do
        API_KEY = ENV["GOOGLE_API_KEY"]
        client = GooglePlaces::Client.new(API_KEY)
        dry_run = ENV['dry_run']
        num_successful_backfills = 0

        klass = (ENV['class'] || "CareerProfile").constantize

        # Where we have an existing place_id and place_details
        records = klass.where("
            place_id IS NOT NULL
            AND place_details::TEXT != '{}'::TEXT
            AND (
                place_details->>'lat' IS NULL
                OR place_details->>'lng' IS NULL
            )
        ")

        puts "Attempting to backfill #{records.size} lat/lng fields for #{klass}s"

        records.each do |record|
            place = client.spot(record.place_id)
            if place.lat.nil? || place.lng.nil?
                puts "WARNING: No lat/lng found for #{klass} belonging to user (#{record.user_id}) with place_id: #{record.place_id}"
                next
            end
            record.place_details["lat"] = place.lat
            record.place_details["lng"] = place.lng

            if !dry_run
                record.save!
            end

            num_successful_backfills += 1
            puts "Added lat/lng to #{klass} for #{record.user.email}"
        end

        puts "Successfully backfilled lat/lng for #{num_successful_backfills} #{klass}s"
    end
end