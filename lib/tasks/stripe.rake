namespace :stripe do

    desc "Create Products and Plans suited for our billable content and access"
    task :ensure_products_and_plans => :environment do

        Cohort::ProgramTypeConfig.configs.to_a
            .select(&:supports_payments?)
            .each do |type_config|
                cohort = Cohort.promoted_cohort(type_config.program_type)
                cohort&.ensure_stripe_products_and_plans
        end

        HiringTeam.ensure_stripe_products_and_plans
    end


    desc "Create Coupons for Subscriptions and Ad-Hoc Use"
    task :ensure_coupons => :environment do

        coupons_info = [
            # Paid Certificates
            {id: "PCAUGVI1", name: "Discount", amount_off: 100000, currency: "usd", duration: "forever"},
            {id: "PC4XTWV6", name: "Discount", amount_off: 90000, currency: "usd", duration: "forever"},
            {id: "PCO8O79U", name: "Discount", amount_off: 80000, currency: "usd", duration: "forever"},
            {id: "PC7K6LHG", name: "Discount", amount_off: 70000, currency: "usd", duration: "forever"},
            {id: "PC6OFCR5", name: "Discount", amount_off: 60000, currency: "usd", duration: "forever"},
            {id: "PCSFDVYY", name: "Discount", amount_off: 50000, currency: "usd", duration: "forever"},
            {id: "PCFUDRH3", name: "Discount", amount_off: 40000, currency: "usd", duration: "forever"},
            {id: "PCMAU4Y3", name: "Discount", amount_off: 30000, currency: "usd", duration: "forever"},
            {id: "PCLDHXDV", name: "Discount", amount_off: 20000, currency: "usd", duration: "forever"},
            {id: "PC75IJEV", name: "Discount", amount_off: 10000, currency: "usd", duration: "forever"},

            # Legacy Plans
            {id: "discount_3500", name: "Scholarship", amount_off: 350000, currency: "usd", duration: "forever"},
            {id: "discount_2950", name: "Scholarship", amount_off: 295000, currency: "usd", duration: "forever"},
            {id: "discount_2400", name: "Scholarship", amount_off: 240000, currency: "usd", duration: "forever"},
            {id: "discount_1900", name: "Scholarship", amount_off: 190000, currency: "usd", duration: "forever"},
            {id: "discount_7200", name: "Scholarship", amount_off: 720000, currency: "usd", duration: "forever"},
            {id: "discount_6100", name: "Scholarship", amount_off: 610000, currency: "usd", duration: "forever"},
            {id: "discount_5100", name: "Scholarship", amount_off: 510000, currency: "usd", duration: "forever"},
            {id: "discount_4100", name: "Scholarship", amount_off: 410000, currency: "usd", duration: "forever"},
            {id: "discount_3100", name: "Scholarship", amount_off: 310000, currency: "usd", duration: "forever"},
            {id: "discount_1600", name: "Scholarship", amount_off: 160000, currency: "usd", duration: "forever"},
            {id: "custom_3300", name: "Scholarship", amount_off: 330000, currency: "usd", duration: "forever"},
            {id: "discount_3550", name: "Scholarship", amount_off: 355000, currency: "usd", duration: "forever"},
            {id: "discount_3000", name: "Scholarship", amount_off: 300000, currency: "usd", duration: "forever"},
            {id: "discount_2500", name: "Scholarship", amount_off: 250000, currency: "usd", duration: "forever"},
            {id: "discount_2000", name: "Scholarship", amount_off: 200000, currency: "usd", duration: "forever"},
            {id: "discount_1400", name: "Scholarship", amount_off: 140000, currency: "usd", duration: "forever"},
            {id: "discount_600", name: "Scholarship", amount_off: 60000, currency: "usd", duration: "forever"},
            {id: "discount_legacy_467", name: "Scholarship", amount_off: 46700, currency: "usd", duration: "forever"},
            {id: "discount_legacy_605", name: "Scholarship", amount_off: 60500, currency: "usd", duration: "forever"},
            {id: "discount_legacy_305", name: "Scholarship", amount_off: 30500, currency: "usd", duration: "forever"},
            {id: "discount_legacy_405", name: "Scholarship", amount_off: 40500, currency: "usd", duration: "forever"},
            {id: "discount_150", name: "Scholarship", amount_off: 15000, currency: "usd", duration: "forever"},
            {id: "discount_250", name: "Scholarship", amount_off: 25000, currency: "usd", duration: "forever"},
            {id: "discount_350", name: "Scholarship", amount_off: 35000, currency: "usd", duration: "forever"},
            {id: "discount_450", name: "Scholarship", amount_off: 45000, currency: "usd", duration: "forever"},
            {id: "discount_550", name: "Scholarship", amount_off: 55000, currency: "usd", duration: "forever"},
            {id: "discount_100_percent", name: "Scholarship", currency: nil, duration: "forever", percent_off:100},

            # New Pricing - Standard Coupon Additions
            {id: "discount_1800", name: "Scholarship", amount_off: 180000, currency: "usd", duration: "forever"},
            # discount_3000 (existing) ^
            {id: "discount_4200", name: "Scholarship", amount_off: 420000, currency: "usd", duration: "forever"},
            {id: "discount_5400", name: "Scholarship", amount_off: 540000, currency: "usd", duration: "forever"},
            {id: "discount_6600", name: "Scholarship", amount_off: 660000, currency: "usd", duration: "forever"},
            # discount_150 -> discount_550 (existing) ^

            # New Pricing - Early Registration Coupon Additions
            {id: "discount_early_1600", name: "Scholarship and Early Registration", amount_off: 160000, currency: "usd", duration: "forever"},
            {id: "discount_early_3100", name: "Scholarship and Early Registration", amount_off: 310000, currency: "usd", duration: "forever"},
            {id: "discount_early_4100", name: "Scholarship and Early Registration", amount_off: 410000, currency: "usd", duration: "forever"},
            {id: "discount_early_5100", name: "Scholarship and Early Registration", amount_off: 510000, currency: "usd", duration: "forever"},
            {id: "discount_early_6100", name: "Scholarship and Early Registration", amount_off: 610000, currency: "usd", duration: "forever"},
            {id: "discount_early_7100", name: "Scholarship and Early Registration", amount_off: 710000, currency: "usd", duration: "forever"},
            {id: "discount_early_75", name: "Scholarship and Early Registration", amount_off: 7500, currency: "usd", duration: "forever"},
            {id: "discount_early_225", name: "Scholarship and Early Registration", amount_off: 22500, currency: "usd", duration: "forever"},
            {id: "discount_early_325", name: "Scholarship and Early Registration", amount_off: 32500, currency: "usd", duration: "forever"},
            {id: "discount_early_425", name: "Scholarship and Early Registration", amount_off: 42500, currency: "usd", duration: "forever"},
            {id: "discount_early_525", name: "Scholarship and Early Registration", amount_off: 52500, currency: "usd", duration: "forever"},
            {id: "discount_early_625", name: "Scholarship and Early Registration", amount_off: 62500, currency: "usd", duration: "forever"},
            {id: "discount_early_100_percent", name: "Scholarship and Early Registration", percent_off: 100, currency: nil, duration: "forever"}

        ]

        coupons_info.each do |coupon_info|

            coupon_id = coupon_info[:id]
            coupon = begin
                Stripe::Coupon.retrieve(coupon_id)
            rescue Stripe::InvalidRequestError => err
                raise err unless err.message.match(/No such coupon/)
            end

            if coupon
                puts "Coupon found for #{coupon_id}: #{coupon.to_h} - Ensuring name consistency"
                if coupon.name != coupon_info[:name]
                    coupon.name = coupon_info[:name]
                    coupon.save
                end
            else
                Stripe::Coupon.create(coupon_info)
            end

        end

    end


    # This is useful if you want to use a test stripe user that is not in your
    # database. Either
    #
    # 1. They were created on staging, but staging has been synced from master so they are gone
    # 2. They were created on staging or another developer's local instance, and you want them in your db
    desc "find a user in stripe and put them in our db"
    task :import_stripe_user => :environment do

        ActiveRecord::Base.transaction do
            customer = Stripe::Customer.retrieve(ENV['id'])
            user = User.new(
                id: customer.id,
                email: customer.email,
                uid: customer.email,
                provider: 'email',
                name: customer.email
            )

            user.password = user.password_confirmation = 'password'
            user.save!
            user.add_to_group 'OPEN COURSES'
            user.add_role :learner

            CohortApplication.create!({
                user_id: user.id,
                cohort_id: Cohort.promoted_cohort('emba')['id'],
                status: 'accepted',
                applied_at: Time.now
            })

            stripe_subscription = customer.subscriptions.list(limit: 1).first
            Subscription.create_or_reconcile_from_stripe_subscription!(user, stripe_subscription)
        end
    end


    desc "clear out subscriptions from our test environment"
    task :clear_subscriptions => :environment do

        if !Rails.env.development?
            raise "This can only be run in a development context!"
        end

        statuses_to_cancel = ['trialing', 'active', 'past_due', 'unpaid']

        statuses_to_cancel.each do |status|

            puts "\nCancelling #{status} subscriptions ...\n"
            while ((subscriptions = Stripe::Subscription.list({limit: 100, status: status})) && subscriptions.data.size > 0)
                subscriptions.each do |subscription|
                    puts "Cancelling #{subscription.id} ..."
                    subscription.delete
                end
            end

        end
    end


    desc "pads a user's subscription billing cycle using trial_end"
    task :pad_subscription => :environment do

        raise "Must provide a customer ID" unless ENV['id']

        customer = Stripe::Customer.retrieve(ENV['id'])
        timezone = ENV['tz'] || 'Pacific Time (US & Canada)'
        trial_end = ActiveSupport::TimeZone[timezone].parse(ENV['trial_end']).to_timestamp

        raise "Parsed trial_end cannot be less than or equal to 0" unless trial_end > 0

        stripe_subscription_id = customer.subscriptions.list(limit: 1).first.id

        stripe_subscription = Stripe::Subscription.update(stripe_subscription_id, {trial_end: trial_end, prorate: false})
        puts "Subscription updated:"

        pp stripe_subscription

    end

    # This task is useful after cloning from prod to local.  At that point
    # the cohort_application and subscription objects will reflect the state
    # of the stripe customer on prod, but the test stripe instance will not
    # reflect the state of the customer.  This task creates subscriptions,
    # invoices, charges, and refunds in stripe so that the test stripe
    # instance will reflect the state described in our database.
    #
    # Make sure that stripe webhooks ARE NOT pointed to your local before
    # running this. You don't want the created invoices, etc. to be
    # recorded in your local db, since they already are.
    desc "Add subscriptions, invoices, etc. to the test environment so that it matches the user's application"
    task :synchronize_stripe_to_application => :environment do
        raise "Only development" unless Rails.env.development?
        query_identifier = ENV['u']
        if query_identifier.looks_like_uuid?
            user = User.find(query_identifier)
        else
            user = User.where(email: query_identifier).first
        end

        ActiveRecord::Base.transaction do

            user.ensure_stripe_customer.subscriptions.each(&:delete)
            user.reset_stripe_customer

            application = user.last_application
            if application.num_charged_payments == 0 && application.num_refunded_payments == 0
                puts "User has made no payments stripe."
                exit
            end

            if user.stripe_customer.default_source.nil?
                user.stripe_customer.sources.create(source: "tok_amex")
            end

            has_subscription = !!user.primary_subscription
            user.primary_subscription&.destroy

            stripe_plan_id = application.stripe_plan_id
            coupon_info = application.applicable_coupon_info
            coupon_id = coupon_info && coupon_info['id']

            puts "creating new subscription and paying first invoice"
            new_subscription = Subscription.create_for_owner(owner: user, stripe_plan_id: stripe_plan_id, coupon_id: coupon_id)
            while (num_invoices = Stripe::Invoice.list(customer: user.stripe_customer.id, subscription: new_subscription.stripe_subscription_id, :expand => ['data.charge'], :limit => 99)
                    .count { |inv| !inv.charge.refunded }) < application.num_charged_payments

                    puts "creating paid invoice #{num_invoices+1} of #{application.num_charged_payments}"
                    Stripe::InvoiceItem.create({
                        customer: user.stripe_customer.id,
                        currency: 'usd',
                        subscription: new_subscription.stripe_subscription_id,
                        amount: application.stripe_plan_amount
                    })
                    invoice = Stripe::Invoice.create({
                        customer: user.stripe_customer.id,
                        subscription: new_subscription.stripe_subscription_id
                    })
                    invoice.pay
            end

            while (num_invoices = Stripe::Invoice.list(customer: user.stripe_customer.id, subscription: new_subscription.stripe_subscription_id, :expand => ['data.charge'], :limit => 99)
                    .count { |inv| inv.charge.refunded }) < application.num_refunded_payments

                    puts "creating refunded invoice #{num_invoices+1} of #{application.num_refunded_payments}"
                    Stripe::InvoiceItem.create({
                        customer: user.stripe_customer.id,
                        currency: 'usd',
                        subscription: new_subscription.stripe_subscription_id,
                        amount: application.stripe_plan_amount
                    })
                    invoice = Stripe::Invoice.create({
                        customer: user.stripe_customer.id,
                        subscription: new_subscription.stripe_subscription_id
                    })
                    invoice.pay
                    Stripe::Refund.create(charge: invoice.charge)
            end

            if !has_subscription
                puts "destroying subscription"
                new_subscription.destroy
            end
        end

    end

    desc "Outputs a list of stripe subscription ids for subscriptions that are in our db but not in stripe"
    task :list_ghost_subscriptions => :environment do
        get_ghost_subscriptions(ENV['user']).each do |subscription|
            puts subscription.stripe_subscription_id
        end
    end

    desc "Deletes any subscription that is in our db but not in stripe"
    task :delete_ghost_subscriptions => :environment do
        get_ghost_subscriptions(ENV['user']).each do |subscription|
            subscription.destroy
            subscription.user.cohort_applications.select do |cohort_application|
                unless (['pre_accepted', 'accepted']).include?(cohort_application.status)
                    cohort_application.registered = false
                    cohort_application.save!
                end
            end.each(&:save!)
        end
    end

    def get_ghost_subscriptions(user_id_or_email)
        stripe_subscription_ids = []
        Stripe::Subscription.find_each do |stripe_subscription|
            stripe_subscription_ids << stripe_subscription.id
        end

        query = Subscription.where.not(stripe_subscription_id: stripe_subscription_ids)

        if user_id_or_email
            query = query.joins(:user).where("users.id = '#{user_id_or_email}' OR users.email = '#{user_id_or_email}'")
        end

        query
    end

    # FIXME: Remove after BW compatible client bumps and this is run. See also: https://trello.com/c/2vfwhnPj
    desc "Removes old pricing keys from existing scholarship_levels"
    task :remove_legacy_scholarship_options => :environment do

        puts "Processing CurriculumTemplates ..."
        CurriculumTemplate.where('scholarship_levels IS NOT NULL').each do |schedulable_item|
            schedulable_item.scholarship_levels.each do |level|
                remove_old_level_options(level)
            end
            schedulable_item.save!
        end

        puts "Processing Cohorts ..."
        Cohort.where('scholarship_levels IS NOT NULL').edit_and_republish do |schedulable_item|
            schedulable_item.scholarship_levels.each do |level|
                remove_old_level_options(level)
            end
        end

        puts "Processing CohortApplications ..."
        CohortApplication.skip_callback(:commit, :after, :identify_user)
        CohortApplication.skip_callback(:update, :after, :update_in_airtable)
        total_size = CohortApplication.where('scholarship_levels IS NOT NULL').count
        batch = 0
        CohortApplication.where('scholarship_levels IS NOT NULL').find_in_batches(batch_size: 250).each do |applications|
            puts "Processing CohortApplication #{(batch * 250) + 1} of #{total_size} ..."
            applications.each do |application|
                if application.scholarship_level
                    remove_old_level_options(application.scholarship_level)
                end
                application.scholarship_levels.each do |level|
                    remove_old_level_options(level)
                end
                application.save!(validate: false)
            end
            batch += 1
        end
    end
    def remove_old_level_options(scholarship_level)
        scholarship_level.except!('coupons')
    end

    desc "Create a billing transaction for each charge in stripe"
    task :backfill_billing_transactions => :environment do

        i = 0
        Stripe::Charge.find_each do |stripe_charge|
            i += 1
            BillingTransaction.find_or_create_by_stripe_charge(stripe_charge)
            if i % 100 == 0
                puts "#{i} charges processed."
            end
        end
        puts "#{i} charges processed."

    end

    desc "Create a refund record for each refund in stripe"
    task :backfill_refunds => :environment do

        i = 0
        errors = 0
        updates = {}
        Stripe::Refund.find_each do |stripe_refund|
            i += 1
            refund = Refund.find_or_create_by_stripe_refund(stripe_refund)

            if refund.nil?
                errors += 1
            else
                updates[refund.billing_transaction_id] ||= {
                    old: refund.billing_transaction.saved_change_to_amount_refunded? ? refund.billing_transaction.saved_changes['amount_refunded'][0] : refund.billing_transaction.amount_refunded
                }
                updates[refund.billing_transaction_id][:new] = refund.billing_transaction.amount_refunded
            end

            if i % 100 == 0
                puts "#{i} refunds processed."
            end
        end

        updates.each do |billing_transaction_id, entry|
            if entry[:new] != entry[:old]
                billing_transaction = BillingTransaction.find(billing_transaction_id)
                puts " * amount_refunded for #{billing_transaction.provider_transaction_id} changed from #{entry[:old]} to #{entry[:new]} (#{billing_transaction.refunds.count} refunds"
            end
        end

        puts ""

        puts "#{i} refunds processed. #{errors} could not be saved because no billing transaction was found."

    end

    # NOTE: Since we have accepted several out of product payments, this should not be used, but is useful
    # for historical purposes, or useful after a certain date, assuming we steer ACH invoicing through Stripe

    # desc "Backfill num_*_payments for billable EMBA applications"
    # task :backfill_num_payments => :environment do

    #     query = User.joins(:cohort_applications).includes(:cohort_applications)
    #         .where("cohort_applications.scholarship_level IS NOT NULL")
    #         .where("cohort_applications.num_charged_payments IS NULL") # try to reduce
    #         .where("cohort_applications.status in ('pre_accepted', 'deferred', 'accepted', 'expelled')") # valid statuses
    #         # .where(:id => '3c784ea8-bafc-4803-9ff4-4a9210121ba3') # reduce to ID for Stripe Test env, as it will error on non-existent, etc

    #     total_count = query.size

    #     count = 0
    #     query.find_in_batches(batch_size: 100) do |users|

    #         users.each do |user|
    #             count += 1

    #             puts "Processing user #{user.email} ..."

    #             application = user.last_application

    #             next if !application.scholarship_level || !application.num_charged_payments.nil?

    #             begin
    #                 # currently users are only on 1 plan type -- this will change in the future, but for now
    #                 # the complexity of get_payment_details mapping to individual plans is unnecessary for the initial backfill
    #                 num_charged_payments, num_refunded_payments = user.get_payment_details
    #             rescue Stripe::InvalidRequestError => err
    #                 if err.message.match(/No such customer/)
    #                     puts "\tNo such customer!"
    #                     next
    #                 end
    #                 raise err
    #             end

    #             # backfill values
    #             application.num_charged_payments = num_charged_payments
    #             application.num_refunded_payments = num_refunded_payments

    #             # account for the fact that we bumped these users required payments initially to compensate for potential under-billing
    #             application.total_num_required_stripe_payments -= num_refunded_payments

    #             application.save! if application.changed?

    #         end
    #         puts "Backfilled #{count} of #{total_count} users"
    #     end

    # end

end