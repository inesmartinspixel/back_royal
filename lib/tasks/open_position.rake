namespace :open_position do

    desc "backfill short_description column using the contents of the 'full' description"
    task :backfill_short_description => :environment do
        query = OpenPosition.where.not(description: nil)
        total_count = query.count
        current_count = 0
        puts "Backfilling short_description for #{total_count} positions"
        query.find_each(batch_size: 100) do |open_position|
            # for open positions that have a description that's less than or equal to 200 characters
            # in length, we can simply set the short_description attribute to the description and
            # then clear the description
            if open_position.description.length <= 200
                open_position.short_description = open_position.description
                open_position.description = nil
            # otherwise, we grab the first 197 characters and add an ellipsis to the end of it
            else
                open_position.short_description = open_position.description[0..196] + '...'
            end
            open_position.save!
            current_count += 1
            if current_count % 100 == 0
                puts "Backfilled #{current_count}/#{total_count}"
            end
        end
        puts "Successfully backfilled short_description for #{current_count}/#{total_count}"
    end

    # FIXME: Delete after running on prod
    task :backfill_position_descriptors => :environment do

        query = OpenPosition.where(position_descriptors: '{}')
        total_count = query.count
        current_count = 0
        puts "Backfilling position_descriptors for #{total_count} positions"
        query.find_each(batch_size: 100) do |open_position|

            # I inspected open_positions on prod without descriptors. I did not
            # find any that jumped out as internships (title ilike '%intern%'),
            # nor did I find any that seemed like they were contractual positions
            # (title ilike '%contract%'). However, I did find a number of part
            # time positions. They all seem to have "Part Time" in the title; not
            # "part-time" or "part_time".
            if open_position.title.match(/Part Time/)
                open_position.update_column(:position_descriptors, ['part_time'])
            else
                open_position.update_column(:position_descriptors, ['permanent'])
            end

            current_count += 1
            if current_count % 100 == 0
                puts "Backfilled #{current_count}/#{total_count}"
            end
        end
        puts "Successfully backfilled position_descriptors for #{current_count}/#{total_count} positions"

    end
end