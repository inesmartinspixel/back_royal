namespace :redshift do

    task :setup_dblink => :environment do
        server_name = ENV['server']
        usernames = ENV['usernames'] ? ENV['usernames'].split(',') : nil
        DbLinker.setup_dblink(server_name, usernames)
    end

    task :setup_red_royal_dblink => :environment do
        DbLinker.setup_red_royal_dblink
    end

    task :drop_dblink => :environment do
        server_name = ENV['server']
        DbLinker.drop_dblink(server_name)
    end

    task :copy_events_to_dev => :environment do
        where_clause = ENV['w']
        RedshiftEvent.establish_connection(REDSHIFT_PROD_READONLY_DB_CONFIG)

        s3 = Aws::S3::Resource.new
        bucket = s3.bucket(ENV['REDSHIFT_DUMP_BUCKET'])
        raise "No bucket for #{ENV['REDSHIFT_DUMP_BUCKET'].inspect}" unless bucket

        dirname = 'dev_copy'
        s3_directory = "s3://#{bucket.name}/#{dirname}"
        bucket.objects(prefix: dirname).each do |object|
            object.delete
        end

        s3_prefix = "#{s3_directory}/#{dirname}/copy_"

        cmd = %Q%
            UNLOAD  ($$select * from events where #{where_clause}$$)
                TO '#{s3_prefix}'
                IAM_ROLE '#{ENV['AWS_ROLE_ARN_REDSHIFT']}'
                manifest
                ESCAPE
                delimiter '|';
        %
        puts cmd
        RedshiftEvent.connection.execute(cmd)
        puts "DUMPED TO S3!!!!!!!!!!!!!!!!!!!!!!"

        cmd = %Q%
            COPY events
                from '#{s3_prefix}manifest'
                IAM_ROLE '#{ENV['AWS_ROLE_ARN_REDSHIFT']}'
                manifest
                ESCAPE
                delimiter '|';
        %
        puts cmd
        RedshiftEvent.establish_connection(REDSHIFT_DB_CONFIG)
        RedshiftEvent.connection.execute(cmd)
        puts "COPIED TO DEV REDSHIFT!!!!!!!!!!!!!!!!!!!!!!"

    end

    # eg - rake redshift:update_column column="action" column_type="character varying(128)" compression="LZO"
    desc "updates column definitions, skipping the dance around the inability to rename or alter compression. NOTE: this does not work on sortkey columns"
    task :update_column => :environment do

        start_time = Time.now
        column = ENV['column']
        column_type = ENV['column_type']
        compression = ENV['compression']

        if column.blank? || column_type.blank? || compression.blank?
            raise "Must provide column, column_type, and compression values."
        end

        RedshiftEvent.establish_connection(REDSHIFT_DB_CONFIG) # just to be sure!

        # NOTE: We are seeing a transaction elongate this process by orders of magnitude, to the point
        # in which it may make sense to remove the transaction completely, once temporarily removing
        # Redshift related jobs from our queue system, then re-enabling once complete.
        commands = [
            "BEGIN",
            "ALTER TABLE events ADD COLUMN #{column}_tmp #{column_type} ENCODE #{compression}",
            "UPDATE events SET #{column}_tmp = #{column}",
            "ALTER TABLE events DROP COLUMN #{column}",
            "ALTER TABLE events RENAME COLUMN #{column}_tmp to #{column}",
            "COMMIT"
        ]

        commands.each do |command|
            puts "#{Time.now}: #{command}"
            RedshiftEvent.connection.execute(command)
        end

        elapsed = (Time.now - start_time).ceil
        puts "Completed column update in #{elapsed} seconds"
    end

    task :vacuum_and_analyze => :environment do
        if ENV['SKIP_REDSHIFT_VACUUM'] != 'true'
            Redshift.vacuum_and_analyze
        end
    end

end
