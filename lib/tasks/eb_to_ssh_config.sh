#!/bin/bash

# failsafe
read -s -d Y -p "This will rewrite a portions of your SSH configuration. Continue?
Press 'Y' to continue, or '^C' to abort."
echo -e "\nUpdating ..."

# determine if current config already has generated content, prune if so
config_path=~/.ssh/config
generated_content_string="#----------- END GENERATED CONTENT -----------#"
has_generated_content=$(grep "${generated_content_string}" $config_path)
if [ -n "$has_generated_content" ]; then
    sed -i '' "1,/$generated_content_string/d" $config_path
fi

# global header directives
read -d '' config_content << EOF
#----------- BEGIN GENERATED CONTENT -----------#

Host *
UseKeychain yes
AddKeysToAgent yes
EOF

# iterate through elastic beanstalk environments
environments=(smartly-staging smartly-production)
for environment in "${environments[@]}"; do
    config_content="${config_content}\n\n# ${environment} instances"
    i=0
    # grabs a list of instance IPs, converted into array for easy iteration
    ips=($(aws ec2 describe-instances --filters "Name=tag:elasticbeanstalk:environment-name,Values=${environment}" --query 'Reservations[].Instances[].PublicIpAddress' --output text | tr "\t" " "))
    for ip in "${ips[@]}"; do
        ((i++))
        # numbered / named host entry for each instance
        read -d '' host_entry << EOF
Host ${environment}-${i}
Hostname ${ip}
User ec2-user
IdentityFile ~/.ssh/${environment}.pem
ProxyCommand ssh -A smartly-bastion -W %h:%p
EOF
    config_content="${config_content}\n\n${host_entry}"
    done
done

# generated content dilimeter
config_content="${config_content}\n\n${generated_content_string}"

# rewrite config file with updated entries
echo -e "${config_content}" | cat - $config_path > $config_path.TEMP && mv $config_path.TEMP $config_path