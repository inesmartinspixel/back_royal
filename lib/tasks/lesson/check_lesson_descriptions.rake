namespace :lesson do
    
    desc "go through lesson descriptions, convert markdown to array, print list of descriptions without markdown lists"
    task :convert_description_markdown => :environment do

        improperly_formatted = ''

        begin

            VersionMigrator.new(Lesson).select('description').each do |lesson|
                # lesson.description after initial conversion is an array containing a single value: 
                # the old description; here we split out the markdown array elements into an actual array
                if lesson.description.length == 1
                    if lesson.description[0] =~ /- /
                        description_parts = lesson.description[0].split('- ')
                        lesson.description = []
                        description_parts.each do |part|
                            part = part.gsub(/\n/, "")
                            if !part.blank?
                                lesson.description << part
                            end
                        end
                    elsif lesson.description[0].blank?
                        # clean up nils
                        lesson.description = []
                    elsif lesson.description[0] =~ /-/
                        # dont output warnings for old versions
                        if lesson.class.name == "Lesson"
                            # this lesson description didnt have a list of items in it. alert the editors!
                            improperly_formatted << "https://smart.ly/editor/lesson/#{lesson.id}/edit -- #{lesson.published_version_id.blank? ? 'U' : 'P'} -- \"#{lesson.description[0]}\"\n\n"
                        end
                    end
                end
            end
        
        ensure

            if !improperly_formatted.blank?
                puts "The following lessons have descriptions that are not formatted as a list: \n\n#{improperly_formatted}"
            end

        end

    end

end # namespace