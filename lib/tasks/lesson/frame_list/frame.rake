require 'fileutils'

namespace :lesson do

    namespace :frame_list do
        namespace :frame do

          desc "print urls for all published frames"
          task :print_published_urls => :environment do
            Lesson::Stream.all_published.each do |stream|
              stream.published_version.lessons.each do |lesson|
                next unless lesson.published_version
                lesson.published_version.content.frames.each do |frame|
                  puts frame.player_link(stream, "http://localhost:3000")
                end
              end
            end
          end

          desc "Generate some probabilities for encountering frametypes together in a lesson"
          task :frame_type_correlations => :environment do
            frametype_correlations_location = Rails.root.join('tmp', 'frametype_correlations.csv')

            frametype_correlations = Hash.new { Hash.new(0) }
            num_lessons = 0

            Lesson.all_published_and_in_a_stream.each do |lesson|
              lesson_length = lesson.content.frames.length.to_f
              num_lessons += 1
              frametypes_in_this_lesson = Hash.new

              puts "\n\n======================="
              puts "Lesson: #{lesson.title}"

              ## Iterate over frames in the lesson
              lesson.content.frames.each_with_index do |frame, index|
                ## Figure out frametype
                components = frame.components.flatten.select
                challenges_component = components.select { |c| c.is_a?(Lesson::Content::FrameList::Frame::Componentized::Component::Challenges) }.first
                frame_type = challenges_component && challenges_component.editor_template || 'no_interaction'

                # Note that we've seen this frametype in this lesson
                frametypes_in_this_lesson[frame_type] = index

                puts "Frame #{index+1} of #{lesson_length.to_i} (#{frame_type})"
              end

              # Increment counts of co-existing frametypes
              frametypes_in_this_lesson.keys.each do |frame_type_in_lesson|
                hash = frametype_correlations[frame_type_in_lesson]
                frametypes_in_this_lesson.keys.each do |other_frame_type|
                  #next if frame_type_in_lesson == other_frame_type
                  hash[other_frame_type] += 1
                end
                frametype_correlations[frame_type_in_lesson] = hash
              end
            end

            ## Write bucketed totals
            csv = CSV.open(frametype_correlations_location, 'w+') do |csv|
              columns = [
                'frame',
                frametype_correlations.keys,
                'total'
              ].flatten
              csv << columns

              frametype_correlations.each do |type, index_hash|
                csv << columns.map do |col|
                  if col == 'frame'
                    type
                  elsif col == 'total'
                    index_hash[type]
                  else
                    index_hash[col]
                  end
                end
              end
            end
            puts "csv written to #{frametype_correlations_location}"
          end

          desc "Generate counts for frametypes and their indices bucketed"
          task :frame_type_bucketed_indices => :environment do
            frametype_bucketed_location = Rails.root.join('tmp', 'frametype_bucketed.csv')

            num_buckets = ENV['NUM_BUCKETS'] || 5
            bucket_indices = Array(1...num_buckets+1).map{|i| "Bucket #{i}"}
            frametype_bucketed = Hash.new { Hash.new(0.to_f) }

            Lesson.all_published_and_in_a_stream.each do |lesson|
              lesson_length = lesson.content.frames.length.to_f

              puts "\n\n======================="
              puts "Lesson: #{lesson.title}"

              ## Iterate over frames in the lesson
              lesson.content.frames.each_with_index do |frame, index|
                ## Figure out frametype
                components = frame.components.flatten.select
                challenges_component = components.select { |c| c.is_a?(Lesson::Content::FrameList::Frame::Componentized::Component::Challenges) }.first
                frame_type = challenges_component && challenges_component.editor_template || 'no_interaction'

                ## Increment has of frametypes to bucketed lesson indices
                # Convert to base of number of buckets
                fractional_lesson = (index+1).to_f / lesson_length
                fractional_bucket = (num_buckets * (index + 1)).to_f / lesson_length
                lower_bucket = fractional_bucket.floor
                upper_bucket = fractional_bucket.ceil

                # Allocate the frametype across the upper and lower buckets
                upper_bucket_amt = [fractional_lesson - (lower_bucket.to_f / num_buckets.to_f), (1.0/lesson_length)].min
                lower_bucket_amt = (1.0 / lesson_length) - upper_bucket_amt

                # Assign to bucketed hash
                hash = frametype_bucketed[frame_type]
                hash["Bucket #{lower_bucket.to_i}"] += lower_bucket_amt
                hash["Bucket #{upper_bucket.to_i}"] += upper_bucket_amt
                frametype_bucketed[frame_type] = hash

                puts "Frame #{index+1} of #{lesson_length.to_i} (#{frame_type}): #{lower_bucket}|#{upper_bucket}: #{lower_bucket_amt} <-> #{upper_bucket_amt} :: #{(lower_bucket_amt + upper_bucket_amt).rationalize}"
              end
            end

            ## Write bucketed totals
            csv = CSV.open(frametype_bucketed_location, 'w+') do |csv|
              columns = [
                'frame',
                bucket_indices
              ].flatten
              csv << columns

              frametype_bucketed.each do |type, index_hash|
                csv << columns.map do |col|
                  if col == 'frame'
                    type
                  else
                    index_hash[col]
                  end
                end
              end
            end
            puts "csv written to #{frametype_bucketed_location}"
          end

          desc "Generate counts for frametypes and their indices"
          task :frame_type_indices => :environment do
            frametype_order_location = Rails.root.join('tmp', 'frametype_order.csv')

            frametype_order = Hash.new { Hash.new(0) }
            frame_indices = Array(1...30).map{|i| "Frame #{i}"}

            Lesson.all_published_and_in_a_stream.each do |lesson|

              ## Iterate over frames in the lesson
              lesson.content.frames.each_with_index do |frame, index|
                ## Figure out frametype
                components = frame.components.flatten.select
                challenges_component = components.select { |c| c.is_a?(Lesson::Content::FrameList::Frame::Componentized::Component::Challenges) }.first
                frame_type = challenges_component && challenges_component.editor_template || 'no_interaction'

                ## Increment hash of frametypes to lesson indices
                hash = frametype_order[frame_type]
                hash["Frame #{index+1}"] += 1
                frametype_order[frame_type] = hash
              end
            end

            ## Write frametype totals
            csv = CSV.open(frametype_order_location, 'w+') do |csv|
              columns = [
                'frame',
                frame_indices
              ].flatten
              csv << columns

              frametype_order.each do |type, index_hash|
                csv << columns.map do |col|
                  if col == 'frame'
                    type
                  else
                    index_hash[col]
                  end
                end
              end
            end
            puts "csv written to #{frametype_order_location}"
          end

          desc "Generate outline of every lesson"
          task :lesson_outline => :environment do
            outline_location =  Rails.root.join('tmp', 'lesson_outline.csv')

            lesson_outline = []

            Lesson.all_published_and_in_a_stream.each do |lesson|
              frames = []

              ## Iterate over frames in the lesson
              lesson.content.frames.each_with_index do |frame, index|
                ## Figure out frametype
                components = frame.components.flatten.select
                challenges_component = components.select { |c| c.is_a?(Lesson::Content::FrameList::Frame::Componentized::Component::Challenges) }.first
                frame_type = challenges_component && challenges_component.editor_template || 'no_interaction'

                ## Store frametypes in order for this lesson
                frames << frame_type
              end

              stream_titles = lesson.streams.map(&:title).join(', ')
              lesson_outline << [stream_titles, lesson.title, frames.length].flatten
            end

            ## Write Lesson outline
            csv = CSV.open(outline_location, 'w+') do |csv|
              columns = [
                'stream_title',
                'lesson_title',
                'frames'
              ].flatten
              csv << columns.map(&:titleize)

              lesson_outline.each do |lesson|
                csv << lesson
              end
            end
            puts "csv written to #{outline_location}"
          end


          desc "Generate a list of all published lessons and their grades"
          task :grade_lessons => :environment do

            location =  Rails.root.join('tmp', 'graded_lessons.csv')

            csv = CSV.open(location, 'w+') do |csv|
              columns = [
                'grade',
                'lesson_title',
                'stream_title',
                'lesson_guid',
                'auto-failures',
                'reasons',
                'notes'
              ]
              csv << columns.map(&:titleize)

              Lesson.all_published_and_in_a_stream.each do |lesson|
                grades = ['A+','A', 'A-', 'B', 'B-', 'C', 'C-', 'D', 'D-', 'F', 'F-']
                auto_fails = [] # these automatically result in an F-
                reasons = [] # each reason lowers your score by one step
                notes = [] # notes don't affect score, but will be shown to the author

                ## Bookkeeping for the rules
                frametype_counts = Hash.new(0)
                frametypes_in_a_row = 0
                last_frametype = ''
                bolded_words_count = Hash.new(0)
                bolded_words_frames = Hash.new { [] }
                interactive_frames_without_messages = []
                interactive_frames_that_could_have_messages = []
                image_count = 0
                frames_with_colored_terms = {}

                ## Iterate over frames in the lesson
                lesson.content.frames.each_with_index do |frame, index|
                  # Capture useful frame-specific details
                  components = frame.components.flatten.select
                  challenges_component = components.select { |c| c.is_a?(Lesson::Content::FrameList::Frame::Componentized::Component::Challenges) }.first
                  bolded_terms = frame.text_content.scan(/\*\*([^\*]+)\*/).to_a.flatten
                  frame_type = challenges_component && challenges_component.editor_template || 'no_interaction'
                  messages = components.select { |c| c.is_a?(Lesson::Content::FrameList::Frame::Componentized::Component::MultipleChoiceMessage) }.map(&:message_text).map(&:text)
                  frame_navigator = components.select { |c| c.is_a?(Lesson::Content::FrameList::Frame::Componentized::Component::FrameNavigator) }.first
                  branching = frame_navigator.has_branching == true
                  next_frame_overridden = frame_navigator.next_frame_guid # FIXME: not working yet for some reason?
                  frame_texts = components.select { |c| c.is_a?(Lesson::Content::FrameList::Frame::Componentized::Component::Text) }.map(&:text)
                  expected_text_for_compose = components.select { |c| c.is_a?(Lesson::Content::FrameList::Frame::Componentized::Component::AnswerMatcher::MatchesExpectedText) }.map(&:expected_text)
                  images = components.select { |c| c.is_a?(Lesson::Content::FrameList::Frame::Componentized::Component::Image) }

                  ## Update bookkeeping
                  # Number of slides with at least one image
                  if images.length > 0
                    image_count += 1
                  end

                  # Bolded terms
                  bolded_terms.each do |word|
                    bolded_words_count[word] += 1
                    bolded_words_frames[word] += [index+1]
                  end

                  # Frametype counts
                  frametype_counts[frame_type] += 1

                  # Frametypes in a row
                  # Hack: if this is a branching step OR next screen isn't default, reset the frame_types_in_a_row
                  if !branching && !next_frame_overridden.nil? && next_frame_overridden.length > 0
                    frametypes_in_a_row = 0
                    last_frametype = ''
                  elsif frame_type == last_frametype
                    frametypes_in_a_row += 1
                  else
                    frametypes_in_a_row = 1
                    last_frametype = frame_type
                  end

                  ## Begin Applying Rules

                  # If lesson does not begin with a no interaction, note it to author.
                  if index == 0 && frame_type != 'no_interaction'
                    notes << "Does not begin with no interaction (begins with #{frame_type})"
                  end

                  # Use the same frametype > 2 times in a row => lower grade for each instance.
                  if frametypes_in_a_row > 2
                    reasons << "#{frame_type} used #{frametypes_in_a_row} times in a row; avoid using a frametype more than twice in a row."
                    frametypes_in_a_row = 0 # reset so we keep counting for the next instance
                    last_frametype = ''
                  end

                  # Duplicate answer messages on an interaction screen => lower grade for each screen.
                  if messages.length > messages.uniq.length
                    reasons << "Duplicate answer messages for frame #{index+1}."
                  end

                  # Track how many frames that *could* have messages don't
                  if frame_type != 'no_interaction' && frame_type != 'compose_blanks' && frame_type != 'compose_blanks_on_image' && frame_type != 'multiple_choice_poll'
                    interactive_frames_that_could_have_messages << index
                    if messages.length <= 0
                      interactive_frames_without_messages << index
                    end
                  end

                  # Too long of text to type >20 characters
                  expected_text_for_compose.each do |text|
                    if text.length > 20
                      reasons << "Frame #{index+1} requires text to type that is longer than 20 characters: '#{text}'"
                    end
                  end

                  ## Frame-text specific Rules
                  colored_terms = []

                  frame_texts.each do |text|
                    # Filter out Mathjax to avoid any confusion - replace with "MATH" to avoid awkward extra spaces or other wierdness.
                    text_without_mathjax = text.gsub(/(%%.*?%%|\$\$.*?\$\$)/m, 'MATH')

                    # Punctuation in markdown bold and italics (. , ! ?)
                    # Don't count cases where an entire sentence is marked up.
                    punctuation_errors = text_without_mathjax.scan(/\b +(\*+?[^\*]+[.,!?]+\*+)/)
                    if punctuation_errors.length > 0
                      reasons << "Frame #{index+1} has punctuation inside of markdown: \"#{punctuation_errors.join('"')}\""
                    end

                    # Track colored text
                    color = text_without_mathjax.scan(/({(purple|plum|yellow|blue|green|pink|grey|orange|red|white|coral|turquoise|darkturquoise|darkpurple|darkyellow|darkblue|darkgreen|darkcoral|darkorange|darkred)\:)(.*?)(\})/)
                    if color.length > 0
                      (colored_terms << color).flatten!
                    end

                    # Question mark and exclamation points together !? or multiple
                    question_marks = text_without_mathjax.scan(/[^!?]{0,10}[!?]{2,}/)
                    if question_marks.length > 0
                      summary = question_marks.map {|s| "...#{s}"}.join(', ')
                      reasons << "Frame #{index+1} has too many question marks and/or exclamation points in a row: #{summary}"
                    end

                    # Using a regular space-hyphen-space instead of an emdash outside of Mathjax
                    bad_hyphens = text_without_mathjax.scan(/[^ ]{0,5} - [^ ]{0,5}/)
                    if bad_hyphens.length > 0
                      summary = bad_hyphens.map {|s| "...#{s}..."}.join(', ')
                      reasons << "Frame #{index+1} uses a hyphen (-) instead of an emdash (—): #{summary}"
                    end

                    # More than one space in a row outside of mathjax followed by a non-space non-return character.
                    too_many_spaces = text_without_mathjax.scan(/[^ ]{0,5} {2,}[^ \n]{1,5}/)
                    if too_many_spaces.length > 0
                      summary = too_many_spaces.map {|s| "...#{s}..."}.join(', ')
                      notes << "Frame #{index+1} uses more than one space in a row: #{summary}"
                    end
                  end

                  # Track which frames have colored text
                  if colored_terms.length > 0
                    frames_with_colored_terms[index+1] = colored_terms
                  end

                  # Debugging
                  puts "#{lesson.title} (#{index+1}):, #{frame_type}, B:#{branching}, NF:#{next_frame_overridden}, [#{bolded_terms.join(',')}]"

                end

                ## Lesson-wide rules
                # > 2 This or Thats || >2 polls || > 2 matching
                if frametype_counts['this_or_that'] > 2
                  reasons << "More than two This or That screens (#{frametype_counts['this_or_that']})"
                end
                if frametype_counts['multiple_choice_poll'] > 2
                  reasons << "More than two Polling screens (#{frametype_counts['multiple_choice_poll']})"
                end
                if frametype_counts['matching'] > 2
                  reasons << "More than two Matching screens (#{frametype_counts['matching']})"
                end

                # No fill in the blank || no multiple choice => lower grade
                if frametype_counts['fill_in_the_blanks'] == 0 && frametype_counts['basic_multiple_choice'] == 0
                  reasons << "No multiple choice or fill in the blanks in this lesson."
                end

                # > 50% no interaction => lower grade
                if frametype_counts['no_interaction'].to_f / lesson.content.frames.length.to_f > 0.5
                  reasons << "More than 50% no interaction screens."
                end

                # > 80% no interaction => even lower grade.
                if frametype_counts['no_interaction'].to_f / lesson.content.frames.length.to_f > 0.8
                  reasons << "More than 80% no interaction screens."
                end

                # If >80% of interactive slides are either multiple choice or fill in the blank => lower grade.
                if (frametype_counts['fill_in_the_blanks'] + frametype_counts['basic_multiple_choice']).to_f / (lesson.content.frames.length - frametype_counts['no_interaction']).to_f > 0.8
                  reasons << "More than 80% of interactive screens are multiple choice or fill in the blanks."
                end

                # If the exact same phrase has been bolded more than once in the same lesson => lower grade for each instance.
                double_bolds = bolded_words_count.find_all {|k, v| v > 1 }
                double_bolds.each do |db|
                  word = db[0]
                  summary = bolded_words_frames[word].uniq.join(', ')
                  reasons << "'#{word}' is bolded more than once in the lesson: frames #{summary}"
                end

                # If >70% of interactive slides have no messages => lower score
                if interactive_frames_without_messages.length.to_f / interactive_frames_that_could_have_messages.length.to_f > 0.7
                  reasons << "Greater than 70% of interactive frames that could have messages don't (Frames #{interactive_frames_without_messages.join(', ')})"
                end

                # Overuse of compose modes
                if (frametype_counts['compose_blanks'] + frametype_counts['compose_blanks_on_image']).to_f / lesson.content.frames.length.to_f > 0.4
                  reasons << "Too many typing screens for this lesson (greater than 40%)"
                end

                # Too few images <2
                if image_count < 2
                  reasons << "Too few slides with images in this lesson (less than 2)"
                end

                # Overuse of colored terms (>30% of screens have colored terms)
                if frames_with_colored_terms.keys.length.to_f / lesson.content.frames.length.to_f > 0.3
                  reasons << "Overuse of colored text in this lesson (greater than 30% of frames)"
                end

                # If lesson length greater than 20
                if lesson.content.frames.length > 20
                  reasons << "Lesson should be less than or equal to 20 slides long (#{lesson.content.frames.length})"
                end

                ## Automatic F- rules
                # Missing description -> auto F-
                if lesson.description.nil? || lesson.description.length == 0
                  auto_fails << "Lesson must have a description."
                end

                # Length of lesson outside the bounds -> auto F-
                if lesson.content.frames.length < 8 || lesson.content.frames.length > 22
                  auto_fails << "Lesson length is non-optimal; must be from 8 to 22 screens long (#{lesson.content.frames.length})"
                end

                # Missing tag -> auto F-
                if lesson.content['tag'].nil? || lesson.content['tag'].length == 0
                  auto_fails << "Lesson must have a tag."
                end

                ## Compute the grade based on above metrics
                # deduct a half grade point for every reason we find
                grade = grades[[grades.length - 1, reasons.length].min]
                if auto_fails.length > 0
                  grade = 'F-'
                end


                csv << columns.map do |col|
                  case col
                  when 'grade'
                    grade
                  when 'stream_title'
                    lesson.streams.map(&:title).join(', ')
                  when 'lesson_guid'
                    lesson.lesson_guid
                  when 'lesson_title'
                    lesson.title
                  when 'auto-failures'
                    auto_fails.join(', ')
                  when 'reasons'
                    reasons.join(', ')
                  when 'notes'
                    notes.join(', ')
                  end
                end
              end
            end

            puts "csv written to #{location}"

          end

          desc "Generate a list of modal text in published content"
          task :list_modals => :environment do

            location =  Rails.root.join('tmp', 'modals.csv')

            csv = CSV.open(location, 'w+') do |csv|
              columns = [
                ['modal_text'],
                ['lesson_guid', :lesson_id],
                ['lesson_title', :lesson_title],
                ['frame_index', Proc.new { |frame| frame.index + 1} ],
                ['text', :text_content]
              ]
              csv << columns.map(&:first).map(&:titleize)
              Lesson.all_published_and_in_a_stream.each do |lesson|
                lesson.content.frames.each_with_index do |frame, index|

                  if frame.text_content.scan(/\[\[([^\]]+)\]\]/).to_a.flatten.uniq.each do |modal_text|
                    puts frame.main_ui_component.component_type
                      csv << columns.map do |col|
                        meth = col[1]
                        if (col[0] == 'modal_text')
                          modal_text
                        elsif meth.is_a?(Symbol)
                          frame.send(meth)
                        else
                          meth.call(frame)
                        end
                      end
                    end
                  end
                end

              end
            end

            puts "csv written to #{location}"

          end

          desc "Generate a list of all bolded words in published content"
          task :list_bolded_words => :environment do

            location =  Rails.root.join('tmp', 'bolded_words.csv')

            csv = CSV.open(location, 'w+') do |csv|
              columns = [
                ['term'],
                ['lesson_guid', :lesson_guid],
                ['lesson_title', :lesson_title],
                ['frame_guid', :frame_guid],
                ['frame_index', Proc.new { |frame| frame.index + 1} ],
                ['text', :text_content]
              ]
              csv << columns.map(&:first).map(&:titleize)
              Lesson.all_published_and_in_a_stream.each do |lesson|
                lesson.content.frames.each_with_index do |frame, index|

                  if frame.text_content.scan(/\*\*([^\*]+)\*/).to_a.flatten.uniq.each do |term|
                    puts frame.main_ui_component.component_type
                      csv << columns.map do |col|
                        meth = col[1]
                        if (col[0] == 'term')
                          term
                        elsif meth.is_a?(Symbol)
                          frame.send(meth)
                        else
                          meth.call(frame)
                        end
                      end
                    end
                  end
                end

              end
            end

            puts "csv written to #{location}"

          end

          desc "Find blanks that have different content from the associated correct answer"
          task :find_unlinked_blanks => :environment do
            frames = Lesson.all.map(&:content).map(&:frames).flatten.select { |f| f.is_a?(Lesson::Content::FrameList::Frame::Componentized) }
            components = frames.map(&:components).flatten.select
            challenges_components = components.select { |c| c.is_a?(Lesson::Content::FrameList::Frame::Componentized::Component::Challenges) }

            linked = []
            unlinked = []

            challenges_components.each do |challenges_component|
              if ['blanks_on_image', 'compose_blanks_on_image', 'fill_in_the_blanks', 'compose_blanks'].include?(challenges_component.editor_template)

                blank_labels = challenges_component.blank_labels
                challenges = challenges_component.challenges

                blank_labels.each_with_index do |blank_label, i|
                  challenge = challenges[i]
                  if blank_label == challenge.correct_answer_text || (challenge.correct_answer_image && blank_label == challenge.correct_answer_image.label)
                    linked << {challenge: challenge, blank_label: blank_label}
                  else
                    unlinked << {challenge: challenge, blank_label: blank_label}
                  end
                end
              end

            end

            puts "#{linked.size} linked blanks"
            puts "#{unlinked.size} unlinked blanks"
            unlinked.each do |entry|
              blank_label = entry[:blank_label]
              challenge = entry[:challenge]
              correct_answer_label = challenge.correct_answer_image ? challenge.correct_answer_image.label : challenge.correct_answer_text
              frame_index = challenge.lesson_content.frames.index(challenge.frame)+1
              challenge_index = challenge.frame.main_ui_component.challenges.index(challenge)
              editor_template = challenge.frame.main_ui_component.editor_template
              puts "#{challenge.lesson.lesson_guid}:#{challenge.lesson.title}: Frame #{frame_index} Challenge #{challenge_index} #{editor_template} --> #{correct_answer_label} != #{blank_label}"
            end

          end

        end
    end
end