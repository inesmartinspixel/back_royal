'use strict';

angular.module('frontRoyalApp').ooDirective('LOWER_CAMEL_CASED_NAMEInfoPanel', [
    'editFrameInfoPanelDirBase',
    EditFrameInfoPanelDirBase => EditFrameInfoPanelDirBase.subclass({
        templateUrl: 'components/lessons/views/frame_types/UNDERSCORED_NAME/UNDERSCORED_NAME_info_panel.html',
        link(scope, elem, attrs) {
            EditFrameInfoPanelDirBase.link(scope, elem, attrs);
        }
    })
]);
