'use strict';

describe('Lessons::FrameTypes::UPPER_CAMEL_CASED_NAMECsvImporter', () => {
    let Importer;
    let frame;
    let row;
    let SpecHelper;

    beforeEach(() => {
        angular.mock.module('frontRoyalApp', 'SpecHelper');

        // create an instance of Collection and spy on it
        angular.mock.inject(['Prototype.Class', 'UPPER_CAMEL_CASED_NAMECsvImporter', 'SpecHelper', (Class, UPPER_CAMEL_CASED_NAMECsvImporter, _SpecHelper_) => {
            Importer = UPPER_CAMEL_CASED_NAMECsvImporter;
            SpecHelper = _SpecHelper_;

            row = {
                'Title': 'Frame Title'
            };

            frame = new Importer(row).frame;
        }]);

    });

    it('should have frame type set to UNDERSCORED_NAME', () => {
        expect(frame.frame_type).toBe('UNDERSCORED_NAME');
    });

    it('should have some tests', () => {
        expect(false).toEqual(true);
        console.log(SpecHelper);
    });
});