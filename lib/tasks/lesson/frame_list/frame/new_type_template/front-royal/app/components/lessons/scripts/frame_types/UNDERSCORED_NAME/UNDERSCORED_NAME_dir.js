'use strict';

angular.module('frontRoyalApp').ooDirective('LOWER_CAMEL_CASED_NAME', [
    'frameTypeDirBase',
    FrameTypeDirBase => FrameTypeDirBase.subclass({
        templateUrl: 'components/lessons/views/frame_types/UNDERSCORED_NAME/UNDERSCORED_NAME.html',

        link(scope, elem, attrs) {
            FrameTypeDirBase.link(scope, elem, attrs);
        }

    })
]);
