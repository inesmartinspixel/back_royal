'use strict';

angular.module('frontRoyalApp')
    .factory('UPPER_CAMEL_CASED_NAMECsvImporter', [
        'Prototype.Class',
        'CsvImporterBase',

        (Class, CsvImporterBase) => Class.create(CsvImporterBase, angular.extend({}, {

            frame_type: 'UNDERSCORED_NAME',

            initialize($super, row) {
                $super(row);
            }
        }))
    ]);
