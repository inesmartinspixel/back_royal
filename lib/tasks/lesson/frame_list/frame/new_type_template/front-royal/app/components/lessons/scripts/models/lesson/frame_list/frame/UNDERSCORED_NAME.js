'use strict';

angular.module('frontRoyalApp').factory('Lesson.FrameList.Frame.UPPER_CAMEL_CASED_NAME', [
    'Lesson.FrameList.Frame', 'UPPER_CAMEL_CASED_NAMECsvImporter',
    (Frame, UPPER_CAMEL_CASED_NAMECsvImporter) => Frame.subclass(function() {
        // identifier used in csv's 'Interactivity' column
        this.alias('UNDERSCORED_NAME');

        this.extend({
            csvImporter: UPPER_CAMEL_CASED_NAMECsvImporter,
            title: '',
            thumbnail: 'Lorem ____ dolor sit amet, consectetur adipiscing ______.',
        });

        return {
            directiveName: 'DASHED_NAME',
            buttonDirectiveName: '',
            infoPanelDirectiveName: 'DASHED_NAME-info-panel'
        };
    })
]);
