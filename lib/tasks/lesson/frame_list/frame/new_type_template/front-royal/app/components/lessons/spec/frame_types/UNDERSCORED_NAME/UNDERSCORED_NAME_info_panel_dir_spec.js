import 'Lessons/angularModule/spec/_mock/fixtures/lessons';

describe('Lessons::UPPER_CAMEL_CASED_NAMEInfoPanelDir', () => {
    let helper;
    let UPPER_CAMEL_CASED_NAME;
    let InfoPanelSpecHelper;

    beforeEach(() => {
        angular.mock.module(
            'frontRoyalApp',
            'LessonSpecHelpers',
            'components/lessons/views/lesson_types/frame_list/info_panel_layout.html',
            'components/lessons/views/frame_types/UNDERSCORED_NAME/UNDERSCORED_NAME_info_panel.html'
        );

        angular.mock.inject(($injector, _InfoPanelSpecHelper_) => {
            $injector.get('LessonFixtures');
            InfoPanelSpecHelper = _InfoPanelSpecHelper_;
            UPPER_CAMEL_CASED_NAME = $injector.get('Lesson.FrameList.Frame.UPPER_CAMEL_CASED_NAME');

            const sampleFrame = UPPER_CAMEL_CASED_NAME.fixtures.getInstance();
            helper = InfoPanelSpecHelper.getHelper(sampleFrame, []);
        });
    });

    it('should have some tests', () => {
        expect(false).toBe(true);
        console.log(helper);
    });
});
