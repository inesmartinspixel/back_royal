Dir.glob(Rails.root.join('db', 'migrate', 'componentized_migrators', '*.rb')).each do |path|
    require path
end

namespace :migrate_frames do

    desc "Removes the miniInstructionsMap, which was not meant to be serialized into the frame"
    task :remove_mini_instructions => :environment do
        migrator = RemoveMiniInstructionsMigrator.new
        migrator.change
    end
end