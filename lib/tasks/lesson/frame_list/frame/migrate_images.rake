namespace :migrate_images do

    desc "Schedule 3x image jobs"
    task :schedule_3x_image_tasks => :environment do                

        helper = MigrateImagesHelper.new
        ids = helper.get_lesson_ids(true)

        ids.each do |id|
            puts "Scheduling lesson #{id}"
            Ensure3xPhotoSizes.perform_later(id, helper.version_types)
        end

        puts "Scheduled #{ids.size} jobs"

    end # task

    desc "Run 3x image jobs"
    task :run_3x_image_tasks => :environment do                

        helper = MigrateImagesHelper.new
        ids = helper.get_lesson_ids(false)

        ids.each do |id|
            puts "Running lesson #{id}"
            Ensure3xPhotoSizes.new.perform(id, helper.version_types, false)
        end

        puts "Ran #{ids.size} jobs"

    end # task

    class MigrateImagesHelper

        def initialize
            #ActiveRecord::Base.logger = Logger.new(STDOUT)
            @loaded_lesson_ids = Set.new
            @lesson_count = Lesson.count
        end

        def get_lesson_ids(skip_queued)
            
            if lesson_id = ENV['lesson_id']
                return [lesson_id]
            else

                limit = ENV['limit'] ? ENV['limit'].to_i : nil
                lesson_ids = []
                if limit
                    new_lesson_ids = load_more_lesson_ids(skip_queued, limit)
                    while lesson_ids.size < limit && @loaded_lesson_ids.size < @lesson_count
                        lesson_ids += new_lesson_ids
                        new_lesson_ids = load_more_lesson_ids(skip_queued, limit)
                    end
                    lesson_ids = lesson_ids.slice(0, limit)
                else
                    lesson_ids = load_more_lesson_ids(skip_queued, nil)
                end

                return lesson_ids
            end               
        end

        def version_types
            ENV['version_types'].split(',') # comma delimited string with some of live, published, old
        end

        def load_more_lesson_ids(skip_queued, limit)
            query = Lesson.where("1=1")
            
            query = query.where(["id not in (?)", @loaded_lesson_ids.to_a]) if @loaded_lesson_ids.any?
            query = query.limit!(limit) if limit
            query = query.select('id', 'content_json', 'title')
            query = query.reorder('updated_at asc') # we want to move recently updated lessons to the end, since they'll be skipped anyway

            lesson_ids = []
            lessons = query.to_a
            lessons.each do |lesson|
                @loaded_lesson_ids << lesson.id
                if !Ensure3xPhotoSizes.has_images_to_process?(lesson)
                    puts "Skipping #{lesson.title}/#{lesson.id} because it has no images to process"
                    next
                end 
                if skip_queued && Ensure3xPhotoSizes.queued?(lesson, version_types)
                    puts "Skipping #{lesson.title}/#{lesson.id} because it is already queued"
                    next
                end
                lesson_ids << lesson.id
            end

            lesson_ids
        end
    end

    desc "Delete queued jobs"
    task :delete_queued_jobs => :environment do
        Delayed::Job.where(queue: 'ensure_3x_photo_sizes').delete_all
    end
end # namespace