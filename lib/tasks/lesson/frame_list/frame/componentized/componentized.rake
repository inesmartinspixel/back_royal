Dir.glob(Rails.root.join('db', 'migrate', 'componentized_migrators', '*.rb')).each do |path|
	require path
end

namespace :lesson do
    namespace :frame_list do
        namespace :frame do
            namespace :componentized do

                desc "Find leading frames that could use the ignore leading zeroes feature"
                task :missing_ignore_leading_zeroes => :environment do
                    published_frames = Lesson.all_published.map(&:content).map(&:frames).flatten
                    challenge_blank_types = [
                        'compose_blanks',
                        'compose_blanks_on_image',
                    ]

                    published_frames.each do |published_frame|
                        if published_frame.frame_type == 'componentized' && challenge_blank_types.include?(published_frame.main_ui_component.editor_template)
                            components = published_frame.components.flatten.select
                            matches_expected_texts = components.select do |c|
                                c.is_a?(Lesson::Content::FrameList::Frame::Componentized::Component::AnswerMatcher::MatchesExpectedText) &&
                                c.expectedText =~ /^0?\.[0-9]+$/i &&
                                c.mode != 'decimal'
                            end


                            if matches_expected_texts.length > 0
                                puts "\nhttps://smart.ly/editor/lesson/#{published_frame.lesson.id}/edit?frame=#{published_frame.id}"
                                matches_expected_texts.map {|m| "#{m.expectedText} :: #{m.mode}" }.each do |m|
                                    puts " - #{m}"
                                end
                            end
                        end
                    end
                end

                desc "Performs an audit given a particular editor template type i.e. `rake lesson:frame_list:frame:componentized:find_all_editor_templates editor_template=this_or_that`"
                task :find_all_editor_templates => :environment do


                    editor_template = ENV['editor_template']
                    lessons = Lesson.all
                    unpublished_total = 0
                    published_total = 0

                    lessons = lessons.sort_by { |l| l.has_published_version? ? 0 : 1 }

                    lessons.each do |lesson|

                        frames = []
                        lesson.content.frames.each_with_index do |frame, i|

                            next unless frame['frame_type'] == 'componentized'
                            next unless frame.main_ui_component.editor_template == editor_template
                            frames << frame
                        end

                        if frames.any?

                            if lesson.published_and_in_a_stream?
                                prefix = 'Published'
                                published_total += 1
                            else
                                prefix = 'Unpublished'
                                unpublished_total += 1
                            end

                            print "\n\n#{prefix} Lesson: '#{lesson.title}'"
                            print "\n * Editor: https://smart.ly/editor/lesson/#{lesson.id}/edit - Frames: "+frames.map(&:index).map { |i| i+1 }.join(', ')
                            if lesson.published_and_in_a_stream?
                                frames.each do |frame|
                                    print "\n * Player frame #{frame.index + 1}: https://smart.ly/stream/#{lesson.published_streams.first.id}/chapter/0/lesson/#{frame.lesson.id}/show?frame=#{frame.id}"
                                end
                            end
                        end


                    end

                    puts "\n\nTotal number of published '#{editor_template}' frames: #{published_total}"
                    puts "\n\nTotal number of unpublished '#{editor_template}' frames: #{unpublished_total}"

        		end


                desc "Produces a list of URLs that have published frames with MathJax content, creating audit lesson as well"
                task :mathjax_frame_audit => :environment do

                    frames = get_published_mathjax_frames

                    puts "#{frames.size} frames found"

                    mathjax_frames = []
                    frames.each do |frame|
                        if frame.text_content.match(/[\%\%|\$\$]+/)
                            mathjax_frames << frame
                            puts "http://localhost:3000/editor/lesson/#{frame.lesson.id}/edit?frame=#{frame.id}"
                        end
                    end

                    stream = create_audit_stream('Mathjax Styling Audit')
                    add_chapter(stream, 'Mathjax', mathjax_frames.shuffle.slice(0, 100))
                    stream.publish!
                end

                desc "Produces a list of URLs that have frames with MathJax punct"
                task :mathjax_frame_audit_punct => :environment do

                    frames = get_all_mathjax_frames

                    puts "#{frames.size} frames found"

                    mathjax_frames = []
                    frames.each do |frame|
                        if frame.full_text_content.match(/[\%\%|\$\$]+.*\\\!/)
                            mathjax_frames << frame
                            puts "http://localhost:3000/editor/lesson/#{frame.lesson.id}/edit?frame=#{frame.id}"
                        end
                    end

                end

                desc "Produces a list of URLs that have frames with MathJax text"
                task :mathjax_frame_audit_text => :environment do

                    frames = get_all_mathjax_frames

                    puts "#{frames.size} frames found"

                    mathjax_frames = []
                    frames.each do |frame|
                        if frame.full_text_content.match(/[\%\%|\$\$]+.*\\text\{.*\".*\}/)
                            mathjax_frames << frame
                            puts "http://localhost:3000/editor/lesson/#{frame.lesson.id}/edit?frame=#{frame.id}"
                        end
                    end
                end


                desc "Produces a list of URLs that have frames with MathJax hSpace"
                task :mathjax_frame_audit_hspace => :environment do

                    frames = get_all_mathjax_frames

                    puts "#{frames.size} frames found"

                    mathjax_frames = []
                    frames.each do |frame|
                        if frame.full_text_content.match(/[\%\%|\$\$]+.*\\hspace\{\-/)
                            mathjax_frames << frame
                            puts "http://localhost:3000/editor/lesson/#{frame.lesson.id}/edit?frame=#{frame.id}"
                        end
                    end

                end

                desc "Produces a list of URLs that have frames with MathJax hSpace"
                task :mathjax_frame_audit_newline_blanks => :environment do

                    frames = get_published_mathjax_frames

                    puts "#{frames.size} frames found"

                    mathjax_frames = []
                    frames.each do |frame|
                        if frame.full_text_content.match(/[\%\%|\$\$]+.*\\\\Blank/)
                            mathjax_frames << frame
                            puts "http://localhost:3000/editor/lesson/#{frame.lesson.id}/edit?frame=#{frame.id}"
                        end
                    end

                end

                desc "Produces a list of URLs of image_hotspot frames"
                task :link_to_image_hotspots => :environment do

                    Lesson.all.each do |lesson|
                        # Go through each frame in the lesson
                        lesson.content.frames.each do |frame|

                            # If the frame is image_hotspot
                            if frame.main_ui_component.editor_template == 'image_hotspot'
                                puts frame.editor_link
                            end
                        end
                    end
                end

                desc "Copies all frames you may have broken by messing with blanks or component_overlay styling and puts them in a stream"
                task :blanks_styling_audit => :environment do

                    stream = create_audit_stream('Blanks Styling Audit')

                    frames = Lesson.all_published_and_in_a_stream.map(&:content).map(&:frames).flatten.select(&:valid?)
                    puts "#{frames.size} frames found"
                    mathjax_frames = []
                    multiple_interactive_card_frames = []

                    [
                        'compose_blanks',
                        'fill_in_the_blanks',
                        'blanks_on_image',
                        'compose_blanks_on_image',
                        'image_hotspot'
                    ].each do |editor_template|

                        frames_for_editor_template = frames.select do |frame|
                            frame.frame_type == 'componentized' && frame.main_ui_component.editor_template == editor_template
                        end

                        frames_for_editor_template.each do |frame|

                            if ['fill_in_the_blanks', 'compose_blanks'].include?(editor_template)
                                frame.main_ui_component.shared_content_for_text.text = "#{frame.main_ui_component.shared_content_for_text.text}\nEditor (Frame #{frame.index + 1}): http://smartly-staging.elasticbeanstalk.com/editor/lesson/#{frame.lesson.id}/edit"
                            else
                                frame.main_ui_component.shared_content_for_text.text = "#{frame.main_ui_component.shared_content_for_text.text}\n\n[Editor (Frame #{frame.index + 1})](http://smartly-staging.elasticbeanstalk.com/editor/lesson/#{frame.lesson.id}/edit)"
                            end

                            if frame.main_ui_component.shared_content_for_text.text.match(/Blank\[/)
                                mathjax_frames << frame
                            end

                            component_overlays = frame.components.select { |component|
                                component.is_a?(Lesson::Content::FrameList::Frame::Componentized::Component::ComponentOverlay)
                            }
                            if component_overlays.size > 1
                                multiple_interactive_card_frames << frame
                            end
                        end

                        add_chapter(stream, editor_template, frames_for_editor_template.shuffle.slice(0, 100)) if frames_for_editor_template.size > 0

                    end

                    add_chapter(stream, 'Mathjax blanks', mathjax_frames.shuffle.slice(0, 100)) if mathjax_frames.size > 0
                    add_chapter(stream, 'Multiple interactive cards', multiple_interactive_card_frames.shuffle.slice(0, 100)) if multiple_interactive_card_frames.size > 0

                    stream.publish!

                end

                desc "Produce a list of URLs with published frames and challenge-blanks"
                task :link_to_all_published_challenge_blanks => :environment do
                    published_frames = Lesson.all_published.map(&:content).map(&:frames).flatten
                    challenge_blank_types = [
                        'compose_blanks',
                        'fill_in_the_blanks',
                        'blanks_on_image',
                        'compose_blanks_on_image',
                        'image_hotspot'
                    ]

                    published_frames.each do |published_frame|
                        if published_frame.frame_type == 'componentized' && challenge_blank_types.include?(published_frame.main_ui_component.editor_template)
                            puts "http://localhost:3000/editor/lesson/#{published_frame.lesson.id}/edit?frame=#{published_frame.id}"
                        end
                    end
                end

                desc "Print out links to all blanks ordered by the font size"
                task :link_to_all_blanks_on_image_by_fontSize => :environment do

                    urls = {}

                    lessons ||= Lesson.all_published_and_in_a_stream
                    text_components = lessons.map(&:content).map(&:frames).flatten.map(&:components).flatten.select { |c| c.is_a?(Lesson::Content::FrameList::Frame::Componentized::Component::ChallengeOverlayBlank) }.map(&:text).compact

                    text_components.each do |component|
                        font_size = component.fontSize
                        url = "/editor/lesson/#{component.frame.lesson.id}/edit?frame=#{component.frame.id}"
                        unless urls[url] && urls[url] > font_size
                            urls[url] = font_size
                        end
                    end
                    #.compact.map(&:fontSize).inject({}) { |h, fontSize| h[fontSize] ||= 0; h[fontSize] = h[fontSize] + 1; h }; hash.keys.sort.map { |k| [k, hash[k]] }

                    file = Tempfile.new('blanks_by_font_size')

                    urls.sort_by { |url, font_size| font_size }.map(&:reverse).each { |v| file.puts "#{v[0]} - <a href=\"http://localhost:3000#{v[1]}\">local</a> - <a href=\"https://smart.ly#{v[1]}\">production</a> <br>" }

                    file.close

                    new_file_name = file.path + ".html"


                    File.rename(file.path, new_file_name)

                    puts "Opening #{new_file_name}"

                    `open #{new_file_name}`


                end

                desc "Copies all frames of some editor template and puts them in a stream ex: `e=matching rake lesson:frame_list:frame:componentized:audit_editor_template`"
                task :audit_editor_template => :environment do

                    editor_template = ENV['e']
                    if editor_template.blank?
                        raise "No editor template"
                    end

                    stream = create_audit_stream(editor_template.titleize + ' Audit')

                    frames = Lesson.all_published_and_in_a_stream.map(&:content).map(&:frames).flatten
                    puts "#{frames.size} frames found"
                    mathjax_frames = []
                    multiple_interactive_card_frames = []

                    [
                        editor_template
                    ].each do |editor_template|

                        frames_for_editor_template = frames.select do |frame|
                            frame.frame_type == 'componentized' && frame.main_ui_component.editor_template == editor_template
                        end

                        add_chapter(stream, editor_template, frames_for_editor_template) if frames_for_editor_template.size > 0

                    end

                    stream.publish!

                end

                def get_published_mathjax_frames
                    Lesson.all_published
                        .where("lessons.content_json::text LIKE ? OR lessons.content_json::text LIKE ?", '%$$%', '%\%\%%')
                        .joins(:lesson_streams_with_just_titles)
                        .map(&:content).map(&:frames).flatten

                end

                def get_all_mathjax_frames
                    Lesson.all
                        .where("lessons.content_json::text LIKE ? OR lessons.content_json::text LIKE ?", '%$$%', '%\%\%%')
                        .map(&:content).map(&:frames).flatten
                end

                def create_audit_stream(title)
                    existing_streams = Lesson::Stream.where('title' => [title])
                    if existing_streams
                        existing_streams.map(&:lessons).flatten.each(&:destroy!)
                        existing_streams.each(&:destroy!)
                    end

                    author = User.first

                    stream = Lesson::Stream.create_from_hash!({
                        "title"=>title,
                        "chapters" => [
                            {
                                "title"=>"Default Chapter Title",
                                "lesson_ids"=>[],
                                "lesson_hashes" => [],
                                "__iguana_type"=>"Lesson.Stream.Chapter"
                            }],
                        "author" => {"id" =>author.id}
                    })

                    stream.chapters = []
                    stream
                end

                def add_chapter(stream, title, frames)
                    chapter =  {'title' => title, 'lesson_hashes' => [], 'lesson_ids' => []}
                    stream.chapters << chapter

                    puts " - add #{frames.size} frames to chapter #{title} in #{stream.title}"

                    i = 0
                    frames.each_slice(20) do |frames_slice|

                        i = i +1

                        lesson = Lesson.create_from_hash!({
                            title: "#{stream.title}: #{title} #{i}",
                            seo_title: "#{stream.title}: #{title} #{i}",
                            frames: frames_slice.as_json,
                            lesson_type: 'frame_list',
                            frame_count: frames_slice.size,
                            "author" => {"id" =>stream.author.id},
                            'tag' => "#{stream.title}"
                        })
                        lesson.was_published = true
                        lesson.pinned = true
                        lesson.pinned_title = 'Pinned by audit'
                        lesson.publish!
                        stream.lessons << lesson
                        chapter['lesson_ids'] << lesson.id
                        chapter['lesson_hashes'] << {'lesson_id' => lesson.id, 'coming_soon' => false}
                    end
                end

                desc "Replaces old centering hack with proper use of alignment property.  Can be removed once run on prod"
                task :re_align => :environment do
                    VersionMigrator.new(Lesson).where(%Q|content_json->>'frames' ~ '->.*<-' and content_json->>'frames' ~ '"unlink_blank_from_answer":true'|).each() do |lesson|

                        lesson.content.frames.each do |frame|
                            next unless frame.main_ui_component.editor_template.match(/_on_image/)
                            unlinked_challenges = frame.components.select do |component|
                                component.is_a?(Lesson::Content::FrameList::Frame::Componentized::Component::Challenge) &&
                                component.unlink_blank_from_answer
                            end

                            unlinked_challenges.each do |challenge|
                                blank = frame.components.detect do |component|
                                    component.is_a?(Lesson::Content::FrameList::Frame::Componentized::Component::ChallengeOverlayBlank) &&
                                    component.challenge_id == challenge.id
                                end

                                if blank.text && match = blank.text.text.match(/^->(.*)<-$/)
                                    blank_text_with_centering = blank.text.text
                                    blank_text_without_centering = match[1]

                                    blank.text.text = match[1]
                                    blank.text.alignment = 'center'

                                    if blank.challenge.correct_answer_text == blank_text_without_centering
                                        challenge.unlink_blank_from_answer  = false
                                    end

                                    if challenge.unlink_blank_from_answer
                                        puts ["still unlinked", blank_text_with_centering, blank.challenge.correct_answer_text,  lesson.attributes['id'], frame.id].inspect
                                    else
                                        puts ["changed to linked", blank_text_with_centering, blank.challenge.correct_answer_text,  lesson.attributes['id'], frame.id].inspect
                                    end

                                elsif blank.text
                                    puts ["skipped", blank.text.text, blank.challenge.correct_answer_text, lesson.attributes['id'], frame.id].inspect
                                end
                            end

                        end
                    end
                end

                desc "Detect image_hotspot frames with an answer alignment set"
                task :detect_image_hotspot_with_alignment => :environment do

                    Lesson.all.each do |lesson|
                        # Go through each frame in the lesson
                        lesson.content.frames.each do |frame|

                            # If the frame is image_hotspot
                            if frame.respond_to?(:components) && frame.main_ui_component.editor_template == 'image_hotspot'

                                # Then go through each of the image_hotspot's components
                                frame.components.each do |component|

                                    # If a component is an answer list
                                    if component.is_a? Lesson::Content::FrameList::Frame::Componentized::Component::AnswerList

                                        # Then go through each answer
                                        component.answers.each do |answer|

                                            # If there is a text component and an alignment is set
                                            if answer.text and answer.text.alignment
                                                puts "#{frame.editor_link} -- #{answer.text.alignment}"
                                            end
                                        end
                                    end
                                end
                            end
                        end
                    end
                end



                desc "Detect any occurrences of frames with Math Processing errors"
                task :detect_math_processing_errors => :environment do

                    error_frames = Lesson.all_published
                        .where("lessons.content_json::text ILIKE '%math processing error%'")
                        .joins(:lesson_streams_with_just_titles)
                        .map(&:frames).flatten.select do |frame|
                            text_components = frame.components.select do |component|
                                component.is_a?(Lesson::Content::FrameList::Frame::Componentized::Component::Text)
                            end
                            text_components.map(&:formatted_text).flatten.join(' ').match(/math processing error/i)
                        end

                    error_frames.each do |frame|
                        puts frame.editor_link
                    end

                end



            end
        end
    end
end