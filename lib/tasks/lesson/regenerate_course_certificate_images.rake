require 'fileutils'

# rake lesson:regenerate_all_certificate_images
# rake lesson:backfill_missing_certificate_images

class CourseCertificateImageGenerator

    def self.process_completed_streams(relation, dry_run = false)
        relation = relation.where.not(completed_at: nil)
        batch = 0
        total = relation.count
        count = 0

        # there is very little danger here of interfering with
        # updates to stream progresses, since these are all already completed
        # anyway and not much can happen to a stream progress once it is done,
        # but since only a tiny portion of the time here is in queries anyway,
        # use a batch size of 1
        relation.find_each(batch_size: 1).with_index do |stream_progress, i|
            if dry_run
                puts " ************ #{count+=1} of #{total} - Running generate_certificate_image with: #{stream_progress.certificate_image.formats['700x275']['url'].inspect}"
            else
                ActiveRecord::Base.transaction do
                    result = stream_progress.generate_certificate_image!

                    if result
                        puts " ************ #{count+=1} of #{total} - Replaced old image with: #{stream_progress.certificate_image.formats['700x275']['url'].inspect}"
                    else
                        puts " ************ #{count+=1} of #{total} - Generated image is the same as the old one: #{stream_progress.certificate_image.formats['700x275']['url'].inspect}"
                    end
                end
            end
        end

        batch += 1
    end

end

namespace :lesson do
    desc "Regenerate certificate images from a CSV containing a list of lesson stream progress record ids"
    task :regenerate_certificate_images_from_csv => :environment do
        dry_run = ENV['dry_run'] == 'true'
        target_file = ENV['file']

        if target_file.blank?
            puts "No file specified; example usage: `rake lesson:regenerate_certificate_images_from_csv file=tmp/stream_progress_record_ids.txt`"
            puts "Proper file format:"
            puts %Q~
                '182fca99-1fc9-47bc-a3b3-5f8571468bfb',
                'f736f6a5-3638-4385-a0e9-6192f757b8b4',
                ...
                '38d2e406-d1f2-427b-b962-d89f64baa7d0',
                '9ee52cc7-edf0-4a8e-ac7a-764fe6e010a7'
            ~
            raise
        end

        file_contents = File.read(target_file)
        query = Lesson::StreamProgress.where("id in (#{file_contents})")
        CourseCertificateImageGenerator.process_completed_streams(query, dry_run)
    end

    desc "Regenerate all certificate images for spanish users"
    task :regenerate_es_certificate_images => :environment do
        CourseCertificateImageGenerator.process_completed_streams(
            Lesson::StreamProgress.joins(:user).where("users.pref_locale" => 'es')
        )
    end # task
end # namespace
