namespace :lesson do

    namespace :stream_progress do

        desc "Backfill the completed_at timestamp for a list of lesson_streams_progress record ids from a file to the updated_at value on the record"
        task :backfill_completed_at_timestamp => :environment do
            target_file = ENV['file']

            if target_file.blank?
                puts "No file specified; example usage: `rake lesson:stream_progress:backfill_completed_at_timestamp file=tmp/stream_progress_record_ids.txt`"
                puts "Proper file format:"
                puts %Q~
                    '182fca99-1fc9-47bc-a3b3-5f8571468bfb',
                    'f736f6a5-3638-4385-a0e9-6192f757b8b4',
                    ...
                    '38d2e406-d1f2-427b-b962-d89f64baa7d0',
                    '9ee52cc7-edf0-4a8e-ac7a-764fe6e010a7'
                ~
                raise
            end

            file_contents = File.read(target_file)

            query = Lesson::StreamProgress.where("id in (#{file_contents})")
            total_count = query.count
            processed_records_count = 0
            batch_size = 100

            puts "Backfilling completed_at timestamp for #{total_count} lesson_streams_progress records"

            query.in_batches(of: batch_size).each_record do |stream_progress|
                stream_progress.update_column(:completed_at, stream_progress.updated_at)
                processed_records_count += 1
                puts "Processed batch #{processed_records_count/batch_size}/#{(total_count/batch_size).ceil}" if processed_records_count % batch_size == 0
            end

            puts "Successfully backfilled completed_at timestamp for #{processed_records_count}/#{total_count} lesson_streams_progress records"
            puts "Rake task complete!"
        end
    end
end