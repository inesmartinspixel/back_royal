namespace :lesson do
    
    desc "go through all lessons and versions and re-generate key terms"
    task :backfill_key_terms => :environment do
        VersionMigrator.new(Lesson).where('content_json IS NOT NULL').each do |lesson|
            all_text = lesson.content_json['frames'].
                            map { |f| f['components'] }.flatten.compact.
                            select { |c| c['component_type'] == 'ComponentizedFrame.Text' }.
                            map { |c| c['text'] }.join(' ')

            
            lesson.key_terms = all_text.scan(/\*\*([^\*]+)\*/).to_a.flatten.map do |term|
                # remove brackets
                term.gsub(/[\[\]]/, '')
            end.uniq
        end
    end

    desc "go through all lessons and print out key terms with non alphanumberic chars"
    task :list_problematic_key_terms => :environment do
        num_to_check = 0
        Lesson.where('content_json IS NOT NULL').each do |lesson|
            all_text = lesson.content_json['frames'].
                            map { |f| f['components'] }.flatten.compact.
                            select { |c| c['component_type'] == 'ComponentizedFrame.Text' }.
                            map { |c| c['text'] }.join(' ')

            key_terms = ""
            all_text.scan(/\*\*([^\*]+)\*/).to_a.flatten.select do |term|
                # filter for alphanumeric
                if term =~ /[^0-9a-z ]/i 
                    key_terms = "#{key_terms} #{term}"
                end
            end

            if !key_terms.blank?
                num_to_check = num_to_check + 1
                puts "Non alphanumeric key terms in lesson #{lesson.id} #{key_terms}" 
            end
        end
        puts "#{num_to_check} lessons to check"
    end

end # namespace