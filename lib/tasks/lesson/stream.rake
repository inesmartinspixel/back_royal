namespace :lesson do

    namespace :stream do

        desc "Create a new stream from a csv file: `rake lesson:stream:create path=/path/to/stream.csv`"
        task :create => :environment do

            path = ENV['path']
                if path.blank?
                    raise "Please provide a path to a .csv describing the new stream. i.e. `rake lesson:stream:create path=/path/to/stream.csv`"
                end
                Lesson::Stream.create_from_csv(path)
        end

        # NOTE: To use this to generate a pretty course summary, do the following:
        #
        # 1. Run this script with the appropriate group and copy-paste the output, e.g.:
        #
        #    rake lesson:stream:pretty_course_summary group=SMARTER locale=en
        #
        # 2. Open the HTML and print to PDF, if you desire
        #
        desc "Generate list of streams and lessons as a nice course summary"
        task :pretty_course_summary => :environment do
            group = ENV['group']
            localeCode = ENV['locale']

            if localeCode.blank? || group.blank?
                puts "Missing arugments. Requires group and locale."
                puts "Example: rake lesson:stream:pretty_course_summary group=SMARTER locale=en"
                raise
            end

            locales = {
                'en' => {
                    'key_terms' => 'KEY TERMS',
                    'coming_soon' => '(COMING SOON)',
                    'course_description' => 'Course Description',
                    'course_highlights' => 'Course Highlights',
                    'chapter' => 'Chapter'
                },
                'es' => {
                    'key_terms' => 'TÉRMINOS CLAVE',
                    'coming_soon' => '(PRÓXIMAMENTE)',
                    'course_description' => 'Descripción del Curso',
                    'course_highlights' => 'Lo más Destacado del Curso',
                    'chapter' => 'Capítulo'
                }
            }

            locale = locales[localeCode.to_s]

            if locale.nil?
                puts "No locale found for code #{localeCode}"
                raise
            end

            require 'csv'

            markdownBuffer = []

            path = File.join(Rails.root, 'tmp', 'lessons.csv')
            CSV.open( path, 'w' ) do |csv|
                csv << ['Stream', 'Chapter Index', 'Chapter', 'Lesson', 'Description']
                streams = Lesson::Stream.all_published.sort_by(&:title).to_a

                streams.reject! do |stream|
                    groups = stream.groups.map(&:name).map(&:upcase)

                    if (!group.blank? && !groups.include?(group.upcase)) || stream.locale.to_s != localeCode.to_s || stream.coming_soon
                        true
                    else
                        false
                    end
                end

                streams.each_with_index do |stream, stream_index|
                    groups = stream.groups.map(&:name).map(&:upcase)
                    topics = stream.locale_pack.content_topics.map do |content_topic|
                        content_topic.locales[localeCode.to_s].upcase
                    end

                    markdownBuffer << "\n#{stream.title} \n======"

                    markdownBuffer << "> *#{locale['course_description']}:*"
                    markdownBuffer << "> "
                    markdownBuffer << "> **#{stream.description}**"

                    markdownBuffer << ">"
                    markdownBuffer << "> *#{locale['course_highlights']}:*"
                    markdownBuffer << "> "
                    stream.what_you_will_learn.each do |w|
                        markdownBuffer << "> * #{w}"
                    end

                    key_terms = []

                    stream.chapters.each_with_index do |chapter, chapter_index|
                        markdownBuffer << "\n#{locale['chapter']} #{chapter_index + 1}: #{chapter['title']} \n--------\n\n"
                        lesson_ids = chapter['lesson_hashes'].map do |h|
                            h['lesson_id']
                        end

                        Lesson.where(id: lesson_ids).each do |lesson|
                            lesson_key_terms = lesson.key_terms
                            lesson_key_terms.map! do |k|
                                k.gsub(/\{\w+?\:(\w+?)\}/) do |m|
                                    $1
                                end
                            end
                            key_terms.concat(lesson_key_terms)

                            markdownBuffer << " 1. #{lesson.title}"
                            lesson.description.each do |d|
                                markdownBuffer << "  * #{d}"
                            end
                            csv << [stream.title, chapter_index+1, chapter['title'], lesson.title, lesson.description.join("\n")]
                        end
                    end

                    markdownBuffer << "\n\n**#{locale['key_terms']}**\n\n"

                    key_terms.sort_by(&:upcase).each do |k|
                        markdownBuffer << " * #{k.mb_chars.upcase}"
                    end

                    if stream_index < streams.length - 1
                        markdownBuffer << "\n-------\n"
                    end
                end
            end
            puts "CSV written to #{path}"

            require 'redcarpet'
            markdown = Redcarpet::Markdown.new(Redcarpet::Render::HTML)
            rendered = markdown.render(markdownBuffer.join("\n"))

            # add some special classes for lists of key terms
            rendered.gsub!("<p><strong>#{locale['key_terms']}</strong></p>\n\n<ul>", "<p><strong>#{locale['key_terms']}</strong></p>\n\n<ul class='key-terms-list'>")

            header = %{
                <!DOCTYPE html>
                <html>
                <head>
                <meta charset="utf-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>Smartly Course Catalog</title>
                <link rel="stylesheet" href="https://stackedit.io/res-min/themes/base.css" />
                <script type="text/javascript" src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_HTML"></script>
                <style type="text/css">
                    .key-terms-list {
                        display: flex;
                        flex-flow: row wrap;
                        justify-content: flex-start;
                    }
                    .key-terms-list li {
                        width: calc( 33% - 15px );
                        text-align: left;
                        margin-right: 15px;
                        padding-right: 5px;
                    }
                    .blockquote {
                        border-left-width: 10px;
                        background-color: #f8f8f8;
                        border-top-right-radius: 5px;
                        border-bottom-right-radius: 5px;
                        padding: 15px 20px;
                        margin: 0 0 1.1em;
                        border-left: 5px solid #eee;
                    }
                    hr {
                        margin-top: 4em;
                    }
                    @media print {
                        html, body {
                            height: 99%;
                        }
                    }
                </style>
                <script type="text/javascript">
                  MathJax.Hub.Config({
                    tex2jax: {inlineMath: [['%%','%%']]}
                  });
                </script>
                </head>
                <body>
                    <div class="container">
                    <h1 style="font-size: 4em">Smartly Course Catalog</h1>
            }
            footer = %{
                    <br><br>
                    &copy;2016 Pedago, LLC. All rights reserved.
                    </div>
                </body>
                </html>
            }

            path = File.join(Rails.root, 'tmp', "#{group}-summary.html")
            File.write(path, header + rendered + footer)

            puts "HTML written to #{path}"
        end

        desc "Return the lesson and frame number where a phrase first occurs"
        task :search_for_phrase_in_stream => :environment do

            streams = Lesson::Stream.all
            streams.each_with_index do |stream, stream_index|
                STDOUT.puts "#{stream_index} :: #{stream.title}"
            end

            STDOUT.puts "Choose a stream to search (0-#{streams.length-1}): "

            input = STDIN.gets.strip.to_i
            if input > 0 && input < streams.length - 1
                stream = streams[input]

                STDOUT.puts "What text should I search for? "
                search_text = STDIN.gets.strip

                # Search through each chapter
                stream.chapters.each_with_index do |chapter, chapter_index|
                    # Search through each lesson
                    STDOUT.puts "SEARCHING CHAPTER #{chapter_index}"
                    chapter['lesson_ids'].to_a.each_with_index do |lesson_id, lesson_index|
                        # search the lesson, one frame at a time, for the text
                        lesson = Lesson.where(id: lesson_id.to_s).first
                        STDOUT.puts "SEARCHING LESSON #{lesson_index}: #{lesson.title}"
                        lesson.content.frames.each_with_index do |frame, index|
                            # Capture useful frame-specific details
                            components = frame.components.flatten.select
                            challenges_component = components.select { |c| c.is_a?(Lesson::Content::FrameList::Frame::Componentized::Component::Challenges) }.first
                            frame_type = challenges_component && challenges_component.editor_template || 'no_interaction'
                            messages = components.select { |c| c.is_a?(Lesson::Content::FrameList::Frame::Componentized::Component::MultipleChoiceMessage) }.map(&:message_text).map(&:text)
                            frame_texts = components.select { |c| c.is_a?(Lesson::Content::FrameList::Frame::Componentized::Component::Text) }.map(&:text)

                            # Search frame texts
                            STDOUT.puts "SEARCHING FRAME #{index}"
                            [frame_texts, messages].flatten.each do |text|
                                if !text.downcase.index(search_text.downcase).nil?
                                    STDOUT.puts "FOUND THE STRING!"
                                    STDOUT.puts "Frame #{index} (#{frame_type}): #{text}"

                                    STDOUT.puts "Keep going? (y/n): "
                                    keep_going = STDIN.gets.strip
                                    if keep_going != 'y'
                                        exit
                                    end
                                end
                            end
                        end
                    end
                end

                STDOUT.puts "STRING NOT FOUND!"
            end
        end

        desc "Generate list of streams and lessons with key terms"
        task :key_terms_for_stream => :environment do

            require 'csv'
            path = File.join(Rails.root, 'tmp', 'lessons_key_terms.csv')
            CSV.open( path, 'w' ) do |csv|
                csv << ['Stream', 'Chapter Index', 'Chapter', 'Lesson', 'Key Terms']
                streams = Lesson::Stream.all_published
                streams.each_with_index do |stream, i|
                    puts "stream #{i+1} of #{streams.size}"
                    stream.chapters.each_with_index do |chapter, chapter_index|
                        Lesson.where(id: chapter['lesson_ids']).select('title') do |lesson|
                            csv << [stream.title, chapter_index+1, chapter['title'], lesson.title, lesson.key_terms.join(',')]
                        end
                    end
                end
            end

            puts "written to #{path}"

        end

        desc "Get list of all lessons with the number of versions since last publish"
        task :count_working_versions => :environment do

            puts [
                "Stream",
                "Lesson",
                "Link",
                "Published at",
                "Versions since published",
                "Edited by"
            ].join("\t")

            Lesson::Stream.all_published
                .joins(:access_groups)
                .where("access_groups.name = 'SMARTER'")
                .includes(:lessons => :content_publishers)
                .find_in_batches(batch_size: 10)
            .each do |streams|
                streams.each do |stream|

                    stream.lessons.each do |lesson|

                        published_version = lesson.published_version
                        if published_version
                            versions_since = lesson.versions
                                                .where("version_created_at > ?", published_version.version_created_at)
                                                .select(:last_editor_id)
                        end
                        line = [
                            stream.title,
                            lesson.title,
                            lesson.editor_link,
                            published_version && published_version.version_created_at,
                            published_version && versions_since.size,
                            published_version && versions_since.map(&:last_editor).uniq.map { |e| e ? e.email : 'null' }.join('; ')
                        ].join("\t")


                        puts line
                    end


                end


            end
        end

    end

end