require 'digest/sha1'

namespace :hiring_relationship do

    desc "link hiring managers to candidates (eg - `h=hiring1@email.com,hiring2@email.com c=cand1@email.com,cand2@email.com rake hiring_relationship:create`"
    task :create => :environment do

        hiring_emails = ENV['h'].split(',')
        candidate_emails = ENV['c'].split(',')

        hiring_managers = hiring_emails.map do |e|
            user = User.find_by_email(e)
            raise "No user found for #{e}" unless user
            raise "#{e} is not a hiring manager" unless user.hiring_application
            user
        end

        candidates = candidate_emails.map do |e|
            user = User.find_by_email(e)
            raise "No user found for #{e}" unless user
            raise "#{e} is not authorized to edit a career_profile" unless user.can_edit_career_profile?
            user
        end

        hiring_managers.each do |hiring_manager|
            candidates.each do |candidate|
                if hiring_manager.candidates.include?(candidate)
                    puts "Already linked: #{hiring_manager.email}  / #{candidate.email}"
                else
                    puts "Linking       : #{hiring_manager.email} to #{candidate.email}"
                end
            end

            unless ENV['dry_run']
                hiring_manager.ensure_candidate_relationships(candidates)
            end
        end

    end


    # some more ids to use:

    # f12cfc13-1ed5-458a-959a-7865162a8206
    # d592e0cf-9a50-4cce-a946-e2d21b920c0b
    # 25ce613d-fb10-494b-8903-0db0d952800a
    # a4e073fa-53d8-4cf3-9bab-b5c100c29048
    # 34b2cf3f-63ab-4f34-9809-310688a59f15
    # 38de1e3a-41bd-4df9-be40-10ab55bf49cd
    # cb2dfd44-b370-41f4-8af6-84c81e10e8f0
    # 3ac3fd70-08cc-41cd-a103-cd641197fca6
    # 233f7d5d-68e3-473d-a8ff-139fbc1823a7
    # 6f03a3cf-d5cd-4d98-8d80-50848835babb
    # 7214dd3a-c5a3-4c60-bc39-3470831fd016
    # 441986eb-fdab-45fd-8882-95def2e8c6e1

    desc "create a bunch of test data"
    task :create_sample_data => :environment do

        raise "only development" unless Rails.env.development?

        # keeping the same tokens allows us to run this over and over
        # without logging in again
        example_hiring_manager = User.find_by_email("test-conn-hiring-example@example.com")

        # destroy existing sample data
        existing_users = User.where("email like 'test-conn-%'")

        @tokens = {}
        conversations = existing_users.map { |u|
            @tokens[u.email] = u.tokens
            u.mailbox.conversations
        }
        existing_users.destroy_all
        conversations.map(&:destroy_all)

        # destroy existing teams
        team = HiringTeam.where("title like 'test-conn-%'").destroy_all
        team = HiringTeam.create!(
            title: 'test-conn-team'
        )

        example_hiring_manager = create_hiring_manager("hiring-example")
        example_hiring_manager.hiring_team_id = team.id
        example_hiring_manager.save!

        teammate = create_hiring_manager("teammate")
        teammate.hiring_team_id = team.id
        teammate.save!

        another_teammate = create_hiring_manager("another_teammate")
        another_teammate.hiring_team_id = team.id
        another_teammate.save!

        create_hiring_manager("hiring-no-connections")

        create_hiring_relationship(example_hiring_manager, "Waiting on hiring manager 1", 'c4676511-cd00-4bc9-b9d4-8418d9763c0c','pending', 'hidden')
        create_hiring_relationship(example_hiring_manager, "Waiting on hiring manager 2", '8c85f117-1055-4849-b9d7-8fd4922c6686','pending', 'hidden')
        create_hiring_relationship(example_hiring_manager, "Waiting on hiring manager 3", 'f4b9d63f-5c16-4e88-9c38-c3eeca6c14c0','pending', 'hidden')
        create_hiring_relationship(example_hiring_manager, "Waiting on hiring manager 4", '5d75dc6a-6ac6-4bcf-8a25-b2b8c21bc7bd','pending', 'hidden')
        create_hiring_relationship(example_hiring_manager, "Waiting on hiring manager 5", 'f5805f97-4ad0-4614-b75f-bcaafc81dff6','pending', 'hidden')

        create_hiring_relationship(example_hiring_manager, "Rejected by hiring manager", '22d1d76c-e9fc-4f0e-b035-3336a229966a', 'rejected', 'hidden')
        create_hiring_relationship(teammate, "Rejected by teammate", '3233667b-f546-4e18-9012-8f033f8db892', 'rejected', 'hidden')

        create_hiring_relationship(example_hiring_manager, "Waiting on candidate", '9782237f-4415-41b2-9bf2-e086c2f9767c', 'accepted', 'pending')
        create_hiring_relationship(teammate, "Teammate connection pending teammate", '6318babc-fed5-4937-9e05-f7538cded539', 'rejected', 'hidden')

        hr = create_hiring_relationship(example_hiring_manager, "Invited to interview", 'fcf957c0-74c2-47ab-a6b8-5356d792ffb8', 'accepted', 'pending')
        hr.open_position = OpenPosition.create!(
            hiring_manager_id: example_hiring_manager.id,
            title: 'Tiger trainer',
            available_interview_times: 'Thursday at 3'
        )
        create_conversation(hr, hr.hiring_manager, hr.candidate, {
            "body"=>
                "Hello Tej,\n\nWould love to connect and hear more about your experience and share about echoecho. When are you free?\n\nHope to hear from you soon,\n\n#{hr.hiring_manager.preferred_name}",
            "metadata"=>
              {"senderCompanyName"=> hr.hiring_manager.company_name,
               "openPosition"=> hr.open_position.as_json}
        })
        hr.save!
        hr = create_hiring_relationship(teammate, "Teammate invited to interview", 'c6a044c8-9b6e-49a1-b02c-60e32601549a', 'accepted', 'accepted')
        hr.open_position = OpenPosition.create!(
            hiring_manager_id: teammate.id,
            title: 'Teammate\'s open position',
            available_interview_times: 'Thursday at 3'
        )
        conversation = create_conversation(hr, hr.hiring_manager, hr.candidate, {
            "body"=>
                "Hello Tej,\n\nWould love to connect and hear more about your experience and share about echoecho. When are you free?\n\nHope to hear from you soon,\n\n#{hr.hiring_manager.preferred_name}",
            "metadata"=>
              {"senderCompanyName"=> hr.hiring_manager.company_name,
               "openPosition"=> hr.open_position.as_json}
        })
        conversation = send_message(conversation, hr.candidate, "That job sounds great")
        conversation = send_message(conversation, example_hiring_manager, "Trust me. You do not want to work for that guy.")
        hr.save!

        create_hiring_relationship(example_hiring_manager, "Rejected by candidate", '15c76195-554b-4c11-b06d-165375f90332', 'accepted', 'rejected')
        create_hiring_relationship(teammate, "Teammate rejected by candidate", '74d71029-8cb2-41fa-a187-c0c5dfd8ab8a', 'accepted', 'rejected')

        create_hiring_relationship(example_hiring_manager, "Saved for later", 'c3c3fa9b-ae15-4b4f-973e-23d2e84a7413', 'saved_for_later', 'hidden')
        create_hiring_relationship(teammate, "Teammate saved for later", 'cd40450a-08fe-4e87-9a24-c43a88470371', 'saved_for_later', 'hidden')

        r = create_hiring_relationship(example_hiring_manager, "Liked by teammate", '1e49ccaa-a607-47af-970b-26b3315459c5','pending', 'hidden')
        create_hiring_relationship(teammate, r.candidate, nil, 'accepted', 'pending')

        create_hiring_relationship(another_teammate, "Liked by another teammate", '9127d7ff-3cd1-4ab4-b87f-d8ada57e1091', 'accepted', 'pending')

        r = create_hiring_relationship(example_hiring_manager, "New match", 'c47fdfe2-3789-4f4a-aaea-aa931f858114', 'accepted', 'accepted')
        create_conversation(r, r.candidate, r.hiring_manager)
        r = create_hiring_relationship(teammate, "Teammate match", 'aabe5849-46ee-4587-b3d4-f312b4dbf8fe', 'accepted', 'accepted')
        create_conversation(r, r.candidate, r.hiring_manager)

        # create conversation with only one message
        relationship = create_hiring_relationship(example_hiring_manager, "With one message", '62bc2a8c-77a9-4716-81d4-b768db02c63f', 'accepted', 'accepted')
        create_conversation(relationship, relationship.hiring_manager, relationship.candidate)

        # create some conversations with multiple messages
        relationships = [
            create_hiring_relationship(example_hiring_manager, "Conversation with unread message", 'dee947ba-7a8a-48fd-86c1-b3efacab9b2e', 'accepted', 'accepted'),
            create_hiring_relationship(example_hiring_manager, "Conversation with read messages", '18ab14e6-16c2-47a9-b6be-f6484dcb8a20', 'accepted', 'accepted'),
            create_hiring_relationship(example_hiring_manager, "Closed relationship", 'bebf051b-4999-4fa4-9062-04f2e7e2921a', 'accepted', 'accepted'),
            create_hiring_relationship(teammate, "Teammate with conversation", 'd0174888-4387-456b-9ce8-8dfeadc6fef0', 'accepted', 'accepted')
        ]
        conversations = relationships.map do |relationship|
            conversation = create_conversation(relationship, relationship.hiring_manager, relationship.candidate)
            conversation = send_message(conversation, relationship.candidate, "Hey, what's up.  Like monkeys?")
            conversation = send_message(conversation, relationship.hiring_manager, "I do like monkeys.  Do you?")
            conversation = send_message(conversation, relationship.candidate, "Of course")
            conversation
        end

        # mark messages as read for one of those conversations
        mark_all_as_read(conversations[1], example_hiring_manager)

        # close one of those relationships
        relationships[2].update_attribute(:hiring_manager_closed, true)

        # archive a hiring relationship before it has any messages
        relationship = create_hiring_relationship(example_hiring_manager, "Closed without conversation", '16442e3a-e589-41cf-819a-5e053edf70d9', 'accepted', 'accepted')
        relationship.hiring_manager_closed = true
        relationship.save!


        r = create_hiring_relationship(example_hiring_manager, "Featured for both teammates", 'f12cfc13-1ed5-458a-959a-7865162a8206','pending', 'hidden')
        r.update_attributes(hiring_manager_priority: 0)
        r = create_hiring_relationship(teammate, r.candidate, nil, 'pending', 'hidden')
        r.update_attributes(hiring_manager_priority: 0)

        # create another open position and create related CandidatePositionInterest records
        open_position = OpenPosition.create!(
            hiring_manager_id: example_hiring_manager.id,
            title: 'Pokemon Trainer',
            role: 'engineering',
            featured: true
        )
        create_candidate_position_interest(open_position, 'Interested in Position 1', '00a34f23-7bec-4c3f-b0cb-43d1824fa6c6', 'unseen', 'accepted')
        create_candidate_position_interest(open_position, 'Interested in Position 2', 'ea38fe11-b921-46cb-a3ee-7cad45cb995d', 'pending', 'accepted')
        create_candidate_position_interest(open_position, 'Interested in Position 3', '672aee1a-a9f3-4cf0-beca-accb7ceeabaa', 'reviewed', 'accepted')

        create_candidate_position_interest(open_position, 'Not Interested in Position', '0cda1780-be43-4588-8034-d02c9b1622bd', 'unseen', 'rejected')
    end

    desc "set conversation_id for existing relationships"
    task :associate_conversations => :environment do
        query = HiringRelationship.where(
            hiring_manager_status: 'accepted'
        )
        total = query.count
        i = 0
        puts "Processing #{total} hiring relationships"
        query.find_each do |hiring_relationship|
            hiring_relationship.conversation = hiring_relationship.unassociated_conversation
            hiring_relationship.save!(validate: false)
            puts "#{i+1} of #{total} processed" if (i+1) % 10 == 0
            i = i + 1
        end
    end

    desc "transfer connections from one hiring manager to another"
    task :transfer_hiring_relationships => :environment do
        ActiveRecord::Base.transaction do
            from = User.find_by_email(ENV['from'])
            to = User.find_by_email(ENV['to'])

            if from.hiring_team_id != to.hiring_team_id
                # There is a "cannot determine role" error when trying to send a message in the conversation
                # if these two hiring managers are not on the same team.  Probably not too hard to resolve
                # if we ever need to transfer relationships between people who are not on the same team,
                # but there is no need for that right now.
                raise "Can only transfer relationships from a hiring manager to another on the same team"
            end

            # we transfer over any relationships that the hiring manager has accepted.
            from.candidate_relationships.includes(:candidate).where(hiring_manager_status: ['accepted']).each do |relationship|
                existing_relationship = to.candidate_relationships.detect { |cr| cr.candidate_id == relationship.candidate_id }
                # if the target hiring manager has a non-accepted relationship, destroy it
                if existing_relationship && ['saved_for_later', 'rejected', 'pending', 'hidden'].include?(existing_relationship.hiring_manager_status)
                    existing_relationship.destroy
                elsif existing_relationship
                    # it should be impossible to hit this line, due to constraints that prevent two
                    # teammates from accepting the same candidate
                    raise "A relationship exists with hiring_manager_status=#{existing_relationship.hiring_manager_status.inspect}"
                end
                relationship.update_attribute(:hiring_manager_id, to.id)
            end

            from.open_positions.each do |open_position|
                open_position.update_attributes(hiring_manager_id: to.id)
            end
        end
    end

    desc "hide applicable candidate position interests for accepted or rejected relationships"
    task :hide_candidate_position_interests_for_accepted_or_rejected_relationships => :environment do

        # restrict the status so we don't needlessly run this for every relationship
        query = HiringRelationship.where("hiring_manager_status in ('accepted', 'rejected')")

        i = 0
        query.find_in_batches(batch_size: 100) do |relationships|
            relationships.each do |relationship|
                relationship.send(:hide_candidate_position_interests_on_accepted_or_rejected)
                i = i + 1
            end
            puts "#{100*i/query.count.to_f}% complete"
        end

    end

    def create_hiring_relationship(hiring_manager, candidate_name_or_candidate, candidate_id, hiring_manager_status, candidate_status)
        if candidate_name_or_candidate.is_a?(String)
            candidate = create_candidate({
                name: candidate_name_or_candidate,
                email: "test-conn-#{candidate_name_or_candidate.underscore.gsub(/\s/, '-')}@example.com",
                id: candidate_id
            })
        else
            candidate = candidate_name_or_candidate
        end

        HiringRelationship.create!(
            hiring_manager_id: hiring_manager.id,
            candidate_id: candidate.id,
            hiring_manager_status: hiring_manager_status,
            candidate_status: candidate_status
        )
    end

    def create_candidate_position_interest(open_position, candidate_name_or_candidate, candidate_id, hiring_manager_status, candidate_status)
        if candidate_name_or_candidate.is_a?(String)
            candidate = create_candidate({
                name: candidate_name_or_candidate,
                email: "test-conn-#{candidate_name_or_candidate.underscore.gsub(/\s/, '-')}@example.com",
                id: candidate_id
            })
        else
            candidate = candidate_name_or_candidate
        end

        CandidatePositionInterest.create!(
            candidate_id: candidate.id,
            open_position_id: open_position.id,
            candidate_status: candidate_status,
            hiring_manager_status: hiring_manager_status
        )
    end

    def mark_all_as_read(conversation, sender)
        hash = conversation.as_json(user_id: sender.id)
        hash['messages'].map { |m| m['receipts'] }.flatten.each do |receipt|
            receipt['is_read'] = true if receipt['receiver_id'] == sender.id
        end
        Mailboxer::Conversation.update_from_hash!(hash, sender)
    end

    def send_message(conversation, sender, body)
        hash = conversation.as_json(user_id: sender.id)
        hash['messages'] << {
            "sender_id" => sender.id,
            "sender_type" => "user",
            "receipts" => [{
               "receiver_id" => sender.id,
               "is_read" => true,
               "trashed" => false,
               "deleted" => false,
               "mailbox_type" => "sentbox"
            }],
            "body":body
         }

         # if you're sending a message, you must
         # have read all the existing messages
         hash['messages'].map { |m| m['receipts'] }.flatten.each do |receipt|
            receipt['is_read'] = true if receipt['receiver_id'] == sender.id
         end
         Mailboxer::Conversation.update_from_hash!(hash, sender)
    end

    def create_conversation(hiring_relationship, sender, recipient, message_ext = {})
        hiring_relationship.conversation = Mailboxer::Conversation.create_from_hash!({
              "created_at": Time.now.to_i,
              "updated_at": Time.now.to_i,
              "recipients":[
                 {
                    "type":"user",
                    "id":sender.id,
                    "display_name":sender.email
                 },
                 {
                    "type":"user",
                    "id":recipient.id,
                    "display_name":recipient.email
                 }
              ],
              "messages":[
                 {
                    "sender_id":sender.id,
                    "sender_type":"user",
                    "receipts":[{
                       "receiver_id": sender.id,
                       "is_read":true,
                       "trashed":false,
                       "deleted":false,
                       "mailbox_type":"sentbox"
                    }],
                    "body":"Starting the conversation with a super-extra long message where I talk about all sorts of wonderful things like my hopes and dreams and how the Indians are in Game 7 of the World series tonight.\nSo long I needed multipel paragraphs to do it.\n\n\n\nEven one more for good measure"
                 }.merge(message_ext)
              ],
              "subject":"Placeholder: conversation subject. Necessary?"
        }, sender)
        hiring_relationship.save!
        hiring_relationship.conversation
    end

    def create_hiring_manager(email_suffix)
        @sample_hiring_application_params ||= {
            "id"=>"bf116032-a771-4bab-aa24-9f29b7b3edc8",
            "applied_at"=>1476726545,
            "accepted_at"=>1477712956,
            "updated_at"=>1477712956,
            "status"=>"accepted",
            "user_id"=>"5d98c659-c85c-40c8-bd7e-e4c9072f665c",
            "company_name"=>"Cleveland.com",
            "company_logo_url"=>"https://www.crunchbase.com/organization/cleveland-com/primary-image/raw",
            "avatar_url"=>"https://uploads-development.smart.ly/avatars/c55b5aebbcd2f9f06e0c8e1d3d33682c/original/c55b5aebbcd2f9f06e0c8e1d3d33682c.png",
            "job_title"=>"bossman",
            "phone"=>"+12163711234",
            "name"=>"Cleveland John",
            "email"=>"cleveland@cleveland.com",
            "phone_extension"=>nil,
            :logo_url=>nil,
            :place_id=>"ChIJLWto4y7vMIgRQhhi91XLBO0",
            :place_details=>
             {"locality"=>{"short"=>"Cleveland", "long"=>"Cleveland"},
              "administrative_area_level_2"=>{"short"=>"Cuyahoga County", "long"=>"Cuyahoga County"},
              "administrative_area_level_1"=>{"short"=>"OH", "long"=>"Ohio"},
              "country"=>{"short"=>"US", "long"=>"United States"},
              "formatted_address"=>"Cleveland, OH, USA",
              "utc_offset"=>-240},
            :company_year=>2016,
            :company_employee_count=>"5000_plus",
            :company_annual_revenue=>"lesson_than_1_million",
            :website_url=>"http://cleveland.com",
            :funding=>"seed",
            :industry=>"consulting",
            :hiring_for=>"multiple_teams",
            :team_name=>"sdfsdf",
            :team_mission=>"sdfsdf",
            :role_descriptors=>["marketing", "legal", "human_resources"],
            :job_role=>"senior_management",
            :fun_fact=>"I've swam the Cuyahoga.",
            :where_descriptors=>["relocate", "remote"],
            :position_descriptors=>["permanent", "part_time", "contract"],
            :last_calculated_complete_percentage => 100,
            :company_sells_recruiting_services => false,
            :salary_ranges => ['less_than_40000']
        }

        email = "test-conn-#{email_suffix}@example.com"
        avatar_url = avatar_url = avatars.shift

        user = User.create!(
            id: Digest::SHA1.hexdigest(email),
            email: email,
            name: email,
            uid: email,
            password: 'testtest',
            password_confirmation: 'testtest',
            sign_up_code: 'HIRING',
            tokens: @tokens[email],
            avatar_url: avatar_url,
            nickname: email,
            job_title: "Lion Tamer",
            phone: '+2348035126088'
        )

        user.register_content_access
        user.professional_organization = ProfessionalOrganizationOption.find_or_create_by(
            image_url: 'https://cdn.centreinfection.ca/wp/sites/2/20170307164901/generic-logo.jpg',
            text: "John Doe Industries",
            locale: 'en'
        )
        user.save!
        user.hiring_application = HiringApplication.new
        user.hiring_application.merge_hash(@sample_hiring_application_params)
        user.hiring_application.save!
        user
    end

    def avatars
        @avatars ||= [
            'https://images-na.ssl-images-amazon.com/images/M/MV5BMTA1ODM2MzQyMjJeQTJeQWpwZ15BbWU2MDEzODAzNQ@@._V1_UY317_CR8,0,214,317_AL_.jpg',
            'http://i1.kym-cdn.com/entries/icons/original/000/018/526/hulk-hogan.jpg',
            'https://joshubuh.files.wordpress.com/2014/10/confused.jpg',
            'http://www.milesspencer.com/wp-content/uploads/2016/06/106609519_Peter_Thi_328312c.jpg',
            'https://a2ua.com/aladdin/aladdin-006.jpg',
            'http://a1.files.biography.com/image/upload/c_fill,cs_srgb,dpr_1.0,g_face,h_300,q_80,w_300/MTIwNjA4NjM0MDQyNzQ2Mzgw.jpg',
            'http://assets1.ignimgs.com/2014/03/30/hulk02172014ca251jpg-e31f8e_1280w.jpg',
            'http://media.salon.com/2016/05/0910_peter-thiel-sept-2014_1024x576.jpg',
            'http://www.incimages.com/uploaded_files/image/1940x900/peter-thiel_41058.jpg',
            'http://thesource.com/wp-content/uploads/2015/07/hulk-hogan.jpg',
            'http://akns-images.eonline.com/eol_images/Entire_Site/2014715/rs_1024x759-140815130808-1024.Hulk-Hogan-WWE.ms.081514.jpg',
            'http://img.lum.dolimg.com/v1/images/character_aladdin_abu_bb8ccab2.jpeg',
            'http://assets.nydailynews.com/polopoly_fs/1.986006.1336765559!/img/httpImage/image.jpg_gen/derivatives/gallery_1200/hulk-hogan-pastamania.jpg',
            'https://upload.wikimedia.org/wikipedia/commons/0/0e/Hulk_Hogan.jpg',
            'https://lh5.googleusercontent.com/-brMpRz3odOU/AAAAAAAAAAI/AAAAAAAAA5g/4BYfjDWIBxA/s0-c-k-no-ns/photo.jpg',
            'https://timedotcom.files.wordpress.com/2016/04/white-walkers-game-of-thrones.jpg?quality=85',
            'https://cdn.vox-cdn.com/uploads/chorus_asset/file/8961503/got7.3dany.jpg',
            'https://timedotcom.files.wordpress.com/2017/06/jon-snow-fb.jpg?quality=85&w=560&h=374&crop=1',
            'http://assets1.ignimgs.com/2017/02/23/khal-drogo-jason-momoa-hints-at-game-of-thrones-return-1487815689758_1280w.jpg',
            'https://timedotcom.files.wordpress.com/2017/06/game-of-thrones-28.jpg?w=560',
            'https://lintvwish.files.wordpress.com/2014/11/ap090610040785.jpg?w=650',
            'https://saymber.files.wordpress.com/2017/05/andre-the-giant-princessbride.jpg',
            'http://media.oregonlive.com/ent_impact_tvfilm/photo/cary-elwes-0756e387e7e0c6f3.jpg',
            'https://media.npr.org/assets/img/2012/10/05/ap071120024261_wide-f107fa4be001708e47901f79c511bab297beb3a7.jpg?s=1400',
            'http://horrornews.net/wp-content/uploads/2016/09/Princess-Bride-photo-5.jpg'
        ]
    end

    def create_candidate(params)
        params = params.with_indifferent_access
        @sample_career_profile_params ||= User.find_by_email('lorenzoligato@icloud.com').career_profile.as_json

        # if multiple profiles reference the same one of these, we can never clean up
        @sample_career_profile_params.delete('resume_id')

        @candidate_index ||= -1
        @candidate_index += 1
        email = params['email'] || "test-conn-#{@candidate_index}@example.com"
        avatar_url = avatars.shift

        user = User.create!({
            email: email,
            uid: email,
            password: 'testtest',
            password_confirmation: 'testtest',
            sign_up_code: 'FREEMBA',
            avatar_url: avatar_url,
            phone: '+16015551337',
            tokens: @tokens[email],
            mba_enabled: true,
            fallback_program_type: 'mba'
        }.merge(params))
        user.can_edit_career_profile = true
        user.save!
        puts "#{user.id}: #{email}: #{params[:name]}"

        user.register_content_access
        career_profile = CareerProfile.update_from_hash!(@sample_career_profile_params.merge({
            id: user.career_profile.id,
            user_id: user.id,
            interested_in_joining_new_company: 'very_interested',
            personal_fact: 'I design sleek products, UI, and logos',
            place_details: {
                formatted_address: 'Washington, DC, USA'
            },
            personal_website_url: 'http://example.com'
        }.stringify_keys))
        user
    end

end