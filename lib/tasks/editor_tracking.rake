require File.expand_path('../../editor_tracking', __FILE__)

# Any versions that are created inside of rake tasks will get the name of the
# rake task as the editor_name
Rake::Task.set_callback :task_started, :after do
    begin
        EditorTracking.global_user = ["rake #{Rake.application.current_task}", EditorTracking.tty_ip_address].compact.join(' / ')
    rescue ActiveRecord::ConnectionNotEstablished => err
        # when rake test:environment runs, there is no connection yet, and this will
        # error.  We can safely ignore that.
    rescue ActiveRecord::NoDatabaseError => err
        # When running rtd, there is no database yet
    end
end