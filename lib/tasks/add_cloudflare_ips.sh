# To run, simply execute this script with no arguments.
# It will show errors if any of the IPs are already added to the groups, but continue on without stopping.
#
# What does this script do?
#
# 1. download the publicly available list of Cloudflare IPs (v4 and v6)
# 2. add each IP address to the staging and production Load Balancer security groups to explicitly allow access to Cloudflare proxy servers
#
# Why do we do want to do this?
#
# The default AWS security group config allows 0.0.0.0/0 to port 80 on the load balancer (the whole world).
# That means that if someone finds the IP address(es) of our load balancer, they could circumvent Cloudflare's proxy and hit us directly
# by making requests to the discovered IP addresses. For example, if they knew smartly-staging.elasticbeanstalk.com was our hostname:
#
#   $ host smartly-staging.elasticbeanstalk.com
#   smartly-staging.elasticbeanstalk.com has address 52.72.174.107
#   smartly-staging.elasticbeanstalk.com has address 34.237.230.197
#
#   $ curl 52.72.174.107
#   ...etc.
#
# To avoid this, we want to remove the default 0.0.0.0/0 rule from the load balancers' security groups and only allow requests to port 80
# for the whitelisted cloudflare IPs.

set -e

# first we download the list of IP ranges from CloudFlare
wget -P /tmp https://www.cloudflare.com/ips-v4
wget -P /tmp https://www.cloudflare.com/ips-v6

set +e


# This helper function adds a set of ingress rules based on the downloaded file data.
function setupLoadBalancer() {

    # iterate over the lines in the downloaded files
    # more details at http://docs.aws.amazon.com/cli/latest/reference/ec2/authorize-security-group-ingress.html
    #
    # The values passed to the --group-id arguments are the load balancer security groups

    echo ""
    echo "Setting up" $2 "load balancer security group..."
    echo " > IPv4..."
    while read p; do aws ec2 authorize-security-group-ingress --profile $1 --group-id $3 --protocol tcp --port 80 --cidr $p; done< /tmp/ips-v4
    echo " > IPv6..."
    while read p; do aws ec2 authorize-security-group-ingress --profile $1 --group-id $3 --ip-permissions IpProtocol=tcp,FromPort=80,ToPort=80,Ipv6Ranges=[{CidrIpv6=$p}]; done< /tmp/ips-v6
}

# Here are the actual Load Balancers we want to secure
setupLoadBalancer "tools" "metabase" "sg-03e1425e3c139b214"
setupLoadBalancer "staging" "staging" "sg-0eb9cc4b337b22a97"
setupLoadBalancer "production" "production" "sg-0876ba312df5dfa29"


echo ""
echo ""
echo ""
echo "================================================"
echo "                    ALL DONE!"
echo "================================================"
echo ""
echo "At this point, you probably want to go into the EC2 Security Groups and..."
echo ""
echo " 1. Make sure that there are a bunch of Inbound port 80 rules added to each group (normally, there's only a single, catch-all 0.0.0.0/0 rule)"
echo " 2. Edit the Inbound rules to remove the 0.0.0.0/0 port 80 rule (if it's still there; it's usually at the top). This will ensure that only Cloudflare servers can reach our Load Balancers."
echo " 3. Ensure that you have performed similar steps for the VPC security group's SSH access via add_bastion_ips.sh"