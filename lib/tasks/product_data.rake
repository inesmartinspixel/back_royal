namespace :product_data do

    message_csv_headers = [
        :time,
        :ts,
        :week,
        :user_id,
        :username,
        :email,
        :name,
        :cohort_id,
        :cohort_name,
        :type,
        :subtype,
        :platform,
        :channel,
        :text]

    # {
    #         "text": "yea, let\u2019s next time",
    #         "ts": "1489369764.923941",
    #         "subtype": "message",
    #         "user": "laviesak",
    #         "type": "message",
    #         "channel": "_general"
    #     },

    task :get_slack_data => :environment do
        user_mappings = get_user_mappings
        all_messages = []
        output_folder = "#{ENV['SLACK_HISTORY_PROJECT_LOCATION']}/channels"

        ActiveSupport::JSON.decode(ENV['SLACK_COHORT_TOKENS']).each do |cohort_name, token|
            next unless ['MBA5', 'EMBA1', 'MBA6'].include?(cohort_name)
            FileUtils.rm_rf(output_folder)
            messages = []
            missing_slack_names = Set.new

            puts "#{Time.now} copying slack messages for #{cohort_name}"
            cohort = Cohort.where("name = ?", cohort_name).first
            cmd  = "python #{ENV['SLACK_HISTORY_PROJECT_LOCATION']}/slack_history.py --token=\"#{token}\"   --skipDirectMessages --skipPrivateChannels"
            puts cmd
            Dir.chdir(ENV['SLACK_HISTORY_PROJECT_LOCATION']) do
                `#{cmd}`
            end

            json = File.read(File.join(output_folder, '_allchannels.json'))

            # ex. {"text"=>"I am hoping we do", "ts"=>"1488825716.000007", "subtype"=>"message", "user"=>"neetu", "type"=>"message", "channel"=>"_general"}
            ActiveSupport::JSON.decode(json)['messages'].each do |message|
                username = message['user']

                next if username.match(/no user id/)
                next if username.match(/slackbot/)

                next unless ['message', 'file_comment', 'file_share', 'file_mention', 'reply_broadcast'].include?(message['subtype'])

                next if username.match(/_smartly/)

                # if !user_mappings[:ids_by_slack_name][username] && !username.match(/smartly/)
                #     puts username
                # end

                user = user_mappings[:users_by_slack_name][username]
                if user.nil?
                    missing_slack_names << username
                end
                time = Time.at(message['ts'].to_f).utc
                messages << {
                    cohort_id: cohort.id,
                    cohort_name: cohort.name,
                    time: time,
                    week: ((time - cohort.start_date) / 1.week).ceil,
                    type: message['type'],
                    subtype: message['subtype'],
                    user_id: user && user.id,
                    channel: message['channel'],
                    platform: 'slack',
                    username: message['user'],
                    ts: message['ts'],
                    email: user && user.email,
                    name: user && user.name,
                    text: message['text'].gsub("\n", "<LINE BREAK>")
                }
            end

            if missing_slack_names.any?
                puts "WARNING::::: No #{cohort_name} users found for slack names: #{missing_slack_names.inspect}"
            end

            target_filepath = Rails.root.join('tmp', "slack-messages.#{cohort.name}.csv")
            CSV.open(target_filepath, "wb", {
                headers: message_csv_headers,
                write_headers: true
            }) do |csv|
                messages.each do |row|
                    csv << row
                end
            end

            all_messages += messages
            puts "Written to #{target_filepath}"
        end

        ActiveRecord::Base.connection.execute('drop table if exists slack_messages')
        ActiveRecord::Base.connection.execute(%Q~
            CREATE TABLE slack_messages(
               time timestamp,
               ts float,
               week int,
               user_id uuid,
               username text,
               email text,
               name text,
               cohort_id uuid,
               cohort_name text,
               type text,
               subtype text,
               platform text,
               channel text,
               text text
            );
        ~)
        inserts = []
        all_messages.each do |message|

            row = message_csv_headers.map do |col|
                val = message[col.to_sym]

                if val.nil?
                    val = 'null'
                elsif col == :time
                    val = "'#{val.strftime('%Y-%m-%d %H:%M:%S.%N')}'"
                elsif [:user_id, :username, :email, :name, :cohort_id, :cohort_name, :type, :subtype, :platform, :channel, :text].include?(col)
                    val = "$$#{val.gsub(/\$+/, '$').gsub(/\$$/, '$ ')}$$" # the gsub should be unnecessary, but I couldn't get the $SomeTag$ trick to work. see https://www.postgresql.org/docs/9.1/static/sql-syntax-lexical.html
                end
                val
            end

            inserts << "(#{row.join(', ')})"
        end
        insert = "INSERT INTO slack_messages VALUES #{inserts.join(',')}"
        ActiveRecord::Base.connection.execute(insert)

    end

    task :transform_facebook_data => :environment do

        s = SimpleSpreadsheet::Workbook.read(Rails.root.join('tmp', 'MBA1 Facebook Group - Data.xlsx'))
        user_mappings = get_user_mappings
        messages = []

        2.upto(20) do |i|
            s.selected_sheet = s.sheets[i]

            headers = []
            s.first_row.upto(s.last_row) do |line|
                if headers.empty?
                    headers << nil # correct for stupid 1-indexing in s.cell method
                    col = 1
                    while s.cell(line, col)
                        headers << s.cell(line, col).strip
                        col += 1
                    end
                else

                    date = s.cell(line, headers.index('Date'))
                    next unless date.is_a?(Date)
                    begin
                        time_str = s.cell(line, headers.index('Time'))
                        time = Time.parse("#{date.to_s} #{time_str} EST").utc
                        poster = s.cell(line, headers.index('Poster'))
                        post_id = SecureRandom.uuid
                    rescue Exception => err
                        debugger
                        1+1
                    end

                    messages << {
                        id: post_id,
                        platform: "facebook",
                        post_id: post_id,
                        time: time,
                        facebook_name: poster,
                        type: 'post',
                        subtype: 'post',
                        character_count: s.cell(line, headers.index('Character count')),
                        user_id: user_mappings[:ids_by_facebook_name].fetch(poster),
                        cohort_id: '8f9f77d1-069b-4ab4-a8b0-28d9d3ab29f2',
                        cohort_name: 'MBA1'
                    }

                    # there is one column for each person listing the
                    # number of comments
                    col = headers.index('Allison Harper')
                    while name = headers[col]
                        comment_count = s.cell(line, col) || 0

                        # for the person who posted, the count includes one
                        # for the post
                        comment_count -= 1 if name == poster

                        1.upto(comment_count) do
                            messages << {
                                id: SecureRandom.uuid,
                                platform: "facebook",
                                post_id: post_id,
                                time: time,
                                facebook_name: name,
                                type: 'comment',
                                subtype: 'comment',
                                user_id: user_mappings[:ids_by_facebook_name].fetch(name),
                                cohort_id: '8f9f77d1-069b-4ab4-a8b0-28d9d3ab29f2',
                                cohort_name: 'MBA1'
                            }
                        end
                        col += 1
                    end

                end

            end

        end

        target_filepath = Rails.root.join('tmp', 'mba1-facebook-messages.csv')
        CSV.open(target_filepath, "wb", {
            headers: message_csv_headers,
            write_headers: true
        }) do |csv|
            messages.each do |row|
                csv << row
            end
        end

        puts "Written to #{target_filepath}"
    end

    task :transform_mobilize_data => :environment do

        s = SimpleSpreadsheet::Workbook.read(Rails.root.join('tmp', 'MBA2 Final Participation Score - Data.xlsx.xlsx'))
        user_mappings = get_user_mappings

        messages = []

        s.selected_sheet = s.sheets[1]

        headers = []
        s.first_row.upto(s.last_row) do |line|
            if headers.empty?
                headers << nil # correct for stupid 1-indexing in s.cell method
                col = 1
                while s.cell(line, col)
                    headers << s.cell(line, col).strip
                    col += 1
                end
            else

                date = s.cell(line, headers.index('Date'))
                next unless date.is_a?(Date)

                # there is one column for each person listing the
                # number of comments
                col = headers.index('Allison Harper')
                while name = headers[col]
                    comment_count = s.cell(line, col) || 0

                    # unlike facebook, we don't subtract one for the poster, since
                    # we're not tracking the posts separately

                    1.upto(comment_count) do
                        messages << {
                            id: SecureRandom.uuid,
                            time: date,
                            mobilize_name: name,
                            type: 'comment', # there are posts here, but the spreadsheet does not make it easy to find out who it was
                            subtype: 'comment',
                            user_id: user_mappings[:ids_by_mobilize_name].fetch(name),
                            platform: "mobilize",
                            cohort_id: '1789b5f0-0436-4e65-b521-3c19f63a71db',
                            cohort_name: 'MBA2'
                        }
                    end
                    col += 1
                end

            end

        end

        target_filepath = Rails.root.join('tmp', 'mba2-mobilize-messages.csv')
        CSV.open(target_filepath, "wb", {
            headers: message_csv_headers,
            write_headers: true
        }) do |csv|
            messages.each do |row|
                csv << row
            end
        end

        puts "Written to #{target_filepath}"
    end

    def get_user_mappings
        s = SimpleSpreadsheet::Workbook.read(Rails.root.join('tmp', 'external user data.xlsx'))

        defaults = {
            'Robert J. Minger' => '87579f95-f665-4ea9-b71c-4c3b6542d7f3',
            'Robert Minger' => '87579f95-f665-4ea9-b71c-4c3b6542d7f3',
            'Count of Will Carius' => '991ade87-304f-4753-9867-9a5888a8b73a',
            'Will Carius' => '991ade87-304f-4753-9867-9a5888a8b73a',
            "Abi Scholz" => '8d94a2b7-abbd-4f6b-9454-e7fd80d169e1',
            "Abi Scholz2" => '8d94a2b7-abbd-4f6b-9454-e7fd80d169e1',
            "nate_brustein" => '56d332f4-467f-42b8-8630-5b39cfc51541',
            'motocross510' => 'f09b12ec-787e-4c8c-913b-811e4a676c6e',
            'ben_dressner' => 'f09b12ec-787e-4c8c-913b-811e4a676c6e',
            'alejandrariveraflavia' => 'a69acd6f-a6d5-4b71-9711-def39b4b7e33',
            'garrett_welson' => '65912f98-c19e-4949-9b38-3f7466245244',
            'garrettwelson' => '65912f98-c19e-4949-9b38-3f7466245244',
            'daniel_lee' => '30693160-4009-45a1-af9d-88bf62d2df72',
            'd_lee' => '30693160-4009-45a1-af9d-88bf62d2df72'


        }
        User.where("email like ?", "%pedago.com%").select('name', 'id').each do |user|
            defaults[user.name] = user.id
        end

        mappings = {
            ids_by_mobilize_name: defaults,
            ids_by_facebook_name: defaults,
            ids_by_slack_name: defaults
        }

        # MASTER MBA List Total, MBA2 weekly interactions
        [0,2].each do |sheet_index|
            s.selected_sheet = s.sheets[sheet_index]

            headers = []
            s.first_row.upto(s.last_row) do |line|
                if headers.empty?
                    headers << nil # correct for stupid 1-indexing in s.cell method
                    col = 1
                    while s.cell(line, col)
                        headers << s.cell(line, col).strip
                        col += 1
                    end
                else

                    user_id = s.cell(line, headers.index('user_id'))
                    next unless user_id.looks_like_uuid?
                    begin
                        if facebook_name = s.cell(line, headers.index('Facebook name'))
                            mappings[:ids_by_facebook_name][facebook_name] = user_id
                        end
                        if mobilize_name = s.cell(line, headers.index('Moblize Name'))
                            mappings[:ids_by_mobilize_name][mobilize_name] = user_id
                        end
                        if slack_name = s.cell(line, headers.index('Slack username'))
                            mappings[:ids_by_slack_name][slack_name] = user_id
                        end
                    rescue Exception => err
                        debugger
                        1+1
                    end

                end
            end
        end

        user_ids = mappings.values.map(&:values).flatten.compact
        users = {}
        User.select('id', 'email', 'name').find(user_ids).each do |user|
            users[user.id] = user
        end
        mappings.keys.each do |key|
            hash = mappings[key]
            new_key = key.to_s.gsub("ids_", "users_").to_sym
            mappings[new_key] = {}
            hash.each do |name, id|
                mappings[new_key][name] = users[id]
            end
        end

        mappings
    end

end