namespace :accreditation do

    def normalize(url)
        Addressable::URI.parse(url).normalize
    end

    def add(csv, known_links, type, stream_title, lesson_title, title, url = nil, avg_hours = '', median_hours = '', max_hours = '')
        # skip known links or links to other smartly courses
        if !url.nil? && (known_links[normalize(url)] || url.include?('//smart.ly/course/'))
            puts "SKIPPING: #{type} :: #{url}"
            return
        end

        puts "ADDING: #{type} :: #{url}"

        csv << [
            type,
            stream_title,
            lesson_title,
            title,
            url.nil? ? 'n/a' : url,
            avg_hours,
            median_hours,
            max_hours
        ]

        if !url.nil?
            known_links[normalize(url)] = true
        end
    end

    def add_streams(csv, known_links, known_streams, streams, required_stream_locale_pack_ids, time_summaries)
        streams.each do |stream|

            if known_streams[stream.locale_pack_id]
                puts "SKIPPING #{stream.title}"
                next
            end

            puts stream.title

            time_summary = time_summaries[stream.locale_pack_id]
            total_avg_hrs = time_summary ? time_summary['total_avg_hrs'] : ''
            total_median_hrs = time_summary ? time_summary['total_median_hrs'] : ''
            total_max_hrs = time_summary ? time_summary['total_max_hrs'] : ''

            add(csv,
                known_links,
                'course_' + (required_stream_locale_pack_ids.include?(stream.locale_pack_id) ? 'required' : 'optional'),
                stream.title,
                'n/a',
                'n/a',
                nil,
                total_avg_hrs,
                total_median_hrs,
                total_max_hrs)

            # course summaries
            stream.summaries.each do |summary|
                add(csv,
                    known_links,
                    'course_summary',
                    stream.title,
                    'n/a',
                    summary['title'],
                    summary['url'])
            end

            # additional resource:link
            stream.resource_links.each do |resource_link|
                add(csv,
                    known_links,
                    'additional_resource:link',
                    stream.title,
                    'n/a',
                    resource_link['title'],
                    resource_link['url'])
            end

            # additional resource:download
            stream.resource_downloads.each do |resource_download|
                add(csv,
                    known_links,
                    'additional_resource:download',
                    stream.title,
                    'n/a',
                    resource_download['title'],
                    resource_download['url'])
            end

            # lesson links
            stream.lessons.each do |lesson|
                lesson.content.frames.each do |frame|
                    # text content: look for external links
                    frame.content_links.each do |url|
                        add(csv,
                            known_links,
                            'lesson:link',
                            stream.title,
                            lesson.title,
                            url, # not using the text, since it's not useful usually
                            url)
                    end
                end
            end

            known_streams[stream.locale_pack_id] = true
        end
    end

    def get_stream_time_summaries(cohort_name)
        summaries = ActiveRecord::Base.connection.execute(%Q~
            with times AS MATERIALIZED (
                select
                    lessons.title as lesson_title,
                    s.title as stream_title,
                    lessons.id as lesson_id,
                    s.locale_pack_id as stream_locale_pack_id,
                    count(*) as number_of_completions,
                    avg(total_lesson_time)/60.0 as avg_minutes,
                    percentile_disc(0.50) WITHIN GROUP (ORDER BY total_lesson_time)/60.0 as median_minutes,
                    min(total_lesson_time)/60.0 as min_minutes,
                    max(total_lesson_time)/60.0 as max_minutes,
                    percentile_disc(0.95) WITHIN GROUP (ORDER BY total_lesson_time)/60.0 as p95_minutes,
                    percentile_disc(0.99) WITHIN GROUP (ORDER BY total_lesson_time)/60.0 as p99_minutes,
                    (array_agg(u.email order by total_lesson_time desc))[1] as max_user
                from user_lesson_progress_records lpr
                    join cohort_applications on cohort_applications.user_id = lpr.user_id
                    join cohorts on cohort_applications.cohort_id = cohorts.id
                    join lessons on lpr.locale_pack_id = lessons.locale_pack_id and lessons.locale = 'en'
                    join lesson_to_stream_joins ltsj on ltsj.lesson_id = lessons.id
                    join lesson_streams s on s.id = ltsj.stream_id
                    join published_cohort_stream_locale_packs pcslp on pcslp.stream_locale_pack_id = s.locale_pack_id
                    join users u on u.id = lpr.user_id
                where pcslp.cohort_name = '#{cohort_name}'
                    and cohort_applications.status = 'accepted'
                    and lpr.completed_at is not null
                    and lpr.total_lesson_time >= 0
                    and u.email not like '%pedago.com'
                    and u.email not like '%smart.ly'
                    and u.email != 'tiffanytychen@gmail.com'

                group by lessons.title, lessons.id, s.title, s.locale_pack_id
            )

            select stream_title
                , count(*) as num_lessons
                , stream_locale_pack_id as locale_pack_id
                , round(10*sum(avg_minutes)/60.0)/10.0 as total_avg_hrs
                , round(10*sum(median_minutes)/60.0)/10.0 as total_median_hrs
                , round(10*sum(p95_minutes)/60.0)/10.0 as p95_hours
                , round(10*sum(p99_minutes)/60.0)/10.0 as p99_hours
                , round(10*sum(max_minutes)/60.0)/10.0 as total_max_hrs
            from times slt
            group by stream_title, stream_locale_pack_id
            order by stream_title asc;
        ~)

        summaries.index_by {|s| s['locale_pack_id'] }
    end

    desc "Get external links for all content within a cohort"
    task :external_links => :environment do

        require "addressable/uri"

        # get all available courses for this cohort
        c = ENV['cohort']
        cohort = Cohort.find_by_name(c)
        if cohort.nil?
            raise "Must provide cohort name, e.g.: cohort=MBA12"
        end

        required_stream_locale_pack_ids = cohort.get_required_stream_locale_pack_ids
        optional_stream_locale_pack_ids = cohort.get_optional_stream_locale_pack_ids - required_stream_locale_pack_ids
        time_summaries = get_stream_time_summaries(cohort.name)

        all_locale_pack_ids = [required_stream_locale_pack_ids, optional_stream_locale_pack_ids].flatten.uniq

        stream_hash = Lesson::Stream.all_published.
            where(locale_pack_id: all_locale_pack_ids, locale: 'en').
            includes(:entity_metadata).
            index_by(&:locale_pack_id)

        known_links = {}
        known_streams = {}

        filepath = File.join('tmp', "#{cohort.name}_links.csv")

        puts "Writing out external link data..."

        CSV.open(filepath, "wb") do |csv|

            csv << [
                'type',
                'course_title',
                'lesson_title',
                'resource_name',
                'resource_url',
                'avg_hours',
                'median_hours',
                'max_hours'
            ]

            puts "Adding Periods..."

            # The `as_json` is necessary here to get the project_documents
            # into the period data
            cohort.as_json['periods'].each_with_index do |period, index|

                puts "Period #{index}..."

                # streams
                streams = period['stream_entries'].map { |entry| stream_hash[entry['locale_pack_id']] }.sort_by do |stream|
                    stream.title
                end

                add_streams(csv, known_links, known_streams, streams, required_stream_locale_pack_ids, time_summaries)

                 # exercises
                period["exercises"]&.each do |exercise|
                    add(csv,
                        known_links,
                        'exercise',
                        'n/a',
                        'n/a',
                        exercise['message'])
                end

                # projects
                LearnerProject.where(id: period['learner_project_ids']).each do |project|
                    project.project_documents.each do |project_document|
                        add(csv,
                            known_links,
                            'project_document',
                            'n/a',
                            'n/a',
                            project_document['title'],
                            project_document['url'])
                    end
                end
            end

            puts "Adding Electives and Specialization Streams..."

            streams = all_locale_pack_ids.map { |locale_pack_id| stream_hash[locale_pack_id] }.sort_by do |stream|
                stream.title
            end

            add_streams(csv, known_links, known_streams, streams, required_stream_locale_pack_ids, time_summaries)
        end

    end

end