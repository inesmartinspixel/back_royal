namespace :hybrid do

    desc "Syncs with a specific commit (defaults to production commit)"
    task :sync do
        sync
    end

    desc "Stamps for Cordova bundling"
    task :stamp do
        stamp
    end


    desc "Syncs with a specific commit and stamps for Cordova bundling"
    task :sync_and_stamp do
        sync
        stamp
    end

end


require 'open-uri'
require 'open3'
require 'active_support/core_ext/object/blank'

def stamp

    production_info = get_production_build_info

    file_names = ['../../vendor/front-royal/modules/BuildConfig/BuildConfig.js', '../../hybrid/config.xml']
    
    file_names.each do |file_name|
        file_path = File.expand_path(file_name, File.dirname(__FILE__))

        puts "Updating #{file_path} ..."

        text = File.read(file_path)

        if file_name == file_names[0]
            text = text.gsub(/APPLICATION_BUILD_NUMBER_CI_STAMPED/, production_info[:build_number])
            text = text.gsub(/APPLICATION_BUILD_TIMESTAMP_CI_STAMPED/, production_info[:build_time])
            text = text.gsub(/APPLICATION_COMMIT_CI_STAMPED/, production_info[:build_commit])
        elsif file_name == file_names[1]
            # NOTE: for android, we're locked into an ugly versioning strategy, but
            # no reason to require iOS to adhere to that. currently, the build #'s in
            # android are getting architecture IDs suffixed (FU cordova), so we can't keep
            # them consistent anyhow
            text = text.gsub(/ios-CFBundleVersion=\"(\d+)\"/, "ios-CFBundleVersion=\"#{production_info[:build_number]}\"")
            text = text.gsub(/android-versionCode=\"(\d+)\"/, "android-versionCode=\"\\1#{production_info[:build_number]}\"")
        end

        File.open(file_path, "w") { |file| file.puts text }
    end


    puts %{

    ,------------------------------------------------------------------------------,
    |                                                                              |
    |  !!! DO NOT COMMIT ANY CHANGES BUILT AS A RESULT OF RUNNING THIS SCRIPT !!!  |
    |                                                                              |
    `------------------------------------------------------------------------------'

    }

end

def sync
    production_info = get_production_build_info
    puts "Checking out revision #{production_info[:build_commit]} ..."
    Run.new("git checkout #{production_info[:build_commit]}", {
        :ignore => "You are in 'detached HEAD' state"
    })
end

def get_production_build_info
    production_info = parse_config 'https://smart.ly/vendor/front-royal/modules/BuildConfig/BuildConfig.js'
    {
        build_commit: production_info[2],
        build_number: production_info[0],
        build_time: production_info[1]
    }
end

def parse_config(url)
    config = read_url(url)
    # if build_config ever changes order, etc, this will have to change
    config.scan(/^.*:\s*'(.*)'/i).flatten
end

def read_url(url)
    page_string = ''
    open(url) do |f|
        page_string = f.read
    end
    page_string
end

class Run
    attr_reader :out, :err

    def initialize(cmd, options = {}, &block)
        @options = {
            ignore: [],
            verbose: false
        }.merge(options)
        @options[:ignore] = [@options[:ignore]] if @options[:ignore].is_a?(String)
        @cmd = cmd
        if block_given?
            yield self
        end
        run
    end

    def run
        puts "Running `#{@cmd}`" if @options[:verbose]
        stdin, stdout, stderr = Open3.popen3(@cmd)
        @out = stdout.read
        @err = stderr.read
        if !@err.blank? && !ignore_error(@err) && !call_error_callback(@err)
            raise "Error occurred while running `#{@cmd}`: #{@err.inspect}"
        end
        if @options[:verbose]
            puts "-- STDOUT:"
            puts @out
            unless @err.blank?
                puts "\n\n-- STDERR:"
                puts @err
            end
        end
        @out
    end

    def on_error(message, &block)
        on_error_callbacks[message] << block
    end

    def call_error_callback(err)
        found_callback = false
        on_error_callbacks.each do |matcher, callbacks|
            if err.match(/#{matcher}/)
                callbacks.each do |callback|
                    callback.call
                    found_callback = true
                end
            end
        end
        return found_callback
    end

    def on_error_callbacks
        @on_error_callbacks ||= Hash.new { |hash, key| hash[key] = [] }
    end

    def ignore_error(err)
        @options[:ignore].each do |message_to_ignore|
            if err.match(/#{message_to_ignore}/)
                return true
            end
        end
        false
    end


end