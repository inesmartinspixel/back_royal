namespace :db do

    desc "db:migrate, db:test:prepare, and annotate"
    task :migrate_plus => [:"db:migrate", :"db:test:prepare"] do
        Dir.chdir(Rails.root) do
            `annotate`
        end
    end

    desc "mv migration files to migrate_old and rebuild structure.sql from the test db"
    task :flatten_migrations do
        Dir.chdir(Rails.root) do
            month = Time.parse('2012/04/01')

            # it's nice to have a few recent months in the main directory, both
            # just for reference and also if you're trying to migrate a dump that is
            # a little bit old after doing copy2to1 or whatever
            while month < Time.now - 4.months
                cmd = "mv db/migrate/#{month.strftime('%Y%m')}*.rb db/migrate_old/"
                puts cmd
                month = month + 1.month
                `#{cmd}`
            end

            # determine if we need to use GNU sed (gsed) variant in OSX
            sed_command = 'sed'
            begin
                Open3.popen3('gsed') do |stdin, stdout, stderr, thread|
                    sed_command = 'gsed'
                end
            rescue Errno::ENOENT
                # no-op
            end

            # -s -x -O are the same parameters passed to db:structure:dump. replace user creation.
            `pg_dump -s -x -O back_royal_test | #{sed_command} -e '/Name: USER MAPPING/,+6d' > structure.sql`

        end
    end

    desc "create a readonly user"
    task :create_readonly_user => :environment do
        username = ENV['u']
        password = ENV['p']
        ActiveRecord::Base.connection.execute(%Q~
            CREATE ROLE #{username} LOGIN PASSWORD '#{password}';

            GRANT SELECT ON ALL TABLES IN SCHEMA public TO #{username};
            ALTER DEFAULT PRIVILEGES IN SCHEMA public
                GRANT SELECT ON TABLES TO #{username};
        ~)
    end

    desc "refresh materialized content views"
    task :refresh_materialized_content_views => :environment do
        RefreshMaterializedContentViews.refresh
    end

    desc "refresh all materialized views"
    task :refresh_materialized_views => :environment do
        Report::RefreshInternalContentTitles.perform_now(true)
        RefreshMaterializedContentViews.refresh(true)
        WriteRegularlyIncrementallyUpdatedTablesJob.perform_now

        # This runs from rake test:build_fixtures.  Since we never use the mdp predictor
        # in tests, we just hack this up to skip that
        RefreshMaterializedInternalReportsViews.perform_now(skip_mdp_success_predictor: Rails.env.test?)
    end

    desc "refresh only unpopulated views.  Useful after running a migration that updates some views"
    task :ensure_all_materialized_views_populated => :environment do
        DbLinker.setup_red_royal_dblink
        ViewHelpers.ensure_all_populated
    end

    # eg - nohup runuser -u webapp bundle exec rake db:rebuild_db_from_source source="smartly-staging.cgueubjoqruk.us-east-1.rds.amazonaws.com" target="smartly-staging-manual-restore.csjz83rbdbqn.us-east-1.rds.amazonaws.com" > /var/log/staging_manual_rebuild 2>&1
    desc "task to rebuild back_royal given a source and target host"
    task :rebuild_db_from_source => :environment do

        start_time = Time.now
        source = ENV['source']
        target = ENV['target']
        dump_opts = ENV['dump_opts'] || ""

        # handle conditional environmental settings
        source_database_name = target_database_name = "back_royal"

        source_user_params = "-U readonly"
        source_password_params = "PGPASSWORD=\"#{ENV['RDS_READONLY_PASSWORD']}\""

        target_user_params = "-U ebroot"
        target_password_params = "PGPASSWORD=\"#{ENV['RDS_PASSWORD']}\""

        target_host_params = "-h #{target}"
        user_info = {"readonly" => ENV['RDS_READONLY_PASSWORD']}
        if target == "localhost"
            target_database_name = "back_royal_development2"
            target_password_params = ""
            target_user_params = ""
            target_host_params = ""
            user_info = {"readonly" => ""}
        end

        # handle optional persistence
        if File.directory?(File.expand_path('~/Downloads/dumps/'))
            backup_path = File.expand_path('~/Downloads/dumps/back_royal_production.dir')
        else
            backup_path = File.expand_path('~/back_royal_production.dir')
        end

        # allows us to break up this task
        copy_only = ENV['copy_only'] == 'true'
        restore_only = ENV['restore_only'] == 'true'

        if (source.blank? && !restore_only) || (target.blank? && !copy_only) || (ENV['RDS_PASSWORD'].blank? && target != "localhost") || ENV['RDS_READONLY_PASSWORD'].nil?
            raise "Must provide a source and target database hostname. A viable password and readonly password must also be accessible."
        end


        Dir.mktmpdir do |tmpdir|

            puts "Beginning Import\n\n"


            # We cannot rely on pg_dumpall at all since we don't have superuser access in RDS, which is required for data operations ...
            # So instead, we have to manually rebuild all extensions / users / roles / access grants / etc

            if !copy_only

                if target == "localhost"
                    ActiveRecord::Base.remove_connection
                end

                # rebuild databases
                cmd = %Q[#{target_password_params} dropdb -h #{target} #{target_user_params} #{target_database_name}]
                begin
                    run_cmd_with_error_handling(cmd, "Dropping #{target_database_name} on target")
                rescue Exception => err
                    raise err unless err.message.match("does not exist")
                end

                cmd = %Q[#{target_password_params} createdb -h #{target} #{target_user_params} #{target_database_name}]
                run_cmd_with_error_handling(cmd, "Creating #{target_database_name} on target")


                # additional user creation
                user_info.each do |user, user_password|
                    cmd = get_statement_command(target_host_params, target_database_name, target_user_params, target_password_params, "DROP ROLE IF EXISTS #{user}")
                    run_cmd_with_error_handling(cmd, "Dropping user #{user} if exists", target != "localhost")

                    cmd = get_statement_command(target_host_params, target_database_name, target_user_params, target_password_params, "CREATE ROLE #{user} WITH PASSWORD '#{user_password}' LOGIN")
                    run_cmd_with_error_handling(cmd, "Creating user #{user}", target != "localhost")
                end

                # installing extensions
                cmd = get_statement_command(target_host_params, target_database_name, target_user_params, target_password_params, 'CREATE EXTENSION IF NOT EXISTS pg_trgm')
                run_cmd_with_error_handling(cmd, "Installing pg_trgm extension")

                cmd = get_statement_command(target_host_params, target_database_name, target_user_params, target_password_params, 'CREATE EXTENSION IF NOT EXISTS \"uuid-ossp\"')
                run_cmd_with_error_handling(cmd, "Installing uuid-ossp extension")

                cmd = get_statement_command(target_host_params, target_database_name, target_user_params, target_password_params, 'CREATE EXTENSION IF NOT EXISTS dblink')
                run_cmd_with_error_handling(cmd, "Installing dblink extension")

                cmd = get_statement_command(target_host_params, target_database_name, target_user_params, target_password_params, 'CREATE EXTENSION IF NOT EXISTS tablefunc')
                run_cmd_with_error_handling(cmd, "Installing tablefunc extension")

                cmd = get_statement_command(target_host_params, target_database_name, target_user_params, target_password_params, 'CREATE EXTENSION IF NOT EXISTS pg_prewarm')
                run_cmd_with_error_handling(cmd, "Installing pg_prewarm extension")

            end

            # determine appropriate file path
            output_path = File.join(tmpdir, 'pg_dump_databases.dat')
            if copy_only || restore_only
                output_path = backup_path
            end

            # dump all database schema / data
            if !restore_only
                cmd = "#{source_password_params} pg_dump -Fd -j 8 -f #{output_path} -h #{source} #{source_user_params} #{dump_opts} -v #{source_database_name}"
                run_cmd_with_error_handling(cmd, "Dumping databases to #{output_path}")
            end


            # Restore process
            if !copy_only

                # import data
                # -- not much we can do about COMMENT(s) -- http://stackoverflow.com/questions/17027611/pg-dump-without-comments-on-objects
                # -- see also: https://www.postgresql.org/message-id/11166.1424357659%40sss.pgh.pa.us
                cmd = "#{target_password_params} pg_restore -Fd -h #{target} #{target_user_params} -d #{target_database_name} -j 8 -v #{output_path}"
                run_cmd_with_error_handling(cmd, "Restoring databases", false)


                # additional user access
                user_info.each do |user, user_password|

                    cmd = get_statement_command(target_host_params, target_database_name, target_user_params, target_password_params, "GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA public TO #{user}")
                    run_cmd_with_error_handling(cmd, "Granting usage to #{user} on public")

                    cmd = get_statement_command(target_host_params, target_database_name, target_user_params, target_password_params, "GRANT SELECT ON ALL TABLES IN SCHEMA public TO #{user}")
                    run_cmd_with_error_handling(cmd, "Granting select access to #{user} on public")

                    cmd = get_statement_command(target_host_params, target_database_name, target_user_params, target_password_params, "ALTER DEFAULT PRIVILEGES #{target == 'localhost' ? '' : 'FOR ROLE ebroot' } IN SCHEMA public GRANT SELECT ON TABLES TO #{user}")
                    run_cmd_with_error_handling(cmd, "Altering default privileges for ebroot on public to user #{user}")

                end

                puts "\t---------------------------------------------------------------------------------------------------"
                puts "\t Run `rake db:refresh_materialized_views` and `DbLinker.setup_red_royal_dblink` (in a console)"
                puts "\t once the new TARGET instance has been pointed to in Elastic Beanstalk config."
                puts "\t This will setup the FDW and populate `pg_foreign_server` and `pg_user_mapping` entries!"
                puts "\t---------------------------------------------------------------------------------------------------"
                puts "\n"
                puts "\t---------------------------------------------------------------------------------------------------"
                puts "\t Additionally, you may wish to run `rake db:prewarm` to prewarm IO cache if not autoloaded."
                puts "\t---------------------------------------------------------------------------------------------------"


            end

            elapsed = ((Time.now - start_time) / 60).ceil
            puts "Completed #{copy_only ? 'copy' : 'restore'} in #{elapsed} minutes"

        end

    end

    desc "Sets up a dblink from the current ActiveRecord::Base user to the current redshift connection, using the server name 'red_royal'"
    task :setup_dblink => :environment do
        server_name = ENV['s']
        raise "please provide server_name" if server_name.blank?
        DbLinker.setup_dblink(server_name, nil, true)
    end

    desc "Sets up a dblink from the readonly user to the staging readonly redshift connection, using the server name 'red_royal_readonly'"
    task :setup_readonly_dblink => :environment do
        env = ENV['env']
        raise "env should be 'production' or 'staging'" unless ['production', 'staging'].include?(env)

        if env == 'production'
            base_env = 'prod_writable_from_local'
        else
            base_env = env
        end

        base_connection_configs = YAML::load(ERB.new(File.read(Rails.root.join("config","database.yml"))).result)
        ActiveRecord::Base.establish_connection(base_connection_configs[base_env])
        RedshiftEvent.establish_connection(REDSHIFT_CONNECTION_CONFIGS["redshift_#{env}_readonly"])

        DbLinker.setup_dblink('red_royal_readonly', ['readonly'], true)
    end

    desc "Drops a dblink from the current ActiveRecord::Base connection"
    task :drop_dblink => :environment do
        server_name = ENV['s']
        raise "please provide server_name" if server_name.blank?
        DbLinker.drop_dblink(server_name, true)
    end

    desc "Rebuilds the pg_analyze schema + user"
    task :rebuild_pganalyze_schema => :environment do
        ActiveRecord::Base.connection.execute "
            DROP SCHEMA pganalyze CASCADE;

            DROP USER IF EXISTS pganalyze;

            DROP EXTENSION pg_stat_statements;

            CREATE USER pganalyze WITH PASSWORD '#{ENV['PG_ANALYZE_PASSWORD']}' CONNECTION LIMIT 5;
            GRANT pg_monitor TO pganalyze;

            CREATE SCHEMA pganalyze;
            GRANT USAGE ON SCHEMA pganalyze TO pganalyze;
            REVOKE ALL ON SCHEMA public FROM pganalyze;

            CREATE EXTENSION IF NOT EXISTS pg_stat_statements;

            CREATE OR REPLACE FUNCTION pganalyze.get_stat_replication() RETURNS SETOF pg_stat_replication AS
            $$
              /* pganalyze-collector */ SELECT * FROM pg_catalog.pg_stat_replication;
            $$ LANGUAGE sql VOLATILE SECURITY DEFINER;
        "
    end

    desc "Pre-warms select tables after an event such as a reboot upgrade"
    task :prewarm => :environment do
        tables = %w(
            career_profiles_skills_options
            career_profiles_skills_options
            career_profiles
            cohort_applications_versions
            cohort_applications
            events
            lesson_progress
            lesson_streams
            lessons
            player_lesson_sessions
            player_lesson_sessions
            professional_organization_options
            user_lesson_progress_records
            users
        )
        ActiveRecord::Base.with_statement_timeout(0) do
            tables.each do |table|
                puts "Warming #{table} ..."
                ActiveRecord::Base.connection.execute "SELECT pg_prewarm ('#{table}')"
            end

        end
    end

    Rake::Task['db:migrate'].enhance do
        if ActiveRecord::Base.connection_config[:bump_content_views_refresh]
            ActiveRecord::Base.connection.execute "UPDATE content_views_refresh SET updated_at = NOW()"
        end
    end

    def get_statement_command(host_params, database_name, user_params, password_params, statement)
         %Q[#{password_params} psql #{host_params} #{user_params} -c "#{statement}" #{database_name}]
    end

    def run_cmd_with_error_handling(cmd, msg, raise_on_error = true)
        print "\n#{msg} ... " unless msg.blank?
        Open3.popen3(cmd) do |stdin, stdout, stderr, wait_thread|
            err_lines = []
            while line = stderr.gets
                err_lines << line
                puts line
            end
            if raise_on_error && !wait_thread.value.success?
                err = err_lines.join("\n")
                raise RuntimeError.new(err)
            end
        end
        puts "DONE!\n\n" unless msg.blank?
    end

end