require 'stripe'

module Stripe

    # This handles batch loading all of the records of a particular
    # type over the stripe api
    def self.find_each(klass, &block)
        last_id = nil
        has_more = true
        while has_more
            options = {limit: 100}
            if last_id
                options[:starting_after] =  last_id
            end
            list = klass.list(options)
            list.data.each do |object|
                yield object
            end
            has_more = list.has_more
            last_id = list.data.last.id
        end
    end

    [Subscription, Invoice, Charge, Refund].each do |klass|
        klass.send(:define_singleton_method, :find_each) do |&block|
            Stripe.find_each(klass, &block)
        end
    end
end