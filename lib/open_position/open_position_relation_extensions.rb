module OpenPosition::OpenPositionRelationExtensions

    # Note: This mimics how the client hits the index endpoint to get
    # recommended positions for the all tab or welcome UI
    def recommended_and_viewable(candidate_or_id:)
        candidate = get_candidate(candidate_or_id)
        self.featured_and_not_archived
            .viewable_when_considering_relationships_and_interests(candidate_or_id: candidate, candidate_has_acted_on: false)
            .recommended(candidate_or_id: candidate)
            .order_by_recommended(candidate_or_id: candidate)
    end

    def featured_and_not_archived
        self.where(featured: true, archived: false)
    end

    # Sometimes we need to filter out positions that have certain
    # interest activity with the candidate. This method will filter out
    # positions for which the owning hiring manager
    # has accepted or rejected the candidate, or the owning hiring manager's
    # hiring team has accepted the candidate (but not rejected), provided that
    # the candidate has not accepted the position already. This allows us to hide
    # positions from the candidate when a hiring manager or hiring team from those
    # positions have already engaged with the candidate, but we don't want to make
    # positions dissappear if the candidate was the first to accept (they go to the
    # closed tab in the featured positions UI, for example).
    #
    # Additionaly, you can use the candidate_has_acted_on flag to filter out
    # positions that the candidate has already acted on. That flag is used in conjunction
    # with the former logic for things like showing positions on the all tab in
    # featured positions UI or determining recommended positions.
    def viewable_when_considering_relationships_and_interests(candidate_or_id:, candidate_has_acted_on: nil)
        candidate = get_candidate(candidate_or_id)

        hiring_teams_who_have_liked_this_candidate = HiringRelationship.joins(:hiring_manager)
            .where(candidate_id: candidate.id, hiring_manager_status: 'accepted')
            .where.not(users: {hiring_team_id: nil})
            .select(:hiring_team_id)

        hiring_managers_who_have_acted_on_this_candidate = HiringRelationship.where(candidate_id: candidate.id, hiring_manager_status: ['accepted', 'rejected'])
                                                            .select(:hiring_manager_id)

        query = self.joins("join users hiring_managers on hiring_managers.id = hiring_manager_id")
            .joins(%Q~
                LEFT OUTER JOIN candidate_position_interests AS interests_for_candidate ON
                    interests_for_candidate.open_position_id = open_positions.id AND
                    interests_for_candidate.candidate_id = '#{candidate.id}'
            ~)
            .where("interests_for_candidate.candidate_status = 'accepted' OR hiring_managers.id NOT IN (#{hiring_managers_who_have_acted_on_this_candidate.to_sql})")
            .where("interests_for_candidate.candidate_status = 'accepted' OR hiring_managers.hiring_team_id NOT IN (#{hiring_teams_who_have_liked_this_candidate.to_sql})")

        if !candidate_has_acted_on.nil?
            query = query.where("interests_for_candidate.id IS NOT NULL = ?", candidate_has_acted_on)
        end

        query
    end

    def recommended(candidate_or_id:)
        candidate = get_candidate(candidate_or_id)
        self.joins("join users hiring_managers on hiring_managers.id = hiring_manager_id")
            .joins("join hiring_applications on hiring_applications.user_id = hiring_managers.id")
            .where("#{self.is_recommended_sql(candidate.career_profile)} = TRUE")
    end

    # When sending notification events to customer.io we'd like to know which positions a
    # candidate has been notified for, which we track in a table called notified_recommended_open_positions
    def not_notified(candidate_or_id:)
        candidate = get_candidate(candidate_or_id)
        self.joins(%Q~
            LEFT OUTER JOIN notified_recommended_open_positions ON
                notified_recommended_open_positions.user_id = '#{candidate.id}'
                AND
                notified_recommended_open_positions.open_position_id = open_positions.id
        ~)
        .where("notified_recommended_open_positions.id IS NULL")
    end

    # Note that you have to really think about the effects of empty or null with a lot of these orders.
    # For example, `Array['foo'] && null` will produce null so if you use `NULLS LAST` you will
    # cause rows with no value to be ordered below rows with no matching value since nulls will be ordered below
    # the false returned from the `&&` comparison. But if you do not use 'NULLS LAST' the null values will be ordered
    # first. The solution is to make sure you COALESCE.
    # Another example is if you are constructing an array as `Array[]` (nothing inside of it) you will get
    # an error unless you typecast to `Array[]::text[]`.
    # Also note that `Array[null] && Array[null]` is false, as is `Array[]::text[] && Array[]::text[]`.
    # Also note that ST_DWithin, used in the location clauses, can return null if the input is null.
    def order_by_recommended(candidate_or_id:)
        candidate = get_candidate(candidate_or_id)
        career_profile = candidate.career_profile
        search_helper = career_profile.career_profile_search_helper

        query = self.joins("join users hiring_managers on hiring_managers.id = hiring_manager_id")
            .joins("join hiring_applications on hiring_applications.user_id = hiring_managers.id")

        # Note: The first four orders match the criteria used when filtering
        # for recommended. This is important because it wouldn't make sense for
        # a non-recommended position to be ordered above a recommended position,
        # which could happen if a criteria not used in filtering was placed higher
        # in precedence than any of the criteria that are used for filtering.

        # Order by the position_descriptors matching what the candidate is looking for
        if career_profile.employment_types_of_interest.present?
            query = query.order(Arel.sql("#{self.recommended_descriptor_sql(career_profile)} DESC"))
        end

        # Order by open_positions whose role matches the candidate's primary_areas_of_interest
        if career_profile.primary_areas_of_interest.present?
            query = query.order(Arel.sql("#{self.recommended_role_sql(career_profile)} DESC"))
        end

        # Order by open_positions whose location is close to the candidate's location or
        # whose location is close to one of the candidate's location_of_interest keys.
        query = query.order(Arel.sql("COALESCE(#{self.recommended_location_sql(career_profile)}, false) DESC"))

        # Order by if the candidate has the proper years_of_experience for the position.
        # If the range matches we get true, so the direction should be DESC.
        query = query.order(Arel.sql("#{self.recommended_years_of_experience_sql(search_helper)} DESC"))

        # Order by the role of any work experience
        if search_helper&.work_experience_roles.present?
            query = query.order(Arel.sql("#{search_helper.work_experience_roles.to_sql} && ARRAY[open_positions.role]::text[] DESC"))
        end

        # Order by the industry of any work experience
        if search_helper&.work_experience_industries.present?
            query = query.order(Arel.sql("#{search_helper.work_experience_industries.to_sql} && ARRAY[hiring_applications.industry]::text[] DESC"))
        end

        # Order by any skills overlap
        if search_helper&.skills_texts.present?
            # First create a CTE with the skills' texts aggregated
            positions_with_skills_info = %Q~
                SELECT open_positions.id AS open_position_id,
                    array_agg(skills_options.text) AS skills_texts
                    FROM open_positions
                        JOIN open_positions_skills_options ON open_positions.id = open_positions_skills_options.open_position_id
                        JOIN skills_options ON open_positions_skills_options.skills_option_id = skills_options.id
                    GROUP BY open_positions.id
            ~

            # Add the CTE to the query
            query = query.with(positions_with_skills_info: positions_with_skills_info)

            # Join it for use in our ordering query
            query = query.joins("LEFT JOIN positions_with_skills_info ON open_positions.id = positions_with_skills_info.open_position_id")

            # Note that `array_intersect` is a custom function that we created in the past.
            # Also note that `select array_length(array_intersect(ARRAY['foo', 'bar']::text[], null), 1);` is null
            # so we still need a COALESCE.
            query = query.order(Arel.sql("COALESCE(array_length(array_intersect(#{search_helper.skills_texts.to_sql}, positions_with_skills_info.skills_texts), 1), 0) DESC"))
        end

        query = query.order(*OpenPosition::DEFAULT_ORDERS)

        query
    end

    def select_recommended(candidate_or_id:)
        candidate = get_candidate(candidate_or_id)
        self.joins("join users hiring_managers on hiring_managers.id = hiring_manager_id")
            .joins("join hiring_applications on hiring_applications.user_id = hiring_managers.id")
            .select("#{self.is_recommended_sql(candidate.career_profile)} AS recommended")
    end

    # I would make the following methods private but then we can't use them above. They are
    # tested via the specs for methods above.

    def is_recommended_sql(career_profile)
        # I'm assuming a user that is actually asking for recommended positions has these
        # things filled out. If that isn't true then we'd have to decide when are we willing
        # to actually recommended positions.
        if !career_profile.employment_types_of_interest.present? ||
            !career_profile.primary_areas_of_interest.present? ||
            (!career_profile.place_details.present? && !career_profile.real_locations_of_interest.present?)
            return "FALSE"
        end

        # Check for positions that do not have a descriptor that overlaps with the candidate's
        # interested employment types
        sql = "#{self.recommended_descriptor_sql(career_profile)} = TRUE"

        # Check for positions for which the role does not mach the candidate's interests
        sql += " AND #{self.recommended_role_sql(career_profile)} = TRUE"

        # Check for positions that are not near the candidate's location or where
        # they have specified they are interested in
        sql += " AND #{self.recommended_location_sql(career_profile)} = TRUE"

        # Check for positions that are not overlapping with a fuzzy range search
        # on the candidate's years_of_experience
        sql += " AND #{self.recommended_years_of_experience_sql(career_profile.career_profile_search_helper)} = TRUE"

        return "(#{sql})"
    end

    def recommended_descriptor_sql(career_profile)
        "(#{career_profile.employment_types_of_interest.to_sql} && open_positions.position_descriptors)"
    end

    def recommended_role_sql(career_profile)
        "((#{career_profile.real_primary_areas_of_interest.to_sql} && ARRAY[open_positions.role]::text[]) AND open_positions.role != 'other')"
    end

    def recommended_location_sql(career_profile)
        location_clauses = []

        if career_profile.place_details.present?
            location_clauses << Location.at_location_sql_string(career_profile.place_details)
        end

        career_profile.real_locations_of_interest.each do |key|
            location_clauses << Location.at_location_sql_string(key)
        end

        # Users are required to have a place_details if their career_profile is complete, but there are a
        # few edge cases where that isn't true. So we handle the possibility of calling this with no
        # locations available.
        if location_clauses.empty?
            return "FALSE"
        else
            return "(#{location_clauses.join(' OR ')})"
        end
    end

    def recommended_years_of_experience_sql(search_helper)
        # This sql makes a fuzzy range (+- 1 year, EXCLUSIVE) out of the years_of_experience taken from the
        # career_profile_search_helper, then compares that fuzzy range against the position's
        # desired_years_of_experience min-max range, INCLUSIVE.
        fuzzy_min = [(search_helper&.get_years_of_experience || 0) - 1, 0].max # -1 would not practically matter, but is still wrong
        fuzzy_max = (search_helper&.get_years_of_experience || 0) + 1

        %Q~
            (
                numrange(#{fuzzy_min}, #{fuzzy_max}, '()') &&
                numrange((desired_years_experience->>'min')::integer, (desired_years_experience->>'max')::integer, '[]')
            )
        ~
    end

    def get_candidate(candidate_or_id)
        candidate_or_id.is_a?(String) ? User.find(candidate_or_id) : candidate_or_id
    end
end