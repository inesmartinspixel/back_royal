const stripJSONComments = require('strip-json-comments');
const fs = require('fs');

module.exports = function parseJsonFile(filepath) {
    const fileContents = fs.readFileSync(filepath, 'utf8');
    return JSON.parse(stripJSONComments(fileContents));
};
