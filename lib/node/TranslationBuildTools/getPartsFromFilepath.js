module.exports = function getPartsFromFilepath(filepath) {
    const relPath = filepath.split('locales/')[1];
    const parts = relPath.split(/\/|-\w\w.json/).filter(part => !!part);
    return parts;
};
