const getPartsFromFilepath = require('./getPartsFromFilepath');
const parseJsonFile = require('./parseJsonFile');

/*
    Take a filename, like 'vendor/front-royal/modules/DialodModal/locales/dialog_modals/dialog_modal_alert.en'
    and an obj loaded from that file, like {"some_key": "Some Translation"}, and return a nested object
    that can be deepmerged into a locale file, like:

    {
        dialog_modals: {
            dialog_modal_alert: {
                some_key: "Some Translation"
            }
        }
    }
*/
module.exports = function buildNestedObject(filepath) {
    const flatObject = parseJsonFile(filepath);
    const parts = getPartsFromFilepath(filepath);

    // Create an object with a hierarchy matching
    // the filepath
    const nestedObject = {};
    let obj = nestedObject;
    parts.forEach((part, i) => {
        obj[part] = obj[part] || {};

        const isLeaf = i === parts.length - 1;
        if (isLeaf) {
            obj[part] = flatObject;
        } else {
            obj = obj[part];
        }
    });

    return nestedObject;
};
