# Building locales

## Critical locales

Our Webpack configuration supports an optional `criticalLocale` environment variable.
`criticalLocale` dictates which locale is bundled into our JavaSript output. It defaults
to English when a value is not provided.

For development purposes, we only require the `import` of English locales in modules.
Webpack will dynamically rewrite the import statements to use the `criticalLocale`
at compile time.

We do something similar for legacy locales. Any statements thatput the contents of a
`require`d .json file into `window.Smartly.locales...` will be rewritten by Webpack to
use the `criticalLocale`. Any other statements that were originally `require`ing the
`criticalLocale` will be rewritten to `require` the English locale in its place.


## Legacy locales

Legacy locales are those in `vendor/front-royal/components`, `vendor/common/components`, etc.

Those locales are built for in a preprocessing step in webpack.
See `buildLocalesAndTemplates`.  We use a glob pattern to find all of the
locales and put them into chunks based on the folder that they exist
in (`frontRoyal`, `common`, etc.)  The preprocessing step puts the files in `tmp`.

### Making legacy locales available to the browser

Critical locales are required in the javascript chunks and put onto window.Smartly.locales.
See, for example `vendor/common/deps.base.js`

Non-critical locales are also required in the deps files (again, see
`vendor/common/deps.base.js`), but they are not stored on the window object and not
included in the javascript chunks.  By requiring them, we run them through the file-loader,
which gets them stamped and moved over to public/.

`buildManifests` in webpack.manifests.js adds these locales to
webpack_asset_manifest.json.

In translation_module.js, we configure $translateProvider to use the
manifest to find appropriate url for a locale file when a user switches
to a language other than the critical locale.

## Locales in es6 modules

In es6 modules, we import the `en-json` version of locale files that we need.

### Making es6 locales available to the browser

We have defined a custom webpack loader, `localeLoader`, which does 2 things when
an `en.json` file is imported.

1. It converts the json file to a few lines of json code that add the
    object defined in the critical locale file to window.Smartly.locales.  In this
    way, we embed the critical locales into the javascript code.
2. It reads all of the non-critical locale files and merges them into files in
    tmp/modules/locales/.  Later, in copyAndStampModulesLocales, those files are
    stamped and moved to public.  Then, same as with legacy files, `buildManifests`
    in webpack.manifests.js adds these locales to webpack_asset_manifest.json.

### Making es6 locales available to specs

If locale files are needed in specs, they need to be explicitly required.  We have defined
a transformer that will build a nested javascript object based on the path to the file
in which those locales are defined. (See `buildNestedObject`)

That nested object then needs to be merged into window.Smartly.locales.  This is handled
by `setSpecLocales`