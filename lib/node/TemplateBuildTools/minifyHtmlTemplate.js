const minify = require('html-minifier').minify;

module.exports = function minifyHtmlTemplate(src) {
    return minify(src, {
        removeComments: true,
        collapseWhitespace: true,
        processScripts: ['text/ng-template'],
        quoteCharacter: '"',
    });
};
