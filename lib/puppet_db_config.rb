# This module is only necessary because in rake:test:setup we need to
# know the database name without configuring the rails app.  If that were not the
# case, then we could just set `paths['config/database']`  in `config/environments/test.rb`
# and everything would work.
module PuppetDbConfig
    class << self
        def enabled?
            ENV['USE_PUPPET_DB'].present?
        end

        def datapase_config_path
            "config/database_puppet.yml"
        end

        def database
            config = YAML.load(ERB.new(File.read(Rails.root.join(datapase_config_path))).result)
            config['test']['database']
        end
    end
end