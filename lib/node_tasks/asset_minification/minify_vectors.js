/* eslint-disable no-console */
const imagemin = require('imagemin');
const imageminSvgo = require('imagemin-svgo');
const fs = require('fs');
const fileGetter = require('./file_getter');

console.log('\x1b[36m[START]\x1b[0m vector minification');
const start = new Date().getTime();
let count = 0;

async function process() {
    const matchers = [
        '**/*.{svg}',
        '!**/industry_icon.svg',
        '!**/lesson_finish_brain.svg',
        '!**/homepage/homepage-header-illustration.svg',
    ];

    const dir = 'vendor/vectors/';
    const dest = 'vendor/vectors/_minified/';

    const files = fileGetter.getFiles(dir, matchers, dest);

    const promises = [];

    files.forEach(file => {
        if (!fs.existsSync(file.finalPath)) {
            console.log(`imagemin: ${file.filepath} --> ${file.folder}`);
            promises.push(
                imagemin([file.filepath], {
                    destination: file.folder,
                    plugins: [
                        imageminSvgo({
                            plugins: [
                                {
                                    removeTitle: true,
                                },
                                {
                                    removeDesc: true,
                                },
                                {
                                    removeViewBox: false,
                                },
                            ],
                        }),
                    ],
                }),
            );
            count += 1;
        }
    });

    await Promise.all(promises);

    console.log(
        `\x1b[42m[FINISHED]\x1b[0m minifying ${count} vectors after ${(
            (new Date().getTime() - start) /
            1000
        ).toString()}s`,
    );
}
process();
