const webpackAssetHelper = require('../../../webpack/webpack.assets');
const webpackManifestHelper = require('../../../webpack/webpack.manifests');

webpackAssetHelper.buildMarketingAssetDependencies();
webpackManifestHelper.buildAssetManifest();
