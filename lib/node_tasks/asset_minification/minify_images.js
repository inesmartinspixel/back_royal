/* eslint-disable no-console */
const imagemin = require('imagemin');
const imageminJpegtran = require('imagemin-jpegtran');
const imageminPngquant = require('imagemin-pngquant');
const fs = require('fs');
const fileGetter = require('./file_getter');

console.log('\x1b[36m[START]\x1b[0m image minification');
const start = new Date().getTime();
let count = 0;

async function process() {
    // Since our /images directories follow the same structure, we can use the same matchers
    const matchers = ['**/*.{png,jpg,jpeg}'];

    const dir = 'vendor/images/';
    const dest = 'vendor/images/_minified/';

    const files = fileGetter.getFiles(dir, matchers, dest);

    const promises = [];

    files.forEach(file => {
        // If the file exists, don't bother overwriting it
        if (!fs.existsSync(file.finalPath)) {
            promises.push(
                imagemin([file.filepath], {
                    destination: file.folder,
                    plugins: [imageminJpegtran(), imageminPngquant()],
                }),
            );
            count += 1;
        }
    });

    await Promise.all(promises);

    console.log(
        `\x1b[42m[FINISHED]\x1b[0m minifying ${count} images after ${(
            (new Date().getTime() - start) /
            1000
        ).toString()}s`,
    );
}
process();
