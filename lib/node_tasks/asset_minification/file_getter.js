const fileSystem = require('file-system');

module.exports.getFiles = (dir, matchers, dest) => {
    const filesMap = [];

    // Recursively traverse the directory and match anything in `matchers`
    fileSystem.recurseSync(dir, matchers, (filepath, relative, filename) => {
        if (!filename) {
            return; // If this is just a folder, return
        }
        if (filepath.toString().match(/\/_minified\//g)) {
            return; // If we're in the _minified directory, return
        }

        const folder = dest + relative.replace(filename, '');

        filesMap.push({
            filepath,
            folder,
            finalPath: folder.concat(filename),
        });
    });

    return filesMap;
};
