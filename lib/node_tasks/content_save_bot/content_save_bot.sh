# #!/bin/sh

# # switch to same directory as this script in anticipation of content_save_bot.js
# cd ${0%/*}

# # allow phantomjs and casperjs to live in project-specific node_modules
# PATH=$(npm bin):$PATH

# # appears to be necessary, even with phantomjs2, as localStorage.clear calls don't appear to work as epxected
# rm -rf ~/Library/Application\ Support/Ofi\ Labs/PhantomJS/*.localstorage

# # run the actual script
# casperjs test --engine=phantomjs content_save_bot.js

# # go back to the previous working directory
# cd $OLDPWD