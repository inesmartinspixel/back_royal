// 'use strict';

// // node dependencies
// var system = require('system');

// //---------------------------
// // Environment Configuration
// //---------------------------


// // setup casper preferences
// casper.options.logLevel = 'info';
// casper.options.verbose = true;
// casper.options.waitTimeout = 20000;
// casper.options.pageSettings = {
//     userAgent: 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.10 (KHTML, like Gecko) Chrome/23.0.1262.0 Safari/537.10',
//     loadPlugins: false,
//     webSecurityEnabled: false,
//     ignoreSslErrors: true
// };
// casper.options.onStepTimeout = () => {
//     casper.log('Step timeout. Proceeding to next step (hopefully) ...');
//     casper.clear();
//     casper.page.stop();
// };

// // setup authentication info
// var authUser = {
//     email: system.env.AUTH_EMAIL,
//     password: system.env.AUTH_PASSWORD
// };

// var updatePublishedOrWorking = system.env.WP; // 'published' or 'working'
// var filters = system.env.FILTERS ? JSON.parse(system.env.FILTERS) : null;

// //---------------------------
// // Tests Entry Point
// //---------------------------

// // is this test loop even necessary?
// casper.test.begin('SaveBot Content Saving', () => {


//     //---------------------------
//     // Event Handling
//     //---------------------------

//     casper.on('remote.message', msg => {
//         if (msg.indexOf('Unsafe JavaScript') === -1) {
//             casper.log(msg, 'info');
//         }
//     });

//     casper.on('error', err => {
//         casper.die('CasperJS has errored: ' + err);
//     });

//     casper.on('page.error', (err, stack) => {
//         var msgStack = ['Webpage has errored: ' + err];
//         if (stack && stack.length) {
//             msgStack.push('TRACE:');
//             stack.forEach(t => {
//                 msgStack.push(' -> ' + t.file + ': ' + t.line + (t['function'] ? ' (in function "' + t['function'] + '")' : ''));
//             });
//         }
//         casper.log('\n' + msgStack.join('\n'), 'error');
//     });

//     casper.on('resource.requested', resource => {
//         casper.log('Resource requested: ' + resource.url.substring(0, 100), 'debug');
//     });

//     casper.on('resource.received', resource => {
//         casper.log('Resource received: ' + resource.url.substring(0, 100), 'debug');
//     });

//     casper.on('resource.error', resource => {
//         casper.log('Resource load error: ' + resource.url.substring(0, 100), 'warning');
//         casper.log('\t\t [' + resource.errorCode + '] ' + resource.errorString, 'warning');
//     });

//     casper.on('casper.page.onResourceTimeout', err => {
//         casper.log('Resource timeout: ' + err, 'warning');
//     });


//     //---------------------------
//     // Processing Tasks
//     //---------------------------

//     function resaveLessons(updatePublishedOrWorking, filters) {
//         var LessonSaver = $('[ng-app]').injector().get('LessonSaver');
//         var instance = new LessonSaver(updatePublishedOrWorking, filters);
//         LessonSaver[instance.id] = instance;
//         instance.exec();
//         return instance.id;
//     }

//     function checkSavingComplete(saverId) {
//         var LessonSaver = $('[ng-app]').injector().get('LessonSaver');
//         var instance = LessonSaver[saverId];
//         return instance.complete;
//     }

//     function checkProgress(saverId) {
//         var LessonSaver = $('[ng-app]').injector().get('LessonSaver');
//         var instance = LessonSaver[saverId];
//         return instance.progressMessage;
//     }

//     function getResults(saverId) {
//         var LessonSaver = $('[ng-app]').injector().get('LessonSaver');
//         var instance = LessonSaver[saverId];
//         return instance.resultLines;
//     }

//     // handles pageload responses for each URL we iterate over
//     function processAllLessons() {

//         casper.log(new Date() + ': Waiting for page content');

//         casper.waitUntilVisible('bot', () => {
//             var saverId;
//             casper.then(() => {
//                 saverId = casper.evaluate(resaveLessons, updatePublishedOrWorking, filters);
//                 casper.log('Started saving ' + saverId, 'info');
//             });

//             var lastProgressMessage;
//             var oneHour = 60 * 60 * 1000;
//             casper.waitFor(() => {
//                 var progressMessage = casper.evaluate(checkProgress, saverId);
//                 if (progressMessage !== lastProgressMessage) {
//                     casper.log(new Date() + ': ' + progressMessage, 'info');
//                     lastProgressMessage = progressMessage;
//                 }

//                 return casper.evaluate(checkSavingComplete, saverId);
//             }, undefined, undefined, 365 * 24 * oneHour); // let it run for a year without timing out

//             casper.then(() => {
//                 var results = casper.evaluate(getResults, saverId);
//                 results.forEach(line => {
//                     casper.log(line, 'info');
//                 });
//             });
//         });



//     }

//     // conducts authentication with backend server, allowing subsequent requests to succeed
//     function setupAuthenticationTask() {

//         // go to bot first so that event logger gets turned off
//         casper.start('http://localhost:3000/editor/bot');
//         casper.viewport(1200, 800);

//         // attempt to login to the application after angular has rendered the form
//         var formSelector = 'form[name="signIn"]';
//         casper.waitUntilVisible(formSelector, () => {
//             casper.log('Loaded sign-in form', 'info');
//             // complete the form
//             casper.fill(formSelector, {
//                 email: authUser.email,
//                 password: authUser.password
//             }, true);

//             // need to force a wait in order to prevent an Operation Cancelled on the API request
//             casper.waitUntilVisible('bot');
//         });

//     }



//     //---------------------------
//     // Test / Processing Start
//     //---------------------------


//     setupAuthenticationTask();
//     processAllLessons();

//     casper.run(() => {
//         casper.test.done();
//         casper.clear();
//         casper.exit();
//     });



// });