== SAVEBOT 5000 NOTES ==

===NOTE: This is currently deprecated and kept for example / iteration purposes. Should be transitioned to Puppeteer, etc.===

 - Ensure you have a admin user setup with the following environment variables: `AUTH_EMAIL` and `AUTH_PASSWORD`

 - Run `WP=published ./lib/node_tasks/content_save_bot/content_save_bot.sh`
    from the `back_royal` root directory in order to re-save each version.

    - Change WP=published to WP=working to save working versions that have not
    been touched in at least an hour. This is safe to run while content authors are
    actively working because the restriction to lessons that have not been touched in
    the last hour will prevent conflicts.

    - set FILTERS to a json string to pass extra filters along to the Lesson.index call

    - Keep an eye on the output for any warnings about lessons that failed to save

 - To do what this does, but in a regular browser, run this in a console:

        $('[ng-app]').injector().get('EventLogger').disabled = true; LessonSaver = $('[ng-app]').injector().get('LessonSaver'); saver = new LessonSaver('published', [], true); saver.exec().then(function() { console.log('complete'); })