== Frame Screengrabbing Notes ==

===NOTE: This is currently deprecated and kept for example / iteration purposes. Should be transitioned to Puppeteer, etc.===

 - Ensure you have a admin user setup with the following environment variables: `AUTH_EMAIL` and `AUTH_PASSWORD`

 - Run `lib/node_tasks/frame_ui_diff/frame_ui_diff.sh` from the `back_royal` root directory in order to establish baseline images.

 - Make some code changes that would affect frame rendering

 - Repeat the last command to conduct a test

 - Repeat again and the diff images will be deleted and a new baseline will be established

 == Configuration

  - in the lib/node_tasks/frame_ui_diff/frame_ui_diff.js, you can set lessonLimit, lessonFilters, and frameFilter to control which frames
    get screenshotted