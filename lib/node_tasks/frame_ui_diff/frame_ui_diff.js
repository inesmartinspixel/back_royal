// 'use strict';

// // node dependencies
// var fs = require('fs');

// var system = require('system');
// var phantomcssPath = './../../../node_modules/phantomcss/';
// var path = phantomcssPath + 'phantomcss';
// var phantomcss = require(path);
// var _ = require('underscore');


// //---------------------------
// // Environment Configuration
// //---------------------------


// // setup casper preferences
// casper.options.logLevel = 'info';
// casper.options.verbose = true;
// casper.options.waitTimeout = 20000;
// casper.options.pageSettings = {
//     userAgent: 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.10 (KHTML, like Gecko) Chrome/23.0.1262.0 Safari/537.10',
//     loadPlugins: false,
//     webSecurityEnabled: false,
//     ignoorslErrors: true
// };
// casper.options.onStepTimeout = () => {
//     casper.log('Step timeout. Proceeding to next step (hopefully) ...');
//     casper.clear();
//     casper.page.stop();
// };

// // setup authentication info
// var authUser = {
//     email: system.env.AUTH_EMAIL,
//     password: system.env.AUTH_PASSWORD
// };
// var existingScreenshotDetails;
// var rebaseScreenshots;

// // lessonFilters is an object that will be merged into the 'filters'
// // property in the Lesson.index call (these ids are all in
// // business foundations as of 11/3/2016)
// var lessonFilters = {
//     id: ['a6c56f05-d22b-4be4-9b67-d241b5819c74',
//         '5cc3b222-9c97-4be2-bd73-1e86eda69409',
//         '3476639f-d895-4b1e-8877-8fe5849c2940',
//         'c4fbb1e8-e570-4335-88fe-7f2e7c206af3',
//         'b72a2e21-1ff0-4f2a-96cb-29a436b83157',
//         'b1c75e80-999f-452e-ab7b-8c1bca17983d',
//         '34f1392f-17da-493e-9cab-9b099450d9e6',
//         'c6be8582-eae4-4258-aa6a-1fa1fd07d2a5',
//         '7771a962-a451-4487-a884-e2428733ce2d',
//         '64c3275c-5cbf-4549-9347-01fd9c67b983',
//         'ee86fadb-9202-41ba-b702-fd62b883baca',
//         '6262cb74-8e95-4283-8e17-0f3c66c0b930',
//         'cc304531-1869-451a-a833-82fec446106c',
//         'f76c194a-036b-440b-9ae7-5df256613949',
//         '7edb42d5-8629-4e65-a0e6-e6fca76dedb2',
//         '873855e9-d572-48d9-be08-2449d14b1f64',
//         'ccc7824f-107e-42b7-b40b-dc4187b4e98c',
//         '2b0c521b-ec74-49d6-b5b1-7f5e918b65ec',
//         '3a836dc7-e2eb-43a0-b880-530b03909b4f',
//         '6788d4ea-8e8b-49f3-8764-ee160ab366ed',
//         '771dee64-17f0-4094-98ce-64efd3edb7c4',
//         'a1502bd4-808c-4e71-bf62-1e1296de8ab1',
//         'd8671de7-dd4c-4bf1-89e7-27d5e9d16f57',
//         '28b2969d-c0e7-4f10-b048-95ac852ea477',
//         '4575893f-4964-4354-8837-79006507170d',
//         'a1882b43-400b-4f60-909b-32407ee7077d',
//         '9fc9bd0b-1805-4275-88a4-f53a2e62a89b',
//         '8322821c-8d54-4786-aeac-200f162e03df',
//         'b07c6f79-fb8d-43a7-986c-f540c46bd4b5',
//         'a48c181d-1dd8-43b5-80de-e04ea86b13d0',
//         'f08ddc38-242c-4e9f-a792-70341f40aac5',
//         '2b5931c6-9db6-4c9e-b7e7-7e9f3762103d',
//         'fd6c37dc-c9c3-4b07-bff3-bfb4a8b18fa9',
//         '7af507d0-b2ec-486c-a49e-34d05082ecce',
//         'f218ff1d-c9f0-43c3-87fb-9b94af7c7780',
//         'dd79ea3a-4f58-41db-94f4-3a99b8b45836',
//         'ede7b8dd-415f-416e-b001-deb4dca1ef28',
//         'aee828d5-6615-4fbd-bc58-41ccd39470ae',
//         '210548de-2d4d-43ad-9412-2416eef6d708',
//         'a6384bbd-65fb-45d9-ade8-fd483a98e050',
//         '06646878-16d8-4d2f-a5cc-936f67a11289',
//         '0d69681f-3958-4801-8f19-e45a723f9350',
//         '90a46390-3060-4f7f-9a69-494806120384',
//         '87b80b75-a0c5-481b-ba41-27d066f91cb8',
//         'be336cd4-21d4-406d-aa1a-a94da271f918',
//         'c0a386d4-1b69-4e7e-94ca-08f3157d429e',
//         '1ccd7f3f-b07d-4ad4-8342-172f17864600',
//         'd58cb433-6fa9-4ebe-81fd-9b073f9519d0',
//         'da19df5f-afa2-4aed-8eba-3a7d42862aab',
//         '87bef1cf-5254-4961-a446-ec1838682458',
//         '491909df-caf7-45b6-9acb-6c64df5f6770',
//         '947e4d91-399a-4f44-8289-870574153bf8',
//         '4cbbdd7c-9d99-43b1-8f7c-492566583e9d',
//         '2c6d4bbe-9840-4a2b-b25a-5eee41b8f237',
//         '3b663906-7b62-4577-b86e-368c6136440d'
//     ]
// };

// // lessonLimit is an integer that will limit the number of lessons to load
// var lessonLimit;

// // frameFilter is a function that takes a frame as an argument.  If it
// // returns true than no screenshots will be taken for that frame
// var frameFilter = frame => //return frame.id !== 'c82eea15-f990-4828-ba02-bdb697ff4c49';
//     //return frame.index() > 1;
//     //return frame.mainUiComponent.editor_template !== 'no_interaction';
//     false;


// //---------------------------
// // Tests Entry Point
// //---------------------------

// // is this test loop even necessary?
// casper.test.begin('SaveBot Content Saving',
//     () => {

//         //---------------------------
//         // PhantomCSS initialization
//         //---------------------------
//         var screenshotRoot = './screenshots/frame_ui_diff';
//         // setup phantom output paths / etc
//         phantomcss.init({
//             captureWaitEnabled: false,
//             addIteratorToImage: false,
//             rebase: rebaseScreenshots,
//             //casper.cli.get('rebase'),
//             casper,
//             //libraryRoot: phantomcssPath,
//             screenshotRoot,
//             failedComparisonsRoot: './screenshots/frame_ui_diff/failures',
//             addLabelToFailedImage: false,
//             //mismatchTolerance: 1,
//             onNewImage() {} // turn off logging
//         });
//         //phantomcss.turnOffAnimations();




//         //---------------------------
//         // Event Handling
//         //---------------------------

//         casper.on('remote.message', msg => {
//             if (msg.indexOf('Unsafe JavaScript') === -1) {
//                 casper.log(msg, 'info');
//             }
//         });

//         casper.on('error', err => {
//             casper.die('CasperJS has errored: ' + err);
//         });

//         casper.on('page.error', (err, stack) => {
//             var msgStack = ['Webpage has errored: ' + err];
//             if (stack && stack.length) {
//                 msgStack.push('TRACE:');
//                 stack.forEach(t => {
//                     msgStack.push(' -> ' + t.file + ': ' + t.line + (t['function'] ? ' (in function "' + t['function'] + '")' : ''));
//                 });
//             }
//             casper.log('\n' + msgStack.join('\n'), 'error');
//         });

//         casper.on('resource.requested', resource => {
//             casper.log('Resource requested: ' + resource.url.substring(0, 100), 'debug');
//         });

//         casper.on('resource.received', resource => {
//             casper.log('Resource received: ' + resource.url.substring(0, 100), 'debug');
//         });

//         casper.on('resource.error', resource => {
//             casper.log('Resource load error: ' + resource.url.substring(0, 100), 'warning');
//             casper.log('\t\t [' + resource.errorCode + '] ' + resource.errorString, 'warning');
//         });

//         casper.on('casper.page.onResourceTimeout', err => {
//             casper.log('Resource timeout: ' + err, 'warning');
//         });


//         //---------------------------
//         // Processing Tasks
//         //---------------------------

//         function createScreenshotter(existingScreenshotDetails, lessonLimit, lessonFilters, frameFilter) {
//             window.ScreenshotOptions = {
//                 disableRandomization: true
//             };
//             var FrameScreenshotter = $('[ng-app]').injector().get('FrameScreenshotter');
//             var instance = new FrameScreenshotter(false, existingScreenshotDetails, lessonLimit, lessonFilters, frameFilter);
//             FrameScreenshotter[instance.id] = instance;
//             instance.exec();
//             return instance.id;
//         }

//         function checkScreenshottingComplete(screenshotterId) {
//             var FrameScreenshotter = $('[ng-app]').injector().get('FrameScreenshotter');
//             var instance = FrameScreenshotter[screenshotterId];
//             return instance.complete;
//         }

//         function checkForFrameReadyForScreenshot(screenshotterId) {
//             var FrameScreenshotter = $('[ng-app]').injector().get('FrameScreenshotter');
//             var instance = FrameScreenshotter[screenshotterId];
//             return instance.visibleFrameDetails;
//         }

//         function getProgressMessage(screenshotterId) {
//             var FrameScreenshotter = $('[ng-app]').injector().get('FrameScreenshotter');
//             var instance = FrameScreenshotter[screenshotterId];
//             return instance.progressMessage;
//         }

//         // create a screenshot of the frame on the given page
//         function takeScreenshot(visibleFrameDetails) {
//             //casper.log('Taking screenshot: ' + frameId, 'info');
//             var label = [
//                 visibleFrameDetails.lessonId,
//                 visibleFrameDetails.paddedIndex,
//                 visibleFrameDetails.frameId
//             ].join('__');
//             phantomcss.screenshot('.frame-container cf-ui-component', label);
//         }

//         function onScreenshotTaken(screenshotterId) {
//             var FrameScreenshotter = $('[ng-app]').injector().get('FrameScreenshotter');
//             var instance = FrameScreenshotter[screenshotterId];
//             instance.onScreenshotTaken();
//         }

//         // function getResults(screenshotterId) {
//         //     var PublishedLessonSaver = $('[ng-app]').injector().get('PublishedLessonSaver');
//         //     var instance = PublishedLessonSaver[screenshotterId];
//         //     return instance.resultLines;
//         // }

//         // handles pageload responses for each URL we iterate over
//         function screenshotAllFrames() {

//             casper.log(new Date() + ': Waiting for page content', 'info');

//             casper.waitUntilVisible('bot', () => {
//                 var screenshotterId;
//                 casper.then(() => {
//                     screenshotterId = casper.evaluate(createScreenshotter, existingScreenshotDetails, lessonLimit, lessonFilters, frameFilter);
//                     casper.log('Started taking screenshots ' + screenshotterId, 'info');
//                 });

//                 var lastVisibleFrameId;
//                 var lastProgressMessage;
//                 //var oneHour = 60 * 60 * 1000;
//                 casper.waitFor(() => {
//                     var visibleFrameDetails = casper.evaluate(checkForFrameReadyForScreenshot, screenshotterId);
//                     if (visibleFrameDetails && visibleFrameDetails.frameId !== lastVisibleFrameId) {
//                         takeScreenshot(visibleFrameDetails);
//                         lastVisibleFrameId = visibleFrameDetails.frameId;
//                         casper.evaluate(onScreenshotTaken, screenshotterId);
//                     }

//                     var progressMessage = casper.evaluate(getProgressMessage, screenshotterId);
//                     if (progressMessage !== lastProgressMessage) {
//                         casper.log('************* ' + new Date() + ' ********************', 'info');
//                         casper.log('  ' + progressMessage, 'info');
//                         lastProgressMessage = progressMessage;
//                     }

//                     return casper.evaluate(checkScreenshottingComplete, screenshotterId);
//                 }, undefined, undefined, 9999999999999999);
//             });



//         }

//         // conducts authentication with backend server, allowing subsequent requests to succeed
//         function setupAuthenticationTask() {

//             // go to bot first so that event logger gets turned off
//             casper.start('http://localhost:3000/public/bot');
//             casper.viewport(1200, 800);

//             // attempt to login to the application after angular has rendered the form
//             var formSelector = 'form[name="signIn"]';
//             casper.waitUntilVisible(formSelector, () => {
//                 casper.log('Loaded sign-in form', 'info');
//                 // complete the form

//                 casper.fill(formSelector, {
//                     email: authUser.email,
//                     password: authUser.password
//                 }, true);

//                 // need to force a wait in order to prevent an Operation Cancelled on the API request
//                 casper.waitUntilVisible('bot');
//             });

//         }



//         //---------------------------
//         // Test / Processing Start
//         //---------------------------


//         //     glob("**/*.js", options, function (er, files) {
//         //   // files is an array of filenames.
//         //   // If the `nonull` option is set, and nothing
//         //   // was found, then files is ["**/*.js"]
//         //   // er is an error object or null.
//         // })
//         var list = fs.list(screenshotRoot);
//         rebaseScreenshots = false;
//         existingScreenshotDetails = _.chain(list).map(path => {
//             // if there are already some diff images, then start over again
//             if (path.match('diff.png')) {
//                 rebaseScreenshots = true;
//             }
//             var match = path.match(/([\w\d-]+)__\d\d__([\w\d-]+).png/);
//             if (match) {
//                 return {
//                     lessonId: match[1],
//                     frameId: match[2]
//                 };
//             }
//         }).compact().value();
//         if (rebaseScreenshots) {
//             fs.removeTree(screenshotRoot);
//             existingScreenshotDetails = [];
//         }

//         setupAuthenticationTask();
//         screenshotAllFrames();
//         casper.then(() => {
//             phantomcss.compareAll();
//         });

//         /*
//         To do in a browser what this does here, go to /editor/bot and run:

//         $('[ng-app]').injector().get('EventLogger').disabled = true; PublishedLessonSaver = $('[ng-app]').injector().get('PublishedLessonSaver'); saver = new PublishedLessonSaver(); saver.exec().then(function() { console.log('complete'); })
//     */

//         casper.run(() => {
//             casper.test.done();
//             casper.clear();
//             casper.exit();
//         });


//     });