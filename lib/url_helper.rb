module UrlHelper

    # Useful for determining the appropriate domain in contexts where no controller request is available
    def self.domain_root(branding_domain = AppConfig.quantic_domain)
        return "localhost" if Rails.env.development?
        return "staging.#{branding_domain}" if ENV['APP_ENV_NAME']&.match?(/staging/i)
        return branding_domain
    end

    # We ran into issues detecting the protocol on staging. We saw in the omniauth gem that
    # because of nginx you have to do a bunch of specific checks rather than using
    # request.protocol (see lib/omniauth/strategy.rb#ssl?),but that did not work either.
    # So we decided to just hardcode to https unless we detect that you're using localhost.
    def self.protocol(request = nil)
        return (Rails.env.development? ? 'http://' : 'https://') if !request
        return (request.host == 'localhost' || request.host.match(/elasticbeanstalk\.com/) ? 'http://' : 'https://')
    end

    def self.port(request = nil)
        return (Rails.env.development? ? ':3000' : '') if !request
        return [80, 443].include?(request.port) ? '' : ":#{request.port}"
    end

    def self.domain_url(branding_domain = AppConfig.quantic_domain)
        "#{self.protocol}#{self.domain_root(branding_domain)}#{self.port}"
    end

end
