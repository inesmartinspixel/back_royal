=begin
This module allows us to store `editor_id` and `editor_name` on audit tables
in our database.

Standard usage:

    Normally, we set the current_user for the lifetime of a transaction using

        EditorTracking.transaction(current_user) { }

    `current_user` here can be either an instance of `User`, in which case both
    `editor_id` and `editor_name` will be set based on the user's attributes, or it
    can just be a string, in which case `editor_name` will be set to the value of the string.

    In many cases, developers will not need to worry about this at all because it will be
    handled by a global method in the controller.  For example, any saves wrapped inside
    the block that is passed to `rescue_validation_error_or_render_record` will automatically
    be tracked with the current_user of the controller.

    In cases where there is no transaction set up in the controller, `EditorTracking.transaction`
    can be called explicitly.  For example, in certain jobs we call it with the name of the job as
    the current_user.

Global usage:

    In certain cases, it makes sense to set the `global_user` and apply it to all
    saves made in the session.  In rake tasks, for example, we automatically set the current_user
    globally to the name of the rake task that is being run (see editor_tracking.rake).

    When `global_user` is set, the name and id variables are defined at the session
    level inside of postgres, so this can only be used when there is a single database connection
    for the rails process and we want all saves to have the same user applied.


=end

require 'active_support'

module EditorTracking

    # Set the name and id variables in the database for the
    # lifetime of a transaction.
    def self.transaction(current_user = nil, &block)
        RetriableTransaction.transaction do
            set_db_vars(current_user, 'local')
            yield
        end
    end

    def self.global_user=(current_user)

        # In select Rake tasks (db_redshift.rake), we toggle the connection to Redshift.
        # Redshift has no concept of Postgres variables, causing this scheme to break down.
        # Since we only care about tracking version changes, it shouldn't be relevant, as
        # no triggers should be executing within a Redshift transaction anyhow. If we ever
        # need to implement this strategy for Redshift, there are some possible workarounds:
        # https://stackoverflow.com/questions/30822176/declare-a-variable-in-redshift
        return if ActiveRecord::Base.connection_config[:store_type] == 'redshift'

        return if current_user == @global_user
        raise NotImplementedError.new("Cannot unset current_user") if current_user.nil? # This can easily be implemented if we need it one day
        set_db_vars(current_user, 'session')
        @global_user = current_user
    end

    def self.global_user
        @global_user
    end

    # when connected to via TTY, this will be the ip you connected from
    def self.tty_ip_address
        ip = nil
        resolved_hostname = `who -m|awk '{print $5}'|sed 's/[()]//g'|tr -d '\n'`
        if !(IPAddr.new(resolved_hostname) rescue nil).nil?
            ip = resolved_hostname # if it's already a valid IP, don't do anything
        else
            ip = `dig +short #{resolved_hostname}|tr -d '\n'` # otherwise, attempt to transform
        end
        ip.blank? ? nil : ip
    end

    private
    # scope_flag is either `local` or `session`.  See https://www.postgresql.org/docs/10/sql-set.html
    def self.set_db_vars(current_user, scope_flag)

        name = nil
        id = nil
        if current_user.is_a?(String)
            name = current_user
        elsif current_user
            name = current_user.name
            id = current_user.id
        end

        statements = []
        if name
            statements << "set #{scope_flag} myvars.editor_name to #{ActiveRecord::Base.connection.quote(name)}"
        end
        if id
            statements << "set #{scope_flag} myvars.editor_id to #{ActiveRecord::Base.connection.quote(id)};"
        end

        if statements.any?
            ActiveRecord::Base.connection.execute statements.join('; ')
        end
    end

    # Set the global_user when a dev is connected via the rails console.
    # see application.rb for how this is triggered when `rails c` runs.
    module Console
        extend ActiveSupport::Concern

        def self.included(console)
            EditorTracking.global_user = ["developer console", EditorTracking.tty_ip_address].compact.join(' / ')
        end
    end

end