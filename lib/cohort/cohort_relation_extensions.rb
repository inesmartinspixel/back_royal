module Cohort::CohortRelationExtensions

    # This is used in data backfills/migrations.  It will
    #
    # 1. raise if any of the cohorts have been edited since they
    #       were last published
    # 2. defer view refresh until all updates have been made
    #
    # Usage:
    #
    #       Cohort.where(...).edit_and_republish do |cohort|
    #           make changes to the cohort
    #       end
    #
    # (Maybe this method should
    # really be in publishable itself, but I can't think
    # when we would have used it for a different class)
    def edit_and_republish
        # Automatically retries in the event of ActiveRecord::PreparedStatementCacheExpired
        RetriableTransaction.transaction do
            self.each do |cohort|
                # to save time, only refresh the views once after
                # everything is all done
                cohort.skip_refresh_views = true

                if cohort.user_facing_change_since_last_published?
                    if Rails.env.development?
                        # Note: the \e[Xm escape sequence allows for colored text in terminal output. X=33 is yellow, X=0 is normal.
                        message = "!! WARNING: Republishing cohort #{cohort.name.inspect} despite it having changes since it was last published !!"
                        bars = "!" * message.length
                        puts "\e[33m#{bars}\e[0m"
                        puts "\e[33m#{message}\e[0m"
                        puts "\e[33m#{bars}\e[0m"
                    else
                        raise "Cannot automatically republish cohort #{cohort.name.inspect} because it has been changed since it was last published"
                    end
                end

                yield(cohort) if block_given?

                if cohort.has_published_version?
                    cohort.publish!({ update_published_at: true })
                else
                    cohort.save!
                end
            end

            RefreshMaterializedContentViews.refresh
        end
    end

    def promoted
        self.where("cohorts.id in (#{CohortPromotion.select(:cohort_id).to_sql}) or cohorts.id in (#{AdmissionRound.where(promoted:true).select(:cohort_id).to_sql})")
    end
end