module Redshift

    def self.vacuum_and_analyze
        begin
            # see also: http://docs.aws.amazon.com/redshift/latest/dg/vacuum-column-limit-exceeded-error.html
            RedshiftEvent.connection.execute('SET wlm_query_slot_count TO 9')
    
            # see also: https://docs.aws.amazon.com/redshift/latest/dg/r_analyze_threshold_percent.html
            # see also: comment in `analyze` below
            RedshiftEvent.connection.execute('SET analyze_threshold_percent TO 0')
    
            # get initial skew values and log to cloudwatch
            stats_info = get_stats_info_and_log_to_cloudwatch
    
            # time the entire operation, which itself records individual component timings
            Cloudwatch.put_timing_metric_data(metric_name: 'RedshiftVacuumTime', ns_suffix: 'Redshift') do
                vacuum_sort(stats_info, ENV['VACUUM_SORT_THRESHOLD'] ? ENV['VACUUM_SORT_THRESHOLD'].to_f : 0.05)
                vacuum_delete(stats_info, ENV['VACUUM_DELETE_THRESHOLD'] ? ENV['VACUUM_DELETE_THRESHOLD'].to_f : 1000000)
                analyze(stats_info, ENV['ANALYZE_THRESHOLD'] ? ENV['ANALYZE_THRESHOLD'].to_f : 0.10)
            end
    
            # log updated skew values
            get_stats_info_and_log_to_cloudwatch
    
        rescue Exception => err
            raise err, RedshiftEvent.sanitized_error_message(err), err.backtrace
    
        ensure
            RedshiftEvent.connection.execute('SET wlm_query_slot_count TO 1')
            RedshiftEvent.connection.execute('SET analyze_threshold_percent TO 10')
        end
    end


    def self.vacuum_sort(stats_info, sort_threshold)
        print "VACUUM SORT ONLY ... "
        if stats_info['unsorted'] >= sort_threshold
            Cloudwatch.put_timing_metric_data(metric_name: 'VacuumSortTime', ns_suffix: 'Redshift') do
                RedshiftEvent.connection.execute("
                    -- Vacuum sort events based on unsorted threshold
                    VACUUM SORT ONLY events TO 100 PERCENT;
    
                    -- Since the volume in these tables is not so
                    -- high, we do not need to override the default threshold.
                    VACUUM SORT ONLY user_utm_params;
                    VACUUM SORT ONLY user_aliases;
                ")
            end
            puts "DONE!"
        else
            puts "Skipping!"
        end
        Cloudwatch.put_metric_data(
            ns_suffix: 'Redshift',
            metric_data: [{
                metric_name: 'sort_threshold',
                value: sort_threshold,
                unit: 'Percent'
            }]
        )
    end
    
    def self.vacuum_delete(stats_info, delete_threshold)
        print "VACUUM DELETE ONLY ... "
        if stats_info['soft_deleted_rows'] >= delete_threshold
            Cloudwatch.put_timing_metric_data(metric_name: 'VacuumDeleteTime', ns_suffix: 'Redshift') do
                RedshiftEvent.connection.execute("
                    -- Vacuum delete events based on stats_off threshold
                    VACUUM DELETE ONLY events TO 100 PERCENT;
    
                    -- These tables are not large, and nothing is
                    -- ever expected to be deleted from them, so no
                    -- need to override the default threshold.
                    VACUUM DELETE ONLY user_utm_params;
                    VACUUM DELETE ONLY user_aliases;
                ")
            end
            puts "DONE!"
        else
            puts "Skipping!"
        end
        Cloudwatch.put_metric_data(
            ns_suffix: 'Redshift',
            metric_data: [{
                metric_name: 'delete_threshold',
                value: delete_threshold,
                unit: 'Count'
            }]
        )
    end
    
    
    def self.analyze(stats_info, analyze_threshold)
        # We could have set the `analyze_threshold_percent` value based off of `analyze_threshold` here, but since
        # we already pulled this from svv_table_info, this should prevent ANALYZE from doing an unnecessary query.
        # As a result, we've disabled `analyze_threshold_percent` during to force if this threshold is met.
        print "ANALYZE ... "
        if stats_info['stats_off'] >= analyze_threshold
            Cloudwatch.put_timing_metric_data(metric_name: 'AnalyzeTime', ns_suffix: 'Redshift') do
                RedshiftEvent.connection.execute("
                ANALYZE
                ")
            end
            puts "DONE!"
        else
            puts "Skipping!"
        end
        Cloudwatch.put_metric_data(
            ns_suffix: 'Redshift',
            metric_data: [{
                metric_name: 'analyze_threshold',
                value: analyze_threshold,
                unit: 'Percent'
            }]
        )
    end
    
    def self.get_stats_info_and_log_to_cloudwatch
    
        print "Collecting stats ... "

        result = RedshiftEvent.connection.execute("
            SELECT unsorted, stats_off FROM svv_table_info where \"table\" = 'events' LIMIT 1
        ").to_a[0]
    
        # if you hit this locally.  try `set analyze_threshold_percent to 0.01; analyze;`
        raise "No stats_info. Maybe ANALYZE has never been run?" unless result
    
        stats_info = result.transform_values(&:to_f)
    
        result = RedshiftEvent.connection.execute(%Q~
            SELECT tbl_rows - (SELECT count(*) FROM events) as soft_deleted_rows FROM svv_table_info WHERE "schema" = 'public' AND "table" = 'events'
        ~).to_a[0]
        stats_info['soft_deleted_rows'] = result['soft_deleted_rows'].to_i
    
        Cloudwatch.put_metric_data(
            ns_suffix: 'Redshift',
            metric_data: [{
                metric_name: 'unsorted',
                value: stats_info['unsorted'],
                unit: 'Percent'
            },{
                metric_name: 'soft_deleted_rows',
                value: stats_info['soft_deleted_rows'],
                unit: 'Count'
            },{
                metric_name: 'stats_off',
                value: stats_info['stats_off'],
                unit: 'Percent'
            }])

        puts "DONE!"
        
        stats_info

    end
end