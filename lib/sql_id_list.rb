module SqlIdList
    def self.get_in_clause(column_name, ids)
        ids.present? ? "#{column_name} IN #{ids.to_sql_list}" : "TRUE"
    end

    def self.get_array_overlap_clause(column_name, ids)
        ids.present? ? "#{column_name} && #{ids.to_sql}" : "TRUE"
    end
end