module SitemapHelper

    def self.domains
        [
            AppConfig.quantic_domain,
            AppConfig.smartly_domain
        ]
    end

    def self.host(domain)
        "https://#{domain}"
    end

    def self.public_path
        "tmp/sitemaps_#{Rails.env}"
    end

    def self.should_write_course_sitemap?(domain)
        domain == AppConfig.quantic_domain
    end

    def self.sitemaps_path(domain)
        "sitemaps/#{domain}"
    end

    def self.compress?
        # do not compress in test mode so we can read the xml
        # and assert on it
        !Rails.env.test?
    end

    def self.sitemap_filename
        self.compress? ? "sitemap.xml.gz" : "sitemap.xml"
    end

    def self.main_sitemap_path(domain)
        "#{public_path}/#{sitemaps_path(domain)}/#{sitemap_filename}"
    end

    def self.sitemap_url(domain)
        "#{host(domain)}/#{sitemap_filename}"
    end

    def self.all_sitemap_urls
        self.domains.map do |domain|
            self.sitemap_url(domain)
        end
    end

    def self.register_route(route, constraint)
        self.registered_routes[route] = constraint
    end

    def self.registered_routes
        @registered_routes ||= {}
    end

    def self.should_include_in_sitemap?(route, domain)
        constraint = SitemapHelper.registered_routes[route] || 

                        # match routes that do not start with a slash
                        SitemapHelper.registered_routes[route.gsub(/^\//, '')] ||

                        # match routes with /(:topic) afterward, since we have so 
                        # many cases like this
                        SitemapHelper.registered_routes["#{route}/(:topic)"]

        raise "No constraint defined for #{route}" unless constraint
        return constraint.matches?(OpenStruct.new(params: {force_host: domain}, domain: domain))
    end

end