'use strict';

/*
	EXHO TEMPLATE  V1.0 BY SUPVIEW.BE


	01. Sticky Navbar
	02. Flexslider ( testimonials )
	03. Share Button
	04. Drag-it img fade Out on Click
	05. Initialise the Carousel
	06. MediaCheck
	07. Animations make-it-appear
	08. VideoBackground
	09. Morphing Buttons
    10. Load the Whole Page
    11. Angular App Code

*/

$(document).ready(function() { // Document ready

    // Smooth scroll anchor links
    var container = $('html,body');
    $('a[href*="#"]').on('click', function(event) {
        event.preventDefault();
        container.animate({
            scrollTop: $(this.hash).offset().top
        }, 750, 'easeOutExpo');
    });

    /*-----------------------------------------------------------------------------------*/
    /*	01. NAVBAR STICKY + SELECTED
    /*-----------------------------------------------------------------------------------*/



    (function() {

        var docElem = $(document),
            didScroll = false,
            changeHeaderOn = 75;

        function init() {
            $(window).scroll(function() {
                if (!didScroll) {
                    didScroll = true;
                    setTimeout(scrollPage, 0);
                }
            });
        }

        function scrollPage() {
            var sy = scrollY();
            if (sy >= changeHeaderOn) {
                $('.cbp-af-header').addClass('cbp-af-header-shrink');
            } else {
                $('.cbp-af-header').removeClass('cbp-af-header-shrink');
            }
            didScroll = false;
        }

        function scrollY() {
            return window.pageYOffset || docElem.scrollTop();
        }

        init();

    })();




    /*-----------------------------------------------------------------------------------*/
    /*	02. FLEXSLIDER - TESTIMONIAL
    /*-----------------------------------------------------------------------------------*/

    $('#slider2').flexslider({
        animation: 'slide',
        directionNav: false,
        controlNav: false,
        smoothHeight: false,
        animationLoop: true,
        slideshowSpeed: 5000,
        slideToStart: 0,
    });


    /*-----------------------------------------------------------------------------------*/
    /*	03. Share Button hover
    /*-----------------------------------------------------------------------------------*/


    $('.cbp-af-header .button.share').hover(
        function() {
            $('.cbp-af-header .social-ico').fadeIn();
        }, // over
        function() {
            $('.cbp-af-header .social-ico').fadeOut();
        } // out
    );

    $('.cbp-af-header .social-ico').hover(
        function() {
            $('.cbp-af-header .social-ico').fadeIn();
        }, // over
        function() {
            $('.cbp-af-header .social-ico').fadeOut();
        } // out
    );


    /*-----------------------------------------------------------------------------------*/
    /*	06. MEDIACHECK
    /*-----------------------------------------------------------------------------------*/

    var isIE9 = $('html').hasClass('ie9');

    if (isIE9) {

        // IE9 won't be able to perform scroll the transitions we'd like, so don't bother
        // doing any of the setup. set opacity and skip to displaying the background video immediately

        $('.make-it-appear-top, .make-it-appear-left, .make-it-appear-right, .make-it-appear-bottom').css('opacity', 1);

        new $.backgroundVideo($('.header-page'), {
            'align': 'centerXY',
            'width': 1280,
            'height': 720,
            'path': '/blue-ocean-strategy/vid/',
            /* Folder wich contain the mp4, ogg and webm video */
            'filename': 'underwater',
            'types': ['mp4', 'webm'] //, "ogg"]
        });

    } else {

        window.mediaCheck({
            media: '(max-width: 768px)',
            entry: function() {
                $('.make-it-appear-top').waypoint(function() {
                    $(this.element).css('opacity', '1');
                }, {
                    offset: '200%'
                });

                $('.make-it-appear-left').waypoint(function() {
                    $(this.element).css('opacity', '1');
                }, {
                    offset: '200%'
                });

                $('.make-it-appear-right').waypoint(function() {
                    $(this.element).css('opacity', '1');
                }, {
                    offset: '200%'
                });

                $('.make-it-appear-bottom').waypoint(function() {
                    $(this.element).css('opacity', '1');
                }, {
                    offset: '200%'
                });


            },
            exit: function() {


                /*-----------------------------------------------------------------------------------*/
                /*  07. ANNIMATIONS MAKE IT APPEAR
                /*-----------------------------------------------------------------------------------*/

                $('.make-it-appear-top').waypoint(function() {
                    $(this.element).addClass('animated fadeInDown');
                }, {
                    offset: '80%'
                });

                $('.make-it-appear-left').waypoint(function() {
                    $(this.element).addClass('animated fadeInLeft');
                }, {
                    offset: '80%'
                });

                $('.make-it-appear-right').waypoint(function() {
                    $(this.element).addClass('animated fadeInRight');
                }, {
                    offset: '80%'
                });

                $('.make-it-appear-bottom').waypoint(function() {
                    $(this.element).addClass('animated fadeInUp');
                }, {
                    offset: '80%'
                });

                $('.bounce').waypoint(function() {
                    $(this.element).addClass('animated bounce');
                }, {
                    offset: '70%'
                });

                $('.pulse').waypoint(function() {
                    $(this.element).addClass('animated pulse');
                }, {
                    offset: '50%'
                });

                /*-----------------------------------------------------------------------------------*/
                /*  08. VIDEO BACKGROUND READY
                /*-----------------------------------------------------------------------------------*/


                new $.backgroundVideo($('.header-page'), {
                    'align': 'centerXY',
                    'width': 1280,
                    'height': 720,
                    'path': '/blue-ocean-strategy/vid/',
                    /* Folder wich contain the mp4, ogg and webm video */
                    'filename': 'underwater',
                    'types': ['mp4', 'webm'] //, "ogg"]
                });

            }


        }); /* END OF THE MEDIACHECK */


    }


}); /* END OF Document Ready */


/*-----------------------------------------------------------------------------------*/
/*	10. Load the Whole Page
/*-----------------------------------------------------------------------------------*/


$(window).on('load', function() {

    // will first fade out the loading animation
    $('#loading-animation').fadeOut();

    // will fade out the whole DIV that covers the website.
    $('#preloader').delay(600).fadeOut('slow');

});