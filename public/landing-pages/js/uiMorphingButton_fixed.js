/**
 * uiMorphingButton_fixed.js v1.0.0
 * http://www.codrops.com
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 *
 * Copyright 2014, Codrops
 * http://www.codrops.com
 */
(function(window) {

    'use strict';

    var transEndEventNames = {
            'WebkitTransition': 'webkitTransitionEnd',
            'MozTransition': 'transitionend',
            'OTransition': 'oTransitionEnd',
            'msTransition': 'MSTransitionEnd',
            'transition': 'transitionend'
        },
        transEndEventName = transEndEventNames[window.Modernizr.prefixed('transition')],
        support = {
            transitions: window.Modernizr.csstransitions
        };

    function extend(a, b) {
        for (var key in b) {
            if (b.hasOwnProperty(key)) {
                a[key] = b[key];
            }
        }
        return a;
    }

    function UIMorphingButton(el, options) {
        this.el = el;
        this.options = extend({}, this.options);
        extend(this.options, options);
        this._init();
    }

    UIMorphingButton.prototype.options = {
        closeEl: '',
        onBeforeOpen: function() {
            return false;
        },
        onAfterOpen: function() {
            return false;
        },
        onBeforeClose: function() {
            return false;
        },
        onAfterClose: function() {
            return false;
        }
    };

    UIMorphingButton.prototype._init = function() {
        // the button
        this.button = $(this.el).find('button');

        // content el
        this.contentEl = $('.morph-content');

        this.overlayEl = $('.overlay');
        // init events
        this._initEvents();
    };

    UIMorphingButton.prototype._initEvents = function() {
        var self = this;
        // open
        this.button.on('click', function() {
            self.toggle();
        });
    };

    UIMorphingButton.prototype.toggle = function() {

        if (this.isAnimating) {
            return false;
        }

        var expanded = this.contentEl.hasClass('open');

        // callback
        if (expanded) {
            this.options.onBeforeClose();
        } else {
            // add class active (solves z-index problem when more than one button is in the page)
            $(this.el).addClass('active');
            this.options.onBeforeOpen();

            // close
            if (this.options.closeEl !== '') {
                var closeEl = this.contentEl.find(this.options.closeEl);
                if (closeEl) {
                    closeEl.on('click', function() {
                        self.toggle();
                    });
                }
            }
        }

        this.isAnimating = true;

        var self = this,
            onEndTransitionFn = function(ev) {

                if (support.transitions) {
                    // open: first opacity then width/height/left/top
                    // close: first width/height/left/top then opacity
                    var propertyName = ev.originalEvent.propertyName;
                    if (expanded && propertyName !== 'opacity' || !expanded && propertyName !== 'width' && propertyName !== 'height' && propertyName !== 'left' && propertyName !== 'top') {
                        return false;
                    }
                    this.removeEventListener(transEndEventName, onEndTransitionFn);
                }

                self.isAnimating = false;

                // callback
                if (expanded) {
                    // remove class active (after closing)
                    $(this.el).removeClass('active');
                    self.options.onAfterClose();
                } else {
                    self.options.onAfterOpen();
                }

            };

        if (support.transitions) {
            this.contentEl.on(transEndEventName, onEndTransitionFn);
        } else {
            onEndTransitionFn();
        }

        // set the left and top values of the contentEl (same like the button)
        var buttonPos = this.button.get(0).getBoundingClientRect();

        // need to reset
        $(this.contentEl).addClass('no-transition');
        this.contentEl.css('left', 'auto');
        this.contentEl.css('top', 'auto');

        // add/remove class "open" to the button wraper
        setTimeout(function() {
            self.contentEl.css('left', buttonPos.left + 'px');
            self.contentEl.css('top', buttonPos.top + 'px');

            if (expanded) {
                $(self.contentEl).removeClass('no-transition');
                $(self.contentEl).removeClass('open');
                $('.morph-button').removeClass('open'); // clear ALL morph button open styles
                $(self.overlayEl).removeClass('open');
                self.contentEl.find(self.options.closeEl).off('click');
            } else {
                setTimeout(function() {
                    $(self.contentEl).removeClass('no-transition');
                    $(self.overlayEl).addClass('open');
                    $(self.el).addClass('open');
                    $(self.contentEl).addClass('open');
                }, 25);
            }
        }, 25);
    };

    // add to global namespace
    window.UIMorphingButton = UIMorphingButton;

})(window);