'use strict';

(function(angular) {

    // set up config defaults
    var landingConfig = angular.extend({
        module: 'GenericLandingPage',
        controller: 'GenericLandingPageCtrl',
        page: 'landing-page',
        pageTitle: 'landing-page-index',
        path: '/landing-page-demo',
        signUpCode: 'LANDINGPAGE',
        facebookConversionImg: undefined,
        googleConversionImg: undefined
    }, window.landingConfig || {});

    // create the application module
    var app = angular.module(landingConfig.module, ['ng-token-auth', 'segmentio']);

    // configure ng-token-auth
    app.config(['$authProvider', '$locationProvider',
        function($authProvider, $locationProvider) {

            // required for oauth handling to work
            $locationProvider.html5Mode(true);

            $authProvider.configure({

                // every API request originating from the ENDPOINT_ROOT
                apiUrl: window.ENDPOINT_ROOT + '/api',

                // defer validation for explicit validation once auth listeners are setup
                validateOnPageLoad: false,

                // required for cordova hybrid
                storage: window.Modernizr && window.Modernizr.localstorage ? 'localStorage' : 'cookies', // we need to fall back to cookies in Safari private mode

                // rely on auth events to perform redirect as necessary
                confirmationSuccessUrl: window.location.origin + '/',
                passwordResetSuccessUrl: window.location.origin + '/settings/profile',

                forceHardRedirect: true, // do oauth in this window
                authProviderPaths: {
                    google_oauth2: '/auth/google_oauth2',
                    facebook: '/auth/facebook',
                    twitter: '/auth/twitter'
                }
            });

        }
    ]);

    app.controller(landingConfig.controller, function($injector) {
        var segmentio = $injector.get('segmentio');

        this.logClick = function(identifier) {
            segmentio.track('user:load-registration-page:' + landingConfig.page + ':' + identifier);
        };
    });

    app.run(['$injector',
        function($injector) {

            var segmentio = $injector.get('segmentio'),
                $http = $injector.get('$http'),
                $location = $injector.get('$location'),
                $auth = $injector.get('$auth'),
                $window = $injector.get('$window');

            $http.get('/api/config.json').then(function(response) {
                if (response && response.contents && response.contents.config && response.contents.config[0]) {
                    var config = response.contents.config[0];
                    segmentio.load(config.segmentio_key);
                    segmentio.page(name, {
                        title: landingConfig.pageTitle,
                        url: window.location.href,
                        path: landingConfig.path
                    });
                }
            });

            // If this is the response to an oauth login,
            // handle it.

            function forwardAfterOauthLogin() {
                if ($location.search().client_id) {
                    $auth.validateUser().then(function() {
                            $window.location.href = '/dashboard';
                        },
                        function() {
                            $window.alert('A login error occurred.');
                        });
                }
            }

            if ($location.search().oauth_registration) {
                forwardAfterOauthLogin();
            } else if ($location.search().client_id) {
                forwardAfterOauthLogin();
            } else if ($location.search().error) {
                $window.location.href = '/join?error=' + $location.search().error;
            }
        }
    ]);

    // create a signup form directive
    app.directive('signupForm', ['$injector',

        function factory($injector) {

            var $auth = $injector.get('$auth'),
                $timeout = $injector.get('$timeout'),
                segmentio = $injector.get('segmentio');

            return {
                template: '<div class="morph-content">' +
                    '<div>' +
                    '<div class="content-style-form content-style-form-2">' +
                    '<span class="icon icon-close"></span>' +
                    '<h2>Almost There!</h2>' +
                    '<p ng-if="!errorMessage" class="form-description">&nbsp;</p>' +
                    '<p ng-if="errorMessage" class="form-description error">{{errorMessage}}</p>' +
                    '<form ng-submit="submitRegistration(form)" role="form" method="post" name="joinForm">' +
                    '<p class="method-p"><span class="method-span">Sign up using</span></p>' +
                    '<button type="button" ng-click="loginWithProvider(\'facebook\')" class="social social-fb-bos"><i class="fa fa-facebook-square"></i> Facebook</button>' +
                    '<button type="button" ng-click="loginWithProvider(\'google_oauth2\')" class="social social-g-bos"><i class="fa fa-google-plus-square"></i> Google</button>' +
                    '<p class="method-p"><span class="method-span">Or</span></p>' +
                    '<p><input type="text" ng-model="form.name" placeholder="Full Name" required="required" spellcheck="false" maxlength="255"></p>' +
                    '<p><input type="email" ng-model="form.email" placeholder="Email" required="required" spellcheck="false" pattern=".+@.+" maxlength="255"></p>' +
                    '<p><input type="password" ng-model="form.password" placeholder="Password" required="required" pattern="(?=.*\\S.*).{8,255}" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"><label class="password-hint">At least 8 characters</label></p>' +
                    '<p class="sign-up-submit-button-container"><span class="terms-span">By submitting I accept the <a href="/terms" target="_blank">terms of service</a>.</span><button type="submit" class="sign-up-submit-button" ng-disabled="joinForm.$invalid || preventSubmit">{{(joinForm.$invalid) ? "Please Complete Above Form" : "Launch The Free Course"}}</button></p>' +
                    '<img height="1" width="1" alt="" style="visibility:hidden" ng-if="facebookConversionImg" ng-src="{{facebookConversionImg}}" />' +
                    '<img height="1" width="1" alt="" style="visibility:hidden" ng-if="googleConversionImg" ng-src="{{googleConversionImg}}" />' +
                    '</form>' +
                    '</div>' +
                    '</div>' +
                    '</div>',
                scope: {},
                restrict: 'E',
                link: function(scope) {
                    scope.preventSubmit = false;
                    scope.form = {};

                    scope.loginWithProvider = function(provider) {
                        scope.preventSubmit = true;

                        $auth.authenticate(provider, {
                            params: {
                                sign_up_code: landingConfig.signUpCode,
                                pref_locale: window.serverDeterminedLocale
                            }
                        }).catch(function() {
                            scope.preventSubmit = false;
                        });
                    };

                    scope.submitRegistration = function() {

                        scope.preventSubmit = true;

                        scope.form.sign_up_code = landingConfig.signUpCode;

                        $auth.submitRegistration(scope.form).then(function(response) {

                                scope.facebookConversionImg = landingConfig.facebookConversionImg;
                                scope.googleConversionImg = landingConfig.googleConversionImg;

                                var user = response.data.data;

                                var path = '/virtual/' + landingConfig.page + '/registration-success';
                                var name = landingConfig.page + ' registration success';
                                segmentio.page(name, {
                                    title: name,
                                    url: window.location.origin + path,
                                    path: path
                                });

                                segmentio.identify(user.id, {
                                    email: user.email
                                });
                                segmentio.track('user:register:' + landingConfig.page);

                                scope.preventSubmit = false;
                                $timeout(function() {
                                    window.location = window.ENDPOINT_ROOT + '/dashboard';
                                }, 750);

                            },
                            function(response) {

                                scope.preventSubmit = false;

                                // TODO: this response object is ideal for form validators. We should probably switch to using that.
                                if (response && response.data && response.data.errors) {
                                    scope.errorMessage = '';
                                    angular.forEach(response.data.errors, function(fieldErrors, field) {
                                        var formattedField;
                                        switch (field) {
                                            case 'name':
                                                formattedField = 'Full Name';
                                                break;
                                            case 'last_name':
                                                formattedField = 'Last Name';
                                                break;
                                            case 'email':
                                                formattedField = 'Email';
                                                break;
                                            case 'password':
                                                formattedField = 'Password';
                                                break;
                                            default:
                                                return; // skip to next error
                                        }

                                        scope.errorMessage += fieldErrors[0] + '. ';
                                    });
                                } else {
                                    scope.errorMessage = 'There was a problem registering. Please check the form and try again!';
                                }
                            });
                    };

                    /*-----------------------------------------------------------------------------------*/
                    /*  Morphing Buttons Initialization
                    /*-----------------------------------------------------------------------------------*/


                    var docElem = window.document.documentElement,
                        didScroll,
                        scrollPosition;

                    // trick to prevent scrolling when opening/closing button
                    function noScrollFn() {
                        window.scrollTo(scrollPosition ? scrollPosition.x : 0, scrollPosition ? scrollPosition.y : 0);
                    }

                    function noScroll() {
                        $(window).off('scroll.scrollPrevent');
                        $(window).on('scroll.scrollPrevent', noScrollFn);
                    }

                    function scrollFn() {
                        $(window).on('scroll.scrollPrevent', scrollHandler);
                    }

                    function canScroll() {
                        $(window).off('scroll.scrollPrevent');
                        scrollFn();
                    }

                    function scrollHandler() {
                        if (!didScroll) {
                            didScroll = true;
                            setTimeout(function() {
                                scrollPage();
                            }, 60);
                        }
                    }

                    function scrollPage() {
                        scrollPosition = {
                            x: window.pageXOffset || docElem.scrollLeft,
                            y: window.pageYOffset || docElem.scrollTop
                        };
                        didScroll = false;
                    }

                    scrollFn();


                    [].slice.call(document.querySelectorAll('.morph-button')).forEach(function(bttn) {
                        new window.UIMorphingButton(bttn, {
                            closeEl: '.icon-close',
                            onBeforeOpen: function() {
                                // don't allow to scroll
                                noScroll();
                            },
                            onAfterOpen: function() {
                                // can scroll again
                                canScroll();
                            },
                            onBeforeClose: function() {
                                // don't allow to scroll
                                noScroll();
                            },
                            onAfterClose: function() {
                                // can scroll again
                                canScroll();
                            }
                        });
                    });
                    //

                }
            };

        }
    ]);

})(angular);