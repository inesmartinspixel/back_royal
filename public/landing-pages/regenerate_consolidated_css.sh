#!/bin/bash

# Regenerate BOS style

cat css/gumby.css css/component.css css/animate.css css/style.css css/smartly.css css/content.css ../blue-ocean-strategy/css/src/overrides.css > ../blue-ocean-strategy/css/consolidated.css

# Regenerate Transform Africa style

cat css/gumby.css css/component.css css/animate.css css/style.css css/smartly.css css/content.css ../transform-africa/css/src/overrides.css > ../transform-africa/css/consolidated.css

# Regenerate WWF style

cat css/gumby.css css/component.css css/animate.css css/style.css css/smartly.css css/content.css ../wwf/css/src/overrides.css > ../wwf/css/consolidated.css