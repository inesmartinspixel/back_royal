const minifyHtmlTemplate = require('../lib/node/TemplateBuildTools/minifyHtmlTemplate');

// eslint-disable-next-line func-names
module.exports = function (content) {
    this.cacheable(true);
    return minifyHtmlTemplate(content);
};
