const TerserPlugin = require('terser-webpack-plugin');
const getWebpackConfig = require('../webpack.config');

module.exports = (env, argv) => {
    const webpackConfig = getWebpackConfig(env, argv);

    //---------------------------
    // Production optimizations
    //---------------------------

    Object.assign(webpackConfig.optimization, {
        minimizer: [
            // Minify JS
            new TerserPlugin({
                // See webpack 5 warning -- https://webpack.js.org/plugins/terser-webpack-plugin/#cache
                cache: true,

                extractComments: false,

                // FIXME: See CircleCI note -- https://webpack.js.org/plugins/terser-webpack-plugin/#parallel
                parallel: 7,

                sourceMap: true,
                terserOptions: {
                    warnings: false,
                    mangle: {
                        reserved: ['$super'],
                    },
                    output: {
                        ascii_only: true,
                        comments: false,
                    },
                },
            }),
        ],
    });

    //---------------------------
    // Source maps
    //---------------------------

    const isCordovaBuild = !!(env && env.cordova);
    webpackConfig.devtool = !isCordovaBuild ? 'source-map' : '';

    return webpackConfig;
};
