/* eslint-disable no-console */
const fs = require('fs');
const crypto = require('crypto');

module.exports.copyAndStampModulesLocales = isDevServer => {
    const src = 'tmp/modules/locales/';
    const dest = 'public/assets/locales/modules/';

    if (!fs.existsSync(src)) {
        return;
    }

    if (!fs.existsSync(dest)) {
        fs.mkdirSync(dest, { recursive: true });
    }

    fs.readdirSync(src).forEach(filePath => {
        let fileName = filePath.slice(0, filePath.lastIndexOf('.'));
        const ext = `.${filePath.split(/\./).pop()}`;

        if (!isDevServer) {
            const fileContents = fs.readFileSync(`${src}/${filePath}`, 'utf8');
            const hash = crypto
                .createHash('md4') // Same algo Webpack uses
                .update(fileContents) // Update with contents of file (so it changes when file changes)
                .digest('hex'); // Digest the content
            fileName = `${fileName}-${hash.substr(0, 8)}`;
        }
        fs.copyFileSync(`${src}${filePath}`, `${dest}${fileName}${ext}`);
    });
};
