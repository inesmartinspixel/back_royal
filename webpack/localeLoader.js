// See lib/node/TranslationBuildTools/README.md for an overview of how locales are built
const getOptions = require('loader-utils').getOptions;
const stripJSONComments = require('strip-json-comments');
const fs = require('fs');
const deepmerge = require('deepmerge');
const getPartsFromFilepath = require('../lib/node/TranslationBuildTools/getPartsFromFilepath');
const parseJsonFile = require('../lib/node/TranslationBuildTools/parseJsonFile');
const buildNestedObject = require('../lib/node/TranslationBuildTools/buildNestedObject');

// Note: Code that is written as a string here is not run through Babel,
// so it needs to be ES5 until we drop IE11, or figure out a way to programatically
// call Babel. We had initially used the spread operator,
// then rewrote it to the ES5 suggestion in https://stackoverflow.com/a/171256/1747491
function generateJsThatAddsLocalesToWindowDotSmartly(locale, content, filepath) {
    // Parse the filepath to the file and come up with an
    // array of keys representing a path to this locale file
    const parts = getPartsFromFilepath(filepath);

    content = stripJSONComments(content);

    let js = '';
    let objName = `window.Smartly.locales.modules.${locale}`;
    parts.forEach(part => {
        objName = `${objName}.${part}`;

        // i.e. `window.Smartly.locales.modules.en.careers.edit_hiring_manager_profile || {}`
        js += `${objName} = ${objName} || {};\n`;
    });

    // i.e. window.Smartly.locales.modules.en.careers.edit_hiring_manager_profile['someKey'] = content['someKey']
    js += `const content = ${content};`;
    js += `for (var attrname in content) { ${objName}[attrname] = content[attrname]; }`;

    return `
        window.Smartly = window.Smartly || {};
        window.Smartly.locales = window.Smartly.locales || {};
        window.Smartly.locales.modules = window.Smartly.locales.modules || {};
        window.Smartly.locales.modules.${locale} = window.Smartly.locales.modules.${locale} || {};
        ${js}
    `;
}

function ensureDir(dir) {
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir, { recursive: true });
    }
    return dir;
}

function parseJsonFileOrEmptyObject(filepath) {
    let outputObject;
    try {
        outputObject = parseJsonFile(filepath);
    } catch (err) {
        outputObject = {};
    }

    return outputObject;
}

function appendToLocaleFile(locale, criticalLocale, criticalLocaleFilePath) {
    // ensure the directory exists
    const destDir = ensureDir('tmp/modules/locales/');
    const destFile = `${destDir}/locales-${locale}.json`;

    // Get the parsed contents of the current output file,
    // or an empty object if it is not created yet
    let outputObject = parseJsonFileOrEmptyObject(destFile, locale);

    const filepath = criticalLocaleFilePath.replace(`-${criticalLocale}.json`, `-${locale}.json`);
    const nestedObject = buildNestedObject(filepath);
    outputObject = deepmerge(outputObject, nestedObject);
    const content = JSON.stringify(outputObject);

    fs.writeFileSync(destFile, content);
}

// eslint-disable-next-line func-names
module.exports = function (content) {
    this.cacheable(true);
    const options = getOptions(this);

    const localesToWrite = ['ar', 'en', 'es', 'it', 'sw', 'zh'];
    localesToWrite.splice(localesToWrite.indexOf(options.criticalLocale), 1);

    // For all non-critical locales, write a file at
    // /tmp/modules/locales-xx.json
    localesToWrite.forEach(locale => {
        try {
            appendToLocaleFile(locale, options.criticalLocale, this.resourcePath);
        } catch (err) {
            // In dev mode, you may not have generated the other locale files yet.
            // We don't want to error in that case
            if (err && err.code === 'ENOENT' && options.isDevServer && options.criticalLocale !== locale) {
                return;
            }
            // eslint-disable-next-line no-console
            console.error(err.code, options);
            throw err;
        }
    });

    // For critical locale, return javascript that will be evaluated in the browser
    // to add these locales to window.Smartly.locales.${criticalLocale}.
    return generateJsThatAddsLocalesToWindowDotSmartly(options.criticalLocale, content, this.resourcePath);
};
