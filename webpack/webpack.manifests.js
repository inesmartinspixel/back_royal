/* eslint-disable no-console */
const fileSystem = require('file-system');
const fs = require('fs');

//---------------------------
// Cordova
//---------------------------

function buildCordovaManifest() {
    console.log('\x1b[36m[START]\x1b[0m building webpack_cordova_manifest');
    const start = new Date().getTime();

    let output = {};
    const dir = 'config/';
    const matchers = ['webpack_manifest.json', 'webpack_locale_manifest.json']; // match only this file
    const dest = 'hybrid/www/';

    // If the destination folder doesn't exist, skip manifest creation.
    // The hybrid/www/ directory will not (and should not) exist on our
    // CI or AWS boxes, but will exist on developer machines.
    if (!fs.existsSync(dest)) {
        console.log(`No ${dest} directory found, skipping...`);
        return;
    }

    // Recursively traverse the directory and match anything in `matchers`
    fileSystem.recurseSync(dir, matchers, filepath => {
        const json = fs.readFileSync(filepath, 'utf8');
        output = { ...output, ...JSON.parse(json) };
    });
    output = JSON.stringify(output);

    if (output) {
        output = `'use strict';\nwindow.webpackManifest = ${output};`;

        // Synchronously write the file
        fs.writeFileSync(`${dest}webpack_cordova_manifest.js`, output);
        console.log(`\x1b[35m[EMITTED]\x1b[0m ${dest}webpack_cordova_manifest.js`);
    }

    console.log(
        `\x1b[42m[FINISHED]\x1b[0m building webpack_cordova_manifest after ${(
            (new Date().getTime() - start) /
            1000
        ).toString()}s`,
    );
}

//---------------------------
// Assets
//---------------------------

function buildManifestfile(isDevServer, directories, outputFileName) {
    console.log(`\x1b[36m[START]\x1b[0m building ${outputFileName}`);
    const start = new Date().getTime();

    const fileMappings = {};
    const dest = 'config/';

    directories.forEach(dir => {
        const matchers = ['**/*.{png,jpg,jpeg,svg,json}']; // match only this file

        // Recursively traverse the directory and match anything in `matchers`
        fileSystem.recurseSync(dir, matchers, filepath => {
            let path;
            if (isDevServer) {
                path = filepath.slice(0, filepath.lastIndexOf('.')).replace(/^public/g, '');
            } else {
                path = filepath
                    .slice(0, filepath.lastIndexOf('.'))
                    .replace(/-[a-z0-9]{8}$/gi, '')
                    .replace(/^public/g, '');
            }

            const ext = `.${filepath.split(/\./).pop()}`;
            const key = path.concat(ext);

            // No need for duplicates
            if (!fileMappings[key]) {
                fileMappings[key] = filepath.replace(/^public/g, '');
            }
        });
    });

    if (Object.keys(fileMappings).length > 0) {
        const output = JSON.stringify(fileMappings, null, 4);

        // If the destination folder doesn't exist, create it
        if (!fs.existsSync(dest)) {
            fs.mkdirSync(dest);
        }

        // Synchronously write the file
        fs.writeFileSync(`${dest}${outputFileName}`, output);
        console.log(`\x1b[35m[EMITTED]\x1b[0m ${dest}${outputFileName}`);
    }

    console.log(
        `\x1b[42m[FINISHED]\x1b[0m building ${outputFileName} after ${(
            (new Date().getTime() - start) /
            1000
        ).toString()}s`,
    );
}

module.exports.buildManifests = isDevServer => {
    // NOTE: we need a manifest for built assets (specifically images and vectors),
    // so we can cache bust those assets in our Rails views.
    buildManifestfile(isDevServer, ['public/assets/images/', 'public/assets/vectors/'], 'webpack_asset_manifest.json');
    buildManifestfile(isDevServer, ['public/assets/locales/'], 'webpack_locale_manifest.json');

    buildCordovaManifest();
};

module.exports.buildAssetManifest = () => {
    buildManifestfile(true, ['public/assets/images/', 'public/assets/vectors/'], 'webpack_asset_manifest.json');
};
