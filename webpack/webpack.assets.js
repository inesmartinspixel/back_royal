/* eslint-disable no-console */
const fileSystem = require('file-system');
const fse = require('fs-extra');
const fs = require('fs');
const rimraf = require('rimraf');
const chokidar = require('chokidar');

//--------------------------
// Helpers
//--------------------------

const copyAssetToTmp = path => {
    const klass = path.split('/')[1];
    const relative = path.split('_minified/')[1];
    const dest = `tmp/${klass}/${relative}`;
    fse.copySync(path, dest);
    console.log(`\x1b[35m[COPIED]\x1b[0m ${path} to ${dest}`);
};

const copyAllAssetsToTmp = () => {
    ['images', 'vectors'].forEach(klass => {
        const dirs = [`vendor/${klass}/_minified`];
        const matchers = ['**/*.{png,jpg,jpeg,svg}'];
        const dest = `tmp/${klass}/`;

        const pathsMap = {};
        dirs.forEach(dir => {
            // Recursively traverse the directory and match anything in `matchers`
            fileSystem.recurseSync(dir, matchers, (filepath, relative) => {
                pathsMap[filepath] = relative;
            });
        });

        Object.keys(pathsMap).forEach(key => {
            // https://github.com/jprichardson/node-fs-extra/blob/HEAD/docs/copy.md
            fse.copySync(key, dest + pathsMap[key]);
        });
    });

    console.log('\x1b[35m[COPIED]\x1b[0m non-revisioned assets to /tmp');
};

const buildMarketingAssetDependencies = () => {
    console.log('\x1b[36m[START]\x1b[0m building marketing assets');
    const start = new Date().getTime();

    let requireStatements = '';
    let requireCount = 0;

    const dir = 'app/views/';
    const matchers = ['**/*.erb'];
    const dest = 'tmp/marketing/';
    const processedAssets = {};

    // Recursively traverse the directory and match anything in `matchers`
    fileSystem.recurseSync(dir, matchers, (filepath, relative, filename) => {
        if (!filename) {
            return; // If this is just a folder, return
        }

        const content = fs.readFileSync(filepath, 'utf8'); // readFileSync is synchronous
        const matches = content.match(/assets\/(images|vectors)\/.*?\.(png|svg|jpg|jpeg)/g) || [];

        matches.forEach(match => {
            let variableName = `assetUrl${requireCount.toString()}`;

            // Prepend variableName and the associated require to output
            const assetPath = match.replace(/assets\//, '');

            // Don't bother attempting to process the asset if we've already processed it.
            if (processedAssets[assetPath]) {
                return;
            }

            requireStatements += `const ${variableName} = require('${assetPath}');\n`;
            processedAssets[assetPath] = true;
            requireCount += 1;

            // As part of the brand name change from Smartly to Quantic, certain images/vectors that included
            // the brand name and/or logo were duplicated and then slightly altered to be specific to Quantic.
            // The file names for the Quantic version of these assets should match the Smartly version but be
            // appended with `_quantic` right before the file extension. For example, if a Smartly asset has
            // a file name of `tmp/marketing/images/foo.png` then the Quantic version of the asset would have
            // a file name of `tmp/marketing/images/foo_quantic.png`. The logic below ensures that the Quantic
            // version of the asset is required and therefore picked up by webpack and included in the webpack
            // asset manifest.
            const i = assetPath.search(/\.\w+$/);
            const quanticAssetPath = [assetPath.slice(0, i), '_quantic', assetPath.slice(i)].join('');

            // Don't bother attempting to process the asset if we've already processed it.
            if (processedAssets[quanticAssetPath]) {
                return;
            }

            // Only require the Quantic version of the asset if it exists.
            if (fs.existsSync(`tmp/${quanticAssetPath}`)) {
                variableName = `assetUrl${requireCount.toString()}`;
                requireStatements += `const ${variableName} = require('${quanticAssetPath}');\n`;
                processedAssets[quanticAssetPath] = true;
                requireCount += 1;
            }
        });
    });

    if (requireStatements) {
        const output = requireStatements;

        // If the destination folder doesn't exist, create it
        if (!fs.existsSync(dest)) {
            fs.mkdirSync(dest);
        }

        // Write the file

        fs.writeFileSync(`${dest}marketing_assets.js`, output);
        console.log(`\x1b[35m[EMITTED]\x1b[0m ${dest}marketing_assets.js`);
    }

    console.log(
        `\x1b[42m[FINISHED]\x1b[0m building marketing assets after ${(
            (new Date().getTime() - start) /
            1000
        ).toString()}s`,
    );
};

const cleanWorkingDirs = () => {
    rimraf.sync('./public/assets/');
    rimraf.sync('./public/oneTrust');
    rimraf.sync('./public/oneTrust-quantic');
    rimraf.sync('./tmp/common');
    rimraf.sync('./tmp/front-royal');
    rimraf.sync('./tmp/reports');
    rimraf.sync('./tmp/admin');
    rimraf.sync('./tmp/editor');
    rimraf.sync('./tmp/marketing');
    rimraf.sync('./tmp/modules');
};

const copyMiscAssetDirs = isCordovaBuild => {
    // Assets needed for both web and cordova applications
    const baseDirMappings = {
        'node_modules/@fortawesome/fontawesome-pro/webfonts': 'public/assets/fontawesome/v5/webfonts',
        'node_modules/@fortawesome/fontawesome-pro/css': 'public/assets/fontawesome/v5/css',
        'node_modules/mathjax': 'public/assets/mathjax',
        'node_modules/arabic-mathjax/dist': 'public/assets/arabic-mathjax',
        'vendor/front-royal/modules/Onetrust/static/oneTrust/logos': 'public/oneTrust/logos',
        'vendor/front-royal/modules/Onetrust/static/oneTrust/skins': 'public/oneTrust/skins',
        'vendor/front-royal/modules/Onetrust/static/oneTrust-quantic/logos': 'public/oneTrust-quantic/logos',
        'vendor/front-royal/modules/Onetrust/static/oneTrust-quantic/skins': 'public/oneTrust-quantic/skins',
    };

    // Assets needed only for web application
    const webDirMappings = {
        'node_modules/fullcalendar/dist/fullcalendar.min.css': 'public/assets/styles/fullcalendar.min.css',
    };

    const dirMappings = isCordovaBuild
        ? baseDirMappings
        : {
              ...baseDirMappings,
              ...webDirMappings,
          };

    Object.keys(dirMappings).forEach(key => {
        fse.copySync(key, dirMappings[key]);
        console.log(`\x1b[35m[COPIED]\x1b[0m ${key} to ${dirMappings[key]}`);
    });
};

const setupAssetWatches = () => {
    const imageWatcher = chokidar.watch('./vendor/images/_minified/**/*.{png,jpg,jpeg,gif}', {
        ignoreInitial: true,
        usePolling: true,
    });
    imageWatcher.on('add', path => copyAssetToTmp(path)).on('change', path => copyAssetToTmp(path));

    const vectorWatcher = chokidar.watch('./vendor/vectors/_minified/**/*.svg', {
        ignoreInitial: true,
        usePolling: true,
    });
    vectorWatcher.on('add', path => copyAssetToTmp(path)).on('change', path => copyAssetToTmp(path));
};

//--------------------------
// Module Export
//--------------------------

module.exports.initialize = (isDevServer, isCordovaBuild) => {
    // Initial Cleanup
    cleanWorkingDirs();

    // Copy non-revisioned images/vectors to tmp/
    copyAllAssetsToTmp();

    // Copy fonts, misc CSS, etc
    copyMiscAssetDirs(isCordovaBuild);

    // Generate a JS file in vendor/marketing/tmp that requires
    // all of the images/vectors referenced from embedded ruby files.
    if (!isCordovaBuild) {
        buildMarketingAssetDependencies();
    }

    // Monitor assets outside of Webpack dependency chain for changes.
    // This will trigger changes to `tmp/<packageName` files which in turn propagates
    // to watches in the Webpack dependency chain, triggering recompilation.
    if (isDevServer) {
        setupAssetWatches();
    }
};

module.exports.buildMarketingAssetDependencies = buildMarketingAssetDependencies;
