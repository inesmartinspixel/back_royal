const webpack = require('webpack');
const open = require('open');

const TimeFixPlugin = require('time-fix-plugin');
const DashboardPlugin = require('webpack-dashboard/plugin');

const getWebpackConfig = require('../webpack.config');

module.exports = async (env, argv) => {
    //---------------------------
    // Webpack config
    //---------------------------

    const webpackConfig = getWebpackConfig(env, argv);
    const host = 'localhost';
    const port = 3001;
    const publicPath = `http://${host}:${port}`;
    let devServerOpen = false;

    webpackConfig.devServer = {
        after: (app, server, compiler) => {
            // HACK: webpack-dev-server supports an `open` property,
            // but there's no way to tell it to defer opening until
            // the compilation is complete without actually tapping
            // into a compiler lifecycle hook.
            compiler.hooks.done.tap('OpenBrowserPlugin', async () => {
                if (!devServerOpen) {
                    await open(publicPath);
                    devServerOpen = true;
                }
            });
        },
        host,
        hot: true,
        overlay: true,
        port,
        proxy: [
            {
                context: ['**'],
                pathRewrite: path => path.replace(/^assets/, 'public/assets/'),
                target: `http://[::1]:3000`,
            },
        ],
        publicPath,
        watchOptions: {
            ignored: [/node_modules\//, /hybrid\//, /^config\//, /public\//, /spec.js$/],
        },
        writeToDisk: true,
    };

    webpackConfig.plugins.push(
        // https://github.com/webpack/watchpack/issues/25
        new TimeFixPlugin(),
    );

    webpackConfig.plugins.push(new DashboardPlugin());

    webpackConfig.plugins.push(new webpack.HotModuleReplacementPlugin());

    return webpackConfig;
};
