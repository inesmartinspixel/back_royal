Stripe
======

[Stripe](https://stripe.com) is the payment provider / API SaaS we use for all transactions in our paid services offerings.


----

## General


### Environments

Stripe contains a [Production](https://dashboard.stripe.com/dashboard) environment and a [Test](https://dashboard.stripe.com/test/dashboard) environment. There is a toggle in the main dashboard sidebar. Be sure to pay attention **at all times** to this and other visual cues to ensure you are operating in the correct environment.

No data / configuration is shared between the environments, except for the **Billing > Settings** section, which appears to be applied globally.

For Webhook responses, API updates can be tested via webhook settings in the Test environment, whereas the Production environment always relies on the current, production-defined API version.

For general API updates, the target API version can be specified via the `Stripe.api_version` (see also: `stripe.rb` initializer).


### Stripe Concepts

Please take a few minutes to familiarize yourself with the following concepts before proceeding:

- [Customers](https://stripe.com/docs/api#customers)
- [Products](https://stripe.com/docs/api#products)
- [Plans](https://stripe.com/docs/api#plans)
- [Subscriptions](https://stripe.com/docs/api#subscriptions)
- [Invoices](https://stripe.com/docs/api#invoices)
- [Charges](https://stripe.com/docs/api#charges)


----


## Implementation Details


### API Usages

The majority of our payment business logic is implemented in `StripeEventHandler` hooks that respond to specific [events](https://stripe.com/docs/api#events). The webhook endpoint URL is specified in the Stripe Dashboard **Developers > Webhooks** section. We log events for certain Stripe events that land, in addition to any business logic we perform. Events contain the object data relevant to the event type (ie a `Charge` object)

The comprehensive list of events we log and process is as follows:

```ruby
[
    "charge.captured",
    "charge.dispute.closed",
    "charge.dispute.created",
    "charge.dispute.funds_reinstated",
    "charge.dispute.funds_withdrawn",
    "charge.dispute.updated",
    "charge.expired",
    "charge.failed",
    "charge.pending",
    "charge.refund.updated",
    "charge.refunded",
    "charge.succeeded",
    "charge.updated",
    "customer.created",
    "customer.deleted",
    "customer.subscription.created",
    "customer.subscription.deleted",
    "customer.subscription.trial_will_end",
    "customer.subscription.updated",
    "customer.updated",
    "invoice.created",
    "invoice.payment_failed",
    "invoice.payment_succeeded",
    "invoice.upcoming"
]
```

Payment processing generally is initiated via our `SubscriptionsController`'s `create` and `update` actions. The UI interfaces with [Stripe Checkout](https://stripe.com/checkout) to collect card information and provide us with an access token. It's important to note that we never know the credit card through this flow (Stripe insulates us from any sort of liability in that respect). Once we have the credit card, we set it as the default, then call into one of the controller actions. At this point, Stripe API use is fairly straightforward in creating a subscription and performing any back-payment billing. The hooks handle the rest of the Stripe event flow.


### Smartly Data Model

We have three models primarily relevant to Stripe payment details:

 - The `Cohort` model contains information about what `stripe_plans` and `scholarship_levels` are valid / available for a corresponding `CohortApplication`.

       - These are set in the Cohorts Admin section of our app. Each `Cohort` (if a billable program type) will contain an interface for selecting available Stripe [Plans](https://stripe.com/docs/api#plans) and **Scholarship Levels** (implemented using [Stripe Coupons](https://stripe.com/docs/api#coupons)).


 - A learner's `CohortApplication` record contains information regarding `stripe_plan_id`, `total_num_required_stripe_payments`, `registered`, `num_charged_payments`, `num_refunded_payments`, `scholarship_level`, and `locked_due_to_past_due_payment`. If a learner is deferred and then re-assigned a new `CohortApplication` entry, then these values will be forward-populated to the new entry so that billing state can be resumed.

       - These can be modified via the Users Admin section of our app. A `User`'s  `CohortApplication` entries will provide options for selecting a `scholarship_level` based on the `Cohort`'s  available `scholarship_levels`.

       - Once a user successfully initiates a new subscription, this model will be snapshotted with their `stripe_plan_id` and `registered` will be set to `true`.

       - A learner's `CohortApplication` has a derived `payment_past_due` value that is calculated based on Subscription `past_due` status (see below) and existing registration / program status.

       - Payment is tracked on the `CohortApplication` via the `num_charged_payments` and `num_refunded_payments` values, which get incremented in respective hooks. If `num_charged_payments > num_refunded_payment` and a user is de-registered for any reason, they will not have the option of enrolling in a new plan and will be re-registered into their existing `stripe_plan_id` plan.

       - A user completes their subscription cycle once `(num_charged_payments - num_refunded_payments) === total_num_required_stripe_payments` at which point their `Subscription` is cancelled.


 - A user's `Subscription` contains values for `stripe_plan_id`, `stripe_subscription_id`, and `past_due`. The user's `primary_subscription` reflects the current relevant `Subscription`

       - Once registration succeeds, a new `primary_subscription` will be created and will map to the Stripe [Subscription](https://stripe.com/docs/api#subscription)'s ID.

       - The `Subscription` will be "reconciled" with the Stripe subscription whenever it is appropriate. This lets us sync status for setting things such as `past_due`

       - `past_due` is set after the **Billing > Settings** *Dunning Management* re-billing scheme has completd without a successful payment. At this point, we nag in the UI, requiring the user to *Modify Payment Details*

       - Once the user re-registeres, any unpaid Stripe [Invoices](https://stripe.com/docs/api#invoices) associated with the subscription will be re-charged in attempts to collect all past-due payments. If the set is successful, the `past_due` status will be reset.

       - Subscriptions can be cancelled via the Users Admin portion of our app.

       - Once a `Subscription` goes into `past_due` status, it will be picked up by a job to block content access once that job sets the corresponding `CohortApplication`'s `locked_due_to_past_due_payment` status.


### Notifications

Notifications are sent to the `#payments` and `#payments-test` Slack channels based on respective environment. All notifications are handled in `StripeEventHandler` and are limited to the basic events around subscription start or cancellation, as well as invoice payment success or failure.


----


## Local Testing Notes

Since you are most likely using a snapshot database, it is important to know that because the Stripe environments are segmented, **the subscription and payment state in your DB for existing user most likely will not match the state found in Stripe Test environment.** As a general rule, for this reason, it may be easier to create new users as needed for each testing scenario.

You will need to establish a publicly accessible `ngrok` tunnel and point the **Test environment hooks** to this endpoint.

If you want to monitor Stripe activity in the `#payments-test` channel, be sure to configure your `application.yml` to enable Slack and Delayed Job processing for `log_to_slack`, then run `bin/delayed_job start` (shutting it down when you're done with `bin/delayed_job stop`).

If you want to test any event types not mentioned above in `API Usages` , i.e. for feature implementation, you will need to manually add the event type to the whitelist in the webhook settings on Stripe.


----


## Advanced Testing Scenarios

### Past Due / Multiple Unpaid Invoices

1. Find an appropriate customer with a subscription in both local DB and Test environment, or create anew

1. Assign the customer a new **default** credit card that will fail (ie `4000 0000 0000 0341`)

1. Invoice the user by using the **Add Invoice Item** button (not "Create Invoice"), specifying the **Subscription ID** and amount

1. Click the **Invoice Now** button

1. Repeat a few times to build up a collection of failed invoices associated with the subscription

1. Manually update the internal user's `primary_subscription`'s `past_due` to `true`

1. Login as the user and navigate to the **registration page**

1. Register, supplying a **valid credit card** -- This will begin the charging cycle

#### To test the some complete / one fails scenario:

1. Before registration in the above flow, uncomment the `sleep` in `Subscription#charge_unpaid_invoices`

1. Perform registration with valid card (as above)

1. During this `sleep`, **reload the Stripe Customer page**

1. Change the card back to a card that would fail (`0341`) so that the next charge will fail

1. Verify in the UX that the partially-complete


----


## Stripe CLI testing/prototyping - ONLY FOR /test/ ENVIRONMENT!

1. If you don't already have one, create a personal API key for CLI use
    1. Navigate to https://dashboard.stripe.com/test/apikeys
    1. Under "Restricted Keys", click on "+ Create restricted key"
    1. Give your key the name `test_stripe_cli_<username>`
    1. Give your key `write` permissions for the following:
        1. Charges
        1. Customers
        1. Invoices
    1. Note that the above permissions corresponds to the namespaces of events we receive to our webhook endpoint from Stripe. If you want to do more robust testing, you may need to add permissions as you go. The CLI will fail if you try to do something you don't have permission to do. An example would be creating/updating/destroying a Subscription.

1. Download the Stripe CLI - `$ brew install stripe/stripe-cli/stripe`

1. Login to the Stripe CLI using your API key: `stripe login --interactive`
    1. Paste your API key and hit enter to configure
    1. Note that we do not use the default `stripe login` or `stripe login --api-key=''` commands here - both of those commands automatically create API keys for both test and live environments, which we do not want.
    1. Once complete, you should see your device under "Devices receiving events with the Stripe CLI" [here](https://dashboard.stripe.com/test/webhooks)

1. In a terminal window, listen for Stripe events: `stripe listen --forward-to localhost:3000/payment/hooks`
    1. In `application.yml`, update `STRIPE_WEBHOOK_SECRET` to be the temporary signing secret provided by this command

1. In another terminal window, trigger Stripe events: `stripe trigger charge.succeeded`

1. You've now setup the CLI, you can find out how to do more advanced testing in the [docs](https://stripe.com/docs/stripe-cli)
