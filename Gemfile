#####################################
## Core
#####################################

source 'https://rubygems.org'

# this allows a bit more flexibility, since the 'ruby' directive lock at patch-levels
fail 'Ruby 2.x.x required' unless RUBY_VERSION =~ /^2\./

gem 'rails', '6.0.3.2'
gem 'puma'


#####################################
## Authentication / Authorization
#####################################

gem 'cancancan'
gem 'devise_token_auth', git: 'https://github.com/lynndylanhurley/devise_token_auth', branch: 'master'
gem 'rolify'
gem 'omniauth'
gem 'omniauth-facebook'
gem 'omniauth-google-oauth2'
gem 'omniauth-twitter'
gem 'omniauth-saml'
gem 'omniauth-apple', git: 'https://github.com/nhosoya/omniauth-apple.git', branch: 'master'
gem 'httparty'
gem 'rack-cors', require: 'rack/cors'
gem 'recaptcha', require: 'recaptcha/rails'


#####################################
## Database / Data-Model
#####################################

gem 'pg'
gem 'acts_as_list'
gem 'annotate'
gem 'ar-find-in-batches-with-order'
gem 'webdack-uuid_migration' # used to update mailboxer to use uuids, might be helpful elsewhere as well
gem 'ctes_in_my_pg', git: 'https://github.com/kmurph73/ctes_in_my_pg.git', branch: 'master'
gem 'marginalia'



#####################################
## Redis Caching
#####################################

gem 'redis'
gem 'hiredis'


#####################################
## Airtable
#####################################

gem 'airrecord'


#####################################
## Google
#####################################

gem 'google_places' # for backfilling Google Place data


#####################################
## JSON (Performance)
#####################################

gem 'oj'
gem 'oj_mimic_json'


#####################################
## Images / S3 + Misc AWS
#####################################

gem 'paperclip'
gem 'posix-spawn'
gem 'aws-sdk-s3'
gem 'aws-sdk-cloudwatch'
gem 'aws-sdk-firehose'


#####################################
## Monitoring / Analytics
#####################################

gem 'skylight'
gem 'sentry-raven'
gem 'customerio'
gem 'simple_segment'             # See https://segment.com/docs/sources/server/ruby/#install-the-gem
gem 'slack-ruby-client'


#####################################
## Miscellaneous
#####################################

# messaging gem used in careers for messaging between hiring managers and candidates
gem 'mailboxer', git: 'https://github.com/booleanbetrayal/mailboxer.git', branch: 'update_attributes-update'
gem 'sitemap_generator'          # dynamic sitemap generation
gem 'fog-aws'                    # used to deploy sitemaps
gem 'figaro'                     # runtime configuration (YAML) parsing
gem 'nokogiri'                   # XML processor
gem 'actionpack-cloudflare'      # fix request.remote_ip to use CF-Connecting-IP properly from Cloudflare
gem 'timezone'                   # used to attach timezones to users in customer.io for localized emails
gem 'geocoder'                   # used by Location to calcualate distances between points
gem 'addressable'                # for accreditation rake task and AutoCheckContentLinksJob
gem 'rubyzip'                    # for DocuSign zip bundles and sign_now.rake tasks
gem 'mail'                       # Mail::Address use for User mailing addresses
gem 'hashdiff'                   # for deep comparing complex hashes, such as cohort.periods

#####################################
## Translation
#####################################

gem 'http_accept_language'       # parse Accept_language and choose best based on supported
gem 'rails-i18n'                 # actual locale files
gem 'phonelib'                   # libphonenumber validation library
gem 'easy_translate'             # google translation tool

#####################################
## Payment
#####################################

gem 'stripe'
gem 'stripe_event'


#####################################
## Qeueing
#####################################

gem 'daemons'
gem 'delayed_job'
gem 'delayed_job_active_record'
gem 'activejob_dj_overrides'

#####################################
## Redshift
#####################################

gem 'activerecord6-redshift-adapter',  git: 'https://github.com/kwent/activerecord6-redshift-adapter', branch: 'master'


#####################################
## Production Specific
#####################################

group :production do
  gem 'unicode'   # used in import rake tasks
end


#####################################
## Development and Testing
#####################################

group :development do
  gem 'binding_of_caller'
  gem 'pry-byebug'
  gem 'bundler-audit'
  gem 'rbtrace'
  gem 'ruby-lint'
  gem 'ruby-prof'
  gem 'redcarpet'
  gem 'rak'
  gem 'rails-erd'           # for creating db diagrams for Klipfolio
  gem 'simple-spreadsheet'  # for reading xlsx files in product data rake task
  gem 'charon'              # for ip:summarize_spam script to lookup data in Spamhaus
end

group :test do
  gem 'fixturies'
  gem 'stripe-ruby-mock', '2.5.8' # 3.0.0 breaks any_instance_of mocking used to bypass lack of expand support
  gem 'rails-controller-testing' # defines `assigns` and maybe other things
end

group :development, :test do
  gem 'httpi'
  gem 'factory_bot_rails'
  gem 'rspec_junit_formatter'
  gem 'pry-rails'


  gem 'rspec-rails'
  gem 'rspec-core'

  # NOTE: as of 10/9/19 - Sprockets 4 raises if no `app/assets/config/manifest.js` assets configuration found
  gem 'sprockets', '3.7.2'

  # keep a rails process running in the background
  gem "spring"
  gem 'spring-commands-rspec', group: :development


end