const coreJsVersion = require('core-js/package.json').version;
const babelRuntimeVersion = require('@babel/runtime/package.json').version;

module.exports = {
    // Usage - https://babeljs.io/docs/en/babel-preset-env#usebuiltins-entry + https://babeljs.io/docs/en/usage
    // Lodash plugin - https://github.com/lodash/babel-plugin-lodash
    presets: [
        [
            '@babel/preset-env',
            {
                useBuiltIns: 'entry',
                corejs: coreJsVersion,
            },
        ],
        '@babel/react',
    ],
    plugins: [
        '@babel/plugin-proposal-class-properties',
        '@babel/plugin-proposal-optional-chaining',
        [
            '@babel/plugin-transform-runtime',
            {
                version: babelRuntimeVersion,
            },
        ],
        'lodash',
        'syntax-trailing-function-commas',
    ],
    env: {
        test: {
            plugins: ['require-context-hook'],
        },
    },
    ignore: ['./vendor/front-royal/modules/Certificates/scripts/'],
    overrides: [
        {
            // We import all of `core-js/stable` in the `common` bundle, but that bundle isn't
            // necessarily available on the page at the same time these are. Since there's code
            // in these bundles that also needs to be polyfilled, we use the `"useBuiltIns": "usage"`
            // option to tell Babel to polyfill everything based on usage inside these bundles.
            test: [
                './vendor/front-royal/modules/DynamicLandingPage/',
                './vendor/front-royal/modules/InlineHeadScripts/',
                './vendor/front-royal/modules/TinyEventLogger/',
                './vendor/front-royal/modules/ServerTime/',
                './vendor/front-royal/modules/logPageTimes/',
                './vendor/front-royal/modules/BuildConfig/',
                './vendor/front-royal/modules/userAgentHelper/',
                './vendor/front-royal/modules/guid/',
                './vendor/front-royal/modules/Auid/',
                './vendor/front-royal/modules/ClientStorage/',
                './vendor/front-royal/modules/FrontRoyalFetch/',
            ],
            presets: [
                [
                    '@babel/preset-env',
                    {
                        // We don't import all of core-js in these modules, and only
                        // want polyfills for what we use.
                        // https://babeljs.io/docs/en/babel-preset-env#usebuiltins-usage
                        useBuiltIns: 'usage',
                        corejs: { version: coreJsVersion, proposals: true },
                    },
                ],
                '@babel/react',
            ],
        },
    ],
};
